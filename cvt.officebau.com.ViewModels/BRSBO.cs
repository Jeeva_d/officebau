﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class Brsbo : BaseBo
    {
        public string BankName { get; set; }
        public string Source { get; set; }
        public int SourceId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ReconsiledDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Amount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? ClosingAmount { get; set; }

        public string Remarks { get; set; }
        public bool IsReconciled { get; set; }
        public Entity Bank { get; set; }
        public List<SelectListItem> BankList { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Notations { get; set; }
    }
}