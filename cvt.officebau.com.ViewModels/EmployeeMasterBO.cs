﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class EmployeeMasterBo : CustomBo
    {
        [Required(ErrorMessage = BOConstants.Enter_EmployeeNo)]
        public string Code { get; set; }

        public string TempEmployeeCode { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Login_Code)]
        public string LoginCode { get; set; }

        public string FullName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_First_Name)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Last_Name)]
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Department)]
        public int? DepartmentId { get; set; }

        public Entity Department { get; set; }
        public IEnumerable<SelectListItem> DepartmentList { get; set; }
        public string DepartmentName { get; set; }

        [Required(ErrorMessage = BOConstants.select_Designation)]
        public int? DesignationId { get; set; }

        public List<SelectListItem> DesignationList { get; set; }
        public string DesignationName { get; set; }

        // [RegularExpression(@"^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$", ErrorMessage = "Not a valid Phone number")]
        [Required(ErrorMessage = BOConstants.EnterContactNo)]
        public string ContactNo { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Emergency_MobileNo)]
        public string EmergencyContactNo { get; set; }

        public string Gender { get; set; }

        [Required(ErrorMessage = BOConstants.select_Gender)]
        public int? GenderId { get; set; }

        public List<SelectListItem> GenderList { get; set; }

        [Required(ErrorMessage =BOConstants.EnterEmailID)]
        [EmailAddress(ErrorMessage = BOConstants.InvalidEmailID)]
        public string EmailId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_DOJ)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOFJoin { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Doc { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_DOB)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Dob { get; set; }

        public DateTime? ContractExpiryDate { get; set; }
        public int? GradeId { get; set; }
        public string GradeName { get; set; }
        public List<SelectListItem> GradeList { get; set; }

        [Required(ErrorMessage = BOConstants.select_Employment_Type)]
        public int? EmploymentTypeId { get; set; }

        public List<SelectListItem> EmploymentTypeList { get; set; }
        public int? JobTypeId { get; set; }
        public List<SelectListItem> JobTypeList { get; set; }
        public int? ReportingToId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_ReportingTo)] 
        public string ReportingToName { get; set; }

        public List<SelectListItem> ReportingToList { get; set; }
        public string Qualification { get; set; }
        public List<SelectListItem> QualificationList { get; set; }

        [Required(ErrorMessage =BOConstants.select_Prefix)]
        public int PrefixId { get; set; }

        public List<SelectListItem> PrefixList { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Aadhar_Card)] 
        public string AadharId { get; set; }

        [Required(ErrorMessage =BOConstants.Select_BusinessUnit)]
        public string BusinessUnitIDs { get; set; }

        public List<SelectListItem> BusinessUnitList { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Region)]
        public int? RegionId { get; set; }

        public List<SelectListItem> RegionList { get; set; }
        public int? BandId { get; set; }
        public string BandName { get; set; }
        public List<SelectListItem> BandList { get; set; }
        public int? FunctionalId { get; set; }
        public string FunctionalName { get; set; }
        public List<SelectListItem> FunctionalList { get; set; }

        [Required(ErrorMessage = BOConstants.Select_BusinessUnit)]
        public int BaseLocationId { get; set; }

        public string BaseLocation { get; set; }
        public int? ApprovedBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? ApprovedOn { get; set; }

        public bool? IsApproved { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string ApproverRemarks { get; set; }

        public string ApproverName { get; set; }

        [Required(ErrorMessage = BOConstants.select_Reason)]
        public int? ReasonId { get; set; }

        public List<SelectListItem> ReasonList { get; set; }
        public string EmploymentType { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Inactive_From)]  
        public DateTime? InactiveFrom { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Contractor)]
        public int? ContractorId { get; set; }

        public List<SelectListItem> ContractorList { get; set; }
        public int? SecondApprovedBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? SecondApprovedOn { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string SecondApproverRemarks { get; set; }

        public string SecondApproverName { get; set; }
        public bool HasAccess { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string FileId { get; set; }
        public Guid? FileUploadId { get; set; }
        public int? PaystructureId { get; set; }
        public string FirstApproverBaseLocation { get; set; }
        public string SecondApproverBaseLocation { get; set; }
        public string PrefixType { get; set; }


        // Other Details
        public int OtherDetailId { get; set; }
        public bool IsClaimRequest { get; set; }
        public int? ApplicationRoleId { get; set; }
        public string StartScreen { get; set; }
        public int NoOfDependent { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Gross { get; set; }

        public List<SelectListItem> ApplicationRoleList { get; set; }
        public List<SelectListItem> StartScreenList { get; set; }
        public PayStructureBO PayStructureBo { get; set; }
        public StatutoryDetailsBo StatutoryDetailsBo { get; set; }
        public PersonalDetailsBo PersonalDetailsBo { get; set; }
        public List<SelectListItem> PayStubMonthList { get; set; }
        public List<CompanyAssetsBo> CompanyAssetList { get; set; }
        public List<NewsAndEventsBo> NewsandEventsList { get; set; }
        public List<DocumentDetailsBo> ExpiryAlert { get; set; }
        public string BiometricCode { get; set; }
        public List<LeaveManagementBo> LeaveManagementList { get; set; }
        public List<SelectListItem> MyPerformanceList { get; set; }

        public string IsUhApprovalRequired { get; set; }
        public string IsStatutoryRequired { get; set; }

        public string CompanyName { get; set; }
        public string Logo { get; set; }
        public Guid? LogoId { get; set; }

        public string EmpCodePattern { get; set; }
        public bool IsBiometricAccessRequired { get; set; }

        public bool IsSuperUser { get; set; }
        public  string IPAddress { get; set; }
    }

    public class StatutoryDetailsBo : BaseBo
    {
        public int StatutoryDetailId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_BankName)]
        public string BankName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Branch_Name)]
        public string BranchName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Bank_AccountNo)]
        public string AccountNo { get; set; } 

        public int? AccountTypeId { get; set; }
        public List<SelectListItem> AccountTypeList { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_PAN)]
        [RegularExpression(@"[A-Za-z]{5}\d{4}[A-Za-z]{1}", ErrorMessage = BOConstants.PAN)]
        public string PanNo { get; set; }

        public string PfNo { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_IFSC_Code)]
        public string IfscCode { get; set; }

        public string EsiNo { get; set; }
        public string Uan { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_AadharNo)]
        public string AadharId { get; set; }

        public string PolicyName { get; set; }
        public string PolicyNo { get; set; }
        public string BaseLocation { get; set; }
        public string CompanyPolicyNo { get; set; }
        public string TpInsurance { get; set; }
        public string BeneId { get; set; }
    }

    public class DocumentDetailsBo : BaseBo
    {
        //Documents
        [Required(ErrorMessage = BOConstants.Enter_Title)]
        public string Title { get; set; }

        public string Description { get; set; }
        public Guid? FileUploadId { get; set; }

        [Required(ErrorMessage = BOConstants.upload_file)]
        public string FileId { get; set; }

        public string FileName { get; set; }
        public string FilePath { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime FileCreateDate { get; set; }

        [Required(ErrorMessage =BOConstants.select_Document_Type)] 
        public int? DocumentTypeId { get; set; }

        public List<SelectListItem> DocumentTypeList { get; set; }
        public string Document { get; set; }
        public DateTime? DateOfExpiry { get; set; }
        public string CreatedByName { get; set; }
    }

    public class PersonalDetailsBo : BaseBo
    {
        //Personal Details
        public int PersonalDetailId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_DOB)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? Dob { get; set; }

        public string FatherName { get; set; }
        public int? BloodGroupId { get; set; }
        public string BloodGroup { get; set; }
        public List<SelectListItem> BloodGroupList { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public int? CountryId { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public int? StateId { get; set; }
        public List<SelectListItem> StateList { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public List<SelectListItem> CityList { get; set; }
        public int? MaritalStatusId { get; set; }
        public string MaritalStatus { get; set; }
        public List<SelectListItem> MaritalStatusList { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? Anniversary { get; set; }

        public string SpouseName { get; set; }
        public int? GenderId { get; set; }
        public List<SelectListItem> GenderList { get; set; }
        public string BaseLocation { get; set; }
        public string MotherName { get; set; }
        public int? EthnicityId { get; set; }
        public List<SelectListItem> EthnicityList { get; set; }
    }

    public class DependentDetailsBo : BaseBo
    {
        public int DependentDetailId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_DOB)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime Dob { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Name)]
        public string Name { get; set; }

        [Required(ErrorMessage = BOConstants.select_Gender)]
        public int GenderId { get; set; }

        public string Gender { get; set; }
        public List<SelectListItem> GenderList { get; set; }

        [Required(ErrorMessage = BOConstants.select_Relationship)]
        public int RelationshipId { get; set; }

        public string Relationship { get; set; }
        public List<SelectListItem> RelationshipList { get; set; }

        [AllowHtml]
        public string Remarks { get; set; }
    }

    public class HrmsDashboardBo : BaseBo
    {
        public int Headcount { get; set; }
        public int CurrentlyHired { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Compensation { get; set; }

        [DisplayFormat(DataFormatString = "{0:f3}", ApplyFormatInEditMode = true)]
        public decimal Attrition { get; set; }

        public string ReportType { get; set; }
        public int BusinessUnitId { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public int EndRangeValue { get; set; }
    }

    public class EmployeeReportBo : StatutoryDetailsBo
    {
        public int? BusinessUnitId { get; set; }
        public int? RegionId { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string BusinessUnit { get; set; }
        public string ContactNo { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string Address { get; set; }
        public string EmailId { get; set; }
        public string Region { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? RelievingDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Doj { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Dob { get; set; }

        public List<SelectListItem> BusinessUnitList { get; set; }
        public List<SelectListItem> RegionList { get; set; }
        public string FatherName { get; set; }

        [DisplayFormat(DataFormatString = "{0:f2}", ApplyFormatInEditMode = true)]
        public decimal GrossSalary { get; set; }

        public string IsActiveRecords { get; set; }
    }

    public class EmploymentDetailsBo : BaseBo
    {
        public int EmploymentDetailsId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_CompanyName)]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Last_Designation)]
        public string LastDesignation { get; set; }

        public string WorkLocation { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Code)]
        public string EmployeeCode { get; set; } 

        public string ReportingTo { get; set; }

        //  public string FinancialYear { get; set; }
        [Required(ErrorMessage =BOConstants.Select_Year)]
        public int YearId { get; set; }

        public List<SelectListItem> YearList { get; set; }
        public string Year { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Month)]
        public int MonthId { get; set; }

        public string Month { get; set; }
        public List<SelectListItem> MonthList { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Year)]
        public int ToYearId { get; set; }

        public string ToYear { get; set; }
        public List<SelectListItem> ToYearList { get; set; }
        public string ToMonth { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Month)]
        public int ToMonthId { get; set; }

        public List<SelectListItem> ToMonthList { get; set; }

        [AllowHtml]
        public string Remarks { get; set; }

        public string Duration { get; set; }
    }


    public class SkillDetailsBo : BaseBo
    {
        public int SkillDetailId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Skills)]
        public int SkillsId { get; set; }

        public List<SelectListItem> SkillList { get; set; }
        public string Skills { get; set; }

        [Required(ErrorMessage = BOConstants.select_Level)]
        public int LevelId { get; set; }

        public List<SelectListItem> LevelList { get; set; }
        public string Level { get; set; }

        public string Experience { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Year)]
        public int YearId { get; set; }

        public List<SelectListItem> YearList { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Month)]
        public int MonthId { get; set; }

        public List<SelectListItem> MonthList { get; set; }

        public string LastUsedVersion { get; set; }

        public int? LastUsedyearId { get; set; }
        public List<SelectListItem> LastUsedyearList { get; set; }
        public string LastUsedYear { get; set; }

        public string Description { get; set; }
        public string FirstName { get; set; }
        public bool IsLocked { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateofExpiry { get; set; }
        public Guid? FileUploadId { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
    }
}
