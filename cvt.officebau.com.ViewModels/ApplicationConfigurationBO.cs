﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ApplicationConfigurationBo : BaseBo
    {
        public string ConfigCode { get; set; }
        public string ConfigDescription { get; set; }
        public string ControlType { get; set; }
        public bool Checkbox { get; set; }

        [Range(0, 100, ErrorMessage = BOConstants.Textbox_Range)]
        public string Textbox { get; set; }

        [Required(ErrorMessage = BOConstants.Select_BusinessUnit)]
        public int BusinessUnitId { get; set; }

        public List<SelectListItem> BusinessUnitList { get; set; }
        public string PayrollDescription { get; set; }
        public string PayrollCode { get; set; }
        public string PayrollControlType { get; set; }
        public string PayrollConfigCode { get; set; }
        public int ComponentId { get; set; }
    }

    public class ApplicationConfigurationReportBo
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public int DomainId { get; set; }
    }
}