﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ProductMasterBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Name)]
        public string Name { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.Amount_Validation)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.Amount_Validation)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Rate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_SGST)]
        [Range(0, 14, ErrorMessage = BOConstants.Value_Range)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.Enter_Valid_SGST)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Sgst { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_CGST)]
        [Range(0, 14, ErrorMessage = BOConstants.Value_Range)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.Enter_Valid_CGST)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Cgst { get; set; }
        
        public string HsnCode { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_HSN_Code)]
        public string GsthsnCode { get; set; }

        public string Code { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.Enter_valid_Opening_Stock)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.Enter_valid_Opening_Stock)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public int? OpeningStock { get; set; }

        public decimal? AvailableStock { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Transaction_Type)]
        public int TransactionTypeId { get; set; }

        public string TransactionType { get; set; }
        public List<SelectListItem> TransactionTypeList { get; set; }

        [Required(ErrorMessage = BOConstants.select_Type)]
        public int TypeId { get; set; }
       
        public string Type { get; set; }
        public List<SelectListItem> TypeList { get; set; }
        public int Hsnid { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_IGST)]
        [Range(0, 28, ErrorMessage = BOConstants.Value_Range)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.Enter_Valid_IGST)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Igst { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Description)]
        public string GstDescription { get; set; }

        [Required(ErrorMessage = BOConstants.select_Ledger)]
        public int LedgerId { get; set; }

        public List<SelectListItem> LedgerList { get; set; }
        public string LedgerName { get; set; }

        public string SalesLedgerName { get; set; }
        public string PurchaseLedgerName { get; set; }

        public int SalesLedgerId { get; set; }
        public int PurchaseLedgerId { get; set; }

        public int? Uomid { get; set; }
        public List<SelectListItem> UomList { get; set; }
        public bool IsInventory { get; set; }
        public bool IsPurchase { get; set; }
        public bool IsSales { get; set; }
        public string SalesDescription { get; set; }
        public string PurchaseDescription { get; set; }
        public decimal? SaleRate { get; set; }
        public decimal? PurchaseRate { get; set; }
        public decimal? LowStockThresold { get; set; }
        public string Uom { get; set; }

        public int SalesCount { get; set; }
        public int PurchaseCount { get; set; }
        public int InventoryCount { get; set; }
    }
}
