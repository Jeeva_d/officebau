﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class TdsDeclarationBo : BaseBo
    {
        public int? ComponentId { get; set; }
        public int? NoOfRecords { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Window_Opens)]
        [Range(1, 31, ErrorMessage = BOConstants.Valid_Month_Open_Date)]
        public int? MonthOpenDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Window_Closes)]
        [Range(1, 31, ErrorMessage = BOConstants.Valid_Month_Close_Date)]
        public int? MonthCloseDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Engine_Process_Date)]
        public int? EngineProcessDate { get; set; }

        public int? IncomeTypeId { get; set; }
        public int? StartYear { get; set; }
        public bool HasDeleted { get; set; }
        public bool IsApprover { get; set; }
        public bool IsOpen { get; set; }
        public bool IsProofOpen { get; set; }
        public bool IsCurrentFy { get; set; }
        public bool IsCleared { get; set; }

        [Required(ErrorMessage = BOConstants.EnterCode)]
        public string Code { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Description)]
        public string Description { get; set; }

        public string ApproverRemarks { get; set; }
        public string SectionGroup { get; set; }
        public string Component { get; set; }
        public string Value { get; set; }
        public string IncomeType { get; set; }
        public string FinancialYear { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.Amount_Validation)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.Amount_Validation)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Amount { get; set; }

        //[Range(0, 100, ErrorMessage = "Please enter the valid Percentage.")]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.Valid_Percentage)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Percentage { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.valid_Declaration)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.valid_Declaration)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Declaration { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.valid_Submitted)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.valid_Submitted)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Submitted { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.valid_Cleared)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.valid_Cleared)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Cleared { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.valid_Rejected)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.valid_Rejected)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Rejected { get; set; }

        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Window_Close_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ProofCloseDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Window_Open_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ProofOpenDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Engine_Process_Time)]
        public DateTime? EngineProcessTime { get; set; }

        public Entity Section { get; set; }
        public Entity Employee { get; set; }
        public Entity RuleType { get; set; }
        public int FinancialYearId { get; set; }
        public List<SelectListItem> SectionList { get; set; }
        public List<SelectListItem> EmployeeList { get; set; }
        public List<SelectListItem> FinancialYearList { get; set; }
        public int ScreenType { get; set; }
        public string BaseLocation { get; set; }
        public int? BusinessUnitId { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
    }
}
