﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class EmployeeBillingBO : BaseBo
    {
        public int EmployeeBillingId { get; set; }

        [Required(ErrorMessage = BOConstants.SelectCustomer)]
        public int CustomerId { get; set; }

        public bool Fixed { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Hourly_Rate)]
        [Display(Name = "Hourly Rate")]
        public decimal? HourlyRate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Fixed_Rate)]
        [Display(Name = "Fixed Rate")]
        public decimal? FixedRate { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }

        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public string CustomerName { get; set; }
        [Required(ErrorMessage =BOConstants.Enter_Billing_Utilization)]
        [Display(Name = "Billing Utilization")]
        public decimal? BillingPercentage { get; set; }
        [Required(ErrorMessage = BOConstants.Enter_Remuneration_Utilization)]
        [Display(Name = "Remuneration Utilization")]
        public decimal? RemunerationPercentage { get; set; }
        public string CurrencyCode { get; set; }

    }
}

