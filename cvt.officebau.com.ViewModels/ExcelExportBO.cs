﻿namespace cvt.officebau.com.ViewModels
{
    public class ExcelExportBo : BaseBo
    {
        public string EmployeeCode { get; set; }
        public string Name { get; set; }
        public string BusinessUnit { get; set; }
        public string ContactNo { get; set; }
        public string EmailId { get; set; }
    }
}
