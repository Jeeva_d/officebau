﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class Pay_PayrollReportsBO : BaseBo
    {
        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int? MonthId { get; set; }

        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int? YearId { get; set; }

        public List<SelectListItem> MonthIdList { get; set; }
        public List<SelectListItem> YearIdList { get; set; }
        [Required(ErrorMessage =BOConstants.Select_BusinessUnit)]
        public string BusinessUnitIds { get; set; }
        public int BusinessUnitId { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public List<SelectListItem> EmployeeIDList { get; set; }
        public string Department { get; set; }
        public int DepartmentID { get; set; }
        public IEnumerable<SelectListItem> DepartmentList { get; set; }
        [Required(ErrorMessage = BOConstants.Select_Financial_Year)]
        public int FinancialYearID { get; set; }
        public List<SelectListItem> FinancialYearList { get; set; }
        public string Month { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Gross { get; set; }
        public int ComponentID { get; set; }
        public List<SelectListItem> ComponentList { get; set; }
        
        [Required(ErrorMessage = BOConstants.Select_From_Month)]
        public int? FromMonthId { get; set; }

        [Required(ErrorMessage = BOConstants.Select_From_Year)]
        public int? FromYearId { get; set; }

        [Required(ErrorMessage = BOConstants.Select_To_Month)]
        public int? ToMonthId { get; set; }

        [Required(ErrorMessage = BOConstants.Select_To_Year)]
        public int? ToYearId { get; set; }
    }
}