﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class RailRakeBo : BaseBo
    {
        //Rake Details
        public int TrainID { get; set; }

        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int CustomerID { get; set; }

        public List<SelectListItem> CustomerList { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_TEU)]
        public int TEU { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_TUE)]
        public int TUE { get; set; }

        [Required(ErrorMessage = BOConstants.EnterCustomer)]
        public string Customer { get; set; }

        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public string Type { get; set; }

        //Train Details
        [Required(ErrorMessage = BOConstants.Enter_Rake_No)]
        public string TrainNo { get; set; }
       
        public string TrainName { get; set; }
        public string SalesExecutive { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:MM}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.Enter_Import_Date)]
        public DateTime Date { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:MM}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.Enter_Export_Date)]
        public DateTime? ExportDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime? ImportDate { get; set; }

        public int? TotalTUE { get; set; }
        public int? MonthID { get; set; }
        public int? YearID { get; set; }
        public Entity BusinessUnit { get; set; }
        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public int? ExportTEU { get; set; }
        public int? ImportTEU { get; set; }
        public int BusinessUnitID { get; set; }
        public string BaseLocation { get; set; }
    }
}
