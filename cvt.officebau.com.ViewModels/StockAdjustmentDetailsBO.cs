﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class StockAdjustmentDetailsBO 
    {
        public int ID { get; set; }
        public List<SelectListItem> ItemProductList { get; set; }
        public int AdjustmentID { get; set; }
        public int ItemProductID { get; set; }
        public string ItemProductName { get; set; }
        public int ItemStockonHand { get; set; }
        public int AdjustedQty { get; set; }
        public int RemainingQty { get; set; }
        public bool IsDeleted { get; set; }
    }
}