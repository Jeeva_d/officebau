﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class InventoryBO : BaseBo
    {
        [Required(ErrorMessage = BOConstants.select_Product)]
        public int ProductID { get; set; }
        public List<SelectListItem> ProductList { get; set; }
        public string ProductName { get; set; }
        [Required(ErrorMessage =BOConstants.Enter_Quantity)]
        public int Qty { get; set; }
        [Required(ErrorMessage =BOConstants.Enter_Cost_Center)]
        public int CostCenterID { get; set; }
        public List<SelectListItem> CostCenterList { get; set; }

        [Required(ErrorMessage = BOConstants.select_Ledger)]
        public int LedgerID { get; set; }
        public List<SelectListItem> LedgerList { get; set; }

        [Required(ErrorMessage = BOConstants.select_Reason)]
        public int ReasonID { get; set; }
        public List<SelectListItem> ReasonList { get; set; }
        public string Remarks { get; set; }
        [Required(ErrorMessage = BOConstants.Select_Date)]
        public DateTime DistributedDate { get; set; }
        [Required(ErrorMessage = BOConstants.select_Employee)]
        public string EmployeeName { get; set; }
        public List<StockAdjustmentDetailsBO> StockAdjustmentDetails { get; set; }

        [Required (ErrorMessage = BOConstants.select_Adjusted_Date)]
        public DateTime? AdjustedDate { get; set; }
        public string LedgerName { get; set; }
        
        public string Reason{ get; set; }
        public int? StockonHand { get; set; }
        public int? CommitedStock { get; set; }
        public int? AvailableStock { get; set; }
        public string UOM { get; set; }
        public string Manufacturer { get; set; }
        [Required(ErrorMessage = BOConstants.select_Party)]
        public string Party { get; set; }
        public string StockType { get; set; }
        public string SourceNo { get; set; }
        public DateTime? SourceDate { get; set; }
        public int? AdjustedStock { get; set; }
        public string CostCenter { get; set; }
        public string AdjustedBy { get; set; }
        public bool IsInternal { get; set; }
        public int? LowStockThreshold { get; set; }
        public int? OutofStock { get; set; }
        public decimal? POQuantity { get; set; }
        public decimal? SOQuantity { get; set; }
    }
}