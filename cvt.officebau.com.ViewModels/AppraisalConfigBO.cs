﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class AppraisalConfigBo : BaseBo
    {
        [Required(ErrorMessage =BOConstants.Enter_AppraisalName)]
        public string AppraisalName { get; set; }

        [Required(ErrorMessage = BOConstants.Select_BusinessUnit)]
        public int BusinessUnitId { get; set; }

        public string BusinessUnit { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Appraisee_Start_Date)]
        public DateTime AppraiseeStartDate { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Appraisee_End_Date)]
        public DateTime AppraiseeEndDate { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Appraiser_Start_Date)]
        public DateTime AppraiserStartDate { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Appraiser_End_Date)]
        public DateTime AppraiserEndDate { get; set; }

        [Required(ErrorMessage =BOConstants.Select_Eligibility_Date)]
        public DateTime EligibilityDate { get; set; }
    }
}