﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class PayStructureBO : BaseBo
    {
        [Required(ErrorMessage = "Please enter the Name.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter the Gross.")]
        [Range(1, 922337203685477, ErrorMessage = "Please enter the valid Amount.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Gross { get; set; }

        [Range(1, 40, ErrorMessage = "Basic must be between 1 to 40")]
        //[Range(1, 922337203685477, ErrorMessage = "Please enter the valid Percentage.")]
        [Required(ErrorMessage = "Please enter the Basic.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Basic { get; set; }

        [Required(ErrorMessage = "Please enter the HRA.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        //[Range(1, 922337203685477, ErrorMessage = "Please enter the valid Percentage.")]
        [Range(1, 20, ErrorMessage = "HRA must be between 1 to 20")]
        public decimal? HRA { get; set; }

        [Required(ErrorMessage = "Please enter the Medical Allowance.")]
        [Range(1, 922337203685477, ErrorMessage = "Please enter the valid Amount.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? MedicalAllowance { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Loans { get; set; }

        [Required(ErrorMessage = "Please enter the Conveyance.")]
        [Range(1, 922337203685477, ErrorMessage = "Please enter the valid Amount.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Conveyance { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? SplAllow { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? EEPF { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? EEESI { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? PT { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? EPF { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? TDS { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? ERPF { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? ERESI { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? EESI { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Bonus { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Gratuity { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? PLI { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Medicalclaim { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? MobileDataCard { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? OtherPerks { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? CTC { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal OtherEarnings { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Deduction { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please enter the Effective From.")]
        public DateTime? EffictiveFrom { get; set; }

        public Entity CompanyPayStucture { get; set; }
        public List<SelectListItem> CompanyPayStuctureList { get; set; }

        [Required(ErrorMessage = "Please select the Company Paystucture.")]
        public int CompanyPayStuctureID { get; set; }

        public string EmployeeName { get; set; }
        public List<SelectListItem> EmployeeIDList { get; set; }
        public int Counts { get; set; }
        public int CheckCompanyPS { get; set; }
        public int BusinessUnitID { get; set; }
        public string BusinessUnitIDs { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public string EmployeeCode { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DOJ { get; set; }

        public string Month { get; set; }
        public int WorkingDays { get; set; }
        public decimal PresentDays { get; set; }
        public string CompanyName { get; set; }
        public string BusinessUnitAddress { get; set; }
        public bool IsPFRequired { get; set; }
        public bool IsESIRequired { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? FixedDA { get; set; }

        [Required(ErrorMessage = "Please enter the Monsoon Allowance.")]
        [Range(1, 922337203685477, ErrorMessage = "Please enter the valid Amount.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? MonsoonAllow { get; set; }

        [Range(1, 922337203685477, ErrorMessage = "Please enter the valid Amount.")]
        [Required(ErrorMessage = "Please enter the Paper and Magazine.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? PaperMagazine { get; set; }

        [Range(1, 922337203685477, ErrorMessage = "Please enter the valid Amount.")]
        [Required(ErrorMessage = "Please enter the Education Allowance.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? EducationAllowance { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? IncomeTax { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? InstalmentOnLoan { get; set; }

        public string Logo { get; set; }
        public string FilePath { get; set; }
        public string Designation { get; set; }
        public string PFNo { get; set; }
        public string ESINo { get; set; }
        public string UANNo { get; set; }
        public string PANNo { get; set; }
        public string AadharNo { get; set; }
        public string BankAccountNo { get; set; }
        public string Code { get; set; }
        public string Region { get; set; }
        public decimal? OverTime { get; set; }
        public decimal? EducationAllow { get; set; }
        public string BaseLocation { get; set; }

        //=====================
        public bool IsEducationAllow { get; set; }
        public bool IsOverTime { get; set; }
        public bool IsEEPF { get; set; }
        public bool IsEEESI { get; set; }
        public bool IsPT { get; set; }
        public bool IsTDS { get; set; }
        public bool IsERPF { get; set; }
        public bool IsERESI { get; set; }
        public bool IsGratuity { get; set; }
        public bool IsPLI { get; set; }
        public bool IsMediclaim { get; set; }
        public bool IsMobileDatacard { get; set; }

        public bool IsOtherPerks { get; set; }
        //=====================
    }
}
