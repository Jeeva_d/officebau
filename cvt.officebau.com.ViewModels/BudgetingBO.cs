﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class BudgetingBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Tag_Name)]
        public string TagName { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.Enter_Amount)]
        public decimal BudgetingAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal TotalAmount { get; set; }

        public string Type { get; set; }

        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public string BudgetType { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int BudgetNameId { get; set; }

        public List<SelectListItem> BudgetNameList { get; set; }

        //Financial
        public int FinancialYearId { get; set; }

        public List<SelectListItem> FinancialYearList { get; set; }
    }
}