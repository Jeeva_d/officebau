﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class EmployeePayStructureBo_V2 : BaseBo
    {

        [Required(ErrorMessage = BOConstants.Enter_Name)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.Name_Range)]
        public string Name { get; set; }

        [Required(ErrorMessage = BOConstants.select_Effective_From)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EffectiveFrom { get; set; }
        [Required(ErrorMessage = BOConstants.Enter_Gross)]
        [Range(1, 922337203685477, ErrorMessage = BOConstants.Amount_Validation)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Gross { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Basic { get; set; }
        public int grossId { get; set; }
        public string EmployeeCode { get; set; }

        public List<SelectListItem> CompanyPayStructureList { get; set; }

        [Required(ErrorMessage = BOConstants.select_Company_Paystructure)]  
        public int CompanyPayStructureId { get; set; }
       
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CED { get; set; }
        public string EmployeeName { get; set; }
        public List<CompanyPayStructureBo_V2> CompanyPayStructureBo { get; set; }
        public int BusinessUnitID { get; set; }
        public string BusinessUnitIDs { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public int Counts { get; set; }
        public Entity CompanyPayStucture { get; set; }
        public List<SelectListItem> CompanyPayStuctureList { get; set; }
        public List<SelectListItem> SearchEmployeeList { get; set; }
        public string ComponentCode { get; set; }
        public string Type { get; set; }
        public int? TypeID { get; set; }
        public List<SelectListItem> TypeList { get; set; }
        public int ComponentID { get; set; }
        public int? Ordinal { get; set; }
    }
}