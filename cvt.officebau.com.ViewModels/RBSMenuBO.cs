﻿namespace cvt.officebau.com.ViewModels
{
    public class RbsMenuBo : BaseBo
    {
        public string Module { get; set; }
        public string SubModule { get; set; }
        public string Menu { get; set; }

        public string Action
        {
            get
            {
                if (!string.IsNullOrEmpty(MenuUrl))
                {
                    return MenuUrl.Split('/')[1];
                }

                return string.Empty;
            }
        }

        public string Controller
        {
            get
            {
                if (!string.IsNullOrEmpty(MenuUrl))
                {
                    return MenuUrl.Split('/')[0];
                }

                return string.Empty;
            }
        }

        public string MenuId { get; set; }
        public string MenuUrl { get; set; }
        public string MenuIcons { get; set; }
        public string SubMenuIcons { get; set; }
        public string Root { get; set; }
        public string MorderId { get; set; }
        public string MsgAlert { get; set; }
        public string SubModuleUrl { get; set; }
        public int ReportMenuId { get; set; }
        public string CheckMenuId { get; set; }
        public int IsMobility { get; set; }
        public int ModuleId { get; set; }
        public string ModuleGroup { get; set; }
    }
}
