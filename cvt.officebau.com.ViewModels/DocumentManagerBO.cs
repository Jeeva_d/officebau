﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class DocumentManagerBO : BaseBo
    {

        [Required(ErrorMessage = BOConstants.select_Document_Category)]
        public int DocumentCategoryId { get; set; }
        public List<SelectListItem> DocumentCategoryList { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public List<FileInformation> FileInformationList { get; set; }
        public string Path { get; set; }
        public string CategoryName { get; set; }
        public HttpPostedFileBase[] Files { get; set; }
        public string FileName { get; set; }
        public string FirstName { get; set; }
        [Required(ErrorMessage = BOConstants.Enter_Document_Name)]
        public string DocumentName { get; set; }
        public Guid HistoryID { get; set; }
        public int FileId { get; set; }
        public int FileUploadId { get; set; }
        
        public Entity DependantDropDown { get; set; }
        public List<SelectListItem> DependantDropDownList { get; set; }
        public string CategoryId { get; set; }
        public List<SelectListItem> checkbox { get; set; }
        public string Parent { get; set; }
        public string Document { get; set; }
        public string Extension { get; set; }
    }

    public class FileInformation
    {
        public string FileName { get; set; }
        public string Path { get; set; }
    }
}