﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class BankBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_BankName)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.BankName_Range)]
        public string BankName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_DisplayName)]
        public string DisplayName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_AccountNumber)]
        [StringLength(20, MinimumLength = 5, ErrorMessage = BOConstants.AccountNumber_Length)]
        public string AccountNo { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Opening_Balance)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = BOConstants.Opening_Balance_Validation)]
        public decimal OpeningBalance { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal ClosingBalance { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Limit)]
        [Range(0, 922337203685477, ErrorMessage = BOConstants.Amount_Validation)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.Amount_Validation)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Limit { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Opening_Bal_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? OpeningBalanceDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Account_Type)]
        public Entity AccountType { get; set; }

        public List<SelectListItem> AccountTypeList { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Branch_Name)]
        public string BranchName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_IFSC_Code)]
        public string IfscCode { get; set; }

        public string SwiftCode { get; set; }
        public string MicrCode { get; set; }
    }
}