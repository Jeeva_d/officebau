﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class HolidayBo : BaseBo
    {
        public int MonthId { get; set; }

        [Required(ErrorMessage =BOConstants.Select_Year)]
        public int YearId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Region)]
        public int RegionId { get; set; }

        public bool IsOptional { get; set; }
        public string HolidayType { get; set; }
        public string Holiday { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Description)]
        public string Description { get; set; }

        public string BusinessUnit { get; set; }
        public string Type { get; set; }
        public string DocumentPath { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Full_Day)] 
        [Range(0, 24, ErrorMessage = BOConstants.Enter_valid_Full_Day)]
        public decimal? FullDay { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Half_Day)]
        [Range(0, 24, ErrorMessage = BOConstants.Enter_valid_Half_Day)]
        public decimal? HalfDay { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.EnterDate)]
        public DateTime? HolidayDate { get; set; }

        public Entity Month { get; set; }
        public int Year { get; set; }
        public string Region { get; set; }
        public List<SelectListItem> RegionList { get; set; }
        public List<SelectListItem> RegionListAll { get; set; }
        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public List<SelectListItem> TypeList { get; set; }
        public string BaseLocation { get; set; }
        public TimeSpan? BusinessStartHour { get; set; }
        public TimeSpan? BusinessEndHour { get; set; }
        public bool IsBasedOnLastCheckout { get; set; }
        public bool IsIncludeLeaveDetailsInPaystub { get; set; }
    }
}
