﻿using cvt.officebau.com.MessageConstants;
using System;
using System.ComponentModel.DataAnnotations;

namespace cvt.officebau.com.ViewModels
{
    public class CompanyPayStructureBo_V2 : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Name)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.Name_Range)]
        public string Name { get; set; }

        [Required(ErrorMessage = BOConstants.select_Effective_From)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EffectiveFrom { get; set; }

        public int ComponentId { get; set; }
        public string Description { get; set; }
        public int Ordinal { get; set; }
        public string Formula { get; set; }
        public int CompanyPayStructureId { get; set; }
        public bool IsEditable { get; set; }
        public bool ShowinPaystructure { get; set; }
        public bool PayStub { get; set; }
        public decimal? Amount { get; set; }
        public bool FixedComponent { get; set; }
        public string PayType { get; set; }

    }
}