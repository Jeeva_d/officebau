﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class BusinessUnitBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Description)]
        public string Description { get; set; }

        [Required(ErrorMessage = BOConstants.EnterBusinessUnit)]
        public string Code { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Region)]
        public int BusinessUnit { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Address)]
        public string Address { get; set; }

        public string BusinessUnitName { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
    }
}