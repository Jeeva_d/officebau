﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class SalesPlanningBO : CustomBo
    {
        public int RakeID { get; set; }
        public int CustomerID { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Planned_TEU)]
        [Range(1, 99999, ErrorMessage = BOConstants.Valid_Planned_TEU)]
        public int Expected { get; set; }
       
        public int BusinessUnitID { get; set; }
        public int SalesExecutiveID { get; set; }
        public string Description { get; set; }
        public string Customer { get; set; }
        public Entity Rake { get; set; }
        public Entity SalesExecutive { get; set; }
        public Entity BusinessUnit { get; set; }
        public List<SelectListItem> RakeList { get; set; }
        public List<SelectListItem> SalesExecutiveList { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public string ExportImportType { get; set; }
    }
}
