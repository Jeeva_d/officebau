﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class AuditBo : BaseBo
    {
        public int AuditNameId { get; set; }
        public List<SelectListItem> AuditList { get; set; }
    }
}