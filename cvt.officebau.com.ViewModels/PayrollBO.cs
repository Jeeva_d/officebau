﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class PayrollBO : BaseBo
    {
        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int? MonthID { get; set; }

        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int? YearID { get; set; }

        public List<SelectListItem> MonthIDList { get; set; }
        public List<SelectListItem> YearIDList { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Gross { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal PaidGross { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Basic { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal HRA { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal MedicalAllowance { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Conveyance { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal SplAllow { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal EducationAllowance { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal PaperMagazine { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal OverTime { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal MonsoonAllowance { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal EEPF { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal EEESI { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal PT { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal NetSalary { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal TDS { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal ERPF { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal ERESI { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Bonus { get; set; }

        public decimal PSBonus { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Gratuity { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal PLI { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Medicalclaim { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal MobileDataCard { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal OtherEarnings { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Deduction { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal FixedGross { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Loans { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal OtherPerks { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal CTC { get; set; }

        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public bool IsProcessed { get; set; }
        public bool IsApproved { get; set; }

        [Required(ErrorMessage = BOConstants.Select_BusinessUnit)]
        public string BusinessUnitIDs { get; set; }

        public int BusinessUnitID { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public int DepartmentID { get; set; }
        public string Department { get; set; }
        public IEnumerable<SelectListItem> DepartmentList { get; set; }
        public string Month { get; set; }
        public List<SelectListItem> EmployeeIDList { get; set; }
        public int Employee { get; set; }
        public int EmployeePayStructureID { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DOJ { get; set; }

        public decimal WorkingDays { get; set; }
        public decimal LOPDays { get; set; }
        public int BaseLocationID { get; set; }
        public int IsESIRequired { get; set; }

        public string Component { get; set; }
        public decimal? Percentage { get; set; }
        public decimal? Limit { get; set; }
        public decimal? Value { get; set; }

        public bool InsertFlag { get; set; }
        public string EmployeePFComponent { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employee_PF_Percentage)]
        [Range(1, 100, ErrorMessage = BOConstants.Enter_Percentage)]
        public decimal? EmployeePFPercentage { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employee_PF_Limit)]
        public decimal? EmployeePFLimit { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employee_PF_Value)]
        public decimal? EmployeePFValue { get; set; }

        public string EmployerPFComponent { get; set; }
       
        [Required(ErrorMessage = BOConstants.Enter_Employer_PF_Percentage)]
        [Range(1, 100, ErrorMessage = BOConstants.Enter_Percentage)]
        public decimal? EmployerPFPercentage { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employer_PF_Limit)]
        public decimal? EmployerPFLimit { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employer_PF_Value)]
        public decimal? EmployerPFValue { get; set; }

        public string EmployeeESIComponent { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employee_ESI_Percentage)]
        [Range(1, 100, ErrorMessage = BOConstants.Enter_Percentage)]
        public decimal? EmployeeESIPercentage { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employee_ESI_Limit)]
        public decimal? EmployeeESILimit { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employee_ESI_Value)]
        public decimal? EmployeeESIValue { get; set; }

        public string EmployerESIComponent { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employer_ESI_Percentage)]
        [Range(1, 100, ErrorMessage = BOConstants.Enter_Percentage)]
        public decimal? EmployerESIPercentage { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employer_ESI_Limit)]
        public decimal? EmployerESILimit { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employer_ESI_Value)]
        public decimal? EmployerESIValue { get; set; }

        public string EmployeeNo { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Financial_Year)]
        public int FinancialYearID { get; set; }

        public List<SelectListItem> FinancialYearList { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Min_Gross)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal MinGross { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Max_Gross)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal MaxGross { get; set; }

        public string FinancialYear { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Amount_Deducted)]
        public decimal Amount { get; set; }
        
        public string BaseLocation { get; set; }
        public DateTime DOB { get; set; }
        public string PANNo { get; set; }
        public string AccountNo { get; set; }
        public string IFSCCode { get; set; }
        public string BeneID { get; set; }

        //=====================
        public bool IsEducationAllow { get; set; }
        public bool IsOverTime { get; set; }
        public bool IsEEPF { get; set; }
        public bool IsEEESI { get; set; }
        public bool IsPT { get; set; }
        public bool IsTDS { get; set; }
        public bool IsERPF { get; set; }
        public bool IsERESI { get; set; }
        public bool IsGratuity { get; set; }
        public bool IsPLI { get; set; }
        public bool IsMediclaim { get; set; }
        public bool IsMobileDatacard { get; set; }

        public bool IsOtherPerks { get; set; }
        //=====================
    }
}
