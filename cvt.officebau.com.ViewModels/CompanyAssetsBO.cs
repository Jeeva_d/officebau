﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class CompanyAssetsBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int AssetTypeId { get; set; }

        public string Make { get; set; }
        public string Name { get; set; }
        public string SerialNumber { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_HandoverOn)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? HandoverOn { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Quantity)]
        [Range(1, 922337203685477, ErrorMessage = BOConstants.Quantity_Range)]
        public int? Qty { get; set; }

        public int CompanyAssetId { get; set; }
        public List<SelectListItem> AssetTypeList { get; set; }
        public Entity AssetsName { get; set; }
        public string ReturnedRemarks { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.Enter_ReturnedOn)]
        public DateTime? ReturnedDate { get; set; }

        [AllowHtml] public string Remarks { get; set; }

        public string BaseLocation { get; set; }
        public string AssetStatus { get; set; }
        public bool ReturnStatus { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public int BaseLocationId { get; set; }
        public string Code { get; set; }
        public string EmployeeName { get; set; }
        public string BusinessUnitName { get; set; }
        public bool? EmployeeStatus { get; set; }
        public bool? ReturnAssetStatus { get; set; }
        public string DepartmentName { get; set; }
        public int OffboardProcessID { get; set; }
    }
}