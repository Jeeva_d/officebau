﻿using System.Collections.Generic;
using System.Web.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace cvt.officebau.com.ViewModels
{
    public class TDSUpdateBo : CustomBo
    {    
        public int MonthId { get; set; }
        public int YearID { get; set; }
        public int ToMonthId { get; set; }
        public int ToYearID { get; set; }
        public int YearName { get; set; }
        public int BaseLocationID { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"^\d{0,6}(\.\d{1,2})?$", ErrorMessage = "Invalid Amount")]
        public decimal? TDSAmount { get; set; }
        public string StatusCode { get; set; }
        public string EmployeeName { get; set; }
        public string BusinessUnitIdList { get; set; }        
        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }        
    }
}