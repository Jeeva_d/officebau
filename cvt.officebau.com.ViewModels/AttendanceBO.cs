﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class AttendanceBo : BaseBo
    {
        public long AttendanceId { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }

        [AllowHtml] public string Reason { get; set; }

        public string BusinessUnitId { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime AttendanceDate { get; set; }

        public TimeSpan CheckIn { get; set; }
        public TimeSpan CheckOut { get; set; }
        public TimeSpan Duration { get; set; }
        public Entity BusinessUnit { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Month)]
        public int MonthId { get; set; }

        public List<SelectListItem> MonthList { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Year)]
        public int YearId { get; set; }

        public List<SelectListItem> YearList { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime LogDate { get; set; }

        public string PunchTime { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Server)]
        public string Server { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_UserCode)]
        public string UserCode { get; set; }

        public string Database { get; set; }
        public string Punches { get; set; }
        public string IsManual { get; set; }
    }
}