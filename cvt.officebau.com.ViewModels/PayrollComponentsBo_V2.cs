﻿using cvt.officebau.com.MessageConstants;
using System.ComponentModel.DataAnnotations;

namespace cvt.officebau.com.ViewModels
{
    public class PayrollComponentsBo_V2 : BaseBo
    {
        public string Name { get; set; }

        [Required(ErrorMessage = BOConstants.EnterCode)]
        public string Code { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Description)]
        public string Description { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Ordinal)]
        public int? Ordinal { get; set; }
    }
}