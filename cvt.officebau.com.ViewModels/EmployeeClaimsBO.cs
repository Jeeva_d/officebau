﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class EmployeeClaimsBo : BaseBo
    {
        public int ClaimId { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; }

        [Required(ErrorMessage =BOConstants.select_Category)]
        public int CategoryId { get; set; }

        public List<SelectListItem> CategoryList { get; set; }
        public int? ProjectId { get; set; }
        public int RequestMode { get; set; }
        public int HasDelete { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Claim_Amount)]
        [Range(1, 922337203685477, ErrorMessage = BOConstants.Amount_Validation)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? ApprovalAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please enter the Expense Date.")]
        public DateTime ClaimedDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? SubmittedDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? FinApprovedDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ApproverDate { get; set; }

        public int ApprovalId { get; set; }
        public int SettlerId { get; set; }
        public int FinancialApproverId { get; set; }
        public int PaymentTypeId { get; set; }
        public List<SelectListItem> PaymentTypeList { get; set; }
        public string ApprovarName { get; set; }
        public string ApproverRemarks { get; set; }
        public string SettlerName { get; set; }
        public string SettlerRemarks { get; set; }
        public List<SelectListItem> ApprovalIdList { get; set; }
        public List<SelectListItem> FinancialApproverList { get; set; }
        public List<SelectListItem> SettlerList { get; set; }
        public Entity ClaimStatusId { get; set; }
        public List<SelectListItem> ClaimStatusList { get; set; }
        public string Remarks { get; set; }
        public string BillNo { get; set; }
        public Guid? FileUploadId { get; set; }
        public string FileUpload { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public bool CheckedId { get; set; }
        public int Counts { get; set; }
        public string Requester { get; set; }
        public int RequesterId { get; set; }
        public int RequesterItemId { get; set; }
        public bool IsChecked { get; set; }
        public string PaymentMode { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string ApproverStatus { get; set; }
        public string FinApproverStatus { get; set; }
        public string BaseLocation { get; set; }

        public string ToEmail { get; set; }
        public string CcEmail { get; set; }
        public string ReceiverName { get; set; }
        public string Regards { get; set; }
        public string LoginCode { get; set; }

        public string ExpenseType { get; set; }

        [Required(ErrorMessage = BOConstants.select_Category)]
        public int ExpenseTypeId { get; set; }

        public List<SelectListItem> ExpenseTypeList { get; set; }
        public string Band { get; set; }

        [Required(ErrorMessage =BOConstants.select_Band)]
        public int BandId { get; set; }

        public List<SelectListItem> BandList { get; set; }
        public string Level { get; set; }

        [Required(ErrorMessage = BOConstants.select_Grade)]
        public int LevelId { get; set; }

        public List<SelectListItem> LevelList { get; set; }
        public string Designation { get; set; }

        [Required(ErrorMessage = BOConstants.select_Designation)]
        public int DesignationId { get; set; }

        public List<SelectListItem> DesignationList { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage =BOConstants.Enter_Metro_Amount)]
        public decimal MetroAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.Enter_NonMetro_Amount)]
        public decimal NonMetroAmount { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Destination_City)]
        public int DestinationId { get; set; }

        public List<SelectListItem> DestinationList { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? FinApprovedAmount { get; set; }

        public string PrefixCode { get; set; }
        public int PrefixId { get; set; }
        public string DestinationCity { get; set; }
        public string Employee { get; set; }
        public string ReportingToName { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public int? BusinessUnitId { get; set; }
        public int? ReportingToId { get; set; }
        public bool IsTravelClaim { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Travel_RequestNo)]
        public string TravelReqNo { get; set; }
        public int? TravelReqId { get; set; }
        public string RequestBy { get; set; }
        public List<SelectListItem> EmployeeList { get; set; }
        public bool HasHRApplicationRole { get; set; }
        public bool IsClaimsEligibilityCheckRequired { get; set; }
    }

    public class EmployeeClaimsReportBo
    {
        public int Id { get; set; }
        public string FullName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime RequestedDate { get; set; }

        public decimal Amount { get; set; }
        public decimal? ApprovalAmount { get; set; }
        public decimal? SettlerAmount { get; set; }
        public int? BusinessUnitId { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public string BusinessUnit { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? StatusId { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public string Status { get; set; }
        public string EmployeeCode { get; set; }
        public string DepartmentName { get; set; }
        public int SNo { get; set; }
        public int CategoryId { get; set; }
        public List<SelectListItem> CategoryList { get; set; }
        public int EmployeeId { get; set; }
        public string Category { get; set; }
    }
}
