﻿using cvt.officebau.com.MessageConstants;
using System;
using System.ComponentModel.DataAnnotations;

namespace cvt.officebau.com.ViewModels
{
    public class CompanyMasterBo : BaseBo
    {
        [Required(ErrorMessage =BOConstants.Enter_CompanyName)]
        public string Name { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Company_FullName)]
        public string FullName { get; set; }

        [Required(ErrorMessage = BOConstants.EnterContactNo)]
        [RegularExpression("^((\\+[ \\-]*[0-9]{0,9}[ \\-]*)|([ \\-]*\\([ \\-]*[0-9]{0,9}[ \\-]*\\)[ \\-]*)|([ \\-]*[0-9]{0,9}[ \\-]*)[ \\-]*)*?[ \\-]*[0-9]{0,9}?[ \\-]*[0-9]{0,9}?$", ErrorMessage = "Phone No is not valid.")]
        public string ContactNo { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Address)]
        public string Address { get; set; }

        [EmailAddress(ErrorMessage = BOConstants.InvalidEmailID)]
        [Required(ErrorMessage = BOConstants.EnterEmailID)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = BOConstants.InvalidEmailID)]
        public string EmailId { get; set; }

        [RegularExpression(@"[A-Za-z]{5}\d{4}[A-Za-z]{1}", ErrorMessage = BOConstants.PAN)]
        public string Panno { get; set; }

        public string Tanno { get; set; }
        public string Gstno { get; set; }
        public string Website { get; set; }
        public Guid? FileUploadId { get; set; }
        public string FileUpload { get; set; }
        public string Logo { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Payment_Favour)]
        public string PaymentFavour { get; set; }

        //[EmailAddress(ErrorMessage = "Email ID is not valid.")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = BOConstants.InValid_Communication_EmailId)]
        public string CommunicationEmailId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Employee_Code_Pattern)]
        public string EmpCodePattern { get; set; }
    }
}