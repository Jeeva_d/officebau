﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class Tdsbo : BaseBo
    {
        public string FinancialYear { get; set; }
        public decimal? ApproximateAmount { get; set; }
        public decimal? PaidAmount { get; set; }
        public string ChallanNumber { get; set; }
        public string ChallanUploadLink { get; set; }
        public string BillNumber { get; set; }
        public string BillUploadLink { get; set; }
        public DateTime? TdsPaidDate { get; set; }

        public string Reference { get; set; }

        //public string Remarks { get; set; }
        public int YearId { get; set; }
        public int Year { get; set; }
        public int MonthId { get; set; }
        public int TdsBillingId { get; set; }
        public decimal? Ctc { get; set; }
        public decimal? TdsRate { get; set; }
        public int CurrentFinancialYearId { get; set; }
        public string CurrentFinancialYear { get; set; }
        public string BankName { get; set; }
        public string BusinessUnitIDs { get; set; }
        public string BusinessUnit { get; set; }
        public string EmployeeName { get; set; }
        public List<SelectListItem> PfStatusList { get; set; }
        public List<SelectListItem> PfBranchList { get; set; }
        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
    }

    public class TdsComponentBo : BaseBo
    {
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }

        public int? FinancialYearId { get; set; }
        public List<SelectListItem> FinancialYearList { get; set; }

        public int? BusinessUnitId { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }

        [DisplayFormat(DataFormatString = "{0:D}", ApplyFormatInEditMode = false)]
        public decimal Bonus { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal Pli { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal OtherIncome { get; set; }

        public string BusinessUnit { get; set; }
        public string FinancialYear { get; set; }
    }
}
