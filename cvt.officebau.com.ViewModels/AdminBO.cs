﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class AdminBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.EnterCustomer)]
        public string CompanyName { get; set; }

        public int ActiveCount { get; set; }
        public int BusinessUnitCount { get; set; }
        public int NoOfEmployees { get; set; }

        [Required(ErrorMessage = BOConstants.EnterBusinessUnit)]
        public string BusinessUnit { get; set; }

        [Required(ErrorMessage = BOConstants.EnterEmailID)]
        [EmailAddress(ErrorMessage = BOConstants.InvalidEmailID)]
        public string EmailId { get; set; }

        [Required(ErrorMessage = BOConstants.EnterContactNo)]
        public string ContactNo { get; set; }

        public string Password { get; set; }
        public string LogoUrl { get; set; }

        public string ModuleNames { get; set; }
        public List<SelectListItem> ModulesList { get; set; }

        [Required(ErrorMessage = BOConstants.EnterVersion)]
        public string Version { get; set; }

        [Required(ErrorMessage = BOConstants.EnterBroadcastMessage)]
        public string BroadcastMessage { get; set; }

        [Required(ErrorMessage = BOConstants.EnterFrom)]
        public DateTime? FromDate { get; set; }
        
        [Required(ErrorMessage = BOConstants.EnterUntil)]
        public DateTime? ToDate { get; set; }
        
        [Required(ErrorMessage = BOConstants.SelectCustomer)]
        public string CompanyIDs { get; set; }
        public List<SelectListItem> CompanyList { get; set; }
    }
}