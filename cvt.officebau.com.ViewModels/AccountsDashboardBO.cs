﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cvt.officebau.com.ViewModels
{
    public class AccountsDashboardBO
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public decimal RevenueBooked { get; set; }
        public decimal RevenueProcessed { get; set; }
        public decimal ExpenseBooked { get; set; }
        public decimal ExpenseProcessed { get; set; }
        public decimal ExpenseIncentiveBooked { get; set; }
        public decimal ExpenseIncentiveProcessed { get; set; }

        public string Ledger { get; set; }
        public string Type { get; set; }
    }
}
