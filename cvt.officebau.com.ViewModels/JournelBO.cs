﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace cvt.officebau.com.ViewModels
{
    public class JournelBO : BaseBo
    {
        [Required(ErrorMessage =BOConstants.Enter_Journal_No)]
        [StringLength(200, MinimumLength = 1, ErrorMessage =BOConstants.Journal_No_Range)]
        public string JournelNo { get; set; }
        
        [Required(ErrorMessage = BOConstants.EnterDate)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }

        public List<JournelDetailBO> JournelDetail { get; set; }
        public decimal? Amount { get; set; }
        public bool? IsNewRecord { get; set; }
        public string Description { get; set; }
    }
}
