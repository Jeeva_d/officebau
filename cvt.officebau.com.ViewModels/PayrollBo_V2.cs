﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class PayrollBo_V2:BaseBo
    {
        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int? MonthId { get; set; }

        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int? YearId { get; set; }

        public List<SelectListItem> MonthIdList { get; set; }
        public List<SelectListItem> YearIdList { get; set; }
        [Required(ErrorMessage = BOConstants.Select_BusinessUnit)]
        public string BusinessUnitIds { get; set; }
        public int BusinessUnitId { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public List<SelectListItem> EmployeeIDList { get; set; }
    }
}