﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class JournelDetailBO //: BaseBO
    {
        public int ID { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public string Description { get; set; }
        public int NameID { get; set; }
        public string Type { get; set; }
        public List<SelectListItem> NameList { get; set; }
        public int LedgerProductID { get; set; }
        public List<SelectListItem> LedgerProductList { get; set; }
        public bool IsDeleted { get; set; }

        [AllowHtml] public string UserMessage { get; set; }
    }
}
