﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class EmployeeCompOffBo : BaseBo
    {
        public int StatusId { get; set; }
        public Entity Status { get; set; }
        public List<SelectListItem> StatusList { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please enter the Comp Off Date.")]
        public DateTime? Date { get; set; }

        [DisplayFormat(DataFormatString = "{0:hh:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan Duration { get; set; }

        public string ApproverName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ApprovedDate { get; set; }

        public string RequesterRemarks { get; set; }
        public int RequesterId { get; set; }
        public int ApproverId { get; set; }
        public List<SelectListItem> ApproverList { get; set; }
        public string StatusCode { get; set; }
       
        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string ApproverRemarks { get; set; }

        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string Prefix { get; set; }
        public string EmployeeEmail { get; set; }
        public string EmployeeCode { get; set; }

        public string ApproverPrefix { get; set; }
        public string ApproverEmail { get; set; }
        public string ApproverDesignation { get; set; }

        [Required(ErrorMessage = BOConstants.select_HR_Approver)] 
        public int HrApproverId { get; set; }

        public string HrName { get; set; }
        public string HrDesignation { get; set; }
        public string HrEmail { get; set; }
        public DateTime? HRApprovedDate { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string HrRemarks { get; set; }

        public string HrPrefix { get; set; }
        public string HrStatusCode { get; set; }
        public string RequestBy { get; set; }
        public List<SelectListItem> EmployeeList { get; set; }
        public bool HasHRApplicationRole { get; set; }
    }
}
