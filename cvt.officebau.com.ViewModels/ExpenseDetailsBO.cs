﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ExpenseDetailsBO //: BaseBO
    {
        public int ID { get; set; }
        public int ExpenseID { get; set; }
        public List<SelectListItem> LedgerList { get; set; }
        public int LedgerID { get; set; }
        public string ExpenseDetailRemarks { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }

        public bool IsDeleted { get; set; }

        [AllowHtml] public string UserMessage { get; set; }

        public decimal SGST { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal SGSTAmount { get; set; }

        public decimal CGST { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal CGSTAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal TotalAmount { get; set; }

        public int Qty { get; set; }
        public bool IsProduct { get; set; }
        public int POIDItemID { get; set; }
    }
}
