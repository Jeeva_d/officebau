﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class LeaveManagementBo : BaseBo
    {
        public byte? Duration { get; set; }
        public int NoOfDays { get; set; }
        public int StatusId { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Leave_Type)]
        public int LeaveTypeId { get; set; }

        [Required(ErrorMessage =BOConstants.Select_Approver)]
        public int ApproverId { get; set; }
        
        public int BusinessUnitId { get; set; }
        public int MonthId { get; set; }
        public int YearId { get; set; }
        public bool IsHalfDay { get; set; }
        public string StatusCode { get; set; }
        public string EmployeeName { get; set; }
        public string RequestedDays { get; set; }
        public string AlternativeContactNo { get; set; }
        public string EmployeeCode { get; set; }

        [AllowHtml] public string RequesterRemarks { get; set; }

        [AllowHtml] public string Description { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Approver)]
        public string Approver { get; set; }
       
        public string ApproverStatus { get; set; }
        public string HrStatus { get; set; }
        public string ApproverName { get; set; }
        public string TotalDays { get; set; }
        public string TotalHours { get; set; }
        public string BusinessUnitIdList { get; set; }

        [Range(0, 31, ErrorMessage = BOConstants.Enter_LOP_Days)]
        public decimal? LopDays { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Available_Leaves)]
        public byte? AvailableLeave { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Reason)]
        public string Reason { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string ApproverRemarks { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string HrRemarks { get; set; }

        public string HrName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Leave_Code)]
        public string LeaveTypeCode { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Leave_Type)]
        public string LeaveTypeName { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage =BOConstants.Enter_From_Date)]
        public DateTime? FromDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.Enter_TO_Date)]
        public DateTime? ToDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime RequestedDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ApprovedDate { get; set; }

        public string LeaveType { get; set; }
        public Entity Status { get; set; }
        public Entity Employee { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Region)]
        public int RegionId { get; set; }
       
        public string Region { get; set; }
        public List<SelectListItem> LeaveTypeList { get; set; }
        public List<SelectListItem> ApproverList { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public List<SelectListItem> EmployeeList { get; set; }
        public List<SelectListItem> RegionList { get; set; }
        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }

        [Required(ErrorMessage =BOConstants.select_HR_Approver)]
        public int HrApproverId { get; set; }

        public DateTime? HrApprovedDate { get; set; }
        public DateTime? Doj { get; set; }
        public decimal TotalLeave { get; set; }
        public decimal AvailedLeave { get; set; }
        public decimal CarryForwardLeave { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Purpose)]
        public int? PurposeId { get; set; }

        public string EmployeeEmail { get; set; }
        public string ApproverEmail { get; set; }
        public string HrEmail { get; set; }
        public string HrLoginCode { get; set; }
        public int LeavesCount { get; set; }
        public int PunchCount { get; set; }
        public int ClaimsCount { get; set; }
        public string BaseLocation { get; set; }
        public int LeavesHrApprovalCount { get; set; }
        public int ClaimsFinancialCount { get; set; }
        public string Prefix { get; set; }
        public string ApproverPrefix { get; set; }
        public string Designation { get; set; }
        public string ApproverDesignation { get; set; }
        public string HrDesignation { get; set; }
        public string IsHrApprovalRequired { get; set; }
        public string Year { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Replacement_Employee)]
        public int? ReplacementEmployeeID { get; set; }
        public string ReplacementEmployee { get; set; }
        public List<SelectListItem> ReplacementEmployeeList { get; set; }

        public bool IsAssetClearanceRequired { get; set; }
        public string LeaveRequestBy { get; set; }
        [Required(ErrorMessage = BOConstants.Enter_Vacation_Name)]
        public string VacationName { get; set; }
        public bool HasHRApplicationRole { get; set; }
        public string EmployeeCodes { get; set; }
    }

    public class MissedPunchesBo : BaseBo
    {
        public int NoOfDays { get; set; }
        public int StatusId { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Punch_Type)]
        public int PunchTypeId { get; set; }
         
        public int BusinessUnitId { get; set; }
        public string StatusCode { get; set; }
        public string EmployeeName { get; set; }

        [AllowHtml] public string Remarks { get; set; }

        public string HrApproverName { get; set; }
        public string BusinessUnitIdList { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string HrApproverRemarks { get; set; }

        public string HrName { get; set; }
        public string PunchTypeName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm:ss}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.EnterDate)]
        public DateTime? PunchDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Time)]
        public TimeSpan PunchTime { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? HrApprovedDate { get; set; }

        public string PunchType { get; set; }
        public Entity Status { get; set; }
        public Entity Employee { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Region)]
        public int RegionId { get; set; }

        public string Region { get; set; }
        public List<SelectListItem> PunchTypeList { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public List<SelectListItem> EmployeeList { get; set; }
        public List<SelectListItem> RegionList { get; set; }

        [Required(ErrorMessage = BOConstants.select_HR_Approver)]
        public int? HrApproverId { get; set; }

        public DateTime? RequestedDate { get; set; }
        public Entity EmployeePunchType { get; set; }

        public int Year { get; set; }
        public int YearName { get; set; }
        public List<SelectListItem> YearList { get; set; }

        public int MonthId { get; set; }
        public List<SelectListItem> MonthList { get; set; }

        public string BusinessUnitIDs { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }

        public string EmployeeCode { get; set; }
        public int TotalCheckIn { get; set; }
        public int TotalCheckOut { get; set; }
        public byte? Duration { get; set; }
        public string TotalDuration { get; set; }
        public int TotalMinutes { get; set; }

        public string BaseLocation { get; set; }
        public string RequestBy { get; set; }
        public bool HasHRApplicationRole { get; set; }
    }

    public class LeaveReportBo : BaseBo
    {
        public DateTime? Date { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        public bool IsHalfDay { get; set; }

        public string BusinessUnit { get; set; }
        public int BusinessUnitId { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }

        public string Status { get; set; }
        public int StatusId { get; set; }
        public List<SelectListItem> StatusList { get; set; }

        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public string LeaveType { get; set; }
        public decimal TotalLeave { get; set; }
        public string Remarks { get; set; }
        public decimal NoOfDays { get; set; }
        public string Region { get; set; }
        public int Minutes { get; set; }
        public string Hours { get; set; }

        public int YearId { get; set; }
        public List<SelectListItem> YearList { get; set; }

        public int MonthId { get; set; }
        public List<SelectListItem> MonthList { get; set; }
    }
}
