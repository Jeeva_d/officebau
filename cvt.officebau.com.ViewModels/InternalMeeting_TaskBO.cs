﻿using System;

namespace cvt.officebau.com.ViewModels
{
    public class InternalMeeting_TaskBO : BaseBo
    {
        public int IMId { get; set; }
        public int AssignedId { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public int StatusId { get; set; }
    }
}