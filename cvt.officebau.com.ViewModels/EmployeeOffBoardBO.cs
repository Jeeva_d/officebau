﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class EmployeeOffBoardBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Reason)]
        public string Reason { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Relieving_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? RelievingDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Tentative_Relieving_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? TentativeRelievingDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? InitiatedDate { get; set; }

        public int StatusId { get; set; }
        public int HrStatusId { get; set; }
        public int? ApproverId { get; set; }
        public string StatusCode { get; set; }
        public string EmployeeName { get; set; }
        public string RequesterRemarks { get; set; }
        public string Description { get; set; }
        public string Approver { get; set; }
        public string HrStatus { get; set; }
        public string ApproverName { get; set; }
        public string BusinessUnitIdList { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Remarks)]
        public string ApproverRemarks { get; set; }

        public string HrRemarks { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ApprovedDate { get; set; }

        public Entity Status { get; set; }
        public Entity Employee { get; set; }
        public Entity BusinessUnit { get; set; }
        public List<SelectListItem> ApproverList { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public List<SelectListItem> EmployeeList { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }

        [Required(ErrorMessage = BOConstants.select_HR_Approver)]  
        public int? HrApproverId { get; set; }

        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? BaseLocationId { get; set; }
        public string DepartmentName { get; set; }

        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int? RelivingReasonId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Exit_Feedback)]
        public string RelievingComments { get; set; }
        
        [Required(ErrorMessage = BOConstants.Enter_RelievedOn)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ActualRelievedDate { get; set; }

        public List<SelectListItem> RelivingReasonList { get; set; }
        public string BusinessUnitName { get; set; }
        public string ModifiedByUser { get; set; }
        public DateTime? WithdrawDate { get; set; }
        public string BaseLocation { get; set; }
        public string AssetStatus { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime Doj { get; set; }
    }
}
