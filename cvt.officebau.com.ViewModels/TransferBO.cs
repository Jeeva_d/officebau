﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class TransferBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Amount)]
        [Range(1, 922337203685477, ErrorMessage = BOConstants.Amount_Validation)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? Amount { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Voucher_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Description)]
        public string Description { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_TransferFrom)]
        public Entity TransferFrom { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_TransferTo)]
        public Entity TransferTo { get; set; }
        
        public List<SelectListItem> TransferToList { get; set; }
        public List<SelectListItem> TransferFromList { get; set; }
        public decimal OpeningBalance { get;  set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Notations { get; set; }
    }
}
