﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class LopDetailsBo : CustomBo
    {
        public int MonthId { get; set; }
        public int YearId { get; set; }
        public bool HasLopDays { get; set; }
        public decimal? LopDays { get; set; }
        public string StatusCode { get; set; }
        public string EmployeeName { get; set; }
        public string BusinessUnitIdList { get; set; }
        public string LopType { get; set; }
        public int ActualWorkDays { get; set; }
        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
    }
}
