﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class MailerBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Select_BusinessUnit)]
        public int BusinessUnitId { get; set; }

        public List<SelectListItem> BusinessUnitList { get; set; }
        public string BusinessUnit { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Event)]
        public int EventId { get; set; }

        public string Event { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_CC_Recipients)]
        public string CCrecpts { get; set; }
        
        public List<SelectListItem> EventList { get; set; }
    }
}
