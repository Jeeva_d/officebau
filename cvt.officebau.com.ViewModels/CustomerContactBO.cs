﻿using cvt.officebau.com.MessageConstants;
using System.ComponentModel.DataAnnotations;

namespace cvt.officebau.com.ViewModels
{
    public class CustomerContactBo : BaseBo
    {
        public int CustomerId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_First_Name)]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Name)]
        public string FullName { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_CustomerName)]
        public string CustomerName { get; set; }

        [EmailAddress(ErrorMessage = BOConstants.InvalidEmailID)]
        public string EmailId { get; set; }

        public int? DesignationId { get; set; }
        public int? DepartmentId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Desigination)]
        public string DesignationName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Department)]
        public string DepartmentName { get; set; }

        public string ContactNo { get; set; }
        public string AlternateNo { get; set; }
        public string BaseLocation { get; set; }
    }
}