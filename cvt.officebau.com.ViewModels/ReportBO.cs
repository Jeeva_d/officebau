﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ReportBaseBo
    {
        public bool? IsActive { get; set; }
        public int Id { get; set; }
        public int UserId { get; set; }
        public bool IsDeleted { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedOn { get; set; }

        public int CreatedBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string MenuCode { get; set; }
        public string Operation { get; set; }
        public int EmployeeId { get; set; }
        public int DomainId { get; set; }
        public int LoginId { get; set; }
        public int SNo { get; set; }
        public string EmployeeName { get; set; }
    }

    public class ReportBo : BaseBo
    {
        public int Expected { get; set; }
        public int Actual { get; set; }
        public int Target { get; set; }
        public int Committed { get; set; }
        public int Achieved { get; set; }
        public string BusinessUnit { get; set; }
        public string SalesExecutive { get; set; }
        public string Customer { get; set; }
        public string TradeType { get; set; }
        public string RakeNo { get; set; }
        public string EnquiryNo { get; set; }
        public int? MonthId { get; set; }
        public int? YearId { get; set; }
        public int? BusinessUnitId { get; set; }
        public int? RakeId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime ExportDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime ImportDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime VisitDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime LastVisit { get; set; }

        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public int Count { get; set; }
        public DateTime? Date { get; set; }
        public string Outcome { get; set; }
    }

    public class OtherReportBo : BaseBo
    {
        public string Name { get; set; }
        public int? VendorId { get; set; }
        public string BillNo { get; set; }
        public string YearlyType { get; set; }
        public string VendorName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? FromDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Outstanding { get; set; }

        public string Type { get; set; }
        public int IsCostCenter { get; set; }

        //Financial
        public Entity FinancialYear { get; set; }
        public List<SelectListItem> FinancialYearList { get; set; }
    }

    public class GstReportBo : BaseBo
    {
        public string Name { get; set; }
        public string Type { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Total { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Cgst { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Sgst { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Igst { get; set; }
    }

    public class ReportInputParamBo : ReportBaseBo
    {
        public string BusinessUnit { get; set; }
        public int? BusinessUnitId { get; set; }
        public int? DesignationId { get; set; }
        public int? DepartmentId { get; set; }
        public List<SelectListItem> DesignationList { get; set; }
        public List<SelectListItem> DepartmentList { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public bool IsActiveRecords { get; set; }
        public int? SkillsId { get; set; }
        public List<SelectListItem> SkillsList { get; set; }
        public int? BeginnerCount { get; set; }
        public int? InterMediateCount { get; set; }
        public int? ExpertCount { get; set; }
        public string Skills { get; set; }
        public int? BeginnerLevelId { get; set; }
        public int? InterMediateLevelId { get; set; }
        public int? ExpertLevelId { get; set; }
        public int? StatusId { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public string Status { get; set; }
        public string ColName { get; set; }
    }
}
