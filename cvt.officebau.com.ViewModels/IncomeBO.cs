﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class IncomeBo : BaseBo
    {
        public int CustomerId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_CustomerName)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.CustomerName_Range)]
        public string CustomerName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Party_Name)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.Party_Name_Range)]
        public string PartyName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Invoice_No)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.Invoice_No_Range)]
        public string InvoiceNo { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Description)]
        public string Description { get; set; }

        [AllowHtml] public string Remarks { get; set; }

       

        [Required(ErrorMessage = BOConstants.Enter_Billing_Address)]
        [StringLength(800, MinimumLength = 1, ErrorMessage = BOConstants.Billing_Address_Range)]
        public string BillingAddress { get; set; }

        public string ScreenType { get; set; }
        public string LocalScreenType { get; set; }
        public int IsCostCenter { get; set; }
        public string PaymentTerms { get; set; }
        public string Reference { get; set; }
        public string Status { get; set; }
       
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = BOConstants.InvalidEmailID)]
        public string CustomerEmailId { get; set; }

        [Range(typeof(decimal), "0", "922337203685477", ErrorMessage = BOConstants.Valid_Currency)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal CurrencyRate { get; set; }

        public string Currency { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Amount)]
        [Range(0, 922337203685477, ErrorMessage = BOConstants.Amount_Validation)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal TotalAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal ReceivedAmount { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Discount_Value)]
        public decimal DiscountValue { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Discount_Percentage)]
        [Range(typeof(decimal), "1", "100", ErrorMessage = BOConstants.Percentage_value_Range)]
        public decimal DiscountPercentage { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Received_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ReceivedDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Invoice_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? InvoiceDate { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Due_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? DueDate { get; set; }

        public int StatusId { get; set; }
        public int InvoicePaymentId { get; set; }
        public Entity PaymentMode { get; set; }
        public Entity Bank { get; set; }
        public Entity Ledger { get; set; }
        public Entity CurrencyId { get; set; }
        public Entity Discount { get; set; }
        public Entity TransactionType { get; set; }
        public Entity StatusType { get; set; }
        public List<SelectListItem> StatusTypeList { get; set; }
        public List<SelectListItem> DiscountList { get; set; }
        public List<SelectListItem> LedgerList { get; set; }
        public List<SelectListItem> BankList { get; set; }
        public List<SelectListItem> PaymentModeList { get; set; }
        public List<SelectListItem> CurrencyList { get; set; }
        public List<ItemDetailsBO> ItemDetail { get; set; }
        public List<InvoiceReceivableBO> ReceivableDetail { get; set; }
        public List<InvoiceHoldingsBO> InvoiceHoldingDetails { get; set; }
        public List<SelectListItem> TransactionTypeList { get; set; }
        public Entity CostCenter { get; set; }
        public List<SelectListItem> CostCenterList { get; set; }
        public string CompanyName { get; set; }
        public string CompanyLogo { get; set; }
        public string Totalinwords { get; set; }
        public string CompanyAddress { get; set; }
        public string StateName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Notations { get; set; }

        public decimal? Amount { get; set; }

        //public string Operation { get; set; }
        public string CompanyEmaild { get; set; }
        public string CompanyGstno { get; set; }
        public string CompanyContactNo { get; set; }
        public string CustomerGstNo { get; set; }
        public string CustomerState { get; set; }
        public string CreatedByName { get; set; }
        public string ToEmail { get; set; }
        public string CcEmail { get; set; }
        public string BccEmail { get; set; }
        public string PaymentFavour { get; set; }
        public string LedgerName { get; set; }
        public string FileID { get; set; }
        public string fileUpload { get; set; }

    }
}
