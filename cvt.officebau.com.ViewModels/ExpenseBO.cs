﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ExpenseBo : BaseBo
    {
        public int VendorId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Vendor_Name)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.Vendor_Name_Range)]
        public string VendorName { get; set; }

        public string BillNo { get; set; }
        public string Remarks { get; set; }
        public string FileUpload { get; set; }
        public string BillingAddress { get; set; }
        public string ScreenType { get; set; }
        public string LocalScreenType { get; set; }
        public string PaymentTerms { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Reference)]
        public string Reference { get; set; }

        public string Status { get; set; }
        public string Notations { get; set; }
        public decimal? Amount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal TotalAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal PaidAmount { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Due_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? DueDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Bill_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? BillDate { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Payment_date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? PaymentDate { get; set; }

        public int StatusId { get; set; }
        public int ExpensePayId { get; set; }
        public int IsCostCenter { get; set; }
        public Entity PaymentMode { get; set; }
        public Entity Bank { get; set; }
        public Entity StatusType { get; set; }
        public Entity Type { get; set; }
        public List<SelectListItem> StatusTypeList { get; set; }
        public List<SelectListItem> ScreenTypeList { get; set; }
        public List<SelectListItem> BankList { get; set; }
        public List<SelectListItem> PaymentModeList { get; set; }
        public List<BillPayBo> BillPay { get; set; }
        public List<ExpenseDetailsBO> ExpenseDetail { get; set; }
        public List<ExpenseHoldingBO> ExpenseHoldingBOs { get; set; }

        public List<ExpenseItemsBO> ExpenseItems { get; set; }
        public HttpPostedFileBase FileName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Description)]
        public string Description { get; set; }

        //Financial Year
        public Entity FinancialYear { get; set; }
        public List<SelectListItem> FinancialYearList { get; set; }
        public Entity CostCenter { get; set; }
        public List<SelectListItem> CostCenterList { get; set; }
        [Required(ErrorMessage = BOConstants.Enter_Cost_Center)]
        public int CostCenterId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string FileId { get; set; }
        public string CreatedByName { get; set; }
        public string PoiDs { get; set; }
        public string PoNos { get; set; }
        public string PoRemarks { get; set; }
        public string Tag { get; set; }
    }
}
