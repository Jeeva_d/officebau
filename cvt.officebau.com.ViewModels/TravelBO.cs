﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class TravelBO : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Travel_RequestNo)]
        public string TravelReqNo { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Travel_Type)]
        public int TravelTypeId { get; set; }

        public string TravelType { get; set; }
        public List<SelectListItem> TravelTypeList { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Mode_of_Transport)]
        public int ModeOfTransportId { get; set; }
        
        public string ModeOfTransport { get; set; }
        public List<SelectListItem> ModeOfTransportList { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Purpose_of_Travel)]
        public string PurposeOfTravel { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Travel_From)]
        public string TravelFrom { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Travel_To)]
        public string TravelTo { get; set; }

        public string EmployeeName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.Enter_Travel_Date)]
        public DateTime TravelDate { get; set; }

        public DateTime? ReturnDate { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Currency)]
        public int CurrencyId { get; set; }

        public List<SelectListItem> CurrencyList { get; set; }

        //[RegularExpression(@"[0-9]{0,}\.[0-9]{0,}", ErrorMessage = "The Total Amount must be a number.")]
        [Range(10, 99999999, ErrorMessage = BOConstants.Total_Amount_Validation)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.Enter_Total_Amount)]
        public decimal TotalAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ApprovedDate { get; set; }

        //[Required(ErrorMessage = "Please select the Approver.")]
        public int ApproverId { get; set; }
        public List<SelectListItem> ApproverList { get; set; }
        public string Approver { get; set; }


        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string ApproverRemarks { get; set; }

        public int? ApproverStatusId { get; set; }

        public int? NoOfSubordinates { get; set; }
        public int NoOfDays { get; set; }

        public List<Subordinates> SubordinatesList { get; set; }

        [Required(ErrorMessage = BOConstants.select_status)]
        public int StatusId { get; set; }

        public string Status { get; set; }
        public List<SelectListItem> StatusList { get; set; }

        public List<SelectListItem> EmployeeList { get; set; }

        [AllowHtml] public string Notes { get; set; }

        public Guid? AttachmentFileId { get; set; }
        public string AttachmentFileName { get; set; }

        public string EmployeeEmail { get; set; }
        public string ApproverEmail { get; set; }
        public string HREmail { get; set; }
        public string HRLoginCode { get; set; }

        public string Prefix { get; set; }
        public string ApproverPrefix { get; set; }
        public string Designation { get; set; }
        public string ApproverDesignation { get; set; }
        public string HRDesignation { get; set; }
        public string IsHRApprovalRequired { get; set; }

        public bool IsBillableToCustomer { get; set; }
        public string RequestBy { get; set; }
        public bool HasHRApplicationRole { get; set; }
    }

    public class Subordinates
    {
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
