﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class FandFBo : BaseBo
    {
        [Required(ErrorMessage =BOConstants.Enter_Employee_Name)]
        public string EmployeeName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfJoining { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Last_Working_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime LastWorkingDate { get; set; }

        public string YearsOfService { get; set; }

        public int? TotalDaysInMonth { get; set; }
        public int? DaysPaid { get; set; }

        public string Status { get; set; }
        public int StatusId { get; set; }
        public List<SelectListItem> StatusList { get; set; }

        public string PFNumber { get; set; }
        public string UANumber { get; set; }
        public string Designation { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Reason_for_Leaving)]
        public string ReasonForLeaving { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Notice_Days)]
        public int? NoticeDays { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Leave_Encashed_Days)]
        public int? LeaveEncashedDays { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_LOP_Days)]
        public int? LOPDays { get; set; }
       
        [Required(ErrorMessage =BOConstants.select_HR_Approver)]
        public int HRApproverId { get; set; }
        public string HRApprover { get; set; }
        public List<SelectListItem> HRApproverList { get; set; }

        public string HRStatus { get; set; }
        public int HRStatusID { get; set; }       

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? HrApprovedDate { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string HrApproverRemarks { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Remarks)]
        public string Remarks { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal NetAmountPayable { get; set; }

        public List<FandFComponentDetailsBo> FandFComponentDetailsBoList { get; set; }
        public string HRName { get; set; }
    }

    public class FandFComponentDetailsBo
    {
        public int Id { get; set; }
        public int FandFId { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public decimal Amount { get; set; }
        public bool IsEditableAmount { get; set; }

        public string Remarks { get; set; }
        public bool IsEditableRemarks { get; set; }

        public bool IsDeleted { get; set; }
        public int Ordinal { get; set; }
    }
}