﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class NewsAndEventsBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Event_Name)]
        public string Name { get; set; }
        [Required(ErrorMessage = BOConstants.Select_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EventDate { get; set; }
        [UIHint("tinymce_jquery_full")]
        [AllowHtml]
        [Required(ErrorMessage = BOConstants.Enter_Event_Description)]
        public string Description { get; set; }

        public HttpPostedFileBase[] Files { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate { get; set; }

        [Required(ErrorMessage = BOConstants.Select_BusinessUnit)]
        public string BusinessUnitId { get; set; }

        public List<SelectListItem> BusinessUnitList { get; set; }
        public List<FileInfarmation> FileInfarmationList { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        public string Path { get; set; }
    }

    public class FileInfarmation
    {
        public string FileName { get; set; }
        public string Path { get; set; }
    }
}
