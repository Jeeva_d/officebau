﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class CustomerBo : BaseBo
    {
        public string Name { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Customer_Enquiry_Name)]
        public string EnquiryName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_CustomerName)]
        public string CustomerName { get; set; }

        public string OrderName { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? StateId { get; set; }
        public string StateName { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PhoneNo { get; set; }
        public string TypeofBusiness { get; set; }

        [Range(1, 99999, ErrorMessage =BOConstants.Business_Volume)]
        public string MonthlyBusinessVolume { get; set; }

        [Required(ErrorMessage = BOConstants.select_status)]
        public int StatusId { get; set; }

        public string StatusName { get; set; }
        public List<SelectListItem> StatusList { get; set; }

        [Required(ErrorMessage = BOConstants.select_Sales_Executive)]
        public int? SalesPersonId { get; set; }

        public string SalesPersonName { get; set; }
        public List<SelectListItem> SalesPersonList { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> StateList { get; set; }
        public List<SelectListItem> CityList { get; set; }
        public int CustomerCount { get; set; }

        [Required(ErrorMessage = BOConstants.EnterContactNo)]
        public string ContactNo { get; set; }

        [Required(ErrorMessage = BOConstants.EnterEmailID)]
        [EmailAddress(ErrorMessage = BOConstants.InvalidEmailID)]
        public string EmailId { get; set; }

        public string CreatedByName { get; set; }

        public string BaseLocation { get; set; }
        ////----------------------------
        ////[Required(ErrorMessage = "Please enter the Customer Name.")]
        //public string Address { get; set; }
        //[Required(ErrorMessage = "Please enter the Payment Terms.")]
        //public string PaymentTerms { get; set; }
        //public string ContactPerson { get; set; }
        //public string ContactPersonNo { get; set; }
        //[RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Email ID is not valid.")]
        //public string CustomerEmailID { get; set; }
        //public string BillingAddress { get; set; }
        //public string PANNo { get; set; }
        //public string TANNo { get; set; }
        //public string GSTNo { get; set; }
        //public string Remarks { get; set; }
        //public Entity City { get; set; }
        //public List<SelectListItem> Citylist { get; set; }
        //public Entity Currency { get; set; }
        //public List<SelectListItem> CurrencyList { get; set; }
    }

    public class CustomerDetailsBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_CustomerName)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.CustomerName_Range)]
        public string Name { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Payment_Terms)]
        public string PaymentTerms { get; set; }

        public string ContactNo { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonNo { get; set; }

        [Required(ErrorMessage = BOConstants.EnterEmailID)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = BOConstants.InvalidEmailID)]
        public string CustomerEmailId { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = BOConstants.InvalidEmailID)]
        public string EmailId { get; set; }

        public string BillingAddress { get; set; }

        [RegularExpression(@"[A-Za-z]{5}\d{4}[A-Za-z]{1}", ErrorMessage = BOConstants.PAN)]
        public string PanNo { get; set; }

        public string TanNo { get; set; }
        public string GstNo { get; set; }
        public string Remarks { get; set; }
        public Entity City { get; set; }
        public List<SelectListItem> Citylist { get; set; }
        public Entity Currency { get; set; }
        public List<SelectListItem> CurrencyList { get; set; }
        public Entity BankID { get; set; }
        public List<SelectListItem> BankList { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Payment_Favour)]
        public string PaymentFavour { get; set; }
    }
}
