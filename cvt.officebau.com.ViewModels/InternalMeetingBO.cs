﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class InternalMeetingBO : BaseBo
    {
        [Required(ErrorMessage = BOConstants.EnterTitle)]
        public string Title { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Meeting_Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? MeetingDate { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Meeting_Time)]
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm tt}")]
        public TimeSpan MeetingTime { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Location)]
        public string Location { get; set; }

        [Required(ErrorMessage = BOConstants.select_Organized_By)]
        public int OrganizerId { get; set; }

        public int TypeId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? NextMeetingDate { get; set; }

        [Required(ErrorMessage = BOConstants.select_Meeting_Type)]
        public int? MeetingTypeId { get; set; }

        public int? PresenterId { get; set; }
        public string Attendees { get; set; }
        public string[] AttendeesArray { get; set; }

        public string Description { get; set; }
        public List<SelectListItem> EmployeeList { get; set; }
        public List<SelectListItem> MeetingTypeList { get; set; }
        public string BaseLocation { get; set; }
    }
}