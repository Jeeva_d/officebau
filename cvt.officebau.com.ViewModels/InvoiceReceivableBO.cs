﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class InvoiceReceivableBO //: BaseBO
    {
        public int ID { get; set; }
        public int InvoiceID { get; set; }
        public string InvoiceNo { get; set; }
        public string Description { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal TotalAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal ReceivedAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal ReceivableAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal OutstandingAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal HoldingAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? DueDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? InvoicedDate { get; set; }

        public bool IsChecked { get; set; }
        public bool IsDeleted { get; set; }
        public int HoldingsCount { get; set; }

        [AllowHtml] public string UserMessage { get; set; }
    }
}
