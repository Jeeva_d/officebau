﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ApplicationConfigBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Select_Currency)]
        public string CurrencyName { get; set; }

        public List<SelectListItem> CurrencyList { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Start_Month)]
        public string StartMonthName { get; set; }

        public List<SelectListItem> StartMonthList { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsCostCenter { get; set; }
        public string Value { get; set; }
        public int CostcenterCount { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_NoOfRecords)]
        [Range(5, 500, ErrorMessage = BOConstants.NoOfRecords_Range)]
        public string NoOfRecords { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_To_Address)]
        public string To { get; set; }

        public string Cc { get; set; }
        public bool IsAttachment { get; set; }
        public string Type { get; set; }
    }
}