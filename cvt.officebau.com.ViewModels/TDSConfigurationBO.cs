﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class TdsConfigurationBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Select_Financial_Year)]
        public int FinancialYearId { get; set; }

        public List<SelectListItem> FinancialYearList { get; set; }
        public string FinancialYear { get; set; }
        public string Section { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Ceiling_Amount)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = BOConstants.Ceiling_Amount_Validation)]
        [Range(1, 10000000, ErrorMessage = BOConstants.Amount_Validation)]
        public decimal Limit { get; set; }
        
        public List<SelectListItem> SectionList { get; set; }

        [Required(ErrorMessage = BOConstants.select_Section)]
        public int SectionId { get; set; }

        public string BaseLocation { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Region)]
        public int RegionId { get; set; }

        public List<SelectListItem> RegionList { get; set; }
        public string Region { get; set; }
        public string Values { get; set; }
        public int PayrollComponentID { get; set; }
        public List<SelectListItem> PayrollComponentList { get; set; }
        [Required(ErrorMessage = BOConstants.select_CTC)]
        public int CTCComponentID { get; set; }
        [Required(ErrorMessage = BOConstants.select_Basic)]
        public int BasicComponentID { get; set; }
        [Required(ErrorMessage = BOConstants.select_Medical)]
        public int MedicalComponentID { get; set; }
        [Required(ErrorMessage = BOConstants.select_Conveyance)]
        public int ConveyanceComponentID { get; set; }
        [Required(ErrorMessage = BOConstants.select_HRA)]
        public int HRAComponentID { get; set; }
    }
}
