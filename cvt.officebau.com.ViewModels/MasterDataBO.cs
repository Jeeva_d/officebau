﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class MasterDataBo : BaseBo
    {
        [Required(ErrorMessage =BOConstants.EnterCode)]
        public string Code { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Description)]
        public string Description { get; set; }

        public string Type { get; set; }
        public string Menu { get; set; }
        public string Url { get; set; }
        public string Parameters { get; set; }
        public string MainTable { get; set; }
        public string DependantTable { get; set; }

        [Required(ErrorMessage = BOConstants.select_Type)]
        public int? TypeId { get; set; }

        public string TypeName { get; set; }
        public Entity DependantDropDown { get; set; }
        public List<SelectListItem> DependantDropDownList { get; set; }
    }
}
