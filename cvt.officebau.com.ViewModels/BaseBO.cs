﻿using cvt.officebau.com.MessageConstants;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class BaseBo
    {
        public bool IsActive { get; set; }
        public Entity Entity { get; set; }
        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }

        [AllowHtml] public string UserMessage { get; set; }

        [AllowHtml] public string Emailhtml { get; set; }

        public bool IsDeleted { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedOn { get; set; }

        public int CreatedBy { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
        public string MenuCode { get; set; }
        public string Operation { get; set; }

        [Required(ErrorMessage = BOConstants.select_Employee)]
        public int EmployeeId { get; set; }

        public int DomainId { get; set; }
        public string Guid { get; set; }
        public int LoginId { get; set; }
        public int SNo { get; set; }
       
    }

    [Serializable]
    public class Entity
    {
        public DateTime? CED { get; set; }

        [Required(ErrorMessage = BOConstants.select_DropDown)]
        public int? Id { get; set; }

        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class CustomBo : BaseBo
    {
        public string Designation { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Doj { get; set; }

        public string Parameter { get; set; }
        public string Parameter1 { get; set; }
        public string Parameter2 { get; set; }
        public string Parameter3 { get; set; }
    }

    public class FileUpload
    {
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileType { get; set; }
        public byte[] FileContent { get; set; }
    }
}