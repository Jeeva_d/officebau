﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class VendorMasterBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Vendor_Name)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.Vendor_Name_Range)]
        public string Name { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Payment_Terms)]
        public string PaymentTerms { get; set; }

        public string ContactNo { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonNo { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = BOConstants.InvalidEmailID)]
        public string EmailId { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = BOConstants.InvalidEmailID)]
        public string VendorEmailId { get; set; }

        public string BillingAddress { get; set; }

        [RegularExpression(@"[A-Za-z]{5}\d{4}[A-Za-z]{1}", ErrorMessage = BOConstants.PAN)]
        public string PanNo { get; set; }

        public string TanNo { get; set; }
        public string TinNo { get; set; }
        public string CstNo { get; set; }
        public string Remarks { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public List<SelectListItem> Statelist { get; set; }
        public List<SelectListItem> Citylist { get; set; }
        public Entity Country { get; set; }
        public Entity State { get; set; }
        public Entity City { get; set; }
        public Entity Currency { get; set; }
        public List<SelectListItem> CurrencyList { get; set; }
        public string GstNo { get; set; }
    }
}
