﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cvt.officebau.com.ViewModels
{
    public class EmployeeTask:BaseBo
    {
        public int TaksID { get; set; }
        public string Description { get; set; }
        public int StatusId { get; set; }
        public decimal HoursWorked { get; set; }
    }
}