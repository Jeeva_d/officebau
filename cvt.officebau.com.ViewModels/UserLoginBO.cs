﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class UserLoginBo : CustomBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Password)]
        [RegularExpression(@"^\S+(?:\s+\S+)?$", ErrorMessage = BOConstants.Space_Validation)]
        public string Password { get; set; }

        public bool LogOutTimeFlag { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_New_Password)]
        [RegularExpression(@"^\S+(?:\s+\S+)?$", ErrorMessage = BOConstants.Space_Validation)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Confirm_Password)]
        public string ConfirmPassword { get; set; }
       
        public string CompanyName { get; set; }
        public string LastLogin { get; set; }
        public string CurrencySymbol { get; set; }
        public string FilePath { get; set; }
        public Guid? FileId { get; set; }
        public string Module { get; set; }
        public string SubModule { get; set; }
        public string Menu { get; set; }
        public string MenuUrl { get; set; }
        public string FirstName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string LoginTime { get; set; }
        public string LogoutTime { get; set; }
        public string BrowserName { get; set; }
        public string IpAddress { get; set; }
        public string FormName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime LoginHistoryTime { get; set; }

        public int FormHitCount { get; set; }
        public bool IsPasswordChanged { get; set; }
        public List<SelectListItem> CompanyList { get; set; }
        public int CompanyId { get; set; }
        public string CompanyIdList { get; set; }
        public string Gender { get; set; }
        public int Pin { get; set; }
    }
}
