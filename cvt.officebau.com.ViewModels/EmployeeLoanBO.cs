﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class EmployeeLoanBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.select_Loan_Type)]
        public int? TypeId { get; set; }

        public string TypeName { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Amount)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [Range(1, 922337203685477, ErrorMessage = BOConstants.Amount_Validation)]
        public decimal Amount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? RateofInterest { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? TotalAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? MaxAmount { get; set; }

        public string Reason { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Tenure)] 
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public int Tenure { get; set; }

        public DateTime? ExpectedDate { get; set; }
        public int ApproverId { get; set; }
        public string ApproverName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ApprovedDate { get; set; }

        public string ApprovedReason { get; set; }
        public int? FinApproverId { get; set; }
        public string FinApproverName { get; set; }
        public DateTime? FinApprovedDate { get; set; }
        public string FinApprovedReason { get; set; }
        public DateTime RequestedDate { get; set; }
        public decimal? Emi { get; set; }
        public Entity PaymentMode { get; set; }
        public List<SelectListItem> PaymentModeList { get; set; }
        public string Requester { get; set; }
        public string Description { get; set; }
        public int LoanId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_ReferenceNo)]
        public string ReferenceNo { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = BOConstants.Enter_Date_of_Realization)]  
        public DateTime? DateOfRealization { get; set; }

        public string StatusName { get; set; }
        public Entity LoanStatusId { get; set; }
        public List<SelectListItem> LoanStatusList { get; set; }
        public int RequesterId { get; set; }
        public int StatusId { get; set; }
        public int SettlerId { get; set; }
        public List<SelectListItem> SettlerList { get; set; }
        public string ApproverStatus { get; set; }
        public string FinApproverStatus { get; set; }
        public int LoanExistsCheck { get; set; }
        public int BusinessUnitId { get; set; }

        public int ConfigurationId { get; set; }

        [Required(ErrorMessage = BOConstants.select_Band)]
        public int BandId { get; set; }

        public string BandName { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Financial_charges_ROI)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = BOConstants.Financial_charges_ROI_Check)]
        public decimal InterestRate { get; set; }

        public List<SelectListItem> BandListItem { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Payment_date)]
        public DateTime? PaymentDate { get; set; }

        public string BaseLocation { get; set; }
        public decimal Gross { get; set; }

        [Required(ErrorMessage = BOConstants.select_Grade)]
        public int GradeId { get; set; }

        [Required(ErrorMessage = BOConstants.select_Designation)]
        public int DesignationId { get; set; }

        public string GradeName { get; set; }
        public string DesignationName { get; set; }
        public List<SelectListItem> BandList { get; set; }
        public List<SelectListItem> GradeList { get; set; }

        public List<SelectListItem> DesignationList { get; set; }

        //[Required(ErrorMessage = "Please enter the Financial charges and ROI.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = BOConstants.Financial_charges_ROI_Forclosure)]
        public decimal? ForecloseInterestRate { get; set; }

        public int ForeCloseMonth { get; set; }
        public decimal DueBalance { get; set; }
    }

    public class EmployeeLoanReportBo
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string LoanType { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }

        public decimal OutstandingAmount { get; set; }
        public int? BusinessUnitId { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public string BusinessUnit { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? StatusId { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        public string Status { get; set; }
        public string ApproverName { get; set; }
        public int? ApproverId { get; set; }
        public string FinApproverName { get; set; }
        public string RepaymentStatus { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime PaidDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime RequestedDate { get; set; }

        public int EmployeeId { get; set; }
        public int? TypeId { get; set; }
        public string ApproverStatus { get; set; }
        public string FinApproverStatus { get; set; }
        public string EmployeeCode { get; set; }
        public int SNo { get; set; }
        public string DepartmentName { get; set; }
    }
}