﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class GroupLedgerBo : BaseBo
    {
        public Entity ReportType { get; set; }

        [Required(ErrorMessage = BOConstants.select_Type)]
        public List<SelectListItem> ReportTypeIdList { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Group)]  
        public string Group { get; set; }

        public string Description { get; set; }
    }
}
