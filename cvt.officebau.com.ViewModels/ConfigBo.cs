﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ConfigBo
    {
        public int ComponentID { get; set; }
        public List<SelectListItem> PayStructureComponentList { get; set; }
    }
}