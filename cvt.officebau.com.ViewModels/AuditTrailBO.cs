﻿using cvt.officebau.com.MessageConstants;
using System;
using System.ComponentModel.DataAnnotations;

namespace cvt.officebau.com.ViewModels
{
    public class AuditTrailBo
    {
        [Required(ErrorMessage = BOConstants.Enter_TableName)]
        public string TableName { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}