﻿using System;

namespace cvt.officebau.com.ViewModels
{
    public class TdsCalculationBo
    {
        public decimal? Ctc { get; set; }
        public decimal? Pt { get; set; }
        public decimal? Bonus { get; set; }
        public decimal? PLI { get; set; }
        public decimal? OtherIncome { get; set; }
        public decimal? HRA { get; set; }
        public decimal? Medicalperannum { get; set; }
        public decimal? ConveyanceAnnaum { get; set; }
        public decimal? ProCTC { get; set; }
        public decimal? ProPT { get; set; }
        public decimal? ProMedicalperannum { get; set; }
        public decimal? ProConveyanceAnnaum { get; set; }
        public decimal? ActCTC { get; set; }
        public decimal? ActPT { get; set; }
        public decimal? ActMedicalperannum { get; set; }
        public decimal? ActConveyanceAnnaum { get; set; }
        public decimal? tax80CC { get; set; }
        public decimal? tax80D { get; set; }
        public decimal? tax80U { get; set; }
        public decimal? tax80TTA { get; set; }
        public decimal? tax80TTB { get; set; }
        public decimal? tax20 { get; set; }
        public decimal? tax80G { get; set; }
        public decimal? taxHouse { get; set; }
        public decimal? OtherTaxDetails { get; set; }
        public decimal? tax80E { get; set; }
        public decimal? ITPaid { get; set; }
        public decimal? TotalTax { get; set; }
        public int NoMonth { get; set; }
        public DateTime? DOJ { get; set; }
        public int? FY { get; set; }
        public string Month { get; set; }
        public string Name { get; set; }
        public string PanNo { get; set; }
        public int IsProof { get; set; }
        public decimal? Surcharge { get; set; }
        public int CessPrecentage { get; set; }
        public decimal? StandardDeductionAnnaum { get; set; }
        public decimal? ProStandardDeductionAnnaum { get; set; }
        public decimal? ActStandardDeductionAnnaum { get; set; }
        public int TDSExemptionMonths { get; set; }
    }
}
