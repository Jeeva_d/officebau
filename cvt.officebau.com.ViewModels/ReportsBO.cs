﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ReportsBo : BaseBo
    {
        public string Name { get; set; }
        public int? VendorId { get; set; }
        public string BillNo { get; set; }
        public string YearlyType { get; set; }
        public string VendorName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd-MMM-yy}", ApplyFormatInEditMode = true)]
        public DateTime? FromDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Outstanding { get; set; }

        public string Type { get; set; }
        public int IsCostCenter { get; set; }

        //Financial
        public int? FinancialYearId { get; set; }
        public Entity FinancialYear { get; set; }
        public List<SelectListItem> FinancialYearList { get; set; }
    }
}
