﻿using cvt.officebau.com.MessageConstants;
using System.ComponentModel.DataAnnotations;

namespace cvt.officebau.com.ViewModels
{
    public class QuestionnaireBo : BaseBo
    {
        public int QuestionId { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Question)]
        public string Question { get; set; }

        public string Remarks { get; set; }
    }
}
