﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class SearchParamsBo : BaseBo
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int LedgerId { get; set; }
        public string LedgerName { get; set; }
        public Entity LedgerListId { get; set; }
        public List<SelectListItem> LedgerList { get; set; }
        public string SearchParameter { get; set; }
        public bool IsReconciled { get; set; }
        public Entity Bank { get; set; }
        public List<SelectListItem> BankList { get; set; }
        public string Amount { get; set; }
        public string Notations { get; set; }
        public Entity NotationsType { get; set; }
        public int VenderId { get; set; }
        public string VendorName { get; set; }
        public int StatusId { get; set; }
        public Entity StatusType { get; set; }
        public List<SelectListItem> StatusTypeList { get; set; }
        public string Status { get; set; }
        public string ScreenType { get; set; }
        public Entity Type { get; set; }
        public int IsCostCenter { get; set; }
        public Entity CostCenter { get; set; }
        public List<SelectListItem> CostCenterList { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string PartyName { get; set; }
        public int CostCenterId { get; set; }
        public string Event { get; set; }
        public string FinancialYear { get; set; }
        public Entity FinancialYearType { get; set; }
        public string ColumnName { get; set; }
        public int StartMonth { get; set; }
        public string Tag { get; set; }
    }
}
