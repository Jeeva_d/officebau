﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ItemDetailsBO //: BaseBO
    {
        public int ID { get; set; }
        public int InvoiceID { get; set; }
        public List<SelectListItem> ProductList { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }

        public string ItemRemarks { get; set; }
        public int QTY { get; set; }
        public decimal Rate { get; set; }
        public decimal SGST { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal SGSTAmount { get; set; }

        public decimal CGST { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal CGSTAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }

        public int IsProduct { get; set; }
        public bool IsDeleted { get; set; }
        public string UserMessage { get; set; }
        public bool IsChecked { get; set; }
        public int SOItemID { get; set; }
    }
}
