﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class CostCenterBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Enter_Code)]
        public string Code { get; set; }

        [Required(ErrorMessage =BOConstants.Enter_Description)]
        public string Description { get; set; }

        public Entity CostCenterType { get; set; }
        public List<SelectListItem> CostCenterTypeList { get; set; }
    }
}
