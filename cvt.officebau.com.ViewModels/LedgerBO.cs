﻿using cvt.officebau.com.MessageConstants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class LedgerBo : BaseBo
    {
        public Entity Group { get; set; }
        public List<SelectListItem> GroupIdList { get; set; }

        [Required(ErrorMessage = BOConstants.Enter_Ledger)]
        [StringLength(100, MinimumLength = 1, ErrorMessage = BOConstants.Account_Range)]
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsExpense { get; set; }

        //[Range(0, 922337203685477, ErrorMessage = "Please enter the valid Amount.")]
        //[RegularExpression("^[0-9\\.]*$", ErrorMessage = "Please enter the valid Amount.")]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = BOConstants.Opening_Balance_Validation)]
        public decimal? OpeningBalance { get; set; }
        
        [Required(ErrorMessage = BOConstants.Enter_Name)]
        public string GroupName { get; set; }

        public string LedgerType { get; set; }
        public decimal? Sgst { get; set; }

        public decimal? Cgst { get; set; }

        //[Required(ErrorMessage = "Please select the HSN Code.")]
        public string HsnCode { get; set; }
        public int Hsnid { get; set; }
        public decimal? Igst { get; set; }
    }
}
