﻿namespace cvt.officebau.com.ViewModels
{
    public class OrganisationChartBo : BaseBo
    {
        public int NodeID { get; set; }
        public int ParentID { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string EmailID { get; set; }
        public string ContactNo { get; set; }
        public string ProfileImages { get; set; }
        public string Gender { get; set; }
    }
}
