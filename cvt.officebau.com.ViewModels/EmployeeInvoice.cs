﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class EmployeeInvoice : BaseBo
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        [Required]
        public int MonthId { get; set; }
        public string MonthName { get; set; }
        [Required]
        public int Year { get; set; }
        public decimal Total { get; set; }
        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public List<EmployeeInvoiceDetails> DetailList { get; set; }
        public string PaymentTerms { get; set; }
        public int? Resources { get; set; }
        public int? Hours { get; set; }
        public IEnumerable<int> EmployeeIds { get; set; }
        public string CurrencySymbol { get; set; }
        public string CurrencyCode { get; set; }
        public decimal? CurrencyRate { get; set; }
        public string EmployeeName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public decimal? Remuneration { get; set; }
        public decimal? Expense { get; set; }
        public decimal? RemunerationPercentage { get; set; }
        public decimal TotalBillingOnCalc { get; set; }
        public decimal TotalRemunerationCalc { get; set; }


    }
    public class EmployeeInvoiceDetails : BaseBo
    {
        public int MonthlyId { get; set; }
        public string EmployeeName { get; set; }
        public bool Fixed { get; set; }
        public bool Selected { get; set; }
        public decimal? FixedRate { get; set; }
        public int? HoursLogged { get; set; }
        public decimal? HourlyRate { get; set; }
        public decimal Total { get; set; }
        public decimal? NetPay { get; set; }
        public string EmployeeCode { get; set; }
        public decimal? Remuneration { get; set; }
        public decimal? Expense { get; set; }
        public decimal? BillingPercentage { get; set; }
        public decimal? RemunerationPercentage { get; set; }
        public int? EmployeeBillingId { get; set; }

    }
}
