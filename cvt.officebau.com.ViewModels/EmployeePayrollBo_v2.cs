﻿namespace cvt.officebau.com.ViewModels
{
    public class EmployeePayrollBo_v2
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int ComponentId { get; set; }
        public string Description { get; set; }
        public int EmployeePayStructureId { get; set; }
        public bool IsEditable { get; set; }
        public decimal? Amount { get; set; }
        public string PayType { get; set; }
        public string ComponentCode { get; set; }
        public string Note { get; set; }
        public string ConfigValue { get; set; }
    }
}