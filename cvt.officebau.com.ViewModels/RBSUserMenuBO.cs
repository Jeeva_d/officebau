﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class RbsUserMenuBo : BaseBo
    {
        public int ModuleId { get; set; }
        public string Module { get; set; }
        public string SubModule { get; set; }
        public string Menu { get; set; }
        public bool MenuRead { get; set; }
        public bool MenuWrite { get; set; }
        public bool MenuEdit { get; set; }
        public bool MenuDelete { get; set; }
        public int MenuId { get; set; }
        public Entity RbsModule { get; set; }
        public List<SelectListItem> EmployeeNameList { get; set; }
        public List<SelectListItem> RbsModuleList { get; set; }
        public List<SelectListItem> RbsMenuList { get; set; }
        public string EmployeeName { get; set; }
        public string EmailId { get; set; }
        public int BusinessUnitId { get; set; }
        public string BusinessUnitIDs { get; set; }
        public List<SelectListItem> BusinessUnitList { get; set; }
        public string LoginCode { get; set; }
    }
}
