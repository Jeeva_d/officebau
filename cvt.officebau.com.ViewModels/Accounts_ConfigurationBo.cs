﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class Profit_Share_Analysis_ConfigurationBo : BaseBo
    {
        public string EmployeeName { get; set; }
        public string CustomerName { get; set; }
        public string VendorName { get; set; }
        public decimal? Percentage { get; set; }
        public bool IsCustomer { get; set; }
        public int? VendorId { get; set; }
        public int? CustomerId { get; set; }

    }
}
