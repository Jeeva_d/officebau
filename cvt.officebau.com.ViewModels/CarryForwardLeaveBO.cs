﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class CarryForwardLeaveBO : BaseBo
    {
        public int LeaveTypeID { get; set; }
        public decimal TotalLeave { get; set; }
        public int Year { get; set; }
        public bool IsCarryForward { get; set; }
        public bool IsEncashment { get; set; }
        public string LeaveType { get; set; }
        public List<SelectListItem> LeaveTypeList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public string FirstName { get; set; }
        public string EmployeeCode { get; set; }
        public string LastName { get; set; }
    }
}