﻿using cvt.officebau.com.MessageConstants;
using System.ComponentModel.DataAnnotations;

namespace cvt.officebau.com.ViewModels
{
    public class FinancialYearBo : BaseBo
    {
        [Required(ErrorMessage = BOConstants.Start_Year)]
        [Range(0, 9999, ErrorMessage = BOConstants.Year_Validation)]
        public string FinancialYear { get; set; }

        public string StartMonth { get; set; }
        public string EndMonth { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
