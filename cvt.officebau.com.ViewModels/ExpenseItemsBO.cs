﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class ExpenseItemsBO : BaseBo
    {
        public int ExpenseID { get; set; }
        public List<SelectListItem> ProductList { get; set; }
        public int ProductID { get; set; }
        public string ExpenseItemRemarks { get; set; }
        public int QTY { get; set; }
        public decimal Rate { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal Amount { get; set; }

        public int Typeid { get; set; }
    }
}
