﻿namespace cvt.officebau.com.ViewModels
{
    public class ReportDashboardBo : BaseBo
    {
        public int ReportTypeId { get; set; }
        public int GroupId { get; set; }
        public string GroupBy { get; set; }
        public string ReportType { get; set; }
        public string TotalMember { get; set; }
        public int HeadcountDesignation { get; set; }
        public int HeadcountDepartment { get; set; }
        public decimal CompensationDesignation { get; set; }
        public decimal CompensationDepartment { get; set; }
    }
}
