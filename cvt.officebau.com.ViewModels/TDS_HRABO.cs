﻿using cvt.officebau.com.MessageConstants;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace cvt.officebau.com.ViewModels
{
    public class TdsHrabo : BaseBo
    {
        public Entity Month { get; set; }
        public Entity Year { get; set; }
        public Entity StartYear { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.valid_Declaration)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.valid_Declaration)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Declaration { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.valid_Submitted)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.valid_Submitted)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Submitted { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.valid_Cleared)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.valid_Cleared)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Cleared { get; set; }

        [Range(0, 922337203685477, ErrorMessage = BOConstants.valid_Rejected)]
        [RegularExpression("^[0-9\\.]*$", ErrorMessage = BOConstants.valid_Rejected)]
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal? Rejected { get; set; }
        
        public string Remarks { get; set; }
        public bool IsOpen { get; set; }
        public bool IsProofOpen { get; set; }
        public bool IsFinancialYear { get; set; }
        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }
        public List<SelectListItem> StartYearList { get; set; }

        [Required(ErrorMessage = BOConstants.Select_Financial_Year)]
        public int? FinancialYearId { get; set; }

        public List<SelectListItem> FinancialYearList { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public decimal? TdsRate { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal Gross { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal Pt { get; set; }

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = false)]
        public decimal NetPay { get; set; }

        [Required(ErrorMessage = BOConstants.select_Form_Type)]
        public string FormType { get; set; }

        [Required(ErrorMessage = BOConstants.upload_file)]
        public Guid FileId { get; set; }

        public string FileName { get; set; }
        public string FinancialYear { get; set; }
        public int FileCount { get; set; }
        public string Status { get; set; }
    }
}
