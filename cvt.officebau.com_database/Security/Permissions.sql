﻿GRANT VIEW ANY COLUMN ENCRYPTION KEY DEFINITION TO PUBLIC;


GO
GRANT VIEW ANY COLUMN MASTER KEY DEFINITION TO PUBLIC;


GO
GRANT CONNECT TO [app.officebau.com];


GO
GRANT CREATE PROCEDURE TO [app.officebau.com];


GO
GRANT CREATE QUEUE TO [app.officebau.com];


GO
GRANT CREATE SERVICE TO [app.officebau.com];


GO
GRANT EXECUTE TO [app.officebau.com];


GO
GRANT INSERT TO [app.officebau.com];


GO
GRANT SELECT TO [app.officebau.com];


GO
GRANT SUBSCRIBE QUERY NOTIFICATIONS TO [app.officebau.com];


GO
GRANT UPDATE TO [app.officebau.com];


GO
GRANT REFERENCES
    ON CONTRACT::[http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification] TO [app.officebau.com];

