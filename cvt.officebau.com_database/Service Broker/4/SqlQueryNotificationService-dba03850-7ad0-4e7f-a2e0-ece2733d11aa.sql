﻿CREATE SERVICE [SqlQueryNotificationService-dba03850-7ad0-4e7f-a2e0-ece2733d11aa]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-dba03850-7ad0-4e7f-a2e0-ece2733d11aa]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

