﻿CREATE SERVICE [SqlQueryNotificationService-dbe59f0e-39b5-410d-8fb2-53e86afabd82]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-dbe59f0e-39b5-410d-8fb2-53e86afabd82]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

