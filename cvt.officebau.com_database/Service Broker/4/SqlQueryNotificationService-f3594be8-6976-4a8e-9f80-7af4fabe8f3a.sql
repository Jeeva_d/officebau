﻿CREATE SERVICE [SqlQueryNotificationService-f3594be8-6976-4a8e-9f80-7af4fabe8f3a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f3594be8-6976-4a8e-9f80-7af4fabe8f3a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

