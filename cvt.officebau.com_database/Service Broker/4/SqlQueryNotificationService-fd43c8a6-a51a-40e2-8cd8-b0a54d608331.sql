﻿CREATE SERVICE [SqlQueryNotificationService-fd43c8a6-a51a-40e2-8cd8-b0a54d608331]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-fd43c8a6-a51a-40e2-8cd8-b0a54d608331]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

