﻿CREATE SERVICE [SqlQueryNotificationService-ec0560c9-3ff5-4ce6-a95e-fa15f5f9156e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ec0560c9-3ff5-4ce6-a95e-fa15f5f9156e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

