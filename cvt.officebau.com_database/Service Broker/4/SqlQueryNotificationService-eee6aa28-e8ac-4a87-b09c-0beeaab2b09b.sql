﻿CREATE SERVICE [SqlQueryNotificationService-eee6aa28-e8ac-4a87-b09c-0beeaab2b09b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-eee6aa28-e8ac-4a87-b09c-0beeaab2b09b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

