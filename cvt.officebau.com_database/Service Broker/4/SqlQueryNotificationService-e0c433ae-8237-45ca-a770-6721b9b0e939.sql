﻿CREATE SERVICE [SqlQueryNotificationService-e0c433ae-8237-45ca-a770-6721b9b0e939]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e0c433ae-8237-45ca-a770-6721b9b0e939]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

