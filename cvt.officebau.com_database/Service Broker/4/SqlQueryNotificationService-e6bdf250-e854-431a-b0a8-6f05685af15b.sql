﻿CREATE SERVICE [SqlQueryNotificationService-e6bdf250-e854-431a-b0a8-6f05685af15b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e6bdf250-e854-431a-b0a8-6f05685af15b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

