﻿CREATE SERVICE [SqlQueryNotificationService-e336a993-35c6-4b1b-8fdd-6b082bcddaca]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e336a993-35c6-4b1b-8fdd-6b082bcddaca]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

