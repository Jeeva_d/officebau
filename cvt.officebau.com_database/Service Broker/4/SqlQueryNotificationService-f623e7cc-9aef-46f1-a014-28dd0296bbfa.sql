﻿CREATE SERVICE [SqlQueryNotificationService-f623e7cc-9aef-46f1-a014-28dd0296bbfa]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f623e7cc-9aef-46f1-a014-28dd0296bbfa]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

