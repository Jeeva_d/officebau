﻿CREATE SERVICE [SqlQueryNotificationService-fbc4643b-66b0-43b4-ba60-0b2a779777c5]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-fbc4643b-66b0-43b4-ba60-0b2a779777c5]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

