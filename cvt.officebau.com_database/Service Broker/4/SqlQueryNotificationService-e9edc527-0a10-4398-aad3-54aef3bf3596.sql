﻿CREATE SERVICE [SqlQueryNotificationService-e9edc527-0a10-4398-aad3-54aef3bf3596]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e9edc527-0a10-4398-aad3-54aef3bf3596]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

