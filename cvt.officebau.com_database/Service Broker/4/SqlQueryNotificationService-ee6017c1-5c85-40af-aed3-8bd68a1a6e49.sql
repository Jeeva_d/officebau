﻿CREATE SERVICE [SqlQueryNotificationService-ee6017c1-5c85-40af-aed3-8bd68a1a6e49]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ee6017c1-5c85-40af-aed3-8bd68a1a6e49]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

