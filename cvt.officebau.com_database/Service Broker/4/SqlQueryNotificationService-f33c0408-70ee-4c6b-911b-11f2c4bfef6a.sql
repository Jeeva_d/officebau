﻿CREATE SERVICE [SqlQueryNotificationService-f33c0408-70ee-4c6b-911b-11f2c4bfef6a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f33c0408-70ee-4c6b-911b-11f2c4bfef6a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

