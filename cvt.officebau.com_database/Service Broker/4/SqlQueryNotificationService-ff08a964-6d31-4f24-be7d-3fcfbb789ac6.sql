﻿CREATE SERVICE [SqlQueryNotificationService-ff08a964-6d31-4f24-be7d-3fcfbb789ac6]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ff08a964-6d31-4f24-be7d-3fcfbb789ac6]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

