﻿CREATE SERVICE [SqlQueryNotificationService-e7c6bc10-f868-4e14-8dbb-b9e85c795a3a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e7c6bc10-f868-4e14-8dbb-b9e85c795a3a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

