﻿CREATE SERVICE [SqlQueryNotificationService-f47d85fa-50b7-4541-ab46-2c5b458e4812]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f47d85fa-50b7-4541-ab46-2c5b458e4812]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

