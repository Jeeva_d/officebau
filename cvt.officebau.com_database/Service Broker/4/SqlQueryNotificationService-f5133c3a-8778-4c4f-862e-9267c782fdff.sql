﻿CREATE SERVICE [SqlQueryNotificationService-f5133c3a-8778-4c4f-862e-9267c782fdff]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f5133c3a-8778-4c4f-862e-9267c782fdff]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

