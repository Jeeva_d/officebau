﻿CREATE SERVICE [SqlQueryNotificationService-fc2f357b-473a-4a46-838e-b6367fa3873e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-fc2f357b-473a-4a46-838e-b6367fa3873e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

