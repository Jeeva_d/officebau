﻿CREATE SERVICE [SqlQueryNotificationService-f8036f79-78fc-4caa-8c5d-fb47cb06ad0e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f8036f79-78fc-4caa-8c5d-fb47cb06ad0e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

