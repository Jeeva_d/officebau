﻿CREATE SERVICE [SqlQueryNotificationService-f67a2cf1-7771-4a16-977f-5caf3d175404]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f67a2cf1-7771-4a16-977f-5caf3d175404]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

