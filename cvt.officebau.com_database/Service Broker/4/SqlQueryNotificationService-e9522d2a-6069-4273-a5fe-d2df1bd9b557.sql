﻿CREATE SERVICE [SqlQueryNotificationService-e9522d2a-6069-4273-a5fe-d2df1bd9b557]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e9522d2a-6069-4273-a5fe-d2df1bd9b557]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

