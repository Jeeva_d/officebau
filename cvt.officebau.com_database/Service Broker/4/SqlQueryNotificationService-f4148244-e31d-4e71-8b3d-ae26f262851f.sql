﻿CREATE SERVICE [SqlQueryNotificationService-f4148244-e31d-4e71-8b3d-ae26f262851f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f4148244-e31d-4e71-8b3d-ae26f262851f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

