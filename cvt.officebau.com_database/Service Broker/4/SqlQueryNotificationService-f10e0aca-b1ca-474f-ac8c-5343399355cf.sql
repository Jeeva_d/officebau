﻿CREATE SERVICE [SqlQueryNotificationService-f10e0aca-b1ca-474f-ac8c-5343399355cf]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f10e0aca-b1ca-474f-ac8c-5343399355cf]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

