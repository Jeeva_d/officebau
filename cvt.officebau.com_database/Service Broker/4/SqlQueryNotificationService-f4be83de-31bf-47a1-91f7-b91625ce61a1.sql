﻿CREATE SERVICE [SqlQueryNotificationService-f4be83de-31bf-47a1-91f7-b91625ce61a1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f4be83de-31bf-47a1-91f7-b91625ce61a1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

