﻿CREATE SERVICE [SqlQueryNotificationService-e7d8a073-cd1b-464c-a5ad-4c7e7d23c478]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e7d8a073-cd1b-464c-a5ad-4c7e7d23c478]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

