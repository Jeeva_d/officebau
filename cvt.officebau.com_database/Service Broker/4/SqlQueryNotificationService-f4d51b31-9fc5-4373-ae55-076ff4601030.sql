﻿CREATE SERVICE [SqlQueryNotificationService-f4d51b31-9fc5-4373-ae55-076ff4601030]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f4d51b31-9fc5-4373-ae55-076ff4601030]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

