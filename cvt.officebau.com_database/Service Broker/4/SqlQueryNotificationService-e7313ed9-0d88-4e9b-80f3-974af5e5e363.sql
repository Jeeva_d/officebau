﻿CREATE SERVICE [SqlQueryNotificationService-e7313ed9-0d88-4e9b-80f3-974af5e5e363]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e7313ed9-0d88-4e9b-80f3-974af5e5e363]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

