﻿CREATE SERVICE [SqlQueryNotificationService-ea960afc-9d6a-48b2-b82f-6627c21ccb52]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ea960afc-9d6a-48b2-b82f-6627c21ccb52]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

