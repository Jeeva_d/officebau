﻿CREATE SERVICE [SqlQueryNotificationService-f48d27f7-986f-4bb0-a9d8-3935894e09da]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f48d27f7-986f-4bb0-a9d8-3935894e09da]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

