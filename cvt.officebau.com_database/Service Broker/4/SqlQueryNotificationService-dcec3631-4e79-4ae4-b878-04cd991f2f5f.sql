﻿CREATE SERVICE [SqlQueryNotificationService-dcec3631-4e79-4ae4-b878-04cd991f2f5f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-dcec3631-4e79-4ae4-b878-04cd991f2f5f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

