﻿CREATE SERVICE [SqlQueryNotificationService-f09a0369-3612-48d9-8b98-05ef84e9e29f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f09a0369-3612-48d9-8b98-05ef84e9e29f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

