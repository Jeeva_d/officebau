﻿CREATE SERVICE [SqlQueryNotificationService-e2d5b343-3d12-48c6-a6a2-fac179e5653c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e2d5b343-3d12-48c6-a6a2-fac179e5653c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

