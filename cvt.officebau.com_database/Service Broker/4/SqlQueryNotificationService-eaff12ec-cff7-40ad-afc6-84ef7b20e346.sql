﻿CREATE SERVICE [SqlQueryNotificationService-eaff12ec-cff7-40ad-afc6-84ef7b20e346]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-eaff12ec-cff7-40ad-afc6-84ef7b20e346]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

