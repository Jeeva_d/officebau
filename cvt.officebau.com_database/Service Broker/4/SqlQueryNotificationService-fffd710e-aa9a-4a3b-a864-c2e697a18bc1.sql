﻿CREATE SERVICE [SqlQueryNotificationService-fffd710e-aa9a-4a3b-a864-c2e697a18bc1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-fffd710e-aa9a-4a3b-a864-c2e697a18bc1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

