﻿CREATE SERVICE [SqlQueryNotificationService-f38a15e4-dbec-49fe-8843-e5496a8e14cc]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f38a15e4-dbec-49fe-8843-e5496a8e14cc]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

