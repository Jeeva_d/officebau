﻿CREATE SERVICE [SqlQueryNotificationService-f8fecc71-2ecc-41e3-8c64-33e1ba0035bd]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f8fecc71-2ecc-41e3-8c64-33e1ba0035bd]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

