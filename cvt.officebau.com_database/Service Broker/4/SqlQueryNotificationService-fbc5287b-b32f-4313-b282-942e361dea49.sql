﻿CREATE SERVICE [SqlQueryNotificationService-fbc5287b-b32f-4313-b282-942e361dea49]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-fbc5287b-b32f-4313-b282-942e361dea49]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

