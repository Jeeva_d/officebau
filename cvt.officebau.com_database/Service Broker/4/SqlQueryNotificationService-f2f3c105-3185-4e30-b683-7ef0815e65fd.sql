﻿CREATE SERVICE [SqlQueryNotificationService-f2f3c105-3185-4e30-b683-7ef0815e65fd]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f2f3c105-3185-4e30-b683-7ef0815e65fd]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

