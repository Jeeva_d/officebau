﻿CREATE SERVICE [SqlQueryNotificationService-e97c3e88-78e7-4d7b-8c04-d1f7263a4ee6]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e97c3e88-78e7-4d7b-8c04-d1f7263a4ee6]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

