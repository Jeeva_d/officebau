﻿CREATE SERVICE [SqlQueryNotificationService-f013f72f-a244-43f9-a00a-85f965687eea]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f013f72f-a244-43f9-a00a-85f965687eea]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

