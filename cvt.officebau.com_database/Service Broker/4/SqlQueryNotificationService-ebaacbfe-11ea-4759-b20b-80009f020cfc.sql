﻿CREATE SERVICE [SqlQueryNotificationService-ebaacbfe-11ea-4759-b20b-80009f020cfc]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ebaacbfe-11ea-4759-b20b-80009f020cfc]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

