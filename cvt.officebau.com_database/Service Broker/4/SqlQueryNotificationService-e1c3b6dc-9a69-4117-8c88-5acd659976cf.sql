﻿CREATE SERVICE [SqlQueryNotificationService-e1c3b6dc-9a69-4117-8c88-5acd659976cf]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e1c3b6dc-9a69-4117-8c88-5acd659976cf]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

