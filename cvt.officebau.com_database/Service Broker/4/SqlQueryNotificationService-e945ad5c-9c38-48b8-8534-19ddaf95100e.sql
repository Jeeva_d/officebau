﻿CREATE SERVICE [SqlQueryNotificationService-e945ad5c-9c38-48b8-8534-19ddaf95100e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e945ad5c-9c38-48b8-8534-19ddaf95100e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

