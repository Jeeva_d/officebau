﻿CREATE SERVICE [SqlQueryNotificationService-dcfc347d-9e50-4891-b0fe-379634c8e55e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-dcfc347d-9e50-4891-b0fe-379634c8e55e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

