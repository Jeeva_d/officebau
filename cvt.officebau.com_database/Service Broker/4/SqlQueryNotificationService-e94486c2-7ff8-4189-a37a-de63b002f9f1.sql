﻿CREATE SERVICE [SqlQueryNotificationService-e94486c2-7ff8-4189-a37a-de63b002f9f1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e94486c2-7ff8-4189-a37a-de63b002f9f1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

