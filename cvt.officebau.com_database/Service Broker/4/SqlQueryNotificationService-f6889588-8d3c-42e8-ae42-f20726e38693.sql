﻿CREATE SERVICE [SqlQueryNotificationService-f6889588-8d3c-42e8-ae42-f20726e38693]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f6889588-8d3c-42e8-ae42-f20726e38693]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

