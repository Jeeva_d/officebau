﻿CREATE SERVICE [SqlQueryNotificationService-e0e16ba8-0888-4b8e-a874-d5c5b0186be1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e0e16ba8-0888-4b8e-a874-d5c5b0186be1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

