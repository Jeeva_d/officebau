﻿CREATE SERVICE [SqlQueryNotificationService-fd2893f7-9524-4dd5-a0c0-7aa4ddbbf43f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-fd2893f7-9524-4dd5-a0c0-7aa4ddbbf43f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

