﻿CREATE SERVICE [SqlQueryNotificationService-f96572cd-28fe-436e-b57e-4aa9ebb8d88d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f96572cd-28fe-436e-b57e-4aa9ebb8d88d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

