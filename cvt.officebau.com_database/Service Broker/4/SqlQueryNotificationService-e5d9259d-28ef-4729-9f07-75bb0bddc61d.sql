﻿CREATE SERVICE [SqlQueryNotificationService-e5d9259d-28ef-4729-9f07-75bb0bddc61d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e5d9259d-28ef-4729-9f07-75bb0bddc61d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

