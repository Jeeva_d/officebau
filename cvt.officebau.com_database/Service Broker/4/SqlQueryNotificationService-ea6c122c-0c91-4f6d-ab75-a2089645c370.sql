﻿CREATE SERVICE [SqlQueryNotificationService-ea6c122c-0c91-4f6d-ab75-a2089645c370]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ea6c122c-0c91-4f6d-ab75-a2089645c370]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

