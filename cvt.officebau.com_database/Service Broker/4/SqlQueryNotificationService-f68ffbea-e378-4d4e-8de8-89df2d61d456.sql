﻿CREATE SERVICE [SqlQueryNotificationService-f68ffbea-e378-4d4e-8de8-89df2d61d456]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f68ffbea-e378-4d4e-8de8-89df2d61d456]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

