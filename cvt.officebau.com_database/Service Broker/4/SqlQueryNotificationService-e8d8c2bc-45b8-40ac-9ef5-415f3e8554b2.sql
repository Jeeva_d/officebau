﻿CREATE SERVICE [SqlQueryNotificationService-e8d8c2bc-45b8-40ac-9ef5-415f3e8554b2]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e8d8c2bc-45b8-40ac-9ef5-415f3e8554b2]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

