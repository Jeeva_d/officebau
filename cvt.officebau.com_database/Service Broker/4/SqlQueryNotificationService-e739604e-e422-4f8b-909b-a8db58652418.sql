﻿CREATE SERVICE [SqlQueryNotificationService-e739604e-e422-4f8b-909b-a8db58652418]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e739604e-e422-4f8b-909b-a8db58652418]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

