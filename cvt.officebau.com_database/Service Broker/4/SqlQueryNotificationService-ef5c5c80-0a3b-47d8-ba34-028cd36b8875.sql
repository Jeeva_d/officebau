﻿CREATE SERVICE [SqlQueryNotificationService-ef5c5c80-0a3b-47d8-ba34-028cd36b8875]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ef5c5c80-0a3b-47d8-ba34-028cd36b8875]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

