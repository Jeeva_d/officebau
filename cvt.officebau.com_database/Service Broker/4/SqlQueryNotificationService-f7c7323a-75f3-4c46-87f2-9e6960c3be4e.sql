﻿CREATE SERVICE [SqlQueryNotificationService-f7c7323a-75f3-4c46-87f2-9e6960c3be4e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f7c7323a-75f3-4c46-87f2-9e6960c3be4e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

