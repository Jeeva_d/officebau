﻿CREATE SERVICE [SqlQueryNotificationService-e5d7154b-fdee-407a-9e49-d9b9c273796c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e5d7154b-fdee-407a-9e49-d9b9c273796c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

