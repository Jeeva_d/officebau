﻿CREATE SERVICE [SqlQueryNotificationService-fad94fe7-83f0-443b-830c-c8d85de4264f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-fad94fe7-83f0-443b-830c-c8d85de4264f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

