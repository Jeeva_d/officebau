﻿CREATE SERVICE [SqlQueryNotificationService-f6dfd18f-da4b-4f60-9fd5-2e9f147fa47c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f6dfd18f-da4b-4f60-9fd5-2e9f147fa47c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

