﻿CREATE SERVICE [SqlQueryNotificationService-e94b8c6b-3804-484e-9b30-7c8162221c9c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e94b8c6b-3804-484e-9b30-7c8162221c9c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

