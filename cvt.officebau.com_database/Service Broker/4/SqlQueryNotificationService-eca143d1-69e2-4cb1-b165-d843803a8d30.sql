﻿CREATE SERVICE [SqlQueryNotificationService-eca143d1-69e2-4cb1-b165-d843803a8d30]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-eca143d1-69e2-4cb1-b165-d843803a8d30]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

