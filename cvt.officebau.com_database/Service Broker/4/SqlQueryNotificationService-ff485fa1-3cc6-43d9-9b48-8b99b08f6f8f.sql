﻿CREATE SERVICE [SqlQueryNotificationService-ff485fa1-3cc6-43d9-9b48-8b99b08f6f8f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ff485fa1-3cc6-43d9-9b48-8b99b08f6f8f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

