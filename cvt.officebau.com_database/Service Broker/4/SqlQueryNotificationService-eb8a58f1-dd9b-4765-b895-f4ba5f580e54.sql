﻿CREATE SERVICE [SqlQueryNotificationService-eb8a58f1-dd9b-4765-b895-f4ba5f580e54]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-eb8a58f1-dd9b-4765-b895-f4ba5f580e54]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

