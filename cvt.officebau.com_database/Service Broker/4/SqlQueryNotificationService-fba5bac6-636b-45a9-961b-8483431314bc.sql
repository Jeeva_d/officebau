﻿CREATE SERVICE [SqlQueryNotificationService-fba5bac6-636b-45a9-961b-8483431314bc]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-fba5bac6-636b-45a9-961b-8483431314bc]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

