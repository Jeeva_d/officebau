﻿CREATE SERVICE [SqlQueryNotificationService-f5a26ede-025e-495a-a19c-3ed9ab2afb62]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f5a26ede-025e-495a-a19c-3ed9ab2afb62]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

