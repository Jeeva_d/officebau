﻿CREATE SERVICE [SqlQueryNotificationService-f42dd984-ef8b-4321-ad12-39f93b0b7529]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f42dd984-ef8b-4321-ad12-39f93b0b7529]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

