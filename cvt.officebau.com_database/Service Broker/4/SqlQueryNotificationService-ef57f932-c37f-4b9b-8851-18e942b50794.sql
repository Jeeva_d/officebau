﻿CREATE SERVICE [SqlQueryNotificationService-ef57f932-c37f-4b9b-8851-18e942b50794]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ef57f932-c37f-4b9b-8851-18e942b50794]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

