﻿CREATE SERVICE [SqlQueryNotificationService-f3bf4870-b3a1-4cb8-addc-fdc77a5f5118]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f3bf4870-b3a1-4cb8-addc-fdc77a5f5118]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

