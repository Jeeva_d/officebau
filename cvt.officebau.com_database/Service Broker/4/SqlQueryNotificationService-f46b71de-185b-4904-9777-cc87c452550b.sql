﻿CREATE SERVICE [SqlQueryNotificationService-f46b71de-185b-4904-9777-cc87c452550b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f46b71de-185b-4904-9777-cc87c452550b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

