﻿CREATE SERVICE [SqlQueryNotificationService-f9e87f8b-dd98-4c32-83a3-8a2131cc80bc]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f9e87f8b-dd98-4c32-83a3-8a2131cc80bc]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

