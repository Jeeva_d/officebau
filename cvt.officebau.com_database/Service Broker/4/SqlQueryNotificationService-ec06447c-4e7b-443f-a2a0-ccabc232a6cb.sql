﻿CREATE SERVICE [SqlQueryNotificationService-ec06447c-4e7b-443f-a2a0-ccabc232a6cb]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ec06447c-4e7b-443f-a2a0-ccabc232a6cb]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

