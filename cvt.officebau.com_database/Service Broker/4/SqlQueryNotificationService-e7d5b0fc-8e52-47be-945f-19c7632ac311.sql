﻿CREATE SERVICE [SqlQueryNotificationService-e7d5b0fc-8e52-47be-945f-19c7632ac311]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e7d5b0fc-8e52-47be-945f-19c7632ac311]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

