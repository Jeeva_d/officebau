﻿CREATE SERVICE [SqlQueryNotificationService-e03b7d3d-d9b2-49df-9524-e4e71dbadec8]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e03b7d3d-d9b2-49df-9524-e4e71dbadec8]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

