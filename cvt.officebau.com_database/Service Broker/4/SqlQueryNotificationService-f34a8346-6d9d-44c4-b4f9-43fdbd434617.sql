﻿CREATE SERVICE [SqlQueryNotificationService-f34a8346-6d9d-44c4-b4f9-43fdbd434617]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f34a8346-6d9d-44c4-b4f9-43fdbd434617]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

