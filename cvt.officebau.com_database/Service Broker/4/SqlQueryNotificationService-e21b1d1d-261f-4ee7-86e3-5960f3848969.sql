﻿CREATE SERVICE [SqlQueryNotificationService-e21b1d1d-261f-4ee7-86e3-5960f3848969]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e21b1d1d-261f-4ee7-86e3-5960f3848969]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

