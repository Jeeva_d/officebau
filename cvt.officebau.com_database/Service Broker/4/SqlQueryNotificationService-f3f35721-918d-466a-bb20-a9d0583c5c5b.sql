﻿CREATE SERVICE [SqlQueryNotificationService-f3f35721-918d-466a-bb20-a9d0583c5c5b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f3f35721-918d-466a-bb20-a9d0583c5c5b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

