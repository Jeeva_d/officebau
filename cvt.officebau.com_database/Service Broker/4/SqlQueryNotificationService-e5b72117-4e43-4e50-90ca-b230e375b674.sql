﻿CREATE SERVICE [SqlQueryNotificationService-e5b72117-4e43-4e50-90ca-b230e375b674]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e5b72117-4e43-4e50-90ca-b230e375b674]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

