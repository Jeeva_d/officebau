﻿CREATE SERVICE [SqlQueryNotificationService-e4a306de-a854-4f82-8d90-30e3f288c5df]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e4a306de-a854-4f82-8d90-30e3f288c5df]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

