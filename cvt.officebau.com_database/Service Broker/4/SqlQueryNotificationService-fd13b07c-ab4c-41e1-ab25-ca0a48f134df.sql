﻿CREATE SERVICE [SqlQueryNotificationService-fd13b07c-ab4c-41e1-ab25-ca0a48f134df]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-fd13b07c-ab4c-41e1-ab25-ca0a48f134df]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

