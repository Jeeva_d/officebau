﻿CREATE SERVICE [SqlQueryNotificationService-dca7967d-96c6-484d-ac9a-0df93ba6b757]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-dca7967d-96c6-484d-ac9a-0df93ba6b757]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

