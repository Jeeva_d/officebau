﻿CREATE SERVICE [SqlQueryNotificationService-ec8dfd1e-09cb-4d89-9f15-acff767b9b27]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ec8dfd1e-09cb-4d89-9f15-acff767b9b27]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

