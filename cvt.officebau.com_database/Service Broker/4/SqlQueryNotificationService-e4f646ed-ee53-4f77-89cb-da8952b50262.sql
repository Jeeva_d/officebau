﻿CREATE SERVICE [SqlQueryNotificationService-e4f646ed-ee53-4f77-89cb-da8952b50262]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e4f646ed-ee53-4f77-89cb-da8952b50262]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

