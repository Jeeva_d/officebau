﻿CREATE SERVICE [SqlQueryNotificationService-f624f397-fe4f-4947-b319-e814d756eabd]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f624f397-fe4f-4947-b319-e814d756eabd]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

