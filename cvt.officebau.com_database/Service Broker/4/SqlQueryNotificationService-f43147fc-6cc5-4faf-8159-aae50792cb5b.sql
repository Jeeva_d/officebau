﻿CREATE SERVICE [SqlQueryNotificationService-f43147fc-6cc5-4faf-8159-aae50792cb5b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f43147fc-6cc5-4faf-8159-aae50792cb5b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

