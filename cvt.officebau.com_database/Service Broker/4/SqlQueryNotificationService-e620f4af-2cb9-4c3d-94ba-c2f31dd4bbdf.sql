﻿CREATE SERVICE [SqlQueryNotificationService-e620f4af-2cb9-4c3d-94ba-c2f31dd4bbdf]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e620f4af-2cb9-4c3d-94ba-c2f31dd4bbdf]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

