﻿CREATE SERVICE [SqlQueryNotificationService-f5acd393-6405-4fae-8d6c-263cf574cf8b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f5acd393-6405-4fae-8d6c-263cf574cf8b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

