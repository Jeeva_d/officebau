﻿CREATE SERVICE [SqlQueryNotificationService-ed9bc3c9-574f-43fe-bd09-cd894836763f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ed9bc3c9-574f-43fe-bd09-cd894836763f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

