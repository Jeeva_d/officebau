﻿CREATE SERVICE [SqlQueryNotificationService-ec763397-a9a9-42be-900b-7f54291c099a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ec763397-a9a9-42be-900b-7f54291c099a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

