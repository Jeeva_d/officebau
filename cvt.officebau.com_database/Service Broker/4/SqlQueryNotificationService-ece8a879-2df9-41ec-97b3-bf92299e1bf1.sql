﻿CREATE SERVICE [SqlQueryNotificationService-ece8a879-2df9-41ec-97b3-bf92299e1bf1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ece8a879-2df9-41ec-97b3-bf92299e1bf1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

