﻿CREATE SERVICE [SqlQueryNotificationService-dc5e52d5-8849-4267-ac8b-41b2aadcfe9f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-dc5e52d5-8849-4267-ac8b-41b2aadcfe9f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

