﻿CREATE SERVICE [SqlQueryNotificationService-e416c1a6-2390-461c-9207-3689ca9ac6f3]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e416c1a6-2390-461c-9207-3689ca9ac6f3]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

