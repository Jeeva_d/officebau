﻿CREATE SERVICE [SqlQueryNotificationService-ef4f12a0-17e0-4ea3-a9b6-994dd9b3b59d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ef4f12a0-17e0-4ea3-a9b6-994dd9b3b59d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

