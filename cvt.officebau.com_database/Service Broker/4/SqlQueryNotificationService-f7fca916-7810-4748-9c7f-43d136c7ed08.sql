﻿CREATE SERVICE [SqlQueryNotificationService-f7fca916-7810-4748-9c7f-43d136c7ed08]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-f7fca916-7810-4748-9c7f-43d136c7ed08]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

