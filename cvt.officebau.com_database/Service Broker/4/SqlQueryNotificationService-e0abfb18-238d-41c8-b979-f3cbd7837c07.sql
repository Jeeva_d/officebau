﻿CREATE SERVICE [SqlQueryNotificationService-e0abfb18-238d-41c8-b979-f3cbd7837c07]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-e0abfb18-238d-41c8-b979-f3cbd7837c07]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

