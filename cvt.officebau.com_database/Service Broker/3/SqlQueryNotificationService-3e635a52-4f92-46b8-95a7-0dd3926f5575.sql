﻿CREATE SERVICE [SqlQueryNotificationService-3e635a52-4f92-46b8-95a7-0dd3926f5575]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-3e635a52-4f92-46b8-95a7-0dd3926f5575]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

