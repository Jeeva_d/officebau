﻿CREATE SERVICE [SqlQueryNotificationService-20ef789d-26d3-42a8-a607-0bd43e57b80c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-20ef789d-26d3-42a8-a607-0bd43e57b80c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

