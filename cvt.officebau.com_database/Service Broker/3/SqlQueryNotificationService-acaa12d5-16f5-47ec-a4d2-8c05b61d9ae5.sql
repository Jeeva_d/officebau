﻿CREATE SERVICE [SqlQueryNotificationService-acaa12d5-16f5-47ec-a4d2-8c05b61d9ae5]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-acaa12d5-16f5-47ec-a4d2-8c05b61d9ae5]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

