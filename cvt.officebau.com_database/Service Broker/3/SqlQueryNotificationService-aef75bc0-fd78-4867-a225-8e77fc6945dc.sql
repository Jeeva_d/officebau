﻿CREATE SERVICE [SqlQueryNotificationService-aef75bc0-fd78-4867-a225-8e77fc6945dc]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-aef75bc0-fd78-4867-a225-8e77fc6945dc]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

