﻿CREATE SERVICE [SqlQueryNotificationService-af905b19-b7e0-476e-bd64-19a9c79bb278]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-af905b19-b7e0-476e-bd64-19a9c79bb278]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

