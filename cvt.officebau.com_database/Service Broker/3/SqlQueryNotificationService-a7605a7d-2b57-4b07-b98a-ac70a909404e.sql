﻿CREATE SERVICE [SqlQueryNotificationService-a7605a7d-2b57-4b07-b98a-ac70a909404e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a7605a7d-2b57-4b07-b98a-ac70a909404e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

