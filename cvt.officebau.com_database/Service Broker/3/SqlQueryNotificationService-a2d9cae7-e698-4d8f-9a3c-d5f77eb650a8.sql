﻿CREATE SERVICE [SqlQueryNotificationService-a2d9cae7-e698-4d8f-9a3c-d5f77eb650a8]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a2d9cae7-e698-4d8f-9a3c-d5f77eb650a8]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

