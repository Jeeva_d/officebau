﻿CREATE SERVICE [SqlQueryNotificationService-c63a028a-0d6f-4104-ad1c-862bc68e87cd]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c63a028a-0d6f-4104-ad1c-862bc68e87cd]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

