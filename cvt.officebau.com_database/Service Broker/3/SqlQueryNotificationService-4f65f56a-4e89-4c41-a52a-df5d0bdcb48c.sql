﻿CREATE SERVICE [SqlQueryNotificationService-4f65f56a-4e89-4c41-a52a-df5d0bdcb48c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-4f65f56a-4e89-4c41-a52a-df5d0bdcb48c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

