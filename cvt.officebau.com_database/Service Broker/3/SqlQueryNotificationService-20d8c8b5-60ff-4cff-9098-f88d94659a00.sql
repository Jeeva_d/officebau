﻿CREATE SERVICE [SqlQueryNotificationService-20d8c8b5-60ff-4cff-9098-f88d94659a00]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-20d8c8b5-60ff-4cff-9098-f88d94659a00]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

