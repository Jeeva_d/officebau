﻿CREATE SERVICE [SqlQueryNotificationService-d14d2031-205e-4ff4-be38-1ad20379a4de]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d14d2031-205e-4ff4-be38-1ad20379a4de]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

