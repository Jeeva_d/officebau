﻿CREATE SERVICE [SqlQueryNotificationService-8bb42f1a-b12a-4d27-bf49-26be806a1146]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8bb42f1a-b12a-4d27-bf49-26be806a1146]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

