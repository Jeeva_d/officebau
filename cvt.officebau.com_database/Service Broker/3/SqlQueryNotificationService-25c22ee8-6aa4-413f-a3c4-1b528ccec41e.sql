﻿CREATE SERVICE [SqlQueryNotificationService-25c22ee8-6aa4-413f-a3c4-1b528ccec41e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-25c22ee8-6aa4-413f-a3c4-1b528ccec41e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

