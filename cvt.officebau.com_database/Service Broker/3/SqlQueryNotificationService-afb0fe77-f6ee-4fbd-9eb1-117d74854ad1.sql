﻿CREATE SERVICE [SqlQueryNotificationService-afb0fe77-f6ee-4fbd-9eb1-117d74854ad1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-afb0fe77-f6ee-4fbd-9eb1-117d74854ad1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

