﻿CREATE SERVICE [SqlQueryNotificationService-ccdf3c34-c2d7-4816-b17c-c07d23ec6eac]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ccdf3c34-c2d7-4816-b17c-c07d23ec6eac]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

