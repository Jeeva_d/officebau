﻿CREATE SERVICE [SqlQueryNotificationService-0b6a2ec5-e1ed-43ef-b63d-2648004e84c3]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-0b6a2ec5-e1ed-43ef-b63d-2648004e84c3]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

