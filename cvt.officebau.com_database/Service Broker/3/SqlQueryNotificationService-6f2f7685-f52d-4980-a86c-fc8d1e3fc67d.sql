﻿CREATE SERVICE [SqlQueryNotificationService-6f2f7685-f52d-4980-a86c-fc8d1e3fc67d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-6f2f7685-f52d-4980-a86c-fc8d1e3fc67d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

