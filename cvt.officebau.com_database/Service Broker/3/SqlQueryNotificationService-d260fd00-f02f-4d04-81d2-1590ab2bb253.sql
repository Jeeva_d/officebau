﻿CREATE SERVICE [SqlQueryNotificationService-d260fd00-f02f-4d04-81d2-1590ab2bb253]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d260fd00-f02f-4d04-81d2-1590ab2bb253]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

