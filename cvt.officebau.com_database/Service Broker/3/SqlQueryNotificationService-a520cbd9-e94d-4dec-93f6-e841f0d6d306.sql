﻿CREATE SERVICE [SqlQueryNotificationService-a520cbd9-e94d-4dec-93f6-e841f0d6d306]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a520cbd9-e94d-4dec-93f6-e841f0d6d306]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

