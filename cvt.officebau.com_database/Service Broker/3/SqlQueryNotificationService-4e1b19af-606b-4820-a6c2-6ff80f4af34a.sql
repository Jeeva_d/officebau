﻿CREATE SERVICE [SqlQueryNotificationService-4e1b19af-606b-4820-a6c2-6ff80f4af34a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-4e1b19af-606b-4820-a6c2-6ff80f4af34a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

