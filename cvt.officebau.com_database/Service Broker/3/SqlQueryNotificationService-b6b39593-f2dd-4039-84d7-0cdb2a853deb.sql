﻿CREATE SERVICE [SqlQueryNotificationService-b6b39593-f2dd-4039-84d7-0cdb2a853deb]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b6b39593-f2dd-4039-84d7-0cdb2a853deb]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

