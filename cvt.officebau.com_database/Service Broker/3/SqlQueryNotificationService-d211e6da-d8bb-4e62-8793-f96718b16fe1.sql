﻿CREATE SERVICE [SqlQueryNotificationService-d211e6da-d8bb-4e62-8793-f96718b16fe1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d211e6da-d8bb-4e62-8793-f96718b16fe1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

