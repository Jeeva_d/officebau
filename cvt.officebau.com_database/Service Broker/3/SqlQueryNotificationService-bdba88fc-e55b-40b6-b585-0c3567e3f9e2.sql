﻿CREATE SERVICE [SqlQueryNotificationService-bdba88fc-e55b-40b6-b585-0c3567e3f9e2]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bdba88fc-e55b-40b6-b585-0c3567e3f9e2]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

