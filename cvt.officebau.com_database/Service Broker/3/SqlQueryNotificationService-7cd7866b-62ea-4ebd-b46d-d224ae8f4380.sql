﻿CREATE SERVICE [SqlQueryNotificationService-7cd7866b-62ea-4ebd-b46d-d224ae8f4380]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7cd7866b-62ea-4ebd-b46d-d224ae8f4380]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

