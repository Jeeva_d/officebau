﻿CREATE SERVICE [SqlQueryNotificationService-b2f79690-21e7-4b5a-987e-d95520c104a0]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b2f79690-21e7-4b5a-987e-d95520c104a0]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

