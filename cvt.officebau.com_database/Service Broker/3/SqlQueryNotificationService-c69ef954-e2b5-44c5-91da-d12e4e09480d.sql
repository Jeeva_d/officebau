﻿CREATE SERVICE [SqlQueryNotificationService-c69ef954-e2b5-44c5-91da-d12e4e09480d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c69ef954-e2b5-44c5-91da-d12e4e09480d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

