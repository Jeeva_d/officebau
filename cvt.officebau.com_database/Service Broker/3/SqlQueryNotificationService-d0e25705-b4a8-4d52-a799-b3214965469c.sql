﻿CREATE SERVICE [SqlQueryNotificationService-d0e25705-b4a8-4d52-a799-b3214965469c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d0e25705-b4a8-4d52-a799-b3214965469c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

