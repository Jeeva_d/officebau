﻿CREATE SERVICE [SqlQueryNotificationService-d0377f00-5303-48ad-8f47-a99a7fee851a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d0377f00-5303-48ad-8f47-a99a7fee851a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

