﻿CREATE SERVICE [SqlQueryNotificationService-8980c9db-8e4c-49c6-a26f-96683a18c481]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8980c9db-8e4c-49c6-a26f-96683a18c481]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

