﻿CREATE SERVICE [SqlQueryNotificationService-5e49853d-1c9f-46d2-a50a-c8a7d131d6f8]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-5e49853d-1c9f-46d2-a50a-c8a7d131d6f8]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

