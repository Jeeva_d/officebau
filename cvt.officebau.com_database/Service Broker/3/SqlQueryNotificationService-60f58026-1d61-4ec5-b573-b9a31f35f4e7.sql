﻿CREATE SERVICE [SqlQueryNotificationService-60f58026-1d61-4ec5-b573-b9a31f35f4e7]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-60f58026-1d61-4ec5-b573-b9a31f35f4e7]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

