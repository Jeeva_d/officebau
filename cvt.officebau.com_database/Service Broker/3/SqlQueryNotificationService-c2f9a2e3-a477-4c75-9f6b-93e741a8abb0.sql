﻿CREATE SERVICE [SqlQueryNotificationService-c2f9a2e3-a477-4c75-9f6b-93e741a8abb0]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c2f9a2e3-a477-4c75-9f6b-93e741a8abb0]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

