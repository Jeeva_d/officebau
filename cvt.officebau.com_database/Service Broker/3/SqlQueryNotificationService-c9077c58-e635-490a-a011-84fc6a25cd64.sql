﻿CREATE SERVICE [SqlQueryNotificationService-c9077c58-e635-490a-a011-84fc6a25cd64]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c9077c58-e635-490a-a011-84fc6a25cd64]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

