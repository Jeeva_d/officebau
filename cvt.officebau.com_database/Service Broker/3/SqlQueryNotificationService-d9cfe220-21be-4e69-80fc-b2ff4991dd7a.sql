﻿CREATE SERVICE [SqlQueryNotificationService-d9cfe220-21be-4e69-80fc-b2ff4991dd7a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d9cfe220-21be-4e69-80fc-b2ff4991dd7a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

