﻿CREATE SERVICE [SqlQueryNotificationService-b5d4bbd7-a2c5-4f79-bd9e-3e9e50ec1284]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b5d4bbd7-a2c5-4f79-bd9e-3e9e50ec1284]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

