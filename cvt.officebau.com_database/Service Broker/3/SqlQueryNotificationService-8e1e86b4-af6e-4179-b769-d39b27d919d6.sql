﻿CREATE SERVICE [SqlQueryNotificationService-8e1e86b4-af6e-4179-b769-d39b27d919d6]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8e1e86b4-af6e-4179-b769-d39b27d919d6]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

