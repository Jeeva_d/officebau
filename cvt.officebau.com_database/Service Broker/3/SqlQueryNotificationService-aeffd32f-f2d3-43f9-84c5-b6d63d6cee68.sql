﻿CREATE SERVICE [SqlQueryNotificationService-aeffd32f-f2d3-43f9-84c5-b6d63d6cee68]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-aeffd32f-f2d3-43f9-84c5-b6d63d6cee68]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

