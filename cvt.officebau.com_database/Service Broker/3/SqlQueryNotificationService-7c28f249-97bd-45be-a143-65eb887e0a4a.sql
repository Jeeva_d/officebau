﻿CREATE SERVICE [SqlQueryNotificationService-7c28f249-97bd-45be-a143-65eb887e0a4a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7c28f249-97bd-45be-a143-65eb887e0a4a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

