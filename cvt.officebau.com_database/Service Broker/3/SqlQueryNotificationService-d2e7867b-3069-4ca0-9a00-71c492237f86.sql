﻿CREATE SERVICE [SqlQueryNotificationService-d2e7867b-3069-4ca0-9a00-71c492237f86]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d2e7867b-3069-4ca0-9a00-71c492237f86]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

