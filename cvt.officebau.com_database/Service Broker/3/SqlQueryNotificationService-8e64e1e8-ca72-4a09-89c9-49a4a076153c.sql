﻿CREATE SERVICE [SqlQueryNotificationService-8e64e1e8-ca72-4a09-89c9-49a4a076153c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8e64e1e8-ca72-4a09-89c9-49a4a076153c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

