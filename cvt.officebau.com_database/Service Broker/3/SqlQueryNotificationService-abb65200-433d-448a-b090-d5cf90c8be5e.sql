﻿CREATE SERVICE [SqlQueryNotificationService-abb65200-433d-448a-b090-d5cf90c8be5e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-abb65200-433d-448a-b090-d5cf90c8be5e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

