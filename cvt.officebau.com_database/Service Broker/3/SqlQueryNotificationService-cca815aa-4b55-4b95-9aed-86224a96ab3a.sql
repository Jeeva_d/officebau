﻿CREATE SERVICE [SqlQueryNotificationService-cca815aa-4b55-4b95-9aed-86224a96ab3a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-cca815aa-4b55-4b95-9aed-86224a96ab3a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

