﻿CREATE SERVICE [SqlQueryNotificationService-d3f149bb-f525-408d-945d-67931f2b1a8d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d3f149bb-f525-408d-945d-67931f2b1a8d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

