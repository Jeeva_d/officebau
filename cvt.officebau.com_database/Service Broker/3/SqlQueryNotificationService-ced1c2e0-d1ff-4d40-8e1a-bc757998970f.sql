﻿CREATE SERVICE [SqlQueryNotificationService-ced1c2e0-d1ff-4d40-8e1a-bc757998970f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ced1c2e0-d1ff-4d40-8e1a-bc757998970f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

