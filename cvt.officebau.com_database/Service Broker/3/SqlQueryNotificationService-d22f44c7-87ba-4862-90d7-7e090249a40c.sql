﻿CREATE SERVICE [SqlQueryNotificationService-d22f44c7-87ba-4862-90d7-7e090249a40c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d22f44c7-87ba-4862-90d7-7e090249a40c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

