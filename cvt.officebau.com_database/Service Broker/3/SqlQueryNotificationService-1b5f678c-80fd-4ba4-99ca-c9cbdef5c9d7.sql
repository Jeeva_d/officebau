﻿CREATE SERVICE [SqlQueryNotificationService-1b5f678c-80fd-4ba4-99ca-c9cbdef5c9d7]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-1b5f678c-80fd-4ba4-99ca-c9cbdef5c9d7]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

