﻿CREATE SERVICE [SqlQueryNotificationService-21bf89b8-f4eb-4d0a-b16c-1c15611b9e8d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-21bf89b8-f4eb-4d0a-b16c-1c15611b9e8d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

