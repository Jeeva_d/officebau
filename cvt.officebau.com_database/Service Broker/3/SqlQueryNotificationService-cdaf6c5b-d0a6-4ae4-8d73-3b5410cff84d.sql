﻿CREATE SERVICE [SqlQueryNotificationService-cdaf6c5b-d0a6-4ae4-8d73-3b5410cff84d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-cdaf6c5b-d0a6-4ae4-8d73-3b5410cff84d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

