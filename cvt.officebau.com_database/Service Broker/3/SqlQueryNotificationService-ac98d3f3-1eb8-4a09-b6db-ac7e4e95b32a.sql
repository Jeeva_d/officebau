﻿CREATE SERVICE [SqlQueryNotificationService-ac98d3f3-1eb8-4a09-b6db-ac7e4e95b32a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ac98d3f3-1eb8-4a09-b6db-ac7e4e95b32a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

