﻿CREATE SERVICE [SqlQueryNotificationService-c86ee4c8-ce53-43a5-b28a-a678fd213527]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c86ee4c8-ce53-43a5-b28a-a678fd213527]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

