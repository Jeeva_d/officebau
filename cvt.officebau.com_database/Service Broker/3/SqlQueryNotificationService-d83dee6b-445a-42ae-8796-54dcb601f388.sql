﻿CREATE SERVICE [SqlQueryNotificationService-d83dee6b-445a-42ae-8796-54dcb601f388]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d83dee6b-445a-42ae-8796-54dcb601f388]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

