﻿CREATE SERVICE [SqlQueryNotificationService-c199bf80-21f5-4822-904c-c929366e0be7]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c199bf80-21f5-4822-904c-c929366e0be7]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

