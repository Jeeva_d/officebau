﻿CREATE SERVICE [SqlQueryNotificationService-9cb1f91c-2eac-48a4-a1ca-098471784631]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-9cb1f91c-2eac-48a4-a1ca-098471784631]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

