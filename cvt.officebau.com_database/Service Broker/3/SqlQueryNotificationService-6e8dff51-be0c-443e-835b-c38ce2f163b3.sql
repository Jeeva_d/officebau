﻿CREATE SERVICE [SqlQueryNotificationService-6e8dff51-be0c-443e-835b-c38ce2f163b3]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-6e8dff51-be0c-443e-835b-c38ce2f163b3]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

