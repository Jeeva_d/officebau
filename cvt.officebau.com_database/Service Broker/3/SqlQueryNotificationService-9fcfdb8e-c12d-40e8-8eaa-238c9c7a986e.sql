﻿CREATE SERVICE [SqlQueryNotificationService-9fcfdb8e-c12d-40e8-8eaa-238c9c7a986e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-9fcfdb8e-c12d-40e8-8eaa-238c9c7a986e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

