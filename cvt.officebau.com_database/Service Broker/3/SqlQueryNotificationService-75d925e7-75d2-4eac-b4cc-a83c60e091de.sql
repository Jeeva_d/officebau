﻿CREATE SERVICE [SqlQueryNotificationService-75d925e7-75d2-4eac-b4cc-a83c60e091de]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-75d925e7-75d2-4eac-b4cc-a83c60e091de]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

