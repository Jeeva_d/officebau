﻿CREATE SERVICE [SqlQueryNotificationService-d70b74f3-2a80-4c9f-9040-b0bdc6747e75]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d70b74f3-2a80-4c9f-9040-b0bdc6747e75]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

