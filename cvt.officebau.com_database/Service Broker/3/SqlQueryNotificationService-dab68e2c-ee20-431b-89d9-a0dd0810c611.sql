﻿CREATE SERVICE [SqlQueryNotificationService-dab68e2c-ee20-431b-89d9-a0dd0810c611]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-dab68e2c-ee20-431b-89d9-a0dd0810c611]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

