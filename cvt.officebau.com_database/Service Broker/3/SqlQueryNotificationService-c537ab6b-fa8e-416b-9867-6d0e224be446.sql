﻿CREATE SERVICE [SqlQueryNotificationService-c537ab6b-fa8e-416b-9867-6d0e224be446]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c537ab6b-fa8e-416b-9867-6d0e224be446]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

