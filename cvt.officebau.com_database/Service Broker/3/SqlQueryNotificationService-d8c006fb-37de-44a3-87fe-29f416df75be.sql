﻿CREATE SERVICE [SqlQueryNotificationService-d8c006fb-37de-44a3-87fe-29f416df75be]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d8c006fb-37de-44a3-87fe-29f416df75be]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

