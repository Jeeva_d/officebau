﻿CREATE SERVICE [SqlQueryNotificationService-7efbd01c-41a6-4e53-82d1-d8d54725968d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7efbd01c-41a6-4e53-82d1-d8d54725968d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

