﻿CREATE SERVICE [SqlQueryNotificationService-64c13b38-5452-4b3e-aae0-11cf5bfe88e1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-64c13b38-5452-4b3e-aae0-11cf5bfe88e1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

