﻿CREATE SERVICE [SqlQueryNotificationService-b4fbf063-1d7e-4d3e-abbd-d99fc7a73dab]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b4fbf063-1d7e-4d3e-abbd-d99fc7a73dab]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

