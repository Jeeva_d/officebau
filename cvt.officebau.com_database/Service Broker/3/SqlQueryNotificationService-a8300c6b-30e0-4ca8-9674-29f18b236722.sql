﻿CREATE SERVICE [SqlQueryNotificationService-a8300c6b-30e0-4ca8-9674-29f18b236722]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a8300c6b-30e0-4ca8-9674-29f18b236722]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

