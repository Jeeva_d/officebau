﻿CREATE SERVICE [SqlQueryNotificationService-a18bd5f3-50e9-40b0-a424-864e17418162]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a18bd5f3-50e9-40b0-a424-864e17418162]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

