﻿CREATE SERVICE [SqlQueryNotificationService-8f93c338-8f97-4a08-b9a3-a4a0ad14626b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8f93c338-8f97-4a08-b9a3-a4a0ad14626b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

