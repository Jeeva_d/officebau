﻿CREATE SERVICE [SqlQueryNotificationService-0c85d7d4-c6de-419c-a394-0f642f7822d9]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-0c85d7d4-c6de-419c-a394-0f642f7822d9]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

