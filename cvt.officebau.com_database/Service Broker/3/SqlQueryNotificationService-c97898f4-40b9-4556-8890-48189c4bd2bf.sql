﻿CREATE SERVICE [SqlQueryNotificationService-c97898f4-40b9-4556-8890-48189c4bd2bf]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c97898f4-40b9-4556-8890-48189c4bd2bf]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

