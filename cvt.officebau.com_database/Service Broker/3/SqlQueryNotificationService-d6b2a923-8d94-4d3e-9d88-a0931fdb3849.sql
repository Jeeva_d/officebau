﻿CREATE SERVICE [SqlQueryNotificationService-d6b2a923-8d94-4d3e-9d88-a0931fdb3849]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d6b2a923-8d94-4d3e-9d88-a0931fdb3849]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

