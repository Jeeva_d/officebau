﻿CREATE SERVICE [SqlQueryNotificationService-cf5e1a9e-c532-4942-b42a-fd3f90b01b60]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-cf5e1a9e-c532-4942-b42a-fd3f90b01b60]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

