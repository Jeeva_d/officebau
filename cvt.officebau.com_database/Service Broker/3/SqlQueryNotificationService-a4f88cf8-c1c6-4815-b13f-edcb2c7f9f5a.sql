﻿CREATE SERVICE [SqlQueryNotificationService-a4f88cf8-c1c6-4815-b13f-edcb2c7f9f5a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a4f88cf8-c1c6-4815-b13f-edcb2c7f9f5a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

