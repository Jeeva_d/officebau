﻿CREATE SERVICE [SqlQueryNotificationService-6754ce06-af8e-4b19-ba10-399c5dff115e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-6754ce06-af8e-4b19-ba10-399c5dff115e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

