﻿CREATE SERVICE [SqlQueryNotificationService-b7870dcc-4d30-455d-927c-d849c5581342]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b7870dcc-4d30-455d-927c-d849c5581342]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

