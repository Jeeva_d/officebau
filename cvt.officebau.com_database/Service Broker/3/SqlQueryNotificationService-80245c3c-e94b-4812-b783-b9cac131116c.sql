﻿CREATE SERVICE [SqlQueryNotificationService-80245c3c-e94b-4812-b783-b9cac131116c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-80245c3c-e94b-4812-b783-b9cac131116c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

