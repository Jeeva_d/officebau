﻿CREATE SERVICE [SqlQueryNotificationService-bfb4eb12-c4c1-4ec0-947f-227f245b003b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bfb4eb12-c4c1-4ec0-947f-227f245b003b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

