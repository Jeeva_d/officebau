﻿CREATE SERVICE [SqlQueryNotificationService-ce0db287-ee0c-43f2-8d13-16d826714bf5]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ce0db287-ee0c-43f2-8d13-16d826714bf5]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

