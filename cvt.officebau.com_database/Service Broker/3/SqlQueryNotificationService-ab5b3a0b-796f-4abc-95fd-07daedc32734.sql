﻿CREATE SERVICE [SqlQueryNotificationService-ab5b3a0b-796f-4abc-95fd-07daedc32734]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ab5b3a0b-796f-4abc-95fd-07daedc32734]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

