﻿CREATE SERVICE [SqlQueryNotificationService-d42b7ea9-d46d-4b1c-9af1-d7a7bcc256e3]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d42b7ea9-d46d-4b1c-9af1-d7a7bcc256e3]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

