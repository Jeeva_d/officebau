﻿CREATE SERVICE [SqlQueryNotificationService-80e02666-068b-443d-9c54-076aa373da69]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-80e02666-068b-443d-9c54-076aa373da69]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

