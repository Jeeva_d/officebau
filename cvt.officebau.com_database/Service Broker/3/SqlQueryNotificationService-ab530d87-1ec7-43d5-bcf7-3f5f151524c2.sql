﻿CREATE SERVICE [SqlQueryNotificationService-ab530d87-1ec7-43d5-bcf7-3f5f151524c2]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ab530d87-1ec7-43d5-bcf7-3f5f151524c2]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

