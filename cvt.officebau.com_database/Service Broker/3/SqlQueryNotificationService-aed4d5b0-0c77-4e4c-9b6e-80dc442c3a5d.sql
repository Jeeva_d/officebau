﻿CREATE SERVICE [SqlQueryNotificationService-aed4d5b0-0c77-4e4c-9b6e-80dc442c3a5d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-aed4d5b0-0c77-4e4c-9b6e-80dc442c3a5d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

