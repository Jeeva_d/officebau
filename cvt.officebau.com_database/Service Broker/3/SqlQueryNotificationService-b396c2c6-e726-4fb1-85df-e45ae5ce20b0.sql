﻿CREATE SERVICE [SqlQueryNotificationService-b396c2c6-e726-4fb1-85df-e45ae5ce20b0]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b396c2c6-e726-4fb1-85df-e45ae5ce20b0]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

