﻿CREATE SERVICE [SqlQueryNotificationService-0ac6ab98-8bd3-46dc-b5c9-e69a0818c17f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-0ac6ab98-8bd3-46dc-b5c9-e69a0818c17f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

