﻿CREATE SERVICE [SqlQueryNotificationService-d537b968-53a6-4b70-b7d2-c4e063d036e2]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d537b968-53a6-4b70-b7d2-c4e063d036e2]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

