﻿CREATE SERVICE [SqlQueryNotificationService-af1a6040-7c7b-4d9a-bea1-a90fd9dc3530]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-af1a6040-7c7b-4d9a-bea1-a90fd9dc3530]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

