﻿CREATE SERVICE [SqlQueryNotificationService-9e6dbf44-31f0-40d4-bf4e-0d5e0420353e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-9e6dbf44-31f0-40d4-bf4e-0d5e0420353e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

