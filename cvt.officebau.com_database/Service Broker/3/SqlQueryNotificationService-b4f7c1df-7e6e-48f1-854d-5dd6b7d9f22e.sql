﻿CREATE SERVICE [SqlQueryNotificationService-b4f7c1df-7e6e-48f1-854d-5dd6b7d9f22e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b4f7c1df-7e6e-48f1-854d-5dd6b7d9f22e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

