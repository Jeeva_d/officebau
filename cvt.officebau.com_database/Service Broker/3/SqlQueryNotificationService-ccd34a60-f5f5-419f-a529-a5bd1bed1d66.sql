﻿CREATE SERVICE [SqlQueryNotificationService-ccd34a60-f5f5-419f-a529-a5bd1bed1d66]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ccd34a60-f5f5-419f-a529-a5bd1bed1d66]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

