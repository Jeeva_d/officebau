﻿CREATE SERVICE [SqlQueryNotificationService-4fde7ce4-9526-46e4-9c29-9dee6af1fdec]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-4fde7ce4-9526-46e4-9c29-9dee6af1fdec]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

