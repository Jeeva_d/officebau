﻿CREATE SERVICE [SqlQueryNotificationService-65a3f09c-e45e-4c7e-922f-dafcf8058569]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-65a3f09c-e45e-4c7e-922f-dafcf8058569]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

