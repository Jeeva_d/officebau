﻿CREATE SERVICE [SqlQueryNotificationService-af7d73d3-5d3f-474d-95ad-0ae439bf5a49]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-af7d73d3-5d3f-474d-95ad-0ae439bf5a49]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

