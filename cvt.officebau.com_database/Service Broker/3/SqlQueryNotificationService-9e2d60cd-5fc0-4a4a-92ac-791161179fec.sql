﻿CREATE SERVICE [SqlQueryNotificationService-9e2d60cd-5fc0-4a4a-92ac-791161179fec]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-9e2d60cd-5fc0-4a4a-92ac-791161179fec]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

