﻿CREATE SERVICE [SqlQueryNotificationService-8fd253dd-cb0b-459b-a6b9-9f4ccb4bf494]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8fd253dd-cb0b-459b-a6b9-9f4ccb4bf494]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

