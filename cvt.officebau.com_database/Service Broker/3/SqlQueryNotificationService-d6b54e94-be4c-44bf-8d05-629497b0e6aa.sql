﻿CREATE SERVICE [SqlQueryNotificationService-d6b54e94-be4c-44bf-8d05-629497b0e6aa]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d6b54e94-be4c-44bf-8d05-629497b0e6aa]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

