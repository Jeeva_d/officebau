﻿CREATE SERVICE [SqlQueryNotificationService-c8fe1d98-9d23-4564-8208-4dc369375fd9]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c8fe1d98-9d23-4564-8208-4dc369375fd9]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

