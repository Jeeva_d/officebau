﻿CREATE SERVICE [SqlQueryNotificationService-1fa4aae5-fb4f-4a2f-b13e-4db348a157da]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-1fa4aae5-fb4f-4a2f-b13e-4db348a157da]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

