﻿CREATE SERVICE [SqlQueryNotificationService-80b3f5b5-d635-4269-9c6c-c5f2887c93ad]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-80b3f5b5-d635-4269-9c6c-c5f2887c93ad]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

