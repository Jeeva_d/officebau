﻿CREATE SERVICE [SqlQueryNotificationService-7ff49725-7e1b-4d3f-9339-933d19a1305f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7ff49725-7e1b-4d3f-9339-933d19a1305f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

