﻿CREATE SERVICE [SqlQueryNotificationService-bb141b1d-4f40-44ea-82bd-12ca2d4188ec]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bb141b1d-4f40-44ea-82bd-12ca2d4188ec]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

