﻿CREATE SERVICE [SqlQueryNotificationService-d76398c1-f6b1-4ead-a5d6-2d2fc15074f7]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d76398c1-f6b1-4ead-a5d6-2d2fc15074f7]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

