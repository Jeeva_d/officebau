﻿CREATE SERVICE [SqlQueryNotificationService-affc6de7-a91e-430b-9c70-523dc1758a6b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-affc6de7-a91e-430b-9c70-523dc1758a6b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

