﻿CREATE SERVICE [SqlQueryNotificationService-7a865ff2-b5df-4f34-bcd2-5696067a2d6b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7a865ff2-b5df-4f34-bcd2-5696067a2d6b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

