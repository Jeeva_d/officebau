﻿CREATE SERVICE [SqlQueryNotificationService-d8cbbe8f-a0f5-4aa3-b31c-fa2bdb13319c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d8cbbe8f-a0f5-4aa3-b31c-fa2bdb13319c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

