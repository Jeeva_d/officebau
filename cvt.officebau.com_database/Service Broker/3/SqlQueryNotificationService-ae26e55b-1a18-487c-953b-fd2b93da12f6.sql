﻿CREATE SERVICE [SqlQueryNotificationService-ae26e55b-1a18-487c-953b-fd2b93da12f6]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ae26e55b-1a18-487c-953b-fd2b93da12f6]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

