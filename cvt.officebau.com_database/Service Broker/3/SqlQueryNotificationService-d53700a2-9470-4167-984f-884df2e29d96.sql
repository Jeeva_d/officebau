﻿CREATE SERVICE [SqlQueryNotificationService-d53700a2-9470-4167-984f-884df2e29d96]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d53700a2-9470-4167-984f-884df2e29d96]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

