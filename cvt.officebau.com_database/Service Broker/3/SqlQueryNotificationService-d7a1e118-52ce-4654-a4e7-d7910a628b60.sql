﻿CREATE SERVICE [SqlQueryNotificationService-d7a1e118-52ce-4654-a4e7-d7910a628b60]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d7a1e118-52ce-4654-a4e7-d7910a628b60]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

