﻿CREATE SERVICE [SqlQueryNotificationService-b5bbcc6b-c302-4ac3-9d3c-8a1d86537af1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b5bbcc6b-c302-4ac3-9d3c-8a1d86537af1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

