﻿CREATE SERVICE [SqlQueryNotificationService-d4ba3836-905d-4484-9894-98d346fa2432]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d4ba3836-905d-4484-9894-98d346fa2432]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

