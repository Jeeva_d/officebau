﻿CREATE SERVICE [SqlQueryNotificationService-bfaff9a0-fbb7-48fb-8aab-19093ee11a1e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bfaff9a0-fbb7-48fb-8aab-19093ee11a1e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

