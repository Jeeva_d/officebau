﻿CREATE SERVICE [SqlQueryNotificationService-bbf5369c-69a4-4b85-a048-a07882df093e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bbf5369c-69a4-4b85-a048-a07882df093e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

