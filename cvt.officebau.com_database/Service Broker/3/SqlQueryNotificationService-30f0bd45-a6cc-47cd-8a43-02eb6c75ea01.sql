﻿CREATE SERVICE [SqlQueryNotificationService-30f0bd45-a6cc-47cd-8a43-02eb6c75ea01]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-30f0bd45-a6cc-47cd-8a43-02eb6c75ea01]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

