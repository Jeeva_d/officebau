﻿CREATE SERVICE [SqlQueryNotificationService-9bd4fca8-32dc-4b82-ac33-3e0d8eab7983]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-9bd4fca8-32dc-4b82-ac33-3e0d8eab7983]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

