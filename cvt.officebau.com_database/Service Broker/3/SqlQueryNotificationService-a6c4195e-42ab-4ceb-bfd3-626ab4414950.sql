﻿CREATE SERVICE [SqlQueryNotificationService-a6c4195e-42ab-4ceb-bfd3-626ab4414950]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a6c4195e-42ab-4ceb-bfd3-626ab4414950]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

