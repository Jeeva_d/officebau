﻿CREATE SERVICE [SqlQueryNotificationService-3f1dd805-1c8c-4f29-a80a-e5ed479e398b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-3f1dd805-1c8c-4f29-a80a-e5ed479e398b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

