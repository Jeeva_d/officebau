﻿CREATE SERVICE [SqlQueryNotificationService-c8c6af6a-bbd3-424d-bfd6-ba9b312508cf]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c8c6af6a-bbd3-424d-bfd6-ba9b312508cf]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

