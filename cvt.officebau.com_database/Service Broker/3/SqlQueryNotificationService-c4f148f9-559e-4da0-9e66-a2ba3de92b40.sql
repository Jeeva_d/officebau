﻿CREATE SERVICE [SqlQueryNotificationService-c4f148f9-559e-4da0-9e66-a2ba3de92b40]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c4f148f9-559e-4da0-9e66-a2ba3de92b40]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

