﻿CREATE SERVICE [SqlQueryNotificationService-d587ded7-fdae-46ad-94f7-66715d8e7978]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d587ded7-fdae-46ad-94f7-66715d8e7978]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

