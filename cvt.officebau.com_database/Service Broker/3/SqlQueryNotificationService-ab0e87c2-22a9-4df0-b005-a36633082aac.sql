﻿CREATE SERVICE [SqlQueryNotificationService-ab0e87c2-22a9-4df0-b005-a36633082aac]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ab0e87c2-22a9-4df0-b005-a36633082aac]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

