﻿CREATE SERVICE [SqlQueryNotificationService-53c98f1d-94e3-41bc-9111-25bf114ac8ee]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-53c98f1d-94e3-41bc-9111-25bf114ac8ee]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

