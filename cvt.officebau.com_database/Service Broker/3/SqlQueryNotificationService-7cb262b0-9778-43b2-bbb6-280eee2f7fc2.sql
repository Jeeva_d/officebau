﻿CREATE SERVICE [SqlQueryNotificationService-7cb262b0-9778-43b2-bbb6-280eee2f7fc2]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7cb262b0-9778-43b2-bbb6-280eee2f7fc2]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

