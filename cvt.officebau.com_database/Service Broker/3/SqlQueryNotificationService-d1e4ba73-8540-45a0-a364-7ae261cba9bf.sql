﻿CREATE SERVICE [SqlQueryNotificationService-d1e4ba73-8540-45a0-a364-7ae261cba9bf]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d1e4ba73-8540-45a0-a364-7ae261cba9bf]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

