﻿CREATE SERVICE [SqlQueryNotificationService-4d76f83a-395a-49e2-9767-d0bca20fc19e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-4d76f83a-395a-49e2-9767-d0bca20fc19e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

