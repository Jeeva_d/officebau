﻿CREATE SERVICE [SqlQueryNotificationService-0fa69aa1-f8f9-4e85-a2d3-10d9d3bdb0d4]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-0fa69aa1-f8f9-4e85-a2d3-10d9d3bdb0d4]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

