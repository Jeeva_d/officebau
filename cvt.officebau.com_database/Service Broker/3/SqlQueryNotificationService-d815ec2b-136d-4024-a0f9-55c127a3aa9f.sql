﻿CREATE SERVICE [SqlQueryNotificationService-d815ec2b-136d-4024-a0f9-55c127a3aa9f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d815ec2b-136d-4024-a0f9-55c127a3aa9f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

