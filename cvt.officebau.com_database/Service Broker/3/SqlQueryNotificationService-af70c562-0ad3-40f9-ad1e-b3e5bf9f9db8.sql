﻿CREATE SERVICE [SqlQueryNotificationService-af70c562-0ad3-40f9-ad1e-b3e5bf9f9db8]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-af70c562-0ad3-40f9-ad1e-b3e5bf9f9db8]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

