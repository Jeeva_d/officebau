﻿CREATE SERVICE [SqlQueryNotificationService-8aec526e-8f60-4851-88d4-7cb71ca689f8]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8aec526e-8f60-4851-88d4-7cb71ca689f8]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

