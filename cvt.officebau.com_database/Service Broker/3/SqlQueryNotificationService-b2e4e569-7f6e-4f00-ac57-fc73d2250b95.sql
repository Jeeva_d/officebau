﻿CREATE SERVICE [SqlQueryNotificationService-b2e4e569-7f6e-4f00-ac57-fc73d2250b95]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b2e4e569-7f6e-4f00-ac57-fc73d2250b95]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

