﻿CREATE SERVICE [SqlQueryNotificationService-ca3b0920-5324-4b0b-85b3-cd50dd7ba981]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ca3b0920-5324-4b0b-85b3-cd50dd7ba981]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

