﻿CREATE SERVICE [SqlQueryNotificationService-bec94f90-0ceb-405b-bbd1-2c414f8b7e2e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bec94f90-0ceb-405b-bbd1-2c414f8b7e2e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

