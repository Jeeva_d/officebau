﻿CREATE SERVICE [SqlQueryNotificationService-d0c4ae04-b6ad-4a98-845f-d96b0abd93aa]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d0c4ae04-b6ad-4a98-845f-d96b0abd93aa]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

