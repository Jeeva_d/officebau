﻿CREATE SERVICE [SqlQueryNotificationService-ca4e99a6-29f3-412c-b08c-43b3da3363cf]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ca4e99a6-29f3-412c-b08c-43b3da3363cf]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

