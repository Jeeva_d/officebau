﻿CREATE SERVICE [SqlQueryNotificationService-38f7a9af-9e4b-4292-9f44-37e016d9707a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-38f7a9af-9e4b-4292-9f44-37e016d9707a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

