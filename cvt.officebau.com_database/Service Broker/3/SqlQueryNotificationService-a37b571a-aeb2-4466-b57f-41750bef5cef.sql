﻿CREATE SERVICE [SqlQueryNotificationService-a37b571a-aeb2-4466-b57f-41750bef5cef]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a37b571a-aeb2-4466-b57f-41750bef5cef]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

