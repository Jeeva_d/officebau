﻿CREATE SERVICE [SqlQueryNotificationService-d84c8dc7-e605-43e4-914a-a1a246126da8]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d84c8dc7-e605-43e4-914a-a1a246126da8]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

