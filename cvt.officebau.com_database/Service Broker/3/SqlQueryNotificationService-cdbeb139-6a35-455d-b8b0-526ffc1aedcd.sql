﻿CREATE SERVICE [SqlQueryNotificationService-cdbeb139-6a35-455d-b8b0-526ffc1aedcd]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-cdbeb139-6a35-455d-b8b0-526ffc1aedcd]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

