﻿CREATE SERVICE [SqlQueryNotificationService-cdcb5a7f-8cd7-4487-9006-887e365f978f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-cdcb5a7f-8cd7-4487-9006-887e365f978f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

