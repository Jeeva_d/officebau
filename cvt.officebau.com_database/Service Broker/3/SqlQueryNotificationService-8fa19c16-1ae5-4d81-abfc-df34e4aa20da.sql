﻿CREATE SERVICE [SqlQueryNotificationService-8fa19c16-1ae5-4d81-abfc-df34e4aa20da]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8fa19c16-1ae5-4d81-abfc-df34e4aa20da]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

