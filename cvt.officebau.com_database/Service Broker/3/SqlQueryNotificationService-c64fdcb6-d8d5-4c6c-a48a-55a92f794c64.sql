﻿CREATE SERVICE [SqlQueryNotificationService-c64fdcb6-d8d5-4c6c-a48a-55a92f794c64]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c64fdcb6-d8d5-4c6c-a48a-55a92f794c64]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

