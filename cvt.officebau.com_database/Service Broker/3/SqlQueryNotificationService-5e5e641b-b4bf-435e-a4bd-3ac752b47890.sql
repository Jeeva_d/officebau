﻿CREATE SERVICE [SqlQueryNotificationService-5e5e641b-b4bf-435e-a4bd-3ac752b47890]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-5e5e641b-b4bf-435e-a4bd-3ac752b47890]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

