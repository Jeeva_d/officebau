﻿CREATE SERVICE [SqlQueryNotificationService-cb66e6f6-4f90-49b3-9594-7026f76420bf]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-cb66e6f6-4f90-49b3-9594-7026f76420bf]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

