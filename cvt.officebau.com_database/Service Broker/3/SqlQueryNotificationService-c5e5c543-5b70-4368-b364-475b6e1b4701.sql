﻿CREATE SERVICE [SqlQueryNotificationService-c5e5c543-5b70-4368-b364-475b6e1b4701]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c5e5c543-5b70-4368-b364-475b6e1b4701]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

