﻿CREATE SERVICE [SqlQueryNotificationService-c979d1be-6d9b-478b-bab9-c003df5b438b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c979d1be-6d9b-478b-bab9-c003df5b438b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

