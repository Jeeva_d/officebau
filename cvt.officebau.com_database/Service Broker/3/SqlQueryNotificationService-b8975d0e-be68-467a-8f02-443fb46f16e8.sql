﻿CREATE SERVICE [SqlQueryNotificationService-b8975d0e-be68-467a-8f02-443fb46f16e8]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b8975d0e-be68-467a-8f02-443fb46f16e8]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

