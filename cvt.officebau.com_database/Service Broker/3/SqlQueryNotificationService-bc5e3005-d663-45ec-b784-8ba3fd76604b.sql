﻿CREATE SERVICE [SqlQueryNotificationService-bc5e3005-d663-45ec-b784-8ba3fd76604b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bc5e3005-d663-45ec-b784-8ba3fd76604b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

