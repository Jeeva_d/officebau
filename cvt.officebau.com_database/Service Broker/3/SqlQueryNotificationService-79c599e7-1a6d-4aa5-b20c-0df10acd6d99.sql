﻿CREATE SERVICE [SqlQueryNotificationService-79c599e7-1a6d-4aa5-b20c-0df10acd6d99]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-79c599e7-1a6d-4aa5-b20c-0df10acd6d99]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

