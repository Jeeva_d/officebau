﻿CREATE SERVICE [SqlQueryNotificationService-d8db8b75-de5f-48c0-8930-56f50aebca4b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d8db8b75-de5f-48c0-8930-56f50aebca4b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

