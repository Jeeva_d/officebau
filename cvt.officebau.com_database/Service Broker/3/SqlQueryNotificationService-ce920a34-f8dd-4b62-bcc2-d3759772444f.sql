﻿CREATE SERVICE [SqlQueryNotificationService-ce920a34-f8dd-4b62-bcc2-d3759772444f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ce920a34-f8dd-4b62-bcc2-d3759772444f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

