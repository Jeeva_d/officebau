﻿CREATE SERVICE [SqlQueryNotificationService-30f928a1-b5a1-4180-8302-790adf7f6c1e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-30f928a1-b5a1-4180-8302-790adf7f6c1e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

