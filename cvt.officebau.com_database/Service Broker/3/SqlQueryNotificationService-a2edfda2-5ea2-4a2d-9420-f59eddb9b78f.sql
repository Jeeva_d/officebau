﻿CREATE SERVICE [SqlQueryNotificationService-a2edfda2-5ea2-4a2d-9420-f59eddb9b78f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a2edfda2-5ea2-4a2d-9420-f59eddb9b78f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

