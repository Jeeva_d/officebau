﻿CREATE SERVICE [SqlQueryNotificationService-cca1135a-a40a-4bdd-863f-350389049b4a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-cca1135a-a40a-4bdd-863f-350389049b4a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

