﻿CREATE SERVICE [SqlQueryNotificationService-a11f058d-005d-457e-9a1c-0e04a5233fc4]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a11f058d-005d-457e-9a1c-0e04a5233fc4]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

