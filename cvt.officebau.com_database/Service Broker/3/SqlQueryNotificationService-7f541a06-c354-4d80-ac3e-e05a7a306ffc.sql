﻿CREATE SERVICE [SqlQueryNotificationService-7f541a06-c354-4d80-ac3e-e05a7a306ffc]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7f541a06-c354-4d80-ac3e-e05a7a306ffc]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

