﻿CREATE SERVICE [SqlQueryNotificationService-d5daa707-6f41-4171-927a-a5c5fb53ea57]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d5daa707-6f41-4171-927a-a5c5fb53ea57]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

