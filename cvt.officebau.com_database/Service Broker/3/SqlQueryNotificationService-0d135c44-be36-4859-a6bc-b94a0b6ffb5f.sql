﻿CREATE SERVICE [SqlQueryNotificationService-0d135c44-be36-4859-a6bc-b94a0b6ffb5f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-0d135c44-be36-4859-a6bc-b94a0b6ffb5f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

