﻿CREATE SERVICE [SqlQueryNotificationService-450e01f0-34ec-4efb-8dbb-a917b365fdfc]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-450e01f0-34ec-4efb-8dbb-a917b365fdfc]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

