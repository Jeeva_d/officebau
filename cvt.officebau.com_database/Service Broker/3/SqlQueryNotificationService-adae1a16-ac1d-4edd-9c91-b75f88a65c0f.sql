﻿CREATE SERVICE [SqlQueryNotificationService-adae1a16-ac1d-4edd-9c91-b75f88a65c0f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-adae1a16-ac1d-4edd-9c91-b75f88a65c0f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

