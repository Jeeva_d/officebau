﻿CREATE SERVICE [SqlQueryNotificationService-c5cc849d-cd06-4ba1-845d-59db716d9763]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c5cc849d-cd06-4ba1-845d-59db716d9763]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

