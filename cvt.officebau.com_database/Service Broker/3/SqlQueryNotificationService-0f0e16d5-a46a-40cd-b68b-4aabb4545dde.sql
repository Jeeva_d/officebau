﻿CREATE SERVICE [SqlQueryNotificationService-0f0e16d5-a46a-40cd-b68b-4aabb4545dde]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-0f0e16d5-a46a-40cd-b68b-4aabb4545dde]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

