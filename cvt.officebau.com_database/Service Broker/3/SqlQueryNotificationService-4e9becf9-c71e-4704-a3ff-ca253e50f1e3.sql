﻿CREATE SERVICE [SqlQueryNotificationService-4e9becf9-c71e-4704-a3ff-ca253e50f1e3]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-4e9becf9-c71e-4704-a3ff-ca253e50f1e3]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

