﻿CREATE SERVICE [SqlQueryNotificationService-6cc2f3a9-92db-4194-a8fb-c4f8092ac87e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-6cc2f3a9-92db-4194-a8fb-c4f8092ac87e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

