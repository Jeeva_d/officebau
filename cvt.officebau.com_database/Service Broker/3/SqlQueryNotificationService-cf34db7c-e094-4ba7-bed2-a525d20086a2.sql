﻿CREATE SERVICE [SqlQueryNotificationService-cf34db7c-e094-4ba7-bed2-a525d20086a2]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-cf34db7c-e094-4ba7-bed2-a525d20086a2]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

