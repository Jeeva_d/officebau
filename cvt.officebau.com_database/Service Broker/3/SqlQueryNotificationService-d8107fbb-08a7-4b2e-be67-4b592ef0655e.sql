﻿CREATE SERVICE [SqlQueryNotificationService-d8107fbb-08a7-4b2e-be67-4b592ef0655e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d8107fbb-08a7-4b2e-be67-4b592ef0655e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

