﻿CREATE SERVICE [SqlQueryNotificationService-b5df3799-dbc9-45a5-9d42-ecb98a97a83e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b5df3799-dbc9-45a5-9d42-ecb98a97a83e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

