﻿CREATE SERVICE [SqlQueryNotificationService-7f268d19-cddc-4b3c-aa5a-c64009ba538d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7f268d19-cddc-4b3c-aa5a-c64009ba538d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

