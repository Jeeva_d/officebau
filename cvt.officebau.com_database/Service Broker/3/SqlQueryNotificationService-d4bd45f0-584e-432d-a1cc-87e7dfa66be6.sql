﻿CREATE SERVICE [SqlQueryNotificationService-d4bd45f0-584e-432d-a1cc-87e7dfa66be6]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d4bd45f0-584e-432d-a1cc-87e7dfa66be6]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

