﻿CREATE SERVICE [SqlQueryNotificationService-bea2a34b-8050-4e1d-b1e9-a2eb2dc2adff]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bea2a34b-8050-4e1d-b1e9-a2eb2dc2adff]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

