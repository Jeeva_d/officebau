﻿CREATE SERVICE [SqlQueryNotificationService-20cc568d-b3f8-43b2-a698-542f6b3dae68]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-20cc568d-b3f8-43b2-a698-542f6b3dae68]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

