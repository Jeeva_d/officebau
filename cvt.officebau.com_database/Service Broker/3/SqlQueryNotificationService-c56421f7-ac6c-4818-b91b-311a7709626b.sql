﻿CREATE SERVICE [SqlQueryNotificationService-c56421f7-ac6c-4818-b91b-311a7709626b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c56421f7-ac6c-4818-b91b-311a7709626b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

