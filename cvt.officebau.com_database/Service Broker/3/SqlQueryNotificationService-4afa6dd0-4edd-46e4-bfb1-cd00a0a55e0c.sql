﻿CREATE SERVICE [SqlQueryNotificationService-4afa6dd0-4edd-46e4-bfb1-cd00a0a55e0c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-4afa6dd0-4edd-46e4-bfb1-cd00a0a55e0c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

