﻿CREATE SERVICE [SqlQueryNotificationService-9b836cce-a25f-4d98-a5b3-0fa34bf44eb1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-9b836cce-a25f-4d98-a5b3-0fa34bf44eb1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

