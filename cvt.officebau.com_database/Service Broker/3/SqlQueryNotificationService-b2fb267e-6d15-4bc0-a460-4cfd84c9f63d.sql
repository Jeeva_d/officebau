﻿CREATE SERVICE [SqlQueryNotificationService-b2fb267e-6d15-4bc0-a460-4cfd84c9f63d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b2fb267e-6d15-4bc0-a460-4cfd84c9f63d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

