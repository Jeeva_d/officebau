﻿CREATE SERVICE [SqlQueryNotificationService-d3835ecd-0a32-4db1-bbf2-d8970715f17b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d3835ecd-0a32-4db1-bbf2-d8970715f17b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

