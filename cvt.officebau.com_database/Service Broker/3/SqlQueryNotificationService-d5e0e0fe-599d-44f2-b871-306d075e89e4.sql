﻿CREATE SERVICE [SqlQueryNotificationService-d5e0e0fe-599d-44f2-b871-306d075e89e4]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d5e0e0fe-599d-44f2-b871-306d075e89e4]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

