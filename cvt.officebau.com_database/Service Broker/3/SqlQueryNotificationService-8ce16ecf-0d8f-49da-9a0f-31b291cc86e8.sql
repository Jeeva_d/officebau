﻿CREATE SERVICE [SqlQueryNotificationService-8ce16ecf-0d8f-49da-9a0f-31b291cc86e8]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8ce16ecf-0d8f-49da-9a0f-31b291cc86e8]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

