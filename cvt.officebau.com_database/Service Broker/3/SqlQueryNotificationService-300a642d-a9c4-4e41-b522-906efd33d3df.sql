﻿CREATE SERVICE [SqlQueryNotificationService-300a642d-a9c4-4e41-b522-906efd33d3df]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-300a642d-a9c4-4e41-b522-906efd33d3df]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

