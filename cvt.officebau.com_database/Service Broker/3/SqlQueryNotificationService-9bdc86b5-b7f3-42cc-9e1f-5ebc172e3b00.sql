﻿CREATE SERVICE [SqlQueryNotificationService-9bdc86b5-b7f3-42cc-9e1f-5ebc172e3b00]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-9bdc86b5-b7f3-42cc-9e1f-5ebc172e3b00]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

