﻿CREATE SERVICE [SqlQueryNotificationService-60ab6c3b-e248-4d60-a141-1da9c221f4c3]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-60ab6c3b-e248-4d60-a141-1da9c221f4c3]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

