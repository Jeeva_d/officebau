﻿CREATE SERVICE [SqlQueryNotificationService-b76ad1f3-b853-4c72-be88-a18da2f1bfe9]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b76ad1f3-b853-4c72-be88-a18da2f1bfe9]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

