﻿CREATE SERVICE [SqlQueryNotificationService-6cd37c08-4b3b-4aee-ad4c-ea595fda721e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-6cd37c08-4b3b-4aee-ad4c-ea595fda721e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

