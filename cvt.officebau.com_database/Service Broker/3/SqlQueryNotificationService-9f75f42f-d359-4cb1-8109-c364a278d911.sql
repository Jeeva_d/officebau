﻿CREATE SERVICE [SqlQueryNotificationService-9f75f42f-d359-4cb1-8109-c364a278d911]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-9f75f42f-d359-4cb1-8109-c364a278d911]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

