﻿CREATE SERVICE [SqlQueryNotificationService-cf7e9216-9bb5-4432-84c5-6cb081bfea6a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-cf7e9216-9bb5-4432-84c5-6cb081bfea6a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

