﻿CREATE SERVICE [SqlQueryNotificationService-5d0e0a5e-de60-42be-a03d-5cdac7866ec3]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-5d0e0a5e-de60-42be-a03d-5cdac7866ec3]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

