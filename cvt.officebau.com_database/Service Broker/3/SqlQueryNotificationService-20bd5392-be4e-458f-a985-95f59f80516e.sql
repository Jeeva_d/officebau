﻿CREATE SERVICE [SqlQueryNotificationService-20bd5392-be4e-458f-a985-95f59f80516e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-20bd5392-be4e-458f-a985-95f59f80516e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

