﻿CREATE SERVICE [SqlQueryNotificationService-d4ce642c-9570-4dfd-99a4-9f3fd340d2a5]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d4ce642c-9570-4dfd-99a4-9f3fd340d2a5]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

