﻿CREATE SERVICE [SqlQueryNotificationService-bfdcb2c9-f8ac-4166-8be1-14d5f12be482]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bfdcb2c9-f8ac-4166-8be1-14d5f12be482]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

