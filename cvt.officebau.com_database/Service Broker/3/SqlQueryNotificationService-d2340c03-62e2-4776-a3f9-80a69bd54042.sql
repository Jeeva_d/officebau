﻿CREATE SERVICE [SqlQueryNotificationService-d2340c03-62e2-4776-a3f9-80a69bd54042]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d2340c03-62e2-4776-a3f9-80a69bd54042]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

