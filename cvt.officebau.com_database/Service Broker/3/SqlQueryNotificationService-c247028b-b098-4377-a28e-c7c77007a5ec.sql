﻿CREATE SERVICE [SqlQueryNotificationService-c247028b-b098-4377-a28e-c7c77007a5ec]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c247028b-b098-4377-a28e-c7c77007a5ec]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

