﻿CREATE SERVICE [SqlQueryNotificationService-6c4fafe2-c17b-4bcd-9e1b-d84d429426d9]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-6c4fafe2-c17b-4bcd-9e1b-d84d429426d9]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

