﻿CREATE SERVICE [SqlQueryNotificationService-6d4d715a-3df0-46f2-b198-bc6858eb222e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-6d4d715a-3df0-46f2-b198-bc6858eb222e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

