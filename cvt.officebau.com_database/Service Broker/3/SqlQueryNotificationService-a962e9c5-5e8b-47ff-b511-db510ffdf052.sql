﻿CREATE SERVICE [SqlQueryNotificationService-a962e9c5-5e8b-47ff-b511-db510ffdf052]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a962e9c5-5e8b-47ff-b511-db510ffdf052]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

