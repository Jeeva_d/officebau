﻿CREATE SERVICE [SqlQueryNotificationService-30b4bc2f-b674-4a72-984b-0308adb11e8a]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-30b4bc2f-b674-4a72-984b-0308adb11e8a]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

