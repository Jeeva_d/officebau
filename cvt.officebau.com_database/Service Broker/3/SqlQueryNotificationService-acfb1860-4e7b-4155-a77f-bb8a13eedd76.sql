﻿CREATE SERVICE [SqlQueryNotificationService-acfb1860-4e7b-4155-a77f-bb8a13eedd76]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-acfb1860-4e7b-4155-a77f-bb8a13eedd76]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

