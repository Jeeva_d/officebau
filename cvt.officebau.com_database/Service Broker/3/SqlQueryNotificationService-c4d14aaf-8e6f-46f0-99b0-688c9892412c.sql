﻿CREATE SERVICE [SqlQueryNotificationService-c4d14aaf-8e6f-46f0-99b0-688c9892412c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c4d14aaf-8e6f-46f0-99b0-688c9892412c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

