﻿CREATE SERVICE [SqlQueryNotificationService-7ed9b7aa-858b-461b-b5dd-80a96dfcc056]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7ed9b7aa-858b-461b-b5dd-80a96dfcc056]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

