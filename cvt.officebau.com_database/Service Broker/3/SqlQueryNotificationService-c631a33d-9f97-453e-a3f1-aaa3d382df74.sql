﻿CREATE SERVICE [SqlQueryNotificationService-c631a33d-9f97-453e-a3f1-aaa3d382df74]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c631a33d-9f97-453e-a3f1-aaa3d382df74]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

