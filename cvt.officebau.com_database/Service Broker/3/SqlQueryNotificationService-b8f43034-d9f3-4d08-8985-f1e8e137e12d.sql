﻿CREATE SERVICE [SqlQueryNotificationService-b8f43034-d9f3-4d08-8985-f1e8e137e12d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b8f43034-d9f3-4d08-8985-f1e8e137e12d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

