﻿CREATE SERVICE [SqlQueryNotificationService-80892fbe-149d-4c03-81db-a15ce8cdcf2f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-80892fbe-149d-4c03-81db-a15ce8cdcf2f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

