﻿CREATE SERVICE [SqlQueryNotificationService-d5cb2c88-7f88-4624-9be1-0b85ce26f905]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d5cb2c88-7f88-4624-9be1-0b85ce26f905]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

