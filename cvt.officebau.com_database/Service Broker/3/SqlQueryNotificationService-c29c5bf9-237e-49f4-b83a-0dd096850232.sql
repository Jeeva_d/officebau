﻿CREATE SERVICE [SqlQueryNotificationService-c29c5bf9-237e-49f4-b83a-0dd096850232]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c29c5bf9-237e-49f4-b83a-0dd096850232]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

