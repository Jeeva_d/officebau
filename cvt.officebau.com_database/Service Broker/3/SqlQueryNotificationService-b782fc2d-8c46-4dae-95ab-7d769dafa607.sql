﻿CREATE SERVICE [SqlQueryNotificationService-b782fc2d-8c46-4dae-95ab-7d769dafa607]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-b782fc2d-8c46-4dae-95ab-7d769dafa607]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

