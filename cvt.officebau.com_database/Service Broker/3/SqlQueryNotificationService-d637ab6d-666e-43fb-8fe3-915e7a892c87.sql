﻿CREATE SERVICE [SqlQueryNotificationService-d637ab6d-666e-43fb-8fe3-915e7a892c87]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d637ab6d-666e-43fb-8fe3-915e7a892c87]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

