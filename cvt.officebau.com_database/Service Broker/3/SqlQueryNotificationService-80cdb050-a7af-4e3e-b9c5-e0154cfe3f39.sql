﻿CREATE SERVICE [SqlQueryNotificationService-80cdb050-a7af-4e3e-b9c5-e0154cfe3f39]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-80cdb050-a7af-4e3e-b9c5-e0154cfe3f39]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

