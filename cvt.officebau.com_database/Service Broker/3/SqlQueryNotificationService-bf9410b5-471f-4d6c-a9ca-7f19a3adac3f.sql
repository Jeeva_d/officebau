﻿CREATE SERVICE [SqlQueryNotificationService-bf9410b5-471f-4d6c-a9ca-7f19a3adac3f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-bf9410b5-471f-4d6c-a9ca-7f19a3adac3f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

