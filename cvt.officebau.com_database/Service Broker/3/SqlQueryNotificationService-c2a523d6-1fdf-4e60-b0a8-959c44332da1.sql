﻿CREATE SERVICE [SqlQueryNotificationService-c2a523d6-1fdf-4e60-b0a8-959c44332da1]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c2a523d6-1fdf-4e60-b0a8-959c44332da1]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

