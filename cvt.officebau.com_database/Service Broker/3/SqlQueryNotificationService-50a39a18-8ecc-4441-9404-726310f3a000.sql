﻿CREATE SERVICE [SqlQueryNotificationService-50a39a18-8ecc-4441-9404-726310f3a000]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-50a39a18-8ecc-4441-9404-726310f3a000]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

