﻿CREATE SERVICE [SqlQueryNotificationService-acfdf881-f59c-4681-a8f7-c466fc6f361d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-acfdf881-f59c-4681-a8f7-c466fc6f361d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

