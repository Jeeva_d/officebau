﻿CREATE SERVICE [SqlQueryNotificationService-8b931b5d-55a3-49ab-af69-b3c8f092d84c]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-8b931b5d-55a3-49ab-af69-b3c8f092d84c]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

