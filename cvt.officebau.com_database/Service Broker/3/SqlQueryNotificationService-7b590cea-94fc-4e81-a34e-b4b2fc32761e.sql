﻿CREATE SERVICE [SqlQueryNotificationService-7b590cea-94fc-4e81-a34e-b4b2fc32761e]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-7b590cea-94fc-4e81-a34e-b4b2fc32761e]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

