﻿CREATE SERVICE [SqlQueryNotificationService-4ba97b06-edf0-4ad3-8ec9-bb8fb3bf9460]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-4ba97b06-edf0-4ad3-8ec9-bb8fb3bf9460]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

