﻿CREATE SERVICE [SqlQueryNotificationService-35e2c2be-a66e-4131-a6c5-44477d7b182b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-35e2c2be-a66e-4131-a6c5-44477d7b182b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

