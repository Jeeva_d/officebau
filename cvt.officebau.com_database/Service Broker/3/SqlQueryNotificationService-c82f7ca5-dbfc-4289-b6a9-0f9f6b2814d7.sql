﻿CREATE SERVICE [SqlQueryNotificationService-c82f7ca5-dbfc-4289-b6a9-0f9f6b2814d7]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c82f7ca5-dbfc-4289-b6a9-0f9f6b2814d7]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

