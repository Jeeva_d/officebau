﻿CREATE SERVICE [SqlQueryNotificationService-c91bd639-8b71-401a-905d-596c129166ea]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c91bd639-8b71-401a-905d-596c129166ea]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

