﻿CREATE SERVICE [SqlQueryNotificationService-a74f9b40-7a2b-465a-9e7d-eb9ff7a92b9b]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a74f9b40-7a2b-465a-9e7d-eb9ff7a92b9b]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

