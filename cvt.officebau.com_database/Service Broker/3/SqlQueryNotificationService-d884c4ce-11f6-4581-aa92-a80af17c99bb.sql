﻿CREATE SERVICE [SqlQueryNotificationService-d884c4ce-11f6-4581-aa92-a80af17c99bb]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d884c4ce-11f6-4581-aa92-a80af17c99bb]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

