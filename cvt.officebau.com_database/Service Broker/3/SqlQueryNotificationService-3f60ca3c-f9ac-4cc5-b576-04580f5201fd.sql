﻿CREATE SERVICE [SqlQueryNotificationService-3f60ca3c-f9ac-4cc5-b576-04580f5201fd]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-3f60ca3c-f9ac-4cc5-b576-04580f5201fd]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

