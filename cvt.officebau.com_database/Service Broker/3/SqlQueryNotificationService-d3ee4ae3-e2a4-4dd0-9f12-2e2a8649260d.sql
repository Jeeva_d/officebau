﻿CREATE SERVICE [SqlQueryNotificationService-d3ee4ae3-e2a4-4dd0-9f12-2e2a8649260d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d3ee4ae3-e2a4-4dd0-9f12-2e2a8649260d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

