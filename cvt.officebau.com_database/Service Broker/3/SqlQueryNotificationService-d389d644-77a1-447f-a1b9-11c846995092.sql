﻿CREATE SERVICE [SqlQueryNotificationService-d389d644-77a1-447f-a1b9-11c846995092]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d389d644-77a1-447f-a1b9-11c846995092]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

