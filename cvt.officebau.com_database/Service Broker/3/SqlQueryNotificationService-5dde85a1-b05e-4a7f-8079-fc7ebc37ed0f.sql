﻿CREATE SERVICE [SqlQueryNotificationService-5dde85a1-b05e-4a7f-8079-fc7ebc37ed0f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-5dde85a1-b05e-4a7f-8079-fc7ebc37ed0f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

