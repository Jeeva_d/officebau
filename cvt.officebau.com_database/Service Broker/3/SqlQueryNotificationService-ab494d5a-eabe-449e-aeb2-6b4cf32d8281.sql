﻿CREATE SERVICE [SqlQueryNotificationService-ab494d5a-eabe-449e-aeb2-6b4cf32d8281]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-ab494d5a-eabe-449e-aeb2-6b4cf32d8281]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

