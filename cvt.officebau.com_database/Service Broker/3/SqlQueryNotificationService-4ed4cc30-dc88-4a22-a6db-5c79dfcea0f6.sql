﻿CREATE SERVICE [SqlQueryNotificationService-4ed4cc30-dc88-4a22-a6db-5c79dfcea0f6]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-4ed4cc30-dc88-4a22-a6db-5c79dfcea0f6]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

