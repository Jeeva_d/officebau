﻿CREATE SERVICE [SqlQueryNotificationService-9f7df204-91cc-46ba-8dbc-e43558b2fd9d]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-9f7df204-91cc-46ba-8dbc-e43558b2fd9d]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

