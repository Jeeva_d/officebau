﻿CREATE SERVICE [SqlQueryNotificationService-d8a789b8-4ead-45df-bfd8-2ab808348a13]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-d8a789b8-4ead-45df-bfd8-2ab808348a13]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

