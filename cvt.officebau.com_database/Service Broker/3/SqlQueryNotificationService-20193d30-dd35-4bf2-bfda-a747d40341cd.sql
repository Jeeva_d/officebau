﻿CREATE SERVICE [SqlQueryNotificationService-20193d30-dd35-4bf2-bfda-a747d40341cd]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-20193d30-dd35-4bf2-bfda-a747d40341cd]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

