﻿CREATE SERVICE [SqlQueryNotificationService-c9cb0711-7438-48bd-920e-59d74e09f68f]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-c9cb0711-7438-48bd-920e-59d74e09f68f]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

