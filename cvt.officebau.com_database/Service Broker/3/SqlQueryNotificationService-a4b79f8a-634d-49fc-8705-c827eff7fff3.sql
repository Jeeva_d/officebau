﻿CREATE SERVICE [SqlQueryNotificationService-a4b79f8a-634d-49fc-8705-c827eff7fff3]
    AUTHORIZATION [dbo]
    ON QUEUE [dbo].[SqlQueryNotificationService-a4b79f8a-634d-49fc-8705-c827eff7fff3]
    ([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification]);

