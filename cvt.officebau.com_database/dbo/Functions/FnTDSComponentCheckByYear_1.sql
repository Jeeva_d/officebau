﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	 select dbo.[FnTDSComponentCheckByYear] (2018,20)
 </summary>                         
 *****************************************************************************/
CREATE FUNCTION [dbo].[FnTDSComponentCheckByYear] (@Year           SMALLINT,
                                                   @ComponentValue MONEY)
RETURNS MONEY
  BEGIN
      DECLARE @Output MONEY

      SET @Output= CASE
                     WHEN @Year <= 2017 THEN
                       Isnull(@ComponentValue, 0)
                     ELSE
                       0
                   END

      RETURN @Output
  END
