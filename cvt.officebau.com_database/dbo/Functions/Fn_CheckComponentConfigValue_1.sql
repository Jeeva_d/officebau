﻿CREATE FUNCTION [dbo].[Fn_CheckComponentConfigValue] (@ComponentCode  VARCHAR(100),
                                                      @BusinessUnitID INT,
                                                      @DomainID       INT)
RETURNS BIT
  BEGIN
      RETURN ISNULL((SELECT ISNULL(pcc.ConfigValue, 0)
                     FROM   tbl_PayrollComponentConfiguration pcc
                            LEFT JOIN tbl_PayrollComponentMaster pcm
                                   ON pcc.PayrollComponentID = pcm.ID
                     WHERE  pcm.Code = @ComponentCode
                            AND pcc.BusinessUnitID = @BusinessUnitID), 0)
  END
