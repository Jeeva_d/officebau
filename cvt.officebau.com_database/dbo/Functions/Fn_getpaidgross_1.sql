﻿CREATE FUNCTION [dbo].[Fn_getpaidgross] (@Gross              DECIMAL(18, 2),
                                        @Basic              DECIMAL(18, 2),
                                        @Conveyance         DECIMAL(18, 2),
                                        @MedicalAllowance   DECIMAL(18, 2),
                                        @HRA                DECIMAL(18, 2),
                                        @EducationAllowance DECIMAL(18, 2),
                                        @MagazineAllowance  DECIMAL(18, 2),
                                        @WorkDays           DECIMAL(18, 2),
                                        @ActualWorkDays     DECIMAL(18, 2))
RETURNS DECIMAL(18, 2)
  BEGIN
      RETURN Round(( dbo.Fn_getlopamount(@Basic, @Workdays, @ActualWorkDays)
                     + dbo.Fn_getlopamount(@Conveyance, @Workdays, @ActualWorkDays)
                     + dbo.Fn_getlopamount(@MagazineAllowance, @Workdays, @ActualWorkDays)
                     + dbo.Fn_getlopamount(@EducationAllowance, @Workdays, @ActualWorkDays)
                     + dbo.Fn_getlopamount((Round(@Gross, 0) - ( Round(@Basic, 0)+ Round(@MedicalAllowance, 0)+ Round(@HRA, 0) + Round(@Conveyance, 0) + Round(@MagazineAllowance, 0) + Round(@EducationAllowance, 0) )), @Workdays, @ActualWorkDays) ), 0)
  END
