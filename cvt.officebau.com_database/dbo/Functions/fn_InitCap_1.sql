﻿CREATE FUNCTION [dbo].[fn_InitCap] (
 @string varchar(255)
)  
RETURNS varchar(255) AS

BEGIN 

 RETURN upper(left(@string, 1)) + right(@string, len(@string) - 1) 

END




