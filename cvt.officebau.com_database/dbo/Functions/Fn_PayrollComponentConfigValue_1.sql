﻿CREATE FUNCTION [dbo].[Fn_PayrollComponentConfigValue] ()
RETURNS TABLE
AS
    RETURN
      (SELECT BusinessUnitID,
              Cast(EducationAllow AS BIT) AS IsEducationAllow,
              Cast(OverTime AS BIT)       AS IsOverTime,
              Cast(EEPF AS BIT)           AS IsEEPF,
              Cast(EEESI AS BIT)          AS IsEEESI,
              Cast(PT AS BIT)             AS IsPT,
              Cast(TDS AS BIT)            AS IsTDS,
              Cast(ERPF AS BIT)           AS IsERPF,
              Cast(ERESI AS BIT)          AS IsERESI,
              Cast(GRATUITY AS BIT)       AS IsGratuity,
              Cast(PLI AS BIT)            AS IsPLI,
              Cast(MEDICLAIM AS BIT)      AS IsMediclaim,
              Cast(MOBILEDATACARD AS BIT) AS IsMobileDatacard,
              Cast(OTHERPERKS AS BIT)     AS IsOtherPerks
       FROM   (SELECT code,
                      ISNULL(pcc.ConfigValue, 0) AS ConfigValue,
                      BusinessUnitID
               FROM   tbl_PayrollComponentConfiguration pcc
                      LEFT JOIN tbl_PayrollComponentMaster pcm
                             ON pcc.PayrollComponentID = pcm.ID) x
              PIVOT( Max(ConfigValue)
                   FOR code IN ( EducationAllow,
                                 OverTime,
                                 EEPF,
                                 EEESI,
                                 PT,
                                 TDS,
                                 ERPF,
                                 ERESI,
                                 GRATUITY,
                                 PLI,
                                 MEDICLAIM,
                                 MOBILEDATACARD,
                                 OTHERPERKS)) p)
