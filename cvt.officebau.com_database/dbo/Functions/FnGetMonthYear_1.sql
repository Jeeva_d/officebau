﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	select [FnGetMonthYear] 7,2015
 </summary>                         
 *****************************************************************************/
 CREATE FUNCTION [dbo].[FnGetMonthYear]
 (
	@Month	TINYINT	,
	@Year	SMALLINT
 )
 RETURNS @Output TABLE(MonthYear NVARCHAR(MAX),RowNo INT)
 BEGIN
	DECLARE @TempMonth	TINYINT		,
			@TempYear	SMALLINT	,
			@TempM		SMALLINT	,
			@TempY		SMALLINT	
		SET @TempMonth = ISNULL(@Month,DATEPART(MONTH,GETDATE()))
		SET @TempYear  = ISNULL(@Year,DATEPART(YEAR,GETDATE()))
		
				DECLARE @TempDate	DATE		,
						@TempPreviousMonthDate DATE,
						@TempPrevMonthDate   DATE,
						@TempFourthMonthDate   DATE
				DECLARE @Temp	TABLE (ID TINYINT ,MonthYear VARCHAR(50))
				DECLARE @TempResult TABLE ( SalaryMonth VARCHAR(100), SalaryAmount MONEY)
				
				SET @TempM = @TempMonth - DATEPART(MONTH,GETDATE())
				SET @TempY = @TempYear - DATEPART(YEAR,GETDATE())
				SET @TempDate = DATEADD(MM,@TempM,GETDATE())
				SET @TempDate = DATEADD(YY,@TempY,@TempDate)
				SET @TempPreviousMonthDate  = DATEADD(MONTH, -1, @TempDate)
				SET @TempPrevMonthDate		= DATEADD(MONTH, 1,@TempDate)
				SET @TempFourthMonthDate  = DATEADD(MONTH, -2, @TempDate)
				
				INSERT INTO @Temp 
				SELECT 3 as [order],(LEFT(DATENAME(MM,@TempDate),3)) +' '+ CONVERT(VARCHAR(10), DATEPART(YEAR,@TempDate))
				UNION 
				SELECT 2 as [order],(LEFT(DATENAME(MM,@TempPreviousMonthDate),3)) +' '+ CONVERT(VARCHAR(10),DATEPART(YEAR,@TempPreviousMonthDate))
				UNION 
				SELECT 4 as [order],(LEFT(DATENAME(MM,@TempPrevMonthDate),3)) + ' '+ CONVERT(VARCHAR(10),DATEPART(YEAR,@TempPrevMonthDate))
				UNION 
				SELECT 1 as [order],(LEFT(DATENAME(MM,@TempFourthMonthDate),3)) + ' '+ CONVERT(VARCHAR(10),DATEPART(YEAR,@TempFourthMonthDate))
				
				INSERT INTO @Output
				SELECT MonthYear, ID FROM @Temp 
			RETURN 
 END
