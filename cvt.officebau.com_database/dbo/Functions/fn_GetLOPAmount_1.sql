﻿CREATE FUNCTION [dbo].[fn_GetLOPAmount] (@Amount         DECIMAL(18, 2),
                                        @WorkDays       DECIMAL(18, 2),
                                        @ActualWorkDays DECIMAL(18, 2))
RETURNS DECIMAL(18, 2)
  BEGIN
      RETURN Round(@Amount / @ActualWorkDays * @Workdays / 1, 0)
  END
