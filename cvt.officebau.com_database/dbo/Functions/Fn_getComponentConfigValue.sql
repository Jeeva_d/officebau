﻿CREATE FUNCTION [dbo].[Fn_getComponentConfigValue] (@Value          MONEY,
                                                   @ComponentCode           VARCHAR(100),
                                                   @BusinessUnitID INT,
                                                   @DomainID       INT)
RETURNS MONEY
  BEGIN
      RETURN ISNULL((SELECT CASE
                              WHEN ISNULL(pcc.ConfigValue, 0) = 0 THEN
                                0
                              ELSE
                                @Value
                            END
                     FROM   tbl_PayrollComponentConfiguration pcc
                            LEFT JOIN tbl_PayrollComponentMaster pcm
                                   ON pcc.PayrollComponentID = pcm.ID
                     WHERE  pcm.Code = @ComponentCode
                            AND pcc.BusinessUnitID = @BusinessUnitID), 0)
  END
