﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:
MODIFIED DATE		:	
 <summary>        
		 SELECT * FROM dbo.FnSplitString('1,2,3',',')
 </summary>                         
 *****************************************************************************/
CREATE FUNCTION [dbo].[FnSplitString] (@String    NVARCHAR(MAX),
                                      @Delimiter CHAR(1))
RETURNS @Output TABLE(
  Splitdata NVARCHAR(MAX))
  BEGIN
      DECLARE @Start INT,
              @End   INT

      SELECT @Start = 1,
             @End = Charindex(@Delimiter, @String)

      WHILE @Start < Len(@String) + 1
        BEGIN
            IF @End = 0
              SET @End = Len(@String) + 1

            INSERT INTO @Output
                        (Splitdata)
            VALUES     (Substring(@String, @Start, @End - @Start))

            SET @Start = @End + 1
            SET @End = Charindex(@Delimiter, @String, @Start)
        END

      RETURN
  END
