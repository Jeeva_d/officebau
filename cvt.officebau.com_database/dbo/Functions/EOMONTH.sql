﻿CREATE FUNCTION  [dbo].[EOMONTH]
(
    @date datetime,
    @months int
)
RETURNS datetime
AS
BEGIN
     declare @eom datetime
     declare @d datetime
     set @d = dateadd(MONTH, @months, @date)
     select @eom =   dateadd(SECOND,-1,DATEADD(MONTH,datediff(MONTH,0,@d)+1,0))
    RETURN  @eom 

END
