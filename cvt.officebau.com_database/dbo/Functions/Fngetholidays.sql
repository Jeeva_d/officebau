﻿-- =============================================
-- select * from dbo.GetHolidays(2015) OPTION (MaxRecursion 1000)
-- =============================================
CREATE FUNCTION [dbo].[Fngetholidays] (@year SMALLINT)
RETURNS @HOLIDAYS TABLE (
  FromDate DATETIME )
AS
  BEGIN
      DECLARE @FirstDateOfYear DATETIME,
              @LastDateOfYear  DATETIME

      --			,@year INT
      --	select @year = 2015;
      SELECT @FirstDateOfYear = Dateadd(yyyy, @Year - 1900, 0);

      SELECT @LastDateOfYear = Dateadd(yyyy, @Year - 1900 + 1, 0);

      WITH cte
           AS (SELECT 1                              AS DayID,
                      @FirstDateOfYear               AS FromDate,
                      Datename(dw, @FirstDateOfYear) AS Dayname
               UNION ALL
               SELECT cte.DayID + 1                             AS DayID,
                      Dateadd(d, 1, cte.FromDate),
                      Datename(dw, Dateadd(d, 1, cte.FromDate)) AS Dayname
               FROM   CTE
               WHERE  Dateadd(d, 1, cte.FromDate) < @LastDateOfYear)
      INSERT INTO @HOLIDAYS
      SELECT FromDate
      FROM   CTE
      WHERE  DayName IN ( 'Saturday', 'Sunday' )
             AND ( CASE
                     WHEN DayName = 'Saturday' THEN ( ( Floor((Day(FromDate)-1)/7) + 1 ) % 2 )
                     ELSE 0
                   END ) = 0
      OPTION (maxrecursion 0)

      --OPTION (MaxRecursion 370)
      RETURN
  END
