﻿CREATE FUNCTION [dbo].[Fn_GetPTAmount] (@Gross          DECIMAL(18, 2),
                                       @MonthID        INT,
                                       @FYID           INT,
                                       @DOJ            DATETIME,
                                       @BusinessUnitID INT,
                                       @WorkDays       DECIMAL(18, 2),
                                       @ActualWorkDays DECIMAL(18, 2),
                                       @DomainID       INT)
RETURNS DECIMAL(18, 2)
  BEGIN
      IF( @MonthID >= 4 )
        SET @FYID=@FYID
      ELSE
        SET @FYID =(SELECT NAME
                    FROM   tbl_FinancialYear
                    WHERE  IsDeleted = 0
                           AND NAME = @FYID - 1
                           AND DomainID = @DomainID)

      DECLARE @NoOfMonths INT

      SET @MonthID =(SELECT MapID
                     FROM   tbl_Month
                     WHERE  id = @MonthID)
      SET @NoOfMonths=CASE
                        WHEN( Month(@DOJ) = 4
                              AND Year(@DOJ) = @FYID
                              AND @MonthID BETWEEN 1 AND 6 )THEN
                          6
                        WHEN( Month(@DOJ) = 5
                              AND Year(@DOJ) = @FYID
                              AND @MonthID BETWEEN 1 AND 6 )THEN
                          5
                        WHEN( Month(@DOJ) = 6
                              AND Year(@DOJ) = @FYID
                              AND @MonthID BETWEEN 1 AND 6 )THEN
                          4
                        WHEN( Month(@DOJ) = 7
                              AND Year(@DOJ) = @FYID
                              AND @MonthID BETWEEN 1 AND 6 )THEN
                          3
                        WHEN( Month(@DOJ) = 8
                              AND Year(@DOJ) = @FYID
                              AND @MonthID BETWEEN 1 AND 6 )THEN
                          2
                        WHEN( Month(@DOJ) = 9
                              AND Year(@DOJ) = @FYID
                              AND @MonthID BETWEEN 1 AND 6 )THEN
                          1
                        WHEN( Month(@DOJ) = 10
                              AND Year(@DOJ) = @FYID
                              AND @MonthID BETWEEN 7 AND 12 )THEN
                          6
                        WHEN( Month(@DOJ) = 11
                              AND Year(@DOJ) = @FYID
                              AND @MonthID BETWEEN 7 AND 12 )THEN
                          5
                        WHEN( Month(@DOJ) = 12
                              AND Year(@DOJ) = @FYID
                              AND @MonthID BETWEEN 7 AND 12 )THEN
                          4
                        WHEN( Month(@DOJ) = 1
                              AND Year(@DOJ) = @FYID + 1
                              AND @MonthID BETWEEN 7 AND 12 )THEN
                          3
                        WHEN( Month(@DOJ) = 2
                              AND Year(@DOJ) = @FYID + 1
                              AND @MonthID BETWEEN 7 AND 12 )THEN
                          2
                        WHEN( Month(@DOJ) = 3
                              AND Year(@DOJ) = @FYID + 1
                              AND @MonthID BETWEEN 7 AND 12 )THEN
                          1
                        ELSE
                          6
                      END

      RETURN ( (SELECT TOP 1 ( Amount )
                FROM   tbl_PTConfiguration
                WHERE  BusinessUnitID = @BusinessUnitID
                       AND FYID = @FYID
                       AND IsDeleted = 0
                       AND ( @Gross / @ActualWorkDays * @Workdays / 1 )BETWEEN MinGross AND MaxGross
                ORDER  BY ModifiedOn DESC) / 1 )
  END
