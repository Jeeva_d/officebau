﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	06-Jun-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
 [ChangePassword] 338,'uV1a4cB7BJTZ5nQKDUorxyRypp4LgdtwQjZtvCSddXE=',1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ChangePassword] (@UserID   INT,
                                        @Password VARCHAR(100),
                                        @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @LoginHistoryID INT = (SELECT TOP 1 ID
             FROM   tbl_LoginHistory
             WHERE  EmployeeID = @UserID
                    AND DomainID = @DomainID
             ORDER  BY ID DESC)

          -- Update Password --
          UPDATE tbl_Login
          SET    Password = @Password,
                 IsChanged = 1,
                 RecentPasswordChangeDate = Getdate()
          WHERE  EmployeeID = @UserID
                 AND DomainID = @DomainID

          -- Insert Password Audit History --
          INSERT tbl_PasswordAudit
                 (LoginHistoryID,
                  ChangedOn,
                  ModifiedOn,
                  DomainID)
          VALUES (ISNULL(@LoginHistoryID,0),
                  Getdate(),
                  Getdate(),
                  @DomainID)

          -- Reset Password
          UPDATE tbl_PasswordReset
          SET    IsUsed = 1
          WHERE  EmployeeID = @UserID
                 AND IsExpired = 0
                 AND DomainID = @domainID

          SELECT 'VALID' AS UserMessage

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
