﻿/**************************************************************************** 
CREATED BY			:	Dhanalakshmi.S
CREATED DATE		:	10-OCT-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
    [Managemissedpunchapprove] 0,4,1,4,12,'12',1,0,1,1,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Managemissedpunchapprove] (@ID           INT,
                                                  @Status       VARCHAR(50),

                                                  --@HRApproverID INT,
                                                  @HRApprovedOn DATETIME,
                                                  @HRRemarks    VARCHAR(8000),
                                                  @IsDeleted    BIT,
                                                  @ModifiedBy   INT,
                                                  @DomainID     INT,
                                                  @EmployeeID   INT,
                                                  @PunchDate    DATE,
                                                  @PunchTypeID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output         VARCHAR(1000) = 'Operation failed!',
              @PunchTime      TIME = (SELECT PunchTime
                 FROM   tbl_EmployeeMissedPunches
                 WHERE  IsDeleted = 0
                        AND DomainID = @DomainID
                        AND EmployeeID = @EmployeeID
                        AND ID = @ID),
              @PunchStatusID  INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = @Status
                        AND Type = 'Leave'
                        AND IsDeleted = 0),
              @ApprovedStatus VARCHAR(50) = (SELECT Code
                 FROM   tbl_Status
                 WHERE  Code = 'Approved'
                        AND Type = 'Leave'
                        AND IsDeleted = 0),
              @BiometricCode  VARCHAR(50) = (SELECT BiometricCode
                 FROM   tbl_EmployeeMaster
                 WHERE  ID = @EmployeeID)

      BEGIN TRY
          BEGIN TRANSACTION DML

          BEGIN
              IF( (SELECT Count(1)
                   FROM   tbl_EmployeeMissedPunches
                   WHERE  IsDeleted = 0
                          AND DomainID = @DomainID
                          AND ID = @ID
                          AND StatusID IN (SELECT ID
                                           FROM   tbl_Status
                                           WHERE  Code IN( 'Approved', 'Pending' )
                                                  AND [Type] = 'Leave'
                                                  AND IsDeleted = 0)) <> 0 )
                BEGIN
                    UPDATE tbl_EmployeeMissedPunches
                    SET    StatusID = @PunchStatusID,
                           --HRApproverID = @HRApproverID,
                           DomainID = @DomainID,
                           ModifiedBy = @ModifiedBy,
                           HRApprovedOn = Getdate(),
                           HRRemarks = @HRRemarks
                    WHERE  id = @ID

                    IF ( @ApprovedStatus = @Status )
                      BEGIN
                          IF EXISTS(SELECT 1
                                    FROM   tbl_EmployeeMissedPunches
                                    WHERE  IsDeleted = 0
                                           AND EmployeeID = @EmployeeID
                                           AND ID = @ID
                                           AND PunchDate = @PunchDate)
                            BEGIN
                                IF( @PunchTypeID = (SELECT ID
                                                    FROM   tbl_CodeMaster
                                                    WHERE  Code = 'Check In'
                                                           AND Type = 'PunchType') )
                                  BEGIN
                                      --UPDATE tbl_Attendance
                                      --SET    CheckIn = @PunchTime,
                                      --       IsManual = 1
                                      --WHERE  EmployeeID = @EmployeeID
                                      --       AND LogDate = @PunchDate
                                      --UPDATE LG
                                      --SET    LG.DURATION = CASE
                                      --                       WHEN ( Isnull(LG.CheckIn, '00:00') > '00:00'
                                      --                              AND Isnull(LG.CheckOut, '00:00') > '00:00' ) THEN
                                      --                         ( CASE
                                      --                             WHEN ( Isnull(LG.CheckIn, '00:00') = '00:00'
                                      --                                     OR Isnull(LG.CheckOut, '00:00') = '00:00' ) THEN
                                      --                               Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, '00:00:00'), CONVERT(TIME, '00:00:00')), '00:00:00') AS TIME)
                                      --                             WHEN ( Isnull(LG.CheckIn, '00:00') > Isnull(LG.CheckOut, '00:00') ) THEN
                                      --                               Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, LG.CheckIn), CONVERT(TIME, '23:59')), LG.CheckOut) AS TIME)
                                      --                             ELSE
                                      --                               Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, LG.CheckIn), CONVERT(TIME, LG.CheckOut)), '00:00:00') AS TIME)
                                      --                           END )
                                      --                       ELSE
                                      --                         '00:00:00'
                                      --                     END
                                      --FROM   tbl_Attendance LG
                                      --WHERE  BIOMETRICCODE = @BiometricCode
                                      --       AND LOGDATE = @PunchDate
                                      EXEC UpdateBiometricPunches
                                        @EmployeeID,
                                        @PunchDate,
                                        'IN',
                                        'INSERT',
                                        NULL,
                                        @PunchTime,
                                        @ModifiedBy,
                                        @DomainID
                                  END
                                ELSE
                                  BEGIN
                                      --UPDATE tbl_Attendance
                                      --SET    CheckOut = @PunchTime,
                                      --       IsManual = 1
                                      --WHERE  BiometricCode = @BiometricCode
                                      --       AND LogDate = @PunchDate
                                      --UPDATE LG
                                      --SET    LG.DURATION = ( CASE
                                      --                         WHEN ( Isnull(LG.CheckIn, '00:00') = '00:00'
                                      --                                 OR Isnull(LG.CheckOut, '00:00') = '00:00' ) THEN
                                      --                           Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, '00:00:00'), CONVERT(TIME, '00:00:00')), '00:00:00') AS TIME)
                                      --                         WHEN ( Isnull(LG.CheckIn, '00:00') > Isnull(LG.CheckOut, '00:00') ) THEN
                                      --                           Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, LG.CheckIn), CONVERT(TIME, '23:59')), LG.CheckOut) AS TIME)
                                      --                         ELSE
                                      --                           Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, LG.CheckIn), CONVERT(TIME, LG.CheckOut)), '00:00:00') AS TIME)
                                      --                       END )
                                      --FROM   tbl_Attendance LG
                                      --WHERE  BIOMETRICCODE = @BiometricCode
                                      --       AND LOGDATE = @PunchDate
                                      EXEC UpdateBiometricPunches
                                        @EmployeeID,
                                        @PunchDate,
                                        'Out',
                                        'INSERT',
                                        NULL,
                                        @PunchTime,
                                        @ModifiedBy,
                                        @DomainID
                                  END

                                SET @Output='Approved Successfully.'
                            END
                      END

                    IF( @Status = 'Rejected' )
                      SET @Output = 'Rejected Successfully.'
                END
              ELSE
                SET @Output = 'Cannot Approve/Reject'
          END

          COMMIT TRANSACTION DML

          SELECT @Output AS UserMessage
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION DML
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
