﻿
/****************************************************************************                                     
CREATED BY   :                                     
CREATED DATE  :                                     
MODIFIED BY   :                                     
MODIFIED DATE  :                                     
 <summary>                                        
 [Pay_ManageEmployeePayroll] 3,7,1,'2',1                                         
 </summary>                                                             
 *****************************************************************************/                      
CREATE PROCEDURE [dbo].[Pay_ManageEmployeePayroll] (@MonthId   INT,                      
                                                    @Year      INT,                      
                                                    @DomainID  INT,                      
                                                    @Location  VARCHAR(100),                      
                                                    @SessionID INT)                      
AS                      
  BEGIN                      
      SET NOCOUNT ON;                      
                      
      DECLARE @Output VARCHAR(8000)                      
                      
      BEGIN TRY                      
          BEGIN TRANSACTION                      
                      
          DECLARE @Yearid INT =@Year                      
          DECLARE @DataSource TABLE                      
            (                      
               id      INT IDENTITY (1, 1),                      
               [value] NVARCHAR(128)                      
            )                      
                      
          INSERT INTO @DataSource                      
                      ([value])                      
          SELECT item                      
          FROM   dbo.Splitstring(@Location, ',')                      
          WHERE  Isnull(item, '') <> ''                      
                      
          DECLARE @TDSYear INT                      
                      
          IF( @MonthID BETWEEN 1 AND 3 )                      
            SET @TDSYear=( (SELECT FY.NAME                      
                            FROM   tbl_FinancialYear FY                      
                            WHERE  FY.NAME = (SELECT NAME                      
                                              FROM   tbl_FinancialYear                      
                                              WHERE  isdeleted = 0                      
                                                     AND id = @Year                      
                                                     AND DomainID = @DomainID)                      
                                   AND FY.IsDeleted = 0                      
                                   AND FY.DomainID = @DomainID) - 1 )                      
          ELSE                      
            SET @TDSYear = (SELECT FY.NAME                      
                            FROM   tbl_FinancialYear FY                      
                            WHERE  FY.NAME = (SELECT NAME                      
                                              FROM   tbl_FinancialYear                      
                                              WHERE  isdeleted = 0                      
                                                     AND id = @Year                      
                                                     AND DomainID = @DomainID)                      
                                   AND FY.IsDeleted = 0                      
                                   AND FY.DomainID = @DomainID)                      
                      
          SET @Year = (SELECT NAME                      
                       FROM   tbl_FinancialYear                      
                       WHERE  id = @Year                      
                              AND IsDeleted = 0                      
            AND DomainID = @DomainID)                      
                      
          DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'                      
            + CONVERT(VARCHAR(10), @Year)                      
          DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth));                      
                      
          SELECT EffectiveFrom AS EffectiveFrom,                      
                 employeeid,                      
                 E.id,                      
                 companypaystructureid                      
          INTO   #maintable                      
          FROM   tbl_pay_employeepaystructure E                      
   JOIN tbl_EmployeeMaster EMP                      
                   ON E.EmployeeID = EMP.ID                      
          WHERE  effectivefrom <= Dateadd(d, -1, Dateadd(m, Datediff(m, 0, Dateadd(year, @Year - 2000, Dateadd( month, @MonthId - 1, '20000101')))                      
   + 1, 0))                      
                 AND E.effectivefrom = (SELECT Max(EffectiveFrom)                      
                                        FROM   tbl_pay_employeepaystructure                      
                                        WHERE  employeeID = E.Employeeid                      
                                               AND isdeleted = 0 and effectivefrom <= Dateadd(d, -1, Dateadd(m, Datediff(m, 0, Dateadd(year, @Year - 2000, Dateadd( month, @MonthId - 1, '20000101')))+ 1, 0)) )                      
             AND E.isdeleted = 0                      
                 AND E.DomainID = @DomainID                      
                 AND EmployeeID NOT IN (SELECT EmployeeID                      
       FROM   tbl_Pay_EmployeePayroll                      
                                        WHERE  MonthId = @MonthId                      
                                               AND [YearID] = @Year                      
                                               AND IsDeleted = 0                      
                                               AND DomainID = @DomainID)                      
                 AND ( ISNULL(emp.IsActive, '') = 0                      
                        OR ( Month(InactiveFrom) = @MonthId                      
                             AND Year(InactiveFrom) = @Year ) )                      
                 AND ( ( @Location = Cast(0 AS VARCHAR) )                      
                        OR emp.BaseLocationID IN (SELECT [value]                      
                                                  FROM   @DataSource) )                      
                 AND ( emp.BaseLocationID <> 0                      
                        OR emp.BaseLocationID <> '' )                      
                 AND emp.HasAccess = 1                      
                 AND emp.IsDeleted = 0                      
        AND emp.EmploymentTypeID =                  
                   (SELECT                  
                              id                  
                         FROM                  
                              tbl_EmployementType                  
                         WHERE                  
                              Name = 'Direct' AND DomainID = @DomainID)    ;              
          --GROUP  BY employeeid,                                 
          --          E.id,                                 
          --          companypaystructureid  
          DECLARE @ID INT                      
          DECLARE @payroll PAYROLL                      
                      
          WHILE ( (SELECT Count(1)                      
                   FROM   #maintable) > 0 )                      
            BEGIN                      
                SET @ID = (SELECT TOP 1 id                      
                           FROM   #maintable)                      
                      
                DELETE @payroll                      
                INSERT INTO @payroll                      
                SELECT 0,                      
                       e.componentid,                      
                       paystructureid,                      
                      CASE                      
                         WHEN ( p.description = 'Tax' ) THEN                      
                           ISNULL(tds.TDSAmount, 0)                      
                         WHEN ( p.Code = 'LOANDED' ) THEN                      
                           Isnull(Isnull(LA.Amount, 0) + ( CASE                      
                                       WHEN Isnull(LA.InterestAmount, 0) = 0 THEN                      
                                                               0                      
                                                             ELSE                      
                                                               Isnull(LA.InterestAmount, 0)                      
                                                           END ) + Isnull((SELECT ob.DueBalance                      
                                              FROM   tbl_LoanAmortization ob                      
                                                                           WHERE  ob.LoanRequestID = l.ID                      
                                AND ob.DomainID = @DomainID                      
                                                                                  AND Month(ob.DueDate) = Month(Dateadd(month, -1, @CurrentMonth))                      
                                                                                  AND Year(ob.DueDate) = Year(Dateadd(month, -1, @CurrentMonth))), 0), 0)                      
            ELSE                      
                           e.amount                      
                       END AS Amount,                      
                       m.EmployeeID                      
                FROM   tbl_pay_employeepaystructuredetails e                      
                       JOIN #maintable m                      
                         ON m.id = e.paystructureid                      
                            AND m.id = @ID                      
                       JOIN tbl_pay_payrollcompontents p                      
                         ON p.id = e.componentid                      
                       LEFT JOIN tbl_LoanRequest l                      
                              ON l.EmployeeID = m.EmployeeID                      
                                 AND l.StatusID = (SELECT ID                      
                  FROM   tbl_Status                      
                                                   WHERE  Type = 'Loan'                      
                                                          AND Code = 'Settled')                      
                       LEFT JOIN tbl_LoanAmortization la                      
         ON la.LoanRequestID = l.ID                      
                                 AND Month(la.DueDate) = Month(@CurrentMonth)                      
                                 AND Year(la.DueDate) = Year(@CurrentMonth)                      
                                 AND la.StatusID = (SELECT ID                      
                        FROM   tbl_Status                      
                                                    WHERE  Type = 'Amortization'                      
                                                           AND Code = 'Open')                      
                       LEFT JOIN tbl_tds TDS                      
                              ON TDS.employeeid = m.employeeid                      
                                 AND tds.monthid = @MonthId                      
                                 AND tds.yearid = (SELECT id                      
                                                   FROM   tbl_financialyear                      
                                     WHERE  isdeleted = 0                      
                                                          AND NAME = @YEAR                      
                                                          AND domainid = @DomainID)                      
                                 AND TDS.isdeleted = 0                 
                WHERE  e.isdeleted = 0                      
                      
                DECLARE @empid INT =(SELECT employeeid                      
                  FROM   #maintable                      
                  WHERE  ID = @ID)                      
                EXEC Pay_ManageSinglePayroll                      
                  @Payroll,                      
                  @Yearid,                      
         @MonthId,                      
                  @DomainID,                      
                  @empid,                      
                  @SessionId,1                      
                      
                DELETE #maintable                      
                WHERE  id = @ID                      
            END                      
                      
          COMMIT TRANSACTION                      
      END TRY                      
      BEGIN CATCH                      
          ROLLBACK TRANSACTION                      
          DECLARE @ErrorMsg    VARCHAR(100),                      
                  @ErrSeverity TINYINT                      
          SELECT @ErrorMsg = Error_message(),                      
                 @ErrSeverity = Error_severity()              
          RAISERROR(@ErrorMsg,@ErrSeverity,1)                      
      END CATCH                      
  END  