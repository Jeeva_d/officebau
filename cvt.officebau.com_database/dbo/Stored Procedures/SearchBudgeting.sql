﻿/****************************************************************************         
CREATED BY   :         
CREATED DATE  :           
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>        
  SearchBudgeting 4,1     
 </summary>                                 
 *****************************************************************************/    
CREATE PROCEDURE [dbo].SearchBudgeting (@FinancialYearID INT,    
                                       @DomainID        INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(1000)    
    
      BEGIN TRY    
          SELECT B.ID,    
                 Amount,    
                 TagName      AS NAME,    
                 Type,    
                 BudgetId,    
                 CASE    
                   WHEN ( b.Type = 'Product' ) THEN    
                     C.NAME    
                   WHEN ( b.Type = 'Vendor' ) THEN    
                     V.NAME    
                   WHEN ( b.Type = 'Ledger' ) THEN    
                     l.NAME    
                 END          AS BudgetName,    
     BudgetType,  
                 EM.FullName  AS ModifiedBy,    
                 B.ModifiedOn AS ModifiedOn    
          FROM   tblBudget B    
                 LEFT JOIN tblProduct_v2 c    
                        ON b.BudgetId = c.ID    
                           AND b.Type = 'Product'    
                 LEFT JOIN tblVendor v    
                        ON b.BudgetId = v.ID    
                           AND b.Type = 'Vendor'    
                 LEFT JOIN tblLedger l    
                        ON b.BudgetId = l.ID    
                           AND b.Type = 'Ledger'    
                 LEFT JOIN tbl_EmployeeMaster EM    
                        ON Em.ID = B.ModifiedBy    
          WHERE  b.IsDeleted = 0    
                 AND b.DomainId = @DomainID    
                 AND b.FyId = @FinancialYearID    
          ORDER  BY Amount DESC    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
