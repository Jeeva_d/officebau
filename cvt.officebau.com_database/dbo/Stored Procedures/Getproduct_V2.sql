﻿/****************************************************************************                         
CREATED BY   : Jeeva                        
CREATED DATE  :                         
MODIFIED BY   :                  
MODIFIED DATE  :                        
 <summary>      
  Getproduct_V2 12         
 </summary>                                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getproduct_V2] (@ID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @ProductQuery   NVARCHAR(max),
                  @ServiceQuery   NVARCHAR(max),
                  @InventoryQuery   NVARCHAR(max),
                  @ProductCount   INT = 0,
                  @ServiceCount   INT = 0,
                  @InventoryCount INT = 0

          ----- Product  
          SET @ServiceQuery = N'SELECT @ServiceCount = @ServiceCount + COUNT(1)  
        FROM   tblInvoiceItem  
        WHERE  ProductID = '
                              + Cast(@ID AS VARCHAR)
                              + '  
                                  --AND IsProduct = 0  
                                  AND IsDeleted = 0;  
          SELECT @ServiceCount = @ServiceCount + COUNT(1)  
          FROM   tblSOItem  
          WHERE  ProductID = '
                              + Cast(@ID AS VARCHAR)
                              + '   
           --AND IsProduct = 0  
           AND IsDeleted = 0;  
         SELECT @ServiceCount = @ServiceCount + COUNT(1)  
          FROM   tblBudget  
          WHERE  BudgetId = '
                              + Cast(@ID AS VARCHAR)
                              + '   
           AND [Type] = ''Service''  
           AND IsDeleted = 0;'

          EXEC Sp_executesql
            @ServiceQuery,
            N'@ServiceCount INT OUTPUT',
            @ServiceCount OUTPUT;

          ----- Service  
          SET @ProductQuery = N'SELECT @ProductCount = @ProductCount + COUNT(1)  
        FROM   tblExpenseDetails  
        WHERE  LedgerID = '
                              + Cast(@ID AS VARCHAR)
                              + '  
                                  --AND IsLedger = 0  
                                  AND IsDeleted = 0;           
          SELECT @ProductCount = @ProductCount + COUNT(1)  
          FROM   tblPODetails  
          WHERE  LedgerID = '
                              + Cast(@ID AS VARCHAR)
                              + '  
           --AND IsLedger = 0  
           AND IsDeleted = 0;  
         SELECT @ProductCount = @ProductCount + COUNT(1)  
          FROM   tblBudget  
          WHERE  BudgetId = '
                              + Cast(@ID AS VARCHAR)
                              + '   
           AND [Type] = ''Product''  
           AND IsDeleted = 0;           '

          EXEC Sp_executesql
            @ProductQuery,
            N'@ProductCount INT OUTPUT',
            @ProductCount OUTPUT;

          ----- Inventory
          SET @InventoryQuery = N'SELECT @InventoryCount = @InventoryCount + COUNT(1)  
        FROM   tbl_Inventory  
        WHERE  ProductID = '
                                + Cast(@ID AS VARCHAR)
                                + 'AND IsDeleted = 0; 
                                          
          SELECT @InventoryCount = @InventoryCount + COUNT(1)  
          FROM   tbl_InventoryStockAdjustmentItems  
          WHERE  ProductID = '
                                + Cast(@ID AS VARCHAR)
                                + 'AND IsDeleted = 0;  
                                
         SELECT @InventoryCount = @InventoryCount + COUNT(1)  
          FROM   tbl_InventoryStockDistribution  
          WHERE  ProductID = '
                                + Cast(@ID AS VARCHAR)
                                + 'AND IsDeleted = 0;'

          EXEC Sp_executesql
            @InventoryQuery,
            N'@InventoryCount INT OUTPUT',
            @InventoryCount OUTPUT;

          --SET @InventoryCount = (SELECT COUNT(1) from tbl_Inventory where IsDeleted = 0 AND ProductID = @ID)  
          SELECT pd.ID           AS ID,
                 pd.NAME         AS NAME,
                 pd.IsInventroy  AS IsInventroy,
                 pd.IsPurchase   AS IsPurchase,
                 pd.IsSales      AS IsSales,
                 TypeID          AS TypeID,
                 MFG             AS Manufacturer,
                 pd.ModifiedOn   AS ModifiedOn,
                 EMP.FullName    AS ModifiedBy,
                 OpeningStock    AS OpeningStock,
                 HSNId           AS HSNID,
                 GH.Code         AS HsnCode,
                 GH.SGST,
                 GH.CGST,
                 GH.IGST,
                 pd.SalesLedgerId,
                 pd.PurchaseLedgerId,
                 pd.SalesDescription,
                 pd.PurchaseDescription,
                 pd.PurchasePrice,
                 pd.SalesPrice,
                 pd.UOMID,
                 U.NAME          AS UOM,
                 pd.LowStockThresold,
                 @ProductCount   AS PurchaseCount,
                 @ServiceCount   AS SalesCount,
                 @InventoryCount AS InventoryCount
          FROM   tblProduct_V2 pd
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = pd.ModifiedBy
                 LEFT JOIN tblGstHSN GH
                        ON pd.HSNId = GH.ID
                 LEFT JOIN tbl_UOM U
                        ON U.ID = pd.UOMID
          WHERE  ( pd.ID = @ID
                    OR @ID IS NULL )
                 AND pd.IsDeleted = 0

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
