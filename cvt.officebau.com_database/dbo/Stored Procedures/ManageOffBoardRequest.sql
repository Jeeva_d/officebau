﻿/****************************************************************************           
CREATED BY  :   DHANALAKSHMI.S        
CREATED DATE :   04-AUG-2017        
MODIFIED BY  :           
MODIFIED DATE :           
 <summary>        
 [Manageoffboardrequest] 0, 268,'04 Aug 2017','','','Tes','',21, 1,1,1,0        
 </summary>                                   
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageOffBoardRequest] (@ID                     INT,  
                                               @EmployeeID             INT,  
                                               @RelievingDate          DATE,  
                                               @TentativeRelievingDate DATE,  
                                               @InitiatedDate          DATE,  
                                               @Reason                 VARCHAR(500),  
                                               @Remarks                VARCHAR(8000),  
                                               @StatusID               INT,  
                                               @ApproverID             INT,  
                                               @SessionID              INT,  
                                               @DomainID               INT,  
                                               @IsDeleted              BIT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output            VARCHAR(50) = 'Operation Failed!',  
              @RelievingStatusID INT = (SELECT ID  
                 FROM   tbl_Status  
                 WHERE  Code = 'Saved'  
                        AND [Type] = 'OffBoard'  
                        AND IsDeleted = 0),  
              @InitiateStatusID  INT = (SELECT ID  
                 FROM   tbl_Status  
                 WHERE  Code = 'Initiated'  
                        AND [Type] = 'OffBoard'  
                        AND IsDeleted = 0),  
              @OffBoardProcessID VARCHAR(20) = ''  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          BEGIN  
              IF( @ID = 0 )  
                BEGIN  
                    IF NOT EXISTS(SELECT 1  
                                  FROM   tbl_OffBoardProcess  
                                  WHERE  EmployeeID = @EmployeeID 
                                         AND DomainID = @DomainID)  
                      BEGIN  
                          INSERT INTO tbl_OffBoardProcess  
                                      (EmployeeID,  
                                       RelievingDate,  
                                       TentativeRelievingDate,  
                                       InitiatedDate,  
                                       Reason,  
                                       Remarks,  
                                       StatusID,  
                                       ApproverID,  
                                       DomainID,  
                                       CreatedBy,  
                                       CreatedOn,  
                                       ModifiedBy,  
                                       ModifiedOn)  
                          VALUES      (@EmployeeID,  
                                       @RelievingDate,  
                                       @TentativeRelievingDate,  
                                       @InitiatedDate,  
                                       @Reason,  
                                       @Remarks,  
                                       @RelievingStatusID,  
                                       @ApproverID,  
                                       @DomainID,  
                                       @SessionID,  
                              Getdate(),  
                                       @SessionID,  
                                       Getdate())  
  
                          SET @OffBoardProcessID = Ident_current('tbl_OffBoardProcess')  
                          SET @Output = 'Inserted Successfully./'  
                                        + Cast(@OffBoardProcessID AS VARCHAR(20))  
                                        + '/'  
                                        + (SELECT code  
                                           FROM   tbl_status  
                                           WHERE  [type] = 'OffBoard'  
                                                  AND code = 'Saved')  
                      END  
                    ELSE  
                      BEGIN  
                          SET @Output = 'Already Exists.'  
                      END  
                END  
              ELSE IF NOT EXISTS(SELECT 1  
                            FROM   tbl_OffBoardProcess  
                            WHERE  EmployeeID = @EmployeeID  
                                   AND IsDeleted = 0  
                                   AND DomainID = @DomainID  
                                   AND ( StatusID IN (SELECT ID  
                                                      FROM   tbl_Status  
                                                      WHERE  Code IN( 'Approved' )  
                                                             AND [Type] = 'OffBoard') ))  
                BEGIN  
                    IF ( (SELECT ID  
                          FROM   tbl_OffBoardProcess  
                          WHERE  ID = @ID) = (SELECT ID  
                                              FROM   tbl_Status  
                                              WHERE  Code IN( 'Rejected' )  
                                                     AND [Type] = 'OffBoard'  
                                                     AND IsDeleted = 0) )  
                      BEGIN  
                          SET @Output = 'Reinitiated Successfully./'  
                                        + Cast (@ID AS VARCHAR(20))  
                      END  
                    ELSE  
                      BEGIN  
                          SET @Output = 'Initiated Successfully./'  
                                        + Cast (@ID AS VARCHAR(20))  
                      END  
  
                    UPDATE tbl_OffBoardProcess  
                    SET    EmployeeID = @EmployeeID,  
                           RelievingDate = @RelievingDate,  
                           TentativeRelievingDate = Dateadd(DAY, +60, Getdate()),  
                           InitiatedDate = Getdate(),  
                           Reason = @Reason,  
                           Remarks = @Remarks,  
                           StatusID = @InitiateStatusID,  
                           ApproverID = @ApproverID,  
                           DomainID = @DomainID,  
                           ModifiedBy = @SessionID,  
                           ModifiedOn = Getdate(),  
                           ApproverRemarks = NULL,  
                           ApprovedOn = NULL  
                    WHERE  ID = @ID  
                END  
          END  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
