﻿/****************************************************************************               
CREATED BY   : 
CREATED DATE  :               
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>            
[Rpt_AgingAP] 0,1,1        
[Rpt_AgingAP] 0,1               
 </summary>                                       
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[Rpt_AgingAP] (@FinancialYearID INT,        
                                     @DomainID        INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
        
          DECLARE @FinancialYear INT;        
          DECLARE @StartFinancialMonth INT;        
          DECLARE @EndFinancialMonth INT;        
          DECLARE @Result TABLE        
            (        
               [Supplier] VARCHAR(200),        
               Total      MONEY        
            )        
        
          SET @StartFinancialMonth = (SELECT Value        
                                      FROM   tblApplicationConfiguration        
                                      WHERE  Code = 'STARTMTH'        
                                             AND DomainID = @DomainID)        
          SET @EndFinancialMonth = ( ( (SELECT Value        
                                        FROM   tblApplicationConfiguration        
                                        WHERE  Code = 'STARTMTH'        
                                               AND DomainID = @DomainID)        
                                       + 11 ) % 12 )        
        
          IF( @FinancialYearID = 0 )        
            BEGIN        
                IF( Month(Getdate()) <= ( ( (SELECT Value        
                                             FROM   tblApplicationConfiguration        
                                             WHERE  Code = 'STARTMTH'        
                                                    AND DomainID = @DomainID)        
                                            + 11 ) % 12 ) )        
                  SET @FinancialYear = Year(Getdate()) - 1        
                ELSE        
                  SET @FinancialYear = Year(Getdate())        
        
                --IF( Month(Getdate()) <= (SELECT DISTINCT EndMonth        
                --                         FROM   tblFinancialYear        
                --                         WHERE  DomainID = @DomainID) )        
                --  SET @FinancialYear = Year(Getdate()) - 1        
                --ELSE        
                --  SET @FinancialYear = Year(Getdate())        
        
                --SET @StartFinancialMonth =(SELECT DISTINCT StartMonth        
                --                           FROM   tblFinancialYear        
                --                           WHERE  DomainID = @DomainID)        
                --SET @EndFinancialMonth = (SELECT DISTINCT EndMonth        
                --                          FROM   tblFinancialYear        
                --                          WHERE  DomainID = @DomainID)        
            END        
          ELSE        
            BEGIN        
                SET @FinancialYear = Substring((SELECT FinancialYear        
                                                FROM   tbl_FinancialYear        
                                                WHERE  ID = @FinancialYearID        
                                                       AND DomainID = @DomainID), 0, 5)        
        
                --SET @FinancialYear = Substring((SELECT FinancialYear        
                --                                FROM   tblFinancialYear        
                --                                WHERE  ID = @FinancialYearID        
                --                                       AND DomainID = @DomainID), 0, 5)        
                --SET @StartFinancialMonth = (SELECT StartMonth        
                --                            FROM   tblFinancialYear        
                --                            WHERE  ID = @FinancialYearID        
                --                                   AND DomainID = @DomainID)        
                --SET @EndFinancialMonth = (SELECT EndMonth        
                --                 FROM   tblFinancialYear        
                --                          WHERE  ID = @FinancialYearID        
                --               AND DomainID = @DomainID)        
            END        
        
          DECLARE @StartDate DATE = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date          
          DECLARE @EndDate DATE = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date         
          DECLARE @All_Months TABLE        
            (        
               MonthID INT,        
               YearID  INT        
            );        
        
          WITH CTE        
               AS (SELECT Vd.NAME                                                            AS Supplier,        
                          [Date],        
                          [DueDate],        
                          Type,        
                          CASE        
                            WHEN Datediff(DD, DueDate, Getdate()) < 0 THEN 'Not yet due'        
                            WHEN Datediff(DD, DueDate, Getdate()) >= 0        
                                 AND Datediff(DD, DueDate, Getdate()) < 30 THEN '0 to 30 days'        
                            WHEN Datediff(DD, DueDate, Getdate()) >= 30        
                                 AND Datediff(DD, DueDate, Getdate()) < 60 THEN '30 to 60 days'        
                            WHEN Datediff(DD, DueDate, Getdate()) >= 60        
                                 AND Datediff(DD, DueDate, Getdate()) < 90 THEN '60 to 90 days'        
                            WHEN Datediff(DD, DueDate, Getdate()) > 90 THEN '90 days and above'        
                          END                                                                AS DATECOUNT,        
                          ( (SELECT ( Isnull(Sum(Amount*Qty), 0) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0))        
                             FROM   tblExpenseDetails        
                             WHERE  ExpenseID = ex.ID        
                                    AND IsDeleted = 0        
                                    AND DomainID = @DomainID)        
                           ) - Isnull(ex.PaidAmount, 0) AS Amount        
                   FROM   tblExpense ex        
                          JOIN tblVendor Vd        
                            ON ex.VendorID = Vd.ID        
                   WHERE  ex.StatusID IN(SELECT ID        
                                         FROM   tbl_CodeMaster        
                                         WHERE  Type IN( 'Open', 'Partial' ) and IsDeleted = 0)        
                          AND Type = 'BILL'        
                          AND ex.DomainID = @DomainID        
        AND ex.IsDeleted = 0        
                          AND [Date] >= @StartDate        
                          AND [Date] <= @EndDate)        
          SELECT Supplier,        
                 [Not yet due],        
                 [0 to 30 days],        
                 [30 to 60 days],        
                 [60 to 90 days],        
                 [90 days and above],        
                 Isnull([Not yet due], 0)        
                 + Isnull([0 to 30 days], 0)        
                 + Isnull([30 to 60 days], 0)        
                 + Isnull([60 to 90 days], 0)        
         + Isnull([90 days and above], 0) [Total]        
          INTO   #billing        
          FROM   (SELECT Supplier,        
                         Amount,        
                         DATECOUNT        
                  FROM   cte)src                       PIVOT ( Sum(Amount)        
                       FOR DateCount IN ([Not yet due],        
                                         [0 to 30 days],        
                                         [30 to 60 days],        
                                         [60 to 90 days],        
                                        [90 days and above]) ) pvt;        
        
      ;        
          WITH Bill        
               AS (SELECT Supplier,        
                          [Total],        
                          [Not yet due],        
                          [0 to 30 days],        
                          [30 to 60 days],        
                          [60 to 90 days],        
                [90 days and above]        
                   FROM   #billing BU        
                   UNION ALL        
                   SELECT 'TOTAL',        
                          Sum([Total]) AS TOTAL,        
                          Sum([Not yet due]),        
                          Sum([0 to 30 days]),        
                          Sum([30 to 60 days]),        
                          Sum([60 to 90 days]),        
                          Sum([90 days and above])        
                   FROM   #billing)        
          SELECT Supplier,        
                 Total,        
                 [Not yet due],        
                 [0 to 30 days],        
                 [30 to 60 days],        
                 [60 to 90 days],        
                 [90 days and above]        
          FROM   Bill        
        
      END TRY        
      BEGIN CATCH        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
