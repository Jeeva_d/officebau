﻿/****************************************************************************     
CREATED BY   : Priya K    
CREATED DATE  : 6-Mar-2018    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
      
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManagePayrollComponentConfiguration] (@ID                 INT,  
                                                             @BusinessUnitID     INT,  
                                                             @PayrollConfigCode  VARCHAR(10),  
                                                             @ComponentID        INT,  
                                                             @PayrollConfigValue BIT,  
                                                             @SessionID          INT,  
                                                             @DomainID           INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(8000)  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SET @Output = 'Operation failed!'  
  
          IF ( Isnull(@ID, 0) = 0 )  
            BEGIN  
                IF EXISTS(SELECT 1  
                          FROM   tbl_PayrollComponentConfiguration  
                          WHERE  @ID <> ID  
                                 AND PayrollComponentID = @ComponentID  
                                 AND BusinessUnitID = @BusinessUnitID)  
                  BEGIN  
                      SET @Output = 'Already Exists.'  
  
                      GOTO FINISH  
                  END  
                ELSE  
                  BEGIN  
                      INSERT INTO tbl_PayrollComponentConfiguration  
                                  (PayrollComponentID,  
                                   BusinessUnitID,  
                                   ConfigValue,  
                                   CreatedBy,  
                                   CreatedOn,  
                                   ModifiedBy,  
                                   ModifiedOn)  
                      VALUES      ( @ComponentID,  
                                    @BusinessUnitID,  
                                    @PayrollConfigValue,  
                                    @SessionID,  
                                    Getdate(),  
                                    @SessionID,  
                                    Getdate())  
  
                      SET @Output = 'Inserted Successfully.'  
  
                      GOTO FINISH  
                  END  
            END  
          ELSE  
            BEGIN  
                UPDATE tbl_PayrollComponentConfiguration  
                SET    ConfigValue = @PayrollConfigValue,  
                       ModifiedBy = @SessionID,  
                       ModifiedOn = Getdate()  
                WHERE  ID = @ID  
                       AND PayrollComponentID = @ComponentID  
                       AND BusinessUnitID = @BusinessUnitID  
  
                SET @Output = 'Updated Successfully.'  
  
                GOTO FINISH  
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
