﻿/****************************************************************************                         
CREATED BY   : Ajith                       
CREATED DATE  : 21-MAY-2019                        
MODIFIED BY   :                       
MODIFIED DATE  :                      
 <summary>                         
                      
 </summary>                                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageFandFSettlement] (@ID                INT,
                                               @EmployeeID        INT,
                                               @LastWorkingDate   DATETIME,
                                               @YearsOfService    VARCHAR(100),
                                               @TotalDaysInMonth  INT,
                                               @DaysPaid          INT,
                                               @ReasonForLeaving  VARCHAR(500),
                                               @NoticeDays        INT,
                                               @LeaveEncashedDays INT,
                                               @LOPDays           INT,
                                               @NetAmountPayable  MONEY,
                                               @Remarks           VARCHAR(500),
                                               @HRApproverID      INT,
                                               @IsDeleted         BIT,
                                               @SessionID         INT,
                                               @DomainID          INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output     VARCHAR(100) = 'Operation Failed!/0',
              @HRStatusID INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = 'Pending'
                        AND [Type] = 'EOS'
                        AND IsDeleted = 0),
              @StatusID   INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = 'Submitted'
                        AND [Type] = 'EOS'
                        AND IsDeleted = 0)

      SET @TotalDaysInMonth = Day(@LastWorkingDate)

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @IsDeleted = 1 )
            BEGIN
                UPDATE tbl_FAndF
                SET    IsDeleted = 1,
                       ModifiedBY = @SessionID
                WHERE  ID = @ID
                       AND DomainID = @DomainID

                SET @Output = 'Deleted Successfully./'
                              + Cast(@ID AS VARCHAR)

                GOTO Finish
            END
          ELSE
            BEGIN
                IF( @ID = 0 )
                  BEGIN
                      IF ( (SELECT Count(1)
                            FROM   tbl_FAndF
                            WHERE  EmployeeID = @EmployeeID
                                   AND IsDeleted = 0
                                   AND DomainID = @DomainID
                                   AND StatusID IN (SELECT ID
                                                    FROM   tbl_Status
                                                    WHERE  Code IN ( 'Approved', 'Submitted' )
                                                           AND [Type] = 'EOS'
                                                           AND IsDeleted = 0)) = 0 )
                        BEGIN
                            INSERT INTO tbl_FandF
                                        (EmployeeID,
                                         LastWorkingDate,
                                         YearsOfService,
                                         TotalDaysInMonth,
                                         DaysPaid,
                                         ReasonForLeaving,
                                         NoticeDays,
                                         LeaveEncashedDays,
                                         LOPDays,
                                         NetAmountPayable,
                                         Remarks,
                                         StatusID,
                                         HRApproverId,
                                         HRApprovedDate,
                                         HRStatusID,
                                         HRRemarks,
                                         IsDeleted,
                                         CreatedOn,
                                         CreatedBy,
                                         ModifiedOn,
                                         ModifiedBy,
                                         HistoryID,
                                         DomainID)
                            VALUES      (@EmployeeID,
                                         @LastWorkingDate,
                                         @YearsOfService,
                                         @TotalDaysInMonth,
                                         @DaysPaid,
                                         @ReasonForLeaving,
                                         @NoticeDays,
                                         @LeaveEncashedDays,
                                         @LOPDays,
                                         @NetAmountPayable,
                                         @Remarks,
                                         @StatusID,
                                         @HRApproverID,
                                         NULL,
                                         @HRStatusID,
                                         NULL,
                                         0,
                                         Getdate(),
                                         @SessionID,
                                         Getdate(),
                                         @SessionID,
                                         Newid(),
                                         @DomainID)

                            SET @ID = @@IDENTITY
                            SET @Output = 'Inserted Successfully./'
                                          + Cast(@ID AS VARCHAR)

                            GOTO Finish
                        END
                      ELSE
                        BEGIN
                            SET @Output = 'Already Exists./0'

                            GOTO Finish
                        END
                  END
                ELSE
                  BEGIN
                      UPDATE tbl_FandF
                      SET    EmployeeID = @EmployeeID,
                             LastWorkingDate = @LastWorkingDate,
                             YearsOfService = @YearsOfService,
                             TotalDaysInMonth = @TotalDaysInMonth,
                             DaysPaid = @DaysPaid,
                             ReasonForLeaving = @ReasonForLeaving,
                             NoticeDays = @NoticeDays,
                             LeaveEncashedDays = @LeaveEncashedDays,
                             LOPDays = @LOPDays,
                             NetAmountPayable = @NetAmountPayable,
                             Remarks = @Remarks,
                             StatusID = @StatusID,
                             HRApproverID = @HRApproverID,
                             HRStatusID = @HRStatusID,
                             ModifiedBY = @SessionID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID
                             AND DomainID = @DomainID

                      SET @Output = 'Updated Successfully./'
                                    + Cast(@ID AS VARCHAR)
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
