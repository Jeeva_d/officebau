﻿/****************************************************************************   
CREATED BY   :  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [GetFNMonth-AppConfig] 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetFNMonth-AppConfig] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;
      BEGIN TRY
          BEGIN TRANSACTION         

          DECLARE @Date     INT,
                  @Endmonth DATETIME,
                  @Value    VARCHAR(10)

          SET @Value=(SELECT Cast(Isnull(Value, 0) AS INT)
                      FROM   tblApplicationConfiguration
                      WHERE  DomainID = @DomainID
                             AND IsDeleted = 0
                             AND Code = 'STARTMTH')

          IF( @Value > 0 )
            BEGIN
                SET @Date = @Value

                SELECT ( CONVERT(VARCHAR(3), Datename(Month, Dateadd(month, @Date - 1, 0))) )      AS StartMonth,
                       ( CONVERT(VARCHAR(3), Datename(Month, Dateadd(month, @Date - 1 + 11, 0))) ) AS EndMonth,
                       (SELECT value
                        FROM   tblApplicationConfiguration
                        WHERE  DomainID = @DomainID
                               AND IsDeleted = 0
                               AND Code = 'STARTMTH')                                              AS Value

            END
          ELSE
            BEGIN
                SET @Date = NULL

                SELECT ( CONVERT(VARCHAR(3), Datename(Month, Dateadd(month, @Date - 1, 0))) )      AS StartMonth,
                       ( CONVERT(VARCHAR(3), Datename(Month, Dateadd(month, @Date - 1 + 11, 0))) ) AS EndMonth,
                       (SELECT value
                        FROM   tblApplicationConfiguration
                        WHERE  DomainID = @DomainID
                               AND IsDeleted = 0
                               AND Code = 'STARTMTH')                                              AS Value
            END

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 

