﻿/****************************************************************************                 
CREATED BY    :               
CREATED DATE  :               
MODIFIED BY   :            
MODIFIED DATE :          
 <summary>               
     [DeleteUnUsedFilesFromFileUploadTable]      
 </summary>                                         
 *****************************************************************************/

CREATE PROCEDURE [dbo].[DeleteUnUsedFilesFromFileUploadTable]
AS
     BEGIN
         BEGIN TRY
             BEGIN TRANSACTION;
             DECLARE @i INT, @Fileid VARCHAR(MAX);
             SELECT id
             INTO #LoopTable
             FROM tbl_FileUpload;
             WHILE(
             (
                 SELECT COUNT(1)
                 FROM #LoopTable
             ) <> 0)
                 BEGIN
                     SET @Fileid =
                     (
                         SELECT TOP 1 id
                         FROM #LoopTable
                     );
                     SET @i = 0;
                     DECLARE @OutPut INT;
                     EXEC [CheckReferenceForGuid] 
                          @Fileid, 
                          @OutPut OUTPUT;
                     IF(@OutPut = 0)
                         BEGIN
                             DELETE tbl_FileUpload
                             WHERE id = @Fileid;
                         END;
                     DELETE #LoopTable
                     WHERE id = @Fileid;
                 END;
             DROP TABLE #LoopTable;
             COMMIT TRANSACTION;
         END TRY
         BEGIN CATCH
             ROLLBACK TRANSACTION;
             DECLARE @ErrorMsg VARCHAR(100), @ErrSeverity TINYINT;
             SELECT @ErrorMsg = ERROR_MESSAGE(), 
                    @ErrSeverity = ERROR_SEVERITY();
             RAISERROR(@ErrorMsg, @ErrSeverity, 1);
         END CATCH;
     END;
