﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
   [Getcompanymasterdetail] 1
 </summary>                            
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetCompanyMasterDetail](@ID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT COM.ID                                  AS ID,
                 NAME                                    AS NAME,
                 COM.FullName                            AS FullName,
                 COM.EmailID                             AS EmailID,
                 Address                                 AS Address1,
                 COM.ContactNo                           AS ContactNo,
                 PanNo                                   AS PANNo,
                 TANNo                                   AS TANNo,
                 GSTNo                                   AS GSTNo,
                 EMP.FullName                            AS ModifiedBy,
                 COM.ModifiedOn                          AS ModifiedOn,
                 Website                                 AS Website,
                 Isnull(FU.OriginalFileName, NULL)       AS Logo,
                 Cast(Isnull(logo, NULL) AS VARCHAR(50)) AS FileUploadID,
                 PAYMENTFAVOUR                           AS PAYMENTFAVOUR,
                 CommunicationEmailId                    AS CommunicationEmailId,
                 COM.EmpCodePattern                          AS EmpCodePattern
          FROM   tbl_Company COM
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = COM.ModifiedBy
                 LEFT JOIN tbl_fileupload FU
                        ON FU.id = COM.logo
          WHERE  COM.IsDeleted = 0
                 AND COM.ID = @ID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
