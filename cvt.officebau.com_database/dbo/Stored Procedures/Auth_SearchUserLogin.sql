﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	29-12-2015
MODIFIED BY			:	Jeeva
MODIFIED DATE		:	06-01-2016
 <summary>        
	Search user for login 
		SELECT * FROM Auth_WebLogin
		select * from Auth_Users
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_SearchUserLogin]
 (
	@Username		varchar(50)		,
	@Password		varchar(100)		
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY				
		SELECT
			WebLgn.ID			AS UserID	,
			WebLgn.FullName		AS Name		,
			Usr.SysAdmin		AS SysAdmin
		FROM 
			Auth_WebLogin	WebLgn
		JOIN Auth_Users Usr ON Usr.WebLoginID = WebLgn.ID
		WHERE Usr.Username = @Username AND Usr.Password = @Password AND Usr.IsDeleted = 0 AND Usr.IsActive  = 1
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
