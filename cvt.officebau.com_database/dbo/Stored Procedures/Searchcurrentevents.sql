﻿/****************************************************************************   
CREATED BY      :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>    
 [SearchCurrentEvents] 5  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchcurrentevents](@EmployeeID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN  
              DECLARE @DomainID INT =(SELECT DomainID  
                FROM   tbl_EmployeeMaster  
                WHERE  id = @EmployeeID)  
  
              SELECT E.ID,  
                     FullName + '-' + E.Code           AS NAME,  
                     'Birthday'                         AS EType,  
                     (SELECT Count(*)  
                      FROM   tbl_Notifications  
                      WHERE  fromid = @EmployeeID  
                             AND toid = e.id  
                             AND WISHTYPE = 'Birthday') AS [Type]  
              FROM   tbl_EmployeePersonalDetails  
                     JOIN tbl_EmployeeMaster E  
                       ON EmployeeID = E.ID  
              WHERE  Datepart(mm, dateofbirth) = Datepart(mm, Getutcdate())  
                     AND Datepart(dd, dateofbirth) = Datepart(dd, Getutcdate())  
                     AND E.ID <> @EmployeeID  
                     AND e.IsActive = 0  
                     AND e.IsDeleted = 0  
                     AND e.Code NOT LIKE'%TMP_%'  
                     AND e.DomainID = @DomainID  
              UNION ALL  
              SELECT E.ID,  
                     FullName + '-' + code,  
                     'Anniversary',  
                     (SELECT Count(*)  
                      FROM   tbl_Notifications  
                      WHERE  fromid = @EmployeeID  
                             AND toid = e.id  
                             AND WISHTYPE = 'Anniversar')AS [Type]  
              FROM   tbl_EmployeePersonalDetails  
                     JOIN tbl_EmployeeMaster E  
                       ON EmployeeID = E.ID  
              WHERE  Datepart(mm, Anniversary) = Datepart(mm, Getutcdate())  
                     AND Datepart(dd, Anniversary) = Datepart(dd, Getutcdate())  
                     AND E.ID <> @EmployeeID  
                     AND e.Code NOT LIKE'%TMP_%'  
                     AND e.IsActive = 0  
                     AND e.IsDeleted = 0  
                     AND e.DomainID = @DomainID  
              UNION ALL  
              SELECT E.ID,  
                     FullName + '-' + code,  
                     'NewJoin',  
                     (SELECT Count(*)  
                      FROM   tbl_Notifications  
                      WHERE  fromid = @EmployeeID  
                             AND toid = e.id  
                             AND WISHTYPE = 'others') AS [Type]  
              FROM   tbl_EmployeeMaster E  
              WHERE  CONVERT(DATE, DOJ) = CONVERT(DATE, Getdate())  
                     AND E.ID <> @EmployeeID  
                     AND e.Code NOT LIKE'%TMP_%'  
                     AND e.DomainID = @DomainID  And FirstName <>'Admin'
              ORDER  BY EType,  
                        NAME  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
