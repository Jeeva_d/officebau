﻿/****************************************************************************               
CREATED BY   :               
CREATED DATE  :               
MODIFIED BY   :               
MODIFIED DATE  :               
<summary>                      
	[Rpt_CompleteSales]   '10-10-2016','10-10-2019', 1
</summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_CompleteSales] (@FromDate DATE,
                                            @ToDate   DATE,
                                            @domainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT cus.NAME                          AS Customer,
                 CONVERT(VARCHAR, inv.[Date], 106) AS [Date],
                 invItm.Rate                       AS Amount,
                 invItm.ItemDescription            AS [Description],
                 cm.Code                           AS [Pay Status]
          FROM   tblInvoiceItem invItm
                 JOIN tblInvoice inv
                   ON inv.ID = invItm.InvoiceID
                 LEFT JOIN tblCustomer cus
                        ON cus.ID = inv.CustomerID
                 LEFT JOIN tbl_CodeMaster cm
                        ON cm.ID = inv.StatusID
          WHERE  inv.IsDeleted = 0
                 AND inv.DomainID = @domainID
                 AND inv.[Date] BETWEEN @FromDate AND @ToDate
          ORDER  BY cus.NAME ASC,
                    inv.[Date] ASC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY();
          RAISERROR(@ErrorMsg,@ErrSeverity,1);
      END CATCH;
  END;
