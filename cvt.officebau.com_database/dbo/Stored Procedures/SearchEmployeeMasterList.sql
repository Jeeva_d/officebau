﻿/**************************************************************************** 
CREATED BY			:	Ajith N
CREATED DATE		:	08 Aug 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
   [SearchEmployeeMasterList]  '' , '', '', '', 'false', 1
 </summary>                          
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchEmployeeMasterList] (@EmployeeCode     VARCHAR(20),
                                                   @Name             VARCHAR(100),
                                                   @DepartmentID     INT,
                                                   @DesignationID    INT,
                                                   @FunctionalID     INT,
                                                   @BusinessUnit     VARCHAR(20),
                                                   @ContactNo        VARCHAR(20),
                                                   @EmailID          VARCHAR(50),
                                                   @EmploymentTypeID INT,
                                                   @ReportingTo      VARCHAR(100),
                                                   @PAN              VARCHAR(50),
                                                   @PFNo             VARCHAR(50),
                                                   @EEESI            VARCHAR(50),
                                                   @AadharID         VARCHAR(50),
                                                   @UAN              VARCHAR(50),
                                                   @PolicyNo         VARCHAR(50),
                                                   @IsActive         BIT,
                                                   @EmployeeID       INT,
                                                   @DomainID         INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @BusinessUnitIDs VARCHAR(50)

          IF( ISNULL(@BusinessUnit, 0) = 0 )
            SET @BusinessUnitIDs = (SELECT BusinessUnitID
                                    FROM   tbl_EmployeeMaster
                                    WHERE  ID = @EmployeeID
                                           AND DomainID = @DomainID)
          ELSE
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','

          DECLARE @BusinessUnitTable TABLE
            (
               BusinessUnit VARCHAR(100)
            )

          INSERT INTO @BusinessUnitTable
          SELECT @BusinessUnitIDs

          SELECT EM.ID                                        AS ID,
                 ISNULL(EM.EmpCodePattern, '') + '' +EM.Code                                      AS EmployeeCode,
                 EM.FullName AS FirstName,
                 DEP.NAME                                     AS DepartmentName,
                 DESI.NAME                                    AS DesignationName,
                 ISNULL(EM.ContactNo, '')                     AS ContactNo,
                 ISNULL(EM.IsActive, 'False')                 AS IsApproved,
                 EM.BaseLocationID
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_Department DEP
                        ON DEP.ID = EM.DepartmentID
                 LEFT JOIN tbl_Designation DESI
                        ON DESI.ID = EM.DesignationID
                 LEFT JOIN tbl_Functional FUN
                        ON FUN.ID = EM.FunctionalID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
                 LEFT JOIN tbl_EmployementType EMPLOYEETYPE
                        ON EMPLOYEETYPE.ID = EM.EmploymentTypeID
                 LEFT JOIN tbl_EmployeeMaster REPORTING
                        ON REPORTING.ID = EM.ReportingToID
                 LEFT JOIN tbl_EmployeeStatutoryDetails STATUTORY
                        ON STATUTORY.EmployeeID = EM.ID
                 LEFT JOIN tbl_EmployeeMaster REPORTINGTO
                        ON REPORTINGTO.ID = EM.ReportingToID
                 JOIN @BusinessUnitTable TBU
                   ON TBU.BusinessUnit LIKE '%,'
                                            + CAST(EM.BaseLocationID AS VARCHAR(10))
                                            + ',%'
          WHERE  EM.IsDeleted = 0
                 AND ( ISNULL(@EmployeeCode, '') = ''
                        OR ( EM.Code LIKE '%' + @EmployeeCode + '%' ) )
                 AND ( ISNULL(@Name, '') = ''
                        OR ( ( EM.FirstName + ' ' + ISNULL(EM.LastName, '') ) LIKE '%' + @Name + '%' ) )
                 AND ( ISNULL(@ContactNo, '') = ''
                        OR ( EM.ContactNo LIKE '%' + @ContactNo + '%' ) )
                 AND ( @BusinessUnit = 0
                        OR EM.BaseLocationID = @BusinessUnit )
                 AND ( ISNULL(EM.IsActive, '0') = @IsActive )
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND EM.domainID = @DomainID
                 AND ( ISNULL(@DepartmentID, '') = ''
                        OR DEP.ID = @DepartmentID )
                 AND ( ISNULL(@DesignationID, '') = ''
                        OR DESI.ID = @DesignationID )
                 AND ( ISNULL(@FunctionalID, '') = ''
                        OR FUN.ID = @FunctionalID )
                 AND ( ISNULL(@EmailID, '') = ''
                        OR ( EM.EmailID LIKE '%' + @EmailID + '%' ) )
                 AND ( ISNULL(@EmploymentTypeID, '') = ''
                        OR EM.EmploymentTypeID = @EmploymentTypeID )
                 AND ( ISNULL(@ReportingTo, '') = ''
                        OR ( ( ISNULL(REPORTINGTO.FirstName, '') + ' '
                               + ISNULL(REPORTINGTO.LastName, '') ) LIKE '%' + @ReportingTo + '%' ) )
                 AND ( ISNULL(@PAN, '') = ''
                        OR ( STATUTORY.PAN_No LIKE '%' + @PAN + '%' ) )
                 AND ( ISNULL(@PFNo, '') = ''
                        OR ( STATUTORY.PFNo LIKE '%' + @PFNo + '%' ) )
                 AND ( ISNULL(@EEESI, '') = ''
                        OR ( STATUTORY.ESINo LIKE '%' + @EEESI + '%' ) )
                 AND ( ISNULL(@AadharID, '') = ''
                        OR ( STATUTORY.AadharID LIKE '%' + @AadharID + '%' ) )
                 AND ( ISNULL(@UAN, '') = ''
                        OR ( STATUTORY.UANNo LIKE '%' + @UAN + '%' ) )
                 AND ( ISNULL(@PolicyNo, '') = ''
                        OR ( STATUTORY.PolicyNo LIKE '%' + @PolicyNo + '%' ) )
          ORDER  BY EM.ModifiedOn DESC

      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
