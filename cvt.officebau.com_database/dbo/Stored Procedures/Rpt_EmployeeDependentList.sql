﻿/****************************************************************************                 
CREATED BY   : Ajith N                
CREATED DATE  : 31 Aug 2018                
MODIFIED BY   :                 
MODIFIED DATE  :                 
 <summary>                        
   [Rpt_EmployeeDependentList]  '' , '', '', null, 1,1                
 </summary>                                          
 *****************************************************************************/          
CREATE PROCEDURE [dbo].[Rpt_EmployeeDependentList] (@DepartmentID  INT,          
                                                   @DesignationID INT,          
                                                   @BusinessUnit  VARCHAR(100),          
                                                   @IsActive      BIT,          
                                                   @UserID        INT,          
                                                   @DomainID      INT)          
AS          
  BEGIN          
      SET NOCOUNT ON;          
          
      BEGIN TRY          
          DECLARE @BusinessUnitIDs VARCHAR(50)          
          
          IF( Isnull(@BusinessUnit, 0) = 0 )          
            SET @BusinessUnitIDs = (SELECT BusinessUnitID          
                                    FROM   tbl_EmployeeMaster          
                                    WHERE  ID = @UserID          
                                           AND DomainID = @DomainID)          
          ELSE          
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','          
          
          DECLARE @BusinessUnitTable TABLE          
            (          
               BusinessUnit VARCHAR(100)          
            )          
          
          INSERT INTO @BusinessUnitTable          
          SELECT @BusinessUnitIDs          
          
          SELECT Isnull(EM.EmpCodePattern, '') + EM.Code     AS [Employee Code],          
                 EM.FullName AS [Employee Name],          
                 --REGION.NAME                                     AS [Region],                
                 --DEP.NAME                                        AS [Department],                
                 --DESI.NAME                                       AS [Designation],                
                 DD.NAME     AS [Dependent],          
                 CAST(dd.DOB  AS DATE)    AS [Date of Birth],          
                 ( CASE          
                     WHEN DD.DOB IS NULL THEN NULL          
                     ELSE Datediff(YEAR, DD.DOB, Getdate())          
                   END )     AS [Age],          
                 GEN.Code    AS [Gender],          
                 ( CASE DD.RelationshipID          
                     WHEN 1 THEN 'Father'          
                     WHEN 2 THEN 'Mother'          
                     WHEN 3 THEN 'Spouse'          
                     WHEN 4 THEN 'Son'          
                     WHEN 5 THEN 'Daughter'          
                     WHEN 6 THEN 'In-Laws'          
                     ELSE NULL          
                   END )     AS [Relationship],          
                 ( CASE          
                     WHEN EM.IsActive = 0 THEN 'Active'          
                     ELSE 'Inactive'          
                   END )     AS [Status],          
                 BU.NAME     AS [Business Unit]          
          FROM   tbl_EmployeeMaster EM          
                 LEFT JOIN tbl_Department DEP          
                        ON DEP.ID = EM.DepartmentID          
                 LEFT JOIN tbl_Designation DESI          
                        ON DESI.ID = EM.DesignationID          
                 LEFT JOIN tbl_Functional FUN          
                        ON FUN.ID = EM.FunctionalID          
                 LEFT JOIN tbl_BusinessUnit BU          
                        ON BU.ID = EM.BaseLocationID          
                 LEFT JOIN tbl_BusinessUnit REGION          
                        ON REGION.ID = EM.RegionID          
                 LEFT JOIN tbl_EmployeeDependentDetails DD          
                        ON DD.EmployeeID = EM.ID          
                 LEFT JOIN tbl_CodeMaster GEN          
                     ON GEN.ID = DD.GenderID          
                 JOIN @BusinessUnitTable TBU          
                   ON TBU.BusinessUnit LIKE '%,'          
                                          + Cast(EM.BaseLocationID AS VARCHAR(10))          
                                            + ',%'          
          WHERE  EM.IsDeleted = 0          
                 AND ( @BusinessUnit = 0          
                        OR EM.BaseLocationID = @BusinessUnit )          
                 AND ( @IsActive IS NULL          
      OR EM.IsActive = @IsActive )          
                 AND EM.Code NOT LIKE 'TMP_%'          
                 AND EM.domainID = @DomainID          
                 AND ( Isnull(@DepartmentID, '') = ''          
                        OR DEP.ID = @DepartmentID )          
                 AND ( Isnull(@DesignationID, '') = ''          
                        OR DESI.ID = @DesignationID )          
          ORDER  BY EM.Code       
      END TRY          
      BEGIN CATCH          
          DECLARE @ErrorMsg    VARCHAR(100),          
                  @ErrSeverity TINYINT;          
          SELECT @ErrorMsg = Error_message(),          
                 @ErrSeverity = Error_severity()          
          RAISERROR(@ErrorMsg,@ErrSeverity,1)          
      END CATCH          
  END
