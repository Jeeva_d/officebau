﻿/****************************************************************************   
CREATED BY		:  
CREATED DATE	: 
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
 [GetAdminforCompany] 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetAdminforCompany] (@ID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT ADM.ID                        AS ID,
                 ADM.NAME                      AS CompanyName,
                 ADM.Modules                   AS Modules,
                 BU.NAME                       AS BusinessUnit,
                 ADM.EmailID                   AS EmailID,
                 EMP.FullName                  AS ModifiedBy,
                 ADM.ModifiedOn                AS ModifiedOn,
                 ADM.ContactNo                 AS ContactNo,
                 Cast(ADM.Logo AS VARCHAR(50)) AS Logo
          FROM   tbl_Company ADM
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = ADM.ModifiedBy
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.DomainID = ADM.ID
                           AND Isnull(BU.ParentID, 0) = 0 and BU.isdeleted=0
          WHERE  ADM.IsDeleted = 0
                 AND ADM.ID = @ID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
