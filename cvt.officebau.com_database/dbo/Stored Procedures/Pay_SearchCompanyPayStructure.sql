﻿/****************************************************************************       
CREATED BY   :       
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>    
[Pay_SearchCompanyPayStructure] 1    
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_SearchCompanyPayStructure] (@DomainID INT)  
AS  
BEGIN  
 BEGIN TRY  
  
  
  SELECT  
   tpcps.id  
     ,tpcps.PaystructureName AS Name  
     ,tpcps.EffectiveFrom  
     ,em.FullName  
     ,tpcps.ModifiedOn AS ModifiedOn  
     ,(SELECT COUNT(*) FROM tbl_Pay_PayrollCompanyPayStructure AS tppcps   
     WHERE tppcps.CompanyPayStructureID=tpcps.ID AND tppcps.IsDeleted=0)  AS Count  
  FROM tbl_Pay_CompanyPayStructure AS tpcps  
  JOIN tbl_EmployeeMaster em  
   ON em.id = tpcps.ModifiedBy  
  WHERE tpcps.DomainID = @DomainID  
  AND tpcps.IsDeleted = 0  
  ORDER BY tpcps.ModifiedOn DESC  
 END TRY  
 BEGIN CATCH  
  DECLARE @ErrorMsg VARCHAR(100)  
      ,@ErrSeverity TINYINT  
  SELECT  
   @ErrorMsg = ERROR_MESSAGE()  
     ,@ErrSeverity = ERROR_SEVERITY()  
  RAISERROR (@ErrorMsg, @ErrSeverity, 1)  
 END CATCH  
END
