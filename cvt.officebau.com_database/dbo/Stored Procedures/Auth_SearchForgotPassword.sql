﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	01-08-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
    Search password using email id
	[Auth_SearchForgotPassword] 'rajbharath.in@gmail.com'
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_SearchForgotPassword]
 (
	@Email		varchar(150)	
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY					
		SELECT 
			AW.FullName		AS FullName		,
			usr.Username	AS UserName		,
			usr.[Password]	AS [Password] 
		FROM 
			Auth_WebLogin AW
		JOIN
			Auth_Users usr ON usr.WebLoginID = AW.ID
		WHERE
			AW.Email = @Email AND AW.IsDeleted = 0

	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
