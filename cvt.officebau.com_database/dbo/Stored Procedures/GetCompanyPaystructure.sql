﻿/****************************************************************************   
CREATED BY   :  Dhanalakshmi. S
CREATED DATE  :   22 JUNE 2017 
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
       [Getcompanypaystructure] 6,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetCompanyPaystructure] (@ID       INT,
                                                 @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT CPS.ID               AS ID,
                 CPS.NAME             AS NAME,
                 CPS.[Basic]          AS [Basic],
                 CPS.HRA              AS HRA,
                 CPS.MedicalAllowance AS MedicalAllowance,
                 CPS.Conveyance       AS Conveyance,
                 CPS.EducationAllow   AS EducationAllowance,
                 CPS.MonsoonAllow     AS MonsoonAllow,
                 CPS.MagazineAllow    AS PaperMagazine,
                 CPS.EffectiveFrom    AS EffictiveFrom,
                 EMP.FullName         AS ModifiedBy,
                 CPS.ModifiedOn       AS ModifiedOn,
                 CPS.BusinessUnitID   AS BusinessUnit,
                 BU.NAME              AS BaseLocation
          FROM   tbl_CompanyPayStructure CPS
                 --LEFT JOIN tbl_EmployeePayStructure EMS
                 --       ON EMS.CompanyPayStubId = CPS.Id
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON CPS.ModifiedBy = EMP.ID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EMP.BaseLocationID
          WHERE  CPS.DomainID = @DomainID
                 AND CPS.ID = @ID
                 AND CPS.IsDeleted = 0

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
