﻿ /****************************************************************************     
CREATED BY  : Dhanalakshmi    
CREATED DATE : 14/11/2016     
MODIFIED BY  : Jennifer S   
MODIFIED DATE : 02 Sep 2017   
<summary>     
select * from tblcustomer;    
</summary>                             
*****************************************************************************/  
CREATE PROCEDURE [dbo].[Managetransfer] (@ID                 INT,  
                                        @FromID             INT,  
                                        @ToID               INT,  
                                        @Amount             MONEY,  
                                        @VoucherDate        DATETIME,  
                                        @VoucherDescription VARCHAR(1000),  
                                        @HasDeleted         BIT,  
                                        @SessionID          INT,  
                                        @DomainID           INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(1000)  
  
      BEGIN TRY  
          IF( (SELECT Count(1)  
               FROM   tblCashBucket  
               WHERE  DomainID = @DomainID) = 0 )  
            INSERT INTO tblCashBucket  
                        (AvailableCash,  
                         DomainID)  
            VALUES      (0,  
                         @DomainID)  
  
          IF( (SELECT 1  
               FROM   tblTransfer  
               WHERE  id = @ID  
                      AND ToID = 0  
                      AND @DomainID = DomainID) = 1 )  
            BEGIN  
                IF NOT EXISTS (SELECT 1  
                               FROM   tblBRS  
                               WHERE  SourceID = @ID  
                                      AND SourceType IN (SELECT ID  
                                                         FROM   tblMasterTypes  
                                                         WHERE  NAME IN ( 'FromBank', 'ToBank' ))  
                                      AND IsReconsiled = 1  
                                      AND IsDeleted = 0  
                                      AND DomainID = @DomainID)  
                  BEGIN  
                      UPDATE tblCashBucket  
                      SET    AvailableCash = AvailableCash - (SELECT amount  
                                                              FROM   tblTransfer  
                                                              WHERE  id = @ID  
                                                                     AND @DomainID = DomainID)  
                      WHERE  DomainID = @DomainID  
  
                      UPDATE tblVirtualCash  
                      SET    IsDeleted = 1  
                      WHERE  SourceID = @ID  
                             AND SourceType IN (SELECT ID  
                                                FROM   tblMasterTypes  
                                                WHERE  NAME IN ( 'FromBank', 'ToBank' ))  
                             AND IsDeleted = 0  
                             AND DomainID = @DomainID  
                  END  
            END  
  
          IF( (SELECT 1  
               FROM   tblTransfer  
               WHERE  id = @ID  
                      AND FromID = 0  
                      AND @DomainID = DomainID) = 1 )  
            BEGIN  
                IF NOT EXISTS (SELECT 1  
                               FROM   tblBRS  
                               WHERE  SourceID = @ID  
                                      AND SourceType IN (SELECT ID  
                                                         FROM   tblMasterTypes  
                                                         WHERE  NAME IN ( 'FromBank', 'ToBank' ))  
                                      AND IsReconsiled = 1  
                                      AND IsDeleted = 0  
                                      AND DomainID = @DomainID)  
                  BEGIN  
                      UPDATE tblCashBucket  
                      SET    AvailableCash = AvailableCash  
                                             + (SELECT amount  
                                                FROM   tblTransfer  
                                                WHERE  id = @ID  
                                                       AND @DomainID = DomainID)  
                      WHERE  @DomainID = DomainID  
  
                      UPDATE tblVirtualCash  
                      SET    IsDeleted = 1  
                      WHERE  SourceID = @ID  
                             AND SourceType IN (SELECT ID  
                                                FROM   tblMasterTypes  
                                                WHERE  NAME IN ( 'FromBank', 'ToBank' ))  
                             AND IsDeleted = 0  
                             AND DomainID = @DomainID  
                  END  
            END  
  
          IF( @HasDeleted = 1 )  
            BEGIN  
                IF EXISTS (SELECT 1  
                           FROM   tblBRS  
                           WHERE  SourceID = @ID  
                                  AND SourceType IN (SELECT ID  
                                                     FROM   tblMasterTypes  
                                                     WHERE  NAME IN ( 'FromBank', 'ToBank' ))  
                                  AND IsReconsiled = 1  
                                  AND IsDeleted = 0  
                                  AND DomainID = @DomainID)  
                  BEGIN  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Warning'  
                                            AND Code = 'RCD_RECON_DEL_WARN'  
                                            AND IsDeleted = 0) --'The record is Reconcilsed. So you cannot Delete.'    
                  END  
                ELSE  
                  BEGIN  
                      UPDATE tblTransfer  
                      SET    IsDeleted = 1  
                      WHERE  ID = @ID  
  
                      UPDATE tblBookedBankBalance  
                      SET    IsDeleted = 1  
                      WHERE  BRSID IN (SELECT ID  
                                       FROM   tblBRS  
                                       WHERE  SourceID = @ID  
                                              AND IsDeleted = 0  
                                              AND DomainID = @DomainID)  
  
                      UPDATE tblBRS  
                      SET    IsDeleted = 1  
                      WHERE  SourceID = @ID  
                             AND DomainID = @DomainID  
  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Information'  
                                            AND Code = 'RCD_DEL'  
                                            AND IsDeleted = 0) --'Deleted Successfully'    
                  END  
            END  
          ELSE  
            BEGIN  
                IF( @ID = 0 )  
                  BEGIN  
                      INSERT INTO tblTransfer  
                                  (FromID,  
                                   ToID,  
                                   Amount,  
                                   VoucherDate,  
                                   VoucherDescription,             
                                   CreatedBy,  
           CreatedOn,  
                                   ModifiedBy,  
           ModifiedOn,  
                                   DomainID)  
                      VALUES      (@FromID,  
                                   @ToID,  
                                   @Amount,  
                                   @VoucherDate,  
                                   @VoucherDescription,  
                                   @SessionID,  
           getdate(),  
                                   @SessionID,  
           getdate(),  
                                   @DomainID)  
  
              DECLARE @Source INT=@@IDENTITY  
  
                      IF( Isnull(@FromID, 0) <> 0 )  
              BEGIN  
                            INSERT INTO tblBRS  
                                        (BankID,  
                                         SourceID,  
                                         SourceDate,  
                                         SourceType,  
                                         Amount,  
                                         DomainID,  
                                         BRSDescription)  
                            VALUES      (@FromID,  
                                         @Source,  
                                         @VoucherDate,  
                                         (SELECT ID  
                                          FROM   tblMasterTypes  
                                          WHERE  NAME = 'FromBank'),  
                                         @Amount,  
                                         @DomainID,  
                                         @VoucherDescription)  
  
                            INSERT INTO tblBookedBankBalance  
                                        (BRSID,  
                                         Amount,  
                                         DomainID,  
                                         CreatedBy,  
                                         ModifiedBy)  
                            VALUES      (@@IDENTITY,  
                                         @Amount,  
                                         @DomainID,  
                                         @SessionID,  
                                         @SessionID)  
  
                            EXEC Managesystembankbalance  
                              @DomainID  
                        END  
  
                      IF( Isnull(@ToID, 0) <> 0 )  
                        BEGIN  
                            INSERT INTO tblBRS  
                                        (BankID,  
                                         SourceID,  
                                         SourceDate,  
                                         SourceType,  
                                         Amount,  
                                         DomainID,  
                                         BRSDescription)  
                            VALUES      (@ToID,  
                                         @Source,  
                                         @VoucherDate,  
                                         (SELECT ID  
                                          FROM   tblMasterTypes  
                                          WHERE  NAME = 'TOBank'),  
                                         @Amount,  
                                         @DomainID,  
                                         @VoucherDescription)  
  
                            INSERT INTO tblBookedBankBalance  
                                        (BRSID,  
                                         Amount,  
                                         DomainID,  
                                         CreatedBy,  
                                         ModifiedBy)  
                            VALUES      (@@IDENTITY,  
                                         @Amount,  
                                         @DomainID,  
                                         @SessionID,  
                                         @SessionID)  
  
                            EXEC Managesystembankbalance  
                              @DomainID  
                        END  
  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Information'  
                                            AND Code = 'RCD_INS'  
                                            AND IsDeleted = 0) --'Inserted Successfully'    
                  END  
                ELSE  
                  BEGIN  
                      IF EXISTS (SELECT 1  
                                 FROM   tblBRS  
                                 WHERE  SourceID = @ID  
AND SourceType IN (SELECT ID  
                                                           FROM   tblMasterTypes  
                                                           WHERE  NAME IN ( 'FromBank', 'ToBank' ))  
                                        AND IsReconsiled = 1  
                                        AND IsDeleted = 0  
                                        AND DomainID = @DomainID)  
                        BEGIN  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Warning'  
                                                  AND Code = 'RCD_RECON_UPD_WARN'  
                                                  AND IsDeleted = 0) --'The record is Reconcilsed. So you cannot Update.'    
                        END  
                      ELSE  
                        BEGIN  
                            UPDATE tblTransfer  
                            SET    FromID = @FromID,  
                                   ToID = @ToID,  
                                   Amount = @Amount,  
                                   VoucherDate = @VoucherDate,  
                                   VoucherDescription = @VoucherDescription,  
                                   ModifiedBY = @SessionID,  
                                   ModifiedOn = getdate()  
                            WHERE  ID = @ID  
  
                            IF( Isnull(@FromID, 0) <> 0 )  
                              BEGIN  
                                  IF EXISTS(SELECT 1  
                                            FROM   tblBRS  
                                            WHERE  SourceID = @ID  
                                                   AND DomainID = @DomainID  
                                                   AND SourceType = (SELECT ID  
                                                                     FROM   tblMasterTypes  
                                                                     WHERE  NAME = 'FromBank')  
                                                   AND IsDeleted = 0  
                                                   AND DomainID = @DomainID)  
                                    BEGIN  
                                        UPDATE tblBRS  
                                        SET    BankID = @FromID,  
                                               SourceDate = @VoucherDate,  
                                               SourceType = (SELECT ID  
                                                             FROM   tblMasterTypes  
                                                             WHERE  NAME = 'FromBank'),  
                                               Amount = @Amount  
                                        WHERE  SourceID = @ID  
                                               AND SourceType = (SELECT ID  
                                                                 FROM   tblMasterTypes  
                                                                 WHERE  NAME = 'FromBank')  
                                               AND @DomainID = DomainID  
  
                                        UPDATE tblBookedBankBalance  
                                        SET    Amount = @Amount  
                                        WHERE  BRSID = (SELECT ID  
                                                        FROM   tblBRS  
                                                        WHERE  SourceID = @ID  
                                                               AND SourceType = (SELECT ID  
                                                                                 FROM   tblMasterTypes  
                                                                                 WHERE  NAME = 'FromBank')  
                                                               AND IsDeleted = 0  
                 AND DomainID = @DomainID)  
                              AND DomainID = @DomainID  
  
                                        EXEC Managesystembankbalance  
                                          @DomainID  
                                    END  
                                  ELSE  
                                    BEGIN  
                                        INSERT INTO tblBRS  
                                                    (BankID,  
                                                     SourceID,  
                                                     SourceDate,  
                                                     SourceType,  
                                                     Amount,  
                                                     DomainID,  
                                                     BRSDescription)  
                                        VALUES      (@FromID,  
                                                     @ID,  
                                                     @VoucherDate,  
                                                     (SELECT ID  
                                                      FROM   tblMasterTypes  
                                                      WHERE  NAME = 'FromBank'),  
                                                     @Amount,  
                                                     @DomainID,  
                                                     @VoucherDescription)  
  
                                        INSERT INTO tblBookedBankBalance  
                                                    (BRSID,  
                                                     Amount,  
                                                     DomainID,  
                                                     CreatedBy,  
                                                     ModifiedBy)  
                                        VALUES      (@@IDENTITY,  
                                                     @Amount,  
                                                     @DomainID,  
                                                     @SessionID,  
                                                     @SessionID)  
  
                                        EXEC Managesystembankbalance  
                                          @DomainID  
                                    END  
                              END  
                            ELSE  
                              BEGIN  
                                  IF( (SELECT 1  
                                       FROM   tblBRS  
                                       WHERE  SourceID = @ID  
                                              AND DomainID = @DomainID  
                                              AND SourceType = (SELECT ID  
                                                                FROM   tblMasterTypes  
                                                                WHERE  NAME = 'FromBank')  
                                              AND IsDeleted = 0  
                                              AND DomainID = @DomainID) = 1 )  
                                    BEGIN  
                                        UPDATE tblBookedBankBalance  
                                        SET    IsDeleted = 1  
                                        WHERE  BRSID = (SELECT ID  
                                                        FROM   tblBRS  
                                                        WHERE  SourceID = @ID  
                                                               AND SourceType = (SELECT ID  
                                                                                 FROM   tblMasterTypes  
                                                                                 WHERE  NAME = 'FromBank')  
                                                               AND IsDeleted = 0  
                                                               AND DomainID = @DomainID)  
                    AND DomainID = @DomainID  
  
                                        UPDATE tblBRS  
                                        SET    IsDeleted = 1  
                                        WHERE  SourceID = @ID  
                                               AND DomainID = @DomainID  
                                               AND SourceType = (SELECT ID  
                                                                 FROM   tblMasterTypes  
                                                                 WHERE  NAME = 'FromBank')  
                                               AND IsDeleted = 0  
                                    END  
                              END  
  
                            IF( Isnull(@ToID, 0) <> 0 )  
                              BEGIN  
                                  IF EXISTS(SELECT 1  
                                            FROM   tblBRS  
                                            WHERE  SourceID = @ID  
                                                   AND DomainID = @DomainID  
                                                   AND SourceType = (SELECT ID  
                                                                     FROM   tblMasterTypes  
                                                                     WHERE  NAME = 'ToBank')  
                                                   AND IsDeleted = 0  
                                                   AND DomainID = @DomainID)  
                                    BEGIN  
                                        UPDATE tblBRS  
                                        SET    BankID = @ToID,  
                                               BRSDescription = @VoucherDescription,  
                                               SourceDate = @VoucherDate,  
                                               SourceType = (SELECT ID  
                                                             FROM   tblMasterTypes  
                                                             WHERE  NAME = 'ToBank'),  
                                               Amount = @Amount  
                                        WHERE  SourceID = @ID  
                                               AND SourceType = (SELECT ID  
                                                                 FROM   tblMasterTypes  
                                                                 WHERE  NAME = 'ToBank')  
                                               AND IsDeleted = 0  
                                               AND @DomainID = DomainID  
  
                                        UPDATE tblBookedBankBalance  
                                        SET    Amount = @Amount  
                                        WHERE  BRSID = (SELECT ID  
                                                        FROM   tblBRS  
                                                        WHERE  SourceID = @ID  
                                                               AND SourceType = (SELECT ID  
                                                                                 FROM   tblMasterTypes  
                                                                                 WHERE  NAME = 'ToBank')  
                                                               AND IsDeleted = 0  
                                                               AND DomainID = @DomainID)  
                                               AND DomainID = @DomainID  
  
                                        EXEC Managesystembankbalance  
                                          @DomainID  
                                    END  
                                  ELSE  
                                    BEGIN  
                                        INSERT INTO tblBRS  
                                                    (BankID,  
                                                     SourceID,  
                                                     SourceDate,  
       SourceType,  
                             Amount,  
                                                     DomainID,  
                                                     BRSDescription)  
                                        VALUES      (@ToID,  
                                                     @ID,  
                                                     @VoucherDate,  
                                                     (SELECT ID  
                                                      FROM   tblMasterTypes  
                                                      WHERE  NAME = 'ToBank'),  
                                                     @Amount,  
                                                     @DomainID,  
                                                     @VoucherDescription)  
  
                                        INSERT INTO tblBookedBankBalance  
                                                    (BRSID,  
                                                     Amount,  
                                                     DomainID,  
                                                     CreatedBy,  
                                                     ModifiedBy)  
                                        VALUES      (@@IDENTITY,  
                                                     @Amount,  
                                                     @DomainID,  
                                                     @SessionID,  
                                                     @SessionID)  
  
                                        EXEC Managesystembankbalance  
                                          @DomainID  
                                    END  
                              END  
                            ELSE  
                              BEGIN  
                                  IF( (SELECT 1  
                                       FROM   tblBRS  
                                       WHERE  SourceID = @ID  
                                              AND DomainID = @DomainID  
                                              AND SourceType = (SELECT ID  
                                                                FROM   tblMasterTypes  
                                                                WHERE  NAME = 'TOBank')  
                                              AND IsDeleted = 0  
                                              AND DomainID = @DomainID) = 1 )  
                                    BEGIN  
                                        UPDATE tblBookedBankBalance  
                                        SET    IsDeleted = 1  
                                        WHERE  BRSID = (SELECT ID  
                                                        FROM   tblBRS  
                                                        WHERE  SourceID = @ID  
                                                               AND SourceType = (SELECT ID  
                                                                                 FROM   tblMasterTypes  
                                                                                 WHERE  NAME = 'TOBank')  
                                                               AND IsDeleted = 0  
                                                               AND DomainID = @DomainID)  
                                               AND DomainID = @DomainID  
  
                                        UPDATE tblBRS  
                                        SET    IsDeleted = 1  
                                        WHERE  SourceID = @ID  
                                               AND DomainID = @DomainID  
                                               AND SourceType = (SELECT ID  
                                                                 FROM   tblMasterTypes  
                                                                 WHERE  NAME = 'TOBank')  
                                               AND IsDeleted = 0  
                                               AND DomainID = @DomainID  
                               END  
                              END  
  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Information'  
                                                  AND Code = 'RCD_UPD'  
                                                  AND IsDeleted = 0) --'Updated Successfully'    
                        END  
                  END  
            END  
  
          IF( @FromID = 0 )  
            BEGIN  
                IF NOT EXISTS (SELECT 1  
                               FROM   tblBRS  
                               WHERE  SourceID = @ID  
                                      AND SourceType IN (SELECT ID  
                                                         FROM   tblMasterTypes  
                                                         WHERE  NAME IN ( 'FromBank', 'ToBank' ))  
                                      AND IsReconsiled = 1  
                                      AND IsDeleted = 0  
                                      AND DomainID = @DomainID)  
                  BEGIN  
                      UPDATE tblCashBucket  
                      SET    AvailableCash = AvailableCash - @Amount  
                      WHERE  @DomainID = DomainID  
  
                      INSERT INTO tblVirtualCash  
                                  (SourceID,  
                                   Amount,  
                                   Date,  
                                   SourceType,  
                                   CreatedBy,  
                                   CreatedOn)  
                      VALUES      (( CASE  
                                       WHEN ( @ID = 0 )THEN( @Source )  
                                       ELSE @ID  
                                     END ),  
                                   ( -1 ) * @Amount,  
                                   @VoucherDate,  
                                   (SELECT ID  
                                    FROM   tblMasterTypes  
                                    WHERE  NAME = 'FromBank'),  
                                   @SessionID,  
                                   getdate())  
                  END  
            END  
          ELSE IF( @ToID = 0 )  
            BEGIN  
                IF NOT EXISTS (SELECT 1  
                               FROM   tblBRS  
                               WHERE  SourceID = @ID  
                                      AND SourceType IN (SELECT ID  
                                                         FROM   tblMasterTypes  
                                                         WHERE  NAME IN ( 'FromBank', 'ToBank' ))  
                                      AND IsReconsiled = 1  
                                      AND IsDeleted = 0  
                                      AND DomainID = @DomainID)  
                  BEGIN  
                      UPDATE tblCashBucket  
                      SET    AvailableCash = AvailableCash + @Amount  
                      WHERE  @DomainID = DomainID  
  
                      INSERT INTO tblVirtualCash  
                                  (SourceID,  
                                   Amount,  
                                   Date,  
                                   SourceType,  
                                   CreatedBy,  
                                   CreatedOn)  
                      VALUES      (( CASE  
                                       WHEN ( @ID = 0 )THEN( @Source )  
                                       ELSE @ID  
                                     END ),  
                                   @Amount,  
                                   @VoucherDate,  
                                   (SELECT ID  
                                    FROM   tblMasterTypes  
                                    WHERE  NAME = 'ToBank'),  
                                   @SessionID,  
                                   getdate())  
          END  
            END  
  
          SELECT @Output  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
