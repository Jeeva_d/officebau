﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	30-12-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
      For Online Payment process
	  select * from Auth_Orders
	  select * from Auth_OrderTransaction
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_ManageOrders]
 (
	@CustomerID			INT				,
	@Package			INT				,
	@Amount				MONEY			,
	@BillingName		VARCHAR(150)	,
	@BillingEmail		VARCHAR(150)	,
	@BillingAddress		VARCHAR(200)	,
	@BillingCity		VARCHAR(100)	,
	@BillingState		VARCHAR(100)	,
	@BillingZip			VARCHAR(100)	,
	@BillingCountry		VARCHAR(100)	,
	@BillingTelephone	VARCHAR(20)		,
	@ShippingName		VARCHAR(150)	,
	@ShippingAddress	VARCHAR(200)	,
	@ShippingCity		VARCHAR(100)	,
	@ShippingState		VARCHAR(100)	,
	@ShippingZip		VARCHAR(100)	,
	@ShippingCountry	VARCHAR(100)	,
	@ShippingTelephone	VARCHAR(20)		
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY					
		SET NOCOUNT ON;
		DECLARE @Output						VARCHAR(100)				
		BEGIN TRANSACTION
			INSERT INTO Auth_Orders
			(
				CustomerID			,
				Package				,
				Amount				,
				BillingName			,
				BillingEmail		,
				BillingAddress		,
				BillingCity			,
				BillingState		,
				BillingZip			,
				BillingCountry		,
				BillingTelephone	,
				ShippingName		,
				ShippingAddress		,
				ShippingCity		,
				ShippingState		,
				ShippingZip			,
				ShippingCountry		,
				ShippingTelephone	
			)
			VALUES
			(
				@CustomerID			,
				@Package			,
				@Amount				,
				@BillingName		,
				@BillingEmail		,
				@BillingAddress		,
				@BillingCity		,
				@BillingState		,
				@BillingZip			,
				@BillingCountry		,
				@BillingTelephone	,
				@ShippingName		,
				@ShippingAddress	,
				@ShippingCity		,
				@ShippingState		,
				@ShippingZip		,
				@ShippingCountry	,
				@ShippingTelephone				
			)
			SET @Output = 'OK'
		SELECT @Output
		COMMIT TRANSACTION      
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
