﻿/****************************************************************************           
CREATED BY   : Jeeva          
CREATED DATE  :           
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>        
 [Getproduct] Null,1,1          
 [GetPreRequestProduct] 1,1        
 [Getprerequestproduct] 'Purchase',1     
 </summary>                                   
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetPreRequestProduct] (@Type     VARCHAR(1000),
                                               @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT pd.ID                                              AS ID,
                 Substring(pd.NAME, 0, 20) + ' ( ' + cd.Code + ' )' AS NAME
          FROM   tblProduct_V2 pd
                 JOIN tbl_CodeMaster cd
                   ON cd.ID = pd.TypeID
          WHERE  pd.DomainID = @DomainID
                 AND IsSales = CASE
                                 WHEN ( @Type = 'Sales' ) THEN IsSales
                                 ELSE IsSales
                               END
                 AND IsPurchase = CASE
                                    WHEN ( @Type = 'Purchase' ) THEN 1
                                    ELSE IsPurchase
                                  END
                 AND pd.IsDeleted = 0
          UNION ALL
          SELECT l.ID,
                 Substring(l.NAME, 0, 20) + '(TAX)'
          FROM   tblLedger l
          WHERE  DomainID = @DomainID
                 AND IsTaxComponent = 1

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
