﻿/****************************************************************************             
CREATED BY  :    
CREATED DATE :     
MODIFIED BY  :     
MODIFIED DATE   :             
 <summary>   
 [GetValidUserForPin] 'ZY322KGJDK',123456                 
 </summary>                                     
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[GetValidUserForPin] (@DeviceInfo VARCHAR(Max),@Pin int)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @OutPut varChar(500),@UserName Varchar(500),@Password varchar(max),@CompanyID int    
                
    
      BEGIN TRY    
          
		  IF Not Exists (Select 1 from tbl_login where DeviceInfo=@DeviceInfo)
		  Begin 
		    set @OutPut = 'PIN not registered for the device'
			GOto FINISH
		  End
		IF Not Exists (Select 1 from tbl_login where DeviceInfo=@DeviceInfo and pin =@Pin)
			Begin 
				set @OutPut = 'PIN invalid'
				GOto FINISH
			End
		IF Exists (Select 1 from tbl_login where DeviceInfo=@DeviceInfo and pin =@Pin)
			Begin 
				Set @Password =(Select l.Password
				 from tbl_Login l join tbl_EmployeeMaster e on e.ID = l.EmployeeID and e.IsActive=0
				 where DeviceInfo=@DeviceInfo and pin =@Pin)
				 Set @UserName=(Select e.LoginCode
				 from tbl_Login l join tbl_EmployeeMaster e on e.ID = l.EmployeeID and  e.IsActive=0
				 where DeviceInfo=@DeviceInfo and pin =@Pin)
				 Set @CompanyID =(Select e.DomainID
				 from tbl_Login l join tbl_EmployeeMaster e on e.ID = l.EmployeeID and  e.IsActive=0
				 where DeviceInfo=@DeviceInfo and pin =@Pin)
				 set @OutPut = 'Valid'
				GOto FINISH
			End

			FINISH:
		Select @OutPut As UserMessage,
		@UserName UserName,@Password Password,@CompanyID companyid

      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
