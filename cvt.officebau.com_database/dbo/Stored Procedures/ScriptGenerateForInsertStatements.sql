﻿/****************************************************************************         
CREATED BY		:         
CREATED DATE	:         
MODIFIED BY		: JENNIFER S       
MODIFIED DATE   : 22-JUN-2017        
<summary>                
   --	ScriptGenerateForInsertStatements 'PettyCashManagement' ,'ID'
</summary>                                 
*****************************************************************************/
CREATE PROC [dbo].[ScriptGenerateForInsertStatements] (@TableName      VARCHAR(Max),
                                                      @IdentityColumn VARCHAR(Max) = '',
                                                      @WhereClause    VARCHAR(Max) = NULL)
AS
  BEGIN
      DECLARE @sql VARCHAR(Max)
      DECLARE @sqlColumns VARCHAR(Max)
      DECLARE @SqlColumnValues VARCHAR(Max)

      SELECT @sqlColumns = COALESCE(@SqlColumns + '],[', '')
                           + Column_Name
      FROM   Information_Schema.Columns
      WHERE  Table_name = @TableName
             AND COLUMN_NAME <> @IdentityColumn

      SELECT @SqlColumnValues = COALESCE(@SqlColumnValues + '+ '','' + ', '')
                                + CASE
                                    WHEN Data_Type IN ( 'varchar', 'nvarchar', 'char', 'nchar',
                                                        'date', 'uniqueidentifier', 'datetime' ) THEN 'ISNULL(QuoteName([' + Column_Name
                                                                                                      + '],''''''''), ''NULL'')'
                                    ELSE 'ISNULL(Cast([' + Column_Name
                                         + '] as varchar(MAX)), ''NULL'')'
                                  END
      FROM   Information_Schema.Columns
      WHERE  Table_name = @TableName
             AND COLUMN_NAME <> @IdentityColumn

      SELECT @sql = 'Select ''Insert Into ' + @TableName + ' (['
                    + @SqlColumns + ']) Values ('' + '
                    + @SqlColumnValues + ' + '')'' FROM '
                    + @TableName
                    + Isnull(' WHERE ' + @WhereClause, '')

      EXEC(@sql)
  END
