﻿/**************************************************************************** 
CREATED BY			:	DhanaLakshmi.S
CREATED DATE		:	23-JUN-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
   [SearchEmployeePayList]  1 , 1
 </summary>                          
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchEmployeePayList] (@ID       INT,
                                               @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT EPS.Id                                       AS ID,
                 EPS.EmployeeId                               AS EmployeeID,
                 Isnull(EMP.Code, '') + ' - ' + EMP.FullName AS EmployeeName,
                 EPS.CompanyPayStubId                         AS CompanyPayStucId,
                 CPS.NAME                                     AS CompanyPayStucName,
                 EPS.Gross                                    AS Gross,
                 EPS.EffectiveFrom                            AS EffictiveFrom,
                 EPS.IsPFRequired                             AS IsPFRequired,
                 EPS.IsESIRequired                            AS IsESIRequired,
                 EMP.doj                                      AS DOJ,
                 EPS.MedicalAllowance                         AS MedicalAllowance,
                 EPS.Conveyance                               AS Conveyance,
                 EPS.EducationAllow                           AS EducationAllowance,
                 EPS.MagazineAllow                            AS PaperMagazine,
                 EPS.MonsoonAllow                             AS MonsoonAllow,
                 E.FullName                                  AS ModifiedBy,
                 EPS.ModifiedOn                               AS ModifiedOn,
				 BU.Name                                      AS BaseLocation
          FROM   tbl_EmployeePayStructure EPS
                 LEFT JOIN tbl_CompanyPayStructure CPS
                        ON CPS.Id = EPS.CompanyPayStubId
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EPS.EmployeeId = EMP.ID
                 LEFT JOIN tbl_employeeMAster E
                        ON E.ID = EPS.ModifiedBy
				 LEFT JOIN tbl_BusinessUnit BU
						ON BU.ID = E.BaseLocationID
          WHERE  EPS.Id = @ID
                 AND EPS.DomainId = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
