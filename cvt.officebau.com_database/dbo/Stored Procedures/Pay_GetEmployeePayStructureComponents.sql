﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :           
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>        
 Pay_GetEmployeePayStructureComponents 1004,20000,0,0      
 </summary>                                   
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Pay_GetEmployeePayStructureComponents] (@CompanyPayStructureId  INT,      
                                                                @Gross                  DECIMAL(18, 2),      
                                                                @ComponentId            INT,      
                                                                @Amount                 DECIMAL(18, 2),      
                                                                @EmployeeId             INT,      
                                                                @EmployeePayStructureId INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION;      
      
          BEGIN      
              DECLARE @Formula NVARCHAR(4000);      
      
              ----------------------------------------      
              SELECT c.ID    AS Id,      
                     psd.id  AS PayStructureDetailsId,      
                     com.Code,      
                     com.Description,      
                     com.Ordinal,      
                     c.ComponentID,      
                     ( CASE      
                         WHEN psd.id IS NOT NULL THEN      
                           Cast(psd.Amount AS VARCHAR)      
                         ELSE      
                           c.value      
                       END ) AS value,      
                     ( CASE      
                         WHEN psd.id IS NULL THEN      
                           ( CASE      
                               WHEN( Ordinal = 1 ) THEN      
                                 Cast(@Gross AS VARCHAR(MAX))      
                               ELSE      
                                 ( CASE      
                                     WHEN ( c.ComponentID = ISNULL(@ComponentId, 0) ) THEN      
                                       Cast(@Amount AS VARCHAR(MAX))      
                                     ELSE      
                                       c.value      
                                   END )      
                             END )      
                         ELSE      
                           ( CASE      
                               WHEN( Ordinal = 1 ) THEN      
                                 Cast(@Gross AS VARCHAR(MAX))      
                               ELSE      
                                 ( CASE      
                                     WHEN ( c.ComponentID = ISNULL(@ComponentId, 0) ) THEN      
                                       Cast(@Amount AS VARCHAR(MAX))      
                                     WHEN ( c.ComponentID != ISNULL(@ComponentId, 0) )      
                                          AND ( c.IsEditable = 1 ) THEN      
                                       Cast(psd.Amount AS VARCHAR)      
                                     ELSE      
                                       c.value      
                                   END )      
                             END )      
                       END ) AS Amount,      
                     0       AS HasUpdated,      
                     c.IsEditable      
              INTO   #tblcompanypay      
              FROM   tbl_Pay_PayrollCompanyPayStructure c      
                     JOIN tbl_Pay_PayrollCompontents com      
                       ON com.ID = c.ComponentID      
                     ---------------      
                     LEFT JOIN tbl_Pay_EmployeePayStructure eps      
                            ON eps.IsDeleted = 0      
                               AND eps.CompanyPayStructureID = c.CompanyPayStructureID      
                AND eps.EmployeeID = @EmployeeId      
                     LEFT JOIN tbl_Pay_EmployeePayStructureDetails psd      
ON psd.PayStructureId = eps.id      
                               AND psd.ComponentID = com.ID      
                               AND eps.id = @EmployeePayStructureId      
              WHERE  c.IsDeleted = 0      
                     AND c.CompanyPayStructureID = @CompanyPayStructureId;      
      
              ----------------------------------------      
              DECLARE @ID    INT,      
                      @count INT= (SELECT Count(1)      
                         FROM   #tblcompanypay);      
      
              ----------------------------------------      
              WHILE( @count <> 0 )      
                BEGIN      
                    SET @ID = (SELECT TOP 1 ID      
                               FROM   #tblcompanypay      
                               WHERE  hasupdated = 0      
                               ORDER  BY Ordinal);      
      
                    -----------------------------------------      
                    UPDATE #tblcompanypay      
                    SET    hasupdated = 1      
                    WHERE  ID = @ID;      
      
                    UPDATE #tblcompanypay      
                    SET    amount = Replace(P.amount, (SELECT Code      
                                                       FROM   #tblcompanypay      
                                                       WHERE  ID = @ID), (SELECT amount      
                                                                          FROM   #tblcompanypay      
                                                                          WHERE  ID = @ID))      
                    FROM   #tblcompanypay P;      
      
                    -----------------------      
                    SET @count = @count - 1;      
                END;      
      
              SET @count = (SELECT Count(1)      
                            FROM   #tblcompanypay);      
      
              WHILE( @count <> 0 )      
                BEGIN      
                    SET @ID = (SELECT TOP 1 ID      
                               FROM   #tblcompanypay      
                               WHERE  hasupdated = 1      
                                and Amount NOT LIKE '%[a-zA-Z][a-zA-Z]%');      
       IF @ID is null  
        Set @ID= (Select ID FROM   #tblcompanypay      
                               WHERE  hasupdated = 1 and amount like '%Case %') 
                    UPDATE #tblcompanypay      
                    SET    hasupdated = 0      
                    WHERE  ID = @ID;      
      
                    SET @Formula = (SELECT amount      
                                    FROM   #tblcompanypay      
                                    WHERE  ID = @ID);      
      
                    DECLARE @input VARCHAR(MAX)= '(' + @Formula + ')';      
                    DECLARE @output        VARCHAR(MAX),      
                            @resultdecimal NVARCHAR(MAX);      
                    DECLARE @i INT= 0;      
      
                    WHILE @i < Len(@input)      
                      BEGIN      
                          SET @i = @i + 1;      
      
                          IF(dbo.isReallyNumeric(Substring(@input, @i, 1)) IS NOT NULL )      
                            BEGIN      
                                SET @output = ISNULL(@output, '') + Substring(@input, @i, 1);      
                            END;      
                          ELSE      
                            BEGIN      
                                SET @resultdecimal = ISNULL(@resultdecimal, '')      
                                                     + ISNULL(@output, '')      
                                                        
                                                     + Substring(@input, @i, 1);      
                                SET @output = '';      
                            END;      
                      END;      
      
                    SET @Formula = @resultdecimal;      
                    SET @resultdecimal = '';      
      
                    DECLARE @sql NVARCHAR(MAX);      
                    DECLARE @result DECIMAL(18, 2);      
      
                    SET @sql = N'set @result = ' + @Formula;      
      
                    EXEC sp_executesql      
                      @sql,      
                      N'@result DECIMAL output',      
                      @result OUT;      
      
                UPDATE #tblcompanypay      
                    SET    amount = @result      
                    WHERE  ID = @ID;      
      
                    SET @count = @count - 1;      
                END;      
      
              SELECT ( CASE      
                         WHEN PayStructureDetailsId IS NOT NULL THEN      
                           PayStructureDetailsId      
                         ELSE      
                           0      
                       END )                                AS Id,      
                     #tblcompanypay.Code,      
                     #tblcompanypay.Description,      
                     #tblcompanypay.Ordinal,      
                     #tblcompanypay.ComponentID,      
                     #tblcompanypay.Value,      
                     Round(Cast(#tblcompanypay.Amount AS DECIMAL),0) AS Amount,      
                     #tblcompanypay.IsEditable      
              FROM   #tblcompanypay;      
      
              DROP TABLE #tblcompanypay;      
          END      
      
          COMMIT TRANSACTION;      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION;      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT;      
          SELECT @ErrorMsg = ERROR_MESSAGE(),      
                 @ErrSeverity = ERROR_SEVERITY();      
          RAISERROR(@ErrorMsg,@ErrSeverity,1);      
      END CATCH;      
  END;
