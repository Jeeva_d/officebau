﻿/****************************************************************************             
CREATED BY  : Naneeshwar.M            
CREATED DATE : 14-JUN-2017    
MODIFIED BY  :         
MODIFIED DATE   :           
 <summary>                    
    [SearchempdropdownforCode] 'Finance Manager',1,2   
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchempdropdownforCode] (@Type       VARCHAR(50),
                                                   @DomainID   INT,
                                                   @EmployeeID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @sql             VARCHAR(MAX),
                  @BusinessUnitIDs VARCHAR(50)

          SET @BusinessUnitIDs = (SELECT EM.BusinessUnitID
                                  FROM   tbl_EmployeeMaster EM
                                  WHERE  EM.ID = @EmployeeID)

          CREATE TABLE #BusinessUnitTable
            (
               BusinessUnit VARCHAR(100)
            )

          INSERT INTO #BusinessUnitTable
          SELECT @BusinessUnitIDs

          IF( @Type != '' )
            BEGIN
                SET @sql = ( ' SELECT A.ID,    
      ISNULL(EmpCodePattern, '''') + Isnull(Code, '''') + '' - '' +  Isnull(FullName, '''') + '''' AS NAME    
      FROM   tbl_EmployeeMaster A    
          LEFT JOIN tbl_EmployeeOtherDetails B    
           ON B.EmployeeID = A.ID    
          LEFT JOIN tbl_ApplicationRole C    
           ON C.ID = B.ApplicationRoleID    
          JOIN #BusinessUnitTable TBU    
           ON TBU.BusinessUnit like ''%,'' + CAST(A.BaseLocationID AS VARCHAR(10)) + '',%''                                         
      WHERE  A.Isdeleted = 0    
        AND ISNULL(A.IsActive, 0) = 0    
          AND B.ApplicationRoleID IN (SELECT ID FROM tbl_ApplicationRole    
                WHERE  NAME LIKE''%'
                             + Cast(@type AS VARCHAR(20))
                             + '%'')    
          AND A.DomainID ='
                             + Cast(@DomainID AS VARCHAR(20))
                             + ' ORDER  BY NAME ASC' )

                PRINT @sql
            END
          ELSE
            SET @sql = ( 'SELECT A.ID,    
       ISNULL(EmpCodePattern, '''')+Isnull(Code, '''')  + '' - '' + Isnull(FullName, '''') + '''' AS NAME    
      FROM   tbl_EmployeeMaster A    
      WHERE  A.Isdeleted = 0    
        AND ISNULL(A.IsActive, 0) = 0    
        AND a.DesignationID IN (SELECT ID    
              FROM   tbl_Designation    
              WHERE  NAME LIKE''%'
                         + Cast(@type AS VARCHAR(20))
                         + '%'')     
              and A.DomainID = '
                         + Cast( @DomainID AS VARCHAR(20))
                         + '    
      ORDER  BY NAME ASC' )

          EXEC(@sql)

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
