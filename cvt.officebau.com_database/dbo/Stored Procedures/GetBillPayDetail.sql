﻿/****************************************************************************     
CREATED BY   : Naneeshwar    
CREATED DATE  :   24-Nov-2016  
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [GetBillPayDetail] 1,1  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetBillPayDetail] (@ID       INT,
                                           @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT ep.ID                             AS ID,
                 ep.Date                           AS PaymentDate,
                 ex.VendorID                       AS VendorID,
                 vd.NAME                           AS VendorName,
                 ex.Type                           AS ScreenType,
                 ep.PaymentModeID                  AS PaymentModeID,
                 vd.PaymentTerms                   AS PaymentTerms,
                 cd.Code                           AS PaymentMode,
                 ep.BankID                         AS BankID,
                 bk.NAME                           AS BankName,
                 ep.Reference                      AS Reference,
                 ep.ModifiedOn                     AS ModifiedOn,
                 EMP.FullName                      AS ModifiedBy,
                 Cast(ex.HistoryID AS VARCHAR(50)) AS FileUploadID
          FROM   tblExpensePayment ep
                 LEFT JOIN tblExpensePaymentMapping expm
                        ON expm.expensepaymentID = @ID
                 LEFT JOIN tblExpense ex
                        ON ex.ID = expm.expenseID
                 LEFT JOIN tblVendor vd
                        ON vd.ID = ex.VendorID
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = ex.ModifiedBy
                 LEFT JOIN tbl_CodeMaster cd
                        ON cd.ID = ep.PaymentModeID
                 LEFT JOIN tblBank bk
                        ON bk.ID = ep.BankID
          WHERE  ep.IsDeleted = 0
                 AND ( ep.ID = @ID
                        OR @ID IS NULL )
                 AND ep.DomainID = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
