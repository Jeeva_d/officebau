﻿
/****************************************************************************   
CREATED BY   :  SIVA
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          [SearchCustomerAutoComplete] 'Sr',1,224
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchCustomerAutoComplete] (@CustomerName VARCHAR(200),
                                                    @DomainID     INT,
                                                    @EmployeeID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @applicationRoleID INT = (SELECT ID
             FROM   tbl_ApplicationRole
             WHERE  NAME = 'Sales Manager'
                    AND DomainID = @DomainID
                    AND Isdeleted = 0)

          SELECT TC.ID          AS ID,
                 TC.EnquiryName AS ContactName
          FROM   tbl_Customer TC
          WHERE  TC.IsDeleted = 0
                 AND TC.DomainID = @DomainID
                 AND tc.EnquiryName LIKE '%' + @CustomerName + '%'
                 AND ( TC.StatusID = (SELECT ID
                                      FROM   tbl_CodeMaster
                                      WHERE  Code = 'Active'
                                             AND [Type] = 'Status') )
                 AND ( TC.SalesPersonID = @EmployeeID
                        OR ( (SELECT Isnull(ApplicationRoleID, 0)
                              FROM   tbl_EMployeeOtherDetails
                              WHERE  EmployeeID = @EmployeeID
                                     AND DomainID = @DomainID
                                     AND Isdeleted = 0) = @applicationRoleID
                             AND ( TC.CreatedBY = @EmployeeID
                                    OR TC.SalesPersonID IN (SELECT DISTINCT T.ID
                                                            FROM   tbl_EmployeeMaster T
                                                                   JOIN tbl_EmployeeOtherDetails TOD
                                                                     ON T.ID = TOD.EmployeeID
                                                                   CROSS APPLY dbo.Splitstring((SELECT BusinessUnitID
                                                                                                FROM   tbl_EmployeeMaster
                                                                                                WHERE  ID = @EmployeeID
                                                                                                       AND Isdeleted = 0
                                                                                                       AND ISNULL(IsActive, 0) = 0
                                                                                                       AND DomainID = @DomainID), ',') S
                                                            WHERE  Patindex('%,' + S.Item + ',%', BusinessUnitID) > 0
                                                                   AND T.ID != @EmployeeID
                                                                   AND TOD.ApplicationRoleID = (SELECT ID
                                                                                                FROM   tbl_ApplicationRole
                                                                                                WHERE  [Name] = 'Sales Executive')
                                                                   AND T.Isdeleted = 0
                                                                   AND ISNULL(T.IsActive, 0) = 0
                                                                   AND T.DomainID = @DomainID) ) ) )
          ORDER  BY TC.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
