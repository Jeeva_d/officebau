﻿/****************************************************************************     
CREATED BY   :  
CREATED DATE  :    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>   
        [CheckPayrollbyMonthly] 5,4,1,'2',2  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Checkpayrollbymonthly] (@MonthID      INT,  
                                               @Year         INT,  
                                               @DomainID     INT,  
                                               @Location     VARCHAR(50),  
                                               @DepartmentID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'  
            + CONVERT(VARCHAR(10), @Year)  
          DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth))  
  
          SET @Year = (SELECT NAME  
                       FROM   tbl_FinancialYear  
                       WHERE  id = @Year  
                              AND IsDeleted = 0  
                              AND DomainId = @DomainID)  
  
          DECLARE @DataSource TABLE  
            (  
               [Value] NVARCHAR(128)  
            )  
  
          INSERT INTO @DataSource  
                      ([Value])  
          SELECT Item  
          FROM   dbo.Splitstring (@Location, ',')  
          WHERE  Isnull(Item, '') <> ''  
  
          DECLARE @PayrollToEmailer VARCHAR(max)=Replace(Replace((SELECT Substring((SELECT Toid  
                                              FROM   tblEmailConfig  
                                              WHERE  [Key] = 'Payroll'  
                                              FOR XML PATH('')), 1, 20000)), '<Toid>', ''), '</Toid>', ',')  
          DECLARE @PayrollCCEmailer VARCHAR(max)=Replace(Replace((SELECT Substring((SELECT CC  
                                              FROM   tblEmailConfig  
                                              WHERE  [Key] = 'Payroll'  
                                              FOR XML PATH('')), 1, 20000)), '<cc>', ''), '</cc>', ',')  
  
          SELECT Cast( Count(1) AS VARCHAR) + '!'  
                 + @PayrollToEmailer + '!' + @PayrollCCEmailer AS UserMessage  
          FROM   tbl_Pay_EmployeePayroll ep  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON emp.ID = ep.EmployeeId  
          WHERE  ep.MonthId = @MonthID  
                 AND ep.YearId = @Year  
                 AND ( ( @Location = Cast(0 AS VARCHAR)  
                          OR @Location IS NULL )  
                        OR ( EMP.BaseLocationID IN (SELECT [Value]  
                                                   FROM   @DataSource  
                                                   WHERE  EMP.IsDeleted = 0  
                                                          AND emp.DomainID = @DomainID) ) )  
                 AND ( ( @DepartmentID IS NULL  
                          OR Isnull(@DepartmentID, 0) = 0 )  
                        OR EMP.DepartmentID = @DepartmentID )  
                 AND ep.IsProcessed = 1  
                 AND ep.IsDeleted = 0  
                 AND ep.DomainId = @DomainID  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
