﻿
/****************************************************************************   
CREATED BY   :   Dhanalakshmi
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [SearchCostCenterList] '',4
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchCostCenterList] (@Name varchar(100),
                                              @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT cc.ID               AS ID,
                 cc.NAME             AS NAME,
                 cc.Remarks          AS Remarks
          FROM   tblCostCenter cc
          WHERE  cc.DomainID = @DomainID
                 AND cc.IsDeleted = 0
          ORDER  BY cc.ModifiedON DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
