﻿/****************************************************************************   
CREATED BY   : Jeeva  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [Adm_UserValidate] '',''
 </summary>                           
 *****************************************************************************/
Create PROCEDURE [dbo].[Adm_SearchDomain] 
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION
		 Select Code,Name,EmailID from Adm_DomainManager

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END




