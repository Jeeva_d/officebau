﻿/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  : 05-July-2017 
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
		[Duplicatenamecheck] 'che1001','EmployeeMaster','',106,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DuplicateNameCheck] (@Name           VARCHAR(100),
                                            @Table          VARCHAR(100),
                                            @BusinessUnitID INT,
                                            @EmployeeID     INT,
                                            @DomainID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(10),
              @SQL    NVARCHAR(max) = ''

      BEGIN TRY
          BEGIN TRANSACTION

          IF ( @Table = 'CompanyPayStructure' )
            BEGIN
                SET @SQL = N'(select  @COUNTS=COUNT(1) from tbl_'
                           + @Table + ' A
				        LEFT JOIN tbl_BusinessUnit B
							ON B.ID = A.BusinessUnitID
						Where A.Name=''' + @Name
                           + ''' and A.DomainID='
                           + CONVERT(VARCHAR(10), @DomainID)
                           + ' and A.IsDeleted=0 AND A.BusinessUnitID='
                           + CONVERT(VARCHAR(10), @BusinessUnitID) + ')'
            END

          IF ( @Table = 'EmployeeMaster' )
            BEGIN
                SET @SQL = N'(select  @COUNTS=COUNT(1) from tbl_'
                           + @Table + ' where LoginCode='''
                           + @Name
                           + ''' and DomainID='
                           + CONVERT(VARCHAR(10), @DomainID)
                           + ' and IsDeleted=0 and ID <> '
                           + CONVERT(VARCHAR(10), @EmployeeID) + ' )'
            END
			
          IF ( @Table = 'Customer' )
            BEGIN
                SET @SQL =  N'(select  @COUNTS=COUNT(1) from tbl_'
                           + @Table + ' Where EnquiryName=''' + @Name
                           + ''' and DomainID='
                           + CONVERT(VARCHAR(10), @DomainID)
                           + ' and IsDeleted=0)'
            END

          EXEC Sp_executesql
            @SQL,
            N'@COUNTS INT OUTPUT',
            @Output OUTPUT;

          SELECT @output AS Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
