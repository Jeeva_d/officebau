﻿/****************************************************************************     
CREATED BY   :     
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
    SearchOtherUserAccess 2, 'Leave request'  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchOtherUserAccess] (@EmployeeID INT,
                                                @ScreenName VARCHAR(100),
                                                @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT *
          INTO   #result
          FROM   dbo.Fnsplitstring ((SELECT DesignationIDs
                                     FROM   tbl_OtherUserAccess
                                     WHERE  ScreenName = @ScreenName), ',')

          SELECT Count(1) AS [Count]
          FROM   #result
          WHERE  Splitdata = (SELECT DesignationID
                              FROM   tbl_EmployeeMaster
                              WHERE  id = @EmployeeID)
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
