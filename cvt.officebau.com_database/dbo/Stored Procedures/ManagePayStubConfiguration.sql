﻿/****************************************************************************             
CREATED BY   : Dhanalakshmi.S            
CREATED DATE  : 12 DEC 2018            
MODIFIED BY   :             
MODIFIED DATE  :             
 <summary>             
          
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManagePayStubConfiguration] (@ID          INT,
                                                    @ComponentID INT,
                                                    @Type        VARCHAR(50),
                                                    @SessionID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(100) = 'Operation Failed!'

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @ID = 0
              AND @Type is not NULL)
            BEGIN
                INSERT INTO tbl_Pay_PaystubConfiguration
                            (ComponentID,
                             [Type],
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn)
                VALUES      (@ComponentID,
                             @Type,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate())

                SET @Output = 'Inserted Successfully.'

                GOTO FINISH
            END
          ELSE
            BEGIN
                      UPDATE tbl_Pay_PaystubConfiguration
                      SET    ComponentID = @ComponentID,
                             [Type] = CASE
                                        WHEN @Type IS NULL THEN [Type]
                                        ELSE @Type
                                      END,
                             IsDeleted = CASE
                                           WHEN @Type IS NULL THEN 1
                                           ELSE 0
                                         END,
                             ModifiedBY = @SessionID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID

                      SET @Output = 'Updated Successfully.'

                      GOTO FINISH
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
