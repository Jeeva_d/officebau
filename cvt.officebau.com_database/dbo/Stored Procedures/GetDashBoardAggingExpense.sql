﻿/****************************************************************************         
CREATED BY   :   
CREATED DATE  :         
MODIFIED BY   :         
MODIFIED DATE  :         
<summary>                
[GetDashBoardAggingExpense] 1        
</summary>                                 
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[GetDashBoardAggingExpense] (@DomainID INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          SELECT Value,        
                 Sum(TotalAmountAP * VAL) TotalAP,        
                 Sum(TotalAmountAR * VAL) TotalAR        
          FROM   (SELECT( (SELECT ( Isnull(Sum(Amount*Qty), 0) )  +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0)      
                           FROM   tblExpenseDetails        
                           WHERE  ExpenseID = ex.ID        
                                  AND IsDeleted = 0        
                                  AND DomainID = @DomainID)        
                           ) - Isnull(ex.PaidAmount, 0)AS TotalAmountAP,        
                        NULL                                                              AS TotalAmountAR,        
                        ( CASE        
                            WHEN Datediff(DD, DueDate, Getdate()) < 0 THEN 1.0        
                            ELSE 0.0        
                          END )                                                           AS [Not yet due],        
                        ( CASE        
                            WHEN Datediff(DD, DueDate, Getdate()) >= 0        
                                 AND Datediff(DD, DueDate, Getdate()) < 30 THEN 1.0        
                            ELSE 0.0        
                          END )                                                           AS [0 to 30],        
                        ( CASE        
                            WHEN Datediff(DD, DueDate, Getdate()) >= 30        
                                 AND Datediff(DD, DueDate, Getdate()) < 60 THEN 1.0        
                            ELSE 0.0        
                          END )                                                           AS [30 to 60],        
                        ( CASE        
                            WHEN Datediff(DD, DueDate, Getdate()) >= 60        
                                 AND Datediff(DD, DueDate, Getdate()) < 90 THEN 1.0        
                            ELSE 0.0        
                          END )                                                           AS [60 to 90],        
                        ( CASE        
                            WHEN Datediff(DD, DueDate, Getdate()) > 90 THEN 1.0        
                            ELSE 0.0        
                          END )                                                           AS [90 & above]        
                  FROM   tblExpense ex        
                  WHERE  ex.StatusID IN(SELECT ID        
                                        FROM   tbl_CodeMaster        
                                        WHERE  Type IN ( 'Open', 'Partial' ) AND IsDeleted = 0        
                                               )        
                         AND Type = 'BILL'        
                         AND DomainID = @DomainID        
                  UNION ALL        
                  SELECT NULL                                   AS TotalAmountAP,        
                         ( CASE        
                             WHEN ( iv.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)+Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0)) - ( Isnull(( Sum(Qty * Rate) * iv.DiscountPercentage ), 0) ), 0) )        
                                                                       FROM   tblInvoiceItem        
                                                        WHERE  InvoiceID = iv.ID        
                              AND IsDeleted = 0        
                          AND DomainID = @DomainID)        
           ELSE (SELECT ( Isnull(( Sum(Qty * Rate)+Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0)) - Isnull(iv.DiscountValue, 0) ), 0) )        
                                   FROM   tblInvoiceItem        
                                   WHERE  InvoiceID = iv.ID        
                                          AND IsDeleted = 0        
                                          AND DomainID = @DomainID)        
                           END ) -( Isnull(iv.ReceivedAmount, 0) +(Select Isnull(Sum(amount),0) from tblInvoiceHoldings where Isdeleted=0 and InvoiceId=iv.ID)) AS TotalAmountAR,        
                         ( CASE        
                             WHEN Datediff(DD, DueDate, Getdate()) < 0 THEN 1.0        
                             ELSE 0.0        
                           END )                                AS [Not yet due],        
                         ( CASE        
                             WHEN Datediff(DD, DueDate, Getdate()) >= 0        
                                  AND Datediff(DD, DueDate, Getdate()) < 30 THEN 1.0        
                             ELSE 0.0        
                           END )                                AS [0 to 30],        
                         ( CASE        
                             WHEN Datediff(DD, DueDate, Getdate()) >= 30        
                                  AND Datediff(DD, DueDate, Getdate()) < 60 THEN 1.0        
                             ELSE 0.0        
                           END )                                AS [30 to 60],        
                         ( CASE        
                             WHEN Datediff(DD, DueDate, Getdate()) >= 60        
                                  AND Datediff(DD, DueDate, Getdate()) < 90 THEN 1.0        
                             ELSE 0.0        
                           END )                                AS [60 to 90],        
                         ( CASE        
                             WHEN Datediff(DD, DueDate, Getdate()) > 90 THEN 1.0        
                             ELSE 0.0        
                           END )                                AS [90 & above]        
                  FROM   tblInvoice iv        
                  WHERE  iv.StatusID IN(SELECT ID        
                                        FROM   tbl_CodeMaster        
                                        WHERE  [Type] IN( 'Open', 'Partial' ) and IsDeleted = 0)        
                         AND DomainID = @DomainID) AS R        
                 UNPIVOT( VAL        
                        FOR Value IN ([Not yet due],        
                                      [0 to 30],        
                                      [30 to 60],        
                                      [60 to 90],        
                                      [90 & above])) AS PivotTable        
          GROUP  BY Value        
          ORDER  BY CASE        
                      WHEN Value = 'Not yet due' THEN 1        
                      WHEN Value = '0 to 30' THEN 2        
                      WHEN Value = '30 to 60' THEN 3        
                      WHEN Value = '60 to 90' THEN 4        
                      WHEN Value = '90 & above' THEN 5        
                    END        
      END TRY        
      BEGIN CATCH        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
