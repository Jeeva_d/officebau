﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   : Naneeshwar.M  
MODIFIED DATE  : 22-SEP-2017  
<summary>      
  [TDS_HRADETAILSENGINE] 14,3,4  
</summary>                           
*****************************************************************************/  
CREATE PROCEDURE [dbo].[TDS_HRADETAILSENGINE] (@EmployeeID INT,  
                                              @Year       INT,  
                                              @Month      INT)  
AS  
  BEGIN  
      DECLARE @StandardDeduction MONEY = 40000; --Standard Deduction (flat 40,000)  
      DECLARE @DomainID INT =(SELECT DomainID  
        FROM   tbl_EmployeeMaster  
        WHERE  id = @EmployeeID)  
      DECLARE @YearNAME           INT = (SELECT FinancialYear  
                 FROM   tbl_FinancialYear  
                 WHERE  id = @Year  
                        AND DomainID = @DomainID),  
              @DateOfJoin         DATE =(SELECT DOJ  
                FROM   tbl_EmployeeMaster  
                WHERE  ID = @EmployeeID),  
              @EmployeeName       VARCHAR(300),  
              @MonthName          VARCHAR(300)= (SELECT code  
                 FROM   tbl_Month  
                 WHERE  id = @Month),  
              @FY                 INT,  
              @PayFY              INT,  
              @DateOfBirth        DATE =(SELECT DateOfBirth  
                FROM   tbl_EmployeePersonalDetails  
                WHERE  EmployeeID = @EmployeeID),  
              @getDate            DATE,  
              @PANNo              VARCHAR(100) = Isnull((SELECT PAN_NO  
                        FROM   tbl_EmployeeStatutoryDetails  
                        WHERE  employeeid = @EmployeeID), ''),  
              @TDSExemptionMonths INT,  
              @FYName             INT  
  
      IF( @Month BETWEEN 1 AND 3 )  
        BEGIN  
            SET @FY = @Year  
            SET @FYName = @YearNAME - 1  
            SET @PayFY=(SELECT NAME  
                        FROM   tbl_FinancialYear  
                        WHERE  IsDeleted = 0  
                               AND NAME = @YearNAME + 1  
                               AND DomainID = @DomainID)  
        END  
      ELSE  
        BEGIN  
            SET @FY =(SELECT ID  
                      FROM   tbl_FinancialYear  
                      WHERE  IsDeleted = 0  
                             AND NAME = @YearNAME  
                             AND DomainID = @DomainID)  
            SET @FYName = @YearNAME  
            SET @PayFY =(SELECT NAME  
                         FROM   tbl_FinancialYear  
                         WHERE  IsDeleted = 0  
                                AND NAME = @YearNAME  
                                AND DomainID = @DomainID)  
        END  
  
      SELECT @EmployeeName = ( Isnull(E.EmpCodePattern, '') + E.Code + ' - ' + FirstName + ' - '  
                               + Isnull(de.NAME, '') + ' ' + Isnull(fun.NAME, '') ),  
             @TDSExemptionMonths = ISNULL(m.MapID, 1) - 1  
      FROM   tbl_EmployeeMaster E  
             LEFT JOIN tbl_Designation de  
                    ON de.ID = e.DesignationID  
             LEFT JOIN tbl_Functional fun  
                    ON fun.ID = e.FunctionalID  
             LEFT JOIN tbl_Month M  
                    ON m.ID = Month(@DateOfJoin)  
                       AND e.DOJ >= Cast('01-April-' + Cast(@FYName AS VARCHAR) AS DATE)  
      WHERE  e.ID = @EmployeeID  
  
      DECLARE @RegionID INT=(SELECT RegionID  
        FROM   tbl_EmployeeMaster  
        WHERE  id = @EmployeeID)  
      -- For selecting the Medical Allowance according to the Proof Open and Close Date  
      DECLARE @ProofCloseDate DATE = (SELECT '31-Mar-' + Cast(CASE WHEN Month(Getdate()) < 4 THEN Year(Getdate()) - 1 ELSE Year(Getdate()) END AS VARCHAR)  
         FROM   TDSConfiguration  
         WHERE  IsDeleted = 0  
                AND Code = 'TDSPCD'  
                AND RegionID = @RegionID  
                AND DomainID = @DomainID)  
      DECLARE @ProofOpenDate DATE = (SELECT VALUE  
         FROM   TDSConfiguration  
         WHERE  IsDeleted = 0  
                AND Code = 'TDSPOD'  
                AND RegionID = @RegionID  
                AND DomainID = @DomainID)  
      DECLARE @IsProofOpen BIT = CASE  
          WHEN CONVERT(DATE, Getdate()) BETWEEN @ProofOpenDate AND @ProofCloseDate THEN  
            1  
          ELSE  
            0  
        END  
  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          SET @getDate =(SELECT CONVERT(DATE, Cast(@YearNAME AS VARCHAR(4)) + '-'  
                                              + Cast(@Month AS VARCHAR(2)) + '-'  
                                              + Cast(1 AS VARCHAR(2))))  
  
          DECLARE @Declartion MONEY =(SELECT CASE  
                             WHEN( @IsProofOpen = 1 ) THEN  
                               ( Isnull(Sum(Cleared), 0) )  
                             ELSE  
                               Isnull(Sum(Declaration), 0)  
                           END  
                    FROM   HRA  
                    WHERE  EmployeeID = @EmployeeID  
                           AND startyearid = @YearNAME  
                           AND DomainID = @DomainID),  
                  @noofmonth  INT =0,  
                  @lessmonth  INT =Datediff(MONTH, @DateOfJoin, Dateadd(s, -1, Dateadd(mm, Datediff(m, 0, @getDate) + 1, 0)))--Get difference between DOJ and DATE of Passed Month and Year  
          --  To get no of months TDS has to be calculated based on DOJ  
          SET @noofmonth =CASE  
                            WHEN ( Year(@DateOfJoin) < @YearNAME  
                                   AND ( Year(@DateOfJoin) < @YearNAME - 1 ) ) THEN--Checks the Year of DOJ is lesser then passed Year.  
                              CASE  
                                WHEN ( @Month >= 4 ) THEN  
                                  ( 12 - @Month ) + 4--if DOJ's Year is Smaller..Then the no of months is calculated based on DOJ Month  
                                ELSE  
                                  4 - @Month  
                              END  
                            WHEN ( Year(@DateOfJoin) = @YearNAME  
                                    OR Year(@DateOfJoin) = @YearNAME - 1 ) THEN  
                              CASE  
                                WHEN ( @Month >= 4 ) THEN  
                                  ( 12 - @Month ) + 4  
                                ELSE  
                                  4 - @Month  
                              END  
                          END  
  
          DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @Month) + '-' + '1' + '-'  
            + CONVERT(VARCHAR(10), @YearNAME)  
          DECLARE @DataSource TABLE  
            (  
               [Value] NVARCHAR(128)  
            )  
          DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth));  
  
          --Sum of all CTC,HRA,Medical,Conveyance is taken from payroll without the Passed @Month of the Passed @year which are Greater then april  
          SELECT @EmployeeID                           EmployeeID,  
                 Sum(GrossEarning) + Sum(OtherEarning) SumofCTC,  
                 Sum(PT)                               AS SumofPT,  
                 Sum(HRA)                              SumofHRA,  
                 ( CASE  
                     WHEN( @IsProofOpen = 1 ) THEN  
                       ( Isnull(Sum(TDS.Cleared), 0) )  
                     ELSE  
                       Isnull(Sum(MedicalAllowance), 0)  
                   END )                               AS SumofMedicalAllowance,  
                 Sum(Conveyance)                       SumofConveyance,  
                 Sum(Basic)                            AS SumofBasic  
          INTO   #ProcessedRecords  
          FROM   tbl_EmployeePayroll e  
                 LEFT JOIN TDSDeclaration TDS  
                        ON TDS.EmployeeID = @EmployeeID  
                           AND ComponentID = (SELECT ID  
                                              FROM   TDSComponent  
                                           WHERE  SectionID = (SELECT ID  
                                                                  FROM   TDSSection  
                                                                  WHERE  Rtrim(Ltrim(code)) = Rtrim(Ltrim('Medical Reimbursement'))  
                                                                         AND IsDeleted = 0)  
                                                     --AND DomainID = @DomainID)  
                                                     AND DomainID = @DomainID  
                                                     AND FYID = @FY)  
                           AND FinancialYearID = @FY  
          WHERE  e.EmployeeId = @EmployeeID  
                 AND MonthId > 3  
                 AND MonthId <> @Month  
                 AND YearId = @YearNAME  
                 AND e.IsDeleted = 0  
                 AND IsProcessed = 1  
                 AND e.DomainID = @DomainID  
  
          --Sum of all CTC,HRA,Medical,Conveyance is taken from payroll without the Passed @Month of the Passed @year which are less then april  
          INSERT INTO #ProcessedRecords  
          SELECT @EmployeeID                           EmployeeID,  
                 Sum(GrossEarning) + Sum(OtherEarning) SumofCTC,  
                 Sum(PT)                               AS SumofPT,  
                 Sum(HRA)                              SumofHRA,  
                 CASE  
                   WHEN( @IsProofOpen = 1 ) THEN  
                     ( Isnull(Sum(TDS.Cleared), 0) )  
                   ELSE  
                     Isnull(Sum(MedicalAllowance), 0)  
                 END                                   SumofMedicalAllowance,  
                 Sum(Conveyance)                       SumofConveyance,  
                 Sum(Basic)                            AS SumofBasic  
          FROM   tbl_EmployeePayroll e  
                 LEFT JOIN TDSDeclaration TDS  
                        ON TDS.EmployeeID = @EmployeeID  
                           AND ComponentID = (SELECT ID  
                                              FROM   TDSComponent  
                                              WHERE  SectionID = (SELECT ID  
                                                                  FROM   TDSSection  
                                                                  WHERE  Rtrim(Ltrim(code)) = Rtrim(Ltrim('Medical Reimbursement'))  
                                                                         AND IsDeleted = 0)  
                                                     --AND DomainID = @DomainID)  
                                                     AND DomainID = @DomainID  
                                                     AND FYID = @FY)  
          WHERE  e.EmployeeId = @EmployeeID  
                 AND MonthId <= 3  
                 AND MonthId <> @Month  
                 AND YearId = @YearNAME + 1  
                 AND e.IsDeleted = 0  
                 AND IsProcessed = 1  
                 AND e.DomainID = @DomainID  
  
          --Sum TDS Amount Paid tillNow  
          DECLARE @TDSPaid MONEY= (SELECT Isnull(Sum(TDSAmount), 0)  
             FROM   tbl_TDS  
             WHERE  EmployeeId = @EmployeeID  
                    AND MonthId > 3  
                    AND YearId = @FY  
                    AND MonthId <> @Month  
                    AND IsDeleted = 0  
                    AND DomainID = @DomainID)  
            + (SELECT Isnull(Sum(TDSAmount), 0)  
               FROM   tbl_TDS  
               WHERE  EmployeeId = @EmployeeID  
                      AND MonthId <= 3  
                      AND YearId = @FY  
                      AND MonthId <> @Month  
                      AND IsDeleted = 0  
                      AND DomainID = @DomainID)  
          DECLARE @Bonus MONEY= (SELECT Isnull(Sum(Bonus), 0)  
             FROM   tbl_TDSEmployeeOtherIncome  
             WHERE  EmployeeId = @EmployeeID  
                    AND Year = @YearNAME  
                    AND IsDeleted = 0  
 AND DomainID = @DomainID)  
          DECLARE @PLIAmount MONEY= (SELECT Isnull(Sum(PLI), 0)  
             FROM   tbl_TDSEmployeeOtherIncome  
             WHERE  EmployeeId = @EmployeeID  
                    AND Year = @YearNAME  
                    AND IsDeleted = 0  
                    AND DomainID = @DomainID)  
          DECLARE @OtherIncome MONEY= (SELECT Isnull(Sum(OtherIncome), 0)  
             FROM   tbl_TDSEmployeeOtherIncome  
             WHERE  EmployeeId = @EmployeeID  
                    AND Year = @YearNAME  
                    AND IsDeleted = 0  
                    AND DomainID = @DomainID)  
  
          SELECT @EmployeeID                                                          EmployeeID,  
                 Isnull(Sum(SumofCTC), 0)                                             SumofCTC,  
                 Isnull(Sum(SumofPT), 0)                                              SumofPT,  
                 Isnull(Sum(SumofHRA), 0)                                             SumofHRA,  
                 dbo.FnTDSComponentCheckByYear(@YearNAME, Sum(SumofMedicalAllowance)) AS SumofMedicalAllowance,  
                 dbo.FnTDSComponentCheckByYear(@YearNAME, Sum(SumofConveyance))       AS SumofConveyance,  
                 Isnull(Sum(SumofBasic), 0)                                           AS SumofBasic,  
                 0                                                                    AS SumofStandardDeduction,  
                 ( CASE  
                     WHEN @YearNAME <= 2017 THEN  
                       0  
                     ELSE  
                       @StandardDeduction / 12  
                   END )                                                              AS StandardDeduction --Standard Deduction (flat 40,000)  
          INTO   #RESULT  
          FROM   #ProcessedRecords  
  
          SELECT Row_number()  
                   OVER (  
                     PARTITION BY ep.EmployeeId  
                     ORDER BY EffectiveFrom DESC) AS ORDERNUMBER,  
                 CASE  
                   WHEN ( Datepart(MM, @DateOfJoin) = @Month  
                          AND Datepart(YYYY, @DateOfJoin) = @year ) THEN  
                     ( @Workdays - ( Datepart(D, @DateOfJoin) - 1 ) - Isnull(LopDays, 0) )  
                   ELSE  
                     @Workdays - Isnull(LopDays, 0)  
                 END                              AS Workdays,  
                 @Workdays                        AS ActualWorkdays,  
                 @DateOfJoin                      AS DOJ,  
                 ep.*  
          INTO   #TableResult  
          FROM   tbl_EmployeePayStructure ep  
                 LEFT JOIN tbl_lopdetails P  
                        ON P.EmployeeId = ep.EmployeeId  
                           AND P.Monthid = @Month  
                           AND P.year = @Year  
          WHERE  ep.EmployeeId = @EmployeeID  
                 AND ep.IsDeleted = 0  
                 AND EffectiveFrom < Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @PayFY - 2000, Dateadd(MONTH, @Month - 1, '20000101')))  
                                                               + 1, 0))  
                 AND ep.DomainID = @DomainID  
  
          SELECT @FY                                                  AS FY,  
                 @DateOfBirth                                         AS DOB,  
                 Gross                                                AS CTC,  
                 PT                                                   AS PT,  
                 HRA,  
                 dbo.FnTDSComponentCheckByYear(@YearNAME, ( CASE  
                                                              WHEN( @IsProofOpen = 1 ) THEN  
                                                                ( Isnull(( TDS.Cleared ), 0) )  
                                                              ELSE  
                                                                Isnull(( MedicalAllowance ), 0)  
                                                            END )) AS MedicalAllowance,  
                 dbo.FnTDSComponentCheckByYear(@YearNAME, Conveyance) AS Conveyance,  
                 Isnull(@Declartion, 0)                               AS Declartion,  
                 @noofmonth                                           AS NOOFMonth,  
                 Basic                                                AS Basic,  
                 @Bonus                                               AS Bonus,  
                 @PLIAmount                                           AS PLI,  
                 @OtherIncome                                         AS OtherIncome,  
                 @TDSPaid                                             AS TDSPaid,  
                 @DateOfJoin                                          AS DateOfJoin,  
                 @EmployeeName                                        AS NAME,  
                 @MonthName                                           AS MonthNames,  
                 @YearNAME                                            AS YearName,  
                 @PANNo                                               AS PanNo,  
                 Cast(@IsProofOpen AS INT)                            AS IsProof,  
                 ( CASE  
                     WHEN @YearNAME <= 2017 THEN  
                       3  
                     ELSE  
                       4  
                   END )                                              AS CessPrecentage,  
                 @TDSExemptionMonths                                  AS TDSExemptionMonths,  
                 r.*  
          FROM   #TableResult  
                 LEFT JOIN #RESULT r  
                        ON #TableResult.EmployeeId = r.EmployeeID  
                 LEFT JOIN TDSDeclaration TDS  
                        ON TDS.EmployeeID = @EmployeeID  
                           AND TDS.ComponentID = (SELECT ID  
                                                  FROM   TDSComponent  
                                                  WHERE  SectionID = (SELECT ID  
                                                                      FROM   TDSSection  
                                                                      WHERE  Rtrim(Ltrim(code)) = Rtrim(Ltrim('Medical Reimbursement'))  
                                                                             AND IsDeleted = 0)  
                                                         --AND DomainID = @DomainID)  
                                                         AND DomainID = @DomainID  
                                                         AND FYID = @FY)  
          WHERE  ORDERNUMBER = 1  
  
          DROP TABLE #Result  
  
          DROP TABLE #TableResult  
  
          DROP TABLE #ProcessedRecords  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
