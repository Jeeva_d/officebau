﻿/****************************************************************************                     
CREATED BY    : Ajith N                  
CREATED DATE  : 22 May 2017                  
MODIFIED BY   :                    
MODIFIED DATE :                    
 <summary>                    
 </summary>                                             
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[UpdateInvoice] (@ID                 INT,        
                                        @InvoiceNo          VARCHAR(50),        
                                        @CustomerID         INT,        
                                        @Description        VARCHAR(1000),        
                                        @Date               DATETIME,        
                                        @DueDate            DATETIME,        
                                        @RevenueCenterID    INT,        
                                        @Type               VARCHAR(100),        
                                        @DiscountID         INT,        
                                        @DiscountValue      MONEY,        
                                        @DiscountPercentage DECIMAL(18, 2),        
                                        @BillAddress        VARCHAR(1000),        
                                        @CurrencyRate       MONEY,        
                                        @SessionID          INT,        
                                        @DomainID           INT,        
                                        @INVOICEITEM        INVOICEITEM READONLY  ,  
                                        @FileID             UNIQUEIDENTIFIER,  
                                        @UploadFile         VARCHAR(8000)  
 
                                        )       
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          DECLARE @OUTPUT    VARCHAR(100),        
                  @InvoiceID INT ,  
                  @InvoiceHistoryID       VARCHAR(MAX) = @FileID  
       
        
          SET @InvoiceID = @ID        
          SET @FileID = ISNULL(@FileID, Newid())      
    BEGIN    
  UPDATE tbl_FileUpload    
                        SET    ReferenceTable = 'tblinvoice'    
                        WHERE  Id = @FileID        
          BEGIN        
              UPDATE tblInvoice        
              SET    CustomerID = @CustomerID,        
                     DueDate = @DueDate,        
                     Date = @Date,        
                     InvoiceNo = @InvoiceNo,        
                     Type = @Type,        
                     Description = @Description,        
                     RevenueCenterID = @RevenueCenterID,        
                     DiscountID = @DiscountID,        
                     DiscountPercentage = @DiscountPercentage,        
                     DiscountValue = @DiscountValue,        
                     BillAddress = @BillAddress,        
                     CurrencyRate = @CurrencyRate,        
                     ModifiedBy = @SessionID,        
                     ModifiedOn = Getdate()  ,  
					 HistoryID =@FileID
                          
              WHERE  ID = @ID        
                     AND DomainID = @DomainID     
       SET @InvoiceHistoryID = (SELECT HistoryID    
                                                 FROM   tblInvoice    
                                                 WHERE  ID = @ID    
                                                        AND DomainID = @DomainID)      
        
              IF ( (SELECT Count(1)        
                    FROM   @INVOICEITEM) > 0 )        
                BEGIN        
                    UPDATE tblInvoiceItem        
             SET    tblInvoiceItem.ProductID = I.ProductID,        
                           tblInvoiceItem.ItemDescription = I.ItemRemarks,        
                           tblInvoiceItem.QTY = I.QTY,        
                           tblInvoiceItem.Rate = I.Rate,        
                           tblInvoiceItem.SGST = I.SGST,        
                           tblInvoiceItem.SGSTAmount = I.SGSTAmount,        
                           tblInvoiceItem.CGST = I.CGST,        
                           tblInvoiceItem.CGSTAmount = I.CGSTAmount,        
                           tblInvoiceItem.IsDeleted = I.IsDeleted,        
                           ModifiedBy = @SessionID,        
                           ModifiedOn = Getdate(),        
                           tblInvoiceItem.IsProduct = I.IsProduct ,  
                           tblInvoiceItem.HistoryID= @InvoiceHistoryID   
                    FROM   tblInvoiceItem        
                           INNER JOIN @INVOICEITEM AS I        
                                   ON tblInvoiceItem.ID = I.ID        
                                      AND I.ID <> 0        
                END        
        
              IF ( (SELECT Count(1)        
                    FROM   @INVOICEITEM) > 0 )        
                BEGIN        
                    INSERT INTO tblInvoiceItem        
                               (InvoiceID,        
                                 ProductID,        
                                 ItemDescription,        
                                 QTY,        
                                 Rate,        
                                 SGST,        
                                 SGSTAmount,        
                                 CGST,        
                                 CGSTAmount,        
                                 DomainID,        
                                 CreatedBy,        
                                 ModifiedBy,        
                                 IsProduct,        
                                 SoItemID)        
                    SELECT @ID,        
                           ProductID,        
                           ItemRemarks,        
                           QTY,        
                           Rate,        
                           SGST,        
                           SGSTAmount,        
                           CGST,        
                           CGSTAmount,        
                           @DomainID,        
                           @SessionID,        
                           @SessionID,        
                           IsProduct,        
                           SoItemID        
                    FROM   @INVOICEITEM        
                    WHERE  ID = 0        
                           AND IsDeleted = 0        
                           AND ProductID <> 0        
                END        
          END        
        
          UPDATE tblInvoice        
          SET    tblInvoice.ReceivedAmount = (SELECT Sum(Amount)        
                                              FROM   tblInvoicePaymentMapping        
                                              WHERE  IsDeleted = 0        
                                                     AND InvoiceID = tblInvoice.ID        
                                                     AND DomainID = @DomainID),        
                 tblInvoice.StatusID = CASE        
                                         WHEN ( (SELECT Sum(Amount)        
                                                 FROM   tblInvoicePaymentMapping        
                                                 WHERE  IsDeleted = 0        
                                                        AND InvoiceID = tblInvoice.ID        
                                                        AND DomainID = @DomainID) >= ( CASE        
                                                                                        WHEN ( tblInvoice.DiscountPercentage <> 0 ) THEN        
                                (SELECT ( ISNULL(Sum(QTY * Rate) + Sum(ISNULL(SGSTAmount, 0))        
                                                                                                           + Sum(ISNULL(CGSTAmount, 0)) - ( ISNULL(( Sum(QTY * Rate) * tblInvoice.DiscountPercentage ), 0) ), 0) )        
                                                                                           FROM   tblInvoiceItem        
                                                                                           WHERE  InvoiceID = tblInvoice.ID        
                                                                                                  AND IsDeleted = 0        
                                                                                                  AND DomainID = @DomainID)        
                                                                                        ELSE        
                                                                                          (SELECT ( ISNULL(( Sum(QTY * Rate) + Sum(ISNULL(SGSTAmount, 0))        
               + Sum(ISNULL(CGSTAmount, 0)) - ISNULL(tblInvoice.DiscountValue, 0) ), 0) )        
                                                                                           FROM   tblInvoiceItem        
                                                                                           WHERE  InvoiceID = tblInvoice.ID        
                                                AND IsDeleted = 0        
                                                                                                  AND DomainID = @DomainID)        
     END ) ) THEN        
                                           (SELECT ID        
                                     FROM   tbl_CodeMaster        
                                            WHERE  [Type] = 'Close'        
                                                   AND IsDeleted = 0)        
                                         WHEN ( ISNULL((SELECT Sum(Amount)        
                                                        FROM   tblInvoicePaymentMapping        
                                                        WHERE  IsDeleted = 0        
                                                               AND DomainID = @DomainID        
                                                               AND InvoiceID = tblInvoice.ID), 0) = 0 ) THEN        
                                           (SELECT ID        
                                            FROM   tbl_CodeMaster        
                                            WHERE  [Type] = 'Open'        
                                                   AND IsDeleted = 0)        
                                         WHEN (( ISNULL((SELECT Sum(Amount)        
                                                         FROM   tblInvoicePaymentMapping        
                                                         WHERE  IsDeleted = 0        
                                                                AND DomainID = @DomainID        
                                                                AND InvoiceID = tblInvoice.ID), 0) <> ( CASE        
                                                                                                          WHEN ( tblInvoice.DiscountPercentage <> 0 ) THEN        
                                                                                                            (SELECT ( ISNULL(Sum(QTY * Rate) + Sum(ISNULL(SGSTAmount, 0))        
                                                                                                                             + Sum(ISNULL(CGSTAmount, 0)) - ( ISNULL(( Sum(QTY * Rate) * tblInvoice.DiscountPercentage ), 0) ), 0) )        
                                                                                                             FROM   tblInvoiceItem        
                                                                                                             WHERE  InvoiceID = tblInvoice.ID        
                                                                                                                    AND IsDeleted = 0        
                                                                                                                    AND DomainID = @DomainID)        
                                                                                                          ELSE        
                                                                                                            (SELECT ( ISNULL(( Sum(QTY * Rate) + Sum(ISNULL(SGSTAmount, 0))        
                                                                                                                               + Sum(ISNULL(CGSTAmount, 0)) - ISNULL(tblInvoice.DiscountValue, 0) ), 0) )        
                                                                                                             FROM   tblInvoiceItem        
                                                                                                             WHERE  InvoiceID = tblInvoice.ID        
                                                                              AND IsDeleted = 0        
                                                                                                                    AND DomainID = @DomainID)        
                                                                                                        END ) )) THEN        
(SELECT ID        
                                            FROM   tbl_CodeMaster        
                                            WHERE  [Type] = 'Partial'        
                                                   AND IsDeleted = 0)        
   END        
        
          SET @OUTPUT = (SELECT [Message]        
                         FROM   tblErrorMessage        
                         WHERE  [Type] = 'Information'        
                                AND Code = 'RCD_UPD'        
                                AND IsDeleted = 0) --'Updated Successfully'          
        
          EXEC [UpdateSOQTY]        
        
          ----Update item to Inventory        
          EXEC ManageInventory        
            @ID,        
            'UPDATE',        
            'Sales',        
            @SessionID,        
            @DomainID        
   END        
          SELECT @OUTPUT        
        
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = ERROR_MESSAGE(),        
                 @ErrSeverity = ERROR_SEVERITY()        
          RAISERROR (@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
