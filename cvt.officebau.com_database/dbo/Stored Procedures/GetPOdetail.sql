﻿/****************************************************************************                       
CREATED BY   :                    
CREATED DATE  :                    
MODIFIED BY   :                       
MODIFIED DATE  :                       
 <summary>                    
 [Gettransactiondetail] 1,1,1                    
 </summary>                                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetPOdetail] (@ID       INT,
                                     @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @sum MONEY =((SELECT ( ( Isnull(Sum(QTY * Amount), 0) ) + Isnull(Sum(CGST), 0) + Isnull(Sum(SGST), 0) )
             FROM   tblPODetails
             WHERE  POID = @ID
                    AND ISNULL(IsDeleted, 0) = 0
                    AND DomainID = @DomainID))

          SELECT ex.ID                      AS ID,
                 ex.Date                    AS BillDate,
                 ex.VendorID                AS VendorID,
                 vd.NAME                    AS VendorName,
                 vd.BillingAddress          AS BillingAddress,
                 vd.PaymentTerms            AS PaymentTerms,
                 ex.Remarks                 AS [Description],
                 ex.PONo                    AS BillNo,
                 @sum                       AS TotalAmount,
                 fu.OriginalFileName        AS [FileName],
                 Cast(fu.Id AS VARCHAR(50)) AS HistoryID,
                 ex.ModifiedOn              AS ModifiedOn,
                 ex.createdOn               AS CreatedOn,
                 EMP.FullName               AS ModifiedBy,
                 ex.Reference               AS Reference,
                 CostCenterID               AS CostCenterID
          FROM   tblPurchaseOrder ex
                 LEFT JOIN tblVendor vd
                        ON vd.ID = ex.VendorID
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = ex.ModifiedBy
                 LEFT JOIN tbl_FileUpload fu
                        ON fu.Id = ex.HistoryID
          WHERE  isnull(ex.IsDeleted, 0) = 0
                 AND ( ex.ID = @ID
                        OR @ID IS NULL )
                 AND ex.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
