﻿  
/****************************************************************************           
CREATED BY    :           
CREATED DATE  :           
MODIFIED BY   : JENNIFER S  
MODIFIED DATE : 02 Sep 2017   
 <summary>                  
         [Getbankaccountnumber] 1,1  
 </summary>                                   
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Getbankaccountnumber] (@BankID   INT,  
                                               @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION Receivable  
  
          SELECT DisplayName + ' ( ' + AccountNo + ' )' BankName  
          FROM   tblBank  
          WHERE  IsDeleted = 0  
                 AND DomainID = @DomainID  
                 AND ID = @BankID  
  
          COMMIT TRANSACTION Receivable  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION Receivable  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
