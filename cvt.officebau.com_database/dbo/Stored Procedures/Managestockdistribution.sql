﻿/****************************************************************************           
CREATED BY   : Dhanalakshmi.S          
CREATED DATE  : 28 Nov 2018          
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>           
        
 </summary>                                   
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Managestockdistribution] (@ID              INT,    
                                                 @EmployeeID      INT,    
                                                 @ProductID       INT,    
                                                 @Qty             INT,    
                                                 @CostCenterID    INT,    
                                                 @Party           VARCHAR(100),    
                                                 @IsInternal      BIT,    
                                                 @DistributedDate DATETIME,    
                                                 @Remarks         VARCHAR(1000),    
                                                 @SessionID       INT,    
                                                 @IsDeleted       BIT,    
                                                 @DomainID        INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(100) = 'Operation Failed!'    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          IF( @IsDeleted = 1 )    
            BEGIN    
                UPDATE tbl_InventoryStockDistribution    
                SET    IsDeleted = 1,    
                       ModifiedBY = @SessionID,    
                       ModifiedOn = Getdate()    
                WHERE  ID = @ID    
                       AND DomainID = @DomainID    
    
                SET @Output = 'Deleted Successfully.'    
    
                GOTO Finish    
            END    
          ELSE    
            BEGIN    
                IF( @ID = 0 )    
                  BEGIN    
                      INSERT INTO tbl_InventoryStockDistribution    
                                  (EmployeeID,    
                                   ProductID,    
                                   Qty,    
                                   CostCenterID,    
                                   Party,    
                                   IsInternal,    
                                   DistributedDate,    
                                   Remarks,    
                                   ModifiedBy,    
                                   ModifiedOn,    
                                   DomainID)    
                      VALUES      (@EmployeeID,    
                                   @ProductID,    
                                   @Qty,    
                                   @CostCenterID,    
                                   @Party,    
                                   @IsInternal,    
                                   @DistributedDate,    
                                   @Remarks,    
                                   @SessionID,    
                                   Getdate(),    
                                   @DomainID)    
    
                      SET @Output = 'Inserted Successfully'    
                  END    
                ELSE    
                  BEGIN    
                      IF( (SELECT ModifiedOn    
                           FROM   tbl_InventoryStockDistribution    
                           WHERE  ID = @ID    
                                  AND DomainID = @DomainID) <= (SELECT TOP 1 ModifiedOn    
                                                                FROM   tbl_InventoryStockAdjustMentItems    
                                                                WHERE  IsDeleted = 0    
                                                                ORDER  BY Modifiedon DESC) )    
                        BEGIN    
                            SET @OUTPUT = 'Stock Adjusted'    
                        END    
                      ELSE    
                        BEGIN    
                            UPDATE tbl_InventoryStockDistribution    
                            SET    EmployeeID = @EmployeeID,    
                                   ProductID = @ProductID,    
                                   Qty = @Qty,    
                                   CostCenterID = @CostCenterID,    
                                   Party = @Party,    
                                   IsInternal = @IsInternal,    
                                   DistributedDate = @DistributedDate,    
                                   Remarks = @Remarks,    
                                   ModifiedBY = @SessionID,    
                                   ModifiedOn = Getdate()    
                            WHERE  ID = @ID    
    
                            SET @Output = 'Updated Successfully'    
                        END    
                  END    
            END    
    
          FINISH:    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
