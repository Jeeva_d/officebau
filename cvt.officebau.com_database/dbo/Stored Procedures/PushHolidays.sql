﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
      PushHolidays 2017,1,',1,2,3,'
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[PushHolidays] (@Year           SMALLINT,
                                      @DomainID       INT,
                                      @BusinessUnitID VARCHAR(1000))
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(50)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed'

          IF NOT EXISTS (SELECT 1
                         FROM   tbl_Holidays
                         WHERE  [Year] = @Year
                                AND [Type] = 'Weekly'
                                AND DomainID = @DomainID
                                AND IsDeleted = 0)
            BEGIN
                INSERT INTO tbl_Holidays
                            ([Year],
                             [Date],
                             [Description],
                             [Day],
                             [Type],
                             [IsOptional],
                             DomainID,
                             RegionID)
                SELECT @Year                   AS [Year],
                       CONVERT(DATE, Fromdate) AS [Date],
                       Datename (DW, Fromdate) AS [Description],
                       Datename (DW, Fromdate) AS [Day],
                       'Weekly'                AS [Type],
                       0                       AS [IsOptional],
                       @DomainID               AS DomainID,
                       @BusinessUnitID         AS BusinessUnitID
                FROM   dbo.Fngetholidays(@Year)

                SET @Output = 'Inserted Successfully.'
            END
          ELSE
            SET @Output = 'Already Exists.'

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
