﻿/****************************************************************************                                     
CREATED BY    :                                   
CREATED DATE  :                                   
MODIFIED BY   :                              
MODIFIED DATE :                                     
 <summary>                                   
[Temp_pandl] 1,'04/01/2018' ,'03/31/2019'                              
 </summary>                                                             
 *****************************************************************************/                              
CREATE PROCEDURE [dbo].[nEWpandl] (@DomainID        INT,                              
                               @SearchStartDate VARCHAR(25),                              
                               @SearchEndDate   VARCHAR(25),                              
                               @CostCenterID    INT = 0)                              
AS                              
  BEGIN                              
      BEGIN TRY                              
          SET @SearchStartDate = Cast(@SearchStartDate AS DATE)                              
          SET @SearchEndDate = Cast(@SearchEndDate AS DATE)                              
                              
          ------- P & l --------------                                  
                                 
 select date, case when (ed.IsLedger=1)  Then                          
l.Name Else lp.Name End As LEDGER,                          
case when (ed.IsLedger=1)  Then                          
g.Name Else gp.Name End As GROUPLEDGER,                          
Sum(QTY*amount) As AMOUNT,                          
Case When (IsLedger=1) Then g.ReportType Else gp.ReportType End ReportTypeid , 'Expense' As CATEGORY,'' NAme,''BranchName ,                       
case when (ed.IsLedger=1)  Then                          
l.Name Else lp.Name End As ID                     
                      
INTO #Result                          
from tblExpenseDetails ed                           
Join tblExpense e on ed.ExpenseID =e.ID  and e.isdeleted =0                        
left Join tblLedger l on l.ID =ed.LedgerID and ed.IsLedger=1                          
left Join tblGroupLedger g on g.ID =l.GroupID and ed.IsLedger=1                          
left Join tblProduct_v2 p on p.ID =ed.LedgerID and ed.IsLedger=0                          
left Join tblLedger lp on lp.ID =p.PurchaseLedgerId and ed.IsLedger=0                          
left Join tblGroupLedger gp on gp.ID =lp.GroupID and ed.IsLedger=0                          
Where  ed.DomainID = @DomainID                            
                                       AND [Date] >= @SearchStartDate                            
                                       AND [Date] <= @SearchEndDate and ed.IsDeleted=0                          
group by IsLedger,l.Name,g.Name,p.Name, g.ReportType,date,gp.name,lp.Name,gp.ReportType,l.ID,lp.id                          
                          
Insert into #Result                          
Select Date,lp.Name,gp.Name,Sum(qty*it.rate),gp.ReportType,'income','','',lp.Name from tblInvoiceItem it                          
Join tblInvoice i on it.InvoiceID = i.id                          
Join tblProduct_v2 p on it.ProductID = p.ID                          
left Join tblLedger lp on lp.ID =p.SalesLedgerId                           
left Join tblGroupLedger gp on gp.ID =lp.GroupID                           
Where  it.DomainID = @DomainID                            
                                       AND [Date] >= @SearchStartDate                            
                                       AND [Date] <= @SearchEndDate and it.IsDeleted=0                          
Group by date,p.Name,lp.Name,gp.Name, gp.ReportType ,lp.Id                         
Insert into #Result                          
Select PaymentDate,l.Name,g.Name,TotalAmount,g.ReportType,                          
'Income' ,'','',l.Name from tbl_Receipts r                          Join tblLedger l on r.LedgerID=l.ID                          
Join tblGroupLedger g on g.ID=l.GroupID              
 Where  r.DomainID = @DomainID                            
                                       AND PaymentDate >= @SearchStartDate                            
                                       AND PaymentDate <= @SearchEndDate and r.IsDeleted=0             
Union All         
Select PaymentDate,l.Name,g.Name,TotalAmount,g.ReportType,                          
'Expense' ,'','',l.Name from tbl_Payment r                          
Join tblLedger l on r.LedgerID=l.ID                          
Join tblGroupLedger g on g.ID=l.GroupID                          
 Where  r.DomainID = @DomainID                            
                                       AND PaymentDate >= @SearchStartDate                            
                                       AND PaymentDate <= @SearchEndDate and r.IsDeleted=0          
Select Date,LEDGER,GROUPLEDGER,case when(ReportTypeid=4) Then Case When (CATEGORY='Income') Then amount*-1 Else AMOUNT END                          
 Else Case When (CATEGORY='Income') Then amount Else AMOUNT*-1 END END  AMOUNT,Case When (ReportTypeid=4)Then 'EXPENSE' Else'INCOME' END AS CATEGORY,BranchName,NAme                     
 ,ID,'Ledger' As [type]                    
 from #Result where ReportTypeid IN (3,4)                          
Union All               
select [journalDate],L.nAME , G.nAME,case when (G.REPORTtYPE=4 ) THEN DEBIT+ (CREDIT*-1) when (G.REPORTtYPE=3 ) THEN (DEBIT *-1)+ (CREDIT)  end               
,case when (G.REPORTtYPE=4 ) THEN 'Expense' when (G.REPORTtYPE=3 ) THEN 'Income' end ,'','',l.Name,'Ledger'              
from tbljournal J              
jOIN TBLLEDGER L ON J.lEDGERpRODUCTID =L.iD   And type='Ledger'          
JOIN TBLGROUPLEDGER G ON G.id = L.gROUPid ANd G.REPORTtYPE IN (3,4)              
 Where  j.DomainID = @DomainID                            
   AND [journalDate] >= @SearchStartDate                            
   AND [journalDate] <= @SearchEndDate and j.IsDeleted=0   
Union All               
select [journalDate],L.nAME , G.nAME,case when (G.REPORTtYPE=4 ) THEN DEBIT+ (CREDIT*-1) when (G.REPORTtYPE=3 ) THEN (DEBIT *-1)+ (CREDIT)  end               
,case when (G.REPORTtYPE=4 ) THEN 'Expense' when (G.REPORTtYPE=3 ) THEN 'Income' end ,'','',l.Name,'Ledger'              
from tbljournal J   
Join tblProduct_v2 p on j.LedgerProductID =p.ID and type='Product'          
jOIN TBLLEDGER L ON ISnull(p.PurchaseLedgerId,p.SalesLedgerId) =L.iD             
JOIN TBLGROUPLEDGER G ON G.id = L.gROUPid ANd G.REPORTtYPE IN (3,4)              
 Where  j.DomainID = @DomainID                            
   AND [journalDate] >= @SearchStartDate                            
   AND [journalDate] <= @SearchEndDate and j.IsDeleted=0              
Union All      
select Date,L.nAME , G.nAME,case when (G.REPORTtYPE=4 ) THEN Amount when (G.REPORTtYPE=3 ) THEN  Amount *-1  end               
,case when (G.REPORTtYPE=4 ) THEN 'Expense' when (G.REPORTtYPE=3 ) THEN 'Income' end ,'','',l.Name,'Ledger'              
from tblInvoiceHoldings J              
jOIN TBLLEDGER L ON J.LedgerId =L.iD             
JOIN TBLGROUPLEDGER G ON G.id = L.gROUPid ANd G.REPORTtYPE IN (3,4)              
 Where  j.DomainID = @DomainID    AND Date >= @SearchStartDate                            
   AND Date <= @SearchEndDate and j.IsDeleted=0                             
 and j.IsDeleted=0   
 Union All      
select Date,L.nAME , G.nAME,case when (G.REPORTtYPE=4 ) THEN Amount when (G.REPORTtYPE=3 ) THEN  Amount *-1  end               
,case when (G.REPORTtYPE=4 ) THEN 'Expense' when (G.REPORTtYPE=3 ) THEN 'Income' end ,'','',l.Name,'Ledger'              
from tblExpenseHoldings J              
jOIN TBLLEDGER L ON J.LedgerId =L.iD             
JOIN TBLGROUPLEDGER G ON G.id = L.gROUPid ANd G.REPORTtYPE IN (3,4)              
 Where  j.DomainID = @DomainID    AND Date >= @SearchStartDate                            
   AND Date <= @SearchEndDate and j.IsDeleted=0                             
 and j.IsDeleted=0                         
drop table #Result                            
                              
      END TRY                              
      BEGIN CATCH                            
          ROLLBACK TRANSACTION                              
          DECLARE @ErrorMsg    VARCHAR(100),                              
                 @ErrSeverity TINYINT                              
          SELECT @ErrorMsg = Error_message(),                              
                 @ErrSeverity = Error_severity()                              
          RAISERROR(@ErrorMsg,@ErrSeverity,1)                              
      END CATCH                              
  END
