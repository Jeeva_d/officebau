﻿    
    
/****************************************************************************       
CREATED BY  : JENNIFER.S    
CREATED DATE : 29-Jul-2017    
MODIFIED BY  : Ajith N    
MODIFIED DATE : 18 Jan 2018    
 <summary>    
  [ManageLoanSettlement]    
 </summary>                               
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ManageLoanSettlement] (@ID                INT,    
                                              @LoanID            INT,    
                                              @Amount            MONEY,    
                                              @DateofRealization DATE,    
                                              @PaymentModeID     INT,    
                                              @Reference         VARCHAR(50),    
                                              @Description       VARCHAR(500),    
                                              @SessionID         INT,    
                                              @DomainID          INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          DECLARE @Ouput    VARCHAR(100),    
                  @Tenure   INT,    
                  @ROI      DECIMAL(5, 2),    
                  @Interest MONEY,    
      @DueDate date = CAST(( CONVERT(VARCHAR(10), MONTH(@DateofRealization)) + '-'    
                  + CONVERT(VARCHAR(10), '1')    
                  + '-' + CONVERT(VARCHAR(10), YEAR(@DateofRealization)))  AS DATE)    
    
          INSERT INTO tbl_LoanSettle    
                      (LoanRequestID,    
                       SettlerID,    
                       Amount,    
                       DateOfRealization,    
                       PaymentModeID,    
                       ReferenceNo,    
                       [Description],    
                       DomainID,    
                       CreatedBy,  
                       CreatedOn,    
                       ModifiedBy,  
                       ModifiedOn)    
          VALUES      ( @LoanID,    
                        @SessionID,    
                        @Amount,    
                        @DateofRealization,    
                        @PaymentModeID,    
                        @Reference,    
                        @Description,    
                        @DomainID,    
                        @SessionID,  
                        GetDate(),    
                        @SessionID,  
                        GetDate() )    
    
          UPDATE tbl_LoanRequest    
          SET    StatusID = (SELECT ID    
                             FROM   tbl_Status    
                             WHERE  [Type] = 'Loan'    
                                    AND Code = 'Settled')    
          WHERE  ID = @LoanID    
    
          SELECT @Tenure = Tenure,    
                 @ROI = InterestRate    
          FROM   tbl_LoanRequest    
          WHERE  ID = @LoanID    
    
          SET @Interest = ( @Amount * @ROI * @Tenure ) / ( 12 * 100 );    
    
          WITH CTE    
               AS (SELECT 1                                     ID,    
                          Dateadd(MONTH, 1, @DueDate) DueDate    
                   UNION ALL    
                   SELECT ( ID + 1 )ID,    
                          Dateadd(MONTH, 1, DueDate)    
                   FROM   CTE    
                   WHERE  ID < @Tenure)    
          INSERT INTO tbl_LoanAmortization    
                      (LoanRequestID,    
                       Amount,    
                       InterestAmount,    
                       DueDate,    
                       StatusID,    
                       DomainID,    
        PaidAmount,    
        IsBalance,    
        DueBalance,    
                       CreatedBy,    
                       ModifiedBy)    
          SELECT @LoanID,    
                 ( @Amount / @Tenure ),    
                 ROUND(ISNULL(( @Interest / @Tenure ), 0), 0),    
                 DueDate,    
                 (SELECT ID    
                  FROM   tbl_Status    
                  WHERE  [Type] = 'Amortization'    
                         AND Code = 'Open'),    
                 @DomainID,    
     0,    
     0,    
     0,    
                 @SessionID,    
                 @SessionID    
          FROM   CTE    
    
          SET @Ouput = 'Loan Settled Successfully.'    
    
     SELECT @Ouput    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
   SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
