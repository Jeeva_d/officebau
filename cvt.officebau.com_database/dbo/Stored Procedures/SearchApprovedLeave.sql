﻿/****************************************************************************   
CREATED BY		: Ajith N
CREATED DATE	: 29 Mar 2018 
MODIFIED BY		:  
MODIFIED DATE	: 
 <summary>          
		SearchApprovedLeave 0,0,71,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchApprovedLeave] (@StatusID    INT,
                                             @LeaveTypeID INT,
                                             @UserID      INT,
                                             @DomainID    INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @BusinessUnitIDs VARCHAR(50)

          SELECT @BusinessUnitIDs = BusinessUnitID
          FROM   tbl_EmployeeMaster EM
          WHERE  ID = @UserID

          --SELECT DISTINCT ParentID
          --INTO   #tempRegion
          --FROM   tbl_BusinessUnit bu
          --WHERE  ParentID <> 0
          --       AND ID IN (SELECT item
          --                  FROM   SplitString(@BusinessUnitIDs, ','))
          SELECT EL.ID,
                 EL.RequesterRemarks                 AS Remarks,
                 EL.Duration                         AS Duration,
                 EL.RequestedDate,
                 (SELECT Count(1)
                  FROM   tbl_EmployeeLeaveDateMapping ELM
                  WHERE  ELM.LeaveRequestID = el.ID) AS NoOfDays,
                 ISNULL(EM.code, '') + ' - '
                 + ISNULL(EM.FullName, '')           AS EmployeeName,
                 lt.NAME                             AS LeaveType,
                 ISNULL(HRS.Code, 'Pending')         AS [Status],
                 EL.ApprovedDate,
                 ISNULL(app.code, '') + ' - '
                 + ISNULL(app.FullName, '')          AS Approver,
                 ISNULL(EL.IsHalfDay, 'false')       AS IsHalfDay,
                 EL.FromDate,
                 EL.ToDate
          --,El.HRApproverID
          --,el.HRStatusID
          --,el.ApproverID, el.ApproverStatusID
          FROM   tbl_EmployeeLeave El
                 JOIN tbl_EmployeeMaster EM
                   ON el.EmployeeID = em.ID
                 LEFT JOIN tbl_LeaveTypes LT
                        ON LT.ID = EL.LeaveTypeID
                 LEFT JOIN tbl_Status HRS
                        ON HRS.ID = EL.HRStatusID
                 LEFT JOIN tbl_EmployeeMaster App
                        ON app.ID = El.ApproverID
          WHERE  El.isdeleted = 0
                 AND em.BaseLocationID IN (SELECT item
                                           FROM   SplitString(@BusinessUnitIDs, ','))
                 AND ( @LeaveTypeID = 0
                        OR EL.LeaveTypeID = @LeaveTypeID )
                 AND ( EL.DomainID = @DomainID )
                 AND ( @StatusID = 0
                        OR ( EL.HRStatusID = @StatusID
                              OR ( EL.HRStatusID IS NULL
                                   AND @StatusID = (SELECT id
                                                    FROM   tbl_Status
                                                    WHERE  IsDeleted = 0
                                                           AND Type = 'Leave'
                                                           AND Code = 'Pending'
                                                           AND IsDeleted = 0) ) ) )
                 AND EL.ApproverStatusID <> (SELECT id
                                             FROM   tbl_Status
                                             WHERE  IsDeleted = 0
                                                    AND Type = 'Leave'
                                                    AND Code = 'rejected'
                                                    AND IsDeleted = 0)
          ORDER  BY el.RequestedDate DESC
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
