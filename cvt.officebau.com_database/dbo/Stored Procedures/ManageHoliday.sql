﻿/****************************************************************************       
CREATED BY  : JENNIFER.S    
CREATED DATE : 30-Jun-2017    
MODIFIED BY  :     
MODIFIED DATE :     
 <summary>    
  [ManageHoliday] 0,'10-SEP-2017','Weekly','er',0,',,1,2,',0,1,1    
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageHoliday] (@ID          INT,  
                                        @HolidayDate DATE,  
                                        @HolidayType VARCHAR(10),  
                                        @Description VARCHAR(8000),  
                                        @IsOptional  BIT,  
                                        @RegionIDs   VARCHAR(1000),  
                                        @IsDeleted   BIT,  
                                        @DomainID    INT,  
                                        @SessionID   INT,  
                                        @Year        VARCHAR(20))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          DECLARE @Ouput VARCHAR(100)  
  
          IF @IsDeleted = 1  
            BEGIN  
                IF ( (SELECT Count(1)  
                      FROM   tbl_EmployeeLeaveDateMapping ELM  
                             LEFT JOIN tbl_EmployeeMaster EM  
                                    ON EM.ID = ELM.EmployeeID  
                      WHERE  ELM.LeaveDate >= @HolidayDate  
                             AND Month(ELM.LeaveDate) = Month(@HolidayDate)  
                             AND ELM.IsDeleted = 0  
                             AND ELM.DomainID = @DomainID  
                             AND @RegionIDs LIKE '%' + Cast(EM.RegionID AS VARCHAR) + '%') > 0 )  
                  BEGIN  
                      SET @Ouput= 'Record referred.'  
                  END  
                ELSE  
                  BEGIN  
                      UPDATE tbl_Holidays  
                      SET    IsDeleted = 1,  
                             ModifiedBy = @SessionID,  
                             ModifiedOn = Getdate()  
                      WHERE  ID = @ID  
  
                      SET @Ouput= 'Deleted Successfully.'  
                  END  
            END  
          ELSE  
            BEGIN  
                IF NOT EXISTS (SELECT 1  
                               FROM   tbl_Holidays  
                               WHERE  [Date] = @HolidayDate  
                                      AND ID <> Isnull(@ID, 0)  
                                      AND IsDeleted = 0  
                                      AND RegionID LIKE '%,' + @RegionIDs + ',%'  
                                      AND [Type] = 'Weekly')  
                  BEGIN  
                      IF( @HolidayType = 'Weekly' )  
                        BEGIN  
                            IF Isnull(@ID, 0) = 0  
                              BEGIN  
                                  INSERT INTO tbl_Holidays  
                                              ([Year],  
                                               [Date],  
                                               [Day],  
                                               [Type],  
                                               [Description],  
                                               IsOptional,  
                                               RegionID,  
                                               DomainID,  
                                               CreatedBy,  
                                               CreatedOn,  
                                               ModifiedBy,  
                                               ModifiedOn)  
                                  VALUES      ( Year(@HolidayDate),  
                                                @HolidayDate,  
                                                Datename(dw, @HolidayDate),  
                                                @HolidayType,  
                                                @Description,  
                                           @IsOptional,  
                                                @RegionIDs,  
                                                @DomainID,  
                                                @SessionID,  
                                                Getdate(),  
                                                @SessionID,  
                                                Getdate() )  
  
                                  SET @Ouput= 'Inserted Successfully.'  
                              END  
                            ELSE  
                              BEGIN  
                                  UPDATE tbl_Holidays  
                                  SET    [Description] = @Description,  
                                         IsOptional = @IsOptional,  
                                         RegionID = @RegionIDs,  
                                         ModifiedBy = @SessionID,  
                                         ModifiedOn = Getdate()  
                                  WHERE  ID = @ID  
  
                                  SET @Ouput= 'Updated Successfully.'  
                              END  
                        END  
                  END  
                ELSE  
                  SET @Ouput= 'Already Exists.'  
  
                IF( @HolidayType = 'Yearly' )  
                  BEGIN  
                      IF NOT EXISTS (SELECT 1  
                                     FROM   tbl_Holidays  
                                     WHERE  [Date] = @HolidayDate  
                                            AND ID <> Isnull(@ID, 0)  
                                            AND IsDeleted = 0  
                                            AND RegionID LIKE '' + @RegionIDs + ''  
                                            AND [Type] = 'Yearly')  
                        BEGIN  
                            IF Isnull(@ID, 0) = 0  
                              BEGIN  
                                  INSERT INTO tbl_Holidays  
                                              ([Year],  
                                               [Date],  
                                               [Day],  
                                               [Type],  
                                               [Description],  
                                               IsOptional,  
                                               RegionID,  
                                               DomainID,  
                                               CreatedBy,  
                                               CreatedOn,  
                                               ModifiedBy,  
                                               ModifiedOn)  
                                  VALUES      ( Year(@HolidayDate),  
                                                @HolidayDate,  
                                                Datename(dw, @HolidayDate),  
                                                @HolidayType,  
                                                @Description,  
                                                @IsOptional,  
                                                @RegionIDs,  
                                                @DomainID,  
                                                @SessionID,  
                                                Getdate(),  
                                                @SessionID,  
                                                Getdate() )  
  
                                  SET @Ouput= 'Inserted Successfully.'  
                              END  
                            ELSE IF ( (SELECT Count(1)  
                                  FROM   tbl_Holidays  
                                  WHERE  [Date] = @HolidayDate  
                                         AND ID <> Isnull(@ID, 0)  
                                         AND IsDeleted = 0  
                                         AND RegionID LIKE '' + @RegionIDs + '') = 0 )  
                              BEGIN  
                               UPDATE tbl_Holidays  
                                  SET    [Description] = @Description,  
                                         IsOptional = @IsOptional,  
                                         RegionID = @RegionIDs,  
                                         ModifiedBy = @SessionID,  
                                         ModifiedOn = Getdate()  
                                  WHERE  ID = @ID  
  
                                  SET @Ouput= 'Updated Successfully.'  
                              END  
                            ELSE  
                              SET @Ouput= 'Already Exists.'  
                        END  
                      ELSE  
                        SET @Ouput= 'Already Exists.'  
                  END  
            END  
  
          SELECT @Ouput  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
