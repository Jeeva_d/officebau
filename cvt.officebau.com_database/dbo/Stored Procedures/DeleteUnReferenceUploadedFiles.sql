﻿/****************************************************************************   
CREATED BY   : Ajith 
CREATED DATE  : 24 May 2018
MODIFIED BY   : 
MODIFIED DATE  :
<summary>          
     DeleteUnReferenceUploadedFiles ''
</summary>                           
*****************************************************************************/
CREATE PROCEDURE DeleteUnReferenceUploadedFiles(@UserName VARCHAR(100))
AS
  BEGIN
      BEGIN TRY
          BEGIN TRANSACTION

          CREATE TABLE #temp
            (
               FileID          UNIQUEIDENTIFIER,
               ReferenceTable  VARCHAR(100),
               ReferenceFileID VARCHAR(100)
            )

          INSERT INTO #temp
          SELECT FU.Id AS FileID,
                 FU.ReferenceTable,
                 ''    AS ReferenceFileID
          FROM   tbl_FileUpload FU
          WHERE  FU.ReferenceTable IS NULL
          UNION
          SELECT FU.Id                            AS FileID,
                 FU.ReferenceTable,
                 Cast(E.HistoryID AS VARCHAR(50)) AS ReferenceFileID
          FROM   tbl_FileUpload FU
                 LEFT JOIN tblExpense E
                        ON FU.Id = E.HistoryID
                           AND FU.ReferenceTable = 'tblExpense'
          WHERE  E.HistoryID IS NULL
                 AND FU.ReferenceTable = 'tblExpense'
          UNION
          SELECT FU.Id                          AS FileID,
                 FU.ReferenceTable,
                 Cast(EM.FileID AS VARCHAR(50)) AS ReferenceFileID
          FROM   tbl_FileUpload FU
                 LEFT JOIN tbl_EmployeeMaster EM
                        ON FU.Id = EM.FileID
                           AND FU.ReferenceTable = 'tbl_EmployeeMaster'
          WHERE  EM.FileID IS NULL
                 AND FU.ReferenceTable = 'tbl_EmployeeMaster'
          UNION
          SELECT FU.Id                              AS FileID,
                 FU.ReferenceTable,
                 Cast(CR.FileUpload AS VARCHAR(50)) AS ReferenceFileID
          FROM   tbl_FileUpload FU
                 LEFT JOIN tbl_ClaimsRequest CR
                        ON FU.Id = CR.FileUpload
                           AND FU.ReferenceTable = 'tbl_ClaimsRequest'
          WHERE  CR.FileUpload IS NULL
                 AND FU.ReferenceTable = 'tbl_ClaimsRequest'
          UNION
          SELECT FU.Id                          AS FileID,
                 FU.ReferenceTable,
                 Cast(ED.FileID AS VARCHAR(50)) AS ReferenceFileID
          FROM   tbl_FileUpload FU
                 LEFT JOIN tbl_EmployeeDocuments ED
                        ON FU.Id = ED.FileID
                           AND FU.ReferenceTable = 'tbl_EmployeeDocuments'
          WHERE  ED.FileID IS NULL
                 AND FU.ReferenceTable = 'tbl_EmployeeDocuments'
          UNION
          SELECT FU.Id                          AS FileID,
                 FU.ReferenceTable,
                 Cast(ED.FileID AS VARCHAR(50)) AS ReferenceFileID
          FROM   tbl_FileUpload FU
                 LEFT JOIN tbl_Form16 ED
                        ON FU.Id = ED.FileID
                           AND FU.ReferenceTable = 'tbl_Form16'
          WHERE  ED.FileID IS NULL
                 AND FU.ReferenceTable = 'tbl_Form16'

          DELETE FROM tbl_FileUpload
          WHERE  Id IN (SELECT FileID
                        FROM   #temp)

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
