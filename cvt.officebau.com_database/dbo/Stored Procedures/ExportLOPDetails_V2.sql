﻿/**************************************************************************** 
CREATED BY			:	Ajith N
CREATED DATE		:	16 Dec 2018
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>       
    ExportLOPDetails_V2 7,3,'1,2,4,3,5,6,7,8,9',1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ExportLOPDetails_V2] (@MonthId  INT,
                                             @Year     INT,
                                             @Location VARCHAR(100),
                                             @DomainID INT)
AS
  BEGIN
      BEGIN TRY
          SET @Year = (SELECT NAME
                       FROM   tbl_FinancialYear
                       WHERE  id = @Year
                              AND DomainID = @DomainID)

          DECLARE @CustomDate DATETIME = Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @year - 2000, Dateadd(MONTH, @monthID - 1, '20000101')))
                                      + 1, 0))
          DECLARE @DataSource TABLE
            (
               [Value] NVARCHAR(128)
            )

          INSERT INTO @DataSource
                      ([Value])
          SELECT Item
          FROM   dbo.Splitstring (@Location, ',')
          WHERE  Isnull(Item, '') <> ''

          SELECT DISTINCT EM.Code               AS [EmpCode],
                          Isnull(LD.LopDays, 0) AS [LopDays],
                          em.DOJ                AS [JoiningDate],
                          EM.FullName           AS [Name],
                          DEP.NAME              AS DepartmentName,
                          BU.NAME               AS BusinessUnit
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_LopDetails LD
                        ON LD.EmployeeId = EM.ID
                           AND LD.Monthid = @MonthId
                           AND LD.year = @Year
                 LEFT JOIN tbl_Department DEP
                        ON DEP.ID = EM.DepartmentID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaselocationID
                 JOIN tbl_Pay_EmployeePayStructure ep
                   ON ep.EmployeeId = EM.ID
          WHERE  EM.domainID = @DomainID
                 AND DOJ <= @CustomDate
                 AND ( ( @Location = Cast(0 AS VARCHAR) )
                        OR EM.BaseLocationID IN (SELECT [Value]
                                                 FROM   @DataSource) )
                 AND ( EM.BaseLocationID <> 0
                        OR EM.BaseLocationID <> '' )
                 AND EM.EmploymentTypeID = (SELECT ID
                                            FROM   tbl_EmployementType
                                            WHERE  NAME = 'Direct'
                                                   AND DomainID = @DomainID)
                 AND EM.IsDeleted = 0
                 AND Isnull(EM.IsActive, '') = 0
                 AND EM.HasAccess = 1
                 AND ep.EffectiveFrom < Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @Year - 2000, Dateadd(MONTH, @MonthId - 1, '20000101')))
                                                               + 1, 0))
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
