﻿/****************************************************************************               
CREATED BY   :               
CREATED DATE  :            
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>            
[SearchExpenseHoldingList] 1,1         
 </summary>                                       
 *****************************************************************************/            
CREATE PROCEDURE [dbo].[SearchExpenseHoldingList] (@ExpenseID INT,            
                                                @DomainID  INT)            
AS            
  BEGIN            
      SET NOCOUNT ON;            
            
      BEGIN TRY            
          BEGIN TRANSACTION            
            
         Select l.Name As LedgerName, i.BillNo, (SELECT ( Isnull(( Sum(Qty * Amount) +Sum(Isnull(SGST,0)+Isnull(CGST,0)) ), 0) )          
                            FROM   tblExpenseDetails          
                            WHERE  ExpenseId = i.ID          
                            AND IsDeleted = 0 and DomainID =  @domainID) AS ExpenseAmount,  (SELECT ( Isnull(( Sum(Qty * Amount) +Sum(Isnull(SGST,0)+Isnull(CGST,0)) ), 0) )          
                            FROM   tblExpenseDetails          
                            WHERE  ExpenseId = i.ID          
                            AND IsDeleted = 0 and DomainID =  @domainID)-(Select ISNULL(Sum(ISNULL(amount,0)),0)        
         from tblExpensePaymentMapping where ExpenseID=@ExpenseID and IsDeleted=0) As OutstandingAmount,i.ID AS ExpenseID,h.ID,h.date,h.LedgerID,h.Amount from  tblExpense i         
        left join tblExpenseHoldings h on i.ID=h.ExpenseId and h.ISdeleted=0     
  Left Join tblLedger l on l.ID=h.LedgerId     
    Where   i.ID=@ExpenseID and i.IsDeleted=0        
          COMMIT TRANSACTION            
      END TRY            
      BEGIN CATCH            
          ROLLBACK TRANSACTION            
          DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT            
          SELECT @ErrorMsg = Error_message(),            
                 @ErrSeverity = Error_severity()            
          RAISERROR(@ErrorMsg,@ErrSeverity,1)            
      END CATCH            
  END
