﻿/****************************************************************************     
CREATED BY   : Ajith    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 select * from tblMasterTypes  
   
 select * from tblbrs  
 [SearchBRS] null,0,2  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getsearchbrs] (@BankID       INT,
                                       @IsReconsiled BIT,
                                       @DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT b.NAME              AS [Bank Name],
                 mt.NAME             AS [Type],
                 CASE mt.NAME
                   WHEN 'PayBill' THEN bk.Amount
                   WHEN 'FromBank' THEN bk.Amount
                   WHEN 'Expense' THEN bk.Amount
                   WHEN 'Make Payment' THEN bk.Amount
                   ELSE ''
                 END                 AS Debit,
                 CASE mt.NAME
                   WHEN 'ToBank' THEN bk.Amount
                   WHEN 'InvoiceReceivable' THEN bk.Amount
                   WHEN 'Receive Payment' THEN bk.Amount
                   ELSE ''
                 END                 AS Credit,
                 bk.AvailableBalance AS ClosingAmount,
                 bk.SourceDate       AS [Booked Date],
                 bk.ReconsiledDate   AS [Reconsile Date]
          FROM   tblBRS bk
                 LEFT JOIN tblBank b
                        ON bk.BankID = b.ID
                 LEFT JOIN tblMasterTypes mt
                        ON bk.SourceType = mt.ID
          WHERE  bk.IsDeleted = 0
                 AND bk.IsReconsiled = @IsReconsiled
                 AND ( bk.BankID = @BankID
                        OR @BankID IS NULL )
                 AND bk.DomainID = @DomainID
          ORDER  BY bk.SourceDate ASC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 



