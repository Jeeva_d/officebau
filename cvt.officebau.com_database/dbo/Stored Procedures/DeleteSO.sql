﻿/****************************************************************************             
CREATED BY    :           
CREATED DATE  :          
MODIFIED BY   :           
MODIFIED DATE :           
 <summary>            
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteSO] (@ID        INT,
                                  @SessionID INT,
                                  @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT VARCHAR(100)

          IF EXISTS (SELECT 1
                     FROM   tblSOItem
                     WHERE  invoicedQTY <> 0
                            AND SOID = @ID
                            AND IsDeleted = 0
                            AND DomainID = @DomainID)
            BEGIN
                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Warning'
                                      AND Code = 'RCD_REF'
                                      AND IsDeleted = 0) --'The record is referred.'          
                GOTO finish
            END
          ELSE
            BEGIN
                UPDATE tblSalesOrder
                SET    IsDeleted = 1
                WHERE  @ID = ID

                UPDATE tblSOItem
                SET    IsDeleted = 1
                WHERE  SOID = @ID

                SET @Output = 'Deleted Successfully'

                ----Delete item to Inventory
                EXEC ManageInventory
                  @ID,
                  'DELETE',
                  'SO',
                  @SessionID,
                  @DomainID

                GOTO finish
            END

          FINISH:

          EXEC [UpdateSOQTY]

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
