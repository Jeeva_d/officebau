﻿  
/****************************************************************************       
CREATED BY  :   Sasirekha   
CREATED DATE :   15 Nov 2017    
MODIFIED BY  :        
MODIFIED DATE :     
 <summary>    
 [SearchMissedPunches] 106,1, '2017-09-14'    
    
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchMissedPunches] (@EmployeeID INT,  
                                             @DomainID   INT,  
                                             @LogDate    DATETIME)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @LeaveStatus INT = (SELECT ID  
             FROM   tbl_Status  
             WHERE  [Type] = 'Leave'  
                    AND Code = 'Approved')  
  
          SELECT CONVERT(VARCHAR(5), EMP.PunchTime, 108) AS PunchTime,  
                 CM.Code                                 AS [Description]  
          FROM   tbl_EmployeeMissedPunches EMP  
                 LEFT JOIN tbl_CodeMaster CM  
                        ON CM.ID = EMP.PunchTypeID  
          WHERE  EMP.PunchDate = @LogDate  
                 AND EMP.EmployeeID = @EmployeeID  
                 AND StatusID = @LeaveStatus  
                 AND EMP.DomainID = @DomainID  
          ORDER  BY EMP.PunchTime  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
