﻿/****************************************************************************     
CREATED BY      : Naneeshwar.M    
CREATED DATE  : 05-SEP-2017    
MODIFIED BY   : Ajith N    
MODIFIED DATE  : 06 Dec 2017    
<summary>      
 [SearchAutocompleteEmployeeBasedonBU] 106 ,'muk',1,106, ''    
</summary>                             
*****************************************************************************/    
CREATE PROCEDURE [dbo].[SearchAutocompleteEmployeeBasedonBU] (@EmployeeID      INT,    
                                                             @SearchParameter VARCHAR(50),    
                                                             @DomainID        INT,    
                                                             @UserID          INT,    
                                                             @Type            VARCHAR(50))    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN    
              IF( @Type = 'ALL' ) -- All Employees     
                BEGIN    
                    SELECT TOP 50 ID                      AS ID,    
                               ISNULL(EmpCodePattern, '')+ Code + ' - ' + FullName AS NAME    
                    FROM   tbl_EmployeeMaster    
                    WHERE  BaseLocationID IN (SELECT Item    
                                              FROM   dbo.Splitstring ((SELECT BusinessUnitID    
                                                                       FROM   tbl_EmployeeMaster    
                                                                       WHERE  id = @UserID    
                                                                              AND DomainID = @DomainID), ',')    
                                              WHERE  Isnull(Item, '') <> '')    
                           AND IsDeleted = 0    
                           AND HasAccess = 1    
                           AND DomainID = @DomainID    
                           AND id <> @EmployeeID    
                           AND FullName LIKE '%' + @SearchParameter + '%'    
                           AND ISNULL(Code, '') NOT LIKE 'TMP_%'    
                END    
              ELSE    
                BEGIN    
                    SELECT TOP 50 ID                      AS ID,    
                                   ISNULL(EmpCodePattern, '')+ Code + ' - ' + FullName AS NAME,    
                                  ReportingToID           AS ReportingID    
                    FROM   tbl_EmployeeMaster    
                    WHERE  BaseLocationID IN (SELECT Item    
                                              FROM   dbo.Splitstring ((SELECT BusinessUnitID    
                                                                       FROM   tbl_EmployeeMaster    
                                                                       WHERE  id = @UserID    
                                                                              AND DomainID = @DomainID), ',')    
                                              WHERE  Isnull(Item, '') <> '')    
                           AND IsDeleted = 0    
                           --AND ReasonID IS NULL    
                           AND IsActive = 0    
                           AND HasAccess = 1    
                           AND DomainID = @DomainID    
                           AND id <> @EmployeeID    
                           AND FullName LIKE '%' + @SearchParameter + '%'    
                           AND ISNULL(Code, '') NOT LIKE 'TMP_%'    
                END    
          END    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR (@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
