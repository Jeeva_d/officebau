﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :           
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>        
 </summary>                                   
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[Pay_GetCompanyPayStructure] (@ID       INT        
                                     )        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          SELECT   
               tpcps.ID,   tpcps.PaystructureName AS Name, tpcps.EffectiveFrom  
          FROM     
   tbl_Pay_CompanyPayStructure AS tpcps WHERE tpcps.ID=@ID  
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
