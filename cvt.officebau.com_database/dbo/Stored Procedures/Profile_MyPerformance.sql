﻿/****************************************************************************         
CREATED BY  :   Naneeshwar.M      
CREATED DATE :   11-Jan-2018      
MODIFIED BY  :         
MODIFIED DATE :         
 <summary>      
 [Profile_MyPerformance] 53 ,14      
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Profile_MyPerformance] (@UserID   INT,      
                                               @DomainID INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          DECLARE @yearID    INT=(SELECT ID      
                    FROM   tbl_financialYear      
                    WHERE  NAME = ( Year(Getdate()) )      
                           AND isdeleted = 0      
                           AND DomainID = @DomainID),      
                  @MonthID   INT=Month(Getdate()),      
                  @IsProfile BIT=1      
  IF @yearID IS Not Null    
          EXEC [Monthlyattedancereport]      
            @MonthID,      
            @yearID,      
            '',      
            @DomainID,      
            @IsProfile    
Else   
truncate table tbl_LopDetails_Process    
      
          SELECT 'Total Hours Clocked'                             AS EmployeeName,      
                 Sum(Cast(Datediff(Minute, 0, t.Duration) AS INT)) AS Duration      
          INTO   #duration      
          FROM   tbl_attendance t      
                 LEFT JOIN tbl_employeemaster e      
                        ON e.id = t.employeeid      
                           AND t.DomainID = e.DomainID      
          WHERE  t.employeeid = @UserID      
                 AND Month(logdate) = Month(Getdate())      
                 AND Year(logdate) = Year(Getdate())      
                 AND e.isactive = 0      
                 AND e.isdeleted = 0      
                 AND t.domainID = @domainID      
      
          SELECT EmployeeName,      
                 Cast( Duration /60 AS VARCHAR) + ':'      
                 + Cast( (Duration % 60 ) AS VARCHAR) AS Duration      
          FROM   #duration      
          UNION ALL      
          SELECT 'Lop days of this Month'                                                      AS EmployeeName,      
                 Cast(Cast(( Max(Isnull(lp.Absent, 0))      
                             + Max(Isnull(lp.Halfday, 0)/2.0) ) AS DECIMAL(18, 1)) AS VARCHAR) AS Duration      
          FROM   (SELECT CASE      
                           WHEN( ishalfday = 1 ) THEN      
                             0.5      
                           WHEN ( ishalfday = 0 ) THEN      
                             1      
                           ELSE      
                             0      
                         END                            AS lop,      
                         Isnull(el.EmployeeID, @UserID) AS ID      
                  FROM   tbl_employeeleave el      
                         LEFT JOIN tbl_employeeleavedatemapping em      
                                ON em.leaverequestid = el.id      
                  WHERE  ( @UserID IS NULL      
                            OR el.employeeid = @UserID )      
                         AND Month(em.LeaveDate) = Month(Getdate())      
                         AND Year(em.LeaveDate) = Year(Getdate())      
                         AND el.isdeleted = 0      
                         AND EL.DomainID = @DomainID      
                         AND el.statusid IN (SELECT ID      
                                             FROM   tbl_status      
                                             WHERE  type = 'Leave'      
                                                    AND code NOT IN ( 'Approved' ))      
                  UNION ALL      
                  SELECT 0,      
                         @userID) a      
                 LEFT JOIN tbl_LopDetails_Process lp      
                        ON lp.employeeid = a.ID      
      END TRY      
      BEGIN CATCH      
          DECLARE @ErrorMsg   VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)          END CATCH      
  END
