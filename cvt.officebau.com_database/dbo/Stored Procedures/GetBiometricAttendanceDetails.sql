﻿/****************************************************************************   
CREATED BY		: Anto
CREATED DATE	:  25-SEP-2017
MODIFIED BY		: Ajith N
MODIFIED DATE	: 06 Oct 2017
 <summary>          
     [GetBiometricAttendanceDetails] 7,3,1463,1

	 To be Rework based on Employee ID

 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetBiometricAttendanceDetails] (@MonthID  INT,
                                                       @Year     INT,
                                                       @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @YearCode VARCHAR(20),
                  @query    VARCHAR(MAX) = ''

          SET @YearCode = (SELECT FinancialYear
                           FROM   tbl_FinancialYear
                           WHERE  ID = @Year
                                  AND IsDeleted = 0
                                  AND DomainID = @DomainID)
          SET @query = ( 'SELECT EmployeeID,
				AttendanceDate,
				InTime,
				OutTime,
				Duration,
				PunchRecords
			FROM   eSSLSmartOffice.dbo.AttendanceLogs_'
                         + CONVERT(VARCHAR, @MonthID) + '_'
                         + CONVERT(VARCHAR, @YearCode) + '' )

          EXEC (@query)
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
