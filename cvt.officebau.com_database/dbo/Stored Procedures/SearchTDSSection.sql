﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>     
		select * from TDSSection
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchTDSSection] (@Code     VARCHAR(20),
                                          @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT sec.ID                             ID,
                 sec.Code                           Code,
                 sec.[Description]                  [Description],
                 emp.FirstName + ' ' + emp.LastName ModifiedBy,
                 sec.ModifiedOn                     ModifiedOn
          FROM   TDSSection sec
                 JOIN tbl_EmployeeMaster emp
                   ON emp.ID = sec.ModifiedBy
          WHERE  sec.IsDeleted = 0
                 --AND sec.DomainID = @DomainID
                 AND sec.Code LIKE '%' + Isnull(@Code, '') + '%'
          ORDER  BY sec.Code
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
