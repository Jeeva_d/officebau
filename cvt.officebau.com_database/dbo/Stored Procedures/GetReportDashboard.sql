﻿
/****************************************************************************           
CREATED BY  :  DhanaLakshmi. S          
CREATED DATE : 16-MAR-2018        
MODIFIED BY  :        
MODIFIED DATE :           
 <summary>                  
    [GetReportDashboard] 'HeadCottunt', 'Department' ,0, 4
 </summary>                                   
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetReportDashboard] ( @ReportType VARCHAR(100),
                                             @GroupBy   VARCHAR(100),
											 @IsActive BIT,
		                                     @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          Declare @BusinessUnit varchar(MAX),@BusinessUnitHeader varchar(MAX),@SumBusinessUnitHeader varchar(MAX),
                  @SQL NVARCHAr(MAX)

          SET @BusinessUnit = STUFF((SELECT DISTINCT ',[' + Name  + ']' 
		                             FROM tbl_BusinessUnit 
									 WHERE IsDeleted = 0 
									       AND ParentID <> 0 
									       AND DomainID = @DomainID FOR XML PATH('')),1,1,'')

          SET @BusinessUnitHeader = STUFF((SELECT DISTINCT ',ISNULL([' + Name  + '],0) AS [' + Name  + ']'
		                             FROM tbl_BusinessUnit 
									 WHERE IsDeleted = 0 
									       AND ParentID <> 0 
									       AND DomainID = @DomainID FOR XML PATH('')),1,1,'')
			   SET @SumBusinessUnitHeader = STUFF((SELECT DISTINCT ',SUM([' + Name  + '])'
		                             FROM tbl_BusinessUnit 
									 WHERE IsDeleted = 0 
									       AND ParentID <> 0 
									       AND DomainID = @DomainID FOR XML PATH('')),1,1,'')							              
 IF (@ReportType = 'Headcount')
	 	 	  BEGIN

				SET @SQL = 'SELECT  Name,'+ @BusinessUnitHeader + ',Total INTO #TempTable FROM 
						(
						SELECT B.Name AS BusinessUnit,
								D.Name AS Name, 
								Count(D.Name) Count,
								Sum(Count(D.Name)) OVER (PARTITION BY D.Name) AS Total
						FROM tbl_EmployeeMaster EM
								LEFT JOIN tbl_'+ @GroupBy +' D
								      ON D.ID = EM.'+ @GroupBy +'ID and D.Isdeleted = 0
								LEFT JOIN tbl_BusinessUnit B
								      ON B.ID = EM.BaseLocationID
						WHERE EM.IsDeleted = 0 
								AND IsActive = ' + CONVERT(VARCHAR(10), @IsActive) + ' 
								AND D.Name is not null
								AND EM.DomainID = ' + CONVERT(VARCHAR(10), @DomainID) + '
						Group by D.Name,
								 B.Name
						) As result
						Pivot( MAX(Count) for BusinessUnit in (' + @BusinessUnit + ')) As spivot
						
						select * from #TempTable
						union all
						 select ''Total'','+@SumBusinessUnitHeader+',SUM(Total) from #TempTable'

                  PRINT(@SQL)
                  EXEC (@SQL)
              END

ELSE
				BEGIN

				SET @SQL = 'SELECT Name,'+ @BusinessUnitHeader + ',Total INTO #TempTable  FROM 
							(
							SELECT B.Name AS BusinessUnit,
							       D.Name AS Name, 
								   Sum(PS.CTC) Count,
								   Sum(Sum(PS.CTC)) OVER (PARTITION BY D.Name) AS Total
							FROM tbl_EmployeeMaster EM
								 LEFT JOIN tbl_EmployeePayStructure PS
								      ON PS.EmployeeId = EM.ID
								 LEFT JOIN tbl_' + @GroupBy +' D
								      ON D.ID = EM.' + @GroupBy +'ID and D.Isdeleted = 0
								 LEFT JOIN tbl_BusinessUnit B
								      ON B.ID = EM.BaseLocationID
							where EM.IsDeleted = 0 
							      AND IsActive = ' + CONVERT(VARCHAR(10), @IsActive) +'
							      AND D.Name is not null
							      AND EM.DomainID = ' + CONVERT(VARCHAR(10), @DomainID) + '
							      AND PS.DomainID = ' + CONVERT(VARCHAR(10), @DomainID) + '
							      AND PS.IsDeleted = 0
							Group by D.Name,
									 B.Name
							) As result
							Pivot( MAX(Count) for BusinessUnit in (' + @BusinessUnit + ')) As spivot
							select * from #TempTable
						   union all
						    select ''Total'','+@SumBusinessUnitHeader+',SUM(Total) from #TempTable'
                  PRINT(@SQL)
				      EXEC (@SQL)
				END

      

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
