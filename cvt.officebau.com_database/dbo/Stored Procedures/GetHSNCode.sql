﻿/****************************************************************************               
CREATED BY   :               
CREATED DATE  :               
MODIFIED BY   :            
MODIFIED DATE  :              
 <summary>            
 [GetHSNCode] 29,'Ledger'   
        
 </summary>                                       
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[GetHSNCode] (@ID   INT,    
                                    @Type VARCHAR(10))    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          IF ( @Type = 'Product' )    
            BEGIN    
                SELECT ISNULL(g.ID, 0)    AS ID,    
                       ISNULL(g.Code, '') AS Code,    
                       ISNULL(SGST, 0)    AS SGST,    
                       ISNULL(CGST, 0)    AS CGST,  
        ISnull(p.SalesPrice,0)  AS SalesPrice,  
        ISNULL(p.PurchasePrice,0) AS PurchasePrice  
                FROM   tblProduct_V2 p    
                       LEFT JOIN tblGstHSN g    
                              ON p.HSNId = g.ID    
                WHERE  p.ID = @ID    
            END    
          ELSE    
            BEGIN    
                SELECT ISNULL(GstHSNID, 0) AS Id,    
                       ISNULL(g.Code, '')  AS Code,    
                       ISNULL(SGST, 0)     AS SGST,    
                       ISNULL(CGST, 0)     AS CGST  ,  
     Cast( 0 AS Decimal) AS SalesPrice,  
        Cast( 0 AS Decimal) AS PurchasePrice  
                FROM   tblLedger p    
                       LEFT JOIN tblGstHSN g    
                              ON p.GstHSNID = g.ID    
                WHERE  p.ID = @ID    
            END    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
