﻿/****************************************************************************               
CREATED BY   :            
CREATED DATE  :               
MODIFIED BY   :             
MODIFIED DATE  :           
<summary>            
[Rpt_daybookbyBank_temp] 1,NULL,NULL,1      
</summary>                                       
*****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_daybookbyBank_temp] (@BankID    INT,
                                                @StartDate DATETIME = NULL,
                                                @EndDate   DATETIME = NULL,
                                                @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @FinancialYear INT;
          DECLARE @StartFinancialMonth INT;
          DECLARE @EndFinancialMonth INT;

          IF ( @StartDate IS NULL
                OR @StartDate = '' )
              OR ( @EndDate IS NULL
                    OR @EndDate = '' )
            BEGIN
                SET @StartFinancialMonth = (SELECT Value
                                            FROM   tblApplicationConfiguration
                                            WHERE  Code = 'STARTMTH'
                                                   AND DomainID = @DomainID)
                SET @EndFinancialMonth = ( ( (SELECT Value
                                              FROM   tblApplicationConfiguration
                                              WHERE  Code = 'STARTMTH'
                                                     AND DomainID = @DomainID)
                                             + 11 ) % 12 )

                IF( Month(Getdate()) <= ( ( (SELECT Value
                                             FROM   tblApplicationConfiguration
                                             WHERE  Code = 'STARTMTH'
                                                    AND DomainID = @DomainID)
                                            + 11 ) % 12 ) )
                  SET @FinancialYear = Year(Getdate()) - 1
                ELSE
                  SET @FinancialYear = Year(Getdate())

                SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date        
                SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date       
            END;

          WITH CTE
               AS (SELECT DISTINCT ip.ID                                     AS ID,
                                   ip.PaymentDate                            [Date],
                                   cd.NAME                                   Party,
                                   (SELECT Sum(AMOUNT)
                                    FROM   tblInvoicePaymentMapping ipm
                                    WHERE  ipm.isdeleted = 0
                                           AND ipm.DomainID = @DomainID
                                           AND ipm.InvoicePaymentID = ip.ID) Receipts,
                                   NULL                                      Payments,
                                   ip.CreatedOn                              modifiedOn,
                                   ip.BankID,                                
                                   mt.NAME                                   AS [Type],
                                   brs.ID                                    AS BRSID
                   FROM   tblInvoice i
                          JOIN tblInvoicePaymentMapping im
                            ON im.InvoiceID = i.ID
                               AND im.IsDeleted = 0
                          LEFT JOIN tblInvoicePayment ip
                                 ON ip.ID = im.InvoicePaymentID
                                    AND ip.IsDeleted = 0
   LEFT JOIN tblCustomer cd
                                 ON cd.ID = i.CustomerID
                          JOIN tblBRS brs
                            ON brs.SourceID = im.ID
                             
                          LEFT JOIN tblMasterTypes mt
                                 ON mt.ID = brs.SourceType
                   WHERE  i.IsDeleted = 0
                          AND im.IsDeleted = 0
						    AND brs.SourceType IN (SELECT id
                                                      FROM   tblMasterTypes
                                                      WHERE  NAME IN( 'InvoiceReceivable' ))
                          AND i.DomainID = @DomainID
                          AND ( ip.BankID = @BankID
                                 OR '' = Isnull(@BankID, '') )
                          AND ( @StartDate = ''
                                 OR i.Date >= @StartDate )
                          AND ( @EndDate = ''
                                 OR i.Date <= @EndDate )
                   UNION ALL
                   SELECT DISTINCT e.ID        AS ID,
                                   e.[Date],
                                   vd.NAME     Party,
                                   NULL        Receipts,
                                   ( CASE
                                       WHEN mt.NAME = 'PayBill' THEN
                                         (SELECT Sum(AMOUNT)
                                          FROM   tblExpensePaymentMapping epm
                                          WHERE  epm.isdeleted = 0
                                                 AND epm.DomainID = @DomainID
                                                 AND epm.ExpensePaymentID = ep.ID)
                                       ELSE
                                         em.Amount
                                     END )     Payments,
                                   e.CreatedOn modifiedOn,
                                   ep.BankID,
                                   --(SELECT NAME
                                   -- FROM   tblMasterTypes
                                   -- WHERE  NAME = ( 'Expense' )
                                   --        AND Code = 'EXP') [Type]
                                   mt.NAME     AS [Type],
                                   brs.ID      AS BRSID
                   FROM   tblExpense e
                          JOIN tblExpensePaymentMapping em
                            ON em.ExpenseID = e.ID
                               AND em.IsDeleted = 0
                          LEFT JOIN tblExpensePayment ep
                                 ON ep.ID = em.ExpensePaymentID
                                    AND ep.IsDeleted = 0
                          LEFT JOIN tblVendor vd
                                 ON vd.ID = e.VendorID
                          JOIN tblBRS brs
                            ON brs.SourceID = em.ID                              
                          LEFT JOIN tblMasterTypes mt
                                 ON mt.ID = brs.SourceType
                   WHERE   e.IsDeleted = 0
                          --AND e.Type = 'Expense'
						   AND brs.SourceType IN (SELECT id
                                                      FROM   tblMasterTypes
                                                      WHERE  NAME IN( 'Expense', 'PayBill' ))
                          AND e.DomainID = @DomainID
                          AND ( ep.BankID = @BankID
                                 OR '' = Isnull(@BankID, '') )
                          AND ( @StartDate = ''
                                 OR e.Date >= @StartDate )
                          AND ( @EndDate = ''
                                 OR e.Date <= @EndDate )
                   UNION ALL
                   --      SELECT DISTINCT ep.ID                                     AS ID,
                   --                      ep.[Date],
   --                      vd.NAME                                   Party,
                   --                      NULL                                      Receipts,
                   --                      (SELECT Sum(AMOUNT)
                   --                       FROM   tblExpensePaymentMapping epm
                   --                       WHERE  epm.isdeleted = 0
                   --                              AND epm.DomainID = @DomainID
                   --                              AND epm.ExpensePaymentID = ep.ID) Payments,
                   --                      ep.CreatedOn                              modifiedOn,
                   --                      ep.BankID,
                   --                      (SELECT NAME
                   --                       FROM   tblMasterTypes
                   --                       WHERE  NAME = 'PayBill'
                   --                              AND CODE = 'BILL')                [Type]
                   --,brs.ID AS BRSID
                   --      FROM   tblExpense e
                   --             JOIN tblExpensePaymentMapping em
                   --                    ON em.ExpenseID = e.ID
                   --                       AND em.IsDeleted = 0
                   --             LEFT JOIN tblExpensePayment ep
                   --                    ON ep.ID = em.ExpensePaymentID
                   --                       AND ep.IsDeleted = 0
                   --             LEFT JOIN tblVendor vd
                   --                    ON vd.ID = e.VendorID
                   --             JOIN tblBRS brs
                   --                    ON brs.SourceID = em.ID
                   --      WHERE  ep.PaymentModeID <> (SELECT ID
                   --                                  FROM   tbl_CodeMaster
                   --                                  WHERE  Code = 'Cash'
                   --                                         AND [Type] = 'PaymentType'
                   --                                         AND IsDeleted = 0)
                   --             AND e.Type <> 'Expense'
                   --             AND e.IsDeleted = 0
                   --             AND e.DomainID = @DomainID
                   --             AND ( ep.BankID = @BankID
                   --                    OR '' = Isnull(@BankID, '') )
                   --             AND ( @StartDate = ''
                   --                    OR e.Date >= @StartDate )
                   --             AND ( @EndDate = ''
                   --                    OR e.Date <= @EndDate )
                   --      UNION ALL
                   SELECT DISTINCT r.ID                            AS ID,
                                   r.[Date],
                                   PartyName                       Party,
                                   ( CASE
                                       WHEN mt.NAME = 'Receive Payment' THEN
                                         r.Amount 
                                       ELSE
                                         NULL
                                     END )                         Receipts,
                                   ( CASE
                                       WHEN mt.NAME = 'Make Payment' THEN
                                         r.Amount 
                                       ELSE
                                         NULL
                                     END )                             Payments,
                                   r.CreatedOn                     modifiedOn,
                                   r.BankID,
                                   mt.NAME AS  [Type],
                                   brs.ID                          AS BRSID
                   FROM   tblReceipts r
                          JOIN tblBRS brs
                            ON brs.SourceID = r.ID
							
                          LEFT JOIN tblMasterTypes mt
                                 ON mt.ID = brs.SourceType
                   WHERE   r.IsDeleted = 0
						 AND brs.SourceType IN (SELECT id
                                                      FROM   tblMasterTypes
                                                      WHERE  NAME IN( 'Receive Payment', 'Make Payment' ))
                          --AND brs.SourceType = (SELECT id
                          --                      FROM   tblMasterTypes
                          --                      WHERE  NAME = 'Receive Payment')
                          AND r.DomainID = @DomainID
                          AND ( r.BankID = @BankID
                                 OR '' = Isnull(@BankID, '') )
                          AND ( @StartDate = ''
                                 OR r.Date >= @StartDate )
                          AND ( @EndDate = ''
                                 OR r.Date <= @EndDate )
                   UNION ALL
                   --SELECT DISTINCT r.ID                            AS ID,
                   --                r.[Date],
                   --                PartyName                       Party,
                   --                NULL                            Receipts,
                   --                ( CASE
                   --                    WHEN ( TransactionType = (SELECT ID
                   --                                              FROM   tblMasterTypes
                   --                                              WHERE  NAME = 'Make Payment') ) THEN
                   --                      ( r.Amount )
                   --                    ELSE
                   --                      NULL
                   --                  END )                         Payments,
                   --                r.CreatedOn                     modifiedOn,
                   --                r.BankID,
                   --                (SELECT NAME
                   --                 FROM   tblMasterTypes
                   --                 WHERE  NAME = 'Make Payment'
                   --                        AND CODE = 'TransType') [Type],
                   --                brs.ID                          AS BRSID
                   --FROM   tblReceipts r
                   --       JOIN tblBRS brs
                   --         ON brs.SourceID = r.ID
                   --WHERE  r.PaymentMode <> (SELECT ID
                   --                         FROM   tbl_CodeMaster
                   --                         WHERE  Code = 'Cash'
                   --                                AND [Type] = 'PaymentType'
                   --                                AND IsDeleted = 0)
                   --       AND r.IsDeleted = 0
                   --       AND r.DomainID = @DomainID
                   --       AND brs.SourceType = (SELECT id
                   --                             FROM   tblMasterTypes
                   --                             WHERE  NAME = 'Make Payment')
                   --       AND ( r.BankID = @BankID
                   --              OR '' = Isnull(@BankID, '') )
                   --       AND ( @StartDate = ''
                   --              OR r.Date >= @StartDate )
                   --       AND ( @EndDate = ''
                   --              OR r.Date <= @EndDate )
                   --UNION ALL
                   SELECT DISTINCT t.ID                      AS ID,
                                   t.VoucherDate             [Date],
                                   b.NAME                    Party,
                                   ( CASE
                                       WHEN ( ToID <> 0 ) THEN
                                         ( t.Amount )
                                       ELSE
                                         NULL
       END )                   Receipts,
                                   NULL                      Payments,
                                   t.CreatedOn               modifiedOn,
                                   t.ToID                    BankID,
                                   mt.Name AS  [Type],
                                   brs.ID                    AS BRSID
                   FROM   tblTransfer t
                          LEFT JOIN tblBank b
                                 ON b.ID = t.ToID
                          JOIN tblBRS brs
                            ON brs.SourceID = t.ID
							LEFT JOIN tblMasterTypes mt
                                 ON mt.ID = brs.SourceType
                   WHERE  t.IsDeleted = 0
                          AND t.DomainID = @DomainID
                          AND brs.SourceType = (SELECT id
                                                FROM   tblMasterTypes
                                                WHERE  NAME = 'ToBank')
                          AND ( t.ToID = @BankID
                                 OR '' = Isnull(@BankID, '') )
                          AND ( @StartDate = ''
                                 OR t.VoucherDate >= @StartDate )
                          AND ( @EndDate = ''
                                 OR t.VoucherDate <= @EndDate )
                   UNION ALL
                   SELECT DISTINCT t.ID                      AS ID,
                                   t.VoucherDate             [Date],
                                   b.NAME                    Party,
                                   NULL                      Receipts,
                                   ( CASE
                                       WHEN ( FromID <> 0 ) THEN
                                         ( t.Amount )
                                       ELSE
                                         NULL
                                     END )                   Payments,
                                   t.CreatedOn               modifiedOn,
                                   t.FromID                  BankID,
                                   mt.Name AS  [Type],
                                   brs.ID                    AS BRSID
                   FROM   tblTransfer t
                          LEFT JOIN tblBank b
                                 ON b.ID = t.FromID
                          JOIN tblBRS brs
                            ON brs.SourceID = t.ID
							LEFT JOIN tblMasterTypes mt
                                 ON mt.ID = brs.SourceType
                   WHERE  t.IsDeleted = 0
                          AND t.DomainID = @DomainID
                          AND brs.SourceType = (SELECT id
                                                FROM   tblMasterTypes
                                                WHERE  NAME = 'FromBank')
                          AND ( t.FromID = @BankID
                                 OR '' = Isnull(@BankID, '') )
                          AND ( @StartDate = ''
                                 OR t.VoucherDate >= @StartDate )
                          AND ( @EndDate = ''
                                 OR t.VoucherDate <= @EndDate ))
          SELECT ID,
                 [Date],
                 Party,
                 Receipts,
                 Payments,
                 CASE
                   WHEN Receipts IS NULL THEN
                     -1 * Payments
                   ELSE
                     Receipts
                 END                     Amount,
                 CONVERT (DECIMAL, 0.00) ClosingBalance,
                 modifiedOn,
                 BankID,
                 [Type],
                 Row_number()
                   OVER(
                     PARTITION BY BankID
                     ORDER BY [Date])    RowOrder
          INTO   #TempResult
          FROM   CTE
          WHERE  [Date] >= @StartDate
                 AND [Date] <= @EndDate

          UPDATE rst
          SET    ClosingBalance = rst.Amount
          FROM   #TempResult rst
          WHERE  RowOrder = 1

          UPDATE rst
          SET    ClosingBalance = (SELECT Sum([AMOUNT])
                                   FROM   #TempResult t
                                   WHERE  rst.BankID = t.BankID
                                          AND t.RowOrder <= rst.RowOrder)
          FROM   #TempResult rst

          SELECT ID,
                 Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-') [Date],
                 Party,
                 Receipts,
                 Payments,
                 ClosingBalance                                       AS [Closing Balance],
                 [Type]
          --  Replace(CONVERT(VARCHAR(20), modifiedOn, 113), ' ', '-') modifiedOn      
          FROM   #TempResult
          UNION ALL
          SELECT NULL,
                 NULL,
                 'Total Amount',
                 Sum(Receipts),
                 Sum(Payments),
                 NULL,--Sum(Receipts) - Sum(Payments),
                 NULL
          --   NULL      
          FROM   #TempResult
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
