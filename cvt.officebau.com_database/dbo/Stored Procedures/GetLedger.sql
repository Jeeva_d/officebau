﻿/****************************************************************************             
CREATED BY   : Naneeshwar            
CREATED DATE  :             
MODIFIED BY   :             
MODIFIED DATE  :             
 <summary>          
 [GetLedger] 58,1          
 </summary>                                     
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[GetLedger] (@ID       INT,        
                                    @DomainID INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          SELECT bk.ID          AS ID,        
                 GroupID        AS GroupID,        
                 NAME           AS NAME,        
                 Remarks        AS Remarks,        
                 OpeningBalance AS OpeningBalance,        
                 IsExpense      AS IsExpense,        
                 bk.ModifiedOn  AS ModifiedOn,        
                 EMP.FullName  AS ModifiedBy,      
                 bk.LedgerType As LedgerType,    
                 bk.GstHSNID AS HSNID,    
                 GH.Code   AS HsnCode,      
                 GH.SGST,      
                 GH.CGST,  
                 GH.IGST ,
				 bk.ISdefault  
          FROM   tblLedger bk        
                 LEFT JOIN tbl_EmployeeMaster EMP        
                        ON EMP.ID = bk.ModifiedBy      
                 LEFT JOIN tblGstHSN GH    
                        ON bk.GstHSNID = GH.ID    
          WHERE  bk.IsDeleted = 0        
                 AND bk.ID = @ID        
                 AND bk.DomainID = @DomainID        
        
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
