﻿/****************************************************************************     
CREATED BY   :  Naneeshwar.M  
CREATED DATE  :     
MODIFIED BY   :   Ajith N  
MODIFIED DATE  :   07 Dec 2017  
 <summary>   
        [Getclaimfinancialapprovallist] 1,9,2,3,'2018-10-09',0  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getclaimfinancialapprovallist] (@DomainID            INT,
                                                       @FinancialApproverID INT,
                                                       @ApproverID          INT,
                                                       @RequesterID         INT,
                                                       @SubmittedDate       DATE,
                                                       @StatusID            INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT CFA.ID                      AS ID,
                 CLR.ID                      AS RequesterItemID,
                 CLR.ClaimDate               AS ClaimedDate,
                 Cast(CLA.CreatedOn AS DATE) AS ApprovedDate,
                 CLC.NAME                    AS CategoryName,
                 CLA.ApprovedAmount          AS ApprovalAmount,
                 CLR.CreatedBy               AS RequesterID,
                 CLA.CreatedBy               AS ApprovalID,
                 STA.Code                    AS Status,
                 FU.[FileName]               AS [FileName],
                 STF.Code                    AS FinApproverStatus,
                 EM.FullName                 AS ModifiedBy,
                 CFA.ModifiedOn              AS ModifiedOn,
                 BU.NAME                     AS BaseLocation,
                 CLR.DestinationID           AS DestinationID,
                 CLR.CategoryID              AS CategoryID,
                 ( CASE
                     WHEN CFA.StatusID = 0 THEN
                       0
                     ELSE
                       ISNULL(CFA.FinApprovedAmount, 0)
                   END )                     AS Amount,
                 DC.NAME                     AS DestinationCity,
                 ( CASE
                     WHEN ISNULL(DC.IsMetro, 0) = 1 THEN
                       CP.MetroAmount
                     ELSE
                       CP.NonMetroAmount
                   END )                     AS TotalAmount,
                 CLR.FileUpload              AS FileUploadID,
                 ISNULL(TR.ID, 0)            AS TravelReqID,
                 CLR.TravelReqNo             AS TravelReqNo
          FROM   tbl_ClaimsRequest CLR
                 LEFT JOIN tbl_ClaimsApprove CLA
                        ON CLR.ID = CLA.ClaimItemID
                 LEFT JOIN tbl_ClaimsFinancialApprove CFA
                        ON CFA.ClaimItemID = CLR.id
                 LEFT JOIN tbl_ClaimCategory CLC
                        ON CLC.ID = CLR.CategoryID
                 LEFT JOIN tbl_Status STA
                        ON STA.ID = CFA.StatusID
                 LEFT JOIN tbl_FileUpload FU
                        ON FU.Id = CLR.FileUpload
                 LEFT JOIN tbl_Status STF
                        ON STF.ID = CLA.FinApproverStatusID
                 LEFT JOIN tbl_EmployeeMaster EM
                        ON EM.ID = CFA.ModifiedBy
                 LEFT JOIN tbl_BusinessUnit BU
                        ON EM.BaseLocationID = BU.ID
                 LEFT JOIN tbl_DestinationCities DC
                        ON DC.ID = CLR.DestinationID
                 LEFT JOIN tbl_EmployeeMaster EMR
                        ON EMR.ID = CLR.ModifiedBy
                 LEFT JOIN tbl_EmployeeDesignationMapping DM
                        ON dm.DesignationID = EMR.DesignationID
                           AND DM.IsDeleted = 0
                 LEFT JOIN tbl_ClaimPolicy CP
                        ON CP.DesignationMappingID = DM.ID
                           AND CP.ExpenseType = CLR.CategoryID
                           AND CP.IsDeleted = 0
                 LEFT JOIN tbl_EmployeeTravelRequest TR
                        ON TR.TravelReqNo = CLR.TravelReqNo
                           AND TR.IsDeleted = 0
          WHERE  CLA.CreatedOn IS NOT NULL
                 AND CLR.IsDeleted = 0
                 AND CLA.DomainID = @DomainID
                 AND CLA.CreatedBy = @ApproverID
                 AND CLA.FinancialApproverID = @FinancialApproverID
                 AND CLA.RequesterID = @RequesterID
                 AND Cast(CLA.CreatedOn AS DATE) = @SubmittedDate
                 AND ( ( Isnull(@StatusID, 0) = 0
                         AND CLA.FinApproverStatusID = (SELECT ID
                                                        FROM   tbl_Status
                                                        WHERE  Code = ( 'Submitted' )
                                                               AND Type = 'Claims'
                                                               AND IsDeleted = 0) )
                        OR CFA.StatusID = @StatusID )
          ORDER  BY CLA.ModifiedOn DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
