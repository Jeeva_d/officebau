﻿/****************************************************************************         
CREATED BY  : Naneeshwar        
CREATED DATE : 22-Nov-2016      
MODIFIED BY  : JENNIFER.S       
MODIFIED DATE : 21-DEC-2016  
<summary>      
	[Searchexpensepaidbyvendor] 28,'',1,'Apr 1 2017','May 20 2017'  
</summary>                                 
*****************************************************************************/
CREATE PROCEDURE [dbo].[Searchexpensepaidbyvendor] (@VendorID  INT,
                                                   @Status    VARCHAR(100),
                                                   @DomainID  INT,
                                                   @StartDate VARCHAR(50),
                                                   @EndDate   VARCHAR(50))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @StartDate != '' )
            SET @StartDate = Cast(@StartDate AS DATETIME)

          IF( @EndDate != '' )
            SET @EndDate = Cast(@EndDate AS DATETIME)

          IF( @Status = 'All' )
            SET @Status='';

          WITH expensePayment
               AS (SELECT empm.ExpensePaymentID AS PayId,
                          --empm.ExpenseID        AS ExpenseID,
                          Sum(empm.Amount)      AS PaidAmount,
                          e.StatusID            AS Status,
                          v.NAME                AS VendorName
                   FROM   tblExpensePaymentMapping empm
                          LEFT JOIN tblExpense e
                                 ON e.ID = empm.ExpenseID
                                    AND empm.IsDeleted = 0
                                    AND e.[Type] = 'Bill'
                          LEFT JOIN tblVendor v
                                 ON e.VendorID = v.ID
                   WHERE  e.VendorID = @VendorID
                          AND e.IsDeleted = 0
                          AND e.DomainID = @DomainID
                          AND ( ( @StartDate IS NULL
                                   OR @StartDate = '' )
                                 OR e.Date >= @StartDate )
                          AND ( ( @EndDate IS NULL
                                   OR @EndDate = '' )
                                 OR e.Date <= @EndDate )
                          AND ( Isnull(@Status, '') = ''
                                AND ( e.StatusID IN (SELECT ID
                                                     FROM   tbl_CodeMaster
                                                     WHERE  [Type] IN ( 'Open', 'Partial' )
                                                            AND IsDeleted = 0) )
                                 OR ( Isnull(@Status, '') != ''
                                      AND e.StatusID IN(SELECT ID
                                                        FROM   tbl_CodeMaster
                                                        WHERE  [Type] = @Status
                                                               AND IsDeleted = 0) ) )
                   GROUP  BY empm.ExpensePaymentID,
                             e.StatusID,
                             v.NAME)
          SELECT ep.PayId,
                 tep.[Date]                                        PayDate,
                 Sum(ep.PaidAmount)                                AS PaidAmount,
                 tep.Reference,
                 tep.ModifiedOn,
                 emp.FirstName                                     ModifiedBy,
                 (SELECT Substring((SELECT + ',' + te.BillNo
                                    FROM   tblExpense te
                                           JOIN tblExpensePaymentMapping tempm
                                             ON te.ID = tempm.ExpenseID
                                                AND tempm.IsDeleted = 0
                                                AND ExpensePaymentID = tep.ID
                          WHERE  te.DomainID = @DomainID
                                    FOR XML PATH('')), 2, 200000)) Bill,
                 VendorName
          FROM   expensePayment ep
                 LEFT JOIN tblExpensePayment tep
                        ON ep.PayId = tep.ID
                 LEFT JOIN tbl_employeeMaster emp
                        ON emp.id = tep.ModifiedBy
          --WHERE  
          --( Isnull(@Status, NULL) = '' )
          --                OR ( Isnull(@Status, NULL) != ''
          --                     AND ep.Status IN(SELECT ID
          --                                      FROM   tbl_CodeMaster
          --                                      WHERE  [type] = @Status
          --                                             AND IsDeleted = 0) )
          --AND ( ( @StartDate IS NULL
          --         OR @StartDate = '' )
          --       OR tep.CreatedOn >= @StartDate )
          --AND ( ( @EndDate IS NULL
          --         OR @EndDate = '' )
          --       OR tep.CreatedOn <= @EndDate ) 
          GROUP  BY ep.PayId,
                    tep.[Date],
                    tep.Reference,
                    tep.ModifiedOn,
                    emp.FirstName,
                    tep.ID,
					VendorName

          --ORDER  BY tep.ModifiedOn DESC
          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
