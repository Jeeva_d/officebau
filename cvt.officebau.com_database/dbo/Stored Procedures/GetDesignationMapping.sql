﻿/****************************************************************************   
CREATED BY		: DHANALAKSHMI.S
CREATED DATE	: 08-JAN-2018 
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
 [GetDesignationMapping] 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetDesignationMapping] (@ID       INT,
                                                @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT DM.ID            AS ID,
                 BA.NAME          AS Band,
                 EG.NAME          AS [Level],
                 D.NAME           AS Designation,
                 DM.DesignationID AS DesignationID,
                 DM.BandID        AS BandID,
                 DM.GradeID       AS LevelID,
                 DM.ModifiedOn,
                 EMP.FullName     AS ModifiedBy,
                 BU.NAME          AS BaseLocation
          FROM   tbl_EmployeeDesignationMapping DM
                 LEFT JOIN tbl_Band BA
                        ON BA.ID = DM.BandID
                 LEFT JOIN tbl_EmployeeGrade EG
                        ON EG.ID = DM.GradeID
                 LEFT JOIN tbl_Designation D
                        ON D.ID = DM.DesignationID
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = DM.ModifiedBy
                 LEFT JOIN tbl_BusinessUnit BU
                        ON EMP.BaseLocationID = BU.ID
          WHERE  DM.IsDeleted = 0
                 AND DM.ID = @ID
                 AND DM.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
