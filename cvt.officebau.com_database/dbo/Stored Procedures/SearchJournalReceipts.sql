﻿/****************************************************************************                 
CREATED BY   :                 
CREATED DATE  :                 
MODIFIED BY   :                 
MODIFIED DATE  :                 
 <summary>              
 SearchJournalPayment 2004,null,1              
 </summary>                                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchJournalReceipts] (@ID INT, @LedgerID INT, @DomainID INT)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION
		IF (ISNULL(@ID, 0) <> 0)
		BEGIN
			SELECT
				p.Id
			   ,j.Id AS RefId
			   ,j.JournalNo
			   ,j.Debit AS Amount
			   ,(SELECT
						ISNULL(SUM(Amount), 0)
					FROM tbl_ReceiptsMapping
					WHERE Isdeleted = 0
					AND DomainId = @DomainID
					AND p.RefId = RefId
					AND PaymentId <> p.PaymentId)
				AS PaidAmount
			   ,ISNULL(p.Amount, 0) AS updateAmount
			FROM tblJournal j
			JOIN tbl_ReceiptsMapping p
				ON p.RefId = j.Id
					AND p.Isdeleted = 0
			WHERE (ISNULL(@ID, 0) = 0
			OR ISNULL(@ID, 0) = p.PaymentId)
	
			AND j.Isdeleted = 0
			AND j.type = 'Ledger'
		END
		ELSE
		BEGIN
			SELECT
				0 AS Id
			   ,j.Id AS RefId
			   ,j.JournalNo
			   ,j.Debit AS Amount
			   ,(SELECT
						ISNULL(SUM(Amount), 0)
					FROM tbl_ReceiptsMapping
					WHERE Isdeleted = 0
					AND DomainId = @DomainID
					AND j.Id = RefId)
				AS PaidAmount
			   ,NULL AS updateAmount
			FROM tblJournal j
			WHERE (ISNULL(@LedgerID, 0) = 0
			OR ISNULL(@LedgerID, 0) = j.LedgerProductID)
			AND  j.type = 'Ledger'
			AND j.Isdeleted = 0
			AND j.Debit > (SELECT
					ISNULL(SUM(Amount), 0)
				FROM tbl_ReceiptsMapping
				WHERE Isdeleted = 0
				AND DomainId = @DomainID
				AND j.Id = RefId
				AND j.type = 'Ledger')

		END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMsg VARCHAR(100)
			   ,@ErrSeverity TINYINT
		SELECT
			@ErrorMsg = ERROR_MESSAGE()
		   ,@ErrSeverity = ERROR_SEVERITY()
		RAISERROR (@ErrorMsg, @ErrSeverity, 1)
	END CATCH
END
