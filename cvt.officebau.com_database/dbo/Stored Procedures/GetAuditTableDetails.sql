﻿/****************************************************************************     
CREATED BY    : Ajith N    
CREATED DATE  : 04 Jul 2018    
MODIFIED BY   :   
MODIFIED DATE :   
 <summary>        
      GetAuditTableDetails '','tbl_ApplicationConfiguration','02-apr-2018','02-aug-2018',4,1  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetAuditTableDetails] (@DBName    VARCHAR(200),
                                              @TableName VARCHAR(200),
                                              @FromDate  DATE = NULL,
                                              @ToDate    DATE = NULL,
                                              @UserID    INT,
                                              @DomainID  INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @Query           VARCHAR(max) = '',
                  @AllColumnsQuery VARCHAR(max) = '',
                  @Columns         VARCHAR(max) = ''

          SET @TableName = @TableName + '_TriggerAudit'

          CREATE TABLE #tmpColumns
            (
               ID         INT IDENTITY(1, 1),
               ColumnName VARCHAR(100)
            )

          IF ( ISNULL(@DBName, '') <> '' )
            BEGIN
                SET @DBName = @DBName + '.'
            END

          SET @AllColumnsQuery = 'insert into #tmpColumns SELECT S.Name FROM '
                                 + @DBName + 'SYS.COLUMNS S
				 JOIN ' + @DBName
                                 + 'SYS.tables T ON S.object_id = T.object_id
				 where T.name = ''' + @TableName
                                 + '''
				 AND S.[Name] not in (''AuditDataState'', ''AuditDateTime'',''AuditDMLAction'',''AuditUser'', ''UpdateColumns'',''HistoryID'')';

          EXEC(@AllColumnsQuery)

          SET @Columns = (SELECT ',' + ColumnName
                          FROM   #tmpColumns
                          FOR xml path(''))

          SET @Query = 'select AuditDataState, AuditDateTime, AuditDMLAction, AuditUser, UpdateColumns'
                       + @Columns + ' INTO #tmpAuditTable from '
                       + @TableName + ' WHERE  AuditDateTime >='''
                       + Cast(@FromDate AS VARCHAR) + ''' AND   AuditDateTime <='''
                       + Cast(@ToDate AS VARCHAR) --+ ''' AND   DomainID = ''' + CAST(@DomainID AS VARCHAR)
                       + ''' ORDER BY AuditDataState DESC;
						   
						   SELECT DISTINCT *  
						   into #tmpResult  
						   FROM   #tmpAuditTable;  
  
						   SELECT  Row_number() over (partition by AuditDateTime order by AuditDataState DESC) AS RowID, *  
						   FROM   #tmpResult
						   ORDER BY AuditDateTime DESC;'

          PRINT @Query

          EXEC (@Query)
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
