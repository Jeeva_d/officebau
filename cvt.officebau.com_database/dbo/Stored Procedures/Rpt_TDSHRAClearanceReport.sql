﻿/****************************************************************************                     
CREATED BY   :                     
CREATED DATE  :                     
MODIFIED BY   :                     
MODIFIED DATE  :                     
<summary>                            
 [Rpt_tdshraclearancereport]   0,4,1,'Cleared'  
</summary>                                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Rpt_TDSHRAClearanceReport] (@EmployeeID      INT,  
                                                   @FinancialYearid INT,  
                                                   @DomainID        INT,  
                                                   @Status          VARCHAR(100))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @FinancialYear       INT = (SELECT NAME  
                     FROM   tbl_FinancialYear  
                     WHERE  IsDeleted = 0  
                            AND DomainID = @DomainID  
                            AND ID = @FinancialYearid),  
                  @StartFinancialMonth INT,  
                  @EndFinancialMonth   INT,  
                  @StartDate           DATETIME,  
                  @EndDate             DATETIME,  
                  @ColumnNames         AS NVARCHAR(MAX),  
                  @SqlQuery            AS NVARCHAR(MAX)  
  
          SET @StartFinancialMonth = (SELECT Value  
                                      FROM   tblApplicationConfiguration  
                                      WHERE  Code = 'STARTMTH'  
                                             AND DomainID = @DomainID)  
          SET @EndFinancialMonth = ( ( (SELECT Value  
                                        FROM   tblApplicationConfiguration  
                                        WHERE  Code = 'STARTMTH'  
                                               AND DomainID = @DomainID)  
                                       + 11 ) % 12 )  
          SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date                  
          SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date      
  
          SELECT Datename(MONTH, Dateadd(MONTH, X.number, @StartDate)) AS [MonthName],  
                 Datename(YY, Dateadd(MONTH, X.number, @StartDate))    [YEAR],  
                 Datepart(MM, Dateadd(MM, X.number, @StartDate))       [MONTH],  
                 X.number                                              AS RowNumber  
          INTO   #TempMonth  
          FROM   master.dbo.spt_values X  
          WHERE  X.type = 'P'  
                 AND X.number <= Datediff(MONTH, @StartDate, @EndDate)  
  
          SET @ColumnNames = (SELECT Substring((SELECT ',[' + Substring([MonthName], 1, 3 ) + '_'  
                                                       + Substring([YEAR], 3, 2) + ']'  
                                                FROM   #TempMonth  
                                                FOR XML PATH('')), 2, 20000))  
          SET @SqlQuery = 'SELECT EmployeeCode AS [Emp Code], EmployeeName AS [Employee Name],'  
                          + @ColumnNames + ' FROM (            
         SELECT Isnull(EmpCodePattern, '''')    
           + Isnull(EM.Code, '''')        AS EmployeeCode,    
           FullName                  AS EmployeeName,    
           (CASE WHEN '''  
                          + Cast(@Status AS VARCHAR)  
                          + ''' = ''Declared'' then ISNULL(HC.Declaration,'''')  
           When '''  
                          + Cast(@Status AS VARCHAR)  
                          + ''' = ''Cleared'' then ISNULL(HC.Cleared,'''')  
                           ELSE ISNULL(HC.Submitted,'''') END)                         AS HRAAmount,                 
           M.Code + ''_''    
           + Substring(Cast(YEARID AS VARCHAR(20)), 3, 2) AS MonthYear    
          FROM  tbl_EmployeeMaster EM    
           LEFT JOIN HRA HC    
            ON HC.EmployeeID = EM.ID    
               AND HC.YearID = '  
                          + Cast(@FinancialYear AS VARCHAR)  
                          + '    
           LEFT JOIN tbl_Month M    
            ON M.ID = HC.MonthID    
          WHERE  EM.IsDeleted = 0    
           AND EM.IsActive = 0    
           AND EM.DomainID = '  
                          + Cast(@DomainID AS VARCHAR)  
                          + '    
           AND ('  
                          + Cast(@EmployeeID AS VARCHAR)  
                          + '= 0 OR EM.ID = '  
                          + Cast(@EmployeeID AS VARCHAR)  
                          + ')) x            
        pivot             
        (            
          Max(HRAAmount)            
         for MonthYear in (' + @ColumnNames  
                          + ')            
        ) p ORDER BY p.[EmployeeName]'  
  
          EXEC ( @SqlQuery )  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity();  
          RAISERROR(@ErrorMsg,@ErrSeverity,1);  
      END CATCH;  
  END
