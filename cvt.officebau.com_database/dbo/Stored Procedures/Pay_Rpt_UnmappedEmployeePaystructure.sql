﻿/****************************************************************************            
CREATED BY   : DHANALAKSHMI S            
CREATED DATE  : 17-MAY-2019            
MODIFIED BY   :            
MODIFIED DATE  :            
<summary>            
  [Pay_Rpt_UnmappedEmployeePaystructure] 1,0, 1       
</summary>            
*****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_Rpt_UnmappedEmployeePaystructure] (@DomainID   INT,
                                                              @EmployeeID INT,
                                                              @IsActive   BIT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT CASE
                   WHEN EMP.Code LIKE 'TMP_%' THEN EMP.Code
                   ELSE Isnull(EMP.EmpCodePattern, '' ) + EMP.Code
                 END                                                   AS [Emp_No],
                 EMP.FullName                                          AS [Employee Name],
                 DEP.NAME                                              AS [Department],
                 DS.NAME                                               AS [Designation],
                 REP.FullName                                          AS [Reporting To],
                 Replace(CONVERT(VARCHAR(20), emp.DOJ, 106), ' ', '-') AS [Date of Joining],
                 ( CASE
                     WHEN EMP.IsActive = 0 THEN 'Active'
                     ELSE 'Inactive'
                   END )                                               AS [Status]
          FROM   tbl_EmployeeMaster EMP
                 LEFT JOIN tbl_Pay_EmployeePayStructure PEPS
                        ON emp.ID = PEPS.EmployeeID
                           AND PEPS.IsDeleted = 0
                 LEFT JOIN tbl_EmployeeMaster REP
                        ON REP.ID = EMP.ReportingToID
                 LEFT JOIN tbl_Department DEP
                        ON DEP.ID = EMP.DepartmentID
                 LEFT JOIN tbl_Designation DS
                        ON DS.ID = EMP.DesignationID
          WHERE  EMP.DomainId = @DomainID
                 AND EMP.IsDeleted = 0
                 AND ( @IsActive IS NULL
                        OR EMP.IsActive = @IsActive )
                 AND ( EMP.ID = @EmployeeID
                        OR Isnull (@EmployeeID, 0) = 0 )
                 AND PEPS.EmployeeID IS NULL
          ORDER  BY EMP.FullName ASC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
