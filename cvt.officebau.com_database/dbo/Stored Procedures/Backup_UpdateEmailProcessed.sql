﻿
/****************************************************************************   
CREATED BY   :  Naneeshwar.M
CREATED DATE  :   20-SEP-2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Backup_UpdateEmailProcessed] (@ID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          INSERT INTO dbo.tbl_EmailProcessor_Backup
                      (ID,
                       FromID,
                       ToID,
                       Cc,
                       Bcc,
                       Subject,
                       Content,
                       Attachment,
                       AttachmentPath,
                       IsPolled,
                       SentDate,
                       RetryCount,
                       CreatedOn,
                       CreatedBy,
                       ModifiedOn,
                       ModifiedBy)
          SELECT ID,
                 FromID,
                 ToID,
                 Cc,
                 Bcc,
                 Subject,
                 Content,
                 Attachment,
                 AttachmentPath,
                 IsPolled,
                 SentDate,
                 RetryCount,
                 CreatedOn,
                 CreatedBy,
                 ModifiedOn,
                 ModifiedBy
          FROM   tbl_EmailProcessor
          WHERE  ID = @ID

          DELETE FROM tbl_EmailProcessor
          WHERE  ID = @ID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END

