﻿/****************************************************************************                                 
CREATED BY  :                              
CREATED DATE :                           
MODIFIED BY  :                    
MODIFIED DATE :                  
<summary>                              
[Temp_RPT_FinancialReport ]                        
</summary>                                                         
*****************************************************************************/            
CREATE PROCEDURE [dbo].[RPT_FinancialReport ] (@FYID     INT,            
                                                   @DomainID INT,            
                                                   @RptType  VARCHAR(100) )            
AS            
  BEGIN            
      SET NOCOUNT ON;            
            
      BEGIN TRY            
          DECLARE @FYIDvalue INT = (SELECT financialyear            
                     FROM   tbl_financialyear            
                     WHERE  id = @FYID),            
                  @Month     INT = (SELECT value            
                     FROM   tblapplicationconfiguration            
                     WHERE  code = 'STARTMTH'            
                            AND domainid = @DomainID)            
          DECLARE @StartDate DATETIME = Dateadd(MONTH, @Month - 1, Dateadd(YEAR, @FYIDvalue - 1900, 0)),            
                  @EndDate   DATETIME = Dateadd(MONTH, CASE            
                                     WHEN ( @Month = 1 ) THEN            
                                       12 - 1            
                                     ELSE            
                                       @Month - 1 - 1            
                                   END, Dateadd(YEAR, @FYIDvalue + 1 - 1900, 0))            
            
          SELECT CASE            
                   WHEN ( ISNULL(l.LedgerType, 'Indirect Expense') = 'Indirect Expense' ) THEN            
                     5            
                   ELSE            
                     2            
                 END                                      AS sortid,            
                 v.NAME,            
                 Sum(it.Amount*Qty) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0)          
                          totalamount,            
                 Datename(MONTH, e.Date)                  monthnames,            
                 ISNULL(l.LedgerType, 'Indirect Expense') GrType,            
                 'Booked'                                 AS ReportType            
          INTO   #Income            
          FROM   tblexpense e            
                 LEFT JOIN tblexpenseDetails it            
                        ON e.ID = it.ExpenseId            
                 LEFT JOIN tblLedger l            
                        ON it.LedgerID = l.ID  And IsLedger=1          
                 LEFT JOIN tblGroupLedger v            
                        ON v.ID = l.GroupID            
          WHERE  e.isdeleted = 0            
                 AND e.Date > @StartDate            
                 AND e.Date < @EndDate  AND e.domainid = @DomainID            
          GROUP  BY v.NAME,            
                    Datename(MONTH, e.Date),            
                    l.LedgerType            
            
          INSERT INTO #Income            
          SELECT 1,            
                 v.NAME,            
                 Sum(ISNULL(it.CGSTAmount, 0))            
                 + Sum(ISNULL(it.SGSTAmount, 0))            
                 + Sum((it.QTY * it.Rate)) totalamount,            
                 Datename(MONTH, e.Date)   monthnames,            
                 'Direct Income'           GrType,            
                 'Booked'            
          FROM   tblInvoice e            
                 LEFT JOIN tblInvoiceItem it            
                        ON e.ID = it.InvoiceID            
                 LEFT JOIN tblCustomer v            
                        ON V.ID = e.CustomerID            
          WHERE  e.isdeleted = 0            
                 AND e.Date > @StartDate            
                 AND e.Date < @EndDate      AND e.domainid = @DomainID           
          GROUP  BY v.NAME,            
                    Datename(MONTH, e.Date)            
            
          INSERT INTO #Income            
          SELECT CASE            
                   WHEN ( ISNULL(l.LedgerType, 'Indirect Expense') = 'Indirect Expense' ) THEN            
                     5            
       ELSE            
                     2            
                 END                                      AS sortid,            
                 g.NAME,            
                 ((ed.Amount*Qty) +(CGST+SGST))/(100/  
((epm.Amount/(Select Sum(Amount*Qty) +Sum(CGST+SGST) from tblExpenseDetails    
where IsDeleted=0 and ExpenseID=ed.ExpenseID)) *100))                totalamount,            
                 Datename(MONTH, e.Date)                  monthnames,            
                 ISNULL(l.LedgerType, 'Indirect Expense') GrType,            
                 'Paid'            
          FROM   tblExpenseDetails Ed            
                 JOIN tblLedger l            
                   ON l.ID = ed.LedgerID   And IsLedger=1        
                 JOIN tblGroupLedger g            
                   ON g.ID = l.GroupID            
                 JOIN tblExpensePaymentMapping epm            
                   ON epm.ExpenseID = ed.ExpenseID            
                      AND epm.IsDeleted = 0            
                 JOIN tblExpense e            
                   ON e.ID = Ed.ExpenseId            
          WHERE  e.isdeleted = 0            
                 AND ed.IsDeleted = 0            
                 AND e.Date > @StartDate            
                 AND e.Date < @EndDate   AND e.domainid = @DomainID              
                 
            
          INSERT INTO #Income            
          SELECT 1,            
                 v.NAME,            
                 Sum(Ipm.Amount)         totalamount,            
                 Datename(MONTH, e.Date) monthnames,            
                 'Direct Income'         GrType,            
                 'Paid'            
          FROM   tblInvoice e            
                 LEFT JOIN tblInvoiceItem it            
                        ON e.ID = it.InvoiceID            
                 LEFT JOIN tblCustomer v            
                        ON V.ID = e.CustomerID            
                 JOIN tblInvoicePaymentMapping IPM            
                   ON ipm.InvoiceID = e.ID            
          WHERE  e.isdeleted = 0            
                 AND e.Date > @StartDate            
                 AND e.Date < @EndDate    AND e.domainid = @DomainID             
          GROUP  BY v.NAME,            
                    Datename(MONTH, e.Date)            
            
          DELETE #Income            
          WHERE  ReportType <> @RptType            
            
          SELECT Datename(MONTH, '2012-' + Cast(number AS VARCHAR(2)) + '-1') monthname            
          INTO   #month            
          FROM   master..spt_values s            
          WHERE  s.type = 'P'            
                 AND number BETWEEN 1 AND 12            
          ORDER  BY number            
            
          SELECT e.sortid,            
                 m.monthname,            
                 e.NAME                   NAME,            
                 ISNULL(e.totalamount, 0) Amount,            
                 e.GrType            
          INTO   #pivottable            
          FROM   #month m            
                 LEFT JOIN #Income e            
                        ON m.monthname = e.monthnames            
            
          CREATE TABLE #months            
            (            
               rowid     INT IDENTITY (1, 1) NOT NULL,            
               monthname VARCHAR(50) NOT NULL            
            )            
  
          INSERT INTO #months            
                      (monthname)            
          SELECT Datename(MONTH, '2012-' + Cast(number AS VARCHAR(2)) + '-1') monthname            
          FROM   master..spt_values s            
          WHERE  s.type = 'P'            
                 AND number BETWEEN 4 AND 12            
          UNION ALL          
          SELECT Datename(MONTH, '2012-' + Cast(number AS VARCHAR(2)) + '-1') monthname            
          FROM   master..spt_values s            
          WHERE  s.type = 'P'            
                 AND number BETWEEN 1 AND 3            
            
          DECLARE @cols  AS NVARCHAR(max),            
                  @cols2 AS NVARCHAR(max),            
                  @query AS NVARCHAR(max),            
                  @cols3 AS NVARCHAR(max),            
                  @cols4 AS NVARCHAR(max),            
                  @cols5 AS NVARCHAR(max);            
            
          SET @cols = Stuff((SELECT ',' + monthname            
                             FROM   #months            
                             FOR xml PATH (''), TYPE) .value('.', 'NVARCHAR(MAX)'), 1, 1, '')            
          SET @cols2 = Stuff((SELECT '+ ISNULL(' + monthname + ',0)'            
                              FROM   #months            
                              FOR xml PATH (''), TYPE) .value('.', 'NVARCHAR(MAX)'), 1, 1, '')            
          SET @cols4 = Stuff((SELECT ', SUM(ISNULL(' + monthname + ',0))'            
                              FROM   #months            
                              FOR xml PATH (''), TYPE) .value('.', 'NVARCHAR(MAX)'), 1, 1, '')            
          SET @cols3 = Stuff((SELECT ',' + monthname + ' money'            
                              FROM   #months            
                              FOR xml PATH (''), TYPE) .value('.', 'NVARCHAR(MAX)'), 1, 1, '')            
          SET @cols5 = Stuff((SELECT ', SUM(ISNULL(a.' + monthname            
                                     + ',0))  -SUM(ISNULL(b.' + monthname + ',0))'            
                              FROM   #months            
                              FOR xml PATH (''), TYPE) .value('.', 'NVARCHAR(MAX)'), 1, 1, '')            
            
          EXEC ('create table ##TempTable(sortid varchar(50),[Name] varchar(500) null,GroupType Varchar(500) null,Total Money,' + @cols3 + ')')            
            
          EXEC ('create table ##TempTable2(sortid varchar(50),[Name] varchar(500) null,GroupType Varchar(500) null,Total Money,' + @cols3 + ')')            
            
          SET @query = 'SELECT sortid, Name,GrType AS GroupType,'            
                       + @cols2 + ' As Total , ' + @cols            
                       + '  from              (                 select sortid, Name                     , amount , GrType                   , monthname                        
                       from #PivotTable            ) x             pivot              (                sum(amount)                                   
               for monthname in (' + @cols            
                       + ')             ) p '            
            
          PRINT @query            
            
          INSERT INTO ##TempTable            
          EXECUTE sp_executesql            
            @query            
            
          SET @query = ' Select sortid,''Total Amount'' ,GroupType,Sum('            
                       + @cols2 + ') ,' + @cols4 + 'From ##TempTable            
  Group by GroupType,sortid            
'            
            
          INSERT INTO ##TempTable2            
          EXECUTE sp_executesql            
            @query            
            
          SET @query = ( 'Select ''4'',''Gross Profit'',''Gross Profit'',SUM(ISNULL(a.total,0))  -SUM(ISNULL(b.total,0)) ,'            
                         + @cols5            
                         + '             
  From ##TempTable2 a LEFT Join ##TempTable2 b  on b.Name=b.Name and b.GroupType =''Direct Expense''            
  Where a.GroupType =''Direct Income''' )            
            
          INSERT INTO ##TempTable2            
          EXECUTE sp_executesql            
            @query            
            
          SET @query = ( 'Select ''6'',''NET Profit'',''NET Profit'',SUM(ISNULL(a.total,0))  -SUM(ISNULL(b.total,0)),'            
                         + @cols5 + '             
  From ##TempTable2 a LEFT Join ##TempTable2 b  on b.Name=b.Name and b.GroupType =''inDirect Expense''            
  Where a.Name =''Gross Profit''' )            
            
          PRINT @query            
            
          INSERT INTO ##TempTable2            
          EXECUTE sp_executesql            
            @query            
            
          EXEC ('SELECT *            
                 FROM   ##TempTable            
                 UNION ALL            
                 SELECT *            
                 FROM   ##TempTable2            
                 ORDER  BY sortid')            
            
          DROP TABLE #income, ##TempTable, #month, #pivottable, #months, ##TempTable2            
      END TRY            
      BEGIN CATCH            
          DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT            
          SELECT @ErrorMsg = ERROR_MESSAGE(),            
                 @ErrSeverity = ERROR_SEVERITY()            
          RAISERROR (@ErrorMsg,@ErrSeverity,1)            
      END CATCH            
  END
