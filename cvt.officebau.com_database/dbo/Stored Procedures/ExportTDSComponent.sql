﻿/**************************************************************************** 
CREATED BY			:	Ajith N
CREATED DATE		:	21 Nov 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>       
    ExportTDSComponent 3, null, 1, 106
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ExportTDSComponent] (@Year           INT,
                                            @BaseLocationID INT,
                                            @DomainID       INT,
                                            @UserID         INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @YearCode INT = (SELECT NAME
             FROM   tbl_FinancialYear
             WHERE  ID = @Year
                    AND DomainID = @DomainID)

          SELECT DISTINCT Rtrim(Ltrim(emp.Code))                         AS EmpCode,
                          emp.FirstName + ' ' + Isnull(emp.LastName, '') AS EmployeeName,
                          @YearCode                                      AS FinancialYear,
                          ''                                             AS Bonus,
                          ''                                             AS PLI,
                          ''                                             AS OtherIncome
          INTO   #tempEmployee
          FROM   tbl_EmployeeMaster emp
                 JOIN tbl_EmployeePayStructure eps
                   ON eps.EmployeeId = emp.ID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON bu.ID = emp.BaseLocationID
                 LEFT JOIN tbl_FinancialYear FY
                        ON FY.ID = @Year
                 JOIN (SELECT *
                       FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                FROM   tbl_EmployeeMaster
                                                WHERE  ID = @UserID
                                                       AND DomainID = @DomainID), ',')
                       WHERE  Isnull(Item, '') <> '') i
                   ON i.Item = BU.ID
          WHERE  emp.IsDeleted = 0
                 AND Isnull(IsActive, 0) = 0
                 AND emp.DomainID = @DomainID
                 AND ( @BaseLocationID IS NULL
                        OR @BaseLocationID = 0
                        OR emp.BaseLocationID = @BaseLocationID )

          SELECT *
          FROM   #tempEmployee
          ORDER  BY EmployeeName
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
