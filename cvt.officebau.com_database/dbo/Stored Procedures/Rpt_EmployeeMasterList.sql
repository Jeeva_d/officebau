﻿/****************************************************************************                       
CREATED BY   : Ajith N                      
CREATED DATE  : 31 Aug 2018                      
MODIFIED BY   :                       
MODIFIED DATE  :                       
 <summary>                              
   [Rpt_EmployeeMasterList]  '' , '', '', null, 1,1                      
 </summary>                                                
 *****************************************************************************/            
CREATE PROCEDURE [dbo].[Rpt_EmployeeMasterList] (@DepartmentID  INT,            
                                                @DesignationID INT,            
                                                @BusinessUnit  VARCHAR(100),            
                                                @IsActive      BIT,            
                                                @UserID        INT,            
                                                @DomainID      INT,
												@ColNames       Varchar(Max)
												)            
AS            
  BEGIN            
      SET NOCOUNT ON;            
            
      BEGIN TRY            
          DECLARE @BusinessUnitIDs VARCHAR(50)            
            
          IF( Isnull(@BusinessUnit, 0) = 0 )            
            SET @BusinessUnitIDs = (SELECT BusinessUnitID            
                                    FROM   tbl_EmployeeMaster            
                                    WHERE  ID = @UserID            
                                           AND DomainID = @DomainID)            
          ELSE            
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','            
            
          DECLARE @BusinessUnitTable TABLE            
            (            
               BusinessUnit VARCHAR(100)            
            )            
            
          INSERT INTO @BusinessUnitTable            
          SELECT @BusinessUnitIDs            
            
          SELECT Isnull(EM.EmpCodePattern, '') + EM.Code           AS [Employee Code],            
                 EM.FullName                                       AS [Employee Name],            
                 REGION.NAME                                       AS [Region],            
                 DEP.NAME                                          AS [Department],            
                 DESI.NAME                                         AS [Designation],            
                 FUN.NAME                                          AS [Functional],            
                 EM.EmailID                                        AS [Email ID],            
                 Isnull(EM.ContactNo, '')                          AS [Contact No],            
                 Isnull(EM.EmergencyContactNo, '')                 AS [Emergency Mobile No],            
                 Cast(em.doj AS DATE)                              AS [Date of Joining],            
                 Isnull(REPORTING.Code, '') + ' - '            
                 + REPORTING.FullName                              AS [Reporting To],            
                 GEN.Code                                          AS [Gender],            
                 PER.DateOfBirth                                   AS [Date of Birth],            
                 BAND.NAME                                         AS [Band],            
                 GRADE.NAME                                        AS [Grade],            
                 EMPLOYEETYPE.NAME                                 AS [Employment_Type],            
                 Cast(em.doc AS DATE)                              AS [Date of Confirmation],            
                 EM.Qualification                                  AS [Educational Qualification],            
                 PER.FatherName                                    AS [Father Name],            
                 PER.MotherName                                    AS [Mother Name],            
				BG.NAME											   AS [Blood_Group],            
                 MS.Code                                           AS [Marital_Status],            
    PER.Anniversary												   AS [Anniversary],            
                 PER.SpouseName									   AS [Spouse Name],            
                 PER.PresentAddress                                AS [Present Address],            
                 PER.PermanentAddress                              AS [Permanent Address],            
                 CITY.NAME                                         AS [City],            
                 STATUTORY.AadharID                                AS [Aadhar No],            
                 STATUTORY.BankName                                AS [Bank Name],            
                 STATUTORY.AccountNo                               AS [Bank Account No],            
                 STATUTORY.BranchName                              AS [Branch Name],            
                 STATUTORY.IFSCCode                                AS [IFSC Code],            
                 STATUTORY.PAN_No                                  AS [PAN],            
                 STATUTORY.PFNo                                    AS [PF No],            
                 STATUTORY.ESINo                                   AS [ESI No],            
                 STATUTORY.UANNo                                   AS [UAN],            
                 STATUTORY.PolicyName                              AS [Medical_Insurance_Provider],            
                 STATUTORY.CompanyPolicyNo                         AS [Company_Policy_No],            
                 STATUTORY.PolicyNo                                AS [Personal_Medical_Insurance_No],            
                 STATUTORY.TPI                                     AS [Third_Party_Insurance],            
                 AR.NAME                                           AS [Application Role],            
                 EM.LoginCode                                      AS [Login Code],            
                 Em.BiometricCode                                  AS [Biometric_Code],            
                 ( CASE            
                     WHEN EM.IsActive = 0 THEN 'Active'            
                     ELSE 'Inactive'            
                   END )                                           AS [Status],      
                 BU.NAME                                           AS [Business Unit]     
				       INTO #Result 
          FROM   tbl_EmployeeMaster EM            
                 LEFT JOIN tbl_Department DEP            
                        ON DEP.ID = EM.DepartmentID            
                 LEFT JOIN tbl_Designation DESI            
                        ON DESI.ID = EM.DesignationID            
                 LEFT JOIN tbl_Functional FUN            
                        ON FUN.ID = EM.FunctionalID            
                 LEFT JOIN tbl_BusinessUnit BU            
                        ON BU.ID = EM.BaseLocationID            
                 LEFT JOIN tbl_BusinessUnit REGION            
                        ON REGION.ID = EM.RegionID            
                 LEFT JOIN tbl_EmployementType EMPLOYEETYPE            
                        ON EMPLOYEETYPE.ID = EM.EmploymentTypeID            
                 LEFT JOIN tbl_EmployeeMaster REPORTING            
                        ON REPORTING.ID = EM.ReportingToID            
                 LEFT JOIN tbl_EmployeeStatutoryDetails STATUTORY            
                        ON STATUTORY.EmployeeID = EM.ID            
                 LEFT JOIN tbl_EmployeeMaster REPORTINGTO            
                        ON REPORTINGTO.ID = EM.ReportingToID            
                 LEFT JOIN tbl_EmployeePersonalDetails PER            
                        ON PER.EmployeeID = EM.ID            
                 LEFT JOIN tbl_CodeMaster GEN            
                        ON GEN.ID = EM.GenderID            
                 LEFT JOIN tbl_Band BAND            
                        ON BAND.ID = EM.BandID            
                 LEFT JOIN tbl_EmployeeGrade GRADE            
                        ON GRADE.ID = em.GradeID            
           LEFT JOIN tbl_BloodGroup BG            
                        ON BG.ID = PER.BloodGroupID            
                 LEFT JOIN tbl_CodeMaster MS            
                        ON MS.ID = PER.MaritalStatusID            
                 LEFT JOIN tbl_City CITY            
                        ON CITY.ID = PER.CityID            
                 LEFT JOIN tbl_EmployeeOtherDetails OTHER            
                        ON OTHER.EmployeeID = em.ID            
                 LEFT JOIN tbl_ApplicationRole AR            
                        ON AR.ID = OTHER.ApplicationRoleID            
                 JOIN @BusinessUnitTable TBU            
                   ON TBU.BusinessUnit LIKE '%,'            
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))            
                                            + ',%'            
          WHERE  EM.IsDeleted = 0            
                 AND ( @BusinessUnit = 0            
                        OR EM.BaseLocationID = @BusinessUnit )            
                 AND ( @IsActive IS NULL            
                        OR EM.IsActive = @IsActive )            
                 AND EM.Code NOT LIKE 'TMP_%'            
                 AND EM.domainID = @DomainID            
                 AND ( Isnull(@DepartmentID, '') = ''            
                        OR DEP.ID = @DepartmentID )            
                 AND ( Isnull(@DesignationID, '') = ''            
                        OR DESI.ID = @DesignationID )            
           ORDER  BY EM.Code     
		   Declare @SQL varchar(MAX) = 'Select ' + @ColNames + ' from #Result'
		Exec(@SQL)        
      END TRY            
      BEGIN CATCH            
          DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT;            
          SELECT @ErrorMsg = Error_message(),            
                 @ErrSeverity = Error_severity()            
          RAISERROR(@ErrorMsg,@ErrSeverity,1)            
      END CATCH            
  END
