﻿

/****************************************************************************                 
CREATED BY  : NANEESHWAR              
CREATED DATE : 06-Jun-2017              
MODIFIED BY  : Ajith N              
MODIFIED DATE : 08 Dec 2017              
 <summary>                        
  [Validateuser] 'mad1202','eWK9XoF32iP4UxA86wE+9LRLWKCN4NItMXvaPr8LR/Q=',2              
 </summary>                                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ValidateUser] (@UserName  VARCHAR(100),
                                      @Password  VARCHAR(100 ),
                                      @CompanyID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @UserMessage          VARCHAR(100),
              @UserID               INT = 0,
              @DomainID             INT = 0,
              @CompanyName          VARCHAR(150),
              @Currency             VARCHAR(150),
              @LastLogin            VARCHAR(30) = '',
              @FirstName            VARCHAR(100),
              @Designation          VARCHAR(100),
              @FileName             VARCHAR(500),
              @FileUploadID         UNIQUEIDENTIFIER = NULL,
              @StartScreen          VARCHAR(100),
              @IsPasswordChanged    BIT = 0,
              @Gender               VARCHAR(100),
              @StartMonth           INT=1,
              @CommunicationEmailId VARCHAR(200),
              @COLORPALETTE         VARCHAR(100)

      BEGIN TRY
          BEGIN TRANSACTION

          IF ( @UserName = Upper('superadmin') )
            BEGIN
                SET @UserID=(SELECT ID
                             FROM   tbl_EmployeeMaster
                             WHERE  Code = 'superadmin'
                                    AND IsDeleted = 1)

                IF (SELECT Count(1)
                    FROM   tbl_Login
                    WHERE  EmployeeID = @UserID
                           AND Password = @Password) = 0
                  BEGIN
                      SET @UserMessage = 'Invalid Password'

                      GOTO OutputResult
                  END
                ELSE
                  BEGIN
                      SET @UserMessage = 'SuperAdmin'
                      SET @StartScreen ='UserLogin/SuperAdmin'
                      SET @FirstName='SuperAdmin'
                      SET @IsPasswordChanged=1

                      GOTO OUTPUTRESULT
                  END

                GOTO OUTPUTRESULT
            END
          ELSE
            BEGIN
                SET @FirstName=(SELECT TOP 1 FirstName
                                FROM   tbl_EmployeeMaster
                                WHERE  LoginCode = @UserName
                                ORDER  BY IsDeleted ASC)

                IF( Upper(@FirstName) = Upper('ADMIN') )
                  BEGIN
                      SET @UserID=(SELECT ID
                                   FROM   tbl_EmployeeMaster
                                   WHERE  LoginCode = @UserName
                                          AND DomainID = @CompanyID)
                  END
                ELSE
                  BEGIN
                      SET @UserID = (SELECT EMP.ID
                                     FROM   tbl_EmployeeMaster EMP
                                     WHERE  Isnull(EMP.IsActive, 0) = 0
                                            AND EMP.IsDeleted = 0
                                            AND LoginCode = @UserName
                                            AND ( EMP.DomainID = @CompanyID
                                                   OR @CompanyID IN(SELECT Item AS ID
                                                                    FROM   dbo.Splitstring ((SELECT companyids
                                                                                             FROM   tbl_employeecompanymapping
                                                                                             WHERE  employeeid = EMP.ID
                                                                                                    AND isdeleted = 0), ',')
                                                                    WHERE  Isnull(Item, '') <> '') ))
                  END

                IF (SELECT Count(1)
                    FROM   tbl_EmployeeMaster E
                    WHERE  HasAccess = 1
                           AND E.ID = @UserID) = 0
                  BEGIN
                      SET @UserMessage = 'Please check your credential.'

                      GOTO OutputResult
                  END

                IF (SELECT Count(1)
                    FROM   tbl_Login
                    WHERE  EmployeeID = @UserID
                           AND Password = @Password) = 0
                  BEGIN
                      SET @UserMessage = 'Invalid Password'

                      GOTO OutputResult
                  END
                ELSE
                  BEGIN
                      SET @UserMessage = 'Valid'

                      SELECT @DomainID = DomainID,
                             @Gender = CM.Code
                      FROM   tbl_EmployeeMaster EM
                             LEFT JOIN tbl_CodeMaster CM
                                    ON EM.GenderID = Cm.ID
                      WHERE  EM.ID = @UserID

                      SET @StartMonth=(SELECT Value
                                       FROM   tblApplicationConfiguration
                                       WHERE  code = 'STARTMTH'
                                              AND domainid = @DomainID)
                      SET @FirstName = (SELECT FirstName + ' ' + Isnull(LastName, '')
                                        FROM   tbl_EmployeeMaster
                                        WHERE  ID = @UserID)

                      SELECT @CompanyName = Isnull(NAME, 'NoCompanyMapped'),
                             @CommunicationEmailId = Isnull(CommunicationEmailId, '')
                      FROM   tbl_Company
                      WHERE  ID = @DomainID

                      SET @LastLogin = (SELECT TOP 1 LoginTime
                                        FROM   tbl_LoginHistory
                                        WHERE  EmployeeID = @UserID
                                        ORDER  BY LoginTime DESC)
                      SET @Currency = Isnull((SELECT CurrencyCode
                                              FROM   tblCurrency
                                              WHERE  id = ((SELECT Value
                                                            FROM   tblApplicationConfiguration
                                                            WHERE  IsDeleted = 0
                                                                   AND code = 'CURNY'
                                                                   AND DomainID = @DomainID))), 'fa fa-inr')
                      SET @Designation = (SELECT D.NAME
                                          FROM   tbl_EmployeeMaster E
                                                 JOIN tbl_Designation D
                                                   ON E.DesignationID = D.ID
                                          WHERE  E.ID = @UserID)

                      SELECT @FileName = D.FileName,
                             @FileUploadID = D.Id
                      FROM   tbl_EmployeeMaster E
                             JOIN tbl_FileUpload D
                               ON E.FileID = D.ID
                      WHERE  E.ID = @UserID

                      SET @StartScreen = (SELECT MenuURL + '?menuCode=' + MenuCode
                                          FROM   tbl_EmployeeMaster EMP
                                                 LEFT JOIN tbl_EmployeeOtherDetails E
                                                        ON EMP.ID = E.EmployeeID
                                                 JOIN tbl_RBSMenu RBS
                                                   ON E.StartMenuURL = RBS.MenuCode
                                                      AND RBS.DomainID = EMP.DomainID
                                                      AND RBS.isdeleted = 0
                                          WHERE  E.IsDeleted = 0
                                                 AND EMP.ID = @UserID)
                      SET @IsPasswordChanged = (SELECT ISChanged
                                                FROM   tbl_login
                                                WHERE  EmployeeID = @UserID)
                      SET @COLORPALETTE = Isnull((SELECT ConfigValue
                                                  FROM   tbl_ApplicationConfiguration
                                                  WHERE  ConfigurationID = (SELECT Id
                                                                            FROM   tbl_ConfigurationMaster
                                                                            WHERE  code = 'COLORPALETTE')
                                                         AND BusinessUnitID = (SELECT BaseLocationID
                                                                               FROM   tbl_EmployeeMaster
                                                                               WHERE  Id = @UserID)), 'skin-blue')

                      GOTO OutputResult
                  END
            END

          OUTPUTRESULT:

          --EXEC DeleteUnReferenceUploadedFiles            
          --  @UserName            
          SELECT @UserMessage                            AS UserMessage,
                 @UserID                                 AS UserID,
                 Isnull(@DomainID, '1')                  AS DomainID,
                 @UserName                               AS UserName,
                 Isnull(@CompanyName, 'NoCompanyMapped') AS CompanyName,
                 @LastLogin                              AS LastLogin,
                 @Currency                               AS CurrencySymbol,
                 @FirstName                              AS FirstName,
                 @Designation                            AS DesignationName,
                 @FileName                               AS FileName,
                 @FileUploadID                           AS FileUploadID,
                 @StartScreen                            AS MenuURL,
                 @IsPasswordChanged                      AS IsPasswordChanged,
                 @Gender                                 AS Gender,
                 @StartMonth                             AS StartMonth,
                 @CommunicationEmailId                   AS CommunicationEmailId,
				  @COLORPALETTE                           AS ColorPalette

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
