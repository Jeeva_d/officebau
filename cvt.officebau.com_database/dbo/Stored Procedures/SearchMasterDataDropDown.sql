﻿
/****************************************************************************               
CREATED BY  :                
CREATED DATE :               
MODIFIED BY  : JENNIFER S             
MODIFIED DATE   : 19-JUN-2017              
 <summary>                      
     [SearchMasterDataDropDown] 'RBSMENU', '',0,0      
 </summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchMasterDataDropDown] (@DependantTable VARCHAR(200),
                                                   @Type           VARCHAR(50),
                                                   @EmployeeID     INT,
                                                   @DomainID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @sql VARCHAR(MAX)

          SET @DependantTable = 'tbl_' + @DependantTable

          IF ( @Type != '' )
            BEGIN
                IF ( @Type != 'Region' )
                  BEGIN
                      IF ( @Type = 'RBSMENU' )
                        BEGIN
                            SET @sql = ( 'Select A.ID,     
           (case when A.MenuCode = ''PROFILE'' THEN ''Profile'' ELSE    
           A.Description + '' ( '' + B.SubModule + '' ) '' END) AS NAME from '
                                         + @DependantTable
                                         + ' A LEFT JOIN tbl_RBSSubmodule B ON B.ID = A.SubmoduleID      
           where (IsSubmenu = 0 OR A.MenuCode = ''PROFILE'') and A.DomainID = '
                                         + Cast(@DomainID AS VARCHAR(20))
                                         + ' and  menucode NOT IN( ''COMPANY'',''SUPERADMIN'')  Order by Name ASC' )
                        END
                      ELSE IF ( @Type = 'ID' )
                        BEGIN
                            SET @sql = ( 'Select TOP 15 ID, CAST(ID AS VARCHAR(50)) AS Name From  '
                                         + @DependantTable + ' where Isdeleted = 0    
         Order by id ASC' )
                        END
                      ELSE
                        SET @sql = ( 'Select ID, Code as Name From '
                                     + @DependantTable + ' where Type =''' + @Type
                                     + ''' AND Isdeleted = 0 Order by Name ASC' )
                  END
                ELSE
                  BEGIN
                      SET @sql = ( 'Select ID, Name as Name From '
                                   + @DependantTable
                                   + ' where Isdeleted = 0 AND ISNULL(ParentID, 0) <> 0 and DomainID = '
                                   + Cast(@DomainID AS VARCHAR(20))
                                   + ' Order by Name ASC' )
                  END
            END
          ELSE IF ( @DependantTable = 'tbl_EmployeeMaster' )
            SET @sql = ( 'Select ID, isnull(RTRIM(LTRIM(FirstName)), '''') + '' '' + isnull(LastName, '''') AS Name From '
                         + @DependantTable
                         + ' where Isdeleted = 0 AND ISNULL(IsActive, 0) = 0 and DomainID = '
                         + Cast(@DomainID AS VARCHAR(20))
                         + ' Order by Name ASC' )
          ELSE IF ( @DependantTable = 'tbl_rbssubmodule' )
            SET @sql = ( 'Select ID, SubModule AS Name From '
                         + @DependantTable
                         + ' where Isdeleted = 0 and DomainID = '
                         + Cast(@DomainID AS VARCHAR(20))
                         + ' Order by Name ASC' )
          ELSE IF ( @DependantTable = 'tbl_SalesVisit' )
            SET @sql = ( 'Select DISTINCT s.CustomerID As ID, s.EnquiryNo +'' - ''+ c.EnquiryName AS Name From '
                         + @DependantTable
                         + ' s LEFT JOIN tbl_Customer c on c.ID = s.CustomerID       
						where s.Isdeleted = 0 and s.EnquiryNo <> '''' and s.Createdby ='
                         + Cast(@EmployeeID AS VARCHAR) )
          ELSE IF ( @DependantTable = 'tbl_Customer' )
            SET @sql = ( 'Select DISTINCT ID As ID, EnquiryName AS Name From '
                         + @DependantTable + ' where Isdeleted = 0 ' )
          ELSE IF ( @DependantTable = 'tbl_Month' )
            SET @sql = ( 'Select ID, Code AS Name From '
                         + @DependantTable + ' where Isdeleted = 0' )
          ELSE IF ( @DependantTable = 'tbl_FinancialYear' )
            SET @sql = ( 'Select ID,  Name From ' + @DependantTable
                         + ' where Isdeleted = 0 AND DomainID='
                         + Cast(@DomainID AS VARCHAR(50))
                         + ' Order by Name DESC' )
          ELSE IF ( @DependantTable = 'tbl_TrainDetails' )
            SET @sql = ( 'Select ID, TrainNo AS Name From '
                         + @DependantTable
                         + ' where Isdeleted = 0 order by TrainNo DESC' )
          ELSE IF ( @DependantTable = 'tbl_CompanyPayStructure' )
            SET @sql = ( 'Select C.ID, C.Name + '' ( '' + BU.NAME + '' ) '' AS Name From '
                         + @DependantTable
                         + ' AS C LEFT JOIN tbl_BusinessUnit BU      
								ON C.BusinessUnitID = bu.ID      
						   where C.Isdeleted = 0 order by C.Name DESC' )
          ELSE IF ( @DependantTable = 'tbl_RBSMenu' )
            SET @sql = ( 'Select A.MenuCode as ID,     
					   (case when A.MenuCode = ''PROFILE'' THEN ''Profile'' ELSE    
					   A.Description + '' ( '' + B.SubModule + '' ) '' END) AS NAME from '
                         + @DependantTable
                         + ' A LEFT JOIN tbl_RBSSubmodule B ON B.ID = A.SubmoduleID      
							where (IsSubmenu = 0 OR A.MenuCode = ''PROFILE'') and A.DomainID = '
                         + Cast(@DomainID AS VARCHAR(20))
                         + ' and  menucode NOT IN( ''COMPANY'',''SUPERADMIN'')  Order by Name ASC' )
          ELSE IF ( @DependantTable = 'tbl_BusinessUnit' )
            SET @sql = ( 'Select ID, Name AS Name From '
                         + @DependantTable
                         + ' where Isdeleted = 0 AND ISNULL(ParentID, 0) = 0 AND DomainID='
                         + Cast(@DomainID AS VARCHAR(50))
                         + ' Order by Name ASC' )
          ELSE IF ( @DependantTable = 'tbl_company' )
            SET @sql = ( 'Select ID, Name AS Name From '
                         + @DependantTable
                         + ' where Isdeleted = 0  Order by Name ASC' )
          ELSE IF ( @DependantTable = 'tbl_TDSSECTION' )
            SET @sql = ( 'Select ID, Code AS Name From  TDSSECTION where Isdeleted = 0    
                         Order by Name ASC' )
          ELSE IF ( @DependantTable = 'tbl_TriggerAudit' )
            SET @sql = ( 'SELECT 1 AS ID, TABLE_NAME AS NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''%_TriggerAudit%''' )
          ELSE IF ( @DependantTable = 'tbl_Pay_CompanyPayStructure' )
            SET @sql = ( 'Select DISTINCT p.ID, PaystructureName AS Name From  tbl_Pay_CompanyPayStructure p 
							Join tbl_Pay_PayrollCompanyPayStructure ps on p.ID = ps.CompanyPayStructureID
							 where p.Isdeleted = 0 
                         Order by PaystructureName DESC' )
          ELSE IF ( @DependantTable = 'tbl_Year' )
            SET @sql = ( 'Select ID, Name AS Name From  tbl_Year where Isdeleted = 0 AND NAME <= YEAR(GETDATE())    
                         Order by Name DESC' )
          ELSE IF ( @DependantTable = 'tbl_Pay_PayrollCompontents' )
            SET @sql = ( 'Select ID, Description AS Name From  tbl_Pay_PayrollCompontents 
                         where Isdeleted = 0 
						 AND DomainId = ' + CAST(@DomainID AS VARCHAR) + '
                         Order by ID ASC' )
          ELSE IF ( @DependantTable = 'tbl_InventoryReason' )
            SET @sql = ( 'Select ID, Name as Name From  tbl_InventoryReason 
                         where Isdeleted = 0 AND DomainID='
                         + Cast(@DomainID AS VARCHAR(50))
                         + '
                         Order by Name ASC' )
          ELSE IF ( @DependantTable = 'tbl_RBSModule' )
		  BEGIN
            SET @sql = ( 'Select ID, Module AS Name From tbl_RBSModule Order by ModuleOrderID ASC' )
          END
          ELSE
            SET @sql = ( 'Select ID, Name From ' + @DependantTable
                         + ' where Isdeleted = 0 AND DomainID='
                         + Cast(@DomainID AS VARCHAR(50))
                         + ' Order by LTRIM(Name) ASC' )

          EXEC (@sql)

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 
