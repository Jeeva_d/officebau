﻿/****************************************************************************               
CREATED BY   :        
CREATED DATE  :             
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>             
        [Pay_Rpt_EmployeePaystructure] 2 ,1        
 </summary>                                       
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Pay_Rpt_EmployeePaystructure] (@CompanyPaystructureID INT,@DomainID int)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          DECLARE @cols  AS NVARCHAR(MAX),    
                  @query AS NVARCHAR(MAX)    
    
       SELECT   CONVERT(VARCHAR(20),ep.EffectiveFrom, 106) EffectiveFrom ,  
  C.Description ,  
  c.Ordinal,
   Isnull(Emp.EmpCodePattern, '') + Emp.Code                          AS Emp_Code,  
   Emp.ID                                    AS EmployeeID,        
         Emp.FullName                              AS Employee_Name,        
         CONVERT(VARCHAR(20), emp.DOJ, 106)        AS Joining_Date,        
         de.NAME Designation,        
         d.NAME                                    AS Department  ,  
   epd.amount,  
   ep.ID PayID  
   INTO #Result      
          FROM   tbl_Pay_EmployeePayStructure ep  
    Join tbl_Pay_EmployeePayStructuredetails epd on ep.id = epd.paystructureID and epd.isdeleted = 0   
    Join tbl_Pay_PayrollCompontents c on c.Id = epd.ComponentID  
     JOIN tbl_EmployeeMaster emp        
           ON emp.ID = ep.EmployeeId        
         JOIN tbl_Department d        
           ON d.ID = emp.DepartmentID        
         JOIN tbl_BusinessUnit b        
           ON b.ID = emp.BaseLocationID        
         JOIN tbl_Designation de        
           ON de.ID = emp.DesignationID   
     Where ep.DomainID =@DomainId and ep.CompanyPayStructureID =@CompanyPayStructureID   
  
      SET @cols = Stuff((SELECT ',' + Quotename(description)    
                             FROM   tbl_Pay_PayrollCompontents   
        Where DomainID =@DomainId and ISdeleted = 0   
                             ORDER  BY ordinal asc    
                             FOR XML path('')), 1, 1, ' ')    
          SET @query = 'SELECT           
       EmployeeID,Emp_Code, Employee_Name,EffectiveFrom ,' + @cols + ', Department, Designation ,Joining_Date AS [Date_of_Joining]   
                                
      from             (                    
      select          
      EmployeeID,  Description, Employee_Name, Joining_Date,EffectiveFrom,  PayID,                     
       Amount, Designation, Department ,Emp_Code             
      from #Result          
               
      ) x            pivot            (sum(Amount)                       
      for Description in (' + @cols    
                       + ')            ) p'    
    
          EXEC (@query);   
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR (@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
