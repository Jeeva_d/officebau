﻿
/**************************************************************************** 
CREATED BY			:	Dhanalakshmi.S
CREATED DATE		:	12 June 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary> 
	[Managedbbackup]  'testDB'
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Managedbbackup] (@Name VARCHAR(100))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @path     VARCHAR(256),
                  @fileName VARCHAR(256),
                  @OUTPUT   VARCHAR(100) = 'Operation Failed!'

          SET @path = 'E:\Test\'

          BEGIN
              SET @fileName = @path + @name + '_'
                              + CONVERT(VARCHAR(20), Getdate(), 112) + '.Bak'

              PRINT @fileName

              PRINT 'BACKUP DATABASE ' + @name + ' TO DISK = '''
                    + @fileName + ''''

              DECLARE @x VARCHAR(max) = 'BACKUP DATABASE ' + @name + ' TO DISK = '''
                + @fileName + ''''

              EXEC (@x)

              EXEC('BACKUP DATABASE ' + @name + ' TO DISK = ''' + @fileName + '''')

              BACKUP DATABASE @name TO DISK = @fileName
          END

          SET @OUTPUT = 'BackUp Successfully'

          FINISH:

          SELECT @OUTPUT

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
