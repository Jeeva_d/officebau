﻿/****************************************************************************                     
CREATED BY    :               
CREATED DATE  :                  
MODIFIED BY   :                 
MODIFIED DATE :                 
<summary>            
  GetPayStructureDetails_V2 1,2   
</summary>                                             
*****************************************************************************/
CREATE PROCEDURE [dbo].[GetPayStructureDetails_V2] (@EmployeePayStructureID INT,
                                                    @DomainID               INT)
AS
  BEGIN
      BEGIN TRY
          CREATE TABLE #tmpPayStructure
            (
               COMPONENT VARCHAR(100),
               [Month]   DECIMAL,
               [year]    DECIMAL,
               [Type]    VARCHAR(50),
               Ordinal   INT
            )

          IF ( @EmployeePayStructureID > 0 )
            BEGIN
                INSERT INTO #tmpPayStructure
                SELECT comp.[Description]  AS COMPONENT,
                       EPD.Amount          AS [Month],
                       ( EPD.Amount * 12 ) AS [year],
                       PSC.[Type]          AS [Type],
                       comp.Ordinal        AS Ordinal
                FROM   tbl_EmployeeMaster em
                       JOIN tbl_pay_EmployeePayStructure PSV2
                         ON PSV2.EmployeeId = em.ID
                            AND PSV2.Isdeleted = 0
                       JOIN tbl_Pay_EmployeePayStructureDetails EPD
                         ON EPD.PayStructureId = PSV2.Id
                            AND EPD.Isdeleted = 0
                       JOIN tbl_Pay_PayrollCompontents comp
                         ON comp.IsDeleted = 0
                            AND comp.ID = EPD.ComponentId
                       JOIN tbl_Pay_PaystubConfiguration PSC
                         ON PSC.IsDeleted = 0
                            AND PSC.ComponentID = comp.ID
                       JOIN tbl_Pay_PayrollCompanyPayStructure cpa
                         ON PSV2.CompanyPayStructureID = cpa.CompanyPayStructureID
                            AND EPD.ComponentID = Cpa.ComponentID
                            AND cpa.ShowinPaystructure = 1
                WHERE  em.DomainID = @DomainID
                       AND PSV2.ID = @EmployeePayStructureID
                ORDER  BY PSC.[Type] DESC,
                          comp.Ordinal ASC
            END
          ELSE
            BEGIN
                INSERT INTO #tmpPayStructure
                SELECT comp.[Description] AS COMPONENT,
                       0                  AS [Month],
                       0                  AS [year],
                       PSC.[Type]         AS [Type],
                       comp.Ordinal       AS Ordinal
                FROM   tbl_Pay_PayrollCompontents comp
                       JOIN tbl_Pay_PaystubConfiguration PSC
                         ON PSC.IsDeleted = 0
                            AND PSC.ComponentID = comp.ID
                WHERE  comp.IsDeleted = 0
                       AND comp.DomainID = @DomainID
            END

          SELECT *
          FROM   (SELECT Component,
                         [Month],
                         [year],
                         [Type],
                         Ordinal
                  FROM   #tmpPayStructure
                  UNION ALL
                  SELECT 'Total'      AS [Total],
                         Sum([Month]) AS [Per Month],
                         Sum([year])  AS [Per Annum ( x12 )],
                         [Type]       AS [Type],
                         99           AS Ordinal
                  FROM   #tmpPayStructure
                  GROUP  BY [Type]
                  UNION ALL
                  SELECT 'Cost to Company' AS [Total],
                         Sum([Month])      AS [Per Month],
                         Sum([year])       AS [Per Annum ( x12 )],
                         NULL              AS [Type],
                         100               AS Ordinal
                  FROM   #tmpPayStructure) AS x
          ORDER  BY x.[Type] DESC,
                    x.Ordinal ASC
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
