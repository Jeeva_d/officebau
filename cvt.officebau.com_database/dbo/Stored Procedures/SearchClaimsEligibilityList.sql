﻿
/****************************************************************************       
CREATED BY  :   
CREATED DATE :    
MODIFIED BY  :     
MODIFIED DATE :     
 <summary>    
   [SearchClaimsEligibilityList] 1 ,106  
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchClaimsEligibilityList] (@DomainID   INT,
                                                     @EmployeeID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT cp.ID             AS ID,
                 cc.NAME           AS ExpenseType,
                 b.NAME            AS Band,
                 l.NAME            AS Level,
                 de.NAME           AS DesignationName,
                 cp.MetroAmount    AS MetroAmount,
                 cp.NonMetroAmount AS NonMetroAmount
          FROM   tbl_ClaimPolicy cp
                 LEFT JOIN tbl_EmployeeDesignationMapping empDM
                        ON empDM.ID = cp.DesignationMappingID
                 LEFT JOIN tbl_ClaimCategory cc
                        ON cc.ID = cp.ExpenseType
                 LEFT JOIN tbl_Band b
                        ON b.ID = empDM.BandID
                 LEFT JOIN tbl_EmployeeGrade l
                        ON l.ID = empDM.GradeID
                 LEFT JOIN tbl_Designation de
                        ON de.ID = empDM.DesignationID
                 LEFT JOIN tbl_EmployeeMaster EM
                        ON empDM.DesignationID = EM.DesignationID
          WHERE  cp.IsDeleted = 0
                 AND cp.DomainID = @DomainID
                 AND ( @EmployeeID = 0
                        OR EM.ID = @EmployeeID )

          -- ORDER  BY cp.ModifiedOn DESC    
          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
