﻿/****************************************************************************     
CREATED BY   :     
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [GetCostCenter] 0,2  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchEmailConfig] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT cc.ID       AS ID,
                 Toid        AS Toid,
                 cc.CC       AS CC,
                 BCC         AS BCC,
				 cc.IsAttachment AS IsAttachment,
                 [key]       AS EmailKey,
                 cc.ModifiedOn,
                 e.FirstName AS ModifiedBy
          FROM   tblemailConfig cc
                 JOIN tbl_EmployeeMaster e
                   ON e.id = cc.ModifiedBy
          WHERE  cc.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
