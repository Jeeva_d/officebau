﻿/****************************************************************************       
CREATED BY    :     
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE :     
 <summary>      
 </summary>                               
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[UpdatePOQTY]   
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
  
  
  update tblPODetails set   
BilledQTY = Case when((Select ISNULL(Sum(QTY),0) from tblExpenseDetails  where poItemid =tblPODetails.ID and IsDeleted=0)>QTY)   
Then QTY ELSE  (Select ISNULL(Sum(QTY),0) from tblExpenseDetails  where poItemid =tblPODetails.ID and IsDeleted=0)  
END  
from tblPODetails   
  
Update tblPurchaseOrder Set StatusID =   
 Case when ((Select Sum(QTY) from tblPODetails where IsDeleted =0 and POID = tblPurchaseOrder.ID) =   
 (Select Sum(BilledQTY) from tblPODetails where IsDeleted =0 and POID = tblPurchaseOrder.ID))  
 Then   
 (Select ID from tbl_CodeMaster where Code='Closed')  
 Else   
 (Select ID from tbl_CodeMaster where Code='Open')  
 END  
 from tblPurchaseOrder   
  
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
