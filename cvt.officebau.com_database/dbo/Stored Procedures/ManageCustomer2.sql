﻿/****************************************************************************           
CREATED BY   : Dhanalakshmi          
CREATED DATE  :   14/11/2016           
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>           
  [ManageCustomer] 0,'test','addr',1,'a',123,'abc','qe','sh',0,1,1,1,0,1,1,1          
  select * from tblcustomer;          
 </summary>                                   
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[ManageCustomer2] (@ID              INT,      
                                         @Name            VARCHAR(100),      
                                         @Address         VARCHAR(1000),      
                                         @CityID          INT,      
                                         @PaymentTerms    VARCHAR(100),      
                                         @ContactNo       VARCHAR(20),      
                                         @ContactPersonNo VARCHAR(20),      
                                         @ContactPerson   VARCHAR(50),      
                                         @EmailID         VARCHAR(100),      
                                         @CustomerEmailID VARCHAR(100),      
                                         @CurrencyID      INT,      
                                         @BillingAddress  VARCHAR(1000),      
                                         @PanNo           VARCHAR(500),      
                                         @TanNo           VARCHAR(500),      
                                         @GSTNo           VARCHAR(500),      
                                         @Remarks         VARCHAR(1000),
                                         @BankID          INt,
                                         @HasDeleted      BIT,      
                                         @SessionID       INT,      
                                         @DomainID        INT )     
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      DECLARE @Output VARCHAR(1000)      
      
      BEGIN TRY      
          IF( @HasDeleted = 1 )      
            BEGIN      
                IF EXISTS (SELECT 1      
                           FROM   tblInvoice      
                           WHERE  CustomerID = @ID      
                                  AND IsDeleted = 0      
                                  AND DomainID = @DomainID)      
                  BEGIN      
                      SET @Output = (SELECT [Message]      
                                     FROM   tblErrorMessage      
                                     WHERE  [Type] = 'Warning'      
                                            AND Code = 'RCD_REF'      
                                            AND IsDeleted = 0) --'The record is referred'          
                      GOTO finish      
                  END      
                ELSE      
                  BEGIN      
                      UPDATE tblCustomer      
                      SET    IsDeleted = 1      
                      WHERE  ID = @ID      
                             AND DomainID = @DomainID      
      
                      SET @Output = (SELECT [Message]      
                                     FROM   tblErrorMessage      
                                     WHERE  [Type] = 'Information'      
                                            AND Code = 'RCD_DEL'      
                                            AND IsDeleted = 0) --'Deleted Successfully'          
                  END      
            END      
          ELSE      
            BEGIN      
                IF( @ID = 0 )      
                  BEGIN      
                      IF( (SELECT Count(1)      
                           FROM   tblCustomer      
                           WHERE  NAME = @Name      
                                  AND DomainID = @DomainID      
                                  AND IsDeleted = 0) = 0 )      
                        BEGIN      
                            INSERT INTO tblCustomer      
                                        (NAME,      
                                         [Address],      
                                         CityID,      
                                         PaymentTerms,      
                                         ContactNo,      
                                         ContactPerson,      
                                         ContactPersonNo,      
                                         EmailID,      
                                         CustomerEmailID,      
                                         CurrencyID,      
                                         BillingAddress,      
                                         PanNo,      
                                         TanNo,      
                                         GSTNo,
                                         BankID,      
                                         Remarks,      
                                         CreatedBy,      
                                         ModifiedBy,      
                                         DomainID)      
                            VALUES      (@Name,      
                                         @Address,      
                                         @CityID,      
                                         Isnull(@PaymentTerms, 0),      
                                         @ContactNo,      
                                         @ContactPerson,      
                                         @ContactPersonNo,      
                                         @EmailID,      
                                         @CustomerEmailID,      
                                         ( CASE      
                                             WHEN @CurrencyID IS NULL THEN (SELECT Value      
                                                                            FROM   tblApplicationConfiguration      
                                                                            WHERE  Code = 'CURNY'      
                                                                                   AND DomainID = @DomainID)      
                                             ELSE ( @CurrencyID )      
                                           END ),      
                                         @BillingAddress,      
                                         @PanNo,      
                                         @TanNo,      
                                         @GSTNo,
                                         @BankID,      
                                         @Remarks,      
                                         @SessionID,      
                                         @SessionID,      
                                         @DomainID      
                                )      
      
                            DECLARE @RefID INT = @@IDENTITY      
      
                            SET @Output = (SELECT [Message]      
                                           FROM   tblErrorMessage      
                                           WHERE  [Type] = 'Information'      
                                                  AND Code = 'RCD_INS'      
                                                  AND IsDeleted = 0)      
                                          + '/' + Cast(@RefID AS VARCHAR(100)) + '/'      
                                          + (SELECT Code      
                                             FROM   tblCurrency      
                                             WHERE  id = ((SELECT Value      
                                                           FROM   tblApplicationConfiguration      
                                                           WHERE  IsDeleted = 0      
                                                                  AND code = 'CURNY'      
                                                                  AND DomainID = @DomainID))) --'Inserted Successfully/'          
                        END      
                      ELSE      
                        BEGIN      
                            SET @Output = (SELECT [Message]      
                                           FROM   tblErrorMessage      
                                           WHERE  [Type] = 'Warning'      
                                                  AND Code = 'RCD_EXIST'      
                                                  AND IsDeleted = 0) --'Already Exists'          
                        END      
                  END      
                ELSE      
                  BEGIN      
                      IF( (SELECT Count(1)      
  FROM   tblCustomer      
                           WHERE  NAME = @Name      
                                  AND DomainID = @DomainID      
                                  AND ID <> @ID      
                                  AND IsDeleted = 0) = 0 )      
                        BEGIN      
                            UPDATE tblCustomer      
                            SET    NAME = @Name,      
                                   [Address] = @Address,      
                                   CityID = @CityID,      
                                   PaymentTerms = @PaymentTerms,      
                                   ContactNo = @ContactNo,      
                                   ContactPerson = @ContactPerson,      
                                   ContactPersonNo = @ContactPersonNo,      
                                   EmailID = @EmailID,      
                                   CustomerEmailID = @CustomerEmailID,      
                                   CurrencyID = ( CASE      
                                                    WHEN @CurrencyID IS NULL THEN (SELECT Value      
                                                                                   FROM   tblApplicationConfiguration      
                                                                                   WHERE  Code = 'CURNY'      
                                                                                          AND DomainID = @DomainID)      
                                                    ELSE ( @CurrencyID )      
                                                  END ),      
                                   BillingAddress = @BillingAddress,      
                                   PanNo = @PanNo,      
                                   TanNo = @TanNo,      
                                   GSTNo = @GSTNo,
                                   BankID = @BankID,      
                                   Remarks = @Remarks,      
                                   ModifiedBY = @SessionID,      
                                   ModifiedOn = Getdate()      
                                       
                            WHERE  ID = @ID      
                                   AND DomainID = @DomainID      
      
                            SET @Output = (SELECT [Message]      
                                           FROM   tblErrorMessage      
                                           WHERE  [Type] = 'Information'      
                                                  AND Code = 'RCD_UPD'      
                                                  AND IsDeleted = 0) --'Updated Successfully'          
                        END      
                      ELSE      
                        BEGIN      
                            SET @Output = (SELECT [Message]      
                                           FROM   tblErrorMessage      
                                           WHERE  [Type] = 'Warning'      
                                                  AND Code = 'RCD_EXIST'      
                                                  AND IsDeleted = 0) --'Already Exists'          
                        END      
                  END      
            END      
      
          FINISH:      
      
          SELECT @Output      
      END TRY      
      BEGIN CATCH      
          --ROLLBACK TRANSACTION          
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
