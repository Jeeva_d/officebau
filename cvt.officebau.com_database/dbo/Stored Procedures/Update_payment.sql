﻿/****************************************************************************                   
CREATED BY    :             
CREATED DATE  :                
MODIFIED BY   :               
MODIFIED DATE :               
<summary>               
</summary>                                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[Update_payment] (@ID            INT,
                                         @Date          DATETIME,
                                         @Description   VARCHAR(1000),
                                         @PartyName     VARCHAR(100),
                                         @TotalAmount   MONEY,
                                         @PaymentModeID INT,
                                         @BankID        INT,
                                         @LedgerID      INT,
                                         @SessionID     INT,
                                         @DomainID      INT,
                                         @Payment       PAYMENT readOnly)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT            VARCHAR(100),
                  @TransactionTypeID INT =(SELECT ID
                    FROM   tblMasterTypes
                    WHERE  NAME = 'Make Payment'),
                  @PreviousCash      MONEY = (SELECT TotalAmount
                     FROM   tbl_Payment
                     WHERE  ID = @ID
                            AND DomainID = @DomainID),
                  @PreviousMode      INT = (SELECT ModeID
                     FROM   tbl_Payment
                     WHERE  isdeleted = 0
                            AND ID = @ID
                            AND DomainID = @DomainID)

          --'The record has been Reconciled.'                
          IF EXISTS (SELECT 1
                     FROM   tblbrs
                     WHERE  SourceID = @ID
                            AND SourceType = @TransactionTypeID
                            AND IsDeleted = 0
                            AND IsReconsiled = 1
                            AND DomainID = @DomainID)
            BEGIN
                SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Information'  
                                                  AND Code = 'RCD_UPD_DES'  
                                                  AND IsDeleted = 0) --'The record is referred. Description is Updated.'            
                UPDATE tbl_Payment
                SET    [Description] = @Description,
                       LedgerID = @LedgerID,
                       ModifiedBy = @SessionID,
                       ModifiedOn = Getdate()
                WHERE  ID = @ID
                       AND DomainID = @DomainID

                UPDATE tblbrs
                SET    BRSDescription = @Description
                WHERE  SourceID = @ID
                       AND SourceType = @TransactionTypeID
                       AND DomainID = @DomainID

                GOTO finish
            END
          ELSE
            BEGIN
                UPDATE tbl_Payment
                SET    PartyName = @PartyName,
                       PaymentDate = @Date,
                       Description = @Description,
                       ModeID = @PaymentModeID,
                       BankID = Isnull(@BankID, 0),
                       LedgerID = @LedgerID,
                       ModifiedBy = @SessionID,
                       ModifiedOn = Getdate(),
                       TotalAmount = @TotalAmount
                WHERE  ID = @ID
                       AND DomainID = @DomainID

                IF(SELECT Count(*)
                   FROM   @Payment
                   WHERE  ID <> 0) <> 0
                  BEGIN
                      UPDATE tbl_PaymentMapping
                      SET    tbl_PaymentMapping.Amount = r.Amount
                      FROM   tbl_PaymentMapping
                             LEFT JOIN @Payment r
                                    ON r.ID = tbl_PaymentMapping.Id
                      WHERE  tbl_PaymentMapping.IsDeleted = 0
                             AND tbl_PaymentMapping.RefId = r.RefId
                  -- AND p.ID <> 0              
                  END

                IF( Isnull(@BankID, 0) <> 0 )
                  BEGIN
                      IF EXISTS(SELECT 1
                                FROM   tblBRS
                                WHERE  SourceID = @ID
                                       AND SourceType = @TransactionTypeID
                                       AND DomainID = @DomainID
                                       AND IsDeleted = 0)
                        BEGIN
                            UPDATE tblbrs
                            SET    Amount = @TotalAmount,
                                   BRSDescription = @Description,
                                   BankID = @BankID,
                                   SourceType = (SELECT ID
                                                 FROM   tblMasterTypes
                                                 WHERE  NAME = 'Make Payment')
                            WHERE  SourceID = @ID
                                   AND SourceType = @TransactionTypeID
                                   AND DomainID = @DomainID

                            UPDATE tblBookedBankBalance
                            SET    Amount = @TotalAmount
                            WHERE  BRSID = (SELECT ID
                                            FROM   tblBRS
                                            WHERE  SourceID = @ID
                                                   AND SourceType = @TransactionTypeID
                                                   AND IsDeleted = 0
                                                   AND DomainID = @DomainID)
                                   AND DomainID = @DomainID

                            EXEC Managesystembankbalance
                              @DomainID
                        END
                      ELSE
                        BEGIN
                            INSERT INTO tblBRS
                                        (BankID,
                                         SourceID,
                                         SourceDate,
                                         SourceType,
                                         Amount,
                                         BRSDescription)
                            VALUES      (@BankID,
                                         @ID,
                                         @Date,
                                         (SELECT ID
                                          FROM   tblMasterTypes
                                          WHERE  NAME = 'Make Payment'),
                                         @TotalAmount,
                                         @Description)

                            INSERT INTO tblBookedBankBalance
                                        (BRSID,
                                         Amount,
                                         DomainID,
                                         CreatedBy,
                                         ModifiedBy)
                            VALUES      (@@IDENTITY,
                                         @TotalAmount,
                                         @DomainID,
                                         @SessionID,
                                         @SessionID)

                            EXEC Managesystembankbalance
                              @DomainID

                            UPDATE tblCashBucket
                            SET    AvailableCash = AvailableCash + @TotalAmount,
                                   ModifiedOn = Getdate()
                            WHERE  DomainID = @DomainID

                            UPDATE tblVirtualCash
                            SET    IsDeleted = 1
                            WHERE  SourceID = @ID
                                   AND SourceType = (SELECT ID
                                                     FROM   tblMasterTypes
                                                     WHERE  NAME = 'Make Payment')
                                   AND IsDeleted = 0
                                   AND DomainID = @DomainID
                        END
                  END
                ELSE
                  BEGIN
                      IF( (SELECT 1
                           FROM   tblBRS
                           WHERE  SourceID = @ID
                                  AND SourceType = @TransactionTypeID
                                  AND DomainID = @DomainID
                                  AND IsDeleted = 0) = 1 )
                        BEGIN
                            UPDATE tblBookedBankBalance
                            SET    IsDeleted = 1
                            WHERE  BRSID = (SELECT ID
                                            FROM   tblBRS
                                            WHERE  SourceID = @ID
                                                   AND SourceType = @TransactionTypeID
                                                   AND IsDeleted = 0)
                                   AND DomainID = @DomainID

                            UPDATE tblBRS
                            SET    IsDeleted = 1
                            WHERE  SourceID = @ID
                                   AND SourceType = @TransactionTypeID
                                   AND DomainID = @DomainID
                                   AND IsDeleted = 0

                            EXEC Managesystembankbalance
                              @DomainID
                        END
                  END

                IF( @PaymentModeID = (SELECT ID
                                      FROM   tbl_CodeMaster
                                      WHERE  [Type] = 'PaymentType'
                                             AND Code = 'Cash'
                                             AND IsDeleted = 0) )
                  BEGIN
                      IF ( @PreviousMode = (SELECT ID
                                            FROM   tbl_CodeMaster
                                            WHERE  [Type] = 'PaymentType'
                                                   AND Code = 'Cash'
                                                   AND IsDeleted = 0) )
                        BEGIN
                            UPDATE tblCashBucket
                            SET    AvailableCash = AvailableCash + @PreviousCash - @TotalAmount,
                                   ModifiedOn = Getdate()
                            WHERE  DomainID = @DomainID

                            UPDATE tblVirtualCash
                            SET    Amount = 0 - @TotalAmount
                            WHERE  SourceID = @ID
                                   AND SourceType = (SELECT ID
                                                     FROM   tblMasterTypes
                                                     WHERE  NAME = 'Make Payment')
                                   AND IsDeleted = 0
                                   AND DomainID = @DomainID
                        END
                      ELSE
                        BEGIN
                            UPDATE tblCashBucket
                            SET    AvailableCash = AvailableCash - @TotalAmount,
                                   ModifiedOn = Getdate()
                            WHERE  DomainID = @DomainID

                            INSERT INTO tblVirtualCash
                                        (SourceID,
                                         Amount,
                                         Date,
                                         SourceType,
                                         DomainID,
                                         CreatedBy,
                                         ModifiedBy)
                            VALUES      (@ID,
                                         0 - @TotalAmount,
                                         @Date,
                                         (SELECT ID
                                          FROM   tblMasterTypes
                                          WHERE  NAME = 'Make Payment'),
                                         @DomainID,
                                         @SessionID,
                                         @SessionID)
                        END
                  END

                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Information'
                                      AND Code = 'RCD_UPD'
                                      AND IsDeleted = 0) --'Updated Successfully.'           

                GOTO finish
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
