﻿/****************************************************************************   
CREATED BY   : Naneeshwar  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [SearchBank] null,null, 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchBank] (@BankName  VARCHAR(100),
                                    @AccountNo VARCHAR(100),
                                    @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Table TABLE
            (
               ID              INT,
               BankName        VARCHAR(100),
               AccountNo       VARCHAR(100),
               AccountTypeName VARCHAR(100),
               OpeningBalance  MONEY,
               OpeningBalDate  DATETIME,
               ClosingBalance  MONEY,
               [Description]   VARCHAR(500),
               ModifiedOn      DATETIME,
               DisplayName     VARCHAR(500),
               BranchName      VARCHAR(500),
               IFSCCode        VARCHAR(500),
               SWIFTCode       VARCHAR(500),
               MICRCode        VARCHAR(500)
            )

          INSERT INTO @Table
          SELECT DISTINCT bk.ID                                                                                         AS ID,
                          bk.NAME                                                                                       AS BankName,
                          bk.AccountNo                                                                                  AS AccountNo,
                          cd.Code                                                                                       AS AccountTypeName,
                          bk.OpeningBalance                                                                             AS OpeningBalance,
                          bk.OpeningBalDate                                                                             AS OpeningBalDate,
                          ( (SELECT Isnull(Sum(amount), 0)
                             FROM   tblbrs
                             WHERE  isdeleted = 0
                                    AND IsReconsiled = 1
                                    AND SourceType IN((SELECT ID
                                                       FROM   tblMasterTypes
                                                       WHERE  BankID = tbrs.BankID
                                                              AND NAME IN ( 'InvoiceReceivable', 'Receive Payment', 'ToBank' )))
                                    AND DomainID = @DomainID) - (SELECT Isnull(Sum(amount), 0)
                                                                 FROM   tblbrs
                                                                 WHERE  isdeleted = 0
                                                                        AND IsReconsiled = 1
                                                                        AND DomainID = @DomainID
                                                                        AND SourceType IN((SELECT ID
                                                                                           FROM   tblMasterTypes
                                                                                           WHERE  BankID = tbrs.BankID
                                                                                                  AND NAME IN ( 'PayBill', 'FromBank', 'Bill', 'Expense', 'Make Payment' )))
                                                                        AND DomainID = @DomainID) ) + bk.OpeningBalance AS ClosingBalance,
                          bk.[Description]                                                                              AS [Description],
                          bk.ModifiedOn                                                                                 AS ModifiedOn,
                          bk.DisplayName                                                                                AS DisplayName,
                          bk.BranchName                                                                                 AS BranchName,
                          bk.IFSCCode                                                                                   AS IFSCCode,
                          bk.SWIFTCode                                                                                  AS SWIFTCode,
                          bk.MICRCode                                                                                   AS MICRCode
          FROM   tblBank bk
                 LEFT JOIN tbl_CodeMaster cd
                        ON cd.ID = bk.AccountTypeID
                 LEFT JOIN TblBrs tbrs
                        ON tbrs.bankID = bk.ID
          WHERE  bk.IsDeleted = 0
                 AND bk.DomainID = @DomainID
                 AND bk.NAME LIKE '%' + Isnull(@BankName, '') + '%'
                 AND bk.AccountNo LIKE '%' + Isnull(@AccountNo, '') + '%'

          SELECT *
          FROM   @Table
          ORDER  BY ModifiedOn DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
