﻿/****************************************************************************           
CREATED BY   : Anto.A        
CREATED DATE  :   14-JUN-2017        
MODIFIED BY   :  Ajith N        
MODIFIED DATE  :   01 April 2018        
 <summary>                  
     [Monthlyattedancereport] 5,5,'2',1, 0        
  0 - ABSENT        
  1 - HALF DAY        
  2- FULL DAY        
        
  To be Rework based on Employee ID        
        
 </summary>                                   
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Monthlyattedancereport] (@MonthID   INT,  
                                                @YEAR      INT,  
                                                @RegionIDs VARCHAR(20),  
                                                @DOMAINID  INT,  
                                                @IsProfile BIT=0)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          IF Object_id('tempdb..##TempAttendenceResult') IS NOT NULL  
            BEGIN  
                DROP TABLE ##TempAttendenceResult  
            END  
  
          --DROP TABLE #Result, #tempAttendance, #CURRENTDATE,##TempAttendenceResult,#CTETEMP,#TempAttendenceCount        
          DECLARE @CURRENTDATE     DATE,  
                  @COLUMNNAME      VARCHAR(MAX) = '',  
                  @HEADERCOLUMN    VARCHAR(MAX) = '',  
                  @MonthCode       VARCHAR(20),  
                  @YearCode        VARCHAR(20),  
                  @StartDate       VARCHAR(20) = 1,  
                  @DEFAULTDATE     VARCHAR(100),  
                  @ADDHEADERCOLUMN VARCHAR(MAX) = '',  
                  @RESULTCOLUMN    VARCHAR(MAX) = ''  
  
          SET @MonthCode = (SELECT Code  
                            FROM   tbl_Month  
                            WHERE  ID = @MonthID  
                                   AND IsDeleted = 0)  
          SET @YearCode = (SELECT FinancialYear  
                           FROM   tbl_FinancialYear  
                           WHERE  ID = @Year  
                                  AND IsDeleted = 0  
                                  AND DomainID = @DomainID)  
          SET @DEFAULTDATE = @MonthCode + ' ' + @StartDate + ' ' + @YearCode;  
  
          IF( @RegionIDs IS NULL  
               OR @RegionIDs = '' )  
            SET @RegionIDs = (SELECT TOP 1 Cast(ID AS VARCHAR)  
                              FROM   tbl_BusinessUnit  
                              WHERE  IsDeleted = 0  
                                     AND DomainID = @DomainID  
                                     AND Isnull(ParentID, 0) <> 0  
                              FOR XML path(''))  
  
          SET @CURRENTDATE = CONVERT(DATE, @DEFAULTDATE);  
  
          CREATE TABLE #tempAttendance  
            (  
               id              INT IDENTITY(1, 1),  
               EmployeeID      VARCHAR(20),  
               LogDate         DATE,  
               Duration        TIME,  
               DomainID        INT,  
               IsPermission    INT,  
               PermissionHours TIME,  
               CheckIn         TIME,  
               BiometricCode   INT  
            )  
  
          CREATE TABLE #Result  
            (  
               id             INT IDENTITY(1, 1),  
               EmployeeID     VARCHAR(20),  
               [Name]         VARCHAR(100),  
               BaseLocationID INT  
            )  
  
          --select * from #tempEmployees        
          INSERT INTO #tempAttendance  
          SELECT TAT.EmployeeID,  
                 TAT.LogDate,  
                 CONVERT(VARCHAR(5), TAT.Duration, 108) Duration,  
                 TAT.DomainID,  
                 CASE  
                   WHEN EL.Duration IS NOT NULL THEN  
                     1  
                   ELSE  
                     ( CASE  
                         WHEN MP.PunchTime IS NOT NULL THEN  
                           3  
                         ELSE  
                           0  
                       END )  
                 END,-- if 3 (Means Missed Check-in/ check-out) & 1 Means(Permission)        
                 ( CASE  
                     WHEN EL.Duration IS NOT NULL THEN  
                       Cast(Replace(Cast(EL.Duration AS DECIMAL(8, 2)), '.', ':') AS TIME)  
                     ELSE  
                       NULL  
                   END )                                AS PermissionHours,  
                 NULL                                   AS CheckIn,  
                 CONVERT(INT, EM.BiometricCode)  
          FROM   tbl_Attendance TAT  
                 --join tbl_BiometricLogs BL on bl.AttendanceId = TAT.ID        
                 JOIN tbl_EmployeeMaster EM  
                   ON EM.ID = TAT.EmployeeID  
                 LEFT JOIN tbl_EmployeeLeaveDateMapping ELM  
                        ON ELM.EmployeeID = TAT.EmployeeID  
                           AND ELM.DomainID = TAT.DomainID  
                           AND ELM.LeaveDate = TAT.LogDate  
                           AND ELM.IsDeleted = 0  
                 LEFT JOIN tbl_EmployeeLeave EL  
                        ON el.DomainID = TAT.DomainID  
                           AND EL.ID = ELM.LeaveRequestID  
                           AND EL.IsDeleted = 0  
                           AND EL.Duration IS NOT NULL  
                 LEFT JOIN tbl_EmployeeMissedPunches MP  
                        ON MP.IsDeleted = 0  
                           AND MP.DomainID = TAT.DomainID  
                           AND Mp.EmployeeID = TAT.EmployeeID  
                           AND mp.PunchDate = TAT.LogDate  
          WHERE  TAT.DomainID = @DomainID  
                 AND Datepart(YYYY, TAT.LogDate) = @YearCode  
                 AND Datepart(MM, TAT.LogDate) = @MonthID  
          --AND ( CONVERT(VARCHAR(5), CheckIn, 108) <> '00:00'        
          --       OR CONVERT(VARCHAR(5), Isnull(PermissionHours, '00:00'), 108) <> '00:00' )        
          UNION ALL  
          SELECT EM.ID,  
                 NULL,  
                 NULL,  
                 @DomainID,  
                 NULL,  
                 NULL,  
                 NULL,  
                 EM.BiometricCode  
          FROM   tbl_EmployeeMaster EM  
          WHERE  EM.IsDeleted <> 1  
                 AND IsActive = 0  
                 AND BaseLocationID = @RegionIDs  
                 AND NOT EXISTS(SELECT EmployeeID  
                                FROM   tbl_Attendance ATT  
                                WHERE  EM.ID = ATT.EmployeeID  
                                       AND ATT.DomainID = @DomainID  
                                       AND Datepart(YYYY, LogDate) = @YearCode  
                                       AND Datepart(MM, LogDate) = @MonthID)  
                 AND NOT EXISTS(SELECT EmployeeID  
                                FROM   tbl_EmployeeLeave EL  
                                WHERE  EM.ID = EL.EmployeeID  
                                       AND EL.DomainID = @DomainID  
                                       AND Datepart(YYYY, FromDate) = @YearCode  
                                       AND Datepart(MM, FromDate) = @MonthID)  
          UNION ALL  
          SELECT EL.EmployeeID                  AS EmployeeID,  
                 ELM.LeaveDate                  AS LogDate,  
                 CASE  
                   WHEN ( (SELECT Count(1)  
                           FROM   tbl_BusinessHours BH  
                           WHERE  BH.IsDeleted = 0  
                                  AND BH.RegionID = Isnull(EM.RegionID, 0)) = 1 ) THEN  
                     ( CASE  
                         WHEN ( Isnull(EL.IsHalfDay, 'false') = 'true' ) THEN  
                          Isnull(TA.Duration, '00:00:00') 
            ELSE  
                           (SELECT TOP 1 CONVERT(TIME, Replace(F.FullDay, '.', ':'))  
                            FROM   tbl_BusinessHours F  
                            WHERE  F.IsDeleted = 0  
                                   AND F.RegionID = Isnull(EM.RegionID, 0))  
                       END )  
                   ELSE  
                     NULL  
                 END                            AS Duration,  
                 EL.DomainID,  
                 2                              AS IsPermission,-- CL, PL & SL (Leave)        
                 NULL                           AS PermissionHours,  
                 NULL                           AS CheckIn,  
                 CONVERT(INT, EM.BiometricCode) AS BiometricCode  
          FROM   tbl_EmployeeLeave EL  
                 LEFT JOIN tbl_EmployeeLeaveDateMapping ELM  
                        ON EL.ID = ELM.LeaveRequestID  
                           AND ELM.IsDeleted = 0  
                           AND EL.EmployeeID = ELM.EmployeeID  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = EL.EmployeeID  
                 LEFT JOIN tbl_Attendance TA  
                        ON ta.EmployeeID = EM.ID  
                           AND ELM.LeaveDate = TA.LogDate  
          WHERE  EL.Duration IS NULL  
                 AND EL.DomainID = @DomainID  
                 AND HRStatusID = (SELECT ID  
                                   FROM   tbl_Status  
                                   WHERE  IsDeleted = 0  
                                          AND [Type] = 'Leave'  
                                          AND Code = 'Approved')  
                 AND EL.IsDeleted = 0;  
  
      --select * from #tempAttendance         
      ;  
          WITH CTE  
               AS (SELECT @CURRENTDATE AS CurrentDate  
                   UNION ALL  
                   SELECT Dateadd(DAY, 1, CurrentDate)  
                   FROM   CTE  
                   WHERE  Dateadd(DD, 1, CurrentDate) < Dateadd(MONTH, 1, @CURRENTDATE))  
          SELECT CONVERT(VARCHAR(20), CurrentDate, 109)                      AS ColumnName,  
                 C1.*,  
                 Substring(CONVERT(VARCHAR(20), CurrentDate, 109), 1, 3)  
                 + ' '  
                 + Cast(Row_number() OVER ( ORDER BY (SELECT 1)) AS VARCHAR) RW,  
                 Datename(DW, CurrentDate)                                   AS [days],  
                 CASE  
                   WHEN HL.[Type] = 'Weekly' THEN  
                     'WO ' + LEFT( HL.[Day], 3) + '/' +  
                     + Cast(Day(CurrentDate) AS VARCHAR)  
                   WHEN HL.[Type] = 'Yearly' THEN  
                     'H ' + LEFT( HL.[Day], 3) + '/' +  
                     + Cast(Day(CurrentDate) AS VARCHAR)  
                   ELSE  
                     Cast(Day(CurrentDate) AS VARCHAR) + '/'  
                 END                                                         AS [header],  
                 Row_number()  
                   OVER(  
                     PARTITION BY CurrentDate  
                     ORDER BY CurrentDate)                                   AS Rowno  
          INTO   #CURRENTDATE  
          FROM   CTE C1  
                 LEFT JOIN tbl_Holidays HL  
                        ON CONVERT(DATE, HL.[Date]) = CONVERT(DATE, C1.CurrentDate)  
                           AND HL.IsDeleted = 0  
                           AND HL.DomainID = @DomainID  
                           AND HL.RegionID LIKE '%'  
                                                + (SELECT ',' + Cast(ParentID AS VARCHAR) + ','  
                                                   FROM   tbl_BusinessUnit  
                                                   WHERE  ID = Cast(@RegionIDs AS INT))  
                                                + '%'  
          --       LEFT JOIN tbl_EmployeeMaster EM        
          --              ON HL.RegionID LIKE '%,' + CAST(EM.RegionID AS VARCHAR) + ',%'        
          --WHERE  EM.RegionID = CAST(@RegionIDs AS INT)        
          ORDER  BY CurrentDate  
  
          --select * from #CURRENTDATE        
          SET @COLUMNNAME = (SELECT ',[' + CONVERT(VARCHAR(30), Isnull(RW, ''))  
                                    + ']'  
                             FROM   #CURRENTDATE  
                             WHERE  Rowno = 1  
                             FOR XML PATH(''))  
          SET @COLUMNNAME = (SELECT Substring(@COLUMNNAME, 2, 20000))  
          SET @RESULTCOLUMN = (SELECT Stuff((SELECT ',[' + Cast(Day(CurrentDate) AS VARCHAR)  
                                                    + '] AS [' + [header] + ']'  
                                             FROM   #CURRENTDATE  
                                             WHERE  Rowno = 1  
                                             FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'), 1, 1, ''))  
          SET @HEADERCOLUMN = (SELECT Stuff((SELECT ',[' + CONVERT(VARCHAR(30), Isnull(RW, ''))  
                                                    + '] AS ['  
                                                    + Cast(Day(CurrentDate) AS VARCHAR) + ']'  
                                             FROM   #CURRENTDATE  
                                             WHERE  Rowno = 1  
                                             FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'), 1, 1, ''))  
          SET @ADDHEADERCOLUMN = 'ALTER TABLE #Result ADD A INT;ALTER TABLE #Result ADD F INT;ALTER TABLE #Result ADD H INT;ALTER TABLE #Result ADD AL INT;ALTER TABLE #Result ADD TH VARCHAR(50);'  
                                 + (SELECT Stuff((SELECT ';ALTER TABLE #Result ADD ['  
                                                         + Cast(Day(CurrentDate) AS VARCHAR)  
                                                         + '] VARCHAR(MAX);'  
                                                  FROM   #CURRENTDATE  
                                                  WHERE  Rowno = 1  
                                                  FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'), 1, 1, ''))  
  
          EXEC(@ADDHEADERCOLUMN)  
  
          --DECLARE @DynamicHoursHeader VARCHAR(MAX)        
          --SELECT @DynamicHoursHeader = SUBSTRING((SELECT (SELECT ',CASE WHEN (['        
          --                                                       + CAST(DAY(CurrentDate) AS VARCHAR(100))        
          --                                                       + '] >= '        
          --                                                       + CONVERT(NVARCHAR(100), ' CONVERT(TIME, REPLACE(FullDay ,''.'','':''))')        
          --                                                       + ') THEN ''F''  WHEN (['        
          --                                                       + CAST(DAY(CurrentDate) AS VARCHAR(100))        
          --                                                       + '] >= '        
          --                                                       + CONVERT(NVARCHAR(100), ' CONVERT(TIME, REPLACE(HalfDay ,''.'','':''))')        
          --                                                       + ') THEN ''H'' ELSE ''A'' END AS ['        
          --                                        + CAST(DAY(CurrentDate) AS VARCHAR(100))        
          --                                                       + ']'        
          --                                                FROM   #CURRENTDATE        
          --                                       FOR XML PATH(''), type).value('.', 'varchar(max)')), 2, 200000)        
          --SELECT *        
          --FROM   #tempAttendance  where EmployeeID = 2 and LogDate = '2018-12-13'      
          DECLARE @Quary VARCHAR(max) = 'insert into #Result SELECT ID, Code + '' - '' + FullName + '''' AS Name , BaseLocationID, null, null, null,null,null,'  
            + @HEADERCOLUMN  
            + '        
           FROM         
           (        
           SELECT EM.ID,EM.FullName ,(ISNULL(EM.EmpCodePattern, '''')+ EM.Code) AS Code, CD.RW ,         
    --CONVERT(VARCHAR(5), (Convert(time, CONVERT(datetime, ISNULL(ATT.Duration, ''00:00:00'')) + CONVERT(datetime, ISNULL(ATT.PermissionHours, ''00:00:00'')))), 108) + ''/'' + CAST(ISNULL(ATT.IsPermission, ''0'') AS VARCHAR) AS Duration,      
    -- BaseLocationID        
            CASE WHEN CONVERT(VARCHAR(5), (Convert(time, CONVERT(datetime, ISNULL(ATT.Duration, ''00:00:00'')))), 108) = ''00:00'' THEN null       
    ELSE  CONVERT(VARCHAR(5),  (Convert(time, CONVERT(datetime, ISNULL(ATT.Duration, ''00:00:00'')))), 108) + ''/'' + CAST(ISNULL(ATT.IsPermission, ''0'') AS VARCHAR) END AS Duration, BaseLocationID  
           FROM tbl_EmployeeMaster EM        
           CROSS JOIN #CURRENTDATE CD        
                        JOIN #tempAttendance ATT        
            ON ATT.BiometricCode = CAST(EM.BiometricCode AS INT)  AND (ATT.LogDate = CD.CurrentDate OR  ATT.LogDate IS NULL )  AND EM.DOMAINID =  ATT.DOMAINID          
                      WHERE ISNULL(EM.IsDeleted, 0) = 0 AND ISNULL(EM.IsActive, 0) = 0  AND EM.DOMAINID ='  
            + Cast(@DOMAINID AS VARCHAR(10))  
            + '  AND CD.Rowno = 1 AND (EM.BaseLocationID in ('  
            + @RegionIDs + '))        
                      ) AS SourceTable        
           PIVOT        
           (        
           MAX(Duration)        
           FOR RW IN ('  
            + @COLUMNNAME  
            + ')        
           ) AS PivotTable ORDER BY FullName + '' - ('' + Code + '')'';'  
  
          EXEC(@Quary)  
  
          SELECT *  
          INTO   ##TempAttendenceResult  
          FROM   #Result;  
  
          --     EXEC('SELECT  EmployeeID, Name , A, F, H, ' + @DynamicHoursHeader + '        
          --INTO ##TempAttendenceResult        
          --FROM   #Result RS        
          --JOIN tbl_EmployeeMaster EM ON EM.ID = RS.EmployeeID        
          --JOIN tbl_BusinessHours BL ON BL.RegionID = EM.RegionID ');        
          --      select cd.CurrentDate, hd.Date as holiday from #CURRENTDATE cd         
          --left join tbl_Holidays hd on hd.Date = cd.CurrentDate;        
          --select * from #tempAttendance;        
          WITH CTE  
               AS (SELECT DISTINCT EmployeeID,  
                                   logdate,  
                                   CASE  
                                     WHEN ( CONVERT(TIME, ( CONVERT(DATETIME, [Duration])  
                                                            + CONVERT(DATETIME, Isnull(PermissionHours, '0:0')) )) >= CONVERT(TIME, Replace(FullDay, '.', ':')) ) THEN  
                                       'F'  
                                     WHEN ( CONVERT(TIME, ( CONVERT(DATETIME, [Duration])  
                                                            + CONVERT(DATETIME, Isnull(PermissionHours, '0:0')) )) >= CONVERT(TIME, Replace(HalfDay, '.', ':'))  
                                            AND CONVERT(TIME, ( CONVERT(DATETIME, [Duration])  
                                                                + CONVERT(DATETIME, Isnull(PermissionHours, '0:0')) )) < CONVERT(TIME, Replace(FullDay, '.', ':')) ) THEN  
                                       'H'  
                                     ELSE  
                                       'A'  
                                   END                                                                    AS [Duration],  
                                   CONVERT(TIME, ( CONVERT(DATETIME, [Duration])  
                                                   + CONVERT(DATETIME, Isnull(PermissionHours, '0:0')) )) AS [TotalDuration]  
                   FROM   #CURRENTDATE CD  
                          LEFT JOIN #tempAttendance TA  
                                 ON TA.LogDate = CD.CurrentDate  
                          JOIN tbl_EmployeeMaster EM  
                            ON Cast(EM.BiometricCode AS INT) = TA.BiometricCode  
                          LEFT JOIN tbl_BusinessHours BL  
                                 ON BL.RegionID = EM.RegionID  
                       AND BL.IsDeleted = 0  
                          LEFT JOIN tbl_Holidays hd  
                                 ON hd.IsDeleted = 0  
                                    AND hd.[Date] = cd.CurrentDate  
                                    AND hd.RegionID LIKE '%,'  
                                                         + Cast(Isnull(EM.RegionID, 0) AS VARCHAR)  
                                                         + ',%'  
                   WHERE  CD.Rowno = 1  
                          AND hd.[Date] IS NULL)  
          SELECT EmployeeID,  
                 Count(DURATION) AS COUN,  
                 Duration  
          INTO   #CTETEMP  
          FROM   (SELECT *,  
                         Row_number()  
                           OVER (  
                             partition BY logdate, employeeid  
                             ORDER BY [TotalDuration] DESC) AS Rowno  
                  FROM   CTE) AS x  
          WHERE  Rowno = 1  
          GROUP  BY EmployeeID,  
                    Duration  
  
          --select * from #CTETEMP;        
          --select * from #Result        
          --SELECT * FROM #CURRENTDATE        
          SELECT EmployeeID,  
                 ( (SELECT Count(1)  
                    FROM   #CURRENTDATE CD  
                    WHERE  CurrentDate <= ( Dateadd(day, -1, Getdate()) )) - Isnull((SELECT Count(1)  
                                                                                     FROM   tbl_Holidays H  
                                                                                     WHERE  H.IsDeleted = 0  
                                                                                            AND H.RegionID LIKE '%,'  
                                                                                                                + Cast(Isnull(EM.RegionID, 0) AS VARCHAR)  
                                                                                                                + ',%'  
                                                                                            AND Datepart(YYYY, [Date]) = @YearCode  
                                                                                            AND Datepart(MM, [Date]) = @MonthID  
                                                                                            AND [Date] <= Getdate()), 0) - Isnull(F, 0) - Isnull(H, 0) ) AS A,  
                 F,  
                 H,  
                 (SELECT Count(1)  
                  FROM   tbl_EmployeeLeave EL  
                         JOIN tbl_EmployeeLeaveDateMapping ELM  
                           ON EL.IsDeleted = 0  
                              AND el.ID = ELM.LeaveRequestID  
                              AND EL.EmployeeID = ELM.EmployeeID  
                  WHERE  el.IsDeleted = 0  
                         AND EL.EmployeeID = pvt.EmployeeID  
                         AND Datepart(YYYY, ELM.LeaveDate) = @YearCode  
                         AND Datepart(MM, ELM.LeaveDate) = @MonthID  
                         AND el.StatusID = (SELECT ID  
                                            FROM   tbl_Status s  
                                            WHERE  s.IsDeleted = 0  
                                                   AND s.[Type] = 'Leave'  
                                                   AND s.Code = 'Approved'))                                                                             AS AL,  
                 (SELECT Sum(Cast(Datediff(Minute, 0, t.Duration) AS INT)) / 60  
                  --( Cast( Sum(Cast(Datediff(Minute, 0, t.Duration) AS INT)) /60 AS VARCHAR)  
                  --                      + ':'  
                  --                      + Cast( (Sum(Cast(Datediff(Minute, 0, t.Duration) AS INT)) % 60 ) AS VARCHAR) ) + ' ' + 'hrs'  
                  FROM   tbl_attendance t  
                  WHERE  t.employeeid = pvt.EmployeeID  
                         AND Month(logdate) = @MonthID  
                         AND Year(logdate) = @YearCode  
                         AND t.domainID = @domainID)                                                                                                     AS TH  
          INTO   #TempAttendenceCount  
          FROM   (SELECT *  
                  FROM   #CTETEMP) AS S  
                 PIVOT ( Max(COUN)  
                       FOR [DURATION] IN (A,  
                                          F,  
                                          H) )AS pvt  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = EmployeeID  
  
          UPDATE TA  
          SET    TA.A = CASE  
                          WHEN TC.A < 0 THEN  
                            0  
                          ELSE  
                            TC.A  
                        END,  
                 TA.F = TC.F,  
                 TA.H = TC.H,  
                 TA.AL = TC.AL,  
                 TA.TH = TC.TH  
          FROM   ##TempAttendenceResult TA  
                 JOIN #TempAttendenceCount TC  
                   ON TC.EmployeeID = TA.EmployeeID  
  
          IF( @IsProfile = 0 )  
            BEGIN  
                EXEC('SELECT EmployeeID AS ID, Name AS [Employee_Name], A, F AS P, H AS HD,AL AS [Approved Leave], TH AS [Total_Hours], ' + @RESULTCOLUMN + ' FROM   ##TempAttendenceResult')  
            END  
  
          EXEC('TRUNCATE TABLE tbl_LopDetails_Process;      
     INSERT INTO tbl_LopDetails_Process (EmployeeId,MonthID, Year,Absent, Present, Halfday )       
     SELECT EmployeeID, ' + @MonthID +',' + @YearCode + ', ISNULL(A, 0) AS Absent, ISNULL(F, 0) AS Present, ISNULL(H, 0) AS Halfday FROM   ##TempAttendenceResult')  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END   