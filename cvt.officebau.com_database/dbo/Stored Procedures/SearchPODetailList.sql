﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :        
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>        
 </summary>                                   
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[SearchPODetailList] (@POID INT,        
                                                 @DomainID  INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          SELECT ed.ID       AS ID,        
                 ed.LedgerID AS LedgerID,        
                 ed.Remarks  AS [Description],  QTY,IsLedger AS IsProduct,      
                 ed.Amount   AS Amount,Isnull(CGST,0) CGST,Isnull(CGSTPercent,0) CGSTPercent,Isnull(SGST,0) SGST,      
     Isnull(SGSTPercent,0) SGSTPercent        
          FROM   tblPODetails ed        
          WHERE  ed.IsDeleted = 0        
                 AND @POID = ed.POID And ed.DomainID=@DomainID        
        
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
