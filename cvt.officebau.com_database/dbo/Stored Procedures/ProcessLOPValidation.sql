﻿
/****************************************************************************   
CREATED BY   : 
CREATED DATE  :    
MODIFIED BY   :   Ajith N
MODIFIED DATE  :   29 Dec 2017
 <summary>       
	 ProcessLOPValidation 12 , 3, 0, 1, 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ProcessLOPValidation](@MonthID      INT,
                                             @Year         INT,
                                             @BusinessUnit VARCHAR(50),
                                             @DomainID     INT,
                                             @UserID       INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @BusinessUnitIDs VARCHAR(20),
                  @Output          VARCHAR(50) = 'Already Exists.',
                  @FinancialYear   INT

          IF( ISNULL(@BusinessUnit, '') = '' )
            SET @BusinessUnitIDs = (SELECT BusinessUnitID
                                    FROM   tbl_EmployeeMaster
                                    WHERE  ISNULL(IsDeleted, 0) = 0
                                           AND ISNULL(IsActive, 0) = 0
                                           AND ID = @UserID
                                           AND DomainID = @DomainID)
          ELSE
            SET @BusinessUnitIDs = Cast(@BusinessUnit AS VARCHAR(50))

          SET @FinancialYear = (SELECT Cast(FinancialYear AS INT)
                                FROM   tbl_FinancialYear
                                WHERE  ID = @Year
                                       AND IsDeleted = 0
                                       AND DomainID = @DomainID)

          IF( @BusinessUnitIDs <> '' )
            BEGIN
                IF NOT EXISTS(SELECT 1
                              FROM   tbl_LopDetails LD
                                     LEFT JOIN tbl_EmployeeMaster EM
                                            ON EM.ID = LD.EmployeeId
                                     JOIN tbl_pay_EmployeePayroll EP
                                       ON EM.ID = EP.EmployeeId
                                          AND LD.MonthID = EP.MonthId
                                          AND LD.[Year] = EP.[YearId]
                              WHERE  EM.BaseLocationID IN (SELECT Item
                                                           FROM   DBO.Splitstring(@BusinessUnitIDs, ','))
                                     AND LD.MonthID = @MonthID
                                     AND LD.[Year] = @FinancialYear
                                     AND LD.DomainId = @DomainID
                                     AND EP.IsDeleted = 0)
                  BEGIN
                      SET @Output = 'success'
                  END
            END
          ELSE IF NOT EXISTS(SELECT 1
                        FROM   tbl_LopDetails LD
                               JOIN tbl_pay_EmployeePayroll EP
                                 ON LD.MonthID = EP.MonthId
                                    AND LD.[Year] = EP.[YearId]
                        WHERE  LD.MonthID = @MonthID
                               AND LD.[Year] = @FinancialYear
                               AND LD.DomainId = @DomainID
                               AND EP.IsDeleted = 0)
            BEGIN
                SET @Output = 'success'
            END

          SELECT @Output
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
