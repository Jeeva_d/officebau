﻿/****************************************************************************                         
CREATED BY  :                         
CREATED DATE :                         
MODIFIED BY  :                 
MODIFIED DATE :            
 <summary>                                
  [Manageaccountreceipts] 1,2,3,'aa','a',8,122,1,2,2,1,'sd',2,1,1,1,1                  
 </summary>                                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Manage_receipts] (@ID            INT,
                                         @Date          DATETIME,
                                         @Description   VARCHAR(1000),
                                         @PartyName     VARCHAR(100),
                                         @TotalAmount   MONEY,
                                         @PaymentModeID INT,
                                         @BankID        INT,
                                         @LedgerID      INT,
                                         @IsDeleted     BIT,
                                         @SessionID     INT,
                                         @DomainID      INT,
                                         @Payment       PAYMENT Readonly)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT            VARCHAR(100),
                  @InvoicePaymentID  INT,
                  @InvoiceID         INT,
                  @cash              MONEY,
                  @TransactionTypeID INT =(SELECT ID
                    FROM   tblMasterTypes
                    WHERE  NAME = 'Receive Payment'
                           AND IsDeleted = 0),
                  @PreviousCash      MONEY = (SELECT TotalAmount
                     FROM   tbl_Receipts
                     WHERE  ID = @ID
                            AND DomainID = @DomainID),
                  @PreviousMode      INT = (SELECT ModeID
                     FROM   tbl_Receipts
                     WHERE  isdeleted = 0
                            AND ID = @ID
                            AND DomainID = @DomainID)

          IF( (SELECT Count(1)
               FROM   tblCashBucket
               WHERE  DomainID = @DomainID) = 0 )
            INSERT INTO tblCashBucket
                        (AvailableCash,
                         DomainID)
            VALUES      (0,
                         @DomainID)

          IF( @IsDeleted = 1 )
            BEGIN
                IF EXISTS (SELECT 1
                           FROM   tblbrs
                           WHERE  SourceID = @ID
                                  AND SourceType = @TransactionTypeID
                                  AND IsDeleted = 0
                                  AND IsReconsiled = 1
                                  AND DomainID = @DomainID)
                  BEGIN
                      SET @Output = (SELECT [Message]
                                     FROM   tblErrorMessage
                                     WHERE  [Type] = 'Information'
                                            AND Code = 'RCD_RECON'
                                            AND IsDeleted = 0) --'The record has been Reconciled.'                
                      GOTO finish
                  END
                ELSE
                  BEGIN
                      IF( @PaymentModeID = (SELECT ID
                                            FROM   tbl_CodeMaster
                                            WHERE  Code = 'Cash'
                                                   AND IsDeleted = 0) )
                        BEGIN
                            UPDATE tblCashBucket
                            SET    AvailableCash = ( AvailableCash - @TotalAmount ),
                                   ModifiedOn = Getdate()
                            WHERE  @DomainID = DomainID

                            UPDATE tblVirtualCash
                            SET    IsDeleted = 1
                            WHERE  SourceID = @ID
                                   AND SourceType = (SELECT ID
                                                     FROM   tblMasterTypes
                                                     WHERE  NAME = 'Receive Payment'
                                                            AND IsDeleted = 0)
                                   AND IsDeleted = 0
                        END

                      UPDATE tbl_Receipts
                      SET    IsDeleted = 1
                      WHERE  ID = @ID

                      UPDATE tbl_ReceiptsMapping
                      SET    IsDeleted = 1
                      WHERE  paymentid = @ID

                      UPDATE tblBookedBankBalance
                      SET    IsDeleted = 1
                      WHERE  BRSID = (SELECT ID
                                      FROM   tblBRS
                                      WHERE  SourceID = @ID
                                             AND SourceType = @TransactionTypeID
                                             AND IsDeleted = 0
                                             AND DomainID = @DomainID)

                      UPDATE tblBRS
                      SET    IsDeleted = 1
                      WHERE  SourceID = @ID
                             AND SourceType = @TransactionTypeID
                             AND DomainID = @DomainID

                      EXEC Managesystembankbalance
                        @DomainID
                  END

                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Information'
                                      AND Code = 'RCD_DEL'
                                      AND IsDeleted = 0) -- 'Deleted Successfully'                
                GOTO finish
            END
          ELSE
            BEGIN
                IF( @ID = 0 )
                  BEGIN
                      INSERT INTO tbl_Receipts
                                  (PartyName,
                                   PaymentDate,
                                   [Description],
                                   ModeID,
                                   BankID,
                                   LedgerID,
                                   DomainID,
                                   CreatedBy,
                                   ModifiedBy,
                                   TotalAmount)
                      VALUES      (@PartyName,
                                   @Date,
                                   @Description,
                                   @PaymentModeID,
                                   @BankID,
                                   @LedgerID,
                                   @DomainID,
                                   @SessionID,
                                   @SessionID,
                                   @TotalAmount)

                      DECLARE @SourceID INT =@@IDENTITY

                      INSERT INTO tbl_ReceiptsMapping
                                  (PaymentId,
                                   RefId,
                                   Amount,
                                   CreatedBy,
                                   CreatedOn,
                                   DomainId)
                      SELECT @SourceID,
                             refid,
                             amount,
                             @SessionID,
                             @SessionID,
                             @DomainID
                      FROM   @Payment

                      IF( Isnull(@BankID, 0) <> 0 )
                        BEGIN
                            INSERT INTO tblBRS
                                        (BankID,
                                         SourceID,
                                         SourceDate,
                                         SourceType,
                                         Amount,
                                         DomainID,
                                         BRSDescription,
                                         CreatedBy,
                                         ModifiedBy)
                            VALUES      (@BankID,
                                         @SourceID,
                                         @Date,
                                         (SELECT ID
                                          FROM   tblMasterTypes
                                          WHERE  NAME = 'Receive Payment'
                                                 AND IsDeleted = 0),
                                         @TotalAmount,
                                         @DomainID,
                                         @Description,
                                         @SessionID,
                                         @SessionID)

                            INSERT INTO tblBookedBankBalance
                                        (BRSID,
                                         Amount,
                                         DomainID,
                                         CreatedBy,
                                         ModifiedBy)
                            VALUES      (@@IDENTITY,
                                         @TotalAmount,
                                         @DomainID,
                                         @SessionID,
                                         @SessionID)

                            EXEC Managesystembankbalance
                              @DomainID
                        END

                      IF( @PaymentModeID = (SELECT ID
                                            FROM   tbl_CodeMaster
                                            WHERE  Code = 'Cash'
                                                   AND IsDeleted = 0) )
                        BEGIN
                            UPDATE tblCashBucket
                            SET    AvailableCash = ( AvailableCash + @TotalAmount ),
                                   ModifiedOn = Getdate()
                            WHERE  @DomainID = DomainID

                            INSERT INTO tblVirtualCash
                                        (SourceID,
                                         Amount,
                                         [Date],
                                         SourceType,
                                         DomainID,
                                         CreatedBy,
                                         ModifiedBy)
                            VALUES      (@SourceID,
                                         @TotalAmount,
                                         @Date,
                                         (SELECT ID
                                          FROM   tblMasterTypes
                                          WHERE  NAME = 'Receive Payment'
                                                 AND IsDeleted = 0),
                                         @DomainID,
                                         @SessionID,
                                         @SessionID)
                        END

                      SET @Output = (SELECT [Message]
                                     FROM   tblErrorMessage
                                     WHERE  [Type] = 'Information'
                                            AND Code = 'RCD_INS'
                                            AND IsDeleted = 0) --'Inserted Successfully'                
                  END
                ELSE
                  BEGIN
                      --'The record has been Reconciled.'                
                      IF EXISTS (SELECT 1
                                 FROM   tblbrs
                                 WHERE  SourceID = @ID
                                        AND SourceType = @TransactionTypeID
                                        AND IsDeleted = 0
                                        AND IsReconsiled = 1
                                        AND DomainID = @DomainID)
                        BEGIN
                            SET @Output = (SELECT [Message]
                                           FROM   tblErrorMessage
                                           WHERE  [Type] = 'Information'
                                                  AND Code = 'RCD_UPD_DES'
                                                  AND IsDeleted = 0) --'The record is referred. Description is Updated.'                
                            UPDATE tbl_Receipts
                            SET    [Description] = @Description,
                                   ModifiedBy = @SessionID,
                                   LedgerID = @LedgerID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID
                                   AND DomainID = @DomainID

                            UPDATE tblbrs
                            SET    BRSDescription = @Description
                            WHERE  SourceID = @ID
                                   AND SourceType = @TransactionTypeID
                                   AND DomainID = @DomainID

                            GOTO finish
                        END
                      ELSE
                        BEGIN
                            UPDATE tbl_Receipts
                            SET    PartyName = @PartyName,
                                   PaymentDate = @Date,
                                   Description = @Description,
                                   TotalAmount = @TotalAmount,
                                   ModeID = @PaymentModeID,
                                   BankID = Isnull(@BankID, 0),
                                   LedgerID = @LedgerID,
                                   ModifiedBy = @SessionID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID
                                   AND DomainID = @DomainID

                            IF(SELECT Count(*)
                               FROM   @Payment
                               WHERE  ID <> 0) <> 0
                              BEGIN
                                  UPDATE tbl_ReceiptsMapping
                                  SET    Amount = Isnull(r.Amount,0)
                                  FROM   tbl_ReceiptsMapping p
                                         LEFT JOIN @Payment r
                                                ON r.ID = p.Id
                                  WHERE  p.Isdeleted = 0 AND p.RefId = r.RefId
                              END

                            IF( Isnull(@BankID, 0) <> 0 )
                              BEGIN
                                  IF EXISTS(SELECT 1
                                            FROM   tblBRS
                                            WHERE  SourceID = @ID
                                                   AND SourceType = @TransactionTypeID
                                                   AND DomainID = @DomainID
                                                   AND IsDeleted = 0)
                                    BEGIN
                                        UPDATE tblbrs
                                        SET    Amount = @TotalAmount,
                                               BRSDescription = @Description,
                                               BankID = @BankID,
                                               SourceType = (SELECT ID
                                                             FROM   tblMasterTypes
                                                             WHERE  NAME = 'Receive Payment'
                                                                    AND IsDeleted = 0)
                                        WHERE  SourceID = @ID
                                               AND SourceType = @TransactionTypeID
                                               AND DomainID = @DomainID

                                        UPDATE tblBookedBankBalance
                                        SET    Amount = @TotalAmount
                                        WHERE  BRSID = (SELECT ID
                                                        FROM   tblBRS
                                                        WHERE  SourceID = @ID
                                                               AND SourceType = @TransactionTypeID
                                                               AND IsDeleted = 0
                                                               AND DomainID = @DomainID)
                                               AND DomainID = @DomainID

                                        EXEC Managesystembankbalance
                                          @DomainID
                                    END
                                  ELSE
                                    BEGIN
                                        INSERT INTO tblBRS
                                                    (BankID,
                                                     SourceID,
                                                     SourceDate,
                                                     SourceType,
                                                     Amount,
                                                     BRSDescription)
                                        VALUES      (@BankID,
                                                     @ID,
                                                     @Date,
                                                     (SELECT ID
                                                      FROM   tblMasterTypes
                                                      WHERE  NAME = 'Receive Payment'
                                                             AND IsDeleted = 0),
                                                     @TotalAmount,
                                                     @Description)

                                        INSERT INTO tblBookedBankBalance
                                                    (BRSID,
                                                     Amount,
                                                     DomainID,
                                                     CreatedBy,
                                                     ModifiedBy)
                                        VALUES      (@@IDENTITY,
                                                     @TotalAmount,
                                                     @DomainID,
                                                     @SessionID,
                                                     @SessionID)

                                        EXEC Managesystembankbalance
                                          @DomainID

                                        UPDATE tblCashBucket
                                        SET    AvailableCash = ( AvailableCash - @TotalAmount ),
                                               ModifiedOn = Getdate()
                                        WHERE  @DomainID = DomainID

                                        UPDATE tblVirtualCash
                                        SET    IsDeleted = 1
                                        WHERE  SourceID = @ID
                                               AND SourceType = (SELECT ID
                                                                 FROM   tblMasterTypes
                                                                 WHERE  NAME = 'Receive Payment'
                                                                        AND IsDeleted = 0)
                                               AND IsDeleted = 0
                                               AND DomainID = @DomainID
                                    END
                              END
                            ELSE
                              BEGIN
                                  IF( (SELECT 1
                                       FROM   tblBRS
                                       WHERE  SourceID = @ID
                                              AND SourceType = @TransactionTypeID
                                              AND DomainID = @DomainID
                                              AND IsDeleted = 0) = 1 )
                                    BEGIN
                                        UPDATE tblBookedBankBalance
                                        SET    IsDeleted = 1
                                        WHERE  BRSID = (SELECT ID
                                                        FROM   tblBRS
                                                        WHERE  SourceID = @ID
                                                               AND SourceType = @TransactionTypeID
                                                               AND IsDeleted = 0)
                                               AND DomainID = @DomainID

                                        UPDATE tblBRS
                                        SET    IsDeleted = 1
                                        WHERE  SourceID = @ID
                                               AND SourceType = @TransactionTypeID
                                               AND DomainID = @DomainID
                                               AND IsDeleted = 0

                                        EXEC Managesystembankbalance
                                          @DomainID
                                    END
                              END

                            IF( @PaymentModeID = (SELECT ID
                                                  FROM   tbl_CodeMaster
                                                  WHERE  Code = 'Cash'
                                                         AND IsDeleted = 0) )
                              BEGIN
                                  IF ( @PreviousMode = (SELECT ID
                                                        FROM   tbl_CodeMaster
                                                        WHERE  Code = 'Cash'
                                                               AND IsDeleted = 0) )
                                    BEGIN
                                        UPDATE tblCashBucket
                                        SET    AvailableCash = ( AvailableCash - @PreviousCash + @TotalAmount ),
                                               ModifiedOn = Getdate()
                                        WHERE  @DomainID = DomainID

                                        UPDATE tblVirtualCash
                                        SET    Amount = ( @TotalAmount )
                                        WHERE  SourceID = @ID
                                               AND SourceType = (SELECT ID
                                                                 FROM   tblMasterTypes
                                                                 WHERE  NAME = 'Receive Payment'
                                                                        AND IsDeleted = 0)
                                               AND IsDeleted = 0
                                               AND DomainID = @DomainID
                                    END
                                  ELSE
                                    BEGIN
                                        UPDATE tblCashBucket
                                        SET    AvailableCash = ( AvailableCash + @TotalAmount ),
                                               ModifiedOn = Getdate()
                                        WHERE  @DomainID = DomainID

                                        INSERT INTO tblVirtualCash
                                                    (SourceID,
                                                     Amount,
                                                     Date,
                                                     SourceType,
                                                     DomainID,
                                                     CreatedBy,
                                                     ModifiedBy)
                                        VALUES      (@ID,
                                                     ( @TotalAmount ),
                                                     @Date,
                                                     (SELECT ID
                                                      FROM   tblMasterTypes
                                                      WHERE  NAME = 'Receive Payment'
                                                             AND IsDeleted = 0),
                                                     @DomainID,
                                                     @SessionID,
                                                     @SessionID)
                                    END
                              END

                            SET @Output = (SELECT [Message]
                                           FROM   tblErrorMessage
                                           WHERE  [Type] = 'Information'
                                                  AND Code = 'RCD_UPD'
                                                  AND IsDeleted = 0) --'Updated Successfully.'                
                            GOTO finish
                        END
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
