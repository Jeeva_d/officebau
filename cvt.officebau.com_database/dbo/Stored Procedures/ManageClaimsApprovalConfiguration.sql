﻿/****************************************************************************         
CREATED BY   : Dhanalakshmi.S        
CREATED DATE  : 03 OCT 2018        
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>         
      
 </summary>                                 
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageClaimsApprovalConfiguration] (@ID         INT,  
                                                            @EmployeeID INT,  
                                                            @ApproverID INT,  
                                                            @SessionID  INT,  
                                                            @DomainID   INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(100) = 'Operation Failed!'  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          IF( @ID = 0 )  
            BEGIN  
                INSERT INTO tbl_ClaimsApproverConfiguration  
                            (EmployeeID,  
                             ApproverID,  
                             CreatedBy,  
                             CreatedOn,  
                             ModifiedBy,  
                             ModifiedOn,  
                             DomainID)  
                VALUES      (@EmployeeID,  
                             @ApproverID,  
                             @SessionID,  
                             Getdate(),  
                             @SessionID,  
                             Getdate(),  
                             @DomainID)  
  
                SET @Output = 'Inserted Successfully.'  
                GOTO FINISH
            END  
          ELSE  
            BEGIN  
                UPDATE tbl_ClaimsApproverConfiguration  
                SET    EmployeeID = @EmployeeID,  
                       ApproverID = @ApproverID,  
                       ModifiedBY = @SessionID,  
                       ModifiedOn = Getdate()  
                WHERE  ID = @ID  
  
                SET @Output = 'Updated Successfully.' 
                GOTO FINISH 
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
