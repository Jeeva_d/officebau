﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>     
		select * from TDSSection
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetTDSComponent] (@ID       INT,
                                         @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT comp.ID,
                 comp.SectionID,
                 comp.Code,
                 comp.Rules,
                 comp.Value,
                 comp.[Description],
				 comp.FYID AS FinancialYearID
          FROM   TDSComponent comp
          WHERE  comp.IsDeleted = 0
                 AND comp.ID = @ID
                 AND comp.DomainID = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
