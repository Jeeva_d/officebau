﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>    
 [Tds_sectiondetailsengineNOLimit] 12,4
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Tds_sectiondetailsengineNOLimit] (@EmployeeID INT,
                                                         @FYID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @DomainID INT =(SELECT DomainID
        FROM   tbl_EmployeeMaster
        WHERE  id = @EmployeeID)
      DECLARE @RegionID INT=(SELECT RegionID
        FROM   tbl_EmployeeMaster
        WHERE  id = @EmployeeID)
      DECLARE @DateOfJoin DATE =(SELECT DOJ
        FROM   tbl_EmployeeMaster
        WHERE  ID = @EmployeeID)
      DECLARE @ProofCloseDate DATE = (SELECT '31-Mar-' + Cast( Year(Getdate()) AS VARCHAR)
         FROM   TDSConfiguration
         WHERE  IsDeleted = 0
                AND Code = 'TDSPCD'
                AND RegionID = @RegionID
                AND DomainID = @DomainID)
      DECLARE @ProofOpenDate DATE = (SELECT VALUE
         FROM   TDSConfiguration
         WHERE  IsDeleted = 0
                AND Code = 'TDSPOD'
                AND RegionID = @RegionID
                AND DomainID = @DomainID)
      DECLARE @IsProofOpen BIT = CASE
          WHEN CONVERT(DATE, Getdate()) BETWEEN @ProofOpenDate AND @ProofCloseDate THEN 1
          ELSE 0
        END

      BEGIN TRY
          SELECT CASE
                   WHEN( @IsProofOpen = 1 ) THEN ( Isnull(TDS.Cleared, 0) )
                   ELSE Isnull(TDS.Declaration, 0)
                 END     AS Declaration,
                 TC.Code AS Component,
                 ts.Code AS Section
          FROM   TDSDeclaration TDS
                 JOIN TDSComponent TC
                   ON TDS.ComponentID = TC.ID  and TC.FYID = @FYID
                      AND TC.SectionID IN (SELECT ID 
                                           FROM   TDSSection
                                           WHERE  Code NOT LIKE '%80C%'
                                                  AND Code NOT LIKE '%Mediclaim%'
                                                  AND Code NOT LIKE '%80DD%'
                                                  AND Code NOT LIKE '%80E'
                                                  AND Code NOT LIKE '%80G'
                                                  AND Code NOT LIKE '%80TTA%'
                                                  AND Code NOT LIKE '%80TTB%'
                                                  AND Code NOT LIKE '%80U%'
                                                  AND Code NOT LIKE '%24%'
                                                  AND IsDeleted = 0)
                                                  --AND DomainID = @DomainID)
                 JOIN TDSSection ts
                   ON ts.ID = TC.SectionID
          WHERE  EmployeeID = @EmployeeID
                 AND FinancialYearID = @FYID
                 AND TDS.DomainID = @DomainID
                 AND tc.Code NOT LIKE '%Is Senior citizens included.%'
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
