﻿/****************************************************************************                 
CREATED BY  :                 
CREATED DATE :             
MODIFIED BY  :                 
MODIFIED DATE :                 
 <summary>              
[Timewatchupload_Temp] 10,'26-Dec-19','09:13,11:41,13:08,13:35,16:32,18:19',1,1            
 </summary>                                         
 *****************************************************************************/            
CREATE PROCEDURE [dbo].[Timewatchupload_Temp] (@TIMEWATCH TIMEWATCHUPLOAD READONLY,            
                                         @SessionID INT,            
                                         @DomainID  INT)            
AS            
  BEGIN            
      SET NOCOUNT ON;            
            
      DECLARE @Output VARCHAR(50)            
            
      BEGIN TRY            
          BEGIN TRANSACTION            
            
          SET @Output = 'One or more of the Entry cannot be Inserted/Updated!'            
            
          SELECT Row_number()            
                   OVER (            
                     ORDER BY BiometricID, DATE ASC) AS ID,            
                 BiometricID,            
                 date,            
                 Punches=( Stuff((SELECT ',' + Isnull(Punch1, '') + ',' + Isnull(Punch2, '')            
                                         + ',' + Isnull(Punch3, '') + ',' + Isnull(Punch4, '')            
                                  FROM   @TIMEWATCH b            
                                  WHERE  b.date = a.date            
                                         AND b.BiometricID = a.BiometricID            
                                  FOR xml path('')), 1, 1, '') )            
          INTO   #tempTimewatch            
          FROM   @TIMEWATCH a            
          GROUP  BY date,            
                    BiometricID            
            
          CREATE TABLE #temppunches            
            (            
               id      INT IDENTITY(1, 1),          
      EmployeeID INT ,            
      LogDate date,          
               punches Time            
            )            
            
          DECLARE @count         INT=1,            
                  @Punches       VARCHAR(max),            
                  @BiometricCode VARCHAR(20),            
                  @LogDate       DATE,            
                  @EmployeeID    INT,          
      @AttendanceID int =0             
            
          WHILE( @count <= (SELECT Count(1)            
                            FROM   #tempTimewatch) )            
            BEGIN            
                SELECT @BiometricCode = Rtrim(Ltrim(BiometricID)),            
                       @LogDate = Date,            
                       @Punches = Replace(Punches, ',,', '')            
                FROM   #tempTimewatch            
                WHERE  id = @count            
            
                SET @EmployeeID=(SELECT ID            
                                 FROM   tbl_EmployeeMaster            
                                 WHERE  IsDeleted = 0            
                                        AND IsActive = 0            
                                        AND BiometricCode = @BiometricCode)            
           
                INSERT INTO #temppunches  (EmployeeID,LogDate,punches)          
                SELECT @EmployeeID,@LogDate, Cast(ITEM As Time)            
                FROM   dbo.Splitstring (LEFT(@Punches + ',,', Charindex(',,', @Punches + ',,') - 1), ',')            
            
               IF Exists(Select 1 from tbl_Attendance where EmployeeID=@EmployeeID and LogDate=@LogDate and IsDeleted=0)          
      Begin           
     Set @AttendanceID = (Select ID from tbl_Attendance where EmployeeID=@EmployeeID and LogDate=@LogDate and IsDeleted=0)          
                
         Insert INto tbl_BiometricLogs (AttendanceId,Direction,PunchTime,LogType)          
      Select @AttendanceID,'In',punches,'Device' from #temppunches          
      Where punches not IN  (Select PunchTime from tbl_BiometricLogs where AttendanceId=@AttendanceID and LogType='Device')          
    Set @Output = 'The biometric details are updated successfully.'      
      End          
      ELSE          
             Begin           
   IF @EmployeeID IS not null           
 Begin    
     INSERT INTO tbl_Attendance (EmployeeID, LogDate,DomainID,CreatedBy,ModifiedBy)          
       Select @EmployeeID,@LogDate,@DomainID,@SessionID,@SessionID          
      Set @AttendanceID  = @@IDENTITY          
         Insert INto tbl_BiometricLogs (AttendanceId,Direction,PunchTime,LogType)          
      Select @AttendanceID,'In',punches,'Device' from #temppunches          
      Where punches not IN  (Select PunchTime from tbl_BiometricLogs where AttendanceId=@AttendanceID and LogType='Device')          
    Set @Output = 'The biometric details are updated successfully.'      
 END     
      End          
          
                TRUNCATE TABLE #temppunches            
            
                SET @count=@count + 1            
            END         
          SELECT @Output AS [Output]            
            
    EXEC UpdateBiometricWorkedHours @domainID        
            
          COMMIT TRANSACTION            
      END TRY            
      BEGIN CATCH            
          ROLLBACK TRANSACTION            
          DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT            
          SELECT @ErrorMsg = Error_message(),            
                 @ErrSeverity = Error_severity()            
          RAISERROR(@ErrorMsg,@ErrSeverity,1)            
      END CATCH            
  END
