﻿/****************************************************************************     
CREATED BY   :     
CREATED DATE  :     
MODIFIED BY   : Naneeshwar    
MODIFIED DATE  :     
 <summary>        
 [TDS_SectionDETAILSENGINE] 5 ,'80C',4   
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[TDS_SectionDETAILSENGINE] (@EmployeeID INT,    
                                                  @Section    VARCHAR(100),    
                                                  @FYID       INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @DomainID INT =(SELECT DomainID    
        FROM   tbl_EmployeeMaster    
        WHERE  id = @EmployeeID)    
      DECLARE @RegionID INT=(SELECT RegionID    
        FROM   tbl_EmployeeMaster    
        WHERE  id = @EmployeeID)    
      DECLARE @DateOfJoin DATE =(SELECT DOJ    
        FROM   tbl_EmployeeMaster    
        WHERE  ID = @EmployeeID)    
      DECLARE @ProofCloseDate DATE = (SELECT '31-Mar-' + Cast( Year(Getdate()) AS VARCHAR)    
         FROM   TDSConfiguration    
         WHERE  IsDeleted = 0    
                AND Code = 'TDSPCD'    
                AND RegionID = @RegionID    
                AND DomainID = @DomainID)    
      DECLARE @ProofOpenDate DATE = (SELECT VALUE    
         FROM   TDSConfiguration    
         WHERE  IsDeleted = 0    
                AND Code = 'TDSPOD'    
                AND RegionID = @RegionID    
                AND DomainID = @DomainID)    
      DECLARE @IsProofOpen BIT = CASE    
          WHEN CONVERT(DATE, Getdate()) BETWEEN @ProofOpenDate AND @ProofCloseDate THEN 1    
          ELSE 0    
        END    
      DECLARE @YEARID INT=(SELECT Cast(NAME AS INT)    
        FROM   tbl_FinancialYear    
        WHERE  id = @FYID    
               AND DomainID = @DomainID)    
      DECLARE @EPFACTUAL MONEY=0    
      DECLARE @EPFPRO MONEY=Cast(0 AS Decimal)   
      DECLARE @NoofMonths INT =(SELECT 12 - Count(*)    
        FROM   tbl_TDS t    
               JOIN tbl_Pay_EmployeePayroll ep    
                 ON ep.EmployeeId = t.EmployeeId    
                    AND ep.IsProcessed = 1    
                    AND ep.IsDeleted = 0    
                    AND ep.MonthId = t.MonthId    
        WHERE  t.EmployeeId = @EmployeeID    
               AND t.IsDeleted = 0    
               AND t.YearID = @FYID    
               AND t.DomainID = @DomainID)    
    
      BEGIN TRY    
          SELECT CASE    
                   WHEN( @IsProofOpen = 1 )    
                       AND tc.Code <> 'Is Senior citizens included.' THEN ( CASE    
                                                                              WHEN( @IsProofOpen = 1 ) THEN Isnull(TDS.Cleared, 0)    
                                                                              ELSE Isnull(TDS.Declaration, 0)    
                                                                            END )    
                   WHEN( @IsProofOpen = 1 )    
                       AND tc.Code = 'Is Senior citizens included.' THEN 1    
                   ELSE Isnull(TDS.Declaration, 0)    
                 END                                    AS Declaration,    
                 TC.Code                                AS Component,    
                 ts.Code                                AS Section,    
                Isnull(@EPFACTUAL, 0) + ( Isnull(@EPFPRO, 0) * Isnull(@NoofMonths, 0) ) AS EEPF    
          FROM   TDSDeclaration TDS    
                 JOIN TDSComponent TC    
                   ON TDS.ComponentID = TC.ID AND TC.FYID = @FYID    
                      AND TC.SectionID IN (SELECT ID    
                                           FROM   TDSSection    
                                           WHERE  Code LIKE '%' + @Section + '%'    
                                                  AND IsDeleted = 0)    
                 JOIN TDSSection ts    
                   ON tc.SectionID = ts.ID    
          WHERE  EmployeeID = @EmployeeID    
                 AND FinancialYearID = @FYID    
                 AND TDS.DomainID = @DomainID    
          UNION ALL    
          SELECT 0                                                                       AS Declaration,    
                 '80C'                                                                   AS Component,    
                 '80C'                                                                   AS Section,    
                 Isnull(@EPFACTUAL, 0) + ( Isnull(@EPFPRO, 0) * Isnull(@NoofMonths, 0) ) AS EEPF    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END  