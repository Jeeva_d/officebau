﻿/**************************************************************************** 
CREATED BY    		:	Ajith N
CREATED DATE		:	21 Nov 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
		UploadTDSComponent '1295','A. Gomathi Nayagam ',2017,1,1,2,106,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[UploadTDSComponent] (@EmployeeCode VARCHAR(20),
                                            @EmployeeName VARCHAR(50),
                                            @Year         INT,
                                            @Bonus        MONEY,
                                            @PLI          MONEY,
                                            @OtherIncome  MONEY,
                                            @UserID       INT,
                                            @DomainID     INT)
AS
  BEGIN
      BEGIN TRY
          SET NOCOUNT ON;

          DECLARE @Output     VARCHAR(100) = '',
                  @EmployeeID INT

          SET @EmployeeID=(SELECT ID
                           FROM   tbl_EmployeeMaster
                           WHERE  IsDeleted = 0
                                  AND Isnull(IsActive, 0) = 0
                                  AND Code = @EmployeeCode
                                  AND Replace(( Rtrim(Ltrim(FirstName))
                                                + Rtrim(Ltrim(Isnull(LastName, ''))) ), ' ', '') = Replace(@EmployeeName, ' ', '')
                                  AND DomainID = @DomainID)

          BEGIN TRANSACTION

          IF ( @Bonus <> 0
                OR @PLI <> 0
                OR @OtherIncome <> 0 )
            BEGIN
                INSERT INTO tbl_TDSEmployeeOtherIncome
                            (EmployeeID,
                             Year,
                             Bonus,
                             PLI,
                             OtherIncome,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             DomainId,
                             HistoryID)
                VALUES      (@EmployeeID,
                             @Year,
                             @Bonus,
                             @PLI,
                             @OtherIncome,
                             @UserID,
                             Getdate(),
                             @UserID,
                             Getdate(),
                             @DomainID,
                             Newid())

                SET @Output = 'Uploaded Successfully.'
            END

          COMMIT TRANSACTION

          SELECT @Output AS [Output]
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
