﻿/****************************************************************************             
CREATED BY    : Priya K          
CREATED DATE  : 20-Nov-2018             
MODIFIED BY   :            
MODIFIED DATE :            
 <summary>            
 </summary>                                     
 *****************************************************************************/
create PROCEDURE [dbo].[UpdateStockAdjustment] (@ID                 INT,
                                       @LedgerID         INT,
                                       @AdjustedDate        DATETIME,
                                       @ReasonID    INT,
                                       @Remarks              VARCHAR(100) = NULL,
                                       @DomainID           INT,
									   @SessionID          INT,
                                       @StockAdjustmentItem   StockAdjustmentItem READONLY)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT    VARCHAR(100),
                  @AdjustmentID INT

          SET @AdjustmentID = @ID

          BEGIN
              UPDATE tbl_InventoryStockAdjustment
              SET    LedgerID = @LedgerID,
                     AdjustedDate = @AdjustedDate,
                     ReasonID = @ReasonID,
                     Remarks = @Remarks,
                     ModifiedBy = @SessionID,
                     ModifiedOn = Getdate()
              WHERE  ID = @ID
                     AND DomainID = @DomainID

              IF ( (SELECT Count(1)
                    FROM   @StockAdjustmentItem) > 0 )
                BEGIN
                    UPDATE  isa
                    SET    isa.ProductID = I.ItemProductID,
                           isa.AdjustedQty = I.AdjustedQty,
                           isa.IsDeleted = I.IsDeleted,
                           ModifiedBy = @SessionID,
                           ModifiedOn = Getdate()
                    FROM   tbl_InventoryStockAdjustmentItems isa
                           JOIN @StockAdjustmentItem AS I
                                   ON isa.ID = I.ID
                                      AND I.ItemProductID <> 0

                    INSERT INTO tbl_InventoryStockAdjustmentItems
                            (AdjustmentID,
                             ProductID,
                             AdjustedQty,
							 RemainingQty,
                             DomainID,
                             CreatedBy,
                             ModifiedBy)
                SELECT     @ID,
                             I.ItemProductID,
                             I.AdjustedQty,
							 I.RemainingQty,
                             @DomainID,
                            @SessionID,
                            @SessionID
                 FROM   tbl_InventoryStockAdjustmentItems isa
                           left JOIN @StockAdjustmentItem AS I
                                   ON I.ID = isa.ID AND I.IsDeleted = 0
				  where isnull(I.ID,0) = 0
							AND  ISNULL(I.ItemProductID,0) <> 0	  
                END
          END

          SET @OUTPUT = (SELECT [Message]
                         FROM   tblErrorMessage
          WHERE  [Type] = 'Information'
                                AND Code = 'RCD_UPD'
                                AND IsDeleted = 0) --'Updated Successfully'  

        

          SELECT @OUTPUT

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
