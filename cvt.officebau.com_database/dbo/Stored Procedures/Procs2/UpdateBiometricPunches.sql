﻿/****************************************************************************            
CREATED BY  :            
CREATED DATE :            
MODIFIED BY  :            
MODIFIED DATE :            
      
        
UpdateBiometricPunches         
                                    
*****************************************************************************/  
CREATE PROCEDURE [dbo].[UpdateBiometricPunches] (@EmployeeID INT,  
                                                 @LogDate    DATE,  
                                                 @PunchType  VARCHAR(50),  
                                                 @Operation  VARCHAR(50),  
                                                 @Duration   DECIMAL(16, 2),  
                                                 @PunchTime  TIME = NULL,  
                                                 @UserID     INT,  
                                                 @DomainID   INT)  
AS  
  BEGIN  
      SET nocount ON;  
  
      BEGIN try  
          DECLARE @OutPut       VARCHAR(100) = 'Operation failed!',  
                  @ID           INT,  
                  @TimeDuration TIME  
  
          IF ( @Operation = 'Insert' )  
            BEGIN  
                IF @PunchType = 'IN'  
                  BEGIN  
                      IF NOT EXISTS(SELECT 1  
                                    FROM   tbl_attendance  
                                    WHERE  logdate = @LogDate  
                                           AND employeeid = @EmployeeID  
                                           AND isdeleted = 0)  
                        BEGIN  
                            INSERT INTO tbl_attendance  
                                        (employeeid,  
                                         logdate,  
                                         domainid,  
                                         createdby,  
                                         modifiedby)  
                            SELECT @EmployeeID,  
                                   @LogDate,  
                                   @DomainID,  
                                   @UserID,  
                                   @UserID  
  
                            SET @ID =@@IDENTITY  
  
                            INSERT INTO tbl_biometriclogs  
                                        (attendanceid,  
                                         logtype,  
                                         direction,  
                                         punchtime,  
                                         createdby,  
                                         modifiedby)  
                            SELECT @ID,  
                                   'OfficeBAU',  
                                   'IN',  
                                   @PunchTime,  
                                   @UserID,  
                                   @UserID  
                        END  
                      ELSE  
                        BEGIN  
                            INSERT INTO tbl_biometriclogs  
                                        (attendanceid,  
                                         logtype,  
                                         direction,  
                                         punchtime,  
                                         createdby,  
                                         modifiedby)  
                            SELECT id,  
                                   'OfficeBAU',  
                                   'IN',  
                                   @PunchTime,  
                                   @UserID,  
                                   @UserID  
                            FROM   tbl_attendance  
                            WHERE  logdate = @LogDate  
                                   AND employeeid = @EmployeeID  
                                   AND isdeleted = 0  
                        END  
  
                      SET @OutPut = 'Inserted Successfully.'  
                  END  
  
                IF @PunchType = 'OUT'  
                  BEGIN  
 IF NOT EXISTS(SELECT 1  
                                    FROM   tbl_attendance  
                                    WHERE  logdate = @LogDate  
                                           AND employeeid = @EmployeeID  
                                           AND isdeleted = 0)  
                        BEGIN  
                            INSERT INTO tbl_attendance  
                                        (employeeid,  
                                         logdate,  
                                         domainid,  
                                         createdby,  
                                         modifiedby)  
                            SELECT @EmployeeID,  
                                   @LogDate,  
                                   @DomainID,  
                                   @UserID,  
                                   @UserID  
  
                            SET @ID =@@IDENTITY  
  
                            INSERT INTO tbl_biometriclogs  
                                        (attendanceid,  
                                         logtype,  
                                         direction,  
                                         punchtime,  
                                         createdby,  
                                         modifiedby)  
                            SELECT @ID,  
                                   'OfficeBAU',  
                                   'OUT',  
                                   @PunchTime,  
                                   @UserID,  
                                   @UserID  
                        END  
                      ELSE  
                        BEGIN  
                            INSERT INTO tbl_biometriclogs  
                                        (attendanceid,  
                                         logtype,  
                                         direction,  
                                         punchtime,  
                                         createdby,  
                                         modifiedby)  
                            SELECT id,  
                                   'OfficeBAU',  
                                   'OUT',  
                                   @PunchTime,  
                                   @UserID,  
                                   @UserID  
                            FROM   tbl_attendance  
                            WHERE  logdate = @LogDate  
                                   AND employeeid = @EmployeeID  
                                   AND isdeleted = 0  
                        END  
  
                      SET @OutPut = 'Inserted Successfully.'  
                  END  
  
                IF @PunchType = 'FullDay'  
                  BEGIN  
                      SET @TimeDuration =(SELECT Cast(Replace(Cast(FullDay AS VARCHAR(10)), '.', ':') AS TIME(7))  
                                          FROM   tbl_BusinessHours  
                                          WHERE  RegionID = (SELECT RegionID  
                                                             FROM   tbl_EmployeeMaster  
                                                             WHERE  ID = @EmployeeID)  
                                                 AND ISdeleted = 0)  
  
                      IF NOT EXISTS(SELECT 1  
                                    FROM   tbl_attendance  
                                    WHERE  logdate = @LogDate  
                                           AND employeeid = @EmployeeID  
                                           AND isdeleted = 0)  
                        BEGIN  
                            INSERT INTO tbl_attendance  
                                        (employeeid,  
                                         logdate,  
                                         LeaveDuration,  
                                         domainid,  
                                         createdby,  
                                         modifiedby)  
                            SELECT @EmployeeID,  
                 @LogDate,  
                        @TimeDuration,  
                                   @DomainID,  
                                   @UserID,  
                                   @UserID  
  
                            SET @OutPut = 'Inserted Successfully.'  
                        END  
                      ELSE  
                        BEGIN  
                            UPDATE tbl_attendance  
                            SET    LeaveDuration = @TimeDuration,  
                                   ModifiedBy = @UserID,  
                                   ModifiedOn = Getdate()  
                            WHERE  logdate = @LogDate  
                                   AND employeeid = @EmployeeID  
                                   AND isdeleted = 0  
  
                            SET @OutPut = 'Updated Successfully.'  
                        END  
                  END  
  
                IF @PunchType = 'HalfDay'  
                  BEGIN  
                      SET @TimeDuration =(SELECT Cast(Replace(Cast(HalfDay AS VARCHAR(10)), '.', ':') AS TIME(7))  
                                          FROM   tbl_BusinessHours  
                                          WHERE  RegionID = (SELECT RegionID  
                                                             FROM   tbl_EmployeeMaster  
                                                             WHERE  ID = @EmployeeID)  
                                                 AND ISdeleted = 0)  
  
                      IF NOT EXISTS(SELECT 1  
                                    FROM   tbl_attendance  
                                    WHERE  logdate = @LogDate  
                                           AND employeeid = @EmployeeID  
                                           AND isdeleted = 0)  
                        BEGIN  
                            INSERT INTO tbl_attendance  
                                        (employeeid,  
                                         logdate,  
                                         LeaveDuration,  
                                         domainid,  
                                         createdby,  
                                         modifiedby)  
                            SELECT @EmployeeID,  
                                   @LogDate,  
                                   @TimeDuration,  
                                   @DomainID,  
                                   @UserID,  
                                   @UserID  
  
                            SET @OutPut = 'Inserted Successfully.'  
                        END  
                      ELSE  
                        BEGIN  
                            UPDATE tbl_attendance  
                            SET    LeaveDuration = Dateadd(second, Datediff(second, 0, ISNULL(LeaveDuration, '00:00:00.000')), @TimeDuration),  
                                   ModifiedBy = @UserID,  
                                   ModifiedOn = Getdate()  
                            WHERE  logdate = @LogDate  
                                   AND employeeid = @EmployeeID  
                                   AND isdeleted = 0  
  
                            SET @OutPut = 'Updated Successfully.'  
                        END  
                  END  
  
                IF @PunchType = 'Permission'  
                  BEGIN  
                      SET @TimeDuration = Cast(Replace(Cast(@Duration AS VARCHAR(10)), '.', ':') AS TIME(7))  
  
                      IF NOT EXISTS(SELECT 1  
                                    FROM   tbl_attendance  
                                    WHERE  logdate = @LogDate  
                                           AND employeeid = @EmployeeID  
                                           AND isdeleted = 0)  
                        BEGIN  
                            INSERT INTO tbl_attendance  
                                        (employeeid,  
                                         logdate,  
                                         -- Duration,  
                LeaveDuration,  
         domainid,  
                                         createdby,  
                                         modifiedby)  
                            SELECT @EmployeeID,  
                                   @LogDate,  
                                   -- @TimeDuration,  
                                   @TimeDuration,  
                                   @DomainID,  
                                   @UserID,  
                                   @UserID  
  
                            SET @OutPut = 'Inserted Successfully.'  
                        END  
                      ELSE  
                        BEGIN  
                            UPDATE tbl_attendance  
                            SET    LeaveDuration = Dateadd(second, Datediff(second, 0, ISNULL(LeaveDuration, '00:00:00.000')), @TimeDuration),  
                                   ModifiedBy = @UserID,  
                                   ModifiedOn = Getdate()  
                            WHERE  logdate = @LogDate  
                                   AND employeeid = @EmployeeID  
                                   AND isdeleted = 0  
  
                            SET @OutPut = 'Updated Successfully.'  
                        END  
                  END  
            END  
  
          IF( @Operation = 'Revert' )  
            BEGIN  
                SET @TimeDuration = Cast(Replace(Cast(@Duration AS VARCHAR(10)), '.', ':') AS TIME(7))  
  
                IF @PunchType = 'FullDay'  
                  BEGIN  
                      SET @TimeDuration =(SELECT Cast(Replace(Cast(FullDay AS VARCHAR(10)), '.', ':') AS TIME(7))  
                                          FROM   tbl_BusinessHours  
                                          WHERE  RegionID = (SELECT RegionID  
                                                             FROM   tbl_EmployeeMaster  
                                                             WHERE  ID = @EmployeeID)  
                                                 AND ISdeleted = 0)  
                  END  
  
                IF @PunchType = 'HalfDay'  
                  BEGIN  
                      SET @TimeDuration =(SELECT Cast(Replace(Cast(HalfDay AS VARCHAR(10)), '.', ':') AS TIME(7))  
                                          FROM   tbl_BusinessHours  
                                          WHERE  RegionID = (SELECT RegionID  
                                                             FROM   tbl_EmployeeMaster  
                                                             WHERE  ID = @EmployeeID)  
                                                 AND ISdeleted = 0)  
                  END  
  
                UPDATE tbl_Attendance  
                SET    LeaveDuration = CONVERT(TIME, Dateadd(MS, Datediff(SS, Isnull(@TimeDuration, '00:00:00.000'), LeaveDuration) * 1000, 0), 114),  
                       ModifiedBy = @UserID,  
                       ModifiedOn = Getdate()  
                WHERE  logdate = @LogDate  
                       AND employeeid = @EmployeeID  
                       AND isdeleted = 0  
  
                SET @OutPut = 'Leave is Reverted Successfully.'  
            END  
          EXEC UpdateBiometricWorkedHours  
            @DomainID  
  
          SELECT @OutPut  
      END try  
      BEGIN catch  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END catch  
  END
