﻿/****************************************************************************                                   
CREATED BY  :                                   
CREATED DATE :                                   
MODIFIED BY  :                            
MODIFIED DATE :                      
<summary>                       
</summary>                                                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[UpdatePO] (@ID            INT,
                                  @Description   VARCHAR(1000),
                                  @Date          DATETIME,
                                  @VendorID      INT,
                                  @TotalAmount   MONEY,
                                  @BillNo        VARCHAR(50),
                                  @UploadFile    VARCHAR(8000),
                                  @SessionID     INT,
                                  @DomainID      INT,
                                  @ExpenseDetail EXPENSEDETAIL readonly,
                                  @FileID        UNIQUEIDENTIFIER,
                                  @Reference     VARCHAR(1000),
                                  @CostCenterID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SET @FileID = Isnull(@FileID, Newid())

          DECLARE @OUTPUT               VARCHAR(100),
                  @ExpenseID            INT,
                  @cash                 MONEY,
                  @AutogenPaddingLength INT,
                  @AutogeneratePrefix   VARCHAR(100),
                  @AutogenNo            VARCHAR(100),
                  @PreviousCash         MONEY = (SELECT PaidAmount
                     FROM   tblExpense
                     WHERE  ID = @ID
                            AND DomainID = @DomainID),
                  @PreviousMode         INT = (SELECT TOP 1 PaymentModeID
                     FROM   tblexpensepayment EP
                            LEFT JOIN tblExpensepaymentmapping EM
                                   ON EP.id = EM.expensePaymentID
                                      AND EM.DomainID = 1
                     WHERE  EP.isdeleted = 0
                            AND EP.DomainID = @DomainID
                            AND EM.expenseID = @id)
          DECLARE @ExpenseVendorID INT

          IF( (SELECT Count(1)
               FROM   tblVendor
               WHERE  NAME = 'Others'
                      AND DomainID = @DomainID
                      AND IsDeleted = 0) = 0 )
            BEGIN
                INSERT INTO tblVendor
                            (NAME,
                             PaymentTerms,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             DomainID)
                VALUES      ('Others',
                             0,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             @DomainID)

                SET @VendorID = @@IDENTITY
            END

          SET @ExpenseVendorID =CASE
                                  WHEN ( @VendorID <> '' ) THEN
                                    (SELECT ID
                                     FROM   tblVendor
                                     WHERE  NAME = 'Others'
                                            AND DomainID = @DomainID
                                            AND IsDeleted = 0)
                                  ELSE
                                    ( @VendorID )
                                END
          SET @AutogeneratePrefix='BIL'
          SET @AutogenPaddingLength='1'
          SET @AutogenNo = @AutogeneratePrefix +
                           + RIGHT('0000000000000001', @AutogenPaddingLength)

          BEGIN
              BEGIN
                  SET @ExpenseID=@ID
                  SET @ExpenseID=@ID

                  BEGIN
                      UPDATE tbl_FileUpload
                      SET    ReferenceTable = 'tblPurchaseOrder'
                      WHERE  Id = @FileID

                      UPDATE tblPurchaseOrder
                      SET    VendorID = @VendorID,
                             [Date] = @Date,
                             PONo = @BillNo,
                             Remarks = @Description,
                             ModifiedBy = @SessionID,
                             ModifiedOn = Getdate(),
                             HistoryID = @FileID,
                             Reference = @Reference,
                             CostCenterID = @CostCenterID
                      WHERE  ID = @ID
                             AND DomainID = @DomainID

                      IF( (SELECT Count(1)
                           FROM   @ExpenseDetail) > 0 )
                        BEGIN
                            UPDATE tblPODetails
                            SET    tblPODetails.LedgerID = S.LedgerID,
                                   tblPODetails.Remarks = S.ExpenseDetailRemarks,
                                   tblPODetails.Amount = S.Amount,
                                   tblPODetails.Qty = S.Qty,
                                   tblPODetails.BilledQTY = 0,
                                   tblPODetails.CGSTPercent = S.CGST,
                                   tblPODetails.SGSTPercent = S.SGST,
                                   tblPODetails.CGST = S.CGSTAmount,
                                   tblPODetails.SGST = S.SGSTAmount,
                                   tblPODetails.IsLedger = S.IsProduct,
                                   tblPODetails.IsDeleted = S.IsDeleted,
                                   ModifiedBy = @SessionID,
                                   ModifiedOn = Getdate()
                            FROM   tblPODetails
                                   INNER JOIN @ExpenseDetail AS S
                                           ON tblPODetails.ID = S.ID
                                              AND S.ID <> 0
                        END

                      IF( (SELECT Count(1)
                           FROM   @ExpenseDetail) > 0 )
                        BEGIN
                            INSERT INTO tblPODetails
                                        (POID,
                                         LedgerID,
                                         Remarks,
                                         Amount,
                                         QTY,
                                         BilledQTY,
                                         CGSTPercent,
                                         SGSTPercent,
                                         CGST,
                                         SGST,
                                         IsLedger,
                                         DomainID,
                                         CreatedBy,
                                         CreatedOn,
                                         ModifiedBy,
                                         ModifiedOn)
                            SELECT @ID,
                                   LedgerID,
                                   ExpenseDetailRemarks,
                                   Amount,
                                   QTY,
                                   0,
                                   CGST,
                                   SGST,
                                   CGSTAmount,
                                   SGSTAmount,
                                   Isproduct,
                                   @DomainID,
                                   @SessionID,
                                   Getdate(),
                                   @SessionID,
                                   Getdate()
                            FROM   @ExpenseDetail
                            WHERE  ID = 0
                                   AND IsDeleted = 0
                                   AND LedgerID <> 0
                        END

                      SET @Output = ' Updated Successfully'

                      ----Update item to Inventory
                      EXEC ManageInventory
                        @ID,
                        'UPDATE',
                        'PO',
                        @SessionID,
                        @DomainID

                      GOTO finish
                  END
              END
          END

          FINISH:

          EXEC UpdatePOQTY

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
