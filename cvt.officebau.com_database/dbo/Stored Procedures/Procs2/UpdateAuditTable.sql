﻿/****************************************************************************   
CREATED BY    : Ajith N  
CREATED DATE  : 17 Jul 2018  
MODIFIED BY   : 
MODIFIED DATE : 
 <summary>      
      UpdateAuditTable '', 1, 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE UpdateAuditTable(@DBName    VARCHAR(200),
                                 @UserID    INT,
                                 @DomainID  INT)
AS
  BEGIN
      BEGIN TRY
	  DECLARE @Output VARCHAR(100) = ''

	  --	truncate table tbl_AuditTables
          SELECT DISTINCT ST.TABLE_NAME AS NAME,
                          ST.TABLE_NAME AS Remarks
          INTO   #tmpAuditTables
          FROM   INFORMATION_SCHEMA.TABLES BT
                 JOIN INFORMATION_SCHEMA.TABLES ST
                   ON BT.TABLE_NAME LIKE '%' + ST.TABLE_NAME + '%'
                      AND ST.TABLE_NAME NOT LIKE '%_TriggerAudit%'
          WHERE  BT.TABLE_TYPE = 'BASE TABLE'
                 AND ( @DBName = ''
                        OR BT.TABLE_CATALOG = @DBName )
                 AND BT.TABLE_NAME LIKE '%_TriggerAudit%'

          INSERT INTO tbl_AuditTables
                      (NAME,
                       Remarks,
                       CreatedBy,
                       ModifiedBy)
          SELECT tt.NAME AS NAME,
                 tt.NAME AS Remarks,
                 @UserID,
                 @UserID
          FROM   #tmpAuditTables TT
                 LEFT JOIN tbl_AuditTables AT
                        ON TT.NAME = AT.NAME
          WHERE  AT.NAME IS NULL

		  SET @Output = 'Updated Successfully.'

		  SELECT @Output As RESULT

      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
