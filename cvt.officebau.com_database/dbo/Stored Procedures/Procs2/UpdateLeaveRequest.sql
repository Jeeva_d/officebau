﻿/****************************************************************************     
CREATED BY  :   Ajith N  
CREATED DATE :   28-Jun-2017  
MODIFIED BY  :    Ajith N 
MODIFIED DATE :   05 Jan 2018  
 <summary>  
	 [UpdateLeaveRequest] 12,1,1,'2017-05-02','2017-05-02',null,'test','test',null,2,1,1,0  
	 select * from tbl_EmployeeLeave   
	 select * from tbl_EmployeeLeaveDateMapping 
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[UpdateLeaveRequest] (@ID                    INT,
                                            @EmployeeID            INT,
                                            @PurposeID             INT,
                                            @LeaveTypeID           INT,
                                            @FromDate              DATE,
                                            @ToDate                DATE,
                                            @Duration              TINYINT,
                                            @Reason                VARCHAR(500),
                                            @RequesterRemarks      VARCHAR(8000),
                                            @AlternativeContactNo  VARCHAR(20),
                                            @ApproverID            INT,
                                            @SessionID             INT,
                                            @DomainID              INT,
                                            @IsDeleted             BIT,
                                            @IsHalfDay             BIT,
                                            @ReplacementEmployeeID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output                 VARCHAR(150),
              @LeaveManagementID      INT,
              @BaseLocationID         INT,
              @AppConfigValue         INT,
              @LeaveStatusID          INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = 'Pending'
                        AND [Type] = 'Leave'
                        AND IsDeleted = 0),
              @RegionIDs              VARCHAR(20) = (SELECT Cast(RegionID AS VARCHAR(10))
                 FROM   tbl_EmployeeMaster EM
                 WHERE  EM.ID = @SessionID
                 FOR XML path('')),
              @PreviousLeaveCount     DECIMAL(32, 1) = (SELECT Count(1)
                 FROM   tbl_EmployeeLeaveDateMapping
                 WHERE  LeaveRequestID = @ID
                        AND IsDeleted = 0),
              @PreviousLeaveIsHalfDay BIT = (SELECT ISNULL(IsHalfDay, 'false')
                 FROM   tbl_EmployeeLeave
                 WHERE  ID = @ID
                        AND IsDeleted = 0)

      SELECT @BaseLocationID = BaseLocationID
      FROM   tbl_EmployeeMaster EM
      WHERE  EM.ID = @SessionID

      SET @AppConfigValue = ISNULL((SELECT ISNULL(ConfigValue, 0) AS Value
                                    FROM   tbl_ApplicationConfiguration AC
                                           JOIN tbl_ConfigurationMaster CM
                                             ON CM.ID = AC.ConfigurationID
                                                AND cm.Code = 'LEVWKEND'
                                    WHERE  AC.BusinessUnitID = @BaseLocationID), 0)

      CREATE TABLE #tempRequestLeaveDays
        (
           RequestedDate DATE,
           RequestedDays VARCHAR(25),
           NoOfDays      INT,
		   [Type] VARCHAR(50)
        )

      INSERT INTO #tempRequestLeaveDays
      EXEC [Searchrequestedleaves]
        @FromDate,
        @ToDate,
        @DomainID,
        @SessionID

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF EXISTS(SELECT 1
                    FROM   tbl_EmployeeLeave
                    WHERE  ID = @ID)
            BEGIN
                IF ( (SELECT Count(1)
   FROM   tbl_EmployeeLeave
                      WHERE  EmployeeID = @EmployeeID
                             AND IsDeleted = 0
                             AND DomainID = @DomainID
                             AND ID <> @ID
                             AND StatusID IN (SELECT ID
                                              FROM   tbl_Status
                                              WHERE  Code IN( 'Approved', 'Pending' )
                                                     AND [Type] = 'Leave'
                                                     AND IsDeleted = 0)
                             AND ( ( FromDate BETWEEN @FromDate AND @ToDate
                                      OR ToDate BETWEEN @FromDate AND @ToDate )
                                    OR ( @FromDate BETWEEN FromDate AND ToDate
                                          OR @ToDate BETWEEN FromDate AND ToDate ) )) = 0 )
                  IF NOT EXISTS (SELECT 1
                                 FROM   tbl_pay_EmployeePayroll
                                 WHERE  IsDeleted = 0
                                        AND DomainId = @DomainID
                                        AND EmployeeId = @EmployeeID
                                        AND ( ( MonthId = Datepart(MM, @FromDate)
                                                AND YearId = Datepart(YYYY, @FromDate) )
                                               OR ( MonthId = Datepart(MM, @ToDate)
                                                    AND YearId = Datepart(YYYY, @ToDate) ) ))
                    IF ( @LeaveTypeID = (SELECT ID
                                         FROM   tbl_LeaveTypes
                                         WHERE  NAME = 'Permission'
                                                AND IsDeleted = 0
                                                AND DomainID = @DomainID) )
                       AND ( NOT EXISTS(SELECT Count(1)
                                        FROM   tbl_Attendance TAT
                                               JOIN tbl_BiometricLogs BL
                                                 ON BL.AttendanceId = TAT.ID
                                        WHERE  TAT.EmployeeID = @EmployeeID
                                               AND TAT.LogDate = @FromDate
                                               AND TAT.IsDeleted = 0
                                               AND TAT.DomainID = @DomainID) )
                      BEGIN
                          SET @Output = 'Permission cannot be raised.This requires punches on the particular date.'
                      END
                    ELSE
                      BEGIN
                          UPDATE tbl_EmployeeLeave
                          SET    LeaveTypeID = @LeaveTypeID,
                                 PurposeID = @PurposeID,
                                 FromDate = @FromDate,
                                 ToDate = @ToDate,
                                 Duration = @Duration,
                                 Reason = @Reason,
                                 RequesterRemarks = @RequesterRemarks,
                                 AlternativeContactNo = @AlternativeContactNo,
                                 ApproverID = @ApproverID,
                                 --RequestedDate = GETDATE(),  
                                 StatusID = @LeaveStatusID,
                                 ApproverStatusID = @LeaveStatusID,
                                 DomainID = @DomainID,
                                 ModifiedBy = @SessionID,
                                 ModifiedOn = Getdate(),
                                 IsHalfDay = @IsHalfDay,
                                 ReplacementEmployeeID = @ReplacementEmployeeID
                          WHERE  ID = @ID

                          SET @LeaveManagementID = @ID;

                          WITH CTE
                               AS (SELECT @FromDate  AS All_Month_Date,
                                          Datename(dw, @FromDate) AS [days]
                                   UNION ALL
                                   SELECT Dateadd(DD, 1, All_Month_Date),
                                          Datename(dw, Dateadd(DD, 1, All_Month_Date)) AS [days]
                                   FROM   CTE
                                   WHERE  Dateadd(DD, 1, All_Month_Date) <= @ToDate)
                          SELECT *
                          INTO   #tempDates
                          FROM   CTE

                          IF EXISTS (SELECT [days]
                                     FROM   #tempDates
                                     WHERE  [days] IN ( 'Sunday', 'Saturday' ))
                            BEGIN
                                IF EXISTS(SELECT 1
                                          FROM   tbl_EmployeeLeaveDateMapping
                                          WHERE  LeaveRequestID = @LeaveManagementID)
                                  DELETE FROM tbl_EmployeeLeaveDateMapping
                                  WHERE  LeaveRequestID = @LeaveManagementID

                                IF( @AppConfigValue = 1 )
                                  BEGIN
                                      INSERT INTO tbl_EmployeeLeaveDateMapping
                                                  (LeaveRequestID,
                                                   EmployeeID,
                                                   LeaveDate,
                                                   DomainID,
                                                   CreatedBy,
                                                   ModifiedBy)
                                      SELECT @LeaveManagementID,
                                             @EmployeeID,
                                             All_Month_Date,
                                             @DomainID,
                                             @SessionID,
                                             @SessionID
                                      FROM   #tempDates
                                      WHERE  All_Month_Date NOT IN (SELECT [Date]
                                                                    FROM   tbl_holidays
                                                                    WHERE  [Type] = 'Yearly'
                                                                           AND IsDeleted = 0
                                                                           AND [Year] = Year(Getdate())
                                                                           AND DomainID = @DomainID
                                                                           AND RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%')
                                  END
                                ELSE
                                  BEGIN
                                      INSERT INTO tbl_EmployeeLeaveDateMapping
                                                  (LeaveRequestID,
                                                   EmployeeID,
                                                   LeaveDate,
                                                   DomainID,
                                                   CreatedBy,
                                                   ModifiedBy)
                                      SELECT @LeaveManagementID,
                                             @EmployeeID,
                                             All_Month_Date,
                                             @DomainID,
                                             @SessionID,
                                             @SessionID
                                      FROM   #tempDates
                                      WHERE  All_Month_Date NOT IN (SELECT [Date]
                    FROM   tbl_holidays
                                              WHERE  --[Type] = 'Yearly' AND
                                                                     IsDeleted = 0
                                                                     AND [Year] = Year(Getdate())
                                                                     AND DomainID = @DomainID
                                                                     AND RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%')
                                  END
                            END
                          ELSE
                            BEGIN
                                IF EXISTS(SELECT 1
                                          FROM   tbl_EmployeeLeaveDateMapping
                                          WHERE  LeaveRequestID = @LeaveManagementID)
                                  DELETE FROM tbl_EmployeeLeaveDateMapping
                                  WHERE  LeaveRequestID = @LeaveManagementID

                                INSERT INTO tbl_EmployeeLeaveDateMapping
                                            (LeaveRequestID,
                                             EmployeeID,
                                             LeaveDate,
                                             DomainID,
                                             CreatedBy,
                                             ModifiedBy)
                                SELECT @LeaveManagementID,
                                       @EmployeeID,
                                       All_Month_Date,
                                       @DomainID,
                                       @SessionID,
                                       @SessionID
                                FROM   #tempDates
                                WHERE  All_Month_Date NOT IN (SELECT [Date]
                                                              FROM   tbl_holidays
                                                              WHERE  IsDeleted = 0
                                                                     AND [Year] = Year(Getdate())
                                                                     AND DomainID = @DomainID
                                                                     AND RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%')
                            END

                          SET @Output = 'Leave Request Updated Successfully.'

                          IF( @Output LIKE '%Successfully%' )
                            BEGIN
                                IF EXISTS(SELECT 1
                                          FROM   tbl_EmployeeAvailedLeave
                                          WHERE  LeaveTypeID = @LeaveTypeID
                                                 AND EmployeeID = @SessionID
                                                 AND Year = Year(@FromDate))
                                  BEGIN
                                      UPDATE tbl_EmployeeAvailedLeave
                                      SET    AvailedLeave = CASE
                                                              WHEN Isnull(@PreviousLeaveIsHalfDay, 'false') = 'false'
                                                                   AND Isnull(@IsHalfDay, 'false') = 'false' THEN
                                                                ( ( Isnull(AvailedLeave, 0) - Isnull(@PreviousLeaveCount, 0) ) + Isnull((SELECT Count(1)
                                                                                                                                         FROM   #tempRequestLeaveDays), 0) )
                                                              WHEN Isnull(@PreviousLeaveIsHalfDay, 'false') = 'false'
                                                                   AND Isnull(@IsHalfDay, 'false') = 'true' THEN
                                               ( ( Isnull(AvailedLeave, 0) - Isnull(@PreviousLeaveCount, 0) ) + 0.5 )
                                                              WHEN Isnull(@PreviousLeaveIsHalfDay, 'false') = 'true'
                                                                   AND Isnull(@IsHalfDay, 'false') = 'true' THEN
                                                                ( ( Isnull(AvailedLeave, 0) - 0.5 ) + 0.5 )
                                                              WHEN Isnull(@PreviousLeaveIsHalfDay, 'false') = 'true'
                                                                   AND Isnull(@IsHalfDay, 'false') = 'false' THEN
                                                                ( ( Isnull(AvailedLeave, 0) - 0.5 ) + Isnull((SELECT Count(1)
                                                                                                              FROM   #tempRequestLeaveDays), 0) )
                                                              ELSE
                                                                0
                                                            END,
                                             ModifiedBy = @SessionID,
                                             ModifiedOn = Getdate()
                                      WHERE  EmployeeID = @SessionID
                                             AND LeaveTypeID = @LeaveTypeID
                                             AND Year = Year(@FromDate)
                                  END
                            END
                      END
                  ELSE
                    BEGIN
                        SET @Output = 'Leave cannot be raised as the Payroll is already processed.'
                    END
                ELSE
                  SET @Output = 'Already Exists.'
            END
          ELSE
            SET @Output = 'No Record Found.'

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
