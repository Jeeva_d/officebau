﻿/****************************************************************************       
CREATED BY  :   JENNIFER.S    
CREATED DATE :   06-Jun-2017    
MODIFIED BY  :       
MODIFIED DATE :       
 <summary>    
[Timewatchupload] 10,'26-Dec-17','09:13,11:41,13:08,13:35,16:32,18:19',1,1  
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Timewatchupload] (@TIMEWATCH TIMEWATCHUPLOAD READONLY,  
                                         @SessionID INT,  
                                         @DomainID  INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(50)  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SET @Output = 'One or more of the Entry cannot be Inserted/Updated!'  
  
          SELECT Row_number()  
                   OVER (  
                     ORDER BY BiometricID, DATE ASC) AS ID,  
                 BiometricID,  
                 date,  
                 Punches=( Stuff((SELECT ',' + Isnull(Punch1, '') + ',' + Isnull(Punch2, '')  
                                         + ',' + Isnull(Punch3, '') + ',' + Isnull(Punch4, '')  
                                  FROM   @TIMEWATCH b  
                                  WHERE  b.date = a.date  
                                         AND b.BiometricID = a.BiometricID  
                                  FOR xml path('')), 1, 1, '') )  
          INTO   #tempTimewatch  
          FROM   @TIMEWATCH a  
          GROUP  BY date,  
                    BiometricID  
  
          CREATE TABLE #temppunches  
            (  
               id      INT IDENTITY(1, 1),  
               punches VARCHAR(max)  
            )  
  
          DECLARE @count         INT=1,  
                  @Punches       VARCHAR(max),  
                  @BiometricCode VARCHAR(20),  
                  @LogDate       DATE,  
                  @EmployeeID    INT  
  
          WHILE( @count <= (SELECT Count(1)  
                            FROM   #tempTimewatch) )  
            BEGIN  
                SELECT --@BiometricCode = Rtrim(Ltrim(BiometricID)),  
                @LogDate = Date,  
                @Punches = Replace(Punches, ',,', '')  
                FROM   #tempTimewatch  
                WHERE  id = @count  
  
                SET @EmployeeID=(SELECT ID  
                                 FROM   tbl_EmployeeMaster  
                                 WHERE  IsDeleted = 0  
                                        AND IsActive = 0  
                                        AND BiometricCode = @BiometricCode)  
  
                INSERT INTO #temppunches  
                SELECT CONVERT(VARCHAR(8), ITEM)  
                FROM   dbo.Splitstring (LEFT(@Punches + ',,', Charindex(',,', @Punches + ',,') - 1), ',')  
  
                DECLARE @CheckIn  TIME = '0:0',  
                        @CheckOut TIME= '0:0',  
                        @Duration TIME;  
  
                IF EXISTS(SELECT 1  
                          FROM   #tempTimewatch)  
                  BEGIN  
                      SET @CheckIn = (SELECT TOP 1 Punches  
                                      FROM   #temppunches  
                                      ORDER  BY id ASC)  
                      SET @CheckOut = (SELECT TOP 1 Punches  
                                       FROM   #temppunches  
                                       ORDER  BY id DESC)  
                  END  
  
                IF( @CheckIn > @CheckOut )  
                  BEGIN  
                      SET @Duration = Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, @CheckIn), CONVERT(TIME, '23:59')), @CheckOut) AS TIME)  
                  END  
                ELSE  
                  BEGIN  
                      SET @Duration = Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, @CheckIn), CONVERT(TIME, @CheckOut)), '00:00:00') AS TIME)  
                  END  
  
                IF EXISTS (SELECT 1  
                           FROM   tbl_Attendance  
                           WHERE  EmployeeID = @EmployeeID  
                       AND LogDate = @LogDate  
                                  AND DomainID = @DomainID  
                                  AND IsDeleted = 0)  
                  BEGIN  
                      IF EXISTS (SELECT 1  
                                 FROM   tbl_Attendance  
                                 WHERE  EmployeeID = @EmployeeID  
                                        AND LogDate = @LogDate  
                                        AND DomainID = @DomainID  
                                        AND IsDeleted = 0  
                                --AND Isnull(IsPermission, 0) <> 1  
                                --AND Isnull(IsManual, 0) <> 1  
                                )  
                        BEGIN  
                            UPDATE tbl_Attendance  
                            SET    --CheckIn = @CheckIn,  
                            --CheckOut = @CheckOut,  
                            Duration = @Duration,  
                            --Punches = @Punches,  
                            --IsManual = 0,  
                            ModifiedBy = @SessionID  
                            WHERE  EmployeeID = @EmployeeID  
                                   AND LogDate = @LogDate  
                                   AND IsDeleted = 0  
  
                            SET @Output = 'Updated Successfully'  
                        END  
                  END  
                ELSE  
                  BEGIN  
                      IF( @EmployeeID IS NOT NULL )  
                        BEGIN  
                            INSERT INTO tbl_Attendance  
                                        (EmployeeID,  
                                         LogDate,  
                                         --CheckIn,  
                                         --CheckOut,  
                                         Duration,  
                                         --Punches,  
                                         --IsManual,  
                                         DomainID,  
                                         CreatedBy,  
                                         ModifiedBy--,BiometricCode  
                            )  
                            VALUES      (@EmployeeID,  
                                         @LogDate,  
                                         --@CheckIn,  
                                         --@CheckOut,  
                                         @Duration,  
                                         -- @Punches,  
                                         -- 0,  
                                         @DomainID,  
                                         @SessionID,  
                                         @SessionID--,@BiometricCode  
                            )  
  
                            SET @Output = 'Inserted Successfully'  
                        END  
                  END  
  
                TRUNCATE TABLE #temppunches  
  
                SET @count=@count + 1  
            END  
  
          SELECT @Output AS Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
