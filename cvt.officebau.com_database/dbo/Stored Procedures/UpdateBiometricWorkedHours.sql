﻿  /****************************************************************************                          
CREATED BY  :                          
CREATED DATE :                          
MODIFIED BY  :                          
MODIFIED DATE :                          
                     
                       
 UpdateBiometricWorkedHours                        
                                                   
 *****************************************************************************/            
CREATE PROCEDURE [dbo].[UpdateBiometricWorkedHours] (@DomainID INT = null)            
AS            
  BEGIN            
            
      BEGIN try            
          DECLARE @OutPut VARCHAR(100) = 'Operation failed!',            
                  @Check  INT =0            
          IF ( @Check <> 0 )            
            BEGIN            
      
                SELECT a.EmployeeID,            
                       a.LogDate,            
                       a.LeaveDuration,            
                       b.ID,            
                       b.AttendanceId,            
                       b.Direction,            
                       b.PunchTime            
                INTO   #temptablebioupdate            
                FROM   tbl_Attendance a            
                       JOIN tbl_BiometricLogs b            
                         ON b.AttendanceId = a.ID            
                WHERE  DomainID = ISNULL( @DomainID ,DomainID)           
                ORDER  BY a.logdate;            
            
                WITH CTE --(Rownumber, EmployeeID, LogDate, LeaveDuration, AttendanceId, PunchTime, Direction)            
                     AS (SELECT ROW_NUMBER()            
                                  OVER(            
                                    ORDER BY EmployeeID, LogDate, PunchTime) Rownumber,            
                                EmployeeID,            
                                LogDate,            
                                LeaveDuration,            
                                AttendanceId,            
                                PunchTime,            
                                Direction            
                         FROM   #temptablebioupdate)            
                SELECT ins.AttendanceId,            
                       ins.EmployeeID,            
                       ins.PunchTime                                                                 Intime,            
                       outs.PunchTime                                                                outTime,            
                       Dateadd(SECOND, -Datediff(SECOND, outs.PunchTime, ins.PunchTime), '00:00:00') Duration            
                INTO   #proceesDurationtable            
                FROM   (SELECT *            
                        FROM   CTE            
                        WHERE  Direction = 'In') AS ins            
                       JOIN (SELECT *            
                             FROM   CTE            
                             WHERE  Direction = 'Out') AS Outs            
                         ON ins.Rownumber = Outs.Rownumber - 1            
                            AND outs.EmployeeID = ins.EmployeeID            
                            AND ins.LogDate = Outs.LogDate            
            
                SELECT AttendanceId,            
                       EmployeeID,            
                       Cast(Dateadd(ms, Sum(Datediff(ms, '00:00:00.000', Duration)), '00:00:00.000') AS TIME) AS Durations            
                INTO   #ResultTable            
                FROM   #proceesDurationtable            
                GROUP  BY AttendanceId,            
                          EmployeeID            
            
                UPDATE tbl_Attendance            
                SET    tbl_Attendance.Duration = Dateadd(second, Datediff(second, 0, ISNULL(a.LeaveDuration, '00:00:00.000')), b.Durations)            
                FROM   tbl_Attendance a            
                       JOIN #ResultTable b            
                         ON b.AttendanceId = a.ID            
            
                DROP TABLE #temptablebioupdate, #proceesDurationtable, #ResultTable            
            
                SET @OutPut = 'Updated Successfully.'            
            END            
          ELSE            
            BEGIN            
                WITH Cte            
                     AS (SELECT b.AttendanceId,           
                                b.PunchTime,            
                                RnAsc = ROW_NUMBER()            
                                          OVER(            
                    PARTITION BY attendanceID            
                                            ORDER BY punchtime),            
                                RnDesc = ROW_NUMBER()            
                       OVER(            
                                             PARTITION BY attendanceID            
                                             ORDER BY punchtime DESC)            
                         FROM   tbl_BiometricLogs b            
                                JOIN tbl_Attendance a            
                                  ON b.AttendanceId = a.ID            
                                     AND a.isdeleted = 0            
                         WHERE  DomainID = ISNULL( @DomainID ,DomainID)          
                                AND b.IsDeleted = 0)            
                SELECT intime.AttendanceId,            
                       Cast(Dateadd(SECOND, -Datediff(SECOND, outtime.PunchTime, intime.PunchTime), '00:00:00')AS TIME) Duration            
                INTO   #Firsrinlastout            
                FROM   (SELECT *            
                        FROM   cte            
                        WHERE  RnAsc = 1) AS intime            
                       JOIN (SELECT *            
                             FROM   cte            
                             WHERE  rndesc = 1) AS outtime            
                         ON intime.AttendanceId = outtime.AttendanceId 
						      UPDATE tbl_Attendance            
                SET    tbl_Attendance.Duration  =null            
                FROM   tbl_Attendance a            
                                  
                UPDATE tbl_Attendance            
                SET    tbl_Attendance.Duration = b.Duration            
                FROM   tbl_Attendance a            
                       JOIN #Firsrinlastout b            
                         ON b.AttendanceId = a.ID            
                SET @OutPut = 'Updated Successfully.'       
            END            
          UPDATE tbl_Attendance            
          SET    duration = Dateadd(second, Datediff(second, 0, ISNULL(LeaveDuration, '00:00:00.000')), Isnull(Duration, '00:00:00'))            
          WHERE  DomainID = ISNULL( @DomainID ,DomainID)         
            
          SELECT @OutPut        
      END try            
      BEGIN catch            
          DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT            
          SELECT @ErrorMsg = Error_message(),            
                 @ErrSeverity = Error_severity()            
          RAISERROR(@ErrorMsg,@ErrSeverity,1)            
      END catch            
  END  
