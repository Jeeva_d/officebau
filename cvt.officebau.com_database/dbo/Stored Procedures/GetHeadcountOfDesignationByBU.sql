﻿
/**************************************************************************** 
CREATED BY    		:	Ajith N
CREATED DATE		:	09 Aug 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
	[GetHeadcountOfDesignationByBU]  '',1, 106
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetHeadcountOfDesignationByBU] (@BusinessUnit VARCHAR(50),
                                                       @DomainID     INT,
                                                       @UserID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT *
          INTO   #tmpAccess
          FROM   Splitstring((SELECT BusinessUnitID
                              FROM   tbl_EmployeeMaster
                              WHERE  ID = @UserID), ',')

          SELECT Isnull(DESI.NAME, 'Others') AS BusinessUnit,
                 Count(1)                    AS Headcount
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
                 LEFT JOIN tbl_Designation DESI
                        ON DESI.ID = EM.DesignationID
          WHERE  EM.IsDeleted = 0
                 AND Isnull(EM.IsActive, 0) = 0
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND EM.DomainID = @DomainID
                 AND ( @BusinessUnit = ''
                        OR Isnull(BU.NAME, 'Others') = @BusinessUnit )
                 AND EM.BaseLocationID IN (SELECT item
                                           FROM   #tmpAccess)
          GROUP  BY DESI.NAME
          ORDER  BY Headcount DESC,
                    BusinessUnit

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
