﻿/****************************************************************************   
CREATED BY		: Dhanalakshmi. S
CREATED DATE	: 28-MAR-2018 
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
[GetAvailableLeave] 538,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetAvailableLeave] (@EmployeeID       INT,
                                           @DomainID        INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT EA.EmployeeID, 
		         L.Name AS LeaveType,
		         EA.TotalLeave As [Total],
			     EA.AvailedLeave
		  FROM tbl_EmployeeAvailedLeave EA
                 LEFT JOIN tbl_LeaveTypes L
                        ON L.ID = EA.LeaveTypeID
          WHERE  EA.IsDeleted = 0
                 AND EA.EmployeeID = @EmployeeID
                 AND EA.DomainID = @DomainID
				 AND YEAR = Year(GETDATE())
				 AND L.DomainID = @DomainID
				 AND L.IsDeleted =0
				 UNION ALL
				SELECT x.EmployeeID, 'Consolidated' AS LeaveType, Sum(x.Consolidated) AS [Total], 0.0 AS AvailedLeave FROM (
            SELECT CASE WHEN (TotalLeave - AvailedLeave) > 10 THEN 10 ELSE (TotalLeave - AvailedLeave) END AS Consolidated, EmployeeID FROM tbl_EmployeeAvailedLeave
					WHERE LeaveTypeID = (SELECT ID FROM tbl_LeaveTypes
							WHERE Code = 'PL' and IsDeleted = 0 AND DomainID = @DomainID) and Year < Year(GETDATE()) and DomainID = @DomainID and EmployeeID = @EmployeeID
							) x
					GROUP BY x.EmployeeID 

				 ORDER BY L.Name

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
