﻿/****************************************************************************   
CREATED BY		:  
CREATED DATE	:   
MODIFIED BY		:   JENNIFER.S
MODIFIED DATE	:   27-NOV-2017
 <summary> 
          SearchCustomer '', '', '53',1,0,224
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchCustomer] (@City          VARCHAR(200),
                                        @OrderName     VARCHAR(200),
                                        @StatusID      INT,
                                        @DomainID      INT,
                                        @SalesPersonID INT,
                                        @EmployeeID    INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @applicationRoleID INT = (SELECT ID
             FROM   tbl_ApplicationRole
             WHERE  NAME = 'Sales Manager'
                    AND DomainID = @DomainID
                    AND Isdeleted = 0)

          SELECT TC.ID                      AS ID,
                 TC.EnquiryName             AS EnquiryName,
                 TC.OrderName               AS OrderName,
                 TCY.NAME                   AS Country,
                 TS.NAME                    AS [State],
                 TCC.NAME                   AS City,
                 TCM.Code                   AS [Status],
                 TC.MonthlyBusinessVolume   AS MonthlyBusinessVolume,
                 TC.TypeofBusiness          AS TypeofBusiness,
                 (SELECT Count(1)
                  FROM   tbl_CustomerContact
                  WHERE  CustomerID = TC.ID
                         AND IsDeleted = 0) AS [Count],
                 TE.FullName               AS EmployeeName,
                 TC.EmailID                 AS EmailID,
                 TC.ContactNo               AS ContactNo,
                 TC.Address1                AS Address1,
                 TC.Address2                AS Address2,
                 EM.FullName               AS CreatedBy,
                 TC.CreatedOn               AS CreatedOn,
                 ST.Code                    AS [Status]
          FROM   tbl_Customer TC
                 LEFT JOIN tbl_State TS
                        ON TS.ID = TC.StateID
                 LEFT JOIN tbl_Status ST
                        ON ST.ID = TC.StatusID
                 LEFT JOIN tbl_Country TCY
                        ON TCY.ID = TC.CountryID
                 LEFT JOIN tbl_City TCC
                        ON TCC.ID = TC.CityID
                 LEFT JOIN tbl_CodeMaster TCM
                        ON TCM.ID = TC.StatusID
                 LEFT JOIN tbl_EmployeeMaster TE
                        ON TE.ID = TC.SalesPersonID
                 LEFT JOIN tbl_EmployeeMaster EM
                        ON EM.ID = TC.CreatedBy
          WHERE  TC.IsDeleted = 0
                 AND TC.DomainID = @DomainID
                 AND ( ( @City IS NULL
                          OR @City = '' )
                        OR ( TCC.NAME LIKE '%' + @City + '%' ) )
                 AND ( ( @OrderName IS NULL
                          OR @OrderName = '' )
                        OR ( TC.OrderName LIKE '%' + @OrderName + '%' ) )
                 AND ( @StatusID = 0
                        OR TCM.ID = @StatusID )
                 AND ( ISNULL(@SalesPersonID, 0) = 0
                        OR ( ISNULL(@SalesPersonID, 0) <> 0
                             AND TC.SalesPersonID = @SalesPersonID ) )
          -- AND ( TC.SalesPersonID = @EmployeeID
          --        OR ( (SELECT Isnull(ApplicationRoleID, 0)
          --              FROM   tbl_EMployeeOtherDetails
          --              WHERE  EmployeeID = @EmployeeID
          --                     AND DomainID = @DomainID
          --                     AND Isdeleted = 0) = @applicationRoleID
          --             AND ( TC.CreatedBY = @EmployeeID
          --                    OR TC.SalesPersonID IN (SELECT DISTINCT T.ID
          --             FROM   tbl_EmployeeMaster T
          --JOIN tbl_EmployeeOtherDetails TOD
          --                                                      ON T.ID = TOD.EmployeeID
          --                                                   CROSS APPLY dbo.Splitstring((SELECT BusinessUnitID
          --                                                                                FROM   tbl_EmployeeMaster
          --                                                                                WHERE  ID = @EmployeeID
          --                                                                                       AND Isdeleted = 0
          --                                                                                       AND ISNULL(IsActive, 0) = 0
          --                                                                                       AND DomainID = @DomainID), ',') S
          --                                            WHERE  PATINDEX('%,' + S.Item + ',%', BusinessUnitID) > 0
          --                                                   AND T.ID != @EmployeeID
          --                                                   AND TOD.ApplicationRoleID =  (SELECT ID
          --                                                                                 FROM   tbl_ApplicationRole
          --                                                                                 WHERE  [Name] = 'Sales Executive')
          --                                                   AND T.Isdeleted = 0
          --   --AND ISNULL(T.IsActive, 0) = 0
          --                                                   AND T.DomainID = @DomainID) ) ) )
          ORDER  BY TC.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
