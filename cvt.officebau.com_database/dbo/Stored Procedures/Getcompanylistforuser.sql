﻿/****************************************************************************           
CREATED BY  : Naneeshwar.M  
CREATED DATE :   
MODIFIED BY  :    Ajith N     
MODIFIED DATE   :    09 Mar 2018         
 <summary>                  
     [Getcompanylistforuser] ''  
 </summary>                                   
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getcompanylistforuser] (@EmployeeCode VARCHAR(100))
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @CompanyIDs VARCHAR(50),
              @FirstName  VARCHAR(100),
              @EmpCode    VARCHAR(50)=(SELECT TOP 1 code
                FROM   tbl_EmployeeMaster
                WHERE  LoginCode = @EmployeeCode
                ORDER  BY IsDeleted ASC)

      BEGIN TRY
          SET @FirstName=(SELECT TOP 1 FirstName
                          FROM   tbl_EmployeeMaster
                          WHERE  LoginCode = @EmployeeCode
                          ORDER  BY IsDeleted ASC)

          IF ( Upper(@FirstName) = Upper('ADMIN')
               AND @EmpCode = 1000 )
            SET @CompanyIDs =(SELECT EM.DomainID
                              FROM   tbl_employeemaster EM
                                     JOIN tbl_Company C
                                       ON C.ID = EM.DomainID
                              WHERE  EM.LoginCode = @EmployeeCode
                                     AND EM.IsDeleted = 1
                                     AND C.IsDeleted = 0
                                     AND C.IsActive = 0)
          ELSE
            SET @CompanyIDs = (SELECT Cast(EM.DomainID AS VARCHAR) + ','
                                      + ISNULL(companyids, '') + ','
                               FROM   tbl_EmployeeMaster EM
                                      LEFT JOIN tbl_employeecompanymapping cm
                                             ON cm.EmployeeID = em.ID
                                      JOIN tbl_Company C
                                        ON C.ID = EM.DomainID
                               WHERE  EM.IsDeleted = 0
                                      AND EM.IsActive = 0
                                      AND EM.LoginCode = @EmployeeCode
                                      AND C.IsDeleted = 0
                                      AND C.IsActive = 0
                               FOR xml path(''))

          SELECT COM.ID   AS ID,
                 COM.NAME AS NAME
          FROM   tbl_company COM
          WHERE  COM.id IN (SELECT Item
                            FROM   dbo.Splitstring(@CompanyIDs, ',')
                            WHERE  Isnull(Item, '') <> '')
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
