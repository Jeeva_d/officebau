﻿/****************************************************************************             
CREATED BY   :             
CREATED DATE  :             
MODIFIED BY   :          
MODIFIED DATE  :            
 <summary>          
 [GetStockonHandValue] 16       
 </summary>                                     
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetStockonHandValue] (@ID   INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
            BEGIN  
                SELECT ISNULL(inv.StockonHand, 0) AS StockonHand,  
           ISNULL(isat.AdjustedQty, 0) as AdjustedQty  
                         
                FROM   tblProduct_V2 p  
                       LEFT JOIN VW_Inventory inv  
                              ON p.ID = inv.ProductID  
        left join tbl_InventoryStockAdjustmentItems isat  
               on isat.ProductID = inv.ProductID   
                WHERE  p.ID = @ID  
            END            
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
