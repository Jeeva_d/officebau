﻿/****************************************************************************       
CREATED BY   :       
CREATED DATE  :        
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>       
 </summary>                               
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[ManageInvoiceHolding] (@ID            INT,   
@InvoiceID int,  
@Date Date,  
@Amount money,     
@Isdeleted bit,  
@LedgerID int ,                                      @SessionID     INT ,@DomainID INT   
                                       )      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      DECLARE @Output VARCHAR(1000)      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          BEGIN      
           IF @Isdeleted =1  
     Begin  
     Update tblInvoiceHoldings set Isdeleted =1 ,ModifiedBy=@SessionID,ModifiedOn=GETDATE() where ID=@ID;  
     Set @Output ='Deleted Successfully'   
     GOTO FINISH;  
         END  
   IF @ID =0  
   BEGIN  
    INSERT INTO tblInvoiceHoldings (InvoiceId,Amount,Date,LedgerId, CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,DomainID)  
    Select @InvoiceID,@Amount,@Date,@LedgerID,@SessionID,GETDATE(),@SessionID,GETDATE(),@DomainID  
     Set @Output ='Inserted  Successfully'   
     GOTO FINISH;  
  
   END  
   ELSE   
    BEGIN  
     Update tblInvoiceHoldings set Date =@Date,LedgerId=@LedgerID,Amount=@Amount where ID=@ID  
     Set @Output ='Updated  Successfully'   
     GOTO FINISH;  
  
  
    END  
          END      
      
          FINISH:    
      
        
  UPDATE tblInvoice    
          SET    tblInvoice.ReceivedAmount = Isnull((SELECT ISNULL(Sum(Amount),0)   
                                                     FROM   tblInvoicePaymentMapping    
                                                     WHERE  IsDeleted = 0    
                                                            AND InvoiceID =tblInvoice.ID    
                                                            AND DomainID = @DomainID), 0),    
                 tblInvoice.StatusID = CASE    
                                         WHEN( Isnull((SELECT ISNULL(Sum(Amount),0)   
                                                       FROM   tblInvoicePaymentMapping    
                                                       WHERE  IsDeleted = 0    
                                                              AND InvoiceID =tblInvoice.ID    
                                                              AND DomainID = @DomainID) + (Select ISNULL(Sum(Amount),0) from tblInvoiceHoldings where IsDeleted = 0    
                                                              AND InvoiceID =tblInvoice.ID), 0) >= ( CASE    
                                                                                                  WHEN ( tblInvoice.DiscountPercentage <> 0 ) THEN   
                          (SELECT ( Isnull(Sum(Qty * Rate)    
                                                     + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) -   
              ( Isnull(( Sum(Qty * Rate) * tblInvoice.DiscountPercentage ), 0) ), 0) )    
                                                                                                                        FROM   tblInvoiceItem   
                                 
                                                                                                                                                    WHERE  InvoiceID =tblInvoice.ID    
                                                                                                                                                           AND IsDeleted = 0    
                                                                                                                                                           AND DomainID = @DomainID)    
                                                                                                  ELSE (SELECT ( Isnull(( Sum(Qty * Rate)    
                                                                                    + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(tblInvoice.DiscountValue, 0) ), 0) )    
                                                                                                        FROM   tblInvoiceItem    
                                                                                                        WHERE  InvoiceID =tblInvoice.ID    
                                                                                                               AND IsDeleted = 0    
                                                                                                               AND DomainID = @DomainID)    
                                                                                                END ) ) THEN (SELECT ID    
                                                                                                              FROM   tbl_CodeMaster    
                                                                                                              WHERE  [type] = 'Close'    
                                                                                                                     AND IsDeleted = 0)    
                                         WHEN( Isnull((SELECT ISNULL(Sum(Amount),0)    
                                                       FROM   tblInvoicePaymentMapping    
                                                       WHERE  IsDeleted = 0    
                                                              AND InvoiceID =tblInvoice.ID    
                                                              AND DomainID = @DomainID)+ (Select ISNULL(Sum(Amount),0) from tblInvoiceHoldings where IsDeleted = 0    
                                                              AND InvoiceID =tblInvoice.ID), 0) = 0 ) THEN (SELECT ID    
                                                                                                        FROM   tbl_CodeMaster    
                                                                                                        WHERE  [type] = 'Open'    
                                                                                                               AND IsDeleted = 0)    
                                         WHEN( Isnull((SELECT ISNULL(Sum(Amount),0)    
                                                       FROM   tblInvoicePaymentMapping    
                                                       WHERE  IsDeleted = 0    
                                                              AND InvoiceID =tblInvoice.ID    
                                                              AND DomainID = @DomainID)+ (Select ISNULL(Sum(Amount),0) from tblInvoiceHoldings where IsDeleted = 0    
                                                              AND InvoiceID =tblInvoice.ID), 0) <> ( CASE    
                                                                                                   WHEN ( Isnull(tblInvoice.DiscountPercentage, 0) <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)    
                                                                                                                                                                                + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum (Qty * Rate
  
) * tblInvoice.DiscountPercentage ), 0) ), 0) )    
                  FROM   tblInvoiceItem    
                                                                                                                                                                WHERE  InvoiceID =tblInvoice.ID    
                                                                                                                                                                       AND DomainID = @DomainID    
               AND IsDeleted = 0)    
                                                                                                   ELSE (SELECT ( Isnull(( Sum(Qty * Rate)    
                                                                                                                           + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(tblInvoice.DiscountValue, 0) ), 0) )    
                                                                                                         FROM   tblInvoiceItem    
                                                                                                         WHERE  InvoiceID =tblInvoice.ID    
                                                                                                                AND DomainID = @DomainID    
                                                                                                                AND IsDeleted = 0)    
                                                                                                 END ) )THEN (SELECT ID    
                                                                                                              FROM   tbl_CodeMaster    
                                                                                                              WHERE  [type] = 'Partial'    
                                                                                                                     AND IsDeleted = 0)    
                                       END    
          FROM   tblInvoice    
          SELECT @Output      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END      
      
ROLLBACK
