﻿/****************************************************************************         
CREATED BY  :   JENNIFER.S      
CREATED DATE :   06-Jun-2017      
MODIFIED BY  :     Naneeshwar.M    
MODIFIED DATE :     22-JAN-2018    
 <summary>      
 [ManageAttendance] 0,1,'22-Jan-18 12:00:00 AM','10:40,15:00',1,1    
 </summary>                                 
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ManageAttendance] (@ID         INT,    
                                          @EmployeeID INT,    
                                          @LogDate    DATE,    
                                          @PunchType  VARCHAR(250),    
                                          @SessionID  INT,    
                                          @DomainID   INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output        VARCHAR(50),    
              @BiometricCode VARCHAR(50) =(SELECT BiometricCode    
                FROM   tbl_EmployeeMaster    
                WHERE  ID = @EmployeeID)    
    
      SET @ID=(SELECT ID    
               FROM   tbl_Attendance    
               WHERE  EmployeeID = @EmployeeID    
                      AND LogDate = @LogDate    
                      AND DomainID = @DomainID    
                      AND IsDeleted = 0)    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SET @Output = 'Operation Failed!'    
    
          CREATE TABLE #temp    
            (    
               id      INT IDENTITY(1, 1),    
               punches VARCHAR(20)    
            )    
    
          SET @PunchType = Replace(@PunchType, ';', ',')    
    
          INSERT INTO #temp    
          SELECT CONVERT(VARCHAR(8), ITEM)    
          FROM   dbo.Splitstring (LEFT(@PunchType + ',,', Charindex(',,', @PunchType + ',,') - 1), ',')    
    
          DECLARE @CheckIn  TIME = '0:0',    
                  @CheckOut TIME= '0:0',    
                  @Duration TIME;    
    
          IF EXISTS(SELECT 1    
                    FROM   #temp)    
            BEGIN    
                SET @CheckIn = (SELECT TOP 1 punches    
                                FROM   #temp    
                                ORDER  BY id ASC)    
                SET @CheckOut = (SELECT TOP 1 punches    
                                 FROM   #temp    
                                 ORDER  BY id DESC)    
            END    
    
          IF( @CheckIn > @CheckOut )    
            BEGIN    
                SET @Duration = Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, @CheckIn), CONVERT(TIME, '23:59')), @CheckOut) AS TIME)    
            END    
          ELSE    
            BEGIN    
                SET @Duration = Cast(Dateadd(ms, Datediff(MS, CONVERT(TIME, @CheckIn), CONVERT(TIME, @CheckOut)), '00:00:00') AS TIME)    
            END    
    
          IF EXISTS (SELECT 1    
                     FROM   tbl_Attendance    
                     WHERE  ID = @ID    
                            AND EmployeeID = @EmployeeID    
                            AND LogDate = @LogDate    
                            AND DomainID = @DomainID    
                            AND IsDeleted = 0)    
            BEGIN    
                UPDATE tbl_Attendance    
                SET    --CheckIn = @CheckIn,    
                --CheckOut = @CheckOut,    
                Duration = @Duration,    
                --Punches = @PunchType,    
                ModifiedBy = @SessionID    
                WHERE  ID = @ID    
                       AND IsDeleted = 0    
    
                SET @Output = 'Updated Successfully'    
            END    
          ELSE    
            BEGIN    
                INSERT INTO tbl_Attendance    
                            (EmployeeID,    
                             LogDate,    
                             --CheckIn,    
                             --CheckOut,    
                             Duration,    
                             -- Punches,    
                             DomainID,    
          CreatedBy,    
                             ModifiedBy--,    
                -- BiometricCode    
                )    
                VALUES      (@EmployeeID,    
                             @LogDate,    
                             --@CheckIn,    
                             --@CheckOut,    
                             @Duration,    
                             -- @PunchType,    
                             @DomainID,    
                             @SessionID,    
                             @SessionID--,    
                -- @BiometricCode    
                )    
    
                SET @Output = 'Inserted Successfully'    
            END    
  
          EXEC UpdateBiometricPunches    
            @EmployeeID,    
            @LogDate,    
            'In',    
            'Insert',    
            NULL,    
            @CheckIn,    
            @SessionID,    
            @DomainID    
  
          EXEC UpdateBiometricPunches    
            @EmployeeID,    
            @LogDate,    
            'Out',    
            'Insert',    
            NULL,    
            @CheckOut,    
            @SessionID,    
            @DomainID    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
