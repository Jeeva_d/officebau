﻿/****************************************************************************                                   
CREATED BY  :                                   
CREATED DATE :                                   
MODIFIED BY  :   JENNIFER S                          
MODIFIED DATE :  02-SEP-2017                          
<summary>                                          
   [UpdateExpense]                  
</summary>                                                           
*****************************************************************************/  
CREATE procedure [dbo].[UpdateExpense] (@ID            INT,    
                                       @Description   VARCHAR(1000),    
                                       @Date          DATETIME,    
                                       @VendorID      INT,    
                                       @TotalAmount   MONEY,    
                                       @DueDate       DATETIME,    
                                       @BillNo        VARCHAR(50),    
                                       @CostCenterID  INT,    
                                       @Type          VARCHAR(50),    
                                       @UploadFile    VARCHAR(8000),    
                                       @SessionID     INT,    
                                       @DomainID      INT,    
                                       @PaymentModeID INT,    
                                       @BankID        INT,    
                                       @Reference     VARCHAR(1000),    
                                       @ExpenseDetail EXPENSEDETAIL readonly,    
                                       @FileID        UNIQUEIDENTIFIER,   
                                       @Tag varchar(200)  )  
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SET @FileID = ISNULL(@FileID, Newid())    
    
          DECLARE @OUTPUT                 VARCHAR(100),    
                  @ExpenseID              INT,    
                  @cash                   MONEY,    
                  @AutogenPaddingLength   INT,    
                  @AutogeneratePrefix     VARCHAR(100),    
                  @AutogenNo              VARCHAR(100),    
                  @ExpenseVendorID        INT,    
                  @ExpenseDtlHistoryID    VARCHAR(MAX),    
                  @ExpenseItmHistoryID    VARCHAR(MAX),    
                  @ExpenseHistoryID       VARCHAR(MAX) = @FileID,    
                  @ExpensePayHistoryID    VARCHAR(MAX) = (SELECT EP.HistoryID    
                     FROM   tblExpensePaymentMapping EPM    
                            JOIN tblExpensePayment EP    
                              ON EP.ID = EPM.ExpensePaymentID    
                     WHERE  EPM.ExpenseID = @ID    
                            AND EPM.DomainID = @DomainID),    
                  @ExpensePayMapHistoryID VARCHAR(MAX) = (SELECT HistoryID    
                     FROM   tblExpensePaymentMapping    
                     WHERE  ExpenseID = @ID    
                            AND DomainID = @DomainID),    
                  @PreviousCash           MONEY = Isnull((SELECT PaidAmount    
                            FROM   tblExpense    
                            WHERE  ID = @ID    
                                   AND DomainID = @DomainID), 0),    
                  @PreviousMode           INT = (SELECT TOP 1 PaymentModeID    
                     FROM   tblexpensepayment EP    
                            LEFT JOIN tblExpensepaymentmapping EM    
                                   ON EP.id = EM.expensePaymentID    
                                      AND EM.DomainID = @DomainID    
                     WHERE  EP.isdeleted = 0    
                            AND EP.DomainID = @DomainID    
                            AND EM.expenseID = @id)    
          DECLARE @DetailsHistoryIDTable TABLE    
            (    
               ID        INT IDENTITY(1, 1),    
               HistoryID VARCHAR(Max)    
            )    
    
          IF( (SELECT Count(1)    
               FROM   tblVendor    
               WHERE  NAME = 'Others'    
                      AND DomainID = @DomainID    
 AND IsDeleted = 0) = 0 )    
            BEGIN    
                INSERT INTO tblVendor    
                            (NAME,    
                             PaymentTerms,    
                             CreatedBy,    
                             CreatedOn,    
                             ModifiedBy,    
                             ModifiedOn,    
 DomainID)    
                VALUES      ('Others',    
                             0,    
                             @SessionID,    
                             Getdate(),    
                             @SessionID,    
                             Getdate(),    
                             @DomainID)    
    
                SET @VendorID = @@IDENTITY    
            END    
    
          SET @ExpenseVendorID = CASE    
                                  WHEN ( ISNULL(@VendorID, 0) = 0 ) THEN    
                                    (SELECT ID    
                                     FROM   tblVendor    
                                     WHERE  NAME = 'Others'    
                                            AND DomainID = @DomainID    
                                            AND IsDeleted = 0)    
                                  ELSE    
                                    ( @VendorID )    
                                END    
          SET @AutogeneratePrefix='BIL'    
          SET @AutogenPaddingLength='1'    
          SET @AutogenNo = @AutogeneratePrefix +    
                           + RIGHT('0000000000000001', @AutogenPaddingLength)    
    
          BEGIN    
              BEGIN    
                  SET @ExpenseID=@ID    
    
                  IF EXISTS(SELECT 1    
                            FROM   tblbrs    
                            WHERE  SourceID IN (SELECT EP.ID    
                                                FROM   tblexpensepayment EP    
                                                       LEFT JOIN tblExpensepaymentmapping EM    
                                                              ON EP.id = EM.expensePaymentID    
                                                WHERE  EP.isdeleted = 0    
                                                       AND EM.expenseID = @id    
                                                       AND EP.DomainID = @DomainID)    
                                   AND SourceType = (SELECT ID    
                                                     FROM   tblMasterTypes    
                                                     WHERE  NAME = 'Expense')    
                                   AND IsDeleted = 0    
                                   AND isreconsiled = 1    
                                   AND DomainID = @DomainID)    
                    BEGIN    
                        UPDATE tbl_FileUpload    
                        SET    ReferenceTable = 'tblExpense'    
                        WHERE  Id = @FileID    
    
                        UPDATE tblExpense    
                        SET    CostCenterID = @CostCenterID,    
                               Remarks = @Description,                                   
                               ModifiedBy = @SessionID,    
                               ModifiedOn = Getdate(),    
                               HistoryID = ( CASE    
                                               WHEN @FileID IS NOT NULL THEN    
                                                 @FileID    
                                               ELSE    
                                                 HistoryID    
                                             END )    
                        WHERE  ID = @ID    
                               AND DomainID = @DomainID    
    
                        SET @ExpenseHistoryID = (SELECT HistoryID    
                                                 FROM   tblExpense    
                                                 WHERE  ID = @ID    
                                                        AND DomainID = @DomainID)    
    
                        IF( (SELECT Count(1)    
                             FROM   @ExpenseDetail) > 0 )    
                          BEGIN    
                              UPDATE tblExpenseDetails    
  SET    tblExpenseDetails.LedgerID = S.LedgerID,    
                                     tblExpenseDetails.Remarks = S.ExpenseDetailRemarks,    
                                     ModifiedBy = @SessionID,    
        ModifiedOn = Getdate(),    
                                     HistoryID = ( CASE    
                                                     WHEN @FileID IS NOT NULL THEN    
                                                       @FileID    
                                                     ELSE    
                                                       HistoryID    
                                                   END )    
                              FROM   tblExpenseDetails    
                                     INNER JOIN @ExpenseDetail AS S    
                                             ON tblExpenseDetails.ID = S.ID    
                                                AND S.ID <> 0    
    
                              INSERT INTO @DetailsHistoryIDTable    
                                          (HistoryID)    
                              SELECT HistoryID    
                              FROM   tblExpenseDetails    
                                     INNER JOIN @ExpenseDetail AS S    
                                             ON tblExpenseDetails.ID = S.ID    
                                                AND S.ID <> 0    
                          END    
    
                        SET @OUTPUT = (SELECT [Message]    
                                       FROM   tblErrorMessage    
                                       WHERE  [Type] = 'Information'    
                                              AND Code = 'RCD_UPD_DES'    
                                              AND IsDeleted = 0) --'The record is referred. Description is Updated.'                            
                        GOTO finish    
                    END    
                  ELSE    
                    BEGIN    
                        UPDATE tbl_FileUpload    
                        SET    ReferenceTable = 'tblExpense'    
                        WHERE  Id = @FileID    
    
                        UPDATE tblExpense    
                        SET    VendorID = @ExpenseVendorID,    
                               DueDate = @DueDate,    
                               [Date] = @Date,    
                               BillNo = @BillNo,    
                               CostCenterID = @CostCenterID,    
                               [Type] = @Type,    
                               Remarks = @Description,
                               Tag =@Tag,     
                               HistoryID = @FileID,    
                               ModifiedBy = @SessionID,    
                               ModifiedOn = Getdate()    
                        WHERE  ID = @ID    
                               --AND [Type] = 'Bill'      
                               AND DomainID = @DomainID    
    
                        IF( (SELECT Count(1)    
                             FROM   @ExpenseDetail) > 0 )    
                          BEGIN    
                              UPDATE tblExpenseDetails    
                              SET    tblExpenseDetails.LedgerID = S.LedgerID,    
                                     tblExpenseDetails.Remarks = S.ExpenseDetailRemarks,    
                                     tblExpenseDetails.Amount = S.Amount,    
                                     tblExpenseDetails.Qty = S.Qty,    
                                     tblExpenseDetails.CGSTPercent = S.CGST,    
                                     tblExpenseDetails.SGSTPercent = S.SGST,    
                                     tblExpenseDetails.CGST = S.CGSTAmount,    
                                     tblExpenseDetails.SGST = S.SGSTAmount,    
                                     tblExpenseDetails.IsDeleted = S.IsDeleted,    
                                     tblExpenseDetails.IsLedger = S.IsProduct,    
                                     ModifiedBy = @SessionID,    
                                     ModifiedOn = Getdate()    
                              FROM   tblExpenseDetails    
                                     INNER JOIN @ExpenseDetail AS S    
                           ON tblExpenseDetails.ID = S.ID    
                                       AND S.ID <> 0    
    
                              INSERT INTO @DetailsHistoryIDTable    
                                          (HistoryID)    
                              SELECT HistoryID    
                              FROM   tblExpenseDetails    
                                     INNER JOIN @ExpenseDetail AS S    
                                             ON tblExpenseDetails.ID = S.ID    
                                                AND S.ID <> 0    
                          END    
    
                        IF( (SELECT Count(1)    
                             FROM   @ExpenseDetail) > 0 )    
                          BEGIN    
                              INSERT INTO tblExpenseDetails    
                                          (ExpenseID,    
                                           LedgerID,    
                                           Remarks,    
                                           QTy,    
                                           Amount,    
                                           CGSTPercent,    
                                           SGSTPercent,    
                                           CGST,    
                                           SGST,    
                                           IsLedger,    
                                           DomainID,    
                                           CreatedBy,    
                                           ModifiedBy)    
                              OUTPUT      INSERTED.HistoryID    
                              INTO @DetailsHistoryIDTable(HistoryID)    
                              SELECT @ID,    
                                     LedgerID,    
                                     ExpenseDetailRemarks,    
                                     QTy,    
                                     Amount,    
                                     CGST,    
                                     SGST,    
                                     CGSTAmount,    
                                     sGSTAmount,    
                                     IsProduct,    
                                     @DomainID,    
                                     @SessionID,    
                                     @SessionID    
                              FROM   @ExpenseDetail    
                              WHERE  ID = 0    
                                     AND IsDeleted = 0    
                                     AND LedgerID <> 0    
                          END    
    
                        UPDATE tblExpense    
                        SET    Remarks = @Description,    
                               HistoryID = @FileID,    
                               Date = @Date,    
                               CostCenterID = @CostCenterID,    
                               ModifiedBy = @SessionID,    
                               ModifiedOn = Getdate()    
                        WHERE  ID = @ID    
                               AND DomainID = @DomainID    
    
                        UPDATE tblExpensePayment    
                        SET    PaymentModeID = @PaymentModeID,    
                               BankID = Isnull(@BankID, 0),    
                               Reference = @Reference,    
                               Date = @Date,    
                               ModifiedBy = @SessionID,    
                               ModifiedOn = Getdate()    
                        WHERE  ID = (SELECT ExpensePaymentID    
                                     FROM   tblExpensePaymentMapping    
                                     WHERE  ExpenseID = @ID    
                                            AND DomainID = @DomainID)    
                               AND DomainID = @DomainID    
    
                        UPDATE tblExpensePaymentMapping    
                        SET    ExpensePaymentID = (SELECT ExpensePaymentID    
                                                   FROM   tblExpensePaymentMapping    
                    WHERE  ExpenseID = @ID    
                    AND DomainID = @DomainID),    
                               ExpenseID = @ExpenseID,    
                               Amount = @TotalAmount,    
                               ModifiedBy = @SessionID,    
                               ModifiedOn = Getdate()    
                        WHERE  ID = (SELECT EM.id    
                                     FROM   tblExpensePaymentMapping EM    
                                            JOIN tblExpensePayment EP    
                                              ON EP.id = EM.ExpensePaymentID    
                                     WHERE  ExpenseID = @ID    
                                            AND EM.isdeleted = 0    
                                            AND EP.isdeleted = 0    
                                            AND EM.DomainID = @DomainID)    
                               AND DomainID = @DomainID    
    
                        IF( @PaymentModeID = (SELECT ID    
                                              FROM   tbl_CodeMaster    
                                              WHERE  [Type] = 'PaymentType'    
                                                     AND Code = 'Cash'    
                                                     AND IsDeleted = 0) )    
                          BEGIN    
                              IF ( @PreviousMode = (SELECT ID    
                                                    FROM   tbl_CodeMaster    
                                                    WHERE  [Type] = 'PaymentType'    
                                                           AND Code = 'Cash'    
                                                           AND IsDeleted = 0) )    
                                BEGIN    
                                    UPDATE tblCashBucket    
                                    SET    AvailableCash = ( AvailableCash + @PreviousCash ) - @TotalAmount    
                                    WHERE  @DomainID = DomainID    
    
                                    UPDATE tblVirtualCash    
                                    SET    Amount = 0 - @TotalAmount    
                                    WHERE  SourceID = (SELECT EP.ID    
                                                       FROM   tblexpensepayment EP    
                                                              LEFT JOIN tblExpensepaymentmapping EM    
                                                                     ON EP.id = EM.expensePaymentID    
                                                       WHERE  EP.isdeleted = 0    
                                                              AND EM.expenseID = @id    
                                                              AND EP.DomainID = @DomainID)    
                                           AND SourceType = (SELECT ID    
                                                             FROM   tblMasterTypes    
                                                             WHERE  NAME = 'Expense')    
                                           AND IsDeleted = 0    
                                           AND DomainID = @DomainID    
                                END    
                              ELSE    
                                BEGIN    
                                    UPDATE tblCashBucket    
                                    SET    AvailableCash = AvailableCash - @TotalAmount    
                                    WHERE  @DomainID = DomainID    
    
                                    INSERT INTO tblVirtualCash    
                                                (SourceID,    
                                                 Amount,    
                                                 Date,    
                                                 SourceType,    
                                                 DomainID,    
                                                 CreatedBy,    
                                                 ModifiedBy)    
                                 VALUES      ((SELECT EP.ID    
                                                  FROM   tblexpensepayment EP    
                               LEFT JOIN tblExpensepaymentmapping EM    
                                                                ON EP.id = EM.expensePaymentID    
                                                  WHERE  isnull(EP.isdeleted, 0) = 0    
                                                         AND EM.expenseID = @id    
                                                         AND EP.DomainID = @DomainID),    
                                                 0 - @TotalAmount,    
                                                 @Date,    
                                                 (SELECT ID    
                                                  FROM   tblMasterTypes    
                                                  WHERE  NAME = 'Expense'),    
                                                 @DomainID,    
                                                 @SessionID,    
                                                 @SessionID)    
                                END    
                          END    
    
                        IF( Isnull(@BankID, 0) <> 0 )    
                          BEGIN    
                              IF EXISTS(SELECT 1    
                                        FROM   tblBRS    
                                        WHERE  SourceID = (SELECT EP.ID    
                                                           FROM   tblexpensepayment EP    
                                                                  LEFT JOIN tblExpensepaymentmapping EM    
                                                                         ON EP.id = EM.expensePaymentID    
                                                           WHERE  EP.isdeleted = 0    
                                                                  AND EM.expenseID = @id    
                                                                  AND EP.DomainID = @DomainID)    
                                               AND SourceType = (SELECT ID    
                                                                 FROM   tblMasterTypes    
                                                                 WHERE  NAME = 'Expense')    
                                               AND DomainID = @DomainID    
                                               AND IsDeleted = 0)    
                                BEGIN    
                                    UPDATE tblbrs    
                                    SET    Amount = @TotalAmount,    
                                           BRSDescription = (SELECT TOP 1 [ExpenseDetailRemarks]    
                                                             FROM   @ExpenseDetail),    
                                           BankID = @BankID,    
                                           SourceType = (SELECT ID    
                                                         FROM   tblMasterTypes    
                                                         WHERE  NAME = 'Expense')    
                                    WHERE  SourceID = (SELECT EP.ID    
                                                       FROM   tblexpensepayment EP    
                                                              LEFT JOIN tblExpensepaymentmapping EM    
                                                                     ON EP.id = EM.expensePaymentID    
                                                       WHERE  EP.isdeleted = 0    
                                                              AND EM.expenseID = @id    
                                                              AND EP.DomainID = @DomainID)    
                                           AND SourceType = (SELECT ID    
                                                             FROM   tblMasterTypes    
                                                             WHERE  NAME = 'Expense')    
                    AND DomainID = @DomainID    
    
                                    UPDATE tblBookedBankBalance    
                                    SET    Amount = @TotalAmount    
                WHERE  BRSID = (SELECT ID    
                            FROM   tblBRS    
                                                    WHERE  SourceID = (SELECT EP.ID    
                                                                       FROM   tblexpensepayment EP    
                                                                              LEFT JOIN tblExpensepaymentmapping EM    
                                                                                     ON EP.id = EM.expensePaymentID    
                                                                       WHERE  EP.isdeleted = 0    
                                                                              AND EM.expenseID = @id    
                                                                              AND EP.DomainID = @DomainID)    
                                                           AND SourceType = (SELECT ID    
                                                                             FROM   tblMasterTypes    
                                                                             WHERE  NAME = 'Expense')    
                                                           AND IsDeleted = 0    
                                                           AND DomainID = @DomainID)    
                                           AND DomainID = @DomainID    
    
                                    EXEC Managesystembankbalance    
                                      @DomainID    
                                END    
                              ELSE    
                                BEGIN    
                                    INSERT INTO tblBRS    
                                                (BankID,    
                                                 SourceID,    
                                                 SourceDate,    
                                                 SourceType,    
                                                 Amount,    
                                                 BRSDescription,    
                                                 CreatedBy,    
                                                 ModifiedBy,    
                                                 DomainID)    
                                    VALUES      (@BankID,    
                                                 (SELECT EP.ID    
                                                  FROM   tblexpensepayment EP    
                                                         LEFT JOIN tblExpensepaymentmapping EM    
                                                                ON EP.id = EM.expensePaymentID    
                                                  WHERE  EP.isdeleted = 0    
                                                         AND EM.expenseID = @ID    
                                                         AND EP.DomainID = @DomainID),    
                                                 @Date,    
                                                 (SELECT ID    
                                                  FROM   tblMasterTypes    
                                                  WHERE  NAME = 'Expense'),    
                                                 @TotalAmount,    
                                                 (SELECT TOP 1 [ExpenseDetailRemarks]    
                                                  FROM   @ExpenseDetail),    
                                                 @SessionID,    
                                                 @SessionID,    
                                                 @DomainID)    
    
                                    INSERT INTO tblBookedBankBalance    
                                                (BRSID,    
                                                 Amount,    
               DomainID,    
                                                 CreatedBy,    
                                                 ModifiedBy)    
                                    VALUES      (@@IDENTITY,    
                                                 @TotalAmount,    
                                                 @DomainID,    
                                                 @SessionID,    
                                                 @SessionID)    
    
                                    EXEC Managesystembankbalance    
                                      @DomainID    
    
                                    UPDATE tblCashBucket    
                                    SET    AvailableCash = AvailableCash + @TotalAmount    
                                    WHERE  @DomainID = DomainID    
    
                                    UPDATE tblVirtualCash    
                                    SET    IsDeleted = 1    
                                    WHERE  SourceID = (SELECT EP.ID    
                                                       FROM   tblexpensepayment EP    
                                                              LEFT JOIN tblExpensepaymentmapping EM    
                                                                     ON EP.id = EM.expensePaymentID    
                                                       WHERE  EP.isdeleted = 0    
                                                              AND EM.expenseID = @ID    
                                                              AND EP.DomainID = @DomainID)    
                                           AND SourceType = (SELECT ID    
                                                             FROM   tblMasterTypes    
                                                             WHERE  NAME = 'Expense')    
                                           AND IsDeleted = 0    
                                           AND DomainID = @DomainID    
                                END    
                          END    
                        ELSE    
                          BEGIN    
                              IF( (SELECT 1    
                                   FROM   tblBRS    
                                   WHERE  SourceID = (SELECT EP.ID    
                                                      FROM   tblexpensepayment EP    
                                                             LEFT JOIN tblExpensepaymentmapping EM    
                                                                    ON EP.id = EM.expensePaymentID    
                                                      WHERE  EP.isdeleted = 0    
                                                             AND EM.expenseID = @ID    
                                                             AND EP.DomainID = @DomainID)    
                                          AND SourceType = (SELECT ID    
                                                            FROM   tblMasterTypes    
                                                            WHERE  NAME = 'Expense')    
                                          AND DomainID = @DomainID    
                                          AND IsDeleted = 0    
                                          AND DomainID = @DomainID) = 1 )    
                                BEGIN    
                                    UPDATE tblBookedBankBalance    
                                    SET    IsDeleted = 1    
                                    WHERE  BRSID = (SELECT ID    
                                                    FROM   tblBRS    
                                                    WHERE  SourceID = (SELECT EP.ID    
                                                                       FROM   tblexpensepayment EP    
                                                                              LEFT JOIN tblExpensepaymentmapping EM    
                                                                                     ON EP.id = EM.expensePaymentID    
                                                                       WHERE  EP.isdeleted = 0    
                                                                              AND EM.expenseID = @ID    
                                                                              AND EP.DomainID = @DomainID)    
                                                           AND SourceType = (SELECT ID    
                   FROM   tblMasterTypes    
                                                                             WHERE  NAME = 'Expense')    
                                                           AND IsDeleted = 0    
                                                           AND DomainID = @DomainID)    
                                           AND DomainID = @DomainID    
    
                                    UPDATE tblBRS    
                                    SET    IsDeleted = 1    
                                    WHERE  SourceID = (SELECT EP.ID    
                                                       FROM   tblexpensepayment EP    
                                                              LEFT JOIN tblExpensepaymentmapping EM    
                                                                     ON EP.id = EM.expensePaymentID    
                                                       WHERE  EP.isdeleted = 0    
                                                              AND EM.expenseID = @ID    
                                                              AND EP.DomainID = @DomainID)    
                                           AND SourceType = (SELECT ID    
                                                             FROM   tblMasterTypes    
                                                             WHERE  NAME = 'Expense')    
                                           AND DomainID = @DomainID    
                                           AND IsDeleted = 0    
    
                                    EXEC Managesystembankbalance    
                                      @DomainID    
                                END    
                          END    
    
                        SET @Output = (SELECT [Message]    
                                       FROM   tblErrorMessage    
                                       WHERE  [Type] = 'Information'    
                                              AND Code = 'EXP_UPD'    
                                              AND IsDeleted = 0) --'Expense Updated Successfully'                            
                        ----Update item to Inventory      
                        EXEC ManageInventory    
                          @ID,    
                          'UPDATE',    
                          'Purchase',    
                          @SessionID,    
                          @DomainID    
    
                        GOTO finish    
                    END    
              END    
          END    
    
          FINISH:    
    
          EXEC Managebusinesseventslog    
            @ExpenseHistoryID,    
            'Date,VendorID',    
            'tblExpense'    
    
          EXEC Managebusinesseventslog    
            @ExpensePayHistoryID,    
            'PaymentModeID,BankID',    
            'tblExpensePayment'    
    
          EXEC Managebusinesseventslog    
            @ExpensePayMapHistoryID,    
            'Amount',    
            'tblExpensePaymentMapping'    
    
          DECLARE @TempCount INT = 1,    
                  @Count     INT = (SELECT Count(1)    
                     FROM   @DetailsHistoryIDTable)    
    
          WHILE @Count >= @TempCount    
            BEGIN    
                SET @ExpenseDtlHistoryID= (SELECT HistoryID    
                                           FROM   @DetailsHistoryIDTable    
                                           WHERE  ID = @TempCount)    
    
                EXEC Managebusinesseventslog    
                  @ExpenseDtlHistoryID,    
                  'LedgerID,Amount',    
                  'tblExpenseDetails'    
    
                SET @TempCount = @TempCount + 1    
            END    
    
          SELECT @Output    
    
          UPDATE tblExpense    
          SET    PaidAmount = (SELECT Sum(Amount)    
                               FROM   tblExpensePaymentMapping    
                               WHERE  IsDeleted = 0    
                                      AND ExpenseID = @ExpenseID    
                                      AND DomainID = @DomainID),    
                 StatusID = CASE    
  WHEN ( (SELECT Sum(Amount)    
                                      FROM   tblExpensePaymentMapping    
                                      WHERE  IsDeleted = 0    
                                             AND ExpenseID = @ExpenseID    
                                             AND DomainID = @DomainID) >= (SELECT ( ( Isnull(Sum(QTY * Amount), 0) ) + Isnull(Sum(CGST), 0) + Isnull(Sum(SGST), 0) )    
                                                                           FROM   tblExpenseDetails    
                                                                           WHERE  ExpenseID = @ExpenseID    
                                                                                  AND IsDeleted = 0    
                                                                                  AND DomainID = @DomainID) ) THEN    
                                (SELECT ID    
                                 FROM   tbl_CodeMaster    
                                 WHERE  [Type] = 'Close'    
                                        AND IsDeleted = 0)    
                              WHEN (( Isnull((SELECT Sum(Amount)    
                                              FROM   tblExpensePaymentMapping    
                                              WHERE  IsDeleted = 0    
                                                     AND ExpenseID = @ExpenseID    
                                                     AND DomainID = @DomainID), 0) = 0 )) THEN    
                                (SELECT ID    
                                 FROM   tbl_CodeMaster    
                                 WHERE  [type] = 'Open'    
                                        AND IsDeleted = 0)    
                              WHEN ( Isnull((SELECT Sum(Amount)    
                                             FROM   tblExpensePaymentMapping    
                                             WHERE  IsDeleted = 0    
                                                    AND ExpenseID = @ExpenseID    
                                                    AND DomainID = @DomainID), 0) <> 0 ) THEN    
                                (SELECT ID    
                                 FROM   tbl_CodeMaster    
                                 WHERE  [type] = 'Partial'    
                                        AND IsDeleted = 0)    
                            END,    
                 ModifiedBy = @SessionID,    
                 ModifiedOn = Getdate()    
          WHERE  ID = @ExpenseID    
                 AND DomainID = @DomainID    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
