﻿/****************************************************************************     
CREATED BY   : K.SASIREKHA    
CREATED DATE  : 29-09-2017    
MODIFIED BY   : DHANALAKSHMI.S    
MODIFIED DATE  :     
 <summary>            
      
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageTDSLimitConfiguration] (@ID        INT,
                                                     @Limit     MONEY,
                                                     @SectionID INT,
                                                     @FYID      INT,
                                                     @RegionID  INT,
                                                     @SessionID INT,
                                                     @IsDeleted BIT,
                                                     @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output         VARCHAR(8000),
              @SectionID80C   INT,
              @SectionID80CCC INT

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'
          SET @SectionID80C = (SELECT ID
                               FROM   TDSSection
                               WHERE  Code = '80C'
                                      --AND DomainID = @DomainID    
                                      AND IsDeleted = 0)
          SET @SectionID80CCC = (SELECT ID
                                 FROM   TDSSection
                                 WHERE  Code = '80CCC'
                                        --AND DomainID = @DomainID    
                                        AND IsDeleted = 0)

          IF( Isnull(@ID, 0) != 0 )
            BEGIN
                IF( @IsDeleted = 1 )
                  BEGIN
                      UPDATE tbl_TDSLimitConfiguration
                      SET    IsDeleted = @IsDeleted,
                             ModifiedBy = @SessionID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID
                             AND DomainID = @DomainID

                      SET @Output = 'Deleted Successfully.'

                      GOTO Finish
                  END

                IF( @IsDeleted = 0 )
                  BEGIN
                      IF NOT EXISTS(SELECT 1
                                    FROM   tbl_TDSLimitConfiguration
                                    WHERE  @ID <> ID
                                           AND FYID = @FYID
                                           AND SectionID = @SectionID
                                           AND IsDeleted = 0
                                           AND DomainID = @DomainID
                                           AND RegionID = @RegionID)
                        BEGIN
                            IF( @SectionID80C = @SectionID )
                              IF EXISTS(SELECT 1
                                        FROM   tbl_TDSLimitConfiguration
                                        WHERE  ID <> @ID
                                               AND SectionID = @SectionID80CCC
                                               AND FYID = @FYID
                                               AND IsDeleted = 0
                                               AND DomainID = @DomainID
                                               AND RegionID = @RegionID)
                                BEGIN
                                    SET @Output = 'Already Exists'

                                    GOTO FINISH
                                END
                              ELSE
                                BEGIN
                                    GOTO UPDATEDATA
                                END
                            ELSE IF( @SectionID80CCC = @SectionID )
                              IF EXISTS(SELECT 1
                                        FROM   tbl_TDSLimitConfiguration
                                        WHERE  ID <> @ID
                                               AND SectionID = @SectionID80C
                                               AND FYID = @FYID
                                               AND IsDeleted = 0
                                               AND DomainID = @DomainID
                                               AND RegionID = @RegionID)
                                BEGIN
                                    SET @Output = 'Already Exists'

                                    GOTO FINISH
                                END
                              ELSE
                                BEGIN
                                    GOTO UPDATEDATA
                                END
                            ELSE
                              BEGIN
                                  GOTO UPDATEDATA
                              END
                        END
                      ELSE
                        BEGIN
                            SET @Output = 'Already Exists'

                            GOTO FINISH
                        END
                  END
            END

          IF( Isnull(@ID, 0) = 0 )
            BEGIN
                IF NOT EXISTS(SELECT 1
                              FROM   tbl_TDSLimitConfiguration
                              WHERE  @ID <> ID
                                     AND FYID = @FYID
                                     AND SectionID = @SectionID
                                     AND IsDeleted = 0
                                     AND DomainID = @DomainID
                                     AND RegionID = @RegionID)
                  BEGIN
                      IF( @SectionID80C = @SectionID )
                        IF EXISTS(SELECT 1
                                  FROM   tbl_TDSLimitConfiguration
                                  WHERE  ID <> @ID
                                         AND SectionID = @SectionID80CCC
                                         AND FYID = @FYID
                                         AND IsDeleted = 0
                                         AND DomainID = @DomainID
                                         AND RegionID = @RegionID)
                          BEGIN
                              SET @Output = 'Already Exists'

                              GOTO FINISH
                          END
                        ELSE
                          BEGIN
                              GOTO INSERTDATA
                          END
                      ELSE IF( @SectionID80CCC = @SectionID )
                        IF EXISTS(SELECT 1
                                  FROM   tbl_TDSLimitConfiguration
                                  WHERE  ID <> @ID
                                         AND SectionID = @SectionID80C
                                         AND FYID = @FYID
                                         AND IsDeleted = 0
                                         AND DomainID = @DomainID
                                         AND RegionID = @RegionID)
                          BEGIN
                              SET @Output = 'Already Exists'

                              GOTO FINISH
                          END
                        ELSE
                          BEGIN
                              GOTO INSERTDATA
                          END
                      ELSE
                        BEGIN
                            GOTO INSERTDATA
                        END
                  END
                ELSE
                  BEGIN
                      SET @Output = 'Already Exists'

                      GOTO FINISH
                  END
            END

          INSERTDATA:

          INSERT INTO tbl_TDSLimitConfiguration
                      (FYID,
                       SectionID,
                       Limit,
                       RegionID,
                       CreatedBy,
                       CreatedOn,
                       ModifiedBy,
                       ModifiedOn,
                       DomainID)
          VALUES      ( @FYID,
                        @SectionID,
                        @Limit,
                        @RegionID,
                        @SessionID,
                        Getdate(),
                        @SessionID,
                        Getdate(),
                        @DomainID)

          SET @Output = 'Inserted Successfully.'

          GOTO FINISH

          UPDATEDATA:

          UPDATE tbl_TDSLimitConfiguration
          SET    FYID = @FYID,
                 SectionID = @SectionID,
                 Limit = @Limit,
                 RegionID = @RegionID,
                 ModifiedBy = @SessionID,
                 ModifiedOn = Getdate()
          WHERE  ID = @ID
                 AND DomainID = @DomainID

          SET @Output = 'Updated Successfully.'

          GOTO FINISH

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
