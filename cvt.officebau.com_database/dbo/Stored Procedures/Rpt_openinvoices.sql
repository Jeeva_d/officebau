﻿/****************************************************************************     
CREATED BY      : Dhanalakshmi s   
CREATED DATE  : 26-March-2019    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>      
 [Rpt_OpenInvoices] 'asdaf'  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Rpt_openinvoices] (@StartDate DATETIME = NULL,            
                                            @EndDate   DATETIME = NULL,  
                                            @CustomerName VARCHAR(100)= NULL,  
                                            @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
         DECLARE @FinancialYear INT;    
          DECLARE @StartFinancialMonth INT;    
          DECLARE @EndFinancialMonth INT;    
            
        IF ( @StartDate IS NULL    
                OR @StartDate = '' )    
              OR ( @EndDate IS NULL    
                    OR @EndDate = '' )    
            BEGIN    
                SET @StartFinancialMonth = (SELECT Value    
                                            FROM   tblApplicationConfiguration    
                                            WHERE  Code = 'STARTMTH'    
                                                   AND DomainID = @DomainID)    
                SET @EndFinancialMonth = ( ( (SELECT Value    
                                              FROM   tblApplicationConfiguration    
                                              WHERE  Code = 'STARTMTH'    
                                                     AND DomainID = @DomainID)    
                                             + 11 ) % 12 )    
    
                IF( Month(Getdate()) <= ( ( (SELECT Value    
                                             FROM   tblApplicationConfiguration    
                                             WHERE  Code = 'STARTMTH'    
                                                    AND DomainID = @DomainID)    
                                            + 11 ) % 12 ) )    
                  SET @FinancialYear = Year(Getdate()) - 1    
                ELSE    
                  SET @FinancialYear = Year(Getdate())    
    
                SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date        
                SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date       
            END;    
              
          SELECT Customer,  
                 InvoiceNo        AS [Invoice No],  
                 [Description],  
                 TotalAmount      AS [Invoiced Amount],  
                 Currency,  
                  RIGHT(CONVERT(VARCHAR, 0) + CONVERT(VARCHAR, Datepart(DAY, [Invoice Date])), 2)    
                 + '-'    
                 + CONVERT(VARCHAR(3), Datename(Month, [Invoice Date]))    
                 + '-'    
                 + CONVERT(VARCHAR(4), Datepart(YEAR, [Invoice Date]))   AS [Invoiced Date],  
                 RIGHT(CONVERT(VARCHAR, 0) + CONVERT(VARCHAR, Datepart(DAY, DueDate)), 2)    
                 + '-'    
                 + CONVERT(VARCHAR(3), Datename(Month, DueDate))    
                 + '-'    
                 + CONVERT(VARCHAR(4), Datepart(YEAR, DueDate))          AS [Due Date],  
                 [Revenue center] AS [Revenue Center]  
          FROM   Vw_OpenInvoices  
          WHERE  ( Isnull(@CustomerName, '') = ''  
                    OR Isnull(Customer, '') = @CustomerName )  
                     AND ( @StartDate = ''    
                                 OR [Invoice Date] >= @StartDate )    
                          AND ( @EndDate = ''    
                                 OR [Invoice Date] <= @EndDate )    
                          END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
