﻿/****************************************************************************   
CREATED BY   : Jeeva  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [Adm_UserValidate] '',''
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Adm_CreateDomain] (@Code VARCHAR(100),
                                                 @Domain      Varchar(500),
												 @Email Varchar(200),
												 @UserID int)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION
		  dECLARE @OUTPUT VARCHAR(10)
       IF  Exists(Select * from Adm_DomainManager Where Code = @Code)
	     Begin
	Update Adm_DomainManager SET Name=@Domain,EmailID=@Email Where Code=@Code
	set @OUTPUT = 'Updated'
		 END
		Else
		 BEGIN
			INSERT INTO  Adm_DomainManager (Code,Name,EmailID,CreatedBy,ModifiedBy)
				Select @Code,@Domain,@Email,@UserID,@UserID
			Insert Into tblEmployeeMaster (Code,FirstName,LastName,PrefixID,DesignationID	,ContactNo,Gender,EmailID	,DOJ	,HasAccess)
				Select @code,@Email,'',0,0,0,0,@Email,GETDATE(),1
			set @OUTPUT = 'Inserted'
			Insert INTO tblLogin (UserID,Password,[RecentPasswordChangeDate])
				Select @@IDENTITY,'eWK9XoF32iP4UxA86wE+9LRLWKCN4NItMXvaPr8LR/Q=',GETDATE()
		 END
		 Select @OUTPUT
          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END




