﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :            
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>           
 </summary>                                   
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageExpenseHolding] (@ID INT,  
@ExpenseID INT,  
@Date DATE,  
@Amount MONEY,  
@Isdeleted BIT,  
@LedgerID INT, @SessionID INT, @DomainID INT)  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 DECLARE @Output VARCHAR(1000)  
  
 BEGIN TRY  
  BEGIN TRANSACTION  
  
  BEGIN  
   IF @Isdeleted = 1  
   BEGIN  
    UPDATE tblExpenseHoldings  
    SET Isdeleted = 1  
       ,ModifiedBy = @SessionID  
       ,ModifiedOn = GETDATE()  
    WHERE id = @ID;  
    SET @Output = 'Deleted Successfully'  
    GOTO FINISH;  
   END  
   IF @ID = 0  
   BEGIN  
    INSERT INTO tblExpenseHoldings (ExpenseId, Amount, DATE, LedgerId, CreatedBy, CreatedOn, ModifiedBy, ModifiedOn, DomainID)  
     SELECT  
      @ExpenseID  
        ,@Amount  
        ,@Date  
        ,@LedgerID  
        ,@SessionID  
        ,GETDATE()  
        ,@SessionID  
        ,GETDATE()  
        ,@DomainID  
    SET @Output = 'Inserted  Successfully'  
    GOTO FINISH;  
  
   END  
   ELSE  
   BEGIN  
    UPDATE tblExpenseHoldings  
    SET DATE = @Date  
       ,LedgerId = @LedgerID  
       ,Amount = @Amount  
    WHERE id = @ID  
    SET @Output = 'Updated  Successfully'  
    GOTO FINISH;  
  
  
   END  
  END  
  
 FINISH:  
  
  UPDATE tblExpense  
  SET tblExpense.PaidAmount = (SELECT  
     SUM(Amount)  
    FROM tblExpensePaymentMapping  
    WHERE IsDeleted = 0  
    AND ExpenseID = S.id  
    AND DomainID = @DomainID)  
     ,tblExpense.StatusID =  
   CASE  
    WHEN (((SELECT  
      ISNULL( SUM(Amount),0)  
      FROM tblExpensePaymentMapping  
      WHERE IsDeleted = 0  
      AND ExpenseID = S.id  
      AND DomainID = @DomainID)  
     + (SELECT  
       ISNULL(SUM(AMOUNT), 0)  
      FROM tblExpenseHoldings  
      WHERE ExpenseId = S.id  
      AND Isdeleted = 0  
      AND DomainID = @DomainID)  
     ) >= (SELECT  
       ((ISNULL(SUM(QTY * Amount), 0)) + ISNULL(SUM(CGST), 0) + ISNULL(SUM(SGST), 0))  
      FROM tblExpenseDetails  
      WHERE ExpenseID = S.id  
      AND IsDeleted = 0  
      AND DomainID = @DomainID)  
     ) THEN (SELECT  
       id  
      FROM tbl_CodeMaster  
      WHERE [type] = 'Close'  
      AND IsDeleted = 0)  
    WHEN (((ISNULL((SELECT  
ISNULL( SUM(Amount),0)  
      FROM tblExpensePaymentMapping  
      WHERE IsDeleted = 0  
      AND ExpenseID = S.id  
      AND DomainID = @DomainID)  
     , 0) + (SELECT  
       ISNULL(SUM(AMOUNT), 0)  
      FROM tblExpenseHoldings  
      WHERE ExpenseId = S.id  
      AND Isdeleted = 0  
      AND DomainID = @DomainID)  
     ) = 0)) THEN (SELECT  
       id  
      FROM tbl_CodeMaster  
      WHERE [type] = 'Open'  
      AND IsDeleted = 0)  
    WHEN ((ISNULL((SELECT  
       ISNULL( SUM(Amount),0)  
      FROM tblExpensePaymentMapping  
      WHERE IsDeleted = 0  
      AND ExpenseID = S.id  
      AND DomainID = @DomainID)  
     , 0) + (SELECT  
       ISNULL(SUM(AMOUNT), 0)  
      FROM tblExpenseHoldings  
      WHERE ExpenseId = S.id  
      AND Isdeleted = 0  
      AND DomainID = @DomainID)  
     ) <> 0) THEN (SELECT  
       id  
      FROM tbl_CodeMaster  
      WHERE [type] = 'Partial'  
      AND IsDeleted = 0)  
   END  
  FROM tblExpense s  
  SELECT  
   @Output  
  
  COMMIT TRANSACTION  
 END TRY  
 BEGIN CATCH  
  ROLLBACK TRANSACTION  
  DECLARE @ErrorMsg VARCHAR(100)  
      ,@ErrSeverity TINYINT  
  SELECT  
   @ErrorMsg = ERROR_MESSAGE()  
     ,@ErrSeverity = ERROR_SEVERITY()  
  RAISERROR (@ErrorMsg, @ErrSeverity, 1)  
 END CATCH  
END  
  
 ROLLBACK
