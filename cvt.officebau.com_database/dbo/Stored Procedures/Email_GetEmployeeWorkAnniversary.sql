﻿CREATE PROCEDURE [dbo].[Email_GetEmployeeWorkAnniversary] (@Type VARCHAR(20))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN
              DECLARE @KeyID INT 

              SET @KeyID = (SELECT ID
                            FROM   tbl_MailerEventKeys
                            WHERE  NAME = 'BIRTHDAY'
                                   AND Isdeleted = 0)

              SELECT emp.Code                                                              AS Code,
                     emp.FirstName + ' ' + Isnull(emp.lastName, '')                        AS EmployeeName,
                     emp.EmailID                                                           AS EmailID,
                     com.FullName                                                          AS CompanyName,
                     com.NAME                                                              AS CompanyFolderName,
                     Cast(DOJ AS DATE)                                                     AS DOJ,
                     de.NAME                                                               AS Designation,
                     BU.NAME                                                               AS Location,
                     fu.Path                                                               AS ProfilePhoto,
                     Stuff((SELECT DISTINCT ';' + EmailID
                            FROM   tbl_EmployeeMaster emp
                                   JOIN (SELECT *
                                         FROM   dbo.Splitstring ((SELECT CCEmailID
                                                                  FROM   tbl_EmailRecipients
                                                                  WHERE  BusinessUnitID = emp.BaseLocationID
                                                                         AND KeyID = @KeyID), ',')
                                         WHERE  Isnull(Item, '') <> '') AS eml
                                     ON emp.ID = eml.Item
                            FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') AS CC 
              FROM   tbl_employeemaster emp
                     LEFT JOIN tbl_Company com
                            ON com.ID = emp.DomainID
                     LEFT JOIN tbl_BusinessUnit BU
                            ON bu.ID = emp.BaseLocationID
                     LEFT JOIN tbl_Designation de
                            ON de.ID = emp.DesignationID
                     LEFT JOIN tbl_FileUpload FU
                            ON FU.Id = emp.FileID
              WHERE  Cast(emp.DOJ AS DATE) = Cast(CONVERT(VARCHAR(10), Month( Getdate())) + '-'
                                                  + CONVERT(VARCHAR(10), Day( Getdate())) + '-'
                                                  + CONVERT(VARCHAR(10), Year(Getdate())- Cast(@Type AS INT))AS DATE)
                     AND emp.isdeleted = 0
                     AND emp.isactive = 0
          END
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
