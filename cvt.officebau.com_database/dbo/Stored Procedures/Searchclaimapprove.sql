﻿/****************************************************************************     
CREATED BY   :  Naneeshwar.M  
CREATED DATE  :     
MODIFIED BY   :   Ajith N  
MODIFIED DATE  :   05 Dec 2014  
 <summary>   
          [Searchclaimapprove] 1,0,3, 'pageload'  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchclaimapprove] (@DomainID      INT,  
                                            @StatusID      INT,  
                                            @EmployeeID    INT,  
                                            @OperationType VARCHAR(20))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Status VARCHAR(100)  
  
      IF( @OperationType = 'pageload' )  
        SET @Status = 'Submitted,Resubmitted'  
      ELSE IF( @OperationType = 'search'  
          AND @StatusID = 0 )  
        SET @Status = 'Submitted,Resubmitted,Approved,Rejected'  
      ELSE IF( @OperationType = 'search'  
          AND @StatusID = (SELECT ID  
                           FROM   tbl_Status  
                           WHERE  Code = 'Submitted'  
                                  AND [Type] = 'Claims'  
                                  AND IsDeleted = 0) )  
        BEGIN  
            SET @StatusID = 0;  
            SET @Status = 'Submitted'  
        END  
      ELSE IF( @OperationType = 'search'  
          AND @StatusID = (SELECT ID  
                           FROM   tbl_Status  
                           WHERE  Code = 'Resubmitted'  
                                  AND [Type] = 'Claims'  
                                  AND IsDeleted = 0) )  
        BEGIN  
            SET @StatusID = 0;  
            SET @Status = 'Resubmitted'  
        END  
  
      BEGIN TRY  
          BEGIN  
              --IF( @StatusID = (SELECT ID  
              --                 FROM   tbl_Status  
              --                 WHERE  Code = 'Submitted'  
              --                        AND [Type] = 'Claims'  
              --                        AND IsDeleted = 0) )  
              --  SET @StatusID = 0;  
              WITH CTE  
                   AS (SELECT CLR.ID                                      AS ID,  
                              SubmittedDate                               AS ClaimedDate,  
                              ( CASE  
                                  WHEN (SELECT ID  
                                        FROM   tbl_Status  
                                        WHERE  Code = 'Approved'  
                                               AND [Type] = 'Claims') = CLR.StatusID THEN CLA.ApprovedAmount  
                                  ELSE CLR.Amount  
                                END )                                     AS Amount,  
                               Isnull(EMP.EmpCodePattern, '') + Isnull(EMP.Code, '') + ' - ' + EMP.FullName AS Requester,  
                              CLR.CreatedBy                               AS RequesterID,  
                              -- Count(*)                                   AS Counts,  
                              ( CASE  
                                  WHEN STA.Code IS NULL THEN CLR.StatusID  
                                  ELSE CLA.StatusID  
                                END )                                     AS StatusID,  
                              ( CASE  
                                  WHEN STA.Code IS NULL THEN SAR.Code  
                                  ELSE STA.Code  
                                END )                                     AS [Status]  
                       FROM   tbl_ClaimsRequest CLR  
                              LEFT JOIN tbl_ClaimsApprove CLA  
                                     ON CLA.ClaimItemID = CLR.ID  
                              LEFT JOIN tbl_EmployeeMaster EMP  
                                     ON EMP.ID = CLR.CreatedBy  
                              LEFT JOIN tbl_Status STA  
                                     ON STA.ID = CLA.StatusID  
                              LEFT JOIN tbl_Status SAR  
                                    ON SAR.ID = CLR.StatusID  
                       WHERE  ApproverID = @EmployeeID  
                              AND CLR.IsDeleted = 0  
                              AND CLR.DomainId = @DomainID  
                              AND ( ( Isnull(@StatusID, 0) = 0  
                                      AND CLR.StatusID IN((SELECT ID  
                                                           FROM   tbl_Status  
                                                           WHERE  Code IN(SELECT Splitdata  
                                                                          FROM   Fnsplitstring(@Status, ','))  
                                                                  AND [Type] = 'Claims'  
                                                                  AND IsDeleted = 0)) )  
                                     OR CLA.StatusID = @StatusID ))  
              SELECT  ClaimedDate                               AS ClaimedDate,  
                     Sum(amount)                              AS Amount,  
                     Requester                                AS Requester,  
                     RequesterID                              AS RequesterID,  
                     StatusID                                 AS StatusID,  
                     [Status]                                 AS [Status],  
                     Count(1)                                 AS Counts,  
                     Cast(Row_number()  
                            OVER(  
                              ORDER BY Requester ASC) AS INT) AS RowNo  
              FROM   CTE  
              GROUP  BY  ClaimedDate ,  
                        RequesterID,  
                        Requester,  
                        StatusID,  
                        [Status]  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
