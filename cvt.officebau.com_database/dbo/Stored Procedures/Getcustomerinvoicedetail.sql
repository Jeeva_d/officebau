﻿/****************************************************************************   
CREATED BY   : Naneeshwar  
CREATED DATE  :   29-Nov-2016
MODIFIED BY   :   Ajith N
MODIFIED DATE  :   16 May 2017
 <summary>
 [Getcustomerinvoicedetail] 3,1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getcustomerinvoicedetail] (@CustomerID INT,
                                                  @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT CUS.PaymentTerms AS PaymentTerms,
                 CUS.CurrencyID   AS CurrencyID,
                 CUR.NAME         AS CurrencyName
          FROM   tblCustomer CUS
                 JOIN tblCurrency CUR
                   ON CUR.id = CUS.CurrencyID
          WHERE  CUS.IsDeleted = 0
                 AND ( CUS.ID = @CustomerID
                        OR @CustomerID IS NULL )
                 AND CUS.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
