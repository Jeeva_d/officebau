﻿/**************************************************************************** 
CREATED BY			:
CREATED DATE		:
MODIFIED BY			:
MODIFIED DATE		:
 <summary>        
 SearchBusinessEventLog '9741bdeb-e7aa-40bb-b8ed-d9e650d087c9'
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchBusinessEventsLog] (@GUID VARCHAR(100))
AS
  BEGIN
      BEGIN TRY
          SELECT BEL.ColumnName,
                 CASE
                   WHEN BEL.ColumnName LIKE '%Date%' THEN RIGHT(CONVERT(VARCHAR, 0) + CONVERT(VARCHAR, Datepart(DAY, BEL.ColumnValue)), 2)
                                                          + '-'
                                                          + CONVERT(VARCHAR(3), Datename(Month, BEL.ColumnValue))
                                                          + '-'
                                                          + CONVERT(VARCHAR(4), Datepart(YEAR, BEL.ColumnValue))
                   ELSE BEL.ColumnValue
                 END                                                   AS ColumnValue,
                 RIGHT(CONVERT(VARCHAR, 0) + CONVERT(VARCHAR, Datepart(DAY, bel.ModifiedOn)), 2)
                 + '-'
                 + CONVERT(VARCHAR(3), Datename(Month, bel.ModifiedOn))
                 + '-'
                 + CONVERT(VARCHAR(4), Datepart(YEAR, bel.ModifiedOn)) AS ModifiedOn,
                 BEL.ChangeType,
                 emp.FirstName + ' ' + emp.LastName                    AS EmployeeName
          FROM   tblBusinessEventsLog BEL
                 JOIN tblEmployeeMaster emp
                   ON BEL.ModifiedBy = emp.ID
          WHERE  GUID = @GUID
          ORDER  BY BEL.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
