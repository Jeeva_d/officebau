﻿/**************************************************************************** 
CREATED BY    		:	Naneeshwar M
CREATED DATE		:	04-DEC-2017
MODIFIED BY			:	Ajith N
MODIFIED DATE		:	22 Jan 2018
 <summary>  
		  UploadLeaveDays '9064','AFROJALAM','5','0','0','0',106,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[UploadLeaveDays] (@EmployeeCode VARCHAR(50),
                                         @EmployeeName VARCHAR(100),
                                         @Year         VARCHAR(100),
                                         @CL           VARCHAR(50),
                                         @PL           VARCHAR(50),
                                         @SL           VARCHAR(50),
                                         @SessionID    INT,
                                         @DomainID     INT)
AS
  BEGIN
      BEGIN TRY
          SET NOCOUNT ON;

          DECLARE @Output     VARCHAR(100) = '',
                  @YearName   INT,
                  @EmployeeID INT

          BEGIN TRANSACTION

          SET @EmployeeID=(SELECT ID
                           FROM   tbl_EmployeeMaster
                           WHERE  Code = @EmployeeCode
                                  AND Replace(( Rtrim(Ltrim(FirstName))
                                                + Rtrim(Ltrim(Isnull(LastName, ''))) ), ' ', '') = @EmployeeName
                                  AND IsDeleted = 0
                                  AND ( Isnull(IsActive, 0) = 0 ))

          DECLARE @LeaveType INT

          SET @YearName=(SELECT NAME
                         FROM   tbl_FinancialYear
                         WHERE  id = @Year
                                AND IsDeleted = 0
                                AND DomainID = @DomainID)
          SET @LeaveType=(SELECT ID
                          FROM   tbl_LeaveTypes
                          WHERE  code = 'CL'
                                 AND IsDeleted = 0
                                 AND DomainID = @DomainID)

          IF( @EmployeeID IS NOT NULL )
            BEGIN
                IF NOT EXISTS(SELECT 1
                              FROM   tbl_EmployeeAvailedLeave
                              WHERE  EmployeeID = @EmployeeID
                                     AND Year = @YearName
                                     AND IsDeleted = 0
                                     AND DomainID = @DomainID
                                     AND LeaveTypeID = @LeaveType)
                  INSERT INTO tbl_EmployeeAvailedLeave
                              (EmployeeID,
                               LeaveTypeID,
                               TotalLeave,
                               AvailedLeave,
                               Year,
                               DomainID,
                               CreatedBy,
                               ModifiedBy)
                  VALUES      (@EmployeeID,
                               @LeaveType,
                               --Isnull(@CL, 0),
                               ( CASE
                                   WHEN @CL = ''
                                         OR @CL IS NULL THEN
                                     0
                                   ELSE
                                     Cast(@CL AS DECIMAL(16, 1))
                                 END ),
                               0,
                               @YearName,
                               @DomainID,
                               @SessionID,
                               @SessionID)
                ELSE
                  UPDATE tbl_EmployeeAvailedLeave
                  SET    TotalLeave = ( CASE
                                          WHEN @CL = ''
                                                OR @CL IS NULL THEN
                                            0
                                          ELSE
                                            Cast(@CL AS DECIMAL(16, 1))
                                        END ),
                         ModifiedBy = @SessionID
                  WHERE  LeaveTypeID = @LeaveType
                         AND Year = @YearName
                         AND EmployeeID = @EmployeeID
                         AND Isnull(@CL, '') <> ''

                SET @LeaveType=(SELECT ID
                                FROM   tbl_LeaveTypes
                                WHERE  code = 'PL'
                                       AND IsDeleted = 0
                                       AND DomainID = @DomainID)

                IF NOT EXISTS(SELECT 1
                              FROM   tbl_EmployeeAvailedLeave
                              WHERE  EmployeeID = @EmployeeID
                                     AND Year = @YearName
                                     AND IsDeleted = 0
                                     AND DomainID = @DomainID
                                     AND LeaveTypeID = @LeaveType)
                  INSERT INTO tbl_EmployeeAvailedLeave
                              (EmployeeID,
                               LeaveTypeID,
                               TotalLeave,
                               AvailedLeave,
                               Year,
                               DomainID,
                               CreatedBy,
                               ModifiedBy)
                  VALUES      (@EmployeeID,
                               @LeaveType,
                               --Isnull(@PL, 0),
                               ( CASE
                                   WHEN @PL = ''
                                         OR @PL IS NULL THEN
                                     0
                                   ELSE
                                     Cast(@PL AS DECIMAL(16, 1))
                                 END ),
                               0,
                               @YearName,
                               @DomainID,
                               @SessionID,
                               @SessionID)
                ELSE
                  UPDATE tbl_EmployeeAvailedLeave
                  SET    TotalLeave = ( CASE
                                          WHEN @PL = ''
                                                OR @PL IS NULL THEN
                                            0
                                          ELSE
                                            Cast(@PL AS DECIMAL(16, 1))
                                        END ),
                         ModifiedBy = @SessionID
                  WHERE  LeaveTypeID = @LeaveType
                         AND Year = @YearName
                         AND EmployeeID = @EmployeeID
                         AND Isnull(@PL, '') <> ''

                SET @LeaveType=(SELECT ID
                                FROM   tbl_LeaveTypes
                                WHERE  code = 'SL'
                                       AND IsDeleted = 0
                                       AND DomainID = @DomainID)

                IF NOT EXISTS(SELECT 1
                              FROM   tbl_EmployeeAvailedLeave
                              WHERE  EmployeeID = @EmployeeID
                                     AND Year = @YearName
                                     AND IsDeleted = 0
                                     AND DomainID = @DomainID
                                     AND LeaveTypeID = @LeaveType)
                  INSERT INTO tbl_EmployeeAvailedLeave
                              (EmployeeID,
                               LeaveTypeID,
                               TotalLeave,
                               AvailedLeave,
                               Year,
                               DomainID,
                               CreatedBy,
                               ModifiedBy)
                  VALUES      (@EmployeeID,
                               @LeaveType,
                               -- Isnull(@SL, 0),
                               ( CASE
                                   WHEN @SL = ''
                                         OR @SL IS NULL THEN
                                     0
                                   ELSE
                                     Cast(@SL AS DECIMAL(16, 1))
                                 END ),
                               0,
                               @YearName,
                               @DomainID,
                               @SessionID,
                               @SessionID)
                ELSE
                  UPDATE tbl_EmployeeAvailedLeave
                  SET    TotalLeave = ( CASE
                                          WHEN @SL = ''
                                                OR @SL IS NULL THEN
                                            0
                                          ELSE
                                            Cast(@SL AS DECIMAL(16, 1))
                                        END ),
                         ModifiedBy = @SessionID
                  WHERE  LeaveTypeID = @LeaveType
                         AND Year = @YearName
                         AND EmployeeID = @EmployeeID
                         AND Isnull(@SL, '') <> ''

                SET @Output = 'Uploaded Successfully.'
            END

          COMMIT TRANSACTION

          SELECT @Output AS [Output]
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
