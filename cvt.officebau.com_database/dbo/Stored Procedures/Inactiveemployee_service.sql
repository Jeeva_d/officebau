﻿
/****************************************************************************   
CREATED BY   :  Naneeshwar.M
CREATED DATE  :   20-SEP-2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Inactiveemployee_service]
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          UPDATE tbl_EmployeeMaster
          SET    IsActive = 1
          WHERE  ( InactiveFrom IS NOT NULL
                    OR Isnull(InactiveFrom, '') <> '' )
                 AND IsDeleted = 0
                 AND CONVERT(DATE, InactiveFrom) <= CONVERT(DATE, Getdate())

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
