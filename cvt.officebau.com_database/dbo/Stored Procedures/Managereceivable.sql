﻿/****************************************************************************                 
CREATED BY    :                 
CREATED DATE  :                 
MODIFIED BY   :     
MODIFIED DATE :   
 <summary>                        
                 
 </summary>                                         
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Managereceivable] (@ID                INT,    
                                          @Date              DATETIME,    
                                          @Description       VARCHAR(1000),    
                                          @CustomerID        INT,    
                                          @TotalAmount       MONEY,    
                                          @SessionID         INT,    
                                          @DomainID          INT,    
                                          @PaymentModeID     INT,    
                                          @BankID            INT,    
                                          @Reference         VARCHAR(1000),    
                                          @LedgerID          INT,    
                                          @IsDeleted         BIT,    
                                          @InvoiceReceivable INVOICERECEIVABLE readonly)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION Receivable    
    
          DECLARE @OUTPUT                 VARCHAR(100),    
                  @InvoicePaymentID       INT,    
                  @Cash                   MONEY,    
                  @InvoicePayHistoryID    VARCHAR(MAX),    
                  @InvoicePayMapHistoryID VARCHAR(MAX),    
                  @InvoicePayColumn       VARCHAR(MAX) = ( CASE    
                        WHEN @IsDeleted = 1 THEN 'IsDeleted'    
                        ELSE 'PaymentModeID,BankID'    
                      END ),    
                  @InvoicePayMapColumn    VARCHAR(MAX) = ( CASE    
                        WHEN @IsDeleted = 1 THEN 'IsDeleted'    
                        ELSE 'Amount'    
                      END ),    
                  @PreviousCash           MONEY = Isnull((SELECT Sum(IM.Amount)    
                            FROM   tblInvoicePayment IP    
                                   LEFT JOIN tblInvoicePaymentMapping IM    
                                          ON IP.id = IM.InvoicePaymentID    
                            WHERE  IP.isdeleted = 0    
                                   AND IP.ID = @id    
                                   AND IP.DomainID = @DomainID), 0),    
                  @PreviousMode           INT = (SELECT TOP 1 PaymentModeID    
                     FROM   tblInvoicePayment IP    
                            LEFT JOIN tblInvoicePaymentMapping IM    
                                   ON IP.id = IM.InvoicePaymentID    
                     WHERE  IP.isdeleted = 0    
                            AND IP.ID = @id    
                            AND IP.DomainID = @DomainID)    
          DECLARE @PayHistoryIDTable TABLE    
            (    
               ID        INT IDENTITY(1, 1),    
               HistoryID VARCHAR(MAX)    
            )    
          DECLARE @PayMapHistoryIDTable TABLE    
            (    
               ID        INT IDENTITY(1, 1),    
               HistoryID VARCHAR(MAX)    
            )    
    
          IF( (SELECT Count(1)    
               FROM   tblCashBucket    
               WHERE  DomainID = @DomainID) = 0 )    
            INSERT INTO tblCashBucket    
                        (AvailableCash,    
                         DomainID)    
            VALUES      (0,    
                         @DomainID)    
    
          IF( @IsDeleted = 1 )    
            BEGIN    
                IF EXISTS (SELECT 1    
                           FROM   tblbrs    
                           WHERE  SourceID = @ID    
                                  AND SourceType = (SELECT ID    
               FROM   tblMasterTypes    
                                                    WHERE  NAME = 'InvoiceReceivable')    
                                  AND IsReconsiled = 1    
                                  AND IsDeleted = 0    
                                  AND DomainID = @DomainID)    
                  BEGIN    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Information'    
                                            AND Code = 'RCD_RECON'    
                                            AND IsDeleted = 0) --'The record has been Reconciled.'        
                      GOTO finish    
                  END    
                ELSE    
                  BEGIN    
                      IF( @PaymentModeID = (SELECT ID    
                                            FROM   tbl_CodeMaster    
                                            WHERE  Code = 'Cash'    
                                                   AND IsDeleted = 0) )    
                        BEGIN    
                            UPDATE tblCashBucket    
                            SET    AvailableCash = AvailableCash - Isnull((SELECT Sum(Amount)    
                                                                           FROM   tblinvoicepaymentmapping    
                                                                           WHERE  InvoicePaymentID = @ID    
                                                                                  AND IsDeleted = 0    
                                                                                  AND DomainID = @DomainID), 0),    
                                   ModifiedBy = @SessionID,    
                                   ModifiedOn = Getdate()    
                            WHERE  @DomainID = DomainID    
    
                            UPDATE tblVirtualCash    
                            SET    IsDeleted = 1,    
                                   ModifiedBy = @SessionID,    
                                   ModifiedOn = Getdate()    
                            WHERE  SourceID = @ID    
                                   AND SourceType = (SELECT ID    
                                                     FROM   tblMasterTypes    
                                                     WHERE  NAME = 'InvoiceReceivable')    
                                   AND IsDeleted = 0    
                                   AND DomainID = @DomainID    
                        END    
    
                      INSERT INTO @PayMapHistoryIDTable    
                                  (HistoryID)    
                      SELECT HistoryID    
                      FROM   tblInvoicePaymentMapping    
                      WHERE  InvoicePaymentID = @ID    
                             AND DomainID = @DomainID    
                             AND IsDeleted = 0    
    
                      INSERT INTO @PayHistoryIDTable    
                                  (HistoryID)    
                      SELECT HistoryID    
                      FROM   tblInvoicePayment    
                      WHERE  ID = @ID    
                             AND DomainID = @DomainID    
                             AND IsDeleted = 0    
    
                      UPDATE tblinvoicepaymentmapping    
                      SET    IsDeleted = 1,    
                             ModifiedBy = @SessionID,    
                             ModifiedOn = Getdate()    
                      WHERE  @ID = InvoicePaymentID    
    
                      UPDATE tblInvoicePayment    
                      SET    IsDeleted = 1,    
                             ModifiedBy = @SessionID,    
                             ModifiedOn = Getdate()    
                      WHERE  ID = @ID    
    
                      UPDATE tblBookedBankBalance    
                      SET    IsDeleted = 1,    
                             ModifiedBy = @SessionID,    
                             ModifiedOn = Getdate()    
                      WHERE  BRSID = (SELECT ID    
                                      FROM   tblBRS    
                                      WHERE  SourceID = @ID    
                                             AND SourceType = (SELECT ID    
                                                    FROM   tblMasterTypes    
                                                               WHERE  NAME = 'InvoiceReceivable')    
                                             AND IsDeleted = 0    
                                             AND DomainID = @DomainID)    
                             AND DomainID = @DomainID    
    
                      UPDATE tblBRS    
                      SET    IsDeleted = 1,    
                             ModifiedBy = @SessionID,    
                             ModifiedOn = Getdate()    
                      WHERE  SourceID = @ID    
                             AND SourceType = (SELECT ID    
                                               FROM   tblMasterTypes    
                                               WHERE  NAME = 'InvoiceReceivable')    
    
                      EXEC Managesystembankbalance    
                        @DomainID    
                  END    
    
                SET @Output = (SELECT [Message]    
                               FROM   tblErrorMessage    
                               WHERE  [Type] = 'Information'    
                                      AND Code = 'RCD_DEL'    
                                      AND IsDeleted = 0) --'Deleted Successfully'        
                GOTO finish    
            END    
          ELSE    
            BEGIN    
                IF( @ID = 0 )    
                  BEGIN    
                      INSERT INTO tblInvoicePayment    
                                  (PaymentModeID,    
                                   PaymentDate,    
                                   BankID,    
                                   Reference,    
                                   paymentDescription,    
                                   LedgerID,    
                                   DomainID,    
                                   CreatedBy,    
                                   ModifiedBy,    
                                   ModifiedOn)    
                      OUTPUT      INSERTED.HistoryID    
                      INTO @PayHistoryIDTable(HistoryID)    
                      VALUES      (@PaymentModeID,    
                                   @Date,    
                                   @BankID,    
                                   @Reference,    
                                   @description,    
                                   @LedgerID,    
                                   @DomainID,    
                                   @SessionID,    
                                   @SessionID,    
                                   GETDATE())    
    
                      SET @InvoicePaymentID =Ident_current('tblInvoicePayment')    
    
                      INSERT INTO tblInvoicePaymentMapping    
                                  (InvoicePaymentID,    
                                   InvoiceID,    
                                   Amount,    
                                   DomainID,    
                                   CreatedBy,    
                                   ModifiedBy,    
                                   ModifiedOn)    
                      OUTPUT      INSERTED.HistoryID    
                      INTO @PayMapHistoryIDTable(HistoryID)    
                      SELECT @InvoicePaymentID,    
                             InvoiceID,    
                             ReceivedAmount,    
                             @DomainID,    
                             @SessionID,    
                             @SessionID,    
                             GETDATE()    
                      FROM   @InvoiceReceivable    
                      WHERE  ReceivedAmount <> 0    
    
                      IF( Isnull(@BankID, 0) <> 0 )    
                        BEGIN   
                            INSERT INTO tblBRS    
                                        (BankID,    
                                         SourceID,    
                                         SourceDate,    
                                         SourceType,    
                                         Amount,    
                                         DomainID,    
                                         BRSDescription,    
                                      CreatedBy,    
                                         ModifiedBy)    
                            VALUES      (@BankID,    
                                         @InvoicePaymentID,    
                                         @Date,    
                                         (SELECT ID    
                                          FROM   tblMasterTypes    
                                          WHERE  NAME = 'InvoiceReceivable'),    
                                         (SELECT Sum(Amount)    
                                          FROM   tblInvoicePaymentMapping    
                                          WHERE  IsDeleted = 0    
                                                 AND InvoicePaymentID = @InvoicePaymentID    
                                                 AND DomainID = @DomainID),    
                                         @DomainID,    
                                         @Description,    
                                         @SessionID,    
                                         @SessionID)    
    
                            INSERT INTO tblBookedBankBalance    
                                        (BRSID,    
                                         Amount,    
                                         DomainID,    
                                         CreatedBy,    
                                         ModifiedBy)    
                            VALUES      (@@IDENTITY,    
                                         @TotalAmount,    
                                         @DomainID,    
                                         @SessionID,    
                                         @SessionID)    
    
                            EXEC Managesystembankbalance    
                              @DomainID    
                        END    
    
                      IF( @PaymentModeID = (SELECT ID    
                                            FROM   tbl_CodeMaster    
                                            WHERE  Code = 'Cash'    
                                                   AND IsDeleted = 0) )    
                        BEGIN    
                            UPDATE tblCashBucket    
                            SET    AvailableCash = AvailableCash + @TotalAmount,    
                                   ModifiedBy = @SessionID,    
                                   ModifiedOn = Getdate()    
                            WHERE  @DomainID = DomainID    
    
                            INSERT INTO tblVirtualCash    
                                        (SourceID,    
                                         Amount,    
                                         Date,    
                                         SourceType,    
                                         DomainID,    
                                         CreatedBy,    
                                         CreatedOn)    
                            VALUES      (@InvoicePaymentID,    
                                         @TotalAmount,    
                                         @Date,    
                                         (SELECT ID    
                                          FROM   tblMasterTypes    
                                          WHERE  NAME = 'InvoiceReceivable'),    
                                         @DomainID,    
                                         @SessionID,    
                                         Getdate())    
                        END    
    
                      --IF( (SELECT Count(1)        
                      --     FROM @InvoiceReceivable) <> 0 )        
                      --  BEGIN        
                      --      UPDATE tblInvoice        
                      --      SET    tblInvoice.ReceivedAmount = (SELECT Sum(Amount)        
                      --                                          FROM   tblInvoicePaymentMapping        
                      --                                          WHERE  IsDeleted = 0        
                      --                                                 AND InvoiceID = S.InvoiceID        
                      --                     AND DomainID = @DomainID),        
                      --             tblInvoice.StatusID = CASE        
                      --                                     WHEN ( tblInvoice.ReceivedAmount >= ( CASE        
                      --                                                                             WHEN ( tblInvoice.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate) - ( Isnull(( Sum(Qty * Rate) * tblInvoice.DiscountPercentage ), 0) ), 0
  
    
                      --)    
                      --)      
                      --                                                                                                                               FROM   tblInvoiceItem        
                      --                                                                                                                               WHERE  InvoiceID = tblInvoice.ID        
                      --                                                                                                                                      AND IsDeleted = 0        
                      --                                                                                                                                      AND DomainID = @DomainID)        
                      --                                                                           END ) ) THEN (SELECT ID        
                      --                                                                                         FROM   tbl_CodeMaster        
                      --                                                                                         WHERE  [type] = 'Close')        
                      --                                     WHEN ( Isnull(tblInvoice.ReceivedAmount, 0) <> (SELECT ( Isnull(Sum(Qty * Rate), 0) )        
                      --                                                                                     FROM   tblInvoiceItem        
                      --                                                                                     WHERE  InvoiceID = S.InvoiceID        
                      --                                                                                            AND DomainID = @DomainID        
                      --                                                                                            AND IsDeleted = 0) )THEN(SELECT ID        
                      --                                                                                                                     FROM   tbl_CodeMaster        
                      --                                                                                                                     WHERE  [type] = 'Partial')        
                      --                                     WHEN ( Isnull(tblInvoice.ReceivedAmount, 0) = 0 )THEN (SELECT ID        
                      --                                                                                            FROM   tbl_CodeMaster        
                      --                                                                                            WHERE  [type] = 'Open')        
                      --                                   END        
                      --      FROM   tblInvoice        
                      --             LEFT JOIN @InvoiceReceivable AS S        
          --                    ON tblInvoice.ID = S.InvoiceID        
                      --      WHERE  s.ReceivedAmount <> 0        
                      --  END        
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Information'    
                                            AND Code = 'RCD_INS'    
                                            AND IsDeleted = 0) --'Inserted Successfully.'        
                      GOTO Finish    
                  END    
                ELSE    
    BEGIN    
                      IF EXISTS (SELECT 1    
                                 FROM   tblbrs    
                                 WHERE  SourceID = @ID    
                                        AND SourceType = (SELECT ID    
                                                          FROM   tblMasterTypes    
                                                          WHERE  NAME = 'InvoiceReceivable')    
                                        AND IsReconsiled = 1    
                                        AND IsDeleted = 0    
                                        AND DomainID = @DomainID)    
                        BEGIN    
                            SET @Output = (SELECT [Message]    
                                           FROM   tblErrorMessage    
                                           WHERE  [Type] = 'Information'    
                                                  AND Code = 'RCD_RECON'    
                                                  AND IsDeleted = 0) --'The record has been Reconciled.'        
    
                            UPDATE tblInvoicePayment    
                            SET    PaymentDescription = @Description,    
                                   ModifiedBy = @SessionID,    
                                   ModifiedOn = Getdate()    
                            WHERE  ID = @ID    
                                   AND DomainID = @DomainID    
    
                            UPDATE tblBRS    
                            SET    BRSDescription = @Description,    
                                   ModifiedBy = @SessionID    
                            WHERE  SourceID = @ID    
                                   AND SourceType = (SELECT ID    
                                                     FROM   tblMasterTypes    
                                                     WHERE  NAME = 'InvoiceReceivable')    
                                   AND IsDeleted = 0    
    
                            GOTO finish    
                        END    
                      ELSE    
                        BEGIN    
                            UPDATE tblInvoicePayment    
                            SET    PaymentModeID = @PaymentModeID,    
                                   PaymentDate = @Date,    
                                   BankID = @BankID,    
                                   Reference = @Reference,    
                                   DomainID = @DomainID,    
                                   ModifiedBy = @SessionID,    
                                   ModifiedOn = Getdate()    
                            WHERE  ID = @ID    
                                   AND DomainID = @DomainID    
    
                            UPDATE tblInvoicePaymentMapping    
                            SET    tblInvoicePaymentMapping.Amount = B.ReceivedAmount,    
                                   tblInvoicePaymentMapping.ModifiedBy = @SessionID,    
                                   tblInvoicePaymentMapping.ModifiedOn = GETDATE()    
                            FROM   tblInvoicePaymentMapping    
                                   INNER JOIN @InvoiceReceivable AS B    
                                           ON tblInvoicePaymentMapping.ID = B.ID    
    
                            INSERT INTO @PayMapHistoryIDTable    
                                        (HistoryID)    
                            SELECT HistoryID    
                            FROM   tblInvoicePaymentMapping    
                                   INNER JOIN @InvoiceReceivable AS B    
                                           ON tblInvoicePaymentMapping.ID = B.ID    
    
                            INSERT INTO tblInvoicePaymentMapping    
                                        (InvoicePaymentID,    
                                         InvoiceID,    
                                         Amount,    
                                         DomainID,    
                                         CreatedBy,    
                                         ModifiedBy,    
                                         ModifiedOn)    
                            OUTPUT      INSERTED.HistoryID    
                   INTO @PayMapHistoryIDTable(HistoryID)    
                            SELECT @ID,    
                                   InvoiceID,    
                                   ReceivedAmount,    
                                   @DomainID,    
                                   @SessionID,    
                                   @SessionID,    
                                   GETDATE()    
                            FROM   @InvoiceReceivable    
                            WHERE  ReceivedAmount <> 0    
                                   AND ID = 0    
    
                            IF( Isnull(@BankID, 0) <> 0 )    
                              BEGIN    
                                  IF EXISTS(SELECT 1    
                                            FROM   tblBRS    
                                            WHERE  SourceID = @ID    
                                                   AND SourceType = (SELECT ID    
                                                                     FROM   tblMasterTypes    
                                                                     WHERE  NAME = 'InvoiceReceivable')    
                                                   AND DomainID = @DomainID    
                                                   AND IsDeleted = 0    
                                                   AND DomainID = @DomainID)    
                                    BEGIN    
                                        UPDATE tblBRS    
                                        SET    BankID = @BankID,    
                                               BRSDescription = @Description,    
                                               SourceDate = @Date,    
                                               Amount = (SELECT Sum(Amount)    
                                                         FROM   tblInvoicePaymentMapping    
                                                         WHERE  IsDeleted = 0    
                                                                AND InvoicePaymentID = @ID    
                                                                AND DomainID = @DomainID),    
                                               DomainID = @DomainID,    
                                               ModifiedBy = @SessionID    
                                        WHERE  SourceID = @ID    
                                               AND SourceType = (SELECT ID    
                                                                 FROM   tblMasterTypes    
                                                                 WHERE  NAME = 'InvoiceReceivable')    
                                               AND IsDeleted = 0    
    
                                        UPDATE tblBookedBankBalance    
                                        SET    Amount = @TotalAmount,    
                                               ModifiedBy = @SessionID,    
                                               ModifiedOn = Getdate()    
                                        WHERE  BRSID = (SELECT ID    
                                                        FROM   tblBRS    
                                                        WHERE  SourceID = @ID    
             AND SourceType = (SELECT ID    
                                                                                 FROM   tblMasterTypes    
                                                                                 WHERE  NAME = 'InvoiceReceivable')    
                                                               AND IsDeleted = 0    
                                                               AND DomainID = @DomainID)    
                                               AND DomainID = @DomainID    
    
                                        EXEC Managesystembankbalance    
                                          @DomainID    
                                    END    
                                  ELSE    
                                    BEGIN    
                                        INSERT INTO tblBRS    
                        (BankID,    
                                                     SourceID,    
                                                     SourceDate,    
                                                     SourceType,    
                                                     Amount,    
                                                     BRSDescription)    
                                        VALUES      (@BankID,    
                                                     @ID,    
                                                     @Date,    
                                                     (SELECT ID    
                                                      FROM   tblMasterTypes    
                                                      WHERE  NAME = 'InvoiceReceivable'),    
                                                     @TotalAmount,    
                                                     @Description)    
    
                                        INSERT INTO tblBookedBankBalance    
                                                    (BRSID,    
                                                     Amount,    
                                                     DomainID,    
                                                     CreatedBy,    
                                                     ModifiedBy)    
                                        VALUES      (@@IDENTITY,    
                                                     @TotalAmount,    
                                                     @DomainID,    
                                                     @SessionID,    
                                                     @SessionID)    
    
                                        EXEC Managesystembankbalance    
                                          @DomainID    
    
                                        UPDATE tblCashBucket    
                                        SET    AvailableCash = ( AvailableCash - @TotalAmount ),    
                                               ModifiedBy = @SessionID,    
                                               ModifiedOn = Getdate()    
                                        WHERE  @DomainID = DomainID    
    
                                        UPDATE tblVirtualCash    
                                        SET    IsDeleted = 1,    
                                               ModifiedBy = @SessionID,    
                                               ModifiedOn = Getdate()    
                                        WHERE  SourceID = @ID    
                                               AND SourceType = (SELECT ID    
                                                                 FROM   tblMasterTypes    
                                                                 WHERE  NAME = 'InvoiceReceivable')    
                                               AND IsDeleted = 0    
                                               AND DomainID = @DomainID    
                                    END    
                              END    
                            ELSE    
                              BEGIN    
  IF( (SELECT 1    
                                       FROM   tblBRS    
                                       WHERE  SourceID = @ID    
                                              AND SourceType = (SELECT ID    
                                                                FROM   tblMasterTypes    
                                                                WHERE  NAME = 'InvoiceReceivable')    
                                              AND DomainID = @DomainID    
                                              AND IsDeleted = 0) = 1 )    
                                    UPDATE tblBookedBankBalance    
                                    SET    IsDeleted = 1    
                                    WHERE  BRSID = (SELECT ID    
                                                    FROM   tblBRS    
                                                    WHERE  SourceID = @ID    
                                                           AND SourceType = (SELECT ID    
                                                                             FROM   tblMasterTypes    
                                                                             WHERE  NAME = 'InvoiceReceivable')    
                                                           AND IsDeleted = 0    
                                                           AND DomainID = @DomainID)    
                                           AND DomainID = @DomainID    
    
                                  UPDATE tblBRS    
                                  SET    IsDeleted = 1,    
                                         ModifiedBy = @SessionID,    
                                         ModifiedOn = Getdate()    
                                  WHERE  SourceID = @ID    
                                         AND SourceType = (SELECT ID    
                                                           FROM   tblMasterTypes    
                                                           WHERE  NAME = 'InvoiceReceivable')    
                                         AND DomainID = @DomainID    
                                         AND IsDeleted = 0    
    
                                  EXEC Managesystembankbalance    
                                    @DomainID    
                              END    
    
                            IF( @PaymentModeID = (SELECT ID    
                                                  FROM   tbl_CodeMaster    
                                                  WHERE  Code = 'Cash'    
                                                         AND IsDeleted = 0) )    
                              BEGIN    
                                  IF ( @PreviousMode = (SELECT ID    
                                                        FROM   tbl_CodeMaster    
                                                        WHERE  Code = 'Cash'    
                                                               AND IsDeleted = 0) )    
                                    BEGIN    
                                        UPDATE tblCashBucket    
                                        SET    AvailableCash = AvailableCash - @PreviousCash + @TotalAmount,    
                                               ModifiedBy = @SessionID,    
                                               ModifiedOn = Getdate()    
                                        WHERE  @DomainID = DomainID    
    
                                        UPDATE tblVirtualCash    
                                        SET    Amount = @TotalAmount,    
                                               ModifiedBy = @SessionID,    
                                               ModifiedOn = Getdate()    
                                        WHERE  SourceID = @ID    
                                               AND SourceType = (SELECT ID    
                                                                 FROM   tblMasterTypes    
                                                                 WHERE  NAME = 'InvoiceReceivable')    
                                               AND IsDeleted = 0    
                                               AND DomainID = @DomainID    
                                    END    
                                  ELSE    
                                    BEGIN    
                                        UPDATE tblCashBucket    
                                        SET    AvailableCash = AvailableCash + @TotalAmount,    
                                               ModifiedBy = @SessionID,    
                                               ModifiedOn = Getdate()    
                                        WHERE  @DomainID = DomainID    
    
                                        INSERT INTO tblVirtualCash    
                                                    (SourceID,    
                                                     Amount,    
                                                     Date,    
                                                     SourceType,    
                                                     DomainID,    
                 CreatedBy,    
                                                     ModifiedBy)    
                                        VALUES      (@ID,    
                                                     @TotalAmount,    
                                                     @Date,    
                                                     (SELECT ID    
                                                      FROM   tblMasterTypes    
                                                      WHERE  NAME = 'InvoiceReceivable'),    
                                                     @DomainID,    
                                                     @SessionID,    
                                                     @SessionID)    
                                    END    
                              END    
    
                            SET @Output = (SELECT [Message]    
                                           FROM   tblErrorMessage    
                                           WHERE  [Type] = 'Information'    
                                                  AND Code = 'RCD_UPD'    
                                                  AND IsDeleted = 0) --'Updated Successfully.'        
                            GOTO Finish    
                        END    
                  END    
            END    
    
          FINISH:    
    
          DECLARE @TempCount INT = 1,    
                  @Count     INT = (SELECT Count(1)    
                     FROM   @PayMapHistoryIDTable)    
    
          WHILE @Count >= @TempCount    
            BEGIN    
                SET @InvoicePayMapHistoryID= (SELECT HistoryID    
                                              FROM   @PayMapHistoryIDTable    
                                              WHERE  ID = @TempCount)    
    
                EXEC Managebusinesseventslog    
                  @InvoicePayMapHistoryID,    
                  @InvoicePayMapColumn,    
                  'tblInvoicePaymentMapping'    
    
                SET @TempCount = @TempCount + 1    
            END    
    
          SET @TempCount = 1    
          SET @Count = (SELECT Count(1)    
                        FROM   @PayHistoryIDTable)    
    
          WHILE @Count >= @TempCount    
            BEGIN    
                SET @InvoicePayHistoryID= (SELECT HistoryID    
                                           FROM   @PayHistoryIDTable    
                                           WHERE  ID = @TempCount)    
    
                EXEC Managebusinesseventslog    
                  @InvoicePayHistoryID,    
                  @InvoicePayColumn,    
                  'tblInvoicePayment'    
    
                SET @TempCount = @TempCount + 1    
            END    
    
          UPDATE tblInvoice    
          SET    tblInvoice.ReceivedAmount = Isnull((SELECT ISNULL(Sum(Amount),0)    
                                                     FROM   tblInvoicePaymentMapping    
                  WHERE  IsDeleted = 0    
                                                            AND InvoiceID = S.InvoiceID    
                                                            AND DomainID = @DomainID), 0),    
                 tblInvoice.StatusID = CASE    
                                         WHEN( Isnull((SELECT ISNULL(Sum(Amount),0)   
                                                       FROM   tblInvoicePaymentMapping    
                                                       WHERE  IsDeleted = 0    
                                                              AND InvoiceID = S.InvoiceID    
                                                              AND DomainID = @DomainID) + (Select ISNULL(Sum(Amount),0) from tblInvoiceHoldings where IsDeleted = 0    
                                                              AND InvoiceID = S.InvoiceID), 0) >= ( CASE    
                                                                                                  WHEN ( tblInvoice.DiscountPercentage <> 0 ) THEN   
                          (SELECT ( Isnull(Sum(Qty * Rate)    
                                                     + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) -   
              ( Isnull(( Sum(Qty * Rate) * tblInvoice.DiscountPercentage ), 0) ), 0) )    
                                                                                                                        FROM   tblInvoiceItem   
                                 
                                                                                                                                                    WHERE  InvoiceID = S.InvoiceID    
                                                                                                                                                           AND IsDeleted = 0    
                                                                                                                                                           AND DomainID = @DomainID)    
                                                                                                  ELSE (SELECT ( Isnull(( Sum(Qty * Rate)    
                                                                                                                          + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(tblInvoice.DiscountValue, 0) ), 0) )    
                                                                                                        FROM   tblInvoiceItem    
                                                                                                        WHERE  InvoiceID = S.InvoiceID    
                                                                                                               AND IsDeleted = 0    
                                                                                                               AND DomainID = @DomainID)    
                                                                                                END ) ) THEN (SELECT ID    
                                                                                                              FROM   tbl_CodeMaster    
                                                                                                              WHERE  [type] = 'Close'    
                                                                                                                     AND IsDeleted = 0)    
                                         WHEN( Isnull((SELECT ISNULL(Sum(Amount),0)   
                                                       FROM   tblInvoicePaymentMapping    
                                                       WHERE  IsDeleted = 0    
                                                              AND InvoiceID = S.InvoiceID    
                                                              AND DomainID = @DomainID)+ (Select ISNULL(Sum(Amount),0) from tblInvoiceHoldings where IsDeleted = 0    
                                    AND InvoiceID = S.InvoiceID), 0) = 0 ) THEN (SELECT ID    
                                                                                                        FROM   tbl_CodeMaster    
                                                                                                        WHERE  [type] = 'Open'    
                                                                                                               AND IsDeleted = 0)    
                                         WHEN( Isnull((SELECT ISNULL(Sum(Amount),0)  
                                                       FROM   tblInvoicePaymentMapping    
                                                       WHERE  IsDeleted = 0    
                                                              AND InvoiceID = S.InvoiceID    
                                                              AND DomainID = @DomainID)+ (Select ISNULL(Sum(Amount),0) from tblInvoiceHoldings where IsDeleted = 0    
                                                              AND InvoiceID = S.InvoiceID), 0) <> ( CASE    
                                                                                                   WHEN ( Isnull(tblInvoice.DiscountPercentage, 0) <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)    
                                                                                                                                                                                + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum (Qty * Rate
  
) * tblInvoice.DiscountPercentage ), 0) ), 0) )    
                  FROM   tblInvoiceItem    
                                                                                                                                                                WHERE  InvoiceID = S.InvoiceID    
                                                                                                                                                                       AND DomainID = @DomainID    
                                                                                                                                                                       AND IsDeleted = 0)    
                                                                                                   ELSE (SELECT ( Isnull(( Sum(Qty * Rate)    
                                                                                                                           + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(tblInvoice.DiscountValue, 0) ), 0) )    
                                                                                                         FROM   tblInvoiceItem    
                                                                                                         WHERE  InvoiceID = S.InvoiceID    
                                                                                                                AND DomainID = @DomainID    
                                                                                                                AND IsDeleted = 0)    
                                                                                                 END ) )THEN (SELECT ID    
                                                                                                              FROM   tbl_CodeMaster    
                                                                                                              WHERE  [type] = 'Partial'    
                                                                                                                     AND IsDeleted = 0)    
                                       END    
          FROM   tblInvoice    
                 JOIN @InvoiceReceivable AS S    
                   ON tblInvoice.ID = S.InvoiceID    
          WHERE  Isnull(s.ReceivedAmount, 0) <> 0    
    
          SELECT @Output    
    
          COMMIT TRANSACTION Receivable    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION Receivable    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
