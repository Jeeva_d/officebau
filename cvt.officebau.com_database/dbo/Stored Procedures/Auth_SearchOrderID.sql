﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	30-12-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	Get Order Coder
	[Auth_SearchOrderID]
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_SearchOrderID]
 AS 
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY				
	DECLARE
		@OrderID	INT	,
		@TrnID		INT
		SELECT @OrderID = ISNULL(MAX(ID),0)+1 FROM Auth_Orders 
		SELECT @TrnID = ISNULL(MAX(ID),0)+1 FROM Auth_OrderTransaction

		SELECT   @OrderID	AS OrderCode ,
				 @TrnID		AS Trancode 
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
