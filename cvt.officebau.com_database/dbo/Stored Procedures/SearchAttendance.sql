﻿/****************************************************************************         
CREATED BY  :   SIVA      
CREATED DATE :   06-Jun-2017      
MODIFIED BY  :   Ajith N  
MODIFIED DATE :   17 Jan 2018      
 <summary>      
  [SearchAttendance] '2018-jan-02',1      
 </summary>                                 
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchAttendance] (@CurrentDate DATE,  
                                          @DomainID    INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          SELECT DISTINCT Isnull(ATTD.ID, 0)                             AS AttendanceID,  
                          EMP.FirstName + ' ' + Isnull(emp.LastName, '') AS EmployeeName,  
                          ISNULL(EMP.EmpCodePattern, '') + '' + EMP.Code                                       AS Code,  
                          EMP.ID                                         AS EmployeeID,  
                          EMP.biometriccode,  
                          CASE  
                            WHEN Isnull(ATTD.ID, 0) <> 0 THEN  
                              ATTD.LogDate  
                            ELSE  
                              @CurrentDate  
                          END                                            AS AttendanceDate,  
                          Substring((SELECT ',' + Cast(PunchTime AS VARCHAR(5))  
                                     FROM   tbl_BiometricLogs BL  
                                     WHERE  Bl.AttendanceId = ATTD.ID  
          order by PunchTime  
                                     FOR xml path('')), 2, 20000)        AS Punches,  
                          ( CASE  
                              WHEN CONVERT(VARCHAR(5), ATTD.Duration, 108) <> '00:00' THEN  
                                CONVERT(VARCHAR(5), ATTD.Duration, 108)  
                              ELSE  
                                ''  
                            END )                                        AS PunchTime,  
                          --Cast(ISNULL(ATTD.IsManual, 0) AS VARCHAR)      AS IsManual  
                          '0'                                            AS IsManual  
          --CONVERT(VARCHAR(5), ATTD.Duration, 108) AS PunchTime       
          FROM   tbl_EmployeeMaster EMP  
                 LEFT JOIN tbl_Attendance ATTD  
                        ON EMP.ID = ATTD.EmployeeID  
                           AND ATTD.LogDate = @CurrentDate  
          WHERE  EMP.DomainID = @DomainID  
                 AND EMP.IsDeleted = 0  
                 AND ( Isnull(EMP.IsActive, 0) = 0  
                        OR ( Isnull(EMP.IsActive, 0) = 1  
                             AND Cast(EMP.InactiveFrom AS DATE) >= ( @CurrentDate ) ) )  
                 AND Cast(EMP.DOJ AS DATE) <= @CurrentDate  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
