﻿/**************************************************************************** 
CREATED BY			:	 Ajith N
CREATED DATE		:	 28 Nov 2017
MODIFIED BY			:	Ajith N
MODIFIED DATE		:	01 Dec 2017
 <summary>        
	 CheckClaimEligibleAmount 2, 1, 1, 224, 1,224
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CheckClaimEligibleAmount] (@ClaimTypeID   INT,
                                                  @DestinationID INT,
                                                  @Amount        MONEY,
                                                  @RequesterID   INT,
                                                  @DomainID      INT,
                                                  @UserID        INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @Output               VARCHAR(8000) = 'Operation Failed!',
                  @DesignationID        INT,
                  @BandID               INT,
                  @GradeID              INT,
                  @DesignationMappingID INT,
                  @EligibleAmount       MONEY = NULL,
                  @IsMetro              BIT = (SELECT IsMetro
                     FROM   tbl_DestinationCities
                     WHERE  ID = @DestinationID
                            AND IsDeleted = 0
                            AND DomainID = @DomainID)

          SELECT @DesignationID = DesignationID,
                 @BandID = BandID,
                 @GradeID = GradeID
          FROM   tbl_EmployeeMaster
          WHERE  ID = @RequesterID

          SELECT @EligibleAmount = ( CASE
                                       WHEN @IsMetro = 1 THEN
                                         CP.MetroAmount
                                       ELSE
                                         CP.NonMetroAmount
                                     END )
          FROM   tbl_claimpolicy CP
                 JOIN tbl_EmployeeDesignationMapping EDM
                   ON EDM.ID = CP.DesignationMappingID
          WHERE  CP.IsDeleted = 0
                 AND CP.ExpenseType = @ClaimTypeID
                 AND EDM.DesignationID = @DesignationID

          IF( @Amount > @EligibleAmount
               OR @EligibleAmount = 0 )
            BEGIN
                IF( @RequesterID = @UserID )
                  SET @Output = 'You are eligible to raise only Rs.'
                                + Cast(Cast(@EligibleAmount AS INT) AS VARCHAR)
                ELSE
                  SET @Output = 'Eligibility criteria is not met.'
            END
          ELSE IF ( Isnull(@EligibleAmount, '') = '' )
            BEGIN
                SET @Output = 'Claim eligibility is not configured for you, please contact IT support.'
            END
          ELSE
            SET @Output = 'Claim amount validation is Successfully.'

          SELECT @Output + '/'
                 + Cast(Isnull(@EligibleAmount, 0) AS VARCHAR) AS [Output]
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
