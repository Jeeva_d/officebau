﻿/****************************************************************************     
CREATED BY    : Ajith N  
CREATED DATE  : 22 May 2017  
MODIFIED BY   :    
MODIFIED DATE :    
 <summary>    
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteInvoice] (@ID        INT,
                                       @SessionID INT,
                                       @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT VARCHAR(100)

          IF NOT EXISTS (SELECT 1
                         FROM   tblInvoice
                         WHERE  StatusID = (SELECT ID
                                            FROM   tbl_CodeMaster
                                            WHERE  [Type] = 'Open'
                                                   AND IsDeleted = 0)
                                AND ID = @ID
                                AND IsDeleted = 0
                                AND DomainID = @DomainID)
            BEGIN
                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Warning'
                                      AND Code = 'RCD_REF'
                                      AND IsDeleted = 0) --'The record is referred.'  
                GOTO finish
            END
          ELSE
            BEGIN
                UPDATE tblInvoice
                SET    IsDeleted = 1
                WHERE  @ID = ID
                       AND DomainID = @DomainID

                UPDATE tblInvoiceItem
                SET    IsDeleted = 1
                WHERE  InvoiceID = @ID
                       AND DomainID = @DomainID

                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Information'
                                      AND Code = 'RCD_DEL'
                                      AND IsDeleted = 0) --'Deleted Successfully'  
               
			    ----Delete item from Inventory
                EXEC ManageInventory
                  @ID,
                  'DELETE',
                  'Sales',
                  @SessionID,
                  @DomainID

                GOTO finish
            END

          FINISH:

          EXEC [UpdateSOQTY]

          SELECT @OUTPUT

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
