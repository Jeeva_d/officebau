﻿/****************************************************************************         
CREATED BY   : Dhanalakshmi. S        
CREATED DATE  :         
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>         
   
 </summary>                                 
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageCarryForwardLeave] (@ID             INT,  
                                                  @EmployeeID     INT,  
                                                  @LeaveTypeID    INT,  
                                                  @TotalLeave     DECIMAL(16, 1),  
                                                  @Year           INT,  
                                                  @IsCarryForward BIT,  
                                                  @IsEncashment   BIT,  
                                                  @SessionID      INT,  
                                                  @DomainID       INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(1000)
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          IF( @ID = 0 )  
            BEGIN  
                INSERT INTO tbl_CarryForwardLeave  
                            (EmployeeID,  
                             LeaveTypeID,  
                             TotalLeave,  
                             [Year],  
                             IsCarryForward,  
                             IsEncashment,  
                             CreatedBy,  
                             ModifiedBy,  
                             DomainID)  
                VALUES      (@EmployeeID,  
                             @LeaveTypeID,  
                             @TotalLeave,  
                             @Year,  
                             @IsCarryForward,  
                             @IsEncashment,  
                             @SessionID,  
                             @SessionID,  
                             @DomainID)  
  
                SET @Output = (SELECT [Message]  
                               FROM   tblErrorMessage  
                               WHERE  [Type] = 'Information'  
                                      AND Code = 'RCD_INS'  
                                      AND IsDeleted = 0) --'Inserted Successfully'        
            END  
          ELSE  
            BEGIN  
                UPDATE tbl_CarryForwardLeave  
                SET    EmployeeID = @EmployeeID,  
                       LeaveTypeID = @LeaveTypeID,  
                       TotalLeave = @TotalLeave,  
                       [Year] = @Year,  
                       IsCarryForward = @IsCarryForward,  
                       IsEncashment = @IsEncashment,  
                       ModifiedBY = @SessionID,  
                       ModifiedOn = Getdate()  
                WHERE  ID = @ID  
                       AND DomainID = @DomainID  
  
                SET @Output = (SELECT [Message]  
                               FROM   tblErrorMessage  
                               WHERE  [Type] = 'Information'  
                                      AND Code = 'RCD_UPD'  
                                      AND IsDeleted = 0) --'Updated Successfully'        
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
