﻿/****************************************************************************         
CREATED BY   :    Priya K    
CREATED DATE  :   13 Nov 2018      
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>       
         [Pay_EmailForMonthlyPayroll] 3,4,1,'2',null     
               
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Pay_EmailForMonthlyPayroll] (@MonthID      INT,      
                                                     @Year         INT,      
                                                     @DomainID     INT,      
                                                     @Location     VARCHAR(50),      
                                                     @DepartmentID INT)      
AS  BEGIN                
          SET NOCOUNT ON;         
     BEGIN TRY            
          DECLARE        
            @cols AS NVARCHAR(MAX),        
                  @query  AS NVARCHAR(MAX)        
          SET @Year = (SELECT NAME      
                       FROM   tbl_FinancialYear      
                       WHERE  id = @Year      
                              AND IsDeleted = 0      
                              AND DomainId = @DomainID)      
      
                 
          DECLARE @DataSource TABLE                
               (                
                    id INT IDENTITY (1, 1) ,                
                    [value] NVARCHAR(128)                
               )                
                          
          INSERT INTO @DataSource                
               (                
                    [value]                
               )                
          SELECT                
               Item                
          FROM                
               dbo.SplitString(@Location, ',')                
          WHERE                
               ISNULL(Item, '') <> ''        
         
     Select * INTO #Result from VW_Payroll vwpay    
   where     
  vwpay.MonthId = @MonthID     
     and   vwpay.YearId = @Year    
      AND     
    ( (@DepartmentID  IS NULL        
                       OR Isnull(@DepartmentID, 0) = 0 )        
                      OR vwpay.DepID = @DepartmentID)        
           and ( (@Location  = CAST(0 AS VARCHAR))                  
                              OR vwpay.BuID IN                  
                              (SELECT         
                                   [value]                 
                              FROM                  
                                   @DataSource) )              
                         AND                  
                         (vwpay.BuID <> 0 OR vwpay.BuID <> '')      
       Set @cols = '[Net Pay],CTC,TAX,[Other Earnings],[Other Deductions]'     
       set @query = 'SELECT     
        Employee_Name,              
     ' + @cols + ',LopDays    
      from             (              
     select    
      Emp_Code, Description, Employee_Name, Joining_Date,                 
     Name, Amount  ,LopDays     
     from #Result    
         
     ) x            pivot            (sum(Amount)                 
     for Description in (' + @cols + ')            ) p '    
    
      exec (@query);              
               END TRY                
               BEGIN CATCH                
                    DECLARE @ErrorMsg VARCHAR(100) ,                
                         @ErrSeverity TINYINT                
                    SELECT                
                         @ErrorMsg    = ERROR_MESSAGE() ,                
                         @ErrSeverity = ERROR_SEVERITY()                
                    RAISERROR (@ErrorMsg, @ErrSeverity, 1)                
  END CATCH                
          END
