﻿

/****************************************************************************             
CREATED BY  :  Ajith N        
CREATED DATE :  25 Jan 2018  
MODIFIED BY  :            
MODIFIED DATE   :       
 <summary>                    
    GetEarlyDepartureReportList 0, 0, 1,2  
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetEarlyDepartureReportList] (@EmployeeID     INT,
                                                     @BusinessUnitID INT,
                                                     @MonthID        INT,
                                                     @Year           INT,
                                                     @DomainID       INT,
                                                     @UserID         INT,
                                                     @Status         VARCHAR(20),
                                                     @Minutes        INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @BusinessUnitTable TABLE
            (
               BusinessUnitID INT
            )

          INSERT INTO @BusinessUnitTable
          SELECT Splitdata
          FROM   FnSplitString((SELECT BusinessUnitID
                                FROM   tbl_EmployeeMaster
                                WHERE  ID = @UserID), ',')
          WHERE  Splitdata <> 0

          SELECT *,
                 ( CASE
                     WHEN Datediff(MINUTE, BusinessEndHour, CheckOut) > 0 THEN
                       Datediff(MINUTE, BusinessEndHour, CheckOut) --- 'Late'  
                     ELSE
                       Datediff(MINUTE, CheckOut, BusinessEndHour) --- 'Early'  
                   END ) AS [Minutes],
                 ( CASE
                     WHEN Datediff(MINUTE, BusinessEndHour, CheckOut) > 0 THEN
                       'Late'
                     ELSE
                       'Early'
                   END ) AS [Status]
          INTO   #tempPunchesList
          FROM   (SELECT Isnull(EM.EmpCodePattern, '') + EM.Code AS EmployeeCode,
                         EM.Id                                   AS EmployeeId,
                         EM.FUllname                             AS EmployeeName,
                         A.LogDate                               AS [Date],
                         BH.BusinessEndHour                      AS BusinessEndHour,
                         (SELECT TOP 1 ISNULL(PunchTime, '00:00')
                          FROM   tbl_BiometricLogs b
                          WHERE  b.IsDeleted = 0
                                 AND b.AttendanceId = a.ID
                                 AND b.Direction = 'In'
                          ORDER  BY PunchTime ASC)               AS CheckIn,
                         (SELECT TOP 1 ISNULL(PunchTime, '00:00')
                          FROM   tbl_BiometricLogs b
                          WHERE  b.IsDeleted = 0
                                 AND b.AttendanceId = a.ID
                                 AND b.Direction = 'Out'
                          ORDER  BY PunchTime DESC)              AS CheckOut,
                         BT.NAME                                 AS BusinessUnit,
                         REG.NAME                                AS Region,
                         Substring((SELECT ',' + Cast(PunchTime AS VARCHAR(5))
                                    FROM   tbl_BiometricLogs b
                                    WHERE  b.IsDeleted = 0
                                           AND b.AttendanceId = a.ID
                                    FOR xml path('')), 2, 20000) AS Punches,
                         ROW_NUMBER()
                           OVER(
                             ORDER BY (SELECT 1))                AS Rowno
                  --INTO   #tempPunchesList  
                  FROM   tbl_Attendance A
                         --join tbl_BiometricLogs BL ON A.ID = BL.AttendanceId  
                         LEFT JOIN tbl_EmployeeMaster EM
                                ON EM.ID = A.EmployeeID
                         LEFT JOIN tbl_BusinessUnit BT
                                ON EM.BaseLocationID = BT.ID
                         LEFT JOIN tbl_BusinessUnit REG
                                ON REG.ID = BT.ParentID
                         LEFT JOIN tbl_BusinessHours BH
                                ON BH.RegionID = EM.RegionID
                  WHERE  A.IsDeleted = 0
                         AND ( ( @BusinessUnitID = 0
                                 AND EM.BaseLocationID IN (SELECT BusinessUnitID
                                                           FROM   @BusinessUnitTable) )
                                OR EM.BaseLocationID = @BusinessUnitID )
                         AND a.DomainID = @DomainID
                         AND ( @EmployeeID = 0
                                OR a.EmployeeID = @EmployeeID )
                         AND ( ISNULL(@MonthID, 0) = 0
                                OR Month(a.LogDate) = @MonthID )
                         AND ( ISNULL(@Year, 0) = 0
                                OR Year(a.LogDate) = @Year )) AS X

          SELECT *,
                 ( CASE
                     WHEN [Minutes] >= 60 THEN
                       (SELECT Cast(([Minutes] / 60) AS VARCHAR(2))
                               + ' H '
                               + CASE
                                   WHEN ( [Minutes] % 60 ) > 0 THEN
                                     Cast(([Minutes] % 60) AS VARCHAR(2))
                                     + ' Min'
                                   ELSE
                                     ''
                                 END)
                     ELSE
                       Cast(([Minutes] % 60) AS VARCHAR(2))
                       + ' Min'
                   END ) AS [PunchTime]
          FROM   #tempPunchesList
          WHERE  CheckIn IS NOT NULL
                 AND CheckOut IS NOT NULL
                 AND ( ISNULL(@Status, '') = ''
                        OR [Status] = @Status )
                 AND ( @Minutes = 0
                        OR [Minutes] > @Minutes )
          ORDER  BY EmployeeName,
                    [Date] DESC,
                    BusinessUnit
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
