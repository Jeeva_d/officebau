﻿/****************************************************************************   
CREATED BY   : DhanaLakshmi.S  
CREATED DATE  : 02 FEB 2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
     [GetLeaveDetails] 5, '2018-04-11' , 4  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetLeaveDetails] (@EmployeeID INT,  
                                         @LeaveDate  DATE,  
                                         @DomainID   INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          SELECT EL.ID                             AS ID,  
                 EL.LeaveTypeID                    AS LeaveTypeID,  
                 LT.NAME                           AS LeaveType,  
                 EL.EmployeeID                     AS EmployeeID,  
                 EMP.Code + ' - ' + EMP.FullName   AS Requester,  
                 EL.FromDate                       AS FromDate,  
                 EL.ToDate                         AS ToDate,  
                 (SELECT Count(1)  
                  FROM   tbl_EmployeeLeaveDateMapping  
                  WHERE  LeaveRequestID = EL.ID  
                         AND IsDeleted = 0  
                         AND DomainID = @DomainID) AS Duration,  
                 EL.IsHalfDay                      AS IsHalfDay,  
                 EL.Reason                         AS Reason,  
                 EL.ApproverID                     AS ApproverID,  
                 EL.StatusID                       AS StatusID,  
                 ST.Code                           AS [Status]  
          FROM   tbl_EmployeeLeave EL  
                 JOIN tbl_EmployeeLeaveDateMapping EM  
                   ON EL.ID = EM.LeaveRequestID  
                 LEFT JOIN tbl_Status ST  
                        ON ST.ID = EL.StatusID  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = EL.EmployeeID  
                 LEFT JOIN tbl_LeaveTypes LT  
                        ON LT.ID = EL.LeaveTypeID  
          WHERE  EM.EmployeeID = @EmployeeID  
                 AND EM.IsDeleted = 0  
                 AND EM.DomainID = @DomainID  
                 AND EM.LeaveDate = @LeaveDate  
                 AND EL.IsDeleted = 0  
                 AND EL.LeaveTypeID NOT IN (SELECT ID  
                                            FROM   tbl_LeaveTypes  
                                            WHERE  Code LIKE '%Onduty%'  
                                                    OR Code LIKE '%Permission%'  
                                                       AND IsDeleted = 0  
                                                       AND DomainID = @DomainID)  
                 AND EL.StatusID IN (SELECT ID  
                                             FROM   tbl_Status  
                                             WHERE  Code = 'Approved'  
                                                    AND [Type] = 'Leave'  
                                                    AND IsDeleted = 0)  
                 --AND EL.HRStatusID NOT IN (SELECT ID  
                 --                          FROM   tbl_Status  
                 --                          WHERE  Code = 'Rejected'  
                 --                                 AND [Type] = 'Leave'  
                 --                                 AND IsDeleted = 0)  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
