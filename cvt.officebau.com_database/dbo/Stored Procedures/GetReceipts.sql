﻿/****************************************************************************             
CREATED BY   :             
CREATED DATE  :         
MODIFIED BY   :             
MODIFIED DATE  :             
 <summary>          
 [Getaccountreceipts] 2,1,1          
 </summary>                                     
 *****************************************************************************/        
Create PROCEDURE [dbo].[GetReceipts] (@ID       INT,        
                                            @DomainID INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          SELECT ar.ID              AS ID,        
                 ar.PaymentDate            AS Date,        
                 ar.PartyName       AS PartyName,        
                 ar.LedgerID        AS LedgerID,        
                 ar.ModeID     AS PaymentMode,        
                 ar.BankID          AS BankID,        
                 ar.Description     AS [Description],        
                 ar.Description       AS Reference,        
                ar.TotalAmount          AS Amount,        
                 ar.ModifiedOn      AS ModifiedOn,        
                 EMP.FullName       AS ModifiedBy        
          FROM   tbl_Receipts ar        
                 LEFT JOIN tbl_EmployeeMaster EMP        
                        ON EMP.ID = ar.ModifiedBy        
          WHERE  ar.IsDeleted = 0        
                 AND ( ar.ID = @ID        
                        OR @ID IS NULL )        
                 AND ar.DomainID = @DomainID        
        
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
