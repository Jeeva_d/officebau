﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
		Check Emailid exist in Auth_GetQuote table
		select * from Auth_WebLogin
		select * from Auth_Users
		[Auth_ValidateRequestQuoteEmailId] 'test@test.com'
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_ValidateRequestQuoteEmailId]
 (
		@Input			VARCHAR(50)
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY		
	DECLARE @Output INT = 0

		IF EXISTS(SELECT 1 FROM Auth_GetQuote WHERE Client_Email = @Input)
			SET @Output = 1
		ELSE
			SET @Output = 0
	
	SELECT	@Output AS Result
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
