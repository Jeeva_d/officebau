﻿/****************************************************************************                 
CREATED BY  :                 
CREATED DATE :              
MODIFIED BY  :            
MODIFIED DATE :      
 <summary>      
 [SearchPurchaseOrder] 1,null,null,null,null,null,null,null            
 </summary>                                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchPurchaseOrder] (@DomainID   INT,
                                             @StartDate  VARCHAR(50),
                                             @EndDate    VARCHAR(50),
                                             @VendorName VARCHAR(50),
                                             @Status     VARCHAR(50),
                                             @Notations  VARCHAR(50),
                                             @Amount     MONEY,
                                             @LedgerName VARCHAR(50))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @Count       INT,
                  @NoOfRecords INT =(SELECT Value
                    FROM   tblApplicationConfiguration
                    WHERE  IsDeleted = 0
                           AND DomainID = @DomainID
                           AND Code = 'NRECORD')
          DECLARE @table TABLE
            (
               DetailDescription VARCHAR(8000),
               EID               INT
            )

          IF( @StartDate != '' )
            SET @StartDate = Cast(@StartDate AS DATETIME)

          IF( @EndDate != '' )
            SET @EndDate = Cast(@EndDate AS DATETIME)

          INSERT INTO @table
          SELECT (SELECT Substring((SELECT + ',' + ed.Remarks
                                    FROM   tblPODetails ed
                                    WHERE  ed.IsDeleted = 0
                                           AND ex.ID = ed.POID
                                    FOR XML PATH('')), 2, 200000)),
                 ex.ID
          FROM   tblPurchaseOrder ex;

          WITH CTE
               AS (SELECT DISTINCT ex.ID                              AS ID,
                                   ex.Date                            AS BillDate,
                                   ex.PONo                            AS BillNo,
                                   ex.VendorID                        AS VendorID,
                                   vd.NAME                            AS VendorName,
                                   (SELECT Isnull(DetailDescription, '')
                                    FROM   @table d
                                    WHERE  d.EID = ex.ID)             AS [Description],
                                   ((SELECT ( ( Isnull(Sum(QTY * Amount), 0) ) + Isnull(Sum(CGST), 0) + Isnull(Sum(SGST), 0) )
                                     FROM   tblPODetails
                                     WHERE  POID = ex.ID
                                            AND Isnull(IsDeleted, 0) = 0
                                            AND DomainID = @domainID))AS TotalAmount,
                                   cd.Code                            AS [Status],
                                   ex.CreatedOn,
                                   ex.modifiedOn,
                                   Cast(fu.Id AS VARCHAR(50))         AS HistoryID,
                                   ex.Remarks                         AS Remarks,
                                   em.FullName                        AS CreatedBy,
                                   ex.CostCenterID                    AS CostCenterID
                   FROM   tblPurchaseOrder ex
                          JOIN tblPODetails ed
                            ON ed.POID = ex.ID
                          LEFT JOIN tblLedger Ld
                                 ON Ld.ID = ed.LedgerID
                          LEFT JOIN tblVendor vd
                                 ON vd.ID = ex.VendorID
                          LEFT JOIN tbl_CodeMaster cd
                                 ON cd.ID = ex.StatusID
                          LEFT JOIN tbl_FileUpload fu
                                 ON fu.Id = ex.HistoryID
                          LEFT JOIN tbl_EmployeeMaster em
                                 ON em.Id = ex.ModifiedBy
                   WHERE  Isnull(ex.IsDeleted, 0) = 0
                          AND ex.DomainID = @domainID
                          AND ( ( Isnull(@Status, '') = ''
                                  AND ex.StatusID IN(SELECT ID
                                                     FROM   tbl_CodeMaster
                                                     WHERE  [Type] != 'Close'
                                                            AND IsDeleted = 0) )
                                 OR ( Isnull(@Status, '') != 'All'
                                      AND ex.StatusID IN(SELECT ID
                                                         FROM   tbl_CodeMaster
                                                         WHERE  [Type] = @Status
                                                                AND IsDeleted = 0) )
                                 OR ( Isnull(@Status, '') = 'All'
                                      AND ex.StatusID IN(SELECT ID
                                                         FROM   tbl_CodeMaster
                                                         WHERE  [Type] IN( 'Open', 'Partial', 'Close' )
                                                                AND IsDeleted = 0) ) )
                          AND ( Isnull(@VendorName, '') = ''
                                 OR vd.NAME LIKE '%' + @VendorName + '%' )
                          AND ( Isnull(@LedgerName, '') = ''
                                 OR Ld.NAME LIKE '%' + @LedgerName + '%' )
                          AND ( ( @StartDate IS NULL
                                   OR @StartDate = '' )
                                 OR [Date] >= @StartDate )
                          AND ( ( @EndDate IS NULL
                                   OR @EndDate = '' )
                                 OR [Date] <= @EndDate ))
          SELECT *
          INTO   #TempExpense
          FROM   CTE
          WHERE  ( Isnull(@Amount, 0) = 0 )
                  OR ( Isnull(@Notations, '') = 'EqualTo'
                       AND TotalAmount = Isnull(@Amount, 0) )
                  OR ( Isnull(@Notations, '') = 'GreaterThanEqual'
                       AND TotalAmount >= @Amount )
                  OR ( Isnull(@Notations, '') = 'LessThanEqual'
                       AND TotalAmount <= @Amount )

          SET @Count = (SELECT Count(1)
                        FROM   #TempExpense)

          SELECT TOP (Isnull(@NoOfRecords, @Count)) *
          FROM   #TempExpense
          ORDER  BY BillDate DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
