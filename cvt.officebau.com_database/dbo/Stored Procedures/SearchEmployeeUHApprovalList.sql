﻿
/****************************************************************************   
CREATED BY   : Ajith N  
CREATED DATE  : 10 Aug 2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
   [SearchEmployeeUHApprovalList]  '' , '', '', '', '', '224',224,1  
 </summary>                            
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchEmployeeUHApprovalList] (@EmployeeCode     VARCHAR(20),
                                                      @TempEmployeeCode VARCHAR(20),
                                                      @Name             VARCHAR(100),
                                                      @ContactNo        VARCHAR(20),
                                                      @IsActive         BIT,
                                                      @ApproverID       INT,
                                                      @EmployeeID       INT,
                                                      @DomainID         INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @BusinessUnitIDs VARCHAR(50) = (SELECT BusinessUnitID
             FROM   tbl_EmployeeMaster
             WHERE  ID = @EmployeeID
                    AND DomainID = @DomainID)
          DECLARE @BusinessUnitTable TABLE
            (
               BusinessUnit VARCHAR(100)
            )

          INSERT INTO @BusinessUnitTable
          SELECT @BusinessUnitIDs

          SELECT EM.ID                                        AS ID,
                 ISNULL(EM.EmpCodePattern, '') + '' + EM.Code                                      AS EmployeeCode,
                 APPROVAL.TempEmployeeCode                    AS TempEmployeeCode,
                 EM.FirstName + ' ' + ISNULL(EM.LastName, '') AS FirstName,
                 DEP.NAME                                     AS DepartmentName,
                 DESI.NAME                                    AS DesignationName,
                 ISNULL(EM.ContactNo, '')                     AS ContactNo,
                 ISNULL(EM.IsActive, 'False')                 AS IsApproved,
                 BU.NAME                                      AS BaseLocation,
                 EM.BaseLocationID
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_Department DEP
                        ON DEP.ID = EM.DepartmentID
                 LEFT JOIN tbl_Designation DESI
                        ON DESI.ID = EM.DesignationID
                 LEFT JOIN tbl_Functional FUN
                        ON FUN.ID = EM.FunctionalID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
                 LEFT JOIN tbl_EmployementType EMPLOYEETYPE
                        ON EMPLOYEETYPE.ID = EM.EmploymentTypeID
                 LEFT JOIN tbl_EmployeeMaster REPORTING
                        ON REPORTING.ID = EM.ReportingToID
                 LEFT JOIN tbl_EmployeeStatutoryDetails STATUTORY
                        ON STATUTORY.EmployeeID = EM.ID
                 LEFT JOIN tbl_EmployeeApproval APPROVAL
                        ON APPROVAL.EmployeeID = EM.ID
                 JOIN @BusinessUnitTable TBU
                   ON TBU.BusinessUnit LIKE '%,'
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))
                                            + ',%'
          WHERE  EM.IsDeleted = 0
                 AND ISNULL(APPROVAL.FirstApproverId, 0) <> 0
                 AND ( ( EM.Code NOT LIKE '%' + ISNULL(@EmployeeCode, 'TMP_') + '%' )
                        OR ( EM.Code LIKE '%' + @EmployeeCode + '%' ) )
                 AND ( ISNULL(@TempEmployeeCode, '') = ''
                        OR EM.Code LIKE '%' + @TempEmployeeCode + '%' )
                 AND ( ISNULL(@Name, '') = ''
                        OR ( ( EM.FirstName + ' ' + ISNULL(EM.LastName, '') ) LIKE '%' + @Name + '%' ) )
                 AND ( ISNULL(@ContactNo, '') = ''
                        OR ( EM.ContactNo LIKE '%' + @ContactNo + '%' ) )
                 AND ( ( ISNULL(@ApproverID, 0) = 0
                         AND ISNULL(APPROVAL.SecondApproverId, 0) = 0 )
                        OR APPROVAL.SecondApproverId = @ApproverID )
                 AND ( ISNULL(@IsActive, '') = ''
                        OR ISNULL(EM.IsActive, 0) = @IsActive )
                 AND EM.domainID = @DomainID
          ORDER  BY EM.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
