﻿/****************************************************************************   
CREATED BY		: Naneeshwar.M
CREATED DATE	: 28-Nov-2017
MODIFIED BY		: 
MODIFIED DATE	: 
 <summary>
		 [Getclaimpolicy] 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetClaimPolicy] (@ID       INT,
                                         @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT cp.ID               AS ID,
                 cp.ExpenseType      AS ExpenseTypeID,
                 empDm.DesignationID AS DesignationID,
                 empDm.BandID        AS BandID,
                 empDm.GradeID       AS LevelID,
                 cp.MetroAmount      AS MetroAmount,
                 cp.NonMetroAmount   AS NonMetroAmount,
                 emp.FullName        AS ModifiedBy,
                 cp.ModifiedOn       AS ModifiedOn,
                 bu.NAME             AS BaseLocation
          FROM   tbl_ClaimPolicy cp
                 LEFT JOIN tbl_EmployeeDesignationMapping empDM
                        ON empDM.ID = cp.DesignationMappingID
                 LEFT JOIN tbl_EmployeeMaster emp
                        ON emp.ID = cp.ModifiedBy
                 LEFT JOIN tbl_BusinessUnit bu
                        ON bu.ID = emp.BaseLocationID
          WHERE  cp.IsDeleted = 0
                 AND cp.DomainID = @DomainID
                 AND cp.ID = @ID
          ORDER  BY cp.ModifiedOn DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
