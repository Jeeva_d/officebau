﻿/****************************************************************************   
CREATED BY		: Naneeshwar.M
CREATED DATE	: 07-DEC-2017
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
[SearchCFSOperation] 0,'','FCIU3423752','','','','21/2016','2136896','VIJAYALAKSHIMI TRADER',1
[SearchCFSOperation] 100,null,null,null,null,null,null,null,null,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchCFSOperation] (@Count           INT,
                                            @SearchParameter VARCHAR(50),
                                            @ContainerNo     VARCHAR(200),
                                            @LinerAgent      VARCHAR(800),
                                            @ConsigneeName   VARCHAR(800),
                                            @TransporterName VARCHAR(800),
                                            @BOENo           VARCHAR(500),
                                            @IGMNo           VARCHAR(200),
                                            @CHANo           VARCHAR(400),
                                            @DomainID        INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF ( @Count <> 0 )
            BEGIN
                DECLARE @query VARCHAR(MAX)=' SELECT TOP '
                 + Cast(Isnull(@Count, 0)AS VARCHAR)
                 + ' *
                FROM   tbl_CFSOperation
                where ( ISNULL(ContainerNo,'''') LIKE ''%'
                 + Isnull(@ContainerNo, '')
                 + '%'')
                               AND ( ISNULL(LinerAgent,'''') LIKE ''%'
                 + Isnull(@LinerAgent, '')
                 + '%'')
                               AND ( ISNULL(ConsigneeName,'''') LIKE ''%'
                 + Isnull(@ConsigneeName, '')
                 + '%'')
                               AND ( ISNULL(TransporterName,'''') LIKE ''%'
                 + Isnull(@TransporterName, '')
                 + '%'')
                               AND ( ISNULL(BOENo,'''') LIKE ''%'
                 + Isnull(@BOENo, '')
                 + '%'')
                               AND ( ISNULL(IGMNo,'''') LIKE ''%'
                 + Isnull(@IGMNo, '')
                 + '%'')
                               AND ( ISNULL(CHANo,'''') LIKE ''%'
                 + Isnull(@CHANo, '')
                 + '%'')
                ORDER  BY ModifiedOn DESC'

                PRINT @query

                EXEC (@query)
            END
          ELSE
            BEGIN
                SELECT TOP 1000 *
                FROM   tbl_CFSOperation
                WHERE  ( ContainerNo LIKE '%' + @ContainerNo + '%'
                          OR @ContainerNo IS NULL )
                       AND ( LinerAgent LIKE '%' + @LinerAgent + '%'
                              OR @LinerAgent IS NULL )
                       AND ( ConsigneeName LIKE '%' + @ConsigneeName + '%'
                              OR @ConsigneeName IS NULL )
                       AND ( TransporterName LIKE '%' + @TransporterName + '%'
                              OR @TransporterName IS NULL )
                       AND ( BOENo LIKE '%' + @BOENo + '%'
                              OR @BOENo IS NULL )
                       AND ( IGMNo LIKE '%' + @IGMNo + '%'
                              OR @IGMNo IS NULL )
                       AND ( CHANo LIKE '%' + @CHANo + '%'
                              OR @CHANo IS NULL )
                ORDER  BY ModifiedOn DESC
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
