﻿/****************************************************************************           
CREATED BY   : Naneeshwar          
CREATED DATE  :   14-Nov-2016        
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>        
 [SearchExpenseDetailList] 1059, 1,1        
 </summary>                                   
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[SearchExpenseDetailList] (@ExpenseID INT,        
                                                 @DomainID  INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          SELECT ed.ID       AS ID,        
                 ed.LedgerID AS LedgerID,        
                 ed.Remarks  AS [Description],  QTY,IsLedger AS IsProduct,      
                 ed.Amount   AS Amount,Isnull(CGST,0) CGST,Isnull(CGSTPercent,0) CGSTPercent,Isnull(SGST,0) SGST,      
     Isnull(SGSTPercent,0) SGSTPercent  ,
	 POitemID AS Poid      
          FROM   tblExpenseDetails ed        
          WHERE  ed.IsDeleted = 0        
                 AND @ExpenseID = ed.ExpenseID And ed.DomainID=@DomainID        
        
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
