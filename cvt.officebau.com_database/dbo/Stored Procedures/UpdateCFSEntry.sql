﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	28-NOV-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  truncate table tbl_CFSEntry
 [Updatecfsentry] 0,'asdU9874663',0,'10-Jun-18',224,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[UpdateCFSEntry] (@ID            INT,
                                        @ContainerName VARCHAR(50),
                                        @InwardTypeID  INT,
                                        @InwardDate    DATETIME,
                                        @SessionID     INT,
                                        @DomainID      INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output    VARCHAR(100) = 'Operation Failed!',
              @MonthDiff INT=6

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @ID = 0 )
            BEGIN
                IF( (SELECT Count(1)
                     FROM   tbl_CFSEntry
                     WHERE  isdeleted = 0
                            AND containerName = @ContainerName
                            AND inwardtypeid = @InwardTypeID) = 0 )
                  BEGIN
                      INSERT INTO tbl_CFSEntry
                                  (ContainerName,
                                   InwardTypeID,
                                   InwardDate,
                                   CreatedBy,
                                   ModifiedBy,
                                   DomainID)
                      VALUES      (@ContainerName,
                                   @InwardTypeID,
                                   @InwardDate,
                                   @SessionID,
                                   @SessionID,
                                   @DomainID)

                      SET @Output = 'Inserted Successfully'
                  END
                ELSE
                  SET @Output = 'Already Exists'
            END
          ELSE
            BEGIN
                IF( (SELECT Count(1)
                     FROM   tbl_CFSEntry
                     WHERE  isdeleted = 0
                            AND id <> @ID
                            AND containerName = @ContainerName
                            AND inwardtypeid = @InwardTypeID) = 0 )
                  BEGIN
                      UPDATE tbl_CFSEntry
                      SET    ContainerName = @ContainerName,
                             InwardTypeID = @InwardTypeID,
                             InwardDate = @InwardDate,
                             ModifiedBY = @SessionID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID
                             AND DomainID = @DomainID

                      SET @Output = 'Updated Successfully'
                  END
                ELSE
                  BEGIN
                      SET @Output = 'Already Exists'
                  END
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
