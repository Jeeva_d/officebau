﻿
/****************************************************************************         
CREATED BY		: Jeeva        
CREATED DATE	:         
MODIFIED BY		: 
MODIFIED DATE   :     
 <summary>     
 [Searchbusinessunittopemployeedashboard] 0, 0, 1    
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchBusinessUnitTopemployeeDashBoard](@MonthID  INT,
                                                               @Year     INT,
                                                               @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @YearName INT =(SELECT FinancialYear
            FROM   tbl_FinancialYear
            WHERE  ID = @Year
                   AND ISNULL(IsDeleted, 0) = 0
                   AND DomainId = @DomainID)

          SELECT TOP 1 EMP.FirstName AS FirstName,
                       Sum(Tue)      AS actual,
                       'I'           AS Type
          INTO   #ResultD
          FROM   tbl_Customer CUS
                 JOIN tbl_EmployeeMaster EMP
                   ON CUS.SalesPersonID = EMP.ID
                      AND EMP.IsDeleted = 0
                 JOIN tbl_RakeDetails RAK
                   ON RAK.CustomerID = CUS.ID
                      AND RAK.Type = 'I'
                      AND RAK.IsDeleted = 0
          WHERE  CUS.IsDeleted = 0
                 AND CUS.DomainID = @DomainID
                 AND ( ISNULL(@MonthID, 0) = 0
                        OR Month(RAK.ModifiedOn) = @MonthID )
                 AND ( ISNULL(@Year, 0) = 0
                        OR Year(RAK.ModifiedOn) = @YearName )
          GROUP  BY EMP.FirstName
          ORDER  BY actual DESC

          INSERT INTO #ResultD
          SELECT TOP 1 EMP.FirstName,
                       Sum(Tue) AS actual,
                       'E'      AS Type
          FROM   tbl_Customer CUS
                 JOIN tbl_EmployeeMaster EMP
                   ON CUS.SalesPersonID = EMP.ID
                      AND EMP.IsDeleted = 0
                 JOIN tbl_RakeDetails RAK
                   ON RAK.CustomerID = CUS.ID
                      AND RAK.Type = 'E'
                      AND RAK.IsDeleted = 0
          WHERE  CUS.IsDeleted = 0
                 AND CUS.DomainID = @DomainID
                 AND ( ISNULL(@MonthID, 0) = 0
                        OR Month(RAK.ModifiedOn) = @MonthID )
                 AND ( ISNULL(@Year, 0) = 0
                        OR Year(RAK.ModifiedOn) = @YearName )
          GROUP  BY EMP.FirstName
          ORDER  BY actual DESC

          SELECT *
          FROM   #ResultD

          DROP TABLE #ResultD
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
