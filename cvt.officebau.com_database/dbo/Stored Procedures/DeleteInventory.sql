﻿/****************************************************************************                                   
CREATED BY  :                                   
CREATED DATE :                                   
MODIFIED BY  :                           
MODIFIED DATE :                           
<summary>                                          
   DeleteInventory 83,'DELETE','Sales'  ,2,1      
</summary>                                                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteInventory] (@ID        INT,
                                          @Operation VARCHAR(10),
                                          @StockType VARCHAR(50),
                                          @SessionID INT,
                                          @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          --DECLARE @TmpID      INT = 0,
          --        @TmpItemIDs VARCHAR(100) = 0;

          --IF( @StockType = 'Purchase' )
          --  BEGIN
          --      SET @TmpID = (SELECT TOP 1 Isnull(PO.POID, 0)
          --                    FROM   tblExpenseDetails ED
          --                           LEFT JOIN tblPODetails PO
          --                                  ON PO.ID = ED.POitemId
          --                                     -- AND PO.LedgerID = 0    
          --                                     AND PO.IsDeleted = 0
          --                    WHERE  ED.IsLedger = 0
          --                           AND ED.ExpenseID = @ID
          --                    ORDER  BY PO.LedgerID DESC)
          --      SET @TmpItemIDs = Substring((SELECT ',' + Cast(Isnull(PO.ID, 0) AS VARCHAR)
          --                                   FROM   tblExpenseDetails ED
          --                                          LEFT JOIN tblPODetails PO
          --                                                 ON PO.ID = ED.POitemId
          --                                                    -- AND PO.LedgerID = 0    
          --                                                    AND PO.IsDeleted = 0
          --                                   WHERE  ED.IsLedger = 0
          --                                          AND ED.ExpenseID = @ID
          --                                   FOR XML PATH('')), 2, 2000)
          --  END
          --ELSE IF( @StockType = 'Sales' )
          --  BEGIN
          --      SET @TmpID = (SELECT TOP 1 Isnull(SO.SOID, 0)
          --                    FROM   tblInvoiceItem II
          --                           LEFT JOIN tblSOItem SO
          --                                  ON SO.ID = II.SoItemID
          --                                     AND SO.IsDeleted = 0
          --                           JOIN tblProduct_V2 P
          --                             ON P.ID = SO.ProductID
          --                                AND p.TypeID = (SELECT ID
          --                                                FROM   tbl_CodeMaster
          --                                                WHERE  [Type] = 'ProductService'
          --                                                       AND Code = 'Product'
          --                                                       AND IsDeleted = 0)
          --                    WHERE  II.InvoiceID = @ID
          --                    ORDER  BY SO.IsProduct DESC)
          --      SET @TmpItemIDs = Substring((SELECT ',' + Cast(Isnull(SO.ID, 0) AS VARCHAR)
          --                                   FROM   tblInvoiceItem II
          --                                          LEFT JOIN tblSOItem SO
          --                                                 ON SO.ID = II.SoItemID
          --                                                    AND SO.IsDeleted = 0
          --                                          JOIN tblProduct_V2 P
          --                                            ON P.ID = SO.ProductID
          --                        AND p.TypeID = (SELECT ID
          --                                                               FROM   tbl_CodeMaster
          --                                                               WHERE  [Type] = 'ProductService'
          --                                                                      AND Code = 'Product'
          --                                                                      AND IsDeleted = 0)
          --                                   WHERE  II.InvoiceID = @ID
          --                                   FOR XML PATH('')), 2, 2000)
          --  END

          IF( @StockType = 'Purchase' )
            BEGIN
                 UPDATE I
                      SET    I.IsDeleted = 1,
                             I.ModifiedBy = @SessionID,
                             I.ModifiedOn = Getdate()
                      FROM   tbl_Inventory I
                      WHERE  I.ReferenceID = @ID
                             AND I.StockType = 'Purchase'
            END
          ELSE IF( @StockType = 'PO' )
            BEGIN
                UPDATE I
                SET    I.IsDeleted = PO.IsDeleted,
                       I.ModifiedBy = @SessionID,
                       I.ModifiedOn = Getdate()
                FROM   tbl_Inventory I
                       JOIN tblPODetails PO
                         ON PO.POID = I.ReferenceID
                            AND PO.ID = I.ItemID
                WHERE  I.ReferenceID = @ID
                       AND I.StockType = 'PO'
            END
          ELSE IF( @StockType = 'Sales' )
            BEGIN
                UPDATE I
                      SET    I.IsDeleted = II.IsDeleted,
                             I.ModifiedBy = @SessionID,
                             I.ModifiedOn = Getdate()
                      FROM   tbl_Inventory I
                             JOIN tblInvoiceItem II
                               ON II.InvoiceID = I.ReferenceID
                                  AND II.ID = I.ItemID
                      WHERE  I.ReferenceID = @ID
                             AND I.StockType = 'Sales'
            END
          ELSE IF( @StockType = 'SO' )
            BEGIN
                UPDATE I
                SET    I.IsDeleted = SO.IsDeleted,
                       I.ModifiedBy = @SessionID,
                       I.ModifiedOn = Getdate()
                FROM   tbl_Inventory I
                       JOIN tblSOItem SO
                         ON SO.SOID = I.ReferenceID
                            AND SO.ID = I.ItemID
                WHERE  I.ReferenceID = @ID
       AND I.StockType = 'SO'
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
