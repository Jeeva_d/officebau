﻿

/****************************************************************************               
CREATED BY   :               
CREATED DATE  :               
MODIFIED BY   :               
MODIFIED DATE  :               
<summary>                      
	[Rpt_TDSITDeclarationReport]  0,8,1,'Declared'
</summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_TDSITDeclarationReport] (@EmployeeID INT,
                                                    @FYID       INT,
                                                    @DomainID   INT,
                                                    @Type       VARCHAR(20) = '')
AS
  BEGIN
      SET NOCOUNT ON;

      --DECLARE @FYID INT = @StartYear
      --SET @StartYear=(SELECT FinancialYear
      --                FROM   tbl_FinancialYear
      --                WHERE  ID = @StartYear
      --                       AND IsDeleted = 0
      --                       AND DomainID = @DomainID)
      --DECLARE @RegionID INT=0
      BEGIN TRY
          --SET @RegionID=(SELECT RegionID
          --               FROM   tbl_EmployeeMaster
          --               WHERE  id = @EmployeeID)
          CREATE TABLE #TempDeclarationTable
            (
               EmployeeName  VARCHAR(1000),
               Section       VARCHAR(1000),
               Component     VARCHAR(1000),
               FinancialYear VARCHAR(100),
               Amount        MONEY,
               [Type]        VARCHAR(50)
            )

          INSERT INTO #TempDeclarationTable
          SELECT em.code + '-' + em.FullName,
                 SEC.Code                    AS Section,
                 COMP.Code                   AS Component,
                 FY.DisplayName              AS DisplayName,
                 ISNULL(DECL.Declaration, 0) AS Amount,
                 'Declared'                  AS [Type]
          FROM   tbl_EmployeeMaster EM
                 JOIN TDSComponent COMP
                   ON COMP.DomainID = EM.DomainID
                      AND COMP.FYID = @FYID
                      AND COMP.Code NOT IN ( 'Is Senior citizens included.' )
                 JOIN TDSSection SEC
                   ON SEC.ID = COMP.SectionID
                 LEFT JOIN TDSDeclaration DECL
                        ON DECL.IsDeleted = 0
                           AND DECL.EmployeeID = EM.ID
                           AND DECL.FinancialYearID = @FYID
                           AND DECL.ComponentID = comp.ID
                 LEFT JOIN tbl_FinancialYear FY
                        ON fy.IsDeleted = 0
                           AND fy.ID = COMP.FYID
          WHERE  EM.IsDeleted = 0
                 AND EM.isActive = 0
                 AND EM.DomainID = @DomainID
                 AND COMP.FYID = @FYID
          -- ORDER  BY SEC.Ordinal ASC
          UNION ALL
          SELECT em.code + '-' + em.FullName,
                 SEC.Code                AS Section,
                 COMP.Code               AS Component,
                 FY.DisplayName          AS DisplayName,
                 ISNULL(DECL.Cleared, 0) AS Amount,
                 'Cleared'               AS [Type]
          FROM   tbl_EmployeeMaster EM
                 JOIN TDSComponent COMP
                   ON COMP.DomainID = EM.DomainID
                      AND COMP.FYID = @FYID
                      AND COMP.Code NOT IN ( 'Is Senior citizens included.' )
                 JOIN TDSSection SEC
                   ON SEC.ID = COMP.SectionID
                 LEFT JOIN TDSDeclaration DECL
                        ON DECL.IsDeleted = 0
                           AND DECL.EmployeeID = EM.ID
                           AND DECL.FinancialYearID = @FYID
                           AND DECL.ComponentID = comp.ID
                 LEFT JOIN tbl_FinancialYear FY
                        ON fy.IsDeleted = 0
           AND fy.ID = COMP.FYID
          WHERE  EM.IsDeleted = 0
                 AND EM.isActive = 0
                 AND EM.DomainID = @DomainID
                 AND COMP.FYID = @FYID
          --ORDER  BY SEC.Ordinal ASC
          UNION ALL
          SELECT em.code + '-' + em.FullName,
                 SEC.Code                AS Section,
                 COMP.Code               AS Component,
                 FY.DisplayName          AS DisplayName,
                 ISNULL(DECL.Submitted, 0) AS Amount,
                 'Proofs submitted'               AS [Type]
          FROM   tbl_EmployeeMaster EM
                 JOIN TDSComponent COMP
                   ON COMP.DomainID = EM.DomainID
                      AND COMP.FYID = @FYID
                      AND COMP.Code NOT IN ( 'Is Senior citizens included.' )
                 JOIN TDSSection SEC
                   ON SEC.ID = COMP.SectionID
                 LEFT JOIN TDSDeclaration DECL
                        ON DECL.IsDeleted = 0
                           AND DECL.EmployeeID = EM.ID
                           AND DECL.FinancialYearID = @FYID
                           AND DECL.ComponentID = comp.ID
                 LEFT JOIN tbl_FinancialYear FY
                        ON fy.IsDeleted = 0
                           AND fy.ID = COMP.FYID
          WHERE  EM.IsDeleted = 0
                 AND EM.isActive = 0
                 AND EM.DomainID = @DomainID
                 AND COMP.FYID = @FYID

          --ORDER  BY SEC.Ordinal ASC
          IF NOT EXISTS(SELECT 1
                        FROM   #TempDeclarationTable)
            BEGIN
                INSERT INTO #TempDeclarationTable
                SELECT em.code + '-' + em.FullName,
                       SEC.Code       AS Section,
                       COMP.Code      AS Component,
                       FY.DisplayName AS DisplayName,
                       0              AS Amount,
                       @Type          AS [Type]
                FROM   tbl_EmployeeMaster EM
                       JOIN TDSComponent COMP
                         ON COMP.DomainID = EM.DomainID
                            AND COMP.FYID = (SELECT TOP 1 FYID
                                             FROM   TDSComponent
                                             WHERE  IsDeleted = 0
                                                    AND DomainID = @DomainID)
                            AND COMP.Code NOT IN ( 'Is Senior citizens included.' )
                       JOIN TDSSection SEC
                         ON SEC.ID = COMP.SectionID
                       LEFT JOIN tbl_FinancialYear FY
                              ON fy.IsDeleted = 0
                                 AND fy.ID = COMP.FYID
                WHERE  EM.IsDeleted = 0
                       AND EM.isActive = 0
                       AND EM.DomainID = @DomainID
                --AND COMP.FYID = @FYID
                ORDER  BY SEC.Ordinal ASC
            END

          DECLARE @Header VARCHAR(max) = Substring((SELECT DISTINCT ',[' + EmployeeName + ']'
                       FROM   #TempDeclarationTable
                       FOR xml path('')), 2, 2000000)

          EXEC('SELECT FinancialYear AS [Financial Year],Section, Component, ' + @Header + ' FROM (SELECT *
				 FROM #TempDeclarationTable
				 where (''' + @Type + '''='''' OR [Type] = ''' + @Type + ''')
					AND ISNULL(Amount,0) > 0) AS SourceTable
				PIVOT
				(
				 SUM(Amount)
				 FOR EmployeeName IN (' + @Header +')
				) AS PivotTable;')
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY();
          RAISERROR(@ErrorMsg,@ErrSeverity,1);
      END CATCH;
  END
