﻿/****************************************************************************     
CREATED BY     :   Ajith N  
CREATED DATE  :   05 Mar 2018  
MODIFIED BY   :      
MODIFIED DATE  :     
<summary>      
 GetPayrollComponentConfigValue '13', 1  
</summary>                             
*****************************************************************************/  
CREATE PROCEDURE [dbo].[GetPayrollComponentConfigValue] (@BusinessUnit VARCHAR(100),  
                                                        @DomainID     INT)  
AS  
  BEGIN  
      BEGIN TRY  
          DECLARE @Result VARCHAR(max) = ''  
  
          SET @Result = ISNULL(Substring((SELECT DISTINCT ',' + Rtrim(Ltrim(UPPER(code))) + '='  
                                                          + ISNULL(pcc.ConfigValue, 0)  
                                          FROM   tbl_PayrollComponentConfiguration pcc  
                                                 LEFT JOIN tbl_PayrollComponentMaster pcm  
                                                        ON pcc.PayrollComponentID = pcm.ID  
                                                 JOIN (SELECT TOP 1 Splitdata  
                                                       FROM   dbo.FNSPLITSTRING (( @BusinessUnit ), ',')  
                                                       WHERE  ISNULL(Splitdata, '') <> '') x  
                                                   ON x.Splitdata = pcc.BusinessUnitID  
                                          FOR xml path('')), 2, 2000), '')  
  
          SELECT @Result AS Result  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
