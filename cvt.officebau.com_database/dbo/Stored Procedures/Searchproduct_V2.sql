﻿/****************************************************************************               
CREATED BY   : Jeeva              
CREATED DATE  :               
MODIFIED BY   :           
MODIFIED DATE  :             
 <summary>     
 [Searchproduct_V2] '',1,0         
 </summary>                                       
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchproduct_V2] (@Name     VARCHAR(100),  
                                          @DomainID INT,
                                          @TypeID  INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT pd.ID                  AS ID,  
                 pd.NAME                AS NAME,  
                 pd.MFG                 AS Manufacturer,  
                 cm.Code                AS TypeName,  
                 pd.TypeID              AS TypeID,  
                 GH.Code                AS HsnCode,  
                 GH.SGST,  
                 GH.CGST,  
                 GH.IGST,  
                 ld.NAME                AS SalesLedgerName,  
                 pd.IsSales             AS IsSales,  
                 pd.SalesDescription    AS SalesDescription,  
                 pd.SalesPrice          AS SalesPrice,  
                 pld.NAME               AS PurchaseLedgerName,  
                 pd.PurchaseDescription AS PurchaseDescription,  
                 pd.PurchasePrice       AS PurchasePrice,  
                 pd.IsPurchase          AS IsPurchase,  
                 pd.IsInventroy         AS IsInventroy,  
                 pd.OpeningStock        AS OpeningStock,  
                 pd.LowStockThresold    AS LowStockThresold  
          FROM   tblproduct_v2 pd  
                 LEFT JOIN tbl_CodeMaster cm  
                        ON cm.ID = pd.TypeID  
                 LEFT JOIN tblGstHSN GH  
                        ON pd.HSNId = GH.ID  
                 LEFT JOIN tblLedger ld  
                        ON ld.ID = pd.SalesLedgerId  
                 LEFT JOIN tblLedger pld  
                        ON pld.ID = pd.PurchaseLedgerId  
          WHERE  pd.NAME LIKE '%' + Isnull(@Name, '') + '%'  
                 AND pd.DomainID = @DomainID  
                 AND pd.IsDeleted = 0  
                 AND (@TypeID = 0 OR pd.TypeID = @TypeID)
          ORDER  BY pd.ModifiedON DESC  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
