﻿/****************************************************************************                         
CREATED BY    :                        
CREATED DATE  :                         
MODIFIED BY   :                       
MODIFIED DATE :                      
<summary>       
[Get_Update_Paystub_Console]  'GET'                 
</summary>                                                 
*****************************************************************************/  
CREATE PROCEDURE [dbo].[Get_Update_Paystub_Console](@Operation VARCHAR(50)='GET')  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN  
              SELECT P.*,  
                     COM.NAME AS CompanyName  ,
					 (select ISnull(IncludeLeaveDetailsInPaystub,0) from tbl_BusinessHours where DomainID = p.DomainID) As leaveDetails
              FROM   tbl_PaystubEmployeeList P  
                     JOIN tbl_Company COM  
                       ON COM.ID = P.DomainID  
  
              TRUNCATE TABLE tbl_PaystubEmployeeList  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
