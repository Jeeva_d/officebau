﻿/****************************************************************************         
CREATED BY   : Jeeva        
CREATED DATE  :         
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>         
 [ManageLedger] 2,1,'','',1,121212,1,1,1,1        
  select * from  tblledger           
  update tblExpenseDetails set isdeleted=0        
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageLedger] (@ID             INT,
                                      @GroupID        INT,
                                      @Name           VARCHAR(100),
                                      @Remarks        VARCHAR(1000),
                                      @OpeningBalance MONEY,
                                      @IsExpense      BIT,
                                      @HasDeleted     BIT,
                                      @SessionID      INT,
                                      @DomainID       INT,
                                      @LedgerType     VARCHAR(100),
                                      @HsnID          INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(1000) = ''

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @HasDeleted = 1 )
            BEGIN
			              
                DECLARE  @TABLEQURY  VARCHAR(max)
                DECLARE @Table TABLE
                  (
                     RCount INT
                  )
                
                SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME
                                         + ' WHERE LedgerID = '
                                         + CONVERT(VARCHAR(10), @ID)
                                         + ' and IsDeleted = 0  and DomainID='
                                         + CONVERT(VARCHAR(10), @DomainID) + '; '
                                  FROM   INFORMATION_SCHEMA.COLUMNS
                                  WHERE  COLUMN_NAME = 'LedgerID'
                                         AND TABLE_NAME IN ('tblProduct', 'tblExpenseDetails', 'tblInvoicePayment', 'tblReceipts')
                                  FOR XML PATH(''))
                SET @TABLEQURY = @TABLEQURY + ( 'SELECT 1 FROM  tblJournal WHERE LedgerProductID = '
                                                + CONVERT(VARCHAR(10), @ID)
                                                + ' and IsDeleted = 0  and DomainID='
                                                + CONVERT(VARCHAR(10), @DomainID) + '; ' )
                SET @TABLEQURY = @TABLEQURY + ( 'SELECT 1 FROM  tblBudget WHERE [Type] = ''Ledger'' AND BudgetId = '
                                                + CONVERT(VARCHAR(10), @ID)
                                                + ' and IsDeleted = 0  and DomainID='
												+ CONVERT(VARCHAR(10), @DomainID) + '; ' )

                PRINT @TABLEQURY

                INSERT INTO @Table
            EXEC(@TABLEQURY)

                IF( (SELECT Count(1)
                     FROM   @Table) = 0 )
                  BEGIN
                      UPDATE tblledger
                      SET    IsDeleted = 1
                      WHERE  ID = @ID
                             AND DomainID = @DomainID

                      SET @Output = (SELECT [Message]
                                     FROM   tblErrorMessage
                                     WHERE  [Type] = 'Information'
                                            AND Code = 'RCD_DEL'
                                            AND IsDeleted = 0) --'Deleted Successfully'   

                      GOTO Finish
                  END
                ELSE
                  BEGIN
                      SET @Output = (SELECT [Message]
                                     FROM   tblErrorMessage
                                     WHERE  [Type] = 'Warning'
                                            AND Code = 'RCD_REF'
                                            AND IsDeleted = 0) --'The record is referred.'        

                      GOTO Finish
                  END
            END
          ELSE
            BEGIN
                IF( @ID = 0 )
                  BEGIN
                      IF( (SELECT Count(1)
                           FROM   tblledger
                           WHERE  NAME = @Name
                                  AND DomainID = @DomainID
                                  AND IsExpense = @IsExpense
                                  AND IsDeleted = 0) = 0 )
                        BEGIN
                            INSERT INTO tblledger
                                        (GroupID,
                                         NAME,
                                         Remarks,
                                         IsExpense,
                                         OpeningBalance,
                                         CreatedBy,
                                         CreatedOn,
                                         ModifiedBy,
                                         ModifiedOn,
                                         DomainID,
                                         HistoryID,
                                         IsTaxComponent,
                                         IsDeleted,
                                         LedgerType,
                                         GstHSNID)
                            VALUES      (@GroupID,
                                         @Name,
                                         @Remarks,
                                         @IsExpense,
                                         @OpeningBalance,
                                         @SessionID,
                                         Getdate(),
                                         @SessionID,
                                         Getdate(),
                                         @DomainID,
                                         Newid(),
                                         0,
                                         0,
                                         @LedgerType,
                                         @HsnID)

                            DECLARE @RefID INT = @@IDENTITY

                            SET @Output = (SELECT [Message]
                                           FROM   tblErrorMessage
                                           WHERE  [Type] = 'Information'
                                                  AND Code = 'RCD_INS'
                                                  AND IsDeleted = 0)
                                          + '/' + Cast(@RefID AS VARCHAR(100)) + '/' + @Name --'Inserted Successfully/'     
                        END
                      ELSE
       SET @Output = (SELECT [Message]
                                       FROM   tblErrorMessage
                                       WHERE  [Type] = 'Warning'
                                              AND Code = 'RCD_EXIST'
                                              AND IsDeleted = 0) --'Already Exists'        
                  END
                ELSE
                  BEGIN
                      IF ( (SELECT Count(1)
                            FROM   tblLedger
                            WHERE  NAME = @Name
                                   AND DomainID = @DomainID
                                   AND ID <> @ID
                                   AND IsDeleted = 0) = 0 )
                        BEGIN
                            UPDATE tblledger
                            SET    GroupID = @GroupID,
                                   NAME = @Name,
                                   Remarks = @Remarks,
                                   OpeningBalance = @OpeningBalance,
                                   IsExpense = @IsExpense,
                                   ModifiedBy = @SessionID,
                                   ModifiedOn = Getdate(),
                                   LedgerType = @LedgerType,
                                   GstHSNID = @HsnID
                            WHERE  ID = @ID

                            SET @Output = (SELECT [Message]
                                           FROM   tblErrorMessage
                                           WHERE  [Type] = 'Information'
                                                  AND Code = 'RCD_UPD'
                                                  AND IsDeleted = 0) --'Updated Successfully'        
                        END
                      ELSE
                        BEGIN
                            SET @Output = (SELECT [Message]
                                           FROM   tblErrorMessage
                                           WHERE  [Type] = 'Warning'
                                                  AND Code = 'RCD_EXIST'
                                                  AND IsDeleted = 0) --'Already Exits'        
                        END
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
