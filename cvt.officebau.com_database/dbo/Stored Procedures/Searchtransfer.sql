﻿  
/****************************************************************************       
CREATED BY   : Sebastin      
CREATED DATE  : 11-11-2016      
MODIFIED BY   :   Dhanalakshmi    
MODIFIED DATE  :    30-03-17   
 <summary>    
 select * from tblcontravocher    
[Searchtransfer] 1,NULL,NULL,1,'EqualTo','14,000.00'  
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchtransfer] (@DomainID  INT,  
                                        @StartDate VARCHAR(50),  
                                        @EndDate   VARCHAR(50),  
                                        @BankID    INT,  
                                        @Notations VARCHAR(50),  
                                        @Amount    MONEY)  
AS  
BEGIN  
    SET NOCOUNT ON;  
  
    BEGIN TRY  
        BEGIN TRANSACTION  
   
        IF( @StartDate != '' )  
          SET @StartDate = Cast(@StartDate AS DATETIME)  
  
        IF( @EndDate != '' )  
          SET @EndDate = Cast(@EndDate AS DATETIME)  
  
        SELECT con.ID             AS ID,  
               FromID             AS FromID,  
               bk.DisplayName            AS TransferFromName,  
               ToID               AS ToID,  
               ban.DisplayName           AS TransferToName,  
               Amount             AS Amount,  
               VoucherDate        AS VoucherDate,  
               VoucherDescription AS VoucherDescription  
        INTO   #TempTransfer  
        FROM   tblTransfer con  
               LEFT JOIN tblBank bk  
                      ON bk.ID = con.FromID  
               LEFT JOIN tblBank ban  
                      ON ban.ID = con.ToID  
        WHERE  con.DomainID = @DomainID  
               AND con.IsDeleted = 0  
               AND ( ( @StartDate IS NULL  
                        OR @StartDate = '' )  
                      OR VoucherDate >= @StartDate )  
               AND ( ( @EndDate IS NULL  
                        OR @EndDate = '' )  
                      OR VoucherDate <= @EndDate )  
               AND ( ( Isnull(@BankID, '') = ''  
                        OR con.FromID = @BankID )  
                      OR ( Isnull(@BankID, '') = ''  
                            OR con.ToID = @BankID ) )  
        ORDER  BY con.ModifiedOn DESC  
  
        SELECT *  
        FROM   #TempTransfer  
        WHERE  ( Isnull(@Amount, 0) = 0 )  
                OR ( Isnull(@Notations, '') = 'EqualTo'  
                     AND Amount = @Amount )  
                OR ( Isnull(@Notations, '') = 'GreaterThanEqual'  
                     AND Amount >= @Amount )  
                OR ( Isnull(@Notations, '') = 'LessThanEqual'  
                     AND Amount <= @Amount )  
  
        COMMIT TRANSACTION  
    END TRY  
    BEGIN CATCH  
        ROLLBACK TRANSACTION  
        DECLARE @ErrorMsg    VARCHAR(100),  
                @ErrSeverity TINYINT  
        SELECT @ErrorMsg = Error_message(),  
               @ErrSeverity = Error_severity()  
        RAISERROR(@ErrorMsg,@ErrSeverity,1)  
    END CATCH  
END
