﻿/****************************************************************************           
CREATED BY  :       
CREATED DATE :       
MODIFIED BY  :           
MODIFIED DATE :           
 <summary>  
 [GetExpiryAlert] 2,1      
 </summary>                                   
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetExpiryAlert] (@EmployeeID INT,
                                         @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @ExpiryValue INT= CASE
              WHEN( Isnumeric((SELECT ConfigValue
                               FROM   tbl_ApplicationConfiguration
                               WHERE  ConfigurationID = (SELECT ID
                                                         FROM   tbl_configurationmaster
                                                         WHERE  code = 'ExpiryNotification')
                                      AND BusinessUnitID = (SELECT BaseLocationID
                                                            FROM   tbl_EmployeeMaster
                                                            WHERE  id = @EmployeeID))) = 0 ) THEN 0
              ELSE Cast((SELECT ConfigValue
                         FROM   tbl_ApplicationConfiguration
                         WHERE  ConfigurationID = (SELECT ID
                                                   FROM   tbl_configurationmaster
                                                   WHERE  code = 'ExpiryNotification')
                                AND BusinessUnitID = (SELECT BaseLocationID
                                                      FROM   tbl_EmployeeMaster
                                                      WHERE  id = @EmployeeID)) AS INT)
            END;

          IF( (SELECT ApplicationRoleID
               FROM   tbl_EmployeeOtherDetails
               WHERE  EmployeeID = @EmployeeID
                      AND IsDeleted = 0) = (SELECT ID
                                            FROM   tbl_ApplicationRole
                                            WHERE  domainID = @DomainID
                                                   AND NAME = 'HR Manager'
                                                   AND IsDeleted = 0) )
            BEGIN
                SELECT 'Skill Set' AS Name,
                       Cast(Count(*) AS VARCHAR) AS BindValue,
                       'fa fa-briefcase' AS Icon
                FROM   tbl_EmployeeSkills
                WHERE  IsDeleted = 0
                       AND DomainID = @DomainID
                       AND DateofExpiry BETWEEN Getdate() AND Getdate() + @ExpiryValue
                UNION ALL
                SELECT 'Document' AS Name,
                       Cast(Count(*) AS VARCHAR) AS BindValue,
                       'fa fa-file-text' AS Icon
                FROM   tbl_EmployeeDocuments
                WHERE  IsDeleted = 0
                       AND DomainID = @DomainID
                       AND DateOfExpiry BETWEEN Getdate() AND Getdate() + @ExpiryValue;
            END;
          ELSE
            BEGIN
                SELECT 'Skill Set' AS Name,
                       Cast(Count(*) AS VARCHAR) AS BindValue,
                       'fa fa-briefcase' AS Icon
                FROM   tbl_EmployeeSkills
                WHERE  IsDeleted = 0
                       AND DomainID = @DomainID
                       AND EmployeeID IN (SELECT Id
                                          FROM   tbl_employeemaster
                                          WHERE  ReportingToID = @EmployeeID)
                       AND DateofExpiry BETWEEN Getdate() AND Getdate() + @ExpiryValue
                UNION ALL
                SELECT 'Document' AS Name,
                       Cast(Count(*) AS VARCHAR) AS BindValue,
                       'fa fa-file-text' AS Icon
                FROM   tbl_EmployeeDocuments
                WHERE  IsDeleted = 0
                       AND EmployeeID IN (SELECT Id
                                          FROM   tbl_employeemaster
                                          WHERE  ReportingToID = @EmployeeID)
                       AND DomainID = @DomainID
                       AND DateOfExpiry BETWEEN Getdate() AND Getdate() + @ExpiryValue;
            END;
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity();
          RAISERROR(@ErrorMsg,@ErrSeverity,1);
      END CATCH;
  END;
