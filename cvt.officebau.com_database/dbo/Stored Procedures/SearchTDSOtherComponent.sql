﻿  
/****************************************************************************   
CREATED BY   : Ajith N  
CREATED DATE  : 21 Nov 2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>       
  [SearchTDSOtherComponent] null, 106, 1,3  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchTDSOtherComponent] (@BusinessUnitID INT,  
                                                 @UserID         INT,  
                                                 @DomainID       INT,  
             @StartYearID INT)  
AS  
  
  BEGIN  
      SET NOCOUNT ON;  
  
   DECLARE @FinancialYearName INT  
  
      SET @FinancialYearName =(SELECT NAME  
                              FROM   tbl_FinancialYear  
                              WHERE  id = @StartYearID  
                                     AND IsDeleted = 0  
                                     AND DomainID = @DomainID)  
  
      BEGIN TRY  
          SELECT OI.ID,  
                 OI.EmployeeID,  
                 ISNULL(emp.EmpCodePattern, '') + emp.Code                                       AS EmployeeCode,  
                 emp.FullName AS EmployeeName,  
                 OI.Year                                        AS FinancialYear,  
                 Isnull(OI.Bonus, 0)                            AS Bonus,  
                 Isnull(OI.PLI, 0)                              AS PLI,  
                 Isnull(OI.OtherIncome, 0)                      AS OtherIncome,  
     BU.Name                                        AS BusinessUnit  
          FROM   tbl_TDSEmployeeOtherIncome OI  
                 JOIN tbl_EmployeeMaster emp  
                   ON emp.ID = OI.EmployeeID  
     LEFT JOIN tbl_FinancialYear FY  
                   ON FY.ID = @StartYearID  
     LEFT JOIN tbl_BusinessUnit BU  
                    ON bu.ID = emp.BaseLocationID  
     JOIN (SELECT *  
                       FROM   dbo.Splitstring ((SELECT BusinessUnitID  
                                                FROM   tbl_EmployeeMaster  
                                                WHERE  ID = @UserID  
                                                       AND DomainID = @DomainID), ',')  
                       WHERE  Isnull(Item, '') <> '') i  
                   ON i.Item = BU.ID  
          WHERE  emp.IsDeleted = 0  
                 AND OI.IsDeleted = 0  
                 AND emp.DomainID = @DomainID  
                 AND (Isnull(@BusinessUnitID, '') = '' OR  @BusinessUnitID = 0 OR emp.BaseLocationID = @BusinessUnitID)  
     AND OI.Year = @FinancialYearName  
                 AND Isnull(BU.ParentID, 0) <> 0  
          ORDER  BY emp.FullName  
  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
