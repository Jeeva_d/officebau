﻿/****************************************************************************       
CREATED BY    :     
CREATED DATE  :     
MODIFIED BY   :   Ajith.N   
MODIFIED DATE :    22-Mar-2017   
 <summary>     
  [GetTrailBalance] 1, '04/01/2017' ,'03/31/2018'      
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Gettrailbalance] (@DomainID        INT,
                                         @SearchStartDate VARCHAR(25) = '',
                                         @SearchEndDate   VARCHAR(25) = '')
AS
  BEGIN
      BEGIN TRY
          SET @SearchStartDate = Cast(@SearchStartDate AS DATETIME)
          SET @SearchEndDate = Cast(@SearchEndDate AS DATETIME)

          ------- P & l --------------    
          DECLARE @PL TABLE
            (
               ID          INT IDENTITY(1, 1),
               NAME        VARCHAR(100),
               AMOUNT      MONEY,
               LEDGER      VARCHAR(100),
               GROUPLEDGER VARCHAR(100),
               CATEGORY    VARCHAR(100),
               BranchName  NVARCHAR(100)
            )
          DECLARE @plvALUE MONEY,
                  @pltypee VARCHAR(100)='EXPENSE'

          INSERT INTO @PL
          SELECT tblledger.NAME,
                 openingBalance,
                 tblledger.NAME,
                 S.NAME,
                 --'EXPENSE'
                 CASE S.ReportType
                   WHEN 1 THEN 'EXPENSE'
                   WHEN 2 THEN 'INCOME'
                   ELSE 'EXPENSE'
                 END,
                 ''
          FROM   tblledger
                 JOIN tblGroupLedger S
                   ON tblledger.GroupID = S.ID
          WHERE  isexpense = 1
                 AND tblledger.DomainID = @DomainID
                 AND tblledger.ModifiedOn >= @SearchStartDate
                 AND tblledger.ModifiedOn <= @SearchEndDate
                 AND tblledger.isdeleted = 0
          UNION ALL
          SELECT E.NAME,
                 E.AMOUNT,
                 E.Ledger,
                 E.Groups,
                 'EXPENSE',
                 --CASE E.ReportType
                 --  WHEN 1 THEN 'EXPENSE'
                 --  WHEN 2 THEN 'INCOME'
                 --  ELSE 'EXPENSE'
                 --END,
                 ''
          FROM   VW_expense E
          WHERE  E.DomainID = @DomainID
                 --AND E.ReportType <> 3
                 AND E.[Date] >= @SearchStartDate
                 AND E.[Date] <= @SearchEndDate
          UNION ALL
          SELECT P.NAME,
                 --( i.QTY * i.Rate )
                 CASE
                   WHEN TI.DiscountPercentage != 0 THEN ( ( I.QTY * I.Rate )+(I.SGSTAmount + I.CGSTAmount) - ( ( I.QTY * I.Rate ) * TI.DiscountPercentage ) )
                   ELSE ( ( I.QTY * I.Rate )+(I.SGSTAmount + I.CGSTAmount) - TI.DiscountValue )
                 END,
                 P.NAME,
                 'Revenue',
                 'INCOME',
                 ''
          FROM   tblinvoiceitem I
                 JOIN tblproduct_v2 P
                   ON I.ProductID = P.ID
                 JOIN tblInvoice TI
                   ON TI.ID = I.InvoiceID
          WHERE  I.isdeleted = 0
                 AND I.DomainID = @DomainID
                 AND TI.[Date] >= @SearchStartDate
                 AND TI.[Date] <= @SearchEndDate
          UNION ALL
          SELECT tblledger.NAME,
                 Isnull(openingBalance, 0),
                 tblledger.NAME,
                 S.NAME,
                 -- 'INCOME'
                 CASE S.ReportType
                   WHEN 1 THEN 'EXPENSE'
                   WHEN 2 THEN 'INCOME'
                   ELSE 'EXPENSE'
                 END,
                 ''
          FROM   tblledger
                 JOIN tblGroupLedger S
                   ON tblledger.GroupID = S.ID
          WHERE  isexpense = 0
                 AND tblledger.DomainID = @DomainID
                 AND tblledger.isdeleted = 0
                 AND tblLedger.ModifiedOn >= @SearchStartDate
                 AND tblLedger.ModifiedOn <= @SearchEndDate

          SET @plvALUE= ( (SELECT Sum(AMOUNT)
                           FROM   @PL
                           WHERE  CATEGORY = 'INCOME') - (SELECT Sum(AMOUNT)
                                                          FROM   @PL
                                                          WHERE  CATEGORY = 'EXPENSE') )

          INSERT INTO @PL
          SELECT tblledger.NAME,
                 Isnull(openingBalance, 0),
                 tblledger.NAME,
                 S.NAME,
                 -- 'Liabilities'
                 CASE S.ReportType
                   WHEN 1 THEN 'EXPENSE'
                   WHEN 2 THEN 'INCOME'
                   ELSE 'EXPENSE'
                 END,
                 ''
          FROM   tblledger
                 JOIN tblGroupLedger S
                   ON tblledger.GroupID = S.ID
          WHERE  isexpense = 1
                 AND tblledger.DomainID = @DomainID
                 AND tblledger.isdeleted = 0
                 AND tblLedger.ModifiedOn >= @SearchStartDate
                 AND tblLedger.ModifiedOn <= @SearchEndDate
          UNION ALL
          SELECT NAME,
                 Amount - Isnull(PaidAmount, 0),
                 NAME,
                 'Accounts Payable',
                 'Liabilities',
                 --CASE ReportType
                 --  WHEN 1 THEN 'Asset'
                 --  WHEN 2 THEN 'Liabilities'
                 --  ELSE 'Liabilities'
                 --END,
                 ''
          FROM   VW_expense
          WHERE  DomainID = @DomainID
                 AND [Date] >= @SearchStartDate
                 AND [Date] <= @SearchEndDate
          UNION ALL
          SELECT CUSTOMER,
                 AMOUNT,
                 CUSTOMER,
                 'Share Capital',
                 'Liabilities',
                 ''
          FROM   VW_INCOME
          WHERE  Type = 'AccountReceipts'
                 AND DomainID = @DomainID
                 AND [Date] >= @SearchStartDate
                 AND [Date] <= @SearchEndDate
          UNION ALL
          SELECT CUSTOMER,
                 AMOUNT - Isnull(Received, 0),
                 CUSTOMER,
                 'RECEIVABLES',
                 'Asset',
                 ''
          FROM   VW_INCOME
          WHERE  DomainID = @DomainID
                 AND [Date] >= @SearchStartDate
                 AND [Date] <= @SearchEndDate
          UNION ALL
          --SELECT tblledger.NAME,
          --       Isnull(openingBalance, 0),
          --       tblledger.NAME,
          --       s.NAME,
          --       -- 'Asset'
          --       CASE s.ReportType
          --         WHEN 1 THEN 'EXPENSE'
          --         WHEN 2 THEN 'INCOME'
          --         ELSE 'EXPENSE'
          --       END
          --FROM   tblledger
          --       JOIN tblGroupLedger s
          --         ON tblledger.GroupID = s.ID
          --WHERE  isexpense = 0
          --       AND tblledger.DomainID = @DomainID
          --       AND tblledger.isdeleted = 0
          --UNION
		  --- Bank Balance  till Search End Date
          SELECT DISTINCT B.NAME,
                          ( (SELECT DISTINCT Isnull(Sum(BB.Amount), 0)
                             FROM   tblbrs BRS
                                    LEFT JOIN tblBookedBankBalance BB
                                           ON BB.BRSID = BRS.ID
                             WHERE  BRS.isdeleted = 0
                                    AND BB.IsDeleted = 0
                                    AND BRS.DomainID = @DomainID
                                    AND BRS.SourceDate <= @SearchEndDate
                                    AND SourceType IN((SELECT ID
                                                       FROM   tblMasterTypes
                                                       WHERE  BankID = TBRS.BankID
                                                              AND NAME IN ( 'InvoiceReceivable', 'Receive Payment', 'ToBank' )))) - (SELECT DISTINCT Isnull(Sum(BB.Amount), 0)
                                                                                                                                     FROM   tblbrs BRS
                                                                                                                                            LEFT JOIN tblBookedBankBalance BB
                                                                                                                                                   ON BB.BRSID = BRS.ID
                                                                                                                                     WHERE  BRS.isdeleted = 0
                                                                                                                                            AND BRS.DomainID = @DomainID
                                                                                                                                            AND BB.IsDeleted = 0
                                                                                                                                            AND BRS.SourceDate <= @SearchEndDate
                                                                                                                                            AND SourceType IN((SELECT ID
                                                                                                                                                               FROM   tblMasterTypes
                                                                                                                                                               WHERE  BankID = TBRS.BankID
                                                                                                                                                                      AND NAME IN ( 'PayBill', 'FromBank', 'Bill', 'Expense', 'Make Payment' )))) ) + B.OpeningBalance,                        

                          B.NAME,
                          'Bank',
                          'Asset',
                          ''
          FROM   tblBank B
                 LEFT JOIN TblBrs TBRS
                        ON TBRS.bankID = B.ID
          WHERE  B.isdeleted = 0
                 AND B.DomainID = @domainID
		  --- Cash Balance  till Search End Date
          UNION ALL
          SELECT 'Cash',
                 Amount,
                 'CASH',
                 'CASH',
                 'Asset',
                 ''
          FROM   tblVirtualCash
          WHERE  DomainID = @domainID
                 AND IsDeleted = 0
                 --AND ModifiedOn >= @SearchStartDate
                 AND [Date] <= @SearchEndDate
          UNION ALL
		  --- Opening Balance of groupledger till Search End Date
          SELECT G.NAME,
                 CASE
                   WHEN ( A.TransactionType = (SELECT ID
                                               FROM   tblMasterTypes
                                               WHERE  NAME = 'Receive payment') ) THEN A.amount * 1
                   ELSE A.amount * -1
                 END AS ad,
                 A.PartyName,
                 G.NAME,
                 'Liabilities',
                 ''
          --CASE g.ReportType
          --  WHEN 1 THEN 'EXPENSE'
          --  WHEN 2 THEN 'INCOME'
          --  ELSE 'PandL'
          --END
          FROM   tblReceipts A
                 JOIN tblLedger L
                   ON A.LedgerID = L.ID
                 JOIN tblgroupledger G
                   ON L.groupid = G.ID
          WHERE  A.isdeleted = 0
                 AND A.DomainID = @domainID
                 --AND A.[Date] >= @SearchStartDate
                 AND A.[Date] <= @SearchEndDate
          UNION ALL
          --SELECT G.NAME,
          --       CASE
          --         WHEN ( A.TransactionType = (SELECT ID
          --                                     FROM   tblMasterTypes
          --                                     WHERE  NAME = 'Receive payment') ) THEN A.amount * 1
          --         ELSE 0
          --       END AS ad,
          --       B.NAME,
          --       G.NAME,
          --       'Asset',
          --       ''
          ----CASE g.ReportType
          ----  WHEN 1 THEN 'EXPENSE'
          ----  WHEN 2 THEN 'INCOME'
          ----  ELSE 'PandL'
          ----END
          --FROM   tblReceipts A
          --       JOIN tblLedger L
          --         ON A.LedgerID = L.ID
          --       JOIN tblgroupledger G
          --         ON L.groupid = G.ID
          --       JOIN tblBank B
          --         ON A.BankID = B.ID
          --WHERE  A.isdeleted = 0
          --       AND A.DomainID = @domainID
          --       --AND A.[Date] >= @SearchStartDate
          --       AND A.[Date] <= @SearchEndDate
          ------  Opening balance of Account Payables, Account Receivable and Receive & Make payment
          --UNION ALL
          SELECT NAME,
                 Amount - Isnull(PaidAmount, 0),
                 NAME,
                 'Accounts Payable',
                 'Liabilities',
                 --CASE ReportType
                 --  WHEN 1 THEN 'Asset'
                 --  WHEN 2 THEN 'Liabilities'
                 --  ELSE 'Liabilities'
                 --END,
                 ''
          FROM   VW_expense
          WHERE  DomainID = @DomainID
                 AND ( [Date] < @SearchStartDate)
          UNION ALL
          SELECT CUSTOMER,
                 AMOUNT,
                 CUSTOMER,
                 'Share Capital',
                 'Liabilities',
                 ''
          FROM   VW_INCOME
          WHERE  Type = 'AccountReceipts'
                 AND DomainID = @DomainID
                 AND ( [Date] < @SearchStartDate)
          UNION ALL
          SELECT CUSTOMER,
                 AMOUNT - Isnull(Received, 0),
                 CUSTOMER,
                 'RECEIVABLES',
                 'Asset',
                 ''
          FROM   VW_INCOME
          WHERE  DomainID = @DomainID
                 AND ( [Date] < @SearchStartDate)
          --UNION ALL
          --SELECT G.NAME,
          --       CASE
          --         WHEN ( A.TransactionType = (SELECT ID
          --                                     FROM   tblMasterTypes
          --                                     WHERE  NAME = 'Receive payment') ) THEN A.amount * 1
          --         ELSE A.amount * -1
          --       END AS ad,
          --       A.PartyName,
          --       G.NAME,
          --       'Liabilities',
          --       ''
          ----CASE g.ReportType
          ----  WHEN 1 THEN 'EXPENSE'
          ----  WHEN 2 THEN 'INCOME'
          ----  ELSE 'PandL'
          ----END
          --FROM   tblReceipts A
          --       JOIN tblLedger L
          --         ON A.LedgerID = L.ID
          --       JOIN tblgroupledger G
          --         ON L.groupid = G.ID
          --WHERE  A.isdeleted = 0
          --       AND A.DomainID = @domainID
          --       AND ( A.[Date] < @SearchStartDate )
          --UNION ALL
          --SELECT G.NAME,
          --       CASE
          --         WHEN ( A.TransactionType = (SELECT ID
          --                                     FROM   tblMasterTypes
          --                                     WHERE  NAME = 'Receive payment') ) THEN A.amount * 1
          --         ELSE 0
          --       END AS ad,
          --       B.NAME,
          --       G.NAME,
          --       'Asset',
          --       ''
          ----CASE g.ReportType
          ----  WHEN 1 THEN 'EXPENSE'
          ----  WHEN 2 THEN 'INCOME'
          ----  ELSE 'PandL'
          ----END
          --FROM   tblReceipts A
          --       JOIN tblLedger L
          --         ON A.LedgerID = L.ID
          --       JOIN tblgroupledger G
          --         ON L.groupid = G.ID
          --       JOIN tblBank B
          --         ON A.BankID = B.ID
          --WHERE  A.isdeleted = 0
          --       AND A.DomainID = @domainID
          --       AND ( A.[Date] < @SearchStartDate)

          SELECT NAME,
                 Isnull(AMOUNT, 0) AS Amount,
                 LEDGER,
                 GROUPLEDGER,
                 CATEGORY,
                 BranchName
          FROM   @pl
          WHERE  Amount <> 0
          ORDER  BY CASE
                      WHEN CATEGORY = 'income' THEN 0
                      WHEN CATEGORY = 'expense' THEN 1
                      WHEN CATEGORY = 'asset' THEN 2
                      WHEN CATEGORY = 'Liabilities' THEN 3
                      ELSE 4
                    END,
                    NAME ASC
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
