﻿
/****************************************************************************               
CREATED BY    : Ajith N            
CREATED DATE  : 05 Dec 2018            
MODIFIED BY   :             
MODIFIED DATE :             
 <summary>             
  DeleteCompanyAndDependentRecords 99,2      
 </summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteCompanyAndDependentRecords] (@ID     INT,
                                                          @UserID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT VARCHAR(100) = 'Operation Failed!',
                  @Query  VARCHAR(max),
                  @RowNo  INT

          --SET @Query = (SELECT CASE    
          --                       WHEN T.NAME = 'tbl_Company' THEN    
          --                         'IF EXISTS(SELECT 1 FROM sys.columns WHERE Name = N''ID'' AND Object_ID = Object_ID(N'''    
          --                         + T.NAME + ''')) BEGIN delete FROM ' + T.NAME    
          --                         + ' where ID = ' + Cast(@ID AS VARCHAR)    
          --                         + '; END '    
          --                       ELSE    
          --                         'IF EXISTS(SELECT 1 FROM sys.columns WHERE Name = N''DomainID'' AND Object_ID = Object_ID(N'''    
          --                         + T.NAME + ''')) BEGIN delete FROM ' + T.NAME    
          --                         + ' where DomainID = '    
          --                         + Cast(@ID AS VARCHAR) + '; END '    
          --                     END    
          --              FROM   sys.tables T    
          --              WHERE  T.[type] = 'U'    
          --                     AND T.NAME NOT IN ( 'tbl_Attendance_Process', 'tbl_Status', 'tblErrorMessage', 'tbl_CodeMaster',    
          --                                         'tblMasterTypes', 'tbl_Year', 'tbl_Month', 'tbl_EmailProcessor_Backup',    
          --                                         'tbl_EmailProcessor', 'tbl_Pay_PayrollDetails', 'tbl_MultipleFileUpload', 'tbl_BiometricLogs',    
          --                                         'tbl_RBSModule', 'DeploymentActivities', 'tbl_EmployeeCompanyMapping', 'ELMAH_Error',    
          --                                         'tbl_SubModule', 'tbl_Menu', 'tbl_MasterMenuforDomain', 'tbl_Notifications',    
          --                                         'tbl_ConfigurationMaster', 'tblBusinessEventsLog', 'tbl_PayrollComponentConfiguration', 'tblCurrency',    
          --                                         'tbl_ApplicationConfiguration', 'tbl_PayrollComponentMaster', 'tbl_BusinessEventsLog', 'sysdiagrams' )    
          --                     AND T.NAME NOT LIKE 'Auth_%'    
          --                     AND T.NAME NOT LIKE '%_TriggerAudit'    
          --              FOR xml path(''))    
          --PRINT @Query      
          --EXEC (@Query)     
          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Company')
            BEGIN
                DELETE FROM tbl_Company
                WHERE  ID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Company), 0)

                DBCC CHECKIDENT ('tbl_Company', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblInvoiceItem')
            BEGIN
                DELETE FROM tblInvoiceItem
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblInvoiceItem), 0)

                DBCC CHECKIDENT ('tblInvoiceItem', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblPODetails')
            BEGIN
                DELETE FROM tblPODetails
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblPODetails), 0)

                DBCC CHECKIDENT ('tblPODetails', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_PaystructureConfiguration')
            BEGIN
                DELETE FROM tbl_PaystructureConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_PaystructureConfiguration), 0)

                DBCC CHECKIDENT ('tbl_PaystructureConfiguration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeSkills')
            BEGIN
                DELETE FROM tbl_EmployeeSkills
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeSkills), 0)

                DBCC CHECKIDENT ('tbl_EmployeeSkills', RESEED, @RowNo)
            END

          DELETE FROM tbl_EmployeeAppraisalQuestionnaireMapping
          WHERE  DomainID = @ID

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeAppraisal')
            BEGIN
                DELETE FROM tbl_EmployeeAppraisal
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeAppraisal), 0)

                DBCC CHECKIDENT ('tbl_EmployeeAppraisal', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_AppraisalConfiguration')
            BEGIN
                DELETE FROM tbl_AppraisalConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_AppraisalConfiguration), 0)

                DBCC CHECKIDENT ('tbl_AppraisalConfiguration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_AttendanceConfiguration')
            BEGIN
                DELETE FROM tbl_AttendanceConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_AttendanceConfiguration), 0)

                DBCC CHECKIDENT ('tbl_AttendanceConfiguration', RESEED, @RowNo)
            END

          DELETE FROM tbl_FileUpload
          WHERE  DomainID = @ID

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Band')
            BEGIN
                DELETE FROM tbl_Band
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Band), 0)

                DBCC CHECKIDENT ('tbl_Band', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_PTConfiguration')
            BEGIN
                DELETE FROM tbl_PTConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_PTConfiguration), 0)

                DBCC CHECKIDENT ('tbl_PTConfiguration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeStatutoryDetails')
            BEGIN
                DELETE FROM tbl_EmployeeStatutoryDetails
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeStatutoryDetails), 0)

                DBCC CHECKIDENT ('tbl_EmployeeStatutoryDetails', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Reason')
            BEGIN
                DELETE FROM tbl_Reason
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Reason), 0)

                DBCC CHECKIDENT ('tbl_Reason', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_AuditTables')
            BEGIN
                DELETE FROM tbl_AuditTables
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_AuditTables), 0)

                DBCC CHECKIDENT ('tbl_AuditTables', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_RolesResponsibility')
            BEGIN
                DELETE FROM tbl_RolesResponsibility
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_RolesResponsibility), 0)

                DBCC CHECKIDENT ('tbl_RolesResponsibility', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_DocumentType')
            BEGIN
                DELETE FROM tbl_DocumentType
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_DocumentType), 0)

                DBCC CHECKIDENT ('tbl_DocumentType', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_TDS')
            BEGIN
                DELETE FROM tbl_TDS
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_TDS), 0)

                DBCC CHECKIDENT ('tbl_TDS', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblProduct')
            BEGIN
                DELETE FROM tblProduct
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblProduct), 0)

                DBCC CHECKIDENT ('tblProduct', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblVirtualCash')
            BEGIN
                DELETE FROM tblVirtualCash
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblVirtualCash), 0)

                DBCC CHECKIDENT ('tblVirtualCash', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_MasterMenu')
            BEGIN
                DELETE FROM tbl_MasterMenu
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_MasterMenu), 0)

                DBCC CHECKIDENT ('tbl_MasterMenu', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_BloodGroup')
            BEGIN
                DELETE FROM tbl_BloodGroup
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_BloodGroup), 0)

                DBCC CHECKIDENT ('tbl_BloodGroup', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblTransfer')
            BEGIN
                DELETE FROM tblTransfer
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblTransfer), 0)

                DBCC CHECKIDENT ('tblTransfer', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployementType')
            BEGIN
                DELETE FROM tbl_EmployementType
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployementType), 0)

                DBCC CHECKIDENT ('tbl_EmployementType', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmailRecipients')
            BEGIN
                DELETE FROM tbl_EmailRecipients
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmailRecipients), 0)

                DBCC CHECKIDENT ('tbl_EmailRecipients', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_TDSEmployeeOtherIncome')
            BEGIN
                DELETE FROM tbl_TDSEmployeeOtherIncome
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_TDSEmployeeOtherIncome), 0)

                DBCC CHECKIDENT ('tbl_TDSEmployeeOtherIncome', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeApproval')
            BEGIN
                DELETE FROM tbl_EmployeeApproval
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeApproval), 0)

                DBCC CHECKIDENT ('tbl_EmployeeApproval', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblPurchaseOrder')
            BEGIN
                DELETE FROM tblPurchaseOrder
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblPurchaseOrder), 0)

                DBCC CHECKIDENT ('tblPurchaseOrder', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_UOM')
            BEGIN
                DELETE FROM tbl_UOM
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_UOM), 0)

                DBCC CHECKIDENT ('tbl_UOM', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_TDSLimitConfiguration')
            BEGIN
                DELETE FROM tbl_TDSLimitConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_TDSLimitConfiguration), 0)

                DBCC CHECKIDENT ('tbl_TDSLimitConfiguration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_BusinessHours')
            BEGIN
                DELETE FROM tbl_BusinessHours
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_BusinessHours), 0)

                DBCC CHECKIDENT ('tbl_BusinessHours', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmploymentDetails')
            BEGIN
                DELETE FROM tbl_EmploymentDetails
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmploymentDetails), 0)

                DBCC CHECKIDENT ('tbl_EmploymentDetails', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmploymentDetails')
            BEGIN
                DELETE FROM tbl_Pay_EmployeePayrollDetails
                WHERE  PayrollId IN (SELECT id
                                     FROM   tbl_Pay_EmployeePayroll
                                     WHERE  DomainID = @ID)

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmploymentDetails), 0)

                DBCC CHECKIDENT ('tbl_EmploymentDetails', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Pay_EmployeePayroll')
            BEGIN
                DELETE FROM tbl_Pay_EmployeePayroll
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Pay_EmployeePayroll), 0)

                DBCC CHECKIDENT ('tbl_Pay_EmployeePayroll', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ExitReason')
            BEGIN
                DELETE FROM tbl_ExitReason
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ExitReason), 0)

                DBCC CHECKIDENT ('tbl_ExitReason', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblVendor')
            BEGIN
                DELETE FROM tblVendor
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblVendor), 0)

                DBCC CHECKIDENT ('tblVendor', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_City')
            BEGIN
                DELETE FROM tbl_City
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_City), 0)

                DBCC CHECKIDENT ('tbl_City', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_State')
            BEGIN
                DELETE FROM tbl_State
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_State), 0)

                DBCC CHECKIDENT ('tbl_State', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Country')
            BEGIN
                DELETE FROM tbl_Country
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Country), 0)

                DBCC CHECKIDENT ('tbl_Country', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Payment')
            BEGIN
                DELETE FROM tbl_Payment
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Payment), 0)

                DBCC CHECKIDENT ('tbl_Payment', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblLedger')
            BEGIN
                DELETE FROM tblLedger
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblLedger), 0)

                DBCC CHECKIDENT ('tblLedger', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblGroupLedger')
            BEGIN
                DELETE FROM tblGroupLedger
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblGroupLedger), 0)

                DBCC CHECKIDENT ('tblGroupLedger', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_PaymentMapping')
            BEGIN
                DELETE FROM tbl_PaymentMapping
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_PaymentMapping), 0)

                DBCC CHECKIDENT ('tbl_PaymentMapping', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeAvailedLeave')
            BEGIN
                DELETE FROM tbl_EmployeeAvailedLeave
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeAvailedLeave), 0)

                DBCC CHECKIDENT ('tbl_EmployeeAvailedLeave', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Receipts')
            BEGIN
                DELETE FROM tbl_Receipts
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Receipts), 0)

                DBCC CHECKIDENT ('tbl_Receipts', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_BusinessNature')
            BEGIN
                DELETE FROM tbl_BusinessNature
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_BusinessNature), 0)

                DBCC CHECKIDENT ('tbl_BusinessNature', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Pay_PayrollCompanyPayStructure')
            BEGIN
                DELETE FROM tbl_Pay_PayrollCompanyPayStructure
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Pay_PayrollCompanyPayStructure), 0)

                DBCC CHECKIDENT ('tbl_Pay_PayrollCompanyPayStructure', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'TDSComponent')
            BEGIN
                DELETE FROM TDSComponent
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   TDSComponent), 0)

                DBCC CHECKIDENT ('TDSComponent', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ReceiptsMapping')
            BEGIN
                DELETE FROM tbl_ReceiptsMapping
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ReceiptsMapping), 0)

                DBCC CHECKIDENT ('tbl_ReceiptsMapping', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_InventoryStockAdjustment')
            BEGIN
                DELETE FROM tbl_InventoryStockAdjustment
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_InventoryStockAdjustment), 0)

                DBCC CHECKIDENT ('tbl_InventoryStockAdjustment', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblCashBucket')
            BEGIN
                DELETE FROM tblCashBucket
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblCashBucket), 0)

                DBCC CHECKIDENT ('tblCashBucket', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_BusinessUnit')
            BEGIN
                DELETE FROM tbl_BusinessUnit
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_BusinessUnit), 0)

                DBCC CHECKIDENT ('tbl_BusinessUnit', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblBRS')
            BEGIN
                DELETE FROM tblBRS
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblBRS), 0)

                DBCC CHECKIDENT ('tblBRS', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Functional')
            BEGIN
                DELETE FROM tbl_Functional
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Functional), 0)

                DBCC CHECKIDENT ('tbl_Functional', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_RBSSubModule')
            BEGIN
                DELETE FROM tbl_RBSSubModule
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_RBSSubModule), 0)

                DBCC CHECKIDENT ('tbl_RBSSubModule', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblBookedBankBalance')
            BEGIN
                DELETE FROM tblBookedBankBalance
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblBookedBankBalance), 0)

                DBCC CHECKIDENT ('tblBookedBankBalance', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblProduct_V2')
            BEGIN
                DELETE FROM tblProduct_V2
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblProduct_V2), 0)

                DBCC CHECKIDENT ('tblProduct_V2', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblExpenseHoldings')
            BEGIN
                DELETE FROM tblExpenseHoldings
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblExpenseHoldings), 0)

                DBCC CHECKIDENT ('tblExpenseHoldings', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblBank')
            BEGIN
                DELETE FROM tblBank
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblBank), 0)

                DBCC CHECKIDENT ('tblBank', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Pay_EmployeePayStructure')
            BEGIN
                DELETE FROM tbl_Pay_EmployeePayStructure
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Pay_EmployeePayStructure), 0)

                DBCC CHECKIDENT ('tbl_Pay_EmployeePayStructure', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Pay_CompanyPayStructure')
            BEGIN
                DELETE FROM tbl_Pay_CompanyPayStructure
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Pay_CompanyPayStructure), 0)

                DBCC CHECKIDENT ('tbl_Pay_CompanyPayStructure', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'TDSConfiguration')
            BEGIN
                DELETE FROM TDSConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   TDSConfiguration), 0)

                DBCC CHECKIDENT ('TDSConfiguration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeCompOff')
            BEGIN
                DELETE FROM tbl_EmployeeCompOff
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeCompOff), 0)

                DBCC CHECKIDENT ('tbl_EmployeeCompOff', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblApplicationConfiguration')
            BEGIN
                DELETE FROM tblApplicationConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblApplicationConfiguration), 0)

                DBCC CHECKIDENT ('tblApplicationConfiguration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_InventoryStockAdjustmentItems')
            BEGIN
                DELETE FROM tbl_InventoryStockAdjustmentItems
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_InventoryStockAdjustmentItems), 0)

                DBCC CHECKIDENT ('tbl_InventoryStockAdjustmentItems', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ClaimCategory')
            BEGIN
                DELETE FROM tbl_ClaimCategory
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ClaimCategory), 0)

                DBCC CHECKIDENT ('tbl_ClaimCategory', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Holidays')
            BEGIN
                DELETE FROM tbl_Holidays
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Holidays), 0)

                DBCC CHECKIDENT ('tbl_Holidays', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblInvoiceHoldings')
            BEGIN
                DELETE FROM tblInvoiceHoldings
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblInvoiceHoldings), 0)

                DBCC CHECKIDENT ('tblInvoiceHoldings', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_AppraisalQuestion')
            BEGIN
                DELETE FROM tbl_AppraisalQuestion
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_AppraisalQuestion), 0)

                DBCC CHECKIDENT ('tbl_AppraisalQuestion', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Customer')
            BEGIN
                DELETE FROM tbl_Customer
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Customer), 0)

                DBCC CHECKIDENT ('tbl_Customer', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'TDSDeclaration')
            BEGIN
                DELETE FROM TDSDeclaration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   TDSDeclaration), 0)

                DBCC CHECKIDENT ('TDSDeclaration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblDepartment')
            BEGIN
                DELETE FROM tblDepartment
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblDepartment), 0)

                DBCC CHECKIDENT ('tblDepartment', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeDependentDetails')
            BEGIN
                DELETE FROM tbl_EmployeeDependentDetails
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeDependentDetails), 0)

                DBCC CHECKIDENT ('tbl_EmployeeDependentDetails', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_CustomerContact')
            BEGIN
                DELETE FROM tbl_CustomerContact
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_CustomerContact), 0)

                DBCC CHECKIDENT ('tbl_CustomerContact', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblCustomer')
            BEGIN
                DELETE FROM tblCustomer
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblCustomer), 0)

                DBCC CHECKIDENT ('tblCustomer', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_CustomerDepartment')
            BEGIN
                DELETE FROM tbl_CustomerDepartment
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_CustomerDepartment), 0)

                DBCC CHECKIDENT ('tbl_CustomerDepartment', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ClaimPolicy')
            BEGIN
                DELETE FROM tbl_ClaimPolicy
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ClaimPolicy), 0)

                DBCC CHECKIDENT ('tbl_ClaimPolicy', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LeavePolicy')
            BEGIN
                DELETE FROM tbl_LeavePolicy
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LeavePolicy), 0)

                DBCC CHECKIDENT ('tbl_LeavePolicy', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_CustomerDesignation')
            BEGIN
                DELETE FROM tbl_CustomerDesignation
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_CustomerDesignation), 0)

                DBCC CHECKIDENT ('tbl_CustomerDesignation', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ClaimsApprove')
            BEGIN
                DELETE FROM tbl_ClaimsApprove
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ClaimsApprove), 0)

                DBCC CHECKIDENT ('tbl_ClaimsApprove', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ClaimsApproverConfiguration')
            BEGIN
                DELETE FROM tbl_ClaimsApproverConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ClaimsApproverConfiguration), 0)

                DBCC CHECKIDENT ('tbl_ClaimsApproverConfiguration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'TDSHouseTax')
            BEGIN
                DELETE FROM TDSHouseTax
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   TDSHouseTax), 0)

                DBCC CHECKIDENT ('TDSHouseTax', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblSOItem')
            BEGIN
                DELETE FROM tblSOItem
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblSOItem), 0)

                DBCC CHECKIDENT ('tblSOItem', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblCostCenter')
            BEGIN
                DELETE FROM tblCostCenter
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblCostCenter), 0)

                DBCC CHECKIDENT ('tblCostCenter', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'TDSSection')
            BEGIN
                DELETE FROM TDSSection
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   TDSSection), 0)

                DBCC CHECKIDENT ('TDSSection', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeDesignationMapping')
            BEGIN
                DELETE FROM tbl_EmployeeDesignationMapping
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeDesignationMapping), 0)

                DBCC CHECKIDENT ('tbl_EmployeeDesignationMapping', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblExpenseDetails')
            BEGIN
                DELETE FROM tblExpenseDetails
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblExpenseDetails), 0)

                DBCC CHECKIDENT ('tblExpenseDetails', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LeaveTypes')
            BEGIN
                DELETE FROM tbl_LeaveTypes
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LeaveTypes), 0)

                DBCC CHECKIDENT ('tbl_LeaveTypes', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ClaimsFinancialApprove')
            BEGIN
                DELETE FROM tbl_ClaimsFinancialApprove
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ClaimsFinancialApprove), 0)

                DBCC CHECKIDENT ('tbl_ClaimsFinancialApprove', RESEED, @RowNo)
            END

          DELETE FROM tbl_SubordinatesDetails
          WHERE  DomainID = @ID

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeDocuments')
            BEGIN
                DELETE FROM tbl_EmployeeDocuments
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeDocuments), 0)

                DBCC CHECKIDENT ('tbl_EmployeeDocuments', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Pay_EmployeePayStructureDetails')
            BEGIN
                DELETE FROM tbl_Pay_EmployeePayStructureDetails
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Pay_EmployeePayStructureDetails), 0)

                DBCC CHECKIDENT ('tbl_Pay_EmployeePayStructureDetails', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_RBSMenu')
            BEGIN
                DELETE FROM tbl_RBSMenu
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_RBSMenu), 0)

                DBCC CHECKIDENT ('tbl_RBSMenu', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LoanAmortization')
            BEGIN
                DELETE FROM tbl_LoanAmortization
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LoanAmortization), 0)

                DBCC CHECKIDENT ('tbl_LoanAmortization', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LoanAmortization')
            BEGIN
                DELETE FROM tbl_EmployeeGrade
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LoanAmortization), 0)

                DBCC CHECKIDENT ('tbl_LoanAmortization', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LoanConfiguration')
            BEGIN
                DELETE FROM tbl_LoanConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LoanConfiguration), 0)

                DBCC CHECKIDENT ('tbl_LoanConfiguration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblJournal')
            BEGIN
                DELETE FROM tblJournal
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblJournal), 0)

                DBCC CHECKIDENT ('tblJournal', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ClaimsRequest')
            BEGIN
                DELETE FROM tbl_ClaimsRequest
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ClaimsRequest), 0)

                DBCC CHECKIDENT ('tbl_ClaimsRequest', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LoanForeclosure')
            BEGIN
                DELETE FROM tbl_LoanForeclosure
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LoanForeclosure), 0)

                DBCC CHECKIDENT ('tbl_LoanForeclosure', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeTravelApproval')
            BEGIN
                DELETE FROM tbl_EmployeeTravelApproval
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeTravelApproval), 0)

                DBCC CHECKIDENT ('tbl_EmployeeTravelApproval', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeTravelRequest')
            BEGIN
                DELETE FROM tbl_EmployeeTravelRequest
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeTravelRequest), 0)

                DBCC CHECKIDENT ('tbl_EmployeeTravelRequest', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblSalesOrder')
            BEGIN
                DELETE FROM tblSalesOrder
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblSalesOrder), 0)

                DBCC CHECKIDENT ('tblSalesOrder', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblDesignation')
            BEGIN
                DELETE FROM tblDesignation
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblDesignation), 0)

                DBCC CHECKIDENT ('tblDesignation', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LoanRequest')
            BEGIN
                DELETE FROM tbl_LoanRequest
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LoanRequest), 0)

                DBCC CHECKIDENT ('tbl_LoanRequest', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LoanSettle')
            BEGIN
                DELETE FROM tbl_LoanSettle
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LoanSettle), 0)

                DBCC CHECKIDENT ('tbl_LoanSettle', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeLeave')
            BEGIN
                DELETE FROM tbl_EmployeeLeave
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeLeave), 0)

                DBCC CHECKIDENT ('tbl_EmployeeLeave', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LopDetails')
            BEGIN
                DELETE FROM tbl_LopDetails
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LopDetails), 0)

                DBCC CHECKIDENT ('tbl_LopDetails', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ClaimsSettle')
            BEGIN
                DELETE FROM tbl_ClaimsSettle
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ClaimsSettle), 0)

                DBCC CHECKIDENT ('tbl_ClaimsSettle', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_RBSUserMenuMapping')
            BEGIN
                DELETE FROM tbl_RBSUserMenuMapping
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_RBSUserMenuMapping), 0)

                DBCC CHECKIDENT ('tbl_RBSUserMenuMapping', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_PasswordReset')
            BEGIN
                DELETE FROM tbl_PasswordReset
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_PasswordReset), 0)

                DBCC CHECKIDENT ('tbl_PasswordReset', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_PasswordReset')
            BEGIN
                DELETE FROM tbl_EmployeeOtherDetails
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_PasswordReset), 0)

                DBCC CHECKIDENT ('tbl_PasswordReset', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Login')
            BEGIN
                DELETE FROM tbl_Login
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Login), 0)

                DBCC CHECKIDENT ('tbl_Login', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LoginForms')
            BEGIN
                DELETE FROM tbl_LoginForms
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LoginForms), 0)

                DBCC CHECKIDENT ('tbl_LoginForms', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LoginHistory')
            BEGIN
                DELETE FROM tbl_LoginHistory
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LoginHistory), 0)

                DBCC CHECKIDENT ('tbl_LoginHistory', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeMaster')
            BEGIN
                DELETE FROM tbl_EmployeeMaster
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeMaster), 0)

                DBCC CHECKIDENT ('tbl_EmployeeMaster', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Skills')
            BEGIN
                DELETE FROM tbl_Skills
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Skills), 0)

                DBCC CHECKIDENT ('tbl_Skills', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_LopDetails_Process')
            BEGIN
                DELETE FROM tbl_LopDetails_Process
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_LopDetails_Process), 0)

                DBCC CHECKIDENT ('tbl_LopDetails_Process', RESEED, @RowNo)
            END

          DELETE FROM tbl_EmployeeLeaveDateMapping
          WHERE  DomainID = @ID

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblBudget')
            BEGIN
                DELETE FROM tblBudget
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblBudget), 0)

                DBCC CHECKIDENT ('tblBudget', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_CompanyAssets')
            BEGIN
                DELETE FROM tbl_CompanyAssets
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_CompanyAssets), 0)

                DBCC CHECKIDENT ('tbl_CompanyAssets', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_NewsAndEvents')
            BEGIN
                DELETE FROM tbl_NewsAndEvents
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_NewsAndEvents), 0)

                DBCC CHECKIDENT ('tbl_NewsAndEvents', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeeMissedPunches')
            BEGIN
                DELETE FROM tbl_EmployeeMissedPunches
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeeMissedPunches), 0)

                DBCC CHECKIDENT ('tbl_EmployeeMissedPunches', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_CompanyPayStructure')
            BEGIN
                DELETE FROM tbl_CompanyPayStructure
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_CompanyPayStructure), 0)

                DBCC CHECKIDENT ('tbl_CompanyPayStructure', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_MailerEventKeys')
            BEGIN
                DELETE FROM tbl_MailerEventKeys
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_MailerEventKeys), 0)

                DBCC CHECKIDENT ('tbl_MailerEventKeys', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblInvoicePaymentMapping')
            BEGIN
                DELETE FROM tblInvoicePaymentMapping
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblInvoicePaymentMapping), 0)

                DBCC CHECKIDENT ('tblInvoicePaymentMapping', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'HRA')
            BEGIN
                DELETE FROM HRA
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   HRA), 0)

                DBCC CHECKIDENT ('HRA', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_OffBoardHistory')
            BEGIN
                DELETE FROM tbl_OffBoardHistory
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_OffBoardHistory), 0)

                DBCC CHECKIDENT ('tbl_OffBoardHistory', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblEmailConfig')
            BEGIN
                DELETE FROM tblEmailConfig
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblEmailConfig), 0)

                DBCC CHECKIDENT ('tblEmailConfig', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_OffBoardProcess')
            BEGIN
                DELETE FROM tbl_OffBoardProcess
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_OffBoardProcess), 0)

                DBCC CHECKIDENT ('tbl_OffBoardProcess', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Questionnaire')
            BEGIN
                DELETE FROM tbl_Questionnaire
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Questionnaire), 0)

                DBCC CHECKIDENT ('tbl_Questionnaire', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_InventoryReason')
            BEGIN
                DELETE FROM tbl_InventoryReason
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_InventoryReason), 0)

                DBCC CHECKIDENT ('tbl_InventoryReason', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeePayroll')
            BEGIN
                DELETE FROM tbl_EmployeePayroll
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeePayroll), 0)

                DBCC CHECKIDENT ('tbl_EmployeePayroll', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_OrganisationChart')
            BEGIN
                DELETE FROM tbl_OrganisationChart
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_OrganisationChart), 0)

                DBCC CHECKIDENT ('tbl_OrganisationChart', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Contract')
            BEGIN
                DELETE FROM tbl_Contract
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Contract), 0)

                DBCC CHECKIDENT ('tbl_Contract', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_PasswordAudit')
            BEGIN
                DELETE FROM tbl_PasswordAudit
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_PasswordAudit), 0)

                DBCC CHECKIDENT ('tbl_PasswordAudit', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Department')
            BEGIN
                DELETE FROM tbl_Department
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Department), 0)

                DBCC CHECKIDENT ('tbl_Department', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblExpensePaymentMapping')
            BEGIN
                DELETE FROM tblExpensePaymentMapping
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblExpensePaymentMapping), 0)

                DBCC CHECKIDENT ('tblExpensePaymentMapping', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblExpense')
            BEGIN
                DELETE FROM tblExpense
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblExpense), 0)

                DBCC CHECKIDENT ('tblExpense', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_AssetType')
            BEGIN
                DELETE FROM tbl_AssetType
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_AssetType), 0)

                DBCC CHECKIDENT ('tbl_AssetType', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Pay_PayrollCompontents')
            BEGIN
                DELETE FROM tbl_Pay_PayrollCompontents
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Pay_PayrollCompontents), 0)

                DBCC CHECKIDENT ('tbl_Pay_PayrollCompontents', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeePayStructure')
            BEGIN
                DELETE FROM tbl_EmployeePayStructure
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeePayStructure), 0)

                DBCC CHECKIDENT ('tbl_EmployeePayStructure', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_ApplicationRole')
            BEGIN
                DELETE FROM tbl_ApplicationRole
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_ApplicationRole), 0)

                DBCC CHECKIDENT ('tbl_ApplicationRole', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_InventoryStockDistribution')
            BEGIN
                DELETE FROM tbl_InventoryStockDistribution
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_InventoryStockDistribution), 0)

                DBCC CHECKIDENT ('tbl_InventoryStockDistribution', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Designation')
            BEGIN
                DELETE FROM tbl_Designation
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Designation), 0)

                DBCC CHECKIDENT ('tbl_Designation', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Attendance')
            BEGIN
                DELETE FROM tbl_Attendance
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Attendance), 0)

                DBCC CHECKIDENT ('tbl_Attendance', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_PayrollConfiguration')
            BEGIN
                DELETE FROM tbl_PayrollConfiguration
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_PayrollConfiguration), 0)

                DBCC CHECKIDENT ('tbl_PayrollConfiguration', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_FinancialYear')
            BEGIN
                DELETE FROM tbl_FinancialYear
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_FinancialYear), 0)

                DBCC CHECKIDENT ('tbl_FinancialYear', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_EmployeePersonalDetails')
            BEGIN
                DELETE FROM tbl_EmployeePersonalDetails
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_EmployeePersonalDetails), 0)

                DBCC CHECKIDENT ('tbl_EmployeePersonalDetails', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblGstHSN')
            BEGIN
                DELETE FROM tblGstHSN
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblGstHSN), 0)

                DBCC CHECKIDENT ('tblGstHSN', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblSearchHistory')
            BEGIN
                DELETE FROM tblSearchHistory
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblSearchHistory), 0)

                DBCC CHECKIDENT ('tblSearchHistory', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblExpensePayment')
            BEGIN
                DELETE FROM tblExpensePayment
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblExpensePayment), 0)

                DBCC CHECKIDENT ('tblExpensePayment', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_Inventory')
            BEGIN
                DELETE FROM tbl_Inventory
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_Inventory), 0)

                DBCC CHECKIDENT ('tbl_Inventory', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblInvoicePayment')
            BEGIN
                DELETE FROM tblInvoicePayment
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblInvoicePayment), 0)

                DBCC CHECKIDENT ('tblInvoicePayment', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tblInvoice')
            BEGIN
                DELETE FROM tblInvoice
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tblInvoice), 0)

                DBCC CHECKIDENT ('tblInvoice', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_DestinationCities')
            BEGIN
                DELETE FROM tbl_DestinationCities
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_DestinationCities), 0)

                DBCC CHECKIDENT ('tbl_DestinationCities', RESEED, @RowNo)
            END

          IF EXISTS (SELECT 1
                     FROM   INFORMATION_SCHEMA.TABLES
                     WHERE  TABLE_NAME = N'tbl_TDSComponentMapping')
            BEGIN
                DELETE FROM tbl_TDSComponentMapping
                WHERE  DomainID = @ID

                SET @RowNo = Isnull((SELECT Max(ID)
                                     FROM   tbl_TDSComponentMapping), 0)

                DBCC CHECKIDENT ('tbl_TDSComponentMapping', RESEED, @RowNo)
            END

          SET @Output ='Deleted Successfully.'

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 
