﻿/****************************************************************************     
CREATED BY   :     
CREATED DATE  :     
MODIFIED BY   : DhanaLakshmi. S    
MODIFIED DATE  : 30 June 2017    
 <summary>      
       ManageEmployePaystructure 917,291,10,'27-Nov-17',1,1,0,0,0,0,0,1,1,null,null,null,null,null,1,1,0    
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageEmployePaystructure] (@ID                 INT,
                                                   @EmployeeID         INT,
                                                   @CompanyPayStubId   INT,
                                                   @EffictiveFrom      DATE,
                                                   @Gross              MONEY,
                                                   @Bonus              MONEY,
                                                   @Mediclaim          MONEY,
                                                   @PLI                MONEY,
                                                   @MobileDataCard     MONEY,
                                                   @OtherPerks         MONEY,
                                                   @CTC                MONEY,
                                                   @IsPFRequired       BIT,
                                                   @IsESIRequired      BIT,
                                                   @MedicalAllowance   MONEY,
                                                   @Conveyance         MONEY,
                                                   @EducationAllowance MONEY,
                                                   @PaperMagazine      MONEY,
                                                   @MonsoonAllow       MONEY,
                                                   @LoginId            INT,
                                                   @DomainID           INT,
                                                   @IsDeleted          BIT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(1000)
      DECLARE @SplAllowance DECIMAL(18, 2)

      SET @SplAllowance = (SELECT( @Gross - ( @Gross / 100 * Basic + ( @Gross / 100 * HRA ) + ( CASE
                                                                                                  WHEN( Isnull(@MedicalAllowance, NULL) IS NOT NULL ) THEN ( @MedicalAllowance )
                                                                                                  ELSE MedicalAllowance
                                                                                                END ) + ( CASE
                                                                                                            WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                                            ELSE Isnull(Conveyance, 0)
                                                                                                          END ) + ( CASE
                                                                                                                      WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( @EducationAllowance )
                                                                                                                      ELSE Isnull(EducationAllow, 0)
                                                                                                                    END ) + ( CASE
                                                                                                                                WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( @PaperMagazine )
                                                                                                                                ELSE Isnull(MagazineAllow, 0)
                                                                                                                              END ) ) )
                           FROM   tbl_CompanyPayStructure
                           WHERE  id = @CompanyPayStubId)

      BEGIN TRY
          BEGIN TRANSACTION

          BEGIN
              IF @IsDeleted = 1
                BEGIN
                    IF EXISTS(SELECT 1
                              FROM   tbl_EmployeePayroll
                              WHERE  EmployeeId = @Employeeid
                                     AND EmployeePayStructureID = @ID
                                     AND isdeleted = 0
                                     AND DomainId = @DomainID)
                      BEGIN
                          SET @Output = 'Payroll has been processed using the Paystructure. So you cannot Delete'
                      END
                    ELSE
                      BEGIN
                          UPDATE tbl_EmployeePayStructure
                          SET    IsDeleted = 1
                          WHERE  Id = @ID

                          SET @Output='Deleted Successfully.'
                      END
                END
              ELSE
                BEGIN
                    IF( @ID = 0 )
                      BEGIN
                          IF NOT EXISTS(SELECT 1
                                        FROM   tbl_EmployeePayStructure
                                        WHERE  EmployeeId = @Employeeid
                                               AND CompanyPayStubId = @CompanyPayStubId
                                               AND isdeleted = 0
                                               AND DomainId = @DomainID
                                               --AND Gross = @Gross    
                                               AND EffectiveFrom = @EffictiveFrom)
                            BEGIN
                                INSERT INTO tbl_EmployeePayStructure
                                            (CompanyPayStubId,
                                             EmployeeId,
                                             Gross,
                                             Basic,
                                             HRA,
                                             MedicalAllowance,
                                             Conveyance,
                                             SplAllow,
                                             EducationAllow,
                                             MagazineAllow,
                                             MonsoonAllow,
                                             EEPF,
                                             EEESI,
                                             PT,
                                             TDS,
                                             ERPF,
                                             ERESI,
                                             Bonus,
                                             Gratuity,
                                             PLI,
                                             Mediclaim,
                                             MobileDataCard,
                                             OtherPerks,
                                             CTC,
                                             EffectiveFrom,
                                             IsPFRequired,
                                             IsESIRequired,
                                             DomainId,
                                             CreatedBy,
                                             CreatedOn,
                                             ModifiedBy,
                                             ModifiedOn)
                                SELECT @CompanyPayStubId,
                                       @EmployeeID,
                                       @Gross,
                                       @Gross / 100 * Basic                    AS [Basic],
                                       @Gross / 100 * HRA                      AS HRA,
                                       CASE
                                         WHEN( Isnull(@MedicalAllowance, NULL) IS NOT NULL ) THEN ( @MedicalAllowance )
                                         ELSE MedicalAllowance
                                       END                                     AS MedicalAllowance,
                                       ( CASE
                                           WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                           ELSE Isnull(Conveyance, 0)
                                         END )                                 AS Conveyance,
                                       @SplAllowance                           AS SplAllow,
                                       CASE
                                         WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( @EducationAllowance )
                                         ELSE EducationAllow
                                       END                                     AS EducationAllowance,
                                       CASE
                                         WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( @PaperMagazine )
                                         ELSE MagazineAllow
                                       END                                     AS PaperMagazine,
                                       CASE
                                         WHEN( Isnull(@MonsoonAllow, NULL) IS NOT NULL ) THEN ( @MonsoonAllow )
                                         ELSE MonsoonAllow
                                       END                                     AS MonsoonAllow,
                                       CASE
                                         WHEN ( @IsPFRequired = 0 ) THEN 0
                                         ELSE
                                           CASE
                                             WHEN( ( @Gross / 100 * Basic + ( CASE
                                                                                WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                ELSE Isnull(Conveyance, 0)
                                                                              END ) + @SplAllowance + ( CASE
                                                                                                          WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( Isnull(@EducationAllowance, 0) )
                                                                                                          ELSE Isnull(EducationAllow, 0)
                                                                                                        END ) + ( CASE
                                                                                                                    WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( Isnull(@PaperMagazine, 0) )
                                                                                                                    ELSE Isnull(MagazineAllow, 0)
                                                                                                                  END ) ) > ( PAYEPF.Limit ) ) THEN ( PAYEPF.Value )
                                             ELSE ( @Gross / 100 * com.Basic + ( CASE
                                                                                   WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                   ELSE Isnull(Conveyance, 0)
                                                                                 END ) + @SplAllowance + ( CASE
                                                                                                             WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( Isnull(@EducationAllowance, 0) )
                                                                                                             ELSE Isnull(EducationAllow, 0)
                                                                                                           END ) + ( CASE
                                                                                                                       WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( Isnull(@PaperMagazine, 0) )
                                                                                                                       ELSE Isnull(MagazineAllow, 0)
                                                                                                                     END ) ) * ( PAYEPF.Percentage ) / 100
                                           END
                                       END                                     AS EEPF,
                                       CASE
                                         WHEN ( @IsESIRequired = 0 ) THEN 0
                                         ELSE
                                           CASE
                                             WHEN ( @Gross <= ( PAYEESI.Limit ) ) THEN @Gross / 100 * ( PAYEESI.Percentage )
                                             ELSE 0
                                           END
                                       END                                     EESI,
                                       0                                       AS PT,
                                       0                                       AS TDS,
                                       CASE
                                         WHEN ( @IsPFRequired = 0 ) THEN 0
                                         ELSE
                                           CASE
                                             WHEN ( ( @Gross / 100 * com.Basic + ( CASE
                                                                                     WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                     ELSE Isnull(Conveyance, 0)
                                                                                   END ) + @SplAllowance + ( CASE
                                                                                                               WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( Isnull(@EducationAllowance, 0) )
                                                                                                               ELSE EducationAllow
                                                                                                             END ) + ( CASE
                                                                                                                         WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( Isnull(@PaperMagazine, 0) )
                                                                                                                         ELSE MagazineAllow
                                                                                                                       END ) ) > ( PAYERPF.Limit ) ) THEN ( PAYERPF.Value )
                                             ELSE ( @Gross / 100 * com.Basic + ( CASE
                                                                                   WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                   ELSE Isnull(Conveyance, 0)
                                                                                 END ) + @SplAllowance + ( CASE
                                                                                                             WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( Isnull(@EducationAllowance, 0) )
                                                                                                             ELSE Isnull(EducationAllow, 0)
                                                                                                           END ) + ( CASE
                                                                                                                       WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( Isnull(@PaperMagazine, 0) )
                                                                                                                       ELSE Isnull(MagazineAllow, 0)
                                                                                                                     END ) ) * ( PAYERPF.Percentage ) / 100
                                           END
                                       END                                     AS ERPF,
                                       CASE
                                         WHEN ( @IsESIRequired = 0 ) THEN 0
                                         ELSE
                                           CASE
                                             WHEN ( @Gross <= ( PAYERESI.Limit ) ) THEN @Gross / 100 * ( PAYERESI.Percentage )
                                             ELSE 0
                                           END
                                       END                                     AS ERESI,
                                       @Bonus                                  AS Bonus,
                                       ( @Gross / 100 * Basic ) * 15 / 26 / 12 AS Gratuity,
                                       @PLI                                    AS PLI,
                                       @Mediclaim                              AS MEDICLAIM,
                                       @MobileDataCard                         AS MobileDataCard,
                                       @OtherPerks                             AS OtherPerks,
                                       @CTC,
                                       @EffictiveFrom,
                                       @IsPFRequired,
                                       @IsESIRequired,
                                       @DomainID,
                                       @LoginId,
                                       Getdate(),
                                       @LoginId,
                                       Getdate()
                                FROM   tbl_CompanyPayStructure com
                                       LEFT JOIN tbl_EmployeeMaster EMP
                                              ON EMP.ID = @EmployeeID
                                       LEFT JOIN tbl_PayrollConfiguration PAYEPF
                                              ON PAYEPF.Components = 'EmployeePF'
                                                 AND PAYEPF.BusinessUnitID = EMP.RegionID
                                       LEFT JOIN tbl_PayrollConfiguration PAYERPF
                                              ON PAYERPF.Components = 'EmployerPF'
                                                 AND PAYERPF.BusinessUnitID = EMP.RegionID
                                       LEFT JOIN tbl_PayrollConfiguration PAYEESI
                                              ON PAYEESI.Components = 'EmployeeESI'
                                                 AND PAYEESI.BusinessUnitID = EMP.RegionID
                                       LEFT JOIN tbl_PayrollConfiguration PAYERESI
                                              ON PAYERESI.Components = 'EmployerESI'
                                                 AND PAYERESI.BusinessUnitID = EMP.RegionID
                                WHERE  com.id = @CompanyPayStubId

                                SET @Output='Inserted Successfully.'
                            END
                          ELSE
                            BEGIN
                                SET @Output='The selected Paystructure is already mapped to this Employee with same Effective Date.'
                            END
                      END
                    ELSE
                      BEGIN
                          IF NOT EXISTS(SELECT 1
                                        FROM   tbl_EmployeePayroll
                                        WHERE  EmployeeId = @Employeeid
                                               AND EmployeePayStructureID = @ID
                                               AND isdeleted = 0
                                               AND DomainId = @DomainID)
                            BEGIN
                                IF NOT EXISTS(SELECT 1
                                              FROM   tbl_EmployeePayStructure
                                              WHERE  EmployeeId = @Employeeid
                                                     AND CompanyPayStubId = @CompanyPayStubId
                                                     AND isdeleted = 0
                                                     AND DomainId = @DomainID
                                                     AND Id <> @ID
                                                     --AND Gross = @Gross    
                                                     AND EffectiveFrom = @EffictiveFrom)
                                  BEGIN
                                      UPDATE tbl_EmployeePayStructure
                                      SET    CompanyPayStubId = @CompanyPayStubId,
                                             EmployeeId = @EmployeeID,
                                             Gross = @Gross,
                                             Basic = @Gross / 100 * CPS.Basic,
                                             HRA = @Gross / 100 * CPS.HRA,
                                             MedicalAllowance = CASE
                                                                  WHEN( @MedicalAllowance >= 0 ) THEN ( @MedicalAllowance )
                                                                  ELSE CPS.MedicalAllowance
                                                                END,
                                             Conveyance = ( CASE
                                                              WHEN( @Conveyance >= 0 ) THEN ( @Conveyance )
                                                              ELSE Isnull(CPS.Conveyance, 0)
                                                            END ),
                                             SplAllow = @Gross - ( @Gross / 100 * CPS.Basic + ( @Gross / 100 * CPS.HRA ) + ( CASE
                                                                                                                               WHEN( Isnull(@MedicalAllowance, NULL) IS NOT NULL ) THEN ( @MedicalAllowance )
                                                                                                                               ELSE CPS.MedicalAllowance
                                                                                                                             END ) + ( CASE
                                                                                                                                         WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                                                                         ELSE Isnull(CPS.Conveyance, 0)
                                                                                                                                       END ) + ( CASE
                                                                                                                                                   WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( @EducationAllowance )
                                                                                                                                                   ELSE Isnull(CPS.EducationAllow, 0)
                                                                                                                                                 END ) + ( CASE
                                                                                                                                                             WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( @PaperMagazine )
                                                                                                                                                             ELSE Isnull(CPS.MagazineAllow, 0)
                                                                                                                                                           END ) ),
                                             EducationAllow = CASE
                                                                WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( @EducationAllowance )
                                                                ELSE Isnull(CPS.EducationAllow, 0)
                                                              END,
                                             MagazineAllow = CASE
                                                               WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( @PaperMagazine )
                                                               ELSE Isnull(CPS.MagazineAllow, 0)
                                                             END,
                                             MonsoonAllow = CASE
                                                              WHEN( Isnull(@MonsoonAllow, NULL) IS NOT NULL ) THEN ( @MonsoonAllow )
                                                              ELSE Isnull(CPS.MonsoonAllow, 0)
                                                            END,
                                             EEPF = CASE
                                                      WHEN ( @IsPFRequired = 0 ) THEN 0
                                                      ELSE
                                                        CASE
                                                          WHEN( ( @Gross / 100 * CPS.Basic + ( CASE
                                                                                                 WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                                 ELSE Isnull(CPS.Conveyance, 0)
                                                                                               END ) + @SplAllowance + ( CASE
                                                                                                                           WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( @EducationAllowance )
                                                                                                                           ELSE Isnull(ep.EducationAllow, 0)
                                                                                                                         END ) + ( CASE
                                                                                                                                     WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( @PaperMagazine )
                                                                                                                                     ELSE Isnull(ep.MagazineAllow, 0)
                                                                                                                                   END ) ) > ( PAYEPF.Limit ) ) THEN ( PAYEPF.Value )
                                                          ELSE ( @Gross / 100 * CPS.Basic + ( CASE
                                                                                                WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                                ELSE Isnull(CPS.Conveyance, 0)
                                                                                              END ) + @SplAllowance + ( CASE
                                                                                                                          WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( @EducationAllowance )
                                                                                                                          ELSE Isnull(ep.EducationAllow, 0)
                                                                                                                        END ) + ( CASE
                                                                                                                                    WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( @PaperMagazine )
                                                                                                                                    ELSE Isnull(ep.MagazineAllow, 0)
                                                                                                                                  END ) ) * ( PAYEPF.Percentage ) / 100
                                                        END
                                                    END,
                                             EEESI = CASE
                                                       WHEN ( @IsESIRequired = 0 ) THEN 0
                                                       ELSE
                                                         CASE
                                                           WHEN ( @Gross <= ( PAYEESI.Limit ) ) THEN @Gross / 100 * ( PAYEESI.Percentage )
                                                           ELSE 0
                                                         END
                                                     END,
                                             PT = 0,
                                             TDS = 0,
                                             ERPF = CASE
                                                      WHEN ( @IsPFRequired = 0 ) THEN 0
                                                      ELSE
                                                        CASE
                                                          WHEN ( ( @Gross / 100 * CPS.Basic + ( CASE
                                                                                                  WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                                  ELSE Isnull(CPS.Conveyance, 0)
                                                                                                END ) + @SplAllowance + ( CASE
                                                                                                                            WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( @EducationAllowance )
                                                                                                                            ELSE Isnull(ep.EducationAllow, 0)
                                                                                                                          END ) + ( CASE
                                                                                                                                      WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( @PaperMagazine )
                                                                                                                                      ELSE Isnull(ep.MagazineAllow, 0)
                                                                                                                                    END ) ) > ( PAYERPF.Limit ) ) THEN ( PAYERPF.Value )
                                                          ELSE ( @Gross / 100 * CPS.Basic + ( CASE
                                                                                                WHEN( Isnull(@Conveyance, NULL) IS NOT NULL ) THEN ( @Conveyance )
                                                                                                ELSE Isnull(CPS.Conveyance, 0)
                                                                                              END ) + @SplAllowance + ( CASE
                                                                                                                          WHEN( Isnull(@EducationAllowance, NULL) IS NOT NULL ) THEN ( @EducationAllowance )
                                                                                                                          ELSE Isnull(ep.EducationAllow, 0)
                                                                                                                        END ) + ( CASE
                                                                                                                                    WHEN( Isnull(@PaperMagazine, NULL) IS NOT NULL ) THEN ( @PaperMagazine )
                                                                                                                                    ELSE Isnull(ep.MagazineAllow, 0)
                                                                                                                                  END ) ) * ( PAYERPF.Percentage ) / 100
                                                        END
                                                    END,
                                             ERESI = CASE
                                                       WHEN ( @IsESIRequired = 0 ) THEN 0
                                                       ELSE
                                                         CASE
                                                           WHEN ( @Gross <= ( PAYERESI.Limit ) ) THEN @Gross / 100 * ( PAYERESI.Percentage )
                                                           ELSE 0
                                                         END
                                                     END,
                                             Bonus = @Bonus,
                                             Gratuity = ( @Gross / 100 * CPS.Basic ) * 15 / 26 / 12,
                                             PLI = @PLI,
                                             Mediclaim = @Mediclaim,
                                             MobileDataCard = @MobileDataCard,
                                             OtherPerks = @OtherPerks,
                                             CTC = @CTC,
                                             EffectiveFrom = @EffictiveFrom,
                                             IsPFRequired = @IsPFRequired,
                                             IsESIRequired = @IsESIRequired,
                                             ModifiedBy = @LoginId,
                                             ModifiedOn = Getdate()
                                      FROM   tbl_EmployeePayStructure EP
                                             LEFT JOIN tbl_CompanyPayStructure CPS
                                                    ON CPS.Id = EP.CompanyPayStubId
                                             LEFT JOIN tbl_EmployeeMaster EMP
                                                    ON EMP.ID = @EmployeeID
                                             LEFT JOIN tbl_PayrollConfiguration PAYEPF
                                                    ON PAYEPF.Components = 'EmployeePF'
                                                       AND PAYEPF.BusinessUnitID = EMP.RegionID
                                             LEFT JOIN tbl_PayrollConfiguration PAYERPF
                                                    ON PAYERPF.Components = 'EmployerPF'
                                                       AND PAYERPF.BusinessUnitID = EMP.RegionID
                                             LEFT JOIN tbl_PayrollConfiguration PAYEESI
                                                    ON PAYEESI.Components = 'EmployeeESI'
                                                       AND PAYEESI.BusinessUnitID = EMP.RegionID
                                             LEFT JOIN tbl_PayrollConfiguration PAYERESI
                                                    ON PAYERESI.Components = 'EmployerESI'
                                                       AND PAYERESI.BusinessUnitID = EMP.RegionID
                                      WHERE  EP.id = @ID

                                      SET @Output='Updated Successfully.'
                                  END
                                ELSE
                                  BEGIN
                                      SET @Output = 'The selected Paystructure is already mapped to this Employee with same Effective Date.'
                                  END
                            END
                          ELSE
                            BEGIN
                                SET @Output = 'Payroll has been processed using the Paystructure. So you cannot Update'
                            END
                      END
                END

              SELECT @Output
          END

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
