﻿/****************************************************************************   
CREATED BY   : K.SASIREKHA  
CREATED DATE  : 29-09-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
  [ManageCompOffRequest] 2,'Madhavaram','Madhavaram','as',1,1,0,1  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchCompOffRequest] (@DomainID   INT,  
                                              @EmployeeID INT,  
                                              @StatusID   INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          SELECT ECO.ID                          AS ID,  
                 ECO.Date                        AS Date,  
                 ATT.Duration                    AS Duration,  
                 ECO.RequesterRemarks            AS Remarks,  
                 ISNULL(EMA.EmpCodePattern, '') + '' + EMA.Code + ' - ' + EMA.FullName AS ApprovarName,  
                 ECO.ApprovedDate                AS ApprovedDate,  
                 ISNULL(EMH.EmpCodePattern, '') + '' +EMH.Code + ' - ' + EMH.FullName AS HRName,  
                 ECO.HRApprovedDate              AS HRApprovedDate,  
                 STA.Code                        AS Status  
          FROM   tbl_EmployeeCompOff ECO  
                 LEFT JOIN tbl_Attendance ATT  
                        ON ECO.CreatedBy = ATT.EmployeeID  
                           AND ECO.Date = ATT.LogDate  
                 LEFT JOIN tbl_Status STA  
                        ON STA.ID = ECO.StatusID  
                 LEFT JOIN tbl_EmployeeMaster EMA  
                        ON EMA.ID = ECO.ApproverID  
                 LEFT JOIN tbl_EmployeeMaster EMH  
                        ON EMH.ID = ECO.HRApproverID  
          WHERE  ECO.CreatedBy = @EmployeeID  
                 AND ECO.Isdeleted = 0  
                 AND ECO.DomainID = @DomainID  
                 AND ( ISNULL(@StatusID, 0) = 0  
                        OR eco.StatusID = @StatusID )  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
