﻿/****************************************************************************             
CREATED BY  :             
CREATED DATE :       
MODIFIED BY  :           
MODIFIED DATE :       
 <summary>          
 [SearchRPTParty] 1,1      
 </summary>                                     
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[SearchRPTParty] (@DomainID INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT C.NAME                     AS Customer,      
                 C.ID                       AS CustomerID,      
                 (SELECT Sum(QTY * Rate)      
                  FROM   tblInvoiceItem      
                  WHERE  InvoiceID = i.ID      
                         AND IsDeleted = 0 and DomainID =  @domainID) AS Booked,'Income' As [Type]      
          FROM   tblInvoice i      
                 JOIN tblCustomer c      
                   ON i.CustomerID = c.ID      
          WHERE   i.IsDeleted = 0 and i.DomainID =  @domainID      
             
  Union ALL SELECT      
          vd.NAME                                                                    AS VendorName,      
          ex.VendorID                                                                AS VendorID,      
          ( (SELECT ( Isnull(Sum(Amount*Qty), 0) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0)    
)      
             FROM   tblExpenseDetails      
             WHERE  ExpenseID = ex.ID      
                    AND IsDeleted = 0 and DomainID =  @domainID)      
             ) AS BilledAmount,'Expense'      
          FROM   tblExpense ex      
                 JOIN tblVendor vd      
                   ON vd.ID = ex.VendorID      
                 JOIN tbl_CodeMaster cd      
                   ON cd.ID = ex.StatusID      
          WHERE  ex.IsDeleted = 0 and ex.DomainID =  @domainID      
                      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
