﻿/**************************************************************************** 
CREATED BY			:	Ajith N
CREATED DATE		:	12 Sep 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
    [CheckHolidays] '2017-09-24' ,1,224
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CheckHolidays] (@Date     DATE,
                                       @DomainID INT,
                                       @UserID   INT)
AS
  BEGIN
      SET NOCOUNT ON

      DECLARE @RegionIDs VARCHAR(20)

      SET @RegionIDs = (SELECT CAST(RegionID AS VARCHAR(10))
                        FROM   tbl_EmployeeMaster EM
                        WHERE  EM.ID = @UserID
                        FOR XML path(''))

      BEGIN TRY
          SELECT COUNT(1) AS [Count]
          FROM   tbl_Holidays HD
          WHERE  HD.DomainID = @DomainID
                 AND HD.IsDeleted = 0
                 AND HD.[Date] = @Date
                 AND HD.RegionID LIKE '%,' + CAST(@RegionIDs AS VARCHAR(20)) + ',%'

      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
