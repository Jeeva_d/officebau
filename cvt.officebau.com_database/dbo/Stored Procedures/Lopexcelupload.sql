﻿/**************************************************************************** 
CREATED BY    		:	Naneeshwar.M	
CREATED DATE		:	13-JULY-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
		[Lopexcelupload] '224',7,2017,21,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Lopexcelupload] (@EmployeeCode VARCHAR(20),
                                        @EmployeeName VARCHAR(50),
                                        @MonthID      INT,
                                        @Year         INT,
                                        @LOPDays      DECIMAL(18, 2),
                                        @SessionID    INT,
                                        @DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output     VARCHAR(100) ='Employee Code  or Excel Uploaded is Invalid.',
              @EmployeeID INT,
              @DOJ        DATE,
              @ActualDays DECIMAL
      DECLARE @CustomDate DATETIME = Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @year - 2000, Dateadd(MONTH, @monthID - 1, '20000101')))
                                  + 1, 0))
      DECLARE @Workdays INT = Datediff(day, @CustomDate, Dateadd(month, 1, @CustomDate))

      SET @EmployeeID=(SELECT ID
                       FROM   tbl_EmployeeMaster
                       WHERE  Code = @EmployeeCode
                              AND Replace(Rtrim(Ltrim(FullName)), ' ', '') = Rtrim(Ltrim(@EmployeeName))
                              AND IsDeleted = 0
                              AND Isnull(IsActive, 0) = 0
                              AND DomainID = @DomainID)
      SET @DOJ =(SELECT DOJ
                 FROM   tbl_EmployeeMaster
                 WHERE  ID = @EmployeeID)
      SET @ActualDays=( CASE
                          WHEN ( Datepart(MM, @DOJ) = @MonthId
                                 AND Datepart(YYYY, @DOJ) = @year ) THEN
                            ( @Workdays - ( Datepart(D, @DOJ) - 1 ) )
                          ELSE
                            @Workdays
                        END )

      BEGIN TRY
          BEGIN TRANSACTION

          IF ( @EmployeeID <> '' )
            BEGIN
                IF EXISTS(SELECT 1
                          FROM   tbl_EmployeeMaster
                          WHERE  id = @EmployeeID)
                  BEGIN
                      IF NOT EXISTS (SELECT 1
                                     FROM   tbl_LopDetails
                                     WHERE  EmployeeId = @EmployeeID
                                            AND DomainId = @DomainID
                                            AND MonthID = @MonthId
                                            AND Year = @Year)
                        BEGIN
                            IF( @ActualDays >= @LOPDays )
                              BEGIN
                                  INSERT INTO tbl_LopDetails
                                              (EmployeeId,
                                               MonthID,
                                               Year,
                                               LopDays,
                                               CreatedBy,
                                               CreatedOn,
                                               ModifiedBy,
                                               ModifiedOn,
                                               DomainId,
                                               IsManual)
                                  VALUES      (@EmployeeID,
                                               @MonthId,
                                               @Year,
                                               @LOPDays,
                                               @SessionID,
                                               Getdate(),
                                               @SessionID,
                                               Getdate(),
                                               @DomainID,
                                               1)

                                  SET @Output ='Inserted Successfully !'
                              END
                        END
                      ELSE
                        BEGIN
                            IF EXISTS(SELECT 1
                                      FROM   tbl_pay_EmployeePayroll
                                      WHERE  MonthId = @MonthId
                                             AND EmployeeID = @EmployeeID
                                             AND YearId = @Year
                                             AND IsDeleted = 0
                                             AND ( (SELECT LopDays
                                                    FROM   tbl_LopDetails
                                                    WHERE  MonthId = @MonthID
                                                           AND Year = @Year
                                                           AND EmployeeId = @EmployeeID
                                                           AND IsDeleted = 0) <> @LOPDays ))
                              BEGIN
                                  UPDATE tbl_pay_EmployeePayroll
                                  SET    isdeleted = 1
                                  WHERE  EmployeeID = @EmployeeID
                                         AND Monthid = @MonthId
                                         AND YearId = @Year
                                         AND DomainId = @DomainID
                                         AND IsProcessed = 0
                              END

                            IF ( ISNULL((SELECT 1
                                         FROM   tbl_pay_EmployeePayroll
                                         WHERE  IsProcessed = 1
                                                AND MonthId = @MonthID
                                                AND YearId = @Year
                                                AND DomainId = @DomainID
                                                AND EmployeeId = @EmployeeID
                                                AND IsDeleted = 0), 0) <> 1 )
                              UPDATE tbl_LopDetails
                              SET    LopDays = @LOPDays,
                                     ModifiedBy = @SessionID,
                                     ModifiedOn = Getdate()
                              WHERE  EmployeeId = @EmployeeID
                                     AND MonthId = @MonthId
                                     AND Year = @Year
                                     AND DomainId = @DomainID
                                     AND ( @ActualDays >= @LOPDays )

                            SET @Output ='Updated Successfully !'
                        END
                  END
                ELSE
                  SET @Output ='Employee Code  or Excel Uploaded is Invalid.'
            END

          COMMIT TRANSACTION

          SELECT @Output AS [Output]
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
