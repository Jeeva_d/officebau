﻿/****************************************************************************       
CREATED BY    :     
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE :     
 <summary>     
  [Temp_GetPANDL] 2,'04/01/2016' ,'03/31/2017'   
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Temp_getpandl] (@DomainID        INT,
                                       @SearchStartDate VARCHAR(25),
                                       @SearchEndDate   VARCHAR(25),
									   @CostCenterID INT = 0)
AS
  BEGIN
      BEGIN TRY
          SET @SearchStartDate = Cast(@SearchStartDate AS DATE)
          SET @SearchEndDate = Cast(@SearchEndDate AS DATE)

          ------- P & l --------------    
          DECLARE @PL TABLE
            (
               ID          INT IDENTITY(1, 1),
               GROUPLEDGER VARCHAR(100),
               LEDGER      VARCHAR(100),
               AMOUNT      MONEY,
               NAME        VARCHAR(100),
               CATEGORY    VARCHAR(100),
               [Date]      DATE
            )

          INSERT INTO @PL
          SELECT e.Groups GL,
                 e.Ledger AS Ledger,
                 Isnull(Amount, 0),
                 ''       AS Sub,
                 --'Expense',
                 CASE e.ReportType
                   --WHEN 1 THEN 'Expense'
                   WHEN 2 THEN 'Expense'
                   ELSE 'Expense'
                 END,
                 e.[Date]
          FROM   Vw_newexpense e
          WHERE  DomainID = @DomainID
                 --AND e.reporttype IN(0)
                 AND Amount <> 0
				 AND (@CostCenterID = 0 OR CostCenterID = @CostCenterID)
          UNION ALL
          SELECT cdm.Code,
                 pdt.NAME,
                 Amount,
                 '',
                 'Income',
                 inc.[Date]
          FROM   vw_income inc
                 JOIN dbo.tblProduct pdt
                   ON pdt.ID = inc.ProductID
                      AND pdt.IsDeleted = 0
                 JOIN dbo.tbl_CodeMaster cdm
                   ON cdm.ID = pdt.TypeID
          WHERE  inc.DomainID = @DomainID
				AND (@CostCenterID = 0 OR inc.RevenueCenterID = @CostCenterID)
		  UNION ALL
          SELECT gl.NAME GL,
                 l.NAME  AS Ledger,
                 Isnull(l.OpeningBalance, 0),
                 ''      AS Sub,
                 -- 'Assets',
                 CASE gl.ReportType
                   WHEN 0 THEN 'Expense'
                   ELSE ''
                 END,
                 l.ModifiedOn
          FROM   tblgroupledger gl
                 LEFT JOIN tblLedger l
                        ON l.GroupID = gl.ID
                 LEFT JOIN Vw_newexpense e
                        ON l.ID = e.LedgerID
          WHERE  gl.Isdeleted = 0
                 AND gl.DomainID = @DomainID 
				 AND (@CostCenterID = 0 OR e.CostCenterID = @CostCenterID)               

          SELECT *
          FROM   @PL
          WHERE  AMOUNT <> 0
				 AND [Date] >= @SearchStartDate
                 AND [Date] <= @SearchEndDate
          ORDER  BY LEDGER ASC
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
