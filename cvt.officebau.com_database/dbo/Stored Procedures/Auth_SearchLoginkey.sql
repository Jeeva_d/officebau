﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	29-12-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
    Activate user from Key
	--  SELECT * FROM Auth_Users
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_SearchLoginkey]
 (
	@RandomKey		varchar(150)	
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY					
		SET NOCOUNT ON;
		DECLARE @Output			VARCHAR(100)	
		BEGIN TRANSACTION
		IF EXISTS ( SELECT 1 FROM Auth_Users WHERE RandomKey = @RandomKey AND IsDeleted = 0 AND IsActive = 1)
			BEGIN
				SET @Output = 'Your Account has been already Activated.Please SignIn...'
			END	
		ELSE IF EXISTS ( SELECT 1 FROM Auth_Users WHERE RandomKey = @RandomKey AND IsDeleted = 0 )
			BEGIN
				UPDATE Auth_Users SET IsActive = 1 WHERE RandomKey = @RandomKey
				SET @Output = 'OK'
			END
		ELSE
			BEGIN
				SET @Output = 'Sorry! Invalid Key.Please contact our Administrator...'
			END	
		SELECT @Output
		COMMIT TRANSACTION      
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
