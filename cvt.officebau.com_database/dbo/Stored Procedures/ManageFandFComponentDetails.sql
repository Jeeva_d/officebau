﻿/****************************************************************************       
CREATED BY   : Ajith N      
CREATED DATE  :  11 Sep 2017     
MODIFIED BY   :       
MODIFIED DATE  :       
<summary>              
	
</summary>                               
*****************************************************************************/
CREATE PROCEDURE [dbo].[ManageFandFComponentDetails] (@ID                        INT,
                                                     @TotalDaysInMonth          INT,
                                                     @LeaveEncashedDays         INT,
                                                     @FandFComponentDetailsList [FANDFCOMPONENTDETAILSLIST] readonly,
                                                     @UserID                    INT,
                                                     @DomainID                  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT VARCHAR(100) = 'Operation Failed!'

          DELETE FROM tbl_FandFDetails
          WHERE  FAndFid = @ID

          ---- Insert FandFDetails
          INSERT INTO tbl_FandFDetails
                      (FAndFid,
                       Type,
                       Description,
                       Amount,
                       IsEditableAmount,
                       Remarks,
                       IsEditableRemarks,
                       Ordinal,
                       IsDeleted,
                       CreatedOn,
                       CreatedBy,
                       ModifiedOn,
                       ModifiedBy,
                       HistoryID,
                       DomainID)
          SELECT @ID,
                 Type,
                 Description,
                 ISNULL(Amount, 0),
                 IsEditableAmount,
                 Remarks,
                 IsEditableRemarks,
                 Ordinal,
                 IsDeleted,
                 Getdate(),
                 @UserID,
                 Getdate(),
                 @UserID,
                 Newid(),
                 @DomainID
          FROM   @FandFComponentDetailsList
          WHERE  IsDeleted = 0

          --Update Leave Encashment
          UPDATE tFsndF
          SET    tFsndF.Amount = (SELECT ( ISNULL(amount, 0) / @TotalDaysInMonth ) * @LeaveEncashedDays
                                  FROM   tbl_FandFDetails
                                  WHERE  [Type] = 'CTC Salary Details'
                                         AND [Description] = 'Basic'
                                         AND FAndFid = @ID)
          FROM   tbl_FandFDetails tFsndF
          WHERE  tFsndF.[Type] = 'Full and Final Payment Details'
                 AND tFsndF.[Description] = 'Leave Encashment'
                 AND tFsndF.FAndFid = @ID

          ---- Sum Of CTC Salary Details
          UPDATE tFsndF
          SET    amount = (SELECT Sum(ISNULL(amount, 0))
                           FROM   tbl_FandFDetails
                           WHERE  [Type] = 'CTC Salary Details'
                                  AND [Description] != 'Gross Earnings'
                                  AND FAndFid = @ID)
          FROM   tbl_FandFDetails tFsndF
          WHERE  tFsndF.[Type] = 'CTC Salary Details'
                 AND tFsndF.[Description] = 'Gross Earnings'
                 AND tFsndF.FAndFid = @ID

          ---- Sum Of Full and Final Payment Details
          UPDATE tFsndF
          SET    tFsndF.amount = (SELECT Sum(ISNULL(amount, 0))
                                  FROM   tbl_FandFDetails
                                  WHERE  [Type] = 'Full and Final Payment Details'
                                         AND [Description] != 'Total FnF Settlement'
                                         AND FAndFid = @ID)
          FROM   tbl_FandFDetails tFsndF
          WHERE  tFsndF.[Type] = 'Full and Final Payment Details'
                 AND tFsndF.[Description] = 'Total FnF Settlement'
                 AND tFsndF.FAndFid = @ID

          ---- Sum Of Deductions
          UPDATE tFsndF
          SET    tFsndF.amount = (SELECT Sum(ISNULL(amount, 0))
                                  FROM   tbl_FandFDetails
                                  WHERE  [Type] = 'Deductions'
                                         AND [Description] != 'Gross Deduction'
                                         AND FAndFid = @ID)
          FROM   tbl_FandFDetails tFsndF
          WHERE  tFsndF.[Type] = 'Deductions'
                 AND tFsndF.[Description] = 'Gross Deduction'
                 AND tFsndF.FAndFid = @ID

          ---- Sum Of Full and Final Settlement
          UPDATE tFsndF
          SET    tFsndF.amount = ( (SELECT Sum(CASE
                                                 WHEN [Description] = 'Gross Earnings' THEN
                                                   Cast(Remarks AS MONEY)
                                                 ELSE
                                                   ISNULL(amount, 0)
                                               END)
                                    FROM   tbl_FandFDetails
                                    WHERE  [Description] IN ( 'Gross Earnings', 'Total FnF Settlement' )
                                           AND FAndFid = @ID) - (SELECT Sum(ISNULL(amount, 0))
                                                                 FROM   tbl_FandFDetails
                                                                 WHERE  [Description] IN ( 'Gross Deduction' )
                                                                        AND FAndFid = @ID) )
          FROM   tbl_FandFDetails tFsndF
          WHERE  tFsndF.[Type] = 'Full and Final Settlement'
                 AND tFsndF.[Description] = 'Net Amount Payable'
                 AND tFsndF.FAndFid = @ID

          ---- Update FandF Net Amount Payable
          UPDATE tbl_FandF
          SET    NetAmountPayable = (SELECT Amount
                                     FROM   tbl_FandFDetails
                                     WHERE  [Type] = 'Full and Final Settlement'
                                            AND [Description] = 'Net Amount Payable'
                                            AND FAndFid = @ID)
          WHERE  ID = @ID

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
