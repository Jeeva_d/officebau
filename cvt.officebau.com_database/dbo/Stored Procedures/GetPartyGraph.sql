﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
<summary>        
	[GetPartyGraph] '2016-10-2','2017-10-2',1,2
</summary>                         
 *****************************************************************************/ 
 CREATE PROCEDURE [dbo].[GetPartyGraph]
 (
	 @PartyID		 int,
	 @ScreenType     VarChar(100),
	 @StartDate		 DATETIME,
     @EndDate		 DATETIME
	,@DomainID		 INT						
 )
 AS
 BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
		;WITH Expense AS
		(
			SELECT
				(LEFT(DATENAME(MONTH,Date),3)) +' '+
				CAST(DATEPART(YEAR,Date) AS VARCHAR(50))		AS ExpenseMonthYear	
				,SUM(Amount)									AS ExpenseAmount
				,SUM(ISNULL(PaidAmount,0))							AS ExpenseReceivedAmount
		FROM Vw_Expense Where VendorID=@PartyID
		GROUP BY (LEFT(DATENAME(MONTH,Date),3)) +' '+ CAST(DATEPART(YEAR,Date) AS VARCHAR(50))
		),
		MonthYear AS
		(
		
				SELECT  LEFT(DATENAME(MONTH, DATEADD(MONTH, x.number, @StartDate)),3)+ ' '+ CONVERT(VARCHAR(10),DATEPART(YEAR,DATEADD(MONTH, x.number, @StartDate))) AS MonthYear
				FROM    master.dbo.spt_values x
				WHERE   x.type = 'P'        
				AND     x.number <= DATEDIFF(MONTH, @StartDate, @EndDate)
		)
		,Income AS 
		(
		SELECT
				(LEFT(DATENAME(MONTH,Date),3)) +' '+
				CAST(DATEPART(YEAR,Date) AS VARCHAR(50))		AS MonthYear	
				,SUM(Amount)									AS Amount
				,SUM(ISNULL(Received,0))							AS ReceivedAmount
		FROM Vw_Income Where CustomerID=@PartyID
		GROUP BY (LEFT(DATENAME(MONTH,Date),3)) +' '+ CAST(DATEPART(YEAR,Date) AS VARCHAR(50))
		)
		SELECT 
		 tem.MonthYear														AS MonthYear	
			 ,ISNULL(expe.ExpenseAmount,0)   								AS ExpenseAmount	
			 ,ISNULL(expe.ExpenseReceivedAmount,0)							AS ExpenseReceivedAmount
	,ISNULL(inc.Amount,0)   												AS Amount	
			 ,ISNULL(inc.ReceivedAmount,0)									AS ReceivedAmount
		FROM MonthYear tem
		LEFT JOIN Expense  expe  ON tem.MonthYear = expe.ExpenseMonthYear 
		LEFT JOIN Income  inc  ON tem.MonthYear = inc.MonthYear 
		
				
	END TRY
	BEGIN CATCH        
			DECLARE @ErrorMsg VARCHAR(100),@ErrSeverity TINYINT        
			SELECT	@ErrorMsg = ERROR_MESSAGE(),@ErrSeverity = ERROR_SEVERITY()        
			RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH  
 END










