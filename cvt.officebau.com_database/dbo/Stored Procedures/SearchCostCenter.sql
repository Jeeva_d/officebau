﻿
/****************************************************************************   
CREATED BY   : Dhanalakshmi  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [SearchGroupLedger] 1,1 
 [SearchCostCenter] 2
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchCostCenter] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT Cast(ISNULL(Value,0) AS INT) AS Value
          FROM   tblApplicationConfiguration
          WHERE  DomainID = @DomainID
                 AND IsDeleted = 0
                 AND Code = 'COSTCT'

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 



