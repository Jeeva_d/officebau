﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :           
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>        
 </summary>                                   
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[Searchjournaldetaillist] (@AutoJournelNo VARCHAR(100),        
                                                 @DomainID      INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          SELECT j.ID              AS ID,        
                 j.LedgerProductID AS LedgerProductID,        
                 j.Debit           AS Debit,        
                 j.Credit          AS Credit,        
                 j.Description     AS Description,        
            j.type AS Type
          FROM   tblJournal j        
          WHERE  j.IsDeleted = 0        
                 AND @AutoJournelNo = j.AutoJournalNo        
     and @DomainID=j.DomainID         
        
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
