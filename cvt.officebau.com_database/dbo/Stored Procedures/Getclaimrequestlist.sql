﻿/****************************************************************************       
CREATED BY   :  Naneeshwar.M    
CREATED DATE  :       
MODIFIED BY   :   Ajith N    
MODIFIED DATE  :   07 Dec 2017    
 <summary>     
         [Getclaimrequestlist] 1,3,2,'11/7/2018 12:00:00 AM',5  
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Getclaimrequestlist] (@DomainID      INT,  
                                             @ApproverID    INT,  
                                             @RequesterID   INT,  
                                             @SubmittedDate DATE,  
                                             @StatusID      INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          IF( @StatusID = (SELECT ID  
                           FROM   tbl_Status  
                           WHERE  Code IN( 'Submitted' )  
                                  AND Type = 'Claims'  
                                  AND IsDeleted = 0) )  
            BEGIN  
                SET @StatusID = 0  
            END  
  
          SELECT CLR.ID                        AS RequesterItemID,  
                 CLA.ID                        AS ID,  
                 CLR.ClaimDate                 AS ClaimedDate,  
                 CLC.NAME                      AS CategoryName,  
                 CLR.Amount                    AS Amount,  
                 Isnull(CLA.ApprovedAmount, 0) AS ApprovalAmount,  
                 CLR.CreatedBy                 AS RequesterID,  
                 STA.Code                      AS [Status],  
                 FU.[FileName]                AS [FileName],  
                 EM.Fullname                   AS ModifiedBy,  
                 CLA.ModifiedOn                AS ModifiedOn,  
                 BU.NAME                       AS BaseLocation,  
                 CLR.DestinationID             AS DestinationID,  
                 CLR.CategoryID                AS CategoryID,  
                 DC.NAME                       AS DestinationCity,  
                 ( CASE  
                     WHEN Isnull(DC.IsMetro, 0) = 1 THEN CP.MetroAmount  
                     ELSE CP.NonMetroAmount  
                   END )                       AS TotalAmount,
                   CLR.FileUpload AS FileUploadID,
                   TR.ID AS TravelReqID,
                   CLR.TravelReqNo 
          FROM   tbl_ClaimsRequest CLR  
                 LEFT JOIN tbl_ClaimsApprove CLA  
                        ON CLA.RequesterID = CLR.CreatedBy  
                           AND CLR.ID = CLA.ClaimItemID  
                 LEFT JOIN tbl_ClaimCategory CLC  
                        ON CLC.ID = CLR.CategoryID  
                 LEFT JOIN tbl_Status STA  
                        ON STA.ID = CLR.StatusID  
                 LEFT JOIN tbl_FileUpload FU  
                        ON FU.Id = CLR.FileUpload  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = CLA.ModifiedBy  
                 LEFT JOIN tbl_BusinessUnit BU  
                        ON EM.BaseLocationID = BU.ID  
                 LEFT JOIN tbl_DestinationCities DC  
                        ON DC.ID = CLR.DestinationID  
                 LEFT JOIN tbl_EmployeeMaster EMR  
                        ON EMR.ID = CLR.ModifiedBy  
                 LEFT JOIN tbl_EmployeeDesignationMapping DM  
                        ON dm.DesignationID = EMR.DesignationID  
                           AND DM.IsDeleted = 0  
                 LEFT JOIN tbl_ClaimPolicy CP  
                        ON CP.DesignationMappingID = DM.ID  
                           AND CP.ExpenseType = CLR.CategoryID  
                           AND CP.IsDeleted = 0  
                  LEFT JOIN tbl_EmployeeTravelRequest TR 
                        ON TR.TravelReqNo = CLR.TravelReqNo  
                           AND TR.IsDeleted =0
          WHERE  CLR.IsDeleted = 0  
                 AND CLR.DomainID = @DomainID  
                 AND CLR.ApproverID = @ApproverID  
                 AND CLR.CreatedBy = @RequesterID  
                 AND CLR.SubmittedDate = @SubmittedDate  
                 AND ( ( Isnull(@StatusID, 0) = 0  
                         AND CLR.StatusID = (SELECT ID  
                                             FROM   tbl_Status  
                                             WHERE  Code IN( 'Submitted' )  
                                                    AND Type = 'Claims'  
                                                    AND IsDeleted = 0) )  
                        OR CLA.StatusID = @StatusID )  
          ORDER  BY CLA.ModifiedOn DESC  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
