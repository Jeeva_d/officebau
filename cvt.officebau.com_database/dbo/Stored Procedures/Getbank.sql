﻿/****************************************************************************     
CREATED BY   : Naneeshwar    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [Getbank] 1,1,1  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getbank] (@ID       INT,
                                 @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT bk.ID          AS ID,
                 NAME           AS BankName,
                 AccountNo      AS AccountNo,
                 AccountTypeID  AS AccountTypeID,
                 OpeningBalance AS OpeningBalance,
                 OpeningBalDate AS OpeningBalDate,
                 Limit          AS Limit,
                 DisplayName    AS DisplayName,
                 [Description]  AS [Description],
                 BranchName     AS BranchName,
                 IFSCCode       AS IFSCCode,
                 SWIFTCode      AS SWIFTCode,
                 MICRCode       AS MICRCode,
                 bk.ModifiedOn  AS ModifiedOn,
                 EMP.FullName  AS ModifiedBy
          FROM   tblBank bk
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = bk.ModifiedBy
          WHERE  bk.IsDeleted = 0
                 AND bk.ID = @ID
                 AND bk.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
