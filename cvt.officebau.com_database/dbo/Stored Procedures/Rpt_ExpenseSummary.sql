﻿  
/****************************************************************************   
CREATED BY   : Jeeva  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
    Rpt_ExpenseSummary null,'2016-12-09','2016-12-01',null,null,null  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Rpt_ExpenseSummary] (@Days      INT,  
                                             @ToDate    DATE,  
                                             @FromDate  DATE,  
                                             @VendorID  VARCHAR(100),  
                                             @CreatedBy VARCHAR(100),  
                                             @Type      VARCHAR(100))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
   IF( Isnull(@ToDate, '') = '' )  
            SET @ToDate=Getutcdate()  
          SELECT NAME,  
                 BillNo,  
                 [Date],  
                 Amount,  
                 [Type],  
                 Ceiling(( Amount - PaidAmount ) * 100) / 100 AS outstanding  
          FROM   vw_expense  
          WHERE  ( ( date >= Case When(ISNULL(@Days,0)=0)THEN @FromDate ELSE Dateadd(day, @Days, @FromDate)END  
                      OR @FromDate IS NULL )  
                   AND ( date <= @ToDate  
                          OR @ToDate IS NULL ) )  
                 AND ( VendorID IN ( @VendorID )  
                        OR @VendorID IS NULL )  
                 AND ( CreatedBy IN( @CreatedBy )  
                        OR @CreatedBy IS NULL )  
                 AND TYPE LIKE '%' + Isnull(@Type, '') + '%'  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
