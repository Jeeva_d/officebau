﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:
MODIFIED DATE		:	
 <summary>   
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getdropdown_ledgerproduct] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON

      BEGIN TRY
          SELECT ID   AS ID,
                 NAME+' ( Ledger )' AS Code
          FROM   tblLedger
          WHERE  IsDeleted = 0
                 AND DomainID = @DomainID
          UNION
          SELECT ID   AS ID,
                 NAME+' ( Product )' AS Code
          FROM   tblProduct_V2
          WHERE  IsDeleted = 0
                 AND DomainID = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
