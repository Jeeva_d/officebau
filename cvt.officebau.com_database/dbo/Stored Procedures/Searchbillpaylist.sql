﻿/****************************************************************************                       
CREATED BY     : Naneeshwar                      
CREATED DATE   : 17-Nov-2016                    
MODIFIED BY    : JENNIFER.S                    
MODIFIED DATE  : 21-DEC-2016                    
 <summary>                    
 exec [Searchbillpaylist] 6106, 3047, 1,1                    
 exec [Searchbillpaylist] 6107, 3047, 1,1                    
 exec [Searchbillpaylist] 2, 3, 2                    
 </summary>                                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchbillpaylist] (@ID       INT,
                                            @VendorID INT,
                                            @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @table TABLE
            (
               DetailDescription VARCHAR(8000),
               EID               INT
            )

          INSERT INTO @table
          SELECT (SELECT Substring((SELECT + ',' + ed.Remarks
                                    FROM   tblExpenseDetails ed
                                    WHERE  ed.IsDeleted = 0
                                           AND ex.ID = ed.ExpenseID
                                    FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)'), 2, 200000)),
                 ex.ID
          FROM   tblExpense ex;

          WITH expenseAmount
               AS (SELECT ExpenseID,
                          ( ( Isnull(Sum(QTY * Amount), 0) ) + Isnull(Sum(CGST), 0) + Isnull(Sum(SGST), 0) ) Amount
                   FROM   tblExpenseDetails
                   WHERE  IsDeleted = 0
                          AND DomainID = @DomainID
                   GROUP  BY ExpenseID)
          SELECT e.ID                                                        AS ExpenseID,
                 epm.ID                                                      AS ExpensePaymentID,
                 e.BillNo                                                    AS BillNo,
                 (SELECT Isnull(DetailDescription, '')
                  FROM   @table d
                  WHERE  d.EID = e.ID)                                       AS [Description],
                 e.[Date]                                                    AS BillDate,
                 e.DueDate                                                   AS DueDate,
                 ( Isnull(ea.Amount, 0) )                                    AS Amount,
                 e.PaidAmount                                                AS PaidAmount,
                 ( Isnull(ea.Amount, 0) - ( Isnull(e.PaidAmount, 0)
                                            + (SELECT Isnull(Sum(Amount), 0)
                                               FROM   tblExpenseHoldings
                                               WHERE  ExpenseId = e.ID
                                                      AND Isdeleted = 0) ) ) AS OutstandingAmount,
                 CASE
                   WHEN ( Isnull(@ID, 0) = 0 ) THEN 0
                   ELSE epm.Amount
                 END                                                         AS updateAmount,
                 CASE
                   WHEN( (SELECT TOP 1 l.NAME
                          FROM   tblLedger l
                          WHERE  l.ID = (SELECT TOP 1 LedgerID
                                         FROM   tblExpenseDetails ed
                                         WHERE  ed.ExpenseID = e.ID
                                                AND ed.IsDeleted = 0
                                                AND IsLedger = 1)) IS NULL ) THEN (SELECT TOP 1 l.NAME
                                                                                   FROM   tblProduct_v2 l
                                                                                   WHERE  l.ID = (SELECT TOP 1 LedgerID
                                                                                                  FROM   tblExpenseDetails ed
                                                                                                  WHERE  ed.ExpenseID = e.ID
                                                                                                         AND ed.IsDeleted = 0
                                                                                                         AND IsLedger = 0))
                   ELSE (SELECT TOP 1 l.NAME
                         FROM   tblLedger l
                         WHERE  l.ID = (SELECT TOP 1 LedgerID
                                        FROM   tblExpenseDetails ed
                                        WHERE  ed.ExpenseID = e.ID
                                               AND ed.IsDeleted = 0
                                               AND IsLedger = 1))
                 END                                                         AS LedgerName,
                 (SELECT Count(*)
                  FROM   tblExpenseHoldings
                  WHERE  ExpenseId = e.ID
                         AND Isdeleted = 0)                                  AS HoldingsCount,
                 (SELECT Isnull(Sum(Amount), 0)
                  FROM   tblExpenseHoldings
                  WHERE  ExpenseId = e.ID
                         AND Isdeleted = 0)                                  AS HoldingAmount
          FROM   tblExpense e
                 LEFT JOIN expenseAmount ea
                        ON ea.ExpenseID = e.ID
                 LEFT JOIN tblExpensePaymentMapping epm
                        ON epm.ExpenseID = e.ID
                           AND epm.ExpensePaymentID = @ID
                           AND epm.IsDeleted = 0
          WHERE  e.IsDeleted = 0
                 AND @VendorID = e.VendorID
                 AND [Type] = 'Bill'
                 AND e.DomainID = @DomainID
                 AND ( ( Isnull(@ID, 0) = 0
                         AND ( Isnull(ea.Amount, 0) - Isnull(e.PaidAmount, 0) ) != 0 )
                        OR Isnull(@ID, 0) != 0 )
          ORDER  BY BillDate DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
