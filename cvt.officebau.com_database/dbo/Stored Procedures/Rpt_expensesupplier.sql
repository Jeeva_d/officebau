﻿        
/****************************************************************************               
CREATED BY   : Naneeshwar.M            
CREATED DATE  :               
MODIFIED BY   :  Ajith N            
MODIFIED DATE  : 11 May 2017               
 <summary>            
 [Rpt_expensesupplier] 0,1           
  [Rpt_expensesupplier] 'May 1 2015','Apr 30 2017',1          
   [Rpt_expensesupplier] '',' ',1          
 </summary>                                       
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[Rpt_expensesupplier] (@StartDate DATETIME = NULL,        
                                             @EndDate   DATETIME = NULL,        
                                             @DomainID  INT)        
AS        
                                                   
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          DECLARE @FinancialYear       INT,        
                  @StartFinancialMonth INT,        
                  @EndFinancialMonth   INT;        
        
          IF ( @StartDate IS NULL        
                OR @StartDate = '' )        
              OR ( @EndDate IS NULL        
                    OR @EndDate = '' )        
            BEGIN        
                SET @StartFinancialMonth = (SELECT Value        
                                            FROM   tblApplicationConfiguration        
                                            WHERE  Code = 'STARTMTH'        
                                                   AND DomainID = @DomainID)        
                SET @EndFinancialMonth = ( ( (SELECT Value        
                                              FROM   tblApplicationConfiguration        
                                              WHERE  Code = 'STARTMTH'        
                                                     AND DomainID = @DomainID)        
                                             + 11 ) % 12 )        
        
                IF( Month(Getdate()) <= ( ( (SELECT Value        
                                             FROM   tblApplicationConfiguration        
                                             WHERE  Code = 'STARTMTH'        
                                                    AND DomainID = @DomainID)        
                                            + 11 ) % 12 ) )        
                  SET @FinancialYear = Year(Getdate()) - 1        
                ELSE        
                  SET @FinancialYear = Year(Getdate())        
        
                SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date            
                SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date           
            END        
        
          DECLARE @MONTHCOUNT          INT = 0,        
                  @currentmonth        INT,        
                  @currentyear         INT,        
                  @SupplierID          VARCHAR(100) = '',        
                  @AMOUNT              VARCHAR(100),        
                  @RowValueUpdateQuery VARCHAR(MAX)= '',        
                  @Count               INT = 0,        
                  @QueryResult         NVARCHAR(MAX),        
                  @ColumnName          VARCHAR(10),        
                  @DynamicSQL          VARCHAR(MAX),        
                  @SumOfColumnQuery    VARCHAR(MAX),        
                  @SumOfRowQuery       VARCHAR(MAX),        
                  @RowTotalInsertQuery VARCHAR(MAX),        
                  @OutputQuery         VARCHAR(MAX)        
        
          CREATE TABLE #Result        
            (        
               ID           INT PRIMARY KEY IDENTITY(1, 1),        
               [SupplierID] VARCHAR(200),        
               [Supplier]   VARCHAR(200),        
               Total        MONEY NULL        
            )        
        
          SELECT Datename(MONTH, Dateadd(MONTH, X.number, @StartDate)) AS [MonthName],        
          Datename(YY, Dateadd(MONTH, X.number, @StartDate))    [YEAR],        
                 Datepart(MM, Dateadd(MM, X.number, @StartDate))       [MONTH],        
                 X.number                                              AS RowNumber        
          INTO   #TempMonth        
          FROM   master.dbo.spt_values X        
          WHERE  X.type = 'P'        
                 AND X.number <= Datediff(MONTH, @StartDate, @EndDate);        
        
          SET @SumOfColumnQuery = (SELECT Substring((SELECT '+ ISNULL( ([' + Substring([MonthName], 1, 3 )        
                                                            + '_' + Substring([YEAR], 3, 2) + ']), 0)'        
                                                     FROM   #TempMonth        
                                                     FOR XML PATH('')), 2, 20000)) --Update Sum Of Column  value        
          SET @SumOfRowQuery = (SELECT Substring((SELECT ',  NULLIF(SUM(ISNULL(['        
                                                         + Substring([MonthName], 1, 3 ) + '_'        
                                                         + Substring([YEAR], 3, 2) + '], 0)), 0) '        
                                                  FROM   #TempMonth        
                                                  FOR XML PATH('')), 2, 20000)) --Update Sum Of Row value        
          SET @OutputQuery = (SELECT Substring((SELECT ',[' + Substring([MonthName], 1, 3 ) + '_'        
                                                       + Substring([YEAR], 3, 2) + ']'        
                                                FROM   #TempMonth        
                                                FOR XML PATH('')), 2, 20000)) -- Final Output Table Fields        
          SET @DynamicSQL =(SELECT 'ALTER TABLE #Result ADD ['        
                                   + Substring([MonthName], 1, 3 ) + '_'        
                                   + Substring([YEAR], 3, 2) + '] MONEY NULL;'        
                            FROM   #TempMonth        
                            FOR XML PATH('')) -- Add dynamic column        
          EXEC(@DynamicSQL) -- Add dynamic column        
          INSERT INTO #Result        
                      ([SupplierID],        
                       [Supplier])        
          SELECT DISTINCT V.ID,        
                          V.NAME        
          FROM   tblVendor V        
                 JOIN tblExpense E        
                   ON V.ID = E.VendorID        
          WHERE  E.DomainID = @DomainID        
                 AND E.IsDeleted = 0        
          ORDER  BY V.NAME ASC        
        
          WHILE( @Count < (SELECT Count(1)        
                           FROM   #Result) )        
            BEGIN        
                SET @SupplierID = (SELECT SupplierID        
                                   FROM   #Result        
                                   WHERE  ID = ( @Count + 1 ))        
        
                WHILE( @MONTHCOUNT < (SELECT Count(1)        
                                      FROM   #TempMonth) )        
                  BEGIN        
                      SELECT @currentmonth = [MONTH],        
                             @currentyear = [year],        
                             @ColumnName = Substring([MonthName], 1, 3 ) + '_'        
                                           + Substring([YEAR], 3, 2) -- Display Column Name        
                      FROM   #TempMonth        
                      WHERE  RowNumber = @MONTHCOUNT        
        
                      SET @AMOUNT = (SELECT Sum(ED.Amount*Ed.Qty) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0)        
                                     FROM   tblExpense E        
                                            JOIN tblExpenseDetails ED        
                                              ON ED.ExpenseID = E.ID        
                                            JOIN tblVendor V        
                                              ON V.ID = E.VendorID        
                                     WHERE  Datepart(MONTH, E.Date) = @currentmonth        
                                            AND Datepart(YEAR, E.Date) = @currentyear        
                                            AND V.ID = @SupplierID        
                                            AND ED.IsDeleted = 0        
                                            AND ( @StartDate = ''        
                                                   OR E.Date >= @StartDate )        
                                            AND ( @EndDate = ''        
                                                   OR E.Date <= @EndDate ))        
                      SET @RowValueUpdateQuery = Isnull('UPDATE #Result SET [' + @ColumnName +'] = ' + @AMOUNT + ' WHERE SupplierID = ''' + @SupplierID + ''';', '')        
                                          + Isnull(@RowValueUpdateQuery, '') --Update Row value        
                      SET @MONTHCOUNT = @MONTHCOUNT + 1        
                  END        
        
                SET @QueryResult = Isnull('UPDATE #Result SET Total = ' + @SumOfColumnQuery + ' WHERE SupplierID = ''' + @SupplierID + ''';', '')        
                                   + Isnull(@QueryResult, '') -- Result  table        
                SET @MONTHCOUNT = 0        
                SET @Count = @Count + 1        
            END        
        
          SET @RowTotalInsertQuery = ' insert into #result SELECT '''',''Total Amount'',NULLIF(Sum(Isnull(Total, 0)), 0),'        
                                     + @SumOfRowQuery + 'FROM   #Result' --Insert Row Total value        
          EXEC(@RowValueUpdateQuery ) --Update Row value        
          EXEC(@QueryResult) -- Result  table        
          EXEC(@RowTotalInsertQuery) --Insert Row Total value    
          EXEC( 'SELECT Supplier, Isnull(Total,0) As Total, '+ @OutputQuery + ' FROM #Result ') -- Final Output table        
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
