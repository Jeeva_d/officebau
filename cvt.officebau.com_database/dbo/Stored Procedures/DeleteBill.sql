﻿/****************************************************************************         
CREATED BY    : Ajith N      
CREATED DATE  : 18 May 2017      
MODIFIED BY   :       
MODIFIED DATE :       
 <summary>        
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteBill] (@ID        INT,
                                    @SessionID INT = 2,
                                    @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT VARCHAR(100)

          IF EXISTS (SELECT 1
                     FROM   tblExpense
                     WHERE  StatusID <> (SELECT id
                                         FROM   tbl_CodeMaster
                                         WHERE  [type] = 'Open'
                                                AND IsDeleted = 0)
                            AND id = @ID
                            AND IsDeleted = 0
                            AND DomainID = @DomainID)
            BEGIN
                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [type] = 'Warning'
                                      AND Code = 'RCD_REF'
                                      AND IsDeleted = 0) --'The record is referred.'      
                GOTO finish
            END
          ELSE
            BEGIN
                UPDATE tblExpense
                SET    IsDeleted = 1
                WHERE  @ID = id

                UPDATE tblExpenseDetails
                SET    IsDeleted = 1
                WHERE  ExpenseID = @ID

                SET @Output ='Deleted Successfully'

                ----Delete item to Inventory
                EXEC ManageInventory
                  @ID,
                  'DELETE',
                  'Purchase',
                  @SessionID,
                  @DomainID

                GOTO finish
            END

          FINISH:

          EXEC UpdatePOQTY

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
