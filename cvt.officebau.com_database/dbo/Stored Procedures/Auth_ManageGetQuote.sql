﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  : 
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
     [Auth_ManageGetQuote] 'test','w@n.in','re','2015-03-12'  
 </summary>                           
 *****************************************************************************/    
 CREATE PROCEDURE [dbo].[Auth_ManageGetQuote]  
 (  
 @Client_Name    VARCHAR(50)  ,  
 @Client_Email    varchar(150) ,  
 @Client_Expectation   varchar(MAX) ,  
 @Quote_Date     DATE   ,  
 @IsRead      BIT    ,  
 @IsDeleted     BIT    
 )  
 AS  
 BEGIN  
 SET NOCOUNT ON  
 BEGIN TRY       
  SET NOCOUNT ON;  
  DECLARE @Output      VARCHAR(100)      
  BEGIN TRANSACTION  
   IF EXISTS ( SELECT 1 FROM Auth_GetQuote WHERE IsDeleted = 0 AND Client_Email = @Client_Email )  
   BEGIN  
    SET @Output = 'Email Id Already Exist'  
    GOTO RESULT  
   END  
     
   INSERT INTO Auth_GetQuote   
   (  
    Client_Name    ,  
    Client_Email   ,  
    Client_Expectation  ,  
    Quote_Date    ,  
    IsRead     ,  
    IsDeleted         
   )  
   VALUES  
   (  
    @Client_Name   ,  
    @Client_Email   ,  
    @Client_Expectation  ,  
    @Quote_Date    ,  
    @IsRead     ,  
    @IsDeleted  
   )  
   SET @Output = 'Thanks for your Visit!  We will get you in touch shortly!'  
 RESULT:  
  SELECT @Output  
  COMMIT TRANSACTION        
 END TRY  
 BEGIN CATCH          
  DECLARE @ErrorMsg VARCHAR(100),  @ErrSeverity TINYINT          
  SELECT @ErrorMsg=ERROR_MESSAGE(), @ErrSeverity=ERROR_SEVERITY()          
  RAISERROR(@ErrorMsg,@ErrSeverity,1)      
 END CATCH   
 END  
