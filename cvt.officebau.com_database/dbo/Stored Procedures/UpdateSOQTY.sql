﻿/****************************************************************************         
CREATED BY    :       
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE :       
 <summary>        
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[UpdateSOQTY]     
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
    
    
  update tblSOItem set     
InvoicedQTY = Case when((Select ISNULL(Sum(QTY),0) from tblInvoiceItem  where SOitemID =tblSOItem.ID and IsDeleted=0)>QTY)     
Then QTY ELSE  (Select ISNULL(Sum(QTY),0) from tblInvoiceItem  where SOitemID =tblSOItem.ID and IsDeleted=0)    
END    
from tblSOItem     
    
Update tblSalesOrder Set StatusID =     
 Case when ((Select Sum(QTY) from tblSOItem where IsDeleted =0 and SOID = tblSalesOrder.ID) =     
 (Select Sum(InvoicedQTY) from tblSOItem where IsDeleted =0 and SOID = tblSalesOrder.ID))    
 Then     
 (Select ID from tbl_CodeMaster where Code='Closed')    
 Else     
 (Select ID from tbl_CodeMaster where Code='Open')    
 END    
 from tblSalesOrder     
    
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
