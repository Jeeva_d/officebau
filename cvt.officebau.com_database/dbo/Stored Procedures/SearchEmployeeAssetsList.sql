﻿  
/****************************************************************************   
CREATED BY   : DHANALAKSHMI S  
CREATED DATE  : 16 Aug 2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
  [SearchEmployeeAssetsList]   '', '', 25,106,1  
 </summary>                            
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchEmployeeAssetsList] (@EmployeeCode VARCHAR(20),  
                                                  @Name         VARCHAR(100),  
                                                  @StatusID     INT,  
                                                  @HRApproverID INT,  
                                                  @DomainID     INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @BusinessUnitIDs VARCHAR(50) = (SELECT BusinessUnitID  
             FROM   tbl_EmployeeMaster  
             WHERE  ID = @HRApproverID  
                    AND DomainID = @DomainID)  
          DECLARE @BusinessUnitTable TABLE  
            (  
               BusinessUnit VARCHAR(100)  
            )  
  
          INSERT INTO @BusinessUnitTable  
          SELECT @BusinessUnitIDs  
  
          SELECT OFP.ID                                         AS ID,  
                 EM.ID                                          AS EmployeeID,  
                 ISNULL(EM.EmpCodePattern, '') + '' + EM.Code                                        AS EmployeeCode,  
                 EM.FirstName + ' ' + Isnull(EM.LastName, '')   AS FirstName,  
                 DEP.NAME                                       AS DepartmentName,  
                 EMP.FirstName + ' ' + Isnull(EMP.LastName, '') AS ApprovarName,  
                 OFP.ApprovedOn                                 AS ApprovedDate,  
                 BU.NAME                                        AS BusinessUnit,  
     ST.Code                                        AS ReturnStatus  
          FROM   tbl_OffBoardProcess OFP  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = OFP.EmployeeID  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = OFP.ApproverID  
                 LEFT JOIN tbl_Department DEP  
                        ON DEP.ID = EM.DepartmentID  
                 LEFT JOIN tbl_Designation DESI  
                        ON DESI.ID = EM.DesignationID  
                 LEFT JOIN tbl_Functional FUN  
                        ON FUN.ID = EM.FunctionalID  
                 LEFT JOIN tbl_BusinessUnit BU  
                        ON BU.ID = EM.BaseLocationID  
     LEFT JOIN tbl_Status ST  
            ON ST.ID = OFP.AssetStatusID  
                 JOIN @BusinessUnitTable TBU  
                   ON TBU.BusinessUnit LIKE '%,'  
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))  
                                            + ',%'  
          WHERE  OFP.HRApproverID = @HRApproverID  
                 AND ( Isnull(@EmployeeCode, '') = ''  
                        OR ( EM.Code LIKE '%' + @EmployeeCode + '%' ) )  
                 AND ( Isnull(@Name, '') = ''  
                        OR ( ( EM.FirstName + ' ' + Isnull(EM.LastName, '') ) LIKE '%' + @Name + '%' ) )  
                 AND ( ( Isnull(@StatusID, 0) = 0  
                         AND Isnull(@StatusID, 0) = 0 )  
                        OR OFP.AssetStatusID = @StatusID )  
                 AND OFP.domainID = @DomainID  
     AND OFP.EmployeeID in (Select EmployeeID   
                            from tbl_CompanyAssets   
          where IsDeleted = 0   
          and DomainID = @DomainID)  
          ORDER  BY EM.FirstName + ' ' + Isnull(EM.LastName, '')  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
