﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [SearchNewsAndEventsBU] 106,1,'profile'
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchNewsAndEventsBU] (@UserID   INT,
                                               @DomainID INT,
                                               @Type     VARCHAR(50))
AS
  BEGIN
      BEGIN TRY
          SELECT DISTINCT Split.a.value('.', 'VARCHAR(100)') AS String,
                          ID
          INTO   #Temptable
          FROM   (SELECT Cast ('<M>'
                               + Replace([BusinessUnit], ',', '</M><M>')
                               + '</M>' AS XML) AS String,
                         ID
                  FROM   tbl_NewsAndEvents
                  WHERE  DomainID = @DomainID) AS A
                 CROSS APPLY String.nodes ('/M') AS Split(a);

          IF( @Type = 'PROFILE' )
            BEGIN
                SELECT ne.ID              AS ID,
                       NAME               AS NAME,
                       Description        AS Description,
                       PublishedOn        AS PublishedOn,
                       EndDate            AS EndDate,
                       em.FullName       AS PublishedBy,
                       em1.FullName      AS CreatedBy,
                       ne.HistoryID       AS HistoryID,
                       StartDate          AS StartDate,
                       bus.BaseLocationID AS BaseLocationID
                FROM   tbl_NewsAndEvents ne
                       LEFT JOIN tbl_EmployeeMaster em
                              ON em.ID = ne.PublishedBy
                       JOIN tbl_EmployeeMaster em1
                         ON em1.ID = ne.CreatedBy
                       JOIN tbl_EmployeeMaster bus
                         ON bus.ID = @UserID
                WHERE  ne.DomainID = @DomainID
                       AND ne.IsDeleted = 0
                       AND ne.ID IN (SELECT ID
                                     FROM   #Temptable
                                     WHERE  String = Bus.BaseLocationID)
                       AND ( Cast (StartDate AS DATE) <= Cast (Getdate()AS DATE) )
                       AND ( Cast (EndDate AS DATE) >= Cast (Getdate() AS DATE)
                             AND Cast (EndDate AS DATE) >= Cast (StartDate AS DATE) )
                UNION ALL
                SELECT 0                                 AS ID,
                       FirstName + ' ' + Isnull(LastName, '') + ' - ' + '( '
                       + Isnull(depart.NAME, '-') + ' )' AS NAME,
                       'ANNIVERSARY'                     AS Description,
                       NULL                              AS PublishedOn,
                       NULL                              AS EndDate,
                       NULL                              AS PublishedBy,
                       NULL                              AS CreatedBy,
                       NULL                              AS HistoryID,
                       NULL                              AS StartDate,
                       NULL                              AS BaseLocationID
                FROM   tbl_EmployeeMaster EMP
                       LEFT JOIN tbl_EmployeePersonalDetails EPD
                              ON EPD.EmployeeID = EMP.ID
                       LEFT JOIN tbl_Department depart
                              ON depart.ID = emp.DepartmentID
                WHERE  EMP.isDeleted = 0
                       AND emp.DomainID = @DomainID
                       AND EMP.IsActive = 0
                       AND HasAccess = 1
                       AND Month(Anniversary) = Month(Getdate())
                       AND Day(Anniversary) = Day(Getdate())
                UNION ALL
                SELECT 0                                 AS ID,
                       FirstName + ' ' + Isnull(LastName, '') + ' - ' + '( '
                       + Isnull(depart.NAME, '-') + ' )' AS NAME,
                       'BIRTHDAY'                        AS Description,
                       NULL                              AS PublishedOn,
                       NULL                              AS EndDate,
                       NULL                              AS PublishedBy,
                       NULL                              AS CreatedBy,
                       NULL                              AS HistoryID,
                       NULL                              AS StartDate,
                       NULL                              AS BaseLocationID
                FROM   tbl_EmployeeMaster emp
                       LEFT JOIN tbl_EmployeePersonalDetails epd
                              ON epd.EmployeeID = emp.ID
                       LEFT JOIN tbl_Department depart
                              ON depart.ID = emp.DepartmentID
                WHERE  emp.IsDeleted = 0
                       AND emp.IsActive = 0
                       AND emp.DomainID = @DomainID
                       AND HasAccess = 1
                       AND Month(epd.DateOfBirth) = Month(Getdate())
                       AND Day(epd.DateOfBirth) = Day(Getdate())
                ORDER  BY StartDate DESC
            END
          ELSE
            BEGIN
                SELECT ne.ID              AS ID,
                       NAME               AS NAME,
                       Description        AS Description,
                       PublishedOn        AS PublishedOn,
                       EndDate            AS EndDate,
                       em.FullName       AS PublishedBy,
                       em1.FullName      AS CreatedBy,
                       ne.HistoryID       AS HistoryID,
                       StartDate          AS StartDate,
					   EventDate          AS EventDate,
                       bus.BaseLocationID AS BaseLocationID
                FROM   tbl_NewsAndEvents ne
                       LEFT JOIN tbl_EmployeeMaster em
                              ON em.ID = ne.PublishedBy
                       JOIN tbl_EmployeeMaster em1
                         ON em1.ID = ne.CreatedBy
                       JOIN tbl_EmployeeMaster bus
                         ON bus.ID = @UserID
                WHERE  ne.DomainID = @DomainID
                       AND ne.IsDeleted = 0
                       AND ne.ID IN (SELECT ID
                                     FROM   #Temptable
                                     WHERE  String = Bus.BaseLocationID)
                       AND ( Cast (StartDate AS DATE) <= Cast (Getdate()AS DATE) )
                       AND ( Cast (EndDate AS DATE) >= Cast (Getdate() AS DATE)
                             AND Cast (EndDate AS DATE) >= Cast (StartDate AS DATE) )
                ORDER  BY StartDate DESC
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
