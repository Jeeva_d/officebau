﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :           
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>           
[ManageDeployment]  
 </summary>                                   
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ManageDeployment] (@ID               INT,    
                                           @BroadcastMessage VARCHAR(1000),    
                                           @IsActive         BIT,    
                                           @FromDate         DATETIME,    
                                           @ToDate           DATETIME,    
                                           @DomainID         VARCHAR(500),    
                                           @SessionID        INT,    
                                           @IsDeleted        BIT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(100) = 'Operation Failed!'    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          IF( @IsDeleted = 1 )    
            BEGIN    
                UPDATE DeploymentActivities    
                SET    IsDeleted = 1    
                WHERE  @ID = ID    
                SET @Output = (SELECT [Message]    
                               FROM   tblErrorMessage    
                               WHERE  [Type] = 'Information'    
                                      AND Code = 'RCD_DEL'    
                                      AND IsDeleted = 0) --'Deleted Successfully'            
            END    
          ELSE    
            BEGIN    
                IF( @ID = 0 )    
                  BEGIN    
                      INSERT INTO DeploymentActivities    
                                  (BrodcastMessage,    
                                   IsActive,    
                                   FromDate,    
                                   ToDate,    
                                   DomainID,    
                                   CreatedBy,    
                                   CreatedOn,    
                                   ModifiedBy,    
                                   ModifiedOn)    
                      VALUES      (@BroadcastMessage,    
                                   @IsActive,    
                                   @FromDate,    
                                   @ToDate,    
                                   @DomainID,    
                                   @SessionID,    
                                   Getdate(),    
                                   @SessionID,    
                                   Getdate())    
                      SET @Output = 'Inserted Successfully'    
                  END    
                ELSE    
                  BEGIN    
                      UPDATE DeploymentActivities    
                      SET    BrodcastMessage = @BroadcastMessage,    
                             IsActive = @IsActive,    
                             FromDate = @FromDate,    
                             ToDate = @ToDate,    
                             DomainID = @DomainID,    
                             ModifiedBy = @SessionID,    
                             ModifiedOn = Getdate()    
                      WHERE  ID = @ID    
    
                      SET @Output = 'Updated Successfully'    
                  END    
            END    
    
          FINISH:    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
