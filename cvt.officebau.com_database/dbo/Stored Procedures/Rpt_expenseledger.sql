﻿/****************************************************************************                   
CREATED BY   :                 
CREATED DATE  :                   
MODIFIED BY   :                   
MODIFIED DATE  :                   
 <summary>                
 [Rpt_expenseledger] 'OCT 1 2017','Mar 30 2019',1                 
 </summary>                                           
 *****************************************************************************/              
CREATE PROCEDURE [dbo].[Rpt_expenseledger] (    
  @StartDate DATETIME ,              
                                           @EndDate   DATETIME ,              
                                           @DomainID  INT     
                 
             )              
AS              
              
BEGIN              
    SET NOCOUNT ON;              
              
    --BEGIN TRY              
        BEGIN TRANSACTION              
              
        DECLARE @FinancialYear INT;              
        DECLARE @StartFinancialMonth INT;              
        DECLARE @EndFinancialMonth INT;              
              
        IF ( @StartDate IS NULL              
              OR @StartDate = '' )              
            OR ( @EndDate IS NULL              
                  OR @EndDate = '' )              
          BEGIN              
              SET @StartFinancialMonth = (SELECT Value              
                                          FROM   tblApplicationConfiguration              
                                          WHERE  Code = 'STARTMTH'              
                                                 AND DomainID = @DomainID)              
              SET @EndFinancialMonth = ( ( (SELECT Value              
                                            FROM   tblApplicationConfiguration              
                                            WHERE  Code = 'STARTMTH'              
                                                   AND DomainID = @DomainID)              
                                           + 11 ) % 12 )              
              
              IF( Month(Getdate()) <= ( ( (SELECT Value              
                                           FROM   tblApplicationConfiguration              
                                           WHERE  Code = 'STARTMTH'              
                                                  AND DomainID = @DomainID)              
                                          + 11 ) % 12 ) )              
                SET @FinancialYear = Year(Getdate()) - 1              
              ELSE              
                SET @FinancialYear = Year(Getdate())              
              
              SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date                
              SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date               
          END              
              
        DECLARE @MONTHCOUNT          INT = 0,              
                @currentmonth        INT,              
                @currentyear         INT,              
                @LedgerID            VARCHAR(100) = '',      
                @Type            VARCHAR(100) = '',              
                
                @AMOUNT              VARCHAR(100),              
                @RowValueUpdateQuery VARCHAR(MAX)= '',              
                @Count               INT = 0,              
                @QueryResult         NVARCHAR(MAX),              
                @ColumnName          VARCHAR(10),              
                @DynamicSQL          VARCHAR(MAX),              
                @SumOfColumnQuery    VARCHAR(MAX),              
                @SumOfRowQuery       VARCHAR(MAX),              
                @RowTotalInsertQuery VARCHAR(MAX),              
                @OutputQuery         VARCHAR(MAX)              
              
        CREATE TABLE #Result              
          (              
             ID         INT PRIMARY KEY IDENTITY(1, 1),              
             [LedgerID] VARCHAR(200),              
             [Ledger]   VARCHAR(200),     
    [Type]  Varchar(10),             
             Total      MONEY NULL              
          )              
              
        SELECT Datename(MONTH, Dateadd(MONTH, X.number, @StartDate)) AS [MonthName],       
               Datename(YY, Dateadd(MONTH, X.number, @StartDate))    [YEAR],              
               Datepart(MM, Dateadd(MM, X.number, @StartDate))  [MONTH],              
               X.number                                              AS RowNumber              
        INTO   #TempMonth              
        FROM   master.dbo.spt_values X              
        WHERE  X.type = 'P'              
               AND X.number <= Datediff(MONTH, @StartDate, @EndDate);              
              
        SET @SumOfColumnQuery = (SELECT Substring((SELECT '+ ISNULL( ([' + Substring([MonthName], 1, 3 )              
                                                          + '_' + Substring([YEAR], 3, 2) + ']), 0)'              
                                                   FROM   #TempMonth              
                                                   FOR XML PATH('')), 2, 20000)) --Update Sum Of Column  value              
        SET @SumOfRowQuery = (SELECT Substring((SELECT ',  NULLIF(SUM(ISNULL(['              
                                                       + Substring([MonthName], 1, 3 ) + '_'              
                                                       + Substring([YEAR], 3, 2) + '], 0)), 0) '              
                                                FROM   #TempMonth              
                                                FOR XML PATH('')), 2, 20000)) --Update Sum Of Row value              
        SET @OutputQuery = (SELECT Substring((SELECT ',[' + Substring([MonthName], 1, 3 ) + '_'              
                                                     + Substring([YEAR], 3, 2) + ']'              
                                              FROM   #TempMonth              
                                              FOR XML PATH('')), 2, 20000)) -- Final Output Table Fields              
        SET @DynamicSQL =(SELECT 'ALTER TABLE #Result ADD ['              
                                 + Substring([MonthName], 1, 3 ) + '_'              
                                 + Substring([YEAR], 3, 2) + '] MONEY NULL;'              
                          FROM   #TempMonth              
                          FOR XML PATH('')) -- Add dynamic column              
        EXEC(@DynamicSQL) -- Add dynamic column              
        INSERT INTO #Result              
                    ([LedgerID],              
                     [Ledger],    
      [Type])              
        SELECT DISTINCT Ld.ID,              
                        Ld.NAME ,    
      'L'             
        FROM   tblLedger ld              
              LEFT JOIN tblExpenseDetails ex              
                      ON ld.ID = ex.LedgerID  And IsLedger=1 and ex.IsDeleted=0            
               JOIN tblExpense e              
                 ON e.ID = ex.ExpenseID              
        WHERE  e.DomainID = @DomainID              
               AND e.IsDeleted = 0              
      ORDER BY Ld.NAME ASC             
            
                 INSERT INTO #Result              
                    ([LedgerID],              
                     [Ledger],    
      [Type])              
        SELECT DISTINCT Ld.ID,              
                        Ld.NAME ,    
      'P'             
        FROM   tblProduct_V2 ld              
              LEFT JOIN tblExpenseDetails ex              
                      ON ld.ID = ex.LedgerID  And IsLedger=0 and ex.IsDeleted=0            
               JOIN tblExpense e              
                 ON e.ID = ex.ExpenseID              
        WHERE  e.DomainID = @DomainID   
               AND e.IsDeleted = 0              
      ORDER BY Ld.NAME ASC     
    
        WHILE( @Count < (SELECT Count(1)              
                         FROM   #Result) )              
          BEGIN              
    
            Set  @LedgerID=  (SELECT LedgerID            
                        FROM   #Result              
                               WHERE  ID = ( @Count + 1 ))      
         Set  @Type=  (SELECT Type            
                               FROM   #Result              
                               WHERE  ID = ( @Count + 1 ))               
             
              WHILE( @MONTHCOUNT < (SELECT Count(1)              
                                    FROM   #TempMonth) )              
                BEGIN              
                    SELECT @currentmonth = [MONTH],              
                           @currentyear = [year],              
                           @ColumnName = Substring([MonthName], 1, 3 ) + '_'              
                                         + Substring([YEAR], 3, 2) -- Column Name              
                    FROM   #TempMonth              
                    WHERE  RowNumber = @MONTHCOUNT              
              
                    SET @AMOUNT = Case When ( @Type ='L') Then (SELECT Sum(Amount*Qty)  +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0)            
                                   FROM   tblExpenseDetails ex              
                                          LEFT JOIN tblExpense e              
                                                 ON e.ID = ex.ExpenseID and ex.IsDeleted=0 And IsLedger=1             
                                          JOIN tblLedger ld              
                                            ON ld.ID = ex.LedgerID              
                                   WHERE   Datepart(MONTH, e.Date) = @currentmonth              
           AND Datepart(YEAR, e.Date) = @currentyear              
                                          AND ld.ID = @LedgerID              
                                          AND ( @StartDate = ''              
                                                 OR e.Date >= @StartDate )              
                                          AND ( @EndDate = ''              
                                                 OR e.Date <= @EndDate ))     
         ELSE            
         (SELECT Sum(Amount*Qty)  +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0)            
                                        FROM   tblExpenseDetails ex              
                                          LEFT JOIN tblExpense e              
                                                 ON e.ID = ex.ExpenseID and ex.IsDeleted=0 And IsLedger=0             
                                          JOIN tblProduct_v2 ld              
                                            ON ld.ID = ex.LedgerID              
                                   WHERE   Datepart(MONTH, e.Date) = @currentmonth              
AND Datepart(YEAR, e.Date) = @currentyear              
                                          AND ld.ID = @LedgerID              
                                          AND ( @StartDate = ''              
                                                 OR e.Date >= @StartDate )              
                                          AND ( @EndDate = ''              
                                                 OR e.Date <= @EndDate ))     
             END     
                    SET @RowValueUpdateQuery = Isnull('UPDATE #Result SET [' + @ColumnName +'] = ' + @AMOUNT + ' WHERE LedgerID = ''' + @LedgerID + ''';', '')              
                                               + Isnull(@RowValueUpdateQuery, '') --Update Row value              
                    SET @MONTHCOUNT = @MONTHCOUNT + 1              
                END              
              
              SET @QueryResult = Isnull('UPDATE #Result SET Total = ' + @SumOfColumnQuery + ' WHERE LedgerID = ''' + @LedgerID + ''';', '')              
       + Isnull(@QueryResult, '') -- Result  table              
              SET @MONTHCOUNT = 0              
              SET @Count = @Count + 1              
          END              
              
        SET @RowTotalInsertQuery = ' insert into #result SELECT '''',''Total Amount'',''T'',NULLIF(Sum(Isnull(Total, 0)), 0),'              
                                   + @SumOfRowQuery + 'FROM   #Result' --Insert Row Total value              
              
        Exec (@RowValueUpdateQuery ) --Update Row value              
              
        Exec(@QueryResult) -- Result  table              
  
       Exec(@RowTotalInsertQuery) --Insert Row Total value              
              
        Exec( 'SELECT Ledger AS [Ledger / Product], Isnull(Total,0) As Total, '+ @OutputQuery + ' FROM #Result ') -- Final Output table              
        COMMIT TRANSACTION              
    --END TRY              
    --BEGIN CATCH              
    --    ROLLBACK TRANSACTION              
    --    DECLARE @ErrorMsg    VARCHAR(100),              
    --            @ErrSeverity TINYINT              
    --    SELECT @ErrorMsg = Error_message(),              
    --           @ErrSeverity = Error_severity()              
    --    RAISERROR(@ErrorMsg,@ErrSeverity,1)              
    --END CATCH              
END
