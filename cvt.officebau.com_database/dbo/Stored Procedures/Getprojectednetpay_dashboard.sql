﻿/****************************************************************************     
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   :     
MODIFIED DATE  :     
<summary>            
 [Getprojectednetpay_dashboard] 1,12,2017,'1,2,3,4,5,6,7,8,9,11,12','BU',71  
</summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getprojectednetpay_dashboard] (@DomainID     INT,
                                                      @MonthId      INT,
                                                      @Year         INT,
                                                      @BusinessUnit VARCHAR(100),
                                                      @Type         VARCHAR(50),
                                                      @EmployeeID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @Date DATETIME=Getdate()
          DECLARE @CurrentMonth DATE = Dateadd(month, 1, Cast(( CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'
                                     + CONVERT(VARCHAR(10), @Year) ) AS DATE))
          DECLARE @Days INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth));
          DECLARE @DataSource TABLE
            (
               [Value] NVARCHAR(128)
            )

          INSERT INTO @DataSource
                      ([Value])
          SELECT Item
          FROM   dbo.Splitstring ((SELECT businessunitid
                                   FROM   tbl_employeemaster
                                   WHERE  id = @EmployeeID
                                          AND DomainID = @DomainID), ',')
          WHERE  Isnull(Item, '') <> '';

          WITH Business
               AS (SELECT LEFT(Datename( month, Dateadd( month, MonthId, -1 )), 3)
                          + ' ' + Cast(( YearId ) AS VARCHAR(50)) AS BUMonthYear,
                          Round(Sum(p.Amount), 0)                 AS BUNet
                   FROM   tbl_Pay_EmployeePayroll ep
                          LEFT JOIN tbl_Pay_EmployeePayrollDetails p
                                 ON p.PayrollID = ep.ID
                                    AND p.Isdeleted = 0
                                    AND p.ComponentID = (SELECT Value
                                                         FROM   tbl_DashBoardConfiguration
                                                         WHERE  [Key] = 'Tile1ID'
                                                                AND DomainID = @DomainID)
                          LEFT JOIN tbl_EmployeeMaster emp
                                 ON emp.ID = ep.EmployeeId
                          LEFT JOIN tbl_BusinessUnit bu
                                 ON bu.ID = emp.BaseLocationID
                   WHERE  ep.DomainID = @DomainID
                          AND ep.IsProcessed = 1
                          AND ep.IsDeleted = 0
                          AND ( ( @BusinessUnit IS NULL
                                   OR @BusinessUnit = '' )
                                 OR ( bu.NAME = @BusinessUnit ) )
                          AND emp.BaselocationID IN(SELECT *
                                                    FROM   @DataSource)
                   GROUP  BY ( LEFT(Datename(MONTH, Dateadd(month, MonthId, -1)), 3) ) + ' ' + Cast(( YearId ) AS VARCHAR(50))),
               Designation
               AS (SELECT LEFT(Datename( month, Dateadd( month, MonthId, -1 )), 3)
                          + ' ' + Cast(( YearId ) AS VARCHAR(50)) AS BUMonthYear,
                          Round(Sum(p.Amount), 0)                 AS DesignNet
                   FROM   tbl_Pay_EmployeePayroll ep
                          LEFT JOIN tbl_Pay_EmployeePayrollDetails p
                                 ON p.PayrollID = ep.ID
                                    AND p.Isdeleted = 0
                                    AND p.ComponentID = (SELECT Value
                                                         FROM   tbl_DashBoardConfiguration
                                                         WHERE  [Key] = 'Tile1ID'
                                                                AND DomainID = @DomainID)
                          LEFT JOIN tbl_EmployeeMaster emp
                                 ON emp.ID = ep.EmployeeId
                          LEFT JOIN tbl_Designation de
                                 ON de.ID = emp.DesignationID
                   WHERE  ep.DomainID = @DomainID
                          AND ep.IsProcessed = 1
                          AND ep.IsDeleted = 0
                          AND ( ( @BusinessUnit IS NULL
                                   OR @BusinessUnit = '' )
                                 OR ( Isnull(de.NAME, 'Others') = @BusinessUnit ) )
                          AND emp.BaselocationID IN(SELECT *
                                                    FROM   @DataSource)
                   GROUP  BY ( LEFT(Datename(MONTH, Dateadd(month, MonthId, -1)), 3) ) + ' ' + Cast(( YearId ) AS VARCHAR(50))),
               Department
               AS (SELECT LEFT(Datename( month, Dateadd( month, MonthId, -1 )), 3)
                          + ' ' + Cast(( YearId ) AS VARCHAR(50)) AS BUMonthYear,
                          Round(Sum(ISNULL(p.Amount, 0)), 0)      AS DepartmentNet
                   FROM   tbl_Pay_EmployeePayroll ep
                          LEFT JOIN tbl_Pay_EmployeePayrollDetails p
                                 ON p.PayrollID = ep.ID
                                    AND p.Isdeleted = 0
                                    AND p.ComponentID = (SELECT Value
                                                         FROM   tbl_DashBoardConfiguration
                                                         WHERE  [Key] = 'Tile1ID'
                                                                AND DomainID = @DomainID)
                          LEFT JOIN tbl_EmployeeMaster emp
                                 ON emp.ID = ep.EmployeeId
                          LEFT JOIN tbl_Department dep
                                 ON dep.ID = emp.DepartmentID
                   WHERE  ep.DomainID = @DomainID
                          AND ep.IsProcessed = 1
                          AND ep.IsDeleted = 0
                          AND ( ( @BusinessUnit IS NULL
                                   OR @BusinessUnit = '' )
                                 OR ( Isnull(dep.NAME, 'Others') = @BusinessUnit ) )
                          AND emp.BaselocationID IN(SELECT *
                                                    FROM   @DataSource)
                   GROUP  BY ( LEFT(Datename(MONTH, Dateadd(month, MonthId, -1)), 3) ) + ' ' + Cast(( YearId ) AS VARCHAR(50))),
               MonthYear
               AS (SELECT MonthYear,
                          RowNo
                   FROM   dbo.Fngetmonthyear (Datepart(month, Dateadd(month, @MonthId, -1)), Cast(( @Year ) AS VARCHAR(50))))
          SELECT tem.MonthYear               AS MonthYear,
                 CASE
                   WHEN( @Type = 'Business Unit' ) THEN
                     Isnull(BU.BUNet, 0)
                   WHEN( @Type = 'Designation' ) THEN
                     Isnull(DE.DesignNet, 0)
                   WHEN( @Type = 'Department' ) THEN
                     Isnull(DEP.DepartmentNet, 0)
                   ELSE
                     0
                 END                         AS Net,
                 Row_number()
                   OVER(
                     ORDER BY tem.RowNo ASC) AS row
          INTO   #temp
          FROM   MonthYear tem
                 LEFT JOIN Business BU
                        ON tem.MonthYear = BU.BUMonthYear
                 LEFT JOIN Designation DE
                        ON tem.MonthYear = DE.BUMonthYear
                 LEFT JOIN Department DEP
                        ON tem.MonthYear = DEP.BUMonthYear

          SELECT MonthYear,
                 CASE
                   WHEN(( NET = 0
                          AND row = 4 )) THEN
                     (SELECT Round(( NET / @Days * Day(@Date) ), 0)
                      FROM   #temp
                      WHERE  row = 3)
                   ELSE
                     NET
                 END AS NET
          FROM   #temp
          WHERE  row < 4
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
