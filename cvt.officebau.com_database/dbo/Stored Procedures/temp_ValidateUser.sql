﻿/****************************************************************************   
CREATED BY   :
CREATED DATE  :
MODIFIED BY   :
MODIFIED DATE  :
 <summary>          
     [temp_ValidateUser] 'user@mass.com','eWK9XoF32iP4UxA86wE+9LRLWKCN4NItMXvaPr8LR/Q=','user' 
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[temp_ValidateUser] (@UserName VARCHAR(100),
                                           @Password VARCHAR(100))
AS

BEGIN
    SET NOCOUNT ON;

    DECLARE @UserMessage VARCHAR(100),
            @UserID      INT = 0,
            @DomainID    INT = 0,
            @CompanyName VARCHAR(150),
            @Currency    VARCHAR(150),
            @LastLogin   VARCHAR(30) = '',
            @FirstName   VARCHAR(100)

    BEGIN TRY

        IF (SELECT Count(1)
            FROM   tblEmployeeMaster
            WHERE  IsActive = 0
                   AND HasAccess = 1
                   AND IsDeleted = 0
                   AND EmailID = @UserName) = 0
          BEGIN
              SET @UserMessage = (SELECT [Message]
                                  FROM   tblErrorMessage
                                  WHERE  [Type] = 'Warning'
                                         AND Code = 'USER_INVAL'
                                         AND IsDeleted = 0) --'Not a valid user'
              GOTO OutputResult
          END

        SET @UserID = (SELECT ID
                       FROM   tblEmployeeMaster
                       WHERE  IsActive = 0
                              AND IsDeleted = 0
                              AND EmailID = @UserName)

        --IF (SELECT Count(1)
        --    FROM   tblLoginHistory
        --    WHERE  IsActive = 0
        --           AND UserID = @UserID) > 0
        --  BEGIN
        --      SET @UserMessage ='User Already Login in other Device'
        --      GOTO OutputResult
        --  END
        IF (SELECT Count(1)
            FROM   tblLogin
            WHERE  UserID = @UserID
                   AND Password = @Password) = 0
          BEGIN
              SET @UserMessage = (SELECT [Message]
                                  FROM   tblErrorMessage
                                  WHERE  [Type] = 'Warning'
                                         AND Code = 'CHK_PASS'
                                         AND IsDeleted = 0) --'Invalid Password'

              GOTO OutputResult
          END
        ELSE
          BEGIN
              SET @UserMessage = (SELECT [Message]
                                  FROM   tblErrorMessage
                                  WHERE  [Type] = 'Warning'
                                         AND Code = 'USER_VALID'
                                         AND IsDeleted = 0) --'Valid'
              SET @DomainID = (SELECT DomainID
                               FROM   tblEmployeeMaster
                               WHERE  IsActive = 0
                                      AND IsDeleted = 0
                                      AND EmailID = @UserName)
              SET @FirstName = (SELECT FirstName
                                FROM   tblEmployeeMaster
                                WHERE  IsActive = 0
                                       AND IsDeleted = 0
                                       AND EmailID = @UserName)
              SET @CompanyName = (SELECT NAME
                                  FROM   tblCompany
                                  WHERE  IsActive = 0
                                         AND IsDeleted = 0
                                         AND ID = @DomainID)
              SET @LastLogin = (SELECT TOP 1 LoginTime
                                FROM   tblLoginHistory TLH
                                       LEFT JOIN tblEmployeeMaster TEM
                                              ON TLH.UserID = TEM.ID
                   WHERE  EmailID = @UserName
                                ORDER  BY LoginTime DESC)
              SET @Currency = (SELECT CurrencyCode
                               FROM   tblCurrency
                               WHERE  id = ((SELECT Value
                                             FROM   tblApplicationConfiguration
                                             WHERE  IsDeleted = 0
                                                    AND code = 'CURNY'
                                                    AND DomainID = @DomainID)))

              GOTO OutputResult
          END

        OUTPUTRESULT:

        SET @UserName = Upper(LEFT(@UserName, 1))
                        + RIGHT(@UserName, Len(@UserName) - 1)

        SELECT @UserMessage AS UserMessage,
               @UserID      AS UserID,
               @DomainID    AS DomainID,
               @UserName    AS UserName,
               @CompanyName AS CompanyName,
               @LastLogin   AS LastLogin,
               @Currency    AS CurrencySymbol,
               @FirstName   AS FirstName

        --Delete UnReference Uploaded Files
          EXEC DeleteUnReferenceUploadedFiles
            @UserName
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMsg    VARCHAR(100),
                @ErrSeverity TINYINT
        SELECT @ErrorMsg = Error_message(),
               @ErrSeverity = Error_severity()
        RAISERROR(@ErrorMsg,@ErrSeverity,1)
    END CATCH
END
