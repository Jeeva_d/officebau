﻿/****************************************************************************     
CREATED BY    :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE :   
 <summary>   
  [Getdashboardbankdetails] 1  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getdashboardbankdetails] (@DomainID INT)
AS
  BEGIN
      BEGIN TRY
          BEGIN TRANSACTION

          SELECT DISTINCT TBRS.BankID,
                          B.DisplayName as NAME,
                          ( (SELECT Isnull(Sum(amount), 0)
                             FROM   tblbrs
                             WHERE  isdeleted = 0
                                    AND IsReconsiled = 1
                                    AND SourceType IN((SELECT ID
                                                       FROM   tblMasterTypes
                                                       WHERE  BankID = TBRS.BankID
                                                              AND NAME IN ( 'InvoiceReceivable', 'Receive Payment', 'ToBank' )))) - (SELECT Isnull(Sum(amount), 0)
                                                                                                                                     FROM   tblbrs
                                                                                                                                     WHERE  isdeleted = 0
                                                                                                                                            AND IsReconsiled = 1
                                                                                                                                            AND SourceType IN((SELECT ID
                                                                                                                                                               FROM   tblMasterTypes
                                                                                                                                                               WHERE  BankID = TBRS.BankID
                                                                                                                                                                      AND NAME IN ( 'PayBill', 'FromBank', 'Bill', 'Expense', 'Make Payment' )))) ) + B.OpeningBalance AS AvailableBalance
          FROM   tblBank B
                 LEFT JOIN TblBrs TBRS
                        ON TBRS.bankID = B.ID
          WHERE  B.isdeleted = 0
                 AND B.DomainID = @DomainID
          UNION
          SELECT ID,
                 'Cash',
                 AvailableCash
          FROM   tblcashBucket
          WHERE  DomainID = @DomainID
          ORDER  BY B.DisplayName ASC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
