﻿/****************************************************************************     
CREATED BY    : Priya K  
CREATED DATE  : 20-Nov-2018
MODIFIED BY   :    
MODIFIED DATE :    
 <summary>  
 DeleteStockAdjustment 1006,1
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteStockAdjustment] (@ID        INT,
                                       @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT VARCHAR(100) = 'Operation failed!'

          IF EXISTS (SELECT 1
                         FROM   tbl_InventoryStockAdjustment
                         WHERE  ID = @ID
                                AND IsDeleted = 0
                                AND DomainID = @DomainID)            
            BEGIN
                UPDATE tbl_InventoryStockAdjustment
                SET    IsDeleted = 1
                WHERE  @ID = ID
                       AND DomainID = @DomainID
                 SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Information'
                                      AND Code = 'RCD_DEL'
                                      AND IsDeleted = 0) --'Deleted Successfully'  

                UPDATE tbl_InventoryStockAdjustmentItems
                SET    IsDeleted = 1
                WHERE  AdjustmentID = @ID
                       AND DomainID = @DomainID

                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Information'
                                      AND Code = 'RCD_DEL'
                                      AND IsDeleted = 0) --'Deleted Successfully'  
               
            END

          FINISH:

          SELECT @OUTPUT

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
