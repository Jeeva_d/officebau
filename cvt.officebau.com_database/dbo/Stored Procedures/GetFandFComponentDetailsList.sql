﻿/****************************************************************************                         
CREATED BY   : Ajith                       
CREATED DATE  : 21-MAY-2019                        
MODIFIED BY   :                       
MODIFIED DATE  :                      
 <summary>                         
      GetFandFComponentDetailsList 0, 2,30,15,0, 2,1 
 </summary>                                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetFandFComponentDetailsList] (@id                        INT,
                                                      @EmployeeID                INT,
                                                      @TotalDaysInMonth          INT,
                                                      @DaysPaid                  INT,
                                                      @LeaveEncashedDays         INT,
                                                      @FandFComponentDetailsList [FANDFCOMPONENTDETAILSLIST] readonly,
                                                      @SessionID                 INT,
                                                      @DomainID                  INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @CompanyPayStucId INT = (SELECT TOP 1 ID
             FROM   tbl_Pay_EmployeePayStructure
             WHERE  IsDeleted = 0
                    AND EmployeeID = @EmployeeID
             ORDER  BY EffectiveFrom DESC)

          CREATE TABLE #tmpFandFComponentDetails
            (
               id                INT,
               FandFId           INT,
               [Type]            VARCHAR(100),
               [Description]     VARCHAR(500),
               Amount            MONEY,
               IsEditableAmount  BIT,
               Remarks           VARCHAR(500),
               IsEditableRemarks BIT,
               IsDeleted         BIT,
               Ordinal           INT
            )

          INSERT INTO #tmpFandFComponentDetails
          SELECT *
          FROM   @FandFComponentDetailsList
          ORDER  BY Ordinal

          IF NOT EXISTS(SELECT 1
                        FROM   #tmpFandFComponentDetails)
            BEGIN
                --SET @TotalDaysInMonth = ( CASE
                --                            WHEN @TotalDaysInMonth = 0 THEN
                --                              1
                --                            ELSE
                --                              @TotalDaysInMonth
                --                          END )
                CREATE TABLE #tmpEmployeePayStructurecomponents
                  (
                     ID                 INT,
                     EmployeeID         INT,
                     EmployeeName       VARCHAR(100),
                     CompanyPayStucId   INT,
                     CompanyPayStucName VARCHAR(100),
                     EffectiveFrom      DATETIME,
                     Gross              MONEY,
                     Code               VARCHAR(50),
                     [Description]      VARCHAR(100),
                     Ordinal            INT,
                     ComponentId        INT,
                     value              MONEY,
                     Amount             MONEY,
                     IsEditable         BIT,
                     [Type]             VARCHAR(100)
                  )

                INSERT INTO #tmpEmployeePayStructurecomponents
                EXEC [Pay_SearchEmployeePayStructurecomponents]
                  @CompanyPayStucId

                IF ( @id = 0 )
                  BEGIN
                      ---- CTC Salary Details
                      INSERT INTO #tmpFandFComponentDetails
                      SELECT 0                                                         AS ID,
                             @id                                                       AS FandFId,
                             'CTC Salary Details'                                      AS [Type],
                             [Description]                                             AS [Description],
                             amount                                                    AS Amount,
                             'false'                                                   AS IsEditableAmount,
                             ( ( ISNULL(Amount, 0) / @TotalDaysInMonth ) * @DaysPaid ) AS Remarks,
                             'false'                                                   AS IsEditableRemarks,
                             'false'                                                   AS IsDeleted,
                             ( CASE
                                 WHEN Code = 'Basic' THEN
                                   1
                                 WHEN Code = 'HRA' THEN
                                   2
                                 WHEN Code = 'STDDED' THEN
                                   3
                                 WHEN Code = 'SPLALL' THEN
                                   4
                                 ELSE
                                   5
                               END )                                                   AS Ordinal
                      FROM   #tmpEmployeePayStructurecomponents
                      WHERE  Code IN ( 'Basic', 'HRA', 'STDDED', 'SPLALL' )

                      ---- Sum Of CTC Salary Details
                      INSERT INTO #tmpFandFComponentDetails
                      SELECT 0                           AS ID,
                             @id                         AS FandFId,
                             'CTC Salary Details'        AS [Type],
                             'Gross Earnings'            AS [Description],
                             Sum(ISNULL(Amount, 0))      AS Amount,
                             'false'                     AS IsEditableAmount,
                             Sum(Cast(Remarks AS MONEY)) AS Remarks,
                             'false'                     AS IsEditableRemarks,
                             'false'                     AS IsDeleted,
                             5
                      FROM   #tmpFandFComponentDetails
                      WHERE  [Type] = 'CTC Salary Details'

                      ---- Full and Final Payment Details
                      INSERT INTO #tmpFandFComponentDetails
                      SELECT 0                                AS ID,
                             @id                              AS FandFId,
                             'Full and Final Payment Details' AS [Type],
                             'Notice Period Pay Out'          AS [Description],
                             0                                AS Amount,
                             'true'                           AS IsEditableAmount,
                             ''                               AS Remarks,
                             'true'                           AS IsEditableRemarks,
                             'false'                          AS IsDeleted,
                             6
                      UNION
                      SELECT 0                                                              AS ID,
                             @id                                                            AS FandFId,
                             'Full and Final Payment Details'                               AS [Type],
                             'Leave Encashment'                                             AS [Description],
                             ( ISNULL(Amount, 0) / @TotalDaysInMonth ) * @LeaveEncashedDays AS Amount,
                             'false'                                                        AS IsEditableAmount,
                             ''                                                             AS Remarks,
                             'true'                                                         AS IsEditableRemarks,
                             'false'                                                        AS IsDeleted,
                             7
                      FROM   #tmpEmployeePayStructurecomponents
                      WHERE  Code IN ( 'Basic' )
                      UNION
                      SELECT 0                                AS ID,
                             @id                              AS FandFId,
                             'Full and Final Payment Details' AS [Type],
                             'Incentive'                      AS [Description],
                             0                                AS Amount,
                             'true'                           AS IsEditableAmount,
                             ''                               AS Remarks,
                             'true'                           AS IsEditableRemarks,
                             'false'                          AS IsDeleted,
                             8
                      UNION
                      SELECT 0                                AS ID,
                             @id                              AS FandFId,
                             'Full and Final Payment Details' AS [Type],
                             'Gratuity'                       AS [Description],
                             0                                AS Amount,
                             'false'                          AS IsEditableAmount,
                             ''                               AS Remarks,
                             'false'                          AS IsEditableRemarks,
                             'false'                          AS IsDeleted,
                             9

                      ---- Sum Of Full and Final Payment Details
                      INSERT INTO #tmpFandFComponentDetails
                      SELECT 0                                AS ID,
                             @id                              AS FandFId,
                             'Full and Final Payment Details' AS [Type],
                             'Total FnF Settlement'           AS [Description],
                             Sum(ISNULL(Amount, 0))           AS Amount,
                             'false'                          AS IsEditableAmount,
                             ''                               AS Remarks,
                             'false'                          AS IsEditableRemarks,
                             'false'                          AS IsDeleted,
                             10
                      FROM   #tmpFandFComponentDetails
                      WHERE  [Type] = 'Full and Final Payment Details'

                      ---- Deductions
                      INSERT INTO #tmpFandFComponentDetails
                      SELECT 0                AS ID,
                             @id              AS FandFId,
                             'Deductions'     AS [Type],
                             'Provident Fund' AS [Description],
                             0                AS Amount,
                             'true'           AS IsEditableAmount,
                             ''               AS Remarks,
                             'true'           AS IsEditableRemarks,
                             'false'          AS IsDeleted,
                             11
                      UNION
                      SELECT 0                  AS ID,
                             @id                AS FandFId,
                             'Deductions'       AS [Type],
                             'Professional Tax' AS [Description],
                             0                  AS Amount,
                             'true'             AS IsEditableAmount,
                             ''                 AS Remarks,
                             'true'             AS IsEditableRemarks,
                             'false'            AS IsDeleted,
                             12
                      UNION
                      SELECT 0            AS ID,
                             @id          AS FandFId,
                             'Deductions' AS [Type],
                             'Income Tax' AS [Description],
                             0            AS Amount,
                             'true'       AS IsEditableAmount,
                             ''           AS Remarks,
                             'true'       AS IsEditableRemarks,
                             'false'      AS IsDeleted,
                             13
                      UNION
                      SELECT 0                                                                         AS ID,
                             @id                                                                       AS FandFId,
                             'Deductions'                                                              AS [Type],
                             'Loan'                                                                    AS [Description],
                             ISNULL((SELECT Sum(LM.Amount + InterestAmount)
                                     FROM   tbl_LoanRequest LR
                                            JOIN tbl_LoanAmortization LM
                                              ON LM.IsDeleted = 0
                                                 AND Lm.LoanRequestID = LR.ID
                                                 AND LM.StatusID IN (SELECT ID
                                                                     FROM   tbl_Status
                                                                     WHERE  [type] = 'Amortization'
                                                                            AND Code IN ( 'Open' ))
                                     WHERE  LR.IsDeleted = 0
                                            AND LR.EmployeeID = @EmployeeID
                                            AND LR.StatusID IN (SELECT ID
                                                                FROM   tbl_Status
                                                                WHERE  [type] = 'loan'
                                                                       AND Code IN ( 'Settled' ))), 0) AS Amount,
                             'false'                                                                   AS IsEditableAmount,
                             ''                                                                        AS Remarks,
                             'true'                                                                    AS IsEditableRemarks,
                             'false'                                                                   AS IsDeleted,
                             14
                      UNION
                      SELECT 0                  AS ID,
                             @id                AS FandFId,
                             'Deductions'       AS [Type],
                             'Other Deductions' AS [Description],
                             0                  AS Amount,
                             'true'             AS IsEditableAmount,
                             ''                 AS Remarks,
                             'true'             AS IsEditableRemarks,
                             'false'            AS IsDeleted,
                             15

                      ---- Sum Of Deductions
                      INSERT INTO #tmpFandFComponentDetails
                      SELECT 0                      AS ID,
                             @id                    AS FandFId,
                             'Deductions'           AS [Type],
                             'Gross Deduction'      AS [Description],
                             Sum(ISNULL(Amount, 0)) AS Amount,
                             'false'                AS IsEditableAmount,
                             ''                     AS Remarks,
                             'false'                AS IsEditableRemarks,
                             'false'                AS IsDeleted,
                             16
                      FROM   #tmpFandFComponentDetails
                      WHERE  [Type] = 'Deductions'

                      ---- Sum Of 'Full and Final Settlement
                      INSERT INTO #tmpFandFComponentDetails
                      SELECT 0                                                                                                                            AS ID,
                             @id                                                                                                                          AS FandFId,
                             'Full and Final Settlement'                                                                                                  AS [Type],
                             'Net Amount Payable'                                                                                                         AS [Description],
                             ( (SELECT Sum(CASE
                                             WHEN [Description] = 'Gross Earnings' THEN
                                               Cast(Remarks AS MONEY)
                                             ELSE
                                               ISNULL(Amount, 0)
                                           END)
                                FROM   #tmpFandFComponentDetails
                                WHERE  [Description] IN ( 'Gross Earnings', 'Total FnF Settlement' )) - (SELECT Sum(ISNULL(Amount, 0))
                                                                                                         FROM   #tmpFandFComponentDetails
                                                                                                         WHERE  [Description] IN ( 'Gross Deduction' )) ) AS Amount,
                             'false'                                                                                                                      AS IsEditableAmount,
                             ''                                                                                                                           AS Remarks,
                             'false'                                                                                                                      AS IsEditableRemarks,
                             'false'                                                                                                                      AS IsDeleted,
                             20
                  END
                ELSE
                  BEGIN
                      INSERT INTO #tmpFandFComponentDetails
                      SELECT ID,
                             FAndFid,
                             [Type],
                             [Description],
                             Amount,
                             IsEditableAmount,
                             Remarks,
                             IsEditableRemarks,
                             IsDeleted,
                             Ordinal
                      FROM   tbl_FandFDetails FNF
                      WHERE  FNF.IsDeleted = 0
                             AND FNF.FAndFid = @id
                  END
            END
          ELSE
            BEGIN
                --Update Leave Encashment
                UPDATE tFsndF
                SET    tFsndF.Amount = (SELECT ( ISNULL(amount, 0) / @TotalDaysInMonth ) * @LeaveEncashedDays
                                        FROM   #tmpFandFComponentDetails
                                        WHERE  [Type] = 'CTC Salary Details'
                                               AND [Description] = 'Basic')
                FROM   #tmpFandFComponentDetails tFsndF
                WHERE  tFsndF.[Type] = 'Full and Final Payment Details'
                       AND tFsndF.[Description] = 'Leave Encashment'

                ---- Sum Of CTC Salary Details
                UPDATE tFsndF
                SET    amount = (SELECT Sum(ISNULL(amount, 0))
                                 FROM   #tmpFandFComponentDetails
                                 WHERE  [Type] = 'CTC Salary Details'
                                        AND [Description] != 'Gross Earnings')
                FROM   #tmpFandFComponentDetails tFsndF
                WHERE  tFsndF.[Type] = 'CTC Salary Details'
                       AND tFsndF.[Description] = 'Gross Earnings'

                ---- Sum Of Full and Final Payment Details
                UPDATE tFsndF
                SET    amount = (SELECT Sum(ISNULL(amount, 0))
                                 FROM   #tmpFandFComponentDetails
                                 WHERE  [Type] = 'Full and Final Payment Details'
                                        AND [Description] != 'Total FnF Settlement')
                FROM   #tmpFandFComponentDetails tFsndF
                WHERE  tFsndF.[Type] = 'Full and Final Payment Details'
                       AND tFsndF.[Description] = 'Total FnF Settlement'

                ---- Sum Of Deductions
                UPDATE tFsndF
                SET    tFsndF.amount = (SELECT Sum(ISNULL(amount, 0))
                                        FROM   #tmpFandFComponentDetails
                                        WHERE  [Type] = 'Deductions'
                                               AND [Description] != 'Gross Deduction')
                FROM   #tmpFandFComponentDetails tFsndF
                WHERE  tFsndF.[Type] = 'Deductions'
                       AND tFsndF.[Description] = 'Gross Deduction'

                ---- Sum Of Full and Final Settlement
                UPDATE tFsndF
                SET    tFsndF.amount = ( (SELECT Sum(CASE
                                                       WHEN [Description] = 'Gross Earnings' THEN
                                                         Cast(Remarks AS MONEY)
                                                       ELSE
                                                         ISNULL(amount, 0)
                                                     END)
                                          FROM   #tmpFandFComponentDetails
                                          WHERE  [Description] IN ( 'Gross Earnings', 'Total FnF Settlement' )) - (SELECT Sum(ISNULL(amount, 0))
                                                                                                                   FROM   #tmpFandFComponentDetails
                                                                                                                   WHERE  [Description] IN ( 'Gross Deduction' )) )
                FROM   #tmpFandFComponentDetails tFsndF
                WHERE  tFsndF.[Type] = 'Full and Final Settlement'
                       AND tFsndF.[Description] = 'Net Amount Payable'
            END

          SELECT *
          FROM   #tmpFandFComponentDetails
          ORDER  BY Ordinal
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
