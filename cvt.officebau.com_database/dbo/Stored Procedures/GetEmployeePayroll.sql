﻿/**************************************************************************** 
CREATED BY    		:	
CREATED DATE		:	
MODIFIED BY			:	Naneeshwar.M
MODIFIED DATE		:	
 <summary>  
 [GetEmployeePayroll] 4,2017,5
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetEmployeePayroll] (@MonthId    INT,
                                             @Year       INT,
                                             @Employeeid INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @TDSYear  INT,
              @DomainID INT

      -- Get Payroll Component Config Value
      SELECT *
      INTO   #tmpParollConfigValue
      FROM   [dbo].[Fn_PayrollComponentConfigValue]()

      SET @DomainID= (SELECT DomainID
                      FROM   tbl_employeemaster
                      WHERE  id = @Employeeid)

      IF( @MonthID BETWEEN 1 AND 3 )
        SET @TDSYear=(SELECT NAME
                      FROM   tbl_FinancialYear
                      WHERE  NAME = @Year - 1
                             AND DomainID = @DomainID)
      ELSE
        SET @TDSYear =(SELECT NAME
                       FROM   tbl_FinancialYear
                       WHERE  NAME = @Year
                              AND DomainID = @DomainID)

      DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'
        + CONVERT(VARCHAR(10), @Year)
      DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth))

      BEGIN TRY
          BEGIN
              SELECT Row_number()
                       OVER (
                         PARTITION BY ep.EmployeeId
                         ORDER BY EffectiveFrom DESC)                                                                                               AS ORDERNUMBER,
                     ( CASE
                         WHEN ( Datepart(MM, DOJ) = @MonthId
                                AND Datepart(YYYY, DOJ) = @year ) THEN
                           ( @Workdays - ( Datepart(D, DOJ) - 1 ) - Isnull(LopDays, 0) )
                         ELSE
                           @Workdays - Isnull(LopDays, 0)
                       END ) - ( CASE
                                   WHEN ( Datepart(MM, InactiveFrom) = @MonthId
                                          AND Datepart(YYYY, InactiveFrom) = @year ) THEN
                                     ( ( @Workdays ) - Day(InactiveFrom) )
                                   ELSE
                                     0
                                 END )                                                                                                              AS Workdays,
                     @Workdays                                                                                                                      AS ActualWorkdays,
                     Isnull(Isnull(LA.Amount, 0) + ( CASE
                                                       WHEN Isnull(LA.InterestAmount, 0) = 0 THEN
                                                         0
                                                       ELSE
                                                         Isnull(LA.InterestAmount, 0)
                                                     END ) + Isnull((SELECT ob.DueBalance
                                                                     FROM   tbl_LoanAmortization ob
                                                                     WHERE  ob.LoanRequestID = LR.ID
                                                                            AND ob.DomainID = @DomainID
                                                                            AND Month(ob.DueDate) = Month(Dateadd(month, -1, @CurrentMonth))
                                                                            AND Year(ob.DueDate) = Year(Dateadd(month, -1, @CurrentMonth))), 0), 0) AS LoanAmount,
                     EMP.Code                                                                                                                       AS Code,
                     EMP.FullName                                                                                                                   AS EmployeeName,
                     EMP.DOJ                                                                                                                        AS DOJ,
                     EMP.BaseLocationID                                                                                                             AS BaseLocationID,
                     EMP.RegionID                                                                                                                   AS RegionID,
                     CASE
                       WHEN TDS.TDSAmount < 0 THEN
                         0
                       ELSE
                         TDS.TDSAmount
                     END                                                                                                                            AS TDSAmount,
                     --ep.*
                     ep.Id,
                     ep.CompanyPayStubId,
                     ep.EmployeeId,
                     ep.Gross,
                     ep.Basic,
                     ep.HRA,
                     ep.MedicalAllowance,
                     ep.Conveyance,
                     ep.SplAllow,
                     dbo.Fn_getComponentConfigValue(ep.EEPF, 'EEPF', emp.BaseLocationID, @DomainID)                                                 AS EEPF,
                     dbo.Fn_getComponentConfigValue(ep.EEESI, 'EEESI', emp.BaseLocationID, @DomainID)                                               AS EEESI,
                     dbo.Fn_getComponentConfigValue(ep.PT, 'PT', emp.BaseLocationID, @DomainID)                                                     AS PT,
                     dbo.Fn_getComponentConfigValue(ep.TDS, 'TDS', emp.BaseLocationID, @DomainID)                                                   AS TDS,
                     dbo.Fn_getComponentConfigValue(ep.ERPF, 'ERPF', emp.BaseLocationID, @DomainID)                                                 AS ERPF,
                     dbo.Fn_getComponentConfigValue(ep.ERESI, 'ERESI', emp.BaseLocationID, @DomainID)                                               AS ERESI,
                     ep.Bonus,
                     dbo.Fn_getComponentConfigValue(ep.Gratuity, 'Gratuity', emp.BaseLocationID, @DomainID)                                         AS Gratuity,
                     dbo.Fn_getComponentConfigValue(ep.PLI, 'PLI', emp.BaseLocationID, @DomainID)                                                   AS PLI,
                     dbo.Fn_getComponentConfigValue(ep.Mediclaim, 'Mediclaim', emp.BaseLocationID, @DomainID)                                       AS Mediclaim,
                     dbo.Fn_getComponentConfigValue(ep.MobileDataCard, 'MobileDataCard', emp.BaseLocationID, @DomainID)                             AS MobileDataCard,
                     ep.EffectiveFrom,
                     ep.CreatedBy,
                     ep.CreatedOn,
                     ep.ModifiedBy,
                     ep.ModifiedOn,
                     ep.DomainId,
                     ep.IsDeleted,
                     dbo.Fn_getComponentConfigValue(ep.OtherPerks, 'OtherPerks', emp.BaseLocationID, @DomainID)                                     AS OtherPerks,
                     ep.CTC,
                     ep.IsPFRequired,
                     ep.IsESIRequired,
                     dbo.Fn_getComponentConfigValue(ep.EducationAllow, 'EducationAllow', emp.BaseLocationID, @DomainID)                             AS EducationAllow,
                     dbo.Fn_getComponentConfigValue(ep.MonsoonAllow, 'OverTime', emp.BaseLocationID, @DomainID)                                     AS MonsoonAllow,
                     ep.MagazineAllow
              INTO   #TableResult
              FROM   tbl_EmployeePayStructure ep
                     LEFT JOIN tbl_lopdetails P
                            ON P.EmployeeId = ep.EmployeeId
                               AND P.Monthid = @MonthId
                               AND P.year = @Year
                     JOIN tbl_EmployeeMaster emp
                       ON emp.ID = ep.EmployeeId
                          AND emp.id = @Employeeid
                     LEFT JOIN tbl_LoanRequest LR
                            ON LR.EmployeeID = ep.EmployeeId
                               AND LR.StatusID = (SELECT ID
                                                  FROM   tbl_Status
                                                  WHERE  Code = 'Settled'
                                                         AND type = 'Loan')
                     LEFT JOIN tbl_LoanAmortization LA
                            ON LA.LoanRequestID = LR.ID
                               AND Year(LA.DueDate) = @Year
                               AND Month(LA.DueDate) = @MonthId
                               AND LA.StatusID = (SELECT ID
                                                  FROM   tbl_Status
                                                  WHERE  Code = 'Open'
                                                         AND type = 'Amortization')
                     LEFT JOIN tbl_TDS TDS
                            ON TDS.employeeID = @EmployeeID
                               AND TDS.MonthId = @MonthId
                               AND TDS.YearID = (SELECT ID
                                                 FROM   tbl_FinancialYear
                                                 WHERE  NAME = @TDSYear
                                                        AND DomainID = @DomainID)
                               AND TDS.isdeleted = 0
              WHERE  EffectiveFrom < Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @Year - 2000, Dateadd(MONTH, @MonthId - 1, '20000101')))
                                                               + 1, 0))
                     AND ep.DomainID = @DomainID
                     AND ep.EmployeeId NOT IN (SELECT employeeid
                                               FROM   tbl_EmployeePayroll
                                               WHERE  monthid = @MonthId
                                                      AND [yearid] = @Year
                                                      AND isdeleted = 0
                                                      AND DomainID = @DomainID)
                     AND ep.IsDeleted = 0
                     AND ( Isnull(emp.IsActive, '') = 0
                            OR ( Month(InactiveFrom) = @MonthId
                                 AND Year(InactiveFrom) = @Year ) )
                     AND emp.HasAccess = 1
                     AND ( emp.BaseLocationID <> 0
                            OR emp.BaseLocationID <> '' )
                     AND emp.EmploymentTypeID = (SELECT ID
                                                 FROM   tbl_EmployementType
                                                 WHERE  NAME = 'Direct'
                                                        AND DomainID = @DomainID)

              SELECT R.Id                                                                                            AS EmployeePaystructureID,
                     R.EmployeeName                                                                                  AS EmployeeName,
                     0                                                                                               AS ID,
                     R.employeeid                                                                                    AS EmployeeID,
                     @MonthId                                                                                        AS Monthid,
                     @Year                                                                                           AS YearId,
                     Cast(R.Workdays AS DECIMAL(18, 2))                                                              AS [No of days payable],
                     Round(Gross / R.ActualWorkdays * R.Workdays, 0)                                                 AS Gross,
                     Round(R.basic / R.ActualWorkdays * R.Workdays, 0)                                               AS Basic,
                     Round(R.HRA / R.ActualWorkdays * R.Workdays, 0)                                                 AS HRA,
                     Round(R.MedicalAllowance / R.ActualWorkdays * R.Workdays, 0)                                    AS MedicalAllowance,
                     Round(R.Conveyance / R.ActualWorkdays * R.Workdays, 0)                                          AS Conveyance,
                     Round(( Gross - ( Round(R.Basic, 0)
                                       + Round(R.MedicalAllowance, 0)
                                       + Round(R.HRA, 0) + Round(R.Conveyance, 0)
                                       + Round(Isnull(R.EducationAllow, 0), 0)
                                       + Round(Isnull(R.MagazineAllow, 0), 0)
                                       + Round(Isnull(R.MonsoonAllow, 0), 0) ) ) / R.ActualWorkdays * R.Workdays, 0) AS SplAllow,
                     Round(Isnull(R.EducationAllow, 0) / R.ActualWorkdays * R.Workdays, 0)                           AS EducationAllowance,
                     Round(Isnull(R.MagazineAllow, 0) / R.ActualWorkdays * R.Workdays, 0)                            AS PaperMagazine,
                     Round(Isnull(R.MonsoonAllow, 0), 0)                                                             AS MonsoonAllow,
                     0                                                                                               AS OverTime,
                     ( CASE
                         WHEN dbo.Fn_CheckComponentConfigValue('EEPF', R.BaseLocationID, @DomainID) = 1 THEN
                           ( CASE
                               WHEN ( R.IsPFRequired = 0 ) THEN
                                 0
                               ELSE
                                 CASE
                                   WHEN dbo.Fn_getpaidgross(Gross, R.basic, R.Conveyance, R.MedicalAllowance, R.HRA, R.EducationAllow, R.MagazineAllow, R.Workdays, R.ActualWorkdays) > PAYEPF.Limit THEN
                                     PAYEPF.Value
                                   ELSE
                                     Round(( ( Gross - ( R.HRA + R.MedicalAllowance ) ) * PAYEPF.Percentage / 100 ) / R.ActualWorkdays * R.Workdays, 0)
                                 END
                             END )
                         ELSE
                           0
                       END )                                                                                         AS EEPF,
                     ( CASE
                         WHEN dbo.Fn_CheckComponentConfigValue('EEESI', R.BaseLocationID, @DomainID) = 1 THEN
                           ( CASE
                               WHEN ( R.IsESIRequired = 0 ) THEN
                                 0
                               ELSE
                                 --CASE
                                 --  WHEN( (Datediff(MONTH, (SELECT TOP 1 EffectiveFrom
                                 --                         FROM   tbl_EmployeePayStructure e
                                 --                         WHERE  e.EffectiveFrom < R.EffectiveFrom
                                 --                                AND EmployeeId = R.EmployeeId
                                 --                                AND IsDeleted = 0
                                 --                         ORDER  BY EffectiveFrom DESC), R.EffectiveFrom) < 6 )  ) THEN Ceiling(( Gross * PAYEESI.Percentage / 100 ) / R.ActualWorkdays * R.Workdays / 1)
                                 --  ELSE
                                 CASE
                                   WHEN ( ( Gross ) >= PAYEESI.Limit ) THEN
                                     PAYEESI.Value
                                   ELSE
                                     Ceiling(( ( Gross ) * PAYEESI.Percentage / 100 ) / R.ActualWorkdays * R.Workdays / 1)
                                 END
                             --END
                             END )
                         ELSE
                           0
                       END )                                                                                         AS EEESI,
                     ( CASE
                         WHEN dbo.Fn_CheckComponentConfigValue('PT', R.BaseLocationID, @DomainID) = 1 THEN
                           Round(dbo.Fn_getptamount(Gross, @monthID, @Year, R.DOJ, R.BaselocationID, R.Workdays, R.ActualWorkdays, @DomainID), 0)
                         ELSE
                           0
                       END )                                                                                         AS PT,
                     Round (Isnull(TDSAmount, 0), 0)                                                                 AS TDS,
                     ( CASE
                         WHEN dbo.Fn_CheckComponentConfigValue('ERPF', R.BaseLocationID, @DomainID) = 1 THEN
                           ( CASE
                               WHEN ( R.IsPFRequired = 0 ) THEN
                                 0
                               ELSE
                                 CASE
                                   WHEN dbo.Fn_getpaidgross(Gross, R.basic, R.Conveyance, R.MedicalAllowance, R.HRA, R.EducationAllow, R.MagazineAllow, R.Workdays, R.ActualWorkdays) > PAYERPF.Limit THEN
                                     PAYERPF.Value
                                   ELSE
                                     Round(( ( Gross - ( R.HRA + R.MedicalAllowance ) ) * PAYERPF.Percentage / 100 ) / R.ActualWorkdays * R.Workdays, 0)
                                 END
                             END )
                         ELSE
                           0
                       END )                                                                                         AS ERPF,
                     ( CASE
                         WHEN dbo.Fn_CheckComponentConfigValue('ERESI', R.BaseLocationID, @DomainID) = 1 THEN
                           ( CASE
                               WHEN ( R.IsESIRequired = 0 ) THEN
                                 0
                               ELSE
                                 --CASE
                                 --  WHEN( Datediff(MONTH, (SELECT TOP 1 EffectiveFrom
                                 --                         FROM   tbl_EmployeePayStructure e
                                 --                         WHERE  e.EffectiveFrom < R.EffectiveFrom
                                 --                                AND EmployeeId = R.EmployeeId
                                 --                                AND IsDeleted = 0
                                 --                         ORDER  BY EffectiveFrom DESC), R.EffectiveFrom) < 6 )THEN Ceiling(( Gross * PAYERESI.Percentage / 100 ) / R.ActualWorkdays * R.Workdays / 1)
                                 --  ELSE
                                 CASE
                                   WHEN ( ( Gross ) >= ( PAYERESI.Limit ) )THEN
                                     ( PAYERESI.Value )
                                   ELSE
                                     Ceiling(( ( Gross ) * PAYERESI.Percentage / 100 ) / R.ActualWorkdays * R.Workdays)
                                 END
                             --END
                             END )
                         ELSE
                           0
                       END )                                                                                         AS ERESI,
                     Round(Bonus / R.ActualWorkdays * R.Workdays, 0)                                                 AS Bonus,
                     Round(Gratuity, 0)                                                                              AS Gratuity,
                     0                                                                                               AS PLI,
                     0                                                                                               AS Medicalclaim,
                     0                                                                                               AS MobileDataCard,
                     0                                                                                               AS [Other Earning],
                     0                                                                                               AS OtherPerks,
                     0                                                                                               AS Deduction,
                     R.LoanAmount                                                                                    AS Loans,
                     0                                                                                               AS NetSalary,
                     CTC                                                                                             AS CTC,
                     0                                                                                               AS IsProcessed,
                     0                                                                                               AS IsApproved,
                     0                                                                                               AS ApproverID,
                     Round(Bonus / R.ActualWorkdays * R.Workdays, 0)                                                 AS PSBonus,
                     R.BaseLocationID                                                                                AS BaseLocationID,
                     Cast(R.IsESIRequired AS INT)                                                                    AS IsESIRequired
                     --------------
                     ,
                     IsEducationAllow,
                     IsOverTime,
                     IsEEPF,
                     IsEEESI,
                     IsPT,
                     IsTDS,
                     IsERPF,
                     IsERESI,
                     IsGratuity,
                     IsPLI,
                     IsMediclaim,
                     IsMobileDatacard,
                     IsOtherPerks
              FROM   #TableResult R
                     LEFT JOIN tbl_PayrollConfiguration PAYEPF
                            ON PAYEPF.Components = 'EmployeePF'
                               AND PAYEPF.BusinessUnitID = R.RegionID
                     LEFT JOIN tbl_PayrollConfiguration PAYERPF
                            ON PAYERPF.Components = 'EmployerPF'
                               AND PAYERPF.BusinessUnitID = R.RegionID
                     LEFT JOIN tbl_PayrollConfiguration PAYEESI
                            ON PAYEESI.Components = 'EmployeeESI'
                               AND PAYEESI.BusinessUnitID = R.RegionID
                     LEFT JOIN tbl_PayrollConfiguration PAYERESI
                            ON PAYERESI.Components = 'EmployerESI'
                               AND PAYERESI.BusinessUnitID = R.RegionID
                     LEFT JOIN #tmpParollConfigValue tpc
                            ON tpc.BusinessUnitID = r.BaselocationID
              WHERE  ORDERNUMBER = 1
              UNION ALL
              SELECT EPR.EmployeePayStructureID                                                                          AS EmployeePaystructureID,
                     EMP.fullname + ' (' + EMP.Code + ')'                                                                AS EmployeeName,
                     EPR.Id                                                                                              AS ID,
                     EPR.EmployeeId                                                                                      AS EmployeeID,
                     EPR.MonthId                                                                                         AS Monthid,
                     EPR.YearId                                                                                          AS YearId,
                     noofdayspayable                                                                                     AS [No of days payable],
                     GrossEarning                                                                                        AS Gross,
                     EPR.Basic                                                                                           AS basic,
                     EPR.HRA                                                                                             AS HRA,
                     EPR.MedicalAllowance                                                                                AS MedicalAllowance,
                     EPR.Conveyance                                                                                      AS Conveyance,
                     EPR.SplAllow                                                                                        AS SplAllow,
                     dbo.Fn_getComponentConfigValue(EPR.EducationAllow, 'EducationAllow', EPR.BaseLocationID, @DomainID) AS EducationAllowance,
                     EPR.MagazineAllow                                                                                   AS PaperMagazine,
                     EPR.MonsoonAllow                                                                                    AS MonsoonAllow,
                     dbo.Fn_getComponentConfigValue(EPR.OverTime, 'OverTime', EPR.BaseLocationID, @DomainID)             AS OverTime,
                     dbo.Fn_getComponentConfigValue(EPR.EEPF, 'EEPF', EPR.BaseLocationID, @DomainID)                     AS EEPF,
                     dbo.Fn_getComponentConfigValue(EPR.EEESI, 'EEESI', EPR.BaseLocationID, @DomainID)                   AS EEESI,
                     dbo.Fn_getComponentConfigValue(EPR.PT, 'PT', EPR.BaseLocationID, @DomainID)                         AS PT,
                     dbo.Fn_getComponentConfigValue(EPR.TDS, 'TDS', EPR.BaseLocationID, @DomainID)                       AS TDS,
                     dbo.Fn_getComponentConfigValue(EPR.ERPF, 'ERPF', EPR.BaseLocationID, @DomainID)                     AS ERPF,
                     dbo.Fn_getComponentConfigValue(EPR.ERESI, 'ERESI', EPR.BaseLocationID, @DomainID)                   AS ERESI,
                     EPR.Bonus                                                                                           AS Bonus,
                     dbo.Fn_getComponentConfigValue(EPR.Gratuity, 'Gratuity', EPR.BaseLocationID, @DomainID)             AS Gratuity,
                     dbo.Fn_getComponentConfigValue(EPR.PLI, 'PLI', EPR.BaseLocationID, @DomainID)                       AS PLI,
                     dbo.Fn_getComponentConfigValue(EPR.Mediclaim, 'Mediclaim', EPR.BaseLocationID, @DomainID)           AS Medicalclaim,
                     dbo.Fn_getComponentConfigValue(EPR.MobileDataCard, 'MobileDataCard', EPR.BaseLocationID, @DomainID) AS MobileDataCard,
                     EPR.OtherEarning                                                                                    AS [Other Earning],
                     dbo.Fn_getComponentConfigValue(EPR.OtherPerks, 'OtherPerks', EPR.BaseLocationID, @DomainID)         AS OtherPerks,
                     OtherDeduction                                                                                      AS Deduction,
                     Loans                                                                                               AS Loans,
                     NetSalary                                                                                           AS NetSalary,
                     EPR.CTC                                                                                             AS CTC,
                     IsProcessed                                                                                         AS IsProcessed,
                     IsApproved                                                                                          AS IsApproved,
                     [ApproverId]                                                                                        AS ApproverID,
                     (SELECT Bonus
                      FROM   tbl_EmployeePayStructure
                      WHERE  EmployeeId = EMP.ID
                             AND IsDeleted = 0
                             AND ID = EPR.EmployeePayStructureID)                                                        AS PSBonus,
                     EMP.BaseLocationID                                                                                  AS BaseLocationID,
                     Cast(eps.IsESIRequired AS INT)                                                                      AS IsESIRequired
                     --------------
                     ,
                     IsEducationAllow,
                     IsOverTime,
                     IsEEPF,
                     IsEEESI,
                     IsPT,
                     IsTDS,
                     IsERPF,
                     IsERESI,
                     IsGratuity,
                     IsPLI,
                     IsMediclaim,
                     IsMobileDatacard,
                     IsOtherPerks
              FROM   tbl_EmployeePayroll EPR
                     JOIN tbl_EmployeeMaster EMP
                       ON EPR.EmployeeId = EMP.ID
                          AND EMP.IsDeleted = 0
                     JOIN tbl_EmployeePayStructure eps
                       ON eps.EmployeeId = EPR.EmployeeId
                          AND eps.Id = EPR.EmployeePayStructureID
                     LEFT JOIN #tmpParollConfigValue tpc
                            ON tpc.BusinessUnitID = EPR.BaselocationID
              WHERE  EPR.monthid = @MonthId
                     AND EPR.[yearid] = @Year
                     AND EPR.isdeleted = 0
                     AND EPR.EmployeeId = @Employeeid

              --AND Isnull(EMP.IsActive, '') = 0
              DROP TABLE #TableResult
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
