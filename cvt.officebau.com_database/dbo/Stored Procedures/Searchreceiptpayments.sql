﻿/****************************************************************************         
CREATED BY   : Naneeshwar        
CREATED DATE  :   11-Jan-2017      
MODIFIED BY   :      Dhanalakshmi   
MODIFIED DATE  :     30-03-17        
 <summary>      
[SearchReceiptPayments] 1,'02/02/2010','03/31/2017',''     
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchreceiptpayments] (@DomainID   INT,
                                               @StartDate  VARCHAR(50),
                                               @EndDate    VARCHAR(50),
                                               @PartyName  VARCHAR(50),
                                               @Notations  VARCHAR(50),
                                               @Amount     MONEY,
                                               @LedgerName VARCHAR(50))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @StartDate != '' )
            SET @StartDate = Cast(@StartDate AS DATETIME)

          IF( @EndDate != '' )
            SET @EndDate = Cast(@EndDate AS DATETIME)

          SELECT ar.ID            AS ID,
                 ar.PartyName     AS PartyName,
                 ar.Date          AS Date,
                 ar.Amount        AS Amount,
                 ar.[Description] AS [Description],
                 ld.Name          AS LedgerName
          INTO   #TempPayments
          FROM   tblReceipts ar
                 LEFT JOIN tblMasterTypes cd
                        ON cd.ID = ar.TransactionType
                 LEFT JOIN tblLedger ld
                        ON ar.LedgerID = ld.ID
          WHERE  ar.IsDeleted = 0
                 AND TransactionType = (SELECT ID
                                        FROM   tblMasterTypes
                                        WHERE  NAME = 'Make Payment')
                 AND ar.DomainID = @domainID
                 AND ( ( @StartDate IS NULL
                          OR @StartDate = '' )
                        OR [Date] >= @StartDate )
                 AND ( ( @EndDate IS NULL
                          OR @EndDate = '' )
                        OR [Date] <= @EndDate )
                 AND ( Isnull(@PartyName, '') = ''
                        OR ar.PartyName LIKE '%' + @PartyName + '%' )
                 AND ( Isnull(@LedgerName, '') = ''
                        OR ld.NAME LIKE '%' + @LedgerName + '%' )
          ORDER  BY ar.ModifiedOn DESC

          SELECT *
          FROM   #TempPayments
          WHERE  ( Isnull(@Amount, 0) = 0 )
                  OR ( Isnull(@Notations, '') = 'EqualTo'
                       AND Amount = @Amount )
                  OR ( Isnull(@Notations, '') = 'GreaterThanEqual'
                       AND Amount >= @Amount )
                  OR ( Isnull(@Notations, '') = 'LessThanEqual'
                       AND Amount <= @Amount )

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
