﻿/****************************************************************************     
CREATED BY      : Naneeshwar.M    
CREATED DATE  : 05-JULY-2017    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>      
 [Payrolldashboardtiles] 11,2018,1 ,2   
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Payrolldashboardtiles] (@MonthId    INT,    
                                               @Year       INT,    
                                               @DomainID   INT,    
                                               @EmployeeID INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN    
              DECLARE @DataSource TABLE    
                (    
                   [Value] NVARCHAR(128)    
                )    
    
              INSERT INTO @DataSource    
                          ([Value])    
              SELECT Item    
              FROM   dbo.Splitstring ((SELECT businessunitid    
                                       FROM   tbl_employeemaster    
                                       WHERE  id = @EmployeeID and DomainID=@DomainID), ',')    
              WHERE  Isnull(Item, '') <> ''    
    
              SELECT  (Select ISNULL(Sum(Amount),0) from tbl_Pay_EmployeePayrollDetails where Isdeleted=0 and PayrollId=ep.ID  and Componentid   
     =(Select Value from tbl_DashBoardConfiguration where [Key]='Tile1ID'  and DomainID =@DomainID) )                 AS NetSalary,    
                    (Select ISNULL(Sum(Amount),0) from tbl_Pay_EmployeePayrollDetails where Isdeleted=0 and PayrollId=ep.ID and Componentid   
     =(Select Value from tbl_DashBoardConfiguration where [Key]='Tile2ID' and DomainID =@DomainID) )   AS PF,    
                     (Select ISNULL(Sum(Amount),0) from tbl_Pay_EmployeePayrollDetails where Isdeleted=0 and PayrollId=ep.ID and Componentid   
     =(Select Value from tbl_DashBoardConfiguration where [Key]='Tile3ID' and DomainID =@DomainID) ) AS ESI,    
                  (Select ISNULL(Sum(Amount),0) from tbl_Pay_EmployeePayrollDetails where Isdeleted=0 and PayrollId=ep.ID and Componentid   
     =(Select Value from tbl_DashBoardConfiguration where [Key]='Tile4ID' and DomainID =@DomainID) )                      AS TDS    
	 INTO #Result
              FROM   tbl_pay_EmployeePayroll ep    
                     LEFT JOIN tbl_EmployeeMaster emp    
                            ON emp.ID = ep.EmployeeId    
                     LEFT JOIN tbl_BusinessUnit bu    
                            ON bu.ID = emp.BaseLocationID    
              WHERE  ep.IsProcessed = 1    
                     AND ep.IsDeleted = 0    
                     AND ( ( @MonthId IS NULL    
                              OR @MonthId = '' )    
                            OR ( ep.monthid = @MonthId ) )    
                     AND [yearid] = @Year    
                     AND ep.DomainId = @DomainID    
                     AND emp.BaselocationID IN(SELECT *    
                                              FROM   @DataSource)   
											  Select Sum(NetSalary) AS NetSalary, Sum(PF) AS PF,SUM(ESI) AS ESI ,SUM(TDS) AS TDS from #Result 
          END    
      END TRY    
      BEGIN CATCH             
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR (@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
