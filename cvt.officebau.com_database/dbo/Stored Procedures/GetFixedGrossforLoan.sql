﻿  
/****************************************************************************   
CREATED BY   : DHANALAKSHMI.S  
CREATED DATE  : 16-JAN-2018  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
   [GetFixedGrossforLoan] 1,107  
 </summary>                            
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetFixedGrossforLoan] (@DomainID   INT,  
                                              @EmployeeID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY ;  
          WITH cte  
               AS (SELECT EPS.ID,  
                          (Select Top 1 Amount from tbl_Pay_EmployeePayStructureDetails where PayStructureId=EPS.ID and Isdeleted=0
						   and ComponentId =(Select Id from tbl_Pay_PayrollCompontents where Isdeleted=0 and Code='BASIC')) AS GROSS,  
                          Eps.EmployeeID,  
                          Row_number()  
                            OVER (  
                              PARTITION BY EPS.EmployeeId  
                              ORDER BY EPS.EffectiveFrom DESC) AS rowno  
                   FROM   tbl_Pay_EmployeePayStructure EPS  
                          LEFT JOIN tbl_EmployeeMaster EMP  
                                 ON emp.ID = EPS.EmployeeId  
                   WHERE  EMP.ID = @EmployeeID  
       AND EPS.IsDeleted = 0  
                   GROUP  BY EPS.Id,  
                             EPS.EmployeeID,  
                            
                             EPS.EffectiveFrom)  
          SELECT *  
          FROM   CTE  
          WHERE  rowno IN( 1 )  
          GROUP  BY ID,  
                    EmployeeID,  
                    rowno,  
                    Gross  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
