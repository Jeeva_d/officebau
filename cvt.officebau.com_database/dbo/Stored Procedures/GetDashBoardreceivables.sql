﻿
/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
<summary>        
	[GetDashBoardreceivables] '',1,2
</summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetDashBoardreceivables] (@Date     DATETIME,
                                                 @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF( Isnull(@Date, '') = '' )
            SET @Date=Getdate();

          WITH Income
               AS (SELECT ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50)) AS MonthYear,
                          Sum(Amount)                                                                          AS Amount,
                          Sum(Isnull(Received, 0))                                                             AS ReceivedAmount
                   FROM   Vw_Income
                   GROUP  BY ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50))),
               MonthYear
               AS (SELECT MonthYear
                   FROM   dbo.Fngetmonthyear (Datepart(Month, @Date), Datepart(YEAR, @Date)))
          SELECT tem.MonthYear                                         AS MonthYear,
                 Isnull(inc.Amount, 0)                                 AS Amount,
                 Isnull(inc.ReceivedAmount, 0)                         AS ReceivedAmount,
                 Isnull(inc.Amount, 0) - Isnull(inc.ReceivedAmount, 0) AS Receivables
          FROM   MonthYear tem
                 LEFT JOIN Income inc
                        ON tem.MonthYear = inc.MonthYear
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 






