﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
  
      OfferLetter 224
 </summary>                         
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[OfferLetter]
(
	@EmployeeID		TINYINT
	
)
AS
	BEGIN
				
		SET NOCOUNT ON;
					DECLARE @Temp VARCHAR(20)
					SET @Temp = right(DATEPART(YEAR,GETDATE()),2)
		BEGIN TRY
				SELECT 
				DISTINCT 
				Emp.ID		AS			ID				,
				Emp.FirstName AS			Name			,
				Emp.DOJ			AS DOJ,
				Emp.BandID  ,
				Emp.JobTypeID ,
				ep.Gross
				FROM
				tbl_EmployeeMaster Emp
				Join tbl_EmployeePayStructure ep on Emp.ID=ep.EmployeeId
							
					WHERE
							Emp.ID = @EmployeeID
	END TRY
	BEGIN CATCH        
			DECLARE @ErrorMsg VARCHAR(100),@ErrSeverity TINYINT        
			SELECT	@ErrorMsg = ERROR_MESSAGE(),@ErrSeverity = ERROR_SEVERITY()        
			RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH   
 END
