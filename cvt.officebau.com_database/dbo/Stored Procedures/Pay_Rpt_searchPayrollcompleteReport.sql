﻿/****************************************************************************             
CREATED BY   :   Jeeva     
CREATED DATE  :            
MODIFIED BY   :             
MODIFIED DATE  :             
 <summary>           
         [Pay_Rpt_searchPayrollcompleteReport] 5,2017,12,2019,1 
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_Rpt_searchPayrollcompleteReport] (@FromMonthID INT,
                                                              @FromYearID  INT,
                                                              @ToMonthID   INT,
                                                              @ToYearID    INT,
                                                              @DomainId    INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @cols  AS NVARCHAR(MAX),
                  @query AS NVARCHAR(MAX)

          SELECT *
          INTO   #Result
          FROM   VW_Payroll vwpay
          WHERE  Cast(Cast(vwpay.YearId AS VARCHAR(10)) + '-'
                      + Cast(vwpay.MonthId AS VARCHAR(10)) + '-'
                      + '01' AS DATE) BETWEEN Cast(Cast(@FromYearID AS VARCHAR(10)) + '-'
                                                   + Cast(@FromMonthID AS VARCHAR(10)) + '-'
                                                   + '01' AS DATE) AND Cast(Cast (@ToYearID AS VARCHAR(10 )) + '-'
                                                                            + Cast( @ToMonthID AS VARCHAR(10)) + '-' + '01' AS DATE)

          SET @cols = Stuff((SELECT ',' + Quotename(description)
                             FROM   VW_Payroll
                             WHERE  DomainId = @DomainId
                             GROUP  BY description,
                                       Ordinal
                             ORDER  BY Ordinal ASC
                             FOR XML path('')), 1, 1, ' ')
          SET @query = 'SELECT         
        MonthID, Employee_Code as Emp_Code, Employee_Name,                  
       Joining_Date, Designation,  Month, YearId as Year, '
                       + @cols + '        
        from             (                  
       select        
        MonthID, Employee_Code, Description, Employee_Name, Joining_Date,Designation,                    
        Month, YearId, Amount           
       from #Result        
             
       ) x            pivot            (sum(Amount)                     
       for Description in (' + @cols
                       + ')            ) p order by Year ASC, MonthID ASC'

          EXEC (@query);
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
