﻿/****************************************************************************               
CREATED BY  :    Ajith N           
CREATED DATE :   22 May 2018            
MODIFIED BY  :          
MODIFIED DATE :         
 <summary>                      
          
 </summary>                                       
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetUploadedFile] (@FileID VARCHAR(50))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
  
          SELECT [Path] + [FileName] AS [FileName],  
                 FileType,  
                 FileContent,  
                 OriginalFileName 
				
          FROM   tbl_FileUpload  
          WHERE  Id = @FileID  
  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
