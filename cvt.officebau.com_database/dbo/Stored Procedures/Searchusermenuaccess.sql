﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
    Searchusermenuaccess 1768, 'MASDATA', 'Save', 6
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchusermenuaccess] (@UserID        INT,
                                               @MenuID        VARCHAR(20),
                                               @OperationType VARCHAR(6),
                                               @DomainID      INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF ( Upper(@OperationType) = 'SAVE' )
            SELECT Count(1) AS Result
            FROM   tbl_RBSUserMenuMapping
            WHERE  EmployeeID = @UserID
                   AND MenuID = (SELECT ID
                                 FROM   tbl_RBSMenu
                                 WHERE  MenuCode = @MenuID and DomainID=@DomainID AND ISdeleted=0)
                   AND MWrite = 1
                   AND IsDeleted = 0
                   AND DomainID = @DomainID

          IF ( Upper(@OperationType) = 'EDIT' )	
            SELECT Count(1) AS Result
            FROM   tbl_RBSUserMenuMapping
            WHERE  EmployeeID = @UserID
                   AND MenuID = (SELECT ID
                                 FROM   tbl_RBSMenu
                                 WHERE  MenuCode = @MenuID and DomainID=@DomainID AND ISdeleted=0)
                   AND MEdit = 1
                   AND IsDeleted = 0
                   AND DomainID = @DomainID

          IF ( Upper(@OperationType) = 'READ' )
            SELECT Count(1) AS Result
            FROM   tbl_RBSUserMenuMapping
            WHERE  EmployeeID = @UserID
                   AND MenuID = (SELECT ID
                                 FROM   tbl_RBSMenu
                                 WHERE  MenuCode = @MenuID and DomainID=@DomainID AND ISdeleted=0)
                   AND MRead = 1
                   AND IsDeleted = 0
                   AND DomainID = @DomainID

          IF ( Upper(@OperationType) = 'DELETE' )
            SELECT Count(1) AS Result
            FROM   tbl_RBSUserMenuMapping
            WHERE  EmployeeID = @UserID
                   AND MenuID = (SELECT ID
                                 FROM   tbl_RBSMenu
                                 WHERE  MenuCode = @MenuID and DomainID=@DomainID AND ISdeleted=0)
                   AND MDelete = 1
                   AND IsDeleted = 0
                   AND DomainID = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
