﻿/**************************************************************************** 
CREATED BY			:	Dhanalakshmi
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary> 
 [ManageFinancialYear]  2,'2016','Apr 2016 - Mar 2017',0,2,2
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageFinancialYear] (@ID            INT,
                                             @FinancialYear VARCHAR(100),
                                             @Description   VARCHAR(1000),
                                             @HasDeleted    BIT,
                                             @SessionID     INT,
                                             @DomainID      INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(1000)

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @HasDeleted = 1 )
            BEGIN
                UPDATE tbl_FinancialYear
                SET    IsDeleted = 1
                WHERE  ID = @ID
                       AND DomainID = @DomainID

                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Information'
                                      AND Code = 'RCD_DEL'
                                      AND IsDeleted = 0) --'Deleted Successfully'
                GOTO Finish
            END
          ELSE
            BEGIN
                DECLARE @StartMonth INT,
                        @EndMonth   INT

                SELECT TOP 1 @StartMonth = Value,
                             @EndMonth = ( Value - 1 )
                FROM   tblApplicationConfiguration
                WHERE  Code = 'STARTMTH'
                       AND DomainID = @DomainID

                SET @StartMonth = ISNULL(@StartMonth, 4)
                SET @EndMonth = ISNULL(@EndMonth, 3)

                IF( @ID = 0 )
                  BEGIN
                      IF( (SELECT Count(1)
                           FROM   tbl_FinancialYear
                           WHERE  FinancialYear = @FinancialYear
                                  AND DomainID = @DomainID
                                  AND IsDeleted = 0) = 0 )
                        BEGIN
                            INSERT INTO tbl_FinancialYear
                                        (FinancialYear,
                                         NAME,
                                         DisplayName,
                                         [FYDescription],
                                         StartMonth,
                                         EndMonth,
                                         CreatedBy,
                                         ModifiedBy,
                                         DomainID)
                            VALUES      (@FinancialYear,
                                         @FinancialYear,
                                         ( @FinancialYear + '-'
                                           + Cast((Cast(@FinancialYear AS INT) + 1) AS VARCHAR) ),
                                         @Description,
                                         @StartMonth,
                                         @EndMonth,
                                         @SessionID,
                                         @SessionID,
                                         @DomainID)

                            SET @Output = (SELECT [Message]
                                           FROM   tblErrorMessage
                                           WHERE  [Type] = 'Information'
                                                  AND Code = 'RCD_INS'
                                                  AND IsDeleted = 0) --'Inserted Successfully'
                        END
                      ELSE
                        SET @Output = (SELECT [Message]
                                       FROM   tblErrorMessage
                                       WHERE  [Type] = 'Warning'
                                              AND Code = 'RCD_EXIST'
                                              AND IsDeleted = 0) --'Already Exists'
                  END

                IF( @ID != 0 )
                  BEGIN
                      IF( (SELECT Count(1)
                           FROM   tbl_FinancialYear
                           WHERE  FinancialYear = @FinancialYear
                                  AND DomainID = @DomainID
                                  AND IsDeleted = 0
                                  AND ID <> @ID) = 0 )
                        BEGIN
                            UPDATE tbl_FinancialYear
                            SET    FinancialYear = @FinancialYear,
                                   NAME = @FinancialYear,
                                   DisplayName = ( @FinancialYear + '-'
                                                   + Cast((Cast(@FinancialYear AS INT) + 1) AS VARCHAR) ),
                                   [FYDescription] = @Description,
                                   StartMonth = @StartMonth,
                                   EndMonth = @EndMonth,
                                   ModifiedBy = @SessionID
                            WHERE  Id = @ID

                            SET @Output = (SELECT [Message]
                                           FROM   tblErrorMessage
                                           WHERE  [Type] = 'Information'
                                                  AND Code = 'RCD_UPD'
                                                  AND IsDeleted = 0) --'Updated Successfully'
                        END
                      ELSE
                        SET @Output = (SELECT [Message]
                                       FROM   tblErrorMessage
                                       WHERE  [Type] = 'Warning'
                                              AND Code = 'RCD_EXIST'
                                              AND IsDeleted = 0)
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
