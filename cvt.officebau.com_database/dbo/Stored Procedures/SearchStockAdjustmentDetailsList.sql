﻿/****************************************************************************             
CREATED BY   :             
CREATED DATE  :          
MODIFIED BY   :             
MODIFIED DATE  :             
 <summary>          
 [SearchStockAdjustmentDetailsList] 1036,1          
 </summary>                                     
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[SearchStockAdjustmentDetailsList] (@AdjustmentID INT,    
                                                           @DomainID     INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SELECT isat.ID                   AS ID,    
                 isat.ProductID            AS ProductID,    
                 isat.AdjustmentID         AS AdjustmentID,    
                 isat.AdjustedQty          AS AdjustedQty,    
                 INVY.StockonHand          AS StockonHand,    
                 isat.RemainingQty   AS RemainingQty    
          FROM   tbl_InventoryStockAdjustmentItems isat    
                 LEFT JOIN tblProduct_v2 p    
                        ON isat.ProductID = p.ID    
                 LEFT JOIN VW_Inventory INVY    
                        ON isat.ProductID = INVY.ProductID    
          WHERE  isat.IsDeleted = 0    
                 AND @AdjustmentID = isat.AdjustmentID    
                 AND isat.DomainID = @DomainID    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
