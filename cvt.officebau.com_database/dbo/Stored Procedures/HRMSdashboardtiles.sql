﻿/****************************************************************************   
CREATED BY      : Ajith N  
CREATED DATE  : 26-JULY-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>    
 [HRMSdashboardtiles] 1, 2  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[HRMSdashboardtiles] (@DomainID INT,  
                                            @UserID   INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @Headcount      INT = 0,  
                  @CurrentlyHired INT = 0,  
                  @Compensation   DECIMAL(32, 2) = 0,  
                  @Attrition      DECIMAL(32, 3) = 0  
  
          SELECT *  
          INTO   #tmpAccess  
          FROM   SplitString((SELECT BusinessUnitID  
                              FROM   tbl_EmployeeMaster  
                              WHERE  ID = @UserID), ',')  
  
          -- Headcount  
          SELECT @Headcount = ISNULL(Count(1), 0)  
          FROM   tbl_EmployeeMaster EM  
          WHERE  EM.IsDeleted = 0  
                 AND isnull(EM.IsActive, 0) = 0  
                 AND EM.Code NOT LIKE 'TMP_%'  
                 AND EM.DomainID = @DomainID  
                 AND EM.BaseLocationID IN (SELECT item  
                                           FROM   #tmpAccess)  
  
          -- CurrentlyHired  
          SELECT @CurrentlyHired = ISNULL(Count(1), 0)  
          FROM   tbl_EmployeeMaster EM  
          WHERE  EM.IsDeleted = 0  
                 AND isnull(EM.IsActive, 0) = 0  
                 AND Year(EM.DOJ) = Year(Getdate())  
                 AND EM.Code NOT LIKE 'TMP_%'  
                 AND EM.DomainID = @DomainID  
                 AND EM.BaseLocationID IN (SELECT item  
                                           FROM   #tmpAccess)  
  
          -- Compensation  
          SELECT @Compensation = Sum(ISNULL(d.Amount, 0))  
          FROM   tbl_EmployeeMaster EM  
                 LEFT JOIN tbl_Pay_EmployeePayStructure PS  
                        ON EM.ID = PS.EmployeeId  
                           AND ISNULL(PS.IsDeleted, 0) = 0  
                           AND PS.EffectiveFrom = (SELECT TOP 1 EffectiveFrom  
                                                   FROM   tbl_Pay_EmployeePayStructure  
                                                   WHERE  EmployeeId = EM.ID  
                                                          AND IsDeleted = 0  
                                                   ORDER  BY EffectiveFrom DESC)  
				LEFT JOIN tbl_Pay_EmployeePayStructureDetails d on d.PayStructureId = PS.ID And d.ISdeleted=0 and d.ComponentId = (Select Value from tbl_DashBoardConfiguration where [Key]='HRMSCOM' and DomainID =@DomainID)
          WHERE  EM.IsDeleted = 0  
                 AND ISNULL(EM.IsActive, 0) = 0  
                 AND EM.Code NOT LIKE 'TMP_%'  
                 AND EM.DomainID = @DomainID  
                 AND EM.BaseLocationID IN (SELECT item  
                                           FROM   #tmpAccess)  
  
          -- Attrition  
          SELECT @Attrition = CASE  
                                WHEN @CurrentlyHired = 0 THEN  
                                  0  
                                ELSE  
                                  ( ( ( (SELECT CASE  
                                                  WHEN ISNULL(Count(1), 0) = 0 THEN  
                                                    1  
                                                  ELSE  
                                                    Count(1)  
                                                END  
                                         FROM   tbl_EmployeeMaster EM  
                                         WHERE  EM.IsDeleted = 0  
                                                AND ISNULL(EM.IsActive, 0) = 1  
                                                AND EM.Code NOT LIKE 'TMP_%'  
                                                AND Year(EM.DOJ) = ( Year(Getdate()) )  
                                                AND EM.DomainID = @DomainID  
                                                AND EM.BaseLocationID IN (SELECT item  
                                                                          FROM   #tmpAccess)) * 100.00 ) / ( ISNULL(Count(1), 0)  
                                                                                                             + ISNULL(@CurrentlyHired, 0) ) ) / 100.00 )  
                              END  
          FROM   tbl_EmployeeMaster EM  
          WHERE  EM.IsDeleted = 0  
                 AND ISNULL(EM.IsActive, 0) = 0  
                 AND EM.Code NOT LIKE 'TMP_%'  
                 AND Year(EM.DOJ) < ( Year(Getdate()) )  
                 AND EM.DomainID = @DomainID  
                 AND EM.BaseLocationID IN (SELECT item  
                                           FROM   #tmpAccess)  
  
          SELECT @Headcount               AS Headcount,  
                 @CurrentlyHired          AS CurrentlyHired,  
                 ISNULL(@Compensation, 0) AS Compensation,  
                 @Attrition               AS Attrition  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
