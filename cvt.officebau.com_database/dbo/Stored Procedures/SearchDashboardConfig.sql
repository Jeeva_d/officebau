﻿/****************************************************************************           
CREATED BY  :           
CREATED DATE :           
MODIFIED BY  :          
MODIFIED DATE   :         
 <summary>     
 [SearchDashboardConfig] 1             
 </summary>                                   
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchDashboardConfig](@DomainID       INT  
                                                )  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          Select [Key] ,Value,p.Description from tbl_DashBoardConfiguration a
		  Join tbl_Pay_PayrollCompontents p on p.ID=a.Value
		  Where a.DomainID = @DomainID  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
