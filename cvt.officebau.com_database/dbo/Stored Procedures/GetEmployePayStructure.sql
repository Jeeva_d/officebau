﻿
/****************************************************************************         
CREATED BY		:  Jeeva D
CREATED DATE	:  
MODIFIED BY		:       Naneeshwar.M
MODIFIED DATE   :        16-Oct-2017
 <summary>  
 [GetEmployePayStructure] 2017,8,914,1083287.00 ,1,0 ,0,0,null,null,null
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetEmployePayStructure](@ID                 INT,
                                               @CompanyPayStubId   INT,
                                               @EmployeeID         INT,
                                               @Gross              MONEY,
                                               @IsPFRequired       BIT,
                                               @IsESIRequired      BIT,
                                               @MedicalAllowance   MONEY,
                                               @Conveyance         MONEY,
                                               @EducationAllowance MONEY,
                                               @PaperMagazine      MONEY,
                                               @MonsoonAllow       MONEY,
                                               @CarAllow           MONEY,
                                               @LTA                MONEY)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @SplAllowance DECIMAL(18, 2),
                  @Region       VARCHAR(100)=(SELECT Upper(NAME)
                    FROM   tbl_BusinessUnit
                    WHERE  id = (SELECT RegionID
                                 FROM   tbl_EmployeeMaster
                                 WHERE  ID = @EmployeeID
                                        AND IsDeleted = 0))
          DECLARE @Designation VARCHAR(50)=(SELECT de.NAME
            FROM   tbl_EmployeeMaster e
                   JOIN tbl_designation de
                     ON de.id = e.DesignationID
            WHERE  e.ID = @EmployeeID
                   AND e.IsDeleted = 0)

          IF( @Designation <> 'CFO' )
            BEGIN
                SET @SplAllowance = (SELECT( @Gross - ( @Gross / 100 * Basic + ( @Gross / 100 * HRA ) + ( CASE
                                                                                                            WHEN( @MedicalAllowance >= 0 ) THEN ( @MedicalAllowance )
                                                                                                            ELSE Isnull(MedicalAllowance, 0)
                                                                                                          END ) + ( CASE
                                                                                                                      WHEN( @Conveyance >= 0 ) THEN ( @Conveyance )
                                                                                                                      ELSE Isnull(Conveyance, 0)
                                                                                                                    END ) + ( CASE
                                                                                                                                WHEN( @EducationAllowance >= 0 ) THEN ( @EducationAllowance )
                                                                                                                                ELSE Isnull(EducationAllow, 0)
                                                                                                                              END ) + ( CASE
                                                                                                                                          WHEN( @PaperMagazine >= 0 ) THEN ( @PaperMagazine )
                                                                                                                                          ELSE Isnull(MagazineAllow, 0)
                                                                                                                                        END ) ) )
                                     FROM   tbl_CompanyPayStructure
                                     WHERE  id = @CompanyPayStubId)

                SELECT @Gross / 100 * c.Basic                       AS [Basic],
                       @Gross / 100 * c.HRA                         AS HRA,
                       CASE
                         WHEN( @MedicalAllowance >= 0 ) THEN ( @MedicalAllowance )
                         ELSE c.MedicalAllowance
                       END                                          AS MedicalAllowance,
                       ( CASE
                           WHEN( @Conveyance >= 0 ) THEN ( @Conveyance )
                           ELSE Isnull(c.Conveyance, 0)
                         END )                                      AS Conveyance,
                       @SplAllowance                                AS SplAllow,
                       CASE
                         WHEN( @EducationAllowance >= 0 ) THEN ( @EducationAllowance )
                         ELSE c.EducationAllow
                       END                                          AS EducationAllowance,
                       CASE
                         WHEN( @PaperMagazine >= 0 ) THEN ( @PaperMagazine )
                         ELSE c.MagazineAllow
                       END                                          AS PaperMagazine,
                       CASE
                         WHEN( @MonsoonAllow >= 0 ) THEN ( @MonsoonAllow )
                         ELSE c.MonsoonAllow
                       END                                          AS MonsoonAllow,
                       Cast(0 AS DECIMAL(18, 2))                    AS CarAllow,
                       Cast(0 AS DECIMAL(18, 2))                    AS LTA,
                       CASE
                         WHEN ( @IsPFRequired = 0 ) THEN 0
                         ELSE
                           CASE
                             WHEN( ( @Gross / 100 * c.Basic + ( CASE
                                                                  WHEN( @Conveyance >= 0 ) THEN ( @Conveyance )
                                                                  ELSE Isnull(c.Conveyance, 0)
                                                                END ) + @SplAllowance + ( CASE
                                                                                            WHEN( @EducationAllowance >= 0 ) THEN ( @EducationAllowance )
                                                                                            ELSE c.EducationAllow
                                                                                          END ) + ( CASE
                                                                                                      WHEN( @PaperMagazine >= 0 ) THEN ( @PaperMagazine )
                                                                                                      ELSE c.MagazineAllow
                                                                                                    END ) + ( CASE
                                                                                                                WHEN ( @Region = 'CHENNAI'
                                                                                                                        OR @Region = 'HYDERABAD' ) THEN ( CASE
                                                                                                                                                            WHEN( Isnull(@MedicalAllowance, NULL) IS NOT NULL ) THEN ( @MedicalAllowance )
                                                                                                                                                            ELSE c.MedicalAllowance
                                                                                                                                                          END )
                                                                                                                ELSE 0
                                                                                                              END ) ) > ( PAYEPF.Limit ) ) THEN ( PAYEPF.Value )
                             ELSE ( @Gross / 100 * c.Basic + ( CASE
                                                                 WHEN( @Conveyance >= 0 ) THEN ( @Conveyance )
                                                                 ELSE Isnull(c.Conveyance, 0)
                                                               END ) + @SplAllowance + ( CASE
                                                                                           WHEN( @EducationAllowance >= 0 ) THEN ( @EducationAllowance )
                                                                                           ELSE c.EducationAllow
                                                                                         END ) + ( CASE
                                                                                                     WHEN( @PaperMagazine >= 0 ) THEN ( @PaperMagazine )
                                                                                                     ELSE c.MagazineAllow
                                                                                                   END ) + ( CASE
                                                                                                               WHEN ( @Region = 'CHENNAI'
                                                                                                                       OR @Region = 'HYDERABAD' ) THEN ( CASE
                                                                                                                                                           WHEN( Isnull(@MedicalAllowance, NULL) IS NOT NULL ) THEN ( @MedicalAllowance )
                                                                                                                                                           ELSE c.MedicalAllowance
                                                                                                                                                         END )
                                                                                                               ELSE 0
                                                                                                             END ) ) * ( PAYEPF.Percentage ) / 100
                           END
                       END                                          AS EPF,
                       CASE
                         WHEN ( @IsESIRequired = 0 ) THEN 0
                         ELSE
                           CASE
                             WHEN ( @Gross <= ( PAYEESI.Limit ) ) THEN @Gross / 100 * ( PAYEESI.Percentage )
                             ELSE 0.00
                           END
                       END                                          AS EESI,
                       0.00                                         AS PT,
                       0.00                                         AS TDS,
                       CASE
                         WHEN ( @IsPFRequired = 0 ) THEN 0
                         ELSE
                           CASE
                             WHEN ( ( @Gross / 100 * c.Basic + ( CASE
                                                                   WHEN( @Conveyance >= 0 ) THEN ( @Conveyance )
                                                                   ELSE Isnull(c.Conveyance, 0)
                                                                 END ) + @SplAllowance + ( CASE
                                                                                             WHEN( @EducationAllowance >= 0 ) THEN ( @EducationAllowance )
                                                                                             ELSE c.EducationAllow
                                                                                           END ) + ( CASE
                                                                                                       WHEN( @PaperMagazine >= 0 ) THEN ( @PaperMagazine )
                                                                                                       ELSE c.MagazineAllow
                                                                                                     END ) + ( CASE
                                                                                                                 WHEN ( @Region = 'CHENNAI'
                                                                                                                         OR @Region = 'HYDERABAD' ) THEN ( CASE
                                                                                                                                                             WHEN( Isnull(@MedicalAllowance, NULL) IS NOT NULL ) THEN ( @MedicalAllowance )
                                                                                                                                                             ELSE c.MedicalAllowance
                                                                                                                                                           END )
                                                                                                                 ELSE 0
                                                                                                               END ) ) > ( PAYERPF.Limit ) ) THEN ( PAYERPF.Value )
                             ELSE ( @Gross / 100 * c.Basic + ( CASE
                                                                 WHEN( @Conveyance >= 0 ) THEN ( @Conveyance )
                                                                 ELSE Isnull(c.Conveyance, 0)
                                                               END ) + @SplAllowance + ( CASE
                                                                                           WHEN( @EducationAllowance >= 0 ) THEN ( @EducationAllowance )
                                                                                           ELSE c.EducationAllow
                                                                                         END ) + ( CASE
                                                                                                     WHEN( @PaperMagazine >= 0 ) THEN ( @PaperMagazine )
                                                                                                     ELSE c.MagazineAllow
                                                                                                   END ) + ( CASE
                                                                                                               WHEN ( @Region = 'CHENNAI'
                                                                                                                       OR @Region = 'HYDERABAD' ) THEN ( CASE
                                                                                                                                                           WHEN( Isnull(@MedicalAllowance, NULL) IS NOT NULL ) THEN ( @MedicalAllowance )
                                                                                                                                                           ELSE c.MedicalAllowance
                                                                                                                                                         END )
                                                                                                               ELSE 0
                                                                                                             END ) ) * ( PAYERPF.Percentage ) / 100
                           END
                       END                                          AS ERPF,
                       CASE
                         WHEN ( @IsESIRequired = 0 ) THEN 0
                         ELSE
                           CASE
                             WHEN ( @Gross <= ( PAYERESI.Limit ) ) THEN @Gross / 100 * ( PAYERESI.Percentage )
                             ELSE 0.00
                           END
                       END                                          AS ERESI,
                       CASE
                         WHEN ( @id = 0 ) THEN @Gross / 12 -- 0
                         ELSE Isnull(e.Bonus, NULL)
                       END                                          AS Bonus,
                       ( @Gross / 100 * c.Basic ) * 15 / 26 / 12    AS Gratuity,
                       Isnull(e.Mediclaim, NULL)                    AS MEDICLAIM,
                       Isnull(e.PLI, NULL)                          AS PLI,
                       Isnull(e.MobileDataCard, NULL)               AS MobileDataCard,
                       Isnull(e.OtherPerks, NULL)                   AS OtherPerks,
                       (SELECT Count(1)
                        FROM   tbl_PayrollConfiguration p
                        WHERE  p.BusinessUnitID = C.BusinessUnitID) AS checkconfig,
                       Cast (Isnull(emp.RegionID, 0) AS INT)        AS checkCompanyPS,
                       @Designation                                 AS DesignationName
                FROM   tbl_CompanyPayStructure C
                       LEFT JOIN tbl_EmployeePayStructure E
                              ON e.CompanyPayStubId = C.Id
                                 AND @ID <> 0
                       LEFT JOIN tbl_EmployeeMaster EMP
                              ON EMP.ID = @EmployeeID
                                 AND EMP.RegionID = C.BusinessUnitID
                       LEFT JOIN tbl_PayrollConfiguration PAYEPF
                              ON PAYEPF.Components = 'EmployeePF'
                                 AND PAYEPF.BusinessUnitID = EMP.RegionID
                       LEFT JOIN tbl_PayrollConfiguration PAYERPF
                              ON PAYERPF.Components = 'EmployerPF'
                                 AND PAYERPF.BusinessUnitID = EMP.RegionID
                       LEFT JOIN tbl_PayrollConfiguration PAYEESI
                              ON PAYEESI.Components = 'EmployeeESI'
                                 AND PAYEESI.BusinessUnitID = EMP.RegionID
                       LEFT JOIN tbl_PayrollConfiguration PAYERESI
                              ON PAYERESI.Components = 'EmployerESI'
                                 AND PAYERESI.BusinessUnitID = EMP.RegionID
                WHERE  C.id = @CompanyPayStubId
                       AND ( ( @ID IS NULL
                                OR @ID = '' )
                              OR ( e.id = @ID ) )
                       AND C.IsDeleted = 0
            END
          ELSE
            BEGIN
                SELECT Cast(433315.00 AS DECIMAL(18, 2))            AS [Basic],
                       0.00                                         AS HRA,
                       CASE
                         WHEN( @MedicalAllowance >= 0 ) THEN ( @MedicalAllowance )
                         ELSE c.MedicalAllowance
                       END                                          AS MedicalAllowance,
                       0.00                                         AS Conveyance,
                       0.00                                         AS SplAllow,
                       0.00                                         AS EducationAllowance,
                       0.00                                         AS PaperMagazine,
                       0.00                                         AS MonsoonAllow,
                       Cast(@CarAllow AS DECIMAL(18, 2))            AS CarAllow,
                       Cast(@LTA AS DECIMAL(18, 2))                 AS LTA,
                       Cast(51998 AS DECIMAL(18, 2))                AS EPF,
                       0.00                                         AS EESI,
                       0.00                                         AS PT,
                       0.00                                         AS TDS,
                       Cast(51998.00 AS DECIMAL(18, 2))             AS ERPF,
                       0.00                                         AS ERESI,
                       0.00                                         AS Bonus,
                       0.00                                         AS Gratuity,
                       Isnull(e.Mediclaim, NULL)                    AS MEDICLAIM,
                       Isnull(e.PLI, NULL)                          AS PLI,
                       Isnull(e.MobileDataCard, NULL)               AS MobileDataCard,
                       Isnull(e.OtherPerks, NULL)                   AS OtherPerks,
                       (SELECT Count(1)
                        FROM   tbl_PayrollConfiguration p
                        WHERE  p.BusinessUnitID = C.BusinessUnitID) AS checkconfig,
                       Cast (Isnull(emp.RegionID, 0) AS INT)        AS checkCompanyPS,
                       @Designation                                 AS DesignationName
                FROM   tbl_CompanyPayStructure C
                       LEFT JOIN tbl_EmployeePayStructure E
                              ON e.CompanyPayStubId = C.Id
                                 AND @ID <> 0
                       LEFT JOIN tbl_EmployeeMaster EMP
                              ON EMP.ID = @EmployeeID
                                 AND EMP.RegionID = C.BusinessUnitID
                WHERE  C.id = @CompanyPayStubId
                       AND ( ( @ID IS NULL
                                OR @ID = '' )
                              OR ( e.id = @ID ) )
                       AND C.IsDeleted = 0
            END

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
