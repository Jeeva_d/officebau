﻿CREATE PROCEDURE [dbo].[Rpt_TravelDetails] (@BusinessUnit VARCHAR(10),      
                                           @DepartmentID INT,      
                                           @EmployeeID   INT,      
                                           @StatusID     INT,      
                                           @UserID       INT,      
                                           @DomainID     INT)      
AS      
  BEGIN      
      BEGIN TRY      
          DECLARE @BusinessUnitIDs VARCHAR(50)      
      
          IF( Isnull(@BusinessUnit, 0) = 0 )      
            SET @BusinessUnitIDs = (SELECT BusinessUnitID      
                                    FROM   tbl_EmployeeMaster      
                                    WHERE  ID = @UserID      
                                           AND DomainID = @DomainID)      
          ELSE      
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','      
      
          DECLARE @BusinessUnitTable TABLE      
            (      
               BusinessUnit VARCHAR(100)      
            )      
      
          INSERT INTO @BusinessUnitTable      
          SELECT @BusinessUnitIDs      
      
          SELECT TR.ID                              AS ReqID,      
                 Isnull(EM.EmpCodePattern, '') + EM.Code + ' - ' + em.FullName      AS [Emp Name],      
                 TR.TravelReqNo                     AS [Travel Request No],      
                 TR.TravelDate                      AS [Travel Date],      
                 TR.TravelFrom                      AS [Travel From],      
                 TR.TravelTo                        AS [Travel To],      
                 TR.ReturnDate                      AS [Return Date],      
                 TR.PurposeOfTravel                 AS Purpose,      
                 CM.Code                            AS [Travel Type],      
                 ( CASE      
                     WHEN TR.ModeOfTransportID = 1 THEN      
                       'Airways'      
                     WHEN TR.ModeOfTransportID = 2 THEN      
                       'Bus'      
                     WHEN TR.ModeOfTransportID = 3 THEN      
                       'Rail'      
                     WHEN TR.ModeOfTransportID = 4 THEN      
                       'Road'      
                     ELSE      
                       ''      
                   END )                            AS [Mode of Transportation],      
                 TR.TotalAmount                     AS [Total Amount],      
                 CY.Code                            AS Currency,      
                 --( CASE      
                 --    WHEN ISNULL(TR.IsBillableToCustomer, 0) = 0 THEN      
                 --      'No'      
                 --    ELSE      
                 --      'Yes'      
                 --  END )                            AS [Billable to customer],      
                 ISNULL(TR.IsBillableToCustomer, 0) AS [Billable to customer],      
                 (SELECT CASE      
                           WHEN Count(1) > 0 THEN      
                             Count(1)      
                           ELSE      
                             NULL      
                         END      
                  FROM   tbl_SubordinatesDetails S      
                  WHERE  S.TravelRequestID = TR.ID) AS [Subordinates],      
                 TR.Notes                           AS [Notes],      
                 AttachmentFileID                   AS [Attachment],      
                 Isnull(A.EmpCodePattern, '') + A.Code + ' - ' + A.FullName        AS [Approver],      
                 TA.ApproverRemarks                 AS [Approver Remarks],      
                 BU.NAME                            AS [Business Unit],      
                 ST.Code                            AS [Status]      
          FROM   tbl_EmployeeTravelRequest TR      
                 LEFT JOIN tbl_EmployeeMaster EM      
                        ON EM.ID = TR.EmployeeID      
                 LEFT JOIN tbl_CodeMaster CM      
                        ON CM.ID = TR.TravelTypeID      
                 LEFT JOIN tblCurrency CY      
                        ON CY.ID = TR.CurrencyID      
                 LEFT JOIN tbl_EmployeeTravelApproval TA      
                        ON TA.TravelRequestID = TR.ID      
                 LEFT JOIN tbl_EmployeeMaster A      
                        ON A.ID = TA.ApproverID      
                 LEFT JOIN tbl_businessunit BU      
                        ON BU.ID = EM.BaseLocationID      
                 LEFT JOIN tbl_Department DEP      
                        ON DEP.ID = EM.DepartmentID      
                 LEFT JOIN tbl_Status ST      
                        ON ST.ID = TR.StatusID      
                 JOIN @BusinessUnitTable TBU      
                   ON TBU.BusinessUnit LIKE '%,'      
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))      
                                            + ',%'      
          WHERE  TR.IsDeleted = 0      
                 AND TR.DomainID = @DomainID      
                 AND ( ISNULL(@EmployeeID, 0) = 0      
                        OR( TR.EmployeeID = @EmployeeID ))      
                 AND ( @BusinessUnit = 0      
                        OR EM.BaseLocationID = @BusinessUnit )      
                 AND ( Isnull(@DepartmentID, '') = ''      
                        OR DEP.ID = @DepartmentID )      
                 AND ( Isnull(@StatusID, 0) = 0      
                        OR ST.ID = @StatusID )      
          ORDER  BY EM.FullName,      
                    TR.TravelDate      
      END TRY      
      BEGIN CATCH      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
