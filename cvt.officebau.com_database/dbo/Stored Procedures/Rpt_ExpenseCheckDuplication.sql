﻿/****************************************************************************               
CREATED BY  : JENNIFER.S           
CREATED DATE : 06-OCT-2016            
MODIFIED BY  :              
MODIFIED DATE :           
 <summary>            
 [Rpt_ExpenseCheckDuplication] 1,'TotalAmount'          
 </summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].Rpt_ExpenseCheckDuplication (@DomainID INT,
@ColumnName VARCHAR(1000))
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @Count INT
			   ,@NoOfRecords INT = (SELECT
						value
					FROM tblApplicationConfiguration
					WHERE IsDeleted = 0
					AND DomainID = @DomainID
					AND Code = 'NRECORD')
		DECLARE @table TABLE (
			DetailDescription VARCHAR(8000)
		   ,EID INT
		)

		INSERT INTO @table
			SELECT
				(SELECT
						SUBSTRING((SELECT
								+',' + ed.Remarks
							FROM tblExpenseDetails ed
							WHERE ed.IsDeleted = 0
							AND ex.Id = ed.ExpenseID
							FOR XML PATH (''))
						, 2, 200000))
			   ,ex.Id
			FROM tblExpense ex;

		WITH CTE
		AS
		(SELECT
				ex.Id AS Id
			   ,ex.[Type] AS [Type]
			   ,ex.[Date] AS BillDate
			   ,ex.DueDate AS DueDate
			   ,ex.BillNo AS BillNo
			   ,ex.VendorID AS VendorID
			   ,ex.CostCenterID AS CostCenterID
			   ,CC.Name AS CostCenterName
			   ,vd.Name AS VendorName
			   ,(SELECT
						ISNULL(DetailDescription, '')
					FROM @table d
					WHERE d.EID = ex.Id)
				AS [Description]
			   ,((SELECT
						((ISNULL(SUM(Qty * Amount), 0)) + ISNULL(SUM(CGST), 0) + ISNULL(SUM(SGST), 0))
					FROM tblExpenseDetails
					WHERE ExpenseID = ex.Id
					AND IsDeleted = 0
					AND DomainID = @domainID)
				) AS TotalAmount
			   ,cd.Code AS [Status]
			   ,ex.ModifiedOn,
			   CASE WHEN ((SELECT COUNT(*) FROM tblExpenseDetails  WHERE  isdeleted =0 AND ExpenseID = ex.Id AND isLedger =1) !=0)
			   THEN ( SELECT
		Name
	FROM tblLedger
	WHERE Id = (SELECT TOP 1
			LedgerId
		FROM tblExpenseDetails
		WHERE IsDeleted = 0
		AND ExpenseID = ex.Id
		AND IsLedger = 1) )
			   ELSE ( SELECT
		Name
	FROM tblProduct_v2 AS p
	WHERE Id = (SELECT TOP 1
			LedgerId
		FROM tblExpenseDetails
		WHERE IsDeleted = 0
		AND ExpenseID = ex.Id
		AND IsLedger = 0) )
	END AS Ledger
			FROM tblExpense ex
			--LEFT JOIN tblExpenseDetails ed    
			--       ON ed.ExpenseID = ex.ID    
			--LEFT JOIN tblLedger Ld    
			--       ON Ld.ID = ed.LedgerID    
			LEFT JOIN tblVendor vd
				ON vd.Id = ex.VendorID
			LEFT JOIN tbl_CodeMaster cd
				ON cd.Id = ex.StatusID
			LEFT JOIN tblCostCenter CC
				ON CC.Id = ex.CostCenterID
			WHERE ex.IsDeleted = 0
			AND ex.DomainID = @domainID)
		SELECT
			* INTO #TempExpense
		FROM CTE
		ORDER BY ModifiedOn DESC

		IF ISNULL(@ColumnName, '') = ''
		BEGIN
			SET @Count = (SELECT
					COUNT(1)
				FROM #TempExpense)

			SELECT TOP (ISNULL(@NoOfRecords, @Count))
				*
			FROM #TempExpense
		END
		ELSE
		BEGIN
			DECLARE @Condition VARCHAR(MAX) = (SELECT
					SUBSTRING((SELECT
							+' AND ex.' + Splitdata + '= ct.' + Splitdata
						FROM dbo.FnSplitString(@ColumnName, ',')
						FOR XML PATH (''))
					, 2, 200000))

			EXEC (';          
              WITH CTE          
                   AS (SELECT ' + @ColumnName + ', Count(1) AS tot          
                       FROM   #TempExpense          
                       GROUP  BY ' + @ColumnName + ')          
              SELECT ex.*          
              FROM   CTE ct JOIN #TempExpense ex ON 1 = 1 ' + @Condition + '          
              WHERE  tot <> 1          
     ORDER BY ' + @ColumnName + ',ex.ModifiedOn DESC')
		END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMsg VARCHAR(100)
			   ,@ErrSeverity TINYINT
		SELECT
			@ErrorMsg = ERROR_MESSAGE()
		   ,@ErrSeverity = ERROR_SEVERITY()
		RAISERROR (@ErrorMsg, @ErrSeverity, 1)
	END CATCH
END
