﻿

/**************************************************************************** 
CREATED BY    		:	Naneeshwar.M
CREATED DATE		:	04-AUG-2017
MODIFIED BY			:	Ajith N
MODIFIED DATE		:	12 Jan 2018
 <summary>  
	Managepayrollloanamortization 106, 2,2018,303,1,106
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManagePayrollLoanAmortization] (@EmployeeId INT,
                                                        @MonthId    INT,
                                                        @Year       INT,
                                                        @Loans      MONEY,
                                                        @DomainID   INT,
                                                        @LoginID    INT)
AS
  BEGIN
      SET NOCOUNT ON;
	
      DECLARE @Open             VARCHAR(10),
              @Payroll          VARCHAR(10),
              @Settled          INT,
              @Closed           INT,
              @ActualLoanAmount MONEY,
              @DueBalanceAmount MONEY,
              @CheckExists      INT,
              @LoanID           INT,
              @LoanStatusID     INT,
              @PayrollDate      DATE = CAST(( CONVERT(VARCHAR(10), @MonthId) + '-'
                  + CONVERT(VARCHAR(10), '1')
                  + '-' + CONVERT(VARCHAR(10), @Year))  AS DATE)

      SET @Open = (SELECT ID
                   FROM   tbl_Status
                   WHERE  Code = 'Open'
                          AND [Type] = 'Amortization'
                          AND IsDeleted = 0)
    
      SET @Payroll =(SELECT ID
                     FROM   tbl_Status
                     WHERE  Code = 'Payroll'
                            AND Type = 'Amortization'
                            AND IsDeleted = 0)
      
      SET @Settled = (SELECT ID
                      FROM   tbl_Status
                      WHERE  Code = 'Settled'
                             AND [Type] = 'Loan'
                             AND IsDeleted = 0)
      SET @Closed = (SELECT ID
                     FROM   tbl_Status
                     WHERE  Code = 'Closed'
                            AND [Type] = 'Loan'
                            AND IsDeleted = 0)
      SET @LoanID = Isnull((SELECT lr.ID
                            FROM   tbl_LoanRequest lr
                            WHERE  lr.EmployeeID = @EmployeeId
									and lr.DomainID = @DomainID
                                   AND lr.IsDeleted = 0
                                   AND lr.StatusID = @Settled), 0)
      SET @LoanStatusID = (SELECT StatusID
                           FROM   tbl_LoanRequest
                           WHERE  ID = @LoanID)

      BEGIN TRY
          BEGIN TRANSACTION

          IF ( @LoanID <> 0
               AND @LoanStatusID = @Settled )
            BEGIN
                SET @ActualLoanAmount = Isnull((SELECT Round(( la.Amount + la.InterestAmount ), 0)
                                                FROM   tbl_LoanAmortization la
                                                WHERE  la.LoanRequestID = @LoanID
                                                       AND la.IsDeleted = 0
                                                       AND la.StatusID = @Open
                                                       AND Month(la.DueDate) = Month(@PayrollDate)
                                                       AND Year(la.DueDate) = Year(@PayrollDate)), 0)
                SET @DueBalanceAmount = Isnull((SELECT Round(DueBalance, 0)
                                                FROM   tbl_LoanAmortization
                                                WHERE  loanrequestID = @LoanID
														AND IsDeleted = 0
                                                       AND Month(DueDate) = Month(Dateadd(month, -1, @PayrollDate))
                                                       AND Year(DueDate) = Year(Dateadd(month, -1, @PayrollDate))), 0)

                IF ( @Loans = Round(( @ActualLoanAmount + @DueBalanceAmount ), 0) )
                  BEGIN
                      IF EXISTS(SELECT 1
                                FROM   tbl_LoanAmortization
                                WHERE  LoanRequestID = @LoanID
										AND IsDeleted = 0
                                       AND Month(DueDate) = Month(@PayrollDate)
                                       AND Year(DueDate) = Year(@PayrollDate))
                        BEGIN
                            UPDATE LA
                            SET    LA.StatusID = @Payroll,
                                   LA.PaidAmount = @Loans
                            FROM   tbl_LoanRequest LR
                                   LEFT JOIN tbl_LoanAmortization LA
                                          ON LA.LoanRequestID = LR.ID
												AND LA.IsDeleted = 0
                                             AND Month(LA.DueDate) = Month(@PayrollDate)
                                             AND Year(LA.DueDate) = Year(@PayrollDate)
                            WHERE  lr.EmployeeID = @EmployeeID
                                   AND LR.ID = @LoanID
                        END
                      ELSE
                        BEGIN
                            INSERT INTO tbl_LoanAmortization
                                        (LoanRequestID,
                                         Amount,
                                         InterestAmount,
                                         DueDate,
                                         StatusID,
                                         DomainID,
                                         IsBalance,
                                         PaidAmount,
                                         DueBalance,
                                         CreatedBy,
                                         ModifiedBy)
                            SELECT @LoanID,
                                   0,
                                   0,
                                   Dateadd(month, 1, (SELECT TOP 1 DueDate
                                                      FROM   tbl_LoanAmortization
                                                      WHERE  loanrequestID = @LoanID
                                                            and IsDeleted=0
                                                      ORDER  BY DueDate DESC)),
                                   @Payroll,
                                   @DomainID,
                                   0,
                                   @Loans,
                                   0,
                                   @LoginID,
                                   @LoginID
                        END

                      IF( (SELECT Round(Sum(totalamount), 0)
                           FROM   tbl_LoanRequest
                           WHERE  ID = @LoanID) <= (SELECT Round(Sum(PaidAmount), 0) + count(1)
                                                   FROM   tbl_LoanAmortization
                                                   WHERE  LoanRequestID = @LoanID
                                                          AND Cast(DueDate AS DATE) <= @PayrollDate) )
                        BEGIN
                            UPDATE tbl_LoanRequest
                            SET    StatusID = @Closed
                            WHERE  ID = @LoanID
                        END
                  END
                ELSE IF ( @Loans < Round(( @ActualLoanAmount + @DueBalanceAmount ), 0) )
                  BEGIN
                      IF EXISTS(SELECT 1
                                FROM   tbl_LoanAmortization
                                WHERE  LoanRequestID = @LoanID
                                       AND Month(DueDate) = Month(@PayrollDate)
                                       AND Year(DueDate) = Year(@PayrollDate))
                        BEGIN
                   UPDATE LA
                            SET    LA.StatusID = @Payroll,
                                   LA.PaidAmount = @Loans,
                          LA.IsBalance = 1,
                                   LA.DueBalance = ( Round(( @ActualLoanAmount + @DueBalanceAmount ), 0) - @Loans )
                            FROM   tbl_LoanRequest LR
                                   LEFT JOIN tbl_LoanAmortization LA
                                          ON LA.LoanRequestID = LR.ID
                                             AND Month(LA.DueDate) = Month(@PayrollDate)
                                             AND Year(LA.DueDate) = Year(@PayrollDate)
                            WHERE  lr.EmployeeID = @EmployeeID
                                   AND LR.ID = @LoanID
                        END
                      ELSE
                        BEGIN
                            INSERT INTO tbl_LoanAmortization
                                        (LoanRequestID,
                                         Amount,
                                         InterestAmount,
                                         DueDate,
                                         StatusID,
                                         DomainID,
                                         IsBalance,
                                         PaidAmount,
                                         DueBalance,
                                         CreatedBy,
                                         ModifiedBy)
                            SELECT @LoanID,
                                   0,
                                   0,
                                   Dateadd(month, 1, (SELECT TOP 1 DueDate
                                                      FROM   tbl_LoanAmortization
                                                      WHERE  loanrequestID = @LoanID
                                                            and IsDeleted=0
                                                      ORDER  BY DueDate DESC)),
                                   @Open,
                                   @DomainID,
                                   1,
                                   @Loans,
                                   ( Round(( @ActualLoanAmount + @DueBalanceAmount ), 0) - @Loans ),
                                   @LoginID,
                                   @LoginID
                        END
                  END
           
            END

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
