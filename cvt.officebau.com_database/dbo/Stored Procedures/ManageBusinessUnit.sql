﻿/****************************************************************************     
CREATED BY   : K.SASIREKHA    
CREATED DATE  : 29-09-2017    
MODIFIED BY   :     
MODIFIED DATE  :      
<summary>              
[ManageBusinessUnit] 2,'Madhavaram','Madhavaram','as',1,1,0,1     
</summary>                              
*****************************************************************************/
CREATE PROCEDURE [dbo].[ManageBusinessUnit] (@ID        INT,
                                            @Name      VARCHAR(1000),
                                            @Remarks   VARCHAR(1000),
                                            @Address   VARCHAR(8000),
                                            @ParentID  INT,
                                            @SessionID INT,
                                            @IsDeleted INT,
                                            @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(8000)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF( Isnull(@ID, 0) != 0 )
            BEGIN
                IF( @IsDeleted = 1 )
                  BEGIN
                      IF EXISTS(SELECT 1
                                FROM   tbl_EmployeeMaster
                                WHERE  @ID = BaseLocationID
                                       AND DomainID = @DomainID
                                       AND IsDeleted = 0)
                        BEGIN
                            SET @Output = 'The record is referred.'

                            GOTO Finish
                        END
                      ELSE
                        BEGIN
                            UPDATE tbl_BusinessUnit
                            SET    IsDeleted = @IsDeleted,
                                   ModifiedBy = @SessionID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID
                                   AND DomainID = @DomainID

                            SET @Output = 'Deleted Successfully'

                            GOTO Finish
                        END
                  END

                IF( @IsDeleted = 0 )
                  BEGIN
                      IF EXISTS(SELECT 1
                                FROM   tbl_BusinessUnit
                                WHERE  @ID <> ID
                                       AND @ParentID = ParentID
                                       AND NAME = @Name
                                       AND DomainID = @DomainID
                                       AND IsDeleted = 0)
                        BEGIN
                            SET @Output = 'Already Exists.'

                            GOTO Finish
                        END
                      ELSE
                        BEGIN
                            UPDATE tbl_BusinessUnit
                            SET    NAME = @Name,
                                   Address = @Address,
                                   Remarks = @Remarks,
                                   ParentID = @ParentID,
                                   ModifiedBy = @SessionID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID
                                   AND DomainID = @DomainID

                            SET @Output = 'Updated Successfully.'

                            GOTO Finish
                        END
                  END
            END

          IF( Isnull(@ID, 0) = 0 )
            BEGIN
                IF EXISTS(SELECT 1
                          FROM   tbl_BusinessUnit
                          WHERE  @ID <> ID
                                 AND NAME = @Name
                                 AND @ParentID = ParentID
                                 AND DomainID = @DomainID
                                 AND IsDeleted = 0)
                  BEGIN
                      SET @Output = 'Already Exists.'

                      GOTO Finish
                  END
                ELSE
                  BEGIN
                      INSERT INTO tbl_BusinessUnit
                                  (NAME,
                                   Address,
                                   Remarks,
                                   ParentID,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   DomainID)
                      VALUES      ( @Name,
                                    @Address,
                                    @Remarks,
                                    @ParentID,
                                    @SessionID,
                                    Getdate(),
                                    @SessionID,
                                    Getdate(),
                                    @DomainID)

                      DECLARE @BusinessUnitIDs VARCHAR(100)=(SELECT BusinessUnitID
                        FROM   tbl_employeemaster
                        WHERE  Upper(FirstName) = Upper('ADMIN')
                               AND Code = '1000'
                               AND DomainID = @DomainID)

                      UPDATE tbl_employeemaster
                      SET    BusinessunitID = @BusinessUnitIDs + ''
                                              + Cast(@@IDENTITY AS VARCHAR) + ','
                      WHERE  Upper(FirstName) = Upper('ADMIN')
                             AND Code = '1000'
                             AND DomainID = @DomainID

                      SET @Output = 'Inserted Successfully.'

                      GOTO Finish
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
