﻿/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  : 06-Jun-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
      
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageLoginHistory] (@ID                INT,
                                             @UserID            INT,
                                             @IpAddress         VARCHAR(20),
                                             @LoginTime         DATETIME,
                                             @IsActive          BIT,
                                             @PhysicalAddress   VARCHAR(100),
                                             @Location          VARCHAR(500),
                                             @Latitudelongitude VARCHAR(500),
                                             @BrowserType       VARCHAR(50),
                                             @Device            VARCHAR(50),
                                             @LogOutTimeFlag    BIT,
                                             @Region            VARCHAR(100),
                                             @LogOutType        VARCHAR(50),
                                             @DomainID          INT,
                                             @ClientHostName    VARCHAR(100))
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(1000) = 'Operation Failed!'

      BEGIN TRY
          BEGIN TRANSACTION

          IF ISNULL(@ID, 0) = 0
            BEGIN
                INSERT INTO tbl_LoginHistory
                            (EmployeeID,
                             IPAddress,
                             LoginTime,
                             IsActive,
                             PhysicalAddress,
                             Location,
                             LatitudeLongitude,
                             BrowserType,
                             Device,
                             LogOutTime,
                             LogOutType,
                             DomainID,
                             ClientHostName)
                VALUES      (@UserID,
                             @IpAddress,
                             Getdate(),
                             @IsActive,
                             @PhysicalAddress,
                             @Location,
                             @Latitudelongitude,
                             @BrowserType,
                             @Device,
                             Getdate(),
                             @LogOutType,
                             @DomainID,
                             @ClientHostName)

                SET @Output = IDENT_CURRENT('tbl_LoginHistory')
            END
          ELSE
            BEGIN
                UPDATE tbl_LoginHistory
                SET    LogOutTime = Getdate(),
                       LogOutType = @LogOutType,
                       IsActive = 1
                -- Duration= CAST(DATEDIFF(hh, LoginTime, LogOutTime) AS Varchar(10))  + ','+  CAST(DATEDIFF(mi,DATEADD(hh,DATEDIFF(hh, LoginTime, LogOutTime),LoginTime),LogOutTime) as Varchar(100))  
                WHERE  ID = @ID
                       AND @IsActive <> 1
                       AND DomainID = @DomainID

                SET @Output = IDENT_CURRENT('tbl_LoginHistory')
            END

          --END  
          --ELSE  
          --SET @Output= 'New Device Please Select Option to send OTP.'  
          SELECT @Output AS UserMessage

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
