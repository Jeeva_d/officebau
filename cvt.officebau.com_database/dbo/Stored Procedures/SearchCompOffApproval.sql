﻿/****************************************************************************   
CREATED BY   : K.SASIREKHA  
CREATED DATE  : 29-09-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
  [SearchCompOffApproval] 1,71  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchCompOffApproval] (@DomainID   INT,  
                                               @EmployeeID INT,  
                                               @StatusID   INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN  
              SELECT ECO.ID                        AS ID,  
                     ECO.Date                      AS Date,  
                     ISNULL(EM.EmpCodePattern, '') + '' + EM.Code + ' - ' + EM.FullName AS EmployeeName,  
                     ATT.Duration                  AS Duration,  
                     ECO.RequesterRemarks          AS Remarks,  
                     ECO.CreatedOn                 AS CreatedOn,  
                     ECO.ApprovedDate              AS ApprovedDate,  
                     STA.Code                      AS Status  
              FROM   tbl_EmployeeCompOff ECO  
                     LEFT JOIN tbl_Attendance ATT  
                            ON ECO.CreatedBy = ATT.EmployeeID  
                               AND ECO.Date = ATT.LogDate  
                     LEFT JOIN tbl_Status STA  
                            ON STA.ID = ECO.ApproverStatusID  
                     LEFT JOIN tbl_EmployeeMaster EM  
                            ON ECO.CreatedBy = EM.ID  
              WHERE  ECO.ApproverID = @EmployeeID  
                     AND ECO.Isdeleted = 0  
                     AND ECO.DomainID = @DomainID  
                     AND ( ISNull(@StatusID, 0) = 0  
                            OR ECO.ApproverStatusID = @StatusID )  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
