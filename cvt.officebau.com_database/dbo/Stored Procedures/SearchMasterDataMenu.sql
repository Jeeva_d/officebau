﻿/****************************************************************************       
CREATED BY   :       
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>              
         [SearchMasterDataMenu] 1  
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchMasterDataMenu] (@DomainID INT,@Rootconfig Varchar(20))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT NAME           AS Menu,  
                 MenuParameters AS MenuParameters  
          FROM   tbl_MasterMenu  
          WHERE  DomainID = @domainID and RootConfig=@Rootconfig 
          ORDER  BY NAME  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
