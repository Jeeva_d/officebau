﻿/****************************************************************************   
CREATED BY		:  Naneeshwar.M
CREATED DATE	:  20-SEP-2017
MODIFIED BY		:  Jennifer.S
MODIFIED DATE	:  16-OCT-2017
 <summary> 
          
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Email_GetNewJoinerList]
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN
              DECLARE @KeyID INT = (SELECT ID
                 FROM   tbl_MailerEventKeys
                 WHERE  NAME = 'NEWJOIN'
                        AND Isdeleted = 0)

              SELECT emp.EmailID                                                           AS EmailID,
                     FirstName + ' ' + Isnull(LastName, '')                                AS EmployeeName,
                     com.FullName                                                          AS CompanyName,
                     Cast(DOJ AS DATE)                                                     AS DOJ,
                     BU.NAME                                                               AS Location,
                     Stuff((SELECT DISTINCT ';' + EmailID
                            FROM   tbl_EmployeeMaster emp
                                   JOIN (SELECT *
                                         FROM   dbo.Splitstring ((SELECT CCEmailID
                                                                  FROM   tbl_EmailRecipients
                                                                  WHERE  BusinessUnitID = emp.BaseLocationID
                                                                         AND KeyID = @KeyID), ',')
                                         WHERE  Isnull(Item, '') <> '') eml
                                     ON emp.ID = eml.Item
                            FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') AS CC
              FROM   tbl_EmployeeMaster emp
                     LEFT JOIN tbl_Company com
                            ON com.ID = emp.DomainID
                     LEFT JOIN tbl_BusinessUnit BU
                            ON bu.ID = emp.BaseLocationID
              WHERE  emp.IsDeleted = 0
                     AND emp.IsActive = 0
                     AND HasAccess = 1
                     AND Cast(DOJ AS DATE)= Cast(Getdate() AS DATE)
          END
      END TRY
      BEGIN CATCH         
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
