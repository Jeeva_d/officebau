﻿/****************************************************************************     
CREATED BY   : Naneeshwar    
CREATED DATE  :   12-Dec-2016  
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [Getaccountreceipts] 2,1,1  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getaccountreceipts] (@ID       INT,
                                            @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT ar.ID              AS ID,
                 ar.Date            AS Date,
                 ar.PartyName       AS PartyName,
                 ar.LedgerID        AS LedgerID,
                 ar.PaymentMode     AS PaymentMode,
                 ar.BankID          AS BankID,
                 ar.Description     AS [Description],
                 ar.Reference       AS Reference,
                 ar.ReceiptNo       AS ReceiptNo,
                 ar.TransactionType AS TransactionType,
                 ar.Amount          AS Amount,
                 ar.CurrencyID      AS CurrencyID,
                 ar.CurrencyRate    AS CurrencyRate,
                 ar.ModifiedOn      AS ModifiedOn,
                 EMP.FullName       AS ModifiedBy
          FROM   tblReceipts ar
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = ar.ModifiedBy
          WHERE  ar.IsDeleted = 0
                 AND ( ar.ID = @ID
                        OR @ID IS NULL )
                 AND ar.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
