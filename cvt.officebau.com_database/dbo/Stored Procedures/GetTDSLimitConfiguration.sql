﻿/****************************************************************************   
CREATED BY		: K.SASIREKHA 
CREATED DATE	: 29-09-2017 
MODIFIED BY		: DHANALAKSHMI.S
MODIFIED DATE	: 26-02-2018  
 <summary>
 [GetTDSLimitConfiguration] 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetTDSLimitConfiguration] (@ID       INT,
                                                   @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT CON.ID,
                 CON.SectionID,
                 CON.FYID,
                 CON.Limit,
                 CON.ModifiedOn,
                 EMP.FullName AS ModifiedBy,
                 BU.NAME      AS BaseLocation,
                 CON.RegionID AS RegionID
          FROM   tbl_TDSLimitConfiguration CON
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = CON.ModifiedBy
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EMP.BaseLocationID
          WHERE  CON.IsDeleted = 0
                 AND CON.ID = @ID
                 AND CON.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
