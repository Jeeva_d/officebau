﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  : 21/03/2016  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>    
    SearchEmployeeHRA 0,1,71  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchEmployeeHRA] (@EmployeeID INT,  
                                           @DomainID   INT,  
                                           @SessionID     INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @DataSource TABLE  
        (  
           ID      INT IDENTITY(1, 1),  
           [Value] NVARCHAR(128)  
        )  
  
      INSERT INTO @DataSource  
                  ([Value])  
      SELECT Item  
      FROM   dbo.Splitstring ((SELECT BusinessUnitID  
                               FROM   tbl_EmployeeMaster  
                               WHERE  ID = @SessionID  
                                      AND DomainID = @DomainID), ',')  
      WHERE  Isnull(Item, '') <> ''  
  
      BEGIN TRY  
          SELECT DISTINCT hra.EmployeeID                                 EmployeeID,  
                          emp.FirstName + ' ' + Isnull(emp.LastName, '') EmployeeName,  
                          ISNULL(emp.EmpCodePattern, '') + emp.Code                                       EmployeeCode  
          FROM   HRA hra  
                 JOIN tbl_EmployeeMaster emp  
                   ON hra.EmployeeID = emp.ID  
          WHERE  hra.IsDeleted = 0  
                 AND hra.DomainID = @DomainID  
                 AND ( Isnull(@EmployeeID, 0) = 0  
                        OR hra.EmployeeID = Isnull(@EmployeeID, 0) )  
                 AND emp.BaseLocationID IN(SELECT Value  
                                           FROM   @DataSource)  
          ORDER  BY emp.FirstName + ' ' + Isnull(emp.LastName, '')  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
