﻿/****************************************************************************               
CREATED BY  :   Ajith N             
CREATED DATE :  20 May 2019             
MODIFIED BY  :              
MODIFIED DATE   :            
 <summary>                      
     
 </summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CalculeteFAndFAmount] (@TotalGrossSalary  MONEY,
                                              @LastWorkingDate   DATETIME,
                                              @TotalDaysInMonth  INT,
                                              @DaysPaid          INT,
                                              @LeaveEncashedDays INT,
                                              @EmployeeID        INT,
                                              @DomainID          INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @sql                                    VARCHAR(MAX),
                  @ServiceYear                            INT,
                  @ESBCalculation                         DECIMAL(32, 4),
                  @ESBAmount                              DECIMAL(32, 2),
                  @TerminatedBeforeContractEndDatePayment DECIMAL(32, 2)--,
      --@TotalServiceDays                       INT = Datediff(DAY, @OriginalHireDate, @TerminationDate)
      --SET @ServiceYear = @TotalServiceDays / 356;
      --SET @ESBCalculation = ( CASE
      --                          WHEN @TerminationReason LIKE '%Resigned%' THEN
      --                            ( CASE
      --                                WHEN @ServiceYear < 2 THEN
      --                                  '0.00'
      --                                WHEN @ServiceYear BETWEEN 2 AND 5 THEN
      --                                  Cast(Cast(( @ServiceYear * ( 16.67 / 100.0 ) ) AS DECIMAL(32, 4)) AS VARCHAR)
      --                                WHEN @ServiceYear BETWEEN 5 AND 10 THEN
      --                                  -- Cast(Cast(( ( 33.33 + ( ( @ServiceYear - 5 ) * 66.67 ) ) / @ServiceYear ) AS DECIMAL(8, 2)) AS VARCHAR)
      --                                  Cast(Cast(( ( 5 * 33.33 / 100.0 ) + ( ( @ServiceYear - 5 ) * 66.67 ) / 100.0 ) AS DECIMAL(32, 4)) AS VARCHAR)
      --                                ELSE
      --                                  --Cast(Cast(( ( 50 + ( ( @ServiceYear - 5 ) * 100.0 ) ) / @ServiceYear ) AS DECIMAL(8, 2)) AS VARCHAR)
      --                                  --Cast(Cast(( ( 5/2.0 + ( ( @ServiceYear - 5 ) ) )  ) AS DECIMAL(8, 2)) AS VARCHAR)
      --                                  Cast(Cast(( ( 5 * 50 / 100.0 ) + ( ( @ServiceYear - 5 ) * 100.0 ) / 100.0 ) AS DECIMAL(32, 2)) AS VARCHAR)
      --                              END )
      --                          WHEN @TerminationReason LIKE ( '%Termination%' )
      --                                OR @TerminationReason LIKE ( '%Terminated%' ) THEN
      --                            ( CASE
      --                                WHEN @ServiceYear <= 5 THEN
      --                                  Cast(Cast(( @ServiceYear * 50 / 100.0 ) AS DECIMAL(32, 2)) AS VARCHAR)
      --                                ELSE
      --                                  --Cast(Cast(( ( 50 + ( ( @ServiceYear - 5 ) * 100.0 ) ) / @ServiceYear ) AS DECIMAL(8, 2)) AS VARCHAR)
      --                                  -- Cast(Cast(( ( 5/2.0 + ( ( @ServiceYear - 5 ) ) )  ) AS DECIMAL(8, 2)) AS VARCHAR)
      --                                  Cast(Cast(( ( 5 * 50 / 100.0 ) + ( ( @ServiceYear - 5 ) * 100.0 ) / 100.0 ) AS DECIMAL(32, 2)) AS VARCHAR)
      --                              END )
      --                          ELSE
      --                            '0.00'
      --                        END )
      --SET @ESBAmount = CONVERT(DECIMAL(32, 2), (( @TotalGrossSalary * @ESBCalculation )))
      --SET @TerminatedBeforeContractEndDatePayment = ( CASE
      --                                                  WHEN @TerminationReason LIKE '%Resigned%' THEN
      --                                                    '0.00'
      --                                                  WHEN @TerminationReason LIKE ( '%Termination%' )
      --                                                        OR @TerminationReason LIKE ( '%Terminated%' ) THEN
      --                                                    ( CASE
      --                                                        WHEN @ContractEndDate > @TerminationDate THEN
      --                                                          ( 2 * @ESBAmount )
      --                                                        ELSE
      --                                                          0
      --                                                      END )
      --                                                  ELSE
      --                                                    '0.00'
      --                                                END )
      --CREATE TABLE #tmpEOSList
      --  (
      --     id      INT IDENTITY(1, 1),
      --     Ordinal INT,
      --     [Name]  VARCHAR(50),
      --     Value   VARCHAR(50)
      --  )
      --INSERT INTO #tmpEOSList
      --SELECT 1,
      --       'ESBInYears',
      --       Cast(@ServiceYear AS VARCHAR)
      --UNION
      --SELECT 2,
      --       'YearsOfService',
      --       Cast(@ServiceYear AS VARCHAR)
      --       + ' year(s) '
      --       + Cast(((@TotalServiceDays % 365) / 30) AS VARCHAR)
      --       + ' Month(s) '
      --       + Cast(((@TotalServiceDays % 365) % 30) AS VARCHAR)
      --       + ' Day(s)'
      --UNION
      --SELECT 3,
      --       'ESBCalculation',
      --       Cast(Cast(@ESBCalculation AS DECIMAL(32, 2)) AS VARCHAR)
      --UNION
      --SELECT 4,
      --       'ESBAmount',
      --       Cast(@ESBAmount AS VARCHAR)
      --UNION
      --SELECT 5,
      --       'TerminatedBeforeContractEndDatePayment',
      --       Cast(@TerminatedBeforeContractEndDatePayment AS VARCHAR)
      --UNION
      --SELECT 6,
      --       'NetESBAmount',
      --       Cast(( @ESBAmount
      --              + @TerminatedBeforeContractEndDatePayment ) AS VARCHAR)
      --SELECT *
      --FROM   #tmpEOSList
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
