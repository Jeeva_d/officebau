﻿/****************************************************************************                   
CREATED BY   :                   
CREATED DATE  :                
MODIFIED BY   :                   
MODIFIED DATE  :        ,           
 <summary>         
 [SearchSOINVOICEDetailList] ',8,' ,1              
 </summary>                                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchSOINVOICEDetailList] (@SoID VARCHAR(MAX),
@DomainID INT)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION

		SELECT
			ed.ID AS ID
		   ,ed.ProductID AS ProductID
		   ,ed.ItemDescription AS [Description]
		   ,QTY - ISNULL(InvoicedQTY, 0) AS QTY
		   ,IsProduct AS IsProduct
		   ,ed.Rate AS Rate
		   ,ISNULL(CGST, 0) CGST
		   ,ISNULL(SGST, 0) SGST
		FROM tblSOItem ed
		WHERE ed.IsDeleted = 0
		AND ed.InvoicedQTY <>QTY
		AND ed.SOID IN (SELECT
				*
			FROM dbo.FnSplitString(@SoID, ','))
		AND ed.DomainID = @DomainID

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMsg VARCHAR(100)
			   ,@ErrSeverity TINYINT
		SELECT
			@ErrorMsg = ERROR_MESSAGE()
		   ,@ErrSeverity = ERROR_SEVERITY()
		RAISERROR (@ErrorMsg, @ErrSeverity, 1)
	END CATCH
END
