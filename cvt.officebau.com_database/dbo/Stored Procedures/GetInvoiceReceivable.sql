﻿/****************************************************************************     
CREATED BY   : Naneeshwar    
CREATED DATE  :   21-Nov-2016  
MODIFIED BY   :     
MODIFIED DATE  :     
<summary>  
[GetInvoiceReceivable] 14,1  
</summary>                             
*****************************************************************************/
CREATE PROCEDURE [dbo].[GetInvoiceReceivable] (@ID       INT,
                                               @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT ip.ID                 AS ID,
                 ip.PaymentDate        AS ReceivedDate,
                 ip.LedgerID           AS LedgerID,
                 ip.PaymentModeID      AS PaymentModeID,
                 ip.BankID             AS BankID,
                 inv.CustomerID        AS CustomerID,
                 cus.NAME              AS CustomerName,
                 cus.PaymentTerms      AS PaymentTerms,
                 ( CASE
                     WHEN ( inv.DiscountPercentage <> 0 ) THEN
                       (SELECT ( Isnull(Sum(Qty * Rate)
                                        + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum(Qty * Rate) * inv.DiscountPercentage ), 0) ), 0) )
                        FROM   tblInvoiceItem
                        WHERE  InvoiceID = inv.ID
                               AND IsDeleted = 0
                               AND DomainID = @DomainID)
                     ELSE
                       (SELECT ( Isnull(( Sum(Qty * Rate)
                                          + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(inv.DiscountValue, 0) ), 0) )
                        FROM   tblInvoiceItem
                        WHERE  InvoiceID = inv.ID
                               AND IsDeleted = 0
                               AND DomainID = @DomainID)
                   END )               AS TotalAmount,
                 inv.ReceivedAmount    AS ReceivedAmount,
                 inv.CurrencyRate      AS CurrencyRate,
                 cur.NAME              AS Currency,
                 inv.ModifiedOn        AS ModifiedOn,
                 EMP.FullName          AS ModifiedBy,
                 ip.PaymentDescription AS [Description],
                 ip.Reference          AS Reference
          FROM   tblInvoicepayment ip
                 LEFT JOIN tblInvoicePaymentMapping im
                        ON im.InvoicePaymentID = ip.id
                 LEFT JOIN tblInvoice inv
                        ON inv.id = im.invoiceID
                 LEFT JOIN tblCustomer cus
                        ON cus.id = inv.CustomerID
                 LEFT JOIN tbl_CodeMaster cd
                        ON cd.ID = inv.DiscountID
                 LEFT JOIN tblcurrency cur
                        ON cur.id = cus.CurrencyID
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = inv.ModifiedBy
          WHERE  ip.IsDeleted = 0
                 AND ( ip.ID = @ID
                        OR @ID IS NULL )
                 AND ip.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
