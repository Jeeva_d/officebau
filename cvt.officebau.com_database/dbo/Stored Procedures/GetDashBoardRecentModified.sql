﻿/****************************************************************************             
CREATED BY   :             
CREATED DATE  :             
MODIFIED BY   :             
MODIFIED DATE  :             
<summary>                    
 [GetDashBoardRecentModified] 1            
</summary>                                     
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[GetDashBoardRecentModified] (@DomainID INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY ;    
          WITH CTE    
               AS (SELECT CASE    
                            WHEN( e.Type = 'Expense' ) THEN( 'EXP ( ' + LEFT(v.NAME, 4) + '.. )' )    
                            ELSE( 'BIL ( ' + LEFT(v.NAME, 4) + '.. )' )    
                          END                                                                           AS Type,    
                          v.NAME + '-' + ( Datename(DD, e.Date) ) + ' ' + LEFT(Datename(MM, e.Date), 3) AS PartyName,    
                          ( (SELECT ( Isnull(Sum(Amount*QTY), 0)    
                                      + Isnull(Sum(CGST), 0) + Isnull(Sum(SGST), 0) )    
                             FROM   tblExpenseDetails    
                             WHERE  ExpenseID = e.ID    
                                    AND IsDeleted = 0    
                                    AND DomainID = @DomainID)    
                             )                                       AS Amount,    
                          e.ModifiedOn,    
                          e.ID    
                   FROM   tblExpense e    
                          LEFT JOIN tblVendor v    
                                 ON v.ID = e.VendorID    
                   WHERE  e.IsDeleted = 0    
                          AND e.DomainID = @DomainID    
                   UNION ALL    
                   SELECT 'INV ( ' + LEFT(c.NAME, 4) + '.. )'                                               AS Type,    
                          c.NAME + '-' + ( Datename(DD, inv.Date) ) + ' ' + LEFT(Datename(MM, inv.Date), 3) AS PartyName,    
                          ( CASE    
                              WHEN ( inv.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)    
                                                                                         + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum(Qty * Rate) * inv.DiscountPercentage ), 0) ), 0) )    
                                                                         FROM   tblInvoiceItem    
                                                                         WHERE  InvoiceID = inv.ID    
                                                                                AND IsDeleted = 0    
                                                                                AND DomainID = @DomainID)    
                              ELSE (SELECT ( Isnull(( Sum(Qty * Rate)    
                                                      + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(inv.DiscountValue, 0) ), 0) )    
                                    FROM   tblInvoiceItem    
                                    WHERE  InvoiceID = inv.ID    
                                           AND IsDeleted = 0    
                                           AND DomainID = @DomainID)    
                            END )                                                                           AS Amount,    
                          inv.ModifiedOn,    
                          inv.ID    
                   FROM   tblInvoice inv    
                          LEFT JOIN tblCustomer c    
                                 ON c.ID = inv.CustomerID    
                   WHERE  inv.IsDeleted = 0    
                          AND inv.DomainID = @DomainID    
                   UNION ALL    
         SELECT DISTINCT 'PAY ( ' + LEFT(v.NAME, 4) + '.. )'                                           AS Type,    
                                   v.NAME + '-' + ( Datename(DD, e.Date) ) + ' ' + LEFT(Datename(MM, e.Date), 3) AS PartyName,    
                                   (SELECT Sum(Amount)    
                                    FROM   tblexpensepaymentMapping    
                                    WHERE  expensePaymentID = e.ID    
                                           AND DomainID = @DomainID),    
                                   e.ModifiedOn,    
                                   e.ID    
                   FROM   tblExpensePayment e    
                          LEFT JOIN tblexpensepaymentMapping em    
                                 ON e.ID = em.ExpensePaymentID    
                          LEFT JOIN tblExpense ex    
                                 ON em.ExpenseID = ex.ID    
                          LEFT JOIN tblVendor v    
                                 ON v.ID = ex.VendorID    
                   WHERE  ex.Type <> 'Expense'    
                          AND e.IsDeleted = 0    
                          AND e.DomainID = @DomainID    
                   UNION ALL    
                   SELECT DISTINCT 'REC ( ' + LEFT(c.NAME, 4) + '.. )'                                                         AS Type,    
                                   c.NAME + '-' + ( Datename(DD, i.PaymentDate) ) + ' ' + LEFT(Datename(MM, i.PaymentDate), 3) AS PartyName,    
                                   (SELECT Sum(Amount)    
                                    FROM   tblInvoicePaymentMapping    
                                    WHERE  InvoicePaymentID = i.ID    
                                           AND DomainID = @DomainID),    
                                   i.ModifiedOn,    
                                   i.ID    
                   FROM   tblInvoicePayment i    
                          LEFT JOIN tblInvoicePaymentMapping im    
                                 ON i.ID = im.InvoicePaymentID    
                          LEFT JOIN tblInvoice inv    
                                 ON im.InvoiceID = inv.ID    
                          LEFT JOIN tblCustomer c    
                                 ON c.ID = inv.CustomerID    
                   WHERE  i.IsDeleted = 0    
                          AND i.DomainID = @DomainID    
                   UNION ALL    
                   --SELECT CASE    
                   --         WHEN( (SELECT NAME    
                   --                FROM   tblMasterTypes cd    
                   --                WHERE  cd.id = r.TransactionType) = 'Receive Payment' ) THEN( 'RPT ( ' + LEFT(PartyName, 4) + '.. )' )    
                   --         ELSE( 'PMT ( ' + LEFT(PartyName, 4) + '.. )' )    
                   --       END                                                                              AS Type,    
                   --       PartyName + '-' + ( Datename(DD, r.Date) ) + ' ' + LEFT(Datename(MM, r.Date), 3) AS PartyName,    
                   --       Amount,    
                   --       r.ModifiedOn,    
                   --       r.ID    
                   --FROM   tblReceipts r    
                   --WHERE  IsDeleted = 0    
                   --       AND r.DomainID = @DomainID    
       --------------------------------  
        SELECT  'RPT ( ' + LEFT(PartyName, 4) + '.. )'        AS Type,    
                          PartyName + '-' + ( Datename(DD, r.PaymentDate) ) + ' ' + LEFT(Datename(MM, r.PaymentDate), 3) AS PartyName,    
                          TotalAmount,    
                          r.ModifiedOn,    
                          r.ID    
                   FROM   tbl_Receipts r    
                   WHERE  IsDeleted = 0    
                          AND r.DomainID = @DomainID    
        Union ALL  
    SELECT 'PMT ( ' + LEFT(PartyName, 4) + '.. )'     AS Type,    
                          PartyName + '-' + ( Datename(DD, r.PaymentDate) ) + ' ' + LEFT(Datename(MM, r.PaymentDate), 3) AS PartyName,    
                          TotalAmount,    
                          r.ModifiedOn,    
                          r.ID    
 FROM   tbl_Payment r    
                   WHERE  IsDeleted = 0    
                          AND r.DomainID = @DomainID    
       --------------------------------  
                   UNION ALL    
                   SELECT 'CONT ( ' + LEFT(Isnull(b.DisplayName, 'Cash'), 4)    
                          + '.. )'                                                                                                    AS Type,    
                          Isnull(b.DisplayName, 'Cash') + '-' + ( Datename(DD, t.VoucherDate) ) + ' ' + LEFT(Datename(MM, t.VoucherDate), 3) AS PartyName,    
                          Amount,    
                          t.ModifiedOn,    
                          t.ID    
                   FROM   tblTransfer t    
                          LEFT JOIN tblBank b    
                                 ON b.ID = t.FromID    
                   --OR b.ID = t.ToID            
                   WHERE  t.IsDeleted = 0    
                          AND t.DomainID = @DomainID)    
          SELECT TOP 15 Type AS Type,    
                        PartyName,    
                        Amount,    
                        ID    
          FROM   CTE    
          ORDER  BY ModifiedOn DESC    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
