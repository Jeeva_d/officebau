﻿  
/****************************************************************************     
CREATED BY   :     
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>     
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageEmailConfiguration] (@TO        VARCHAR(MAX),  
                                                  @CC        VARCHAR(MAX),  
                                                  @BCC       VARCHAR(MAX),  
                                                  @SessionID INT,  
                                                  @DomainID  INT,  
              @ISATTACHMENT BIT,  
                                                  @Key       VARCHAR(20))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(1000)  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          IF( (SELECT Count(1)  
               FROM   tblEmailConfig  
               WHERE  DomainID = @DomainID  
                      AND [key] = @Key) = 0 )  
            BEGIN  
                INSERT INTO tblEmailConfig  
                            (Toid,  
        IsAttachment,  
                             CC,  
                             BCC,  
                             [key],  
                             DomainID,  
                             CreatedBy,  
                             ModifiedBy)  
                VALUES      (@TO,  
                 @ISATTACHMENT,  
                             @CC,  
                             @BCC,  
                             @Key,  
                             @DomainID,  
                             @SessionID,  
                             @SessionID )  
  
                SET @Output = (SELECT [Message]  
                               FROM   tblErrorMessage  
                               WHERE  [Type] = 'Information'  
                                      AND Code = 'RCD_INS'  
                                      AND IsDeleted = 0) --'Inserted Successfully.'    
            END  
          ELSE  
            BEGIN  
                UPDATE tblEmailConfig  
                SET    Toid = @TO,  
           IsAttachment = @ISATTACHMENT,  
                       CC = @CC,  
                       bcc = @BCC,  
                       ModifiedBy = @SessionID,  
                       ModifiedOn = Getdate()  
                WHERE  DomainID = @DomainID  
                       AND [key] = @Key  
  
                SET @Output = (SELECT [Message]  
                               FROM   tblErrorMessage  
                               WHERE  [Type] = 'Information'  
                                      AND Code = 'RCD_UPD'  
                                      AND IsDeleted = 0) --'Updated Successfully.'    
                GOTO Finish  
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
