﻿/****************************************************************************       
CREATED BY  :   DHANALAKSHMI. S   
CREATED DATE :   02-MAR-2018    
MODIFIED BY  :     
MODIFIED DATE :     
 <summary>    
  
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageRevertLeave] (@EmployeeID INT,
                                           @LeaveDate  DATE,
                                           @SessionID  INT,
                                           @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @LeaveTypeID    INT,
                  @IsHalfDay      BIT,
                  @Output         VARCHAR(100) = 'Operation Failed!',
                  @LeaveRequestID INT,
                  @Duration       DECIMAL(8, 2),
                  @SessionCode    VARCHAR(50) = (SELECT FullName
                     FROM   tbl_EmployeeMaster
                     WHERE  ID = @SessionID),
                  @RevertRemarks  VARCHAR(300)

          SET @RevertRemarks = 'Leave is reverted by '
                               + Isnull(@SessionCode, '') + ''

          SELECT TOP 1 @LeaveRequestID = LeaveRequestID,
                       @LeaveTypeID = LeaveTypeID,
                       @IsHalfDay = IsHalfDay,
                       @Duration = Cast(EL.Duration AS DECIMAL(8, 2))
          FROM   tbl_EmployeeLeaveDateMapping EM
                 JOIN tbl_EmployeeLeave EL
                   ON EL.ID = EM.LeaveRequestID
          WHERE  EM.EmployeeID = @EmployeeID
                 AND EM.IsDeleted = 0
                 AND EM.DomainID = @DomainID
                 AND EM.LeaveDate = @LeaveDate
                 AND EL.IsDeleted = 0

          BEGIN
              IF EXISTS(SELECT 1
                        FROM   tbl_EmployeeLeave
                        WHERE  EmployeeID = @EmployeeID
                               AND ID = @LeaveRequestID
                               AND DomainID = @DomainID
                               AND IsDeleted = 0
                               AND LeaveTypeID NOT IN (SELECT ID
                                                       FROM   tbl_LeaveTypes
                                                       WHERE  Code LIKE '%Onduty%'
                                                               OR Code LIKE '%Permission%'
                                                                  AND IsDeleted = 0
                                                                  AND DomainID = @DomainID)
                               AND StatusID IN (SELECT ID
                                                FROM   tbl_Status
                                                WHERE  Code = 'Approved'
                                                       AND [Type] = 'Leave'
                                                       AND IsDeleted = 0))
                BEGIN
                    IF NOT EXISTS (SELECT 1
                                   FROM   tbl_pay_EmployeePayroll
                                   WHERE  IsDeleted = 0
                                          AND DomainId = @DomainID
                                          AND EmployeeId = @EmployeeID
                                          AND ( MonthId = Datepart(MM, @LeaveDate)
                                                AND YearId = Datepart(YYYY, @LeaveDate) ))
                      BEGIN
                          BEGIN
                              UPDATE tbl_EmployeeLeave
                              SET    IsDeleted = 1,
                                     RevertRemarks = @RevertRemarks,
                                     ModifiedBy = @SessionID,
                                     ModifiedOn = Getdate()
                              WHERE  ID = @LeaveRequestID
                                     AND DomainID = @DomainID

                              UPDATE tbl_EmployeeLeaveDateMapping
                              SET    IsDeleted = 1,
                                     ModifiedBy = @SessionID,
                                     ModifiedOn = Getdate()
                              WHERE  EmployeeID = @EmployeeID
                                     AND LeaveRequestID = @LeaveRequestID
                                     AND DomainID = @DomainID
                          END

                          SET @Output = 'Leave is Reverted Successfully.'

                          IF( @Output LIKE '%Successfully%' )
                            BEGIN
                                IF EXISTS(SELECT 1
                                          FROM   tbl_EmployeeAvailedLeave
                                          WHERE  EmployeeID = @EmployeeID
                                                 AND Year = Year(@LeaveDate)
                                                 AND (SELECT ID
                                                      FROM   tbl_LeaveTypes
                                                      WHERE  NAME = 'Permission'
                                                             AND DomainID = @DomainID) = @LeaveTypeID
                                                  OR LeaveTypeID = @LeaveTypeID)
                                  BEGIN
                                      UPDATE tbl_EmployeeAvailedLeave
                                      SET    AvailedLeave = CASE
                                                              WHEN Isnull(@IsHalfDay, 'false') = 'false' THEN
                                                                ( Isnull(AvailedLeave, 0) - Isnull((SELECT Count(1)
                                                                                                    FROM   tbl_EmployeeLeaveDateMapping
                                                                                                    WHERE  EmployeeID = @EmployeeID
                                                                                                           AND LeaveRequestID = @LeaveRequestID), 0) )
                                                              ELSE
                                                                ( Isnull(AvailedLeave, 0) - 0.5 )
                                                            END,
                                             ModifiedBy = @SessionID,
                                             ModifiedOn = Getdate()
                                      WHERE  EmployeeID = @EmployeeID
                                             AND Year = Year(@LeaveDate)
                                             AND DomainID = @DomainID
                                             AND LeaveTypeID = @LeaveTypeID

                                      DECLARE @PunchType VARCHAR(100) = ( CASE
                                            WHEN @Duration IS NOT NULL THEN
                                              'Permission'
                                            ELSE
                                              ( CASE
                                                  WHEN Isnull(@IsHalfDay, 'false') = 'false' THEN
                                                    'FullDay'
                                                  ELSE
                                                    'HalfDay'
                                                END )
                                          END )

                                      EXEC UpdateBiometricPunches
                                        @EmployeeID,
                                        @LeaveDate,
                                        @PunchType,
                                        'REVERT',
                                        @Duration,
                                        NULL,
                                        @SessionID,
                                        @DomainID
                                  END
                                ELSE
                                  SET @Output = 'No Records Found.'
                            END
                      END
                    ELSE
                      SET @Output = 'Leave cannot be revert as the Payroll is already processed for the selected month.'
                END
              ELSE
                SET @Output = 'No Records Found.'
          END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
