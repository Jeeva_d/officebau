﻿/****************************************************************************       
CREATED BY   :       
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>    
 [Pay_SearchPayrollComponents]  7   
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_SearchPayrollComponents] (@DomainID INT)  
AS  
  BEGIN  
      BEGIN TRY  
          IF NOT EXISTS (SELECT TOP 1 ID  
                         FROM   tbl_Pay_PayrollCompontents AS tppc  
                         WHERE  tppc.Code = 'TAX'  
                                AND tppc.IsDeleted = 0  
                                AND DomainID = @DomainID)  
            BEGIN  
                INSERT INTO tbl_Pay_PayrollCompontents  
                            (tbl_Pay_PayrollCompontents.Code,  
                             tbl_Pay_PayrollCompontents.Description,  
                             tbl_Pay_PayrollCompontents.Ordinal,  
                             tbl_Pay_PayrollCompontents.DomainID)  
                SELECT 'TAX',  
                       'TAX',  
                       2,  
                       @DomainID  
            END  
  
          SELECT ppc.ID,  
                 ppc.Code                     AS Code,  
                 Description,  
                 Ordinal                      AS Ordinal,  
                 ISNULL(em.FullName, 'Admin') AS ModifiedBy,  
                 ppc.ModifiedOn               AS ModifiedOn  
          FROM   tbl_Pay_PayrollCompontents ppc  
                 LEFT JOIN tbl_EmployeeMaster em  
                        ON em.ID = ppc.ModifiedBy  
          WHERE  ppc.DomainID = @DomainID  
                 AND ppc.IsDeleted = 0  
          ORDER  BY ppc.Ordinal  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
