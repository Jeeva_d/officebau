﻿
/****************************************************************************         
CREATED BY  : Naneeshwar        
CREATED DATE :         
MODIFIED BY  :   
MODIFIED DATE :         
 <summary>         
 [Managesystembankbalance] 1  
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Managesystembankbalance] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(50)

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT bb.ID,
                 bb.BRSID,
                 brs.BankID,
                 brs.SourceDate,
                 brs.Amount,
                 bb.ClosingBalance,
                 bnk.OpeningBalance,
                 brs.SourceType,
                 CASE
                   WHEN mst.NAME = 'Expense'
                         OR mst.NAME = 'PayBill'
                         OR mst.NAME = 'Make Payment'
                         OR mst.NAME = 'FromBank' THEN ( -1 ) * brs.Amount
                   ELSE brs.Amount
                 END                                       SourceAmount,
                 Row_number()
                   OVER (
                     partition BY brs.BankID
                     ORDER BY brs.BankID, brs.SourceDate ) AS BankRows,
                 mst.NAME
          INTO   #TempBookedBalance
          FROM   tblBookedBankBalance bb
                 JOIN tblBRS brs
                   ON bb.BRSID = brs.ID
                 JOIN tblBank bnk
                   ON brs.BankID = bnk.ID
                 JOIN tblMasterTypes mst
                   ON brs.SourceType = mst.ID
          WHERE  bb.IsDeleted = 0
                 AND brs.IsDeleted = 0
                 AND bnk.IsDeleted = 0
                 AND bb.DomainID = @DomainID
          ORDER  BY brs.BankID,
                    brs.SourceDate

          --UPDATE #TempBookedBalance  
          --SET    ClosingBalance = SourceAmount  
          --WHERE  BankRows = 1  
          UPDATE bb
          SET    ClosingBalance = ( CASE
                                      WHEN bb.BankRows <> 1 THEN (SELECT Isnull(ClosingBalance, 0)
                                                                  FROM   #TempBookedBalance
                                                                  WHERE  BankRows = bb.BankRows - 1
                                                                         AND BankID = bb.BankID)
                                                                 + Isnull(SourceAmount, 0)
                                      ELSE Isnull(OpeningBalance, 0)
                                           + Isnull(SourceAmount, 0)
                                    END )
          FROM   #TempBookedBalance bb

          UPDATE bb
          SET    bb.ClosingBalance = tbb.ClosingBalance
          FROM   tblBookedBankBalance bb
                 JOIN #TempBookedBalance tbb
                   ON tbb.ID = bb.ID
                      AND bb.DomainID = @DomainID

          SET @Output = (SELECT [Message]
                         FROM   tblErrorMessage
                         WHERE  [Type] = 'Information'
                                AND Code = 'RCD_UPD'
                                AND IsDeleted = 0) --'Updated Successfully.'  
          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 




