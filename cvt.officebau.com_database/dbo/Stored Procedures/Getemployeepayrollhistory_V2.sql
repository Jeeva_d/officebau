﻿/****************************************************************************         
CREATED BY   : Naneeshwar.M        
CREATED DATE  : 10-JULY-2017        
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>                
 [Getemployeepayrollhistory_V2] 1,2         
 </summary>                                  
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Getemployeepayrollhistory_V2] (@DomainID   INT,      
                                                      @EmployeeID INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          DECLARE @CompanyName VARCHAR(100)= (SELECT Isnull(FullName, '')      
             FROM   tbl_company      
             WHERE  id = @DomainID)      
      
          SELECT id            AS id,      
                 [Description] AS Description      
          INTO   #tmpPayrollCompontents      
          FROM   tbl_Pay_PayrollCompontents      
          WHERE  isdeleted = 0      
                 AND DomainID = @DomainID      
          ORDER  BY Ordinal ASC      
      
          SELECT pd.Amount,      
                 pc.Description                  AS Description,      
                 @CompanyName                    AS [CompanyName],      
                 Isnull(BU.Address, '')          AS [BusinessUnitAddress],      
                 EMP.DOJ                         AS [DOJ],      
                 CONVERT(VARCHAR(50), COM.Logo)  AS LOGO,      
                 CONVERT(VARCHAR(50), mon.Code) + ' - '      
                 + CONVERT(VARCHAR(50), YearID ) AS MonthYear  ,YearID,MonthID    
          INTO   #tempPayStub      
          FROM   #tmpPayrollCompontents pc      
                 JOIN tbl_Pay_EmployeePayrollDetails pd      
                   ON pd.ComponentId = pc.id      
                 JOIN tbl_Pay_EmployeePayroll epay      
                   ON epay.ID = pd.PayrollId      
                 JOIN tbl_EmployeeMaster emp      
                   ON emp.ID = @EmployeeID      
                 LEFT JOIN tbl_EmployeeStatutoryDetails EST      
                        ON EST.EmployeeID = EMP.ID      
                 LEFT JOIN tbl_BusinessUnit BU      
                        ON BU.ID = emp.BaseLocationID      
                 LEFT JOIN tbl_Company COM      
                        ON COM.ID = epay.DomainId      
                 LEFT JOIN tbl_FileUpload FU      
                        ON FU.Id = COM.Logo      
                 LEFT JOIN tbl_Designation DE      
                        ON DE.ID = EMP.DesignationID      
                 LEFT JOIN tbl_Department DEPART      
                        ON DEPART.ID = EMP.DepartmentID      
                 JOIN tbl_month mon      
                   ON mon.id = epay.MonthId      
          WHERE  epay.EmployeeId = @Employeeid      
                 AND epay.IsProcessed = 1      
                 AND epay.DomainID = @DomainID      
                 AND epay.Isdeleted = 0      
     Order by YearID,MonthID    
      
          DECLARE @header VARCHAR(max)=''      
      
          SET @header = Substring((SELECT ',[' + Description + ']'      
                                   FROM   #tmpPayrollCompontents      
                                   FOR xml Path('')), 2, 20000)      
          EXEC ('SELECT             
      MonthYear AS [Month Year],      
       '+@header+'      
   FROM   (SELECT *      
     FROM   #tempPayStub) AS SourceTable     
       PIVOT ( Max(Amount)      
       FOR [Description] IN ('+@header+') ) AS PivotTable Order by YearID desc,MonthID desc; ')      
      END TRY      
      BEGIN CATCH      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT;      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
