﻿/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  : 05-JUN-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
   [Searchuser] 1  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchUser] (@DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          SELECT ID                                  AS ID,  
                 Isnull(EmpCodePattern, '') + Isnull(Code, '') + ' - ' + FullName AS NAME,  
                 EmailID                             AS EmailID,  
                  Isnull(EmpCodePattern, '') + Isnull(Code, '') + ' - ' + FullName AS EmployeeName  
          FROM   tbl_EmployeeMaster  
          WHERE  IsDeleted = 0  
                 AND ISNULL(IsActive, 0) = 0  
                 AND DomainID = @DomainID  
          ORDER  BY FullName ASC  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
