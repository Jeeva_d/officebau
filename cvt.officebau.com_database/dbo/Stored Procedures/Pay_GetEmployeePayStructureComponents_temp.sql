﻿/****************************************************************************                                   
CREATED BY   :                                   
CREATED DATE  :                                   
MODIFIED BY   :                                   
MODIFIED DATE  :                                   
<summary>                                
[Pay_GetEmployeePayStructureComponents_temp] 5,0,null,0,0                              
</summary>                                                           
*****************************************************************************/    
CREATE PROCEDURE [dbo].[Pay_GetEmployeePayStructureComponents_temp] (@CompanyPayStructureId INT,    
                                                                    @Gross                 DECIMAL(18, 2),    
                                                                    @Payroll               PAYROLL ReadOnly,    
                                                                    @WorkDays              INT=0,    
                                                                    @Lop                   INT = 0)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
         BEGIN TRANSACTION;    
    
          BEGIN    
              DECLARE @Formula NVARCHAR(4000);    
              DECLARE @LopAmount DECIMAL(18, 6) =(SELECT Sum(p.amount)    
                FROM   tbl_Pay_PayrollCompanyPayStructure c    
                       JOIN tbl_Pay_PayrollCompontents com    
                         ON com.ID = c.ComponentID    
                       LEFT JOIN @Payroll p    
                              ON ISNULL(p.ComponentId, Com.ID) = com.ID    
                WHERE  c.Fixed = 1    
                       AND c.CompanyPayStructureID = @CompanyPayStructureId    
                       AND c.IsDeleted = 0)    
    
              ----------------------------------------                              
              SELECT c.ID AS Id,    
                     p.id AS PayStructureDetailsId,    
                     com.Code,    
                     com.Description,    
                     com.Ordinal,    
                     c.ComponentID,    
                     c.value,    
                     CASE    
                       WHEN ( com.Description = 'Gross' ) THEN    
                         Cast(ISNULL(p.Amount, @Gross) AS VARCHAR(MAX))    
                       ELSE    
                         CASE    
                           WHEN ( c.IsEditable = 1 ) THEN    
                             Cast(ISNULL(p.Amount, c.Value) AS VARCHAR(MAX))    
                           ELSE    
                             ISNULL(c.value, '')    
                         END    
                     END  AS Amount,    
                     0    AS HasUpdated,    
                     c.IsEditable,    
                     c.Fixed    
              INTO   #tblcompanypay    
              FROM   tbl_Pay_PayrollCompanyPayStructure c    
                     JOIN tbl_Pay_PayrollCompontents com    
                       ON com.ID = c.ComponentID    
                     LEFT JOIN @Payroll p    
                            ON ISNULL(p.ComponentId, Com.ID) = com.ID    
              WHERE  c.IsDeleted = 0    
                     AND c.CompanyPayStructureID = @CompanyPayStructureId;    
    
              --IF( @WorkDays <> 0 )            
              --  UPDATE #tblcompanypay            
              --  SET    amount = '(' + Cast(amount AS VARCHAR(Max)) + '*('            
              --                  + Cast(@LOP AS VARCHAR) + '/'            
              --                  + Cast(Cast(@Workdays AS DECIMAL(16, 2))AS VARCHAR)            
              --                  + '))'            
              --  WHERE  Fixed = 1            
              ----------------------------------------                              
              DECLARE @ID    INT,    
                      @count INT= (SELECT Count(1)    
                         FROM   #tblcompanypay);    
    
              ----------------------------------------                            
              WHILE( @count <> 0 )    
                BEGIN    
                    SET @ID = (SELECT TOP 1 ID    
                               FROM   #tblcompanypay    
                               WHERE  hasupdated = 0    
                               ORDER  BY Ordinal);    
    
                    -----------------------------------------                              
                    UPDATE #tblcompanypay    
                    SET    hasupdated = 1    
                    WHERE  ID = @ID;    
    
                    UPDATE p    
                    SET    p.amount = Replace(P.amount, (SELECT Code    
                                                         FROM   #tblcompanypay    
                                                         WHERE  ID = @ID), (SELECT amount    
                                                                            FROM   #tblcompanypay    
                                                                            WHERE  ID = @ID))    
                    FROM   #tblcompanypay P    
                    WHERE  p.Fixed = 1;    
    
                    -----------------------                              
                    SET @count = @count - 1;    
                END;    
    
              SET @count = (SELECT Count(1)    
                            FROM   #tblcompanypay);
							  
							
              WHILE( @count <> 0 )    
                BEGIN    
                    SET @ID = (SELECT TOP 1 ISNULL(ID ,(Select Top 1 ID FROM   #tblcompanypay    
                               WHERE  hasupdated = 1 and Amount Like '%CASE When%'))   
                               FROM   #tblcompanypay    
                               WHERE  hasupdated = 1    
                               and Amount NOT LIKE '%[a-zA-Z][a-zA-Z]%');    
   IF @ID is null
        Set @ID= (Select ID FROM   #tblcompanypay    
                               WHERE  hasupdated = 1 and amount like '%Case %')
                    UPDATE #tblcompanypay    
                    SET    hasupdated = 0    
                    WHERE  ID = @ID;    
              
			        SET @Formula = (SELECT amount    
                                    FROM   #tblcompanypay    
                                    WHERE  ID = @ID);    
    
                    DECLARE @input VARCHAR(MAX)= '(' + @Formula + ')';    
                    DECLARE @output        VARCHAR(MAX),    
                            @resultdecimal NVARCHAR(MAX);    
                    DECLARE @i INT= 0;    
    
    
                    WHILE @i < Len(@input)    
                      BEGIN    
                          SET @i = @i + 1;    
    
                          IF( dbo.isReallyNumeric(Substring(@input, @i, 1)) IS NOT NULL )    
                            BEGIN    
                                SET @output = ISNULL(@output, '') + Substring(@input, @i, 1);    
                            END;    
                          ELSE    
                            BEGIN    
                                SET @resultdecimal = ISNULL(@resultdecimal, '')    
                                                     + ISNULL(@output, '') + Substring(@input, @i, 1);    
                                SET @output = '';    
                            END;    
                      END;    
    
                    SET @Formula = @resultdecimal;    
                    SET @resultdecimal = '';    
    
                    DECLARE @sql NVARCHAR(MAX);    
                    DECLARE @result DECIMAL(18, 2);    
    
                    SET @sql = N'set @result = ' + @Formula;    
    
                    EXEC sp_executesql    
           @sql,    
                      N'@result DECIMAL(16,5) output',    
                      @result OUT;    
    
                    UPDATE #tblcompanypay    
                    SET    amount = CASE    
                                      WHEN ( fixed = 1    
                                             AND @WorkDays <> 0 ) THEN    
                                        @result * @lop / @WorkDays    
                                      ELSE    
                                        @result    
                                    END    
    WHERE  ID = @ID;    
    
                    UPDATE p    
                    SET    p.amount = Replace(P.amount, (SELECT Code    
                                                         FROM   #tblcompanypay    
                                                         WHERE  ID = @ID), (SELECT amount    
                  FROM   #tblcompanypay    
                                                                            WHERE  ID = @ID))    
                    FROM   #tblcompanypay P    
                    WHERE  p.Fixed = 0;    
    
                    SET @count = @count - 1;    
                END;    
    
              SET @LopAmount = @LopAmount - (SELECT Sum(ISNULL(Cast(amount AS DECIMAL(18, 6)), 0))    
                                             FROM   #tblcompanypay    
                                             WHERE  fixed = 1)    
    
              UPDATE #tblcompanypay    
              SET    amount = @LopAmount    
              WHERE  code = 'LOPAMOUNT'    
    
              SELECT ( CASE    
                         WHEN PayStructureDetailsId IS NOT NULL THEN    
                           PayStructureDetailsId    
                         ELSE    
                           0    
                       END )                          AS Id,    
                     a.Code,    
                     a.Description,    
                     a.Ordinal,    
                     a.ComponentID,    
                     a.Value,    
                    ROUND( Cast(a.Amount AS DECIMAL(16, 3)),0) AS Amount,    
                     a.IsEditable,    
                     b.Type    
              FROM   #tblcompanypay a    
                     LEFT JOIN tbl_Pay_PaystubConfiguration b    
                            ON a.ComponentID = b.ComponentID    
                               AND b.IsDeleted = 0    
              ORDER  BY --b.Type DESC,
						a.Ordinal ASC;    
    
              DROP TABLE #tblcompanypay;    
          END    
    
          COMMIT TRANSACTION;    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION;    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT;    
          SELECT @ErrorMsg = ERROR_MESSAGE(),    
                 @ErrSeverity = ERROR_SEVERITY();    
          RAISERROR(@ErrorMsg,@ErrSeverity,1);    
      END CATCH;    
  END;
