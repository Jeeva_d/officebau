﻿/****************************************************************************                           
CREATED BY   : Jeeva                          
CREATED DATE  :                           
MODIFIED BY   :                 
MODIFIED DATE  :                       
 <summary>            
  EXEC ManageProduct_v2 @ID = 22,@HasDeleted = 'false',@SessionID = 2,@DomainID = 1,@TypeID = 0,@Name = '',@UOMID = 0,@Manufacturer = '',@OpeningStock = 0,@HsnID = '0',@LowStock = 0,@IsInventory = 'False',@IsPurchase = 'False',@IsSales = 'False',@Purchase
  
Price = 0,@SalesPrice = 0,@PurchaseDescription = '',@SaleseDescription = '',@PurchaseLedgerID = 0,@SalesLedgerID = 0                   
 </summary>                                                   
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageProduct_V2] (@ID                  INT,  
                                           @TypeID              INT,  
                                           @Name                VARCHAR(100),  
                                           @UOMID               INT,  
                                           @Manufacturer        VARCHAR(100),  
                                           @OpeningStock        INT,  
                                           @HsnID               INT,  
                                           @LowStock            DECIMAL(18, 2),  
                                           @IsPurchase          BIT,  
                                           @IsSales             BIT,  
                                           @IsInventory         BIT,  
                                           @PurchasePrice       MONEY,  
                                           @SalesPrice          MONEY,  
                                           @PurchaseDescription VARCHAR(1000),  
                                           @SaleseDescription   VARCHAR(1000),  
                                           @PurchaseLedgerID    INT,  
                                           @SalesLedgerID       INT,  
                                           @HasDeleted          BIT,  
                                           @SessionID           INT,  
                                           @DomainID            INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(1000),  
              @Query  NVARCHAR(MAX),  
              @Count  INT = 0  
  
      BEGIN TRY  
          IF( @HasDeleted = 1 )  
            BEGIN  
                --IF EXISTS (SELECT 1    
                --           FROM   tblExpenseDetails    
                --           WHERE  LedgerID = @ID    
                --                  AND IsLedger = 0    
                --                  AND IsDeleted = 0    
                --                  AND DomainID = @DomainID)    
                --  BEGIN    
                --      SET @Output = (SELECT [Message]    
                --                     FROM   tblErrorMessage    
                --                     WHERE  [Type] = 'Warning'    
                --                            AND Code = 'RCD_REF'    
                --                            AND IsDeleted = 0) --'The record is referred.'                        
                --      GOTO Finish    
                --  END    
                --IF EXISTS (SELECT 1    
                --           FROM   tblBudget    
                --           WHERE  BudgetId = @ID    
                --                  AND [Type] = 'Product'    
                --                  AND IsDeleted = 0    
                --                  AND DomainID = @DomainID)    
                --  BEGIN    
                --      SET @Output = (SELECT [Message]    
                --                     FROM   tblErrorMessage    
                --                     WHERE  [Type] = 'Warning'    
                --                            AND Code = 'RCD_REF'    
                --                            AND IsDeleted = 0) --'The record is referred.'                        
                --      GOTO Finish    
                --  END    
                --IF EXISTS (SELECT 1    
                --           FROM   tblInvoiceItem    
                --           WHERE  ProductID = @ID    
                --                  AND IsDeleted = 0    
                --                  AND DomainID = @DomainID)    
                --  BEGIN    
                --      SET @Output = (SELECT [Message]    
                --                     FROM   tblErrorMessage    
                --                     WHERE  [Type] = 'Warning'    
                --                    AND Code = 'RCD_REF'    
                --                            AND IsDeleted = 0) --'The record is referred.'                        
                --      GOTO Finish    
                --  END    
                --ELSE    
                --  BEGIN    
                --      UPDATE tblProduct_v2    
                --      SET    IsDeleted = 1    
                --      WHERE  ID = @ID    
                --      SET @Output = (SELECT [Message]    
                --                     FROM   tblErrorMessage    
                --                     WHERE  [Type] = 'Information'    
                --                            AND Code = 'RCD_DEL'    
                --                            AND IsDeleted = 0) --'Deleted Successfully'                        
                --      GOTO Finish    
                --  END        
                SET @Query = N'SELECT @Count = @Count + COUNT(1)    
                           FROM   tblExpenseDetails    
                           WHERE  LedgerID = '  
                             + Cast(@ID AS VARCHAR)  
                             + '    
                                  AND IsLedger = 0    
                                  AND IsDeleted = 0    
                                  AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR)  
                             + ';    
        SELECT @Count = @Count + COUNT(1)    
                           FROM   tblPODetails    
                           WHERE  LedgerID = '  
                             + Cast(@ID AS VARCHAR)  
                             + '    
                                  AND IsLedger = 0    
                                  AND IsDeleted = 0    
                                  AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR)  
                             + ';    
       SELECT  @Count = @Count + COUNT(1)    
                           FROM   tblBudget    
                           WHERE  BudgetId = '  
                             + Cast(@ID AS VARCHAR)  
                             + '    
                                  AND [Type] = ''Product''    
                                  AND IsDeleted = 0    
                                  AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR)  
                             + ';    
       SELECT  @Count = @Count + COUNT(1)    
                           FROM   tblInvoiceItem    
                           WHERE  ProductID = '  
                             + Cast(@ID AS VARCHAR)  
                             + '    
                                  AND IsDeleted = 0    
                                  AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR)  
                             + ';    
        SELECT  @Count = @Count + COUNT(1)    
                           FROM   tblSOItem    
                           WHERE  ProductID = '  
                             + Cast(@ID AS VARCHAR)  
                             + '    
                                  AND IsDeleted = 0    
                                  AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR)  
                             + ';    
       SELECT  @Count = @Count + COUNT(1)    
                           FROM   tbl_InventoryStockAdjustmentItems    
                           WHERE  ProductID = '  
                             + Cast(@ID AS VARCHAR)  
                             + '    
                                  AND IsDeleted = 0    
                                  AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR)  
                             + ';  
                               
          SELECT  @Count = @Count + COUNT(1)    
                           FROM   tbl_InventorystockDistribution    
                           WHERE  ProductID = '  
                             + Cast(@ID AS VARCHAR)  
                             + '    
                                  AND IsDeleted = 0    
                                  AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR) + ';'  
  
                EXEC Sp_executesql  
                  @Query,  
                  N'@Count INT OUTPUT',  
                  @Count OUTPUT;  
  
                IF( @Count = 0 )  
                  BEGIN  
                      UPDATE tblProduct_v2  
                      SET    IsDeleted = 1,  
                             ModifiedBy = @SessionID,  
                             ModifiedOn = Getdate()  
                      WHERE  ID = @ID  
  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Information'  
                                            AND Code = 'RCD_DEL'  
                                            AND IsDeleted = 0) --'Deleted Successfully'                        
  
                      GOTO Finish  
                  END  
                ELSE  
                  BEGIN  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Warning'  
                                            AND Code = 'RCD_REF'  
                                            AND IsDeleted = 0) --'The record is referred.'                        
                      GOTO Finish  
                  END  
            END  
          ELSE  
            BEGIN  
                IF( @ID = 0 )  
                  BEGIN  
                      IF( (SELECT Count(1)  
                           FROM   tblProduct_v2  
                           WHERE  NAME = @Name  
                                  AND DomainID = @DomainID  
                                  AND IsDeleted = 0) = 0 )  
                        BEGIN  
                            INSERT INTO tblProduct_v2  
                                        (NAME,  
                                         TypeID,  
                                         UOMId,  
                                         MFG,  
                                         OpeningStock,  
                                         HSNId,  
                                         LowStockThresold,  
                                         IsInventroy,  
                                         IsPurchase,  
                                         PurchasePrice,  
                                         PurchaseLedgerId,  
                                         PurchaseDescription,  
                                         IsSales,  
                                         SalesPrice,  
                                         SalesLedgerId,  
                                         SalesDescription,  
                                         CreatedBy,  
                                         CreatedOn,  
                                         ModifiedBy,  
                                         ModifiedOn,  
                                         DomainId)  
                            SELECT @Name,  
                                   @TypeID,  
                                   @UOMID,  
                                   @Manufacturer,  
                                   @OpeningStock,  
                                   @HsnID,  
                                   @LowStock,  
                                   @IsInventory,  
                                   @IsPurchase,  
                                   @PurchasePrice,  
                                   @PurchaseLedgerID,  
                                   @PurchaseDescription,  
                                   @IsSales,  
                                   @SalesPrice,  
                                   @SalesLedgerID,  
                                   @SaleseDescription,  
                                   @SessionID,  
                                   Getdate(),  
                                   @SessionID,  
                                   Getdate(),  
                                   @DomainID  
  
                            DECLARE @RefID INT=@@IDENTITY  
  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Information'  
                                                  AND Code = 'RCD_INS'  
                                                  AND IsDeleted = 0)  
                                          + '/' + Cast(@RefID AS VARCHAR(100))  
                        --'Inserted Successfully/'                        
                        END  
                      ELSE  
                        SET @Output = (SELECT [Message]  
                                       FROM   tblErrorMessage  
                                       WHERE  [Type] = 'Warning'  
                                              AND Code = 'RCD_EXIST'  
                                              AND IsDeleted = 0) --'Already Exists'                   
                  END  
                ELSE  
                  BEGIN  
                      IF( ( (SELECT TypeID  
                             FROM   tblProduct_V2  
                             WHERE  id = @ID) <> @TypeID )  
                          AND ( (SELECT Count(1)  
                                 FROM   tblProduct_V2 P  
                                        LEFT JOIN tblInvoiceItem II  
                                               ON ii.ProductID = p.ID  
                                                  AND ii.IsDeleted = 0  
                                        LEFT JOIN tblExpenseDetails ed  
                                               ON ed.LedgerID = p.ID  
                                                  AND ed.IsLedger = 0  
                                                  AND ed.IsDeleted = 0  
                                 WHERE  ii.ProductID = @ID  
                                         OR ed.LedgerID = @ID) > 0 ) )  
                        BEGIN  
                            IF (SELECT Count(1)  
                                FROM   tblInvoiceItem  
                                WHERE  ProductID = @id  
                                       AND DomainID = @DomainID  
                                       AND IsDeleted = 0) <> 0  
                              BEGIN  
                                  SET @Output = 'The Service Type is referred in Invoice.'  
  
                                  GOTO Finish  
                              END  
                            ELSE  
                              BEGIN  
                                  SET @Output = 'The Product Type is referred in Expense.'  
  
                                  GOTO Finish  
                              END  
                        END  
                      ELSE  
                        BEGIN  
                            IF( (SELECT Count(1)  
                                 FROM   tblProduct_V2  
                                 WHERE  NAME = @Name  
                                        AND ID <> @ID  
                                        AND DomainID = @DomainID  
                                        AND IsDeleted = 0) = 0 )  
                              BEGIN  
                                  BEGIN  
                                      BEGIN  
                            UPDATE tblProduct_V2  
                                          SET    NAME = @Name,  
                                                 TypeID = @TypeID,  
                                                 UOMId = @UOMID,  
                                                 MFG = @Manufacturer,  
                                                 OpeningStock = @OpeningStock,  
                                                 HSNId = @HsnID,  
                                                 LowStockThresold = @LowStock,  
                                                 IsInventroy = @IsInventory,  
                                                 IsPurchase = @IsPurchase,  
                                                 PurchasePrice = @PurchasePrice,  
                                                 PurchaseLedgerId = @PurchaseLedgerID,  
                                                 PurchaseDescription = @PurchaseDescription,  
                                                 IsSales = @IsSales,  
                                                 SalesPrice = @SalesPrice,  
                                                 SalesLedgerId = @SalesLedgerID,  
                                                 SalesDescription = @SaleseDescription,  
                                                 ModifiedBy = @SessionID,  
                                                 ModifiedOn = Getdate()  
                                          WHERE  ID = @ID  
                                                 AND DomainID = @DomainID  
  
                                          SET @Output = (SELECT [Message]  
                                                         FROM   tblErrorMessage  
                                                         WHERE  [Type] = 'Information'  
                                                                AND Code = 'RCD_UPD'  
                                                                AND IsDeleted = 0) --'Updated Successfully'                   
                                      END  
                                  END  
                              END  
                            ELSE  
                              BEGIN  
                                  SET @Output = (SELECT [Message]  
                                                 FROM   tblErrorMessage  
                                                 WHERE  [Type] = 'Warning'  
                                                        AND Code = 'RCD_EXIST'  
                                                        AND IsDeleted = 0) --'Already Exists'                        
                              END  
                        END  
                  END  
            END  
  
          FINISH:  
  
          SELECT @Output  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
