﻿/****************************************************************************         
CREATED BY   :         
CREATED DATE  :      
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>      
 [Searchinvoiceitemlist]  4,1,1      
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Searchinvoiceitemlist] (@InvoiceID INT,      
                                                @DomainID  INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT it.ID              AS ID,      
                 it.ProductID       AS ProductID,      
                 it.ItemDescription AS [Description],      
                 it.Qty             AS Qty,      
                 it.Rate            AS Rate,      
     it.SGST            AS SGST,      
     it.SGSTAMOUNT,      
     it.CGST,      
     it.CGSTAMOUNT,      
     p.Name AS ProductName,      
     it.IsProduct,      
     g.Code  AS HsnCode ,
	 fu.OriginalFileName  as HistoryID   
          FROM   tblInvoiceItem it      
   LEFT JOin tblProduct_v2 p on it.ProductID=p.ID      
   Left Join tblGstHSN g on p.HSNID =g.ID 
   left join tbl_FileUpload Fu on Fu.Id=it.HistoryID   
          WHERE  it.IsDeleted = 0      
                 AND @InvoiceID = it.InvoiceID And it.DomainID=@DomainID      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
