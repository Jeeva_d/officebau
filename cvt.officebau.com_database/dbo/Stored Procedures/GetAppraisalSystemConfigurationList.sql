﻿/**************************************************************************** 
CREATED BY			:	Ajith N
CREATED DATE		:	05 Oct 2018
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	[GetAppraisalSystemConfigurationList] 1,4,1
 </summary>                          
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetAppraisalSystemConfigurationList] (@BusinessUnitID INT,
                                                             @DomainID       INT,
                                                             @UserID         INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT ac.ID                 AS ID,
                 ac.AppraisalName      AS [Name],
                 ac.BUID               AS [BusinessUnitID],
                 bu.NAME               AS [BusinessUnit],
                 ac.AppraiseeStartDate AS [AppraiseeStartDate],
                 ac.AppraiseeEndDate   AS [AppraiseeEndDate],
                 ac.AppraiserStartDate AS [AppraiserStartDate],
                 ac.AppraiserEndDate   AS [AppraiserEndDate],
                 ac.EligibilityDate    AS [EligibilityDate]
          FROM   tbl_AppraisalConfiguration ac
                 LEFT JOIN tbl_BusinessUnit bu
                        ON bu.ID = ac.BUID
          WHERE  ac.IsDeleted = 0
                 AND ac.DomainID = @DomainID
                 AND ( @BusinessUnitID = 0
                        OR ac.BUID = @BusinessUnitID )
          ORDER  BY ac.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
