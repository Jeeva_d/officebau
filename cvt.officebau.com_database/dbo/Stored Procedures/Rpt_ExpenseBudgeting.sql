﻿/****************************************************************************                                                
CREATED BY    :                                              
CREATED DATE  :                                              
MODIFIED BY   :                                           
MODIFIED DATE :                                         
 [Rpt_ExpenseBudgeting] 4,1,'rec'                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_ExpenseBudgeting] (@FinancialYearID INT = 4,
                                              @DomainID        INT = 0,
                                              @Type            VARCHAR(100))
AS
  BEGIN
      BEGIN try
          DECLARE @FYIDvalue INT = (SELECT financialyear
                     FROM   tbl_FinancialYear
                     WHERE  id = @FinancialYearID),
                  @Month     INT = (SELECT value
                     FROM   tblapplicationconfiguration
                     WHERE  code = 'STARTMTH'
                            AND domainid = @DomainID)
          DECLARE @StartDate DATETIME = Dateadd(month, @Month - 1, Dateadd(year, @FYIDvalue - 1900, 0)),
                  @EndDate   DATETIME = Dateadd(month, CASE
                                     WHEN ( @Month = 1 ) THEN
                                       12 - 1
                                     ELSE
                                       @Month - 1 - 1
                                   END, Dateadd(year, @FYIDvalue + 1 - 1900, 0))

          SELECT v.NAME                                                                                                               AS NAME,
                 ( ( e.Amount * Qty ) + ( CGST + SGST ) ) / ( 100 / ( ( epm.Amount / (SELECT Sum(Amount*Qty) + Sum(CGST+SGST)
                                                                                      FROM   tblExpenseDetails
                                                                                      WHERE  IsDeleted = 0
                                                                                             AND ExpenseID = e.ExpenseID) ) * 100 ) ) TotalAmount,
                 Datename(month, e1.date)                                                                                             MonthNames,
                 b.amount,
                 'Payment'                                                                                                            AS Typed,
                 'Ledger'                                                                                                             AS btype
          INTO   #expense
          FROM   tblbudget b
                 LEFT JOIN tblexpensedetails e
                        ON b.budgetid = e.LedgerID
                           AND Isledger = 1
                           AND b.BudgetType = 'OutFlow'
                           AND e.isdeleted = 0
                 LEFT JOIN tblExpense e1
                        ON e.ExpenseID = e1.ID
                 LEFT JOIN tblexpensepaymentmapping epm
                        ON epm.expenseid = e.ExpenseID
                           AND epm.isdeleted = 0
                 JOIN tblexpensepayment ep
                   ON epm.expensepaymentid = ep.id
                      AND b.fyid = @FinancialYearID
                      AND e1.date > @StartDate
                      AND e1.date < @EndDate
                 LEFT JOIN tblledger v
                        ON b.budgetid = v.id
                           AND b.BudgetType = 'OutFlow'
          WHERE  b.BudgetType = 'OutFlow'
                 AND b.isdeleted = 0
                 AND b.domainid = @DomainID

          INSERT INTO #expense
          SELECT v.NAME                  AS NAME,
                 Sum(ed.amount*QTY) + Isnull(Sum(CGST), 0)
                 + Isnull(Sum(SGST), 0)  TotalAmount,
                 Datename(month, e.date) MonthNames,
                 b.amount,
                 'Booked',
                 'Ledger'                AS btype
          FROM   tblbudget b
                 LEFT JOIN tblExpenseDetails ed
                        ON b.BudgetId = ed.LedgerID
                           AND ed.IsDeleted = 0
                           AND Isledger = 1
                           AND ed.LedgerID = b.BudgetId
                 LEFT JOIN tblexpense e
                        ON ed.ExpenseID = e.ID
                           AND b.BudgetType = 'OutFlow'
                           AND b.fyid = @FinancialYearID
                           AND e.date > @StartDate
                           AND e.date < @EndDate
                           AND e.isdeleted = 0
                 LEFT JOIN tblledger v
                        ON b.budgetid = v.id
                           AND b.BudgetType = 'OutFlow'
          WHERE  b.BudgetType = 'OutFlow'
                 AND b.isdeleted = 0
                 AND b.domainid = @DomainID
          GROUP  BY v.NAME,
                    Datename(month, e.date),
                    b.amount
          UNION ALL
          SELECT l.NAME                  AS NAME,
                 Sum(ed.amount*QTY) + Isnull(Sum(CGST), 0)
                 + Isnull(Sum(SGST), 0)  TotalAmount,
                 Datename(month, e.date) MonthNames,
                 b.amount,
                 'Booked',
                 'Ledger'                AS btype
          FROM   tblbudget b
                 LEFT JOIN tblProduct_v2 v
                        ON b.budgetid = v.PurchaseLedgerId
                           AND b.BudgetType = 'OutFlow'
                 JOIN tblledger l
                   ON l.ID = v.PurchaseLedgerId
                 LEFT JOIN tblExpenseDetails ed
                        ON v.Id = ed.LedgerID
                           AND ed.IsDeleted = 0
                           AND Isledger = 0
                           AND ed.LedgerID = b.BudgetId
                 LEFT JOIN tblexpense e
                        ON ed.ExpenseID = e.ID
                           AND b.BudgetType = 'OutFlow'
                           AND b.fyid = @FinancialYearID
                           AND e.date > @StartDate
                           AND e.date < @EndDate
                           AND e.isdeleted = 0
          WHERE  b.BudgetType = 'OutFlow'
                 AND b.isdeleted = 0
                 AND b.domainid = @DomainID
          GROUP  BY l.NAME,
                    Datename(month, e.date),
                    b.amount
          UNION ALL
          SELECT l.NAME                                                                                                               AS NAME,
                 ( ( e.Amount * Qty ) + ( CGST + SGST ) ) / ( 100 / ( ( epm.Amount / (SELECT Sum(Amount*Qty) + Sum(CGST+SGST)
                                                                                      FROM   tblExpenseDetails
                                                                                      WHERE  IsDeleted = 0
                                                                                             AND ExpenseID = e.ExpenseID) ) * 100 ) ) TotalAmount,
                 Datename(month, e1.date)                                                                                             MonthNames,
                 b.amount,
                 'Payment'                                                                                                            AS Typed,
                 'Ledger'                                                                                                             AS btype
          FROM   tblbudget b
                 LEFT JOIN tblProduct_v2 v
                        ON b.budgetid = v.PurchaseLedgerId
                           AND b.BudgetType = 'OutFlow'
                 JOIN tblledger l
                   ON l.Id = v.PurchaseLedgerId
                 LEFT JOIN tblexpensedetails e
                        ON v.ID = e.LedgerID
                           AND Isledger = 0
                           AND b.BudgetType = 'OutFlow'
                           AND e.isdeleted = 0
                 LEFT JOIN tblExpense e1
                        ON e.ExpenseID = e1.ID
                 LEFT JOIN tblexpensepaymentmapping epm
                        ON epm.expenseid = e.ExpenseID
                           AND epm.isdeleted = 0
                 JOIN tblexpensepayment ep
                   ON epm.expensepaymentid = ep.id
                      AND b.fyid = @FinancialYearID
                      AND e1.date > @StartDate
                      AND e1.date < @EndDate
          WHERE  b.BudgetType = 'OutFlow'
                 AND b.isdeleted = 0
                 AND b.domainid = @DomainID

          --INSERT INTO #expense
          --SELECT v.NAME                  AS NAME,
          --       Sum(e.amount)           TotalAmount,
          --       Datename(month, e.date) MonthNames,
          --       b.amount,
          --       @Type                   AS Typed,
          --       'Ledger'                AS btype
          --FROM   tblbudget b
          --       LEFT JOIN tblReceipts e
          --              ON b.budgetid = e.LedgerID
          --                 AND b.BudgetType = 'OutFlow'
          --                 AND e.isdeleted = 0
          --                 AND e.TransactionType = (SELECT Id
          --                                          FROM   tblMasterTypes
          --                                          WHERE  NAME = 'Make Payment')
          --                 AND e.date > @StartDate
          --                 AND e.date < @EndDate
          --       LEFT JOIN tblledger v
          --              ON b.budgetid = v.id
          --                 AND b.BudgetType = 'OutFlow'
          --WHERE  b.BudgetType = 'OutFlow'
          --       AND b.isdeleted = 0
          --       AND b.domainid = @DomainID
          --GROUP  BY v.NAME,
          --          Datename(month, e.date),
          --          b.amount

		  INSERT INTO #expense
          SELECT v.NAME                  AS NAME,
                 Sum(e.TotalAmount)           TotalAmount,
                 Datename(month, e.paymentdate) MonthNames,
                 b.amount,
                 @Type                   AS Typed,
                 'Ledger'                AS btype
          FROM   tblbudget b
                 LEFT JOIN tbl_payment e
                        ON b.budgetid = e.LedgerID
                           AND b.BudgetType = 'OutFlow'
                           AND e.isdeleted = 0                          
                           AND e.paymentdate > @StartDate
                           AND e.paymentdate < @EndDate
                 LEFT JOIN tblledger v
                        ON b.budgetid = v.id
                           AND b.BudgetType = 'OutFlow'
          WHERE  b.BudgetType = 'OutFlow'
                 AND b.isdeleted = 0
                 AND b.domainid = @DomainID
          GROUP  BY v.NAME,
                    Datename(month, e.paymentdate),
                    b.amount

          IF( @Type = 'Payment' )
            DELETE #expense
            WHERE  Typed <> 'Payment'
          ELSE
            DELETE #expense
            WHERE  Typed = 'Payment'

          SELECT Datename(month, '2012-' + Cast(number AS VARCHAR(2)) + '-1') monthname,
                 amount,
                 CASE
                   WHEN ( b.type = 'Product' ) THEN
                     v.NAME
                   ELSE
                     l.NAME
                 END                                                          AS NAME
          INTO   #month
          FROM   master..spt_values s
                 LEFT JOIN tblbudget b
                        ON b.id = b.id
                           AND b.isdeleted = 0
                           AND b.fyid = @FinancialYearID
                           AND b.domainid = @DomainID
                           AND b.BudgetType = 'OutFlow'
                 LEFT JOIN tblProduct_v2 v
                        ON b.budgetid = v.id
                           AND b.BudgetType = 'OutFlow'
                 LEFT JOIN tblLedger l
                        ON b.budgetid = l.id
                           AND b.BudgetType = 'OutFlow'
          WHERE  s.type = 'P'
                 AND number BETWEEN 1 AND 12
          ORDER  BY number

          CREATE TABLE #months
            (
               rowid     INT IDENTITY (1, 1) NOT NULL,
               monthname VARCHAR(50) NOT NULL
            )

          SELECT m.monthname,
                 m.NAME                   NAME,
                 Isnull(e.totalamount, 0) Amount,
                 m.amount                 pro
          INTO   #pivottable
          FROM   #month m
                 LEFT JOIN #expense e
                        ON m.monthname = e.monthnames
                           AND m.NAME = e.NAME

          INSERT INTO #months
                      (monthname)
          SELECT Datename(month, '2012-' + Cast(number AS VARCHAR(2)) + '-1') monthname
          FROM   master..spt_values s
          WHERE  s.type = 'P'
                 AND number BETWEEN 4 AND 12
          UNION ALL
          SELECT Datename(month, '2012-' + Cast(number AS VARCHAR(2)) + '-1') monthname
          FROM   master..spt_values s
          WHERE  s.type = 'P'
                 AND number BETWEEN 1 AND 3

          DECLARE @cols  AS NVARCHAR(max),
                  @cols2 AS NVARCHAR(max),
                  @query AS NVARCHAR(max);

          SET @cols = Stuff((SELECT ',' + monthname
                             FROM   #months
                             FOR xml path (''), type) .value('.', 'NVARCHAR(MAX)'), 1, 1, '')
          SET @cols2 = Stuff((SELECT '+' + monthname
                              FROM   #months
                              FOR xml path (''), type) .value('.', 'NVARCHAR(MAX)' ), 1, 1, '')
          SET @query = 'SELECT Name AS Particulars, pro AS Planned ,('
                       + @cols2 + ') As Actual, CASE WHEN((' + @cols2
                       + ')=0) then 0 Else ((' + @cols2
                       + ')*100)/pro END AS [Percent]  ,' + @cols
                       + ' from              (                 select Name                     , amount                     , monthname ,pro                                      
                       from #PivotTable    Where Name IS Not Null        ) x             pivot   (                sum(amount)                                                 
               for monthname in (' + @cols
                       + ')             ) p Order by pro desc'

          PRINT @query

          EXECUTE (@query)

          DROP TABLE #month, #expense, #pivottable, #months
      END try
      BEGIN catch
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END catch
  END
