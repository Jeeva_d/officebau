﻿/****************************************************************************   
CREATED BY		: Naneeshwar.M
CREATED DATE	: 30-NOV-2017
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
 [Getcfsentry]1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetCFSEntry] (@DomainID INT,
                                     @ID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT ID            AS ID,
                 ContainerName AS ContainerName,
                 CASE
                   WHEN( InwardTypeID = 1 ) THEN'Import'
                   ELSE 'Export'
                 END           AS InwardTypeName,
                 InwardTypeID  AS InwardTypeID,
                 InwardDate    AS InwardDate
          FROM   tbl_CFSEntry
          WHERE  DomainID = @DomainID
                 AND ID = @ID
                 AND IsDeleted = 0
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
