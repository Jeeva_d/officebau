﻿/****************************************************************************   
CREATED BY   : Naneeshwar  
CREATED DATE  :   17-Nov-2016
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [Getbillpay] null,1,1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getbillpay] (@VendorID INT,
                                    @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT ex.ID            AS ID,
                 ep.Date          AS [Date],
                 ex.VendorID      AS VendorID,
                 vd.NAME          AS VendorName,
                 vd.PaymentTerms  AS PaymentTerms,
                 ep.Reference     AS Reference,
                 ex.PaidAmount    AS TotalAmount,
                 ep.PaymentModeID AS PaymentModeID,
                 cd.code          AS PaymentMode,
                 ep.BankID        AS BankID,
                 bk.NAME          AS BankName,
                 ex.ModifiedOn    AS ModifiedOn,
                 EMP.FullName    AS ModifiedBy
          FROM   tblExpense ex
                 LEFT JOIN tblVendor vd
                        ON vd.ID = ex.VendorID
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = ex.ModifiedBy
                 LEFT JOIN tblExpensePaymentMapping expm
                        ON expm.ExpenseID = ex.ID
                 LEFT JOIN tblExpensePayment ep
                        ON ep.ID = expm.ExpensePaymentID
                 LEFT JOIN tbl_CodeMaster cd
                        ON cd.ID = ep.PaymentModeID
                 LEFT JOIN tblBank bk
                        ON bk.ID = ep.BankID
          WHERE  ex.IsDeleted = 0
                 AND ( ex.VendorID = @VendorID
                        OR @VendorID IS NULL ) AND ex.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
