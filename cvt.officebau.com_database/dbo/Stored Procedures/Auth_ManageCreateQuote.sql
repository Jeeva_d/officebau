﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
		Create new Quote account in application
		[Auth_ManageCreateQuote] 'dfd','sdsd'
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_ManageCreateQuote]
 (
	@Email			VARCHAR(100)		,
	@AppPassword	VARCHAR(10)		
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY		
		
	SELECT 'demo' AS Domain

	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
