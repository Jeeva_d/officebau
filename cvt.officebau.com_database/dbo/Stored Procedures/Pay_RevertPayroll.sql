﻿/****************************************************************************   
CREATED BY      : Priya K  
CREATED DATE  : 08-Nov-2018  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>    
 [Revertpayroll] ' 224 - Victor  Santhosaraj p, 1001 - Ajith, 225 - Naneeshwar, ','', 6, 5,1  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_RevertPayroll] (@EmployeeName VARCHAR(500),  
                                       @Location     VARCHAR(500),  
                                       @MonthId      INT,  
                                       @Year         INT,  
                                       @DomainID     INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @YEARID  INT = @Year,  
              @TDSYear INT  
  
      IF( @MonthID BETWEEN 1 AND 3 )  
        SET @TDSYear = (SELECT (CAST(NAME AS INT) - 1)  
                      FROM   tbl_FinancialYear  
                      WHERE  ID = @Year   
                             AND DomainID = @DomainID
							 and IsDeleted = 0)  
      ELSE  
        SET @TDSYear =(SELECT NAME  
                       FROM   tbl_FinancialYear  
                       WHERE  ID = @Year  
                              AND DomainID = @DomainID
							  and IsDeleted = 0)  
   
      DECLARE @DataSource TABLE  
        (  
           [Value] NVARCHAR(128)  
        )  
  
      INSERT INTO @DataSource  
                  ([Value])  
      SELECT Item  
      FROM   dbo.Splitstring (@Location, ',')  
      WHERE  Isnull(Item, '') <> ''  
  
      DECLARE @EmployeeNameSplit TABLE  
        (  
           [Value] NVARCHAR(128)  
        )  
  
      INSERT INTO @EmployeeNameSplit  
                  ([Value])  
      SELECT Item  
      FROM   dbo.Splitstring (@EmployeeName, ',')  
      WHERE  Isnull(Item, '') <> ''  
  
      SELECT ID  
      INTO   #tmpEmployeeList  
      FROM   tbl_employeemaster  
      WHERE  code IN(SELECT dbo.Getnumeric(Value) AS ID  
                     FROM   @EmployeeNameSplit)  
             AND DomainID = @DomainID  
  
      SET @Year = (SELECT NAME  
                   FROM   tbl_FinancialYear  
                   WHERE  id = @Year and @DomainID=DomainID and IsDeleted = 0)  
  
      DECLARE @OutPut VARCHAR(100) =''  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          BEGIN  
              IF Isnull((SELECT Count(1)  
                         FROM   #tmpEmployeeList), 0) <> 0  
                BEGIN  
                    IF (SELECT Count(1)  
                        FROM   tbl_Pay_EmployeePayroll  
                        WHERE  IsDeleted = 0  
                               AND MonthId = @MonthId  
                               AND YearId = @Year  
                               AND EmployeeId IN (SELECT *  
                                                  FROM   #tmpEmployeeList)  
                               AND IsProcessed = 0  
                               AND DomainID = @DomainID) <> 0  
                      BEGIN  
                          UPDATE tbl_Pay_EmployeePayroll  
                          SET    IsDeleted = 1  
                          WHERE  EmployeeId IN (SELECT *  
                                                FROM   #tmpEmployeeList)  
                                 AND IsProcessed = 0  
                                 AND MonthId = @MonthId  
                                 AND YearId = @Year  
                                 AND DomainID = @DomainID  
  
                          UPDATE tbl_TDS  
                          SET    IsDeleted = 1  
                          WHERE  EmployeeId IN (SELECT *  
                                                FROM   #tmpEmployeeList)  
                                 AND MonthId = @MonthId  
                                 AND YearId = (SELECT ID  
                                               FROM   tbl_financialYear  
                                     WHERE  NAME = @TDSYear  
                                                      AND DomainID = @DomainID and IsDeleted = 0)  
                                 AND ISDEleted = 0  
                                AND DomainId = @DomainID  
  
                    SET @OutPut = 'Payroll is Successfully Reverted'  
                      END  
                    ELSE IF ( (SELECT Count(1)  
                          FROM   tbl_Pay_EmployeePayroll  
                          WHERE  IsDeleted = 0  
                                 AND MonthId = @MonthId  
                                 AND YearId = @Year  
                                 AND EmployeeId IN (SELECT *  
                                                    FROM   #tmpEmployeeList)  
                                 AND IsProcessed = 1) <> 0 )  
                      SET @OutPut = 'Payroll has been processed for this selected Employee. So you cannot Revert.'  
                    ELSE  
                      SET @OutPut = 'Payroll has not been processed for the selected Employee.'  
  
                    UPDATE tbl_TDS  
                    SET    IsDeleted = 1  
                    WHERE  EmployeeId IN (SELECT *  
                                          FROM   #tmpEmployeeList)  
                           AND MonthId = @MonthId  
                           AND YearId = (SELECT ID  
                                         FROM   tbl_financialYear  
                                         WHERE  NAME = @TDSYear  
                                                AND DomainID = @DomainID and IsDeleted = 0)  
                           AND ISDEleted = 0  
                           AND DomainId = @DomainID  
                END  
              ELSE IF(SELECT Count(1)  
                 FROM   @DataSource) <> 0  
                BEGIN  
                    IF (SELECT Count(1)  
                        FROM   tbl_Pay_EmployeePayroll PEPR  
                               JOIN tbl_EmployeeMaster EM  
                                 ON PEPR.EmployeeId = EM.ID  
                               JOIN @DataSource DS  
                                 ON DS.Value = EM.BaseLocationID  
                        WHERE  PEPR.IsDeleted = 0  
                               AND PEPR.IsProcessed = 0  
                               AND PEPR.MonthId = @MonthId  
                               AND PEPR.YearId = @Year) <> 0  
                      BEGIN  
                          UPDATE PEPR  
                          SET    PEPR.ISDELETED = 1  
                          FROM   tbl_Pay_EmployeePayroll PEPR  
                                 JOIN tbl_EmployeeMaster EM  
                                   ON PEPR.EmployeeId = EM.ID  
                                 JOIN @DataSource DS  
                                   ON DS.Value = EM.BaseLocationID  
                          WHERE  MONTHID = @MonthId  
                                 AND YEARID = @Year  
                                 AND PEPR.IsDeleted = 0  
                                 AND ISPROCESSED = 0  
  
                          UPDATE tbl_TDS  
                          SET    IsDeleted = 1  
                          WHERE  BaseLocationID IN (SELECT *  
                                                    FROM   @DataSource)  
                                 AND MonthId = @MonthId  
                                 AND YearId = (SELECT ID  
                                               FROM   tbl_financialYear  
                                               WHERE  NAME = @TDSYear  
                                                      AND DomainID = @DomainID and IsDeleted = 0)  
                                 AND ISDEleted = 0  
                                 AND DomainId = @DomainID  
  
                          SET @OutPut = 'Payroll is Successfully Reverted'  
                      END  
                    ELSE IF (SELECT Count(1)  
          FROM   tbl_Pay_EmployeePayroll PEPR  
                               JOIN tbl_EmployeeMaster EM  
                                 ON PEPR.EmployeeId = EM.ID  
                               JOIN @DataSource DS  
                                 ON DS.Value = EM.BaseLocationID  
                        WHERE  PEPR.IsDeleted = 0  
                        AND PEPR.IsProcessed = 1  
    AND PEPR.MonthId = @MonthId  
                               AND PEPR.YearId = @Year) <> 0  
                      SET @OutPut = 'Payroll has been processed for the selected Business Units. So you cannot Revert.'  
                    ELSE  
                      BEGIN  
                          SET @OutPut = 'Payroll has not been processed for this selected Business Unit.'  
                      END  
  
                    UPDATE tbl_TDS  
                    SET    IsDeleted = 1  
                    WHERE  BaseLocationID IN (SELECT *  
                                              FROM   @DataSource)  
                           AND MonthId = @MonthId  
                           AND YearId = (SELECT ID  
                                         FROM   tbl_financialYear  
                                         WHERE  NAME = @TDSYear  
                                                AND DomainID = @DomainID and IsDeleted = 0)  
                           AND ISDEleted = 0  
                           AND DomainId = @DomainID  
                END  
              ELSE  
                BEGIN  
                    IF EXISTS (SELECT 1  
                               FROM   tbl_Pay_EmployeePayroll  
                               WHERE  MonthId = @MonthId  
                                      AND IsProcessed = 0  
                                      AND YearId = @Year  
                                      AND IsDeleted = 0)  
                      BEGIN  
                          UPDATE tbl_Pay_EmployeePayroll  
                          SET    IsDeleted = 1  
                          WHERE  MonthId = @MonthId  
                                 AND IsProcessed = 0  
                                 AND YearId = @Year  
                                 AND IsDeleted = 0  
  
                          UPDATE tbl_TDS  
                          SET    IsDeleted = 1  
                          WHERE  MonthId = @MonthId  
                                 AND YearId = (SELECT ID  
                                               FROM   tbl_financialYear  
                                               WHERE  NAME = @TDSYear  
                                                      AND DomainID = @DomainID and IsDeleted = 0)  
                                 AND ISDEleted = 0  
                                 AND DomainId = @DomainID  
  
                          SET @OutPut = 'Payroll is Successfully Reverted'  
                      END  
                    ELSE IF EXISTS (SELECT 1  
                               FROM   tbl_Pay_EmployeePayroll  
                               WHERE  MonthId = @MonthId  
                                      AND IsProcessed = 1  
                                      AND YearId = @Year  
                                      AND IsDeleted = 0)  
                      BEGIN  
                          SET @OutPut = 'Payroll has been processed for the selected Month. So you cannot Revert.'  
                      END  
                    ELSE  
                      BEGIN  
                          SET @OutPut = 'Payroll has not been processed for this selected Month.'  
                      END  
  
                    UPDATE tbl_TDS  
                    SET    IsDeleted = 1  
                    WHERE  MonthId = @MonthId  
                           AND YearId = (SELECT ID  
                                         FROM   tbl_financialYear  
                                         WHERE  NAME = @TDSYear  
                                                AND DomainID = @DomainID and IsDeleted = 0)  
                           AND ISDEleted = 0  
                           AND DomainId = @DomainID  
                END  
          END  
  
          SELECT @OutPut  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
