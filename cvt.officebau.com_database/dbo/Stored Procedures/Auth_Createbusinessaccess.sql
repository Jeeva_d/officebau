﻿/****************************************************************************   
CREATED BY   :	Ajith N
CREATED DATE  : 14 Feb 2018
MODIFIED BY   :	 
MODIFIED DATE  :  
<summary> 
	Auth_Createbusinessaccess 'SKYBase','SKYBase','SAP@test.com','eWK9XoF32iP4UxA86wE+9LRLWKCN4NItMXvaPr8LR/Q=' , '96',''
</summary>                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[Auth_Createbusinessaccess] (@CompanyName     VARCHAR(500),
                                                   @BusinessUnit    VARCHAR(200),
                                                   @EmailID         VARCHAR(200),
                                                   @DefaultPassword VARCHAR(100),
                                                   @ContactNo       VARCHAR(100),
                                                   @Logo            VARCHAR(500),
                                                   @Modules         VARCHAR(100))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          -- BEGIN TRANSACTION DML
          DECLARE @DomainID       INT,
                  @RegionID       INT,
                  @BusinessUnitID INT,
                  @EmployeeID     INT,
                  @IsDeleted      BIT = 0,
                  @IsActive       BIT = 0,
                  @CurrentDate    DATETIME = Getdate(),
                  @UserID         INT = 1,
                  @Code           INT = 1000,
                  @Output         VARCHAR(100) = 'Operation Failed!'
          DECLARE @LoginCode VARCHAR(100);

          SET @LoginCode = @EmailID

          --IF NOT EXISTS(SELECT 1
          --              FROM   tbl_Company
          --              WHERE  NAME = @CompanyName)
          --  BEGIN
          --      IF NOT EXISTS(SELECT 1
          --                    FROM   tbl_EmployeeMaster
          --                    WHERE  LoginCode = @LoginCode)
          BEGIN
              -- Insert Company Details
              INSERT INTO tbl_Company
                          (NAME,
                           ContactNo,
                           EmailID,
                           Modules,
                           IsDeleted,
                           IsActive,
                           CreatedOn,
                           CreatedBy,
                           ModifiedOn,
                           ModifiedBy,
                           HistoryID,
                           FullName,
                           Logo)
              VALUES      (@CompanyName,
                           @ContactNo,
                           @EmailID,
                           @Modules,
                           @IsDeleted,
                           @IsActive,
                           @CurrentDate,
                           @UserID,
                           @CurrentDate,
                           @UserID,
                           Newid(),
                           @CompanyName,
                           @Logo)

              SET @DomainID = Ident_current('tbl_Company')

              -- Insert Business Region
              INSERT INTO tbl_BusinessUnit
                          (NAME,
                           Remarks,
                           IsDeleted,
                           CreatedOn,
                           CreatedBy,
                           ModifiedOn,
                           ModifiedBy,
                           DomainID,
                           HistoryID,
                           ParentID,
                           Address)
              VALUES      (@BusinessUnit,
                           @BusinessUnit,
                           @IsDeleted,
                           @CurrentDate,
                           @UserID,
                           @CurrentDate,
                           @UserID,
                           @DomainID,
                           Newid(),
                           0,
                           @BusinessUnit)

              SET @RegionID = Ident_current('tbl_BusinessUnit')

              -- Insert Business Unit
              INSERT INTO tbl_BusinessUnit
                          (NAME,
                           Remarks,
                           IsDeleted,
                           CreatedOn,
                           CreatedBy,
                           ModifiedOn,
                           ModifiedBy,
                           DomainID,
                           HistoryID,
                           ParentID,
                           Address)
              VALUES      (@BusinessUnit,
                           @BusinessUnit,
                           @IsDeleted,
                           @CurrentDate,
                           @UserID,
                           @CurrentDate,
                           @UserID,
                           @DomainID,
                           Newid(),
                           @RegionID,
                           @BusinessUnit)

              SET @BusinessUnitID = Ident_current('tbl_BusinessUnit')

              -- Insert Employee Details
              INSERT INTO tbl_EmployeeMaster
                          (Code,
                           FirstName,
                           LastName,
                           FullName,
                           PrefixID,
                           DepartmentID,
                           DesignationID,
                           ContactNo,
                           GenderID,
                           EmailID,
                           GradeID,
                           JobTypeID,
                           ReportingToID,
                           EmploymentTypeID,
                           DOJ,
                           DOC,
                           ContractExpiryDate,
                           Qualification,
                           FileName,
                           IsActive,
                           IsDeleted,
                           CreatedOn,
                           CreatedBy,
                           ModifiedOn,
                           ModifiedBy,
                           DomainID,
                           HistoryID,
                           HasAccess,
                           BusinessUnitID,
                           FileID,
                           BandID,
                           FunctionalID,
                           BaseLocationID,
                           ApprovedOn,
                           ApprovedBy,
                           ReasonID,
                           InactiveFrom,
                           EmergencyContactNo,
                           ContractorId,
                           RegionID,
                           LoginCode,
                           BiometricCode)
              VALUES      (@Code,
                           'Admin',
                           '',
                           'Admin',
                           (SELECT TOP 1 ID
                            FROM   tbl_CodeMaster
                            WHERE  Type = 'Prefix'
                                   AND IsDeleted = 0),
                           0,
                           0,
                           @ContactNo,
                           NULL,
                           @EmailID,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           @CurrentDate,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           @IsActive,
                           1,--@IsDeleted,
                           @CurrentDate,
                           @UserID,
                           @CurrentDate,
                           @UserID,
                           @DomainID,
                           Newid(),
                           1,
                           ',' + Cast(@BusinessUnitID AS VARCHAR) + ',',
                           NULL,
                           NULL,
                           NULL,
                           @BusinessUnitID,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           @RegionID,
                           @LoginCode,
                           Cast(@Code AS VARCHAR(20)))

              SET @EmployeeID = Ident_current('tbl_EmployeeMaster')

              -- Insert login details
              INSERT INTO tbl_Login
                          (EmployeeID,
                           Password,
                           RecentPasswordChangeDate,
                           CreatedOn,
                           CreatedBy,
                           HistoryID,
                           DomainID,
                           IsChanged)
              VALUES      ( @EmployeeID,
                            @DefaultPassword,
                            @CurrentDate,
                            @CurrentDate,
                            @UserID,
                            Newid(),
                            @DomainID,
                            0)

              --- insert RBS Menu Access & RBS Menu Permission
              EXEC Auth_CreateMenuAccess
                @EmployeeID,
                @DomainID,
                0,
                @Modules

              --- insert Default GroupLedger & Ledger
              EXEC Managedefaultgroupandledger
                @EmployeeID,
                @DomainID

              -- insert Menus
              INSERT INTO tbl_MasterMenu
                          (NAME,
                           OrderID,
                           Remarks,
                           MenuParameters,
                           CreatedBy,
                           CreatedOn,
                           ModifiedBy,
                           ModifiedOn,
                           DomainID,
                           HistoryID,
                           RootConfig)
              SELECT NAME,
                     OrderID,
                     Remarks,
                     MenuParameters,
                     @EmployeeID,
                     @CurrentDate,
                     @EmployeeID,
                     @CurrentDate,
                     @DomainID,
                     Newid(),
                     RootConfig
              FROM   tbl_MasterMenuforDomain

              -- insert Financial Year				
              INSERT INTO tbl_FinancialYear
                          (NAME,
                           FinancialYear,
                           StartMonth,
                           EndMonth,
                           DomainID,
                           IsDeleted,
                           CreatedOn,
                           CreatedBy,
                           ModifiedOn,
                           ModifiedBy,
                           HistoryID,
                           DisplayName,
                           FYDescription)
              VALUES      (Year(Getdate()) - 1,
                           Year(Getdate()) - 1,
                           4,
                           3,
                           @DomainID,
                           0,
                           @CurrentDate,
                           @EmployeeID,
                           @CurrentDate,
                           @EmployeeID,
                           Newid(),
                           ( Cast(( Year(Getdate()) - 1 ) AS VARCHAR)
                             + '-' + Cast(Year(Getdate()) AS VARCHAR) ),
                           ( 'Apr '
                             + Cast(( Year(Getdate()) - 1 ) AS VARCHAR)
                             + ' - ' + 'Mar '
                             + Cast(Year(Getdate()) AS VARCHAR) ))

              INSERT INTO tbl_FinancialYear
                          (NAME,
                           FinancialYear,
                           StartMonth,
                           EndMonth,
                           DomainID,
                           IsDeleted,
                           CreatedOn,
                           CreatedBy,
                           ModifiedOn,
                           ModifiedBy,
                           HistoryID,
                           DisplayName,
                           FYDescription)
              VALUES      (Year(Getdate()),
                           Year(Getdate()),
                           4,
                           3,
                           @DomainID,
                           0,
                           @CurrentDate,
                           @EmployeeID,
                           @CurrentDate,
                           @EmployeeID,
                           Newid(),
                           ( Cast( Year(Getdate()) AS VARCHAR) + '-'
                             + Cast((Year(Getdate()) + 1) AS VARCHAR) ),
                           ( 'Apr ' + Cast( Year(Getdate()) AS VARCHAR)
                             + ' - ' + 'Mar '
                             + Cast((Year(Getdate()) + 1) AS VARCHAR) ))

              -- insert  Application Role
              INSERT INTO tbl_ApplicationRole
                          (NAME,
                           Remarks,
                           IsDeleted,
                           CreatedOn,
                           CreatedBy,
                           ModifiedOn,
                           ModifiedBy,
                           HistoryID,
                           DomainID)
              SELECT NAME,
                     Remarks,
                     IsDeleted,
                     @CurrentDate,
                     @EmployeeID,
                     @CurrentDate,
                     @EmployeeID,
                     Newid(),
                     @DomainID
              FROM   tbl_ApplicationRole
              WHERE  DomainID = (SELECT TOP 1 DomainID
                                 FROM   tbl_ApplicationRole
                                 ORDER  BY DomainID ASC)

              -- insert Employement Type
              INSERT INTO tbl_EmployementType
                          (NAME,
                           Description,
                           IsDeleted,
                           CreatedBy,
                           CreatedOn,
                           ModifiedBy,
                           ModifiedOn,
                           DomainID,
                           HistoryID)
              SELECT NAME,
                     Description,
                     IsDeleted,
                     @EmployeeID,
                     @CurrentDate,
                     @EmployeeID,
                     @CurrentDate,
                     @DomainID,
                     Newid()
              FROM   tbl_EmployementType
              WHERE  DomainID = (SELECT TOP 1 DomainID
                                 FROM   tbl_EmployementType
                                 ORDER  BY DomainID ASC)

              ---- Insert Default Department
              INSERT INTO tbl_Department
                          ([Name],
                           [Remarks],
                           [IsDeleted],
                           [CreatedOn],
                           [CreatedBy],
                           [ModifiedOn],
                           [ModifiedBy],
                           [HistoryID],
                           [DomainID])
              VALUES      ('Information Technology',
                           'Information Technology',
                           0,
                           Getdate(),
                           2,
                           Getdate(),
                           2,
                           Newid(),
                           @DomainID)

              ---- Insert Default Designation
              INSERT INTO tbl_Designation
                          ([Name],
                           [IsDeleted],
                           [CreatedOn],
                           [CreatedBy],
                           [ModifiedOn],
                           [ModifiedBy],
                           [HistoryID],
                           [Remarks],
                           [DomainID])
              VALUES      ('Admin',
                           0,
                           Getdate(),
                           2,
                           Getdate(),
                           2,
                           Newid(),
                           'Admin',
                           @DomainID)

              ------ Insert Default Email Configuration
              INSERT INTO tblEmailConfig
                          ([Key],
                           [Toid],
                           [BCC],
                           [CC],
                           [DomainID],
                           [CreatedBy],
                           [CreatedOn],
                           [ModifiedBy],
                           [ModifiedOn],
                           [IsAttachment])
              SELECT [Key],
                     NULL,
                     NULL,
                     NULL,
                     @DomainID,
                     2,
                     Getdate(),
                     2,
                     Getdate(),
                     [IsAttachment]
              FROM   tblEmailConfig
              WHERE  DomainID = (SELECT TOP 1 DomainID
                                 FROM   tblEmailConfig
                                 ORDER  BY DomainID ASC)

              ---- Insert Default Application Configuration
              INSERT INTO tblApplicationConfiguration
                          ([Code],
                           [Description],
                           [Value],
                           [CreatedBy],
                           [CreatedOn],
                           [ModifiedBy],
                           [ModifiedOn],
                           [IsDeleted],
                           [DomainID],
                           [HistoryID])
              SELECT [Code],
                     [Description],
                     ( CASE [Code]
                         WHEN 'STARTMTH' THEN
                           4
                         WHEN 'NRECORD' THEN
                           100
                         ELSE
                           [Value]
                       END ) AS [Value],
                     [CreatedBy],
                     Getdate(),
                     [ModifiedBy],
                     Getdate(),
                     [IsDeleted],
                     @DomainID,
                     Newid()
              FROM   tblApplicationConfiguration
              WHERE  DomainID = (SELECT TOP 1 DomainID
                                 FROM   tblApplicationConfiguration
                                 ORDER  BY DomainID ASC)

              ---- TDS
              ---- Insert Default TDSComponent								   
              INSERT INTO TDSComponent
                          (SectionID,
                           Code,
                           Description,
                           Rules,
                           CreatedBy,
                           CreatedOn,
                           ModifiedBy,
                           ModifiedOn,
                           IsDeleted,
                           DomainID,
                           CALID,
                           HistoryID,
                           Value,
                           FYID)
              SELECT SectionID,
                     Code,
                     Description,
                     Rules,
                     CreatedBy,
                     CreatedOn,
                     ModifiedBy,
                     ModifiedOn,
                     IsDeleted,
                     @DomainID,
                     CALID,
                     HistoryID,
                     Value,
                     (SELECT ID
                      FROM   tbl_FinancialYear
                      WHERE  NAME = Year(Getdate())
                             AND DomainID = @DomainID)
              FROM   TDSComponent
              WHERE  DomainID = (SELECT TOP 1 DomainID
                                 FROM   TDSComponent
                                 ORDER  BY DomainID ASC)

              ---Inventory Reason
              INSERT INTO tbl_InventoryReason
                          ([Name],
                           [Remarks],
                           [IsDeleted],
                           [DomainID])
              SELECT [Name],
                     [Remarks],
                     [IsDeleted],
                     @DomainID
              FROM   tbl_InventoryReason
              WHERE  DomainID = (SELECT TOP 1 DomainID
                                 FROM   tbl_InventoryReason
                                 ORDER  BY DomainID ASC)

              ---DashBoard Configuration Keys
              INSERT INTO tbl_DashBoardConfiguration
                          ([Key],
                           [DomainID],
                           Value)
              SELECT [Key],
                     @DomainID,
                     Value
              FROM   tbl_DashBoardConfiguration
              WHERE  DomainID = (SELECT TOP 1 DomainID
                                 FROM   tbl_DashBoardConfiguration
                                 ORDER  BY DomainID ASC)

              ---Blood Group
              INSERT INTO tbl_BloodGroup
                          (NAME,
                           Remarks,
                           IsDeleted,
                           CreatedOn,
                           CreatedBy,
                           ModifiedOn,
                           ModifiedBy,
                           DomainID,
                           HistoryID)
              SELECT NAME,
                     Remarks,
                     IsDeleted,
                     Getdate(),
                     CreatedBy,
                     Getdate(),
                     ModifiedBy,
                     @DomainID,
                     Newid()
              FROM   tbl_BloodGroup
              WHERE  DomainID = (SELECT TOP 1 DomainID
                                 FROM   tbl_BloodGroup
                                 ORDER  BY DomainID ASC)

              ----  TDS Component Mapping
              INSERT INTO tbl_TDSComponentMapping
                          (TDSKey,
                           ComponentId,
                           DomainId,
                           CreatedBy,
                           CreatedOn,
                           ModifiedBy,
                           ModifiedOn)
              SELECT TDSKey,
                     ComponentId,
                     @DomainId,
                     CreatedBy,
                     Getdate(),
                     ModifiedBy,
                     Getdate()
              FROM   tbl_TDSComponentMapping
              WHERE  DomainId = (SELECT TOP 1 DomainID
                                 FROM   tbl_TDSComponentMapping
                                 ORDER  BY DomainID ASC)

              SET @Output = 'Inserted Successfully./'
                            + CONVERT(VARCHAR, @DomainID)
          END

          --      ELSE
          --        BEGIN
          --            SET @Output = 'Email ID Already Exist.'
          --        END
          --  END
          --ELSE
          --  BEGIN
          --      SET @Output = 'Company Name Already Exist.'
          --  END
          SELECT @Output AS Output
      -- COMMIT TRANSACTION DML
      END TRY
      BEGIN CATCH
          -- ROLLBACK TRANSACTION DML
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
