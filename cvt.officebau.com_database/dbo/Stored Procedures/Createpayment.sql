﻿/****************************************************************************   
CREATED BY    : Ajith N
CREATED DATE  : 31 May 2017
MODIFIED BY   : Jennifer.S
MODIFIED DATE : 05-09-2017
<summary> 	
</summary>                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[Createpayment] (@Date            DATETIME,
                                       @ReceiptNO       VARCHAR(100),
                                       @Description     VARCHAR(1000),
                                       @PartyName       VARCHAR(100),
                                       @TransactionType VARCHAR(100),
                                       @TotalAmount     MONEY,
                                       @PaymentModeID   INT,
                                       @BankID          INT,
                                       @Reference       VARCHAR(1000),
                                       @LedgerID        INT,
                                       @SessionID       INT,
                                       @DomainID        INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT            VARCHAR(100),
                  @TransactionTypeID INT =(SELECT ID
                    FROM   tblMasterTypes
                    WHERE  NAME = @TransactionType)

          IF( (SELECT Count(1)
               FROM   tblCashBucket
               WHERE  DomainID = @DomainID) = 0 )
            INSERT INTO tblCashBucket
                        (AvailableCash,
                         DomainID)
            VALUES      (0,
                         @DomainID)

          INSERT INTO tblReceipts
                      (PartyName,
                       [Date],
                       ReceiptNo,
                       TransactionType,
                       [Description],
                       Amount,
                       PaymentMode,
                       BankID,
                       Reference,
                       LedgerID,
                       DomainID,
                       CreatedBy,
                       ModifiedBy)
          VALUES      (@PartyName,
                       @Date,
                       Isnull(@ReceiptNO, ''),
                       @TransactionTypeID,
                       @Description,
                       @TotalAmount,
                       @PaymentModeID,
                       @BankID,
                       @Reference,
                       @LedgerID,
                       @DomainID,
                       @SessionID,
                       @SessionID)

          DECLARE @SourceID INT = @@IDENTITY

          IF( Isnull(@BankID, 0) <> 0 )
            BEGIN
                INSERT INTO tblBRS
                            (BankID,
                             SourceID,
                             SourceDate,
                             SourceType,
                             Amount,
                             DomainID,
                             BRSDescription,
                             CreatedBy,
                             ModifiedBy)
                VALUES      (@BankID,
                             @SourceID,
                             @Date,
                             ( CASE
                                 WHEN ( @TransactionType = 'Receive Payment' ) THEN (SELECT ID
                                                                                     FROM   tblMasterTypes
                                                                                     WHERE  NAME = 'Receive Payment')
                                 ELSE((SELECT ID
                                       FROM   tblMasterTypes
                                       WHERE  NAME = 'Make Payment'))
                               END ),
                             @TotalAmount,
                             @DomainID,
                         @Description,
                             @SessionID,
                             @SessionID)

                INSERT INTO tblBookedBankBalance
                            (BRSID,
                             Amount,
                             DomainID,
                             CreatedBy,
                             ModifiedBy)
                VALUES      (@@IDENTITY,
                             @TotalAmount,
                             @DomainID,
                             @SessionID,
                             @SessionID)

                EXEC Managesystembankbalance
                  @DomainID
            END

          IF( @PaymentModeID = (SELECT ID
                                FROM   tbl_CodeMaster
                                WHERE [Type] = 'PaymentType' and Code = 'Cash' and IsDeleted = 0) )
            BEGIN
                UPDATE tblCashBucket
                SET    AvailableCash = ( CASE
                                           WHEN ( @TransactionType = 'Receive Payment' ) THEN ( AvailableCash + @TotalAmount )
                                           ELSE( AvailableCash - @TotalAmount )
                                         END ),
                       ModifiedOn = Getdate()
                WHERE  DomainID = @DomainID

                INSERT INTO tblVirtualCash
                            (SourceID,
                             Amount,
                             Date,
                             SourceType,
                             DomainID,
                             CreatedBy,
                             ModifiedBy)
                VALUES      (@SourceID,
                             ( CASE
                                 WHEN ( @TransactionType = 'Receive Payment' ) THEN ( @TotalAmount )
                                 ELSE( 0 - @TotalAmount )
                               END ),
                             @Date,
                             ( CASE
                                 WHEN ( @TransactionType = 'Receive Payment' ) THEN (SELECT ID
                                                                                     FROM   tblMasterTypes
                                                                                     WHERE  NAME = 'Receive Payment')
                                 ELSE((SELECT ID
                                       FROM   tblMasterTypes
                                       WHERE  NAME = 'Make Payment'))
                               END ),
                             @DomainID,
                             @SessionID,
                             @SessionID)
            END

          SET @Output = (SELECT [Message]
                         FROM   tblErrorMessage
                         WHERE  [Type] = 'Information'
                                AND Code = 'RCD_INS'
                                AND IsDeleted = 0) --'Inserted Successfully'

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
