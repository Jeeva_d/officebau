﻿/****************************************************************************   
CREATED BY		: K.SASIREKHA
CREATED DATE	: 04-10-2017 
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
 [SearchBusinessUnit] 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchBusinessUnit] ( @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY

          SELECT BUS.ID as ID,
                 BUS.ParentID as ParentID,
				 b.name as BusinessUnit,
				 BUS.Name as Name,
                BUS.Remarks as Remarks,
				BUS.Address as Address
          FROM   tbl_BusinessUnit BUS
		  left JOIn tbl_BusinessUnit b on b.ID=BUS.ParentID
          WHERE  BUS.IsDeleted = 0
                 AND BUS.DomainID = @DomainID
				 and bus.ParentID<>0
				 order by bus.ModifiedOn desc

      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
