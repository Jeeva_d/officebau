﻿/****************************************************************************   
CREATED BY		: Naneeshwar.M
CREATED DATE	: 30-NOV-2017
MODIFIED BY		:   Ajith N
MODIFIED DATE	:   26 Dec 2017
 <summary>
	[Searchcfsentry] null,1,1,106, 0
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchCFSEntry] (@ContainerName VARCHAR(50),
                                        @InwardTypeID  INT,
                                        @DomainID      INT,
                                        @EmployeeID    INT,
                                        @SalesPersonID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT CFS.ID                                         AS ID,
                 CFS.ContainerName                              AS ContainerName,
                 CASE
                   WHEN( CFS.InwardTypeID = 1 ) THEN
                     'Import'
                   ELSE
                     'Export'
                 END                                            AS InwardTypeName,
                 CFS.CreatedOn                                  AS CreatedOn,
                 Cast(ROW_NUMBER()
                        OVER (
                          ORDER BY CFS.ModifiedOn DESC) AS INT) AS Rowno,
                 EMP.FirstName + ' ' + ISNULL(EMp.lastname, '') AS [Name]
          FROM   tbl_CFSEntry CFS
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON emp.ID = CFS.CreatedBy
          WHERE  CFS.DomainID = @DomainID
                 -- AND CreatedBy = @EmployeeID
                 AND CFS.IsDeleted = 0
                 AND CFS.ContainerName LIKE '%' + Isnull(@ContainerName, '') + '%'
                 AND ( @InwardTypeID IS NULL
                        OR CFS.InwardTypeID = @InwardTypeID )
                 AND ( @SalesPersonID = 0
                        OR CFS.ModifiedBy = @SalesPersonID )
          ORDER  BY CFS.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
