﻿/****************************************************************************   
CREATED BY		:   DHANALAKSHMI.S
CREATED DATE	:   29-SEP-2017
MODIFIED BY		:   Ajith N
MODIFIED DATE	:   10 Oct 2017
 <summary>
	[Biometricsynchronize]  3,8,1

	To be Rework based on Employee ID

 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Biometricsynchronize] (@Year     INT,
                                              @MonthID  INT,
                                              @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output   VARCHAR(100),
              @SQl      NVARCHAR(MAX),
              @YearCode VARCHAR(20)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @YearCode = (SELECT FinancialYear
                           FROM   tbl_FinancialYear
                           WHERE  ID = @Year
                                  AND IsDeleted = 0
                                  AND DomainID = @DomainID)
          SET @SQl = '
		   
				IF EXISTS (SELECT 1 
					FROM tbl_Attendance_Process WHERE MONTH(AttendanceDate)='
                     + CONVERT(VARCHAR, @MonthID)
                     + ' AND YEAR(AttendanceDate)='
                     + CONVERT(VARCHAR, @YearCode)
                     + ')
									BEGIN
				IF EXISTS (SELECT 1
						   FROM   tbl_Attendance
						   WHERE  IsDeleted = 0 AND DomainID = '
                     + CONVERT(VARCHAR, @DomainID)
                     + '
								  AND Datepart(YYYY, LogDate) = '
                     + CONVERT(VARCHAR, @YearCode)
                     + '
								  AND Datepart(MM, LogDate) ='
                     + CONVERT(VARCHAR, @MonthID)
                     + ')
				  BEGIN
						UPDATE at
						SET    at.CheckIn = lg.InTime,
								at.CheckOut = lg.OutTime,
								----at.Duration = CONVERT(time, dateadd(minute,lg.Duration,0)),
								at.DURATION = ( CASE WHEN ( ISNULL(LG.InTime, ''00:00'') = ''00:00'' OR ISNULL(LG.OutTime, ''00:00'') = ''00:00'' ) THEN CAST(DATEADD(ms, DATEDIFF(MS, CONVERT(TIME, ''00:00:00''), CONVERT(TIME, ''00:00:00'')), ''00:00:00'') AS TIME)
													WHEN ( ISNULL(LG.InTime, ''00:00'') > ISNULL(LG.OutTime, ''00:00'') ) THEN CAST(DATEADD(ms, DATEDIFF(MS, CONVERT(TIME, LG.InTime), CONVERT(TIME, ''23:59'')), LG.OutTime) AS TIME)
													ELSE CAST(DATEADD(ms, DATEDIFF(MS, CONVERT(TIME, LG.InTime), CONVERT(TIME, LG.OutTime)), ''00:00:00'') AS TIME)
												END ),
								at.Punches = lg.PunchRecords
						FROM   dbo.Tbl_attendance at
								JOIN tbl_Attendance_process lg
								ON at.EmployeeID = lg.EmployeeId
									AND at.LogDate = CONVERT(DATE, lg.AttendanceDate)
						WHERE ISNULL(at.IsManual, ''0'') = 0

				  if((SELECT count(1)
					FROM   tbl_Attendance
					WHERE  IsDeleted = 0 AND DomainID = '
                     + CONVERT(VARCHAR, @DomainID)
                     + '
								  AND Datepart(YYYY, LogDate) = '
                     + CONVERT(VARCHAR, @YearCode)
                     + '
								  AND Datepart(MM, LogDate) ='
                     + CONVERT(VARCHAR, @MonthID) + ') <> (SELECT COUNT(1)
											FROM   tbl_Attendance_process))
						BEGIN
								INSERT INTO dbo.tbl_Attendance
								  (EmployeeID,
								  BiometricCode,
								   LogDate,
								   CheckIn,
								   CheckOut,
								   Punches,
								   Duration,
								   DomainID)
							SELECT LG.EmployeeId,
								LG.EmployeeId AS BiometricCode,
								 CONVERT(DATE, AttendanceDate, 100),
								 InTime,
								 OutTime,
								 PunchRecords,
								 ----CONVERT(TIME, Dateadd(minute, LG.Duration, 0)),
								 ( CASE WHEN ( ISNULL(LG.InTime, ''00:00'') = ''00:00'' OR ISNULL(LG.OutTime, ''00:00'') = ''00:00'' ) THEN CAST(DATEADD(ms, DATEDIFF(MS, CONVERT(TIME, ''00:00:00''), CONVERT(TIME, ''00:00:00'')), ''00:00:00'') AS TIME)
										WHEN ( ISNULL(LG.InTime, ''00:00'') > ISNULL(LG.OutTime, ''00:00'') ) THEN CAST(DATEADD(ms, DATEDIFF(MS, CONVERT(TIME, LG.InTime), CONVERT(TIME, ''23:59'')), LG.OutTime) AS TIME)
										ELSE CAST(DATEADD(ms, DATEDIFF(MS, CONVERT(TIME, LG.InTime), CONVERT(TIME, LG.OutTime)), ''00:00:00'') AS TIME)
										END ),
									'
                     + CONVERT(VARCHAR, @DomainID) + '
					  FROM   tbl_Attendance_process LG
							 LEFT JOIN dbo.Tbl_attendance at
							   ON at.EmployeeID = lg.EmployeeId AND CONVERT(DATE,at.LogDate) = CONVERT(DATE, lg.AttendanceDate)
						WHERE  at.LogDate IS NULL
								  END
				  END
				ELSE
				  BEGIN
					  INSERT INTO dbo.tbl_Attendance
								  (EmployeeID,
								BiometricCode,
								   LogDate,
								   CheckIn,
								   CheckOut,
								   Punches,
								   Duration,
								   DomainID)
					  SELECT EmployeeId,
							 EmployeeId AS BiometricCode,
							 convert(date, LG.AttendanceDate, 100),
							 LG.InTime,
							 LG.OutTime,
							 LG.PunchRecords,
							 ----CONVERT(time, dateadd(minute, LG.Duration, 0)),
							 ( CASE WHEN ( ISNULL(LG.InTime, ''00:00'') = ''00:00'' OR ISNULL(LG.OutTime, ''00:00'') = ''00:00'' ) THEN CAST(DATEADD(ms, DATEDIFF(MS, CONVERT(TIME, ''00:00:00''), CONVERT(TIME, ''00:00:00'')), ''00:00:00'') AS TIME)
									WHEN ( ISNULL(LG.InTime, ''00:00'') > ISNULL(LG.OutTime, ''00:00'') ) THEN CAST(DATEADD(ms, DATEDIFF(MS, CONVERT(TIME, LG.InTime), CONVERT(TIME, ''23:59'')), LG.OutTime) AS TIME)
									ELSE CAST(DATEADD(ms, DATEDIFF(MS, CONVERT(TIME, LG.InTime), CONVERT(TIME, LG.OutTime)), ''00:00:00'') AS TIME)
									END ),
							 '
                     + CONVERT(VARCHAR, @DomainID) + '
					  FROM   tbl_Attendance_process LG
				  END
				  BEGIN
				  SELECT ''Synchronized Successfully.''
				  END
				 END
				 ELSE
				 BEGIN 
				 SELECT ''No records found for the selected month and year.''
				 END
				TRUNCATE TABLE tbl_Attendance_Process
				'

          PRINT ( @SQl )

          EXEC(@SQl)

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
