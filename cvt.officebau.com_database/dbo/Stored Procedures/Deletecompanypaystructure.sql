﻿/****************************************************************************   
CREATED BY    : Dhanalakshmi. S
CREATED DATE  : 27 June 2017
MODIFIED BY   : 
MODIFIED DATE : 
 <summary> 
 	[DeleteCompanyPayStructure] 16,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Deletecompanypaystructure] (@ID       INT,
                                                   @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT VARCHAR(100)

          IF EXISTS (SELECT 1
                     FROM   tbl_EmployeePayStructure
                     WHERE  CompanyPayStubId = @ID
                            AND IsDeleted = 0
                            AND DomainID = @DomainID)
            BEGIN
                SET @Output = 'Payroll has been processed for this Company Pay structure. Hence cannot be Deleted.'

                GOTO finish
            END
          ELSE
            BEGIN
                UPDATE tbl_CompanyPayStructure
                SET    IsDeleted = 1
                WHERE  @ID = ID
                       AND DomainId = @DomainID

                SET @Output = 'Deleted successfully.'

                GOTO finish
            END

          FINISH:

          SELECT @OUTPUT

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
