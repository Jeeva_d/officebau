﻿      
/****************************************************************************               
CREATED BY  :               
CREATED DATE :         
MODIFIED BY  :         
MODIFIED DATE :         
 <summary>            
 select * from tblvendor        
 [Searchrptpartydetails] 2013,Income,1,1        
 select * from tblCustomer        
 </summary>                                       
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Searchrptpartydetails] (@PartyID  INT,      
                                               @Type     VARCHAR(100),      
                                               @DomainID INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          IF( @Type = 'Expense' )      
            BEGIN      
                SELECT t.ID,      
                       t.Date,      
                       t.Type,      
                       Isnull((SELECT Sum(Amount*Qty) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0)    
 FROM tblExpenseDetails WHERE isdeleted=0 AND expenseID =t.ID AND DomainID = @domainID), 0)      
                       AS Amount      
                FROM   TblExpense AS t      
                WHERE  t.isdeleted = 0      
                       AND t.DomainID = @domainID      
                       AND t.VendorID = @PartyID      
                UNION ALL      
                SELECT epm.ID,      
                       ep.Date,      
                       'Payment',      
                       epm.Amount      
                FROM   tblExpensePaymentMapping AS epm      
                       JOIN tblExpensePayment AS ep      
                         ON epm.ExpensePaymentID = ep.ID      
                WHERE  epm.ExpenseID IN (SELECT ID      
                                         FROM   TblExpense      
                                         WHERE  isdeleted = 0      
                                                AND VendorID = @PartyID      
                                                AND DomainID = @domainID)      
                       AND epm.DomainID = @domainID      
            END      
          ELSE      
            BEGIN      
                SELECT i.ID,      
                       i.Date,      
                       i.Type,      
                       (SELECT Sum(Qty * Rate)      
                        FROM   tblinvoiceitem      
                        WHERE  isdeleted = 0      
                               AND InvoiceID = i.ID      
                               AND DomainID = @domainID) AS Amount      
                FROM   tblinvoice i      
                WHERE  isdeleted = 0      
                       AND CustomerID = @PartyID      
                       AND i.DomainID = @domainID      
                UNION ALL      
                SELECT epm.ID,      
                       ep.paymentDate,      
                       'Payment',      
                       epm.Amount      
                FROM   tblInvoicePaymentMapping AS epm      
                       JOIN tblinvoicePayment AS ep      
                         ON epm.invoicePaymentID = ep.ID      
                WHERE  epm.InvoiceID IN (SELECT ID      
                                         FROM   TblInvoice      
                                         WHERE  isdeleted = 0      
                                                AND CustomerID = @PartyID      
                                                AND DomainID = @domainID)      
                       AND epm.DomainID = @domainID      
            END      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
