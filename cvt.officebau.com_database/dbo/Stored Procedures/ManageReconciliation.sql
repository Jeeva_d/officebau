﻿

/****************************************************************************       
CREATED BY    :     
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE :     
 <summary>     
[ManageReconciliation] 1,1    
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageReconciliation] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output            VARCHAR(50),
              @PreClosingBalance MONEY,
              @ClosingBalance    MONEY,
              @BankCount         INT,
              @Count             INT,
              @TempBankCount     INT = 1,
              @TempCount         INT = 1

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Bank TABLE
            (
               ID             INT IDENTITY,
               BANKID         INT,
               OpeningBalance MONEY,
               OpeningBalDate DATE
            )
          DECLARE @BreakEvnt INT = 0

          INSERT INTO @Bank ----- Getting All Bank Detalis    
          SELECT DISTINCT ID             AS ID,
                          OpeningBalance AS OpeningBalance,
                          OpeningBalDate AS OpeningBalDate
          FROM   tblBank
          WHERE  IsDeleted = 0
                 AND DomainID = @DomainID

          ------ Set 0 for All records    
          --UPDATE tblBRS    
          --SET    IsReconsiled = 0  
          --      -- ReconsiledDate = NULL    
          --WHERE  IsDeleted = 0    
          --       AND DomainID = @DomainID    
          --       AND BankID IN (SELECT BankID    
          --                     FROM   @Bank    
          --                     )    
          --       AND ReconsiledDate >= (SELECT OpeningBalDate    
          --                              FROM   @Bank    
          --                              )    
          SET @BankCount = (SELECT Count(1)
                            FROM   @Bank)

          WHILE @BankCount >= @TempBankCount
            BEGIN
                SET @PreClosingBalance = 0

                DECLARE @RedoReconciliation TABLE
                  (
                     ID                   INT IDENTITY,
                     HasReconciled        INT,
                     ReconciliationBankID INT,
                     [Type]               INT,
                     [Date]               DATETIME,
                     Amount               MONEY,
                     DomainID             INT
                  )

                INSERT INTO @RedoReconciliation
                SELECT ID             AS ID,
                       BankID         AS BankID,
                       [SourceType]   AS [Type],
                       ReconsiledDate AS ReconsiledDate,
                       Amount         AS Amount,
                       @DomainID      AS DomainID
                FROM   tblBRS
                WHERE  IsDeleted = 0
                       AND DomainID = @DomainID
                       AND BankID = (SELECT BankID
                                     FROM   @Bank
                                     WHERE  ID = @TempBankCount)
                       AND ReconsiledDate >= (SELECT OpeningBalDate
                                              FROM   @Bank
                                              WHERE  ID = @TempBankCount)
                ORDER  BY ReconsiledDate

                SET @Count = (SELECT Count(ID)
                              FROM   @RedoReconciliation)

                DECLARE @InitialSetup BIT = 0

                WHILE @Count >= @TempCount
                  BEGIN
                      DECLARE @Type INT = (SELECT [Type]
                         FROM   @RedoReconciliation
                         WHERE  ID = @TempCount)
                      DECLARE @Amount MONEY = Isnull((SELECT Amount
                                FROM   @RedoReconciliation
                                WHERE  ID = @TempCount), 0)
                      DECLARE @StartingBalance MONEY = Isnull((SELECT OpeningBalance
                                FROM   @Bank
                                WHERE  ID = @TempBankCount), 0)

                      IF( Isnull(@PreClosingBalance, 0) = 0
                          AND @InitialSetup = 0 )
                        BEGIN
                            SET @PreClosingBalance = @StartingBalance
                            SET @InitialSetup = 1
                        END

                      SET @ClosingBalance = ( CASE
                                                WHEN @Type IN (SELECT ID
                                                               FROM   tblMasterTypes
                                                               WHERE  NAME IN ( 'PayBill', 'FromBank', 'Bill', 'Expense', 'Make Payment' )) THEN
                                                  Isnull(@PreClosingBalance, 0) - Isnull(@Amount, 0)
                                                ELSE
                                                  Isnull(@PreClosingBalance, 0)
                                                  + Isnull(@Amount, 0)
                                              END )

                      UPDATE tblBRS
                      SET    AvailableBalance = Isnull(@ClosingBalance, 0),
                             ModifiedOn = Getdate()
                      WHERE  ID = (SELECT HasReconciled
                                   FROM   @RedoReconciliation
                                   WHERE  ID = @TempCount)
                             AND DomainID = @DomainID

                      SET @PreClosingBalance = Isnull(@ClosingBalance, 0)

                      --SET @Output = 'Please Check Bank Balance.'    
                      UPDATE tblBRS
                      SET    IsReconsiled = 1--,    
                      --ReconsiledDate = ReconsiledDate    
                      WHERE  ID = (SELECT HasReconciled
                                   FROM   @RedoReconciliation
                                   WHERE  ID = @TempCount)
                             AND DomainID = @DomainID

                      SET @TempCount = @TempCount + 1
                  END

                SET @TempBankCount = @TempBankCount + 1
            END

          SET @Output = (SELECT [Message]
                         FROM   tblErrorMessage
                         WHERE  [Type] = 'Information'
                                AND Code = 'RCD_UPD'
                                AND IsDeleted = 0) --'Updated Successfully.'    
          SELECT @Output,
                 @Count,
                 @TempCount

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
