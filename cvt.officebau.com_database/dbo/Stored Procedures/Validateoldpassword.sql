﻿/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  : 06-Jun-2017  
MODIFIED BY   :  
MODIFIED DATE  : 
 <summary>          
    [Validateoldpassword] 'naneeshwar_m@skybasenet.com','eWK9XoF32iP4UxA86wE+9LRLWKCN4NItMXvaPr8LR/Q='  
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Validateoldpassword] (@UserName VARCHAR(100),
                                              @Password VARCHAR(100))
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @UserMessage VARCHAR(100),
              @UserID      INT = 0,
              @DomainID    INT = 0,
              @CompanyName VARCHAR(150)

      BEGIN TRY
          BEGIN TRANSACTION

          -- SET @UserName= CASE
          --                  WHEN( Substring(@UserName, 1, 3) = 'PNP' ) THEN( 'PAN' + Substring(@UserName, 4, 4) )
          --                  WHEN ( Substring(@UserName, 1, 3) = 'AHD' ) THEN( 'AHM' + Substring(@UserName, 4, 4) )
          --ELSE @UserName
          --                END
          SET @UserID = (SELECT ID
                         FROM   tbl_EmployeeMaster
                         WHERE  ( Isnull(IsActive, 0) = 0
                                   OR ( Isnull(IsActive, 0) = 1
                                        AND CONVERT(DATE, InactiveFrom) >= CONVERT(DATE, GETDATE()) ) )
                                AND IsDeleted = 0
                                AND LoginCode = @UserName)

          IF (SELECT COUNT(1)
              FROM   tbl_Login
              WHERE  EmployeeID = @UserID
                     AND Password = @Password) = 0
            BEGIN
                SET @UserMessage = 'Invalid Password'

                GOTO OutputResult
            END
          ELSE
            BEGIN
                SET @UserMessage = 'Valid'

                GOTO OutputResult
            END

          OUTPUTRESULT:

          SELECT @UserMessage AS UserMessage,
                 @UserID      AS UserID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
