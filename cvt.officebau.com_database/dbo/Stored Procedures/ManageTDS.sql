﻿/****************************************************************************   
CREATED BY		:   
CREATED DATE	:   
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageTDS] (@TDSAmount  MONEY,
                                   @EmployeeID INT,
                                   @MonthID    INT,
                                   @Year     INT,
                                   @SessionID  INT,
                                   @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(50)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF EXISTS (SELECT 1
                     FROM   tbl_TDS
                     WHERE  EmployeeID = @EmployeeID
                            AND MonthId = @MonthID
                            AND YearID = @Year
                            AND DomainID = @DomainID
                            AND IsDeleted = 0)
            BEGIN
                UPDATE tbl_TDS
                SET    TDSAmount = @TDSAmount
				--CASE
    --                                 WHEN( @TDSAmount <= 0 ) THEN 0
    --                                 ELSE @TDSAmount
    --                               END
                WHERE  EmployeeID = @EmployeeID
                       AND MonthId = @MonthID
                       AND YearID = @Year
                       AND DomainID = @DomainID
                       AND IsDeleted = 0

                SET @Output = 'Updated'
            END
          ELSE
            BEGIN
                INSERT INTO tbl_TDS
                            (EmployeeID,
                             MonthId,
                             YearID,
                             TDSAmount,
                             BaseLocationID,
                             DomainId,
                             CreatedBy,
                             ModifiedBy,
                             CreatedOn,
                             ModifiedOn)
                SELECT @EmployeeID,
                       @MonthID,
                       @Year,
					   @TDSAmount,
                       --CASE
                       --  WHEN( @TDSAmount <= 0 ) THEN 0
                       --  ELSE @TDSAmount
                       --END,
                       (SELECT BaseLocationID
                        FROM   tbl_EmployeeMaster
                        WHERE  id = @EmployeeID),
                       @DomainID,
                       @SessionID,
                       @SessionID,
                       Getdate(),
                       Getdate()

                SET @Output = 'Inserted'
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
