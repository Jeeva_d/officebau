﻿/**************************************************************************** 
CREATED BY			:	Dhanalakshmi.S
CREATED DATE		:	12 June 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary> 
[Manageclaimcategory]  4,'Test1','Test1',0,1,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageClaimCategory] (@ID        INT,
                                             @Name      VARCHAR(100),
                                             @Remarks   VARCHAR(1000),
                                             @IsDeleted BIT,
                                             @SessionID INT,
                                             @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(100) = 'Operation Failed!'

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @IsDeleted = 1 )
            BEGIN
                IF( ( (SELECT Count(1)
                       FROM   tbl_ClaimsRequest
                       WHERE  CategoryID = @ID
                              AND DomainID = @DomainID
                              AND IsDeleted = 0) > 0 )
                     OR ( (SELECT Count(1)
                           FROM   tbl_ClaimPolicy
                           WHERE  ExpenseType = @ID
                                  AND IsDeleted = 0) > 0 ) )
                  BEGIN
                      SET @Output = 'The record is referred.'

                      GOTO Finish
                  END
                ELSE
                  BEGIN
                      UPDATE tbl_ClaimCategory
                      SET    IsDeleted = 1,
                             ModifiedBY = @SessionID
                      WHERE  ID = @ID
                             AND DomainID = @DomainID

                      SET @Output = 'Deleted Successfully'

                      GOTO Finish
                  END
            END
          ELSE
            BEGIN
                IF( @ID = 0 )
                  BEGIN
                      IF( (SELECT Count(1)
                           FROM   tbl_ClaimCategory
                           WHERE  NAME = @Name
                                  AND DomainID = @DomainID
                                  AND IsDeleted = 0) = 0 )
                        BEGIN
                            INSERT INTO tbl_ClaimCategory
                                        (NAME,
                                         Remarks,
                                         CreatedBy,
                                         ModifiedBy,
                                         DomainID)
                            VALUES      (@Name,
                                         @Remarks,
                                         @SessionID,
                                         @SessionID,
                                         @DomainID)

                            SET @Output = 'Inserted Successfully'
                        END
                      ELSE
                        SET @Output = 'Already Exists'
                  END
                ELSE
                  BEGIN
                      IF( (SELECT Count(1)
                           FROM   tbl_ClaimCategory
                           WHERE  NAME = @Name
                                  AND DomainID = @DomainID
                                  AND ID <> @ID
                                  AND IsDeleted = 0) = 0 )
                        BEGIN
                            UPDATE tbl_ClaimCategory
                            SET    NAME = @Name,
                                   Remarks = @Remarks,
                                   ModifiedBY = @SessionID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID
                                   AND DomainID = @DomainID

                            SET @Output = 'Updated Successfully'
                        END
                      ELSE
                        BEGIN
                            SET @Output = 'Already Exists'
                        END
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
