﻿/****************************************************************************         
CREATED BY   :   
CREATED DATE  :  
MODIFIED BY   :  
MODIFIED DATE    
 <summary>      
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchSalesOrder] (@DomainID     INT,
                                          @StartDate    VARCHAR(50),
                                          @EndDate      VARCHAR(50),
                                          @CustomerName VARCHAR(50),
                                          @Status       VARCHAR(50),
                                          @Notations    VARCHAR(50),
                                          @Amount       MONEY)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF( @StartDate != '' )
            SET @StartDate = Cast(@StartDate AS DATETIME)

          IF( @EndDate != '' )
            SET @EndDate = Cast(@EndDate AS DATETIME)

          SELECT inv.ID                                            AS ID,
                 inv.CustomerID                                    AS CustomerID,
                 cus.NAME                                          AS CustomerName,
                 inv.Date                                          AS InvoiceDate,
                 inv.SONo                                          AS InvoiceNo,
                 (SELECT Substring((SELECT + ',' + it.ItemDescription
                                    FROM   tblSOItem it
                                    WHERE  it.IsDeleted = 0
                                           AND inv.ID = it.SOID
                                    FOR XML PATH('')), 2, 200000)) AS [Description],
                 (SELECT (( Sum(Qty * Rate)
                            + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) ))
                  FROM   tblSOItem
                  WHERE  SOID = inv.ID
                         AND IsDeleted = 0
                         AND DomainID = @DomainID)                 AS TotalAmount,
                 cd.Code                                           AS Status,
                 inv.ModifiedOn,
                 em.FullName                                       AS CreatedBy
          INTO   #TempIncome
          FROM   tblSalesOrder inv
                 JOIN tblCustomer cus
                   ON cus.ID = inv.CustomerID
                 LEFT JOIN tbl_CodeMaster cd
                        ON cd.ID = inv.StatusID
                 LEFT JOIN tbl_EmployeeMaster em
                        ON em.Id = inv.ModifiedBy
          WHERE  inv.IsDeleted = 0
                 AND inv.DomainID = @DomainID
                 AND ( ( Isnull(@Status, '') = ''
                         AND inv.StatusID IN(SELECT ID
                                             FROM   tbl_CodeMaster
                                             WHERE  Code != 'Close'
                                                    AND IsDeleted = 0) )
                        OR ( Isnull(@Status, '') != 'All'
                             AND inv.StatusID IN(SELECT ID
                                                 FROM   tbl_CodeMaster
                                                 WHERE  [Type] = @Status
                                                        AND IsDeleted = 0) )
                        OR ( Isnull(@Status, '') = 'All'
                             AND inv.StatusID IN(SELECT ID
                                                 FROM   tbl_CodeMaster
                                                 WHERE  Type IN( 'Open', 'Partial', 'Close' )
                                                        AND IsDeleted = 0) ) )
                 AND ( Isnull(@CustomerName, '') = ''
                        OR cus.NAME LIKE '%' + @CustomerName + '%' )
                 AND ( ( @StartDate IS NULL
                          OR @StartDate = '' )
                        OR [Date] >= @StartDate )
                 AND ( ( @EndDate IS NULL
                          OR @EndDate = '' )
                        OR [Date] <= @EndDate )
          ORDER  BY inv.ModifiedOn DESC

          SELECT *
          FROM   #TempIncome
          WHERE  ( Isnull(@Amount, 0) = 0 )
                  OR ( Isnull(@Notations, '') = 'EqualTo'
                       AND TotalAmount = Isnull(@Amount, 0) )
                  OR ( Isnull(@Notations, '') = 'GreaterThanEqual'
                       AND TotalAmount >= @Amount )
                  OR ( Isnull(@Notations, '') = 'LessThanEqual'
                       AND TotalAmount <= @Amount )
          ORDER  BY InvoiceDate DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
