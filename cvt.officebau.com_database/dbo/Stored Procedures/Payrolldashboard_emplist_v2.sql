﻿
/****************************************************************************       
CREATED BY   :    Priya K  
CREATED DATE  :   13 Nov 2018    
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>     
    [Payrolldashboard_emplist] null,2017,'BU',1,'Hyderabad'   
             
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Payrolldashboard_emplist_v2] (@MonthId      INT,
                                                     @Year         INT,
                                                     @Type         VARCHAR(50),
                                                     @DomainID     INT,
                                                     @BusinessUnit VARCHAR(100),
                                                     @EmployeeID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @cols  AS NVARCHAR(MAX),
                  @query AS NVARCHAR(MAX)
          DECLARE @DataSource TABLE
            (
               [Value] NVARCHAR(128)
            )

          INSERT INTO @DataSource
                      ([Value])
          SELECT Item
          FROM   dbo.Splitstring ((SELECT businessunitid
                                   FROM   tbl_employeemaster
                                   WHERE  id = @EmployeeID
                                          AND DomainID = @DomainID), ',')
          WHERE  Isnull(Item, '') <> ''

          SELECT *
          INTO   #Result
          FROM   VW_Payroll vwpay
          WHERE  vwpay.MonthId = @MonthID
                 AND vwpay.YearId = @Year
                 AND ( ( @Type = 'Business Unit'
                         AND vwpay.NAME = @BusinessUnit )
                        OR ( @Type = 'Department'
                             AND vwpay.Department = @BusinessUnit )
                        OR ( @Type = 'Designation'
                             AND Isnull(vwpay.Designation, 'Others') = @BusinessUnit ) )
                 AND vwpay.BuID IN(SELECT *
                                   FROM   @DataSource)

          SET @cols = Stuff((SELECT ',' + Quotename(description)
                             FROM   VW_Payroll
							 WHERE DomainId = @DomainID
                             GROUP  BY description,
                                       ComponentId
                             ORDER  BY ComponentId ASC
                             FOR XML path('')), 1, 1, ' ')
          SET @query = 'SELECT   
       EmployeeId, MonthId,Month,YearId,Emp_Code, Employee_Name,            
     Joining_Date,  Name As Business_Unit,'
                       + @cols + '  
      from             (            
     select  
      EmployeeId, MonthId,Month,YearId, Employee_Code AS Emp_Code, Description, Employee_Name, Joining_Date,               
     Name, Amount     
     from #Result  
       
     ) x            pivot            (sum(Amount)               
     for Description in (' + @cols
                       + ')            ) p '

          EXEC (@query);
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
