﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	05-JUN-2017
MODIFIED BY			:	
MODIFIED DATE		:	
<summary>        
  [Searchrbsusermenuaccess] 1768,'NOTAUTH',6
</summary>                         
*****************************************************************************/
CREATE PROCEDURE [dbo].[SearchRBSUserMenuAccess] (@UserID   INT,
                                                  @MenuCode VARCHAR(20),
                                                  @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT MRead   AS MenuRead,
                 MWrite  AS MenuWrite,
                 MEdit   AS MenuEdit,
                 MDelete AS MenuDelete
          FROM   tbl_RBSUserMenuMapping
          WHERE  EmployeeID = @UserID
                 AND MenuID = (SELECT ID
                               FROM   tbl_RBSMenu
                               WHERE  MenuCode = @MenuCode and DomainID=@DomainID And IsDeleted=0)
                 AND IsDeleted = 0
                 AND DomainID = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
