﻿

/****************************************************************************   
CREATED BY		: DHANALAKSHMI. S
CREATED DATE	: 05-JAN-2018
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
 [SearchDesignationMapping] 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchDesignationMapping] ( @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY

        Select	DM.ID AS ID,
				BA.Name AS Band,
				EG.Name AS [Level],
				D.Name  As DesignationName,
				DM.DesignationID AS DesignationID,
                DM.BandID        AS BandID,
                DM.GradeID       AS LevelID
        FROM    tbl_EmployeeDesignationMapping DM
			 LEFT JOIN tbl_Band BA
			 ON BA.ID = DM.BandID
			 LEFT JOIN tbl_EmployeeGrade EG
			 ON EG.ID = DM.GradeID
			 LEFT JOIN tbl_Designation D
			 ON D.ID = DM.DesignationID
		 WHERE DM.IsDeleted = 0 
		       AND DM.DomainID = @DomainID
         ORDER BY D.Name ASC

      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
