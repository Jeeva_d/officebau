﻿/****************************************************************************     
CREATED BY   :  Naneeshwar.M  
CREATED DATE  :   20-SEP-2017  
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>   
           [Getptconfiguration] 1  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GETPTConfiguration] (@ID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT pt.ID             AS ID,
                 MinGross          AS MinGross,
                 MaxGross          AS MaxGross,
                 Amount            AS Amount,
                 pt.BusinessUnitID AS BaseLocationID,
                 fy.id             AS FinancialYear,
                 emp.FullName      AS ModifiedBy,
                 pt.ModifiedOn     AS ModifiedOn,
                 BU.NAME           AS BaseLocation
          FROM   tbl_PTConfiguration pt
                 LEFT JOIN tbl_EmployeeMaster emp
                        ON emp.ID = pt.ModifiedBy
                 LEFT JOIN tbl_FinancialYear fy
                        ON fy.NAME = pt.FYID
                           AND pt.DomainID = fy.DomainID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = emp.BaseLocationID
          WHERE  pt.ID = @ID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
