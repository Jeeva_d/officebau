﻿    
 /****************************************************************************     
CREATED BY      :     
CREATED DATE  :     
MODIFIED BY   : Ajith N    
MODIFIED DATE  : 01 Mar 2018    
 <summary>      
 [Rpt_searchemployeepayroll] 1,0,'1,2,3,4,5,6,7,8,9,10,11,12', 'NetSalary',3    
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Rpt_searchemployeepayroll] (@DomainID        INT,    
                                                   @DepartmentID    INT,    
                                                   @Location        VARCHAR(100),    
                                                   @Component       VARCHAR(50),    
                                                   @FinancialYearID INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          DECLARE @CurrentMonthYear DATE,    
                  @DynamicQuery     VARCHAR(MAX) = '',    
                  @Month            INT = 3,    
                  @MonthID          VARCHAR(20),    
                  @lastMonthYear    DATETIME,    
                  @PivoteField      VARCHAR(MAX) = '',    
                  @OutputField      VARCHAR(MAX) = '',    
                  @TotalAmountField VARCHAR(MAX) = '',    
                  @SqlQuery         VARCHAR(MAX) = '',    
                  @i                INT = 0,    
                  @NoOfMonths       INT = 11 -- Total six Months 0 to 11    
          DECLARE @Year INT= (SELECT Cast(NAME AS INT) + 1    
             FROM   tbl_FinancialYear    
             WHERE  ID = @FinancialYearID    
                    AND Isnull(IsDeleted, 0) = 0    
                    AND DomainId = @DomainID)    
    
          IF( Isnull(@Component, '') = '' )    
            SET @Component = 'NetSalary'    
    
          CREATE TABLE #DataSource    
            (    
               [Value] NVARCHAR(128)    
            )    
    
          INSERT INTO #DataSource    
                      ([Value])    
          SELECT Item    
          FROM   dbo.Splitstring (@Location, ',')    
          WHERE  Isnull(Item, '') <> ''    
    
          --SELECT @Month = Max(MonthID),    
          --       @Year = Max(YearId)    
          --FROM   tbl_EmployeePayroll EP    
          --       LEFT JOIN tbl_EmployeeMaster EMP    
          --              ON EMP.ID = EP.EmployeeId    
          --       LEFT JOIN tbl_BusinessUnit BU    
          --              ON BU.ID = EMP.BusinessUnitID    
          --WHERE  EP.IsDeleted = 0    
          --       AND EP.IsProcessed = 1    
          --       AND ( ( @Location = Cast(0 AS VARCHAR)    
          --                OR @Location IS NULL )    
          --              OR ( EP.BaseLocationID IN (SELECT [Value]    
          --                                         FROM   #DataSource    
          --                                         WHERE  EMP.IsDeleted = 0) ) )    
          --       AND ( ( @DepartmentID IS NULL    
          --                OR Isnull(@DepartmentID, 0) = 0 )    
          --              OR EMP.DepartmentID = @DepartmentID );    
          SELECT @CurrentMonthYear = Cast(CONVERT(VARCHAR(10), @Month) + '-' + '1' + '-'    
                                          + CONVERT(VARCHAR(10), @Year) AS DATE);    
                    
          SET @lastMonthYear = ( Dateadd(MONTH, -@NoOfMonths, @CurrentMonthYear) )    
    
          WHILE( @i <= @NoOfMonths )    
          BEGIN    
                SET @PivoteField += ',['    
                                    + Cast(Month(Dateadd(MONTH, @i, @lastMonthYear)) AS VARCHAR)    
                                    + ']'    
                --IF( Month(Dateadd(MONTH, @i, @lastMonthYear)) > Month(Getdate()) )    
                --  BREAK    
                SET @OutputField += ',CAST(['    
                                    + Cast(Month(Dateadd(MONTH, @i, @lastMonthYear)) AS VARCHAR)    
                                    + '] AS DECIMAL(32,2)) AS '    
            + Substring(Cast(Dateadd(MONTH, @i, @lastMonthYear) AS VARCHAR), 1, 3)    
                SET @TotalAmountField += '+ SUM(ISNULL(['    
               + Cast(Month(Dateadd(MONTH, @i, @lastMonthYear)) AS VARCHAR)    
                                         + '],0))'    
                SET @i = @i + 1    
            END    
    
          SET @TotalAmountField = Substring(@TotalAmountField, 2, 2000)    
    
          SELECT DISTINCT EMP.ID                                         AS EmployeeId,    
                          Isnull(EMP.EmpCodePattern, '') + EMP.Code    AS [EmpCode],    
                          EMP.FirstName + ' ' + Isnull(EMP.LastName, '') AS EmpName,    
                          CONVERT(VARCHAR(20), EMP.DOJ, 106)             AS JoiningDate,    
                          BU.NAME                                        AS Business_Unit,    
                          EPR.BaseLocationID                             AS BaseLocationID,    
                          DEP.NAME                                       AS [Department_Name],    
                          EPR.DomainId                                   AS DomainId    
          INTO   #tmpEmployee    
          FROM   tbl_EmployeePayroll EPR    
                 LEFT JOIN tbl_EmployeeMaster EMP    
                        ON EPR.EmployeeId = EMP.ID    
                           AND EMP.IsDeleted = 0    
                 LEFT JOIN tbl_BusinessUnit BU    
                        ON BU.ID = EPR.BaseLocationID    
                 LEFT JOIN tbl_Department DEP    
                        ON DEP.ID = EMP.DepartmentID    
          WHERE  Cast(CONVERT(VARCHAR, EPR.[yearid]) + '-'    
                      + CONVERT(VARCHAR, EPR.monthid) + '-01' AS DATETIME) >= @lastMonthYear    
                 AND Cast(CONVERT(VARCHAR, EPR.[yearid]) + '-'    
                          + CONVERT(VARCHAR, EPR.monthid) + '-01' AS DATETIME) <= @CurrentMonthYear    
                 AND EPR.IsDeleted = 0    
                 AND EPR.IsProcessed = 1    
                 AND EMP.IsDeleted = 0    
                 AND EPR.DomainId = @DomainID    
                 AND ( ( @Location = Cast(0 AS VARCHAR) )    
                        OR ( EPR.BaseLocationID IN (SELECT [Value]    
                                                    FROM   #DataSource) ) )    
                 AND ( ( @DepartmentID IS NULL    
                          OR Isnull(@DepartmentID, 0) = 0 )    
                        OR EMP.DepartmentID = @DepartmentID )    
          ORDER  BY Isnull(EMP.EmpCodePattern, '') + EMP.Code    
    
          SET @SqlQuery = 'SELECT EmployeeId, BaseLocationID , DomainId '    
                          + @PivoteField    
                          + '    
       INTO   #Salary    
       FROM   (SELECT EmployeeId, '    
                          + @Component    
                          + ',    
           MonthId,    
           EPR.BaseLocationID, ISNULL(EPR.DomainId,0) AS DomainId    
       FROM   tbl_EmployeePayroll  EPR    
           JOIN tbl_EmployeeMaster EMP    
          ON EPR.EmployeeId = EMP.ID    
          AND EMP.IsDeleted = 0    
           LEFT JOIN tbl_BusinessUnit BU    
            ON BU.ID = EPR.BaseLocationID    
       WHERE  EPR.IsDeleted = 0    
           AND EPR.isprocessed = 1    
           AND EMP.IsDeleted = 0    
            AND EPR.DomainId = '    
                          + Cast(@DomainID AS VARCHAR)    
                          + '    
          AND CAST(CONVERT(VARCHAR, EPR.[yearid]) + ''-''    
      + CONVERT(VARCHAR, EPR.monthid) + ''-01'' AS DATETIME) >= CAST('''    
            + Cast(@lastMonthYear AS VARCHAR)    
                          + ''' AS DATETIME)    
      AND CAST(CONVERT(VARCHAR, EPR.[yearid]) + ''-''    
                    + CONVERT(VARCHAR, EPR.monthid) + ''-01'' AS DATETIME) <= CAST('''    
                          + Cast(@CurrentMonthYear AS VARCHAR)    
                          + ''' AS DATETIME)    
                     AND ( (''' + @Location    
                          + '''= CAST(0 AS VARCHAR) )    
         OR ( EPR.BaseLocationID IN (SELECT [Value]    
                  FROM #DataSource) ) )    
          AND ( ('    
                          + Cast(@DepartmentID AS VARCHAR)    
                          + 'IS NULL    
          OR Isnull('    
                          + Cast(@DepartmentID AS VARCHAR)    
         + ', 0) = 0 )    
           OR EMP.DepartmentID ='    
                          + Cast(@DepartmentID AS VARCHAR)    
                          + ')) P    
           PIVOT(SUM(' + @Component    
                          + ')    
            FOR MonthId IN ( '    
                          + Substring(@PivoteField, 2, 2000)    
                          + ')) AS pt    
    
     SELECT [EmpCode], [EmpName], [JoiningDate], [Business_Unit],[Department_Name],'    
                          + Isnull(@TotalAmountField, 0)    
                          + ' AS [Total_Amount]' + @OutputField    
                          --+',row_number() over (order by EmpName) as "S.No"'      
                          + '   
  INTO #tmpResult  
     FROM   #tmpEmployee E    
         LEFT JOIN #Salary S    
          ON E.EmployeeId = S.EmployeeId and E.BaseLocationID=s.BaseLocationID AND s.DomainId = e.DomainId    
          GROUP BY [EmpCode], [EmpName], [JoiningDate], [Business_Unit],[Department_Name],s.DomainId,e.DomainId'    
             + @PivoteField + ';SELECT * FROM #tmpResult WHERE [Total_Amount] > 0'  
    
    
          PRINT @SqlQuery    
    
          EXEC(@SqlQuery)    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR (@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
