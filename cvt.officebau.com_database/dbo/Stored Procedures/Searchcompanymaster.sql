﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
   [Searchcompanymaster] null,null,null,null
 </summary>                          
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchcompanymaster](@Name      VARCHAR(100),
                                            @ContactNo VARCHAR(100),
                                            @EmailID   VARCHAR(100),
                                            @GSTNO     VARCHAR(100))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT ID        AS ID,
                 NAME      AS NAME,
                 FullName  AS FullName,
                 EmailID   AS EmailID,
                 Address   AS Address1,
                 ContactNo AS ContactNo,
                 PanNo     AS PANNo,
                 TANNo     AS TANNo,
                 GSTNo     AS GSTNo,
                 Website   AS Website
          FROM   tbl_Company
          WHERE  IsDeleted = 0
                 AND Isnull(NAME, '') LIKE '%' + Isnull(@Name, '') + '%'
                 AND Isnull(ContactNo, '')LIKE '%' + Isnull(@ContactNo, '') + '%'
                 AND Isnull(EmailID, '')LIKE '%' + Isnull(@EmailID, '') + '%'
                 AND Isnull(GSTNo, '')LIKE '%' + Isnull(@GSTNO, '') + '%'
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
