﻿/****************************************************************************         
CREATED BY  :   JENNIFER.S      
CREATED DATE :   06-Jun-2017      
MODIFIED BY  :   Ajith N     
MODIFIED DATE :   05 Jan 2018      
 <summary>      
  [Manageleaverequest] 0,1,1,'2017-10-18','2017-10-18',null,'test','test',null,2,1,1,0,1      
  select * from tbl_EmployeeLeave       
  select * from tbl_EmployeeLeaveDateMapping      
 </summary>                                 
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Manageleaverequest] (@ID                   INT,    
                                             @EmployeeID           INT,    
                                             @PurposeID            INT,    
                                             @LeaveTypeID          INT,    
                                             @FromDate             DATE,    
                                             @ToDate               DATE,    
                                             @Duration             TINYINT,    
                                             @Reason               VARCHAR(500),    
                                             @RequesterRemarks     VARCHAR(8000),    
                                             @AlternativeContactNo VARCHAR(20),    
                                             @ApproverID           INT,    
                                             @SessionID            INT,    
                                             @DomainID             INT,    
                                             @IsDeleted            BIT,    
                                             @IsHalfDay            BIT,    
            @ReplacementEmployeeID int)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output            VARCHAR(150),    
              @LeaveManagementID INT,    
              @BaseLocationID    INT,    
              @AppConfigValue    INT,    
              @LeaveStatusID     INT = (SELECT ID    
                 FROM   tbl_Status    
                 WHERE  Code = 'Pending'    
                        AND [Type] = 'Leave'    
                        AND IsDeleted = 0),    
              @RegionIDs         VARCHAR(20) = (SELECT Cast(RegionID AS VARCHAR(10))    
                 FROM   tbl_EmployeeMaster EM    
                 WHERE  EM.ID = @SessionID    
                 FOR XML path(''))    
    
      SELECT @BaseLocationID = BaseLocationID    
      FROM   tbl_EmployeeMaster EM    
      WHERE  EM.ID = @SessionID    
    
      SET @AppConfigValue = ISNULL((SELECT ISNULL(ConfigValue, 0) AS Value    
                                    FROM   tbl_ApplicationConfiguration AC    
                                           JOIN tbl_ConfigurationMaster CM    
                                             ON CM.ID = AC.ConfigurationID    
                                                AND cm.Code = 'LEVWKEND'    
                                    WHERE  AC.BusinessUnitID = @BaseLocationID), 0)    
    
      CREATE TABLE #tempRequestLeaveDays    
        (    
           RequestedDate DATE,    
           RequestedDays VARCHAR(25),    
           NoOfDays      INT,    
     [Type] VARCHAR(50)    
        )    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SET @Output = 'Operation Failed!'    
    
          INSERT INTO #tempRequestLeaveDays    
          EXEC [Searchrequestedleaves]    
            @FromDate,    
            @ToDate,    
            @DomainID,    
            @SessionID    
    
          IF( @IsDeleted != 0 )    
            BEGIN    
                IF NOT EXISTS (SELECT 1    
                               FROM   tbl_EmployeeLeave    
                               WHERE  ID = @ID    
                                      AND StatusID = (SELECT ID    
                                                      FROM   tbl_Status    
                                                      WHERE  Code = 'Approved'    
                         AND [Type] = 'Leave'    
                                                             AND isdeleted = 0))    
                  BEGIN    
        UPDATE tbl_EmployeeLeave    
                      SET    IsDeleted = @IsDeleted,    
                             ModifiedBy = @SessionID,    
                             ModifiedOn = Getdate()    
                      WHERE  ID = @ID    
    
                      UPDATE tbl_EmployeeLeaveDateMapping    
                      SET    IsDeleted = @IsDeleted,    
                             ModifiedBy = @SessionID,    
                             ModifiedOn = Getdate()    
                      WHERE  LeaveRequestID = @ID    
    
                      SET @Output = 'Leave Request Deleted Successfully.'    
    
                      IF ( @Output LIKE '%Successfully%' )    
                        BEGIN    
                            SET @LeaveTypeID = (SELECT LeaveTypeID    
                                                FROM   tbl_EmployeeLeave    
                                                WHERE  ID = @ID)    
    
                            IF EXISTS (SELECT 1    
                                       FROM   tbl_EmployeeAvailedLeave    
                                       WHERE  LeaveTypeID = @LeaveTypeID    
                                              AND EmployeeID = @EmployeeID    
                                              AND Year = Year(@FromDate))    
                              BEGIN    
                                  SET @IsHalfDay = (SELECT IsHalfDay    
                                                    FROM   tbl_EmployeeLeave    
                                                    WHERE  ID = @ID)    
    
                                  UPDATE tbl_EmployeeAvailedLeave    
                                  SET    AvailedLeave = CASE    
                                                          WHEN Isnull(@IsHalfDay, 'false') = 'false' THEN    
                                                            ( Isnull(AvailedLeave, 0) - Isnull((SELECT Count(1)    
                                                                                                FROM   tbl_EmployeeLeaveDateMapping    
                                                                                                WHERE  LeaveRequestID = @ID), 0) )    
                                                          ELSE    
                                                            ( Isnull(AvailedLeave, 0) - 0.5 )    
                                                        END    
                                  WHERE  LeaveTypeID = @LeaveTypeID    
                                         AND EmployeeID = @EmployeeID    
                                         AND Year = Year(@FromDate)    
                              END    
                        END    
                  END    
                ELSE    
                  SET @Output = 'Cannot Delete Approved Leave Request.'    
            END    
          ELSE    
            BEGIN    
                IF NOT EXISTS (SELECT 1    
                               FROM   tbl_pay_EmployeePayroll    
                               WHERE  IsDeleted = 0    
                                      AND DomainId = @DomainID    
                                      AND EmployeeId = @EmployeeID    
                                      AND ( ( MonthId = Datepart(MM, @FromDate)    
                                              AND YearId = Datepart(YYYY, @FromDate) )    
                                             OR ( MonthId = Datepart(MM, @ToDate)    
                                                  AND YearId = Datepart(YYYY, @ToDate) ) ))    
                  BEGIN    
                      IF NOT EXISTS(SELECT 1    
                                    FROM   tbl_EmployeeLeave    
                                    WHERE  EmployeeID = @EmployeeID    
                                           AND IsDeleted = 0    
                                           AND DomainID = @DomainID    
                                           AND ( StatusID IN (SELECT ID    
                                                              FROM   tbl_Status    
                                                              WHERE  Code IN( 'Approved', 'Pending' )    
                             AND [Type] = 'Leave'    
                                                                     AND IsDeleted = 0) )    
                                           AND ( ( FromDate BETWEEN @FromDate AND @ToDate    
                                                    OR ToDate BETWEEN @FromDate AND @ToDate )    
                                                  OR ( @FromDate BETWEEN FromDate AND ToDate    
                                                        OR @ToDate BETWEEN FromDate AND ToDate ) ))    
                        IF ( @LeaveTypeID = (SELECT ID    
                                             FROM   tbl_LeaveTypes    
                                             WHERE  NAME = 'Permission'    
                                                    AND IsDeleted = 0    
                                                    AND DomainID = @DomainID) )    
                           AND ( NOT EXISTS(SELECT 1    
                                            FROM   tbl_Attendance TAT    
                                                   JOIN tbl_BiometricLogs BL    
                                                     ON BL.AttendanceId = TAT.ID    
                                            WHERE  TAT.EmployeeID = @EmployeeID    
                                                   AND TAT.LogDate = @FromDate    
                                                   AND TAT.IsDeleted = 0    
                                                   AND TAT.DomainID = @DomainID) )    
                          BEGIN    
                              SET @Output = 'Permission cannot be raised. This requires punches on the particular date.'    
                          END    
                        ELSE    
                          BEGIN    
                              INSERT INTO tbl_EmployeeLeave    
                                          (EmployeeID,    
                                           PurposeID,    
                                           LeaveTypeID,    
                                           FromDate,    
                                           ToDate,    
                                           Duration,    
                                           Reason,    
                                           RequesterRemarks,    
                                           AlternativeContactNo,    
                                           ApproverID,    
                                           RequestedDate,    
                                           StatusID,    
                                           ApproverStatusID,    
                                           DomainID,    
                                           CreatedBy,    
                                           ModifiedBy,    
                                           IsHalfDay,    
                                           ApprovedDate,    
             ReplacementEmployeeID)    
                              VALUES      (@EmployeeID,    
                                           @PurposeID,    
                                           @LeaveTypeID,    
                                           @FromDate,    
                                           @ToDate,    
                                           @Duration,    
                                           @Reason,    
                                           @RequesterRemarks,    
                                           @AlternativeContactNo,    
                                           @ApproverID,    
                                           Getdate(),    
                                           @LeaveStatusID,    
                                           @LeaveStatusID,    
                                         @DomainID,    
                                           @SessionID,    
                                           @SessionID,    
                                           @IsHalfDay,    
                                           NULL ,    
             @ReplacementEmployeeID)    
    
                              SET @LeaveManagementID = Ident_current('tbl_EmployeeLeave');      
                              WITH CTE    
                                   AS (SELECT @FromDate     AS All_Month_Date,    
                                              Datename(dw, @FromDate) AS [days]    
                                       UNION ALL    
                                       SELECT Dateadd(DD, 1, All_Month_Date)               AS All_Month_Date,    
                                              Datename(dw, Dateadd(DD, 1, All_Month_Date)) AS [days]    
                                       FROM   CTE    
                                       WHERE  Dateadd(DD, 1, All_Month_Date) <= @ToDate)    
                              SELECT *    
                              INTO   #tempDates    
                              FROM   CTE    
    
                              IF EXISTS (SELECT [days]    
                                         FROM   #tempDates    
                                         WHERE  [days] IN ( 'Sunday', 'Saturday' ))    
                                BEGIN    
                                    IF( @AppConfigValue = 1 )    
                                      BEGIN    
                                          INSERT INTO tbl_EmployeeLeaveDateMapping    
                                                      (LeaveRequestID,    
                                                       EmployeeID,    
                                                       LeaveDate,    
                                                       DomainID,    
                                                       CreatedBy,    
                                                       ModifiedBy)    
                                          SELECT @LeaveManagementID,    
                                                 @EmployeeID,    
                                                 All_Month_Date,    
                                                 @DomainID,    
                                                 @SessionID,    
                                                 @SessionID    
                                          FROM   #tempDates    
                                          WHERE  All_Month_Date NOT IN (SELECT [Date]    
                                                                        FROM   tbl_Holidays    
                                                                        WHERE  [Type] = 'Yearly'    
                                                                               AND IsDeleted = 0    
                                                                               AND [Year] = Year(Getdate())    
                                                                               AND DomainID = @DomainID    
                                                                               AND RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%')    
                                      END    
                                    ELSE    
                                      BEGIN    
                                          INSERT INTO tbl_EmployeeLeaveDateMapping    
                                                      (LeaveRequestID,    
                                                       EmployeeID,    
                                                       LeaveDate,    
                                                       DomainID,    
                                                       CreatedBy,    
                                                       ModifiedBy)    
                                          SELECT @LeaveManagementID,    
                    @EmployeeID,    
                                                 All_Month_Date,    
                                                 @DomainID,    
                                                 @SessionID,    
                                                 @SessionID    
                                          FROM   #tempDates    
                                          WHERE  All_Month_Date NOT IN (SELECT [Date]    
                                                                        FROM   tbl_Holidays    
                                                                        WHERE  --[Type] = 'Yearly' AND      
                                                                         IsDeleted = 0    
                                                                         AND [Year] = Year(Getdate())    
                                                                         AND DomainID = @DomainID    
                                                                         AND RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%')    
                                      END    
                                END    
                              ELSE    
                                BEGIN    
                                    INSERT INTO tbl_EmployeeLeaveDateMapping    
                                                (LeaveRequestID,    
                                                 EmployeeID,    
                                                 LeaveDate,    
                                                 DomainID,    
                                                 CreatedBy,    
                                                 ModifiedBy)    
                                    SELECT @LeaveManagementID,    
                                           @EmployeeID,    
                                           All_Month_Date,    
                                           @DomainID,    
                                           @SessionID,    
                                           @SessionID    
                                    FROM   #tempDates    
                                    WHERE  All_Month_Date NOT IN (SELECT [Date]    
                                                                  FROM   tbl_holidays    
                                                                  WHERE  IsDeleted = 0    
                                                                         AND [Year] = Year(Getdate())    
                                                                         AND DomainID = @DomainID    
                                                                         AND RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%')    
                                END    
    
                              SET @Output = 'Leave Request Created Successfully.,'    
                                            + CONVERT(VARCHAR, @LeaveManagementID)    
    
                              IF( @Output LIKE '%Successfully%' )    
                                BEGIN    
                                    IF EXISTS(SELECT 1    
                                              FROM   tbl_EmployeeAvailedLeave    
                                              WHERE  LeaveTypeID = @LeaveTypeID    
                                                     AND EmployeeID = @SessionID    
                                                     AND Year = Year(@FromDate))    
                                      BEGIN    
                                          UPDATE tbl_EmployeeAvailedLeave    
                                          SET    AvailedLeave = CASE    
                                                                  WHEN Isnull(@IsHalfDay, 'false') = 'false' THEN    
                                                                    ( Isnull(AvailedLeave, 0)    
                                                                      + Isnull((SELECT Count(1) FROM #tempRequestLeaveDays), 0) )    
                                                                  ELSE    
                                                                    ( Isnull(AvailedLeave, 0) + 0.5 )    
                                                                END,    
                                                 ModifiedBy = @SessionID,    
                                                 ModifiedOn = Getdate()    
                                          WHERE  EmployeeID = @SessionID    
                                                 AND LeaveTypeID = @LeaveTypeID    
                                                 AND Year = Year(@FromDate)    
                                  END    
                                END    
   END    
                      ELSE    
                        SET @Output = 'Already Exists.'    
                  END    
                ELSE    
                  SET @Output = 'Leave cannot be created as the Payroll is already processed.'    
            END    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
