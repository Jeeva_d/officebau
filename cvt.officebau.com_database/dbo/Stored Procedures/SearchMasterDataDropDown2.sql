﻿/****************************************************************************                     
CREATED BY   : Jeeva                    
CREATED DATE  :                     
MODIFIED BY   : Ajith N                    
MODIFIED DATE  : 16 May 2017                    
 <summary>                            
     [SearchMasterDataDropDown2] 'Company', 1              
 </summary>                                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchMasterDataDropDown2] (@DependantTable VARCHAR(200),  
                                                   @DomainID       INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          DECLARE @sql VARCHAR(MAX)  
  
          SET @DependantTable='tbl' + @DependantTable  
  
          IF( @DependantTable = 'tblrbssubmodule' )  
            BEGIN  
                SET @sql= ( 'Select ID, Submodule as Name From '  
                            + @DependantTable  
                            + ' where ISNULL(Isdeleted, 0) = 0 Order by Name ASC' )  
            END  
          ELSE IF( @DependantTable = 'tblMonth' )  
             OR ( @DependantTable = 'tblCurrency' )  
            BEGIN  
                SET @sql= ( 'Select ID, Name From ' + @DependantTable  
                            + ' where ISNULL(Isdeleted, 0) = 0 Order by Name ASC' )  
            END  
          ELSE IF( @DependantTable = 'tblFinancialYear' )  
            BEGIN  
                SET @sql = ( 'Select ID, FYDescription AS Name From tbl_FinancialYear            
                             where ISNULL(Isdeleted, 0) = 0 AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR)  
                             + 'Order by Name desc' )  
            END  
          ELSE IF( @DependantTable = 'tblBank' )  
            BEGIN  
                SET @sql = ( 'Select ID, DisplayName AS Name From '  
                             + @DependantTable  
                             + ' where ISNULL(IsDeleted, 0) = 0 AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR)  
                             + 'Order by Name ASC' )  
            END  
          ELSE IF( @DependantTable = 'tblProduct_V2' )  
            BEGIN  
                SET @sql = ( 'Select ID, Name AS Name From '  
                             + @DependantTable  
                             + ' where ISNULL(IsDeleted, 0) = 0 AND IsInventroy = 1 AND DomainID = '  
                             + Cast(@DomainID AS VARCHAR)  
                             + 'Order by Name ASC' )  
            END  
          ELSE IF( @DependantTable = 'tblCompany' )  
            BEGIN  
                SET @sql = ( 'Select ID, Name AS Name From tbl_Company   
                             where Isdeleted = 0 AND ISNULL(IsActive, 0) = 0 Order by Name ASC' )  
            END  
            ELSE IF( @DependantTable = 'tblLeaveTypes' )  
            BEGIN  
                SET @sql = ( 'Select ID, Name AS Name From tbl_LeaveTypes   
                             where Isdeleted = 0 AND NAME NOT IN ( ''Permission'', ''On Duty'', ''Comp off'', ''Vacation 1'', ''Vacation 2'')')  
            END 
          ELSE  
            BEGIN  
                SET @sql= ( 'Select ID, Name From ' + @DependantTable  
                            + ' where ISNULL(Isdeleted, 0) = 0 and DomainID='  
                            + CONVERT(VARCHAR(10), @DomainID)  
                            + ' Order by Name ASC' )  
            END  
  
          EXEC(@sql)  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
