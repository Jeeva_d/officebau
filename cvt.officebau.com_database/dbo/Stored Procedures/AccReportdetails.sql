﻿/****************************************************************************               
CREATED BY    :             
CREATED DATE  :             
MODIFIED BY   :          
MODIFIED DATE :        
 <summary>             
       [AccReportdetails]'Ledger',156,'01-Apr-2018','31-Mar-2019'    
 </summary>                                       
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[AccReportdetails] (@Name            VARCHAR(200), @ID int,   
                                          @SearchStartDate VARCHAR(25),    
                                          @SearchEndDate   VARCHAR(25))    
AS    
  BEGIN    
      BEGIN TRY    
          SET @SearchStartDate = Cast(@SearchStartDate AS DATETIME)    
          SET @SearchEndDate = Cast(@SearchEndDate AS DATETIME);    
    
         IF(@Name='Ledger')  
    Begin   
       Select g.Name As NAME,l.Name As Ledger,0 AS credit,(ed.Amount*Qty) AS Debit,date from tblExpenseDetails ed  
    Join tblExpense e on ed.ExpenseID =e.ID  
    Join tblLedger l on l.ID =@ID  
    Join tblGroupLedger G on G.ID =l.GroupID  
    Where ed.IsLedger=1 and LedgerID=@ID and ed.IsDeleted=0  
    AND [Date] >= @SearchStartDate    
             AND [Date] <= @SearchEndDate    
    Union All  
      Select g.Name,l.Name,0,(ed.Amount*Qty),date from tblExpenseDetails ed  
     Join tblExpense e on ed.ExpenseID =e.ID  
     Join tblLedger l on l.ID =@ID  
     Join tblGroupLedger G on G.ID =l.GroupID   
     Where ed.IsLedger=0 and LedgerID IN (Select ID from tblProduct_V2 where PurchaseLedgerId=@ID and IsDeleted =0 )  
     AND [Date] >= @SearchStartDate    
     AND [Date] <= @SearchEndDate  and ed.IsDeleted=0   
   Union All   
       Select  g.Name,l.Name,(it.Rate*Qty),0,date from tblinvoiceitem it  
    Join tblinvoice i on it.invoiceid=i.Id  
    Join tblLedger l on l.ID =@ID  
     Join tblGroupLedger G on G.ID =l.GroupID   
     Where ProductID IN  (Select ID from tblProduct_V2 where SalesLedgerId=@ID and IsDeleted =0 )  
      AND [Date] >= @SearchStartDate    
     AND [Date] <= @SearchEndDate  and it.IsDeleted=0   
   --Union All   
   --   Select  g.Name,l.Name, Case When ((Select id from tblMasterTypes where Name='Receive Payment') =TransactionType) Then          
   -- Amount Else 0 End ,Case When ((Select id from tblMasterTypes where Name='Receive Payment') =TransactionType) Then          
   -- 0 Else Amount End,date from tblReceipts r  
   --     Join tblLedger l on l.ID =@ID  
   --  Join tblGroupLedger G on G.ID =l.GroupID   
   --   Where LedgerID =@ID  AND [Date] >= @SearchStartDate    
   --  AND [Date] <= @SearchEndDate  and r.IsDeleted=0   
    END  
    IF (@Name = 'Vendor')  
       Begin  
     Select 'Account Payable' As NAME,v.Name As Ledger,0  AS credit,(Select (SUM(AMOUNT*QTY)+Sum(SGST+SGST))  from  tblExpenseDetails where IsDeleted=0 and ExpenseID=e.id) AS Debit  
    ,Date from tblExpense e   
     Join tblVendor v on v.ID=e.VendorID  
     where VendorID=@ID   
     AND [Date] >= @SearchStartDate    
     AND [Date] <= @SearchEndDate  And e.IsDeleted=0  
    Union All   
     Select  'Account Payable',v.Name,epm.Amount,0,Date from tblExpensePaymentMapping epm  
     Join tblExpense e on epm.ExpenseID=e.ID and e.IsDeleted=0  
     Join tblVendor v on e.VendorID = v.ID  
     Where [Date] >= @SearchStartDate    
     AND [Date] <= @SearchEndDate  and epm.IsDeleted=0   
    End  
    IF (@Name = 'Customer')  
       Begin  
     Select 'Accounts Receivable' As NAME,v.Name As Ledger,0  AS Debit,(Select (SUM(Rate*QTY)+Sum(SGSTAmount+CGSTAmount))  from  tblInvoiceItem where IsDeleted=0 and InvoiceID=e.id) AS credit   
    ,Date from tblinvoice e   
   Join tblcustomer v on e.customerId = v.ID  
     where CustomerID=@ID   
     AND [Date] >= @SearchStartDate    
     AND [Date] <= @SearchEndDate  And e.IsDeleted=0  
    Union All   
     Select  'Accounts Receivable',v.Name,0,epm.Amount,Date from tblinvoicepaymentmapping epm  
     Join tblinvoice e on epm.InvoiceID=e.ID and e.IsDeleted=0  
     Join tblcustomer v on e.customerId = v.ID  
     Where [Date] >= @SearchStartDate    
     AND [Date] <= @SearchEndDate  and epm.IsDeleted=0   
    End  
  
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
