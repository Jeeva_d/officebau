﻿  
/****************************************************************************     
CREATED BY   : Ajith N  
CREATED DATE  :   31 Jul 2017  
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>   
          [SearchCompleteLoanList] 4, '', '', '', '', '', 0, null  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchCompleteLoanList] (@DomainID       INT,  
                                                @StatusID       INT,  
                                                @FromDate       DATETIME,  
                                                @ToDate         DATETIME,  
                                                @BusinessUnitID INT,  
                                                @ApproverID     INT,  
                                                @EmployeeID     INT,  
                                                @LoanTypeID     INT)  
AS  
  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN  
              SELECT LR.ID                                                AS ID,  
                     Isnull(EM.EmpCodePattern, '') + EM.Code                                              AS EmployeeCode,  
                     EM.FullName                                         AS FullName,  
                     EM.ID                                                AS EmployeeID,  
                     LR.CreatedOn                                         AS RequestedDate,  
                     --LR.TotalAmount                               AS Amount,  
                     --TS.Code                          AS [Status],  
                     CASE  
                       WHEN LR.LoanTypeID = 1 THEN  
                         'Soft Loan'  
                       ELSE  
                         'Salary Advance'  
                     END                                                  AS [Type],  
                     LR.TotalAmount                                       AS TotalAmount,  
                     CASE  
                       WHEN FC.LoanRequestID IS NOT NULL THEN  
                         (SELECT Sum(ISNULL(Amount, 0))  
                                 + Sum(ISNULL(InterestAmount, 0))  
                          FROM   tbl_LoanAmortization  
                          WHERE  LoanRequestID = LR.ID  
                                 AND IsDeleted = 0  
                                 AND DomainID = @DomainID  
                                 AND StatusID IN (SELECT ID  
                                                  FROM   tbl_Status  
                                                  WHERE  [Type] = 'Amortization'  
                                                         AND Code != 'Open'))  
                       ELSE  
                         LR.TotalAmount  
                     END                                                  AS Amount,  
                     --LR.TotalAmount      AS Amount,  
                     CASE  
                       WHEN FC.LoanRequestID IS NOT NULL THEN  
                         (SELECT id  
                          FROM   tbl_Status  
                          WHERE  IsDeleted = 0  
                                 AND Type = 'Amortization'  
                                 AND Code = 'Foreclosed')  
                       ELSE  
                         LR.StatusID  
                     END                                                  AS StatusID,  
                     BU.NAME                                              AS BusinessUnit,  
                     Isnull(EMA.EmpCodePattern, '') + EMA.Code + ' - ' + EMA.FullName                     AS Approver,  
                      Isnull(EMF.EmpCodePattern, '') + EMF.Code + ' - ' + EMF.FullName                     AS FinApproverName,  
                     -- RS.Code                           
                     CASE  
                       WHEN ((SELECT RS.Code  
                              FROM   tbl_LoanAmortization TLA  
                                     LEFT JOIN tbl_Status RS  
                                            ON RS.ID = TLA.StatusID  
                              WHERE  TLA.LoanRequestID = LR.ID  
                              FOR xml path(''))) LIKE '%foreclosed%' THEN  
                         'Foreclosed'  
                       ELSE  
                         ( CASE  
                             WHEN ((SELECT RS.Code  
                                    FROM   tbl_LoanAmortization TLA  
                                           LEFT JOIN tbl_Status RS  
                                                  ON RS.ID = LR.StatusID  
                                    WHERE  TLA.LoanRequestID = LR.ID  
                                    FOR xml path(''))) LIKE '%closed%' THEN  
                               'Closed'  
                             ELSE  
                               ( CASE  
                                   WHEN ((SELECT RS.Code  
                                          FROM   tbl_LoanAmortization TLA  
                                                 LEFT JOIN tbl_Status RS  
                                                        ON RS.ID = TLA.StatusID  
                                          WHERE  TLA.LoanRequestID = LR.ID  
                                                 AND ( ISNULL(TLA.DueBalance, 0) = 0  
                                                        OR ISNULL(TLA.PaidAmount, 0) <> 0 )  
                                          FOR xml path(''))) LIKE '%payroll%' THEN  
                                     'Payroll'  
                                   ELSE  
                                     'Open'  
                                 END )  
                           END )  
                     END                                                  AS RepaymentStatus,  
                     AST.Code                                             AS ApproverStatus,  
                     FST.Code                                             AS FinApproverStatus,  
                     Dep.NAME                                             AS DepartmentName,  
                     Cast(ROW_NUMBER()  
                            OVER(  
                              ORDER BY LR.CreatedOn DESC) AS INT)         AS RowNo,  
                     (SELECT Sum(ISNULL(PaidAmount, 0))  
                      FROM   tbl_LoanAmortization  
                      WHERE  LoanRequestID = LR.ID  
                             AND IsDeleted = 0  
                             AND StatusID IN (SELECT ID  
                                              FROM   tbl_Status  
                                              WHERE  [Type] = 'Amortization'  
                                                     AND Code != 'Open')) AS PaidAmount  
              INTO   #tempLoanList  
              FROM   tbl_LoanRequest LR  
                     LEFT JOIN tbl_LoanForeclosure FC  
                            ON FC.LoanRequestID = LR.ID  
                     LEFT JOIN tbl_EmployeeMaster EM  
                            ON EM.ID = LR.EmployeeID  
                     LEFT JOIN tbl_BusinessUnit BU  
                            ON BU.ID = LR.BusinessUnitID  
                     --LEFT JOIN tbl_Status TS  
                     --       ON TS.ID = LR.StatusID  
                     LEFT JOIN tbl_EmployeeMaster EMA  
                            ON EMA.ID = LR.ApproverID  
                     LEFT JOIN tbl_EmployeeMaster EMF  
                            ON EMF.ID = LR.FinancialApproverID  
                     LEFT JOIN tbl_Status AST  
                            ON AST.ID = LR.ApproverStatusID  
                     LEFT JOIN tbl_Status FST  
                            ON FST.ID = LR.FinancialStatusID  
                     LEFT JOIN tbl_Department Dep  
                            ON Dep.ID = EM.DepartmentID  
              WHERE  LR.IsDeleted = 0  
                     AND LR.DomainID = @DomainID  
                     AND ( ISNULL(@BusinessUnitID, 0) = 0  
                            OR LR.BusinessUnitID = @BusinessUnitID )  
                     --AND ( ISNULL(@StatusID, 0) = 0  
  --       OR LR.StatusID = @StatusID )  
                     AND ( ISNULL(@ApproverID, 0) = 0  
                            OR LR.FinancialApproverID = @ApproverID )  
                     AND ( ISNULL(@FromDate, '') = ''  
                            OR LR.CreatedOn >= @FromDate )  
                     AND ( ISNULL(@FromDate, '') = ''  
                            OR LR.CreatedOn <= @ToDate )  
                     AND ( Isnull(@EmployeeID, 0) = 0  
                            OR ( EM.ID = @EmployeeID ) )  
                     AND ( @LoanTypeID IS NULL  
                            OR ( LR.LoanTypeID = @LoanTypeID ) )  
  
              SELECT TT.*,  
                     TS.Code AS [Status],  
     CASE WHEN ( (SELECT Round(Sum(totalamount), 0)  
                           FROM   tbl_LoanRequest  
                           WHERE  ID = TT.ID) <= (SELECT Round(Sum(PaidAmount), 0) + count(1)  
                                                   FROM   tbl_LoanAmortization  
                                                   WHERE  LoanRequestID = TT.ID) ) THEN ''  
     ELSE  
                     CASE  
                       WHEN TS.Code = 'Settled' THEN  
                         ( Round(tt.TotalAmount, 0) - ( CASE  
                                                          WHEN ( TT.RepaymentStatus = 'Payroll' ) THEN  
                                                            (SELECT Sum(ISNULL(PaidAmount, 0))  
                                                             FROM   tbl_LoanAmortization LA  
                                                             WHERE  LA.LoanRequestID = TT.ID  
                                                                    AND LA.IsDeleted = 0  
                                                                    AND StatusID IN (SELECT id  
                                                                                     FROM   tbl_Status  
                                                                                     WHERE  [Type] = 'Amortization'  
                                                                                            AND Code != 'Open')  
                                                                    AND ( ISNULL(LA.DueBalance, 0) = 0  
                                                                           OR ISNULL(LA.PaidAmount, 0) <> 0 ))  
                                                          ELSE  
                                                            ISNULL((SELECT Sum(ISNULL(Amount, 0))  
                                                                           + Sum(ISNULL(InterestAmount, 0))  
                                                                    FROM   tbl_LoanAmortization LA  
                                                                    WHERE  LA.LoanRequestID = TT.ID  
                                                                           AND StatusID IN (SELECT id  
                                                                                            FROM   tbl_Status  
                                                                                            WHERE  [Type] = 'Amortization'  
                                                                                                   AND Code != 'Open')  
                                                                           AND ( ISNULL(LA.DueBalance, 0) = 0  
                                                                                  OR ISNULL(LA.PaidAmount, 0) <> 0 )), 0)  
                                                        END ) )  
                       ELSE  
                         ''  
                     END  END   AS OutstandingAmount  
              FROM   #tempLoanList TT  
                     LEFT JOIN tbl_Status TS  
                            ON TS.ID = TT.StatusID  
              WHERE  ( ISNULL(@StatusID, 0) = 0  
                        OR StatusID = @StatusID )  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg   VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
