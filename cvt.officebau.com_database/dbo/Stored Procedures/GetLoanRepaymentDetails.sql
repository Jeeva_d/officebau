﻿
/****************************************************************************   
CREATED BY   : Ajith N
CREATED DATE  :   31 Jul 2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          [GetLoanRepaymentDetails] 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetLoanRepaymentDetails] (@DomainID      INT,
                                                 @LoanRequestID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT case When (ST.Code = 'Payroll') Then LA.PaidAmount
		  ELSE  ( LA.Amount + LA.InterestAmount ) END AS Amount,
                 LA.DueDate                        AS DueDate,
                 LA.ModifiedOn                     AS PaidDate,
				 ST.Code                           AS [Status]                      
          FROM   tbl_LoanAmortization LA
		         LEFT JOIN tbl_Status ST
				 ON LA.StatusID = ST.ID
          WHERE  LA.IsDeleted = 0
                 AND LA.LoanRequestID = @LoanRequestID
                 AND DomainID = @DomainID
                 AND StatusID IN (SELECT id
                                  FROM   tbl_Status
                                  WHERE  [Type] = 'Amortization'
                                         AND ( Code = 'Payroll'
                                                OR Code = 'Foreclosed' ))
				 AND (ISNULL(LA.DueBalance,0) = 0 
						OR ISNULL(LA.PaidAmount,0) <> 0 )

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
