﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	06-Jun-2017
MODIFIED BY			:	Ajith N
MODIFIED DATE		:	8 Dec 2017
 <summary>        
     [ResetPassword] 'CMTL009','012564'
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ResetPassword] (@UserName VARCHAR(100),
                                       @GUID     VARCHAR(10))
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @UserMessage VARCHAR(1000),
              @UserID      INT=0,
              @DomainID    INT

      BEGIN TRY
          BEGIN TRANSACTION

          --SET @UserName= CASE
          --                            WHEN( Substring(@UserName, 1, 3) = 'PNP' ) THEN( 'PAN' + Substring(@UserName, 4, 4) )
          --                            WHEN ( Substring(@UserName, 1, 3) = 'AHD' ) THEN( 'AHM' + Substring(@UserName, 4, 4) )
          --                            ELSE @UserName
          --                          END
          IF (SELECT COUNT(1)
              FROM   tbl_EmployeeMaster
              WHERE  ISNULL(IsActive, 0) = 0
                     AND IsDeleted = 0
                     AND LoginCode = @UserName) = 0
            BEGIN
                SET @UserMessage = 'Please check your credential/'

                GOTO OutputResult
            END

          BEGIN
              SET @UserID = (SELECT ID
                             FROM   tbl_EmployeeMaster
                             WHERE  ISNULL(IsActive, 0) = 0
                                    AND IsDeleted = 0
                                    AND LoginCode = @UserName)

              UPDATE tbl_PasswordReset
              SET    IsExpired = 1
              WHERE  RequestedOn < ( DATEADD(n, -30, GETDATE()) )

              IF(SELECT COUNT(1)
                 FROM   tbl_PasswordReset
                 WHERE  IsExpired = 0
                        AND EmployeeID = @UserID) = 0
                BEGIN
                    INSERT dbo.tbl_PasswordReset
                           (EmployeeID,
                            GUID,
                            RequestedOn)
                    VALUES (@UserID,
                            @GUID,
                            GETDATE())

                    SET @UserMessage = 'Valid' + '/'
                                       + (SELECT GUID
                                          FROM   tbl_PasswordReset
                                          WHERE  EmployeeID = @UserID
                                                 AND IsExpired = 0)
                                       + '/'
                                       + CAST((SELECT DomainID 
												FROM tbl_EmployeeMaster
												 WHERE ISNULL(IsActive, 0) = 0
													AND IsDeleted = 0 
													AND LoginCode = @UserName) AS VARCHAR)
                                       + '/'
                                       + (SELECT Isnull(EmailID, NULL)
                                          FROM   tbl_EmployeeMaster
                                          WHERE   ISNULL(IsActive, 0) = 0
                                                 AND IsDeleted = 0
                                                 AND LoginCode = @UserName)
                                       + '/'
          + (SELECT FirstName + ' ' + ISNULL(LastName, '')
                                          FROM   tbl_EmployeeMaster
                                          WHERE ISNULL(IsActive, 0) = 0
                                                 AND IsDeleted = 0
                                                 AND LoginCode = @UserName)--'Valid/'

                    GOTO OutputResult
                END
              ELSE
                SET @UserMessage = 'Valid' + '/'
                                   + (SELECT GUID
                                      FROM   tbl_PasswordReset
                                      WHERE  EmployeeID = @UserID
                                             AND IsExpired = 0)
                                   + '/'
                                   + CAST((SELECT DomainID 
											FROM tbl_EmployeeMaster
											 WHERE ISNULL(IsActive, 0) = 0
												AND IsDeleted = 0 
												AND LoginCode = @UserName) AS VARCHAR)
                                   + '/'
                                   + (SELECT Isnull(EmailID, NULL)
                                      FROM   tbl_EmployeeMaster
                                      WHERE   ISNULL(IsActive, 0) = 0
                                             AND IsDeleted = 0
                                             AND LoginCode = @UserName)
                                   + '/'
                                   + (SELECT FirstName + ' ' + ISNULL(LastName, '')
                                      FROM   tbl_EmployeeMaster
                                      WHERE  ISNULL(IsActive, 0) = 0
                                             AND IsDeleted = 0
                                             AND LoginCode = @UserName)--'Valid/'

              GOTO OutputResult
          END

          OUTPUTRESULT:

          SELECT @UserMessage AS UserMessage

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
