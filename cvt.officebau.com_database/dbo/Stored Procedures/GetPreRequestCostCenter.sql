﻿/****************************************************************************   
CREATED BY   : Jeeva  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [Getproduct] Null,1,1  
 [GetPreRequestProduct] 1,1
 [GetPreRequestCostCenter] 'Expense',1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetPreRequestCostCenter] (@Type     VARCHAR(1000),
                                                 @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT pd.ID   AS ID,
                 pd.NAME AS NAME
          FROM   tblCostCenter pd
                 JOIN tbl_CodeMaster cd
                   ON cd.ID = pd.CostCenterTypeID
          WHERE  pd.DomainID = @DomainID
                 AND pd.CostCenterTypeID IN (SELECT ID
                                             FROM   tbl_CodeMaster
                                             WHERE  code IN ( @Type, 'Both' ))
                 AND pd.IsDeleted = 0
          ORDER  BY pd.NAME ASC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
