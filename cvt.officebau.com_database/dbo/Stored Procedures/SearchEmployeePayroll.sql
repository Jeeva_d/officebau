﻿/****************************************************************************     
CREATED BY      : Naneeshwar.M    
CREATED DATE  :     
MODIFIED BY   :   Ajith N  
MODIFIED DATE  :   06 April 2018  
<summary>      
 [Searchemployeepayroll] 3,3,4,'13',null,null,0  
</summary>                             
*****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchEmployeePayroll] (@MonthId      INT,  
                                               @Year         INT,  
                                               @DomainID     INT,  
                                               @Location     VARCHAR(100),  
                                               @DepartmentID INT,  
                                               @StatusID     INT,  
                                               @Proceed      INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @TABLEQURY VARCHAR(max)=''  
      DECLARE @TDSYear INT  
  
      -- Get Payroll Component Config Value  
      SELECT *  
      INTO   #tmpParollConfigValue  
      FROM   [dbo].[Fn_PayrollComponentConfigValue]()  
  
      IF( @MonthID BETWEEN 1 AND 3 )  
        SET @TDSYear=( (SELECT FY.NAME  
                        FROM   tbl_FinancialYear FY  
                        WHERE  FY.NAME = (SELECT NAME  
                                          FROM   tbl_FinancialYear  
                                          WHERE  isdeleted = 0  
                                                 AND id = @Year  
                                                 AND DomainID = @DomainID)  
                               AND FY.IsDeleted = 0  
                               AND FY.DomainID = @DomainID) - 1 )  
      ELSE  
        SET @TDSYear =(SELECT FY.NAME  
                       FROM   tbl_FinancialYear FY  
                       WHERE  FY.NAME = (SELECT NAME  
                                         FROM   tbl_FinancialYear  
                                         WHERE  isdeleted = 0  
                                                AND id = @Year  
                                                AND DomainID = @DomainID)  
                              AND FY.IsDeleted = 0  
                              AND FY.DomainID = @DomainID)  
  
      SET @Year = (SELECT NAME  
                   FROM   tbl_FinancialYear  
                   WHERE  id = @Year  
                          AND IsDeleted = 0  
                          AND DomainID = @DomainID)  
  
      DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'  
        + CONVERT(VARCHAR(10), @Year)  
      DECLARE @DataSource TABLE  
        (  
           ID      INT IDENTITY(1, 1),  
           [Value] NVARCHAR(128)  
        )  
  
      INSERT INTO @DataSource  
                  ([Value])  
      SELECT Item  
      FROM   dbo.Splitstring (@Location, ',')  
      WHERE  Isnull(Item, '') <> ''  
  
      DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth));  
  
      SET @TABLEQURY ='SELECT 1 FROM tbl_TDS where monthID = '  
                      + Cast (@MonthId AS VARCHAR(10))  
                      + ' and isdeleted=0  and yearID = '  
                      + Cast ((SELECT ID FROM tbl_FinancialYear WHERE NAME = @TDSYear AND isdeleted = 0 AND DomainID = @DomainID) AS VARCHAR(10))  
                      + ' and DomainID = '  
                      + Cast (@DomainID AS VARCHAR(10))  
                      + '     
        and baselocationid IN (SELECT Item FROM dbo.Splitstring ('''  
                      + Cast(@Location AS VARCHAR(50))  
                      + ''', '','')    
       WHERE  Isnull(Item, '''') <> '''')    
        AND (SELECT COUNT(Item) FROM   dbo.Splitstring ('''  
                      + Cast(@Location AS VARCHAR(50))  
                      + ''', '','')    
        WHERE  Isnull(Item, '''') <> '''')     
       = (SELECT COUNT(DISTINCT baselocationid) FROM tbl_TDS     
        where monthID = '  
                      + Cast( @MonthId AS VARCHAR(50))  
              + '  and yearID = '  
                      + Cast( (SELECT ID FROM tbl_FinancialYear WHERE NAME = @TDSYear AND isdeleted = 0 AND DomainID = @DomainID) AS VARCHAR(50))  
                      + ' and isdeleted=0 and baselocationid IN (SELECT Item    
      FROM   dbo.Splitstring ('''  
                      + Cast(@Location AS VARCHAR(50))  
                      + ''', '','')    
      WHERE  Isnull(Item, '''') <> '''')) '  
  
      DECLARE @query VARCHAR(max)=( ' IF EXISTS(' + @TABLEQURY  
         + ')    
         select ''1''    
          ELSE    
         select  ''2''' )  
      DECLARE @RESULT TABLE  
        (  
           RESULT VARCHAR(2)  
        )  
  
      INSERT INTO @RESULT  
      EXEC(@query)  
  
      BEGIN TRY  
          BEGIN  
              IF( (SELECT RESULT  
                   FROM   @RESULT) = 1  
                   OR @Proceed = 1 )  
                BEGIN  
                    SELECT Row_number()  
                             OVER (  
                               PARTITION BY ep.EmployeeId  
                               ORDER BY EffectiveFrom DESC)                                                                                               AS ORDERNUMBER,  
                           ( CASE  
                               WHEN ( Datepart(MM, DOJ) = @MonthId  
                                      AND Datepart(YYYY, DOJ) = @year ) THEN  
                                 ( @Workdays - ( Datepart(D, DOJ) - 1 ) - Isnull(LopDays, 0) )  
                               ELSE  
                                 @Workdays - Isnull(LopDays, 0)  
                             END ) - ( CASE  
                                         WHEN ( Datepart(MM, InactiveFrom) = @MonthId  
                                                AND Datepart(YYYY, InactiveFrom) = @year ) THEN  
                                           ( ( @Workdays ) - Day(InactiveFrom) )  
                                         ELSE  
                                           0  
                                       END )                                                                                                              AS Workdays,  
                           @Workdays                                                                                                                      AS ActualWorkdays,  
                           Isnull(Isnull(LA.Amount, 0) + ( CASE  
                                                             WHEN Isnull(LA.InterestAmount, 0) = 0 THEN  
                                                               0  
                                                             ELSE  
                                                               Isnull(LA.InterestAmount, 0)  
                                                           END ) + Isnull((SELECT ob.DueBalance  
                                                                           FROM   tbl_LoanAmortization ob  
                                                                           WHERE  ob.LoanRequestID = LR.ID  
                                                                                  AND ob.DomainID = @DomainID  
                                                                                  AND Month(ob.DueDate) = Month(Dateadd(month, -1, @CurrentMonth))  
                                                                                  AND Year(ob.DueDate) = Year(Dateadd(month, -1, @CurrentMonth))), 0), 0) AS LoanAmount,  
                           ISNULL(EMP.EmpCodePattern, '')+ '' + EMP.Code                                                                                                                       AS Code,  
                           EMP.FirstName + ' ' + Isnull(EMP.LastName, '')                                                                                 AS EmployeeName,  
                           EMP.DOJ                                                                                                                        AS DOJ,  
                           EMP.BaselocationID                                                                                                             AS BaselocationID,  
                           EMP.RegionID                                                                                                                   AS RegionID,  
                           BU.NAME                                                                                                                        AS BusinessUnit,  
                           DM.NAME                                                                                                                        AS DepartmentName,  
                           CASE  
                             WHEN TDS.TDSAmount < 0 THEN  
                               0  
                             ELSE  
                               TDS.TDSAmount  
                           END                                                                                                                            AS TDSAmount,  
                           Isnull(EPD.DateOfBirth, '')                                                                                                    AS DOB,  
                           Isnull(ESD.AccountNo, '')                                                                                                      AS AccountNo,  
                           Isnull(ESD.IFSCCode, '')                                                                                                       AS IFSCCode,  
                           Isnull(ESD.PAN_No, '')                                                                                                         AS PANNo,  
                           Isnull(ESD.BeneID, '')                                                                                                         AS BeneID,  
                           --ep.*,  
                           ep.Id,  
                           ep.CompanyPayStubId,  
                           ep.EmployeeId,  
                           ep.Gross,  
                           ep.Basic,  
                           ep.HRA,  
                           ep.MedicalAllowance,  
                           ep.Conveyance,  
                           ep.SplAllow,  
                           dbo.Fn_getComponentConfigValue(ep.EEPF, 'EEPF', emp.BaseLocationID, @DomainID)                                                 AS EEPF,  
                           dbo.Fn_getComponentConfigValue(ep.EEESI, 'EEESI', emp.BaseLocationID, @DomainID)                                               AS EEESI,  
                           dbo.Fn_getComponentConfigValue(ep.PT, 'PT', emp.BaseLocationID, @DomainID)                                                     AS PT,  
                           dbo.Fn_getComponentConfigValue(ep.TDS, 'TDS', emp.BaseLocationID, @DomainID)                                                   AS TDS,  
                           dbo.Fn_getComponentConfigValue(ep.ERPF, 'ERPF', emp.BaseLocationID, @DomainID)                                                 AS ERPF,  
                           dbo.Fn_getComponentConfigValue(ep.ERESI, 'ERESI', emp.BaseLocationID, @DomainID)                                               AS ERESI,  
                           ep.Bonus,  
                           dbo.Fn_getComponentConfigValue(ep.Gratuity, 'Gratuity', emp.BaseLocationID, @DomainID)                                         AS Gratuity,  
                           dbo.Fn_getComponentConfigValue(ep.PLI, 'PLI', emp.BaseLocationID, @DomainID)                                                   AS PLI,  
                           dbo.Fn_getComponentConfigValue(ep.Mediclaim, 'Mediclaim', emp.BaseLocationID, @DomainID)                                       AS Mediclaim,  
                           dbo.Fn_getComponentConfigValue(ep.MobileDataCard, 'MobileDataCard', emp.BaseLocationID, @DomainID)                             AS MobileDataCard,  
                           ep.EffectiveFrom,  
                           ep.CreatedBy,  
                           ep.CreatedOn,  
                           ep.ModifiedBy,  
                           ep.ModifiedOn,  
                           ep.DomainId,  
                           ep.IsDeleted,  
                           dbo.Fn_getComponentConfigValue(ep.OtherPerks, 'OtherPerks', emp.BaseLocationID, @DomainID)                                     AS OtherPerks,  
                           ep.CTC,  
                           ep.IsPFRequired,  
                           ep.IsESIRequired,  
                           dbo.Fn_getComponentConfigValue(ep.EducationAllow, 'EducationAllow', emp.BaseLocationID, @DomainID)                             AS EducationAllow,  
                           dbo.Fn_getComponentConfigValue(ep.MonsoonAllow, 'OverTime', emp.BaseLocationID, @DomainID)                                     AS MonsoonAllow,  
                           ep.MagazineAllow  
                    INTO   #TableResult  
                    FROM   tbl_EmployeePayStructure ep  
                           LEFT JOIN tbl_lopdetails P  
                                  ON P.EmployeeId = ep.EmployeeId  
                                     AND p.DomainId = ep.DomainId  
                                     AND P.Monthid = @MonthId  
                                     AND P.year = @Year  
                           JOIN tbl_EmployeeMaster emp  
                             ON ep.EmployeeId = emp.ID  
                           LEFT JOIN tbl_LoanRequest LR  
                                  ON LR.EmployeeID = ep.EmployeeId  
                                     AND LR.StatusID = (SELECT ID  
                                                        FROM   tbl_Status  
                                                        WHERE  Code = 'Settled'  
                                                               AND type = 'Loan')  
                           LEFT JOIN tbl_LoanAmortization LA  
                                  ON LA.LoanRequestID = LR.ID  
                                     AND LA.isdeleted = 0  
                                     AND Year(LA.DueDate) = @Year  
                                     AND Month(LA.DueDate) = @MonthId  
                                     AND LA.StatusID = (SELECT ID  
                                                        FROM   tbl_Status  
                                                        WHERE  Code = 'Open'  
                                                               AND type = 'Amortization')  
                           LEFT JOIN tbl_BusinessUnit BU  
                                  ON bu.ID = emp.BaseLocationID  
                           LEFT JOIN tbl_Department DM  
                                  ON DM.id = emp.DepartmentID  
                           LEFT JOIN tbl_TDS TDS  
                                  ON TDS.EmployeeId = ep.EmployeeId  
                                     AND tds.MonthId = @MonthId  
                                     AND tds.YearID = (SELECT ID  
                                                       FROM   tbl_FinancialYear  
                                                       WHERE  isdeleted = 0  
                                                              AND NAME = @TDSYear  
                                                              AND DomainID = @DomainID)  
                                     AND TDS.IsDeleted = 0  
                           LEFT JOIN tbl_EmployeePersonalDetails EPD  
                                  ON EPD.EmployeeID = emp.ID  
                           LEFT JOIN tbl_EmployeeStatutoryDetails ESD  
                                  ON ESD.EmployeeID = emp.ID  
                    WHERE  EffectiveFrom <= Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @Year - 2000, Dateadd(MONTH, @MonthId - 1, '20000101')))  
                                                                      + 1, 0))  
                           AND ep.DomainID = @DomainID  
                           AND ep.EmployeeId NOT IN (SELECT employeeid  
                                                     FROM   tbl_EmployeePayroll  
                                                     WHERE  monthid = @MonthId  
                                                            AND [yearid] = @Year  
                                                            AND isdeleted = 0  
                                                            AND DomainID = @DomainID)  
                           AND ep.IsDeleted = 0  
                           AND ( Isnull(emp.IsActive, '') = 0  
                                  OR ( Month(InactiveFrom) = @MonthId  
                                       AND Year(InactiveFrom) = @Year ) )  
                           AND emp.HasAccess = 1  
                           AND emp.IsDeleted = 0  
                           AND ( ( @Location = Cast(0 AS VARCHAR) )  
                                  OR emp.BaseLocationID IN (SELECT [Value]  
                                                            FROM   @DataSource) )  
                           AND ( emp.BaseLocationID <> 0  
                                  OR emp.BaseLocationID <> '' )  
                           AND ( ( @DepartmentID = 0  
                                    OR @DepartmentID IS NULL )  
                                  OR ( emp.DepartmentID = @DepartmentID ) )  
                           AND emp.EmploymentTypeID = (SELECT ID  
                                                       FROM   tbl_EmployementType  
                                                       WHERE  NAME = 'Direct'  
                                                              AND DomainID = @DomainID);  
  
                    WITH cte  
                         AS (SELECT R.ID                                                                                                                              AS EmployeePaystructureID,  
                                    R.Code                                                                                                                            AS EmployeeCode,  
                                    R.EmployeeName                                                                                                                    AS EmployeeName,  
                                    R.DOJ                                                                                                                             AS DOJ,  
                                    0                                                                                                                                 AS ID,  
                                    R.employeeid                                                                                                                      AS EmployeeID,  
                                    @MonthId                                                                                                                          AS Monthid,  
                                    @Year                                                                                                                             AS YearId,  
                                    Round(( Gross ), 0)                                                                                                               AS FixedGross,  
                                    Cast(R.Workdays AS DECIMAL(18, 2))                                                                                                AS [No of days payable],  
                                    Round(R.basic / R.ActualWorkdays * R.Workdays, 0)                                                                                 AS Basic,  
                                    Round(R.HRA / R.ActualWorkdays * R.Workdays, 0)                                                                                   AS HRA,  
                                    Round(R.MedicalAllowance / R.ActualWorkdays * R.Workdays, 0)                                            AS MedicalAllowance,  
                                    Round(R.Conveyance / R.ActualWorkdays * R.Workdays, 0)                                                                            AS Conveyance,  
                                    Round(( Gross - ( Round(R.Basic, 0)  
                                                      + Round(R.MedicalAllowance, 0)  
                                                      + Round(R.HRA, 0) + Round(R.Conveyance, 0)  
                                                      + Round(Isnull(R.EducationAllow, 0), 0)  
                                                      + Round(Isnull(R.MagazineAllow, 0), 0) ) ) / R.ActualWorkdays * R.Workdays, 0)                                  AS SplAllow,  
                                    Round(Isnull(R.EducationAllow, 0) / R.ActualWorkdays * R.Workdays, 0)                                                             AS EducationAllowance,  
                                    Round(Isnull(R.MagazineAllow, 0) / R.ActualWorkdays * R.Workdays, 0)                                                              AS PaperMagazine,  
                                    Round(Isnull(R.MonsoonAllow, 0), 0)                                                                                               AS MonsoonAllow,  
                                    0                                                                                                                                 AS OverTime,  
                                    0                                                                                                                                 AS [Other Earning],  
                                    Round(Gross / R.ActualWorkdays * R.Workdays, 0)                                                                                   AS [Paid Gross],  
                                    ( CASE  
                                        WHEN dbo.Fn_CheckComponentConfigValue('EEPF', R.BaseLocationID, @DomainID) = 1 THEN  
                                          ( CASE  
                                              WHEN ( R.IsPFRequired = 0 ) THEN  
                                                0  
                                              ELSE  
                                                CASE  
                                                  WHEN dbo.Fn_getpaidgross(Gross, R.basic, R.Conveyance, R.MedicalAllowance, R.HRA, R.EducationAllow, R.MagazineAllow, R.Workdays, R.ActualWorkdays) > PAYEPF.Limit THEN  
                                                    PAYEPF.Value  
                                                  ELSE  
                                                    Round(( ( Gross - ( R.HRA + R.MedicalAllowance ) ) * PAYEPF.Percentage / 100 ) / R.ActualWorkdays * R.Workdays, 0)  
                                                END  
                                            END )  
                                        ELSE  
                                          0  
                                      END )                                                                                                                           AS EEPF,  
                                    ( CASE  
                                        WHEN dbo.Fn_CheckComponentConfigValue('EEESI', R.BaseLocationID, @DomainID) = 1 THEN  
                                          ( CASE  
                                              WHEN ( R.IsESIRequired = 0 ) THEN  
                                                0  
                                              ELSE  
                                                --CASE    
                                                --  WHEN( (Datediff(MONTH, (SELECT TOP 1 EffectiveFrom    
                                                --                         FROM   tbl_EmployeePayStructure e    
                                                --                         WHERE  e.EffectiveFrom < R.EffectiveFrom    
                                                --                                AND EmployeeId = R.EmployeeId    
                                                --                                AND IsDeleted = 0   
                                                --                         ORDER  BY EffectiveFrom DESC), R.EffectiveFrom) < 6 )  ) THEN Ceiling(( Gross * PAYEESI.Percentage / 100 ) / R.ActualWorkdays * R.Workdays / 1)    
                                                --  ELSE    
                                                CASE  
                                                  WHEN ( ( Gross ) >= PAYEESI.Limit ) THEN  
                                                    PAYEESI.Value  
                                                  ELSE  
                                                    Ceiling(( ( Gross ) * PAYEESI.Percentage / 100 ) / R.ActualWorkdays * R.Workdays / 1)  
                                                END  
                                            --END    
                                            END )  
                                        ELSE  
                                          0  
                                      END )                                                                                                                           AS EEESI,  
                                    Round(Isnull(TDSAmount, 0), 0)                                                                                                    AS TDS,  
                                    Round(Isnull(dbo.Fn_getptamount(Gross, @MonthId, @Year, R.DOJ, R.BaselocationID, R.Workdays, R.ActualWorkdays, @DomainID), 0), 0) AS PT,  
                                    R.LoanAmount                                                                                                                      AS Loans,  
                                    0                                                                                                                                 AS [Other Deductions],  
                                    ( CASE  
                                        WHEN dbo.Fn_CheckComponentConfigValue('ERPF', R.BaseLocationID, @DomainID) = 1 THEN  
                                          ( CASE  
                                              WHEN ( R.IsPFRequired = 0 ) THEN  
                                                0  
                                              ELSE  
                                                CASE  
                                                  WHEN dbo.Fn_getpaidgross(Gross, R.basic, R.Conveyance, R.MedicalAllowance, R.HRA, R.EducationAllow, R.MagazineAllow, R.Workdays, R.ActualWorkdays) > PAYERPF.Limit THEN  
                                                    PAYERPF.Value  
                                                  ELSE  
                                                    Round(( ( Gross - ( R.HRA + R.MedicalAllowance ) ) * PAYERPF.Percentage / 100 ) / R.ActualWorkdays * R.Workdays, 0)  
                                                END  
                                            END )  
                                        ELSE  
                                          0  
                                      END )                                                                                                                           AS ERPF,  
                                    ( CASE  
                                        WHEN dbo.Fn_CheckComponentConfigValue('ERESI', R.BaseLocationID, @DomainID) = 1 THEN  
                                          ( CASE  
                                              WHEN ( R.IsESIRequired = 0 ) THEN  
                                                0  
                                              ELSE  
                                                --CASE    
                                                --  WHEN( Datediff(MONTH, (SELECT TOP 1 EffectiveFrom    
                        --                         FROM   tbl_EmployeePayStructure e    
                                                --                       WHERE  e.EffectiveFrom < R.EffectiveFrom    
                                                --                                AND EmployeeId = R.EmployeeId    
                                                --                                AND IsDeleted = 0    
                                                --                 ORDER  BY EffectiveFrom DESC), R.EffectiveFrom) < 6 )THEN Ceiling(( Gross * PAYERESI.Percentage / 100 ) / R.ActualWorkdays * R.Workdays / 1)    
                                                --  ELSE    
                                                CASE  
                                                  WHEN ( ( Gross ) >= ( PAYERESI.Limit ) )THEN  
                                                    ( PAYERESI.Value )  
                                                  ELSE  
                                                    Ceiling(( ( Gross ) * PAYERESI.Percentage / 100 ) / R.ActualWorkdays * R.Workdays)  
                                                END  
                                            --END    
                                            END )  
                                        ELSE  
                                          0  
                                      END )                                                                                                                           AS ERESI,  
                                    Round(Bonus / R.ActualWorkdays * R.Workdays, 0)                                                                                   AS Bonus,  
                                    Round(Gratuity, 0)                                                                                                                AS Gratuity,  
                                    0                                                                                                                                 AS PLI,  
                                    0                                                                                                                                 AS [Mobile/DataCard],  
                                    0                                                                                                                                 AS Mediclaim,  
                                    0                                                                                                                                 AS OtherPerks,  
                                    0                                                                                                                                 AS IsProcessed,  
                                    0                                                                                                                                 AS IsApproved,  
                                    Round(Gross / R.ActualWorkdays * R.Workdays, 0)                                                                                   AS Gross,  
                                    0                                                                                                                                 AS NetSalary,  
                                    CTC                                                                                                                               AS CTC,  
                                    Round(Bonus / R.ActualWorkdays * R.Workdays, 0)                                                                                   AS PSBonus,  
                                    R.BaseLocationID                                                                                                                  AS BaseLocationID,  
                                    R.BusinessUnit                                               AS BusinessUnit,  
                                    R.DepartmentName                                                                                                                  AS DepartmentName,  
                                    R.AccountNo                                                                                                                       AS AccountNo,  
                                    R.IFSCCode                                                                                                                        AS IFSCCode,  
                                    R.DOB                                                                                                                             AS DOB,  
                                    R.PANNo                                                                                                                           AS PANNO,  
                                    R.BeneID                                                                                                                          AS BeneID  
                                    --------------  
                                    ,  
                                    IsEducationAllow,  
                                    IsOverTime,  
                                    IsEEPF,  
                                    IsEEESI,  
                                    IsPT,  
                                    IsTDS,  
                                    IsERPF,  
                                    IsERESI,  
                                    IsGratuity,  
                                    IsPLI,  
                                    IsMediclaim,  
                                    IsMobileDatacard,  
                                    IsOtherPerks  
                             FROM   #TableResult R  
                                    LEFT JOIN tbl_PayrollConfiguration PAYEPF  
                                           ON PAYEPF.Components = 'EmployeePF'  
                                              AND PAYEPF.BusinessUnitID = R.RegionID  
                                              AND PAYEPF.DomainID = R.DomainID  
                                    LEFT JOIN tbl_PayrollConfiguration PAYERPF  
                                           ON PAYERPF.Components = 'EmployerPF'  
                                              AND PAYERPF.BusinessUnitID = R.RegionID  
                                              AND PAYERPF.DomainID = R.DomainID  
                                    LEFT JOIN tbl_PayrollConfiguration PAYEESI  
                                           ON PAYEESI.Components = 'EmployeeESI'  
                                              AND PAYEESI.BusinessUnitID = R.RegionID  
                                              AND PAYEESI.DomainID = R.DomainID  
                                    LEFT JOIN tbl_PayrollConfiguration PAYERESI  
                                           ON PAYERESI.Components = 'EmployerESI'  
                                              AND PAYERESI.BusinessUnitID = R.RegionID  
                                              AND PAYERESI.DomainID = R.DomainID  
                                    LEFT JOIN #tmpParollConfigValue tpc  
                                           ON tpc.BusinessUnitID = r.BaselocationID  
                             WHERE  ORDERNUMBER = 1  
                             UNION ALL  
                             SELECT R.EmployeePayStructureID                                                                      AS EmployeePaystructureID,  
                                    ISNULL(E.EmpCodePattern, '')+ '' + E.Code                                                                                        AS EmployeeCode,  
                                    E.FirstName + ' ' + Isnull(E.LastName, '')                                                    AS EmployeeName,  
                                    E.DOJ                                                          AS DOJ,  
                                    R.Id                                                                                          AS ID,  
                                    R.EmployeeId                                                                                  AS EmployeeID,  
                                    R.MonthId                                                                                     AS Monthid,  
                                    YearId                                                                                        AS YearId,  
                                    Round((SELECT Gross  
                                           FROM   tbl_EmployeePayStructure  
                                           WHERE  EmployeeId = R.EmployeeId  
                                                  AND IsDeleted = 0  
                                                  AND ID = R.EmployeePayStructureID  
                                                  AND DomainID = @DomainID), 0)                                                   AS FixedGross,  
                                    NoofDaysPayable                                                                               AS [No of days payable],  
                                    Basic                                                                                         AS Basic,  
                                    HRA                                                                                           AS HRA,  
                                    MedicalAllowance                                                                              AS MedicalAllowance,  
                                    Conveyance                                                                                    AS Conveyance,  
                                    SplAllow                                                                                      AS SplAllow,  
                                    dbo.Fn_getComponentConfigValue(EducationAllow, 'EducationAllow', E.BaseLocationID, @DomainID) AS EducationAllowance,  
                                    MagazineAllow                                                                                 AS PaperMagazine,  
                                    MonsoonAllow                                                                                  AS MonsoonAllow,  
                                    dbo.Fn_getComponentConfigValue(OverTime, 'OverTime', E.BaseLocationID, @DomainID)             AS OverTime,  
                                    OtherEarning                                                                                  AS [Other Earning],  
                                    Round(( Basic + HRA + MedicalAllowance + Conveyance  
                                            + SplAllow  
                                            + dbo.Fn_getComponentConfigValue(EducationAllow, 'EducationAllow', E.BaseLocationID, @DomainID)  
                                            + dbo.Fn_getComponentConfigValue(OverTime, 'OverTime', E.BaseLocationID, @DomainID)  
                                            + MagazineAllow ), 0)                                                                 AS [Paid Gross],  
                                    dbo.Fn_getComponentConfigValue(EEPF, 'EEPF', E.BaseLocationID, @DomainID)                     AS EEPF,  
                                    dbo.Fn_getComponentConfigValue(EEESI, 'EEESI', E.BaseLocationID, @DomainID)                   AS EEESI,  
                                    dbo.Fn_getComponentConfigValue(TDS, 'TDS', E.BaseLocationID, @DomainID)                       AS TDS,  
                                    dbo.Fn_getComponentConfigValue(PT, 'PT', E.BaseLocationID, @DomainID)                         AS PT,  
                                    Loans                     AS Loans,  
                                    OtherDeduction                                                                                AS [Other Deductions],  
                                    dbo.Fn_getComponentConfigValue(ERPF, 'ERPF', E.BaseLocationID, @DomainID)                     AS ERPF,  
                                    dbo.Fn_getComponentConfigValue(ERESI, 'ERESI', E.BaseLocationID, @DomainID)                   AS ERESI,  
                                    Bonus                                                                                         AS Bonus,  
                                    dbo.Fn_getComponentConfigValue(Gratuity, 'Gratuity', E.BaseLocationID, @DomainID)             AS Gratuity,  
                                    dbo.Fn_getComponentConfigValue(PLI, 'PLI', E.BaseLocationID, @DomainID)                       AS PLI,  
                                    dbo.Fn_getComponentConfigValue(MobileDataCard, 'MobileDataCard', E.BaseLocationID, @DomainID) AS [Mobile/Datacard],  
                                    dbo.Fn_getComponentConfigValue(Mediclaim, 'Mediclaim', E.BaseLocationID, @DomainID)           AS Mediclaim,  
                                    dbo.Fn_getComponentConfigValue(OtherPerks, 'OtherPerks', E.BaseLocationID, @DomainID)         AS OtherPerks,  
                                    IsProcessed                                                                                   AS IsProcessed,  
                                    IsApproved                                                                                    AS IsApproved,  
                                    Round(R.GrossEarning, 0)                                                                      AS Gross,  
                                    Isnull(NetSalary, 0)                                                                          AS NetSalary,  
                                    Isnull(CTC, 0)                                                                                AS CTC,  
                                    (SELECT Bonus  
                                     FROM   tbl_EmployeePayStructure  
                                     WHERE  EmployeeId = E.ID  
                                            AND IsDeleted = 0  
                                            AND ID = r.EmployeePayStructureID  
                                            AND DomainID = @DomainID)                                                             AS PSBonus,  
                                    e.BaseLocationID                                                                              AS BaseLocationID,  
                                    BU.NAME                                                                                       AS BusinessUnit,  
                                    DM.NAME                                                                                       AS DepartmentName,  
                                    Isnull(ES.AccountNo, '')                                                                      AS AccountNo,  
                                    Isnull(ES.IFSCCode, '')                                                                       AS IFSCCode,  
                                    Isnull(EP.DateOfBirth, '')                                                                    AS DOB,  
                                    Isnull(ES.PAN_No, '')                                                                         AS PANNO,  
                                    Isnull(ES.BeneID, '')                                                                         AS BeneID  
                                    --------------  
                                    ,  
                                    IsEducationAllow,  
                                    IsOverTime,  
                                    IsEEPF,  
                                    IsEEESI,  
 IsPT,  
                                    IsTDS,  
                                    IsERPF,  
                                    IsERESI,  
                                    IsGratuity,  
                                    IsPLI,  
                                    IsMediclaim,  
                                    IsMobileDatacard,  
                                    IsOtherPerks  
                             FROM   tbl_EmployeePayroll R  
                                    JOIN tbl_EmployeeMaster E  
                                      ON R.EmployeeId = E.ID  
                                         AND E.IsDeleted = 0  
                                    LEFT JOIN tbl_lopdetails P  
                                           ON P.EmployeeId = R.EmployeeId  
                                              AND p.DomainId = r.DomainId  
                                              AND P.Monthid = @MonthId  
                                              AND P.year = @Year  
                                    LEFT JOIN tbl_BusinessUnit bu  
                                           ON BU.ID = R.BaseLocationID  
                                    LEFT JOIN tbl_Department DM  
                                           ON DM.id = E.DepartmentID  
                                    LEFT JOIN tbl_EmployeePersonalDetails EP  
                                           ON EP.EmployeeID = e.ID  
                                    LEFT JOIN tbl_EmployeeStatutoryDetails ES  
                                           ON ES.EmployeeID = e.ID  
                                    LEFT JOIN #tmpParollConfigValue tpc  
                                           ON tpc.BusinessUnitID = r.BaselocationID  
                             WHERE  R.monthid = @MonthId  
                                    AND R.DomainID = @DomainID  
                                    AND [yearid] = @Year  
                                    AND R.isdeleted = 0  
                                    AND ( ( @Location = Cast(0 AS VARCHAR) )  
                                           OR ( R.BaseLocationID IN (SELECT [Value]  
                                                                     FROM   @DataSource  
                                                                     WHERE  E.IsDeleted = 0) ) )  
                                    AND ( ( @DepartmentID = 0  
                                             OR @DepartmentID IS NULL )  
                                           OR ( E.DepartmentID = @DepartmentID ) ))  
                    SELECT *  
                    INTO   #PAY  
                    FROM   cte  
                    ORDER  BY EmployeeCode ASC  
  
                    IF ( @StatusID = NULL  
                          OR @StatusID IS NULL )  
                      BEGIN  
                          SELECT *  
                          FROM   #PAY  
                      END  
                    ELSE IF ( @StatusID = 0 )  
                      BEGIN  
                          SELECT *  
                          FROM   #PAY  
                          WHERE  id = 0  
                      END  
                    ELSE IF( @StatusID = 1 )  
                      BEGIN  
                          SELECT *  
                          FROM   #PAY  
                          WHERE  id <> 0  
                                 AND IsProcessed = 0  
                                 AND IsApproved = 0  
                      END  
                    ELSE IF( @StatusID = 2 )  
                      BEGIN  
                          SELECT *  
                          FROM   #PAY  
                          WHERE  IsApproved = 1  
                                 AND IsProcessed = 0  
                      END  
                    ELSE IF( @StatusID = 3 )  
                      BEGIN  
                          SELECT *  
                          FROM   #PAY  
                          WHERE  IsProcessed = 1  
                                 AND IsApproved = 1  
                      END  
            END  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
