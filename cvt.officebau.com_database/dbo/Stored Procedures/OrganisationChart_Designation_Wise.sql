﻿
/****************************************************************************   
CREATED BY   :   Naneeshwar.M
CREATED DATE  :   18-DEC-2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
	 [Organisationchart_designation_wise] 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[OrganisationChart_Designation_Wise](@DomainID INT)
AS
  BEGIN
      BEGIN TRY
          SELECT DISTINCT o.DesignationID AS ID,
                          de.NAME         AS DesignationName,
                          demap.NAME      AS EmployeeName,
                          o.MappingID     AS MappingID
          FROM   tbl_organisationchart o
                 JOIN tbl_Designation de
                   ON de.ID = o.DesignationID
                      AND de.IsDeleted = 0
                 JOIN tbl_Designation demap
                   ON demap.ID = o.MappingID
                      AND demap.IsDeleted = 0
          WHERE  o.DomainID = @DomainID
                 AND o.IsDeleted = 0
          ORDER  BY ID ASC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
