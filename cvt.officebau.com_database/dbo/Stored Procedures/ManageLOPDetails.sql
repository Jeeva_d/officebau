﻿/****************************************************************************   
CREATED BY		: JENNIFER.S
CREATED DATE	: 03-Jul-2017
MODIFIED BY		: JENNIFER.S
MODIFIED DATE	: 05-Jul-2017
 <summary>
		[ManageLOPDetails]
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageLOPDetails] (@SessionID  INT,
                                          @DomainID   INT,
                                          @LOPDetails LOPDETAILS READONLY)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Ouput VARCHAR(100) = 'Operation Failed!'

          IF EXISTS(SELECT 1
                    FROM   tbl_pay_EmployeePayroll Pay
                           JOIN @LOPDetails det
                             ON Pay.EmployeeId = DET.EmployeeID
                                AND Pay.MonthID = DET.MonthID
                                AND PAY.YearID = DET.[YEAR]
                                AND PAY.IsProcessed = 0
                                AND PAY.IsDeleted = 0
                                AND PAY.DomainID = @DomainID
                           LEFT JOIN tbl_LopDetails lop
                                  ON lop.ID = det.ID
                                     AND det.monthID = lop.MonthiD
                                     AND det.Year = lop.Year
                                     AND lop.EmployeeId = det.EmployeeID)
            BEGIN
                UPDATE Pay
                SET    Pay.IsDeleted = 1
                FROM   tbl_pay_EmployeePayroll Pay
                       JOIN @LOPDetails det
                         ON Pay.EmployeeId = det.EmployeeID
                            AND Pay.MonthID = DET.MonthID
                            AND PAY.YearID = DET.[YEAR]
                            AND PAY.IsProcessed = 0
                            AND PAY.IsDeleted = 0
                            AND PAY.DomainID = @DomainID
                       LEFT JOIN tbl_LopDetails lop
                              ON lop.ID = det.ID
                                 AND lop.EmployeeId = det.EmployeeID
                                 AND det.monthID = lop.MonthiD
                                 AND det.Year = lop.Year
                WHERE  Isnull(det.LOPDays, 0) <> Isnull(lop.LOPDays, 0)
            END

          -- Insert LOP Details
          INSERT INTO tbl_LopDetails
                      (EmployeeId,
                       MonthID,
                       [Year],
                       LopDays,
                       IsManual,
                       CreatedBy,
                       ModifiedBy,
                       DomainId)
          SELECT EmployeeID,
                 MonthID,
                 [Year],
                 Isnull(LOPDays, 0),
                 1,
                 @SessionID,
                 @SessionID,
                 @DomainID
          FROM   @LOPDetails
          WHERE  Isnull(ID, 0) = 0
                 AND PayrollStatus = 'Open'

          -- AND Isnull(LOPDays, 0) <> 0
          -- Update LOP Details
          UPDATE lop
          SET    lop.LopDays = Isnull(det.LOPDays, 0),
                 IsManual = 1,
                 lop.ModifiedBy = @SessionID,
                 lop.ModifiedOn = Getdate()
          FROM   tbl_LopDetails lop
                 JOIN @LOPDetails det
                   ON lop.ID = det.ID
                      AND lop.EmployeeId = det.EmployeeID
                      --AND Isnull(det.ID, 0) <> 0
                      AND PayrollStatus = 'Open'

          SET @Ouput = 'Updated Successfully'

          SELECT @Ouput

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
