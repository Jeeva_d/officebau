﻿/****************************************************************************   
CREATED BY   :  DhanaLakshmi. S
CREATED DATE  :  22 JUNE 2017 
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          [Searchcompanypaystructure] 2,2
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchcompanypaystructure] (@DomainID   INT,
                                                   @EmployeeID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT COM.ID           AS ID,
                 COM.NAME         AS NAME,
                 [Basic]          AS [Basic],
                 HRA              AS HRA,
                 MedicalAllowance AS MedicalAllowance,
                 Conveyance       AS Conveyance,
                 EffectiveFrom    AS EffictiveFrom,
                 BU.NAME          AS BusinessUnit
          FROM   tbl_CompanyPayStructure COM
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = COM.BusinessUnitID
          WHERE  BusinessUnitID IN(SELECT DISTINCT BU.ParentID
                                   FROM   tbl_BusinessUnit BU
                                          JOIN (SELECT *
                                                FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                                         FROM   tbl_EmployeeMaster
                                                                         WHERE  ID = @EmployeeID
                                                                                AND DomainID = @DomainID), ',')
                                                WHERE  Isnull(Item, '') <> '') i
                                            ON i.Item = BU.ID
                                   WHERE  COM.DomainID = @DomainID
                                          AND COM.IsDeleted = 0)
                 AND COM.DomainId = @DomainID
          ORDER  BY COM.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
