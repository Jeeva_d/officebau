﻿
/****************************************************************************   
CREATED BY   :  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          SearchCustomerContact  '', '', '', '', 1, 224
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchCustomerContact] (@CustomerName VARCHAR(100),
                                               @ContactName  VARCHAR(100),
                                               @EmailID      VARCHAR(50),
                                               @ContactNo    VARCHAR(50),
                                               @DomainID     INT,
                                               @EmployeeID   INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @applicationRoleID INT = (SELECT ID
             FROM   tbl_ApplicationRole
             WHERE  NAME = 'Sales Manager'
                    AND DomainID = @DomainID
                    AND Isdeleted = 0)

          SELECT CCO.ID                                         AS ID,
                 CCO.FirstName + ' ' + Isnull(CCO.LastName, '') AS ContactName,
                 CUS.EnquiryName                                AS CustomerName,
                 CCO.EmailID                                    AS EmailID,
                 DEST.NAME                                      AS DesignationName,
                 DEP.NAME                                       AS DepartmentName,
                 CCO.ContactNo1                                 AS ContactNo,
                 CCO.ContactNo2                                 AS AlternateNo
          FROM   tbl_CustomerContact CCO
                 LEFT JOIN tbl_Customer CUS
                        ON CCO.CustomerID = CUS.ID
                 LEFT JOIN tbl_CustomerDepartment DEP
                        ON DEP.ID = CCO.CustomerDepartmentID
                 LEFT JOIN tbl_CustomerDesignation DEST
                        ON DEST.ID = CCO.CustomerDesignationID
          WHERE  Isnull(CCO.IsDeleted, 0) = 0
                 AND ( ISNULL(@CustomerName, '') = ''
                        OR ( CUS.EnquiryName + ' '
                             + Isnull(CUS.OrderName, '') ) LIKE '%' + @CustomerName + '%' )
                 AND ( ISNULL(@ContactName, '') = ''
                        OR ( CCO.FirstName + ' ' + Isnull(CCO.LastName, '') ) LIKE '%' + @ContactName + '%' )
                 AND ( ISNULL(@EmailID, '') = ''
                        OR ISNULL(CCO.EmailID, '') LIKE '%' + @EmailID + '%' )
                 AND ( ISNULL(@ContactNo, '') = ''
                        OR ISNULL(CCO.ContactNo1, '') LIKE '%' + @ContactNo + '%' )
                 AND CCO.DomainID = @DomainID
                 AND ( Isnull(CUS.SalesPersonID, 0) = @EmployeeID
                        OR ( (SELECT Isnull(ApplicationRoleID, 0)
                              FROM   tbl_EMployeeOtherDetails
                              WHERE  EmployeeID = @EmployeeID
                                     AND DomainID = @DomainID
                                     AND Isdeleted = 0) = @applicationRoleID
                             AND ( CUS.CreatedBY = @EmployeeID
                                    OR CUS.SalesPersonID IN (SELECT DISTINCT T.ID
                                                             FROM   tbl_EmployeeMaster T
                                                                    JOIN tbl_EmployeeOtherDetails TOD
                                                                      ON T.ID = TOD.EmployeeID
                                                                    CROSS APPLY dbo.Splitstring((SELECT BusinessUnitID
                                                                                                 FROM   tbl_EmployeeMaster
                                                                                                 WHERE  ID = @EmployeeID
                                                                                                        AND Isdeleted = 0
                                                                                                        AND ISNULL(IsActive, 0) = 0
                                                                                                        AND DomainID = @DomainID), ',') S
                                                             WHERE  Patindex('%,' + S.Item + ',%', BusinessUnitID) > 0
                                                                    AND T.ID != @EmployeeID
                                                                    AND TOD.ApplicationRoleID = (SELECT ID
                                                                                                 FROM   tbl_ApplicationRole
                                                                                                 WHERE  [Name] = 'Sales Executive')
                                                                    AND T.Isdeleted = 0
                                                                    --AND ISNULL(T.IsActive, 0) = 0
                                                                    AND T.DomainID = @DomainID) ) ) )
          ORDER  BY CCO.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
