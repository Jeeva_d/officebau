﻿
 /****************************************************************************   
CREATED BY		:   
CREATED DATE	:   
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
 [ManageNewsAndEvents] 7,'Event1','Description Event1',1,1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageNewsAndEvents] (@ID          INT,
                                             @Name        VARCHAR(200),
                                             @Description VARCHAR(MAX),
                                             @SessionID   INT,
                                             @DomainID    INT,
                                             @IsDeleted   BIT,
											 @EventDate Date)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(100)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF( @IsDeleted = 1 )
            BEGIN
                IF EXISTS (SELECT 1
                           FROM   tbl_NewsAndEvents
                           WHERE  ID = @ID
                                  AND Isnull(PublishedBy, 0) <> 0
                                  AND DomainID = @DomainID
                                  AND IsDeleted = 0)
                  BEGIN
                      SET @Output = 'Record Referred.'
                  END
                ELSE
                  BEGIN
                      UPDATE tbl_NewsAndEvents
                      SET    IsDeleted = 1,
                             ModifiedBY = @SessionID
                      WHERE  ID = @ID
                             AND DomainID = @DomainID

                      DELETE FROM tbl_MultipleFileUpload
                      WHERE  ID = (SELECT HistoryID
                                      FROM   tbl_NewsAndEvents 
                                      WHERE  ID = @ID)

                      SET @Output = 'Deleted Successfully'
                  END
            END
          ELSE
            BEGIN
                IF ( @ID <> 0 )
                  BEGIN
                      UPDATE tbl_NewsAndEvents
                      SET    NAME = @Name,
                             Description = @Description ,EventDate=@EventDate
                      WHERE  ID = @ID

                      SET @Output = 'Updated Successfully./' + CONVERT(VARCHAR(500), (SELECT HistoryID FROM tbl_NewsAndEvents WHERE ID= @ID))
                  END
                ELSE
                  BEGIN
                      INSERT INTO tbl_NewsAndEvents
                                  (NAME,
                                   Description,
                                   DomainID,
                                   CreatedBy,
                                   ModifiedBy,EventDate)
                      VALUES      ( @Name,
                                    @Description,
                                    @DomainID,
                                    @SessionID,
                                    @SessionID ,@EventDate )

                      SET @Output = 'Inserted Successfully./' + CONVERT(VARCHAR(500), (SELECT HistoryID FROM tbl_NewsAndEvents WHERE ID= @@IDENTITY))
                  END
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
