﻿CREATE PROCEDURE [dbo].[GetMonthNameForPaystub]  
(
@EmployeeId int 
)
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION;  
 Declare @Date  date = ( Select Top 1 Dateadd(year, yearId - 2000,Dateadd(month, monthId - 1, '20000101')) AS StartDate from tbl_Pay_EmployeePayroll
Where employeeId =@EmployeeId order by StartDate desc );
          WITH cte  
               AS (SELECT 1 AS num  
                   UNION ALL  
                   SELECT num + 1  
                   FROM   cte  
                   WHERE  num < 3)  
          SELECT CONVERT(VARCHAR(10), Month(dates)) + ','  
                 + CONVERT(VARCHAR(10), Year(dates)) ID,  
                 Datename(month, dates) + '  '  
                 + CONVERT(VARCHAR(10), Year(dates))  
                 + ' - Payslip'                      AS NAME  
          FROM   (SELECT Dateadd(mm, -num, Dateadd(dd, 1, (SELECT dbo.Eomonth(@Date, -0)))) AS dates  
                  FROM   cte) A  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END  