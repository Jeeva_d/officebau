﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>    
 [TDS_SectionDETAILSENGINE] 2 ,'80cc'
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[TDS_HouseTaxDETAILSENGINE] (@EmployeeID INT,
                                                   @FYID         INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT TDS.Declaration AS Declaration,
                 TDS.Component   AS Component,
                 TDS.Type        AS [Type]
          FROM   TDSHouseTax TDS
          WHERE  EmployeeID = @EmployeeID
                 AND FinancialYearID = @FYID
                 AND TDS.DomainID = (SELECT DomainID
                                     FROM   tbl_EmployeeMaster
                                     WHERE  id = @EmployeeID)
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
