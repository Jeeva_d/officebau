﻿/****************************************************************************             
CREATED BY   :           
CREATED DATE  :             
MODIFIED BY   :     Ajith N        
MODIFIED DATE  :    12 May 2017         
 <summary>          
 [Rpt_IncomeCustomer] 2017,1,1          
 [Rpt_IncomeCustomer] '',' ',1        
 [Rpt_IncomeCustomer]'apr 1 2018','june 30 2018',1       
 </summary>                                     
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[Rpt_IncomeCustomer] (@StartDate DATETIME = NULL,        
                                            @EndDate   DATETIME = NULL,        
                                            @DomainID  INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          DECLARE @FinancialYear       INT,        
                  @StartFinancialMonth INT,        
                  @EndFinancialMonth   INT;        
        
          IF ( @StartDate IS NULL        
                OR @StartDate = '' )        
              OR ( @EndDate IS NULL        
                    OR @EndDate = '' )        
            BEGIN        
                SET @StartFinancialMonth = (SELECT Value        
                                            FROM   tblApplicationConfiguration        
                                            WHERE  Code = 'STARTMTH'        
                                                   AND DomainID = @DomainID)        
        
                SET @EndFinancialMonth = ( ( (SELECT Value        
                                              FROM   tblApplicationConfiguration        
                                              WHERE  Code = 'STARTMTH'        
                                                     AND DomainID = @DomainID)        
                                             + 11 ) % 12 )        
        
                IF( MONTH(GETDATE()) <= ( ( (SELECT Value        
                                             FROM   tblApplicationConfiguration        
                                             WHERE  Code = 'STARTMTH'        
                                                    AND DomainID = @DomainID)        
                                            + 11 ) % 12 ) )        
                  SET @FinancialYear = YEAR(GETDATE()) - 1        
                ELSE        
                  SET @FinancialYear = YEAR(GETDATE())        
        
                SET @StartDate = DATEADD(MONTH, -1, DATEADD(MONTH, @StartFinancialMonth, DATEADD(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date            
        
                SET @EndDate = DATEADD(DAY, -1, DATEADD(MONTH, @EndFinancialMonth, DATEADD(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date           
        
            END        
        
          DECLARE @MONTHCOUNT          INT = 0,        
                  @currentmonth        INT,        
                  @currentyear         INT,        
                  @CustomerID          VARCHAR(100) = '',        
                  @AMOUNT              VARCHAR(100),        
                  @RowValueUpdateQuery VARCHAR(MAX)= '',        
                  @Count               INT = 0,        
                  @QueryResult         NVARCHAR(MAX),        
                  @ColumnName          VARCHAR(10),        
                  @DynamicSQL          VARCHAR(MAX),        
                  @SumOfColumnQuery    VARCHAR(MAX),        
                  @SumOfRowQuery       VARCHAR(MAX),        
                  @RowTotalInsertQuery VARCHAR(MAX),        
                  @OutputQuery         VARCHAR(MAX)        
        
          CREATE TABLE #RESULT        
            (        
               ID           INT PRIMARY KEY IDENTITY(1, 1),        
               [CustomerID] VARCHAR(200),        
               [Customer]   VARCHAR(200),        
               Total        MONEY NULL        
            )        
        
          SELECT DATENAME(MONTH, DATEADD(MONTH, X.number, @StartDate)) AS [MonthName],        
                 DATENAME(YY, DATEADD(MONTH, X.number, @StartDate))    [YEAR],        
                 DATEPART(MM, DATEADD(MM, X.number, @StartDate))       [MONTH],        
                 X.number                                              AS RowNumber        
          INTO   #TEMPMONTH        
          FROM   MASTER.DBO.spt_values X        
          WHERE  X.type = 'P'        
                 AND X.number <= DATEDIFF(MONTH, @StartDate, @EndDate);        
        
          SET @SumOfColumnQuery = (SELECT SUBSTRING((SELECT '+ ISNULL( ([' + SUBSTRING([MonthName], 1, 3 )        
                                                            + '_' + SUBSTRING([YEAR], 3, 2) + ']), 0)'        
                                                     FROM   #TEMPMONTH        
                                                     FOR XML PATH('')), 2, 20000)) --Update Sum Of Column  value        
        
          SET @SumOfRowQuery = (SELECT SUBSTRING((SELECT ',  NULLIF(SUM(ISNULL(['        
                                                         + SUBSTRING([MonthName], 1, 3 ) + '_'        
                                                         + SUBSTRING([YEAR], 3, 2) + '], 0)), 0) '        
                                                  FROM   #TEMPMONTH        
                                                  FOR XML PATH('')), 2, 20000)) --Update Sum Of Row value        
        
          SET @OutputQuery = (SELECT SUBSTRING((SELECT ',[' + SUBSTRING([MonthName], 1, 3 ) + '_'        
                                                       + SUBSTRING([YEAR], 3, 2) + ']'        
                                                FROM   #TEMPMONTH        
                                                FOR XML PATH('')), 2, 20000)) -- Final Output Table Fields        
        
          SET @DynamicSQL =(SELECT 'ALTER TABLE #Result ADD ['        
                                   + SUBSTRING([MonthName], 1, 3 ) + '_'        
                                   + SUBSTRING([YEAR], 3, 2) + '] MONEY NULL;'        
                            FROM   #TEMPMONTH        
                            FOR XML PATH('')) -- Add dynamic column        
        
          EXEC(@DynamicSQL) -- Add dynamic column        
        
          INSERT INTO #RESULT        
                      ([CustomerID],        
                       [Customer])        
          SELECT DISTINCT C.ID,        
                          C.NAME        
          FROM   tblCustomer C        
                 JOIN tblInvoice I        
                   ON C.ID = I.CustomerID        
          WHERE  I.DomainID = @DomainID        
    AND I.IsDeleted=0        
    ORDER BY C.Name ASC        
        
          WHILE( @Count < (SELECT COUNT(1)        
                           FROM   #RESULT) )        
            BEGIN        
                SET @CustomerID = (SELECT CustomerID        
                                   FROM   #RESULT        
                                   WHERE  ID = ( @Count + 1 ))        
        
                WHILE( @MONTHCOUNT < (SELECT COUNT(1)        
                                      FROM   #TEMPMONTH) )        
                  BEGIN        
                      SELECT @currentmonth = [MONTH],        
                             @currentyear = [year],        
                             @ColumnName = SUBSTRING([MonthName], 1, 3 ) + '_'        
                                           + SUBSTRING([YEAR], 3, 2) -- Display Column Name        
                      FROM   #TEMPMONTH        
                      WHERE  RowNumber = @MONTHCOUNT        
        
                      SET @AMOUNT = (SELECT SUM(CASE        
                                                  WHEN ( I.DiscountPercentage <> 0 ) THEN      
               ( Isnull(( IT.Qty * IT.Rate )+      
               ISNULL(IT.SGSTAMOUNT,0)+ISNULL(IT.cGSTAMOUNT,0)  -      
                ( Isnull(( ( IT.Qty * IT.Rate ) * I.DiscountPercentage ), 0) ), 0) )        
                                                  ELSE Isnull(( ( IT.Qty * IT.Rate )+      
               ISNULL(IT.SGSTAMOUNT,0)+ISNULL(IT.cGSTAMOUNT,0)  - Isnull(I.DiscountValue, 0) ), 0)        
                                                END) ProcessedAmount        
                                     FROM   tblInvoice I        
                                            JOIN tblInvoiceItem IT        
                                              ON IT.InvoiceID = I.ID and I.IsDeleted=0  and IT.ISDELETED=0      
                                            JOIN tblCustomer CU        
                                 ON CU.ID = I.CustomerID        
                                     WHERE  DATEPART(MONTH, I.Date) = @currentmonth        
                                            AND DATEPART(YEAR, I.Date) = @currentyear        
                                            AND CU.ID = @CustomerID        
                                            AND ( @StartDate = ''        
  OR I.Date >= @StartDate )        
                                            AND ( @EndDate = ''        
                                                   OR I.Date <= @EndDate ))        
        
                      SET @RowValueUpdateQuery = Isnull('UPDATE #Result SET [' + @ColumnName +'] = ' + @AMOUNT + ' WHERE CustomerID = ''' + @CustomerID + ''';', '')        
                                                 + Isnull(@RowValueUpdateQuery, '') --Update Row value        
        
                      SET @MONTHCOUNT = @MONTHCOUNT + 1        
                  END        
        
                SET @QueryResult = Isnull('UPDATE #Result SET Total = ' + @SumOfColumnQuery + ' WHERE CustomerID = ''' + @CustomerID + ''';', '')        
                                   + Isnull(@QueryResult, '') -- Result  table        
        
                SET @MONTHCOUNT = 0        
        
                SET @Count = @Count + 1        
            END        
          SET @RowTotalInsertQuery = ' insert into #result SELECT '''',''Total Amount'',NULLIF(Sum(Isnull(Total, 0)), 0),'        
                                     + @SumOfRowQuery + 'FROM   #Result' --Insert Row Total value        
        
          EXEC(@RowValueUpdateQuery ) --Update Row value        
        
          EXEC(@QueryResult) -- Result  table        
        
          EXEC(@RowTotalInsertQuery) --Insert Row Total value        
        
          EXEC( 'SELECT Customer,Isnull(Total,0) As Total, '+ @OutputQuery + ' FROM #Result ') -- Final Output table        
        
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
