﻿/****************************************************************************         
CREATED BY   :  Naneeshwar.M      
CREATED DATE  :         
MODIFIED BY   :   Ajith N      
MODIFIED DATE  :   07 Dec 2017      
 <summary>       
        [Getclaimapprovallist] 1,341,106,224,'8-29-17',4      
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Getclaimapprovallist] (@DomainID      INT,      
                                              @SettlerID     INT,      
                                              @ApproverID    INT,      
                                              @RequesterID   INT,      
                                              @SubmittedDate DATE,      
                                              @StatusID      INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT CLS.ID                       AS ID,      
                 CLR.ID                       AS RequesterItemID,      
                 CLR.ClaimDate                AS ClaimedDate,      
                 Cast(CLA.CreatedOn AS DATE)  AS ApprovedDate,      
                 CLC.NAME                     AS CategoryName,      
                 CFA.FinApprovedAmount        AS ApprovalAmount,      
                 Isnull(CLS.SettlerAmount, 0) AS SettlerAmount,      
                 CLR.CreatedBy                AS RequesterID,      
                 CLA.CreatedBy                AS ApprovalID,      
                 STA.Code                     AS Status,      
                 CM.Code                      AS PaymentTypeID,      
                 CLS.PaymentDate              AS PaymentDate,      
                 FU.[FileName]                    AS [FileName],      
                 SD.BankName                  AS BankName,      
                 SD.AccountNo                 AS AccountNo,      
                 ST.Code                      AS ApproverStatus,      
                 EM.FullName                 AS ModifiedBy,      
                 CLS.ModifiedOn               AS ModifiedOn,      
                 BU.NAME                      AS BaseLocation,      
                 DC.NAME                      AS DestinationCity,      
                 ( CASE      
                     WHEN ISNULL(DC.IsMetro, 0) = 1 THEN      
                       CP.MetroAmount      
                     ELSE      
                       CP.NonMetroAmount      
                   END )                      AS TotalAmount,
                   FU.ID AS    FileUploadID   
          FROM   tbl_ClaimsRequest CLR      
                 LEFT JOIN tbl_ClaimsApprove CLA      
                        ON CLR.ID = CLA.ClaimItemID      
                 LEFT JOIN tbl_ClaimsFinancialApprove CFA      
                        ON CFA.ClaimItemID = CLR.ID      
                 LEFT JOIN tbl_ClaimsSettle CLS      
                        ON CLR.ID = CLS.ClaimItemID      
                 LEFT JOIN tbl_ClaimCategory CLC      
                        ON CLC.ID = CLR.CategoryID      
                 LEFT JOIN tbl_Status STA      
                        ON STA.ID = CLS.StatusID      
                 LEFT JOIN tbl_CodeMaster CM      
                        ON CM.ID = CLS.PaymentTypeID      
                 LEFT JOIN tbl_FileUpload FU      
                        ON FU.Id = CLR.FileUpload      
                 LEFT JOIN tbl_EmployeeStatutoryDetails SD      
                        ON SD.EmployeeID = CLR.CreatedBy      
                           AND Isnull(SD.IsActive, 0) = 0      
                           AND Isnull(SD.IsDeleted, 0) = 0      
                 LEFT JOIN tbl_Status ST      
                        ON ST.ID = CFA.SettlerStatusID      
                 LEFT JOIN tbl_EmployeeMaster EM      
                        ON EM.ID = CLS.ModifiedBy      
                 LEFT JOIN tbl_BusinessUnit BU      
                        ON EM.BaseLocationID = BU.ID      
                 LEFT JOIN tbl_DestinationCities DC      
                        ON DC.ID = CLR.DestinationID      
                 LEFT JOIN tbl_ClaimPolicy CP      
    ON CP.DesignationMappingID = CLR.DestinationID      
                           AND CP.ExpenseType = CLR.CategoryID      
                           AND CP.IsDeleted = 0      
          WHERE  CFA.CreatedOn IS NOT NULL      
                 AND CLR.IsDeleted = 0      
                 AND CLR.DomainID = @DomainID      
                 AND CFA.ApproverID = @ApproverID      
                 --AND cf.SettlerID = @SettlerID      
                 AND CLA.RequesterID = @RequesterID      
                 AND Cast(CLR.SubmittedDate AS DATE) = @SubmittedDate      
                 AND ( ( Isnull(@StatusID, 0) = 0      
                         AND CFA.SettlerStatusID = (SELECT ID      
                                                    FROM   tbl_Status      
                                                    WHERE  Code IN( 'Submitted' )      
                                                           AND Type = 'Claims'      
                                                           AND IsDeleted = 0) )      
                        OR ( CFA.SettlerStatusID = @StatusID ) )      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
