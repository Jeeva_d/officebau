﻿/****************************************************************************       
CREATED BY   : Naneeshwar      
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>    
 [Rpt_incometransactiondetail] 'aUG_2018','Consulting Services','',1    
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Rpt_incometransactiondetail] (@SearchParameter VARCHAR(100),  
                                                     @PartyName       VARCHAR(100),  
                                                     @Type            VARCHAR(50),  
                                                     @DomainID        INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @SearchDate DATETIME=CONVERT(DATETIME, Replace(@SearchParameter, '_', ' 20'))  
      DECLARE @StartDate DATETIME=Dateadd(MONTH, Datediff(MONTH, 0, @SearchDate), 0)  
      DECLARE @EndDate DATETIME=Dateadd(SECOND, -1, Dateadd(MONTH, 1, Dateadd(MONTH, Datediff(MONTH, 0, @SearchDate), 0)))  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          IF( @Type = 'Party' )  
            BEGIN  
                SELECT i.ID      AS ID,  
                       i.Type    AS Type,  
                       ( CASE  
                           WHEN ( i.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)  
                                                                                    + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum(Qty * Rate) * i.DiscountPercentage ), 0) ), 0) )  
                                                                    FROM   tblInvoiceItem  
                                                                    WHERE  InvoiceID = i.ID  
                                                                           AND IsDeleted = 0  
                                                                           AND DomainID = @DomainID)  
                           ELSE (SELECT ( Isnull(( Sum(Qty * Rate)  
                                                   + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(i.DiscountValue, 0) ), 0) )  
                                 FROM   tblInvoiceItem  
                                 WHERE  InvoiceID = i.ID  
                                        AND IsDeleted = 0  
                                        AND DomainID = @DomainID)  
                         END )   AS Amount,  
                       InvoiceNo AS BillNo,  
                       Date      AS Date,  
                       c.NAME    AS VendorName,  
                       cm.Code   AS Status  
                FROM   tblInvoice i  
                       LEFT JOIN tblCustomer c  
                              ON c.ID = i.CustomerID  
                       LEFT JOIN tbl_CodeMaster cm  
                              ON cm.ID = i.StatusID  
                WHERE  Date >= @StartDate  
                       AND Date <= @EndDate  
                       AND i.IsDeleted = 0  
                       AND i.DomainID = @DomainID  
                       AND i.CustomerID = (SELECT ID  
                                           FROM   tblCustomer  
                                           WHERE  @PartyName = NAME  
                                                  AND IsDeleted = 0  
                                                  AND DomainID = @DomainID)  
                GROUP  BY i.ID,  
                          i.Type,  
                          c.NAME,  
                          cm.Code,  
                          InvoiceNo,  
                          Date,  
                          i.DiscountPercentage,  
                          i.DiscountValue  
                ORDER  BY Date DESC  
            END  
          ELSE IF( @Type = 'Product' )  
            BEGIN  
                SELECT i.ID      AS ID,  
                       i.Type    AS Type,  
                       ( CASE  
                           WHEN ( i.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)  
                                                                                    + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum(Qty * Rate) * i.DiscountPercentage ), 0) ), 0) )  
                                                                    FROM   tblInvoiceItem  
                                                                    WHERE  InvoiceID = i.ID  
                                                                           AND IsDeleted = 0  
                                                                           AND DomainID = @DomainID aND pRODUCTID IN (SELECT ID  
                                           FROM   tblProduct_v2  
                                           WHERE  @PartyName = NAME  
                                                  AND IsDeleted = 0  
                                                  AND DomainID = @DomainID))  
                           ELSE (SELECT ( Isnull(( Sum(Qty * Rate)  
                                                   + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(i.DiscountValue, 0) ), 0) )  
                                 FROM   tblInvoiceItem  
                                 WHERE  InvoiceID = i.ID  
                                        AND IsDeleted = 0  
                                        AND DomainID = @DomainID aND pRODUCTID IN (SELECT ID  
                                           FROM   tblProduct_v2  
                                           WHERE  @PartyName = NAME  
                                                  AND IsDeleted = 0  
                                                  AND DomainID = @DomainID))  
                         END )   AS Amount,  
                       InvoiceNo AS BillNo,  
                       Date      AS Date,  
                       c.NAME    AS VendorName,  
                       cm.Code   AS Status  
                FROM   tblInvoice i  
                       LEFT JOIN tblInvoiceItem it  
                              ON it.InvoiceID = i.ID  
                       LEFT JOIN tblCustomer c  
                              ON c.ID = i.CustomerID  
                       LEFT JOIN tbl_CodeMaster cm  
                              ON cm.ID = i.StatusID  
                WHERE  Date >= @StartDate  
                       AND Date <= @EndDate  
                       AND i.IsDeleted = 0  
                       AND i.DomainID = @DomainID  
                       AND it.ProductID = (SELECT ID  
                                           FROM   tblProduct_v2  
                                           WHERE  @PartyName = NAME  
                                                  AND IsDeleted = 0  
                                                  AND DomainID = @DomainID)  
                GROUP  BY i.ID,  
                          i.Type,  
                          c.NAME,  
                          cm.Code,  
                          InvoiceNo,  
                          Date,  
                          i.DiscountPercentage,  
                          i.DiscountValue  
                ORDER  BY Date DESC  
            END  
          ELSE  
            BEGIN  
                SELECT i.ID      AS ID,  
                       i.Type    AS Type,  
                       ( CASE  
                           WHEN ( i.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)  
                                                                                    + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum(Qty * Rate) * i.DiscountPercentage ), 0) ), 0) )  
                                                                    FROM   tblInvoiceItem  
                                                                    WHERE  InvoiceID = i.ID  
                                                                           AND IsDeleted = 0  
                                                                           AND DomainID = @DomainID)  
                           ELSE (SELECT ( Isnull(( Sum(Qty * Rate)  
                                                   + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(i.DiscountValue, 0) ), 0) )  
                                 FROM   tblInvoiceItem  
                                 WHERE  InvoiceID = i.ID  
                                        AND IsDeleted = 0  
                                        AND DomainID = @DomainID)  
                         END )   AS Amount,  
                       InvoiceNo AS BillNo,  
                       Date      AS Date,  
                       c.NAME    AS VendorName,  
                       cm.Code   AS Status  
         FROM   tblInvoice i  
                       LEFT JOIN tblCustomer c  
                              ON c.ID = i.CustomerID  
                       LEFT JOIN tbl_CodeMaster cm  
                              ON cm.ID = i.StatusID  
                WHERE  Date >= @StartDate  
                       AND Date <= @EndDate  
                       AND i.IsDeleted = 0  
                       AND i.DomainID = @DomainID  
                       AND i.RevenueCenterID = (SELECT ID  
                                                FROM   tblCostCenter  
                                                WHERE  @PartyName = NAME  
                                                       AND IsDeleted = 0  
                                                       AND DomainID = @DomainID)  
                GROUP  BY i.ID,  
                          i.Type,  
                          c.NAME,  
                          cm.Code,  
                          InvoiceNo,  
                          Date,  
                          i.DiscountPercentage,  
                          i.DiscountValue  
                ORDER  BY Date DESC  
            END  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
