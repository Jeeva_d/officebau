﻿/****************************************************************************                                       
CREATED BY  :                                       
CREATED DATE :                                       
MODIFIED BY  :   JENNIFER S                              
MODIFIED DATE :  13-SEP-2017                              
<summary>                           
</summary>                                                               
*****************************************************************************/  
CREATE PROCEDURE [dbo].[UpdateBill] (@ID            INT,  
                                    @Description   VARCHAR(1000),  
                                    @Date          DATETIME,  
                                    @VendorID      INT,  
                                    @TotalAmount   MONEY,  
                                    @DueDate       DATETIME,  
                                    @BillNo        VARCHAR(50),  
                                    @CostCenterID  INT,  
                                    @Type          VARCHAR(50),  
                                    @UploadFile    VARCHAR(8000),  
                                    @SessionID     INT,  
                                    @DomainID      INT,  
                                    @PaymentModeID INT,  
                                    @BankID        INT,  
                                    @Reference     VARCHAR(1000),  
                                    @ExpenseDetail EXPENSEDETAIL READONLY,  
                                    @FileID        UNIQUEIDENTIFIER)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SET @FileID = ISNULL(@FileID, Newid())  
  
          DECLARE @OUTPUT               VARCHAR(100),  
                  @ExpenseID            INT,  
                  @cash                 MONEY,  
                  @AutogenPaddingLength INT,  
                  @AutogeneratePrefix   VARCHAR(100),  
                  @AutogenNo            VARCHAR(100),  
                  @PreviousCash         MONEY = (SELECT PaidAmount  
                     FROM   tblExpense  
                     WHERE  ID = @ID  
                            AND DomainID = @DomainID),  
                  @PreviousMode         INT = (SELECT TOP 1 PaymentModeID  
                     FROM   tblExpensePayment EP  
                            LEFT JOIN tblExpensePaymentMapping EM  
                                   ON EP.ID = EM.expensePaymentID  
                                      AND EM.DomainID = 1  
                     WHERE  EP.IsDeleted = 0  
                            AND EP.DomainID = @DomainID  
                            AND EM.expenseID = @ID)  
          DECLARE @ExpenseVendorID INT  
  
          IF ( (SELECT Count(1)  
                FROM   tblVendor  
                WHERE  NAME = 'Others'  
                       AND DomainID = @DomainID  
                       AND IsDeleted = 0) = 0 )  
            BEGIN  
                INSERT INTO tblVendor  
                            (NAME,  
                             PaymentTerms,  
                             CreatedBy,  
                             CreatedOn,  
                             ModifiedBy,  
                             ModifiedOn,  
                             DomainID)  
                VALUES      ('Others',  
                             0,  
                             @SessionID,  
                             Getdate(),  
                             @SessionID,  
                             Getdate(),  
                             @DomainID)  
  
                SET @VendorID = @@IDENTITY  
            END  
  
          SET @ExpenseVendorID = CASE  
                                   WHEN ( @VendorID <> '' ) THEN  
                                     (SELECT ID  
                                      FROM   tblVendor  
                                      WHERE  NAME = 'Others'  
                                             AND DomainID = @DomainID  
                            AND IsDeleted = 0)  
                                   ELSE  
                                     ( @VendorID )  
                                 END  
          SET @AutogeneratePrefix = 'BIL'  
          SET @AutogenPaddingLength = '1'  
          SET @AutogenNo = @AutogeneratePrefix +  
                           + RIGHT('0000000000000001', @AutogenPaddingLength)  
  
          BEGIN  
              BEGIN  
                  SET @ExpenseID = @ID  
                  SET @ExpenseID = @ID  
  
                  IF EXISTS (SELECT 1  
                             FROM   tblBRS  
                             WHERE  SourceID IN (SELECT EP.ID  
                                                 FROM   tblExpensePayment EP  
                                                        LEFT JOIN tblExpensePaymentMapping EM  
                                                               ON EP.ID = EM.expensePaymentID  
                                                 WHERE  EP.IsDeleted = 0  
                                                        AND EM.expenseID = @ID  
                                                        AND EP.DomainID = @DomainID)  
                                    AND SourceType = (SELECT ID  
                                                      FROM   tblMasterTypes  
                                                      WHERE  NAME = 'Expense')  
                                    AND IsDeleted = 0  
                                    AND isreconsiled = 1  
                                    AND DomainID = @DomainID)  
                    BEGIN  
                        UPDATE tblExpense  
                        SET    BillNo = @BillNo,  
                               CostCenterID = @CostCenterID,  
                               Remarks = @Description,  
                               --HistoryID = @FileID,                            
                               ModifiedBy = @SessionID,  
                               ModifiedOn = Getdate()  
                        WHERE  ID = @ID  
                               AND DomainID = @DomainID  
  
                        IF ( (SELECT Count(1)  
                              FROM   @ExpenseDetail) > 0 )  
                          BEGIN  
                              UPDATE tblExpenseDetails  
                              SET    tblExpenseDetails.LedgerID = S.LedgerID,  
                                     tblExpenseDetails.Remarks = S.ExpenseDetailRemarks,  
                                     ModifiedBy = @SessionID,  
                                     ModifiedOn = Getdate()  
                              FROM   tblExpenseDetails  
                                     INNER JOIN @ExpenseDetail AS S  
                                             ON tblExpenseDetails.ID = S.ID  
                                                AND S.ID <> 0  
                          END  
  
                        SET @OUTPUT = (SELECT [Message]  
                                       FROM   tblErrorMessage  
                                       WHERE  [Type] = 'Information'  
                                              AND Code = 'RCD_UPD_DES'  
                                              AND IsDeleted = 0) --'The record is referred. Description is Updated.'                              
  
                        GOTO finish  
                    END  
                  ELSE  
                    BEGIN  
                        UPDATE tbl_FileUpload  
                        SET    ReferenceTable = 'tblExpense'  
                        WHERE  ID = @FileID  
  
                        UPDATE tblExpense  
                        SET    VendorID = @VendorID,  
                               DueDate = @DueDate,  
                               [Date] = @Date,  
                               BillNo = @BillNo,  
                               CostCenterID = @CostCenterID,  
                               [Type] = @Type,  
                               Remarks = @Description,  
         ModifiedBy = @SessionID,  
                               ModifiedOn = Getdate(),  
                               HistoryID = @FileID  
                        WHERE  ID = @ID  
                               AND [Type] = 'Bill'  
                               AND DomainID = @DomainID  
  
                        IF ( (SELECT Count(1)  
                              FROM   @ExpenseDetail) > 0 )  
                          BEGIN  
                              UPDATE tblExpenseDetails  
                              SET    tblExpenseDetails.LedgerID = S.LedgerID,  
                                     tblExpenseDetails.Remarks = S.ExpenseDetailRemarks,  
                                     tblExpenseDetails.Amount = S.Amount,  
                                     tblExpenseDetails.QTY = S.QTY,  
                                     tblExpenseDetails.CGSTPercent = S.CGST,  
                                     tblExpenseDetails.SGSTPercent = S.SGST,  
                                     tblExpenseDetails.CGST = S.CGSTAmount,  
                                     tblExpenseDetails.SGST = S.SGSTAmount,  
                                     tblExpenseDetails.IsLedger = S.IsProduct,  
                                     tblExpenseDetails.IsDeleted = S.IsDeleted,  
                                     ModifiedBy = @SessionID,  
                                     ModifiedOn = Getdate()  
                              FROM   tblExpenseDetails  
                                     INNER JOIN @ExpenseDetail AS S  
                                             ON tblExpenseDetails.ID = S.ID  
                                                AND S.ID <> 0  
                          END  
  
                        IF ( (SELECT Count(1)  
                              FROM   @ExpenseDetail) > 0 )  
                          BEGIN  
                              INSERT INTO tblExpenseDetails  
                                          (ExpenseID,  
                                           LedgerID,  
                                           Remarks,  
                                           Amount,  
                                           QTY,  
                                           CGSTPercent,  
                                           SGSTPercent,  
                                           CGST,  
                                           SGST,  
                                           IsLedger,  
                                           DomainID,  
                                           CreatedBy,  
                                           CreatedOn,  
                                           ModifiedBy,  
                                           ModifiedOn,  
                                           PoitemId)  
                              SELECT @ID,  
                                     LedgerID,  
                                     ExpenseDetailRemarks,  
                                     Amount,  
                                     QTY,  
                                     CGST,  
                                     SGST,  
                                     CGSTAmount,  
                                     SGSTAmount,  
                                     IsProduct,  
                                     @DomainID,  
                                     @SessionID,  
                                     Getdate(),  
                                     @SessionID,  
                                     Getdate(),  
                                     PoitemId  
                              FROM   @ExpenseDetail  
                              WHERE  ID = 0  
                                     AND IsDeleted = 0  
                                     AND LedgerID <> 0  
                          END  
  
                        UPDATE tblExpense  
                        SET    PaidAmount = (SELECT Sum(Amount)  
                                             FROM   tblExpensePaymentMapping  
    WHERE  IsDeleted = 0  
                                                    AND ExpenseID = @ExpenseID  
                                                    AND DomainID = @DomainID),  
                               StatusID = CASE  
                                            WHEN ( (SELECT Sum(Amount)  
                                                    FROM   tblExpensePaymentMapping  
                                                    WHERE  IsDeleted = 0  
                                                           AND ExpenseID = @ExpenseID  
                                                           AND DomainID = @DomainID) >= (SELECT ( ( ISNULL(Sum(QTY * Amount), 0) ) + ISNULL(Sum(CGST), 0) + ISNULL(Sum(SGST), 0) )  
                                                                                        FROM   tblExpenseDetails  
                                                                                        WHERE  ExpenseID = @ExpenseID  
                                                                                               AND IsDeleted = 0  
                                                                                               AND DomainID = @DomainID) ) THEN  
                                              (SELECT ID  
                                               FROM   tbl_CodeMaster  
                                               WHERE  [Type] = 'Close'  
                                                      AND IsDeleted = 0)  
                                            WHEN (( ISNULL((SELECT Sum(Amount)  
                                                            FROM   tblExpensePaymentMapping  
                                                            WHERE  IsDeleted = 0  
                                                                   AND ExpenseID = @ExpenseID  
                                                                   AND DomainID = @DomainID), 0) = 0 )) THEN  
                                              (SELECT ID  
                                               FROM   tbl_CodeMaster  
                                               WHERE  [Type] = 'Open'  
                                                      AND IsDeleted = 0)  
                                            WHEN ( ISNULL((SELECT Sum(Amount)  
                                                           FROM   tblExpensePaymentMapping  
                                                           WHERE  IsDeleted = 0  
                                                                  AND ExpenseID = @ExpenseID  
                                                                  AND DomainID = @DomainID), 0) <> 0 ) THEN  
                                              (SELECT ID  
                                               FROM   tbl_CodeMaster  
                                               WHERE  [Type] = 'Partial'  
                                                      AND IsDeleted = 0)  
                                          END,  
                               HistoryID = @FileID,  
                               ModifiedBy = @SessionID,  
                               ModifiedOn = Getdate()  
                        WHERE  ID = @ExpenseID  
                               AND DomainID = @DomainID  
  
                        SET @Output = (SELECT [Message]  
                                       FROM   tblErrorMessage  
                                       WHERE  [Type] = 'Information'  
                                              AND Code = 'BIL_UPD'  
                                              AND IsDeleted = 0) --'Bill Updated Successfully'     
                         
         ----Update item to Inventory  
                        EXEC ManageInventory  
                          @ID,  
                          'UPDATE',  
                          'Purchase',  
                          @SessionID,  
                          @DomainID  
  
                        GOTO finish  
                    END  
              END  
         END  
  
          FINISH:  
  
          EXEC UpdatePOQTY  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = ERROR_MESSAGE(),  
                 @ErrSeverity = ERROR_SEVERITY()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
