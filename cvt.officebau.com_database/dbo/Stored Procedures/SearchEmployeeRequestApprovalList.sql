﻿/****************************************************************************   
CREATED BY   : Ajith N  
CREATED DATE  : 10 Aug 2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
 [SearchEmployeeRequestApprovalList]  '' , '', '', '', 'true', '',224,1  
 </summary>                            
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchEmployeeRequestApprovalList] (@EmployeeCode     VARCHAR(20),
                                                           @TempEmployeeCode VARCHAR(20),
                                                           @Name             VARCHAR(100),
                                                           @ContactNo        VARCHAR(20),
                                                           @IsActive         BIT,
                                                           @ApproverID       INT,
                                                           @EmployeeID       INT,
                                                           @DomainID         INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @BusinessUnitIDs VARCHAR(50) = (SELECT BusinessUnitID
             FROM   tbl_EmployeeMaster
             WHERE  ID = @EmployeeID
                    AND DomainID = @DomainID)
          DECLARE @BusinessUnitTable TABLE
            (
               BusinessUnit VARCHAR(100)
            )

          INSERT INTO @BusinessUnitTable
          SELECT @BusinessUnitIDs

          SELECT EM.ID                                        AS ID,
                 CASE
                   WHEN EM.Code LIKE 'TMP_%' THEN
                     ''
                   ELSE
                     ISNULL(EM.EmpCodePattern, '') + EM.Code
                 END                                          AS EmployeeCode,
                 APPROVAL.TempEmployeeCode                    AS TempEmployeeCode,
                 EM.FirstName + ' ' + Isnull(EM.LastName, '') AS FirstName,
                 DEP.NAME                                     AS DepartmentName,
                 DESI.NAME                                    AS DesignationName,
                 Isnull(EM.ContactNo, '')                     AS ContactNo,
                 Isnull(EM.IsActive, 'False')                 AS IsApproved,
                 BU.NAME                                      AS BaseLocation,
                 EM.BaseLocationID,
                 APPROVAL.FirstApproverId,
                 Em.DOJ                                       AS DOJ
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_Department DEP
                        ON DEP.ID = EM.DepartmentID
                 LEFT JOIN tbl_Designation DESI
                        ON DESI.ID = EM.DesignationID
                 LEFT JOIN tbl_Functional FUN
                        ON FUN.ID = EM.FunctionalID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
                 LEFT JOIN tbl_EmployementType EMPLOYEETYPE
                        ON EMPLOYEETYPE.ID = EM.EmploymentTypeID
                 LEFT JOIN tbl_EmployeeMaster REPORTING
                        ON REPORTING.ID = EM.ReportingToID
                 LEFT JOIN tbl_EmployeeStatutoryDetails STATUTORY
                        ON STATUTORY.EmployeeID = EM.ID
                 LEFT JOIN tbl_EmployeeApproval APPROVAL
                        ON APPROVAL.EmployeeID = EM.ID
                 JOIN @BusinessUnitTable TBU
                   ON TBU.BusinessUnit LIKE '%,'
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))
                                            + ',%'
          WHERE  EM.IsDeleted = 0
                 AND ( Isnull(@EmployeeCode, '') = ''
                        OR ( EM.Code LIKE '%' + @EmployeeCode + '%' ) )
                 AND ( Isnull(@TempEmployeeCode, '') = ''
                        OR EM.Code LIKE '%' + @TempEmployeeCode + '%' )
                 AND ( Isnull(@Name, '') = ''
                        OR ( ( EM.FirstName + ' ' + Isnull(EM.LastName, '') ) LIKE '%' + @Name + '%' ) )
                 AND ( Isnull(@ContactNo, '') = ''
                        OR ( EM.ContactNo LIKE '%' + @ContactNo + '%' ) )
                 AND ( ( Isnull(@ApproverID, 0) = 0
                         AND Isnull(APPROVAL.FirstApproverId, 0) = 0 )
                        OR APPROVAL.FirstApproverId = @ApproverID )
                 AND ( Isnull(@IsActive, '') = ''
                        OR Isnull(EM.IsActive, 0) = @IsActive )
                 AND EM.domainID = @DomainID
          ORDER  BY EM.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
