﻿/****************************************************************************     
CREATED BY  :   Naneeshwar.M  
CREATED DATE :   05-SEP-2017  
MODIFIED BY  :     
MODIFIED DATE :     
<summary>  
 [Employeelistforpasswordreset] 1,'1,2,3,4,5,6,7,8,9,',0  
</summary>                             
*****************************************************************************/
CREATE PROCEDURE [dbo].[EmployeeListforPasswordReset] (@DomainID     INT,
                                                      @BusinessUnit VARCHAR(20),
                                                      @EmployeeID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(50)

      BEGIN TRY
          SELECT emp.ID                                    AS EmployeeID,
                 Isnull(emp.EmpCodePattern, '') + emp.Code AS Employeecode,
                 Emp.FullName                              AS EmployeeName,
                 BU.NAME                                   AS BusinessUnit,
                 EmailID                                   AS EmailID,
                 LoginCode                                 AS LoginCode
          FROM   tbl_EmployeeMaster emp
                 LEFT JOIN tbl_BusinessUnit BU
                        ON bu.ID = emp.BaseLocationID
          WHERE  IsActive = 0
                 AND emp.IsDeleted = 0
                 AND emp.HasAccess = 1
                 AND emp.DomainID = @DomainID
                 AND ( Isnull(EmploymentTypeID, 0) = (SELECT Id
                                                      FROM   tbl_EmployementType
                                                      WHERE  Code = 'Direct'
                                                             AND DomainID = @DomainID)
                        OR EmploymentTypeID IS NOT NULL )
                 AND BaseLocationID IN (SELECT Item
                                        FROM   dbo.Splitstring (@BusinessUnit, ',')
                                        WHERE  Isnull(Item, '') <> '')
                 AND ( EMP.ID = @EmployeeID
                        OR Isnull (@EmployeeID, 0) = 0 )
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
