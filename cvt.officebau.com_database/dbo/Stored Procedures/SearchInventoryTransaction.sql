﻿/****************************************************************************                                   
CREATED BY   : Dhanalakshmi. S                                  
CREATED DATE  :   20-11-2018                                 
MODIFIED BY   :                               
MODIFIED DATE  :                                  
 <summary>                                
      [SearchInventoryTransaction] 2, 1                        
 </summary>                                                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchInventoryTransaction] (@ProductID INT,  
                                                    @DomainID  INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT Inven.ProductID,  
                 Inven.StockType,  
                 Date,  
                 Isnull(Inven.Qty, 0)                         AS Qty,  
                 V.NAME                                       AS Party,  
                 Isnull('Bill No - ' + E.BillNo, '')          AS SourceNo,  
                 EM.FirstName + ' ' + Isnull(EM.LastName, '') AS ModifiedBy,  
                 Inven.ModifiedOn                             AS ModifiedOn  
          FROM   tbl_Inventory Inven  
                 LEFT JOIN tblExpenseDetails ED  
                        ON ED.LedgerID = Inven.ProductID  
                           AND ED.IsLedger = 0  
                 LEFT JOIN tblExpense E  
                        ON E.ID = Inven.ReferenceID  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = Inven.ModifiedBy  
                 LEFT JOIN tblVendor V  
                        ON v.ID = E.VendorID  
          WHERE  Inven.IsDeleted = 0  
                 AND Inven.DomainID = @DomainID  
                 AND Inven.ProductID = @ProductID  
                 AND Inven.StockType = 'Purchase'  
                 AND V.NAME IS NOT NULL  
          GROUP  BY Inven.ProductID,  
                    Inven.StockType,  
                    V.NAME,  
                    Date,  
                    E.BillNo,  
                    EM.FirstName + ' ' + Isnull(EM.LastName, ''),  
                    Inven.ModifiedOn,  
                    Inven.Qty  
          UNION ALL  
          SELECT Inven.ProductID,  
                 Inven.StockType,  
                 Date,  
                 Isnull(Inven.Qty, 0)                         AS Qty,  
                 C.NAME                                       AS Party,  
                 Isnull('Invoice No - ' + I.InvoiceNo, '')    AS SourceNo,  
                 EM.FirstName + ' ' + Isnull(EM.LastName, '') AS ModifiedBy,  
                 Inven.ModifiedOn                             AS ModifiedOn  
          FROM   tbl_Inventory Inven  
                 LEFT JOIN tblInvoiceItem IT  
                        ON IT.ProductID = Inven.ProductID  
                           AND IT.IsProduct = 0  
                 LEFT JOIN tblInvoice I  
                        ON I.ID = Inven.ReferenceID  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = Inven.ModifiedBy  
                 LEFT JOIN tblCustomer c  
                        ON c.ID = I.CustomerID  
          WHERE  Inven.IsDeleted = 0  
                 AND Inven.DomainID = @DomainID  
                 AND Inven.ProductID = @ProductID  
                 AND Inven.StockType = 'Sales'  
                 AND C.NAME IS NOT NULL  
          GROUP  BY Inven.ProductID,  
                    Inven.StockType,  
                    C.NAME,  
                    Date,  
                    I.InvoiceNo,  
                    EM.FirstName + ' ' + Isnull(EM.LastName, ''),  
                    Inven.ModifiedOn,  
                    Inven.Qty  
          UNION ALL  
          SELECT POD.LedgerID,  
                 'PO',  
                 PO.Date,  
                 Isnull(POD.Qty, 0)                           AS Qty,  
                 V.NAME                           AS Party,  
                 Isnull('PO No - ' + PO.PONo, '')             AS SourceNo,  
                 EM.FirstName + ' ' + Isnull(EM.LastName, '') AS ModifiedBy,  
                 POD.ModifiedOn                               AS ModifiedOn  
          FROM   tblPODetails POD  
                 LEFT JOIN tblPurchaseOrder PO  
                        ON PO.ID = POD.PoID  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = POD.ModifiedBy  
                 LEFT JOIN tblVendor V  
                        ON V.ID = PO.VendorID  
          WHERE  POD.IsDeleted = 0  
                 AND POD.DomainID = @DomainID  
                 AND POD.LedgerID = @ProductID  
                 AND POD.IsLedger = 0  
          GROUP  BY POD.LedgerID,  
                    V.NAME,  
                    PO.Date,  
                    PO.PONo,  
                    EM.FirstName + ' ' + Isnull(EM.LastName, ''),  
                    POD.ModifiedOn,  
                    POD.Qty  
          UNION ALL  
          SELECT SOT.ProductID,  
                 'SO',  
                 Date,  
                 Isnull(SOT.Qty, 0)                           AS Qty,  
                 C.NAME                                       AS Party,  
                 Isnull('SO No - ' + SO.SONo, '')             AS SourceNo,  
                 EM.FirstName + ' ' + Isnull(EM.LastName, '') AS ModifiedBy,  
                 SOT.ModifiedOn                               AS ModifiedOn  
          FROM   tblSoItem SOT  
                 LEFT JOIN tblSalesOrder SO  
                        ON SO.ID = SOT.SOID  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = SOT.ModifiedBy  
                 LEFT JOIN tblCustomer C  
                        ON C.ID = SO.CustomerID  
          WHERE  SOT.IsDeleted = 0  
                 AND SOT.DomainID = @DomainID  
                 AND SOT.ProductID = @ProductID  
                 AND SOT.IsProduct = 0  
          GROUP  BY SOT.ProductID,  
                    C.NAME,  
                    SO.Date,  
                    SO.SONo,  
                    EM.FirstName + ' ' + Isnull(EM.LastName, ''),  
                    SOT.ModifiedOn,  
                    SOT.Qty  
          UNION ALL  
          SELECT ISD.ProductID,  
                 'Distribution',  
                 DistributedDate,  
                 Isnull(ISD.Qty, 0)                           AS Qty,  
                 CASE  
                   WHEN ISD.EmployeeID = 0 THEN ISD.Party  
                   ELSE EMP.FirstName + ' ' + Isnull(EMP.LastName, '')  
                 END                                          AS Party,  
                 NULL                                         AS SourceNo,  
                 EM.FirstName + ' ' + Isnull(EM.LastName, '') AS ModifiedBy,  
                 ISD.ModifiedOn                               AS ModifiedOn  
          FROM   tbl_InventorystockDistribution ISD  
                 LEFT JOIN tblProduct_V2 P  
                        ON ISD.ProductID = P.ID  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = ISD.EmployeeID  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = ISD.ModifiedBy  
          WHERE  ISD.IsDeleted = 0  
                 AND ISD.DomainID = @DomainID  
                 AND ISD.ProductID = @ProductID  
          GROUP  BY ISD.ProductID,  
                    EMP.FirstName + ' ' + Isnull(EMP.LastName, ''),  
                    DistributedDate,  
                    EM.FirstName + ' ' + Isnull(EM.LastName, ''),  
                    ISD.ModifiedOn,  
                    ISD.Qty,  
                    ISD.EmployeeID,  
                    ISD.Party  
          ORDER  BY [ModifiedOn] DESC  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
