﻿/****************************************************************************   
CREATED BY      : Ajith N  
CREATED DATE  : 31 Oct 2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>    
 [SearchTopPaidMonthly]  1,2, 10000,2  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchTopPaidMonthly] (@DomainID       INT,  
                                              @UserID         INT,  
                                              @Amount         MONEY,  
                                              @BusinessUnitID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          CREATE TABLE #tmpAccess  
            (  
               ID   INT IDENTITY(1, 1),  
               item INT  
            )  
  
          IF( @BusinessUnitID <> 0 )  
            BEGIN  
                INSERT INTO #tmpAccess  
                SELECT @BusinessUnitID AS item  
            END  
          ELSE  
            BEGIN  
                INSERT INTO #tmpAccess  
                SELECT item  
                FROM   Splitstring((SELECT BusinessUnitID  
                                    FROM   tbl_EmployeeMaster  
                                    WHERE  ID = @UserID), ',')  
            END  
  
          SELECT Isnull(EM.FirstName, '') + ' '  
                 + Isnull(EM.LastName, '')           AS NAME,  
                 ( Isnull(d.Amount, 0) )             AS Amount,  
                 EM.ID,  
                 Row_number()  
                   OVER (  
                     PARTITION BY PS.EmployeeId  
                     ORDER BY PS.EffectiveFrom DESC) AS rowno  
          INTO   #temp  
          FROM   tbl_EmployeeMaster EM  
                 LEFT JOIN tbl_pay_EmployeePayStructure PS  
                        ON EM.ID = PS.EmployeeId  
                           AND Isnull(PS.IsDeleted, 0) = 0  
				LEFT JOIN tbl_Pay_EmployeePayStructureDetails d on d.PayStructureId = PS.ID
				 And d.ISdeleted=0 and d.ComponentId = (Select Value from tbl_DashBoardConfiguration where [Key]='HRMSCOM' and DomainID =@DomainID)
                 LEFT JOIN tbl_BusinessUnit BU  
                        ON BU.ID = EM.BaseLocationID  
          WHERE  EM.IsDeleted = 0  
                 AND Isnull(EM.IsActive, 0) = 0  
                 AND EM.Code NOT LIKE 'TMP_%'  
                 AND EM.DomainID = @DomainID  
                 AND Isnull(d.Amount, 0) <> 0  
                 AND EM.BaseLocationID IN (SELECT item  
                                           FROM   #tmpAccess)  
  
          SELECT TOP 10 NAME,  
                        ( Amount ) AS Amount,  
                        ID,  
                        rowno  
          FROM   #temp  
          WHERE  rowno = 1  
                 AND Amount <= @Amount  
          ORDER  BY Amount DESC  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
