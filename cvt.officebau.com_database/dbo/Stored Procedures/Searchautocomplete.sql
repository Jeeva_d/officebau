﻿/****************************************************************************     
CREATED BY   : Jeeva    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
     [Searchautocomplete] 'Vendor','Name','t' ,1   
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchautocomplete] (@TableName       VARCHAR(200),  
                                             @columnName      VARCHAR(200),  
                                             @SearchParameter VARCHAR(200),  
                                             @DomainID        INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          DECLARE @sql VARCHAR(MAX)  
  
          IF( @TableName = 'city' )  
            BEGIN  
                SET @TableName='tbl_' + @TableName  
            END  
          ELSE  
            BEGIN  
                SET @TableName='tbl' + @TableName  
            END  
  
          SET @sql= ( 'Select ID, Name From ' + @TableName  
                      + ' Where ' + @columnName + ' like''%'  
                      + @SearchParameter  
                      + '%'' and IsDeleted = 0 and DomainID = '  
                      + CONVERT(VARCHAR(10), @DomainID) )  
  
          EXEC(@sql)  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
