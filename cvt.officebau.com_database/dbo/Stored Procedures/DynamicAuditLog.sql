﻿
CREATE PROCEDURE [dbo].[DynamicAuditLog] (@TBL_NM       VARCHAR(MAX),
                                         @TablePKID    VARCHAR(MAX),
                                         @PKColumn     VARCHAR(MAX),
                                         @TYPE         VARCHAR(50),
                                         @ModifiedBy   VARCHAR(MAX),
                                         @ModifiedDate DATETIME)
AS
  BEGIN
      DECLARE @TypeCastColumns VARCHAR(MAX),
              @CreateQuery     VARCHAR(MAX),
              @COL_NM          VARCHAR(MAX),
              @TraceQuery      VARCHAR(max)

      SET @COL_NM = (SELECT Substring((SELECT ',[' + COLUMN_NAME + '] ' +
                                              + CASE
                                                  WHEN ( data_type IN ( 'varchar', 'nvarchar', 'text', 'binary',
                                                                        'char', 'nchar', 'ntext', 'varbinary' ) ) THEN data_type + ' ('
                                                                                                                       + CASE
                                                                                                                           WHEN CHARACTER_MAXIMUM_LENGTH = -1 THEN 'max'
                                                                                                                           ELSE CONVERT(VARCHAR(100), CHARACTER_MAXIMUM_LENGTH)
                                                                                                                         END
                                                                                                                       + ')'
                                                  ELSE data_type
                                                END
                                       FROM   INFORMATION_SCHEMA.COLUMNS
                                       WHERE  TABLE_NAME = @TBL_NM
                                       FOR XML PATH('')), 2, 80000))

      IF NOT EXISTS(SELECT *
                    FROM   INFORMATION_SCHEMA.TABLES
                    WHERE  TABLE_NAME = @TBL_NM + '_TriggerAudit')
        BEGIN
            SET @CreateQuery = 'CREATE TABLE ' + @TBL_NM + '_TriggerAudit ('
                               + @COL_NM + '); '

            EXEC(@CreateQuery)

            EXEC('ALTER TABLE '+ @TBL_NM + '_TriggerAudit ADD AuditAction VARCHAR(10);ALTER TABLE '+ @TBL_NM + '_TriggerAudit ADD AuditDate DATETIME;ALTER TABLE '+ @TBL_NM + '_TriggerAudit ADD AuditUser VARCHAR(200);')
        END

      SET @COL_NM = (SELECT Substring((SELECT ',[' + COLUMN_NAME + ']'
                                       FROM   INFORMATION_SCHEMA.COLUMNS
                                       WHERE  TABLE_NAME = @TBL_NM
                                       FOR XML PATH('')), 2, 80000))
      SET @TraceQuery = 'INSERT INTO ' + @TBL_NM + '_TriggerAudit ('
                        + @COL_NM
                        + ',AuditAction,AuditDate,AuditUser) SELECT '
                        + @COL_NM + ',''' + @TYPE + ''','''
                        + CONVERT(NVARCHAR(MAX), @ModifiedDate, 120)
                        + ''',''' + @ModifiedBy + ''' FROM ' + @TBL_NM
                        + ' WHERE ' + @PKColumn + ' = ''' + @TablePKID + ''''

      EXEC(@TraceQuery)
  END 
