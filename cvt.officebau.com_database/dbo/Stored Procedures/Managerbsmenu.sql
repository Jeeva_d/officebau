﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	05-JUN-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
      
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Managerbsmenu] (@UserID     INT,
                                       @MenuID     INT,
                                       @MRead      BIT,
                                       @MWrite     BIT,
                                       @MEdit      BIT,
                                       @MDelete    BIT,
                                       @ModifiedBy INT,
                                       @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(50)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF EXISTS (SELECT 1
                     FROM   tbl_RBSUserMenuMapping
                     WHERE  EmployeeID = @UserID
                            AND MenuID = @MenuID
                            AND IsDeleted = 0
                            AND DomainID = @DomainID)
            BEGIN
                UPDATE tbl_RBSUserMenuMapping
                SET    MRead = @MRead,
                       MWrite = @MWrite,
                       MEdit = @MEdit,
                       MDelete = @MDelete,
                       DomainID = @DomainID,
                       CreatedBy = @ModifiedBy,
                       ModifiedBy = @ModifiedBy,
                       ModifiedOn = Getdate()
                WHERE  EmployeeID = @UserID
                       AND MenuID = @MenuID
                       AND IsDeleted = 0
                       AND DomainID = @DomainID

                SET @Output = 'RBS Menu Updated Successfully.'
            END
          ELSE
            BEGIN
                INSERT INTO tbl_RBSUserMenuMapping
                            (EmployeeID,
                             MenuID,
                             MRead,
                             MWrite,
                             MEdit,
                             DomainID,
                             MDelete,
                             CreatedBy,
							 ModifiedBy)
                VALUES      ( @UserID,
                              @MenuID,
                              @MRead,
                              @MWrite,
                              @MEdit,
                              @DomainID,
                              @MDelete,
                              @ModifiedBy,
							  @ModifiedBy )

                SET @Output = 'RBS Menu Inserted Successfully.'
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
