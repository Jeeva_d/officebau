﻿/****************************************************************************             
CREATED BY    :           
CREATED DATE  :          
MODIFIED BY   :            
MODIFIED DATE :            
 <summary>            
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[UpdateSO] (@ID           INT,
                                  @InvoiceNo    VARCHAR(50),
                                  @CustomerID   INT,
                                  @Description  VARCHAR(1000),
                                  @Date         DATETIME,
                                  @BillAddress  VARCHAR(1000),
                                  @CurrencyRate MONEY,
                                  @SessionID    INT,
                                  @DomainID     INT,
                                  @INVOICEITEM  INVOICEITEM READONLY)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT    VARCHAR(100),
                  @InvoiceID INT

          SET @InvoiceID = @ID

          BEGIN
              UPDATE tblSalesOrder
              SET    CustomerID = @CustomerID,
                     DATE = @Date,
                     SONo = @InvoiceNo,
                     Description = @Description,
                     BillAddress = @BillAddress,
                     CurrencyRate = @CurrencyRate,
                     ModifiedBy = @SessionID,
                     ModifiedOn = Getdate()
              WHERE  ID = @ID
                     AND DomainID = @DomainID

              IF ( (SELECT Count(1)
                    FROM   @INVOICEITEM) > 0 )
                BEGIN
                    UPDATE tblSOItem
                    SET    tblSOItem.ProductID = I.ProductID,
                           tblSOItem.ItemDescription = I.ItemRemarks,
                           tblSOItem.QTY = I.QTY,
                           tblSOItem.Rate = I.Rate,
                           tblSOItem.SGST = I.SGST,
                           tblSOItem.SGSTAmount = I.SGSTAmount,
                           tblSOItem.CGST = I.CGST,
                           tblSOItem.CGSTAmount = I.CGSTAmount,
                           tblSOItem.IsDeleted = I.IsDeleted,
                           ModifiedBy = @SessionID,
                           ModifiedOn = Getdate(),
                           tblSOItem.IsProduct = I.IsProduct
                    FROM   tblSOItem
                           INNER JOIN @INVOICEITEM AS I
                                   ON tblSOItem.ID = I.ID
                                      AND I.ID <> 0
                END

              IF ( (SELECT Count(1)
                    FROM   @INVOICEITEM) > 0 )
                BEGIN
                    INSERT INTO tblSOItem
                                (SOID,
                                 ProductID,
                                 ItemDescription,
                                 QTY,
                                 Rate,
                                 SGST,
                                 SGSTAmount,
                                 CGST,
                                 CGSTAmount,
                                 DomainID,
                                 CreatedBy,
                                 ModifiedBy,
                                 IsProduct)
                    SELECT @ID,
                           ProductID,
                           ItemRemarks,
                           QTY,
                           Rate,
                           SGST,
                           SGSTAmount,
                           CGST,
                           CGSTAmount,
                           @DomainID,
                           @SessionID,
                           @SessionID,
                           IsProduct
                    FROM   @INVOICEITEM
                    WHERE  ID = 0
                           AND IsDeleted = 0
                           AND ProductID <> 0
                END
          END

          SET @OUTPUT = (SELECT [Message]
                         FROM   tblErrorMessage
                         WHERE  [Type] = 'Information'
                                AND Code = 'RCD_UPD'
                                AND IsDeleted = 0) --'Updated Successfully'      

          EXEC [UpdateSOQTY]

          ----Update item to Inventory
          EXEC ManageInventory
            @ID,
            'UPDATE',
            'SO',
            @SessionID,
            @DomainID

          SELECT @OUTPUT

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
