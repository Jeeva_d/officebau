﻿/****************************************************************************     
CREATED BY  :   SASIREKHA. K  
CREATED DATE :    
MODIFIED BY  :     
MODIFIED DATE :     
 <summary>  
	[ValidateLeaveRequest] 1,106,4  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Validateleaverequest] (@LeavetypeID      INT,
                                              @LeaveAppliedDays INT,
                                              @IsHalfDay        BIT,
                                              @EmployeeID       INT,
                                              @DomainID         INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @LeaveBalance DECIMAL(16, 1) = 0,
              @message      VARCHAR(80),
              @LeaveType    VARCHAR(200),
              @NoOfDays     INT,
              @flagValid    INT = 0

      SET @LeaveBalance= IsNULL((SELECT ( TotalLeave - AvailedLeave )
                                 FROM   tbl_EmployeeAvailedLeave
                                 WHERE  EmployeeID = @EmployeeID
                                        AND LeaveTypeID = @LeavetypeID
                                        AND DomainID = @DomainID), 0)
      SET @LeaveType=(SELECT NAME
                      FROM   tbl_LeaveTypes
                      WHERE  ID = @LeavetypeID
                             AND DomainID = @DomainID)

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @LeaveBalance = 0 )
            BEGIN
                SET @message = 'You don’t have ' + @LeaveType
                               + ' balance. So you can’t raise the '
                               + @LeaveType + ' Request.'
                SET @flagValid = 1
            END
          ELSE IF ( ( @LeaveBalance < @LeaveAppliedDays )
               AND ( @LeaveBalance != 0.5 ) )
            BEGIN
                IF( @NoOfDays = 0 )
                  BEGIN
                      IF( @LeaveBalance = 1 )
                        BEGIN
                            SET @message = 'Totally ' + Cast(@LeaveBalance AS INT)
                                           + ' day Available.';
                            SET @flagValid = 1
                        END
                      ELSE IF( ( @LeaveBalance < 1 )
                          AND ( @LeaveBalance > 0 ) )
                        BEGIN
                            SET @message = 'Totally 0.5 day Available.';
                            SET @flagValid = 1
                        END
                      ELSE
                        BEGIN
                            SET @message = 'Totally ' + @LeaveBalance
                                           + ' days Available.';
                            SET @flagValid = 1
                        END
                  END
                ELSE
                  BEGIN
                      IF ( @NoOfDays <= 365 )
                        BEGIN
                            SET @message = 'The Privilege Leave cannot be raise since you have not completed one year of Service.';
                            SET @flagValid = 1
                        END
                  END
            END
          ELSE IF( ( @LeaveBalance = 0.5 )
              AND ( @IsHalfDay = 'false' ) )
            BEGIN
                SET @message = 'Totally ' + @LeaveBalance + ' day Available.';
                SET @flagValid = 1
            END

          SELECT @message + '/' + @flagValid AS output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
