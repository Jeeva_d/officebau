﻿/****************************************************************************         
CREATED BY		:         
CREATED DATE	:         
MODIFIED BY		: JENNIFER S       
MODIFIED DATE   : 22-JUN-2017        
 <summary>                
     [SearchActualSalesReport] 1, 6,2017,'Mohammed AMJAD ALI','E'
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchActualSalesReport](@DomainID       INT,
                                                @MonthID        TINYINT,
                                                @Year           SMALLINT,
                                                @SalesPerson    VARCHAR(100),
                                                @Type           VARCHAR(10),
                                                @BusinessUnitID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT EMP.FirstName + ' ' + Isnull(EMP.LastName, '') SalesPerson,
                 TRD.TrainNo                                    RakeNo,
                 CASE
                   WHEN @Type = 'I' THEN TRD.[Date]
                   ELSE TRD.ExportDate
                 END                                            ExportDate,
                 CUS.EnquiryName                                CustomerName,
                 RAK.TUE                                        Actual,
                 EMP.ID											ID
          FROM   tbl_RakeDetails RAK
                 JOIN tbl_TrainDetails TRD
                   ON RAK.TrainID = TRD.ID
                      AND TRD.IsDeleted = 0
                      AND TRD.BusinessUnitID = @BusinessUnitID
                      AND Month(TRD.[Date]) = @MonthID
                      AND Year(TRD.[Date]) = @Year
                 JOIN tbl_Customer CUS
                   ON RAK.CustomerID = CUS.ID
                      AND CUS.IsDeleted = 0
                 JOIN tbl_EmployeeMaster EMP
                   ON CUS.SalesPersonID = EMP.ID
                      AND EMP.FirstName + ' ' + Isnull(EMP.LastName, '') = @SalesPerson
          WHERE  RAK.Isdeleted = 0
                 AND RAK.DomainID = @DomainID
                 AND RAK.[Type] = @Type

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
