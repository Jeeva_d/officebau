﻿/****************************************************************************     
CREATED BY   :   Dhanalakshmi  
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [GetApplicationAccountConfiguration] 1  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetApplicationAccountConfiguration] (@DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT apc.Code                         AS Code,  
                 apc.Value                        AS Value,  
                 (SELECT Count(1)  
                  FROM   tbl_FinancialYear  
                  WHERE  DomainID = @DomainID  
                         AND IsDeleted = 0)       AS [Count],  
                 ( (SELECT Count(1)  
                    FROM   tblExpense  
                    WHERE  ( CostCenterID >= 1  
                              OR CostCenterID = 0 )  
                           AND DomainID = @DomainID  
                           AND IsDeleted = 0)  
                   + (SELECT Count(1)  
                      FROM   tblInvoice  
                      WHERE  ( RevenueCenterID >= 1  
                                OR RevenueCenterID = 0 )  
                             AND DomainID = @DomainID  
                             AND IsDeleted = 0) ) AS CostcenterCount,  
                 apc.ModifiedOn                   AS ModifiedOn,  
                 EMP.FullName                    AS ModifiedBy  
          FROM   tblApplicationConfiguration apc  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = apc.ModifiedBy  
          WHERE  apc.IsDeleted = 0  
                 AND apc.DomainID = @DomainID  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
