﻿/****************************************************************************     
CREATED BY  : K.SASIREKHA   
CREATED DATE : 20-10-2017   
MODIFIED BY  :     
MODIFIED DATE :     
 <summary>  
	[Getccrecpts] 1,1 
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getccrecpts] (@ID       INT,
                                      @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT EMR.ID,
                 EMR.keyID     AS eventID,
                 EMR.BusinessUnitID,
                 EMR.CCEmailID AS CCrecpts,
                 EMR.ModifiedOn,
                 EMP.FullName  AS ModifiedBy
          FROM   tbl_EmailRecipients EMR
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = EMR.ModifiedBy
          WHERE  EMR.IsDeleted = 0
                 AND EMR.ID = @ID
                 AND EMR.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
