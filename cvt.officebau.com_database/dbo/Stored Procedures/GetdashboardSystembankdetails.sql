﻿/****************************************************************************       
CREATED BY    : Naneeshwar    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE :     
 <summary>     
  [GetdashboardSystembankdetails] 1    
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetdashboardSystembankdetails] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT DISTINCT TBRS.BankID,
                          B.DisplayName AS Name,
                          (( (SELECT DISTINCT Isnull(Sum(BB.Amount), 0)
                              FROM   tblbrs BRS
                                     LEFT JOIN tblBookedBankBalance BB
                                            ON BB.BRSID = BRS.ID
                              WHERE  BRS.isdeleted = 0
                                     AND BB.IsDeleted = 0
                                     AND BRS.DomainID = @DomainID
                                     AND SourceType IN((SELECT ID
                                                        FROM   tblMasterTypes
                                                        WHERE  BankID = TBRS.BankID
                                                               AND NAME IN ( 'InvoiceReceivable', 'Receive Payment', 'ToBank' )))) - (SELECT DISTINCT Isnull(Sum(BB.Amount), 0)
                                                                                                                                      FROM   tblbrs BRS
                                                                                                                                             LEFT JOIN tblBookedBankBalance BB
                                                                                                                                                    ON BB.BRSID = BRS.ID
                                                                                                                                      WHERE  BRS.isdeleted = 0
                                                                                                                                             AND BRS.DomainID = @DomainID
                                                                                                                                             AND BB.IsDeleted = 0
                                                                                                                                             AND SourceType IN((SELECT ID
                                                                                                                                                                FROM   tblMasterTypes
                                                                                                                                                                WHERE  BankID = TBRS.BankID
                                                                                                                                                                       AND NAME IN ( 'PayBill', 'FromBank', 'Bill', 'Expense', 'Make Payment' )))) )) + B.OpeningBalance AS AvailableBalance
          FROM   tblBank B
                 LEFT JOIN TblBrs TBRS
                        ON TBRS.bankID = B.ID
          WHERE  B.isdeleted = 0
                 AND B.DomainID = @DomainID
          GROUP  BY TBRS.BankID,
                    B.DisplayName,
                    B.OpeningBalance,
                    TBRS.ID
          UNION
          SELECT ID,
                 'Cash',
                 AvailableCash
          FROM   tblcashBucket
          WHERE  DomainID = @DomainID
          ORDER  BY b.DisplayName ASC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
