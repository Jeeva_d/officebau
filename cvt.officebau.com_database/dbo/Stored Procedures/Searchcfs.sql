﻿/****************************************************************************   
CREATED BY   :   Naneeshwar.M
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [Searchcfs] 'Madhavaram'
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchcfs] (@BusinessUnit VARCHAR(50),
                                   @MonthID      INT,
                                   @Year         INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @YearName INT =(SELECT FinancialYear
            FROM   tbl_FinancialYear
            WHERE  ID = @Year
                   AND ISNULL(IsDeleted, 0) = 0)

          SELECT MON.Code      AS [Month],
                 SUM(CFS.unit) Unit
          FROM   tbl_cfs CFS
                 JOIN tbl_BusinessUnit BUS
                   ON BUS.ID = CFS.businessunit
                 LEFT JOIN tbl_Month MON
                        ON MON.ID = CFS.monthID
          WHERE  BUS.NAME = @BusinessUnit
                 AND ( ISNULL(@MonthID, 0) = 0
                        OR CFS.MonthID = @MonthID )
                 AND ( ISNULL(@Year, 0) = 0
                        OR CFS.YearID = @YearName )
          GROUP  BY BUS.NAME,
                    MON.ID,
                    MON.Code
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
