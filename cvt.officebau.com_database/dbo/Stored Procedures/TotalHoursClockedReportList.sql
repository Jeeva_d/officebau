﻿/****************************************************************************             
CREATED BY  :  Ajith N        
CREATED DATE :  13 Nov 2017         
MODIFIED BY  :            
MODIFIED DATE   :       
 <summary>                    
    TotalHoursClockedReportList 4,1, 1, 106,'2'  
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[TotalHoursClockedReportList] (@Year         INT,
                                                     @MonthID      INT,
                                                     @DomainID     INT,
                                                     @UserID       INT,
                                                     @BusinessUnit VARCHAR(50))
AS
  BEGIN
      BEGIN TRY
          DECLARE @BusinessUnitIDs VARCHAR(50),
                  @YearCode        INT = (SELECT NAME
                     FROM   tbl_FinancialYear
                     WHERE  id = @Year
                            AND IsDeleted = 0
                            AND DomainID = @DomainID)

          IF( Isnull(@BusinessUnit, '') = '' )
            SET @BusinessUnitIDs = (SELECT BusinessUnitID
                                    FROM   tbl_EmployeeMaster
                                    WHERE  ID = @UserID
                                           AND DomainID = @DomainID)
          ELSE
            SET @BusinessUnitIDs =@BusinessUnit

          DECLARE @BusinessUnitTable TABLE
            (
               BusinessUnit INT
            )

          INSERT INTO @BusinessUnitTable
                      (BusinessUnit)
          SELECT Item
          FROM   Splitstring(@BusinessUnitIDs, ',')
          WHERE  Isnull(Item, '') <> ''

          SELECT Isnull(ET.EmpCodePattern, '') + ET.Code      AS EmployeeCode,
                 ET.Id                                        AS EmployeeId,
                 ET.FirstName + ' ' + Isnull(ET.LastName, '') AS EmployeeName,
                 Ltrim(Datediff(MINUTE, 0, AC.Duration))      AS Duration,
                 BT.NAME                                      AS BusinessUt,
                 REG.NAME                                     AS Region
          INTO   #temp
          FROM   tbl_EmployeeMaster ET
                 LEFT JOIN tbl_Attendance AC
                        ON ET.ID = AC.EmployeeID
                 LEFT JOIN tbl_BusinessUnit BT
                        ON ET.BaseLocationID = BT.ID
                 LEFT JOIN tbl_BusinessUnit REG
                        ON REG.ID = BT.ParentID
          WHERE  ( ISNULL(@YearCode, 0) = 0
                    OR Year(AC.LogDate) = @YearCode )
                 AND ( ISNULL(@MonthID, 0) = 0
                        OR Month(AC.LogDate) = @MonthID )
                 AND ET.BaseLocationID IN (SELECT BusinessUnit
                                           FROM   @BusinessUnitTable)
                 AND ET.IsDeleted = 0
                 AND ( ET.IsActive = 0
                        OR ( ET.IsActive = 1
                             AND Month(ET.InactiveFrom) >= @MonthID
                             AND Year(ET.InactiveFrom) >= @YearCode ) )
                 AND ET.DomainID = @DomainID

          SELECT EmployeeCode,
                 EmployeeId,
                 EmployeeName,
                 Sum(Cast(Duration AS INT))                                                                                                   AS [Minutes],
                 Replace(Cast(( Sum(Cast(Duration AS INT)) / 60 + ( Sum(Cast(Duration AS INT)) % 60 ) / 100.0 ) AS DECIMAL(18, 2)), '.', ':') AS Duration,
                 -- Replace(Cast(Sum(Cast(Duration AS DECIMAL(18, 2)) / 60.0)AS DECIMAL(18, 2)), '.', ':')  AS Duration,  
                 --CAST((CAST((CAST(CONVERT(VARCHAR, Sum(Cast(Duration AS INT))) AS int) / 60) AS varchar) + ' : '  + right('0' + CAST((CAST(CONVERT(VARCHAR, Sum(Cast(Duration AS INT))) AS int) % 60) AS varchar(2)),2)) AS varchar) AS Duration,  
                 BusinessUt                                                                                                                   AS [BusinessUnit],
                 Region
          FROM   #temp
          GROUP  BY EmployeeCode,
                    EmployeeId,
                    EmployeeName,
                    BusinessUt,
                    Region
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
