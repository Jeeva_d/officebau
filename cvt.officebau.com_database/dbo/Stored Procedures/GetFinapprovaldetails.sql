﻿/****************************************************************************     
CREATED BY   :  Naneeshwar.M  
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>   
          [GetFinapprovaldetails] 1,2  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetFinapprovaldetails] (@DomainID      INT,  
                                               @LoanRequestID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT LR.ID                                       AS ID,  
                 LR.StatusID                                 AS StatusID,  
                 LR.CreatedBy                                AS RequesterID,  
                 STA.Code                                    AS [Status],  
                 LR.CreatedOn                                AS RequestedOn,  
                 LR.LoanTypeID                               AS TypeID,  
                 LoanAmount                                  AS Amount,  
                 Isnull(EMP.EmpCodePattern, '') + Isnull(EMP.Code, '') + ' - ' + EMP.FullName AS Requester,  
                 LR.CreatedBy                                AS RequesterID,  
                 LR.Tenure                                   AS Tenure,  
                 LR.InterestRate                             AS RateofInterest,  
                 LR.TotalAmount                              AS TotalAmount,  
                 LR.ApproverRemarks                          AS ApprovedReason,  
                 LR.ApproverID                               AS ApproverID,  
                 EM.FullName                                 AS ModifiedBy,  
                 LR.FinancialApprovedDate                    AS ModifiedOn,  
                 ST.Code                                     AS FinApproverStatus,  
                 BU.NAME                                     AS BaseLocation,  
                 LR.ExpectedDate                             AS ExpectedDate,  
                 LR.Purpose                                  AS Purpose  
          FROM   tbl_LoanRequest LR  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = LR.CreatedBy  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = LR.FinancialApproverID  
                 LEFT JOIN tbl_Status STA  
                        ON STA.ID = LR.StatusID  
                 LEFT JOIN tbl_Status ST  
                        ON ST.ID = LR.FinancialStatusID  
                 LEFT JOIN tbl_BusinessUnit BU  
                        ON EM.BaseLocationID = BU.ID  
          WHERE  LR.ID = @LoanRequestID  
                 AND LR.IsDeleted = 0  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
