﻿/****************************************************************************   
CREATED BY   :  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
         [Rpt_EmployeeMenuList] 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_EmployeeMenuList] (@ModuleID INT,
                                               @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT DISTINCT EMP.code + ' - ' + EMP.FullName AS EmployeeName,
                          RSM.SubModule                   AS Module,
                          RBS.Menu                        AS Menu,
                          Isnull(RUM.MRead, 0)            AS MenuRead,
                          Isnull(RUM.MWrite, 0)           AS MenuWrite,
                          Isnull(RUM.MEdit, 0)            AS MenuEdit,
                          Isnull(RUM.MDelete, 0)          AS MenuDelete,
                          emp.ID
          FROM   tbl_RBSUserMenuMapping RUM
                 LEFT JOIN tbl_RBSMenu RBS
                        ON RBS.ID = RUM.MenuID And RBS.isdeleted=0
                 LEFT JOIN tbl_RBSSubModule RSM
                        ON RSM.ID = RBS.SubModuleID
                 JOIN tbl_EmployeeMaster EMP
                   ON EMP.ID = RUM.EmployeeID
                      AND Isnull(EMP.IsActive, 0) = 0
          WHERE  RUM.MenuID = @ModuleID
                 AND RUM.IsDeleted = 0
                 AND RUM.DomainID = @DomainID
                 AND ( RUM.MRead = 1
                        OR RUM.MEdit = 1
                        OR RUM.MWrite = 1
                        OR RUM.MDelete = 1 )
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
