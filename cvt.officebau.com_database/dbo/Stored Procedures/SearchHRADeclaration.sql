﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:
 <summary>     
		select * from HRA 
		[SearchHRADeclaration] 0,715,3,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchHRADeclaration] (@ID          INT,
                                              @EmployeeID  INT,
                                              @StartYearID INT,
                                              @DomainID    INT)
AS
  BEGIN
      SET NOCOUNT ON;
      SET @StartYearID=(SELECT FinancialYear
                        FROM   tbl_FinancialYear
                        WHERE  ID = @StartYearID
                               AND IsDeleted = 0
                               AND DomainID = @DomainID)

      DECLARE @RegionID INT=0

      BEGIN TRY
          SET @RegionID=(SELECT RegionID
                         FROM   tbl_EmployeeMaster
                         WHERE  id = @EmployeeID)

          DECLARE @MonthOpenedDate INT = (SELECT VALUE
             FROM   TDSConfiguration
             WHERE  IsDeleted = 0
                    AND Code = 'TDSMOD'
                    AND DomainID = @DomainID
                    AND RegionID = @RegionID)
          DECLARE @MonthClosedDate INT = (SELECT VALUE
             FROM   TDSConfiguration
             WHERE  IsDeleted = 0
                    AND Code = 'TDSMCD'
                    AND DomainID = @DomainID
                    AND RegionID = @RegionID)
          DECLARE @ProofOpenDate DATE = (SELECT VALUE
             FROM   TDSConfiguration
             WHERE  IsDeleted = 0
                    AND Code = 'TDSPOD'
                    AND DomainID = @DomainID
                    AND RegionID = @RegionID)
          DECLARE @ProofCloseDate DATE = (SELECT '31-Mar-' + Cast( Year(Getdate()) AS VARCHAR)
             FROM   TDSConfiguration
             WHERE  IsDeleted = 0
                    AND Code = 'TDSPCD'
                    AND DomainID = @DomainID
                    AND RegionID = @RegionID)
          DECLARE @IsMonthOpened BIT = CASE
              WHEN Datepart(dd, Getdate()) BETWEEN @MonthOpenedDate AND @MonthClosedDate THEN 1
              ELSE 0
            END
          DECLARE @IsProofOpen BIT = CASE
              WHEN CONVERT(DATE, Getdate()) BETWEEN @ProofOpenDate AND @ProofCloseDate THEN 1
              --WHEN @ProofCloseDate > CONVERT(DATE, Getdate()) THEN 1
              ELSE 0
            END
          DECLARE @FnYearID INT = CASE
              WHEN Month(Getdate()) < 4 THEN Year(Getdate()) - 1
              ELSE Year(Getdate())
            END

          SET @StartYearID = CASE
                               WHEN Isnull(@StartYearID, 0) != 0 THEN @StartYearID
                               ELSE
                                 CASE
                                   WHEN Month(Getdate()) < 4 THEN Year(Getdate()) - 1
                                   ELSE Year(Getdate())
                                 END
                             END

          DECLARE @IsCurrentFnYear BIT = CASE
              WHEN @FnYearID != @StartYearID THEN 0
              ELSE 1
            END

          SELECT Isnull(tds.ID, 0)                                                                                      ID,
                 MN.CODE + ' - ' + CONVERT(VARCHAR(10), CASE WHEN mn.id <4 THEN @StartYearID + 1 ELSE @StartYearID END) MonthYear,
                 ( CASE
                     WHEN mn.id < 4 THEN @StartYearID + 1
                     ELSE @StartYearID
                   END )                                                                                                YearID,
                 @StartYearID                                                                                           StartYearID,
                 mn.id                                                                                                  MonthID,
                 @EmployeeID                                                                                            EmployeeID,
                 tds.Declaration                                                                                        Declaration,
                 Isnull(tds.Submitted, 0)                                                                               Submitted,
                 Isnull(tds.Cleared, 0)                                                                                 Cleared,
                 Isnull(tds.Rejected, 0)                                                                                Rejected,
                 Isnull(tds.Remarks, NULL)                                                                              Remarks,
                 tds.ModifiedOn                                                                                         ModifiedOn,
                 @IsMonthOpened                                                                                         IsOpen,
                 @IsProofOpen                                                                                           IsProofOpen,
                 @IsCurrentFnYear                                                                                       IsFinancialYear
          FROM   HRA tds
                 RIGHT JOIN [tbl_Month] MN
                         ON tds.monthid = MN.ID
                            AND tds.[StartYearID] = @StartYearID
                            AND tds.EmployeeID = @EmployeeID
          WHERE  Isnull(tds.IsDeleted, 0) = 0
                 AND Isnull(tds.DomainID, @DomainID) = @DomainID
                 AND Isnull(tds.StartYearID, @StartYearID) = @StartYearID
          ORDER  BY MN.MAPID,
                    tds.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
