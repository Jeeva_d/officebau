﻿/****************************************************************************               
CREATED BY  :     
CREATED DATE      
MODIFIED BY       
MODIFIED DATE    
 <summary>            
 </summary>                                       
 *****************************************************************************/  
Create PROCEDURE [dbo].[SearchPayablesTileDetails] (
@TileType Varchar(100) = 'PAYABLES', @DomainID INT =1
)  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 BEGIN TRY  
 
 DECLARE @table TABLE (  
   DetailDescription VARCHAR(8000)  
     ,EID INT  
  )  
  
  
  INSERT INTO @table  
   SELECT  
    (SELECT  
      SUBSTRING((SELECT  
        +',' + ed.Remarks  
       FROM tblExpenseDetails ed  
       WHERE ed.Isdeleted = 0  
       AND ex.Id = ed.ExpenseID  
       FOR XML PATH (''))  
      , 2, 200000))  
      ,ex.Id  
   FROM tblExpense ex; 

Select e.ID,v.Name VendorName, sum(ep.qty*ep.Amount) + (Sum(CGST+SGST)) AS ExpenseAmount,ISNULL(PaidAmount,0)+ISNULL(Sum(eh.Amount),0) AS Paid,e.Date,e.DueDate INTO #Result from tblexpense e
Join tblExpenseDetails ep on e.ID = ep.ExpenseID and ep.IsDeleted=0
Join tblVendor v on e.vendorID = v.ID
left Join tblExpenseHoldings eh on eh.ExpenseId = e.ID and eh.IsDeleted=0
where e.IsDeleted=0 and e.domainID=@DomainID
group by PaidAmount,e.Date,v.Name,e.DueDate,e.ID

IF @TileType = 'PAYABLES'
Select VendorName, ExpenseAmount-Paid Payable,(SELECT  
      ISNULL(DetailDescription, '')  
     FROM @table d  
     WHERE d.EID = #Result.Id)  
    AS [Description] ,ExpenseAmount AS Booked, Paid PaidAmount,DueDate  from #Result 
ELSE
Select VendorName, ExpenseAmount-Paid Payable,(SELECT  
      ISNULL(DetailDescription, '')  
     FROM @table d  
     WHERE d.EID = #Result.Id)  
    AS [Description] ,ExpenseAmount AS Booked, Paid PaidAmount,DueDate  from #Result  where DueDate <getdate()
 END TRY  
 BEGIN CATCH  
  DECLARE @ErrorMsg VARCHAR(100)  
      ,@ErrSeverity TINYINT  
  SELECT  
   @ErrorMsg = ERROR_MESSAGE()  
     ,@ErrSeverity = ERROR_SEVERITY()  
  RAISERROR (@ErrorMsg, @ErrSeverity, 1)  
 END CATCH  
END
