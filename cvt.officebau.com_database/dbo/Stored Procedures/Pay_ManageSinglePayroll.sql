﻿   /****************************************************************************                                  
CREATED BY   :                                  
CREATED DATE  :                                  
MODIFIED BY   :                                  
MODIFIED DATE  :                                  
 <summary>                                  
</summary>                                  
*****************************************************************************/                          
CREATE PROCEDURE [dbo].[Pay_ManageSinglePayroll] (@Payroll    PAYROLL READONLY,                          
                                                  @Year       INT,                          
                                                  @MonthId    INT,                          
                                                  @DomainID   INT,                          
                                                  @EmployeeID INT,                          
                                                  @SessionId  INT,                 
              @BulkInsert int =0,                 
              @Remarks Varchar(Max)='')                          
AS                          
  BEGIN                          
      SET NOCOUNT ON;                          
                          
      DECLARE @Formula VARCHAR(MAX)                          
                    Declare @LopAmount Decimal(18,6) =(select Sum(p.amount) from tbl_Pay_PayrollCompontents AS tppc                          
                 LEFT JOIN @Payroll p                          
                        ON tppc.Id = p.ComponentId                          
                 LEFT JOIN tbl_Pay_EmployeePayStructure AS tpeps                          
                        ON tpeps.Id = p.EmployeePayStructureID                          
                 LEFT JOIN tbl_Pay_PayrollCompanyPayStructure AS tppcps                          
        ON tppc.Id = tppcps.ComponentId                          
                           AND tppcps.IsDeleted = 0                          
                           AND tppcps.CompanyPayStructureID = tpeps.CompanyPayStructureID                          
                 JOIN tbl_Pay_EmployeePayStructureDetails AS tpepsd                          
                   ON tpeps.Id = tpepsd.PayStructureId                          
                      AND tpepsd.ComponentId = tppcps.ComponentId                          
                      AND tpepsd.Isdeleted = 0                          
          WHERE  tppc.Isdeleted = 0                          
                 AND tppc.DomainId = @DomainID and  tppcps.Fixed  =1 )      
      SET @Year = (SELECT NAME                          
                   FROM   tbl_FinancialYear                          
                   WHERE  id = @Year                          
                          AND IsDeleted = 0                          
                          AND DomainID = @DomainID)                          
                          
      BEGIN TRY                          
          BEGIN TRANSACTION                          
                          
          ----- Check for reference ------------                                  
          DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'                          
            + CONVERT(VARCHAR(10), @Year);                          
          DECLARE @Workdays INT =  Datediff(DAY, @CurrentMonth, Dateadd(MONTH, 1, @CurrentMonth));                          
           DECLARE @LOP DECIMAL=(                            
    SELECT                                 
                     ( CASE                                  
                         WHEN ( Datepart(MM, DOJ) = @MonthId                                  
                                AND Datepart(YYYY, DOJ) = @Year ) THEN                                  
                           ( @Workdays - ( Datepart(D, DOJ) - 1 ) - Isnull(LopDays, 0) )        
                         ELSE                                  
                           @Workdays - Isnull(LopDays, 0)                                  
                       END ) - ( CASE                                  
                                   WHEN ( Datepart(MM, InactiveFrom) = @MonthId                                  
                                          AND Datepart(YYYY, InactiveFrom) = @Year ) THEN                                  
                                     ( ( @Workdays ) - Day(InactiveFrom) )                                  
                                   ELSE                                  
                                     0                                  
                                 END )                                                       
                                               
              FROM   tbl_employeemaster ep                                  
                     LEFT JOIN tbl_LopDetails P                                  
                            ON P.EmployeeID = ep.ID                                  
                               AND P.DomainID = @DomainID                                  
                               AND P.MonthId = @MonthId                                  
                               AND P.year = @Year     where ep.ID=@EmployeeID)                          
                          
          SELECT tppc.Id,                          
                 tppc.Code,                          
                 tppc.Description,                          
                 tppc.Ordinal,         
                 tppc.Id ComponentId,                          
                 tppcps.value,                          
                 CASE                       
                   WHEN ( tppc.Description = 'Gross' ) THEN                          
                     Cast(Cast(p.Amount as decimal(16,2)) AS VARCHAR(MAX))                          
                   ELSE                          
                     CASE                          
                       WHEN ( tppcps.IsEditable = 1 ) THEN          
        Case when(fixed = 0) Then                     
                         Cast(ISNULL(Cast(p.Amount as decimal(16,2)), 0) AS VARCHAR(MAX))          
       Else        
       Cast( tpepsd.Amount AS VARCHAR(MAX))        
       END                        
                       ELSE                          
               ISNULL(tppcps.value, '')                          
                     END                          
                 END     AS Amount,                          
   0       AS HasUpdated,                          
                 tppcps.IsEditable  , tppcps.Fixed                          
          INTO   #tblcompanypay                          
          FROM   tbl_Pay_PayrollCompontents AS tppc                          
                 LEFT JOIN @Payroll p                          
                        ON tppc.Id = p.ComponentId                          
                 LEFT JOIN tbl_Pay_EmployeePayStructure AS tpeps                          
                        ON tpeps.Id = p.EmployeePayStructureID                          
                 LEFT JOIN tbl_Pay_PayrollCompanyPayStructure AS tppcps                          
  ON tppc.Id = tppcps.ComponentId                          
                           AND tppcps.IsDeleted = 0                          
                           AND tppcps.CompanyPayStructureID = tpeps.CompanyPayStructureID                          
                 JOIN tbl_Pay_EmployeePayStructureDetails AS tpepsd                          
                   ON tpeps.Id = tpepsd.PayStructureId                          
                      AND tpepsd.ComponentId = tppcps.ComponentId                          
                      AND tpepsd.Isdeleted = 0                          
          WHERE  tppc.Isdeleted = 0                          
                 AND tppc.DomainId = @DomainID              
                          
          ----------------------------------------                                  
          DECLARE @ID    INT,                          
                  @count INT = (SELECT Count(1)                        
                     FROM   #tblcompanypay);           
      --IF(@BulkInsert =0)                 
      -- update  #tblcompanypay set amount = '(' + Cast(amount AS varchar(max)) + '*(' +Cast(@LOP AS varchar) +'/'+ Cast(Cast(@Workdays AS Decimal(16,1))AS varchar) +'))'  Where Fixed=1     and IsEditable=0            
      -- Else            
      -- update  #tblcompanypay set amount = '(' + Cast(amount AS  varchar(max)) + '*(' +Cast(@LOP AS varchar) +'/'+ Cast(Cast(@Workdays AS Decimal(16,1))AS varchar) +'))'  Where Fixed=1            
          
          ----------------------------------------                                  
          WHILE ( @count <> 0 )                          
            BEGIN                          
                SET @ID = (SELECT TOP 1 Id                          
                           FROM   #tblcompanypay                          
                           WHERE  hasupdated = 0                          
                           ORDER  BY Ordinal);                          
                          
                -----------------------------------------                                  
                UPDATE #tblcompanypay                          
                SET    hasupdated = 1                          
                WHERE  Id = @ID;                          
                      
     UPDATE #tblcompanypay                          
                SET    Amount = Replace(P.Amount, (SELECT Code                          
                                                   FROM   #tblcompanypay                          
                                                   WHERE  Id = @ID), (SELECT Amount                          
                          FROM   #tblcompanypay                          
                                                                      WHERE  Id = @ID))                          
                FROM   #tblcompanypay P where P.Fixed = 1 ;          
                      
                -----------------------                                  
                SET @count = @count - 1;                          
            END;                          
                          
          SET @count = (SELECT Count(1)                          
                        FROM   #tblcompanypay);                          
    WHILE ( @count <> 0 )                          
            BEGIN                          
                SET @ID = (SELECT TOP 1 Id                          
                           FROM   #tblcompanypay                          
                           WHERE  hasupdated = 1                          
                           and Amount NOT LIKE '%[a-zA-Z][a-zA-Z]%');   
		 IF @ID is null  
        Set @ID= (Select ID FROM   #tblcompanypay      
                               WHERE  hasupdated = 1 and amount like '%Case %') 				   
						   
						                          
                UPDATE #tblcompanypay                          
                SET    hasupdated = 0                          
                WHERE  Id = @ID;                          
                          
                SET @Formula = (SELECT Amount                          
                                FROM   #tblcompanypay                          
                                WHERE  Id = @ID);                          
                          
                DECLARE @Input VARCHAR(MAX) = '(' +@Formula + ')';                          
                DECLARE @output        VARCHAR(MAX),                          
                        @resultdecimal NVARCHAR(MAX);                          
                DECLARE @i INT = 0;                          
                          
                WHILE @i < Len(@Input)                          
                  BEGIN                          
                      SET @i = @i + 1;                          
                          
                      IF ( dbo.isReallyNumeric(Substring(@Input, @i, 1)) IS NOT NULL )                          
                        BEGIN                          
                            SET @output = ISNULL(@output, '') + Substring(@Input, @i, 1);                          
                        END;                          
                      ELSE                          
      BEGIN                          
                            SET @resultdecimal = ISNULL(@resultdecimal, '')                          
                                                 + ISNULL(@output, '')                          
                           + Substring(@Input, @i, 1);                          
                            SET @output = '';                  
                        END;                          
                  END;                          
                          
                SET @Formula = @resultdecimal;                          
                SET @resultdecimal = '';                          
                          
                DECLARE @sql NVARCHAR(MAX);                          
                DECLARE @result DECIMAL(18, 2);                          
                          
                SET @sql = N'set @result = ' + @Formula;                          
                          
                EXEC sp_executesql                          
                  @sql,                          
                  N'@result DECIMAL(16,2) output',                          
                  @result OUT;                          
                          
                UPDATE #tblcompanypay                          
                SET    Amount = @result                       
                WHERE  Id = @ID;           
                           
                   UPDATE #tblcompanypay          
                    SET    amount =  Case when (fixed=1 and @WorkDays<>0 ) Then  @result * @lop/@WorkDays Else @result END          
                    WHERE  ID = @ID;         
              
     UPDATE p          
                    SET    p.amount = Replace(P.amount, (SELECT Code          
                                                       FROM   #tblcompanypay          
                                                       WHERE  ID = @ID), (SELECT amount          
                                                             FROM   #tblcompanypay          
                                                                          WHERE  ID = @ID))          
                    FROM   #tblcompanypay P where p.Fixed = 0;        
        
                SET @count = @count - 1;                          
            END;                          
                          
   set @LopAmount = @LopAmount - (select Sum(ISNULL(CAST(amount as decimal(18,6)),0)) from #tblcompanypay where fixed = 1)        
     update #tblcompanypay set amount=@LopAmount  where code ='LOPAMOUNT'            
                                   
          DECLARE @PayID INT,                          
                  @EmpID INT                          
                          
          SET @PayID = (SELECT TOP 1 EmployeePayStructureID                          
                        FROM   @Payroll);                          
          SET @EmpID = (SELECT TOP 1 EmployeeId                          
                   FROM   @Payroll)                          
                          
        IF EXISTS (SELECT Id                          
                     FROM   @Payroll                          
                     WHERE  Id <> 0)                          
            BEGIN                          
                           
     Update tbl_Pay_EmployeePayroll set Remarks=@Remarks ,ModifiedBy=@SessionId,ModifiedOn=getdate()                      
     Where ID = (Select PayrollId from tbl_Pay_EmployeePayrollDetails where ID =(Select Top 1 ID from @Payroll))                      
                      
                UPDATE tbl_Pay_EmployeePayrollDetails                          
                SET    Amount = Round(T.Amount,0)    ,ModifiedBy=@SessionId,ModifiedOn=getdate()                      
                FROM   tbl_Pay_EmployeePayrollDetails u                          
        LEFT JOIN @Payroll p                          
                              ON p.Id = u.Id                          
                                 AND u.ComponentId = p.ComponentId                          
                       LEFT JOIN #tblcompanypay AS t                          
                     ON p.ComponentId = t.ComponentId                          
                WHERE  u.ID = p.ID                          
                           
                      
                SELECT 'Updated Successfully'                          
            END                          
          ELSE                          
            BEGIN                          
                INSERT INTO tbl_Pay_EmployeePayroll                          
                            (EmployeeId,               
                             EmployeePayStructureID,                          
                             MonthId,                          
                             YearId,                          
                             Isdeleted,                          
                             CreatedBy,                          
                             CreatedOn,                          
                             ModifiedBy,                          
                             ModifiedOn,                          
   DomainId,Remarks)                          
                SELECT @EmpID,                          
                       @PayID,                          
                       @MonthId,                          
                       @Year,                          
                       0,                          
                       @SessionId,                          
                       Getdate(),                          
                       @SessionId,                          
                       Getdate(),                          
                       @DomainID ,@Remarks                         
                          
         SET @ID = @@IDENTITY                          
                          
                INSERT INTO tbl_Pay_EmployeePayrollDetails                          
                            (tbl_Pay_EmployeePayrollDetails.PayrollId,                          
                             tbl_Pay_EmployeePayrollDetails.ComponentId,                          
                             tbl_Pay_EmployeePayrollDetails.Amount,                          
                             tbl_Pay_EmployeePayrollDetails.Isdeleted,                          
                             tbl_Pay_EmployeePayrollDetails.CreatedBy,                          
                             tbl_Pay_EmployeePayrollDetails.ModifiedOn,                          
                             tbl_Pay_EmployeePayrollDetails.ModifiedBy,                          
                             tbl_Pay_EmployeePayrollDetails.CreatedOn)                          
                SELECT @ID,                          
                       T.ComponentId,                          
                       Round(T.Amount,0),                          
                       0,                          
                       @SessionId,                          
                       Getdate(),                          
                       @SessionId,                          
                       Getdate()                          
                FROM   #tblcompanypay AS t                          
                       JOIN @Payroll p                       
                         ON p.ComponentId = t.ComponentId                          
                          
                SELECT 'Inserted Successfully'                          
            END                          
                       
          DROP TABLE #tblcompanypay;                          
              
          COMMIT TRANSACTION                          
      END TRY                          
      BEGIN CATCH                          
          ROLLBACK TRANSACTION                          
          DECLARE @ErrorMsg    VARCHAR(100),                          
                  @ErrSeverity TINYINT                          
          SELECT @ErrorMsg = ERROR_MESSAGE(),                          
                 @ErrSeverity = ERROR_SEVERITY()                          
          RAISERROR (@ErrorMsg,@ErrSeverity,1)                          
      END CATCH          
  END
