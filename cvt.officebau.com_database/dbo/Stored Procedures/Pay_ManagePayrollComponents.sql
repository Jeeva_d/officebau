﻿
/****************************************************************************         
CREATED BY   : Jeeva        
CREATED DATE  :         
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>        
  
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_ManagePayrollComponents] (@ID        INT,
                                                      @Name      VARCHAR(100),
                                                      @Remarks   VARCHAR(1000),
                                                      @Ordinal   INT,
                                                      @IsDeleted BIT,
                                                      @SessionID INT,
                                                      @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(1000)= '';

      BEGIN TRY
          BEGIN TRANSACTION;

          --IF EXISTS (SELECT TOP 1 id
          --           FROM   tbl_Pay_PayrollCompontents
          --           WHERE  ID <> @ID
          --                  AND Ordinal = @Ordinal
          --                  AND IsDeleted = 0
          --                  AND DomainID = @DomainID)
          --  BEGIN
          --      SET @Output = (SELECT [Message]
          --                     FROM   tblErrorMessage
          --                     WHERE  [Type] = 'Warning'
          --                            AND Code = 'RCD_EXIST'
          --                            AND IsDeleted = 0);

          --      GOTO FINISH;
          --  END;

          IF( @IsDeleted = 1 )
            BEGIN
                IF EXISTS (SELECT TOP 1 id
                           FROM   tbl_Pay_PayrollCompanyPayStructure AS tpcps
                           WHERE  IsDeleted = 0
                                  AND tpcps.ComponentID = @ID
                                  AND tpcps.DomainID = @DomainID)
                  BEGIN
                      SET @Output = (SELECT [Message]
                                     FROM   tblErrorMessage
                                     WHERE  [type] = 'Warning'
                                            AND Code = 'RCD_REF'
                                            AND IsDeleted = 0);
                  END;
                ELSE
                  BEGIN
                      UPDATE tbl_Pay_PayrollCompontents
                      SET    IsDeleted = 1,
                             ModifiedBy = @SessionID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID;

                      SET @Output = (SELECT [Message]
                                     FROM   tblErrorMessage
                                     WHERE  [Type] = 'Information'
                                            AND Code = 'RCD_DEL'
                                            AND IsDeleted = 0);
                  END;

                GOTO FINISH;
            END;
          ELSE
            BEGIN
                IF EXISTS (SELECT TOP 1 id
                           FROM   tbl_Pay_PayrollCompontents
                           WHERE  ID <> @ID
                                  AND IsDeleted = 0
                                  AND Code = @Name
                                  AND DomainID = @DomainID)
                  BEGIN
                      SET @Output = (SELECT [Message]
                                     FROM   tblErrorMessage
                                     WHERE  [Type] = 'Warning'
                                            AND Code = 'RCD_EXIST'
                                            AND IsDeleted = 0);

                      GOTO FINISH;
                  END;

                IF( ISNULL(@ID, 0) = 0 )
                  BEGIN
                      INSERT INTO tbl_Pay_PayrollCompontents
                                  (Code,
                         Description,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   Ordinal,
                                   HistoryID,
                                   DomainID)
                      SELECT @Name,
                             @Remarks,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             @Ordinal,
                             Newid(),
                             @DomainID;

                      SET @Output = (SELECT [Message]
                                     FROM   tblErrorMessage
                                     WHERE  [Type] = 'Information'
                                            AND Code = 'RCD_INS'
                                            AND IsDeleted = 0);

                      GOTO FINISH;
                  END;
                ELSE
                  BEGIN
                      UPDATE tbl_Pay_PayrollCompontents
                      SET    code = @Name,
                             Description = @Remarks,
                             Ordinal = @Ordinal,
                             ModifiedBy = @SessionID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID;

                      SET @Output = (SELECT [Message]
                                     FROM   tblErrorMessage
                                     WHERE  [Type] = 'Information'
                                            AND Code = 'RCD_UPD'
                                            AND IsDeleted = 0);

                      GOTO FINISH;
                  END;
            END;

          FINISH:

          SELECT @Output;

          COMMIT TRANSACTION;
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION;
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY();
          RAISERROR(@ErrorMsg,@ErrSeverity,1);
      END CATCH;
  END; 
