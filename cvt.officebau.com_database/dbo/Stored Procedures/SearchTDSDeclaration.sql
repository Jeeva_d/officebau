﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>       
  select * from TDSDeclaration  
  SearchTDSDeclaration 12,4,0,1  
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchTDSDeclaration] (@EmployeeID INT,
                                               @StartYear  INT,
                                               @IsApprover BIT,
                                               @DomainID   INT,
                                               @Type       VARCHAR(20) = '')
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @FYID INT = @StartYear

      SET @StartYear=(SELECT FinancialYear
                      FROM   tbl_FinancialYear
                      WHERE  ID = @StartYear
                             AND IsDeleted = 0
                             AND DomainID = @DomainID)

      DECLARE @RegionID INT=0

      BEGIN TRY
          SET @RegionID=(SELECT RegionID
                         FROM   tbl_EmployeeMaster
                         WHERE  id = @EmployeeID)

          DECLARE @TempDeclarationTable TABLE
            (
               ID              INT,
               SectionID       INT,
               Section         VARCHAR(1000),
               ComponentID     INT,
               Component       VARCHAR(1000),
               TypeID          INT,
               [Type]          VARCHAR(20),
               Declaration     MONEY,
               Submitted       MONEY,
               Cleared         MONEY,
               Rejected        MONEY,
               Remarks         VARCHAR(8000),
               IsCleared       INT,
               ApproverRemarks VARCHAR(8000)
            )
          DECLARE @TempComponentTable TABLE
            (
               Component VARCHAR(100),
               TypeID    INT,
               [Type]    VARCHAR(20)
            )
          DECLARE @MonthOpenedDate INT = (SELECT VALUE
             FROM   TDSConfiguration
             WHERE  IsDeleted = 0
                    AND Code = 'TDSMOD'
                    AND DomainID = @DomainID
                    AND RegionID = @RegionID)
          DECLARE @MonthClosedDate INT = (SELECT VALUE
             FROM   TDSConfiguration
             WHERE  IsDeleted = 0
                    AND Code = 'TDSMCD'
                    AND DomainID = @DomainID
                    AND RegionID = @RegionID)
          DECLARE @ProofCloseDate DATE = (SELECT '31-Mar-'
                    + Cast( Year(Getdate()) AS VARCHAR)
             FROM   TDSConfiguration
             WHERE  IsDeleted = 0
                    AND Code = 'TDSPCD'
                    AND DomainID = @DomainID
                    AND RegionID = @RegionID)
          DECLARE @ProofOpenDate DATE = (SELECT VALUE
             FROM   TDSConfiguration
             WHERE  IsDeleted = 0
                    AND Code = 'TDSPOD'
                    AND DomainID = @DomainID
                    AND RegionID = @RegionID)
          DECLARE @IsMonthOpened BIT = CASE
              WHEN Datepart(dd, Getdate()) BETWEEN @MonthOpenedDate AND @MonthClosedDate THEN
                1
              ELSE
                0
            END
          DECLARE @IsProofOpen BIT = CASE
              WHEN CONVERT(DATE, Getdate()) BETWEEN @ProofOpenDate AND @ProofCloseDate THEN
                1
              --WHEN @ProofCloseDate > CONVERT(DATE, Getdate()) THEN 1  
              ELSE
                0
            END

          SET @StartYear = CASE
                             WHEN Isnull(@StartYear, 0) != 0 THEN
                               @StartYear
                             ELSE
                               CASE
                                 WHEN Month(Getdate()) < 4 THEN
                                   Year(Getdate()) - 1
                                 ELSE
                                   Year(Getdate())
                               END
                           END

          DECLARE @YearID INT = CASE
              WHEN Month(Getdate()) < 4 THEN
                Year(Getdate()) - 1
              ELSE
                Year(Getdate())
            END
          DECLARE @IsCurrentFnYear BIT = CASE
              WHEN @YearID != @StartYear THEN
                0
              ELSE
                1
            END
          DECLARE @StartMonth INT = 4 -- Fiscal year month starting  
          DECLARE @StartDate DATE = Dateadd(MONTH, @StartMonth - 1, Dateadd(YEAR, @StartYear - 1900, 0)) -- Fiscal Year Start date  
          DECLARE @EndDate DATE = Dateadd(DAY, -1, Dateadd(MONTH, @StartMonth - 1, Dateadd(YEAR, ( @StartYear + 1 ) - 1900, 0))) -- Fiscal Year End date  

          IF @IsApprover = 0 ----- Requester -----  
            BEGIN
                INSERT INTO @TempDeclarationTable ----- TDS Declaration ----  
                SELECT Isnull(DECL.ID, 0),
                       COMP.ID,
                       SEC.Code,
                       COMP.ID,
                       COMP.Code,
                       0,
                       NULL,
                       Declaration,
                       Submitted,
                       Cleared,
                       Isnull(Rejected, 0),
                       Remarks,
                       CASE
                         WHEN ( Cleared IS NULL ) THEN
                           0
                         ELSE
                           1
                       END AS IsCleared,
                       ApproverRemarks
                FROM   TDSComponent COMP
                       LEFT JOIN TDSDeclaration DECL
                              ON COMP.ID = DECL.ComponentID
                                 AND DECL.IsDeleted = 0
                                 AND EmployeeID = @EmployeeID
                                 AND CONVERT (DATE, DECL.ModifiedOn) BETWEEN @StartDate AND @EndDate
                       JOIN TDSSection SEC
                         ON SEC.ID = COMP.SectionID
                WHERE  Isnull(COMP.IsDeleted, 0) = 0
                       AND Isnull(COMP.DomainID, 0) = @DomainID
                       AND COMP.FYID = @FYID
                ORDER  BY SEC.Ordinal ASC
            END

          IF @IsApprover = 1 ----- Approver ----  
            BEGIN
                INSERT INTO @TempDeclarationTable ----- TDS Declaration ----  
                SELECT DECL.ID,
                       COMP.SectionID,
                       SEC.Code,
                       ComponentID,
                       COMP.Code,
                       0,
                       NULL,
                       Declaration,
                       Submitted,
                       Cleared,
                       Rejected,
                       Remarks,
                       CASE
                         WHEN ( Cleared IS NULL ) THEN
                           0
                         ELSE
                           1
                       END AS IsCleared,
                       ApproverRemarks
                FROM   TDSDeclaration DECL
                       JOIN TDSComponent COMP
                         ON COMP.ID = DECL.ComponentID
                            AND COMP.FYID = @FYID
                       JOIN TDSSection SEC
                         ON SEC.ID = COMP.SectionID
                WHERE  DECL.IsDeleted = 0
                       AND DECL.EmployeeID = @EmployeeID
                       AND Isnull(DECL.Submitted, 0) != 0
                       AND DECL.DomainID = @DomainID
                ORDER  BY SEC.Ordinal ASC
            END

          IF( @Type = 'HOUSING' )
            BEGIN
                SELECT *,
                       @IsMonthOpened   IsOpen,
                       @IsProofOpen     IsProofOpen,
                       @IsCurrentFnYear IsCurrentFY
                FROM   @TempDeclarationTable
                WHERE  [Type] IS NULL
            END
          ELSE IF( @Type = 'IT' )
            BEGIN
                SELECT *,
                       @IsMonthOpened   IsOpen,
                       @IsProofOpen     IsProofOpen,
                       @IsCurrentFnYear IsCurrentFY
                FROM   @TempDeclarationTable
                WHERE  [Type] IS NOT NULL
            END
          ELSE
            BEGIN
                SELECT *,
                       @IsMonthOpened   IsOpen,
                       @IsProofOpen     IsProofOpen,
                       @IsCurrentFnYear IsCurrentFY
                FROM   @TempDeclarationTable
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
