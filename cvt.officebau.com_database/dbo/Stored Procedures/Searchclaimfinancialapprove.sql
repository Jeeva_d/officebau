﻿/****************************************************************************     
CREATED BY   :  Naneeshwar.M  
CREATED DATE  :     
MODIFIED BY   :   Ajith N  
MODIFIED DATE  :   04 Dec 2014  
 <summary>   
          [Searchclaimfinancialapprove] 1,0,71, 'search'  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchclaimfinancialapprove] (@DomainID      INT,  
                                                     @StatusID      INT,  
                                                     @EmployeeID    INT,  
                                                     @OperationType VARCHAR(20))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Status VARCHAR(100)  
  
      IF( @OperationType = 'pageload' )  
        SET @Status = 'Submitted'  
      ELSE IF( @OperationType = 'search'  
          AND @StatusID = 0 )  
        SET @Status = 'Submitted,Approved,Rejected'  
  
      BEGIN TRY  
          BEGIN  
              --DECLARE @Submitted INT = (SELECT ID  
              --   FROM   tbl_Status  
              --   WHERE  Code = 'Submitted'  
              --          AND [Type] = 'Claims'  
              --          AND IsDeleted = 0);  
              WITH CTE  
                   AS (SELECT Cast(CLA.CreatedOn AS DATE)                  AS ApprovedOn,  
                              Cast(CLR.SubmittedDate AS DATE)              AS SubmittedDate,  
                              --CLA.ApprovedAmount                           AS Amount,  
                              ( CASE  
                                  WHEN STA.Code IS NULL THEN  
                                    CLA.ApprovedAmount  
                                  ELSE  
                                    CFA.FinApprovedAmount  
                                END )                                      AS Amount,  
                               Isnull(CRE.EmpCodePattern, '') +  Isnull(CRE.Code, '') + ' - ' + CRE.FullName AS ApprovarName,  
                                Isnull(EMP.EmpCodePattern, '') + Isnull(EMP.Code, '') + ' - ' + EMP.FullName AS Requester,  
                              CLA.CreatedBy                                AS ApprovalID,  
                              CLA.RequesterID                              AS RequesterID,  
                              -- Count(*)                                       AS Counts,  
                              ( CASE  
                                  WHEN STA.Code IS NULL THEN  
                                    CLAA.Code  
                                  ELSE  
                                    STA.Code  
                                END )                                      AS Status,  
                              Isnull(CFA.StatusID, 0)                      AS StatusID  
                       FROM   tbl_ClaimsRequest CLR  
                              LEFT JOIN tbl_ClaimsApprove CLA  
                                     ON CLR.ID = CLA.ClaimItemID  
                              LEFT JOIN tbl_ClaimsFinancialApprove CFA  
                                     ON CLR.id = CFA.ClaimItemID  
                              LEFT JOIN tbl_EmployeeMaster CRE  
                                     ON CRE.ID = CLA.CreatedBy  
                              LEFT JOIN tbl_EmployeeMaster EMP  
                                     ON EMP.ID = CLR.CreatedBy  
                              LEFT JOIN tbl_Status STA  
                                     ON STA.ID = CFA.StatusID  
                              LEFT JOIN tbl_Status CLAA  
                                     ON CLAA.ID = CLA.FinApproverStatusID  
                       WHERE  FinancialApproverID = @EmployeeID  
                              AND CLR.IsDeleted = 0  
                              AND ( ( Isnull(@StatusID, 0) = 0  
                                      AND CLA.FinApproverStatusID IN((SELECT ID  
                                                                      FROM   tbl_Status  
                                                                      WHERE  Code IN(SELECT Splitdata  
                                                                                     FROM   FnSplitString(@Status, ','))  
                                                                             AND [Type] = 'Claims'  
                                                                             AND IsDeleted = 0)) )  
                                     OR CLA.FinApproverStatusID = @StatusID )  
                              AND CLR.DomainID = @DomainID)  
              SELECT ApprovedOn                               AS ApprovedOn,  
                     --SubmittedDate AS SubmittedDate,  
                     Sum(Amount)                              AS Amount,  
                     Requester                                AS Requester,  
                     ApprovarName                             AS ApprovarName,  
                     ApprovalID                               AS ApprovalID,  
                     RequesterID                              AS RequesterID,  
                     Count(1)                                 AS Counts,  
                     Status                                   AS Status,  
                     StatusID                                 AS StatusID,  
                     Cast(ROW_NUMBER()  
                            OVER(  
                              ORDER BY Requester ASC) AS INT) AS RowNo  
              FROM   CTE  
              GROUP  BY ApprovedOn,  
                        --SubmittedDate,  
                        Requester,  
                        ApprovarName,  
                        ApprovalID,  
                        RequesterID,  
                        Status,  
                        StatusID  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
