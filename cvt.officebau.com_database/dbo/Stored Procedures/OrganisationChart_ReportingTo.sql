﻿/****************************************************************************     
CREATED BY   :   Naneeshwar.M  
CREATED DATE  :   18-DEC-2017  
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [Organisationchart_reportingto] 'true', 1, 2  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[OrganisationChart_ReportingTo](@IsActive BIT = NULL,
                                                      @DomainID INT,
                                                      @UserID   INT)
AS
  BEGIN
      BEGIN TRY
          CREATE TABLE #TMPORG
            (
               NodeID          INT IDENTITY(1, 1),
               ParentID        INT,
               EmployeeName    VARCHAR(100),
               DesignationName VARCHAR(100),
               EmailID         VARCHAR(100),
               ContactNo       VARCHAR(100),
               ProfileImages   VARCHAR(max),
               Gender          VARCHAR(100),
               EmpID           INT,
               IsActive        BIT
            )

          INSERT INTO #TMPORG
          SELECT NULL                                       AS ParentID,
                 e.FirstName + ' ' + Isnull(e.LastName, '') AS EmployeeName,
                 d.NAME                                     AS DesignationName,
                 e.EmailID                                  AS EmailID,
                 e.ContactNo,
                 FU.FileName                                AS ProfileImages,
                 cd.Code                                    AS Gender,
                 e.ID                                       AS EmpID,
                 e.IsActive                                 AS IsActive
          FROM   tbl_EmployeeMaster e
                 LEFT JOIN tbl_Designation de
                        ON de.ID = e.DesignationID
                 LEFT JOIN tbl_Designation d
                        ON d.ID = e.DesignationID
                 LEFT JOIN tbl_FileUpload fu
                        ON fu.Id = e.FileID
                           AND ReferenceTable = 'tbl_EmployeeMaster'
                 LEFT JOIN tbl_CodeMaster cd
                        ON cd.ID = e.GenderID
          WHERE  e.IsActive = 0
                 AND e.IsDeleted = 0
                 AND e.id = @UserID

          DECLARE @Count    INT = 1,
                  @EmpID    INT,
                  @ParentID INT

          WHILE( @Count <= (SELECT Count(1)
                            FROM   #TMPORG) )
            BEGIN
                SET @EmpID = (SELECT TOP 1 EmpID
                              FROM   #TMPORG
                              WHERE  NodeID = @Count)
                SET @ParentID = (SELECT TOP 1 NodeID
                                 FROM   #TMPORG
                                 WHERE  NodeID = @Count)

                IF( (SELECT Count(1)
                     FROM   #TMPORG
                     WHERE  EmpID = @EmpID) < 2 )
                  BEGIN
                      INSERT INTO #TMPORG
                      SELECT @ParentID                                    AS ParentID,
                             er.FirstName + ' ' + Isnull(er.LastName, '') AS EmployeeName,
                             d.NAME                                       AS DesignationName,
                             er.EmailID                                   AS EmailID,
                             er.ContactNo,
                             fu.Filename                                  AS ProfileImages,
                             cd.Code                                      AS Gender,
                             er.ID                                        AS EmpID,
                             er.IsActive                                  AS IsActive
                      FROM   tbl_EmployeeMaster e
                             LEFT JOIN tbl_EmployeeMaster er
                                    ON er.ReportingToID = e.id
                             LEFT JOIN tbl_Designation de
                                    ON de.ID = e.DesignationID
                             LEFT JOIN tbl_Designation d
                                    ON d.ID = er.DesignationID
                             LEFT JOIN tbl_FileUpload fu
                                    ON fu.Id = er.FileID
                                       AND ReferenceTable = 'tbl_EmployeeMaster'
                             LEFT JOIN tbl_CodeMaster cd
                                    ON cd.ID = er.GenderID
                      WHERE  e.DomainID = @DomainID
                             AND e.IsDeleted = 0
                             AND er.ReportingToID IS NOT NULL
                             AND e.IsActive = 0
                             AND er.ReportingToID = @EmpID
                             AND ( ISNULL(@IsActive, 1) = 1
                                    OR ( er.IsActive = @IsActive ) )
                  END

                SET @Count = @Count + 1
            END

          SELECT *
          FROM   #TMPORG
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
