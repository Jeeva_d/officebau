﻿/****************************************************************************   
CREATED BY    : Ajith N  
CREATED DATE  : 04 Jul 2018  
MODIFIED BY   : 
MODIFIED DATE : 
 <summary>      
      SearchAutoCompleteAuditTableName '','',1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchAutoCompleteAuditTableName] (@DBName    VARCHAR(200),
                                                           @TableName VARCHAR(200),
                                                           @UserID    INT,
                                                           @DomainID  INT)
AS
  BEGIN
      BEGIN TRY
          --SELECT DISTINCT ST.TABLE_NAME,
          --                ST.TABLE_CATALOG
          --INTO   #tempAuditTables
          --FROM   INFORMATION_SCHEMA.TABLES BT
          --       JOIN INFORMATION_SCHEMA.TABLES ST
          --         ON BT.TABLE_NAME LIKE '%' + ST.TABLE_NAME + '%'
          --            AND ST.TABLE_NAME NOT LIKE '%_TriggerAudit%'
          --WHERE  BT.TABLE_TYPE = 'BASE TABLE'
          --       AND ( @DBName = ''
          --              OR BT.TABLE_CATALOG = @DBName )
          --       AND BT.TABLE_NAME LIKE '%_TriggerAudit%'
          --SELECT TOP 50 TABLE_NAME AS NAME
          --FROM   #tempAuditTables
          --WHERE  TABLE_NAME LIKE '%' + @TableName + '%'
          SELECT TOP 50 ID,
                        NAME
          FROM   tbl_AuditTables
          WHERE  ( @TableName = ''
                    OR NAME LIKE '%' + @TableName + '%' )
          ORDER  BY NAME
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
