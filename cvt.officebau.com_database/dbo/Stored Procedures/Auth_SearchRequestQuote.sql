﻿/**************************************************************************** 
CREATED BY			:	Priya K
CREATED DATE		:	30-11-2018
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_SearchRequestQuote]
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY				
		SELECT 
			ID				AS ID,
			Client_Name		AS ClientName,
			Client_Email			AS ClientEmail,
			Client_Expectation	AS ClientExpectation,
			Quote_Date		AS QuoteDate,
			ISNULL(IsEmailSend,0) AS IsEmailSend,
			ISNULL(IsReject,0) AS IsReject	
		FROM 
			Auth_GetQuote 
		WHERE 
			IsDeleted = 0 
		ORDER BY ID Desc
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
