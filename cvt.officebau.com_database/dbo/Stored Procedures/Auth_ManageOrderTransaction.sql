﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	30-12-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
      For after Online Payment process - capture the status ( it may me success / failure)
	  select * from Auth_Orders
	  select * from Auth_OrderTransaction
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_ManageOrderTransaction]
 (
	@OrderID			INT				,
	@TrackingID			VARCHAR(20)		,
	@bank_ref_no		VARCHAR(150)	,
	@order_status		VARCHAR(100)	,
	@failure_message	VARCHAR(250)	,
	@payment_mode		VARCHAR(100)	,
	@card_name			VARCHAR(100)	,
	@Status_code		VARCHAR(100)	,
	@vault				VARCHAR(100)	,
	@offer_type			VARCHAR(100)	,
	@offer_code			VARCHAR(100)	,
	@discount_value		VARCHAR(100)	,
	@mer_amount			MONEY			,
	@eci_value			VARCHAR(100)	
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY					
		SET NOCOUNT ON;
		DECLARE @Output						VARCHAR(100)				
		BEGIN TRANSACTION
			INSERT INTO Auth_OrderTransaction
			(
				OrderID				,
				TrackingID			,
				bank_ref_no			,
				order_status		,
				failure_message		,
				payment_mode		,
				card_name			,
				Status_code			,
				vault				,
				offer_type			,
				offer_code			,
				discount_value		,
				mer_amount			,
				eci_value			
			)
			VALUES
			(
				@OrderID			,
				@TrackingID			,
				@bank_ref_no		,
				@order_status		,
				@failure_message	,
				@payment_mode		,
				@card_name			,
				@Status_code		,
				@vault				,
				@offer_type			,
				@offer_code			,
				@discount_value		,
				@mer_amount			,
				@eci_value			
			)
			SET @Output = 'OK'
	
		SELECT @Output
		COMMIT TRANSACTION      
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
