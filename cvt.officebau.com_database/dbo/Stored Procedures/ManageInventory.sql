﻿/****************************************************************************                                         
CREATED BY  :                                         
CREATED DATE :                                         
MODIFIED BY  :                                 
MODIFIED DATE :                                 
<summary>                                                
   ManageInventory   7087,'INSERT','Purchase'  ,2,1            
   ManageInventory  1,'INSERT','Sales'  ,2,1                
</summary>                                                                 
*****************************************************************************/
CREATE PROCEDURE [dbo].[ManageInventory] (@ID        INT,
                                         @Operation VARCHAR(10),
                                         @StockType VARCHAR(50),
                                         @SessionID INT,
                                         @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @TmpID      INT = 0,
                  @TmpItemIDs VARCHAR(100) = 0;

          IF( @StockType = 'Purchase' )
            BEGIN
                SET @TmpID = (SELECT TOP 1 Isnull(PO.POID, 0)
                              FROM   tblExpenseDetails ED
                                     LEFT JOIN tblPODetails PO
                                            ON PO.ID = ED.POitemId
                                               -- AND PO.LedgerID = 0          
                                               AND PO.IsDeleted = 0
                              WHERE  ED.IsLedger = 0
                                     AND ED.ExpenseID = @ID
                              ORDER  BY PO.LedgerID DESC)
                SET @TmpItemIDs = Substring((SELECT ',' + Cast(Isnull(PO.ID, 0) AS VARCHAR)
                                             FROM   tblExpenseDetails ED
                                                    LEFT JOIN tblPODetails PO
                                                           ON PO.ID = ED.POitemId
                                                              -- AND PO.LedgerID = 0          
                                                              AND PO.IsDeleted = 0
                                             WHERE  ED.IsLedger = 0
                                                    AND ED.ExpenseID = @ID
                                             FOR XML PATH('')), 2, 2000)
            END
          ELSE IF( @StockType = 'Sales' )
            BEGIN
                SET @TmpID = (SELECT TOP 1 Isnull(SO.SOID, 0)
                              FROM   tblInvoiceItem II
                                     LEFT JOIN tblSOItem SO
                                            ON SO.ID = II.SoItemID
                                               AND SO.IsDeleted = 0
                              WHERE  II.IsDeleted = 0
                                     AND II.InvoiceID = @ID
                              ORDER  BY SO.ProductID DESC)
                SET @TmpItemIDs = Substring((SELECT ',' + Cast(Isnull(SO.ID, 0) AS VARCHAR)
                                             FROM   tblInvoiceItem II
                                                    LEFT JOIN tblSOItem SO
                                                           ON SO.ID = II.SoItemID
                                                              AND SO.IsDeleted = 0
                                             WHERE  II.IsDeleted = 0
                                                    AND II.InvoiceID = @ID
                                             FOR XML PATH('')), 2, 2000)
            END

          IF( @Operation = 'INSERT'
              AND Isnull(@TmpID, 0) <> 0 )
            BEGIN
                SET @Operation = 'UPDATE'
            END

          IF( @Operation = 'INSERT' )
            BEGIN
                IF( @StockType = 'Purchase' )
                  BEGIN
                      INSERT INTO tbl_Inventory
                                  (ReferenceID,
                                   StockType,
                                   ProductID,
                                   Qty,
                                   ItemID,
                                   Remarks,
                                   IsDeleted,
                                   DomainID,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   HistoryID)
                      SELECT ED.ExpenseID,
                             'Purchase',
                             LedgerID,
                             Qty,
                             ED.ID AS ItemID,
                             Remarks,
                             0,
                             @DomainID,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             ED.HistoryID
                      FROM   tblExpenseDetails ED
                      WHERE  ED.IsDeleted = 0
                             AND ED.IsLedger = 0
                             AND ED.ExpenseID = @ID
                  END
                ELSE IF( @StockType = 'Sales' )
                  BEGIN
                      INSERT INTO tbl_Inventory
                                  (ReferenceID,
                                   StockType,
                                   ProductID,
                                   Qty,
                                   ItemID,
                                   Remarks,
                                   IsDeleted,
                                   DomainID,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   HistoryID)
                      SELECT II.InvoiceID,
                             'Sales',
                             ProductID,
                             Qty,
                             II.ID AS ItemID,
                             ItemDescription,
                             0,
                             @DomainID,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             II.HistoryID
                      FROM   tblInvoiceItem II
                             JOIN tblProduct_V2 P
                               ON P.ID = II.ProductID
                                  AND p.TypeID = (SELECT ID
                                                  FROM   tbl_CodeMaster
                                                  WHERE  [Type] = 'ProductService'
                                                         AND Code = 'Product'
                                                         AND IsDeleted = 0)
                                  AND P.IsInventroy = 1
                      WHERE  II.IsDeleted = 0
                             AND II.InvoiceID = @ID
                  END
                ELSE IF( @StockType = 'PO' )
                  BEGIN
                      INSERT INTO tbl_Inventory
                                  (ReferenceID,
                                   StockType,
                                   ProductID,
                                   Qty,
                                   ItemID,
                                   Remarks,
                                   IsDeleted,
                                   DomainID,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   HistoryID,
                                   MapId,
                                   MapItemId)
                      SELECT PO.POID,
                             'PO',
                             LedgerID,
                             Qty,
                             PO.ID   AS ItemID,
                             Remarks,
                             0,
                             @DomainID,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             PO.HistoryID,
                             PO.POID AS MapId,
                             PO.ID   AS MapItemId
                      FROM   tblPODetails PO
                      WHERE  PO.IsDeleted = 0
                             AND PO.IsLedger = 0
                             AND PO.POID = @ID
                  END
                ELSE IF( @StockType = 'SO' )
                  BEGIN
                      INSERT INTO tbl_Inventory
                                  (ReferenceID,
                                   StockType,
                                   ProductID,
                                   Qty,
                                   ItemID,
                                   Remarks,
                                   IsDeleted,
                                   DomainID,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   HistoryID,
                                   MapId,
                                   MapItemId)
                      SELECT SI.SOID,
                             'SO',
                             ProductID,
                             Qty,
                             SI.ID   AS ItemID,
                             ItemDescription,
                             0,
                             @DomainID,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             SI.HistoryID,
                             SI.SOID AS MapId,
                             SI.ID   AS MapItemId
                      FROM   tblSOItem SI
                             JOIN tblProduct_V2 P
                               ON P.ID = SI.ProductID
                                  AND p.TypeID = (SELECT ID
                                                  FROM   tbl_CodeMaster
                                                  WHERE  [Type] = 'ProductService'
                                                         AND Code = 'Product'
                                                         AND IsDeleted = 0)
                                  AND P.IsInventroy = 1
                      WHERE  SI.IsDeleted = 0
                             AND SI.SOID = @ID
                  END
            END
          ELSE IF( @Operation = 'UPDATE' )
            BEGIN
                IF( @StockType = 'Purchase' )
                  BEGIN
                      INSERT INTO tbl_Inventory
                                  (ReferenceID,
                                   StockType,
                                   ProductID,
                                   Qty,
                                   ItemID,
                                   Remarks,
                                   IsDeleted,
                                   DomainID,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   HistoryID)
                      SELECT ED.ExpenseID,
                             'Purchase',
                             ED.LedgerID,
                             ED.Qty,
                             ED.ID AS ItemID,
                             ED.Remarks,
                             0,
                             @DomainID,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             ED.HistoryID
                      FROM   tblExpenseDetails ED
                             LEFT JOIN tbl_Inventory I
                                    ON I.ReferenceID = ED.ExpenseID
                                       AND I.ItemID = ED.ID
                                       AND I.StockType = 'Purchase'
                      WHERE  I.ItemID IS NULL
                             AND ED.IsLedger = 0
                             AND ed.ExpenseID = @ID

                      UPDATE I
                      SET    I.Qty = ED.QTY,
                             I.ProductID = ED.LedgerID,
                             I.Remarks = ED.Remarks,
                             I.IsDeleted = ED.IsDeleted,
                             ModifiedBy = @SessionID,
                             ModifiedOn = Getdate()
                      FROM   tbl_Inventory I
                             JOIN tblExpenseDetails ED
                               ON ED.ExpenseID = I.ReferenceID
                                  AND ED.ID = I.ItemID
                             LEFT JOIN tblProduct_V2 P
                                    ON P.ID = ED.LedgerID
                      WHERE  I.ReferenceID = @ID
                             AND I.StockType = 'Purchase'
                  END
                ELSE IF( @StockType = 'Sales' )
                  BEGIN
                      INSERT INTO tbl_Inventory
                                  (ReferenceID,
                                   StockType,
                                   ProductID,
                                   Qty,
                                   ItemID,
                                   Remarks,
                                   IsDeleted,
                                   DomainID,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   HistoryID,
                                   MapId,
                                   MapItemId)
                      SELECT ID.InvoiceID,
                             'Sales',
                             ID.ProductID,
                             ID.Qty,
                             ID.ID   AS ItemID,
                             ID.ItemDescription,
                             0,
                             @DomainID,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             ID.HistoryID,
                             SO.SOID AS MapId,
                             SO.ID   AS MapItemId
                      FROM   tblInvoiceItem ID
                             JOIN tblProduct_V2 P
                               ON P.ID = ID.ProductID
                                  AND p.TypeID = (SELECT ID
                                                  FROM   tbl_CodeMaster
                                                  WHERE  [Type] = 'ProductService'
                                                         AND Code = 'Product'
                                                         AND IsDeleted = 0)
                                  AND P.IsInventroy = 1
                             LEFT JOIN tbl_Inventory I
                                    ON I.ReferenceID = ID.InvoiceID
                                       AND I.ItemID = ID.ID
                                       AND I.StockType = 'Sales'
                             LEFT JOIN tblSOItem SO
                                    ON SO.ID = ID.SoItemID
                                       AND so.IsProduct = 0
                      WHERE  I.ItemID IS NULL
                             AND ID.InvoiceID = @ID

                      UPDATE I
                      SET    I.Qty = II.QTY,
                             I.ProductID = II.ProductID,
                             I.Remarks = II.ItemDescription,
                             I.IsDeleted = II.IsDeleted,
                             ModifiedBy = @SessionID,
                             ModifiedOn = Getdate()
                      FROM   tbl_Inventory I
                             JOIN tblInvoiceItem II
                               ON II.InvoiceID = I.ReferenceID
                                  AND II.ID = I.ItemID
                             --AND II.ProductID = I.ProductID      
                             LEFT JOIN tblProduct_V2 P
                                    ON P.ID = II.ProductID
                      WHERE  I.ReferenceID = @ID
                             AND I.StockType = 'Sales'
                  END
                ELSE IF( @StockType = 'PO' )
                  BEGIN
                      INSERT INTO tbl_Inventory
                                  (ReferenceID,
                                   StockType,
                                   ProductID,
                                   Qty,
                                   ItemID,
                                   Remarks,
                                   IsDeleted,
                                   DomainID,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   HistoryID,
                                   MapId,
                                   MapItemId)
                      SELECT PO.POID,
                             'PO',
                             PO.LedgerID,
                             PO.Qty,
                             PO.ID   AS ItemID,
                             PO.Remarks,
                             PO.IsDeleted,
                             @DomainID,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             PO.HistoryID,
                             PO.POID AS MapId,
                             PO.ID   AS MapItemId
                      FROM   tblPODetails PO
                             LEFT JOIN tbl_Inventory I
                                    ON I.ReferenceID = PO.POID
                                       AND I.ItemID = PO.ID
                                       AND I.StockType = 'PO'
                      WHERE  I.ItemID IS NULL
                             AND PO.IsDeleted = 0
                             AND PO.IsLedger = 0
                             AND PO.POID = @ID

                      UPDATE I
                      SET    I.ProductID = PO.LedgerID,
                             I.IsDeleted = ( CASE
                                               WHEN PO.IsLedger = 0 THEN PO.IsDeleted
                                               ELSE 'true'
                                             END ),
                             I.Remarks = PO.Remarks,
                             I.Qty = PO.Qty,
                             I.ModifiedBy = @SessionID,
                             I.ModifiedOn = Getdate()
                      FROM   tbl_Inventory I
                             JOIN tblPODetails PO
                               ON PO.POID = I.ReferenceID
                                  --AND PO.IsLedger = 0          
                                  AND I.ItemID = PO.ID
                                  AND I.StockType = 'PO'
                      -- AND PO.LedgerID = I.ProductID      
                      WHERE  I.ReferenceID = @ID
                  END
                ELSE IF( @StockType = 'SO' )
                  BEGIN
                      INSERT INTO tbl_Inventory
                                  (ReferenceID,
                                   StockType,
                                   ProductID,
                                   Qty,
                                   ItemID,
                                   Remarks,
                                   IsDeleted,
                                   DomainID,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   HistoryID,
                                   MapId,
                                   MapItemId)
                      SELECT SI.SOID,
                             'SO',
                             SI.ProductID,
                             SI.Qty,
                             SI.ID   AS ItemID,
                             SI.ItemDescription,
                             0,
                             @DomainID,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             SI.HistoryID,
                             SI.SOID AS MapId,
                             SI.ID   AS MapItemId
                      FROM   tblSOItem SI
                             JOIN tblProduct_V2 P
                               ON P.ID = SI.ProductID
                                  AND p.TypeID = (SELECT ID
                                                  FROM   tbl_CodeMaster
                                                  WHERE  [Type] = 'ProductService'
                                                         AND Code = 'Product'
                                                         AND IsDeleted = 0)
                                  AND P.IsInventroy = 1
                             LEFT JOIN tbl_Inventory I
                                    ON I.ReferenceID = SI.SOID
                                       AND I.ItemID = SI.ID
                                       AND I.StockType = 'SO'
                      WHERE  I.ItemID IS NULL
                             AND SI.SOID = @ID

                      UPDATE I
                      SET    I.ProductID = SI.ProductID,
                             I.IsDeleted = ( CASE
                                               WHEN p.TypeID = (SELECT ID
                                                                FROM   tbl_CodeMaster
                                                                WHERE  [Type] = 'ProductService'
                                                                       AND Code = 'Product'
                                                                       AND IsDeleted = 0) THEN SI.IsDeleted
                                               ELSE 'true'
                                             END ),
                             I.Remarks = SI.ItemDescription,
                             I.Qty = SI.Qty,
                             I.ModifiedBy = @SessionID,
                             I.ModifiedOn = Getdate()
                      FROM   tbl_Inventory I
                             JOIN tblSOItem SI
                               ON SI.SOID = I.ReferenceID
                                  AND I.ItemID = SI.ID
                                  --AND SI.ProductID = I.ProductID      
                                  AND I.StockType = 'SO'
                             LEFT JOIN tblProduct_V2 P
                                    ON P.ID = SI.ProductID
                      WHERE  I.ReferenceID = @ID
                  END
            END
          ELSE IF( @Operation = 'DELETE' )
            BEGIN
                EXEC [Deleteinventory]
                  @ID,
                  @Operation,
                  @StockType,
                  @SessionID,
                  @DomainID
            END

          UPDATE I
          SET    I.Qty = ( CASE
                             WHEN x.QTY IS NULL THEN Isnull(it.Qty, 0)
                             ELSE Isnull(x.QTY, 0)
                           END ),
                 I.ModifiedBy = @SessionID,
                 I.ModifiedOn = Getdate()
          FROM   tbl_Inventory I
                 LEFT JOIN (SELECT i.StockType,
                                   ( Isnull(so.QTY, 0) - Isnull(Sum(i.QTY), 0) ) AS QTY,
                                   II.SoItemID                                   AS ItemID,
                                   so.SOID                                       AS ReferenceID,
                                   I.ProductID                                   AS ProductID
                            FROM   tbl_Inventory I
                                   JOIN tblinvoiceitem II
                                     ON II.id = I.ItemID
                                        AND ii.InvoiceID = i.ReferenceID
                                   JOIN tblSOItem so
                                     ON so.ID = II.SoItemID
                                        AND so.ProductID = II.ProductID
                            WHERE  i.IsDeleted = 0
                                   AND i.StockType = 'Sales'
                            GROUP  BY i.StockType,
                                      II.SoItemID,
                                      so.SOID,
                                      so.QTY,
                                      I.ProductID) AS x
                        ON x.ReferenceID = i.ReferenceID
                           AND x.ItemID = i.ItemID
                           AND x.ProductID = i.ProductID
                 LEFT JOIN tblSOItem it
                        ON it.SOID = i.ReferenceID
                           AND it.ID = i.ItemID
                           AND it.ProductID = i.ProductID
                           AND it.IsDeleted = 0
          WHERE  i.StockType = 'SO'

          UPDATE I
          SET    I.Qty = ( CASE
                             WHEN x.QTY IS NULL THEN Isnull(it.Qty, 0)
                             ELSE Isnull(x.QTY, 0)
                           END ),
                 I.ModifiedBy = @SessionID,
                 I.ModifiedOn = Getdate()
          FROM   tbl_Inventory I
                 LEFT JOIN (SELECT i.StockType,
                                   ( Isnull(PO.QTY, 0) - Isnull(Sum(i.QTY), 0) ) AS QTY,
                                   II.POitemId                                   AS ItemID,
                                   PO.POID                                       AS ReferenceID,
                                   II.LedgerID                                   AS ProductID
                            FROM   tbl_Inventory I
                                   LEFT JOIN tblExpenseDetails II
                                          ON II.id = I.ItemID
                                             AND ii.ExpenseID = i.ReferenceID
                                   JOIN tblPODetails PO
                                     ON PO.ID = II.POitemId
                                        AND po.LedgerID = II.LedgerID
                            WHERE  i.IsDeleted = 0
                                   AND i.StockType = 'Purchase'
                            GROUP  BY i.StockType,
                                      II.POitemId,
                                      PO.POID,
                                      PO.QTY,
                                      II.LedgerID) AS x
                        ON x.ReferenceID = i.ReferenceID
                           AND x.ItemID = i.ItemID
                           AND x.ProductID = i.ProductID
                 LEFT JOIN tblPODetails it
                        ON it.POID = i.ReferenceID
                           AND it.ID = i.ItemID
                           AND it.LedgerID = i.ProductID
                           AND it.IsDeleted = 0
          WHERE  i.StockType = 'PO'
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
