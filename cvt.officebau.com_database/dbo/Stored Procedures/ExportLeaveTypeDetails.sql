﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar
CREATED DATE		:	04-JAN-2018
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>       
    [Exportleavetypedetails] 3,9,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ExportLeaveTypeDetails] (@Year           INT,
                                                @BaseLocationID INT,
                                                @DomainID       INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @Query VARCHAR(Max)=''
          DECLARE @YearCode INT = (SELECT NAME
             FROM   tbl_FinancialYear
             WHERE  ID = @Year
                    AND DomainID = @DomainID)
          DECLARE @header VARCHAR(max)= Substring((SELECT ',[' + NAME + ']'
                       FROM   tbl_leavetypes
                       WHERE  isdeleted = 0
                              AND domainID = @domainID
                              AND Code NOT IN ( 'OD', 'PM', 'COMP' )
                       FOR xml path('')), 2, 100)

          SET @Query=( ' SELECT [Employee Code],[Employee Name], '
                       + @header + '
          FROM   (SELECT EM.CODE As [Employee Code],
		                 Em.FirstName + '' '' + Isnull(em.LastName, '''') AS [Employee Name],
                         lt.Name                                      AS LT,
                         eal.TotalLeave                             AS totalleave
                  FROM   tbl_EmployeeMaster em
                         LEFT JOIN tbl_EmployeeAvailedLeave eal
                                ON em.id = eal.EmployeeID
								 AND ( eal.Year='
                       + Cast( @YearCode AS VARCHAR)
                       + ' OR eal.Year IS NULL)
                         LEFT JOIN tbl_LeaveTypes lt
                                ON lt.id = eal.LeaveTypeID OR eal.LeaveTypeID IS NULL
                  WHERE  em.BaseLocationID = '
                       + Cast( @BaseLocationID AS VARCHAR)
                       + '
                         AND IsActive = 0
                         AND em.IsDeleted = 0
                         AND lt.IsDeleted = 0
						) AS SourceTable
                 PIVOT ( MAX(totalleave)
                       FOR LT IN ('
                       + @header + ') ) AS PivotTable' )

          EXEC (@Query)
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
