﻿
/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	[ManageHRA] 0,38,3,2016,2016,1,1,1,1,NULL,38,0,2,1
	select * from HRA
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageHRA] (@ID                INT,
                                   @EmployeeID        INT,
                                   @MonthID           INT,
                                   @Year            INT,
                                   @StartYearID       INT,
                                   @Declaration       MONEY,
                                   @Submitted         MONEY,
                                   @Cleared           MONEY,
                                   @Rejected          MONEY,
                                   @Remarks           VARCHAR(8000),
                                   @SessionEmployeeID INT,
                                   @IsDeleted         BIT,
                                   @DomainID          INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output    VARCHAR(8000),
              @TempMonth INT = Month(Getdate()),
              @TempYear  INT = Year(Getdate())

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF( Isnull(@ID, 0) != 0 )
            BEGIN
                IF( @IsDeleted = 1 )
                  BEGIN
                      UPDATE HRA
                      SET    IsDeleted = 1,
                             ModifiedBy = @SessionEmployeeID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID
                             AND EmployeeID = @EmployeeID

                      SET @Output = 'Deleted Successfully.'

                      GOTO Finish
                  END

                IF( @IsDeleted = 0 )
                  BEGIN
                      IF EXISTS (SELECT 1
                                 FROM   HRA
                                 WHERE  EmployeeID = @EmployeeID
                                        AND IsDeleted = 0
                                        AND MonthID = @MonthID
                                        AND YearID = @Year
                                        AND ID = @ID)
                        BEGIN
                            UPDATE HRA
                            SET    Declaration = @Declaration,
                                   Submitted = @Submitted,
                                   Cleared = @Cleared,
                                   Rejected = @Rejected,
                                   Remarks = @Remarks,
                                   ModifiedBy = @SessionEmployeeID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID
                                   AND DomainID = @DomainID
                                   AND EmployeeID = @EmployeeID

                            SET @Output = 'Updated Successfully.'

                            GOTO Finish
                        END
                      ELSE
                        BEGIN
                            SET @Output = 'Already Exists!'

                            GOTO Finish
                        END
                  END
            END

          IF( Isnull(@ID, 0) = 0 )
            BEGIN
                IF NOT EXISTS (SELECT 1
                               FROM   HRA
                               WHERE  EmployeeID = @EmployeeID
                                      AND MonthID = @MonthID
                                      AND YearID = @Year
                                      AND IsDeleted = 0)
                  BEGIN
                      --IF @YearID <= @TempYear
                      --  BEGIN
                      --      IF @MonthID <= @TempMonth
                              BEGIN
                                  INSERT INTO HRA
                                              (EmployeeID,
                                               MonthID,
                                               YearID,
                                               StartYearID,
                                               Declaration,
                                               Submitted,
                                               Cleared,
                                               Rejected,
                                               Remarks,
                                               CreatedBy,
                                               ModifiedBy,
                                               DomainID)
                                  VALUES      ( @EmployeeID,
                                                Isnull(@MonthID, 0),
                                                Isnull(@Year, 0),
                                                Isnull(@StartYearID, 0),
                                                @Declaration,
                                                @Submitted,
                                                @Cleared,
                                                @Rejected,
                                                @Remarks,
                                                @SessionEmployeeID,
                                                @SessionEmployeeID,
                                                @DomainID)

                                  SET @Output = 'Inserted Successfully.'

                                  GOTO Finish
                              END
                      --      ELSE
                      --        BEGIN
                      --            SET @Output = 'Cannot Declare the HRA for the Future Month.'

                      --            GOTO Finish
                      --        END
                      --  END
                      --ELSE
                      --  BEGIN
                      --      SET @Output = 'Cannot Declare the HRA for the Future Year.'

                      --      GOTO Finish
                      --  END
                  END
                ELSE
                  BEGIN
                      SET @Output = 'Already Exists!'

                      GOTO Finish
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
