﻿/****************************************************************************               
CREATED BY   : Naneeshwar              
CREATED DATE  :               
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>            
 [Rpt_expensetransactiondetail] 'May_17','Munivel Teacher House','',2            
 </summary>                                       
 *****************************************************************************/            
CREATE PROCEDURE [dbo].[Rpt_expensetransactiondetail] (@SearchParameter VARCHAR(100),            
                                                      @PartyName       VARCHAR(100),            
                                                      @Type            VARCHAR(50),            
                                                      @DomainID        INT)            
AS            
  BEGIN            
      SET NOCOUNT ON;            
            
      DECLARE @SearchDate DATETIME=CONVERT(DATETIME, Replace(@SearchParameter, '_', ' 20'))            
      DECLARE @StartDate DATETIME=Dateadd(MONTH, Datediff(MONTH, 0, @SearchDate), 0)            
      DECLARE @EndDate DATETIME=Dateadd(SECOND, -1, Dateadd(MONTH, 1, Dateadd(MONTH, Datediff(MONTH, 0, @SearchDate), 0)))            
            
      BEGIN TRY            
          BEGIN TRANSACTION            
            
          IF( @Type = 'Party' )            
            BEGIN            
                SELECT e.ID                                    AS ID,            
                       e.Type                                  AS Type,            
                       ( (SELECT ( Isnull(Sum(Amount*Qty), 0)+Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0) )            
                          FROM   tblExpenseDetails            
                          WHERE  ExpenseID = e.ID            
                                 AND IsDeleted = 0            
                                 AND DomainID = @domainID)    )        
                          AS Amount,            
                       BillNo                                  AS BillNo,            
                       Date                                    AS Date,            
                       v.NAME                                  AS VendorName,            
                       c.Code                                  AS Status            
                FROM   tblExpense e            
                       LEFT JOIN tblVendor v            
                              ON v.ID = e.VendorID            
                       LEFT JOIN tbl_CodeMaster c            
                              ON c.ID = e.StatusID            
                WHERE  Date >= @StartDate            
                       AND Date <= @EndDate            
                       AND e.IsDeleted = 0            
                       AND e.DomainID = @DomainID            
                       AND e.VendorID = (SELECT ID            
                                         FROM   tblVendor            
                                         WHERE  @PartyName = NAME            
                                                AND IsDeleted = 0            
                                                AND DomainID = @DomainID)            
                GROUP  BY e.ID,            
                          e.Type,            
                          v.NAME,            
                          c.Code,            
                          BillNo,            
                          Date            
            Order by Date DESC          
            END            
          ELSE IF( @Type = 'Ledger' )            
            BEGIN            
   Declare @lId int =(SELECT ID            
                  FROM   tblLedger            
                                          WHERE  @PartyName = NAME            
                                   AND IsDeleted = 0            
                                                 AND DomainID = @DomainID)     
             IF(ISNULL(@lId,0)=0)    
             Set @lId =(SELECT ID            
                                          FROM   tblProduct_v2            
                                          WHERE  @PartyName = NAME            
                                                 AND IsDeleted = 0            
                                                 AND DomainID = @DomainID)     
                SELECT e.ID                                AS ID,            
                       e.Type                              AS Type,            
                       Sum(ed.Amount*Qty) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0) AS Amount,            
                       BillNo                              AS BillNo,            
                    e.Date                              AS Date,            
                       v.NAME                              AS VendorName,                             c.Code                              AS Status            
                FROM   tblExpense e            
                       LEFT JOIN tblExpenseDetails ed        
                              ON ed.ExpenseID = e.ID          And ed.isdeleted=0       
                   LEFT JOIN tblVendor v            
                              ON v.ID = e.VendorID            
                       LEFT JOIN tbl_CodeMaster c            
                              ON c.ID = e.StatusID            
                WHERE  e.Date >= @StartDate            
                       AND e.Date <= @EndDate            
                       AND e.IsDeleted = 0            
                       AND e.DomainID = @DomainID            
                       AND ed.LedgerID =@lId           
                GROUP  BY e.ID,            
                          e.Type,            
                          v.NAME,            
                          c.Code,            
                          BillNo,            
                          Date            
                          Order by Date DESC          
            END            
          ELSE            
            BEGIN            
                SELECT e.ID                                    AS ID,            
                       e.Type                                  AS Type,            
                       ( (SELECT ( Isnull(Sum(Amount *Qty), 0)+Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0) )            
                          FROM   tblExpenseDetails            
                          WHERE  ExpenseID = e.ID            
                                 AND IsDeleted = 0            
                                 AND DomainID = @domainID)            
                         ) AS Amount,            
                       BillNo                                  AS BillNo,            
                       Date                                    AS Date,            
                       v.NAME                                  AS VendorName,            
                       c.Code                                  AS Status            
                FROM   tblExpense e            
                       LEFT JOIN tblVendor v            
                              ON v.ID = e.VendorID            
                       LEFT JOIN tbl_CodeMaster c            
                              ON c.ID = e.StatusID            
                WHERE  Date >= @StartDate            
                       AND Date <= @EndDate            
                       AND e.IsDeleted = 0            
                       AND e.DomainID = @DomainID            
                       AND e.CostCenterID = (SELECT ID            
                                             FROM   tblCostCenter            
        WHERE  @PartyName = NAME            
                                                    AND IsDeleted = 0            
                                                    AND DomainID = @DomainID)            
                GROUP  BY e.ID,            
                          e.Type,            
                          v.NAME,            
                          c.Code,            
                          BillNo,            
                          Date            
                          Order by Date DESC          
            END            
            
          COMMIT TRANSACTION            
      END TRY            
      BEGIN CATCH            
          ROLLBACK TRANSACTION            
     DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT            
          SELECT @ErrorMsg = Error_message(),            
                 @ErrSeverity = Error_severity()            
          RAISERROR(@ErrorMsg,@ErrSeverity,1)            
      END CATCH            
  END
