﻿/****************************************************************************   
CREATED BY		: Naneeshwar.M
CREATED DATE	: 
MODIFIED BY		: 
MODIFIED DATE	:  
 <summary>
	[Searchinventorysales] 1
 </summary>                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[Searchinventorysales] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT invi.InvoiceID AS ID,
                 invi.ProductID AS ProductID,
                 invi.Qty       AS QTY,
                 invi.Rate      AS Rate,
                 pdt.NAME       AS Product,
                 inv.[Date]     AS Date,
                 inv.InvoiceNo  AS InvoiceNo
          FROM   tblInvoiceItem invi
                 JOIN tblInvoice inv
                   ON inv.ID = invi.InvoiceID
                 JOIN tblProduct pdt
                   ON pdt.ID = invi.ProductID
          WHERE  invi.IsDeleted = 0
                 AND invi.DomainID = @DomainID
                 AND pdt.TypeID = (SELECT ID
                                   FROM   tbl_CodeMaster
                                   WHERE [Type] = 'ProductService' 
									AND Code IN ( 'Product' ) 
									and IsDeleted = 0)
          ORDER  BY inv.ModifiedOn

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
