﻿/****************************************************************************             
CREATED BY  :         
CREATED DATE :         
MODIFIED BY  :             
MODIFIED DATE :             
 <summary>          
   [GetExpiryAlertDetails]   2,1, 'Skill Set'       
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetExpiryAlertDetails] (@EmployeeID INT,
                                                @DomainID   INT,
                                                @Key        VARCHAR(100))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @ExpiryValue INT= CASE
              WHEN( Isnumeric((SELECT ConfigValue
                               FROM   tbl_ApplicationConfiguration
                               WHERE  ConfigurationID = (SELECT ID
                                                         FROM   tbl_configurationmaster
                                                         WHERE  code = 'ExpiryNotification')
                                      AND BusinessUnitID = (SELECT BaseLocationID
                                                            FROM   tbl_EmployeeMaster
                                                            WHERE  id = @EmployeeID))) = 0 ) THEN 0
              ELSE Cast((SELECT ConfigValue
                         FROM   tbl_ApplicationConfiguration
                         WHERE  ConfigurationID = (SELECT ID
                                                   FROM   tbl_configurationmaster
                                                   WHERE  code = 'ExpiryNotification')
                                AND BusinessUnitID = (SELECT BaseLocationID
                                                      FROM   tbl_EmployeeMaster
                                                      WHERE  id = @EmployeeID)) AS INT)
            END;

          IF( (SELECT ApplicationRoleID
               FROM   tbl_EmployeeOtherDetails
               WHERE  EmployeeID = @EmployeeID
                      AND DomainID = @DomainID
                      AND IsDeleted = 0) = (SELECT ID
                                            FROM   tbl_ApplicationRole
                                            WHERE  domainID = @DomainID
                                                   AND NAME = 'HR Manager'
                                                   AND IsDeleted = 0) )
            BEGIN
                IF @Key = 'Skill Set'
                  SELECT FullName + '-' + Code AS FullName,
                         sk.NAME               AS DocType,
                         DateofExpiry          AS DateOfExpiry
                  FROM   tbl_EmployeeSkills s
                         JOIN tbl_employeemaster e
                           ON e.id = s.EmployeeID
                         JOIN tbl_Skills sk
                           ON s.SkillsID = sk.ID
                  WHERE  s.IsDeleted = 0
                         AND s.DomainID = @DomainID
                         AND DateofExpiry BETWEEN Getdate() AND Getdate() + @ExpiryValue;

                IF @Key = 'Document'
                  SELECT FullName + '-' + Code AS FullName,
                         d.NAME                AS DocType,
                         DateOfExpiry          AS DateOfExpiry
                  FROM   tbl_EmployeeDocuments s
                         JOIN tbl_DocumentType d
                           ON s.DocumentTypeId = d.ID
                         JOIN tbl_EmployeeMaster e
                           ON s.EmployeeID = e.ID
                  WHERE  s.IsDeleted = 0
                         AND s.DomainID = @DomainID
                         AND DateOfExpiry BETWEEN Getdate() AND Getdate() + @ExpiryValue;
            END;
          ELSE
            BEGIN
                IF @Key = 'Skill Set'
                  SELECT FullName + '-' + Code AS FullName,
                         sk.NAME               AS DocType,
                         DateofExpiry          AS DateOfExpiry
                  FROM   tbl_EmployeeSkills s
                         JOIN tbl_employeemaster e
                           ON e.id = s.EmployeeID
                         JOIN tbl_Skills sk
                           ON s.SkillsID = sk.ID
                  WHERE  s.IsDeleted = 0
                         AND EmployeeID IN (SELECT Id
                                            FROM   tbl_employeemaster
                                            WHERE  ReportingToID = @EmployeeID)
                         AND s.DomainID = @DomainID
                         AND DateofExpiry BETWEEN Getdate() AND Getdate() + @ExpiryValue;

                IF @Key = 'Document'
                  SELECT FullName + '-' + Code AS FullName,
                         d.NAME                AS DocType,
                         DateOfExpiry          AS DateOfExpiry
                  FROM   tbl_EmployeeDocuments s
                         JOIN tbl_DocumentType d
                           ON s.DocumentTypeId = d.ID
                         JOIN tbl_EmployeeMaster e
                           ON s.EmployeeID = e.ID
                  WHERE  s.IsDeleted = 0
                         AND EmployeeID IN (SELECT Id
                                            FROM   tbl_employeemaster
                                            WHERE  ReportingToID = @EmployeeID)
                         AND s.DomainID = @DomainID
                         AND DateOfExpiry BETWEEN Getdate() AND Getdate() + @ExpiryValue;
            END;
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity();
          RAISERROR(@ErrorMsg,@ErrSeverity,1);
      END CATCH;
  END;
