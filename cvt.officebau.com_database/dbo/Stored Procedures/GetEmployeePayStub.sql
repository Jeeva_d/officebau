﻿/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  : 10-JULY-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
 [Getemployeepaystub] 7,2018,4,1,false
 </summary>                            
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetEmployeePayStub] (@MonthId    INT,
                                            @Year       INT,
                                            @Employeeid INT,
                                            @DomainID   INT,
                                            @IsProfile  BIT = NULL)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @CompanyName VARCHAR(100)= (SELECT Isnull(FullName, '')
         FROM   tbl_company
         WHERE  id = @DomainID)
      DECLARE @DurationPayslip INT=(SELECT PayslipDuration
        FROM   tbl_BusinessUnit
        WHERE  id = (SELECT BaseLocationID
                     FROM   tbl_EmployeeMaster
                     WHERE  id = @Employeeid))
      DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'
        + CONVERT(VARCHAR(10), @Year)
      DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth))
      DECLARE @PaySlipPreviewDate DATE = CONVERT(VARCHAR(10), @MonthId) + '-'
        + CONVERT(VARCHAR(10), @DurationPayslip) + '-'
        + CONVERT(VARCHAR(10), @Year)

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT BusinessUnitID,
                 Cast(EducationAllow AS BIT) AS IsEducationAllow,
                 Cast(OverTime AS BIT)       AS IsOverTime,
                 Cast(EEPF AS BIT)           AS IsEEPF,
                 Cast(EEESI AS BIT)          AS IsEEESI,
                 Cast(PT AS BIT)             AS IsPT,
                 Cast(TDS AS BIT)            AS IsTDS,
                 Cast(ERPF AS BIT)           AS IsERPF,
                 Cast(ERESI AS BIT)          AS IsERESI,
                 Cast(GRATUITY AS BIT)       AS IsGratuity,
                 Cast(PLI AS BIT)            AS IsPLI,
                 Cast(MEDICLAIM AS BIT)      AS IsMediclaim,
                 Cast(MOBILEDATACARD AS BIT) AS IsMobileDatacard,
                 Cast(OTHERPERKS AS BIT)     AS IsOtherPerks
          INTO   #tmpParollConfigValue
          FROM   (SELECT code,
                         Isnull(pcc.ConfigValue, 0) AS ConfigValue,
                         BusinessUnitID
                  FROM   tbl_PayrollComponentConfiguration pcc
                         LEFT JOIN tbl_PayrollComponentMaster pcm
                                ON pcc.PayrollComponentID = pcm.ID) x
                 PIVOT( Max(ConfigValue)
                      FOR code IN ( EducationAllow,
                                    OverTime,
                                    EEPF,
                                    EEESI,
                                    PT,
                                    TDS,
                                    ERPF,
                                    ERESI,
                                    GRATUITY,
                                    PLI,
                                    MEDICLAIM,
                                    MOBILEDATACARD,
                                    OTHERPERKS)) p

          SELECT EMP.FirstName + ' ' + Isnull(EMP.LastName, '') AS EmployeeName,
                 --Substring(LTRIM( depart.NAME), 1, 4) + '-'  
                 --+ Isnull( fun.NAME, '') +'-'+ EMP.Code             AS EmployeeCode,  
                 EMP.Code                                       AS EmployeeCode,
                 Isnull(DE.NAME, '') + '-'
                 + Isnull(DEPART.NAME, '')                      AS DesignationName,
                 Isnull(EST.PFNo, '-')                          AS PFNo,
                 Isnull(EST.ESINo, '-')                         AS ESINo,
                 Isnull(EST.UANNo, '-')                         AS UAN,
                 Isnull(EST.PAN_No, '-')                        AS PANNo,
                 Isnull(EST.AadharID, '-')                      AS AadharID,
                 Isnull(EST.AccountNo, '-')                     AS AccountNo,
                 Round(GrossEarning, 0)
                 + Round(Isnull(OverTime, 0), 0)                AS Gross,
                 Round(Basic, 0)                                AS Basic,
                 Round(HRA, 0)                                  AS HRA,
                 Round(MedicalAllowance, 0)                     AS MedicalAllowance,
                 Round(Conveyance, 0)                           AS Conveyance,
                 Round(SplAllow, 0)                             AS SplAllow,
                 Round(EEPF, 0)                                 AS EEPF,
                 Round(EEESI, 0)                                AS EEESI,
                 Round(PT, 0)                                   AS PT,
                 Round(TDS, 0)                                  AS TDS,
                 Round(ERPF, 0)                                 AS ERPF,
                 Round(ERESI, 0)                                AS ERESI,
                 Round(Bonus, 0)                                AS Bonus,
                 Round(Gratuity, 0)                             AS Gratuity,
                 Round(PLI, 0)                                  AS PLI,
                 Round(Mediclaim, 0)                            AS Medicalclaim,
                 Round(MobileDataCard, 0)                       AS MobileDataCard,
                 Round(OtherEarning, 0)                         AS [Other Earning],
                 Round(OtherPerks, 0)                           AS OtherPerks,
                 Round(OtherDeduction, 0)                       AS Deduction,
                 Round(Loans, 0)                                AS Loans,
                 Cast(@WorkDays AS INT)                         AS WorkingDays,
                 @CompanyName                                   AS CompanyName,
                 Isnull(BU.Address, '')                         AS BusinessUnitAddress,
                 EMP.DOJ                                        AS DOJ,
                 NoofDaysPayable                                AS PresentDays,
                 Cast(0 AS DECIMAL(32, 2))                      AS FixedDA,
                 Cast(0 AS DECIMAL(32, 2))                      AS EducationAllowance,
                 Cast(0 AS DECIMAL(32, 2))                      AS IncomeTax,
                 Cast(0 AS DECIMAL(32, 2))                      AS InstalmentOnLoan,
                 CONVERT(VARCHAR(50), COM.Logo)                 AS LOGO,
                 Rtrim(Ltrim(REG.NAME))                         AS Region,
                 EPR.OverTime                                   AS OverTime,
                 EPR.EducationAllow                             AS EducationAllow,
                 EPR.MagazineAllow                              AS PaperMagazine,
                 EPR.MonsoonAllow                               AS MonsoonAllow
                 --------------  
                 ,
                 IsEducationAllow,
                 IsOverTime,
                 IsEEPF,
                 IsEEESI,
                 IsPT,
                 IsTDS,
                 IsERPF,
                 IsERESI,
                 IsGratuity,
                 IsPLI,
                 IsMediclaim,
                 IsMobileDatacard,
                 IsOtherPerks
          INTO   #tempEmployeePayroll
          FROM   tbl_EmployeePayroll EPR
                 JOIN tbl_EmployeeMaster EMP
                   ON EPR.EmployeeId = EMP.ID
                      AND EMP.IsDeleted = 0
                 LEFT JOIN tbl_lopdetails P
                        ON P.EmployeeId = @Employeeid
                           AND P.Monthid = @MonthId
                           AND P.year = @Year
                 LEFT JOIN tbl_Company COM
                        ON COM.ID = EPR.DomainId
                 LEFT JOIN tbl_FileUpload FU
                        ON FU.Id = COM.Logo
                 LEFT JOIN tbl_Designation DE
                        ON DE.ID = EMP.DesignationID
                 LEFT JOIN tbl_Department DEPART
                        ON DEPART.ID = EMP.DepartmentID
                 LEFT JOIN tbl_EmployeeStatutoryDetails EST
                        ON EST.EmployeeID = EMP.ID
                 LEFT JOIN tbl_Functional FUN
                        ON FUN.ID = EMP.FunctionalID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EPR.BaseLocationID
                 LEFT JOIN tbl_BusinessUnit REG
                        ON REG.ID = EMP.RegionID
                 LEFT JOIN #tmpParollConfigValue tpc
                        ON tpc.BusinessUnitID = EPR.BaselocationID
          WHERE  EPR.monthid = @MonthId
                 AND [yearid] = @Year
                 AND EPR.EmployeeId = @Employeeid
                 AND EPR.IsProcessed = 1
                 AND EPR.DomainID = @DomainID

          IF( Isnull(@IsProfile, 'false') = 'true' )
            BEGIN
                IF( Month(@CurrentMonth) = Month(@PaySlipPreviewDate) )
                  BEGIN
                      BEGIN
                          IF( Cast(Getdate() AS DATE) < Cast(Dateadd(MONTH, 1, @PaySlipPreviewDate) AS DATE) )
                            BEGIN
                                DELETE FROM #tempEmployeePayroll
                            END
                      END
                  END
            END

          SELECT *
          FROM   #tempEmployeePayroll

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
