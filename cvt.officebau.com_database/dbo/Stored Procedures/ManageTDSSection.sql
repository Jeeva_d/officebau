﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	ManageTDSSection
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageTDSSection] (@ID                INT,
                                          @Code              VARCHAR(100),
                                          @Description       VARCHAR(8000),
                                          @HasDeleted        BIT,
                                          @SessionEmployeeID INT,
                                          @DomainID          INT,
                                          @CALID             INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(8000),
		@FYID int = 0

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF( Isnull(@ID, 0) != 0 )
            BEGIN
                IF( @HasDeleted = 1 )
                  BEGIN
                      IF NOT EXISTS (SELECT 1
                                     FROM   TDSComponent
                                     WHERE  SectionID = @ID
                                            AND DomainID = @DomainID
											and (@FYID = 0 OR FYID = @FYID)
                                            AND IsDeleted = 0)
                        BEGIN
                            UPDATE TDSSection
                            SET    IsDeleted = 1,
                                   ModifiedBy = @SessionEmployeeID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID

                            SET @Output = 'Deleted Successfully.'

                            GOTO Finish
                        END
                      ELSE
                        BEGIN
                            SET @Output = 'The selected Record is already referred. So You cannot Delete.'

                            GOTO Finish
                        END
                  END

                IF( @HasDeleted = 0 )
                  BEGIN
                      IF NOT EXISTS (SELECT 1
                                     FROM   TDSSection
                                     WHERE  ID <> @ID
                                            AND Code = @Code
                                            AND DomainID = @DomainID
                                            AND IsDeleted = 0)
                        BEGIN
                            UPDATE TDSSection
                            SET    Code = @Code,
                                   [Description] = @Description,
                                   ModifiedBy = @SessionEmployeeID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID

                            SET @Output = 'Updated Successfully.'

                            GOTO Finish
                        END
                      ELSE
                        BEGIN
                            SET @Output = 'Already Exists!'

                            GOTO Finish
                        END
                  END
            END

          IF( Isnull(@ID, 0) = 0 )
            BEGIN
                IF NOT EXISTS (SELECT 1
                               FROM   TDSSection
                               WHERE  Code = @Code
                                      AND DomainID = @DomainID
                                      AND IsDeleted = 0)
                  BEGIN
                      INSERT INTO TDSSection
                                  (Code,
                                   [Description],
                                   CreatedBy,
                                   ModifiedBy,
                                   DomainID)
                      VALUES      ( @Code,
                                    @Description,
              @SessionEmployeeID,
                                    @SessionEmployeeID,
                                    @DomainID)

                      SET @Output = 'Inserted Successfully.'

                      GOTO Finish
                  END
                ELSE
                  BEGIN
                      SET @Output = 'Already Exists!'

                      GOTO Finish
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
