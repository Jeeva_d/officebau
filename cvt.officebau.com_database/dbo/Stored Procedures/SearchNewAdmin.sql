﻿  
/****************************************************************************     
CREATED BY   : Dhanalakshmi. S    
CREATED DATE  : 14-FEB-2018    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
  [SearchNewAdmin]  ''    
 </summary>                              
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchNewAdmin] (@Name VARCHAR(100))  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 BEGIN TRY  
  SELECT  
   CM.ID AS ID  
     ,CM.FullName AS CompanyName  
     ,CM.Name AS Name  
     ,(SELECT  
     COUNT(1)  
    FROM tbl_EmployeeMaster  
    WHERE DomainID = CM.ID  
    AND IsDeleted = 0  
    AND IsActive = 0  
    AND ISNULL(FirstName, '') NOT LIKE '%Admin%')  
   AS ActiveCount  
     ,(SELECT  
     COUNT(1)  
    FROM tbl_BusinessUnit  
    WHERE DomainID = CM.ID  
    AND IsDeleted = 0  
    AND ParentID <> 0)  
   AS BusinessUnitCount  
     ,(SELECT  
     COUNT(1)  
    FROM tbl_EmployeeMaster  
    WHERE DomainID = CM.ID  
    AND IsDeleted = 0  
    AND ISNULL(FirstName, '') NOT LIKE '%Admin%')  
   AS NoOfEmployees  
     ,CM.IsActive AS IsActive  
     ,CM.EmailID AS EmailID  
     ,CM.ContactNo AS ContactNo,
     CreatedOn  
  FROM tbl_Company CM  
  WHERE CM.IsDeleted = 0  
  AND ISNULL(FullName, '') LIKE '%' + ISNULL(@Name, '') + '%'  
  ORDER BY FullName ASC  
 END TRY  
 BEGIN CATCH  
  DECLARE @ErrorMsg VARCHAR(100)  
      ,@ErrSeverity TINYINT;  
  SELECT  
   @ErrorMsg = ERROR_MESSAGE()  
     ,@ErrSeverity = ERROR_SEVERITY()  
  RAISERROR (@ErrorMsg, @ErrSeverity, 1)  
 END CATCH  
END
