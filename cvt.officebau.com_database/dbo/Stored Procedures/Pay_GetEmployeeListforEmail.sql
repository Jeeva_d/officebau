﻿/****************************************************************************                       
CREATED BY    :                      
CREATED DATE  :                       
MODIFIED BY   :                     
MODIFIED DATE :                    
<summary>     
[Pay_getemployeelistforemail] 11,2018,'2',1                   
</summary>                                               
*****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_GetEmployeeListforEmail] (@MonthId      INT,
                                                     @Year         INT,
                                                     @BusinessUnit VARCHAR(50),
                                                     @DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN
              DECLARE @DataSource TABLE
                (
                   ID      INT IDENTITY(1, 1),
                   [Value] NVARCHAR(128)
                )

              INSERT INTO @DataSource
                          ([Value])
              SELECT Item
              FROM   dbo.Splitstring (@BusinessUnit, ',')
              WHERE  Isnull(Item, '') <> ''

              INSERT INTO tbl_PaystubEmployeeList
                          (employeeID,
                           MonthID,
                           YearID,
                           DomainID)
              SELECT pay.EmployeeID AS EmployeeID,
                     pay.MonthId    AS MonthId,
                     pay.YearId,
                     pay.DomainId
              FROM   tbl_Pay_EmployeePayroll pay
                     JOIN tbl_employeemaster emp
                       ON emp.id = pay.EmployeeID
              WHERE  isProcessed = 1
                     AND pay.isdeleted = 0
                     AND monthid = @MonthID
                     AND yearid = @Year
                     AND ( emp.BaseLocationID IN (SELECT [Value]
                                                  FROM   @DataSource) )
                     AND NOT EXISTS(SELECT 1
                                    FROM   tbl_PaystubEmployeeList TP
                                    WHERE  TP.employeeID = pay.EmployeeID
                                           AND TP.MonthID = pay.MonthId
                                           AND TP.YearID = pay.YearId
                                           AND TP.DomainID = pay.DomainId)

              SELECT pay.EmployeeID AS ID,
                     emp.EmailID    AS EmailID
              FROM   tbl_Pay_EmployeePayroll pay
                     JOIN tbl_employeemaster emp
                       ON emp.id = pay.EmployeeID
              WHERE  isProcessed = 1
                     AND pay.isdeleted = 0
                     AND monthid = @MonthID
                     AND yearid = @Year
                     AND ( emp.BaseLocationID IN (SELECT [Value]
                                                  FROM   @DataSource) )
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
