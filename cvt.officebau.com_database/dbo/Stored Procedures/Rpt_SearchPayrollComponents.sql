﻿/****************************************************************************   
CREATED BY   :  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
         [Rpt_searchpayrollcomponents] 4,3,1,'1,2,3,4,5,6,7,8,9',null
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_SearchPayrollComponents] (@MonthID      INT,
                                                     @Year         INT,
                                                     @DomainID     INT,
                                                     @Location     VARCHAR(50),
                                                     @DepartmentID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SET @Year = (SELECT NAME
                       FROM   tbl_FinancialYear
                       WHERE  id = @Year
                              AND IsDeleted = 0
                              AND DomainId = @DomainID)

          DECLARE @DataSource TABLE
            (
               [Value] NVARCHAR(128)
            )

          INSERT INTO @DataSource
                      ([Value])
          SELECT Item
          FROM   dbo.Splitstring (@Location, ',')
          WHERE  Isnull(Item, '') <> ''

          -- Get Payroll Component Config Value
          SELECT *
          INTO   #tmpParollConfigValue
          FROM   [dbo].[Fn_PayrollComponentConfigValue]()

          SELECT BU.NAME                  AS BusinessUnit,
                 ep.BaseLocationID        AS BaseLocationID,
                 dep.NAME                 AS DepartmentName,
                 Sum(ep.NoofDaysPayable)  AS [No of Days Payable],
                 Sum(eps.Gross)           AS FixedGross,
                 Sum(GrossEarning)        AS Gross,
                 Sum(ep.Basic)            AS Basic,
                 Sum(ep.HRA)              AS HRA,
                 Sum(ep.MedicalAllowance) AS MedicalAllowance,
                 Sum(ep.Conveyance)       AS Conveyance,
                 Sum(ep.SplAllow)         AS SplAllow,
                 Sum(ep.OverTime)         AS OverTime,
                 Sum(ep.EducationAllow)   AS EducationAllowance,
                 Sum(ep.MagazineAllow)    AS PaperMagazine,
                 Sum(ep.MonsoonAllow)     AS MonsoonAllow,
                 Sum(ep.OtherEarning)     AS [Other Earning],
                 Sum(ep.EEPF)             AS EEPF,
                 Sum(ep.EEESI)            AS EEESI,
                 Sum(ep.TDS)              AS TDS,
                 Sum(ep.PT)               AS PT,
                 Sum(ep.Loans)            AS Loans,
                 Sum(ep.OtherDeduction)   AS [Other Deductions],
                 Sum(ep.ERPF)             AS ERPF,
                 Sum(ep.ERESI)            AS ERESI,
                 Sum(ep.Bonus)            AS Bonus,
                 Sum(ep.Gratuity)         AS Gratuity,
                 Sum(ep.PLI)              AS PLI,
                 Sum(ep.Mediclaim)        AS Mediclaim,
                 Sum(ep.MobileDataCard)   AS [Mobile/Datacard],
                 Sum(ep.OtherPerks)       AS OtherPerks,
                 Sum(ep.NetSalary)        AS NetSalary,
                 Sum(ep.CTC)              AS CTC
          INTO   #tempPayroll
          FROM   tbl_EmployeePayroll ep
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON emp.ID = ep.EmployeeId
                 LEFT JOIN tbl_BusinessUnit BU
                        ON bu.ID = ep.BaseLocationID
                 LEFT JOIN tbl_EmployeePayStructure eps
                        ON eps.Id = ep.EmployeePayStructureID
                 LEFT JOIN tbl_Month mon
                        ON mon.ID = ep.MonthId
                 LEFT JOIN tbl_Department dep
                        ON dep.ID = emp.DepartmentID
          WHERE  ep.MonthId = @MonthID
                 AND ep.YearId = @Year
                 AND ( ( @Location = Cast(0 AS VARCHAR)
                          OR @Location IS NULL )
                        OR ( EP.BaseLocationID IN (SELECT [Value]
                                                   FROM   @DataSource
                                                   WHERE  EMP.IsDeleted = 0
                                                          AND emp.DomainID = @DomainID) ) )
                 AND ( ( @DepartmentID IS NULL
                          OR Isnull(@DepartmentID, 0) = 0 )
                        OR EMP.DepartmentID = @DepartmentID )
                 AND ep.IsProcessed = 1
                 AND ep.IsDeleted = 0
                 AND ep.DomainId = @DomainID
          GROUP  BY dep.NAME,
                    BU.NAME,
                    ep.BaseLocationID

          SELECT *
                 --------------
                 ,
                 IsEducationAllow,
                 IsOverTime,
                 IsEEPF,
                 IsEEESI,
                 IsPT,
                 IsTDS,
                 IsERPF,
                 IsERESI,
                 IsGratuity,
                 IsPLI,
                 IsMediclaim,
                 IsMobileDatacard,
                 IsOtherPerks
          FROM   #tempPayroll tPR
                 LEFT JOIN #tmpParollConfigValue tpc
                        ON tpc.BusinessUnitID = tPR.BaselocationID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
