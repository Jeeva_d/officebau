﻿
/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>     
		select * from TDSDeclaration
		SearchTDSDeclaration 3,0,2,1,1
		SearchEmployeeTDSDeclaration 106,3,1,106,11
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchEmployeeTDSDeclaration] (@EmployeeID     INT,
                                                      @StartYear      INT,
                                                      @DomainID       INT,
                                                      @SessionID      INT,
                                                      @BusinessUnitID INT)
AS
  BEGIN
      SET NOCOUNT ON;
      SET @StartYear=(SELECT FinancialYear
                      FROM   tbl_FinancialYear
                      WHERE  ID = @StartYear
                             AND IsDeleted = 0
                             AND DomainID = @DomainID)
      SET @StartYear = CASE
                         WHEN Isnull(@StartYear, 0) != 0 THEN
                           @StartYear
                         ELSE
                           CASE
                             WHEN Month(Getdate()) < 4 THEN
                               Year(Getdate()) - 1
                             ELSE
                               Year(Getdate())
                           END
                       END

      DECLARE @StartMonth INT = 4 -- Fiscal year month starting
      DECLARE @StartDate DATE = Dateadd(MONTH, @StartMonth - 1, Dateadd(YEAR, @StartYear - 1900, 0)) -- Fiscal Year Start date
      DECLARE @EndDate DATE = Dateadd(DAY, -1, Dateadd(MONTH, @StartMonth - 1, Dateadd(YEAR, ( @StartYear + 1 ) - 1900, 0))) -- Fiscal Year End date
      BEGIN TRY
          SELECT DISTINCT emp.ID                                                                              EmployeeID,
                          emp.FirstName + ' ' + Isnull(emp.LastName, '')                                      EmployeeName,
                          emp.Code                                                                            EmployeeCode,
                          ( (SELECT Count(*)
                             FROM   TDSDeclaration
                             WHERE  IsDeleted = 0
                                    AND DomainID = @DomainID
                                    AND EmployeeID = emp.ID
                                    AND Isnull(Submitted, 0) != 0
                                    AND Isnull(Submitted, 0) != Isnull(Cleared, 0) + Isnull(Rejected, 0))
                            + (SELECT Count(*)
                               FROM   TDSHouseTax
                               WHERE  IsDeleted = 0
                                      AND DomainID = @DomainID
                                      AND EmployeeID = emp.ID
                                      AND Isnull(Submitted, 0) != 0
                                      AND Isnull(Submitted, 0) != Isnull(Cleared, 0) + Isnull(Rejected, 0)) ) NoOfRecords
          FROM   tbl_EmployeeMaster emp
                 LEFT JOIN TDSDeclaration decl
                        ON decl.EmployeeID = emp.ID
                 LEFT JOIN TDSHouseTax tax
                        ON tax.EmployeeID = emp.ID
          WHERE  emp.IsDeleted = 0
                 AND emp.DomainID = @DomainID
                 AND ( Isnull(decl.Submitted, 0) != 0
                        OR Isnull(tax.Submitted, 0) != 0 )
                 --AND ( Isnull(@EmployeeID, 0) = 0
                 --       OR decl.EmployeeID = Isnull(@EmployeeID, 0)
                 --       OR tax.EmployeeID = Isnull(@EmployeeID, 0) )
                 AND ( ( CONVERT (DATE, decl.ModifiedOn) BETWEEN @StartDate AND @EndDate )
                        OR ( CONVERT (DATE, tax.ModifiedOn) BETWEEN @StartDate AND @EndDate ) )
                 AND emp.BaseLocationID = @BusinessUnitID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
