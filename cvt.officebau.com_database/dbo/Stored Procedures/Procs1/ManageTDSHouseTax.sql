﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	ManageTDSSection
	SearchTDSDeclaration 3,0,2,1
	select * from TDSHouseTax
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageTDSHouseTax] (@ID                INT,
                                           @Component         VARCHAR(100),
                                           @Type              VARCHAR(20),
                                           @Declaration       MONEY,
                                           @Submitted         MONEY,
                                           @Cleared           MONEY,
                                           @Rejected          MONEY,
                                           @Remarks           VARCHAR(8000),
                                           @ApproverRemarks   VARCHAR(8000),
                                           @FinancialYearID   INT,
                                           @IsApprover        BIT,
                                           @HasDeleted        BIT,
                                           @SessionEmployeeID INT,
                                           @DomainID          INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(8000)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF( Isnull(@ID, 0) != 0 )
            BEGIN
                IF( @HasDeleted = 1 )
                  BEGIN
                      UPDATE TDSHouseTax
                      SET    IsDeleted = 1,
                             ModifiedBy = @SessionEmployeeID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID

                      SET @Output = 'Deleted Successfully.'

                      GOTO Finish
                  END

                IF( @HasDeleted = 0 )
                  BEGIN
                      IF @IsApprover = 0
                        BEGIN
                            UPDATE TDSHouseTax
                            SET    Declaration = @Declaration,
                                   Remarks = @Remarks,
                                   Submitted = @Submitted,
                                   ModifiedBy = @SessionEmployeeID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID

                            SET @Output = 'Updated Successfully.'

                            GOTO Finish
                        END

                      IF @IsApprover = 1
                        BEGIN
                            UPDATE TDSHouseTax
                            SET    Cleared = @Cleared,
                                   Rejected = @Rejected,
                                   ApproverRemarks = @ApproverRemarks,
                                   ModifiedBy = @SessionEmployeeID,
                                   ModifiedOn = Getdate()
                            WHERE  ID = @ID

                            SET @Output = 'Updated Successfully.'

                            GOTO Finish
                        END
                  END
            END

          IF( Isnull(@ID, 0) = 0 )
            BEGIN
                INSERT INTO TDSHouseTax
                            (EmployeeID,
                             Component,
                             [Type],
                             Declaration,
                             Remarks,
                             Submitted,
                             FinancialYearID,
                             CreatedBy,
                             ModifiedBy,
                             DomainID)
                VALUES      ( @SessionEmployeeID,
                              @Component,
                              @Type,
                              @Declaration,
                              @Remarks,
                              @Submitted,
                              @FinancialYearID,
                              @SessionEmployeeID,
                              @SessionEmployeeID,
                              @DomainID)

                SET @Output = 'Inserted Successfully.'

                GOTO Finish
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
