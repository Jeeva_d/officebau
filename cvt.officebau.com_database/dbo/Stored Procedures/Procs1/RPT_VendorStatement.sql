﻿/****************************************************************************                                                   
CREATED BY  :                                                
CREATED DATE :                                             
MODIFIED BY  :                                      
MODIFIED DATE :                                    
<summary>                                                
  [RPT_VendorStatement] '01-Jan-2017','01-Jan-2019',1,'godaddy'                                        
</summary>                                                                           
*****************************************************************************/                
CREATE PROCEDURE [RPT_VendorStatement](@StartDate  DATETIME,                
                                       @EndDate    DATETIME,                
                                       @DomainID   INT,                
                                       @LedgerName VARCHAR(100) = NULL)                
AS                
  BEGIN                
      SET NOCOUNT ON;                
                
      BEGIN TRY                
          DECLARE @LedgerID INT                
                
          IF( @LedgerName IS NULL                
               OR @LedgerName = '' )                
            SET @LedgerID = 0                
          ELSE                
            SET @LedgerID = (SELECT TOP 1 ID                
                             FROM   tblVendor                
                             WHERE  IsDeleted = 0                
                                    AND NAME = @LedgerName                
                                    AND DomainID = @DomainID);                
                
          WITH CTE                
               AS (Select  E.ID,E.Date AS Date,E.Date AS OrderByID,E.Type,          
(Ed.Amount * ED.QTY)+(SGST+CGST) AS Debit,NULL As Credit,          
Case When (IsLedger=1) Then l.Name Else p.Name End AS LedgerName,          
ED.Remarks AS Description ,'A' orderbyName         
from tblExpenseDetails ED           
left Join tblLedger l on l.ID=ed.LedgerID and IsLedger=1          
left Join tblProduct_v2 p on p.ID = ed.LedgerID and IsLedger=0          
left Join tblExpense e on e.ID = ed.ExpenseID          
 Where ed.IsDeleted=0          
 AND E.DomainID = @DomainID                
                          AND ( E.VendorID = @LedgerID )                
                          AND ( @StartDate IS NULL                
                                 OR E.[Date] >= @StartDate )                
                          AND ( @EndDate IS NULL                
                    OR E.[Date] <= @EndDate )                
          
 Union All Select Ep.ID,Ep.Date,E.Date AS OrderByID,CASE WHEN (E.Type='Bill') Then 'Payment'ELSE e.Type END, null,Amount ,Case When (BankID=0) Then 'Cash' else b.DisplayName End,Ep.Reference          
 ,'B' orderbyName from tblExpensePaymentMapping EPM          
 Join tblExpensePayment ep on EPM.ExpensePaymentID=ep.ID          
 Join tblBank b on b.ID=Ep.BankID          
 Join tblExpense e on epm.ExpenseID = e.ID          
 Where Epm.IsDeleted=0 AND E.DomainID = @DomainID                
                          AND ( E.VendorID = @LedgerID )                
                          AND ( @StartDate IS NULL                
                                 OR E.[Date] >= @StartDate )                
                          AND ( @EndDate IS NULL                
                    OR E.[Date] <= @EndDate )             
          
 Union All          
 Select eh.ExpenseId,eh.Date,E.Date AS OrderByID,'Holdings' ,null,Amount,l.Name,null ,'C' orderbyName from tblExpenseHoldings Eh          
 Join tblLedger l on l.ID = eh.LedgerId          
 Join tblExpense e on eh.ExpenseId = e.ID          
 where eh.Isdeleted=0          
  AND E.DomainID = @DomainID                
                          AND ( E.VendorID = @LedgerID )                
                          AND ( @StartDate IS NULL                
               OR E.[Date] >= @StartDate )                
                          AND ( @EndDate IS NULL                
    OR E.[Date] <= @EndDate )       
           
      
Union All       
Select p.Id,p.PaymentDate,p.PaymentDate,'Payments',TotalAmount,null,l.Name,Description,'D' from tbl_Payment p       
Join tblLedger l on p.LedgerId=l.ID      
Where p.Isdeleted=0 AND p.DomainID = @DomainID                
                        AND ( p.PartyName = @LedgerName )                
                          AND ( @StartDate IS NULL                
                                 OR p.PaymentDate>= @StartDate )                
                          AND ( @EndDate IS NULL                
                                 OR p.PaymentDate <= @EndDate )      
Union All       
Select p.Id,p.PaymentDate,p.PaymentDate,'Payments',null,TotalAmount,Case When(p.BankID=0) Then 'Cash' Else  l.Name END,p.Description,      
'E' from tbl_Payment p       
Join tblBank l on p.BankID=l.ID      
Where p.Isdeleted=0 AND p.DomainID = @DomainID                
                        AND ( p.PartyName = @LedgerName )                
                          AND ( @StartDate IS NULL                
                                 OR p.PaymentDate>= @StartDate )                
                          AND ( @EndDate IS NULL                
                                 OR p.PaymentDate <= @EndDate ) 
UNION ALL
 Select eh.ID,eh.JournalDate,Eh.JournalDate AS OrderByID,'Journal' ,Debit,Credit,eh.JournalNo,eh.Description ,'F' orderbyName from 
 tblJournal Eh          
 Join tblVendor l on l.ID = eh.LedgerProductID and eh.type='Vendor'          
 where eh.Isdeleted=0  AND eh.DomainID = @DomainID                
                        AND ( eh.LedgerProductID = @LedgerID )                
                          AND ( @StartDate IS NULL                
                                 OR eh.JournalDate>= @StartDate )                
                          AND ( @EndDate IS NULL                
                                 OR eh.JournalDate <= @EndDate ) 
               
)                
         Select * INTO #TempResult from CTE               
                
          -- OpeningBalance and OutstandingBalance Calculation                                          
          DECLARE @OpeningBalance MONEY                
  DECLARE @OutstandingBalance MONEY                
                
          SET @OutstandingBalance = ( (SELECT Sum(Isnull(Credit, 0)) - Sum(Isnull(Debit, 0))                
                                       FROM   #TempResult TEMP                
)            
                 + Isnull(@OpeningBalance, 0) )                
                
          SELECT @OpeningBalance     AS OpeningBalance,                
                 @OutstandingBalance AS OutstandingBalance                
                
          SELECT ID,                
                 Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-') [Date],                
                 [Type],                
				 LedgerName As Particulars,                
                 CASE WHEN( Sum(Debit)=0) Then null ELSE  Sum(Debit) END                                          Debit,                
                 CASE WHEN( Sum(Credit)=0) Then null ELSE  Sum(Credit) END                                         Credit,                
                   Description             [Description]              
     FROM   #TempResult V                
          GROUP  BY ID,                
                    Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-'),                
                    [Type],                
                    Description,                
                    LedgerName ,                
                    OrderByID,orderbyName                
          ORDER  BY OrderByID,                
                    orderbyName        
                
          DROP TABLE #TempResult                
      END TRY                
      BEGIN CATCH                
          DECLARE @ErrorMsg    VARCHAR(100),                
                  @ErrSeverity TINYINT                
          SELECT @ErrorMsg = Error_message(),                
                 @ErrSeverity = Error_severity()                
          RAISERROR(@ErrorMsg,@ErrSeverity,1)                
      END CATCH                
  END
