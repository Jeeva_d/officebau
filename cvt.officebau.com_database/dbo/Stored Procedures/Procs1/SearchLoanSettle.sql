﻿  
/****************************************************************************     
CREATED BY  :  JENNIFER.S  
CREATED DATE :  29-Jul-2017  
MODIFIED BY  :   Ajith N  
MODIFIED DATE :   18 Jan 2018  
 <summary>   
          [SearchLoanSettle] 0,1,224  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchLoanSettle] (@StatusID   INT,  
                                          @DomainID   INT,  
                                          @EmployeeID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN  
              SELECT LNR.ID                           LoanID,  
                     Isnull(LNS.ID, 0)                ID,  
                     LNR.LoanAmount                   Amount,  
                     Isnull(REQ.EmpCodePattern, '') + REQ.Code + ' - ' + REQ.FullName Requester,  
                     LNR.CreatedOn,  
                     Isnull(FIN.EmpCodePattern, '') + FIN.Code + ' - ' + FIN.FullName Approver,  
                     LNR.ApprovedDate,  
                     --TS.Code                          AS [status],  
                 CASE  
                       WHEN LNF.LoanRequestID IS NOT NULL THEN  
                         (SELECT Code  
                          FROM   tbl_Status  
                          WHERE  IsDeleted = 0  
                                 AND Type = 'Amortization'  
                                 AND Code = 'Foreclosed')  
                       ELSE  
         (SELECT Code  
                          FROM   tbl_Status  
                          WHERE  IsDeleted = 0  
                                 AND Type = 'Loan'  
                                AND ID = LNR.StatusID)  
                           
                     END                                          AS [Status],  
                     --LNR.TotalAmount                  AS TotalAmount  
       CASE  
         WHEN LNF.LoanRequestID IS NOT NULL THEN  
        (SELECT SUM(ISNULL(Amount, 0)) + SUM(ISNULL(InterestAmount, 0))  
         FROM tbl_LoanAmortization   
         WHERE LoanRequestID = LNR.ID   
               AND IsDeleted = 0   
               AND StatusID IN (SELECT ID  
                                              FROM   tbl_Status  
                                              WHERE  [Type] = 'Amortization'  
                                                     AND Code != 'Open'))   
         ELSE    
         LNR.TotalAmount END     AS TotalAmount  
              FROM   tbl_LoanRequest LNR  
                     JOIN tbl_EmployeeMaster REQ  
                       ON LNR.EmployeeID = REQ.ID  
                     JOIN tbl_EmployeeMaster FIN  
                       ON LNR.FinancialApproverID = FIN.ID  
                     LEFT JOIN tbl_LoanSettle LNS  
                            ON LNR.ID = LNS.LoanRequestID  
                     LEFT JOIN tbl_Status TS  
                            ON TS.ID = LNR.StatusID  
      LEFT JOIN tbl_LoanForeclosure LNF  
                          ON LNS.LoanRequestID = LNF.LoanRequestID  
              WHERE  LNR.DomainID = @DomainID  
                     AND LNR.IsDeleted = 0  
                     AND LNR.FinancialStatusID = (SELECT ID  
                                                  FROM   tbl_status  
                                                  WHERE  [Type] = 'Loan'  
                                                         AND Code = 'Approved')  
                     AND ( LNR.StatusID = @StatusID  
                            OR Isnull(@StatusID, 0) = 0 )  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
