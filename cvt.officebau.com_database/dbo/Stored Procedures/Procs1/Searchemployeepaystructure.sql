﻿  
/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  : 27-JUN-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
   [Searchemployeepaystructure] 1,0,1,'0,3,4,5,1,6,2,7'  
 </summary>                            
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchemployeepaystructure] (@DomainID   INT,  
                                                    @EmployeeID INT,  
                                                    @IsActive   BIT,  
                                                    @Location   VARCHAR(100))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          -- TODO : Need to replace XML stuff  
          DECLARE @DataSource TABLE  
            (  
               [Value] NVARCHAR(128)  
            )  
  
          INSERT INTO @DataSource  
                      ([Value])  
          SELECT Item  
          FROM   dbo.Splitstring (@Location, ',')  
          WHERE  Isnull(Item, '') <> '';  
  
          WITH cte  
               AS (SELECT EPS.Id                                       AS ID,  
                          EPS.EmployeeId                               AS EmployeeID,  
                          Isnull(EMP.EmpCodePattern, '') + Isnull(EMP.Code, '') + ' - ' + EMP.FullName AS EmployeeName,  
                          EPS.CompanyPayStubId                         AS CompanyPayStucId,  
                          CPS.NAME + ' - ' + BU.NAME                   AS CompanyPayStucName,  
                          EPS.Gross                                    AS Gross,  
                          EPS.EffectiveFrom                            AS EffictiveFrom,  
                          EMP.Code                                     AS EmployeeCode,  
                          (SELECT Count(1)  
                           FROM   tbl_EmployeePayStructure  
                           WHERE  IsDeleted = 0  
                                  AND EmployeeId = EPS.EmployeeId)     AS Counts,  
                          EPS.ModifiedOn                               AS ModifiedOn,  
                          Row_number()  
                            OVER (  
                              PARTITION BY EPS.EmployeeId  
                              ORDER BY EPS.EffectiveFrom DESC)         AS rowno,  
                          EMP.IsActive                                 AS IsActive  
                   FROM   tbl_EmployeePayStructure EPS  
                          LEFT JOIN tbl_CompanyPayStructure CPS  
                                 ON CPS.Id = EPS.CompanyPayStubId  
                          LEFT JOIN tbl_EmployeeMaster EMP  
                                 ON emp.ID = EPS.EmployeeId  
                          LEFT JOIN tbl_BusinessUnit BU  
                                 ON BU.ID = CPS.BusinessUnitID  
                   WHERE  EPS.DomainId = @DomainID  
                          AND EPS.IsDeleted = 0  
                          AND emp.IsDeleted = 0  
                          AND ( @Location = Cast(0 AS VARCHAR)  
                                 OR @Location IS NULL )  
                           OR ( emp.BaseLocationID IN (SELECT [Value]  
                                                       FROM   @DataSource  
                                                       WHERE  EPS.IsDeleted = 0) )  
                              AND ( EMP.ID = @EmployeeID  
                                     OR Isnull (@EmployeeID, 0) = 0 )  
                   GROUP  BY EPS.Id,  
                             EPS.EmployeeId,  
                             EMP.FullName,  
                             EMP.Code,  
                             EPS.CompanyPayStubId,  
                             CPS.NAME + ' - ' + BU.NAME,  
                             EPS.Gross,  
                             EPS.EffectiveFrom,  
                             EMP.BaseLocationID,  
                             EPS.ModifiedOn,  
                             EMP.IsActive,
                             EMP.EmpCodePattern)  
          SELECT ID,  
           EmployeeID,  
                 EmployeeCode,  
                 EmployeeName,  
                 CompanyPayStucId,  
                 CompanyPayStucName,  
                 Gross,  
                 EffictiveFrom,  
                 Counts,  
                 ModifiedOn,  
                 IsActive  
          INTO   #temp  
          FROM   cte  
          WHERE  rowno IN( 1 )  
          GROUP  BY ID,  
                    EmployeeID,  
                    EmployeeCode,  
                    EmployeeName,  
                    CompanyPayStucId,  
                    CompanyPayStucName,  
                    Gross,  
                    Counts,  
                    EffictiveFrom,  
                    ModifiedOn,  
                    IsActive  
          ORDER  BY ModifiedOn DESC  
  
          IF( @IsActive = 1  
              AND @EmployeeID = 0 )  
            BEGIN  
                SELECT *  
                FROM   #temp  
                WHERE  IsActive = 0  
            END  
          ELSE  
            BEGIN  
                SELECT *  
                FROM   #temp  
            END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
