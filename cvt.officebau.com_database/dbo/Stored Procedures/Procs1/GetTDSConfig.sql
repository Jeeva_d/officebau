﻿/****************************************************************************     
CREATED BY   :   DHANALAKSHMI. S  
CREATED DATE  :     
MODIFIED BY   :   
MODIFIED DATE  :     
 <summary>           
   [GetTDSConfig] 1
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[GetTDSConfig] (@DomainID INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SELECT tds.TDSKey AS Value,   
                 tds.ComponentId AS PayrollComponentId,   
                 emp.FullName   AS ModifiedBy,    
                 tds.ModifiedOn AS ModifiedOn    
          FROM   tbl_TDSComponentMapping tds  
                 LEFT JOIN tbl_EmployeeMaster emp    
                        ON emp.ID = tds.ModifiedBy    
          WHERE  tds.DomainID = @DomainID   
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
