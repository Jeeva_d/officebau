﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	19-SEP-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
 [Getemployeeofferletterdetail] 1479
 </summary>                          
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetEmployeeOfferLetterDetail] (@Employeeid INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN
              SELECT Isnull(COM.NAME, '') + '/'
                     + Isnull(Substring( Upper( BU.NAME), 1, 3), '')
                     + '/' + Isnull(DEP.NAME, '') + '/'
                     + Isnull(E.Code, '') + '/'
                     + CASE
                         WHEN( ApprovedOn IS NULL
                                OR ApprovedOn = '' ) THEN
                           Cast( Year(DOJ) AS VARCHAR) + '-'
                           + Cast( Year(DOJ) + 1 AS VARCHAR)
                         ELSE
                           Cast( Year(ApprovedOn) AS VARCHAR) + ' - '
                           + Cast( Year(ApprovedOn) + 1 AS VARCHAR)
                       END                                   AS ReferenceNo,
                     --CASE
                     --  WHEN( doj >= '01-APR-2017'
                     --         OR ApprovedOn >= '01-APR-2017' ) THEN 1
                     --  ELSE 0
                     --END                            AS Eligiable,
                     1                                       AS Eligiable,
                     (SELECT TOP 1 Gross
                      FROM   tbl_EmployeePayStructure
                      WHERE  employeeID = @Employeeid
                             AND IsDeleted = 0
                      ORDER  BY EffectiveFrom ASC)           AS Gross,
                     BU.NAME                                 AS BusinessUnit,
                     COM.FullName                            AS CompanyName,
                     (SELECT TOP 1 Code + ' - ' + FullName
                      FROM   tbl_EmployeeMaster
                      WHERE  FullName LIKE '%Ramachandran%') AS ReportingToName,
                     (SELECT TOP 1 ID
                      FROM   tbl_EmployeeMaster
                      WHERE  FullName LIKE '%Ramachandran%') AS ReportingToID
              FROM   tbl_EmployeeMaster E
                     JOIN tbl_BusinessUnit BU
                       ON BU.ID = E.BaseLocationID
                     LEFT JOIN tbl_Company COM
                            ON COM.ID = E.DomainID
                     LEFT JOIN tbl_Department DEP
                            ON DEP.ID = E.DepartmentID
              WHERE  E.ID = @Employeeid
                     AND E.IsDeleted = 0
          --AND ( doj >= '01-APR-2017'
          --       OR ApprovedOn >= '01-APR-2017' )
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
