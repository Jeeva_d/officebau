﻿/****************************************************************************                             
CREATED BY   : Dhanalakshmi S                          
CREATED DATE  : 3 Oct 2018                            
MODIFIED BY   :                             
MODIFIED DATE  :                             
 <summary>                                    
     GetPaystubComponentsList    1                 
 </summary>                                                      
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetPaystubComponentsList] (@DomainID INT)
AS

BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        SELECT pc.Description,
               pc.ID                       AS ComponentID,
               p.Type,
               Isnull(p.ID, 0)             AS ID,
               CM.ID                       AS TypeID,
               (SELECT TOP 1(EM.FullName)
                FROM   tbl_Pay_PaystubConfiguration P
                     LEFT JOIN tbl_EmployeeMaster EM
                      ON EM.ID = P.ModifiedBy
                WHERE  p.IsDeleted = 0
                ORDER  BY P.ModifiedOn DESC)  AS ModifiedBy,
               (SELECT TOP 1( ModifiedOn )
                FROM   tbl_Pay_PaystubConfiguration
                WHERE  IsDeleted = 0
                ORDER  BY ModifiedOn DESC) AS ModifiedOn,
               (SELECT Count(*)
                FROM   tbl_Pay_PaystubConfiguration
                WHERE  IsDeleted = 0)      AS Count
        FROM   tbl_Pay_PayrollCompontents PC
               LEFT JOIN tbl_Pay_PaystubConfiguration p
                      ON p.ComponentID = pc.ID
                         AND p.IsDeleted = 0
               LEFT JOIN tbl_CodeMaster CM
                      ON P.[Type] = CM.Code
               LEFT JOIN tbl_EmployeeMaster EM
                      ON EM.ID = P.ModifiedBy
        WHERE  pc.IsDeleted = 0
               AND Pc.DomainID = @DomainID
        ORDER  BY Ordinal
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMsg    VARCHAR(100),
                @ErrSeverity TINYINT;
        SELECT @ErrorMsg = Error_message(),
               @ErrSeverity = Error_severity()
        RAISERROR(@ErrorMsg,@ErrSeverity,1)
    END CATCH
END
