﻿  
/****************************************************************************     
CREATED BY   : Jeeva    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
 [DBA_GetAllTableForDomain] 1    
 </summary>                             
 *****************************************************************************/  
  
CREATE PROCEDURE [dbo].[DBA_GetAllTableForDomain](@DomainID INT)  
AS  
     BEGIN  
         SET NOCOUNT ON;  
         BEGIN TRY  
             BEGIN TRANSACTION;  
             SELECT ROW_NUMBER() OVER(  
                    ORDER BY ta.name) AS rowNum,   
                    ta.name TableName  
             INTO #temp  
             FROM sys.tables ta  
                  INNER JOIN sys.partitions pa ON pa.OBJECT_ID = ta.OBJECT_ID  
                  INNER JOIN sys.schemas sc ON ta.schema_id = sc.schema_id  
                  JOIN INFORMATION_SCHEMA.columns c ON c.TABLE_NAME = ta.name  
                                                    And  c.COLUMN_NAME ='DomainID'
             WHERE ta.is_ms_shipped = 0  
                   AND pa.index_id IN(1, 0)  ANd ta.Name Not IN('tbl_login','tbl_LoginHistory','tbl_PasswordAudit','tbl_PasswordReset','tblCashBucket','tblGstHSN','tbl_PaystubEmployeeList')
             GROUP BY sc.name,   
                      ta.name  
             ORDER BY ta.name ASC;  
             CREATE TABLE #tempTable  
             (TableName     VARCHAR(200),   
              tableRowCount INT,   
              ID            INT  
             );  
             DECLARE @totCount INT=  
             (  
                 SELECT COUNT(1)  
                 FROM #temp  
             );  
             DECLARE @count INT= 1, @tableName VARCHAR(200)= '', @tableRowCount INT= 0;  
             WHILE(@count < @totCount)  
                 BEGIN  
                     SET @tableName =  
                     (  
                         SELECT TableName  
                         FROM #temp  
                         WHERE rowNum = @count  
                     );  
                     EXEC ('insert into #tempTable select '''+@tableName+''' , (select count (*) from '+@tableName+' where DomainID = '+@DomainID+'),'+@count);  
                     SET @count = @count + 1;  
                 END;  
             DELETE FROM #tempTable  
             WHERE tableRowCount = 0;  
             SELECT tablename,   
                    tableRowCount  
             FROM #tempTable;  
             WHILE(  
             (  
                 SELECT COUNT(1)  
                 FROM #tempTable  
             ) > 0)  
                 BEGIN  
                     SET @count =  
                     (  
                         SELECT TOP 1 ID  
                         FROM #tempTable  
                     );  
                     SET @tableName =  
                     (  
                         SELECT tablename  
                         FROM #tempTable  
                         WHERE id = @count  
                     );  
                     EXEC ('Select a.*,e.FullName As [Created_by],e1.FullName as [Modified_by] from '+@tableName+' a  Join tbl_employeemaster e on a.CreatedBy=e.ID Join tbl_employeemaster e1 on a.Modifiedby =e1.ID Where a.DomainID = '+@DomainID);  
                     DELETE FROM #tempTable  
                     WHERE ID = @count;  
                 END;  
             DROP TABLE #temp, #tempTable;  
             COMMIT TRANSACTION;  
         END TRY  
         BEGIN CATCH  
             ROLLBACK TRANSACTION;  
             DECLARE @ErrorMsg VARCHAR(100), @ErrSeverity TINYINT;  
             SELECT @ErrorMsg = ERROR_MESSAGE(),   
                    @ErrSeverity = ERROR_SEVERITY();  
             RAISERROR(@ErrorMsg, @ErrSeverity, 1);  
         END CATCH;  
     END;
