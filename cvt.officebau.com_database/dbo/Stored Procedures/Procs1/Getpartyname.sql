﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar
CREATED DATE		:	
MODIFIED BY			:
MODIFIED DATE		:	
 <summary>   
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getpartyname] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON

      BEGIN TRY
          SELECT ID                     AS ID,
                 NAME + ' ( Customer )' AS NAME
          FROM   tblCustomer
          WHERE  IsDeleted = 0
                 AND DomainID = @DomainID
          UNION
          SELECT ID                   AS ID,
                 NAME + ' ( Vendor )' AS NAME
          FROM   tblVendor
          WHERE  IsDeleted = 0
                 AND DomainID = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 






