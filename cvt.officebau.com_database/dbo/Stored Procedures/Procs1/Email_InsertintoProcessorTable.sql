﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	20-SEP-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	 
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Email_InsertintoProcessorTable] (@FromID     VARCHAR(200),
                                                        @ToID       VARCHAR(max),
                                                        @cc         VARCHAR(max),
                                                        @Bcc        VARCHAR(max),
                                                        @Subject    VARCHAR(max),
                                                        @content    VARCHAR(max),
                                                        @ScreenType VARCHAR(50),
                                                        @FileID     VARCHAR(50),
                                                        @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(8000) = 'Operation Failed!'
      DECLARE @IsAttachment BIT = (SELECT IsAttachment
         FROM   tblEmailConfig
         WHERE  DomainID = @DomainID
                AND [KEY] = @ScreenType)

      BEGIN TRY
          BEGIN TRANSACTION

          INSERT INTO tbl_emailProcessor
                      (fromID,
                       ToID,
                       CC,
                       BCC,
                       Subject,
                       Content,
                       Attachment)
          VALUES      (@FromID,
                       @ToID,
                       @cc,
                       @Bcc,
                       @Subject,
                       @content,
                       ( CASE
                           WHEN @IsAttachment = 1 THEN
                             @FileID
                           ELSE
                             NULL
                         END ))

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
