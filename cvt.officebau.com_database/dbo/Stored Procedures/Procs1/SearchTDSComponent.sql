﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>     
		select * from TDSSection
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchTDSComponent] (@Code     VARCHAR(20),
                                             @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

	  DECLARE @FYID INT = 0

      BEGIN TRY
          SELECT comp.ID                            ID,
                 comp.SectionID                     SectionID,
                 sec.Code                           Section,
                 comp.Code                          Code,
                 comp.Rules                         Rules,
                 comp.Value                         Value,
                 comp.[Description]                 [Description],
                 emp.FullName ModifiedBy,
                 comp.ModifiedOn                    ModifiedOn,
                 comp.FYID                          AS FinancialYearID
          FROM   TDSComponent comp
                 JOIN tbl_EmployeeMaster emp
                   ON emp.ID = comp.ModifiedBy
                 JOIN TDSSection sec
                   ON sec.ID = comp.SectionID
          WHERE  comp.IsDeleted = 0
                 AND comp.DomainID = @DomainID
                 AND comp.Code LIKE '%' + Isnull(@Code, '') + '%'
				 AND (@FYID = 0 OR comp.FYID	= @FYID)
          ORDER  BY sec.Code
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
