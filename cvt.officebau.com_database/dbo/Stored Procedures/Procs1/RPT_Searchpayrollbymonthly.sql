﻿/****************************************************************************     
CREATED BY   :    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>   
         [RPT_Searchpayrollbymonthly] 6,4,1,'2',NULL  
           
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[RPT_Searchpayrollbymonthly] (@MonthID      INT,  
                                                     @Year         INT,  
                                                     @DomainID     INT,  
                                                     @Location     VARCHAR(50),  
                                                     @DepartmentID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          SET @Year = (SELECT NAME  
                       FROM   tbl_FinancialYear  
                       WHERE  id = @Year  
                              AND IsDeleted = 0  
                              AND DomainId = @DomainID)  
  
          DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'  
            + CONVERT(VARCHAR(10), @Year)  
          DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth))  
          DECLARE @DataSource TABLE  
            (  
               [Value] NVARCHAR(128)  
            )  
  
          INSERT INTO @DataSource  
                      ([Value])  
          SELECT Item  
          FROM   dbo.Splitstring (@Location, ',')  
          WHERE  Isnull(Item, '') <> ''  
  
          -- Get Payroll Component Config Value  
          SELECT *  
          INTO   #tmpParollConfigValue  
          FROM   [dbo].[Fn_PayrollComponentConfigValue]()  
  
          SELECT EMP.FirstName + ' ' + Isnull(emp.LastName, '') AS EmployeeName,  
                 Substring(Upper(BU.NAME), 1, 3) + '_' + EMP.Code  
                 + '_' + Isnull(dep.NAME, '')                   AS EmployeeCode,  
                 Isnull(EMP.EmpCodePattern, '') + EMP.Code                                       AS EmployeeNo,  
                 BU.NAME                                        AS BusinessUnit,  
                 mon.Code + '-' + Cast(ep.YearId AS VARCHAR)    AS MonthYear,  
                 EMP.DOJ                                        AS DOJ,  
                 ep.NoofDaysPayable                             AS [No of Days Payable],  
                 eps.Gross                                      AS FixedGross,  
                 ep.GrossEarning                                AS Gross,  
                 ep.Basic                                       AS Basic,  
                 ep.HRA                                         AS HRA,  
                 ep.MedicalAllowance                            AS MedicalAllowance,  
                 ep.Conveyance                                  AS Conveyance,  
                 ep.SplAllow                                    AS SplAllow,  
                 ep.OverTime                                    AS OverTime,  
                 ep.EducationAllow                              AS EducationAllowance,  
                 ep.MagazineAllow                               AS PaperMagazine,  
                 ep.MonsoonAllow                                AS MonsoonAllow,  
                 ep.OtherEarning                                AS [Other Earning],  
                 ep.EEPF                                        AS EEPF,  
                 ep.EEESI                                       AS EEESI,  
                 ep.TDS                                         AS TDS,  
                 ep.PT                                          AS PT,  
                 ep.Loans                                       AS Loans,  
                 ep.OtherDeduction                              AS [Other Deductions],  
                 ep.ERPF                                        AS ERPF,  
                 ep.ERESI                                       AS ERESI,  
                 ep.Bonus                                       AS Bonus,  
           ep.Gratuity                                    AS Gratuity,  
                 ep.PLI                                         AS PLI,  
                 ep.Mediclaim                                   AS Mediclaim,  
                 ep.MobileDataCard                              AS [Mobile/Datacard],  
                 ep.OtherPerks                                  AS OtherPerks,  
                 ep.NetSalary                                   AS NetSalary,  
                 ep.CTC                                         AS CTC,  
                 @Workdays - ep.NoofDaysPayable                 AS LOPDays  
                 --------------  
                 ,  
                 IsEducationAllow,  
                 IsOverTime,  
                 IsEEPF,  
                 IsEEESI,  
                 IsPT,  
                 IsTDS,  
                 IsERPF,  
                 IsERESI,  
                 IsGratuity,  
                 IsPLI,  
                 IsMediclaim,  
                 IsMobileDatacard,  
                 IsOtherPerks  
          FROM   tbl_EmployeePayroll ep  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON emp.ID = ep.EmployeeId  
                 LEFT JOIN tbl_BusinessUnit BU  
                        ON bu.ID = ep.BaseLocationID  
                 LEFT JOIN tbl_EmployeePayStructure eps  
                        ON eps.Id = ep.EmployeePayStructureID  
                 LEFT JOIN tbl_Month mon  
                        ON mon.ID = ep.MonthId  
                 LEFT JOIN tbl_Department dep  
                        ON dep.ID = emp.DepartmentID  
                 LEFT JOIN #tmpParollConfigValue tpc  
                        ON tpc.BusinessUnitID = ep.BaselocationID  
          WHERE  ep.MonthId = @MonthID  
                 AND ep.YearId = @Year  
                 AND ( ( @Location = Cast(0 AS VARCHAR)  
                          OR @Location IS NULL )  
                        OR ( EP.BaseLocationID IN (SELECT [Value]  
                                                   FROM   @DataSource  
                                                   WHERE  EMP.IsDeleted = 0  
                                                          AND emp.DomainID = @DomainID) ) )  
                 AND ( ( @DepartmentID IS NULL  
                          OR Isnull(@DepartmentID, 0) = 0 )  
                        OR EMP.DepartmentID = @DepartmentID )  
                 AND ep.IsProcessed = 1  
                 AND ep.IsDeleted = 0  
                 AND ep.DomainId = @DomainID  
                 AND ep.DomainId = @DomainID  
          --AND ep.EmployeeId = @EmployeeID   
          ORDER  BY EmployeeName ASC,  
                    BusinessUnit ASC  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
