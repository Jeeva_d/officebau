﻿/****************************************************************************     
CREATED BY   : Dhanalakshmi    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>     
 [ManageCostCenter]  3,'Test1','Test1',0,1,2    
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ManageCostCenter] (@ID               INT,    
                                          @Name             VARCHAR(100),    
                                          @Remarks          VARCHAR(1000),    
                                          @HasDeleted       BIT,    
                                          @SessionID        INT,    
                                          @DomainID         INT)    
AS    
    
BEGIN    
    SET NOCOUNT ON;    
    
    DECLARE @Output VARCHAR(1000)    
    
    BEGIN TRY    
        BEGIN TRANSACTION    
    
        IF( @HasDeleted = 1 )    
          BEGIN    
              IF( (SELECT Count(1)    
                   FROM   tblExpense    
                   WHERE  @ID = CostCenterID    
                          AND DomainID = @DomainID    
                          AND IsDeleted = 0) > 0 )    
                BEGIN    
                    SET @Output = (SELECT [Message]    
                                   FROM   tblErrorMessage    
                                   WHERE  [Type] = 'Warning'    
                                          AND Code = 'RCD_REF'    
                                          AND IsDeleted = 0) --'The record is referred.'    
    
                    GOTO Finish    
                END    
    
              IF( (SELECT Count(1)    
                   FROM   tblInvoice    
                   WHERE  @ID = RevenueCenterID    
                          AND DomainID = @DomainID    
                          AND IsDeleted = 0) > 0 )    
                BEGIN    
                    SET @Output = (SELECT [Message]    
                                   FROM   tblErrorMessage    
                                   WHERE  [Type] = 'Warning'    
                                          AND Code = 'RCD_REF'    
                                          AND IsDeleted = 0) --'The record is referred.'    
    
                    GOTO Finish    
                END    
              ELSE    
                BEGIN    
                    UPDATE tblCostCenter    
                    SET    IsDeleted = 1    
                    WHERE  ID = @ID    
                           AND DomainID = @DomainID    
    
                    SET @Output = (SELECT [Message]    
                                   FROM   tblErrorMessage    
                                   WHERE  [Type] = 'Information'    
                                          AND Code = 'RCD_DEL'    
                                          AND IsDeleted = 0) --'Deleted Successfully'    
    
                    GOTO Finish    
                END    
          END    
        ELSE    
          BEGIN    
              IF( @ID = 0 )    
                BEGIN    
                    IF( (SELECT Count(1)    
                         FROM   tblCostCenter    
                         WHERE  NAME = @Name    
                                AND DomainID = @DomainID    
                                AND IsDeleted = 0) = 0 )    
                      BEGIN    
                          INSERT INTO tblCostCenter    
                                      (NAME,    
                                       Remarks,    
                                       CreatedBy,    
                                       ModifiedBy,    
                                       DomainID)    
                          VALUES      (@Name,    
                                       @Remarks,    
                                       @SessionID,    
                                       @SessionID,    
                                       @DomainID)    
    
                          SET @Output = (SELECT [Message]    
                 FROM   tblErrorMessage    
                                         WHERE  [Type] = 'Information'    
                                                AND Code = 'RCD_INS'    
                                         AND IsDeleted = 0) --'Inserted Successfully'    
                      END    
                    ELSE    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Warning'    
                                            AND Code = 'RCD_EXIST'    
                                            AND IsDeleted = 0) --'Already Exists'    
                END    
              ELSE    
                BEGIN    
                    IF( (SELECT Count(1)    
                         FROM   tblCostCenter    
                         WHERE  NAME = @Name    
                                AND DomainID = @DomainID    
                                AND ID <> @ID    
                                AND IsDeleted = 0) = 0 )    
                      BEGIN    
                          UPDATE tblCostCenter    
                          SET    NAME = @Name,    
                                 Remarks = @Remarks,    
                                 ModifiedBY = @SessionID,    
                                 ModifiedOn = Getdate()    
                          WHERE  ID = @ID    
                                 AND DomainID = @DomainID    
    
                          SET @Output = (SELECT [Message]    
                                         FROM   tblErrorMessage    
                                         WHERE  [Type] = 'Information'    
                                                AND Code = 'RCD_UPD'    
                                                AND IsDeleted = 0) --'Updated Successfully'    
                      END    
                    ELSE    
                      BEGIN    
                          SET @Output = (SELECT [Message]    
                                         FROM   tblErrorMessage    
                                         WHERE  [Type] = 'Warning'    
                                                AND Code = 'RCD_EXIST'    
                                                AND IsDeleted = 0) --'Already Exists'    
                      END    
                END    
          END    
    
        FINISH:    
    
        SELECT @Output    
    
        COMMIT TRANSACTION    
    END TRY    
    BEGIN CATCH    
        ROLLBACK TRANSACTION    
        DECLARE @ErrorMsg    VARCHAR(100),    
                @ErrSeverity TINYINT    
        SELECT @ErrorMsg = Error_message(),    
               @ErrSeverity = Error_severity()    
        RAISERROR(@ErrorMsg,@ErrSeverity,1)    
    END CATCH    
END
