﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	
MODIFIED BY			:	SIVA
MODIFIED DATE		:	30-11-2016
 <summary>        
    Rpt_income null,'2016-12-1','2016-11-30',null,null
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_Income] (@Days      INT,
                                     @ToDate    DATE,
                                     @FromDate  DATE,
                                     @VendorID  VARCHAR(100),
                                     @CreatedBy VARCHAR(100))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF( Isnull(@ToDate, '') = '' )
            SET @ToDate=Getutcdate()

          SELECT Customer,
                 InvoiceNo,
                 [Date],
                 ISNULL(Amount,0) As Amount,
                 ISNULL(Ceiling(( Amount - Received ) * 100) / 100,0) AS outstanding
          FROM   vw_income
          WHERE  ( ( date >= Case When(ISNULL(@Days,0)=0)THEN @FromDate ELSE Dateadd(day, @Days, @FromDate)END
                      OR @FromDate IS NULL )
                   AND ( date <= @ToDate
                          OR @ToDate IS NULL ) )
                 AND ( CustomerID IN ( @VendorID )
                        OR @VendorID IS NULL )
                 AND ( CreatedBy IN( @CreatedBy )
                        OR @CreatedBy IS NULL )
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 








