﻿
/****************************************************************************   
CREATED BY		: K.SASIREKHA
CREATED DATE	: 29-09-2017 
MODIFIED BY		: DHANALAKSHMI.S  
MODIFIED DATE	: 26-02-2018    
 <summary>
 [SearchTDSLimitConfiguration] 3,12, 4
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchTDSLimitConfiguration] (@FYID     INT,
                                                     @RegionID INT,
                                                     @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT CON.ID          AS ID,
                 CON.SectionID   AS SectionID,
                 tdssec.Code     AS SectionName,
                 CON.FYID        AS FYID,
                 fin.DisplayName AS FinancialYear,
                 CON.Limit       AS Limit,
				 CON.RegionID    AS RegionID,
				 BUS.Name        AS Region
          FROM   tbl_TDSLimitConfiguration CON
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = CON.ModifiedBy
                 LEFT JOIN TDSSection tdssec
                        ON tdssec.ID = CON.SectionID
                 LEFT JOIN tbl_FinancialYear fin
                        ON fin.ID = CON.FYID
				 LEFT JOIN tbl_BusinessUnit BUS
                        ON BUS.ID = CON.RegionID
          WHERE  CON.IsDeleted = 0
                 AND CON.DomainID = @DomainID
                 AND CON.FYID = @FYID
				 AND CON.RegionID = @RegionID

        ORDER BY CON.ModifiedOn DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
