﻿    
  /****************************************************************************         
CREATED BY   :         
CREATED DATE  :           
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>         
 ManageBudgeting 5,1,'','',1,1,4,5,6      
 </summary>                                 
 *****************************************************************************/        
CREATE PROCEDURE [dbo].ManageBudgeting (@ID        INT,        
                                   @BudgetID      Int,        
                                   @Type Varchar(50),  
                                   @BudgetType Varchar(50),        
           @Name Varchar(100),        
           @Amount Money,        
           @HasDeleted Bit,        
           @FinancialYearID int ,       
                                   @SessionID INT,        
                                   @DomainID  INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      DECLARE @Output VARCHAR(1000)        
   IF Exists (Select 1 from tblBudget where IsDeleted=0 and DomainId=@DomainID and FyId=@FinancialYearID And freeze=1)        
   BEGIN        
    set @Output= 'FinancialYear Already Freezed'        
            
   END        
        
      BEGIN TRY        
          BEGIN        
    IF(@HasDeleted=1)      
    Begin      
      Update tblBudget Set         
    IsDeleted=1,ModifiedBy=@SessionID,ModifiedOn=GETDATE()        
     Where Id=@ID        
      set @Output ='Deleted Successfully'        
   End      
     Else   IF(ISNULL(@ID,0)=0)        
          BEGIN        
    IF Exists( select 1 from tblBudget where  IsDeleted=0 and DomainId=@DomainID and FyId=@FinancialYearID and BudgetId =@BudgetID and Type=@Type AND BudgetType=@BudgetType)        
    BEGIN         
    set @Output= 'Already Exists'        
    END        
    ELSE         
    BEGIN        
     Insert Into tblBudget (BudgetId,Type,BudgetType,TagName,Amount,FyId,CreatedBy,CreatedOn,DomainId,ModifiedBy,ModifiedOn)        
     Select @BudgetID,@Type,@BudgetType,@Name,@Amount,@FinancialYearID,@SessionID,GETDATE(),@DomainID ,@SessionID,GETDATE()       
    set @Output= 'Inserted Successfully'        
    END        
    END        
        
    ELSE        
     IF Exists( select 1 from tblBudget where  IsDeleted=0 and DomainId=@DomainID and FyId=@FinancialYearID and BudgetId =@BudgetID and Type=@Type and BudgetType=@BudgetType and Id<>@ID)        
    BEGIN         
    set @Output= 'Already Exists'        
    END        
    ELSE         
    BeGin        
     Update tblBudget Set         
      BudgetId = @BudgetID,        
      Type=@Type,BudgetType=@BudgetType,TagName=@Name,Amount=@Amount,FyId=@FinancialYearID,ModifiedBy=@SessionID,ModifiedOn=GETDATE()        
      Where Id=@ID            
    set @Output='Updated Successfully'        
          END        
          END        
        
          SELECT @Output        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
