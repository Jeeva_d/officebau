﻿/****************************************************************************     
CREATED BY   :     
CREATED DATE  :    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Getjournal] (@ID       INT,  
                                     @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT j.ID            AS ID,  
                 j.JournalNo     AS JournalNo,  
                 j.JournalDate   AS JournalDate, 
				 j.Description , 
                 j.ModifiedOn    AS ModifiedOn,  
                 EMP.FullName    AS ModifiedBy,  
                 j.AutoJournalNo AS AutoJournalNo  
          FROM   tblJournal j  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = j.ModifiedBy  
          WHERE  j.IsDeleted = 0  
                 AND ( j.ID = @ID  
                        OR @ID IS NULL )  
                 AND j.DomainID = @DomainID  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
