﻿/****************************************************************************                       
CREATED BY  :               
CREATED DATE :             
MODIFIED BY  :                       
MODIFIED DATE :                       
 <summary>                    
[SearchDeploymentActivitiesList]                   
 </summary>                                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchDeploymentActivitiesList]
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT ID,
                 BrodcastMessage AS BroadcastMessage,
                 IsActive,
                 FromDate,
                 ToDate,
                 (SELECT NAME + ', '
                  FROM   tbl_Company
                  WHERE  ID IN (SELECT item
                                FROM   dbo.Splitstring(DomainID, ','))
                  FOR xml Path('')) AS CompanyName
          FROM   DeploymentActivities
          WHERE  IsDeleted = 0
          ORDER  BY ModifiedOn DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
