﻿/****************************************************************************                                     
CREATED BY  :                                     
CREATED DATE :                                     
MODIFIED BY  :                              
MODIFIED DATE :                  
 <summary>                                            
 </summary>                                                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CreatePO] (@Description   VARCHAR(1000),
                                  @Date          DATETIME,
                                  @VendorID      INT,
                                  @TotalAmount   MONEY,
                                  @BillNo        VARCHAR(50),
                                  @UploadFile    VARCHAR(8000),
                                  @SessionID     INT,
                                  @DomainID      INT,
                                  @ExpenseDetail EXPENSEDETAIL readonly,
                                  @FileID        UNIQUEIDENTIFIER,
                                  @Reference     VARCHAR(1000),
                                  @CostCenterID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SET @FileID = Isnull(@FileID, Newid())

          DECLARE @OUTPUT               VARCHAR(100),
                  @ExpenseID            INT,
                  @cash                 MONEY,
                  @AutogenPaddingLength INT,
                  @AutogeneratePrefix   VARCHAR(100),
                  @AutogenNo            VARCHAR(100)
          DECLARE @ExpenseVendorID INT

          IF( (SELECT Count(1)
               FROM   tblVendor
               WHERE  NAME = 'Others'
                      AND DomainID = @DomainID
                      AND IsDeleted = 0) = 0 )
            BEGIN
                INSERT INTO tblVendor
                            (NAME,
                             PaymentTerms,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             DomainID)
                VALUES      ('Others',
                             0,
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             @DomainID)

                IF( ISNULL(@VendorID, '') = '' )
                  SET @VendorID = @@IDENTITY
            END

          SET @AutogeneratePrefix='BIL'
          SET @AutogenPaddingLength='1'
          SET @AutogenNo = @AutogeneratePrefix +
                           + RIGHT('0000000000000001', @AutogenPaddingLength)

          INSERT INTO tblPurchaseOrder
                      (VendorID,
                       Date,
                       PONo,
                       Remarks,
                       HistoryID,
                       StatusID,
                       Reference,
                       DomainID,
                       CreatedBy,
                       CreatedOn,
                       ModifiedBy,
                       ModifiedOn,
					   CostCenterID)
          VALUES      ( CASE
                          WHEN ( ISNULL(@VendorID, '') = '' ) THEN
                            (SELECT ID
                             FROM   tblVendor
                             WHERE  NAME = 'Others'
                                    AND DomainID = @DomainID
                                    AND IsDeleted = 0)
                          ELSE
                            ( @VendorID )
                        END,
                        @Date,
                        @BillNo,
                        @Description,
                        @FileID,
                        (SELECT ID
                         FROM   tbl_CodeMaster
                         WHERE  [Type] = 'Open'),
                        @Reference,
                        @DomainID,
                        @SessionID,
                        Getdate(),
                        @SessionID,
                        Getdate(),
						@CostCenterID)

          SET @ExpenseID =@@IDENTITY

          UPDATE tbl_FileUpload
          SET    ReferenceTable = 'tblPurchaseOrder'
          WHERE  ID = @FileID

          IF( (SELECT Count(1)
               FROM   @ExpenseDetail) > 0 )
            BEGIN
                INSERT INTO tblPODetails
                            (POID,
                             LedgerID,
                             Remarks,
                             Qty,
                             BilledQTY,
                             Amount,
                             SGSTPercent,
                             CGSTPercent,
                             SGST,
                             CGST,
                             IsLedger,
                             DomainID,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn)
                SELECT @ExpenseID,
                       LedgerID,
                       ExpenseDetailRemarks,
                       Qty,
                       0,
                       Amount,
                       SGST,
                       CGST,
                       SGSTAmount,
                       CGSTAmount,
                       Isproduct,
                       @DomainID,
                       @SessionID,
                       Getdate(),
                       @SessionID,
                       Getdate()
                FROM   @ExpenseDetail
                WHERE  IsDeleted = 0
                       AND LedgerID <> 0
            END

          EXEC updatePOQTY

          SET @Output = (SELECT [Message]
                         FROM   tblErrorMessage
                         WHERE  [Type] = 'Information'
                                AND Code = 'RCD_INS'
                                AND IsDeleted = 0) --'Bill Created Successfully'                            
          ----Add item to Inventory
          EXEC ManageInventory
            @ExpenseID,
            'INSERT',
            'PO',
            @SessionID,
            @DomainID

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
