﻿/****************************************************************************   
CREATED BY	:	Ajith. N
CREATED DATE  :   14 Jul 2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          [GenerateEmployeeCode]	4, 1 	  
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GenerateEmployeeCode] (@BusinessUnitID INT,
                                              @DomainID       INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @OUTPUT VARCHAR(100)
		  DECLARE @RegionID INT = (Select ParentID from tbl_BusinessUnit Where ID = @BusinessUnitID)

          CREATE TABLE #temp
            (
               ID               INT IDENTITY(1, 1),
               EmployeeID       INT,
               EmployeeCode     VARCHAR(10),
               BaseLocationCode VARCHAR(10),
               Code             INT
            )

          INSERT INTO #temp
          SELECT EMP.ID   AS EmployeeID,
                 EMP.Code AS EmployeeCode,
                 --UPPER(SUBSTRING(BU.NAME, 1, 3)),
                 --SUBSTRING(EMP.Code, 4, 5)
                 PBU.NAME AS BaseLocationCode,
                 EMP.Code AS Code
          FROM   tbl_EmployeeMaster EMP
                 LEFT JOIN tbl_BusinessUnit BU
                        ON EMP.RegionID = BU.ID
                 LEFT JOIN tbl_BusinessUnit PBU
                        ON PBU.ID = BU.ParentID
          WHERE  EMP.RegionID = @RegionID
                AND EMP.Code NOT LIKE '%TMP_%'
				AND EMP.Code NOT LIKE 'SUPERADMIN'

          SET @OUTPUT = (SELECT TOP 1 CAST(( Code + 1 ) AS VARCHAR(10))
                         FROM   #temp
                         ORDER  BY Code DESC)

          IF Isnull(@OUTPUT, '') = ''
            SET @OUTPUT = (SELECT CASE UPPER(SUBSTRING(PBU.NAME, 1, 3))
                                  WHEN 'CHE' THEN '1001'
                                  ELSE '1'
                                END                                
                         FROM   tbl_BusinessUnit BU
                                LEFT JOIN tbl_BusinessUnit PBU
                                       ON PBU.ID = BU.ParentID
                         WHERE  BU.ID = @BusinessUnitID
                                AND BU.IsDeleted = 0)

          SELECT @OUTPUT AS UserMessage
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
