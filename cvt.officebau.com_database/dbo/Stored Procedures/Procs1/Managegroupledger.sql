﻿/****************************************************************************   
CREATED BY   : Dhanalakshmi  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>   
 [ManageGroupLedger]  5,'test','et',1,0,1,1,1  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Managegroupledger] (@ID         INT,  
                                           @Name       VARCHAR(100),  
                                           @Remarks    VARCHAR(1000),  
                                           @ReportType INT,  
                                           @HasDeleted BIT,  
                                           @SessionID  INT,  
                                           @DomainID   INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(1000)  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          IF( @HasDeleted = 1 )  
            BEGIN  
                IF EXISTS (SELECT 1  
                           FROM   tblLedger  
                           WHERE  GroupID = @ID  
                                  AND IsDeleted = 0  
                                  AND DomainID = @DomainID)  
                  BEGIN  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Warning'  
                                            AND Code = 'RCD_REF'  
                                            AND IsDeleted = 0) --'The record is referred'  
                      GOTO finish  
                  END  
                ELSE  
                  BEGIN  
                      UPDATE tblGroupLedger  
                      SET    IsDeleted = 1  
                      WHERE  ID = @ID  
                             AND DomainID = @DomainID  
  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Information'  
                                            AND Code = 'RCD_DEL'  
                                            AND IsDeleted = 0) --'Deleted Successfully'  
                  END  
            END  
          ELSE  
            BEGIN  
                IF( @ID = 0 )  
                  BEGIN  
                      IF( (SELECT Count(1)  
                           FROM   tblGroupLedger  
                           WHERE  NAME = @Name  
                                  AND DomainID = @DomainID  
                                  AND IsDeleted = 0) = 0 )  
                        BEGIN  
                            INSERT INTO tblGroupLedger  
                                        (NAME,  
                                         Remarks,  
                                         ReportType,  
                                         CreatedOn,  
                                         CreatedBy,  
                                         ModifiedBy,  
                                         ModifiedOn,  
                                         HistoryID,  
                                         DomainID,  
           IsDeleted)  
                            VALUES      (@Name,  
                                         @Remarks,  
                                         @ReportType,  
                                         Getdate(),  
                                         @SessionID,  
                                         @SessionID,  
                                         Getdate(),  
                                         Newid(),  
                                         @DomainID,  
           0)  
  
                            DECLARE @RefID INT=@@IDENTITY  
  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Information'  
                                                  AND Code = 'RCD_INS'  
                AND IsDeleted = 0)  
                                          + '/' + Cast(@RefID AS VARCHAR(100)) --'Inserted Successfully/'  
                        END  
                      ELSE  
                        SET @Output = (SELECT [Message]  
                                       FROM   tblErrorMessage  
                                       WHERE  [Type] = 'Warning'  
                                              AND Code = 'RCD_EXIST'  
                                              AND IsDeleted = 0) --'Already Exists'  
                  END  
                ELSE  
                  BEGIN  
                      IF ( (SELECT Count(1)  
                            FROM   tblGroupLedger  
                            WHERE  NAME = @Name  
                                   AND DomainID = @DomainID  
                                   AND ID <> @ID  
                                   AND IsDeleted = 0) = 0 )  
                        BEGIN  
                            UPDATE tblGroupledger  
                            SET    NAME = @Name,  
                                   Remarks = @Remarks,  
                                   ReportType = @ReportType,  
                                   ModifiedBy = @SessionID,  
                                   ModifiedOn = Getdate()  
                            WHERE  ID = @ID  
                                   AND DomainID = @DomainID  
  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Information'  
                                                  AND Code = 'RCD_UPD'  
                                                  AND IsDeleted = 0) --'Updated Successfully'  
                        END  
                      ELSE  
                        BEGIN  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Warning'  
                                                  AND Code = 'RCD_EXIST'  
                                                  AND IsDeleted = 0) --'Already Exits'  
                        END  
                  END  
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
