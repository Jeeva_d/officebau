﻿/****************************************************************************   
CREATED BY   : Dhanalakshmi  
CREATED DATE  : 14/11/2016  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
[Duplicatecheck] 'Reliance','Cusatomer',1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DuplicateNameCheck2] (@Name     VARCHAR(100),
                                            @Table    VARCHAR(100),
                                            @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(100),
              @SQL    NVARCHAR(max)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @SQL = N'(select  @COUNTS=COUNT(1) from tbl'
                     + @Table + ' Where Name=''' + @Name
                     + ''' and DomainID='
                     + CONVERT(VARCHAR(10), @DomainID)
                     + ' and IsDeleted=0)'

          EXEC Sp_executesql
            @SQL,
            N'@COUNTS INT OUTPUT',
            @Output OUTPUT;

          SELECT @output AS Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
