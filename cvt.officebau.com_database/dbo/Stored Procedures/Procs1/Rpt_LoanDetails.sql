﻿/****************************************************************************     
CREATED BY   :   
CREATED DATE  : 
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_LoanDetails](@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          --SELECT l.Id,
          --       l.NAME,
          --       CASE
          --         WHEN ( (SELECT NAME
          --                 FROM   tblMasterTypes
          --                 WHERE  id = TransactionType) = 'Receive Payment' ) THEN
          --           ( -1 * Amount )
          --         ELSE
          --           Amount
          --       END Amount
          --INTO   #Result
          --FROM   tblReceipts r
          --       JOIN tblLedger l
          --         ON r.LedgerID = l.id
          --            AND NAME LIKE '%Loan%'
          --WHERE  r.IsDeleted = 0
          --       AND r.DomainID = @DomainID
          CREATE TABLE #Result
            (
               Id     INT,
               NAME   VARCHAR(100),
               Amount MONEY
            )

          INSERT INTO #Result
          SELECT l.Id,
                 l.NAME,
                 ( -1 * TotalAmount ) AS Amount
          FROM   tbl_Receipts r
                 JOIN tblLedger l
                   ON r.LedgerID = l.id
                      AND NAME LIKE '%Loan%'
          WHERE  r.IsDeleted = 0
                 AND r.DomainID = @DomainID
          UNION ALL
          SELECT l.Id,
                 l.NAME,
                 TotalAmount AS Amount
          FROM   tbl_payment r
                 JOIN tblLedger l
                   ON r.LedgerID = l.id
                      AND NAME LIKE '%Loan%'
          WHERE  r.IsDeleted = 0
                 AND r.DomainID = @DomainID

          SELECT Id,
                 NAME,
                 Sum(amount) Amount
          FROM   #result
          GROUP  BY NAME,
                    id

          DROP TABLE #result

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
