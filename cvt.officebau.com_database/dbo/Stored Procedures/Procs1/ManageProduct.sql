﻿/****************************************************************************                     
CREATED BY   : Jeeva                    
CREATED DATE  :                     
MODIFIED BY   : SIVA.B                    
MODIFIED DATE  : 28-11-2016                      
 <summary>                     
 [Manageproduct]  1, 'hjh','',1004,null,0,'',1010,0,1,1,2                   
 select * from  tblProduct                         
 </summary>                                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ManageProduct] (@ID                INT,    
                                       @Name              VARCHAR(100),    
                                       @Manufacturer      VARCHAR(100),    
                                       @TransactionTypeID INT,    
                                       @Rate              MONEY,    
                                       @OpeningStock      DECIMAL(18, 2),    
                                       @Remarks           VARCHAR(1000),    
                                       @TypeID            INT,    
                                       @HasDeleted        BIT,    
                                       @SessionID         INT,    
                                       @DomainID          INT,    
                                       @HsnID             INT,    
                                       @LedgerID          INT,    
                                       @UOMID             INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(1000)    
    
      BEGIN TRY    
          IF( @HasDeleted = 1 )    
            BEGIN    
                IF EXISTS (SELECT 1    
                           FROM   tblExpenseDetails    
                           WHERE  LedgerID = @ID    
                                  AND IsLedger = 0    
                                  AND IsDeleted = 0    
                                  AND DomainID = @DomainID)    
                  BEGIN    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Warning'    
                                            AND Code = 'RCD_REF'    
                                            AND IsDeleted = 0) --'The record is referred.'                  
    
                      GOTO Finish    
                  END    
    
                IF EXISTS (SELECT 1    
                           FROM   tblBudget    
                           WHERE  BudgetId = @ID    
                                  AND [Type] = 'Product'    
                                  AND IsDeleted = 0    
                                  AND DomainID = @DomainID)    
                  BEGIN    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Warning'    
                                            AND Code = 'RCD_REF'    
                                            AND IsDeleted = 0) --'The record is referred.'                  
    
                      GOTO Finish    
                  END    
    
                IF EXISTS (SELECT 1    
                           FROM   tblInvoiceItem    
                           WHERE  ProductID = @ID    
                                  AND IsDeleted = 0    
                                  AND DomainID = @DomainID)    
                  BEGIN    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Warning'    
                                            AND Code = 'RCD_REF'    
                                            AND IsDeleted = 0) --'The record is referred.'                  
    
                      GOTO Finish    
                  END    
                ELSE    
   BEGIN    
                      UPDATE tblProduct    
                      SET    IsDeleted = 1    
                      WHERE  ID = @ID    
    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Information'    
                                            AND Code = 'RCD_DEL'    
                                            AND IsDeleted = 0) --'Deleted Successfully'                  
    
                      GOTO Finish    
                  END    
            END    
          ELSE    
            BEGIN    
                IF( @ID = 0 )    
                  BEGIN    
                      IF( (SELECT Count(1)    
                           FROM   tblProduct    
                           WHERE  NAME = @Name    
                                  AND DomainID = @DomainID    
                                  AND IsDeleted = 0) = 0 )    
                        BEGIN    
                            INSERT INTO tblProduct    
                                        (NAME,    
                                         Code,    
                                         Rate,    
                                         TransactionTypeID,    
                                         TypeID,    
                                         Manufacturer,    
                                         ProductDescription,    
                                         OpeningStock,    
                                         GstHSNID,    
                                         LedgerID,    
                                         CreatedBy,    
                                         ModifiedBy,    
                                         DomainID,    
                                         UOMID)    
                            VALUES      (@Name,    
                                         @Name,    
                                         @Rate,    
                                         @TransactionTypeID,    
                                         @TypeID,    
                                         @Manufacturer,    
                                         @Remarks,    
                                         @OpeningStock,    
                                         @HsnID,    
                                         @LedgerID,    
                                         @SessionID,    
                                         @SessionID,    
                                         @DomainID,    
                                         @UOMID)    
    
                            DECLARE @RefID INT=@@IDENTITY    
    
                            SET @Output = (SELECT [Message]    
                                           FROM   tblErrorMessage    
                                           WHERE  [Type] = 'Information'    
                                                  AND Code = 'RCD_INS'    
                                                  AND IsDeleted = 0)    
                                          + '/' + Cast(@RefID AS VARCHAR(100)) --'Inserted Successfully/'                  
                        END    
                      ELSE    
                        SET @Output = (SELECT [Message]    
                                       FROM   tblErrorMessage    
                                       WHERE  [Type] = 'Warning'    
                                              AND Code = 'RCD_EXIST'    
                                              AND IsDeleted = 0) --'Already Exists'             
                  END    
                ELSE    
                  BEGIN    
                      IF( ( (SELECT TypeID    
                             FROM  tblProduct    
                             WHERE  id = @ID) <> @TypeID )    
                          AND ( (SELECT Count(1)    
                                 FROM   tblProduct P    
                                        LEFT JOIN tblInvoiceItem II    
            ON ii.ProductID = p.ID    
                                                  AND ii.IsDeleted = 0    
                                        LEFT JOIN tblExpenseDetails ed    
                                               ON ed.LedgerID = p.ID    
                                                  AND ed.IsLedger = 0    
                                                  AND ed.IsDeleted = 0    
                                 WHERE  ii.ProductID = @ID    
                                         OR ed.LedgerID = @ID) > 0 ) )    
                        BEGIN    
                            IF (SELECT Count(1)    
                                FROM   tblInvoiceItem    
                                WHERE  ProductID = @id    
                                       AND DomainID = @DomainID    
                                       AND IsDeleted = 0) <> 0    
                              BEGIN    
                                  SET @Output = 'The Service Type is referred in Invoice.'    
    
                                  GOTO Finish    
                              END    
                            ELSE    
                              BEGIN    
                                  SET @Output = 'The Product Type is referred in Expense.'    
    
                                  GOTO Finish    
                              END    
                        END    
                      ELSE    
                        BEGIN    
                            IF( (SELECT Count(1)    
                                 FROM   tblProduct    
                                 WHERE  NAME = @Name    
                                        AND ID <> @ID    
                                        AND DomainID = @DomainID    
                                        AND IsDeleted = 0) = 0 )    
                              BEGIN    
                                  --IF(SELECT Code        
                                  --   FROM   tbl_CodeMaster        
                                  --   WHERE  ID = @TransactionTypeID) <> 'Both'        
                                  BEGIN    
                                      IF ( @TransactionTypeID ) <> (SELECT TransactionTypeID    
                                                                    FROM   tblProduct    
                                                                    WHERE  id = @ID    
                                                                           AND DomainID = @DomainID)    
                                        ----------------------------            
                                        BEGIN    
                                            IF (SELECT Code    
                                                FROM   tbl_CodeMaster    
                                                WHERE  ID = @TransactionTypeID) = 'Purchase'    
                                              BEGIN    
                                                  IF (SELECT Count(1)    
                                                      FROM   tblInvoiceItem    
                                                      WHERE  ProductID = @id    
                                                             AND DomainID = @DomainID    
                                                             AND IsDeleted = 0) <> 0    
                                                    BEGIN    
                                                        SET @Output = (SELECT [Message]    
                                                                       FROM   tblErrorMessage    
 WHERE  [Type] = 'Warning'    
                                                    AND Code = 'RCD_REF_INV'    
                                                                              AND IsDeleted = 0) --'The record is referred in Invoice.'            
    
                                                        GOTO Finish    
    END    
                                                  ELSE    
                                                    BEGIN    
                                                        UPDATE tblproduct    
                                                        SET    NAME = @Name,    
                                                               Manufacturer = @Manufacturer,    
                                                               TransactionTypeID = @TransactionTypeID,    
                                                               TypeID = @TypeID,    
                                                               Rate = @Rate,    
                                                               OpeningStock = @OpeningStock,    
                                                               GstHSNID = @HsnID,    
                                                               ProductDescription = @Remarks,    
                                                               ModifiedBy = @SessionID,    
                                                               ModifiedOn = Getdate(),    
                                                               LedgerID = @LedgerID,    
                                                               UOMID = @UOMID    
                                                        WHERE  ID = @ID    
                                                               AND DomainID = @DomainID    
    
                                                        SET @Output = (SELECT [Message]    
                                                                       FROM   tblErrorMessage    
                                                                       WHERE  [Type] = 'Information'    
                                                                              AND Code = 'RCD_UPD'    
                                                                              AND IsDeleted = 0) --'Updated Successfully'                  
                                                    END    
                                              END    
    
                                            IF (SELECT Code    
                                                FROM   tbl_CodeMaster    
                                                WHERE  ID = @TransactionTypeID) = 'Sales'    
                                              BEGIN    
                                                  IF (SELECT Count(1)    
                                                      FROM   tblExpenseDetails    
                                                      WHERE  LedgerID = @id    
                                                             AND IsLedger = 0    
                                                             AND DomainID = @DomainID    
                                                             AND IsDeleted = 0) <> 0    
                                                    BEGIN    
                                                        SET @Output = (SELECT [Message]    
                                                                       FROM   tblErrorMessage    
                                                                       WHERE  [Type] = 'Warning'    
                                                                              AND Code = 'RCD_REF_EXP'    
                                                                              AND IsDeleted = 0) --'The record is referred in Expense.'                  
    
                                                        GOTO Finish    
                                           END    
                                                  ELSE    
                                                    BEGIN    
                                       UPDATE tblproduct    
                                                        SET    NAME = @Name,    
                                                               Manufacturer = @Manufacturer,    
                                    TransactionTypeID = @TransactionTypeID,    
                                                               TypeID = @TypeID,    
                                                               Rate = @Rate,    
                                                               OpeningStock = @OpeningStock,    
                                                               GstHSNID = @HsnID,    
                                                               ProductDescription = @Remarks,    
                                                               ModifiedBy = @SessionID,    
                                                               ModifiedOn = Getdate(),    
                                                               LedgerID = @LedgerID,    
                                                               UOMID = @UOMID    
                                                        WHERE  ID = @ID    
                                                               AND DomainID = @DomainID    
    
                                                        SET @Output = (SELECT [Message]    
                                                                       FROM   tblErrorMessage    
                                                                       WHERE  [Type] = 'Information'    
                                                                              AND Code = 'RCD_UPD'    
                                                                              AND IsDeleted = 0) --'Updated Successfully'                  
                                                    END    
                                              END    
                                        END    
                                      -------------------------------------            
                                      ELSE    
                                        BEGIN    
                                            UPDATE tblproduct    
                                            SET    NAME = @Name,    
                                                   Manufacturer = @Manufacturer,    
                                                   TransactionTypeID = @TransactionTypeID,    
                                                   TypeID = @TypeID,    
                                                   Rate = @Rate,    
                                                   GstHSNID = @HsnID,    
                                                   OpeningStock = @OpeningStock,    
                                                   ProductDescription = @Remarks,    
                                                   ModifiedBy = @SessionID,    
                                                   ModifiedOn = Getdate(),    
                                                   LedgerID = @LedgerID,    
                                                   UOMID = @UOMID    
                                            WHERE  ID = @ID    
                                                   AND DomainID = @DomainID    
    
                                            SET @Output = (SELECT [Message]    
                                                           FROM   tblErrorMessage    
                                                           WHERE  [Type] = 'Information'    
                                                                  AND Code = 'RCD_UPD'    
                                                                  AND IsDeleted = 0) --'Updated Successfully'             
                                        END    
                                  END    
                              --ELSE        
                              --  BEGIN        
                              --      IF (SELECT Code        
                              --          FROM   tbl_CodeMaster        
                    --          WHERE  ID = @TransactionTypeID) = 'Purchase'        
                              --        BEGIN        
  --            IF (SELECT Count(1)        
                              --                FROM   tblInvoiceItem        
                              --                WHERE  ProductID = @id        
                              --                       AND DomainID = @DomainID        
                              --                       AND IsDeleted = 0) <> 0        
                              --              BEGIN        
                              --                  SET @Output = (SELECT [Message]        
                              --                                 FROM   tblErrorMessage        
                              --                                 WHERE  [Type] = 'Warning'        
                              --                                        AND Code = 'RCD_REF_INV'        
                              --                                        AND IsDeleted = 0) --'The record is referred in Invoice.'                  
                              --                  GOTO Finish        
                              --              END        
                              --            ELSE        
                              --              BEGIN        
                              --                  UPDATE tblproduct        
                              --                  SET    NAME = @Name,        
                              --                         Manufacturer = @Manufacturer,        
                              --                         TransactionTypeID = @TransactionTypeID,        
                              --                         TypeID = @TypeID,        
                              --                         Rate = @Rate,        
                              --                         OpeningStock = @OpeningStock,        
                              --                         GstHSNID = @HsnID,        
                              --                         ProductDescription = @Remarks,        
                              --                         ModifiedBy = @SessionID,        
                              --                         ModifiedOn = Getdate(),        
                              --                         LedgerID = @LedgerID        
                              --                  WHERE  ID = @ID        
                              --                         AND DomainID = @DomainID        
                              --                  SET @Output = (SELECT [Message]        
                              --                                 FROM   tblErrorMessage        
                              --                                 WHERE  [Type] = 'Information'        
                              --                                        AND Code = 'RCD_UPD'        
                              --                                        AND IsDeleted = 0) --'Updated Successfully'                  
                              --              END        
                              --        END        
                              --      IF (SELECT Code        
                              --          FROM   tbl_CodeMaster        
                              --          WHERE  ID = @TransactionTypeID) = 'Sales'        
                              --        BEGIN        
                              --            IF (SELECT Count(1)        
                              --           FROM   tblExpenseDetails        
                              --                WHERE  LedgerID = @id        
                              --                       AND IsLedger = 0 ---            
                              --                       AND DomainID = @DomainID        
                              --                       AND IsDeleted = 0) <> 0        
                              --              BEGIN        
                              --                  SET @Output = (SELECT [Message]        
                              --                FROM   tblErrorMessage        
                              --                                 WHERE  [Type] = 'Warning'        
                              --              AND Code = 'RCD_REF_EXP'        
                              --                                        AND IsDeleted = 0) --'The record is referred in Expense.'                  
                              --                  GOTO Finish        
                              --              END        
                              --            ELSE        
                              --              BEGIN        
                              --                  UPDATE tblproduct        
                              --                  SET    NAME = @Name,        
                              --                         Manufacturer = @Manufacturer,        
                              --                         TransactionTypeID = @TransactionTypeID,        
                              --                         TypeID = @TypeID,        
                              --                         Rate = @Rate,        
                              --                         GstHSNID = @HsnID,        
                              --                         OpeningStock = @OpeningStock,        
                              --                         ProductDescription = @Remarks,        
                              --                         ModifiedBy = @SessionID,        
                              --                         ModifiedOn = Getdate(),        
                              --                         LedgerID = @LedgerID        
                              --                  WHERE  ID = @ID        
                              --                         AND DomainID = @DomainID        
                              --                  SET @Output = (SELECT [Message]        
                              --                                 FROM   tblErrorMessage        
                              --                                 WHERE  [Type] = 'Information'        
                              --                                        AND Code = 'RCD_UPD'        
                              --                                        AND IsDeleted = 0) --'Updated Successfully'                  
                              --              END        
                              --        END        
                              --      ELSE        
                              --        BEGIN        
                              --            UPDATE tblproduct        
                              --            SET    NAME = @Name,        
                              --                   Manufacturer = @Manufacturer,        
                              --                   TransactionTypeID = @TransactionTypeID,        
                              --                   TypeID = @TypeID,        
                              --                   Rate = @Rate,        
                              --                   OpeningStock = @OpeningStock,        
                              --                   GstHSNID = @HsnID,        
                              --                   ProductDescription = @Remarks,        
                              --                   ModifiedBy = @SessionID,        
                              --                   ModifiedOn = Getdate(),        
                              --      LedgerID = @LedgerID        
                              --            WHERE  ID = @ID        
                              --                   AND DomainID = @DomainID        
                              --            SET @Output = (SELECT [Message]        
                              --                           FROM   tblErrorMessage        
                              --                           WHERE  [Type] = 'Information'        
                              --                                  AND Code = 'RCD_UPD'        
               --                                  AND IsDeleted = 0) --'Updated Successfully'                  
                              --        END        
                              --  END        
                              END    
                            ELSE    
                              BEGIN    
                                  SET @Output = (SELECT [Message]    
                                                 FROM   tblErrorMessage    
                                                 WHERE  [Type] = 'Warning'    
                                                        AND Code = 'RCD_EXIST'    
                                                        AND IsDeleted = 0) --'Already Exists'                  
                              END    
                        END    
                  END    
            END    
    
          FINISH:    
    
          SELECT @Output    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
