﻿/****************************************************************************         
CREATED BY   : Dhanalakshmi.S        
CREATED DATE  : 16 JAN 2019       
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>         
      
 </summary>                                 
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageTDSConfig] (@ID                 INT,  
                                          @Value             VARCHAR(100),  
                                          @PayrollComponentID INT,  
                                          @SessionID          INT,  
                                          @DomainID           INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(100) = 'Operation Failed!'  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
            
            IF EXISTS (SELECT 1        
                     FROM   tbl_TDSComponentMapping        
                     WHERE  TDSKey = @Value        
                            AND DomainID = @DomainID)        
            BEGIN  
             UPDATE tbl_TDSComponentMapping  
                SET    TDSKey = @Value,  
                       ComponentId = @PayrollComponentID,  
                       ModifiedBY = @SessionID,  
                       ModifiedOn = Getdate()  
                WHERE  DomainID = @DomainID  
                       AND TDSKey = @Value  
  
                SET @Output = 'Updated Successfully.'  
                  
                 GOTO FINISH    
            END  
          
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
