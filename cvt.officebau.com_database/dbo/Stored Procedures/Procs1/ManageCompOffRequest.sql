﻿/****************************************************************************     
CREATED BY   : K.SASIREKHA    
CREATED DATE  : 29-09-2017    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
  [ManageCompOffRequest] 2,106,'2018-01-06','abc',71,1,1,0    
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageCompOffRequest] (@ID               INT,  
                                              @EmployeeID       INT,  
                                              @Date             DATE,  
                                              @RequesterRemarks VARCHAR(8000),  
                                              @ApproverID       INT,  
                                              @SessionID        INT,  
                                              @DomainID         INT,  
                                              @IsDeleted        INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output         VARCHAR(8000),  
              @LeaveStatusID  INT=(SELECT ID  
                FROM   tbl_Status  
                WHERE  Code = 'Pending'  
                       AND [Type] = 'Leave'  
                       AND IsDeleted = 0),  
              @RegionID       INT,  
              @BaseLocationID INT,  
              @Duration       TIME,  
              @RegionIDH      VARCHAR(20)  
  
      SELECT @BaseLocationID = BaseLocationID,  
             @RegionID = RegionID  
      FROM   tbl_EmployeeMaster  
      WHERE  ID = @EmployeeID  
  
      --SET @Duration = CONVERT(TIME, (SELECT Replace(Cast(FullDay AS VARCHAR), '.', ':')  
      --                               FROM   tbl_BusinessHours  
      --                               WHERE  RegionID = @RegionID))  
      SET @Duration = CONVERT(TIME, (SELECT Replace(Cast(Cast(AC.ConfigValue AS DECIMAL(8, 2)) AS VARCHAR), '.', ':')  
                                     FROM   tbl_ApplicationConfiguration AC  
                                            JOIN tbl_ConfigurationMaster cm  
                                              ON cm.ID = ac.ConfigurationID  
                                                 AND cm.Code = 'COMPOFF_ELIGIBILITY'  
                                     WHERE  ac.BusinessUnitID = @BaseLocationID))  
      SET @RegionIDH= (SELECT Cast(RegionID AS VARCHAR(20))  
                       FROM   tbl_EmployeeMaster  
                       WHERE  ID = @EmployeeID)  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SET @Output = 'Operation Failed!'  
  
          IF( ISNULL(@Duration, '') <> '' )  
            BEGIN  
                IF( @IsDeleted != 0 )  
                  BEGIN  
                      IF NOT EXISTS (SELECT 1  
                                     FROM   tbl_EmployeeCompOff  
                                     WHERE  ID = @ID  
                                            AND StatusID = (SELECT ID  
                                                            FROM   tbl_Status  
                                                            WHERE  Code = 'Approved'  
                                                                   AND [Type] = 'Leave'  
                                                                   AND IsDeleted = 0))  
                        BEGIN  
                            UPDATE tbl_EmployeeCompOff  
                            SET    IsDeleted = @IsDeleted,  
                                   ModifiedBy = @SessionID,  
                                   ModifiedOn = Getdate()  
                            WHERE  ID = @ID  
  
                            SET @Output = 'Deleted Successfully.'  
                        END  
                      ELSE  
                        SET @Output = 'Cannot Delete Request.'  
                  END  
                ELSE  
                  BEGIN  
                      --IF NOT EXISTS (SELECT 1  
                      --               FROM   tbl_pay_EmployeePayroll  
                      --               WHERE  IsDeleted = 0  
                      --                      AND DomainId = @DomainID  
                      --                      AND EmployeeId = @EmployeeID  
                      --                      AND MonthId = Datepart(MM, @Date)  
                      --                      AND YearId = Datepart(YYYY, @Date))  
                      --  BEGIN  
                      IF NOT EXISTS(SELECT 1  
                                    FROM   tbl_EmployeeCompOff  
                                    WHERE  @ID <> ID  
                                           AND Date = @Date  
                                           AND DomainID = @DomainID  
                                           AND ( StatusID IN (SELECT ID  
                                                              FROM   tbl_Status  
                                                              WHERE  Code IN( 'Approved', 'Pending' )  
                                                                     AND [Type] = 'Leave'  
                                                                     AND IsDeleted = 0) )  
                                           AND IsDeleted = 0  
                                           AND @EmployeeID = CreatedBy)  
                        BEGIN  
                            IF EXISTS(SELECT 1  
                                      FROM   tbl_Holidays  
                                      WHERE  Date = @Date  
                                             AND RegionID LIKE '%,' + @RegionIDH + ',%'  
                                             AND IsDeleted = 0)  
                              BEGIN  
                                  IF EXISTS(SELECT 1  
                                            FROM   tbl_BusinessHours  
                                            WHERE  RegionID = @RegionID  
                                                   AND IsDeleted = 0)  
                                    BEGIN  
                                        IF (SELECT Duration  
                                            FROM   tbl_Attendance  
                                            WHERE  LogDate = @Date  
                                                   AND EmployeeID = @EmployeeID) >= @Duration  
                                          BEGIN  
                                              IF( Isnull(@ID, 0) = 0 )  
                                                BEGIN  
                                                    INSERT INTO tbl_EmployeeCompOff  
                                                                (Date,  
                                                                 RequesterRemarks,  
                                                                 ApproverID,  
                                                                 StatusID,  
                                                                 CreatedBy,  
                                                                 CreatedOn,  
                                                                 ModifiedBy,  
                                                                 ModifiedOn,  
                                                                 DomainID,  
                                                                 ApproverStatusID)  
                                                    VALUES      ( @Date,  
                                                                  @RequesterRemarks,  
                                                                  @ApproverID,  
                                                                  @LeaveStatusID,  
                                                                  @EmployeeID,  
                                                                  Getdate(),  
                                                                  @SessionID,  
                                                                  Getdate(),  
                                                                  @DomainID,  
                                             @LeaveStatusID)  
  
                                                    SET @ID = IDENT_CURRENT('tbl_EmployeeCompOff')  
                                                    SET @Output = 'Inserted Successfully./'  
                                                                  + Cast(@ID AS VARCHAR)  
  
                                                    GOTO Finish  
                                                END  
                                              ELSE  
                                                BEGIN  
                                                    UPDATE tbl_EmployeeCompOff  
                                                    SET    Date = @Date,  
                                                           RequesterRemarks = @RequesterRemarks,  
                                                           StatusID = @LeaveStatusID,  
                                                           DomainID = @DomainID,  
                                                           ModifiedBy = @SessionID,  
                                                           ApproverStatusID = @LeaveStatusID  
                                                    WHERE  ID = @ID  
  
                                                    SET @Output = 'Updated Successfully./'  
                                                                  + Cast(@ID AS VARCHAR)  
                                                END  
                                          END  
                                        ELSE  
                                          BEGIN  
                                              SET @Output = 'Duration must meet the '  
                                                            + CONVERT(VARCHAR(5), Cast(@Duration AS VARCHAR), 106)  
                                                            + ' hours eligibility criteria.'  
                                          END  
                                    END  
                                  ELSE  
                                    BEGIN  
                                        SET @Output = 'Business Hours have not been configured for your Region .'  
                                    END  
                              END  
                            ELSE  
                              BEGIN  
                                  SET @Output = 'Request can only be created for Holidays and Weekends.'  
                              END  
                        END  
                      ELSE  
                        BEGIN  
                            SET @Output = 'Already exists.'  
                        END  
                  --END  
                  --ELSE  
                  --BEGIN  
                  --    SET @Output = 'Payroll has already processed for this selected month and year.'  
                  --END  
                  END  
            END  
          ELSE  
            BEGIN  
                SET @Output = 'Configure Comp-off eligibility hours.'  
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
