﻿/****************************************************************************   
CREATED BY   : K.SASIREKHA  
CREATED DATE  : 29-09-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
  [ManageLeaveDays] 2,4,2,'8.0',1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageLeaveDays] (@EmployeeID  INT,
                                         @Year        INT,
                                         @LeaveTypeID INT,
                                         @LopDays     DECIMAL(16,2),
                                         @SessionID   INT,
                                         @DomainID    INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output   VARCHAR(100),
              @yearname VARCHAR(80)= (SELECT NAME
                 FROM   tbl_FinancialYear
                 WHERE  ID = @Year
                        AND IsDeleted = 0)

      BEGIN TRY
          BEGIN TRANSACTION

          BEGIN
              IF NOT EXISTS (SELECT 1
                             FROM   tbl_EmployeeAvailedLeave
                             WHERE  EmployeeID = @EmployeeID
                                    AND LeaveTypeID = @LeaveTypeID
                                    AND Year = @yearname)
                BEGIN
                    INSERT INTO tbl_EmployeeAvailedLeave
                                (EmployeeID,
                                 LeaveTypeID,
                                 Year,
                                 TotalLeave,
                                 AvailedLeave,
                                 CreatedBy,
                                 CreatedOn,
                                 ModifiedBy,
                                 ModifiedOn,
                                 DomainID)
                    VALUES      ( @EmployeeID,
                                  @LeaveTypeID,
                                  @yearname,
                                  ISNULL(@LopDays,0),
                                  '0.0',
                                  @SessionID,
                                  Getdate(),
                                  @SessionID,
                                  Getdate(),
                                  @DomainID)

                    SET @Output = 'Inserted Successfully.'

                    GOTO Finish
                END
              ELSE
                BEGIN
                    UPDATE tbl_EmployeeAvailedLeave
                    SET    TotalLeave = ISNULL(@LopDays,0),
                           DomainID = @DomainID,
                           ModifiedBy = @SessionID
                    WHERE  EmployeeID = @EmployeeID
                           AND LeaveTypeID = @LeaveTypeID
                           AND Year = @yearname

                    SET @Output = 'Updated Successfully.'

                    GOTO Finish
                END
          END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
