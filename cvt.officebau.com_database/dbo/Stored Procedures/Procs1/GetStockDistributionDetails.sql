﻿/****************************************************************************                 
CREATED BY   :                 
CREATED DATE  :                 
MODIFIED BY   :                 
MODIFIED DATE  :                 
 <summary>        
 [GetStockDistributionDetails] 2054,1      
 </summary>                                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetStockDistributionDetails] (@ID       INT,  
                                                     @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT isd.ID                                    AS ID,  
                 isd.CostCenterID                          AS CostCenterID,  
                 DistributedDate                           AS DistributedDate,  
                 isd.ProductID                             AS ProductID,  
                 pt.NAME                                   AS ProductName,  
				 isd.IsInternal                            as IsInternal,
				isd.EmployeeID                            AS EmployeeID,
            Isnull(em.Code, '') + ' - ' + em.FullName   AS EmployeeName,  
		        isd.Party                              AS Party,  
                 isd.Qty                                   AS Qty,  
                 isd.Remarks                               AS [Description]

          FROM   tbl_InventoryStockDistribution isd  
                 LEFT JOIN tbl_EmployeeMaster em  
                        ON em.ID = isd.EmployeeID  
                 LEFT JOIN tblProduct_V2 pt  
                        ON pt.ID = isd.ProductID  
          WHERE  isd.ID = @ID  
                 AND isd.DomainID = @DomainID  
                 AND isd.IsDeleted = 0  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
