﻿/****************************************************************************       
CREATED BY   : Naneeshwar.M      
CREATED DATE  : 08-Aug-2017      
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>              
          
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageCompanyMaster] (@ID                   INT,
                                             @Name                 VARCHAR(200),
                                             @ContactNo            VARCHAR(50),
                                             @Address              VARCHAR(250),
                                             @EmailID              VARCHAR(200),
                                             @PANNO                VARCHAR(100),
                                             @TANNO                VARCHAR(100),
                                             @GSTNO                VARCHAR(100),
                                             @Website              VARCHAR(100),
                                             @Logo                 VARCHAR(8000),
                                             @FullName             VARCHAR(500),
                                             @pAYMENTfAVOUR        VARCHAR(500),
                                             @SessionID            INT,
                                             @IsDeleted            INT,
                                             @CommunicationEmailId VARCHAR(200),
                                             @EmpCodePattern       VARCHAR(10))
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(1000)

      BEGIN TRY
          IF( @IsDeleted = 1 )
            BEGIN
                IF( (SELECT Count(1)
                     FROM   tbl_EmployeeCompanyMapping
                     WHERE  CompanyIDs LIKE '%' + Cast( @ID AS VARCHAR) + ',%'
                            AND IsDeleted = 0) = 1 )
                  BEGIN
                      SET @Output = 'The record is referred.'

                      GOTO Finish
                  END

                IF EXISTS(SELECT 1
                          FROM   tbl_EmployeeMaster
                          WHERE  DomainID = @ID
                                 AND IsDeleted = 0)
                  BEGIN
                      SET @Output = 'The record is referred.'

                      GOTO Finish
                  END
                ELSE
                  BEGIN
                      UPDATE tbl_Company
                      SET    IsDeleted = 1,
                             Modifiedon = Getdate(),
                             ModifiedBy = @sessionID
                      WHERE  ID = @ID

                      SET @Output = 'Deleted Successfully'

                      GOTO Finish
                  END
            END
          ELSE
            BEGIN
                IF Isnull(@ID, 0) = 0
                  BEGIN
                      IF( (SELECT Count(1)
                           FROM   tbl_Company
                           WHERE  NAME = @Name
                                  AND IsDeleted = 0) = 0 )
                        BEGIN
                            INSERT INTO tbl_Company
                                        (NAME,
                                         FullName,
                                         EmailID,
                                         ContactNo,
                                         Address,
                                         PanNo,
                                         TANNo,
                                         GSTNo,
                                         Website,
                                         Logo,
                                         CreatedBy,
                                         CreatedOn,
                                         ModifiedBy,
                                         ModifiedOn,
                                         PaymentFavour,
                                         CommunicationEmailId,
                                         EmpCodePattern)
                            VALUES      ( @Name,
                                          @FullName,
                                          @EmailID,
                                          @ContactNo,
                                          @Address,
                                          @PANNO,
                                          @TANNO,
                                          @GSTNO,
                                          @Website,
                                          @Logo,
                                          @SessionID,
                                          Getdate(),
                                          @SessionID,
                                          Getdate(),
                                          @pAYMENTfAVOUR,
                                          @CommunicationEmailId,
                                          @EmpCodePattern)

                            SET @Output = 'Inserted Successfully.'

                            GOTO Finish
                        END
                      ELSE
                        SET @Output = 'Already Exists'

                      GOTO Finish
                  END
                ELSE
                  BEGIN
                      IF( (SELECT Count(1)
                           FROM   tbl_Company
                           WHERE  NAME = @Name
                                  AND ID <> @ID
                                  AND IsDeleted = 0) = 0 )
                        BEGIN
                            UPDATE tbl_Company
                            SET    NAME = @Name,
                                   FullName = @FullName,
                                   ContactNo = @ContactNo,
                                   Address = @Address,
                                   EmailID = @EmailID,
                                   PaymentFavour = @pAYMENTfAVOUR,
                                   PanNo = @PANNO,
                                   TANNo = @TANNO,
                                   GSTNo = @GSTNO,
                                   Website = @Website,
                                   Logo = @Logo,
                                   ModifiedBy = @SessionID,
                                   ModifiedOn = Getdate(),
                                   CommunicationEmailId = @CommunicationEmailId,
                                   EmpCodePattern = @EmpCodePattern
                            WHERE  ID = @ID

                            SET @Output = 'Updated Successfully.'

                            GOTO Finish
                        END
                      ELSE
                        BEGIN
                            SET @Output = 'Already Exists'

                            GOTO Finish
                        END
                  END
            END

          FINISH:

          SELECT @Output
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
