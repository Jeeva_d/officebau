﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :           
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>        
 [SearchEmailRptContent] '01-Jul-2018','07-Jul-2019','FSM'     
 </summary>                                   
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[SearchEmailRptContent] (@fromDate Datetime, @toDate Datetime,     
                                                @DomainName VARCHAR(200))      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          DECLARE @DomainID INT=(SELECT Id      
            FROM   tbl_Company      
            WHERE  NAME = @DomainName      
                   AND Isdeleted = 0)      
 ;WITH CTE AS(    
          SELECT      
           c.NAME,      
                 ips.PaymentDate,      
                 Isnull(ipm.Amount,0) Amount,      
                 ISNULL(ips.PaymentDescription, 'No comments updated') Reference, (Select top 1 l.Name from tblInvoiceItem it      
 Join tblProduct_v2 l on  l.id=it.ProductID  where InvoiceID = i.id) As TType      
          FROM   tblinvoicepaymentmapping ipm      
                 JOIN tblInvoicePayment ips      
                   ON ipm.InvoicePaymentID = ips.ID      
                 JOIN tblInvoice i      
                   ON ipm.invoiceid = i.ID      
                 JOIN tblCustomer c      
                   ON i.customerid = c.id      
          WHERE  ( @fromDate IS NULL              
                               OR Ips.PaymentDate >= @fromDate )              
                        AND ( @toDate IS NULL              
                               OR Ips.PaymentDate <= @toDate )       
                 AND ipm.IsDeleted = 0      
                 AND ipm.DomainID = @DomainID      
          UNION ALL      
          SELECT  tp.PartyName,      
                 tp.PaymentDate,      
                 Isnull(tp.TotalAmount,0) Amount,      
                 ISNULL(tp.Description, 'No comments updated') Reference,l.Name      
          FROM   tbl_Payment tp      
    Join tblLedger l on l.ID = tp.LedgerID      
          WHERE   ( @fromDate IS NULL              
                               OR PaymentDate>= @fromDate )              
                        AND ( @toDate IS NULL              
                               OR PaymentDate <= @toDate )       
                 AND tp.IsDeleted = 0      
                 AND tp.DomainID = @DomainID     
                 )    
                 Select * from CTE order by PaymentDate desc    
                     
                 ;with CTE as(     
      
          SELECT   V.NAME,      
                 ep.Date,      
                Isnull( epm.Amount,0) Amount,      
                 ISNULL(expDetails.Remarks, ISNULL(ep.Reference, 'No comments updated')) AS Reference,(Select top 1 l.Name      
      from tblExpenseDetails it join tblLedger l on l.ID =it.LedgerID where it.ExpenseID=e.id) As TType      
          FROM   tblExpensePaymentMapping epm      
                 JOIN tblexpensepayment ep      
                   ON epm.expensepaymentid = ep.ID      
                 JOIN tblExpense e      
                   ON epm.expenseid = e.id      
                 JOIN tblvendor v      
                   ON e.vendorid = v.id      
                 JOIN tblExpenseDetails expDetails      
                   ON expDetails.ExpenseID = e.ID      
          WHERE  ( @fromDate IS NULL              
                               OR ep.date>= @fromDate )              
                        AND ( @toDate IS NULL              
                               OR ep.date <= @toDate )        
                 AND epm.IsDeleted = 0      
                 AND epm.DomainID = @DomainID   
				   UNION ALL      
          SELECT  tp.PartyName,      
                 tp.PaymentDate,      
                 Isnull(tp.TotalAmount,0) Amount,      
                 ISNULL(tp.Description, 'No comments updated') Reference,l.Name      
          FROM   tbl_Receipts tp      
    Join tblLedger l on l.ID = tp.LedgerID      
          WHERE   ( @fromDate IS NULL              
                               OR PaymentDate>= @fromDate )              
                        AND ( @toDate IS NULL              
                               OR PaymentDate <= @toDate )       
                 AND tp.IsDeleted = 0      
                 AND tp.DomainID = @DomainID   
           
                 )    
                 select * from CTE order by date desc    
      
          SELECT  bk.NAME                  AS TransferFromName,      
                 ISNULL(ban.NAME, 'CASH') AS TransferToName,      
                Isnull( Amount ,0)                  AS Amount,      
                 VoucherDate              AS VoucherDate,      
                 VoucherDescription       AS VoucherDescription      
          FROM   tblTransfer con      
                 LEFT JOIN tblBank bk      
                        ON bk.ID = con.FromID      
                 LEFT JOIN tblBank ban      
                        ON ban.ID = con.ToID      
          WHERE ( @fromDate IS NULL              
                               OR VoucherDate>= @fromDate )              
                        AND ( @toDate IS NULL              
                               OR VoucherDate <= @toDate )     
                 AND con.isdeleted = 0      
                 AND con.DomainID = @DomainID      
           Order by VoucherDate desc    
      
          SELECT DISTINCT B.NAME,      
                          ( (SELECT Isnull(Sum(amount), 0)      
                             FROM   tblbrs      
                    WHERE  isdeleted = 0      
AND IsReconsiled = 1      
                                    AND SourceType IN((SELECT ID      
                                                       FROM   tblMasterTypes      
                                                       WHERE  BankID = TBRS.BankID      
                                                              AND NAME IN ( 'InvoiceReceivable', 'Receive Payment', 'ToBank' )))) -       
                 (SELECT Isnull(Sum(amount), 0)      
                                                        FROM   tblbrs      
                                                        WHERE  isdeleted = 0      
                                                            AND IsReconsiled = 1      
                                                            AND SourceType IN((SELECT ID      
                                                                                FROM   tblMasterTypes      
                                                                                WHERE  BankID = TBRS.BankID      
                                                                                        AND NAME IN ( 'PayBill', 'FromBank', 'Bill', 'Expense', 'Make Payment' )))) )      
                       + B.OpeningBalance AS Amount      
          FROM   tblBank B      
                 LEFT JOIN TblBrs TBRS      
                        ON TBRS.bankID = B.ID      
          WHERE  B.isdeleted = 0      
                 AND B.DomainID = @DomainID      
          UNION      
          SELECT 'Cash',      
                 AvailableCash      
          FROM   tblcashBucket      
          WHERE  DomainID = @DomainID      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
