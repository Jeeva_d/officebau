﻿/****************************************************************************         
CREATED BY		: Naneeshwar.M        
CREATED DATE	: 14-JUN-2017
MODIFIED BY		:  Dhanalakshmi.S	
MODIFIED DATE   :  18-SEP-2017
 <summary>                
    [Searchempdropdowndesignation] 224,1
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchempdropdowndesignation] (@EmployeeID INT,
                                                      @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @applicationRoleID INT = (SELECT ID
             FROM   tbl_ApplicationRole
             WHERE  NAME = 'Sales Manager'
                    AND DomainID = @DomainID
                    AND Isdeleted = 0)

          IF( (SELECT Isnull(ApplicationRoleID, 0)
               FROM   tbl_EMployeeOtherDetails
               WHERE  EmployeeID = @EmployeeID
                      AND DomainID = @DomainID
                      AND Isdeleted = 0) = @applicationRoleID )
            BEGIN ;
                WITH cte
                     AS (SELECT DISTINCT t.ID                     AS ID,
                                         Isnull(t.FirstName, '') + ' '
                                         + Isnull(t.LastName, '') AS NAME
                         FROM   tbl_EmployeeMaster t
                                JOIN tbl_EmployeeOtherDetails TOD
                                  ON T.ID = TOD.EmployeeID
                                CROSS APPLY dbo.Splitstring((SELECT BusinessUnitID
                                                             FROM   tbl_EmployeeMaster
                                                             WHERE  ID = @EmployeeID
                                                                    AND Isdeleted = 0
                                                                    AND Isnull(IsActive, 0) = 0
                                                                    AND DomainID = @DomainID), ',') s
                         WHERE  Patindex('%,' + s.Item + ',%', BusinessUnitID) > 0
                                AND t.ID != @EmployeeID
                                AND TOD.ApplicationRoleID = (SELECT ID
                                                             FROM   tbl_ApplicationRole
                                                             WHERE  [Name] = 'Sales Executive')
                                AND t.Isdeleted = 0
                                AND Isnull(t.IsActive, 0) = 0
                                AND t.DomainID = @DomainID
                         UNION
                         SELECT t.ID                     AS ID,
                                Isnull(t.FirstName, '') + ' '
                                + Isnull(t.LastName, '') AS NAME
                         FROM   tbl_Customer C
                                JOIN tbl_EmployeeMaster t
                                  ON t.ID = C.SalesPersonID
                                     AND Isnull(t.IsActive, 0) = 1
                                JOIN tbl_EmployeeOtherDetails TOD
                                  ON T.ID = TOD.EmployeeID
                                CROSS APPLY dbo.Splitstring((SELECT BusinessUnitID
                                                             FROM   tbl_EmployeeMaster
                                                             WHERE  ID = @EmployeeID
                                                                    AND Isdeleted = 0
                                                                    AND Isnull(IsActive, 0) = 0
                                                                    AND DomainID = @DomainID), ',') s
                         WHERE  Patindex('%,' + s.Item + ',%', BusinessUnitID) > 0
                                AND t.ID != @EmployeeID
                                AND TOD.ApplicationRoleID = (SELECT ID
                                                             FROM   tbl_ApplicationRole
                                                             WHERE  [Name] = 'Sales Executive')
                                AND t.Isdeleted = 0
                                AND C.IsDeleted = 0
                                AND t.DomainID = @DomainID)
                SELECT *
                FROM   cte
                ORDER  BY NAME
            END
          ELSE
            BEGIN
                SELECT ID                     AS ID,
                       Isnull(FirstName, '') + ' '
                       + Isnull(LastName, '') AS NAME
                FROM   tbl_EmployeeMaster
                WHERE  ID = @EmployeeID
                       AND Isdeleted = 0
                       AND Isnull(IsActive, 0) = 0
                       AND DomainID = @DomainID
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
