﻿/**************************************************************************** 
CREATED BY    		:	Naneeshwar.M	
CREATED DATE		:	13-JULY-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
[Payrollexcelupload] '1358','VictorSanthosarajP',4,2017,0,0,0,0,0,0,0,0,5000,0,0,106,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Payrollexcelupload] (@EmployeeCode   VARCHAR(20),
                                            @EmployeeName   VARCHAR(50),
                                            @MonthId        INT,
                                            @Year           INT,
                                            @PT             MONEY,
                                            @TDS            MONEY,
                                            @PLI            MONEY,
                                            @Mediclaim      MONEY,
                                            @MobileDataCard MONEY,
                                            @OtherEarning   MONEY,
                                            @OtherPerks     MONEY,
                                            @OtherDeduction MONEY,
                                            @OverTime       MONEY,
                                            @MonsoonAllow   MONEY,
                                            @Loans          MONEY,
                                            @Bonus          MONEY,
                                            @EEESI          MONEY,
                                            @ERESI          MONEY,
                                            @SessionID      INT,
                                            @DomainID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output          VARCHAR(100) ='',
              @EmployeeID      INT,
              @TDSYear         INT,
              @BaseLocationID  INT,
              @RegionID        INT,
              @PAYEESILimit    INT,
              @PAYEESIValue    MONEY,
              @PAYEESIPercent  DECIMAL(18, 2),
              @PAYERESILimit   INT,
              @PAYERESIValue   MONEY,
              @PAYERESIPercent DECIMAL(18, 2)

      IF( @MonthID BETWEEN 1 AND 3 )
        SET @TDSYear=( (SELECT FY.NAME
                        FROM   tbl_FinancialYear FY
                        WHERE  FY.NAME = @Year
                               AND FY.IsDeleted = 0
                               AND FY.DomainID = @DomainID) - 1 )
      ELSE
        SET @TDSYear =(SELECT FY.NAME
                       FROM   tbl_FinancialYear FY
                       WHERE  FY.NAME = @Year
                              AND FY.IsDeleted = 0
                              AND FY.DomainID = @DomainID)

      BEGIN TRY
          BEGIN TRANSACTION

          BEGIN
              SET @EmployeeID=(SELECT ID
                               FROM   tbl_EmployeeMaster
                               WHERE  Code = @EmployeeCode
                                      AND Replace(( Rtrim(Ltrim(FirstName))
                                                    + Rtrim(Ltrim(Isnull(LastName, ''))) ), ' ', '') = @EmployeeName
                                      AND IsDeleted = 0
                                      AND ( Isnull(IsActive, 0) = 0
                                             OR ( Isnull(IsActive, 0) = 1
                                                  AND Month(InactiveFrom) >= @MonthId ) ))
              SET @BaseLocationID=(SELECT BaseLocationID
                                   FROM   tbl_EmployeeMaster
                                   WHERE  ID = @EmployeeID)
              SET @RegionID = (SELECT ParentID
                               FROM   tbl_BusinessUnit
                               WHERE  id = @BaseLocationID)
              SET @PAYEESILimit=(SELECT Limit
                                 FROM   tbl_PayrollConfiguration
                                 WHERE  Components = 'EmployeeESI'
                                        AND BusinessUnitID = @RegionID)
              SET @PAYEESIValue=(SELECT Value
                                 FROM   tbl_PayrollConfiguration
                                 WHERE  Components = 'EmployeeESI'
                                        AND BusinessUnitID = @RegionID)
              SET @PAYEESIPercent=(SELECT Percentage
                                   FROM   tbl_PayrollConfiguration
                                   WHERE  Components = 'EmployeeESI'
                                          AND BusinessUnitID = @RegionID)
              SET @PAYERESILimit=(SELECT Limit
                                  FROM   tbl_PayrollConfiguration
                                  WHERE  Components = 'EmployerESI'
                                         AND BusinessUnitID = @RegionID)
              SET @PAYERESIValue=(SELECT Value
                                  FROM   tbl_PayrollConfiguration
                                  WHERE  Components = 'EmployerESI'
                                         AND BusinessUnitID = @RegionID)
              SET @PAYERESIPercent=(SELECT Percentage
                                    FROM   tbl_PayrollConfiguration
                                    WHERE  Components = 'EmployerESI'
                                           AND BusinessUnitID = @RegionID)

              --SET @EEESI= CASE
              --              WHEN ( ( (SELECT ( GrossEarning - OverTime )
              --                        FROM   tbl_EmployeePayroll
              --                        WHERE  EmployeeId = @EmployeeID
              --                               AND MonthId = @MonthId
              --                               AND YearId = @Year
              --                               AND IsDeleted = 0)
              --                       + @OverTime ) >= @PAYEESILimit ) THEN @PAYEESIValue
              --              ELSE Ceiling(( ( (SELECT ( GrossEarning - OverTime )
              --                                FROM   tbl_EmployeePayroll
              --                                WHERE  EmployeeId = @EmployeeID
              --                                       AND MonthId = @MonthId
              --                                       AND YearId = @Year
              --                                       AND IsDeleted = 0)
              --                               + @OverTime ) * @PAYEESIPercent / 100 ))
              --            END
              --SET @ERESI= CASE
              --              WHEN ( ( (SELECT ( GrossEarning - OverTime )
              --                        FROM   tbl_EmployeePayroll
              --                        WHERE  EmployeeId = @EmployeeID
              --                               AND MonthId = @MonthId
              --                               AND YearId = @Year
              --                               AND IsDeleted = 0)
              --                       + @OverTime ) >= @PAYERESILimit ) THEN @PAYERESIValue
              --              ELSE Ceiling(( ( (SELECT ( GrossEarning - OverTime )
              --                                FROM   tbl_EmployeePayroll
              --                                WHERE  EmployeeId = @EmployeeID
              --                                       AND MonthId = @MonthId
              --                                       AND YearId = @Year
              --                                       AND IsDeleted = 0)
              --                               + @OverTime ) * @PAYERESIPercent / 100 ))
              --            END
              UPDATE tbl_EmployeePayroll
              SET    PT = @PT,
                     TDS = @TDS,
                     PLI = @PLI,
                     Mediclaim = @Mediclaim,
                     MobileDataCard = @MobileDataCard,
                     OtherEarning = @OtherEarning,
                     OtherPerks = @OtherPerks,
                     OtherDeduction = @OtherDeduction,
                     Loans = @Loans,
                     Bonus = @Bonus,
                     OverTime = @OverTime,
                     EEESI = @EEESI,
                     ERESI = @ERESI,
                     MonsoonAllow = @MonsoonAllow,
                     NetSalary = (SELECT ( ( GrossEarning + @PLI + @MobileDataCard
                                             + @OtherEarning + @OtherPerks + @OverTime
                                             + @MonsoonAllow ) - ( @EEESI + EEPF + @TDS + @PT + @OtherDeduction + @Loans
                                                                   + MonsoonAllow ) )
                                  FROM   tbl_EmployeePayroll
                                  WHERE  EmployeeId = @EmployeeID
                                         AND MonthId = @MonthId
                                         AND YearId = @Year
                                         AND IsDeleted = 0),
                     CTC = (SELECT ( GrossEarning + @PLI + @MobileDataCard
                                     + @OtherEarning + @OtherPerks + @OverTime + @Bonus
                                     + @Mediclaim + @ERESI + ERPF + Gratuity
                                     + @MonsoonAllow )
                            FROM   tbl_EmployeePayroll
                            WHERE  EmployeeId = @EmployeeID
                                   AND MonthId = @MonthId
                                   AND YearId = @Year
                                   AND IsDeleted = 0),
                     ModifiedBy = @SessionID,
                     ModifiedOn = Getdate()
              WHERE  EmployeeId = @EmployeeID
                     AND MonthId = @MonthId
                     AND YearId = @Year
                     AND IsDeleted = 0
                     AND IsProcessed = 0

              SET @Output ='Updated Successfully !'

              GOTO FINISH
          END

          FINISH:

          IF EXISTS(SELECT 1
                    FROM   tbl_TDS
                    WHERE  EmployeeId = @EmployeeID
                           AND MonthId = @MonthId
                           AND YearID = (SELECT ID
                                         FROM   tbl_FinancialYear
                                         WHERE  NAME = @TDSYear
                                                AND IsDeleted = 0
                                                AND DomainID = @DomainID)
                           AND IsDeleted = 0)
            BEGIN
                UPDATE tbl_TDS
                SET    TDSAmount = @TDS
                WHERE  MonthId = @MonthId
                       AND YearID = (SELECT ID
                                     FROM   tbl_FinancialYear
                                     WHERE  NAME = @TDSYear
                                            AND IsDeleted = 0
                                            AND DomainID = @DomainID)
                       AND EmployeeId = @EmployeeID
            END

          IF NOT EXISTS(SELECT 1
                        FROM   tbl_TDS
                        WHERE  EmployeeId = @EmployeeID
                               AND MonthId = @MonthId
                               AND YearID = (SELECT ID
                                             FROM   tbl_FinancialYear
                                             WHERE  NAME = @TDSYear
                                                    AND IsDeleted = 0
                                                    AND DomainID = @DomainID)
                               AND IsDeleted = 0)
            BEGIN
                INSERT INTO tbl_TDS
                            (EmployeeId,
                             MonthId,
                             YearID,
                             TDSAmount,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             DomainId,
                             BaseLocationID)
                VALUES      (@EmployeeID,
                             @MonthId,
                             (SELECT ID
                              FROM   tbl_FinancialYear
                              WHERE  NAME = @TDSYear
                                     AND IsDeleted = 0
                                     AND DomainID = @DomainID),
                             Isnull(@TDS, 0),
                             @SessionID,
                             Getdate(),
                             @SessionID,
                             Getdate(),
                             @DomainID,
                             @BaseLocationID)
            END

          SELECT @Output AS [Output]

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
