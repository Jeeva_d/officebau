﻿/****************************************************************************           
CREATED BY  : Priya K        
CREATED DATE : 30-Oct-2018        
MODIFIED BY  :        
MODIFIED DATE :        
<summary>        
 [Pay_SearchTDSDetails] 11,4,4,1       
</summary>                                   
*****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_SearchTDSDetails] (@MonthID      INT,  
                                              @Year         INT,  
                                              @BusinessUnit VARCHAR(1000),  
                                              @DomainID     INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @YearName INT = (SELECT NAME  
             FROM   tbl_FinancialYear  
             WHERE  id = @Year)  
          DECLARE @CustomDate DATETIME = Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @YearName - 2000, Dateadd(MONTH, @MonthID - 1, '20000101')))  
                                      + 1, 0))  
  
          SELECT Isnull(TDS.Id, 0)    AS ID,  
                 EMP.ID               AS EmployeeID,  
                 EMP.FullName         AS EmployeeName,  
                 Isnull(EMP.EmpCodePattern, '') + EMP.Code             AS EmployeeNo,  
                 Isnull(DEP.NAME, '') AS DepartmentName,  
                 @MonthID             AS MonthID,  
                 @Year                AS YearID,  
                 TDS.TDSAmount        AS TDSAmount,  
                 DOJ                  AS DOJ,  
                 CASE  
                   WHEN Isnull(PAY.ID, 0) <> 0 THEN 'Processed'  
                   ELSE 'Open'  
                 END                  AS [Status],  
                 BUU.NAME             AS BusinessUnit,  
                 BUU.ID               AS BaseLocationID  
          INTO   #tmptds  
          FROM   tbl_EmployeeMaster EMP  
                 JOIN tbl_BusinessUnit BUU  
                   ON EMP.BaseLocationID = BUU.ID  
                 LEFT JOIN tbl_TDS TDS  
                        ON EMP.ID = TDS.EmployeeId  
                           AND TDS.MonthId = @MONTHID  
                           AND TDS.IsDeleted = 0  
                           AND TDS.YearID = @Year  
                 LEFT JOIN tbl_Pay_EmployeePayroll PAY  
                        ON PAY.EmployeeId = EMP.ID  
                           AND PAY.MonthId = @MONTHID  
                           AND PAY.YearId = @YearName  
                           AND PAY.IsDeleted = 0  
                 LEFT JOIN tbl_department DEP  
                        ON DEP.ID = EMP.DepartmentID  
          WHERE  EMP.DomainID = @DomainID  
                 AND EMP.IsDeleted = 0  
                 AND ( Isnull(emp.IsActive, '') = 0  
                        OR ( Month(InactiveFrom) = @MonthID  
                             AND Year(InactiveFrom) = @Year ) )  
                 AND ( ( Isnull(@BusinessUnit, '') <> ''  
                         AND EMP.BaseLocationID IN (SELECT Item  
                                                    FROM   dbo.Splitstring (@BusinessUnit, ',')  
                                                    WHERE  Isnull(Item, '') <> '') )  
                        OR Isnull(@BusinessUnit, '') = '' )  
                 AND Cast(EMP.DOJ AS DATE) <= Cast (@CustomDate AS DATE)  
                 AND EMP.IsDeleted = 0  
                 AND EMP.HasAccess = 1  
                 AND EMP.EmploymentTypeID = (SELECT ID  
                                             FROM   tbl_EmployementType  
                                             WHERE  NAME = 'Direct'  
                                                    AND DomainID = @DomainID)  
          ORDER  BY EMP.Code + ' - ' + EMP.FullName  
  
          SELECT *  
          FROM   #tmptds  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
