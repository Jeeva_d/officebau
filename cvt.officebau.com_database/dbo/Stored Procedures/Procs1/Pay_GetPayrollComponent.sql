﻿/****************************************************************************         
CREATED BY   :         
CREATED DATE  :         
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>      
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Pay_GetPayrollComponent] (@ID INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT 
               tppc.ID,
			   tppc.Code AS Code,
			   tppc.Description,
			   tppc.Ordinal
          FROM tbl_Pay_PayrollCompontents AS tppc
		  WHERE tppc.ID=@ID
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
