﻿/****************************************************************************       
CREATED BY   : Ajith N      
CREATED DATE  :  11 Sep 2017     
MODIFIED BY   :       
MODIFIED DATE  :       
<summary>              
	
</summary>                               
*****************************************************************************/
CREATE PROCEDURE [dbo].[ManageSubordinates] (@ID               INT,
                                            @SubordinatesList [SUBORDINATESLIST] readonly,
                                            @UserID           INT,
                                            @DomainID         INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT VARCHAR(100) = 'Operation Failed!'

          DELETE FROM tbl_SubordinatesDetails
          WHERE  TravelRequestID = @ID

          INSERT INTO tbl_SubordinatesDetails
                      (TravelRequestID,
                       NAME,
                       CreatedBy,
                       CreatedOn,
                       ModifiedBy,
                       ModifiedOn,
                       DomainID,
                       HistoryID)
          SELECT @ID,
                 NAME,
                 @UserID,
                 Getdate(),
                 @UserID,
                 Getdate(),
                 @DomainID,
                 Newid()
          FROM   @SubordinatesList
          WHERE  IsDeleted = 0 and Name IS NOT NULL

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
