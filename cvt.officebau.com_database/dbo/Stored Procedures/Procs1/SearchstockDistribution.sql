﻿/****************************************************************************                   
CREATED BY   : Priya K                  
CREATED DATE  :                   
MODIFIED BY   :                   
MODIFIED DATE  :                   
 <summary>                
 [Searchstockdistribution] 0, 1                
 </summary>                                           
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[SearchstockDistribution] (@EmployeeID INT,      
                                                 @DomainID   INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT isd.ID                                    AS ID,      
                 isd.CostCenterID                          AS CostCenterID,      
                 DistributedDate                           AS DistributedDate,      
                 isd.ProductID                             AS ProductID,      
                 pt.NAME                                   AS ProductName,      
                 isd.EmployeeID                            AS EmployeeID,      
            (case when (isd.IsInternal = '0')
			then
            Isnull(em.Code, '') + ' - ' + em.FullName
		  else
		 isd.Party
		 end)                                              AS EmployeeName,      
                 isd.Qty                                   AS Qty,      
                 isd.Remarks                               AS Description,      
                 CC.NAME                                   AS [Cost Center]      
          FROM   tbl_InventoryStockDistribution isd      
                 LEFT JOIN tblCostCenter cc      
                        ON cc.ID = isd.CostCenterID      
                 LEFT JOIN tbl_EmployeeMaster em      
                        ON em.ID = isd.EmployeeID      
                 LEFT JOIN tblProduct_V2 pt      
                        ON pt.ID = isd.ProductID      
          WHERE  isd.DomainID = @DomainID      
                 AND isd.IsDeleted = 0      
                 AND ( @EmployeeID = 0      
                        OR isd.EmployeeID = @EmployeeID )    
                        AND IsInventroy = 1    
          ORDER  BY isd.ModifiedOn DESC      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
