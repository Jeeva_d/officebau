﻿
/****************************************************************************   
CREATED BY		:  
CREATED DATE	: 
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
	[ManageEmployeeLeaveDays] 1027,'10-jan-2019',1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageEmployeeLeaveDays] (@EmployeeID INT,
                                                 @Date       DATE,
                                                 @UserID     INT,
                                                 @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF NOT EXISTS(SELECT 1
                        FROM   tbl_EmployeeAvailedLeave
                        WHERE  IsDeleted = 0
                               AND EmployeeID = @EmployeeID
                               AND [Year] = Year(@Date))
            BEGIN
                INSERT INTO tbl_EmployeeAvailedLeave
                            (EmployeeID,
                             LeaveTypeID,
                             TotalLeave,
                             AvailedLeave,
                             Year,
                             DomainID,
                             HistoryID,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             IsDeleted)
                SELECT @EmployeeID AS EmployeeID,
                       LT.ID       AS LeaveTypeId,
                       0           AS TotalLeave,
                       0           AS [TotalLeave],
                       Year(@Date) AS [Year],
                       @DomainID   AS DomainID,
                       Newid()     AS HistoryID,
                       @UserID     AS CreatedBy,
                       Getdate()   AS CreatedOn,
                       @UserID     AS ModifiedBy,
                       Getdate()   AS ModifiedOn,
                       0           AS IsDeleted
                FROM   tbl_LeaveTypes LT
                --LEFT JOIN tbl_EmployeeAvailedLeave AL
                --       ON AL.IsDeleted = 0
                --          AND AL.LeaveTypeID = LT.ID
                --          AND AL.[Year] = Year(@Date)
                --          AND AL.EmployeeID = @EmployeeID
                --LEFT JOIN tbl_EmployeeMaster EM
                --       ON em.ID = AL.EmployeeID
                WHERE  LT.IsDeleted = 0
                       AND LT.DomainID = @DomainID
                       AND LT.Code NOT IN ( 'VACATION', 'COMP', 'COMPOFF', 'PERMISSION',
                                            'PM', 'OD' )
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 
