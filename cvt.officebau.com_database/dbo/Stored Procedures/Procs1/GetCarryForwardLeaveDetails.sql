﻿/****************************************************************************     
CREATED BY   :   DHANALAKSHMI. S  
CREATED DATE  :   22-JAN-2019  
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>         
  [Getcarryforwardleavedetails] 4,1,3  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetCarryForwardLeaveDetails] (@Year        INT,
                                                     @DomainID    INT,
                                                     @LeaveTypeID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @Yearname  VARCHAR(80)= (SELECT NAME
                     FROM   tbl_FinancialYear
                     WHERE  ID = @Year
                            AND IsDeleted = 0
                            AND DomainID = @DomainID),
                  @LeaveType VARCHAR(50) = (SELECT NAME
                     FROM   tbl_LeaveTypes
                     WHERE  ID = @LeaveTypeID
                            AND IsDeleted = 0
                            AND DomainID = @DomainID)

          SELECT Isnull(cf.ID, 0)       AS ID,
                 EM.ID                  AS EmployeeID,
                 Isnull(EmpCodePattern, '')
                 + Isnull(EM.Code, '')  AS EmployeeCode,
                 FirstName              AS FirstName,
                 Isnull(LastName, '')       AS LastName,
                 @LeaveType             AS LeaveType,
                 ( CASE
                     WHEN CF.LeaveTypeID IS NULL THEN @LeaveTypeID
                     ELSE CF.LeaveTypeID
                   END )                AS LeaveTypeID,
                 ( CASE
                     WHEN CF.ID IS NULL THEN ( Isnull(EAL.TotalLeave, 0) - Isnull(EAL.AvailedLeave, 0) )
                     ELSE CF.TotalLeave
                   END )                AS TotalLeave,
                 Cast(@Yearname AS INT) AS Year
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_EmployeeAvailedleave EAL
                        ON EAL.EmployeeID = EM.ID
                           AND EAL.Year = ( @Yearname - 1 )
                           AND EAL.LeaveTypeID = @LeaveTypeID
                           AND EAL.DomainID = @DomainID
                           AND EAL.IsDeleted = 0
                 LEFT JOIN tbl_CarryForwardLeave CF
                        ON CF.EmployeeID = EM.ID
                           AND CF.LeaveTypeID = @LeaveTypeID
                           AND CF.Year = @Yearname
                           AND CF.DomainID = @DomainID
                           AND CF.IsDeleted = 0
                 LEFT JOIN tbl_LeaveTypes LT
                        ON LT.ID = CF.LeaveTypeID
                           AND LT.IsDeleted = 0
                           AND LT.DomainID = @DomainID
                           AND NAME NOT IN ( 'Permission', 'On Duty', 'Comp off', 'Vacation 1', 'Vacation 2' )
          WHERE  EM.IsDeleted = 0
                 AND EM.IsActive = 0
                 AND EM.DomainID = @DomainID
                 ORDER BY FirstName Asc
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
