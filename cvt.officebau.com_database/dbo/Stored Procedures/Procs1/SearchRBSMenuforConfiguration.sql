﻿/****************************************************************************                   
CREATED BY   :   Naneeshwar.M                
CREATED DATE  :   03-SEP-2018                
MODIFIED BY   :                   
MODIFIED DATE  :                   
 <summary>                
 [SearchRBSMenuforConfiguration] 2,1 ,'EMPCONFIG'               
 </summary>                                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchRBSMenuforConfiguration] (@UserID   INT,  
                                                       @DomainID INT,  
                                                       @MenuCode VARCHAR(50))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          DECLARE @ModuleID INT =(SELECT ID  
            FROM   tbl_RBSMenu  
            WHERE  MenuCode = @MenuCode  
                   AND DomainID = @DomainID)  
  
          SELECT SUB.SubModule   AS Module,  
                 RBS.Menu        AS Menu,  
                 RBS.ID          AS MenuID,  
                 RBS.MenuURL     AS MenuURL,  
                 RBS.MenuCode    AS MenuCode,  
                 RBS.MenuOrderID AS MORDERID,  
                 RBS.SubModuleID AS ModuleID,  
                 RBS.description AS ModuleGroup,  
                 ( CASE  
                     WHEN RBS.MenuURL LIKE 'EmployeeClaimsConfiguration/SearchDesignationMapping%' THEN  
                       'Employee Management'  
                     WHEN RBS.MenuURL LIKE 'Holiday%' THEN  
                       'Leave Management'  
                     WHEN RBS.MenuURL LIKE 'LeaveManagement%' THEN  
                       'Leave Management'  
                     WHEN RBS.MenuURL LIKE 'EmployeeClaimsConfiguration%' THEN  
                       'Claims'  
                     WHEN RBS.MenuURL LIKE 'TDSDeclaration%' THEN  
                       'TDS'  
                     WHEN RBS.MenuURL LIKE 'TDSConfiguration%' THEN  
                       'TDS'  
                     WHEN RBS.MenuURL LIKE 'EmployeeLoan%' THEN  
                       'Loan Management'  
                     WHEN RBS.MenuURL LIKE 'EmployeeAppraisalSystem/SearchAppraisalSystemConfiguration%' THEN  
                       'Employee Management'  
                     WHEN RBS.MenuURL LIKE 'CarryForwardLeave%' THEN  
                       'Leave Management'  
                     WHEN RBS.MenuURL = 'ApplicationConfiguration%' THEN  
                       'Application Configuration'  
                     ELSE  
                       'Payroll'  
                   END )         AS SubModule,  
                 ( CASE  
                     WHEN RBS.MenuURL LIKE 'EmployeeClaimsConfiguration/SearchDesignationMapping%' THEN  
                       1  
                     WHEN RBS.MenuURL LIKE 'Holiday%' THEN  
                       4  
                     WHEN RBS.MenuURL LIKE 'LeaveManagement%' THEN  
                       4  
                     WHEN RBS.MenuURL LIKE 'EmployeeClaimsConfiguration%' THEN  
                       5  
                     WHEN RBS.MenuURL LIKE 'TDSDeclaration%' THEN  
                       3  
                     WHEN RBS.MenuURL LIKE 'TDSConfiguration%' THEN  
                       3  
                     WHEN RBS.MenuURL LIKE 'EmployeeLoan%' THEN  
                       6  
                     ELSE  
                       2  
                   END )         AS RNo  
          --INTO   #tmp      
          FROM   tbl_RBSMenu RBS  
                 JOIN tbl_RBSMenu SUBRBS  
                   ON SUBRBS.ID = RBS.SubModuleID  
                 LEFT JOIN tbl_RBSSubModule SUB  
                        ON SUB.ID = SUBRBS.SubModuleID  
                 LEFT JOIN tbl_RBSUserMenuMapping RUM  
                        ON RUM.MenuID = RBS.ID  
                           AND RUM.EmployeeID = Isnull(@UserID, RUM.EmployeeID)  
                           AND RUM.DomainID = @DomainID  
          WHERE  RUM.MRead = 1  
               AND RBS.ISDELETED = 0  
                 AND SUBRBS.ID = Isnull(@ModuleID, SUBRBS.ID)  
                 AND RBS.IsSubmenu = 1  
                 AND RBS.RootConfig = 'CONFIG'  
                 AND RBS.DomainID = @DomainID  
          ORDER  BY RNo,  
                    SUBRBS.ID ASC,  
                    RBS.MenuOrderID ASC  
  
          --SELECT *      
          --FROM   #tmp      
          --ORDER  BY RNo      
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END  