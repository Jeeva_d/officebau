﻿/****************************************************************************     
CREATED BY      : Naneeshwar.M    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
<summary>      
[Searchbusinessunitbyemployee] 1,68    
</summary>                             
*****************************************************************************/    
CREATE PROCEDURE [dbo].[SearchBusinessUnitByEmployee] (@DomainID   INT,    
                                                      @EmployeeID INT)    
AS    
  BEGIN    
      BEGIN TRY    
          BEGIN    
              SELECT *    
              INTO   #temp    
              FROM   dbo.[Fnsplitstring]((SELECT businessunitid    
                                          FROM   tbl_employeemaster    
                                          WHERE  id = @EmployeeID    
                                                 AND isdeleted = 0    
                                                 AND domainID = @DomainID), ',')    
              WHERE  Isnull(splitdata, '') <> ''    
    
              SELECT BU.ID,    
                     BU.NAME    
              FROM   #temp t    
                     LEFT JOIN tbl_BusinessUnit BU    
                            ON BU.ID = t.splitdata    
          END    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR (@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
