﻿/****************************************************************************   
CREATED BY		:  Naneeshwar.M
CREATED DATE	:  20-SEP-2017
MODIFIED BY		:  Jennifer.S
MODIFIED DATE	:  16-OCT-2017  
 <summary> 
          [Email_getnewjoinerforlastmonth]  
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Email_GetNewJoinerForLastMonth]
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN
              DECLARE @KeyID INT = (SELECT ID
                 FROM   tbl_MailerEventKeys
                 WHERE  NAME = 'LASTMONTHJOIN'
                        AND Isdeleted = 0)

              SELECT emp.Code                                                              AS Code,
                     emp.FirstName + ' ' + Isnull(emp.lastName, '')                        AS EmployeeName,
                     emp.EmailID                                                           AS EmailID,
                     com.FullName                                                          AS CompanyName,
                     com.NAME                                                              AS CompanyFolderName,
                     Cast(DOJ AS DATE)                                                     AS DOJ,
                     BU.NAME                                                               AS Location,
                     Stuff((SELECT DISTINCT ';' + EmailID
                            FROM   tbl_EmployeeMaster emp
                                   JOIN (SELECT *
                                         FROM   dbo.Splitstring ((SELECT CCEmailID
                                                                  FROM   tbl_EmailRecipients
                                                                  WHERE  BusinessUnitID = emp.BaseLocationID
                                                                         AND KeyID = @KeyID), ',')
                                         WHERE  Isnull(Item, '') <> '') AS eml
                                     ON emp.ID = eml.Item
                            FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') AS CC,
                     depart.NAME                                                           AS Department,
                     design.NAME                                                           AS Designation,
                     fu.Path                                                               AS ProfilePhoto
              FROM   tbl_employeemaster emp
                     LEFT JOIN tbl_Company com
                            ON com.ID = emp.DomainID
                     LEFT JOIN tbl_BusinessUnit BU
                            ON bu.ID = emp.BaseLocationID
                     LEFT JOIN tbl_Department depart
                            ON depart.ID = emp.DepartmentID
                     LEFT JOIN tbl_Designation design
                            ON design.ID = emp.DesignationID
                     LEFT JOIN tbl_FileUpload fu
                            ON fu.Id = emp.FileID
              WHERE  Cast(emp.DOJ AS DATE) BETWEEN CONVERT(VARCHAR(10), Month( Getdate())-1)
                                                   + '-' + '1' + '-'
                                                   + CONVERT(VARCHAR(10), Year(Getdate())) AND CONVERT(VARCHAR(10), Month( Getdate())-1)
                                                                                               + '-' + '30' + '-'
                                                                                               + CONVERT(VARCHAR(10), Year(Getdate()))
                     AND emp.isdeleted = 0
                     AND emp.isactive = 0
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
