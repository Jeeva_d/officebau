﻿
/****************************************************************************   
CREATED BY   :   Naneeshwar.M
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
	[Searchcfstotal] 
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchcfstotal](@MonthID  INT,
                                       @Year     INT,
                                       @DomainID INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @YearName INT =(SELECT FinancialYear
            FROM   tbl_FinancialYear
            WHERE  ID = @Year
                   AND ISNULL(IsDeleted, 0) = 0
                   AND DomainId = @DomainID)

          SELECT BUS.NAME      AS NAME,
                 Sum(CFS.unit) Unit
          FROM   tbl_cfs CFS
                 JOIN tbl_BusinessUnit BUS
                   ON BUS.ID = CFS.businessunit
          WHERE  CFS.IsDeleted = 0
                 AND CFS.DomainId = @DomainID
                 AND ( ISNULL(@MonthID, 0) = 0
                        OR CFS.MonthID = @MonthID )
                 AND ( ISNULL(@Year, 0) = 0
                        OR CFS.YearID = @YearName )
          GROUP  BY BUS.NAME
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
