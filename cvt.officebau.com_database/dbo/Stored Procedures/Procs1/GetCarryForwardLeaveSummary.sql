﻿/****************************************************************************         
CREATED BY   :   DHANALAKSHMI. S      
CREATED DATE  :   22-JAN-2019      
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>             
  [GetCarryForwardLeaveSummary] 1      
 </summary>                                 
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetCarryForwardLeaveSummary] (@DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @ColumnNames AS NVARCHAR(MAX),  
                  @SqlQuery    AS NVARCHAR(MAX);  
  
          SET @ColumnNames = Stuff((SELECT DISTINCT ',' + Quotename(L.NAME)  
                                    FROM   tbl_LeaveTypes L  
                                    WHERE  IsDeleted = 0  
                                           AND DomainID = @DomainID  
                                           AND NAME NOT IN ( 'Permission', 'On Duty', 'Comp off', 'Vacation 1', 'Vacation 2' )  
                                    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')  
          SET @SqlQuery = 'SELECT EmployeeID,  EmployeeCode AS [Emp Code], FirstName AS [First Name], LastName AS [Last Name],'  
                          + @ColumnNames  
                          + ' from       
            (      
                select EM.ID AS EmployeeID,    
                      Isnull(EmpCodePattern, '''') + Isnull(EM.Code, '''')  AS EmployeeCode,  
                      FirstName              AS FirstName,  
                      Isnull(LastName, '''')       AS LastName,     
                      LT.Name As LeaveType       
                      , TotalLeave      
                from tbl_EmployeeMaster EM      
                     LEFT JOIN tbl_CarryForwardLeave CF      
                       ON CF.EmployeeID = EM.ID       
                       LEFT JOIN tbl_LeaveTypes LT      
                      ON LT.ID = CF.LeaveTypeID and LT.IsDeleted = 0      
                          AND LT.DomainID = '  
                          + Cast(@DomainID AS VARCHAR(50))  
                          + '      
                          AND NAME NOT IN ( ''Permission'', ''On Duty'', ''Comp off'', ''Vacation 1'', ''Vacation 2'' )       
                      where EM.IsDeleted =0       
                      and EM.IsActive =0       
                      and EM.DomainID = '  
                          + Cast(@DomainID AS VARCHAR(50))  
                          + '      
           ) x      
            pivot       
            (      
                 Sum(TotalLeave)      
                for LeaveType in ('  
                          + @ColumnNames + ')      
            ) p ORDER BY p.[FirstName]' 
            
              
          EXECUTE(@SqlQuery)  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
