﻿/**************************************************************************** 
CREATED BY    		:	Naneeshwar M
CREATED DATE		:	04-DEC-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
		 truncate table tbl_CFSOperation
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CFSEXCELUPLOAD] (@CFSUPLOAD CFSUPLOAD READONLY,
                                        @SessionID INT,
                                        @DomainID  INT)
AS
  BEGIN
      BEGIN TRY
          SET NOCOUNT ON;

          DECLARE @Output VARCHAR(100) = ''

          BEGIN TRANSACTION

          SELECT Row_number()
                   OVER (
                     PARTITION BY c.ContainerNo
                     ORDER BY c.ContainerNo DESC) AS ORDERNUMBER,
                 *
          INTO   #Temp
          FROM   @CFSUPLOAD c

          INSERT INTO tbl_CFSOperation
                      (PayerName,
                       ShippingBillNo,
                       ContainerNo,
                       Size,
                       ExporterName,
                       ContainerStatus,
                       GateInDate,
                       LinerAgent,
                       ConsigneeName,
                       TransporterName,
                       TrailerNo,
                       CargoDesciption,
                       Scanning,
                       BOENo,
                       BOEDate,
                       IGMNo,
                       VesselNo,
                       CHANo,
                       CreatedBy,
                       CreatedOn,
                       ModifiedBy,
                       ModifiedOn,
                       DomainId,
                       HistoryID)
          SELECT cf.PayerName,
                 cf.ShippingBillNo,
                 cf.ContainerNo,
                 cf.Size,
                 cf.ExporterName,
                 cf.ContainerStatus,
                 cf.GateInDate,
                 cf.LinerAgent,
                 cf.ConsigneeName,
                 cf.TransporterName,
                 cf.TrailerNo,
                 cf.CargoDesciption,
                 cf.Scanning,
                 cf.BOENo,
                 cf.BOEDate,
                 cf.IGMNo,
                 cf.VesselNo,
                 cf.CHANo,
                 @SessionID,
                 Getdate(),
                 @SessionID,
                 Getdate(),
                 @DomainID,
                 Newid()
          FROM   #Temp cf
                 LEFT JOIN tbl_CFSOperation cfs
                        ON cf.ContainerNo = cfs.ContainerNo
          WHERE  cfs.ContainerNo IS NULL
                 AND cf.ContainerNo IS NOT NULL
                 AND cf.ORDERNUMBER = 1

          SET @Output = 'Uploaded Successfully.'

          COMMIT TRANSACTION

          SELECT @Output AS [Output]
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
