﻿ /**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	06-JUN-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary> 
	[SearchMasterData] 'null','destinationcities',1      
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchMasterData] (@DependantTable VARCHAR(200),
                                          @MainTable      VARCHAR(200),
                                          @DomainID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY

          DECLARE @sql VARCHAR(MAX) = ''

          IF( @MainTable = 'Currency' )
            BEGIN
                IF( Isnull(@DependantTable, '') = ''
                     OR Isnull(@DependantTable, '') = 'Null' )
                  SET @sql =( 'Select ID,Name,Remarks ,null AS DRPName from tbl'
                              + @MainTable
                              + ' Where IsDeleted = 0 order by Name Asc' )
            END
          ELSE
          BEGIN
              IF( Isnull(@DependantTable, '') = ''
                   OR Isnull(@DependantTable, '') = 'Null' )
                BEGIN
                    IF( @MainTable = 'BusinessUnit' )
                      BEGIN
                          SET @sql = ( 'Select ID,Name,Remarks ,null AS DRPName from tbl_'
                                       + @MainTable
                                       + ' Where IsDeleted = 0 and ISNULL(ParentID, 0) = 0 and DomainID = '
                                       + CONVERT(VARCHAR(10), @domainID)
                                       + 'order by Name Asc' )
                      END
                    ELSE IF( @MainTable = 'Destinationcities' )
                      BEGIN
                          SET @sql = ( 'Select ID,Name,Remarks ,(case when IsMetro=1 then''Metro'' else ''Non-Metro'' END) AS DRPName from tbl_'
                                       + @MainTable
                                       + ' Where IsDeleted = 0 and DomainID = '
                                       + CONVERT(VARCHAR(10), @domainID)
                                       + 'order by Name Asc' )
                      END
                    ELSE
                      BEGIN
                          SET @sql = ( 'Select ID,Name,Remarks ,null AS DRPName from tbl_'
                                       + @MainTable
                                       + ' Where IsDeleted = 0 and DomainID = '
                                       + CONVERT(VARCHAR(10), @domainID)
                                       + 'order by Name Asc' )
                      END
                END
              ELSE IF( @DependantTable = 'BusinessUnit' )
                SET @sql = ( 'Select A.ID,A.Name,A.Remarks,B.NAME AS DRPName from tbl_'
                             + @MainTable + ' AS A Join tbl_'
                             + @DependantTable
                             + ' B ON A.ParentID=B.ID Where A.IsDeleted = 0 and B.IsDeleted = 0 and A.DomainID = '
                             + CONVERT(VARCHAR(10), @domainID)
                             + 'ORDER BY A.Name ASC' )
              ELSE
                SET @sql = ( 'Select A.ID,A.Name,A.Remarks,B.NAME AS DRPName from tbl_'
                             + @MainTable + ' AS A Join tbl_'
                             + @DependantTable + ' B ON A.' + @DependantTable
                             + 'ID=B.ID Where A.IsDeleted = 0 and B.IsDeleted = 0 and A.DomainID = '
                             + CONVERT(VARCHAR(10), @domainID) )
          END

          EXEC(@sql)

      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
