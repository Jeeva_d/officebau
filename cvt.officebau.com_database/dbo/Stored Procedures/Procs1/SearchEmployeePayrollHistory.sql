﻿/****************************************************************************     
CREATED BY      : Naneeshwar.M    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>      
 [SearchEmployeePayrollHistory] 2   
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchEmployeePayrollHistory] (@EmployeeId INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN  
              DECLARE @Days INT=( Day(Dateadd(DD, -1, Dateadd(mm, Datediff(mm, 0, Getdate()), 0))) )  
  
     -- Get Payroll Component Config Value  
              SELECT *  
              INTO   #tmpParollConfigValue  
              FROM   [dbo].[Fn_PayrollComponentConfigValue]()  
  
              SELECT Isnull(EMP.EmpCodePattern, '') + EMP.Code                                                  AS EmployeeCode,  
                     EMP.FirstName + ' ' + Isnull(EMP.Lastname, '')            AS EmployeeName,  
                     EMP.DOJ                                                   AS DOJ,  
                     EPR.Id                                                    AS Id,  
                     EPR.EmployeeId                                            AS EmployeeId,  
                     EPR.MonthId                                               AS MonthId,  
                     MON.Code                                                  AS [Month],  
                     YearId                                                    AS YearId,  
                     Round((SELECT Gross  
                            FROM   tbl_EmployeePayStructure  
                            WHERE  EmployeeId = EPR.EmployeeId  
                                   AND IsDeleted = 0  
                                   AND ID = EPR.EmployeePayStructureID), 0)    AS FixedGross,  
                     NoofDaysPayable                                           AS [No of days payable],  
                     Basic                                                     AS Basic,  
                     HRA                                                       AS HRA,  
                     MedicalAllowance                                          AS MedicalAllowance,  
                     Conveyance                                                AS Conveyance,  
                     SplAllow                                                  AS SplAllow,  
                     OverTime                                                  AS OverTime,  
                     EducationAllow                                            AS EducationAllowance,  
                     MagazineAllow                                             AS PaperMagazine,  
                     MonsoonAllow                                              AS MonsoonAllow,  
                     OtherEarning                                              AS [Other Earning],  
                     Round(( Basic + HRA + MedicalAllowance + Conveyance  
                             + SplAllow + EducationAllow + MagazineAllow ), 0) AS [Paid Gross],  
                     EEPF                                                      AS EEPF,  
                     EEESI                                                     AS EEESI,  
                     TDS                                                       AS TDS,  
                     PT                                                        AS PT,  
                     Loans                                                     AS Loans,  
                     OtherDeduction                                            AS [Other Deductions],  
                     [NetSalary]                                               AS [NetPayable],  
                     ERPF                                                      AS ERPF,  
                     ERESI                                                     AS ERESI,  
                     Bonus                                                     AS Bonus,  
                     Gratuity                                                  AS Gratuity,  
                     PLI                                                       AS PLI,  
                     Mediclaim                                                 AS Mediclaim,  
                     MobileDataCard                                            AS MobileDataCard,  
                     OtherPerks                                                AS OtherPerks,  
                     CTC                                                       AS CTC,  
                     DM.NAME                                                   AS DepartmentName,  
                     BU.NAME                                                   AS BusinessUnit,  
                     Cast(ROW_NUMBER()  
                            OVER(  
                              ORDER BY EPR.MonthId DESC, YearId DESC)AS INT)   AS RowNo  
                     --------------  
                     ,  
                     IsEducationAllow,  
                     IsOverTime,  
                     IsEEPF,  
                     IsEEESI,  
                     IsPT,  
                     IsTDS,  
                     IsERPF,  
                     IsERESI,  
                     IsGratuity,  
                     IsPLI,  
                     IsMediclaim,  
                     IsMobileDatacard,  
                     IsOtherPerks  
              FROM   tbl_EmployeePayroll EPR  
                     JOIN tbl_EmployeeMaster EMP  
                       ON EPR.EmployeeId = EMP.ID  
                          AND EMP.IsDeleted = 0  
                     JOIN tbl_Month MON  
                       ON EPR.MonthId = MON.ID  
                     LEFT JOIN tbl_lopdetails P  
                            ON P.EmployeeId = EPR.EmployeeId  
                               AND P.Monthid = MON.ID  
                               AND p.Year = EPR.YearId  
                     LEFT JOIN tbl_BusinessUnit bu  
                            ON BU.ID = EPR.BaseLocationID  
                     LEFT JOIN tbl_Department DM  
                            ON DM.ID = EMP.DepartmentID  
                     LEFT JOIN #tmpParollConfigValue tpc  
                            ON tpc.BusinessUnitID = EPR.BaselocationID  
              WHERE  EPR.isdeleted = 0  
                     AND EPR.IsProcessed = 1  
                     AND EPR.EmployeeId = @EmployeeId  
              ORDER  BY EPR.YearId DESC,  
                        EPR.MonthId DESC  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
