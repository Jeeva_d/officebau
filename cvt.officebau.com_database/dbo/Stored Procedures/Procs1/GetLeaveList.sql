﻿/****************************************************************************   
CREATED BY   : DhanaLakshmi.S  
CREATED DATE  : 02 FEB 2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
     [GetLeaveList] 2019,2, 1,4  
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetLeaveList] (@Year       INT,
                                       @EmployeeID INT,
                                       @DomainID   INT,
                                       @UserID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT LT.NAME                                            AS LeaveType,
                 Cast(Round(EL.TotalLeave, 1, 1) AS DECIMAL(16, 1)) AS TotalLeave,
                 -- CAST(EL.TotalLeave as decimal(16,1))            AS TotalLeave,
                 EL.AvailedLeave                                    AS AvailedLeave,
                 ISNULL(CF.TotalLeave, 0)                           AS CarryForwardLeave
          FROM   tbl_EmployeeAvailedLeave EL
                 LEFT JOIN tbl_LeaveTypes LT
                        ON LT.ID = EL.LeaveTypeID
                 LEFT JOIN tbl_CarryForwardLeave CF
                        ON cf.IsDeleted = 0
                           AND cf.EmployeeID = EL.EmployeeID
                           AND cf.LeaveTypeID = EL.LeaveTypeID
                           AND cf.[Year] = EL.[Year]
          WHERE  el.IsDeleted = 0
                 AND EL.DomainID = @DomainID
                 AND EL.EmployeeID = @EmployeeID
                 AND EL.[Year] = @Year
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
