﻿  /****************************************************************************                           
CREATED BY      :                          
CREATED DATE  :                           
MODIFIED BY   :                         
MODIFIED DATE  :                        
<summary>                            
[Pay_GetEmployeePayroll_temp] 0,132,10,4,1      
[Pay_GetEmployeePayroll] 0,132,10,4,1                       
                       
</summary>                                                   
*****************************************************************************/              
CREATE PROCEDURE [dbo].[Pay_GetEmployeePayroll_temp] (@ID INT = 16              
, @EmployeePayStructureId INT = 61              
, @MonthId INT = 10              
, @Year INT = 4              
, @DomainID INT = 1)              
AS              
BEGIN              
 SET NOCOUNT ON;              
              
 BEGIN TRY              
  BEGIN              
       Declare @TDSYear INT                  
             DECLARE @Formula VARCHAR(MAX)                
          IF( @MonthID BETWEEN 1                  
          AND                  
          3 )                  
          SET @TDSYear=(                  
          (SELECT                  
               FY.NAME                  
          FROM                  
               tbl_FinancialYear FY                  
          WHERE                  
               FY.NAME =                  
               (SELECT                  
                    NAME                  
               FROM                  
                    tbl_FinancialYear                  
               WHERE                  
                    isdeleted = 0 AND id = @Year AND DomainID = @DomainID)                  
               AND FY.IsDeleted = 0 AND FY.DomainID = @DomainID)                  
          - 1 )                  
          ELSE                  
          SET @TDSYear =                  
          (SELECT                  
               FY.NAME                  
          FROM                  
               tbl_FinancialYear FY                  
          WHERE                  
               FY.NAME =                  
               (SELECT                  
                    NAME                  
               FROM                  
                    tbl_FinancialYear                  
               WHERE                  
                    isdeleted = 0 AND id = @Year AND DomainID = @DomainID)                  
               AND FY.IsDeleted = 0 AND FY.DomainID = @DomainID)         
   SET @Year = (SELECT              
     Name              
    FROM tbl_FinancialYear              
    WHERE Id = @Year              
    AND Isdeleted = 0              
    AND DomainId = @DomainID);              
   DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'              
   + CONVERT(VARCHAR(10), @Year);              
   DECLARE @Workdays INT = DATEDIFF(DAY, @CurrentMonth, DATEADD(MONTH, 1, @CurrentMonth));              
              
   IF @ID = 0        
   Begin      
   Declare @Payroll Payroll         
   INSERT INTO @Payroll         
    SELECT              
     0 AS Id          
   ,tppc.Id ComponentId             
       ,tpeps.Id AS EmployeePayStructureID      
     ,CASE              
      WHEN (tppc.Description <> 'Gross'  ) THEN        
   Case When (tppc.Description ='TAX') Then tds.TDSAmount Else        
    CAST(tpepsd.Amount AS DECIMAL(18, 2))              
    END            
      ELSE CAST(tpepsd.Amount AS DECIMAL(18, 2))              
     END Amount         
       ,tpeps.EmployeeId              
    FROM tbl_Pay_EmployeePayStructureDetails AS tpepsd              
    JOIN tbl_Pay_EmployeePayStructure AS tpeps              
     ON tpeps.Id = tpepsd.PayStructureId              
    JOIN tbl_Pay_PayrollCompontents AS tppc              
     ON tppc.Id = tpepsd.ComponentId              
   LEFT JOIN tbl_tds TDS             
                    ON TDS.employeeid = tpeps.employeeid             
    AND tds.monthid = @MonthId             
         AND tds.yearid = (SELECT id             
                                         FROM   tbl_financialyear             
                         WHERE  isdeleted = 0             
                                                AND NAME = @TDSYear             
                                                AND domainid =@DomainID)             
                       AND TDS.isdeleted = 0              
    WHERE tpeps.DomainId = @DomainID              
    AND tpepsd.PayStructureId = @EmployeePayStructureId              
    AND tpeps.Isdeleted = 0;              
      
   SELECT              
                    tppc.Id             ,              
                    tppc.Code           ,              
                    tppc.Description    ,              
                    tppc.Ordinal        ,              
                    tppc.Id ComponentId ,              
                    tppcps.value        ,              
                    CASE              
                         WHEN (tppc.Description = 'Gross')              
                              THEN CAST(tpepsd.Amount AS VARCHAR(MAX))              
                              ELSE              
                              CASE              
                                   WHEN (tppcps.IsEditable = 1)              
                                        THEN CAST(ISNULL(p.Amount, 0) AS VARCHAR(MAX))              
                                        ELSE ISNULL(tppcps.value, '')              
                              END              
                    END AS Amount ,0 AS HasUpdated ,              
                    tppcps.IsEditable    ,      
      tpeps.EmployeeID,      
      tpeps.ID AS  EmployeePayStructureID      
               INTO #tblcompanypay              
               FROM              
                    tbl_Pay_PayrollCompontents AS tppc              
               LEFT JOIN              
                    @Payroll p              
               ON              
                    tppc.Id = p.ComponentId              
               LEFT JOIN              
                    tbl_Pay_EmployeePayStructure AS tpeps              
               ON              
                    tpeps.Id = p.EmployeePayStructureID              
               LEFT JOIN              
                    tbl_Pay_PayrollCompanyPayStructure AS tppcps              
               ON              
                    tppc.Id                      = tppcps.ComponentId and tppcps.IsDeleted =0              
               AND  tppcps.CompanyPayStructureID = tpeps.CompanyPayStructureID              
               JOIN              
                    tbl_Pay_EmployeePayStructureDetails AS tpepsd              
               ON              
                    tpeps.Id           = tpepsd.PayStructureId              
               AND  tpepsd.ComponentId = tppcps.ComponentId  and tpepsd.Isdeleted=0            
               WHERE              
                    tppc.Isdeleted = 0              
               AND  tppc.DomainId  = @DomainID        
      
      
        DECLARE @IDs INT ,              
                    @count INT = (    SELECT              
             COUNT(1)              
                    FROM              
                         #tblcompanypay);              
                             
               ----------------------------------------              
               WHILE (@count <> 0)              
               BEGIN              
                    SET @IDs = (    SELECT              
                              TOP 1 Id              
                         FROM              
                              #tblcompanypay              
            WHERE              
                              hasupdated = 0              
                        and Amount NOT LIKE '%[a-zA-Z][a-zA-Z]%');              
                    -----------------------------------------  
					 IF @ID is null  
        Set @ID= (Select ID FROM   #tblcompanypay      
                               WHERE  hasupdated = 1 and amount like '%Case %')             
                    UPDATE              
                 #tblcompanypay              
                    SET  hasupdated = 1              
                    WHERE              
                         Id = @IDs              
                    ;              
                                  
                    UPDATE              
      #tblcompanypay              
                    SET  Amount = REPLACE(P.Amount, (    SELECT              
                                   Code              
                              FROM              
                                   #tblcompanypay              
                              WHERE              
                                   Id = @IDs) , (    SELECT              
                                   Amount              
                              FROM              
                                   #tblcompanypay              
                              WHERE              
                                   Id = @IDs) )              
                    FROM              
                         #tblcompanypay P              
                    ;              
                                  
                    -----------------------              
                                  
                    SET @count = @count - 1;              
               END;              
               SET @count = (    SELECT              
                         COUNT(1)              
                    FROM              
                         #tblcompanypay);              
               WHILE (@count <> 0)              
               BEGIN              
                    SET @IDs = (    SELECT              
                              TOP 1 Id              
                         FROM              
                              #tblcompanypay              
                         WHERE              
                              hasupdated = 1              
                         ORDER BY              
                              Ordinal);              
                    UPDATE              
                         #tblcompanypay              
                    SET  hasupdated = 0              
                    WHERE              
                         Id = @IDs              
                    ;              
                                  
                    SET @Formula = (    SELECT              
                              Amount              
                         FROM              
                              #tblcompanypay              
                         WHERE              
                              Id = @IDs);         
         DECLARE @LOP DECIMAL = (SELECT         
                     ( CASE          
                         WHEN ( Datepart(MM, DOJ) = @MonthId          
                                AND Datepart(YYYY, DOJ) = @Year ) THEN          
                           ( @Workdays - ( Datepart(D, DOJ) - 1 ) - Isnull(LopDays, 0) )          
                         ELSE          
                           @Workdays - Isnull(LopDays, 0)          
                       END ) - ( CASE          
                                   WHEN ( Datepart(MM, InactiveFrom) = @MonthId          
                                          AND Datepart(YYYY, InactiveFrom) = @Year ) THEN          
                                     ( ( @Workdays ) - Day(InactiveFrom) )          
                                   ELSE          
                                     0          
                                 END )                               
                       
              FROM   tbl_employeemaster ep          
                     LEFT JOIN tbl_LopDetails P          
                            ON P.EmployeeID = ep.ID          
                               AND P.DomainID = @DomainID          
                               AND P.MonthId = @MonthId          
                               AND P.year = @Year     where ep.ID=(Select top 1 employeeID from #tblcompanypay where Id=@IDs))           
      
                    DECLARE @Input VARCHAR(MAX) = '(' + REPLACE(@Formula, '.00', '') + ')';              
                    DECLARE @output VARCHAR(MAX) ,              
                         @resultdecimal NVARCHAR(MAX);              
                    DECLARE @i INT = 0;              
                    WHILE @i < LEN(@Input)              
                    BEGIN              
                         SET @i = @i + 1;              
             IF (dbo.isReallyNumeric(SUBSTRING(@Input, @i, 1)) IS NOT NULL)              
                         BEGIN              
                              SET @output = ISNULL(@output, '') + SUBSTRING(@Input, @i, 1);              
                         END;              
                         ELSE              
                         BEGIN              
                              SET @resultdecimal = ISNULL(@resultdecimal, '') + ISNULL(@output, '') +              
                              CASE              
                              WHEN (              
                                        ISNUMERIC(@output) = 1              
                                   )              
                                   THEN              
                                   '.00'              
               ELSE ''              
                              END              
                              + SUBSTRING(@Input, @i, 1);              
                              SET @output = '';              
                         END;              
                    END;              
                    SET @Formula = @resultdecimal;              
                    SET @resultdecimal = '';              
                    DECLARE @sql NVARCHAR(MAX);              
                    DECLARE @result DECIMAL(18, 2);              
              SET @sql = N'set @result = ' + @Formula;              
                    EXEC sp_executesql              
                         @sql ,              
                      N'@result DECIMAL output' ,              
                         @result OUT;              
                    UPDATE              
                         #tblcompanypay              
                    SET  Amount =              
                         CASE              
                              WHEN (#tblcompanypay.Description    = 'Gross'              
                                   OR   #tblcompanypay.IsEditable = 1)              
                                   THEN @result              
                                   ELSE @result * (@LOP / @Workdays)              
                         END              
                    WHERE              
                         Id = @IDs              
                    ;              
                                  
                    SET @count = @count - 1;              
               END;       
      
      Select * from #tblcompanypay      
 END      
   IF @ID != 0              
    SELECT              
tppd.ID AS Id              
       ,tpep.EmployeePayStructureID AS EmployeePayStructureID              
       ,tpep.EmployeeId              
       ,tppc.Id ComponentId              
       ,tppc.Description              
       ,Round(tppd.Amount,0) AS Amount              
       ,tppcps.IsEditable              
    FROM tbl_Pay_EmployeePayroll AS tpep              
    JOIN tbl_Pay_EmployeePayrollDetails AS tppd              
     ON tppd.PayrollId = tpep.Id              
    JOIN tbl_Pay_PayrollCompontents AS tppc              
     ON tppc.Id = tppd.ComponentId              
    JOIN tbl_Pay_PayrollCompanyPayStructure AS tppcps              
     ON tppcps.ComponentId = tppc.Id and    tppcps.isdeleted=0          
      AND tppcps.CompanyPayStructureID = (SELECT              
        tpeps.CompanyPayStructureID              
       FROM tbl_Pay_EmployeePayStructure AS tpeps              
       WHERE Id = tpep.EmployeePayStructureID)              
    WHERE tpep.Id = @ID              
              
  END              
 END TRY           
 BEGIN CATCH              
  DECLARE @ErrorMsg VARCHAR(100)              
      ,@ErrSeverity TINYINT              
  SELECT              
   @ErrorMsg = ERROR_MESSAGE()              
     ,@ErrSeverity = ERROR_SEVERITY()              
  RAISERROR (@ErrorMsg, @ErrSeverity, 1)              
 END CATCH              
END
