﻿/****************************************************************************   
CREATED BY		: JENNIFER.S
CREATED DATE	: 29-Jun-2017
MODIFIED BY		: 
MODIFIED DATE	: 
 <summary>
		[SearchHoliday] 0,2017,0,'Weekly',1,106
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchHoliday] (@MonthID  TINYINT,
                                       @Year     SMALLINT,
                                       @RegionID INT,
                                       @Type     VARCHAR(10),
                                       @DomainID INT,
                                       @UserID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          IF( Isnull(@RegionID, 0) = 0 )
            SET @RegionID = (SELECT RegionID
                             FROM   tbl_EmployeeMaster
                             WHERE  ID = @UserID)

          SELECT HOL.ID                                AS ID,
                 HOL.[Date]                            AS HolidayDate,
                 HOL.[Day]                             AS Holiday,
                 HOL.[Type]                            AS [Type],
                 HOL.[Description]                     AS [Description],
                 HOL.IsOptional                        AS IsOptional,
                 Substring((SELECT ', ' + NAME
                            FROM   tbl_BusinessUnit
                            WHERE  ID IN (SELECT item
                                          FROM   dbo.Splitstring(HOL.RegionID, ','))
                            FOR XML PATH('')), 2, 100) AS Region
          FROM   tbl_Holidays HOL
          WHERE  IsDeleted = 0
                 AND [Type] = @Type
                 AND DomainID = @DomainID
                 AND [Year] = @Year
                 AND ( ( Isnull(@MonthID, 0) <> 0
                         AND Month([Date]) = @MonthID )
                        OR Isnull(@MonthID, 0) = 0 )
                 AND ( Isnull(@RegionID, 0) = 0
                        OR ( Isnull(@RegionID, 0) <> 0
                             AND RegionID LIKE '%,' + CONVERT(VARCHAR(10), @RegionID) + ',%' ) )
          ORDER  BY HOL.[Date] ASC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
