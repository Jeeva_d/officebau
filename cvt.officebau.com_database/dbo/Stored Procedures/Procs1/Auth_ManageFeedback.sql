﻿/**************************************************************************** 
CREATED BY			:	Jeeva	
CREATED DATE		:	04-01-2016
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
     Mange User Feedback
	 [Auth_ManageFeedback] 'dddd','ddd','ddd1'
	 select * from Auth_Feedback
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_ManageFeedback]
 (
	@Name						VARCHAR(150)	,
	@Email						VARCHAR(150)	,
	@FeedbackMsg				VARCHAR(MAX)	
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY					
		SET NOCOUNT ON;
		DECLARE @Output						VARCHAR(100)				
		BEGIN TRANSACTION
		IF NOT EXISTS(SELECT 1 FROM Auth_Feedback WHERE Name=@Name AND Email=@Email AND FeedbackMsg = @FeedbackMsg AND IsDeleted = 0)
		BEGIN
			INSERT INTO Auth_Feedback 
			(
				Name				,
				Email				,
				FeedbackMsg				
			)
			VALUES
			(
				@Name			,
				@Email			,
				@FeedbackMsg		
			)
			SET @Output = 'OK'
		END
		ELSE
			SET @Output = 'Your message already recived!'

		SELECT @Output
		COMMIT TRANSACTION      
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
