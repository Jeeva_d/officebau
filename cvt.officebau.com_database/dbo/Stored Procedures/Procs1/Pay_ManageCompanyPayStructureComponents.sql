﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :           
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>                  
 </summary>                                   
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_ManageCompanyPayStructureComponents]  
(@ID                    INT,   
 @Formula               VARCHAR(500),   
 @ComponentId           INT,   
 @CompanyPayStructureId INT,   
 @SessionID             INT,   
 @IsDeleted             BIT,   
 @IsEditable            BIT,   
 @ShowinPaystructure    BIT,   
 @PayStub               BIT,   
 @Fixed                 BIT,   
 @DomainID              INT  
)  
AS  
    BEGIN  
        SET NOCOUNT ON;  
        DECLARE @Output VARCHAR(8000);  
        BEGIN TRY  
            BEGIN TRANSACTION;          
            --------------Check for Reference----------------------          
            --------------Delete --------------          
            IF @IsDeleted = 1  
                BEGIN  
                    UPDATE tbl_Pay_PayrollCompanyPayStructure  
                      SET   
                          IsDeleted = 1,   
                          ModifiedBy = @SessionID,   
                          ModifiedOn = GETDATE()  
                    WHERE ID = @ID;  
                    SET @Output =  
                    (  
                        SELECT [Message]  
                        FROM tblErrorMessage  
                        WHERE [Type] = 'Information'  
                              AND Code = 'RCD_DEL'  
                              AND IsDeleted = 0  
                    );  
                    UPDATE tbl_Pay_EmployeePayStructureDetails  
                      SET   
                          Isdeleted = 1  
                    WHERE PayStructureId IN  
                    (  
                        SELECT ID  
                        FROM tbl_Pay_EmployeePayStructure  
                        WHERE CompanyPayStructureID = @CompanyPayStructureId  
                              AND IsDeleted = 0  
                    )  
                          AND ComponentId = @ComponentId;  
                    GOTO FINISH;  
            END;          
            ---------------------------Update ---------------------------          
            IF @ID <> 0  
                BEGIN  
                    UPDATE tbl_Pay_PayrollCompanyPayStructure  
                      SET   
                          VALUE = @Formula,   
                          IsEditable = @IsEditable,   
                          ShowinPaystructure = @ShowinPaystructure,   
                          PayStub = @PayStub,   
                          Fixed = @Fixed,   
                          ModifiedBy = @SessionID,   
                          ModifiedOn = GETDATE()  
                    WHERE ID = @ID;  
                    SET @Output =  
                    (  
                        SELECT [Message]  
                        FROM tblErrorMessage  
                        WHERE [Type] = 'Information'  
                              AND Code = 'RCD_UPD'  
                              AND IsDeleted = 0  
                    );  
                    GOTO FINISH;  
            END;          
            ---------------------------Insert ----------------------------          
            IF @ID = 0  
                BEGIN  
                    INSERT INTO tbl_Pay_PayrollCompanyPayStructure  
                    (CompanyPayStructureID,   
                     ComponentID,   
                     Value,   
                     IsEditable,   
                     IsDeleted,   
                     DomainID,   
                     CreatedBy,   
                     CreatedOn,   
                     ModifiedBy,   
                     ModifiedOn,   
                     HistoryID,   
                     ShowinPaystructure,   
                     PayStub,   
                     Fixed  
                    )  
     SELECT @CompanyPayStructureId,   
                                  @ComponentId,   
                                  @Formula,   
                                  @IsEditable,   
                                  0,   
                                  @DomainID,   
                                  @SessionID,   
                                  GETDATE(),   
                                  @SessionID,   
                                  GETDATE(),   
                                  NEWID(),   
                                  @ShowinPaystructure,   
                                  @PayStub,   
                                  @Fixed;  
     Insert into tbl_Pay_EmployeePayStructureDetails (PayStructureId,ComponentId,Amount,CreatedBy,ModifiedBy)  
     Select ID,@ComponentId,0,@SessionID,@SessionID FROM tbl_Pay_EmployeePayStructure  
                        WHERE CompanyPayStructureID = @CompanyPayStructureId  
  
                    SET @Output =  
                    (  
                        SELECT [Message]  
                        FROM tblErrorMessage  
                        WHERE [Type] = 'Information'  
                              AND Code = 'RCD_INS'  
                              AND IsDeleted = 0  
                    );  
                    GOTO FINISH;  
            END;  
            FINISH:  
            SELECT @Output;  
            COMMIT TRANSACTION;  
        END TRY  
        BEGIN CATCH  
            ROLLBACK TRANSACTION;  
            DECLARE @ErrorMsg VARCHAR(100), @ErrSeverity TINYINT;  
            SELECT @ErrorMsg = ERROR_MESSAGE(),   
                   @ErrSeverity = ERROR_SEVERITY();  
            RAISERROR(@ErrorMsg, @ErrSeverity, 1);  
        END CATCH;  
    END;
