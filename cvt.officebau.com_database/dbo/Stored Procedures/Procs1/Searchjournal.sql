﻿/****************************************************************************             
CREATED BY   :             
CREATED DATE  :             
MODIFIED BY   :             
MODIFIED DATE  :             
 <summary>          
 [Searchjournal] 1       
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchjournal] (@DomainID INT, @ScreenType VARCHAR(10))
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION
		IF @ScreenType = 'S'
		BEGIN
			SELECT DISTINCT
				(SELECT TOP 1
						id
					FROM tblJournal
					WHERE j.JournalNo = JournalNo
					AND IsDeleted = 0
					AND DomainID = @DomainID)
				AS id
			   ,j.JournalNo AS JournalNo
			   ,(SELECT DISTINCT
						SUM(Debit)
					FROM tblJournal
					WHERE j.JournalNo = JournalNo
					AND IsDeleted = 0
					AND DomainID = @DomainID)
				AS Amount
			   ,j.JournalDate AS JournalDate
			   ,(SELECT
						Name
					FROM tblLedger
					WHERE id = (SELECT TOP 1
							LedgerProductID
						FROM tblJournal
						WHERE j.JournalNo = JournalNo
						AND IsDeleted = 0
						AND type = 'Ledger'
						AND DomainID = @DomainID))
				AS Ledger
			   ,(SELECT TOP 1
						CreatedOn
					FROM tblJournal
					WHERE j.JournalNo = JournalNo
					AND IsDeleted = 0
					AND DomainID = @DomainID)
				AS CreatedOn
			   ,j.Description
			FROM tblJournal j
			-- Join tblLedger l on j.LedgerProductID = l.ID      
			WHERE j.IsDeleted = 0
			AND j.DomainID = @DomainID
		END
		ELSE
		BEGIN
			SELECT
				REPLACE(CONVERT(VARCHAR(11), JournalDate, 106), ' ', '-') AS [Journal Date]
			   ,JournalNo AS [Journal No]
			   ,CASE
					WHEN (j.type = 'Ledger') THEN l.Name
					WHEN (j.type = 'Product') THEN p.Name
					WHEN (j.type = 'Vendor') THEN v.Name
					WHEN (j.type = 'Customer') THEN c.Name
				END
				Ledger
			   ,Debit
			   ,Credit
			FROM tblJournal j
			LEFT JOIN tblLedger l
				ON j.LedgerProductID = l.id
					AND type = 'Ledger'
			LEFT JOIN tblProduct_v2 p
				ON j.LedgerProductID = p.id
					AND type = 'Product'
			LEFT JOIN tblVendor v
				ON j.LedgerProductID = v.id
					AND type = 'Vendor'
			LEFT JOIN tblCustomer C
				ON j.LedgerProductID = C.id
					AND type = 'Customer'

					AND j.IsDeleted = 0
					WHERE j.IsDeleted = 0
			AND j.DomainID = @DomainID
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMsg VARCHAR(100)
			   ,@ErrSeverity TINYINT
		SELECT
			@ErrorMsg = ERROR_MESSAGE()
		   ,@ErrSeverity = ERROR_SEVERITY()
		RAISERROR (@ErrorMsg, @ErrSeverity, 1)
	END CATCH
END
