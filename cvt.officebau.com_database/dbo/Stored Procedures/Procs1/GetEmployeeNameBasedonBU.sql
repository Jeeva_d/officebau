﻿/****************************************************************************   
CREATED BY      : Dhanalakshmi. S  
CREATED DATE  : 29-DEC-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
<summary>    
 [GetEmployeeNameBasedonBU] 2, 1  
</summary>                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[GetEmployeeNameBasedonBU] (@EmployeeID INT,
                                                   @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN
              SELECT DISTINCT EM.ID,
                              ISNULL(EmpCodePattern, '') + Isnull(Code, '') + ' - ' + Isnull(FullName, '')
                              + '' AS NAME
              FROM   tbl_EmployeeMaster EM
                     JOIN (SELECT Item BusinessUnitID
                           FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                    FROM   tbl_EmployeeMaster
                                                    WHERE  ID = @EmployeeID
                                                           AND DomainID = @DomainID), ',')
                           WHERE  Isnull(Item, '') <> '') i
                       ON EM.BaseLocationID = i.BusinessUnitID
                     LEFT JOIN tbl_EmployeeOtherDetails B
                            ON B.EmployeeID = EM.ID
                     LEFT JOIN tbl_ApplicationRole C
                            ON EM.ID = B.ApplicationRoleID
              WHERE  EM.IsDeleted = 0
                     AND EM.DomainID = @DomainID
                     AND Isnull(EM.IsActive, 0) = 0
                     AND Isnull(B.ApplicationRoleID, 0) IN (SELECT ID
                                                            FROM   tbl_ApplicationRole
                                                            WHERE  NAME LIKE '%HR Manager%')
              ORDER  BY NAME
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
