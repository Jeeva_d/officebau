﻿  
/****************************************************************************     
CREATED BY   : Dhanalakshmi    
CREATED DATE  : 13/03/2017    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [GetCustomer] 3,1,1    
 select * from tblCustomer  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetCompany] (@ID       INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT com.ID              AS ID,  
                 com.NAME            AS NAME,  
     com.Code            AS Code,  
                 com.EmailID         AS EmailID,  
                 com.ContactNo       AS ContactNo,  
                 com.ContactPerson   AS ContactPerson,  
                 com.[Address]       AS [Address],  
     com.Logo            As Logo,  
                 com.PanNo           AS PanNo,  
                 com.TanNo           AS TanNo,  
                 com.CSTNo           AS CSTNo,
                 com.PaymentFavor,  
     com.ModifiedOn      AS ModifiedOn,  
                 EMP.FirstNAME       AS ModifiedBy  
          FROM   tblCompany com  
    LEFT JOIN tblEmployeeMaster EMP  
                        ON EMP.ID = com.ModifiedBy  
          WHERE  com.ID = @ID  
    and com.IsDeleted=0  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
