﻿
/****************************************************************************     
CREATED BY   : Naneeshwar    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
  Searchsourcescreen 10161,'Expense',1,1  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchsourcescreen] (@ID         INT,
                                            @SourceType VARCHAR(50),
                                            @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @SourceType = 'InvoiceReceivable' )
            BEGIN
                SELECT DISTINCT CustomerID AS ID
                FROM   tblInvoice
                WHERE  ID IN (SELECT InvoiceID
                              FROM   tblInvoicePaymentMapping
                              WHERE  InvoicePaymentID = @ID
                                     AND DomainID = @DomainID
                                     AND IsDeleted = 0)
                       AND DomainID = @DomainID
            END

          IF( @SourceType = 'Expense' )
            BEGIN
                SELECT DISTINCT ID AS ID
                FROM   tblExpense
                WHERE  ID IN (SELECT ExpenseID
                              FROM   tblExpensePaymentMapping
                              WHERE  ExpensePaymentID = @ID
                                     AND DomainID = @DomainID
                                     AND IsDeleted = 0)
                       AND DomainID = @DomainID
            END
          ELSE
            BEGIN
                SELECT DISTINCT VendorID AS ID
                FROM   tblExpense
                WHERE  ID IN (SELECT ExpenseID
                              FROM   tblExpensePaymentMapping
                              WHERE  ExpensePaymentID = @ID
                                     AND DomainID = @DomainID
                                     AND IsDeleted = 0)
                       AND DomainID = @DomainID
            END

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END


