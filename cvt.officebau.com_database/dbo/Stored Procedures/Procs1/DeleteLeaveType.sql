﻿/****************************************************************************   
CREATED BY		:   JENNIFER.S
CREATED DATE	:   06-Jun-2017
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary> 
			DeleteLeaveType 1, 9, 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteLeaveType](@ID       INT,
                                 @UserID   INT,
                                 @DomainID INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @Output     VARCHAR(50) = 'Record referred',
                  @TableQuery VARCHAR(MAX),
                  @count      INT
          DECLARE @Table TABLE
            (
               [RecordCount] INT
            )

          BEGIN TRANSACTION

          SET @TableQuery = (SELECT 'SELECT 1 FROM ' + TABLE_NAME
                                    + ' WHERE LeaveTypeID = '
                                    + CONVERT(VARCHAR(10), @ID)
                                    + ' and IsDeleted = 0  and DomainID='
                                    + CONVERT(VARCHAR(10), @DomainID) + '; '
                             FROM   INFORMATION_SCHEMA.COLUMNS
                             WHERE  COLUMN_NAME LIKE '%LeaveTypeID%'
                             FOR XML PATH(''))

          INSERT INTO @Table
          EXEC(@TableQuery)

          SET @count = (SELECT Count(1)
                        FROM   @Table)

          IF( @count = 0 )
            BEGIN
                UPDATE tbl_LeaveTypes
                SET    IsDeleted = 1,
                       ModifiedBy = @UserID,
                       ModifiedOn = Getdate()
                WHERE  id = @id

                SET @Output = 'Deleted Successfully'
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
