﻿ /****************************************************************************  
CREATED BY   :  
CREATED DATE  :  
MODIFIED BY   :  
MODIFIED DATE  :  
 <summary>  
 </summary>  
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_ManageCompanyPayStructure]  
(  
     @ID INT,  
     @Name VARCHAR(100),  
     @EffectiveFrom DATETIME,  
     @SessionID INT,  
     @IsDeleted BIT,  
     @DomainID INT  
)  
AS  
     BEGIN  
          SET NOCOUNT ON;  
            
          DECLARE @Output VARCHAR(8000)  
            
          BEGIN TRY  
               BEGIN TRANSACTION  
               ----- Check for reference ------------  
               IF EXISTS  
               (SELECT  
                    TOP 1 id  
               FROM  
                    tbl_pay_CompanyPayStructure  
               WHERE  
                    PaystructureName=@Name AND IsDeleted=0 AND ID <>@ID AND DomainID=@DomainID)  
               BEGIN  
                    SET @Output =  
                    (SELECT  
                         [Message]  
                    FROM  
                         tblErrorMessage  
                    WHERE  
                         [Type] = 'Warning' AND Code = 'RCD_EXIST' AND IsDeleted = 0)  
                    GOTO FINISH;  
               END  
                 
               IF EXISTS(SELECT   top 1 id FROM tbl_Pay_EmployeePayStructure AS tpeps  
                   Where  tpeps.IsDeleted=0 AND tpeps.CompanyPayStructureID =@ID  
                                       AND tpeps.DomainID=@DomainID )  
                  BEGIN  
                SET @Output = ( SELECT  
                    [Message]  
                   FROM tblErrorMessage  
                   WHERE [Type] = 'Warning'  
                   AND Code = 'RCD_REF'  
                   AND IsDeleted = 0)  
               GOTO FINISH;  
               END  
                 
                 
                 
               IF ((SELECT  Count(*) FROM tbl_Pay_PayrollCompanyPayStructure AS tppcps WHERE tppcps.IsDeleted=0 AND  
                       tppcps.CompanyPayStructureID =@ID AND tppcps.DomainID=@DomainID) >= 3 And @IsDeleted=1 ) 
                  BEGIN  
                SET @Output = ( SELECT  
                    [Message]  
                   FROM tblErrorMessage  
                   WHERE [Type] = 'Warning'  
                   AND Code = 'RCD_REF'  
                   AND IsDeleted = 0)  
               GOTO FINISH;  
               END  
                 
                 
               ------------------------Delete ---------------------------  
               IF @IsDeleted = 1  
               BEGIN  
                    UPDATE  
                         tbl_Pay_CompanyPayStructure  
                    SET  IsDeleted =1         ,  
                         ModifiedBy=@SessionID,  
                         ModifiedOn=GETDATE()  
                    WHERE  
                         ID=@ID  
                      
                    SET @Output =  
                    (SELECT  
                         [Message]  
                    FROM  
                         tblErrorMessage  
                    WHERE  
                         [Type] = 'Information' AND Code = 'RCD_DEL' AND IsDeleted = 0)  
                    GOTO FINISH;  
               END  
                 
               --------------Update ------------  
               IF @ID <>0  
               BEGIN  
                    UPDATE  
                         tbl_Pay_CompanyPayStructure  
                    SET  PaystructureName = @Name          ,  
                         EffectiveFrom    = @EffectiveFrom ,  
                         ModifiedBy       = @SessionID     ,  
                         ModifiedOn       = GETDATE()  
                    WHERE  
                         ID = @ID  
                      
                      
                    SET @Output =  
                    ( SELECT  
                         [Message]  
                    FROM  
                         tblErrorMessage  
                    WHERE  
      [Type] = 'Information' AND Code = 'RCD_UPD' AND IsDeleted = 0)  
                    GOTO FINISH;  
               END  
               ---------------------Insert -----------------  
               IF @ID =0  
               BEGIN  
                      
                    INSERT INTO tbl_Pay_CompanyPayStructure  
                         (  
                              PaystructureName,  
                              EffectiveFrom   ,  
                              CreatedBy       ,  
                              CreatedOn       ,  
                              ModifiedBy      ,  
                              ModifiedOn      ,  
                              HistoryID       ,  
                              DomainID  
                         )  
                    SELECT  
                         @Name         ,  
                         @EffectiveFrom,  
                         @SessionID    ,  
                         GETDATE()     ,  
                         @SessionID    ,  
                         GETDATE()     ,  
                         NEWID()       ,  
                         @DomainID  
                      
                    SET @Output =  
                    ( SELECT  
                         [Message]  
                    FROM  
                         tblErrorMessage  
                    WHERE  
                         [Type] = 'Information' AND Code = 'RCD_INS' AND IsDeleted = 0)  
                    GOTO FINISH;  
               END  
                 
               FINISH:  
               SELECT  
                    @Output COMMIT TRANSACTION  
          END TRY  
          BEGIN CATCH  
               ROLLBACK TRANSACTION  
               DECLARE @ErrorMsg VARCHAR(100),  
                    @ErrSeverity TINYINT  
               SELECT  
                    @ErrorMsg    = Error_message(),  
                    @ErrSeverity = Error_severity()  
               RAISERROR(@ErrorMsg,@ErrSeverity,1)  
          END CATCH  
     END
