﻿/****************************************************************************       
CREATED BY  : JENNIFER.S    
CREATED DATE : 01-Aug-2017    
MODIFIED BY  : JENNIFER.S    
MODIFIED DATE : 08-AUG-2017    
 <summary>    
  [ManageLoanForeclosure] 0, 3, 1000, 27, '2018-02-18',0,'',106,1    
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageLoanForeclosure] (@ID            INT,  
                                               @LoanID        INT,  
                                               @Amount        MONEY,  
                                               @PaymentModeID INT,  
                                               @PaymentDate   DATE,  
                                               @Interest      DECIMAL(5, 2),  
                                               @Description   VARCHAR(500),  
                                               @SessionID     INT,  
                                               @DomainID      INT,  
                                               @Reference     VARCHAR(50),  
                                               @EmployeeID    INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          DECLARE @DesignationMappingID INT,  
                  @ForecloseInterest    MONEY,  
                  @MonthCount           INT,  
                  @Ouput                VARCHAR(100)  
  
          SET @MonthCount= (SELECT Count(1)  
                            FROM   tbl_LoanAmortization AM  
                            WHERE  AM.LoanRequestID = @LoanID  
                                   AND AM.StatusID = (SELECT ID  
                                                      FROM   tbl_Status  
                                                      WHERE  [Type] = 'Amortization'  
                                                             AND Code = 'Open')  
                                   AND IsDeleted = 0)  
          SET @DesignationMappingID = (SELECT DM.ID  
                                       FROM   tbl_EmployeeDesignationMapping DM  
                                              LEFT JOIN tbl_EmployeeMaster Em  
                                                     ON EM.DesignationID = DM.ID  
                                       WHERE  Em.ID = @EmployeeID  
                                              AND Em.IsDeleted = 0)  
          SET @ForecloseInterest = (SELECT ForecloseInterestRate  
                                    FROM   tbl_LoanConfiguration  
                                    WHERE  DesignationMappingID = @DesignationMappingID  
                                           AND IsDeleted = 0)  
  
          IF(SELECT 1  
             FROM   tbl_LoanAmortization  
             WHERE  LoanRequestID = @LoanID  
                    AND StatusID = (SELECT ID  
                                    FROM   tbl_Status  
                                    WHERE  Code = 'Foreclosed'  
                                           AND [Type] = 'Amortization')) = 1  
            SET @Ouput = 'Already Foreclosed.'  
          ELSE  
            BEGIN  
                INSERT INTO tbl_LoanForeclosure  
                            (LoanRequestID,  
                             Amount,  
                             InterestRate,  
                             TotalAmount,  
                             PaymentModeID,  
                             PaymentDate,  
                             [Description],  
                             DomainID,  
                             CreatedBy,  
                             CreatedOn,  
                             ModifiedBy,  
                             ReferenceNo,  
                             ModifiedOn)  
                VALUES      (@LoanID,  
                             @Amount,  
                             Isnull(@Interest, 0),  
                             @Amount  
                             + Isnull(( @Amount * Isnull(@ForecloseInterest, 0) * @MonthCount) / (100 * 12), 0),  
                             @PaymentModeID,  
                             @PaymentDate,  
                             @Description,  
                             @DomainID,  
                             @SessionID,  
                             Getdate(),  
                             @SessionID,  
                             @Reference,  
                             Getdate())  
  
                UPDATE tbl_LoanAmortization  
                SET    IsDeleted = 1  
                WHERE  LoanRequestID = @LoanID  
                       AND StatusID = (SELECT ID  
                                       FROM   tbl_Status  
                                       WHERE  [Type] = 'Amortization'  
                                              AND Code = 'Open')  
  
                INSERT INTO tbl_LoanAmortization  
                            (LoanRequestID,  
                             Amount,  
                             InterestAmount,  
                             DueDate,  
                             StatusID,  
                             DomainID,  
                             CreatedBy,  
                             ModifiedBy,  
                             IsBalance,  
                             PaidAmount,  
                             DueBalance)  
                SELECT @LoanID,  
                       @Amount,  
                       Isnull(( @Amount * Isnull(@ForecloseInterest, 0) * @MonthCount ) / ( 100 * 12 ), 0),  
                       @PaymentDate,  
                       (SELECT ID  
                        FROM   tbl_Status  
                        WHERE  [Type] = 'Amortization'  
                               AND Code = 'Foreclosed'),  
                       @DomainID,  
                       @SessionID,  
                       @SessionID,  
                       0,  
                       ( Isnull(@Amount, 0) + Isnull(@Interest, 0) ),  
                       0  
  
                UPDATE tbl_LoanRequest  
                SET    StatusID = (SELECT ID  
                                   FROM   tbl_Status  
                                   WHERE  [Type] = 'Loan'  
                                          AND Code = 'Closed')  
                WHERE  ID = @LoanID  
  
                SET @Ouput = 'Loan Foreclosed Successfully.'  
            END  
  
          SELECT @Ouput  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
