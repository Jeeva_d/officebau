﻿/****************************************************************************   
CREATED BY   : K.SASIREKHA  
CREATED DATE  : 29-09-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
  [ManageCompOffApproval] 1005,1,71, '1/6/2018 12:00:00 AM','','fgdfgd',71,71,0,'Approved',71  
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageCompOffApproval] (@ID               INT,
                                               @DomainID         INT,
                                               @RequesterID      INT,
                                               @Date             DATETIME,
                                               @RequesterRemarks VARCHAR(8000),
                                               @ApproverRemarks  VARCHAR(8000),
                                               @HRApproverID     INT,
                                               @SessionID        INT,
                                               @IsDeleted        BIT,
                                               @Status           VARCHAR(80),
                                               @ModifiedBy       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output          VARCHAR(1000) = 'Operation failed!',
              @LeaveType       INT,
              @CompOffStatusID INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = @Status
                        AND Type = 'Leave'
                        AND IsDeleted = 0),
              @ApprovedStatus  VARCHAR(50) = (SELECT Code
                 FROM   tbl_Status
                 WHERE  Code = 'Approved'
                        AND Type = 'Leave'
                        AND IsDeleted = 0)

      BEGIN TRY
          BEGIN TRANSACTION

          IF( (SELECT Count(1)
               FROM   tbl_EmployeeCompOff
               WHERE  IsDeleted = 0
                      AND DomainID = @DomainID
                      AND ID = @ID
                      AND ApproverStatusID IN (SELECT ID
                                               FROM   tbl_Status
                                               WHERE  Code IN ( 'Approved', 'Pending' )
                                                      AND [Type] = 'Leave'
                                                      AND IsDeleted = 0)) <> 0 )
            BEGIN
                --UPDATE tbl_EmployeeCompOff
                --SET    StatusID = @CompOffStatusID,
                --       DomainID = @DomainID,
                --       ApproverID = @ApproverID,
                --       ApproverRemarks = @ApproverRemarks,
                --       ModifiedBy = @ModifiedBy,
                --       ApprovedDate = Getdate()
                --WHERE  ID = @ID
                IF( @Status = 'Approved' )
                  BEGIN
                      SET @Output='Approved Successfully./'
                                  + Cast(@ID AS VARCHAR)

                      UPDATE tbl_EmployeeCompOff
                      SET    ApproverRemarks = @ApproverRemarks,
                             ApprovedDate = Getdate(),
                             ApproverStatusID = @CompOffStatusID,
                             HRApproverID = @HRApproverID,
                             HRStatusID = (SELECT ID
                                           FROM   tbl_Status
                                           WHERE  Code = 'Pending'
                                                  AND Type = 'Leave'
                                                  AND IsDeleted = 0)
                      WHERE  ID = @ID
                  --SET @LeaveType=(SELECT ID
                  --                FROM   tbl_LeaveTypes
                  --                WHERE  code = 'COMP'
                  --                       AND IsDeleted = 0
                  --                       AND DomainID = @DomainID)
                  --IF NOT EXISTS(SELECT 1
                  --              FROM   tbl_EmployeeAvailedLeave
                  --              WHERE  EmployeeID = @RequesterID
                  --                     AND Year = Year(@Date)
                  --                     AND IsDeleted = 0
                  --                     AND DomainID = @DomainID
                  --                     AND LeaveTypeID = @LeaveType)
                  --  BEGIN
                  --      INSERT INTO tbl_EmployeeAvailedLeave
                  --                  (EmployeeID,
                  --                   LeaveTypeID,
                  --                   TotalLeave,
                  --                   AvailedLeave,
                  --                   Year,
                  --                   DomainID,
                  --                   CreatedBy,
                  --                   ModifiedBy)
                  --      VALUES      (@RequesterID,
                  --                   @LeaveType,
                  --                   1,
                  --                   0,
                  --                   Year(@Date),
                  --                   @DomainID,
                  --                   @SessionID,
                  --                   @SessionID)
                  --  END
                  --ELSE
                  --  BEGIN
                  --      UPDATE tbl_EmployeeAvailedLeave
                  --      SET    TotalLeave = EAL.TotalLeave + 1,
                  --             ModifiedBy = @SessionID
                  --      FROM   tbl_EmployeeAvailedLeave EAL
                  --      WHERE  EAL.LeaveTypeID = @LeaveType
                  --             AND EAL.Year = Year(@Date)
                  --             AND EAL.EmployeeID = @RequesterID
                  --  END
                  END
                ELSE IF( @Status = 'Rejected' )
                  BEGIN
                      SET @Output = 'Rejected Successfully./'
                                    + Cast(@ID AS VARCHAR)

                      UPDATE tbl_EmployeeCompOff
                      SET    ApproverRemarks = @ApproverRemarks,
                             ApprovedDate = Getdate(),
							 StatusID = @CompOffStatusID,
                             ApproverStatusID = @CompOffStatusID
                      WHERE  ID = @ID
                  END
            END
          ELSE
            BEGIN
                SET @Output = 'Cannot Approve/Reject'
            END

          SELECT @Output AS [output]

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
