﻿


  /****************************************************************************       
CREATED BY   :    Priya K  
CREATED DATE  :   13 Nov 2018    
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>     
         [Pay_RPT_Searchpayrollbymonthly] 11,4,1,'2',0   
             
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_Rpt_searchpayrollbymonthly] (@MonthID      INT,
                                                         @Year         INT,
                                                         @DomainID     INT,
                                                         @Location     VARCHAR(50),
                                                         @DepartmentID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @cols  AS NVARCHAR(MAX),
                  @query AS NVARCHAR(MAX)

          SET @Year = (SELECT NAME
                       FROM   tbl_FinancialYear
                       WHERE  id = @Year
                              AND IsDeleted = 0
                              AND DomainId = @DomainID)

          DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthID) + '-' + '1' + '-'
            + CONVERT(VARCHAR(10), @Year)
          DECLARE @DataSource TABLE
            (
               id      INT IDENTITY (1, 1),
               [value] NVARCHAR(128)
            )

          INSERT INTO @DataSource
                      ([value])
          SELECT Item
          FROM   dbo.SplitString(@Location, ',')
          WHERE  ISNULL(Item, '') <> ''

          SELECT *
          INTO   #Result
          FROM   VW_Payroll vwpay
          WHERE  vwpay.MonthId = @MonthID
                 AND vwpay.YearId = @Year
                 AND ( ( @DepartmentID IS NULL
                          OR Isnull(@DepartmentID, 0) = 0 )
                        OR vwpay.DepID = @DepartmentID )
                 AND ( ( @Location = Cast(0 AS VARCHAR) )
                        OR vwpay.BuID IN (SELECT [value]
                                          FROM   @DataSource) )
                 AND ( vwpay.BuID <> 0
                        OR vwpay.BuID <> '' )

          SET @cols = Stuff((SELECT ',' + Quotename(description)
                             FROM   VW_Payroll
							 where DomainId = @DomainID
                             GROUP  BY description,
                                       ComponentId
                             ORDER  BY ComponentId ASC
                             FOR XML path('')), 1, 1, ' ')
          SET @query = 'SELECT   
       Emp_Code, Employee_Name,            
     Joining_Date,  Name As Business_Unit,'
                       + @cols + '  
      from             (            
     select  
      Emp_Code, Description, Employee_Name, Joining_Date,               
     Name, Amount     
     from #Result  
       
     ) x            pivot            (sum(Amount)               
     for Description in (' + @cols
                       + ')            ) p '

          EXEC (@query);
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END

