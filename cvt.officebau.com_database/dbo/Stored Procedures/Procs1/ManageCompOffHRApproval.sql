﻿/****************************************************************************   
CREATED BY   : Ajith N
CREATED DATE  : 19 Sep 2018
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
  [ManageCompOffHRApproval] 1005,1,71, '1/6/2018 12:00:00 AM','','fgdfgd',71,71,0,'Approved',71  
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageCompOffHRApproval] (@ID               INT,
                                                 @DomainID         INT,
                                                 @RequesterID      INT,
                                                 @Date             DATETIME,
                                                 @RequesterRemarks VARCHAR(8000),
                                                 @HRRemarks        VARCHAR(8000),
                                                 @HRApproverID     INT,
                                                 @SessionID        INT,
                                                 @IsDeleted        BIT,
                                                 @Status           VARCHAR(80),
                                                 @ModifiedBy       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output           VARCHAR(1000) = 'Operation failed!',
              @LeaveType        INT,
              -- @CompOffConfigDuration TIME,
              @WorkingDuration  TIME,
              @BaseLocationID   INT,
              @RegionID         INT,
              @EligibleLeaveDay DECIMAL(8, 2),
              @CompOffStatusID  INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = @Status
                        AND Type = 'Leave'
                        AND IsDeleted = 0),
              @ApprovedStatus   VARCHAR(50) = (SELECT Code
                 FROM   tbl_Status
                 WHERE  Code = 'Approved'
                        AND Type = 'Leave'
                        AND IsDeleted = 0)

      SELECT @BaseLocationID = BaseLocationID,
             @RegionID = RegionID
      FROM   tbl_EmployeeMaster
      WHERE  ID = @RequesterID

      SET @WorkingDuration = (SELECT Duration
                              FROM   tbl_EmployeeCompOff co
                                     JOIN tbl_Attendance a
                                       ON a.IsDeleted = 0
                                          AND co.[Date] = a.LogDate
                                          AND co.CreatedBy = a.EmployeeID
                              WHERE  co.IsDeleted = 0
                                     AND co.DomainID = @DomainID
                                     AND co.CreatedBy = @RequesterID
                                     AND co.[Date] = @Date
                                     AND co.HRApproverID = @HRApproverID
                                     AND co.HRStatusID = (SELECT ID
                                                          FROM   tbl_Status
                                                          WHERE  Code = 'Pending'
                                                                 AND Type = 'Leave'
                                                                 AND IsDeleted = 0))

      BEGIN TRY
          BEGIN TRANSACTION

          IF( (SELECT Count(1)
               FROM   tbl_EmployeeCompOff
               WHERE  IsDeleted = 0
                      AND DomainID = @DomainID
                      AND ID = @ID
                      AND StatusID IN (SELECT ID
                                       FROM   tbl_Status
                                       WHERE  Code IN( 'Approved', 'Pending' )
                                              AND [Type] = 'Leave'
                                              AND IsDeleted = 0)) <> 0 )
            BEGIN
                UPDATE tbl_EmployeeCompOff
                SET    StatusID = @CompOffStatusID,
                       HRStatusID = @CompOffStatusID,
                       HRApproverRemarks = @HRRemarks,
                       HRApprovedDate = Getdate()
                WHERE  ID = @ID

                IF( @Status = 'Approved' )
                  BEGIN
                      SET @Output = 'Approved Successfully./'
                                    + Cast(@ID AS VARCHAR)
                      SET @LeaveType = (SELECT ID
                                        FROM   tbl_LeaveTypes
                                        WHERE  code = 'COMP'
                                               AND IsDeleted = 0
                                               AND DomainID = @DomainID)
                      SET @EligibleLeaveDay = (SELECT ( CASE
                                                          WHEN Replace(Cast(Cast(FullDay AS DECIMAL(8, 2)) AS VARCHAR), '.', ':') <= @WorkingDuration THEN
                                                            1
                                                          ELSE
                                                            ( Cast(Cast(@WorkingDuration AS VARCHAR(2)) AS INT) * ( 10.0 / FullDay ) ) / 10
                                                        END )
                                               FROM   tbl_BusinessHours
                                               WHERE  IsDeleted = 0
                                                      AND RegionID = @RegionID)

                      IF NOT EXISTS(SELECT 1
                                    FROM   tbl_EmployeeAvailedLeave
                                    WHERE  EmployeeID = @RequesterID
                                           AND Year = Year(@Date)
                                           AND IsDeleted = 0
                                           AND DomainID = @DomainID
                                           AND LeaveTypeID = @LeaveType)
                        BEGIN
                            INSERT INTO tbl_EmployeeAvailedLeave
                                        (EmployeeID,
                                         LeaveTypeID,
                                         TotalLeave,
                                         AvailedLeave,
                                         Year,
                                         DomainID,
                                         CreatedBy,
                                         ModifiedBy)
                            VALUES      (@RequesterID,
                                         @LeaveType,
                                         @EligibleLeaveDay,
                                         0,
                                         Year(@Date),
                                         @DomainID,
                                         @SessionID,
                                         @SessionID)
                        END
                      ELSE
                        BEGIN
                            UPDATE tbl_EmployeeAvailedLeave
                            SET    TotalLeave = EAL.TotalLeave + @EligibleLeaveDay,
                                   ModifiedBy = @SessionID
                            FROM   tbl_EmployeeAvailedLeave EAL
                            WHERE  EAL.IsDeleted = 0
                                   AND EAL.LeaveTypeID = @LeaveType
                                   AND EAL.Year = Year(@Date)
                                   AND EAL.EmployeeID = @RequesterID
                        END
                  END
                ELSE IF( @Status = 'Rejected' )
                  SET @Output = 'Rejected Successfully./'
                                + Cast(@ID AS VARCHAR)
            END
          ELSE
            BEGIN
                SET @Output = 'Cannot Approve/Reject'
            END

          SELECT @Output AS [output]

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
