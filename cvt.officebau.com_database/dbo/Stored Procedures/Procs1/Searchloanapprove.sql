﻿  
/****************************************************************************     
CREATED BY   :  Naneeshwar.M  
CREATED DATE  :     
MODIFIED BY   :   Ajith N  
MODIFIED DATE  :   18 Jan 2018  
 <summary>   
          [Searchloanapprove] 1,12,106  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchloanapprove] (@DomainID   INT,  
                                           @StatusID   INT,  
                                           @EmployeeID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN  
              SELECT LR.ID                                        AS ID,  
                     LR.CreatedOn                                 AS RequestedDate,  
                     LoanAmount                                   AS Amount,  
                     Isnull(EMP.EmpCodePattern, '') + Isnull(EMP.Code, '') + ' - ' + EMP.FullName AS Requester,  
                     LR.CreatedBy                                 AS RequesterID,  
                     LR.StatusID                                  AS StatusID,  
                     STA.Code                                     AS [Status],  
                     LR.LoanTypeID                                AS [Type],  
                     LR.TotalAmount                               AS TotalAmount  
              FROM   tbl_LoanRequest LR  
                     LEFT JOIN tbl_EmployeeMaster EMP  
                            ON EMP.ID = LR.CreatedBy  
                     LEFT JOIN tbl_Status STA  
                            ON STA.ID = LR.ApproverStatusID  
              WHERE  ApproverID = @EmployeeID  
                     AND LR.IsDeleted = 0  
                     AND LR.DomainID = @DomainID  
                     AND ( Isnull(@StatusID, 0) = 0  
                            OR LR.ApproverStatusID = @StatusID )  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
