﻿/****************************************************************************        
CREATED BY   : Priya K        
CREATED DATE  : 22-Oct-2018        
MODIFIED BY   :        
MODIFIED DATE  :        
<summary>        
  [Pay_Searchemployeepaystructure] 1,0,1,'4,11,2,'        
</summary>        
*****************************************************************************/    
CREATE PROCEDURE [dbo].[Pay_Searchemployeepaystructure] (@DomainID   INT,    
                                                        @EmployeeID INT,    
                                                        @IsActive   BIT,    
                                                        @Location   VARCHAR(100))    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          DECLARE @DataSource TABLE    
            (    
               [Value] NVARCHAR(128)    
            )    
    
          INSERT INTO @DataSource    
                      ([Value])    
          SELECT Item    
          FROM   dbo.Splitstring (@Location, ',')    
          WHERE  Isnull(Item, '') <> '';    
    
          WITH cte    
               AS (SELECT PEPS.ID                                                                     AS ID,    
                          PEPS.EmployeeID                                                             AS EmployeeID,    
                          Isnull(EMP.EmpCodePattern, '')    
                          + Isnull(EMP.Code, '') + ' - ' + EMP.FullName                               AS EmployeeName,    
                          PEPS.CompanyPayStructureID                                                  AS CompanyPayStucId,    
                          PCPS.PaystructureName                                                       AS CompanyPayStucName,    
                          PEPS.EffectiveFrom                                                          AS EffectiveFrom,    
                          EMP.Code                                                                    AS EmployeeCode,    
                          Isnull((SELECT Amount    
                                  FROM   tbl_Pay_EmployeePayStructureDetails    
                                  WHERE  PayStructureId = PEPS.ID    
                                         AND ComponentId = (SELECT ID    
                                                            FROM   tbl_Pay_PayrollCompontents    
                                                            WHERE  Description = 'Gross'    
                                                                   AND Isdeleted = 0    
                                                                   AND DomainId = PEPS.DomainID)), 0) AS Gross,    
                          Isnull((SELECT Amount    
                                  FROM   tbl_Pay_EmployeePayStructureDetails    
                                  WHERE  PayStructureId = PEPS.ID    
                                         AND ComponentId = (SELECT ID    
                                                            FROM   tbl_Pay_PayrollCompontents    
                                                            WHERE  Description = 'Basic'    
                                                                   AND Isdeleted = 0    
                                                                   AND DomainId = PEPS.DomainID)), 0) AS Basic,    
                          PEPS.ModifiedOn                                                             AS ModifiedOn,    
                          EMP.IsActive                                                                AS IsActive    
                   FROM   tbl_Pay_EmployeePayStructure PEPS    
                          LEFT JOIN tbl_Pay_CompanyPayStructure PCPS    
                                 ON PCPS.ID = PEPS.CompanyPayStructureID    
                          LEFT JOIN tbl_EmployeeMaster EMP    
                                 ON emp.ID = PEPS.EmployeeID    
                   WHERE  PEPS.DomainId = @DomainID    
                          AND PEPS.IsDeleted = 0    
                          AND emp.IsDeleted = 0    
                          AND ( @Location = Cast(0 AS VARCHAR)    
                                 OR @Location IS NULL )    
                           OR ( emp.BaseLocationID IN (SELECT [Value]    
                                                       FROM   @DataSource    
                                                       WHERE  PEPS.IsDeleted = 0) )    
                              AND ( EMP.ID = @EmployeeID    
                                     OR Isnull (@EmployeeID, 0) = 0 )    
                   GROUP  BY PEPS.ID,    
                             PEPS.EmployeeID,    
                             EMP.FullName,    
                             EMP.Code,    
                             PEPS.CompanyPayStructureID,    
                             PCPS.PaystructureName,    
                             PEPS.EffectiveFrom,    
                             EMP.BaseLocationID,    
                             PEPS.ModifiedOn,    
                             EMP.IsActive,    
                             PEPS.DomainID,    
                             EMP.EmpCodePattern)    
          SELECT ID,    
                 EmployeeID,    
                 EmployeeName,    
                 CompanyPayStucId,    
                 CompanyPayStucName,    
                 EffectiveFrom,    
                 EmployeeCode,    
                 Gross,    
                 BASIC,    
                 ModifiedOn,    
                 IsActive    
          INTO   #temp    
          FROM   cte    
          ORDER  BY ModifiedOn DESC    
    
          IF( @IsActive = 1    
              AND @EmployeeID = 0 )    
            BEGIN    
                SELECT *    
                FROM   #temp    
                WHERE  IsActive = 0    
            END    
          ELSE    
            BEGIN    
                SELECT *    
                FROM   #temp    
            END    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT;    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
