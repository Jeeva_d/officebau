﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	06 Dec 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
     [Checkduplicate_namewithdob] 0,'1','Punitharaj R','anbu.varadhan@foreseemax.com','1965-07-25',1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CheckDuplicate_NameWithDOB] (@ID           INT,
                                                    @EmployeeCode VARCHAR(50),
                                                    @Fullname     VARCHAR(100),
                                                    @EmailID      VARCHAR(100),
                                                    @DateofBirth  DATE,
                                                    @DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF ( @ID = 0 )
            BEGIN
                IF EXISTS(SELECT 1
                          FROM   tbl_EmployeeMaster em
                                 JOIN tbl_EmployeePersonalDetails pd
                                   ON em.ID = pd.EmployeeID
                          WHERE  em.FullName = @Fullname
                                 AND pd.DateOfBirth = @DateofBirth
                                 AND em.IsDeleted = 0
                                 AND Em.DomainID = @DomainID)
                  BEGIN
                      SELECT 1 AS Count
                  END
                ELSE
                  SELECT 0 AS Count
            END
          ELSE
            BEGIN
                IF EXISTS(SELECT 1
                          FROM   tbl_EmployeeMaster em
                                 JOIN tbl_EmployeePersonalDetails pd
                                   ON em.ID = pd.EmployeeID
                          WHERE  em.FullName = @Fullname
                                 AND pd.DateOfBirth = @DateofBirth
                                 AND em.IsDeleted = 0
                                 AND Em.DomainID = @DomainID
                                 AND EM.id <> @ID)
                  BEGIN
                      SELECT 1 AS Count
                  END
                ELSE
                  SELECT 0 AS Count
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
