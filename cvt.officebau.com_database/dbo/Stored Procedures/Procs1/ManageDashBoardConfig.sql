﻿/****************************************************************************       
CREATED BY   :       
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>       
 </summary>                               
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[ManageDashBoardConfig] (   
                                           @KEY       VARCHAR(100),      
                                           @Value    VARCHAR(1000),      
                                           @SessionID  INT,      
                                           @DomainID   INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      DECLARE @Output VARCHAR(1000)      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
    Update [tbl_DashBoardConfiguration] set value = @Value ,
	modifiedby=@SessionID,Modifiedon = getdate()
	 Where [Key] = @KEY And DomainID = @DomainID  
             
    Set @Output  = 'Updated Successfully.'  
          FINISH:      
      
          SELECT @Output      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
