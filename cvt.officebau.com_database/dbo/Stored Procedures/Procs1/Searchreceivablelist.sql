﻿ /****************************************************************************         
CREATED BY   : Naneeshwar        
CREATED DATE  :   22-Nov-2016      
MODIFIED BY   :     SIVA.B     
MODIFIED DATE  :     21-12-2016    
 <summary>      
 [Searchreceivablelist] 1,1,1,1      
 SearchReceivableList    
 </summary>                                 
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Searchreceivablelist] (@ID         INT,    
                                              @CustomerID INT,    
                                              @DomainID   INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SELECT i.ID                                    AS InvoiceID,    
                 (SELECT ID    
                  FROM   tblInvoicePaymentMapping    
                  WHERE  InvoicePaymentID = @ID    
                         AND InvoiceID = i.ID    
                         AND IsDeleted = 0)              AS InvoicePaymentID,    
                 i.InvoiceNo                             AS InvoiceNo,    
                 (SELECT Substring((SELECT + ',' + it.ItemDescription    
                                    FROM   tblInvoiceItem it    
                                    WHERE  it.IsDeleted = 0    
                                           AND i.ID = it.InvoiceID    
                                    FOR XML PATH('')), 2, 200000)) AS [Description],    
                 i.Date                                  AS InvoicedDate,    
                 i.DueDate                               AS DueDate,    
                 ( CASE    
                     WHEN ( i.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)+Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0)) - ( Isnull(( Sum(Qty * Rate) * i.DiscountPercentage ), 0) ), 0) )    
                                                              FROM   tblInvoiceItem    
                                                              WHERE  InvoiceID = i.ID    
                                                                     AND IsDeleted = 0 and DomainID =  @domainID)    
                     ELSE (SELECT ( Isnull(( Sum(Qty * Rate)+Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0)) - Isnull(i.DiscountValue, 0) ), 0) )    
                           FROM   tblInvoiceItem    
                           WHERE  InvoiceID = i.ID    
                                  AND IsDeleted = 0 and DomainID =  @domainID)    
                   END )                                 AS Amount,    
                 i.ReceivedAmount                        AS ReceivedAmount,    
                 (( CASE    
                      WHEN ( i.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)+Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0)) - ( Isnull(( Sum(Qty * Rate) * i.DiscountPercentage ), 0) ), 0) )    
                                                               FROM   tblInvoiceItem    
                                                               WHERE  InvoiceID = i.ID    
                                                                      AND IsDeleted = 0 and DomainID =  @domainID)    
                      ELSE (SELECT ( Isnull(( Sum(Qty * Rate) +Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0))- Isnull(i.DiscountValue, 0) ), 0) )    
                            FROM   tblInvoiceItem    
                            WHERE  InvoiceID = i.ID    
                                   AND IsDeleted = 0 and DomainID =  @domainID)    
                    END )) -( Isnull(i.ReceivedAmount, 0) +(Select ISNULL(Sum(Amount),0) from tblInvoiceHoldings where IsDeleted = 0  
                                                              AND InvoiceID = i.ID)) AS OutstandingAmount,    
                 CASE    
                   WHEN ( Isnull(@ID, 0) = 0 ) THEN 0    
                   ELSE (SELECT Amount    
                         FROM   tblInvoicePaymentMapping    
                         WHERE  InvoicePaymentID = @ID    
                                AND InvoiceID = i.ID    
                                AND IsDeleted = 0 and DomainID =  @domainID)   
                 END                                     AS updateAmount ,  
     (Select Top 1 p.Name FROM   tblInvoiceItem  it  
     Join tblProduct_v2 p on p.ID=it.ProductID  
                            WHERE  InvoiceID = i.ID    
                                   AND it.IsDeleted = 0 and it.DomainID =  @domainID)    AS ProductName ,
		(Select ISNULL(Sum(Amount),0) from tblInvoiceHoldings where IsDeleted = 0  
                                                              AND InvoiceID = i.ID) As HoldingAmount 
															  ,
	   (Select Count(*) from tblInvoiceHoldings where IsDeleted = 0  
                                                              AND InvoiceID = i.ID) As HoldingsCount 
          FROM   tblInvoice i       
          WHERE  i.IsDeleted = 0    
                 AND @CustomerID = i.CustomerID    
                 AND type = 'Invoice'    
                 AND ((ISNULL(@ID,0) = 0 AND i.StatusID IN (SELECT ID FROM   tbl_CodeMaster WHERE  [Type] NOT IN( 'Close' )))     
        OR (ISNULL(@ID,0) != 0)) and i.DomainID =  @domainID    
                     
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
              @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
  RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
