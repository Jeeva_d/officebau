﻿

/****************************************************************************   
CREATED BY   : Ajith N  
CREATED DATE  : 22 Jan 2018  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
 [GetAvailableLeaveList] 8,4,1,1 
 </summary>                            
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetAvailableLeaveList] (@EmployeeCodes  VARCHAR(500),
                                               @Year           INT,
                                               @BusinessUnitID INT,
                                               @DomainID       INT,
                                               @UserID         INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @YearCode       INT,
                  @Query          VARCHAR(max) = '',
                  @LeaveHeader    VARCHAR(500) = '',
                  @TotalLeaves    VARCHAR(500) = '',
                  @LeaveType      VARCHAR(500) = '',
                  @EmpCodePattern VARCHAR(500) = (SELECT Isnull(EmpCodePattern, '')
                     FROM   tbl_Company
                     WHERE  IsDeleted = 0
                            AND ID = @DomainID)

          SET @YearCode = (SELECT Cast(NAME AS INT)
                           FROM   tbl_FinancialYear
                           WHERE  ID = @Year
                                  AND IsDeleted = 0
                                  AND DomainID = @DomainID)
          SET @LeaveHeader = Substring((SELECT ',SUM(ISNULL([' + Code + '],0)) AS [' + Code + ']'
                                        FROM   tbl_LeaveTypes
                                        WHERE  IsDeleted = 0
                                               AND NAME NOT IN ( 'Permission', 'On Duty' )
                                               AND DomainID = @DomainID
                                        FOR xml path('')), 2, 2000)
          SET @TotalLeaves = Substring((SELECT '+ISNULL([' + Code + '], 0)'
                                        FROM   tbl_LeaveTypes
                                        WHERE  IsDeleted = 0
                                               AND NAME NOT IN ( 'Permission', 'On Duty' )
                                               AND DomainID = @DomainID
                                        FOR xml path('')), 2, 2000)
          SET @LeaveType = Substring((SELECT ',[' + Code + ']'
                                      FROM   tbl_LeaveTypes
                                      WHERE  IsDeleted = 0
                                             AND NAME NOT IN ( 'Permission', 'On Duty' )
                                             AND DomainID = @DomainID
                                      FOR xml path('')), 2, 2000)

          CREATE TABLE #EmployeeList
            (
               EmpCode VARCHAR(500)
            )

          INSERT INTO #EmployeeList
                      (EmpCode)
          SELECT DISTINCT Replace(Replace(Substring(Item, 0, Charindex('-', Item, 0)), @EmpCodePattern, ''), ' ', '') --COLLATE SQL_Latin1_General_CP1_CI_AS
          FROM   dbo.Splitstring (@EmployeeCodes, ',')
          WHERE  Isnull(Item, '') <> ''

          SET @Query = 'SELECT EmployeeCode AS [Employee Code],  
         NAME,  
         Year,          
         ' + @LeaveHeader + ', sum('
                       + @TotalLeaves
                       + ') AS [Total],   
        sum(AvailedLeave) AS [Availed], (sum('
                       + @TotalLeaves
                       + ') - SUM(AvailedLeave)) AS [Balance]  
     FROM   (SELECT CASE
                     WHEN EM.Code LIKE ''TMP_%'' THEN EM.Code
                     ELSE Isnull(EM.EmpCodePattern, '''') + EM.Code
                   END       AS employeecode, 
           em.FullName  AS NAME,  
           lt.code       AS LeaveType,  
           ISNULL(al.TotalLeave, 0) AS TotalLeave,  
           ISNULL(al.AvailedLeave, 0) AS AvailedLeave,  
           Year,  
           al.DomainID AS DomainID,  
           EM.BaseLocationID AS BaseLocationID,  
           ROW_NUMBER() over(order by (select 1)) as RowNo  
       FROM   tbl_EmployeeAvailedLeave al  
           LEFT JOIN tbl_LeaveTypes LT  
            ON lt.ID = al.LeaveTypeID  
           JOIN tbl_EmployeeMaster EM  
            ON EM.ID = al.EmployeeID AND EM.IsDeleted = 0
           AND (EM.IsActive = 0 OR (EM.IsActive = 1 AND YEAR(EM.InactiveFrom) = '
                       + Cast(@YearCode AS VARCHAR)
                       + '))
					   WHERE (Isnull('''
                       + Isnull(@EmployeeCodes, '')
                       + ''', '''') =  ''''
                       OR EM.Code IN (SELECT EmpCode COLLATE Latin1_General_CI_AI
                                       FROM   #EmployeeList) )) AS p  
         PIVOT(Max(TotalLeave)  
        FOR LeaveType IN (' + @LeaveType
                       + ')) AS PivotTable  
     WHERE  Year = '
                       + Cast(@YearCode AS VARCHAR)
                       + ' AND DomainID = '
                       + Cast(@DomainID AS VARCHAR)
                       + ' AND BaseLocationID = '
                       + Cast(@BusinessUnitID AS VARCHAR)
                       + '  
     group by employeecode,  
      NAME,  
      Year'

          PRINT @Query

          EXEC(@Query)
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
