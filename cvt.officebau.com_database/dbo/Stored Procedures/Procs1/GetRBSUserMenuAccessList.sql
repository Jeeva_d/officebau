﻿/****************************************************************************     
CREATED BY  : Ajith N  
CREATED DATE : 29 Jun 2018 
MODIFIED BY  : 
MODIFIED DATE : 
 <summary>            
		GetRBSUserMenuAccessList '','','Account Payable ',1
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetRBSUserMenuAccessList] (@Module        VARCHAR(100),
                                                  @SubModule     VARCHAR(100),
                                                  @SubModuleMenu VARCHAR(100),
                                                  @DomainID      INT)
AS
  BEGIN
      BEGIN TRY
          SELECT DISTINCT R.Module      AS Module,
                          RSM.SubModule AS SubModule,
                          RM.Menu       AS Menu,
                          EM.Code +' - ' + EM.FullName   AS EmployeeName,
                          RUM.MRead     AS MenuRead,
                          RUM.MWrite    AS MenuWrite,
                          RUM.MEdit     AS MenuEdit,
                          RUM.MDelete   AS MenuDelete
          FROM   tbl_RBSUserMenuMapping RUM
                 LEFT JOIN tbl_EmployeeMaster EM
                        ON EM.ID = RUM.EmployeeID
                 LEFT JOIN tbl_RBSMenu RM
                        ON RM.ID = RUM.MenuID And RM.ISdeleted=0
                 LEFT JOIN tbl_RBSSubModule RSM
                        ON RSM.ID = RM.SubModuleID
                 JOIN tbl_RBSModule R
                   ON R.ID = RSM.ModuleID
          WHERE  RM.Menu IS NOT NULL
                 AND EM.FullName IS NOT NULL
				 AND EM.Code IS NOT NULL
                 AND ( RUM.DomainID = @DomainID
                       AND ( RM.Menu = 'true'
                              OR RUM.MRead = 'true'
                              OR RUM.MWrite = 'true'
                              OR RUM.MEdit = 'true'
                              OR RUM.MDelete = 'true' ) )
                 AND ( Isnull(@Module, '') = ''
                        OR R.Module = @Module )
                 AND ( Isnull(@SubModule, '') = ''
                        OR RSM.SubModule = @SubModule )
                 AND ( Isnull(@SubModuleMenu, '') = ''
                        OR RM.Menu = @SubModuleMenu )
          ORDER  BY RM.Menu
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
