﻿/****************************************************************************             
CREATED BY  :    
CREATED DATE :             
MODIFIED BY   :            
MODIFIED DATE  :         
<summary>          
   [FindusedFile] 'D:\sdhana\chntfs.visualstudio.com\fsm.officebau.erp\Code\fsm.officeBAU.com_Ingrated\FSM.OfficeBAU.com\Images\FSM\COMPANYLOGOS\1_636721756292934172_DEsk1eGXcAAEleq.jpg'  
</summary>                                     
*****************************************************************************/

CREATE PROCEDURE [dbo].[FindUsedFile]
AS
     BEGIN
         SET NOCOUNT ON;
         BEGIN TRY
             EXEC [DeleteUnUsedFilesFromFileUploadTable];
             SELECT CAST(path + FileName AS VARCHAR(MAX)) AS filePath
             FROM tbl_FileUpload
             UNION ALL
             SELECT CAST(CAST(tbl_EmailProcessor.AttachmentPath AS VARCHAR(MAX)) + CAST(Attachment AS VARCHAR(MAX)) AS VARCHAR(MAX))
             FROM tbl_EmailProcessor
             WHERE AttachmentPath IS NOT NULL
                   AND CAST(AttachmentPath AS VARCHAR(MAX)) <> ''
             UNION ALL
             SELECT CAST(path + FileName AS VARCHAR(MAX)) AS filePath
             FROM tbl_MultipleFileUpload AS tmfu;
         END TRY
         BEGIN CATCH
             DECLARE @ErrorMsg VARCHAR(100), @ErrSeverity TINYINT;
             SELECT @ErrorMsg = ERROR_MESSAGE(), 
                    @ErrSeverity = ERROR_SEVERITY();
             RAISERROR(@ErrorMsg, @ErrSeverity, 1);
         END CATCH;
     END;