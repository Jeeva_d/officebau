﻿
/****************************************************************************         
CREATED BY   :  Naneeshwar.M      
CREATED DATE  :   20-SEP-2017      
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>       
                
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[GetEmailList]      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT email.ID             AS ID,      
                 email.ToID           AS ToID,      
                 email.CC             AS CC,      
                 email.BCC            AS BCC,      
                 email.FromID         AS FromID,      
                 email.Subject        AS Subject,      
                 email.Content        AS Content,      
                 email.AttachmentPath AS AttachmentPath,      
                 email.Attachment     AS Attachment ,    
					FL.FileContent    AS FileContent,  
					FL.FileName     AS FileName,  
					FL.FileType     AS FileType    
          FROM   tbl_EmailProcessor email     
         LEFT join tbl_FileUpload FL on CONVERT(VARCHAR(100),FL.Id) = CONVERT(VARCHAR(100),email.Attachment)    
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
