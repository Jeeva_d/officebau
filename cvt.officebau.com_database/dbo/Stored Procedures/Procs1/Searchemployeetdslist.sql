﻿/****************************************************************************         
CREATED BY   : Dhanalakshmi. S        
CREATED DATE  : 19/08/2017        
MODIFIED BY   :         
MODIFIED DATE  :        
 <summary>             
  [SearchEmployeeTDSList] 0,2,4,'',1        
 </summary>                                 
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[Searchemployeetdslist] (@ID          INT,        
                                               @EmployeeID  INT,        
                                               @StartYearID INT,        
                                               @Type        VARCHAR(20),        
                                               @DomainID    INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      DECLARE @StartYear INT =(SELECT FinancialYear        
        FROM   tbl_FinancialYear        
        WHERE  id = @StartYearID        
               AND IsDeleted = 0        
               AND DomainID = @DomainID)        
        
      BEGIN TRY        
          SET @StartYear = CASE        
                             WHEN Isnull(@StartYear, 0) != 0 THEN        
                               @StartYear        
                             ELSE        
                               CASE        
                                 WHEN Month(Getdate()) < 4 THEN        
                                   Year(Getdate()) - 1        
                                 ELSE        
                                   Year(Getdate())        
                               END        
                           END;        
          WITH CTE        
               AS (SELECT t.MonthId                                                                                        AS MonthID,        
                          s.CODE + ' - ' + CONVERT(VARCHAR(10), CASE WHEN s.ID <4 THEN @StartYear + 1 ELSE @StartYear END) MonthYear,        
                          s.Code                                                                                           AS [Month],        
                          @EmployeeID                                                                                      AS EmployeeID,        
                          Isnull(t.TDSAmount, 0)                                                                           AS TDSAmount,        
                          (Select Amount from tbl_pay_employeepayrolldetails where payrollid = ep.ID and ComponentId = (select ComponentId from tbl_TDSComponentMapping      
     Where TdsKey='CTC' And DomainID=@DomainID))                                                                                 AS Gross,        
                        Cast(0 As decimal)                                                                                         AS PT,        
                         (Select Amount from tbl_pay_employeepayrolldetails where payrollid = ep.ID and ComponentId = (select ComponentId from tbl_TDSComponentMapping      
     Where TdsKey='CTC' And DomainID=@DomainID))                                                                                    AS NetPay   ,
	 (Select description from tbl_Pay_PayrollCompontents where ID =  (select ComponentId from tbl_TDSComponentMapping      
     Where TdsKey='CTC' And DomainID=@DomainID)) As Header     
                   FROM   tbl_Month s        
                          LEFT JOIN tbl_TDS t        
                                 ON t.MonthId = s.ID        
                                    AND t.YearID = @StartYearID        
                                    AND t.EmployeeId = @EmployeeID        
                                    AND t.IsDeleted = 0        
                          LEFT JOIN tbl_pay_employeepayroll ep        
                                 ON ep.EmployeeId = @EmployeeID        
                                    AND ep.IsDeleted = 0        
                                    AND IsProcessed = 1        
                                    AND ep.MonthId = s.ID        
                                    AND ep.YearID = (SELECT NAME        
    FROM   tbl_FinancialYear        
                                                     WHERE  id = @StartYearID        
                                                           AND IsDeleted = 0        
                         AND DomainID = @DomainID)        
                   WHERE  s.ID > 3        
                          AND Isnull(t.DomainID, @DomainID) = @DomainID        
                   UNION ALL        
                   SELECT t.MonthId                                                                                        AS MonthID,        
                          s.CODE + ' - ' + CONVERT(VARCHAR(10), CASE WHEN s.ID <4 THEN @StartYear + 1 ELSE @StartYear END) MonthYear,        
                          s.Code                                                                                           AS [Month],        
                          @EmployeeID                                                                                      AS EmployeeID,        
                          Isnull(t.TDSAmount, 0)                                                                           AS TDSAmount,        
                         (Select Amount from tbl_pay_employeepayrolldetails where payrollid = ep.ID and ComponentId = (select ComponentId from tbl_TDSComponentMapping      
     Where TdsKey='CTC' And DomainID=@DomainID))                                                                                 AS Gross,        
                        Cast(0 As decimal)                                                                                         AS PT,        
                         (Select Amount from tbl_pay_employeepayrolldetails where payrollid = ep.ID and ComponentId = (select ComponentId from tbl_TDSComponentMapping      
     Where TdsKey='CTC' And DomainID=@DomainID))       ,
	 (Select description from tbl_Pay_PayrollCompontents where ID =  (select ComponentId from tbl_TDSComponentMapping      
     Where TdsKey='CTC' And DomainID=@DomainID)) As Header
       FROM   tbl_Month s        
                          LEFT JOIN tbl_TDS t        
                                 ON t.MonthId = s.ID        
                                    AND t.YearID = @StartYearID        
                                    AND t.EmployeeId = @EmployeeID        
                                    AND t.IsDeleted = 0        
                          LEFT JOIN tbl_pay_employeepayroll ep        
                                 ON ep.EmployeeId = @EmployeeID        
                                    AND ep.IsDeleted = 0        
                                    AND IsProcessed = 1        
                                    AND ep.MonthId = s.ID        
                                    AND ep.YearID = (SELECT NAME        
                                                     FROM   tbl_FinancialYear        
                                                     WHERE  NAME = @StartYear + 1        
                                                            AND IsDeleted = 0        
                                                            AND DomainID = @DomainID)        
                   WHERE  s.ID < 4        
                          AND Isnull(t.DomainID, @DomainID) = @DomainID)        
          SELECT *        
          INTO   #CTE        
          FROM   cte        
            
          IF ( @Type = 'REPORT' )        
            BEGIN        
                SELECT MonthID,        
                       MonthYear,        
                       Month,        
                       EmployeeID,        
                       TDSAmount,        
                       Gross,        
                       PT,        
                       NetPay,        
                       0 AS [Count]   ,
					   Header     
                FROM   #CTE        
            END        
          ELSE        
            BEGIN        
                SELECT MonthID,        
                       MonthYear,        
                       Month,        
                       EmployeeID,        
                       TDSAmount,        
                       Gross,        
                       PT,        
                       NetPay,        
        0 AS [Count]      ,Header  
                FROM   #CTE        
                WHERE  MonthID IS NOT NULL        
                       AND Gross IS NOT NULL       
            END        
      END TRY        
      BEGIN CATCH        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT;        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
