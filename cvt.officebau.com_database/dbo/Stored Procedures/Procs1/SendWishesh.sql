﻿/****************************************************************************   
CREATED BY		: 
CREATED DATE	: 
MODIFIED BY		: 
MODIFIED DATE	: 
 <summary>
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SendWishesh] (@ID         INT,
                                     @FromID     INT,
                                     @TOID       INT,
                                     @Oldmessage VARCHAR(500),
                                     @message    VARCHAR(500),
                                     @Type       VARCHAR(10))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Ouput VARCHAR(100) = 'Message sent Successfully'

          IF ( @ID = 0 )
            BEGIN
                INSERT INTO tbl_Notifications
                            (FromID,
                             FromName,
                             ToID,
                             Message,
                             OldMessage,
                             ReceiverIsViewed,
                             SenderIsViewed,
                             IsViewed,
                             WishType,
                             ImagePath,
                             CreatedBy,
                             ModifiedBy)
                VALUES      (@FromID,
                             (SELECT FullName + '-' + Code
                              FROM   tbl_EmployeeMaster
                              WHERE  ID = @FromID),
                             @TOID,
                             @message,
                             @Oldmessage,
                             0,
                             0,
                             0,
                             @Type,
                             (SELECT Path
                              FROM   tbl_FileUpload
                              WHERE  id = (SELECT FileID
                                           FROM   tbl_EmployeeMaster
                                           WHERE  ID = @ID)),
                             @TOID,
                             @FromID)
            END
          ELSE
            BEGIN
                UPDATE tbl_Notifications
                SET    OldMessage = @Oldmessage,
                       ReceiverIsViewed = 1,
                       ModifiedOn = Getdate(),
                       ModifiedBy = @TOID
                WHERE  id = @ID
            END

          SELECT @Ouput

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
 
/****** Object:  StoredProcedure [dbo].[TDS_HouseTaxDETAILSENGINE]    Script Date: 16-Feb-18 6:17:10 PM ******/
SET ANSI_NULLS ON
