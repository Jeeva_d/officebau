﻿
/**************************************************************************** 
CREATED BY			:	Ajith N
CREATED DATE		:	23-Jun-2017
MODIFIED BY			:	Ajith N
MODIFIED DATE		:	08 Jan 2018
 <summary>        
    ProcessLOPDetails 9, 3, null , 1, 224
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ProcessLOPDetails](@MonthID      INT,
                                          @Year         INT,
                                          @BusinessUnit VARCHAR(50),
                                          @DomainID     INT,
                                          @EmployeeID   INT)
AS
  BEGIN
      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Output          VARCHAR(50) = 'Already Exists',
                  @StatusID        INT = (SELECT ID
                     FROM   tbl_Status
                     WHERE  IsDeleted = 0
                            AND Code = 'Approved'
                            AND [Type] = 'Leave'),
                  @LeaveTypeIDs    VARCHAR(50) = Isnull((SELECT ID
                            FROM   tbl_LeaveTypes
                            WHERE  IsDeleted = 0
                                   AND DomainID = @DomainID
                                   AND NAME IN ( 'On Duty' )), 0),
                  @Salarydate      DATE,
                  @SalaryStartDate VARCHAR(20) = 1,
                  @SalaryMonth     VARCHAR(10),
                  @SalaryYear      VARCHAR(10),
                  @SalaryDayCount  INT,
                  @day             DATE,
                  @Allday          VARCHAR(max) = '',
                  @Count           INT = 1,
                  @Query           VARCHAR(max) = '',
                  @BusinessUnitIDs VARCHAR(20)

          IF( Isnull(@BusinessUnit, '') = '' )
            SET @BusinessUnitIDs = (SELECT BusinessUnitID
                                    FROM   tbl_EmployeeMaster
                                    WHERE  Isnull(IsDeleted, 0) = 0
                                           AND Isnull(IsActive, 0) = 0
                                           AND ID = @EmployeeID
                                           AND DomainID = @DomainID)
          ELSE
            SET @BusinessUnitIDs = Cast(@BusinessUnit AS VARCHAR(50))

          -- Set Salary Start Date
          SET @SalaryMonth = (SELECT Code
                              FROM   tbl_Month
                              WHERE  ID = @MonthID
                                     AND IsDeleted = 0)
          SET @SalaryYear = (SELECT FinancialYear
                             FROM   tbl_FinancialYear
                             WHERE  ID = @Year
                                    AND IsDeleted = 0
                                    AND DomainID = @DomainID)

          IF NOT EXISTS(SELECT 1
                        FROM   tbl_LopDetails
                        WHERE  MonthID = @MonthID
                               AND [Year] = @SalaryYear)
            BEGIN
                INSERT INTO tbl_LopDetails
                            (EmployeeId,
                             MonthID,
                             [Year],
                             LopDays,
                             IsManual,
                             CreatedBy,
                             ModifiedBy,
                             DomainId)
                SELECT EM.ID                                       AS EmployeeId,
                       @MonthID                                    AS MonthID,
                       @SalaryYear                                 AS [Year],
                       ISNULL(( [Absent] + ( Halfday / 2.0 ) ), 0) AS LopDays,
                       0                                           AS IsManual,
                       @EmployeeID                                 AS CreatedBy,
                       @EmployeeID                                 AS ModifiedBy,
                       @DomainID                                   AS DomainId
                FROM   tbl_EmployeeMaster EM
                       LEFT JOIN tbl_LopDetails_Process LP
                              ON EM.ID = LP.EmployeeId
                                 AND LP.MonthID = @MonthID
                                 AND LP.Year = @SalaryYear
                WHERE  em.IsDeleted = 0
                       AND EM.IsActive = 0
                       AND EM.BaseLocationID IN (SELECT Item
                                                 FROM   DBO.Splitstring(@BusinessUnitIDs, ','))

                SET @Output = 'LOP Process Completed Successfully'
            END
          ELSE
            BEGIN
                INSERT INTO tbl_LopDetails
                            (EmployeeId,
                             MonthID,
                             [Year],
                             LopDays,
                             IsManual,
                             CreatedBy,
                             ModifiedBy,
                             DomainId)
                SELECT EM.ID                                       AS EmployeeId,
                       @MonthID                                    AS MonthID,
                       @SalaryYear                                 AS [Year],
                       ISNULL(( [Absent] + ( Halfday / 2.0 ) ), 0) AS LopDays,
                       0                                           AS IsManual,
                       @EmployeeID                                 AS CreatedBy,
                       @EmployeeID                                 AS ModifiedBy,
                       @DomainID                                   AS DomainId
                FROM   tbl_EmployeeMaster EM
                       LEFT JOIN tbl_LopDetails_Process LP
                              ON EM.ID = LP.EmployeeId
                                 AND LP.MonthID = @MonthID
                                 AND LP.Year = @SalaryYear
                       LEFT JOIN tbl_LopDetails LD
                              ON EM.ID = LD.EmployeeId
                                 AND ISNULL(LD.MonthID, @MonthID) = @MonthID
                                 AND ISNULL(LD.[Year], @SalaryYear) = @SalaryYear
                WHERE  em.IsDeleted = 0
                       AND EM.IsActive = 0
                       AND EM.BaseLocationID IN (SELECT Item
                                                 FROM   DBO.Splitstring(@BusinessUnitIDs, ','))
                       AND LD.EmployeeId IS NULL

                UPDATE LOP
                SET    LOP.MonthID = @MonthID,
                       LOP.ModifiedBy = @EmployeeID,
                       LOP.LopDays = ISNULL(( LP.[Absent] + ( LP.Halfday / 2.0 ) ), 0)
                FROM   tbl_LopDetails LOP
                       LEFT JOIN tbl_LopDetails_Process LP
                              ON LOP.MonthID = LP.MonthID
                                 AND LOP.[Year] = LP.Year
                                 AND LP.EmployeeID = LOP.EmployeeId
                WHERE  Isnull(LOP.EmployeeId, 0) <> 0
                       AND Isnull(LOP.IsManual, 0) = 0

                SET @Output = 'LOP Process Completed Successfully'
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
