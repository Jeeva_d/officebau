﻿/**************************************************************************** 
CREATED BY    		:	Naneeshwar.M
CREATED DATE		:	08-NOV-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
[Calculateesiindividual] 2000,1,106,5,11,2017,2,4000,2000,1250,1600,1150,0,0,0,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CalculateESIIndividual] (@OverTime           MONEY,
                                                @IsESIRequired      INT,
                                                @EmployeeID         INT,
                                                @WorkingDays        MONEY,
                                                @MonthID            INT,
                                                @Year               INT,
                                                @BaseLocationID     INT,
                                                @Basic              MONEY,
                                                @HRA                MONEY,
                                                @MedicalAllowance   MONEY,
                                                @Conveyance         MONEY,
                                                @SplAllow           MONEY,
                                                @EducationAllowance MONEY,
                                                @PaperMagazine      MONEY,
                                                @MonsoonAllow       MONEY,
                                                @DomainID           INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'
        + CONVERT(VARCHAR(10), @Year)
      DECLARE @ActualWorkdays  INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth)),
              @RegionID        INT = (SELECT ParentID
                 FROM   tbl_BusinessUnit
                 WHERE  id = @BaseLocationID),
              @PAYEESILimit    INT,
              @PAYEESIValue    MONEY,
              @PAYEESIPercent  DECIMAL(18, 2),
              @PAYERESILimit   INT,
              @PAYERESIValue   MONEY,
              @PAYERESIPercent DECIMAL(18, 2),
              @YEARID          INT =(SELECT ID
                FROM   tbl_FinancialYear
                WHERE  NAME = @Year
                       AND IsDeleted = 0
                       AND DomainID = @DomainID)

      SET @PAYEESILimit=(SELECT Limit
                         FROM   tbl_PayrollConfiguration
                         WHERE  Components = 'EmployeeESI'
                                AND BusinessUnitID = @RegionID)
      SET @PAYEESIValue=(SELECT Value
                         FROM   tbl_PayrollConfiguration
                         WHERE  Components = 'EmployeeESI'
                                AND BusinessUnitID = @RegionID)
      SET @PAYEESIPercent=(SELECT Percentage
                           FROM   tbl_PayrollConfiguration
                           WHERE  Components = 'EmployeeESI'
                                  AND BusinessUnitID = @RegionID)
      SET @PAYERESILimit=(SELECT Limit
                          FROM   tbl_PayrollConfiguration
                          WHERE  Components = 'EmployerESI'
                                 AND BusinessUnitID = @RegionID)
      SET @PAYERESIValue=(SELECT Value
                          FROM   tbl_PayrollConfiguration
                          WHERE  Components = 'EmployerESI'
                                 AND BusinessUnitID = @RegionID)
      SET @PAYERESIPercent=(SELECT Percentage
                            FROM   tbl_PayrollConfiguration
                            WHERE  Components = 'EmployerESI'
                                   AND BusinessUnitID = @RegionID)

      BEGIN TRY
          BEGIN
              SELECT CASE
                       WHEN ( @IsESIRequired = 0 ) THEN 0
                       ELSE
                         CASE
                           WHEN ( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                                    + @SplAllow + @EducationAllowance
                                    + @PaperMagazine + @OverTime ) >= @PAYEESILimit ) THEN @PAYEESIValue
                           ELSE Ceiling(( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                                            + @SplAllow + @EducationAllowance
                                            + @PaperMagazine + @OverTime ) * @PAYEESIPercent / 100 ) / @ActualWorkdays * @WorkingDays / 1)
                         END
                     END AS EEESI,
                     CASE
                       WHEN ( @IsESIRequired = 0 ) THEN 0
                       ELSE
                         CASE
                           WHEN ( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                                    + @SplAllow + @EducationAllowance
                                    + @PaperMagazine + @OverTime ) >= @PAYERESILimit ) THEN @PAYERESIValue
                           ELSE Ceiling(( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                                            + @SplAllow + @EducationAllowance
                                            + @PaperMagazine + @OverTime ) * @PAYERESIPercent / 100 ) / @ActualWorkdays * @WorkingDays / 1)
                         END
                     END AS ERESI
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
