﻿CREATE PROCEDURE [dbo].[Auth_ManageWebLogin]
 (
	@FullName		VARCHAR(100)	,
	@Company		varchar(150)	,
	@Email			varchar(50)		,
	@TeamSize		INT				,
	@IndustryType	INT				,
	@Username		varchar(50)		,
	@Password		varchar(100)	,
	@RandomKey		varchar(150)	
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY					
		SET NOCOUNT ON;
		DECLARE @Output						VARCHAR(100)				
		BEGIN TRANSACTION
		    IF EXISTS ( SELECT 1 FROM Auth_WebLogin WHERE IsDeleted = 0 AND Company = @Company )
			BEGIN
				SET @Output = 'Company name already Exist'
				GOTO RESULT
			END	

			IF EXISTS ( SELECT 1 FROM Auth_WebLogin WHERE IsDeleted = 0 AND Email = @Email )
			BEGIN
				SET @Output = 'Email ID already Exist'
				GOTO RESULT
			END
			
			IF EXISTS ( SELECT 1 FROM Auth_Users WHERE IsDeleted = 0 AND Username = @Username )
			BEGIN
				SET @Output = 'Username already Exist'
				GOTO RESULT
			END	
			
			INSERT INTO Auth_WebLogin 
			(
				FullName			,
				Company				,
				Email				,
				TeamSize			,
				IndustryType		
			)
			VALUES
			(
				@FullName			,
				@Company			,
				@Email				,
				@TeamSize			,
				@IndustryType		
			)


			INSERT INTO Auth_Users 
			(
				WebLoginID			,
				Username			,
				[Password]			,
				RandomKey
			)
			VALUES
			(
				@@IDENTITY			,
				@Username			,
				@Password			,
				@RandomKey
			)
			SET @Output = 'OK'
	RESULT:
		SELECT @Output
		COMMIT TRANSACTION      
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
