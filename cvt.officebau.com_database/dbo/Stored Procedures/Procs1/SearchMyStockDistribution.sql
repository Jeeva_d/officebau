﻿/***************************************************************************                   
CREATED BY   :  DHANALAKSHMI. S          
CREATED DATE  : 22-Nov-2018                
MODIFIED BY   :                  
MODIFIED DATE  :              
 <summary>        
 [SearchMyStockDistribution] 1,0,2          
 </summary>                                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchMyStockDistribution] (@DomainID   INT,  
                                                   @ProductID  INT,  
                                                   @EmployeeID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT ProductID,  
                 P.NAME                            AS ProductName,  
                 Sum(Qty)                          AS QTY,  
                 Cast(ISD.DistributedDate AS DATE) AS DistributedDate,  
                 U.NAME                            AS UOM,  
                 CC.NAME                           AS [Cost Center],  
                 (SELECT TOP 1 Remarks  
                  FROM   tbl_InventoryStockDistribution Inv  
                  WHERE  Inv.ProductID = p.ID 
                  AND INV.CostCenterID = CC.ID 
                  ORDER  BY Modifiedon ASC)       AS [Description]  
          FROM   tbl_InventoryStockDistribution ISD  
                 LEFT JOIN tblProduct_V2 P  
                        ON P.ID = ISD.ProductID  
                 LEFT JOIN tbl_UOM U  
                        ON U.ID = P.uomId  
                 LEFT JOIN tblCostCenter CC  
                        ON CC.ID = ISD.CostCenterID  
          WHERE  ISD.IsDeleted = 0  
                 AND EmployeeID = @EmployeeID  
                 AND P.NAME IS NOT NULL  
                 AND ( @ProductID = 0  
                        OR ISD.ProductID = @ProductID ) 
                       AND IsInventroy = 1 
          GROUP  BY ProductID,  
                    Cast(ISD.DistributedDate AS DATE),  
                    P.NAME,  
                    uomId,  
                    U.NAME,  
                    CC.NAME,  
                    P.ID,
                    CC.ID  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
