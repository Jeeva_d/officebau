﻿/****************************************************************************                 
CREATED BY  :                  
CREATED DATE :                 
MODIFIED BY  :             
MODIFIED DATE   :               
 <summary>                        
   [SearchContractEmployeeDropDown] '',1      
 </summary>                                         
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchContractEmployeeDropDown] (  
                                                   @Type           VARCHAR(50),  
                                                   @DomainID       INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
  Select Code,ID from tbl_contractCodeMaster where DomainID =@DomainID and Type=@Type
         
 
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = ERROR_MESSAGE(),  
                 @ErrSeverity = ERROR_SEVERITY()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
