﻿/****************************************************************************     
CREATED BY   :   Dhanalakshmi  
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [Getfinancialyear] 1,1  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetFinancialYear] (@ID       INT,
                                          @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @Date     DATETIME,
                  @Endmonth DATETIME,
                  @Value    VARCHAR(10)

          SET @Value=(SELECT Cast(Isnull(Value, 0) AS INT)
                      FROM   tblApplicationConfiguration
                      WHERE  DomainID = @DomainID
                             AND IsDeleted = 0
                             AND Code = 'STARTMTH')

          IF( @Value > 0 )
            BEGIN
                SET @Date = Dateadd(day, -1, ( Cast(( Cast(Year(Getdate()) AS VARCHAR(4)) + '-'
                                                      + Cast((Cast(@Value AS INT) + 1) AS VARCHAR)
                                                      + '-' + '1' ) AS DATE) ))
                SET @Endmonth = ( Dateadd(month, 11, @Date) )
            END
          ELSE
            BEGIN
                SET @Date =NULL
                SET @Endmonth=NULL
            END

          SELECT fl.ID                                               AS ID,
                 FinancialYear                                       AS FinancialYear,
                 ( CONVERT(VARCHAR(3), Datename(Month, @Date)) )     AS StartMonth,
                 ( CONVERT(VARCHAR(3), Datename(Month, @Endmonth)) ) AS EndMonth,
                 @Value                                              AS Value
          FROM   tbl_FinancialYear fl
          WHERE  fl.IsDeleted = 0
                 AND fl.ID = @ID
                 AND fl.DomainID = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
