﻿  
/****************************************************************************     
CREATED BY  :  JENNIFER.S  
CREATED DATE :  01-AUG-2017  
MODIFIED BY  :     
MODIFIED DATE :     
 <summary>   
          [SearchLoanForeclosure] 18,1,106  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchLoanForeclosure] (@StatusID   INT,  
                                               @DomainID   INT,  
                                               @EmployeeID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN ;  
              WITH Amortization  
                   AS (SELECT LoanRequestID,  
                              Sum(Amount) Amount,  
                              StatusID  
                       FROM   tbl_LoanAmortization  
                       WHERE  DomainID = @DomainID  
                              AND StatusID = (SELECT ID  
                                              FROM   tbl_Status  
                                              WHERE  [Type] = 'Amortization'  
                                                     AND Code = 'Open')  
                       GROUP  BY LoanRequestID,  
                                 StatusID)  
              SELECT LNR.ID                           LoanID,  
                     AMZ.Amount,  
                     Isnull(REQ.EmpCodePattern, '') + REQ.Code + ' - ' + REQ.FullName Requester,  
                     LNS.CreatedOn                    SettlementOn,  
                     Isnull(LNF.ID, 0)                ID,  
                     LNR.LoanTypeID                   TypeID,  
                     LNR.StatusID                     AS LoanStatusID,  
                     CASE  
                       WHEN LNF.LoanRequestID IS NOT NULL THEN  
                         (SELECT ID  
                          FROM   tbl_Status  
                          WHERE  IsDeleted = 0  
                                 AND Type = 'Amortization'  
                                 AND Code = 'Foreclosed')  
                       ELSE  
                         LNR.StatusID  
                     END                              AS StatusID,  
                     CASE  
                       WHEN LNF.LoanRequestID IS NOT NULL THEN  
                         (SELECT Code  
                          FROM   tbl_Status  
                          WHERE  IsDeleted = 0  
                                 AND Type = 'Amortization'  
                                 AND Code = 'Foreclosed')  
                       ELSE  
                         (SELECT Code  
                          FROM   tbl_Status  
                          WHERE  IsDeleted = 0  
                                 AND Type = 'Loan'  
                                 AND ID = LNR.StatusID)  
                     END                              AS [Status],  
                     LNR.EmployeeID                   AS EmployeeID,  
                     LNF.TotalAmount                  AS TotalAmount  
              INTO   #tempLoanTable  
              FROM   tbl_LoanSettle LNS  
                     JOIN tbl_LoanRequest LNR  
                       ON LNS.LoanRequestID = LNR.ID  
                     JOIN tbl_EmployeeMaster REQ  
                       ON LNR.EmployeeID = REQ.ID  
                     JOIN Amortization AMZ  
                       ON LNS.LoanRequestID = AMZ.LoanRequestID  
                     LEFT JOIN tbl_LoanForeclosure LNF  
                            ON LNS.LoanRequestID = LNF.LoanRequestID  
              WHERE  LNR.IsDeleted = 0  
                     AND LNS.DomainID = @DomainID  
                     --AND ( Isnull(@StatusID, 0) = 0   
                     --                  AND (LNR.StatusID in (SELECT ID  
                     --                                      FROM   tbl_Status  
                     --                                      WHERE  [Type] = 'Loan'  
                     --                                             AND Code = 'Settled') ) OR LNR.StatusID = @StatusID   
                     --         OR AMZ.StatusID = @StatusID)   
                     AND LNR.LoanTypeID <> 0  
  
              SELECT *  
              FROM   #tempLoanTable  
              WHERE  ( ISNULL(@StatusID, 0) = 0  
                       AND LoanStatusID IN (SELECT ID  
                                            FROM   tbl_Status  
                                            WHERE  [Type] = 'Loan'  
                                                   AND Code = 'Settled')  
                        OR StatusID = @StatusID )  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
