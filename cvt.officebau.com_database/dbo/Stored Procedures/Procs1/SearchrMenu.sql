﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
<summary>          
  [Searchrbsusermenuaccess] 1,1,1  
</summary>                           
*****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchrMenu] (@MenuCode VARCHAR(20)  
                                                 )  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          SELECT MenuURL AS Menu,RootConfig
		   from tbl_RBSMenu  
          WHERE  MenuCode=@MenuCode 
			AND ISDEleted=0  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
