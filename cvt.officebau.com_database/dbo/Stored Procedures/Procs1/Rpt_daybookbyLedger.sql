﻿/****************************************************************************                                                 
CREATED BY  : Ajith N                                              
CREATED DATE : 19 May 2017                                                
MODIFIED BY  : Jennifer.S                                        
MODIFIED DATE : 18-Sep-2017                                        
<summary>                                              
[Rpt_daybookbyLedger] '01-Jan-2017','01-Jan-2019',1,'Bank Charges'                                        
</summary>                                                                         
*****************************************************************************/            
CREATE PROCEDURE [dbo].[Rpt_daybookbyLedger](@StartDate  DATETIME = NULL,            
                                            @EndDate    DATETIME = NULL,            
                                            @DomainID   INT,            
                                            @LedgerName VARCHAR(100) = NULL)            
AS            
  BEGIN            
      SET NOCOUNT ON;            
            
      BEGIN TRY            
          DECLARE @LedgerID INT            
            
          IF( @LedgerName IS NULL            
               OR @LedgerName = '' )            
            SET @LedgerID = 0            
          ELSE            
            SET @LedgerID = (SELECT TOP 1 ID            
                             FROM   tblLedger            
                             WHERE  NAME = @LedgerName            
                                    AND DomainID = @DomainID);            
            
          WITH CTE            
               AS (SELECT e.ID                                                    AS ID,            
                          E.[Date]                                                AS [Date],            
                          E.ID                                                    AS OrderID,            
                          ED.LedgerID                                             AS LedgerID,            
                          VD.NAME                                                 AS Ledger,            
                          ( ED.Amount * qty ) + ISNULL(CGST, 0) + ISNULL(SGST, 0) AS Amount,            
                          E.[Type]                                                AS [Type],            
                                     
                          ED.Remarks                                              AS [Description],            
                          e.BillNo                                            AS [Instrument No]            
                   FROM   tblExpense E            
                          LEFT JOIN tblExpensePaymentMapping EM            
                                 ON EM.ExpenseID = E.ID            
                                    AND EM.IsDeleted = 0            
                          LEFT JOIN tblExpensePayment EP            
                                 ON EP.ID = EM.ExpensePaymentID            
                                    AND EP.IsDeleted = 0            
                          LEFT JOIN tblExpenseDetails ED            
                                 ON ED.ExpenseID = E.ID            
                                    AND ED.LedgerID = @LedgerID            
                                    AND ED.IsLedger = 1            
                                    AND ED.IsDeleted = 0            
                          LEFT JOIN tblLedger VD            
                                 ON VD.ID = ED.LedgerID            
                   WHERE  E.IsDeleted = 0            
                          AND E.[Type] = 'Expense'            
                          AND E.DomainID = @DomainID            
                          AND ( ED.LedgerID = @LedgerID )            
                          AND ( @StartDate IS NULL            
                                 OR E.[Date] >= @StartDate )            
            AND ( @EndDate IS NULL            
                                 OR E.[Date] <= @EndDate )            
           UNION ALL            
                   SELECT e.ID                                                    AS ID,           
                          E.[Date]                                                AS [Date],            
      E.ID                                                    AS OrderID,            
                          l.ID                              AS LedgerID,            
                          l.NAME                                                  AS Ledger,            
                          ( ED.Amount * qty ) + ISNULL(CGST, 0) + ISNULL(SGST, 0) AS Amount,            
                                     
                          E.[Type]                                                AS [Type],            
                                   
                          ED.Remarks                                              AS [Description],            
                          E.BillNo                                            AS [Instrument No]            
                   FROM   tblExpense E            
                          LEFT JOIN tblExpensePaymentMapping EM            
                                 ON EM.ExpenseID = E.ID            
                                    AND EM.IsDeleted = 0            
                          LEFT JOIN tblExpensePayment EP            
                                 ON EP.ID = EM.ExpensePaymentID            
                                    AND EP.IsDeleted = 0            
                          LEFT JOIN tblExpenseDetails ED            
                                 ON ED.ExpenseID = E.ID            
                                    AND ED.IsLedger = 0            
                                    AND ED.IsDeleted = 0            
                          LEFT JOIN tblProduct_v2 VD            
                                 ON VD.ID = ED.LedgerID            
                          LEFT JOIN tblLedger l            
                                 ON l.ID = vd.PurchaseLedgerId            
                   WHERE  E.IsDeleted = 0            
                          AND E.[Type] = 'Expense'            
                          AND E.DomainID = @DomainID            
                          AND ( ED.LedgerID = @LedgerID )            
                          AND ( @StartDate IS NULL            
                                 OR E.[Date] >= @StartDate )            
                          AND ( @EndDate IS NULL            
                                 OR E.[Date] <= @EndDate )            
                   UNION ALL            
                   SELECT DISTINCT e.ID                                                    AS ID,            
                                   E.[Date]                                                AS [Date],            
                                   E.ID                                                    AS OrderID,            
                                   ED.LedgerID                                             AS LedgerID,            
                                   VD.NAME                                                 AS Ledger,            
                                   ( ED.Amount * qty ) + ISNULL(CGST, 0) + ISNULL(SGST, 0) As Amount,            
                                   'Bill'                                                  AS [Type],            
                                   ED.Remarks                                              AS [Description],            
                                   E.BillNo                                                      AS [Instrument No]            
                   FROM   tblExpense E            
                          LEFT JOIN tblExpenseDetails ED            
                                 ON ED.ExpenseID = E.ID            
                                    AND ED.IsLedger = 1            
                                    AND ED.IsDeleted = 0            
                          LEFT JOIN tblLedger VD            
                                 ON VD.ID = ED.LedgerID            
                   WHERE  E.IsDeleted = 0            
          AND E.[Type] = 'Bill'            
                          AND E.DomainID = @DomainID            
                          AND ( ED.LedgerID = @LedgerID )            
                          AND ( @StartDate IS NULL            
                                OR E.[Date] >= @StartDate )            
                          AND ( @EndDate IS NULL            
                                 OR E.[Date] <= @EndDate )            
                           
       UNION ALL            
                   SELECT DISTINCT e.ID                                                    AS ID,            
                                   E.[Date]                                                AS [Date],           
                                   E.ID                                                    AS OrderID,            
                                   l.ID                                                    AS LedgerID,            
                                   l.NAME                                                  AS Ledger,            
                                   ( ED.Amount * qty ) + ISNULL(CGST, 0) + ISNULL(SGST, 0) AS Amount,            
                                   'Bill'                                                  AS [Type],            
                                   ED.Remarks                                              AS [Description],            
                                  E.BillNo                                                    AS [Instrument No]            
                   FROM   tblExpense E            
                          LEFT JOIN tblExpenseDetails ED            
                                 ON ED.ExpenseID = E.ID            
                                    AND ED.IsLedger = 0            
                                    AND ED.IsDeleted = 0            
                          LEFT JOIN tblProduct_v2 VD            
                                 ON VD.ID = ED.LedgerID            
                          LEFT JOIN tblLedger l            
                                 ON l.ID = vd.PurchaseLedgerId            
                   WHERE  E.IsDeleted = 0            
                          AND E.[Type] = 'Bill'            
                          AND E.DomainID = @DomainID            
                          AND ( VD.PurchaseLedgerId = @LedgerID )            
                          AND ( @StartDate IS NULL            
                                 OR E.[Date] >= @StartDate )            
                          AND ( @EndDate IS NULL            
                                 OR E.[Date] <= @EndDate )            
                             
                   UNION ALL            
                   SELECT DISTINCT e.ID                                                              AS ID,            
                                   E.[Date]                                                          AS [Date],            
                                   E.ID                                                              AS OrderID,            
                                   l.ID                                                              AS LedgerID,            
                                   l.NAME                                                            AS Ledger,            
                                   ( ED.Rate * qty ) + ISNULL(CGSTAmount, 0) + ISNULL(SGSTAmount, 0) AS Amount,            
                                   'Invoice'                                                         AS [Type],            
                                   ED.ItemDescription                                                AS [Description],            
                                  e.InvoiceNo                                                             AS [Instrument No]           
                   FROM   tblInvoice E            
                          --LEFT JOIN tblExpensePaymentMapping EM                                  
                          --       ON EM.ExpenseID = E.ID                                  
                          --          AND EM.IsDeleted = 0                                  
                          --LEFT JOIN tblExpensePayment EP                                  
                          --       ON EP.ID = EM.ExpensePaymentID                                  
                          --          AND EP.IsDeleted = 0                                  
           LEFT JOIN tblInvoiceItem ED            
                                 ON ED.InvoiceID = E.ID            
                                    AND Ed.isdeleted = 0            
                          LEFT JOIN tblProduct_v2 VD            
                                 ON VD.ID = ED.ProductID            
LEFT JOIN tblLedger l            
                                 ON l.ID = vd.SalesLedgerId            
                   WHERE  E.IsDeleted = 0            
                          AND E.DomainID = @DomainID            
                          AND ( VD.SalesLedgerId = @LedgerID )            
                          AND ( @StartDate IS NULL            
         OR E.[Date] >= @StartDate )            
                          AND ( @EndDate IS NULL            
                                 OR E.[Date] <= @EndDate )            
                   UNION ALL            
                   SELECT DISTINCT r.ID            AS ID,            
                                   R.[paymentDate] AS [Date],            
                                   r.ID            AS OrderID,            
                                   R.LedgerID      AS LedgerID,            
                                   LD.NAME         AS Ledger,            
                                   R.TotalAmount   AS Amount,            
                                   'Receipts'      AS [Type],            
                                              
                                   R.[Description] AS [Description],            
                                   NULL            AS [Instrument No]            
                   FROM   tbl_Receipts R            
                          LEFT JOIN tblLedger LD            
                                 ON LD.ID = R.LedgerID            
                          LEFT JOIN tblbank B            
                                 ON B.ID = R.BankID            
                   --LEFT JOIN tblMasterTypes m            
                   --       ON m.ID = r.TransactionType            
                   WHERE  R.IsDeleted = 0            
                          AND R.DomainID = @DomainID            
                          AND ( R.LedgerID = @LedgerID )            
                          AND ( @StartDate IS NULL            
                                 OR R.[paymentDate] >= @StartDate )            
                          AND ( @EndDate IS NULL            
                                 OR R.[paymentDate] <= @EndDate )            
                   UNION ALL            
                   SELECT DISTINCT r.ID            AS ID,            
                                   R.[paymentDate] AS [Date],            
                                   r.ID            AS OrderID,            
                                   R.LedgerID      AS LedgerID,            
                                   LD.NAME         AS Ledger,            
                                   R.TotalAmount   AS Amount,            
                                   'Payments'      AS [Type],            
                                            
                                   R.[Description] AS [Description],            
                                   NULL            AS [Instrument No]            
                   FROM   tbl_Payment R            
                          LEFT JOIN tblLedger LD            
       ON LD.ID = R.LedgerID            
                          LEFT JOIN tblbank B            
                                 ON B.ID = R.BankID            
                   WHERE  R.IsDeleted = 0            
                          AND R.DomainID = @DomainID            
                          AND ( R.LedgerID = @LedgerID )            
              AND ( @StartDate IS NULL            
                                 OR R.[paymentDate] >= @StartDate )            
                          AND ( @EndDate IS NULL            
                                 OR R.[paymentDate] <= @EndDate )            
          
    Union All           
        Select eh.ID,Date,eh.ID,eh.LedgerId,LD.Name,Amount,'Expense Holding',LD.Name,'' from tblExpenseHoldings eh          
     LEFT JOIN tblLedger LD            
                                 ON LD.ID = eh.LedgerID            
      where eh.Isdeleted=0            
      AND eh.DomainID = @DomainID            
                          AND ( eh.LedgerID = @LedgerID )            
              AND ( @StartDate IS NULL            
                                 OR eh.Date >= @StartDate )            
                          AND ( @EndDate IS NULL            
                                 OR eh.Date <= @EndDate )            
     Union All           
        Select eh.ID,Date,eh.ID,eh.LedgerId,LD.Name,Amount,'Invoice Holding',LD.Name,'' from tblInvoiceHoldings eh          
     LEFT JOIN tblLedger LD            
                                 ON LD.ID = eh.LedgerID            
      where eh.Isdeleted=0            
      AND eh.DomainID = @DomainID            
                          AND ( eh.LedgerID = @LedgerID )            
              AND ( @StartDate IS NULL            
                                 OR eh.Date >= @StartDate )            
                          AND ( @EndDate IS NULL            
                            OR eh.Date <= @EndDate )          
                  ----------------------------------------       
  Union All Select j.ID,j.JournalDate,j.ID,j.LedgerProductID,l.Name,    
  CASE         
                   WHEN ( g.reporttype = 1 OR g.ReportType=4 ) THEN (ISNULL(j.debit,0) + ISNULL( credit *- 1,0 ) )        
                   WHEN ( g.reporttype = 2 OR g.ReportType=3 ) THEN (ISNULL(j.debit*-1,0) + ISNULL( credit ,0 ) )         
                 END,    
  'Journal',j.Description,j.JournalNo from tblJournal J    
  Join tblLedger l on l.ID = j.LedgerProductID   And j.Type='Ledger'  
  Join tblGroupLedger g on g.ID = l.GroupID    
  Where j.IsDeleted=0    And j.Type='Ledger'  
   AND j.DomainID = @DomainID            
                          AND ( j.LedgerProductID = @LedgerID )            
              AND ( @StartDate IS NULL            
                                 OR j.JournalDate >= @StartDate )            
                          AND ( @EndDate IS NULL            
                            OR j.JournalDate <= @EndDate )    
Union All Select j.ID,j.JournalDate,j.ID,j.LedgerProductID,l.Name,    
  CASE         
                   WHEN ( g.reporttype = 1 OR g.ReportType=4 ) THEN (ISNULL(j.debit,0) + ISNULL( credit *- 1,0 ) )        
                   WHEN ( g.reporttype = 2 OR g.ReportType=3 ) THEN (ISNULL(j.debit*-1,0) + ISNULL( credit ,0 ) )         
                 END,    
  'Journal',j.Description,j.JournalNo from tblJournal J   
  join tblproduct_v2 p  on p.ID=j.LedgerProductID and j.type='Product'  
  Join tblLedger l on l.ID = ISNUll(p.PurchaseLedgerId,p.SalesLedgerId)     
  Join tblGroupLedger g on g.ID = l.GroupID    
  Where j.IsDeleted=0    And j.Type='Product'  
   AND j.DomainID = @DomainID            
                          AND ( l.ID = @LedgerID )            
              AND ( @StartDate IS NULL            
                                 OR j.JournalDate >= @StartDate )            
                          AND ( @EndDate IS NULL            
                            OR j.JournalDate <= @EndDate )         
                  )            
          SELECT ID,            
    [Date],            
                 OrderID,            
                 [Instrument No],            
                 LedgerID,            
                 Ledger,            
                Amount,            
                 [Type],            
                 Row_number()            
                   OVER(            
                     PARTITION BY LedgerID            
                     ORDER BY [Date]) RowOrder,            
                            
                 [Description]            
          INTO   #TempResult            
          FROM   CTE            
          ORDER  BY OrderID ASC            
            
          -- OpeningBalance and OutstandingBalance Calculation                                        
          DECLARE @OpeningBalance MONEY            
          DECLARE @OutstandingBalance MONEY            
            
          SET @OpeningBalance = (SELECT Isnull(0, 0)            
                                 FROM   tblLedger            
                                 WHERE  ID = @LedgerID)            
          SET @OutstandingBalance = ( (SELECT Sum(Isnull(0, 0))             
                                       FROM   #TempResult TEMP            
                                              JOIN tblLedger L            
                                                ON L.ID = TEMP.LedgerID)            
                                      + @OpeningBalance )            
            
          SELECT @OpeningBalance     AS OpeningBalance,            
                 @OutstandingBalance AS OutstandingBalance            
            
          SELECT ID,            
                 Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-') [Date],            
                 [Type],            
                 Sum(Amount)                                           Amount,            
                 Substring((SELECT + ',' + [Description]            
                            FROM   #TempResult ed            
                            WHERE  V.ID = ed.ID            
                            FOR XML PATH('')), 2, 200000)             [Description],            
                 [Instrument No]            
          FROM   #TempResult V            
          GROUP  BY ID,            
                    Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-'),            
                    [Type],            
                    [Instrument No],            
                    OrderID            
          ORDER  BY OrderID,            
                    [Type],            
                    CASE            
                      WHEN( Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-') IS NULL ) THEN            
                        '1-1-2999'            
                      ELSE            
                        Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-')            
                    END ASC            
      END TRY            
      BEGIN CATCH            
          DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT            
          SELECT @ErrorMsg = Error_message(),            
                 @ErrSeverity = Error_severity()            
          RAISERROR(@ErrorMsg,@ErrSeverity,1)            
      END CATCH            
  END
