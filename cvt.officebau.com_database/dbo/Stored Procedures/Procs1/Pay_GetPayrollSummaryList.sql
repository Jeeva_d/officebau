﻿/****************************************************************************     
CREATED BY   :    
CREATED DATE  :    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
[Pay_GetPayrollSummaryList] 11,2018,'4,2,11',1     
 </summary>                              
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_GetPayrollSummaryList] (@MonthID      INT,  
                                                   @Year         INT,  
                                                   @BusinessUnit VARCHAR(50),  
                                                   @DomainID     INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @CompanyName VARCHAR(100)= (SELECT Isnull(FullName, '')  
                     FROM   tbl_company  
                     WHERE  id = @DomainID),  
                  @YearName    INT = (SELECT NAME  
                     FROM   tbl_FinancialYear  
                     WHERE  IsDeleted = 0  
                            AND DomainID = @DomainID  
                            AND id = @Year)  
  
          CREATE TABLE #tempPayStub  
            (  
               [Description] VARCHAR(100),  
               Amount        MONEY,  
               Ordinal       INT ,
			   Type Varchar(100) 
            )  
  
          SELECT pc.id            AS id,  
                 [Description] AS [Description],  
                 DomainID      AS DomainID,  
                 Ordinal       AS Ordinal ,
				  p.Type 
          INTO   #tmpPayrollCompontents  
          FROM   tbl_Pay_PayrollCompontents  pc
		 left  Join tbl_Pay_PaystubConfiguration p on p.ComponentID = pc.ID and p.IsDeleted=0
          WHERE  pc.isdeleted = 0  
                 AND DomainID = @DomainID  
  
          IF EXISTS(SELECT 1  
                    FROM   tbl_Pay_EmployeePayroll  
                    WHERE  Isdeleted = 0  
                           AND MonthId = @MonthID  
                           AND YearId = @YearName)  
            BEGIN  
                INSERT INTO #tempPayStub  
                SELECT pc.[Description] AS [Description],  
                       pd.Amount,  
                       pc.Ordinal,
					   pc.Type  
                FROM   #tmpPayrollCompontents pc  
                       JOIN tbl_EmployeeMaster emp  
                         ON emp.DomainID = pc.DomainID  
                       JOIN tbl_Pay_EmployeePayroll epay  
                         ON epay.Isdeleted = 0  
                            AND epay.EmployeeID = emp.ID  
                            AND epay.MonthId = @MonthID  
                            AND epay.YearId = @YearName  
                       JOIN tbl_Pay_EmployeePayrollDetails pd  
                         ON pd.PayrollId = epay.ID  
                            AND pd.ComponentId = pc.id  
                            AND pd.Isdeleted = 0  
                WHERE  emp.Isdeleted = 0  
                       AND emp.BaseLocationID IN (SELECT item  
                                                  FROM   dbo.SplitString(@BusinessUnit, ','))  
            END  
          ELSE  
            BEGIN  
                SELECT *  
                INTO   #tmpEmployeePayStructure  
                FROM   (SELECT ED.ID,  
                               ED.EmployeeID,  
                               ED.EffectiveFrom,  
                               ED.IsDeleted,  
                               ROW_NUMBER()  
                                 OVER (  
                                   partition BY ED.EmployeeID  
                                   ORDER BY ED.EffectiveFrom DESC) AS RowNo  
                        FROM   tbl_Pay_EmployeePayStructure ED  
                        WHERE  ED.IsDeleted = 0) AS T  
                WHERE  T.RowNo = 1  
  
                INSERT INTO #tempPayStub  
                SELECT pc.[Description] AS [Description],  
                       PSD.Amount,  
                       pc.Ordinal  ,
					   pc.Type
                FROM   #tmpPayrollCompontents pc  
                       JOIN tbl_EmployeeMaster emp  
                         ON emp.DomainID = pc.DomainID  
                       JOIN #tmpEmployeePayStructure EPS  
                         ON eps.EmployeeID = emp.ID  
                       JOIN tbl_Pay_EmployeePayStructureDetails PSD  
                         ON PSD.PayStructureId = EPS.ID  
                            AND PSD.ComponentId = pc.id  
                            AND PSD.Isdeleted = 0  
                WHERE  emp.Isdeleted = 0  
                       AND emp.IsActive = 0  
                       AND emp.BaseLocationID IN (SELECT item  
                                                  FROM   dbo.SplitString(@BusinessUnit, ','))  
            END  
  
          SELECT [Description],  
                 Sum(Amount) AS Amount,
				 Type  
          FROM   #tempPayStub  
          GROUP  BY [Description],  
                    Ordinal  ,Type
          ORDER  BY Type  desc
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
