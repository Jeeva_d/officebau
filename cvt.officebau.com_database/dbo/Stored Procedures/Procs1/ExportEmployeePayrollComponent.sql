﻿/**************************************************************************** 
CREATED BY    		:	Ajith N
CREATED DATE		:	17 Aug 2017
MODIFIED BY			:	
MODIFIED DATE		:	
<summary>  
	[ExportEmployeePayrollComponent] 10,3,1,'1,2,3,4,5,6,7,8,9',null,null
</summary>                         
*****************************************************************************/
CREATE PROCEDURE [dbo].[ExportEmployeePayrollComponent] (@MonthId      INT,
                                                        @Year         INT,
                                                        @DomainID     INT,
                                                        @Location     VARCHAR(100),
                                                        @DepartmentID INT,
                                                        @StatusID     INT)
AS
  BEGIN
      SET NOCOUNT ON;
      SET @Year = (SELECT NAME
                   FROM   tbl_FinancialYear
                   WHERE  id = @Year
                          AND DomainID = @DomainID)

      DECLARE @CurrentMonth DATE = Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @year - 2000, Dateadd(MONTH, @monthID - 1, '20000101')))
                                  + 1, 0))
      DECLARE @DataSource TABLE
        (
           [Value] NVARCHAR(128)
        )

      INSERT INTO @DataSource
                  ([Value])
      SELECT Item
      FROM   dbo.Splitstring (@Location, ',')
      WHERE  Isnull(Item, '') <> ''

      BEGIN TRY
          BEGIN
              WITH cte
                   AS (SELECT E.Code               AS EmpCode,
                              PT                   AS PT,
                              TDS                  AS TDS,
                              PLI                  AS PLI,
                              Mediclaim            AS Mediclaim,
                              MobileDataCard       AS MobileDataCard,
                              OtherEarning         AS OtherEarning,
                              OtherDeduction       AS OtherDeductions,
                              OtherPerks           AS OtherPerks,
                              Loans                AS Loans,
                              Bonus                AS Bonus,
                              OverTime             AS OverTime,
                              EEESI                AS EmployeeESI,
                              ERESI                AS EmployerESI,
                              R.MonsoonAllow       AS MonsoonAllow,
                              E.FullName           AS EmployeeName,
                              BU.NAME              AS BusinessUnit,
                              Isnull(DEP.NAME, '') AS Department,
                              IsProcessed          AS IsProcessed,
                              IsApproved           AS IsApproved
                       FROM   tbl_EmployeePayroll R
                              JOIN tbl_EmployeeMaster E
                                ON R.EmployeeId = E.ID
                                   AND E.IsDeleted = 0
                              LEFT JOIN tbl_lopdetails P
                                     ON P.EmployeeId = R.EmployeeId
                                        AND P.Monthid = @MonthId
                                        AND P.year = @Year
                              LEFT JOIN tbl_BusinessUnit BU
                                     ON BU.ID = R.BaseLocationID
                              LEFT JOIN tbl_department DEP
                                     ON DEP.ID = E.DepartmentID
                       WHERE  R.monthid = @MonthId
                              AND [yearid] = @Year
                              AND R.isdeleted = 0
                              AND ( ( @Location = Cast(0 AS VARCHAR) )
                                     OR ( R.BaseLocationID IN (SELECT [Value]
                                                               FROM   @DataSource
                                                               WHERE  E.IsDeleted = 0) ) )
                              AND ( ( @DepartmentID = 0
                                       OR @DepartmentID IS NULL )
                                     OR ( E.DepartmentID = @DepartmentID ) ))
              SELECT *
              INTO   #PAY
              FROM   cte
              ORDER  BY EmpCode ASC

              IF( @StatusID = 1 )
                BEGIN
                    SELECT EmpCode,
                           EmployeeName,
                           Department,
                           TDS,
                           PT,
                           EmployeeESI,
                           OtherDeductions,
                           EmployerESI,
                           PLI,
                           Mediclaim,
                           MobileDataCard,
                           OtherEarning,
                           OtherPerks,
                           Loans,
                           Bonus,
                           OverTime,
                           MonsoonAllow,
                           BusinessUnit
                    FROM   #PAY
                    WHERE  IsProcessed = 0
                           AND IsApproved = 0
                END
              ELSE
                BEGIN
                    IF( @StatusID IS NULL )
                      SELECT EmpCode,
                             EmployeeName,
                             Department,
                             TDS,
                             PT,
                             EmployeeESI,
                             OtherDeductions,
                             EmployerESI,
                             PLI,
                             Mediclaim,
                             MobileDataCard,
                             OtherEarning,
                             OtherPerks,
                             Loans,
                             Bonus,
                             OverTime,
                             MonsoonAllow,
                             BusinessUnit
                      FROM   #PAY
                END
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
