﻿/****************************************************************************   
CREATED BY		: 
CREATED DATE	: 
MODIFIED BY		: Naneeshwar
MODIFIED DATE	: 
 <summary>
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Notificationmessageread] (@ID         INT,
                                                 @EmployeeID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Ouput VARCHAR(100) = 'Message sent Successfully'

          IF( (SELECT FromID
               FROM   tbl_Notifications
               WHERE  id = @ID) = @EmployeeID )
            BEGIN
                UPDATE tbl_Notifications
                SET    SenderIsViewed = 1
                WHERE  Id = @ID
            END

          IF( (SELECT ToID
               FROM   tbl_Notifications
               WHERE  id = @ID) = @EmployeeID )
            BEGIN
                UPDATE tbl_Notifications
                SET    ReceiverIsViewed = 1
                WHERE  Id = @ID
            END

          SELECT @Ouput

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
