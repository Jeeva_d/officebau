﻿/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  :   06-JUN-2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
     [ManageMasterData] 6,'1','1','','DESTINATIONCITIES',null,1, 1 
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageMasterData] (@ID               INT,
                                          @Code             VARCHAR(200),
                                          @Description      VARCHAR(1000),
                                          @DependantTable   VARCHAR(200),
                                          @MainTable        VARCHAR(200),
                                          @DependantTableID INT,
                                          @SessionID        INT,
                                          @DomainID         INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @sql      VARCHAR(MAX),
                  @countsql NVARCHAR(MAX),
                  @count    INT,
                  @OUTPUT   VARCHAR(100) = 'Operation Failed!'

          SET @MainTable = 'tbl_' + @MainTable

          IF( Isnull(@DependantTable, '') <> '' )
            SET @DependantTable=@DependantTable + 'ID'

          IF( Isnull(@DependantTable, '') = '' )
            BEGIN
                IF( @ID = 0 )
                  BEGIN
                      IF( @MainTable = 'tbl_BusinessUnit' )
                        BEGIN
                            SET @countsql= N'SELECT @COUNTS=COUNT(1) from '
                                           + @MainTable + ' where Name =''' + @Code
                                           + '''  
										 and DomainID = '
                                           + CONVERT(VARCHAR(10), @DomainID)
                                           + ' and Isdeleted = 0 AND ISNULL(ParentID, 0) = 0'
                        END
                      ELSE IF( @MainTable = 'tbl_DESTINATIONCITIES' )
                        BEGIN
                            SET @countsql= N'SELECT @COUNTS=COUNT(1) from '
                                           + @MainTable + ' where Name =''' + @Code
                                           + '''  
										 and DomainID = '
                                           + CONVERT(VARCHAR(10), @DomainID)
                                           + ' and Isdeleted = 0'
                        END
                      ELSE
                        BEGIN
                            SET @countsql= N'SELECT @COUNTS=COUNT(1) from '
                                           + @MainTable + ' where Name =''' + @Code
                                           + '''  
										 and DomainID = '
                                           + CONVERT(VARCHAR(10), @DomainID)
                                           + ' and Isdeleted = 0'
                        END

                      EXEC Sp_executesql
                        @countsql,
                        N'@COUNTS INT OUTPUT',
                        @count OUTPUT;

                      IF( @count = 0 )
                        BEGIN
                            IF( @MainTable = 'tbl_DESTINATIONCITIES' )
                              BEGIN
                                  SET @sql= ( 'INSERT INTO ' + @MainTable
                                              + ' (Name,Remarks,IsMetro,DomainID,CreatedBy,ModifiedBy) Values('''
                                              + @Code + ''',  
										 ''' + @Description + ''','
                                              + CONVERT(VARCHAR(10), @DependantTableID)
                                              + ',' + CONVERT(VARCHAR(10), @DomainID) + ','
                                              + CONVERT(VARCHAR(10), @SessionID) + ','
                                              + CONVERT(VARCHAR(10), @SessionID) + ')' )
                                  SET @OUTPUT = 'Inserted Successfully'
                              END
                            ELSE
                              BEGIN
                                  SET @sql= ( 'INSERT INTO ' + @MainTable
                                              + ' (Name,Remarks,DomainID,CreatedBy,ModifiedBy) Values('''
                                              + @Code + ''',  
										 ''' + @Description + ''','
                                              + CONVERT(VARCHAR(10), @DomainID) + ','
                                              + CONVERT(VARCHAR(10), @SessionID) + ','
                                              + CONVERT(VARCHAR(10), @SessionID) + ')' )
                                  SET @OUTPUT = 'Inserted Successfully'
                              END
                        END
                      ELSE
                        SET @OUTPUT = 'Already Exists'
                  END
                ELSE
                  BEGIN
                      IF( @MainTable = 'tbl_BusinessUnit' )
                        BEGIN
                            SET @countsql= N'SELECT @COUNTS=COUNT(1) from '
                                           + @MainTable + ' where Name =''' + @Code
                                           + '''   
										  AND ID <>'
                                           + CONVERT(VARCHAR(10), @ID)
                                           + ' and DomainID = '
                                           + CONVERT(VARCHAR(10), @DomainID)
                                           + ' AND ISNULL(ParentID, 0) = 0'
                        END
                      ELSE IF( @MainTable = 'tbl_DESTINATIONCITIES' )
                        BEGIN
                            SET @countsql= N'SELECT @COUNTS=COUNT(1) from '
                                           + @MainTable + ' where Name =''' + @Code
                                           + '''   
										  AND ID <>'
                                           + CONVERT(VARCHAR(10), @ID)
                                           + ' and DomainID = '
                                           + CONVERT(VARCHAR(10), @DomainID)
                                           + ' and Isdeleted = 0'
                        END
                      ELSE
                        BEGIN
                            SET @countsql= N'SELECT @COUNTS=COUNT(1) from '
                                           + @MainTable + ' where Name =''' + @Code
                                           + '''   
										  AND ID <>'
                                           + CONVERT(VARCHAR(10), @ID)
                                           + ' and IsDeleted = 0 AND DomainID = '
                                           + CONVERT(VARCHAR(10), @DomainID)
                        END

                      EXEC Sp_executesql
                        @countsql,
                        N'@COUNTS INT OUTPUT',
                        @count OUTPUT;

                      IF( @count = 0 )
                        BEGIN
                            IF( @MainTable = 'tbl_DESTINATIONCITIES' )
                              BEGIN
                                  SET @sql= ( 'UPDATE ' + @MainTable + ' SET Name=''' + @Code
                                              + ''',Remarks=''' + @Description
											   + ''',IsMetro=''' +CONVERT(VARCHAR(10), @DependantTableID)
                                              + ''' ,ModifiedBy= '
                                              + CONVERT(VARCHAR(10), @SessionID)
                                              + ' WHERE ID = ' + CONVERT(VARCHAR(10), @ID) + '' )
                                  SET @OUTPUT = 'Updated Successfully'
                              END
                            ELSE
                              BEGIN
                                  SET @sql= ( 'UPDATE ' + @MainTable + ' SET Name=''' + @Code
                                              + ''',Remarks=''' + @Description
                                              + '''  
										,ModifiedBy= '
                                              + CONVERT(VARCHAR(10), @SessionID)
                                              + ' WHERE ID = ' + CONVERT(VARCHAR(10), @ID) + '' )
                                  SET @OUTPUT = 'Updated Successfully'
                              END
                        END
                      ELSE
                        SET @OUTPUT = 'Already Exists'
                  END
            END
          ELSE
            BEGIN
                IF( @ID = 0 )
                  BEGIN
                      SET @countsql= N'SELECT @COUNTS=COUNT(1) from '
                                     + @MainTable + ' where Name =''' + @Code
                                     + '''   
										AND ' + @DependantTable + '='
                                     + CONVERT(VARCHAR(10), @DependantTableID)
                                     + '  
										 AND DomainID = '
                                     + CONVERT(VARCHAR(10), @DomainID)
                                     + ' and Isdeleted = 0'

                      EXEC Sp_executesql
                        @countsql,
                        N'@COUNTS INT OUTPUT',
                        @count OUTPUT;

                      IF( @count = 0 )
                        BEGIN
                            SET @sql =( 'INSERT INTO ' + @MainTable
                                        + ' (Name,Remarks,' + @DependantTable
                                        + '  
											 ,DomainID) Values(''' + @Code
                                        + ''',''' + @Description + ''','
                                        + CONVERT(VARCHAR(10), @DependantTableID)
                                        + ',  
										'
                                        + CONVERT(VARCHAR(10), @DomainID) + ')' )
                            SET @OUTPUT = 'Inserted Successfully'
                        END
                      ELSE
                        SET @OUTPUT = 'Already Exists'
                  END
                ELSE
                  BEGIN
                      SET @countsql= N'SELECT @COUNTS=COUNT(1) from '
                                     + @MainTable + ' where Name =''' + @Code
                                     + ''' AND ID <>' + CONVERT(VARCHAR(10), @ID)
                                     + '  
									 AND ' + @DependantTable + '='
                                     + CONVERT(VARCHAR(10), @DependantTableID)
                                     + '  
										 AND DomainID = '
                                     + CONVERT(VARCHAR(10), @DomainID)
                                     + ' and Isdeleted = 0'

                      EXEC Sp_executesql
                        @countsql,
                        N'@COUNTS INT OUTPUT',
                        @count OUTPUT;

                      IF( @count = 0 )
                        BEGIN
                            SET @sql= ( 'UPDATE ' + @MainTable + ' SET Name=''' + @Code
                                        + ''',Remarks=''' + @Description + ''',  
         '
                                        + @DependantTable + '='
                                        + CONVERT(VARCHAR(10), @DependantTableID)
                                        + '  
											,ModifiedBy= '
                                        + CONVERT(VARCHAR(10), @SessionID)
                                        + ' WHERE ID = ' + CONVERT(VARCHAR(10), @ID) + '' )
                            SET @OUTPUT = 'Updated Successfully'
                        END
                      ELSE
                        SET @OUTPUT = 'Already Exists'
                  END
            END

          EXEC(@sql)

          SELECT @OUTPUT

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
