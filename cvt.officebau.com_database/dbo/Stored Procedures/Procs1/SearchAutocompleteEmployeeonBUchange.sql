﻿
/****************************************************************************     
CREATED BY      : SASIREKHA.K   
CREATED DATE  :   26 Feb 2019    
MODIFIED BY   :     
MODIFIED DATE  :     
<summary>      
 [SearchAutocompleteEmployeeonBUchange] 'test',1,1,4   
</summary>                             
*****************************************************************************/
CREATE PROCEDURE [dbo].[SearchAutocompleteEmployeeonBUchange] (@SearchParameter VARCHAR(50),
                                                              @DomainID        INT,
                                                              @UserID          INT,
                                                              @BusinessUnitID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN
              IF( @BusinessUnitID = 0 )
                SELECT TOP 50 ID            AS ID,
                                Isnull(EmpCodePattern, '') + Code + ' - '
                                                                + FullName
                                     AS NAME,
                              ReportingToID AS ReportingID
                FROM   tbl_EmployeeMaster
                WHERE  BaseLocationID IN (SELECT Item
                                          FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                                   FROM   tbl_EmployeeMaster
                                                                   WHERE  id = @UserID
                                                                          AND DomainID = @DomainID), ',')
                                          WHERE  Isnull(Item, '') <> '')
                       AND IsDeleted = 0
                       --AND ReasonID IS NULL    
                       AND HasAccess = 1
                       AND DomainID = @DomainID
                       AND FullName LIKE '%' + @SearchParameter + '%'
					   AND Code NOT LIKE 'TMP_%'  
              ELSE
                BEGIN
                    SELECT TOP 50 ID            AS ID,
                                   Isnull(EmpCodePattern, '') + Code + ' - '
                                                                    + FullName
                                           AS NAME,
                                  ReportingToID AS ReportingID
                    FROM   tbl_EmployeeMaster
                    WHERE  BaseLocationID IN (SELECT Item
                                              FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                                       FROM   tbl_EmployeeMaster
                                                                       WHERE  id = @UserID
                                                                              AND DomainID = @DomainID), ',')
                                              WHERE  Isnull(Item, '') <> '')
                           AND IsDeleted = 0
                           --AND ReasonID IS NULL    
                           AND HasAccess = 1
                           AND DomainID = @DomainID
                           AND FullName LIKE '%' + @SearchParameter + '%'
                           AND BaseLocationID = @BusinessUnitID
						    AND Code NOT LIKE 'TMP_%' 
                END
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
