﻿          
/****************************************************************************             
CREATED BY   : Jeeva            
CREATED DATE  :             
MODIFIED BY   :             
MODIFIED DATE  :             
 <summary>             
 Getaccountsdashboardtiledetails 1          
 </summary>                                     
 *****************************************************************************/          
CREATE PROCEDURE [dbo].[GetAccountsDashBoardTileDetails] (@DomainID INT)          
AS          
  BEGIN          
      SET NOCOUNT ON;          
          
      BEGIN TRY          
          BEGIN TRANSACTION          
          
          DECLARE @PaidAmount           MONEY=(SELECT Isnull(Sum(PaidAmount), 0)          
                    FROM   tblexpense          
                    WHERE  isdeleted = 0          
                           AND DomainID = @DomainID) +(Select Isnull(Sum(Amount), 0)          
                    FROM   tblexpenseholdings         
                    WHERE  isdeleted = 0          
                           AND DomainID = @DomainID AND ExpenseId IN (Select ID from    tblexpense          
                    WHERE  isdeleted = 0          
                           AND DomainID = @DomainID) ),          
                       
                  @ExpenseDetailsAmount MONEY = (SELECT Isnull(Sum(Amount*Qty), 0) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0)         
                     FROM   tblExpenseDetails          
                     WHERE  isdeleted = 0          
                            AND DomainID = @DomainID),          
                  @Payable              MONEY,          
                  @PayableOverDue       MONEY,          
                  @InVoicePaid          MONEY,          
                  @BookedAmount         MONEY,          
                  @Recivables           MONEY,          
                  @RecivablesOverDue    MONEY          
          DECLARE @Table TABLE          
            (          
               Amount MONEY          
            )          
          DECLARE @TableWithDue TABLE          
            (          
               Amount MONEY          
            )          
          
          INSERT INTO @Table          
          SELECT ( CASE          
                     WHEN ( tblInvoice.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)+Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0)) - ( Isnull(( Sum(Qty * Rate) * tblInvoice.DiscountPercentage ), 0) ), 0) )          
                                                                       FROM   tblInvoiceItem          
                                                                       WHERE  InvoiceID = tblInvoice.ID          
                                                                              AND IsDeleted = 0          
                                                                              AND DomainID = @DomainID)          
                     ELSE (SELECT ( Isnull(( Sum(Qty * Rate)+Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0)) - Isnull(tblInvoice.DiscountValue, 0) ), 0) )          
                           FROM   tblInvoiceItem          
                           WHERE  InvoiceID = tblInvoice.ID          
                                  AND IsDeleted = 0          
                                  AND DomainID = @DomainID)          
                   END ) AS value          
          FROM   tblinvoice          
          WHERE  IsDeleted = 0          
                 AND DomainID = @DomainID          
          
          INSERT INTO @TableWithDue          
          SELECT ( CASE          
                     WHEN ( tblInvoice.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)+Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0)) - ( Isnull(( Sum(Qty * Rate) * tblInvoice.DiscountPercentage ), 0) ), 0) )          
                                                                       FROM   tblInvoiceItem          
                                                                       WHERE  InvoiceID = tblInvoice.ID          
                                                                              AND IsDeleted = 0          
                                                                    AND DomainID = @DomainID)          
                     ELSE (SELECT ( Isnull(( Sum(Qty * Rate)+Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0)) - Isnull(tblInvoice.DiscountValue, 0) ), 0) )          
                           FROM   tblInvoiceItem          
                WHERE  InvoiceID = tblInvoice.ID          
                         AND IsDeleted = 0          
                                  AND DomainID = @DomainID)          
                   END ) AS value          
          FROM   tblinvoice          
          WHERE IsDeleted = 0          
                 AND DomainID = @DomainID          
                 AND Duedate < Getdate()          
          
          SET @Payable= @ExpenseDetailsAmount - @PaidAmount          
          SET @PaidAmount =(SELECT Isnull(Sum(PaidAmount), 0)          
                            FROM   tblexpense          
                            WHERE  isdeleted = 0          
                                   AND DomainID = @DomainID          
                                   AND DueDate < Getdate())  +(Select Isnull(Sum(Amount), 0)          
                    FROM   tblexpenseholdings         
                    WHERE  isdeleted = 0          
                           AND DomainID = @DomainID AND ExpenseId IN (Select ID from    tblexpense          
                    WHERE  isdeleted = 0          
                           AND DomainID = @DomainID))        
                
          SET @ExpenseDetailsAmount = (SELECT Isnull(Sum(Amount*Qty), 0) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0)         
                                       FROM   tblExpenseDetails          
                                       WHERE  isdeleted = 0          
                                              AND DomainID = @DomainID          
                                              AND ExpenseID IN (SELECT ID          
                                                                FROM   tblexpense          
                                                                WHERE  isdeleted = 0          
                                                                       AND DueDate < Getdate()))          
          SET @PayableOverDue=@ExpenseDetailsAmount - @PaidAmount          
          ---- Invoice ----          
          SET @InVoicePaid =(SELECT Isnull(Sum(ReceivedAmount), 0)          
                             FROM   tblinvoice          
                             WHERE  isdeleted = 0          
                                    AND DomainID = @DomainID)          
          SET @BookedAmount = (SELECT Isnull(Sum(Amount), 0)          
                               FROM   @Table)          
          SET @Recivables = @BookedAmount - @InVoicePaid          
          SET @InVoicePaid =(SELECT Isnull(Sum(ReceivedAmount), 0)          
                             FROM   tblinvoice          
                             WHERE  isdeleted = 0          
                                    AND DomainID = @DomainID          
                                    AND Duedate < Getdate())          
          SET @BookedAmount = (SELECT Isnull(Sum(Amount), 0)          
                               FROM   @TableWithDue)          
          SET @RecivablesOverDue = @BookedAmount - @InVoicePaid          
          
          SELECT @Payable           AS Payable,          
                 @PayableOverDue    AS PayableOverDue,          
                 @Recivables        AS Recivables,          
                 @RecivablesOverDue AS RecivablesOverDue          
          
          COMMIT TRANSACTION          
      END TRY          
      BEGIN CATCH          
          ROLLBACK TRANSACTION          
          DECLARE @ErrorMsg    VARCHAR(100),          
                  @ErrSeverity TINYINT          
          SELECT @ErrorMsg = Error_message(),          
                 @ErrSeverity = Error_severity()          
          RAISERROR(@ErrorMsg,@ErrSeverity,1)          
      END CATCH          
  END
