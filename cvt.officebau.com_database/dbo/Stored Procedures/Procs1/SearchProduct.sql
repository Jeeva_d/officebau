﻿/****************************************************************************           
CREATED BY   : Jeeva          
CREATED DATE  :           
MODIFIED BY   : SIVA.B        
MODIFIED DATE  : 28-11-2016          
 <summary>        
 SearchProduct asdasdasdsa,1004,1,1          
 select * from tblproduct        
 </summary>                                   
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchProduct] (@Name            VARCHAR(100),  
                                       @TransactionType INT,  
                                       @DomainID        INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT pd.ID                 AS ID,  
                 pd.NAME               AS NAME,  
                 pd.Rate               AS Rate,  
                 pd.Manufacturer       AS Manufacturer,  
                 cd.Code               AS TransactionTypeName,  
                 cm.Code               AS TypeName,  
                 pd.TypeID             AS TypeID,  
                 pd.TransactionTypeID  AS TransactionTypeID,  
                 pd.ProductDescription AS Remarks,  
                 CASE  
                   WHEN ( pd.TypeID <> (SELECT ID  
                                        FROM   tbl_CodeMaster  
                                        WHERE  Code = 'Service') ) THEN( (SELECT Isnull(Sum(QTY), 0)  
                                                                          FROM   tblExpenseDetails eit  
                                                                          WHERE  pd.ID = eit.LedgerID  
                                                                                 AND DomainID = @DomainID  
                                                                                 AND IsDeleted = 0 and IsLedger=0) - (SELECT Isnull(Sum(QTY), 0)  
                                                                                                       FROM   tblInvoiceItem it  
                                                                                                       WHERE  pd.ID = it.ProductID  
                                                                                                              AND DomainID = @DomainID  
                                                                                                              AND IsDeleted = 0) + Isnull(pd.OpeningStock, 0) )  
                 END                   AS AvailableStock,  
                 GH.Code               AS HsnCode,  
                 GH.SGST,  
                 GH.CGST,  
                 GH.IGST,  
                 pd.LedgerId,  
                 ld.Name   As LedgerName  
          FROM   tblproduct pd  
                 LEFT JOIN tbl_CodeMaster cd  
                        ON cd.ID = pd.TransactionTypeID  
                 LEFT JOIN tbl_CodeMaster cm  
                        ON cm.ID = pd.TypeID  
                 LEFT JOIN tblGstHSN GH  
                        ON pd.GstHSNID = GH.ID  
                 LEFT JOIN tblLedger ld  
                        ON ld.ID = pd.LedgerId  
          WHERE  pd.NAME LIKE '%' + Isnull(@Name, '') + '%'  
                 AND ( pd.TransactionTypeID = @TransactionType  
                        OR @TransactionType = 0 )  
                 AND pd.DomainID = @DomainID  
                 AND pd.IsDeleted = 0  
          ORDER  BY pd.ModifiedON DESC  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
