﻿/****************************************************************************       
CREATED BY   : Naneeshwar      
CREATED DATE  :   21-Nov-2016    
MODIFIED BY   :     Dhanalakshmi  
MODIFIED DATE  :     30-03-17  
 <summary>    
 [Searchincome] 1,'Apr 01 2019 00:00:00','Mar 31 2020 00:00:00 ','','All','EqualTo',NULL,0  
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchincome] (@DomainID        INT,  
                                      @StartDate       VARCHAR(50),  
                                      @EndDate         VARCHAR(50),  
                                      @CustomerName    VARCHAR(50),  
                                      @Status          VARCHAR(50),  
                                      @Notations       VARCHAR(50),  
                                      @Amount          MONEY,  
                                      @RevenueCenterID INT=0)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          IF( @StartDate != '' )  
            SET @StartDate = Cast(@StartDate AS DATETIME)  
  
          IF( @EndDate != '' )  
            SET @EndDate = Cast(@EndDate AS DATETIME)  
  
          SELECT inv.ID                                            AS ID,  
                 inv.CustomerID                                    AS CustomerID,  
                 cus.NAME                                          AS CustomerName,  
                 inv.Date                                          AS InvoiceDate,  
                 inv.DueDate                                       AS DueDate,  
                 inv.InvoiceNo                                     AS InvoiceNo,  
                 inv.ReceivedAmount                                AS ReceivedAmount,  
                 inv.RevenueCenterID                               AS RevenueCenterID,  
                 (SELECT Substring((SELECT + ',' + it.ItemDescription  
                                    FROM   tblInvoiceItem it  
                                    WHERE  it.IsDeleted = 0  
                                           AND inv.ID = it.InvoiceID  
                                    FOR XML PATH('')), 2, 200000)) AS [Description],  
                 ( CASE  
                     WHEN ( inv.DiscountPercentage <> 0 ) THEN  
                       (SELECT ( Isnull(Sum(Qty * Rate)  
                                        + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum(Qty * Rate) * inv.DiscountPercentage ), 0) ), 0) )  
                        FROM   tblInvoiceItem  
                        WHERE  InvoiceID = inv.ID  
                               AND IsDeleted = 0  
                               AND DomainID = @DomainID)  
                     ELSE  
                       (SELECT ( Isnull(( Sum(Qty * Rate)  
                                          + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(inv.DiscountValue, 0) ), 0) )  
                        FROM   tblInvoiceItem  
                        WHERE  InvoiceID = inv.ID  
                               AND IsDeleted = 0  
                               AND DomainID = @DomainID)  
                   END )                                           AS TotalAmount,  
                 cd.Code                                           AS Status,  
                 inv.Type                                          AS [Type],  
                 inv.ModifiedOn  ,
				 CAST(fu.Id AS VARCHAR(50)) AS HistoryId
          INTO   #TempIncome  
          FROM   tblInvoice inv  
                 JOIN tblCustomer cus  
                   ON cus.ID = inv.CustomerID  
                 LEFT JOIN tbl_CodeMaster cd  
                        ON cd.ID = inv.StatusID  
                 LEFT JOIN tblCostCenter cc  
                        ON cc.ID = inv.RevenueCenterID  
				Left join tbl_FileUpload fu
						on fu.ID = inv.HistoryID
          WHERE  inv.IsDeleted = 0  
                 AND inv.DomainID = @DomainID  
                 AND ( ( Isnull(@Status, '') = ''  
                         AND inv.StatusID IN(SELECT ID  
     FROM   tbl_CodeMaster  
                                 WHERE  Code != 'Close'  
                                                    AND IsDeleted = 0) )  
                        OR ( Isnull(@Status, '') != 'All'  
                             AND inv.StatusID IN(SELECT ID  
                                                 FROM   tbl_CodeMaster  
                                                 WHERE  [Type] = @Status  
                                                        AND IsDeleted = 0) )  
                        OR ( Isnull(@Status, '') = 'All'  
                             AND inv.StatusID IN(SELECT ID  
                                                 FROM   tbl_CodeMaster  
                                                 WHERE  Type IN( 'Open', 'Partial', 'Close' )  
                                                        AND IsDeleted = 0) ) )  
                 AND ( Isnull(@CustomerName, '') = ''  
                        OR cus.NAME LIKE '%' + @CustomerName + '%' )  
                 AND ( ( @StartDate IS NULL  
                          OR @StartDate = '' )  
                        OR [Date] >= @StartDate )  
                 AND ( ( @EndDate IS NULL  
                          OR @EndDate = '' )  
                        OR [Date] <= @EndDate )  
                 AND (( Isnull(@RevenueCenterID, '') = ''  
                         OR inv.RevenueCenterID = @RevenueCenterID ))  
          ORDER  BY inv.ModifiedOn DESC  
  
          SELECT *  
          FROM   #TempIncome  
          WHERE  ( Isnull(@Amount, 0) = 0 )  
                  OR ( Isnull(@Notations, '') = 'EqualTo'  
                       AND TotalAmount = Isnull(@Amount, 0) )  
                  OR ( Isnull(@Notations, '') = 'GreaterThanEqual'  
                       AND TotalAmount >= @Amount )  
                  OR ( Isnull(@Notations, '') = 'LessThanEqual'  
                       AND TotalAmount <= @Amount )  
          ORDER  BY InvoiceDate DESC  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END  