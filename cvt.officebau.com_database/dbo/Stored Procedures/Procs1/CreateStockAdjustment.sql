﻿/****************************************************************************         
CREATED BY    : Priya K      
CREATED DATE  : 20-Nov-2018      
MODIFIED BY   :        
MODIFIED DATE :        
 <summary>        

 </summary>                                 
 *****************************************************************************/

create PROCEDURE [dbo].[CreateStockAdjustment] (
					@ID         INT = 0,
                                       @LedgerID         INT,
                                       @AdjustedDate        DATETIME,
                                       @ReasonID    INT,
                                       @Remarks              VARCHAR(100) = '',
                                       @DomainID           INT,
									   @SessionID          INT,
                                       @StockAdjustmentItem   StockAdjustmentItem READONLY)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT               VARCHAR(100) = 'Operation Failed!.',
                  @AdjustmentID         INT
                 
         

          IF ( @ID = 0)
				 BEGIN
				  INSERT INTO tbl_InventoryStockAdjustment
							 (LedgerID,
							 AdjustedDate,
							 ReasonID,
							 Remarks,
							 DomainID,
							  CreatedBy,
							  ModifiedBy)
				  VALUES     (@LedgerID,
							 @AdjustedDate,
							 @ReasonID,
							 @Remarks,
							 @DomainID,
							 @SessionID,
							 @SessionID)
				  SET @AdjustmentID =@@IDENTITY

				  SET @OUTPUT= (SELECT [Message]
                        FROM   tblErrorMessage
                        WHERE  [Type] = 'Information'
                               AND Code = 'RCD_INS'
                               AND IsDeleted = 0) --'Inserted Successfully'   
		  END
		  ELSE
		  BEGIN
			SET @OUTPUT= 'Already Exist.'  
		  END

		 if(@OUTPUT like '%Successfully%')
		  BEGIN
          IF( (SELECT Count(1)
               FROM   @StockAdjustmentItem) > 0 )
            BEGIN
                INSERT INTO tbl_InventoryStockAdjustmentItems
                            (AdjustmentID,
                             ProductID,
                             AdjustedQty,
							 RemainingQty,
                             DomainID,
                             CreatedBy,
                             ModifiedBy)
                SELECT     @AdjustmentID,
                            ItemProductID,
                             AdjustedQty,
							 RemainingQty,
                             @DomainID,
                            @SessionID,
                            @SessionID
                FROM   @StockAdjustmentItem
                WHERE  IsDeleted = 0
                       AND ItemProductID <> 0
            END
			END
           
        
          SELECT @OUTPUT 

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
