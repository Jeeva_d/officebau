﻿/****************************************************************************   
CREATED BY   : Dhanalakshmi  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>   
[ManageApplicationAccountConfiguration] 'CURNY' ,44,1,4  
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageApplicationAccountConfiguration] (@Code      VARCHAR(100),
                                                               @Value     VARCHAR(100),
                                                               @SessionID INT,
                                                               @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(1000)

      BEGIN TRY
          BEGIN TRANSACTION

          IF( (SELECT Count(1)
               FROM   tblApplicationConfiguration
               WHERE  DomainID = @DomainID
                      AND Code = @Code) = 0 )
            BEGIN
                INSERT INTO tblApplicationConfiguration
                            (Code,
                             Value,
                             DomainID)
                VALUES      (@Code,
                             @Value,
                             @DomainID )

                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Information'
                                      AND Code = 'RCD_INS'
                                      AND IsDeleted = 0) --'Inserted Successfully.'  
            END
          ELSE
            BEGIN
                UPDATE tblApplicationConfiguration
                SET    Value = @Value,
                       ModifiedBy = @SessionID,
                       ModifiedOn = Getdate()
                WHERE  Code = @Code
                       AND DomainID = @DomainID

                SET @Output = (SELECT [Message]
                               FROM   tblErrorMessage
                               WHERE  [Type] = 'Information'
                                      AND Code = 'RCD_UPD'
                                      AND IsDeleted = 0) --'Updated Successfully.'  
                GOTO Finish
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
