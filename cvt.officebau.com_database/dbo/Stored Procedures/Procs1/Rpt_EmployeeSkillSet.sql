﻿/****************************************************************************             
CREATED BY   : Ajith N            
CREATED DATE  : 31 Aug 2018            
MODIFIED BY   :             
MODIFIED DATE  :             
 <summary>                    
   [Rpt_EmployeeSkillSet]  '', '', '', null, 1,1            
 </summary>                                      
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Rpt_EmployeeSkillSet] (@SkillsID      INT,    
                                                @DesignationID INT,    
                                                @BusinessUnit  VARCHAR(100),    
                                                @IsActive      BIT,    
                                                @UserID        INT,    
                                                @DomainID      INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          DECLARE @BusinessUnitIDs VARCHAR(50)    
    
          IF( Isnull(@BusinessUnit, 0) = 0 )    
            SET @BusinessUnitIDs = (SELECT BusinessUnitID    
                                    FROM   tbl_EmployeeMaster    
                                    WHERE  ID = @UserID    
                                           AND DomainID = @DomainID)    
          ELSE    
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','    
    
          DECLARE @BusinessUnitTable TABLE    
            (    
               BusinessUnit VARCHAR(100)    
            )    
    
          INSERT INTO @BusinessUnitTable    
          SELECT @BusinessUnitIDs    
    
          SELECT Isnull(EM.EmpCodePattern, '') + EM.Code          AS [Employee Code],    
                 EM.FullName      AS [Employee Name],    
                 DESI.NAME        AS [Designation],    
                 SK.NAME          AS [Skills],    
                 CM.Code          AS [Level],    
                 CONVERT(VARCHAR(10), ES.[Year]) + ' Year(s)'    
                 + ' ' + CONVERT(VARCHAR(10), ES.[Month])    
                 + ' Month(s)'    AS [Experience],    
                 Y.NAME           AS [Last used year],    
                 ES.[Description] AS [Description],    
                 ( CASE    
                     WHEN EM.IsActive = 0 THEN 'Active'    
                     ELSE 'Inactive'    
                   END )          AS [Status],    
                 BU.NAME          AS [Business Unit]    
          FROM   tbl_EmployeeMaster EM    
                 LEFT JOIN tbl_Designation DESI    
                        ON DESI.ID = EM.DesignationID    
                 LEFT JOIN tbl_BusinessUnit BU    
                        ON BU.ID = EM.BaseLocationID    
                 LEFT JOIN tbl_EmployeeSkills ES    
                        ON ES.EmployeeID = EM.ID    
                           AND ES.IsDeleted = 0    
                 LEFT JOIN tbl_Skills SK    
                        ON SK.ID = ES.SkillsID    
                 LEFT JOIN tbl_CodeMaster CM    
                        ON CM.ID = ES.LevelID    
                 LEFT JOIN tbl_Year Y    
                        ON Y.ID = ES.LastUsedYearID    
                 JOIN @BusinessUnitTable TBU    
                   ON TBU.BusinessUnit LIKE '%,'    
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))    
                                            + ',%'    
          WHERE  EM.IsDeleted = 0    
                 AND ( @BusinessUnit = 0    
                        OR EM.BaseLocationID = @BusinessUnit )    
                 AND ( @IsActive IS NULL    
                        OR EM.IsActive = @IsActive )    
                 AND EM.Code NOT LIKE 'TMP_%'    
                 AND EM.domainID = @DomainID    
                 AND ( Isnull(@SkillsID, '') = ''    
                        OR ES.SkillsID = @SkillsID )    
                 AND ( Isnull(@DesignationID, '') = ''    
                        OR DESI.ID = @DesignationID )    
           ORDER  BY EM.Code  
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT;    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
