﻿/****************************************************************************     
CREATED BY    :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE :   
 <summary>    
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteExpense] (@ID            INT,
                                       @DomainID      INT,
                                       @PaymentModeID INT,
                                       @SessionID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT                 VARCHAR(100),
                  @ExpenseDtlHistoryID    VARCHAR(MAX),
                  @ExpenseItmHistoryID    VARCHAR(MAX),
                  @ExpenseHistoryID       VARCHAR(MAX) = (SELECT HistoryID
                     FROM   tblExpense
                     WHERE  ID = @ID),
                  @ExpensePayHistoryID    VARCHAR(MAX) = (SELECT ep.HistoryID
                     FROM   tblExpensePaymentMapping epm
                            JOIN tblExpensePayment ep
                              ON ep.ID = epm.ExpensePaymentID
                     WHERE  epm.ExpenseID = @ID
                            AND epm.DomainID = @DomainID),
                  @ExpensePayMapHistoryID VARCHAR(MAX) = (SELECT HistoryID
                     FROM   tblExpensePaymentMapping
                     WHERE  ExpenseID = @ID
                            AND DomainID = @DomainID)
          DECLARE @DetailsHistoryIDTable TABLE
            (
               ID        INT IDENTITY(1, 1),
               HistoryID VARCHAR(Max)
            )
          DECLARE @ItemsHistoryIDTable TABLE
            (
               ID        INT IDENTITY(1, 1),
               HistoryID VARCHAR(Max)
            )

          BEGIN
              IF EXISTS (SELECT 1
                         FROM   tblbrs
                         WHERE  SourceID = (SELECT ep.ID
                                            FROM   tblexpensepayment ep
                                                   LEFT JOIN tblExpensepaymentmapping em
                                                          ON ep.id = em.expensePaymentID
                                            WHERE  ep.isdeleted = 0
                                                   AND em.expenseID = @id
                                                   AND ep.DomainID = @DomainID)
                                AND sourcetype = (SELECT ID
                                                  FROM   tblMasterTypes
                                                  WHERE  NAME = 'Expense')
                                AND IsDeleted = 0
                                AND isreconsiled = 1
                                AND DomainID = @DomainID)
                BEGIN
                    SET @Output = (SELECT [Message]
                                   FROM   tblErrorMessage
                                   WHERE  [Type] = 'Warning'
                                          AND Code = 'RCD_REF'
                                          AND IsDeleted = 0) --'The record is referred.'  

                    GOTO finish
                END
              ELSE
                BEGIN
                    IF( @PaymentModeID = (SELECT ID
                                          FROM   tbl_CodeMaster
                                          WHERE  Code = 'Cash'
                                                 AND IsDeleted = 0) )
                      BEGIN
                          UPDATE tblCashBucket
                          SET    AvailableCash = Isnull(AvailableCash
                                                        + (SELECT Amount
                                                           FROM   tblExpensePaymentmapping
                                                           WHERE  ExpenseID = @ID
                                                                  AND IsDeleted = 0
                                                                  AND DomainID = @DomainID), 0)
                          WHERE  @DomainID = DomainID

                          UPDATE tblVirtualCash
                          SET    IsDeleted = 1
                          WHERE  SourceID = (SELECT ep.ID
                                             FROM   tblexpensepayment ep
                                                    LEFT JOIN tblExpensepaymentmapping em
                                                           ON ep.id = em.expensePaymentID
                                             WHERE  ep.isdeleted = 0
                                                    AND em.expenseID = @id
                                                    AND ep.DomainID = @DomainID)
                                 AND SourceType = (SELECT ID
                                                   FROM   tblMasterTypes
                                                   WHERE  NAME = 'Expense')
                                 AND IsDeleted = 0
                                 AND DomainID = @DomainID
                      END

                    UPDATE tblExpense
                    SET    IsDeleted = 1
                    WHERE  @ID = ID
                           AND DomainID = @DomainID

                    UPDATE tblBookedBankBalance
                    SET    IsDeleted = 1
                    WHERE  BRSID = (SELECT ID
                                    FROM   tblBRS
                                    WHERE  SourceID = (SELECT ep.ID
                                                       FROM   tblexpensepayment ep
                                                              LEFT JOIN tblExpensepaymentmapping em
                                                                     ON ep.id = em.expensePaymentID
                                                       WHERE  ep.isdeleted = 0
                                                              AND em.expenseID = @id
                                                              AND ep.DomainID = @DomainID)
                                           AND SourceType = (SELECT ID
                                                             FROM   tblMasterTypes
                                                             WHERE  NAME = 'Expense')
                                           AND IsDeleted = 0
                                           AND DomainID = @DomainID)
                           AND DomainID = @DomainID

                    UPDATE tblBRS
                    SET    IsDeleted = 1
                    WHERE  SourceID = (SELECT ep.ID
                                       FROM   tblexpensepayment ep
                                              LEFT JOIN tblExpensepaymentmapping em
                                                     ON ep.id = em.expensePaymentID
                                       WHERE  ep.isdeleted = 0
                                              AND em.expenseID = @id
                                              AND ep.DomainID = @DomainID)
                           AND DomainID = @DomainID

                    EXEC Managesystembankbalance
                      @DomainID

                    UPDATE tblExpensePaymentmapping
                    SET    IsDeleted = 1
                    WHERE  @ID = ExpenseID
                           AND DomainID = @DomainID

                    UPDATE tblExpensePayment
                    SET    IsDeleted = 1
                    WHERE  ID = (SELECT ExpensePaymentID
                                 FROM   tblExpensePaymentmapping
                                 WHERE  expenseID = @ID
                                        AND DomainID = @DomainID)
                           AND DomainID = @DomainID

                    UPDATE tblExpenseDetails
                    SET    IsDeleted = 1
                    WHERE  ExpenseID = @ID
                           AND DomainID = @DomainID

                    INSERT INTO @DetailsHistoryIDTable
                                (HistoryID)
                    SELECT HistoryID
                    FROM   tblExpenseDetails
                    WHERE  ExpenseID = @ID

                    ----Delete item to Inventory
                    EXEC ManageInventory
                      @ID,
                      'DELETE',
                      'Purchase',
                      @SessionID,
                      @DomainID
                END

              SET @Output = (SELECT [Message]
                             FROM   tblErrorMessage
                             WHERE  [Type] = 'Information'
                                    AND Code = 'RCD_DEL'
                                    AND IsDeleted = 0) --'Deleted Successfully'  
          END

          FINISH:

          EXEC Managebusinesseventslog
            @ExpenseHistoryID,
            'IsDeleted',
            'tblExpense'

          EXEC Managebusinesseventslog
            @ExpensePayHistoryID,
            'IsDeleted',
            'tblExpensePayment'

          EXEC Managebusinesseventslog
            @ExpensePayMapHistoryID,
            'IsDeleted',
            'tblExpensePaymentMapping'

          DECLARE @TempCount INT = 1,
                  @Count     INT = (SELECT Count(1)
                     FROM   @DetailsHistoryIDTable)

          WHILE @Count >= @TempCount
            BEGIN
                SET @ExpenseDtlHistoryID= (SELECT HistoryID
                                           FROM   @DetailsHistoryIDTable
                                           WHERE  ID = @TempCount)

                EXEC Managebusinesseventslog
                  @ExpenseDtlHistoryID,
                  'IsDeleted',
                  'tblExpenseDetails'

                SET @TempCount = @TempCount + 1
            END

          DECLARE @TempCountItem INT = 1,
                  @CountItem     INT = (SELECT Count(1)
                     FROM   @ItemsHistoryIDTable)

          WHILE @CountItem >= @TempCountItem
            BEGIN
                SET @ExpenseItmHistoryID = (SELECT HistoryID
                                            FROM   @ItemsHistoryIDTable
                                            WHERE  ID = @TempCountItem)
                SET @TempCountItem = @TempCountItem + 1
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
