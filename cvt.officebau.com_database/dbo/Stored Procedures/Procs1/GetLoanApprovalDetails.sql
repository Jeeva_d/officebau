﻿/****************************************************************************     
CREATED BY   :  Naneeshwar.M  
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>   
         [GetLoanApprovalDetails] 1,12,106  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetLoanApprovalDetails] (@DomainID   INT,  
                                                @StatusID   INT,  
                                                @EmployeeID INT,  
                                                @ID         INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT LR.ID                                       AS ID,  
                 LR.StatusID                                 AS StatusID,  
                 LR.CreatedBy                                AS RequesterID,  
                 STA.Code                                    AS [Status],  
                 LR.CreatedOn                                AS RequestedOn,  
                 LR.Purpose                                  AS Purpose,  
                 LR.LoanTypeID                               AS TypeID,  
                 LoanAmount                                  AS Amount,  
                 Isnull(EMP.EmpCodePattern, '') + Isnull(EMP.Code, '') + ' - ' + EMP.FullName AS Requester,  
                 LR.CreatedBy                                AS RequesterID,  
                 LR.Tenure                                   AS Tenure,  
                 LR.InterestRate                             AS RateofInterest,  
                 LR.TotalAmount                              AS TotalAmount,  
                 LR.ApproverRemarks                          AS ApprovedReason,  
                 LR.ApproverID                               AS ApproverID,  
                 EM.FullName                                 AS ModifiedBy,  
                 LR.ApprovedDate                             AS ModifiedOn,  
                 BU.NAME                                     AS BaseLocation,  
                 STA.Code                                    AS ApproverStatus,  
                 LR.ExpectedDate                             AS ExpectedDate  
          FROM   tbl_LoanRequest LR  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = LR.CreatedBy  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = LR.ApproverID  
                 LEFT JOIN tbl_Status STA  
                        ON STA.ID = LR.ApproverStatusID  
                 LEFT JOIN tbl_BusinessUnit BU  
                        ON EM.BaseLocationID = BU.ID  
          WHERE  LR.ApproverID = @EmployeeID  
                 AND LR.IsDeleted = 0  
                 AND LR.ID = @ID  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
