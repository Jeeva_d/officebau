﻿/****************************************************************************         
CREATED BY		: Dhanalakhsmi. S    
CREATED DATE	: 27-OCT-2017
MODIFIED BY		:     
MODIFIED DATE   :       
 <summary>                
 [GetRegionBasedOnEmplpyeeAccess] 2,2,0,''
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetRegionBasedOnEmplpyeeAccess] (@EmployeeID INT,
                                                        @DomainID   INT,
                                                        @regionID   INT,
                                                        @Type       VARCHAR(50))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          IF ( @Type = '' )
            BEGIN
                IF ( @regionID = 0 )
                  BEGIN
                      SELECT ID,
                             NAME
                      FROM   tbl_BusinessUnit
                      WHERE  id IN(SELECT DISTINCT BU.ParentID
                                   FROM   tbl_BusinessUnit BU
                                          JOIN (SELECT *
                                                FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                                         FROM   tbl_EmployeeMaster
                                                                         WHERE  ID = @EmployeeID
                                                                                AND DomainID = @DomainID), ',')
                                                WHERE  Isnull(Item, '') <> '') i
                                            ON i.Item = BU.ID
                                   WHERE  BU.DomainID = @DomainID)
                      ORDER  BY NAME ASC
                  END
                ELSE
                  BEGIN
                      SELECT DISTINCT BU.ID,
                                      bu.NAME
                      FROM   tbl_BusinessUnit BU
                             JOIN (SELECT *
                                   FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                            FROM   tbl_EmployeeMaster
                                                            WHERE  ID = @EmployeeID
                                                                   AND DomainID = @DomainID), ',')
                                   WHERE  Isnull(Item, '') <> '') i
                               ON i.Item = BU.ID
                      WHERE  BU.DomainID = @DomainID
                             AND BU.ParentID = @regionID
                      ORDER  BY NAME ASC
                  END
            END
          ELSE
            BEGIN
                SELECT com.ID,
                       com.NAME + ' ( ' + bu.NAME + ' )' AS NAME
                FROM   tbl_CompanyPayStructure com
                       LEFT JOIN tbl_BusinessUnit bu
                              ON bu.ID = com.BusinessUnitID
                WHERE  BusinessUnitID IN(SELECT DISTINCT BU.ParentID
                                         FROM   tbl_EmployeeMaster C
                                                LEFT JOIN tbl_BusinessUnit BU
                                                       ON C.BaseLocationID = bu.ID
                                                JOIN (SELECT *
                                                      FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                                               FROM   tbl_EmployeeMaster
                                                                               WHERE  ID = @EmployeeID
                                                                                      AND DomainID = @DomainID), ',')
                                                      WHERE  Isnull(Item, '') <> '') i
                                                  ON i.Item = BU.ID
                                         WHERE  com.Isdeleted = 0
                                                AND com.DomainID = @DomainID)
                ORDER  BY NAME ASC
            END

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
