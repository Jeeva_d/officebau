﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	05-01-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
		Check Emailid exist in Auth_RequestTrial table
		select * from Auth_WebLogin
		select * from Auth_Users
		[Auth_ValidateRequestTrialEmailId] 'test@test.com'
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_ValidateRequestTrialEmailId]
 (
		@Input			VARCHAR(50)
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY		
	DECLARE @Output INT = 0

		IF EXISTS(SELECT 1 FROM Auth_RequestTrial WHERE EmailID = @Input)
			SET @Output = 1
		ELSE
			SET @Output = 0
	
	SELECT	@Output AS Result
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
