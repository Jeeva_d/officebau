﻿/****************************************************************************     
CREATED BY   : Naneeshwar.M    
CREATED DATE  : 20-Jun-2017    
MODIFIED BY   : Ajith N    
MODIFIED DATE  : 30 Nov 2017    
 <summary>            
        
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Createclaimfinancialapproval] (@ID              INT,  
                                                      @ApproverID      INT,  
                                                      @SettlerID       INT,  
                                                      @Remarks         VARCHAR(8000),  
                                                      @RequesterItemID INT,  
                                                      @Status          VARCHAR(50),  
                                                      @CreatedBy       INT,  
                                                      @IsDeleted       INT,  
                                                      @ModifiedBy      INT,  
                                                      @DomainID        INT,  
                                                      @Amount          MONEY)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output      VARCHAR(1000),  
              @ClaimStatus INT = (SELECT ID  
                 FROM   tbl_Status  
                 WHERE  Code = @Status  
                        AND Type = 'Claims'  
                        AND IsDeleted = 0)  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          BEGIN  
              --- Insert/Update Claims Financial Approve    
              IF( @ID = 0 )  
                BEGIN  
                    INSERT INTO dbo.tbl_ClaimsFinancialApprove  
                                (ApproverID,  
                                 Remarks,  
                                 StatusID,  
                                 SettlerStatusID,  
                                 ClaimItemID,  
                                 CreatedBy,  
                                 CreatedOn,  
                                 ModifiedBy,  
                                 ModifiedOn,  
                                 DomainID,  
                                 FinApprovedAmount)  
                    VALUES      ( @ApproverID,  
                                  @Remarks,  
                                  @ClaimStatus,  
                                  CASE  
                                    WHEN( @Status IN( 'Rejected' ) )THEN ((SELECT ID  
                                                                           FROM   tbl_Status  
                                                                           WHERE  Code = 'Rejected'  
                                                                                  AND Type = 'Claims'  
                                                                                  AND IsDeleted = 0))  
                                    ELSE ((SELECT ID  
                                           FROM   tbl_Status  
                                           WHERE  Code = 'Submitted'  
                                                  AND Type = 'Claims'  
                                                  AND IsDeleted = 0))  
                                  END,  
                                  @RequesterItemID,  
                                  @CreatedBy,  
                                  Getdate(),  
                                  @ModifiedBy,  
                                  Getdate(),  
                                  @DomainID,  
                                  @Amount )  
  
                    IF( @Status = 'Rejected' )  
                      SET @Output='Rejected Successfully.'  
                    ELSE  
                      SET @Output='Approved Successfully.'  
                END  
              ELSE  
                BEGIN  
                    UPDATE tbl_ClaimsFinancialApprove  
                    SET    StatusID = @ClaimStatus,  
                           ApproverID = @ApproverID,  
                           Remarks = @Remarks,  
                           SettlerStatusID = CASE  
                                               WHEN( @Status IN( 'Rejected' ) )THEN ((SELECT ID  
                                                                                      FROM   tbl_Status  
                                                                                      WHERE  Code = 'Rejected'  
                                                                                             AND Type = 'Claims'  
                                                                                             AND IsDeleted = 0))  
                                               ELSE ((SELECT ID  
                                                      FROM   tbl_Status  
                                                      WHERE  Code = 'Submitted'  
                                                             AND Type = 'Claims'  
                                                             AND IsDeleted = 0))  
                                             END,  
                           ClaimItemID = @RequesterItemID,  
                           ModifiedBy = @ModifiedBy,  
                           ModifiedOn = Getdate(),  
                           DomainID = @DomainID,  
                           FinApprovedAmount = @Amount  
                    WHERE  id = @ID  
  
                    IF( @Status = 'Rejected' )  
                      SET @Output='Rejected Successfully.'  
                    ELSE  
                      SET @Output='Approved Successfully.'  
                END  
  
              --- Update Claims Approve Status    
              UPDATE tbl_ClaimsApprove  
              SET    FinApproverStatusID = @ClaimStatus  
              WHERE  ClaimItemID = @RequesterItemID  
  
              --- Update Request Approve Status    
              UPDATE tbl_ClaimsRequest  
              SET    StatusID = @ClaimStatus  
              WHERE  id = @RequesterItemID  
          END  
  
          SELECT @Output AS UserMessage  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
