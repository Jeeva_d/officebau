﻿/****************************************************************************         
CREATED BY		:  Jennifer S
CREATED DATE	:  
MODIFIED BY		:       
MODIFIED DATE   :        
 <summary>  
 [Getrakenodropdown] 1, 1
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetRakeNoDropdown] (@UserID   INT,
                                           @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT ID,
                 TrainNo NAME
          FROM   tbl_TrainDetails tr
          WHERE  IsDeleted = 0
                 AND DomainID = @DomainID
                 AND BusinessUnitID IN ((SELECT Item
                                         FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                                  FROM   tbl_EmployeeMaster
                                                                  WHERE  ID = @UserID), ',')
                                         WHERE  Isnull(Item, '') <> ''))
          ORDER  BY [Date] DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
