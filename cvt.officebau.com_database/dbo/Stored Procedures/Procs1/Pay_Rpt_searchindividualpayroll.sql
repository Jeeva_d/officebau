﻿
/****************************************************************************           
CREATED BY   :    Priya K      
CREATED DATE  :   13 Nov 2018        
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>         
         [Pay_Rpt_searchindividualpayroll] 5      
                 
 </summary>                                   
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_Rpt_searchindividualpayroll] (@EmployeeId INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @cols     AS NVARCHAR(MAX),
                  @query    AS NVARCHAR(MAX),
                  @DomainId INT

          SELECT @DomainId = DomainID
          FROM   tbl_EmployeeMaster
          WHERE  ID = @EmployeeId

          SELECT *
          INTO   #Result
          FROM   VW_Payroll vwpay
          WHERE  vwpay.EmployeeID = @EmployeeId

          SET @cols = Stuff((SELECT ',' + Quotename(description)
                             FROM   VW_Payroll
                             WHERE  DomainId = @DomainId
                             GROUP  BY description,
                                       ComponentId
                             ORDER  BY ComponentId ASC
                             FOR XML path('')), 1, 1, ' ')
          SET @query = 'SELECT       
						  MonthID, Employee_Code as Emp_Code, Employee_Name,                
						 Joining_Date,  Month, YearId as Year, '
                       + @cols + '      
						  from             (                
						 select      
						  MonthID, Employee_Code, Description, Employee_Name, Joining_Date,                   
						  Month, YearId, Amount         
						 from #Result      
           
						 ) x            pivot            (sum(Amount)                   
						 for Description in (' + @cols
                       + ')            ) p order by Year desc, MonthID desc'

          EXEC (@query);
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END

