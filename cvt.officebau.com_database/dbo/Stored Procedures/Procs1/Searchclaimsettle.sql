﻿/****************************************************************************       
CREATED BY   :  Naneeshwar.M    
CREATED DATE  :       
MODIFIED BY   :   Ajith N    
MODIFIED DATE  :   05 Dec 2017    
 <summary>     
          [SearchclaimSettle] 1,0,71,'search'    
 </summary>                               
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Searchclaimsettle] (@DomainID      INT,    
                                            @StatusID      INT,    
                                            @EmployeeID    INT,    
                                            @OperationType VARCHAR(20))    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN    
              DECLARE @Approved INT = (SELECT ID    
                 FROM   tbl_Status    
                 WHERE  Code = 'Submitted'    
                        AND [Type] = 'Claims');    
              --IF( @StatusID = (SELECT ID    
              --                 FROM   tbl_Status    
              --                 WHERE  Code = 'Approved'    
              --                        AND [Type] = 'Claims') )    
              --  SET @StatusID = 0;    
              DECLARE @Status VARCHAR(100)    
    
              IF( @OperationType = 'pageload' )    
                BEGIN    
                    SET @Status = 'Submitted'    
                    SET @StatusID = (SELECT ID    
                                     FROM   tbl_Status    
                                     WHERE  Code = 'Submitted'    
                                            AND [Type] = 'Claims'    
                                            AND IsDeleted = 0);    
                END    
              ELSE IF( @OperationType = 'search'    
                  AND @StatusID = 0 )    
                SET @Status = 'Submitted,Approved,Settled';    
              ELSE IF( @StatusID = (SELECT ID    
                               FROM   tbl_Status    
                               WHERE  Code = 'Approved'    
                                      AND [Type] = 'Claims'    
                                      AND IsDeleted = 0) )    
                SET @StatusID = (SELECT ID    
                                 FROM   tbl_Status    
                                 WHERE  Code = 'Submitted'    
                                        AND [Type] = 'Claims'    
                                        AND IsDeleted = 0);    
    
              WITH CTE    
                   AS (SELECT Cast(CLA.CreatedOn AS DATE)                 AS ApprovedOn,    
                              Cast(CLR.SubmittedDate AS DATE)             AS SubmittedDate,    
                              Cast(CFA.CreatedOn AS DATE)                 AS FinApprovedDate,    
                              CFA.finApprovedAmount                       AS Amount,    
                              Isnull(CRE.EmpCodePattern, '') + Isnull(CRE.Code, '') + ' - ' + CRE.FullName AS ApprovarName,    
                              Isnull(EMP.EmpCodePattern, '') + Isnull(EMP.Code, '') + ' - ' + EMP.FullName AS Requester,    
                              Isnull(FAR.EmpCodePattern, '') + Isnull(FAR.Code, '') + ' - ' + FAR.FullName AS FinApprovarName,    
                              CLA.CreatedBy                               AS ApprovalID,    
                              CLA.RequesterID                             AS RequesterID,    
                              S.Code                                      AS Status,    
                              CFA.SettlerStatusID                         AS StatusID    
                       FROM   tbl_ClaimsRequest CLR    
                              LEFT JOIN tbl_ClaimsSettle CLS    
                                     ON CLS.ClaimItemID = CLR.ID    
                              LEFT JOIN tbl_ClaimsFinancialApprove CFA    
                                     ON CLR.ID = CFA.ClaimItemID    
                              LEFT JOIN tbl_ClaimsApprove CLA    
                                     ON CLA.ClaimItemID = CLR.ID    
                              LEFT JOIN tbl_EmployeeMaster CRE    
                                     ON CRE.ID = CLA.CreatedBy    
                              LEFT JOIN tbl_EmployeeMaster EMP                                       ON EMP.ID = CLR.CreatedBy    
                              LEFT JOIN tbl_EmployeeMaster FAR    
                                     ON FAR.ID = CFA.CreatedBy    
                              LEFT JOIN tbl_Status S    
                                     ON S.ID = CLR.StatusID    
                       WHERE  CLR.IsDeleted = 0    
                              AND ( ( Isnull(@StatusID, 0) = 0    
                                      AND CFA.SettlerStatusID IN((SELECT ID    
                                                                  FROM   tbl_Status    
                                                                  WHERE  Code IN(SELECT Splitdata    
                                                                                 FROM   FnSplitString(@Status, ','))    
                                                                         AND [Type] = 'Claims'    
                                                                         AND IsDeleted = 0)) )    
                                     OR CFA.SettlerStatusID = @StatusID )    
                              AND CLR.DomainID = @DomainID)    
              SELECT ApprovedOn                               AS ApprovedOn,    
                     SubmittedDate                            AS SubmittedDate,    
                     FinApprovedDate                          AS FinApprovedDate,    
                     Sum(Amount)                              AS Amount,    
                     Requester                                AS Requester,    
                     ApprovarName                             AS ApprovarName,    
                     --FinApprovarName                          AS FinApprovarName,    
                     ApprovalID                               AS ApprovalID,    
                     RequesterID                              AS RequesterID,    
                     Count(1)                                 AS Counts,    
                     Status                                   AS Status,    
                     StatusID                                 AS StatusID,    
                     Cast(ROW_NUMBER()    
                            OVER(    
                              ORDER BY Requester ASC) AS INT) AS RowNo    
              FROM   CTE    
              GROUP  BY ApprovedOn,    
                        SubmittedDate,    
                        FinApprovedDate,    
                        Requester,    
                        ApprovarName,    
                        --FinApprovarName,    
                        ApprovalID,    
                        RequesterID,    
                        Status,    
                        StatusID    
          END    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
