﻿/****************************************************************************     
CREATED BY  :   Dhanalakshmi.S  
CREATED DATE :   20-SEP-2017  
MODIFIED BY  :  Ajith N   
MODIFIED DATE :   13 Aoct 2017  
 <summary>  
  [SearchMyAttendance] 1463,1, '2017-09-14'  
  
  To be Rework based on Employee ID  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchMyAttendance] (@EmployeeID INT,
                                            @DomainID   INT,
                                            @LogDate    DATETIME)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @BiometricCode VARCHAR(20) = (SELECT BiometricCode
                     FROM   tbl_EmployeeMaster
                     WHERE  ID = @EmployeeID),
                  @LeaveStatus   INT = (SELECT ID
                     FROM   tbl_Status
                     WHERE  [Type] = 'Leave'
                            AND Code = 'Approved')

          SELECT *
          INTO   #AttendencePunchDetails
          FROM   (SELECT TAT.EmployeeID,
                         TAT.[LogDate],
                         TAT.Duration                           PT,
                         CONVERT(VARCHAR(5), bl.PunchTime, 108) AS punchTime,
                         ''                                     AS Duration,
                         BL.Direction
                  FROM   tbl_Attendance TAT
                         JOIN tbl_BiometricLogs BL
                           ON BL.AttendanceId = TAT.ID
                  -- AND BL.LogType <> 'OfficeBAU'  
                  WHERE  TAT.IsDeleted = 0
                         AND TAT.LogDate = @LogDate
                         AND TAT.DomainID = @DomainID
                         AND TAT.EmployeeID = @EmployeeID
                  --AND TAT.LeaveDuration IS NULL  
                  UNION ALL
                  SELECT EmployeeID,
                         FromDate,
                         '',
                         CONVERT(VARCHAR(5), Duration, 108) + ' ' + 'hrs',
                         '',
                         'Permission'
                  FROM   tbl_EmployeeLeave
                  WHERE  IsDeleted = 0
                         AND FromDate = @LogDate
                         AND ToDate = @LogDate
                         AND EmployeeID = @EmployeeID
                         -- AND ISNULL(IsPermission, 'false') = 'true'  
                         AND DomainID = @DomainID
                         AND Duration IS NOT NULL
                  UNION ALL
                  SELECT EL.EmployeeID,
                         ELM.LeaveDate AS LogDate,
                         '',
                         CASE
                           WHEN ( ISNULL(EL.IsHalfDay, 'false') = 'true' ) THEN
                             (SELECT TOP 1 CONVERT(VARCHAR(5), CONVERT(TIME, Replace(H.HalfDay, '.', ':')), 108)
                                           + ' hrs'
                              FROM   tbl_BusinessHours H
                              WHERE  H.IsDeleted = 0
                                     AND H.RegionID = ISNULL(EM.RegionID, 0)
                                     AND H.DomainID = @DomainID)
                           WHEN ( ISNULL(EL.IsHalfDay, 'false') = 'false' ) THEN
                             (SELECT TOP 1 CONVERT(VARCHAR(5), CONVERT(TIME, Replace(F.FullDay, '.', ':')), 108)
                                           + ' hrs'
                              FROM   tbl_BusinessHours F
                              WHERE  F.IsDeleted = 0
                                     AND F.RegionID = ISNULL(EM.RegionID, 0)
                                     AND F.DomainID = @DomainID)
                           ELSE
                             ''
                         END,
                         '',
                         LT.NAME -- CL, PL & SL   
                  FROM   tbl_EmployeeLeave EL
                         LEFT JOIN tbl_EmployeeLeaveDateMapping ELM
                                ON EL.ID = ELM.LeaveRequestID
                                   AND ELM.IsDeleted = 0
                         LEFT JOIN tbl_EmployeeMaster EM
                                ON EM.ID = EL.EmployeeID
                         LEFT JOIN tbl_LeaveTypes LT
                                ON EL.LeaveTypeID = LT.ID
                  WHERE  EL.IsDeleted = 0
                         AND EL.Duration IS NULL
                         AND ELM.LeaveDate = @LogDate
                         AND ELM.EmployeeID = @EmployeeID
                         AND EL.StatusID = @LeaveStatus
                         AND EL.DomainID = @DomainID) AS a

          ---------------- Missed Check In & Check Out---------------------  
          SELECT a.punchTime,
                 em.ID,
                 CASE
                   WHEN ( em.ID IS NOT NULL ) THEN
                     'Missed ' + CM.Code
                   ELSE
                     a.Direction
                 END AS [Description]
          FROM   #AttendencePunchDetails a
                 LEFT JOIN tbl_EmployeeMissedPunches em
                        ON a.[LogDate] = em.PunchDate
                           AND CONVERT(VARCHAR(5), em.PunchTime, 108) = a.punchTime
                           AND em.EmployeeID = a.EmployeeID
                           AND a.Direction <> 'Permission'
                 LEFT JOIN tbl_CodeMaster CM
                        ON CM.ID = em.PunchTypeID

          DROP TABLE #AttendencePunchDetails
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
