﻿/****************************************************************************       
CREATED BY   : Jeeva      
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>    
 [Getproduct] 2,1,1      
 select * from tblledger    
 </summary>                               
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[SearchLedger] (@DomainID INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SELECT pd.ID             AS ID,    
                 pd.NAME           AS NAME,    
                 pd.Remarks        AS Remarks,    
                 pd.IsExpense      AS IsExpense,    
                 pd.OpeningBalance AS OpeningBalance,    
                 GL.NAME           AS GroupNAME,  
                 pd.LedgerType     AS LedgerType,
                 GH.Code   AS HsnCode,        
                 GH.SGST,        
                 GH.CGST,    
                 GH.IGST  
          FROM   tblledger pd    
                 JOIN tblGroupLedger GL    
                   ON pd.GroupID = GL.ID
                   LEFT JOIN tblGstHSN GH      
                        ON pd.GstHSNID = GH.ID      
          WHERE  pd.DomainID = @DomainID    
                 AND pd.IsDeleted = 0    
          ORDER  BY pd.ModifiedON DESC    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
