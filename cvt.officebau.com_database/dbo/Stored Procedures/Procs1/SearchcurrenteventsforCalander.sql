﻿
/****************************************************************************     
CREATED BY      :     
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>      
[SearchcurrenteventsforCalander] 5 ,01,2019  
 </summary>                             
 *****************************************************************************/

CREATE PROCEDURE [dbo].[SearchcurrenteventsforCalander]
(@EmployeeID INT, 
 @Month      INT, 
 @Year       INT
)
AS
     BEGIN
         SET NOCOUNT ON;
         BEGIN TRY
             BEGIN
                 DECLARE @DomainID INT=
                 (
                     SELECT DomainID
                     FROM tbl_EmployeeMaster
                     WHERE id = @EmployeeID
                 );
                 SELECT '  '+FullName+'-'+E.Code AS title, 
                        CAST(CAST(CAST(@Year AS VARCHAR)+'-'+CAST(@Month AS VARCHAR)+'-'+CAST(DATEPART(dd, dateofbirth) AS VARCHAR) AS DATE) AS VARCHAR) AS [start], 
                        CAST(CAST(CAST(@Year AS VARCHAR)+'-'+CAST(@Month AS VARCHAR)+'-'+CAST(DATEPART(dd, dateofbirth) AS VARCHAR) AS DATE) AS VARCHAR) AS [end], 
                        'birthday-cake' AS icon, 
                        'Green' AS color
                 FROM tbl_EmployeePersonalDetails
                      JOIN tbl_EmployeeMaster E ON EmployeeID = E.ID
                 WHERE DATEPART(mm, dateofbirth) = @month
                       AND e.IsActive = 0
                       AND e.IsDeleted = 0
                       AND e.Code NOT LIKE '%TMP_%'
                       AND e.DomainID = @DomainID
                 UNION ALL
                 SELECT '  '+FullName+'-'+E.Code AS title, 
                        CAST(CAST(CAST(@Year AS VARCHAR)+'-'+CAST(@Month AS VARCHAR)+'-'+CAST(DATEPART(dd, Anniversary) AS VARCHAR) AS DATE) AS VARCHAR) AS [start], 
                        CAST(CAST(CAST(@Year AS VARCHAR)+'-'+CAST(@Month AS VARCHAR)+'-'+CAST(DATEPART(dd, Anniversary) AS VARCHAR) AS DATE) AS VARCHAR) AS [end], 
                        'diamond' AS icon, 
                        'blue' AS color
                 FROM tbl_EmployeePersonalDetails
                      JOIN tbl_EmployeeMaster E ON EmployeeID = E.ID
                 WHERE DATEPART(mm, Anniversary) = @month
                       AND e.Code NOT LIKE '%TMP_%'
                       AND e.IsActive = 0
                       AND e.IsDeleted = 0
                       AND e.DomainID = @DomainID
                 UNION ALL
                 SELECT Type+' Holiday ' AS title, 
                        CAST(CAST(CAST(@Year AS VARCHAR)+'-'+CAST(@Month AS VARCHAR)+'-'+CAST(DATEPART(dd, date) AS VARCHAR) AS DATE) AS VARCHAR) AS [start], 
                        CAST(CAST(CAST(@Year AS VARCHAR)+'-'+CAST(@Month AS VARCHAR)+'-'+CAST(DATEPART(dd, date) AS VARCHAR) AS DATE) AS VARCHAR) AS [end], 
                        '' AS icon, 
                        'Red' AS color
                 FROM tbl_Holidays
                 WHERE DATEPART(mm, date) = @month
                       AND DATEPART(YY, date) = @Year
                       AND IsDeleted = 0
                       AND DomainID = @DomainID
                 UNION ALL
                 SELECT 'Leave taken' AS title, 
                        CAST(fromdate AS VARCHAR) AS [start], 
                        CAST(Todate AS VARCHAR) AS [end], 
                        '' AS icon, 
                        'Orange' AS color
                 FROM tbl_EmployeeLeave
                 WHERE DATEPART(mm, fromdate) = @month
                       AND DATEPART(YY, fromdate) = @Year
                       AND IsDeleted = 0
                       AND DomainID = @DomainID;
             END;
         END TRY
         BEGIN CATCH
             DECLARE @ErrorMsg VARCHAR(100), @ErrSeverity TINYINT;
             SELECT @ErrorMsg = ERROR_MESSAGE(), 
                    @ErrSeverity = ERROR_SEVERITY();
             RAISERROR(@ErrorMsg, @ErrSeverity, 1);
         END CATCH;
     END;
