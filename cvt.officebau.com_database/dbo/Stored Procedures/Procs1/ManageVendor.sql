﻿/****************************************************************************       
CREATED BY   : Dhanalakshmi      
CREATED DATE  :   04/11/2016       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>       
  [ManageVendor]       
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageVendor] (@ID              INT,  
                                      @Name            VARCHAR(100),  
                                      @Address         VARCHAR(1000),  
                                      @CityID          INT,  
                                      @PaymentTerms    VARCHAR(100),  
                                      @ContactNo       VARCHAR(20),  
                                      @ContactPerson   VARCHAR(50),  
                                      @ContactPersonNo VARCHAR(20),  
                                      @EmailID         VARCHAR(100),  
                                      @VendorEmailID   VARCHAR(100),  
                                      @CurrencyID      INT,  
                                      @BillingAddress  VARCHAR(1000),  
                                      @PanNo           VARCHAR(500),  
                                      @TanNo           VARCHAR(500),  
                                      @TinNo           VARCHAR(500),  
                                      @CSTNo           VARCHAR(500),  
                                      @Remarks         VARCHAR(1000),  
                                      @HasDeleted      BIT,  
                                      @SessionID       INT,  
                                      @DomainID        INT,  
                                      @GSTNo           VARCHAR(50))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(1000)  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          IF( @HasDeleted = 1 )  
            BEGIN  
                IF EXISTS (SELECT 1  
                           FROM   tblExpense  
                           WHERE  VendorID = @ID  
                                  AND IsDeleted = 0  
                                  AND DomainID = @DomainID)  
                  BEGIN  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Warning'  
                                            AND Code = 'RCD_REF'  
                                            AND IsDeleted = 0) --'The record is referred.'      
                      GOTO finish  
                  END  
                ELSE  
                  BEGIN  
                      UPDATE tblVendor  
                      SET    IsDeleted = 1  
                      WHERE  ID = @ID  
                             AND DomainID = @DomainID  
  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Information'  
                                            AND Code = 'RCD_DEL'  
                                            AND IsDeleted = 0) --'Deleted Successfully'      
                  END  
            END  
          ELSE  
            BEGIN  
                IF( @ID = 0 )  
                  BEGIN  
                      IF( (SELECT Count(1)  
                           FROM   tblVendor  
                           WHERE  NAME = @Name  
                                  AND DomainID = @DomainID  
                                  AND IsDeleted = 0) = 0 )  
                        BEGIN  
                            INSERT INTO tblVendor  
                                        (NAME,  
                                         Address,  
                                         CityID,  
                                         PaymentTerms,  
                                         ContactNo,  
                                         ContactPerson,  
                     ContactPersonNo,  
                                         EmailID,  
                                         VendorEmailID,  
                                         CurrencyID,  
                                         BillingAddress,  
                                         PanNo,  
                                         TanNo,  
                                         TinNo,  
                                         CSTNo,  
                                         GSTNo,  
                                         Remarks,  
                                         CreatedBy,  
                                         ModifiedBy,  
                                         DomainID)  
                            VALUES      (@Name,  
                                         @Address,  
                                         @CityID,  
                                         Isnull(@PaymentTerms, 0),  
                                         @ContactNo,  
                                         @ContactPerson,  
                                         @ContactPersonNo,  
                                         @EmailID,  
                                         @VendorEmailID,  
                                         ( CASE  
                                             WHEN @CurrencyID IS NULL THEN  
                                               (SELECT Value  
                                                FROM   tblApplicationConfiguration  
                                                WHERE  Code = 'CURNY'  
                                                       AND DomainID = @DomainID)  
                                             ELSE  
                                               ( @CurrencyID )  
                                           END ),  
                                         @BillingAddress,  
                                         @PanNo,  
                                         @TanNo,  
                                         @TinNo,  
                                         @CSTNo,  
                                         @GSTNo,  
                                         @Remarks,  
                                         @SessionID,  
                                         @SessionID,  
                                         @DomainID)  
  
                            DECLARE @RefID INT = @@IDENTITY  
  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Information'  
                                                  AND Code = 'RCD_INS'  
                                                  AND IsDeleted = 0)  
                                          + '/' + Cast(@RefID AS VARCHAR(100)) --'Inserted Successfully/'    
                        END  
                      ELSE  
                        BEGIN  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Warning'  
                                                  AND Code = 'RCD_EXIST'  
                                                  AND IsDeleted = 0) --'Already Exists'      
                        END  
                  END  
                ELSE  
                  BEGIN  
                      IF( (SELECT Count(1)  
                           FROM   tblVendor  
                           WHERE  NAME = @Name  
                                  AND DomainID = @DomainID  
                                  AND ID <> @ID  
                                  AND IsDeleted = 0) = 0 )  
                        BEGIN  
                            UPDATE tblVendor  
                            SET    NAME = @Name,  
                                   [Address] = @Address,  
                                   CityID = @CityID,  
                                   PaymentTerms = @PaymentTerms,  
                                   ContactNo = @ContactNo,  
                                   ContactPerson = @ContactPerson,  
                                   ContactPersonNo = @ContactPersonNo,  
                                   EmailID = @EmailID,  
                                   VendorEmailID = @VendorEmailID,  
                                   CurrencyID = ( CASE  
                                                    WHEN @CurrencyID IS NULL THEN  
                                                      (SELECT Value  
                                                       FROM   tblApplicationConfiguration  
                                                       WHERE  Code = 'CURNY'  
                                                              AND DomainID = @DomainID)  
                                                    ELSE  
                                                      ( @CurrencyID )  
                                                  END ),  
                                   BillingAddress = @BillingAddress,  
                                   PanNo = @PanNo,  
                                   TanNo = @TanNo,  
                                   TinNo = @TinNo,  
                                   CSTNo = @CSTNo,  
                                   GSTNo = @GSTNo,  
                                   Remarks = @Remarks,  
                                   ModifiedBY = @SessionID,  
                                   ModifiedOn = Getdate()  
                            WHERE  ID = @ID  
                                   AND DomainID = @DomainID  
  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Information'  
                                                  AND Code = 'RCD_UPD'  
                                                  AND IsDeleted = 0) --'Updated Successfully'      
                        END  
                      ELSE  
                        BEGIN  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Warning'  
                                                  AND Code = 'RCD_EXIST'  
                                                  AND IsDeleted = 0) --'Already Exists'      
                        END  
                  END  
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
