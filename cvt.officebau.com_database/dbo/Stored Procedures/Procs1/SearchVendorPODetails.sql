﻿/****************************************************************************             
CREATED BY    :             
CREATED DATE  :             
MODIFIED BY   :          
MODIFIED DATE :             
 <summary>   
 [SearchVendorPODetails] 91,1                 
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchVendorPODetails] (@VendorID INT,
                                               @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT p.ID,
                 PONo                                                                                                                                                               AS BillNo,
                 ( ( QTY - BilledQTY ) * Amount ) + ( ( ( ( ( QTY - BilledQTY ) * Amount ) / 100 ) * SGSTPercent ) + ( ( ( ( QTY - BilledQTY ) * Amount ) / 100 ) * SGSTPercent ) ) Amount,
                 p.CostCenterID                                                                                                                                                     AS CostCenterID
          INTO   #Result
          FROM   tblPurchaseOrder p
                 JOIN tblPODetails po
                   ON po.POID = p.ID
          WHERE  p.IsDeleted = 0
                 AND VendorID = @VendorID
                 AND StatusID <> (SELECT ID
                                  FROM   tbl_CodeMaster
                                  WHERE  IsDeleted = 0
                                         AND [Type] = 'Close')

          SELECT ID,
                 BillNo,
                 Sum(Amount) AS Amount,
				 CostCenterID
          FROM   #Result
          GROUP  BY ID,
                    BillNo,
					CostCenterID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
