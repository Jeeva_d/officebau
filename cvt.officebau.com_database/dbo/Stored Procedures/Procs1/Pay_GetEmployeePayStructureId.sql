﻿/****************************************************************************                  
CREATED BY      :                  
CREATED DATE  :                  
MODIFIED BY   :                  
MODIFIED DATE  :                  
<summary>                  
        [Pay_SearchEmployeePayroll] 2,54,1,'1,2,3,4',9,9                  
</summary>                  
*****************************************************************************/      
CREATE PROCEDURE [dbo].[Pay_GetEmployeePayStructureId] (@MonthId  INT,      
                                                   @Year     INT,      
                                                   @DomainID INT,      
                                                   @EmployeeId  INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
   
      BEGIN TRY      
          BEGIN      
            
      
              SELECT ID
              FROM   tbl_pay_employeepaystructure E      
              WHERE  effectivefrom <= Dateadd(d, -1, Dateadd(m, Datediff(m, 0, Dateadd(year, @Year - 2000, Dateadd( month, @MonthId - 1, '20000101')))      
                                                                + 1, 0))      
                     AND E.effectivefrom = (SELECT Max(EffectiveFrom)      
                                            FROM   tbl_pay_employeepaystructure      
                                            WHERE  employeeID = E.Employeeid      
                                                   AND isdeleted = 0)      
                     AND E.Isdeleted = 0      
                     AND E.DomainID = @DomainID     and E.EmployeeID=@EmployeeId  
          END      
      END TRY      
      BEGIN CATCH      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR (@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
