﻿/****************************************************************************             
CREATED BY  :  Ajith N        
CREATED DATE :  25 Jan 2018  
MODIFIED BY  :            
MODIFIED DATE   :       
 <summary>                    
    GetCompleteLeaveReportList '03-Oct-2017','31-Jan-2018',0,0, 0, 1,106  
 </summary>                                     
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetCompleteLeaveReportList] (@FromDate       DATE,  
                                                    @Todate         DATE,  
                                                    @StatusID       INT,  
                                                    @EmployeeID     INT,  
                                                    @BusinessUnitID INT,  
                                                    @DomainID       INT,  
                                                    @UserID         INT)  
AS  
  BEGIN  
      BEGIN TRY  
          DECLARE @BusinessUnitTable TABLE  
            (  
               BusinessUnitID INT  
            )  
  
          INSERT INTO @BusinessUnitTable  
          SELECT Splitdata  
          FROM   FnSplitString((SELECT BusinessUnitID  
                                FROM   tbl_EmployeeMaster  
                                WHERE  ID = @UserID), ',')  
          WHERE  Splitdata <> 0  
  
          SELECT Isnull(EM.EmpCodePattern, '') + EM.Code                  AS EmployeeCode,  
                 EM.Id                    AS EmployeeId,  
                 EM.FUllname              AS EmployeeName,  
                 EL.FromDate              AS FromDate,  
                 el.ToDate                AS ToDate,  
                 EL.IsHalfDay             AS IsHalfDay,  
                 CASE  
                   WHEN EL.IsHalfDay = 'true' THEN  
                     0.5  
                   ELSE  
                     1  
                 END                      AS Total,  
                 isnull(EL.Reason, '')    AS Remarks,  
                 TS.Code                  AS [Status],  
                 LT.NAME                  AS [Type],  
                 BT.NAME                  AS BusinessUnit,  
                 EL.id                    AS LeaveRequestID,  
                 REG.NAME                 AS Region,  
                 ROW_NUMBER()  
                   OVER(  
                     ORDER BY (SELECT 1)) AS Rowno  
          INTO   #tempLeaveList  
          FROM   tbl_EmployeeLeave EL  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = el.EmployeeID  
                 LEFT JOIN tbl_EmployeeLeaveDateMapping ELM  
                        ON EL.ID = ELM.LeaveRequestID  
                 LEFT JOIN tbl_BusinessUnit BT  
                        ON EM.BaseLocationID = BT.ID  
                 LEFT JOIN tbl_Status TS  
                        ON EL.StatusID = TS.ID  
                 LEFT JOIN tbl_LeaveTypes LT  
                        ON EL.LeaveTypeID = LT.ID  
                 LEFT JOIN tbl_BusinessUnit REG  
                        ON REG.ID = BT.ParentID  
          WHERE  El.IsDeleted = 0  
                 AND ( ( @BusinessUnitID = 0  
                         AND EM.BaseLocationID IN (SELECT BusinessUnitID  
                                                   FROM   @BusinessUnitTable) )  
                        OR EM.BaseLocationID = @BusinessUnitID )  
                 AND ( @StatusID = 0  
                        OR EL.StatusID = @StatusID )  
                 AND ( @EmployeeID = 0  
                        OR EL.EmployeeID = @EmployeeID )  
                 AND EL.FromDate >= @FromDate  
                 AND EL.ToDate <= @Todate  
                 AND EL.DomainID = @DomainID  
                 AND El.LeaveTypeID NOT IN (SELECT id  
                                            FROM   tbl_LeaveTypes  
                                            WHERE  IsDeleted = 0  
                                                   AND DomainID = @DomainID  
                                                   AND NAME LIKE '%Permission%')  
  
          SELECT EmployeeCode,  
                 EmployeeId,  
                 LL.EmployeeName,  
                 LL.FromDate,  
                 LL.ToDate,  
                 LL.IsHalfDay,  
                 (SELECT Sum(Total) AS Total  
                  FROM   #tempLeaveList lC  
                  WHERE  LL.EmployeeId = lC.EmployeeId)              AS Total,  
                 (SELECT Sum(Total) AS Total  
                  FROM   #tempLeaveList lCl  
                  WHERE  LL.EmployeeId = lCl.EmployeeId  
                         AND ll.LeaveRequestID = lCl.LeaveRequestID) AS NoOfDays,  
                 LL.Remarks,  
                 LL.[Status],  
                 LL.[Type],  
                 LL.BusinessUnit,  
                 LL.Region  
          FROM   #tempLeaveList LL  
          GROUP  BY LL.EmployeeCode,  
                    LL.EmployeeId,  
                    LL.EmployeeName,  
                    LL.FromDate,  
                    LL.ToDate,  
                    LL.IsHalfDay,  
                    LL.Total,  
                    LL.Remarks,  
                    LL.[Status],  
                    LL.[Type],  
                    LL.BusinessUnit,  
                    ll.LeaveRequestID,  
                    LL.Region  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
