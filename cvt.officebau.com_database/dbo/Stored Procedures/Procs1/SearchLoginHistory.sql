﻿/****************************************************************************       
CREATED BY   :      
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>     
          [SearchLoginHistory] '','',1    
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchLoginHistory] (@FromDate DATETIME,    
                                            @ToDate   DATETIME,    
                                            @DomainID INT)    
AS    
  
BEGIN  
    SET NOCOUNT ON;  
  
    BEGIN TRY  
        SELECT TOP(500) LH.ID                           AS ID,  
                        (SELECT Count(1)  
                         FROM   tbl_LoginForms  
                         WHERE  LoginHistoryID = LH.ID) AS [Count],  
                        Isnull(EM.EmpCodePattern, '') + EM.Code + ' - ' + EM.FullName   AS EmployeeName,  
                        LH.LoginTime                    AS [Login Time],--Utc to Local time    
                        LH.BrowserType                  AS [Browser Name],  
                        LH.IPAddress                    AS [IP Address],  
                        LH.LogOutTime                   AS [LogOut Time],  
                        location                        AS Location,  
                        LatitudeLongitude               AS LatitudeLongitude  
        --  Duration As Duration    
        FROM   tbl_LoginHistory LH  
               JOIN tbl_EmployeeMaster EM  
                 ON LH.EmployeeID = EM.ID  
        WHERE  LH.LoginTime >= CASE  
                                 WHEN ( Isnull(@FromDate, '') = '' ) THEN LH.LoginTime  
                                 ELSE @FromDate  
                               END  
               AND LH.LoginTime <= CASE  
                                     WHEN ( Isnull(@ToDate, '') = '' ) THEN LH.LoginTime  
                                     ELSE @ToDate  
                                   END  
               AND LH.domainID = @DomainID  
        ORDER  BY LH.LoginTime DESC  
    END TRY  
    BEGIN CATCH  
        DECLARE @ErrorMsg    VARCHAR(100),  
                @ErrSeverity TINYINT  
        SELECT @ErrorMsg = Error_message(),  
               @ErrSeverity = Error_severity()  
        RAISERROR(@ErrorMsg,@ErrSeverity,1)  
    END CATCH  
END
