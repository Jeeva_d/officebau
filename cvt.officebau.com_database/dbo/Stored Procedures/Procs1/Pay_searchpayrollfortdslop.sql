﻿/****************************************************************************   
CREATED BY      : Naneeshwar.M  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
<summary>    
 [Pay_SearchPayrollforTDSLOP] 1,8,1,'4,2'  
</summary>                           
*****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_searchpayrollfortdslop] (@MonthId     INT,  
                                                    @Year        INT,  
                                                    @DomainID    INT,  
                                                    @Location    VARCHAR(100))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @TDS VARCHAR(max)='',  
              @LOP VARCHAR(max)=''  
      DECLARE @query VARCHAR(max)  
      DECLARE @TDSYear INT  
  
      IF( @MonthID BETWEEN 1 AND 3 )  
        SET @TDSYear=( (SELECT FY.NAME  
                        FROM   tbl_FinancialYear FY  
                        WHERE  FY.NAME = (SELECT NAME  
                                          FROM   tbl_FinancialYear  
                                          WHERE  id = @Year  
                                                 AND isdeleted = 0  
                                                 AND domainid = @DomainID)  
                               AND FY.isdeleted = 0  
                               AND FY.domainid = @DomainID) - 1 )  
      ELSE  
        SET @TDSYear =(SELECT FY.NAME  
                       FROM   tbl_FinancialYear FY  
                       WHERE  FY.NAME = (SELECT NAME  
                                         FROM   tbl_FinancialYear  
                                         WHERE  id = @Year  
                                                AND isdeleted = 0  
                                                AND domainid = @DomainID)  
                              AND FY.isdeleted = 0  
                              AND FY.domainid = @DomainID)  
  
      SET @Year = (SELECT NAME  
                   FROM   tbl_FinancialYear  
                   WHERE  id = @Year  
                          AND isdeleted = 0  
                          AND domainid = @DomainID)  
  
      DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'  
        + CONVERT(VARCHAR(10), @Year)  
      DECLARE @DataSource TABLE  
        (  
           ID      INT IDENTITY(1, 1),  
           [Value] NVARCHAR(128)  
        )  
  
      INSERT INTO @DataSource  
                  ([Value])  
      SELECT Item  
      FROM   dbo.Splitstring (@Location, ',')  
      WHERE  Isnull(Item, '') <> ''  
  
      DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth));  
  
      SET @TDS ='( SELECT TOP 1 1 FROM tbl_TDS where monthID = '  
                + Cast (@MonthId AS VARCHAR(10))  
                + ' and DomainID = '  
                + Cast (@DomainID AS VARCHAR(10))  
                + '  and isdeleted=0  and yearID = '  
                + Cast ((SELECT ID FROM tbl_FinancialYear WHERE NAME = @Year AND isdeleted = 0 AND DomainID=@DomainID) AS VARCHAR(10))  
                + '   
       and baselocationid IN (SELECT Item FROM dbo.Splitstring ('''  
                + Cast(@Location AS VARCHAR(50))  
                + ''', '','')  
         WHERE  Isnull(Item, '''') <> '''')  
          AND (SELECT COUNT(Item) FROM   dbo.Splitstring ('''  
                + Cast(@Location AS VARCHAR(50))  
                + ''', '','')  
          WHERE  Isnull(Item, '''') <> '''')   
         = (SELECT COUNT(DISTINCT baselocationid) FROM tbl_TDS   
          where monthID = '  
                + Cast( @MonthId AS VARCHAR(50))  
                + '  and yearID = '  
                + Cast( (SELECT ID FROM tbl_FinancialYear WHERE NAME = @Year AND isdeleted = 0 AND domainid = @DomainID) AS VARCHAR(50))  
                + ' and isdeleted=0  and DomainID = '  
                + Cast (@DomainID AS VARCHAR(10))  
                + ' and baselocationid IN (SELECT Item  
        FROM   dbo.Splitstring ('''  
   + Cast(@Location AS VARCHAR(50))  
                + ''', '','')  
        WHERE  Isnull(Item, '''') <> '''')) )'  
      SET @Lop ='( SELECT TOP 1 1 FROM tbl_lopdetails L left JOIn tbl_employeemaster EM on EM.ID=L.EmployeeID where monthID = '  
                + Cast (@MonthId AS VARCHAR(10))  
                + '  and isdeleted=0  and year = '  
                + Cast ((SELECT NAME FROM tbl_FinancialYear WHERE NAME = @Year AND isdeleted = 0 AND DomainID=@DomainID) AS VARCHAR(10))  
                + '   
       and EM.baselocationid IN (SELECT Item FROM dbo.Splitstring ('''  
                + Cast(@Location AS VARCHAR(50))  
                + ''', '','')  
         WHERE  Isnull(Item, '''') <> '''')  
          AND (SELECT COUNT(Item) FROM   dbo.Splitstring ('''  
                + Cast(@Location AS VARCHAR(50))  
                + ''', '','')  
          WHERE  Isnull(Item, '''') <> '''')   
         = (SELECT COUNT(DISTINCT baselocationid) FROM tbl_lopdetails L left JOIn tbl_employeemaster EM on EM.ID=L.EmployeeID   
          where monthID = '  
                + Cast( @MonthId AS VARCHAR(50))  
                + ' and L.DomainID = '  
                + Cast( @DomainID AS VARCHAR(50))  
                + '  and year = '  
                + Cast( (SELECT NAME FROM tbl_FinancialYear WHERE NAME = @Year AND isdeleted = 0 AND domainid = @DomainID) AS VARCHAR(50))  
                + ' and isdeleted=0 and EM.baselocationid IN (SELECT Item  
        FROM   dbo.Splitstring ('''  
                + Cast(@Location AS VARCHAR(50))  
                + ''', '','')  
        WHERE  Isnull(Item, '''') <> '''')) )'  
      SET @query =( ' IF(( ISNULL(' + Isnull(@LOP, 0)  
                    + ',0) = 0) AND ( ISNULL(' + Isnull(@TDS, 0)  
                    + ',0) = 0 ) )  
       BEGIN  
        select ''LOP,TDS''  
        END  
      ELSE IF( ISNULL(' + Isnull(@LOP, 0)  
                    + ',0) = 0  )  
       BEGIN  
        select ''LOP''  
        END  
      ELSE IF (ISNULL(' + Isnull(@TDS, 0)  
                    + ',0) = 0  )  
       BEGIN  
        select ''TDS''  
       END  
      ELSE  
       BEGIN  
        select  ''YES''  
       END' )  
  
      CREATE TABLE #RESULT  
        (  
           RESULT VARCHAR(10)  
        )  
  
      INSERT INTO #RESULT  
      EXEC(@query)  
  
      BEGIN TRY  
          BEGIN  
              (SELECT RESULT AS UserMessage  
               FROM   #RESULT)  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
