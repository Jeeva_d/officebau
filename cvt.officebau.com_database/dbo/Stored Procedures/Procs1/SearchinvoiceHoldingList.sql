﻿/****************************************************************************               
CREATED BY   :               
CREATED DATE  :            
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>            
[SearchinvoiceHoldingList] 1,1         
 </summary>                                       
 *****************************************************************************/            
CREATE PROCEDURE [dbo].[SearchinvoiceHoldingList] (@InvoiceID INT,            
                                                @DomainID  INT)            
AS            
  BEGIN            
      SET NOCOUNT ON;            
            
      BEGIN TRY            
          BEGIN TRANSACTION            
            
         Select l.Name As LedgerName, i.InvoiceNo, (SELECT ( Isnull(( Sum(Qty * Rate) +Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0))- Isnull(i.DiscountValue, 0) ), 0) )          
                            FROM   tblInvoiceItem          
                            WHERE  InvoiceID = i.ID          
                            AND IsDeleted = 0 and DomainID =  @domainID) AS InvoicedAmount,  (SELECT ( Isnull(( Sum(Qty * Rate) +Sum(Isnull(SGSTAmount,0)+Isnull(CGSTAmount,0))- Isnull(i.DiscountValue, 0) ), 0) )          
                            FROM   tblInvoiceItem          
                            WHERE  InvoiceID = i.ID          
                            AND IsDeleted = 0 and DomainID =  @domainID)-(Select ISNULL(Sum(ISNULL(amount,0)),0)        
         from tblInvoicePaymentMapping where InvoiceID=@InvoiceID and IsDeleted=0) As OutstandingAmount,i.ID AS InvoiceID,h.ID,h.date,h.LedgerID,h.Amount from  tblinvoice i         
        left join tblInvoiceHoldings h on i.ID=h.Invoiceid and h.ISdeleted=0     
  Left Join tblLedger l on l.ID=h.LedgerId     
    Where   i.ID=@InvoiceID and i.IsDeleted=0        
          COMMIT TRANSACTION            
      END TRY            
      BEGIN CATCH            
          ROLLBACK TRANSACTION            
          DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT            
          SELECT @ErrorMsg = Error_message(),            
                 @ErrSeverity = Error_severity()            
          RAISERROR(@ErrorMsg,@ErrSeverity,1)            
      END CATCH            
  END
