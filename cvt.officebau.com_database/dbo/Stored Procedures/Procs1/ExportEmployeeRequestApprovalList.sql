﻿    
/****************************************************************************     
CREATED BY   : Ajith N    
CREATED DATE  : 08 Aug 2017    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
   [ExportEmployeeRequestApprovalList]  '' , '', '', '', 'false', 224,1,1    
 </summary>                              
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ExportEmployeeRequestApprovalList] (@EmployeeCode     VARCHAR(20),    
                                                           @TempEmployeeCode VARCHAR(20),    
                                                           @Name             VARCHAR(100),    
                                                           @ContactNo        VARCHAR(20),    
                                                           @IsActive         BIT,    
                                                           @ApproverID       INT,    
                                                           @EmployeeID       INT,    
                                                           @DomainID         INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          DECLARE @BusinessUnitIDs VARCHAR(50) = (SELECT BusinessUnitID    
             FROM   tbl_EmployeeMaster    
             WHERE  ID = @EmployeeID    
                    AND DomainID = @DomainID)    
          DECLARE @BusinessUnitTable TABLE    
            (    
               BusinessUnit VARCHAR(100)    
            )    
    
          INSERT INTO @BusinessUnitTable    
          SELECT @BusinessUnitIDs    
    
          SELECT APPROVAL.TempEmployeeCode                         AS [Temporary No],  
                 EM.Code                                           AS [Employee Code],    
                 --Upper(Substring(REGION.NAME, 1, 3)) + '_'    
                 --+ Upper(Substring(BU.NAME, 1, 3)) + '_' + EM.Code AS [Employee No],    
                 EM.FirstName + ' ' + Isnull(EM.LastName, '')      AS [Name],    
                 DEP.NAME                                          AS [Department],    
                 DESI.NAME                                         AS [Designation],    
     EM.DOJ                                            AS [Date of Joining],    
                 BU.NAME                                           AS [Business Unit],    
                 Isnull(EM.ContactNo, '')                          AS [Contact No],    
                 Isnull(EM.EmergencyContactNo, '')                 AS [Emergency Mobile No],    
                 EM.EmailID                                        AS [Email ID],    
                 EMPLOYEETYPE.NAME                                 AS [Employment Type]    
          FROM   tbl_EmployeeMaster EM    
                 LEFT JOIN tbl_Department DEP    
                        ON DEP.ID = EM.DepartmentID    
                 LEFT JOIN tbl_Designation DESI    
                        ON DESI.ID = EM.DesignationID    
                 LEFT JOIN tbl_BusinessUnit BU    
                        ON BU.ID = EM.BaseLocationID    
                 LEFT JOIN tbl_BusinessUnit REGION    
                        ON REGION.ID = EM.RegionID    
                 LEFT JOIN tbl_EmployementType EMPLOYEETYPE    
                        ON EMPLOYEETYPE.ID = EM.EmploymentTypeID    
                 LEFT JOIN tbl_EmployeeApproval APPROVAL    
                        ON APPROVAL.EmployeeID = EM.ID    
                 JOIN @BusinessUnitTable TBU    
                   ON TBU.BusinessUnit LIKE '%,'    
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))    
                                            + ',%'    
          WHERE  EM.IsDeleted = 0    
                 AND ( Isnull(@EmployeeCode, '') = ''    
                        OR ( EM.Code LIKE '%' + @EmployeeCode + '%' ) )    
                 AND ( Isnull(@TempEmployeeCode, '') = ''    
                        OR EM.Code LIKE '%' + @TempEmployeeCode + '%' )    
                 AND ( Isnull(@Name, '') = ''    
                        OR ( ( EM.FirstName + ' ' + Isnull(EM.LastName, '') ) LIKE '%' + @Name + '%' ) )    
                 AND ( Isnull(@ContactNo, '') = ''    
              OR ( EM.ContactNo LIKE '%' + @ContactNo + '%' ) )    
                 AND ( ( Isnull(@ApproverID, 0) = 0    
                     AND Isnull(APPROVAL.FirstApproverId, 0) = 0 )    
                        OR APPROVAL.FirstApproverId = @ApproverID )    
                 AND ( Isnull(@IsActive, '') = ''    
                        OR Isnull(EM.IsActive, 0) = @IsActive )    
                 AND EM.domainID = @DomainID    
          ORDER  BY EM.FirstName + ' ' + Isnull(EM.LastName, '')    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT;    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
