﻿/****************************************************************************                                                   
CREATED BY    :                                                  
CREATED DATE  :                                                  
MODIFIED BY   :                                               
MODIFIED DATE :                                              
  [Temp_getbalancesheet] 1,'01-feb-2017','28-feb-2017'                                              
  Temp_NewBalancesheet 1,'04/01/2018' ,'03/31/2019'                                            
  Select * from tblAccountReceipts                                                 
                                                                             
 *****************************************************************************/       
CREATE PROCEDURE [dbo].[NewBalancesheet] (@DomainID        INT,       
                                         @SearchStartDate VARCHAR(25),       
                                         @SearchEndDate   VARCHAR(25))       
AS       
  BEGIN       
      BEGIN try       
          SET @SearchStartDate = Cast(@SearchStartDate AS DATETIME)       
          SET @SearchEndDate = Cast(@SearchEndDate AS DATETIME)       
      
          DECLARE @InputGSTGroup  VARCHAR(200),       
                  @OutputGSTGroup VARCHAR(200),       
                  @InputCGST      VARCHAR(200),       
                  @InputSGST      VARCHAR(200),       
                  @OutputCGST     VARCHAR(200),       
                  @OutputSGST     VARCHAR(200)       
      
          SET @InputCGST=(SELECT NAME       
                          FROM   tblledger       
                          WHERE  NAME = 'Input GST-CGST(Purchase)'       
                                 AND domainid = @domainID)       
          SET @InputSGST=(SELECT NAME       
                          FROM   tblledger       
                          WHERE  NAME = 'Input GST-SGST (Purchases)'       
                                 AND domainid = @domainID)       
          SET @OutputCGST=(SELECT NAME       
                           FROM   tblledger       
                           WHERE  NAME = 'Output GST-CGST'       
                                  AND domainid = @domainID)       
          SET @OutputSGST=(SELECT NAME       
                           FROM   tblledger       
                           WHERE  NAME = 'Output GST-SGST'       
                                  AND domainid = @domainID)       
          SET @InputGSTGroup=(SELECT NAME       
                              FROM   tblgroupledger       
                              WHERE  id = (SELECT groupid       
                                           FROM   tblledger       
                                           WHERE  NAME =       
                                          'Input GST-CGST(Purchase)'       
                                                  AND domainid = @domainID))       
          SET @OutputGSTGroup=(SELECT NAME       
                               FROM   tblgroupledger       
                               WHERE  id = (SELECT groupid       
                                            FROM   tblledger       
                                            WHERE  NAME = 'Output GST-SGST'       
                                                   AND domainid = @domainID))       
      
          DECLARE @PL TABLE       
            (       
               id          NVARCHAR(100),       
               groupledger NVARCHAR(100),       
               ledger      NVARCHAR(100),       
               amount      MONEY,       
               NAME        NVARCHAR(100),       
               branchname  NVARCHAR(100),       
               category    VARCHAR(100),       
               [type]      VARCHAR(100)       
            )       
      
          SELECT date,       
                 CASE       
                   WHEN ( ed.isledger = 1 ) THEN l.NAME       
                   ELSE lp.NAME       
                 END AS LEDGER,       
                 CASE       
                   WHEN ( ed.isledger = 1 ) THEN g.NAME       
                   ELSE gp.NAME       
          END               AS GROUPLEDGER,       
                 Sum(qty * amount) AS AMOUNT,       
                 CASE       
                   WHEN ( isledger = 1 ) THEN g.reporttype       
                   ELSE gp.reporttype       
                 END               ReportTypeid,       
                 'Expense'         AS CATEGORY,       
                 ''                NAME,       
                 ''                BranchName,       
                 CASE       
                   WHEN ( ed.isledger = 1 ) THEN l.NAME       
                   ELSE lp.NAME       
                 END               AS ID       
          INTO   #result       
          FROM   tblexpensedetails ed       
                 JOIN tblexpense e       
                   ON ed.expenseid = e.id       
                      AND e.isdeleted = 0       
                 LEFT JOIN tblledger l       
                        ON l.id = ed.ledgerid       
                           AND ed.isledger = 1       
                 LEFT JOIN tblgroupledger g       
                        ON g.id = l.groupid       
                           AND ed.isledger = 1       
                 LEFT JOIN tblproduct_v2 p       
                        ON p.id = ed.ledgerid       
                           AND ed.isledger = 0       
                 LEFT JOIN tblledger lp       
                        ON lp.id = p.PurchaseLedgerId       
                           AND ed.isledger = 0       
                 LEFT JOIN tblgroupledger gp       
                        ON gp.id = lp.groupid       
                           AND ed.isledger = 0       
          WHERE  ed.domainid = @DomainID       
                 AND [date] >= @SearchStartDate       
                 AND [date] <= @SearchEndDate       
                 AND ed.isdeleted = 0       
          GROUP  BY isledger,       
                    l.NAME,       
                    g.NAME,       
                    p.NAME,       
                    g.reporttype,       
                    date,       
                    gp.NAME,       
                    lp.NAME,       
                    gp.reporttype,       
                    l.id,       
                    lp.id       
      
          INSERT INTO #result       
          SELECT date,       
                 lp.NAME,       
                 gp.NAME,       
                 Sum(qty * it.rate),       
                 gp.reporttype,       
                 'income',       
                 '',       
                 '',       
                 lp.NAME       
          FROM   tblinvoiceitem it       
                 JOIN tblinvoice i       
                   ON it.invoiceid = i.id       
                 JOIN tblproduct_v2 p       
                   ON it.productid = p.id       
                 LEFT JOIN tblledger lp       
                        ON lp.id = p.SalesLedgerId       
                 LEFT JOIN tblgroupledger gp       
                        ON gp.id = lp.groupid       
          WHERE  it.domainid = @DomainID       
                 AND [date] >= @SearchStartDate       
                 AND [date] <= @SearchEndDate       
                 AND it.isdeleted = 0       
          GROUP  BY date,       
                    p.NAME,       
                    lp.NAME,       
                    gp.NAME,       
                    gp.reporttype,       
                    lp.id       
      
          INSERT INTO #result       
          SELECT paymentdate,       
                 l.NAME,       
                 g.NAME,       
                 totalamount AS Amount,       
                 g.reporttype,       
                 'Income',       
                 '',       
                 '',       
                 l.NAME       
          FROM   tbl_receipts r       
                 JOIN tblledger l       
                   ON r.ledgerid = l.id       
                 JOIN tblgroupledger g       
           ON g.id = l.groupid       
          WHERE  r.domainid = @DomainID       
                 AND paymentdate >= @SearchStartDate       
                 AND paymentdate <= @SearchEndDate       
                 AND r.isdeleted = 0       
      
          INSERT INTO #result       
          SELECT paymentdate,       
                 l.NAME,     
                 g.NAME,       
                 totalamount AS Amount,       
                 g.reporttype,       
                 'Expense',       
                 '',       
                 '',       
                 l.NAME       
          FROM   tbl_payment r       
                 JOIN tblledger l       
                   ON r.ledgerid = l.id       
                 JOIN tblgroupledger g       
                   ON g.id = l.groupid       
          WHERE  r.domainid = @DomainID       
                 AND paymentdate >= @SearchStartDate       
                 AND paymentdate <= @SearchEndDate       
                 AND r.isdeleted = 0       
      
          INSERT INTO @PL       
                      (groupledger,       
                       ledger,       
                       NAME,       
                       category,       
                       amount,       
                       branchname,       
                       id,       
                       [type])       
          SELECT groupledger,       
                 ledger,       
                 NAME,       
                 CASE       
                   WHEN ( reporttypeid = 1 )THEN 'Assets'       
                   ELSE'Liabilities'       
                 END      AS CATEGORY,       
                 CASE       
                   WHEN( reporttypeid = 1 ) THEN       
                     CASE       
                       WHEN ( category = 'Income' ) THEN amount *- 1       
                       ELSE amount       
                     END       
                   ELSE       
                     CASE       
                       WHEN ( category = 'Income' ) THEN amount       
                       ELSE amount *- 1       
                     END       
                 END      AMOUNT,       
                 branchname,       
                 id,       
                 'Ledger' AS [type]       
          FROM   #result       
          WHERE  reporttypeid IN ( 2, 1 )       
      
          INSERT INTO @PL       
                      (groupledger,       
                       ledger,       
                       NAME,       
                       category,       
                       amount,       
                       branchname,       
                       id,       
                       [type])       
          SELECT 'Current Liabilities'       
                 GROUPLEDGER       
                 ,       
                 v.NAME       
                 AS LEDGER,       
                 ''                                                    AS [Name]       
                 ,       
                 'Liabilities'       
                 CATEGORY,       
                 ( Sum(qty * ed.amount) + ( Sum(sgst) + Sum(cgst) ) ) -       
                 ( (SELECT Isnull(Sum(Isnull(amount, 0)), 0)       
                    FROM   tblexpensepaymentmapping       
                    WHERE  tblexpensepaymentmapping.expenseid = e.id       
                           AND isdeleted = 0)       
                   + (SELECT Isnull(Sum(Isnull(amount, 0)), 0)       
                      FROM   tblexpenseholdings       
                      WHERE  isdeleted = 0       
                             AND tblexpenseholdings.expenseid = e.id) )AS Amount       
                 ,       
                 ''       
                 BranchName,       
                 V.NAME                                                AS ID,       
                 'Vendor'                                              AS [Type]       
          FROM   tblexpensedetails ed       
                 JOIN tblexpense e       
                   ON ed.expenseid = e.id       
                      AND e.isdeleted = 0       
                 JOIN tblvendor v       
                   ON e.vendorid = v.id       
          WHERE  ed.domainid = @DomainID       
                 AND [date] >= @SearchStartDate       
                 AND [date] <= @SearchEndDate       
                 AND ed.isdeleted = 0       
          GROUP  BY v.NAME,       
                    e.id,       
                    V.id       
          UNION ALL       
          SELECT 'Current Assets',       
                 @InputGSTGroup AS GroupName,       
                 @InputSGST     AS PartyName,       
                 'Assets',       
                 ( Sum(sgst) )  AS Amount,       
                 ''             BranchName,       
                 @InputSGST,       
                 @InputSGST       
          FROM   tblexpensedetails ed       
                 JOIN tblexpense e       
                   ON ed.expenseid = e.id       
                      AND e.isdeleted = 0       
                 JOIN tblvendor v       
                   ON e.vendorid = v.id       
          WHERE  ed.domainid = @DomainID       
                 AND [date] >= @SearchStartDate       
       AND [date] <= @SearchEndDate       
                 AND ed.isdeleted = 0       
          UNION ALL       
          SELECT 'Current Assets',       
                 @InputGSTGroup AS GroupName,       
                 @InputCGST     AS PartyName,       
                 'Assets',       
                 ( Sum(cgst) )  AS Amount,       
                 ''             BranchName,       
                 @InputCGST,       
                 @InputCGST       
          FROM   tblexpensedetails ed       
                 JOIN tblexpense e       
                   ON ed.expenseid = e.id       
                      AND e.isdeleted = 0       
                 JOIN tblvendor v       
                   ON e.vendorid = v.id       
          WHERE  ed.domainid = @DomainID       
                 AND [date] >= @SearchStartDate       
                 AND [date] <= @SearchEndDate       
                 AND ed.isdeleted = 0       
          -- Union All                                       
          --select  'Current Liabilities',l.name,'','Liabilities', Case when                                      
          --   (TransactionType=(select ID from tblMasterTypes where name='Receive Payment')) Then Sum(Amount)                                      
          -- Else -1*Sum(Amount) End                                        
          --,'' BranchName,l.Name,'Ledger' from tblReceipts r                                       
          -- Join tblLedger l on r.LedgerID=l.ID                                       
          --where r.IsDeleted =0 AND [Date] >= @SearchStartDate                                           
          -- AND [Date] <= @SearchEndDate and r.DomainID=@DomainID                                       
          --Group by l.Name,r.TransactionType ,l.ID                                      
          UNION ALL       
          SELECT 'Current Assets',       
                 c.NAME,       
                 '',       
                 'Assets',       
                 ( Sum(qty * rate) + ( Sum(Isnull(cgstamount, 0)) + (       
                                       Sum(Isnull(sgstamount, 0))       
                                                                    )       
                                       ) - ( (SELECT Isnull(Sum(Isnull(amount, 0       
                                                                ))       
                                                     , 0)       
                                              FROM   tblinvoicepaymentmapping       
                                              WHERE       
          tblinvoicepaymentmapping.invoiceid = i.id       
          AND isdeleted = 0)       
                                             + (SELECT Isnull(Sum(Isnull(amount,       
                                                                  0       
                     )),       
                                                       0)       
                                                FROM   tblinvoiceholdings       
                                                WHERE       
      tblinvoiceholdings.invoiceid = i.id       
      AND isdeleted = 0) ) ),       
                 '' BranchName,       
                 C.Name,       
                 'Customer'       
          FROM   tblinvoiceitem it       
                 JOIN tblinvoice i       
                   ON i.id = it.invoiceid       
                 JOIN tblcustomer c       
                   ON i.customerid = c.id       
          WHERE  it.isdeleted = 0       
  AND [date] >= @SearchStartDate       
                 AND [date] <= @SearchEndDate       
                 AND i.domainid = @DomainID       
          GROUP  BY c.NAME,       
                    i.id,       
                    C.id       
          UNION ALL       
          SELECT 'Current Liabilities',       
                 @OutputGSTGroup     AS GroupName,       
                 @OutputCGST         AS PartyName,       
                 'Liabilities',       
                 ( Sum(cgstamount) ) AS Amount,       
                 ''                  BranchName,       
                 @OutputCGST,       
                 @OutputCGST       
          FROM   tblinvoiceitem ed       
                 JOIN tblinvoice e       
                   ON ed.invoiceid = e.id       
                      AND e.isdeleted = 0       
          WHERE  ed.domainid = @DomainID       
                 AND [date] >= @SearchStartDate       
                 AND [date] <= @SearchEndDate       
                 AND ed.isdeleted = 0       
          UNION ALL       
          SELECT'Current Liabilities',       
                @OutputGSTGroup     AS GroupName,       
                @OutputSGST         AS PartyName,       
                'Liabilities',       
                ( Sum(cgstamount) ) AS Amount,       
                ''                  BranchName,       
                @OutputSGST,       
                @OutputSGST       
          FROM   tblinvoiceitem ed       
                 JOIN tblinvoice e       
                   ON ed.invoiceid = e.id       
                      AND e.isdeleted = 0       
          WHERE  ed.domainid = @DomainID       
                 AND [date] >= @SearchStartDate       
                 AND [date] <= @SearchEndDate       
                 AND ed.isdeleted = 0       
          UNION ALL       
          SELECT DISTINCT 'Current Assets',       
                          B.NAME,       
                          '',       
                          'Assets',       
                          (( (SELECT DISTINCT Isnull(Sum(BB.amount), 0)       
                              FROM   tblbrs BRS       
                                     LEFT JOIN tblbookedbankbalance BB       
                                            ON BB.brsid = BRS.id       
                              WHERE  BRS.isdeleted = 0       
                                     AND BB.isdeleted = 0       
                                     AND BRS.domainid = @DomainID       
                                     AND sourcetype IN((SELECT id       
                                                        FROM   tblmastertypes       
                                                        WHERE       
                                         bankid = TBRS.bankid       
                                         AND       
                                         NAME IN ( 'InvoiceReceivable',       
                                                   'Receive Payment',       
                                                   'ToBank' )))) -       
                                (SELECT DISTINCT Isnull(Sum(BB.amount), 0)       
                                 FROM   tblbrs BRS       
                                        LEFT JOIN tblbookedbankbalance BB       
                                               ON BB.brsid = BRS.id    
                                 WHERE  BRS.isdeleted = 0       
                                        AND BRS.domainid = @DomainID       
                                        AND BB.isdeleted = 0       
                                        AND sourcetype IN((SELECT id       
                                                           FROM   tblmastertypes       
                                                           WHERE       
                                            bankid = TBRS.bankid       
                                            AND       
                                            NAME IN ( 'PayBill',       
                                                      'FromBank'       
                                                      ,       
                                                      'Bill'       
                                                      ,       
                                                      'Expense'       
                                                      ,       
                                                      'Make Payment'       
                                                    )))) )) +       
                          B.openingbalance       
                          AS AvailableBalance,       
                          '',       
                          Cast(B.id AS VARCHAR),       
                          'BANK'       
          FROM   tblbank B       
                 LEFT JOIN tblbrs TBRS       
                        ON TBRS.bankid = B.id       
          WHERE  B.isdeleted = 0       
                 AND B.domainid = @DomainID       
          GROUP  BY TBRS.bankid,       
                    B.NAME,       
                    B.openingbalance,       
     TBRS.id,       
                    B.id       
          UNION       
          SELECT 'Current Assets',       
                 'Cash',       
                 '',       
                 'Assets',       
                 availablecash,       
                 '',       
                 'Cash',       
                 'Cash'       
          FROM   tblcashbucket       
          WHERE  domainid = @DomainID       
          UNION ALL       
          SELECT G.NAME,       
                 L.NAME,       
                 '',       
                 CASE       
                   WHEN ( G.reporttype = 1 ) THEN 'Assets'       
                   WHEN ( G.reporttype = 2 ) THEN 'Liabilities'       
                 END,       
                 CASE       
                   WHEN ( G.reporttype = 1 ) THEN debit + ( credit *- 1 )       
                   WHEN ( G.reporttype = 2 ) THEN ( debit *- 1 ) + ( credit )       
                 END,       
                 '',       
                 l.NAME,       
                 'Ledger'       
          FROM   tbljournal J       
                 JOIN tblledger L       
                   ON J.ledgerproductid = L.id  And j.Type='Ledger'     
                 JOIN tblgroupledger G       
                   ON G.id = L.groupid  
				   WHERE  j.domainid = @DomainID       
                 AND [journaldate] >= @SearchStartDate       
                 AND [journaldate] <= @SearchEndDate       
                 AND j.isdeleted = 0    
        UNION ALL       
          SELECT G.NAME,       
                 L.NAME,       
                 '',       
                 CASE       
                   WHEN ( G.reporttype = 1 ) THEN 'Assets'       
                   WHEN ( G.reporttype = 2 ) THEN 'Liabilities'       
                 END,       
                 CASE       
                   WHEN ( G.reporttype = 1 ) THEN debit + ( credit *- 1 )       
                   WHEN ( G.reporttype = 2 ) THEN ( debit *- 1 ) + ( credit )       
                 END,       
                 '',       
                 l.NAME,       
                 'Ledger'       
          FROM   tbljournal J     
    Join tblProduct_v2 p on j.LedgerProductID =p.ID  And j.Type='Product'  
                 JOIN tblledger L       
                   ON ISNULL(p.PurchaseLedgerId,p.SalesLedgerId) = L.id       
                 JOIN tblgroupledger G       
                   ON G.id = L.groupid        
          WHERE  j.domainid = @DomainID       
                 AND [journaldate] >= @SearchStartDate       
                 AND [journaldate] <= @SearchEndDate       
                 AND j.isdeleted = 0    
    UNION ALL       
          SELECT 'Current Assets',       
                 c.NAME,       
                 '',       
              'Assets'       
                  ,       
                      
                   ( debit  ) + ( credit*- 1 )       
                 ,       
                 '',       
                 c.NAME,       
                 'Customer'       
          FROM   tbljournal J       
                 JOIN tblCustomer c       
                   ON J.ledgerproductid = c.id  And j.Type='Customer'   
         WHERE  j.domainid = @DomainID       
                 AND [journaldate] >= @SearchStartDate       
                 AND [journaldate] <= @SearchEndDate       
                 AND j.isdeleted = 0    
  UNION ALL       
          SELECT 'Current Liabilities',       
                 c.NAME,       
                 '',       
              'Liabilities'       
                  ,       
                      
                   ( debit *- 1  ) + ( credit)       
                 ,       
                 '',       
                 c.NAME,       
                 'Vendor'       
          FROM   tbljournal J       
                 JOIN tblVendor c       
                   ON J.ledgerproductid = c.id  And j.Type='Vendor'   
         WHERE  j.domainid = @DomainID       
                 AND [journaldate] >= @SearchStartDate       
                 AND [journaldate] <= @SearchEndDate       
                 AND j.isdeleted = 0         
          UNION ALL       
          SELECT G.NAME,       
                 L.NAME,       
                 '',       
                 CASE       
                   WHEN ( G.reporttype = 1 ) THEN 'Assets'       
                   WHEN ( G.reporttype = 2 ) THEN 'Liabilities'       
                 END,       
                 CASE       
                   WHEN ( G.reporttype = 1 ) THEN Isnull(amount, 0)       
                   WHEN ( G.reporttype = 2 ) THEN Isnull(amount, 0) *- 1       
                 END,       
                 '',       
                 l.NAME,       
                 'Ledger'       
          FROM   tblinvoiceholdings J       
                 JOIN tblledger L       
                   ON J.ledgerid = L.id       
                 JOIN tblgroupledger G       
                   ON G.id = L.groupid       
          WHERE  j.domainid = @DomainID       
                 AND date >= @SearchStartDate       
                 AND date <= @SearchEndDate       
                 AND j.isdeleted = 0       
          UNION ALL       
          SELECT G.NAME,       
                 L.NAME,       
                 '',       
                 CASE       
                   WHEN ( G.reporttype = 1 ) THEN 'Assets'       
                   WHEN ( G.reporttype = 2 ) THEN 'Liabilities'       
                 END,       
                 CASE       
                   WHEN ( G.reporttype = 1 ) THEN Isnull(amount, 0) *- 1       
                   WHEN ( G.reporttype = 2 ) THEN Isnull(amount, 0)       
                 END,       
                 '',       
                 l.NAME,       
                 'Ledger'       
          FROM   tblexpenseholdings J       
                 JOIN tblledger L       
                   ON J.ledgerid = L.id       
                 JOIN tblgroupledger G       
                   ON G.id = L.groupid       
          WHERE  j.domainid = @DomainID       
                 AND date >= @SearchStartDate       
                 AND date <= @SearchEndDate       
                 AND j.isdeleted = 0       
      
          SELECT *       
          FROM   @PL       
          --WHERE  Amount <> 0                                 
          ORDER  BY CASE       
                      WHEN groupledger = 'Share Capital' THEN 1       
                      WHEN groupledger = 'P and L (Profit)' THEN 2       
                      WHEN groupledger = 'Secured Loan' THEN 3       
                      WHEN groupledger = 'Unsecured Loan' THEN 4       
                      WHEN groupledger = 'Current Liabilities' THEN 5       
                      WHEN groupledger = 'Statutory Payables' THEN 6       
                      WHEN groupledger = 'Fixed Assets' THEN 7       
                      WHEN groupledger = 'Investments (long term)' THEN 8       
                      WHEN groupledger = 'Loans & Advances (long term)' THEN 9       
                      WHEN groupledger = 'Current Assets' THEN 10       
  WHEN groupledger = 'P and L (Loss)' THEN 11       
                      ELSE 12       
                    END       
      END try       
      
      BEGIN catch       
          ROLLBACK TRANSACTION       
      
          DECLARE @ErrorMsg    VARCHAR(100),       
                  @ErrSeverity TINYINT       
      
          SELECT @ErrorMsg = Error_message(),       
                 @ErrSeverity = Error_severity()       
      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)       
      END catch       
  END
