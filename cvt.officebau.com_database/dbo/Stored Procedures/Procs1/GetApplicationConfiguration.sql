﻿/****************************************************************************     
CREATED BY   : Naneeshwar.M    
CREATED DATE  : 19-JAN-2018    
MODIFIED BY   : Ajith N    
MODIFIED DATE  : 02 Feb 2018    
 <summary>            
     [Getapplicationconfiguration] 6    
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetApplicationConfiguration] (@BusinessUnitID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT Isnull(AC.ID, 0)                             AS ID,
                 CM.Code                                      AS ConfigCode,
                 CM.Description                               AS ConfigDescription,
                 CM.ControlType                               AS ConfigControlType,
                 AC.ConfigValue                               AS ConfigValue,
                 AC.BusinessUnitID                            AS BusinessUnitID,
                 em.FirstName + ' ' + Isnull(em.lastname, '') AS ModifiedBy,
                 AC.ModifiedOn                                AS ModifiedOn,
                 BU.NAME                                      AS BusinessUnit,
                 ConfigurationOrderID
          INTO   #temp
          FROM   tbl_ConfigurationMaster cm
                 LEFT JOIN tbl_ApplicationConfiguration ac
                        ON cm.ID = ac.ConfigurationID
                           AND ac.BusinessUnitID = @BusinessUnitID
                 LEFT JOIN tbl_EmployeeMaster em
                        ON em.id = ac.ModifiedBy
                 LEFT JOIN tbl_BusinessUnit bu
                        ON bu.ID = ac.BusinessUnitID

          IF EXISTS(SELECT 1
                    FROM   #temp
                    WHERE  BusinessUnitID = @BusinessUnitID)
            BEGIN
                SELECT ID                            AS ID,
                       ConfigCode,
                       ConfigDescription,
                       ConfigControlType,
                       Isnull(ConfigValue, 0)        AS ConfigValue,
                       @BusinessUnitID               AS BusinessUnitID,
                       Isnull(ModifiedBy, '')        AS ModifiedBy,
                       Isnull(ModifiedOn, Getdate()) AS ModifiedOn,
                       Isnull(BusinessUnit, '')      AS BusinessUnit,
                       ConfigurationOrderID
                FROM   #temp
                WHERE  Isnull(BusinessUnitID, @BusinessUnitID) = @BusinessUnitID
                ORDER  BY ConfigurationOrderID ASC
            END
          ELSE
            BEGIN
                SELECT DISTINCT 0               AS ID,
                                ConfigCode,
                                ConfigDescription,
                                ConfigControlType,
                                0               AS ConfigValue,
                                @BusinessUnitID AS BusinessUnitID,
                                NULL            AS ModifiedBy,
                                NULL            AS ModifiedOn,
                                NULL            AS BusinessUnit,
                                ConfigurationOrderID
                FROM   #temp
                ORDER  BY ConfigurationOrderID ASC
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
