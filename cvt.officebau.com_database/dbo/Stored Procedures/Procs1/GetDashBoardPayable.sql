﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
<summary>          
 [GetDashBoardPayable] '',1,2  
</summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetDashBoardPayable] (@Date     DATETIME,  
                                             @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          IF( Isnull(@Date, '') = '' )  
            SET @Date=Getdate();  
  
          WITH Income  
               AS (SELECT ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50)) AS MonthYear,  
                          Isnull(Sum(Amount), 0)                                                               AS Amount,  
                          Sum(Isnull(Received, 0))                                                             AS ReceivedAmount  
                   FROM   Vw_Income
                   WHERE DomainID = @DomainID  
                   GROUP  BY ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50))),  
               Expense  
               AS (SELECT ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50)) AS ExpenseMonthYear,  
                          Isnull(Sum(Amount), 0)                                                               AS ExpenseAmount,  
                          Sum(Isnull(PaidAmount, 0))                                                           AS ExpenseReceivedAmount  
                   FROM   Vw_Expense
                   WHERE  DomainID = @DomainID  
                   GROUP  BY ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50))),  
               MonthYear  
               AS (SELECT MonthYear  
                   FROM   dbo.Fngetmonthyear (Datepart(Month, @Date), Datepart(YEAR, @Date)))  
          SELECT tem.MonthYear                                                         AS MonthYear,  
                 Isnull(inc.Amount, 0)                                                 AS Amount,  
                 Isnull(expe.ExpenseAmount, 0)                                         AS ExpenseAmount,  
                 Isnull(inc.ReceivedAmount, 0)                                         AS ReceivedAmount,  
                 Isnull(expe.ExpenseReceivedAmount, 0)                                 AS ExpenseReceivedAmount,  
                 Isnull(inc.Amount, 0) - Isnull(inc.ReceivedAmount, 0)                 AS Receivables,  
                 Isnull(expe.ExpenseAmount, 0) - Isnull(expe.ExpenseReceivedAmount, 0) AS Payable  
          FROM   MonthYear tem  
                 LEFT JOIN Income inc  
                        ON tem.MonthYear = inc.MonthYear  
                 LEFT JOIN Expense expe  
                        ON tem.MonthYear = expe.ExpenseMonthYear  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END  


