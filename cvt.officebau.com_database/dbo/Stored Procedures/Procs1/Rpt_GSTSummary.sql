﻿/****************************************************************************                 
CREATED BY   : Ajith N            
CREATED DATE  : 01 Aug 2018                
MODIFIED BY   :                 
MODIFIED DATE  :                 
 <summary>              
 [Rpt_GSTSummary] 4,1 ,''         
 </summary>                                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_GSTSummary] (@FinancialYearID INT,
                                         @DomainID        INT,
                                         @ReportType      VARCHAR(50))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @FinancialYear INT;
          DECLARE @StartFinancialMonth INT;
          DECLARE @EndFinancialMonth INT;

          CREATE TABLE #tmpResult
            (
               --id      INT IDENTITY(1, 1),    
               [Type] VARCHAR(100),
               NAME   VARCHAR(100),
               Total  MONEY
            )

          SET @StartFinancialMonth = (SELECT Value
                                      FROM   tblApplicationConfiguration
                                      WHERE  Code = 'STARTMTH'
                                             AND DomainID = @DomainID)
          SET @EndFinancialMonth = ( ( (SELECT Value
                                        FROM   tblApplicationConfiguration
                                        WHERE  Code = 'STARTMTH'
                                               AND DomainID = @DomainID)
                                       + 11 ) % 12 )

          IF( @FinancialYearID = 0 )
            BEGIN
                IF( Month(Getdate()) <= ( ( (SELECT Value
                                             FROM   tblApplicationConfiguration
                                             WHERE  Code = 'STARTMTH'
                                                    AND DomainID = @DomainID)
                                            + 11 ) % 12 ) )
                  SET @FinancialYear = Year(Getdate()) - 1
                ELSE
                  SET @FinancialYear = Year(Getdate())
            END
          ELSE
            BEGIN
                SET @FinancialYear = Substring((SELECT FinancialYear
                                                FROM   tbl_FinancialYear
                                                WHERE  ID = @FinancialYearID
                                                       AND DomainID = @DomainID), 0, 5)
            END

          DECLARE @StartDate DATE = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date            
          DECLARE @EndDate DATE = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date           

          SELECT im.InvoiceID                                                AS ID,
                 c.NAME                                                      AS Customer,
                 Isnull(G.Code, '')                                          AS [HSN Code],
                 Isnull(c.GSTNo, '')                                         AS [GST No],
                 i.Type                                                      AS [Type],
                 i.InvoiceNo                                                 AS [Invoice No],
                 ( im.qty * im.Rate )                                        AS [Total Amount],
                 Isnull(im.CGSTAmount, 0)                                    AS CGST,
                 Cast(Isnull(im.CGST, 0) AS VARCHAR)                         AS [CGST %],
                 Isnull(im.SGSTAmount, 0)                                    AS SGST,
                 Cast(Isnull(im.SGST, 0) AS VARCHAR)                         AS [SGST %],
                 0                                                           AS IGST,
                 Cast(0 AS VARCHAR)                                          AS [IGST %],
                 ( Isnull(im.CGSTAmount, 0) ) + ( Isnull(im.SGSTAmount, 0) ) AS [Total GST]
          INTO   #tmpGstOutput
          FROM   tblInvoiceItem im
                 LEFT JOIN tblInvoice I
                        ON I.ID = im.InvoiceID
                 LEFT JOIN tblCustomer c
                        ON c.ID = I.CustomerID
                 LEFT JOIN tblProduct_v2 P
                        ON P.ID = im.ProductID
                 LEFT JOIN tblGstHSN G
                        ON G.ID = P.HSNID
          WHERE  im.IsDeleted = 0
                 AND im.DomainID = @DomainID
                 AND I.[Date] >= @StartDate
                 AND I.[Date] <= @EndDate
          ORDER  BY c.NAME ASC,
                    Isnull(G.Code, '');

          SELECT EI.ExpenseID                               AS ID,
                 v.NAME                                     AS Vendor,
                 Isnull(v.GSTNo, '')                        AS [GST No],
                 Isnull(G.Code, '')                         AS [HSN Code],
                 E.[Type]                                   AS [Type],
                 E.billNo                                   AS [Bill No],
                 ( EI.Qty * EI.Amount )                     AS [Total Amount],
                 Isnull(EI.CGST, 0)                         AS CGST,
                 Cast(Isnull(EI.CGSTPercent, 0) AS VARCHAR) AS [CGST %],
                 Isnull(EI.SGST, 0)                         AS SGST,
                 Cast(Isnull(EI.SGSTPercent, 0) AS VARCHAR) AS [SGST %],
                 Isnull(EI.IGST, 0)                         AS IGST,
                 Cast(Isnull(EI.IGSTPercent, 0) AS VARCHAR) AS [IGST %],
                 ( Isnull(EI.CGST, 0) + Isnull(EI.SGST, 0)
                   + Isnull(EI.IGST, 0) )                   AS [Total GST]
          INTO   #tmpGstInput
          FROM   tblExpenseDetails EI
                 LEFT JOIN tblExpense E
                        ON E.ID = EI.ExpenseID
                 LEFT JOIN tblVendor v
                        ON v.ID = E.VendorID
                 LEFT JOIN tblLedger L
                        ON L.ID = EI.LedgerID
                 LEFT JOIN tblGstHSN G
                        ON G.ID = L.GstHSNID
          WHERE  EI.IsDeleted = 0
                 AND EI.DomainID = @DomainID
                 AND E.[Date] >= @StartDate
                 AND E.[Date] <= @EndDate
          ORDER  BY v.NAME ASC,
                    E.[Type] ASC,
                    Isnull(G.Code, '');

          ----- GST Output    
          INSERT INTO #tmpResult
          SELECT 'Output'  AS [Type],
                 'CGST'    AS NAME,
                 Sum(CGST) AS CGST
          FROM   #tmpGstOutput
          UNION ALL
          SELECT 'Output'  AS [Type],
                 'SGST'    AS NAME,
                 Sum(SGST) AS SGST
          FROM   #tmpGstOutput
          UNION ALL
          SELECT 'Output'  AS [Type],
                 'IGST'    AS NAME,
                 Sum(IGST) AS IGST
          FROM   #tmpGstOutput
          UNION ALL
          SELECT 'Output'                              AS [Type],
                 'Total'                               AS NAME,
                 ( Sum(CGST) + Sum(SGST) + Sum(IGST) ) AS Total
          FROM   #tmpGstOutput
          UNION ALL
          ----- GST Input    
          SELECT 'Input'   AS [Type],
                 'CGST'    AS NAME,
                 Sum(CGST) AS CGST
          FROM   #tmpGstInput
          UNION ALL
          SELECT 'Input'   AS [Type],
                 'SGST'    AS NAME,
                 Sum(SGST) AS SGST
          FROM   #tmpGstInput
          UNION ALL
          SELECT 'Input'   AS [Type],
                 'IGST'    AS NAME,
                 Sum(IGST) AS IGST
          FROM   #tmpGstInput
          UNION ALL
          SELECT 'Input'                               AS [Type],
                 'Total'                               AS NAME,
                 ( Sum(CGST) + Sum(SGST) + Sum(IGST) ) AS Total
          FROM   #tmpGstInput

          SELECT *
          FROM   #tmpResult
      --SELECT *    
      --FROM   (SELECT *    
      --        FROM   #tmpResult)src    
      --       PIVOT ( Max(id)    
      --             FOR Name IN ([Output],    
      --                             [Input]) ) piv;    
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
