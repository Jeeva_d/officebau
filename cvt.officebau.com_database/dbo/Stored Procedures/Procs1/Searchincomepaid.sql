﻿/****************************************************************************       
CREATED BY   : Naneeshwar      
CREATED DATE  :   21-Nov-2016    
MODIFIED BY   :   Naneeshwar.B    
MODIFIED DATE  :     3-1-2017  
<summary>    
	[Searchincomepaid] 5,'',1,'2017-05-4',''   
</summary>                               
*****************************************************************************/
CREATE PROCEDURE [dbo].[Searchincomepaid] (@CustomerID INT,
                                          @Status     VARCHAR(100),
                                          @DomainID   INT,
										  @StartDate VARCHAR(50),
                                          @EndDate   VARCHAR(50))
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
        BEGIN TRANSACTION

        IF( @StartDate != '' )
          SET @StartDate = Cast(@StartDate AS DATETIME)

        IF( @EndDate != '' )
          SET @EndDate = Cast(@EndDate AS DATETIME)

        IF( @Status = 'All' )
          SET @Status='';

        WITH receivable
             AS (SELECT invpm.InvoicePaymentID AS RecID,
                        --invpm.InvoiceID        AS InvoiceID,
                        Sum(invpm.Amount)      AS PaidAmount,
                        i.StatusID             AS Status
                 FROM   tblInvoicePaymentMapping invpm
                        LEFT JOIN tblInvoice i
                               ON i.ID = invpm.InvoiceID
                                  AND invpm.IsDeleted = 0
                                  AND i.[Type] = 'Invoice'
                 WHERE  i.CustomerID = @CustomerID
                        AND i.IsDeleted = 0
                        AND i.DomainID = @DomainID
                        AND ( ( @StartDate IS NULL
                                 OR @StartDate = '' )
                               OR i.Date >= @StartDate )
                        AND ( ( @EndDate IS NULL
                                 OR @EndDate = '' )
                               OR i.Date <= @EndDate )
                        AND ( ( Isnull(@Status, '') = ''
                                AND i.StatusID IN(SELECT ID
                                                  FROM   tbl_CodeMaster
                                                  WHERE  [Type] IN( 'Open', 'Partial' )
                                                         AND IsDeleted = 0) )
                               OR ( Isnull(@Status, '') != ''
                                    AND i.StatusID IN(SELECT ID
                                                      FROM   tbl_CodeMaster
                                                      WHERE  [Type] = @Status
                                                             AND IsDeleted = 0) ) )
                       
                 GROUP  BY invpm.InvoicePaymentID,
                           invpm.InvoiceID,
                           i.StatusID)
        SELECT rec.RecID                                         RecID,
               inpay.PaymentDate                                 PaymentDate,
               Sum(rec.PaidAmount)                               RecAmount,
               inpay.ModifiedOn,
               emp.FirstName                                     ModifiedBy,
               (SELECT Substring((SELECT + ',' + inv.InvoiceNo
                                  FROM   tblInvoice inv
                                         JOIN tblInvoicePaymentMapping invpm
                                           ON inv.ID = invpm.InvoiceID
                                              AND invpm.IsDeleted = 0
                                              AND InvoicePaymentID = inpay.ID
                                  WHERE  inv.DomainID = @DomainID
                                         AND invpm.amount <> 0
                                  FOR XML PATH('')), 2, 200000)) InvoiceNo
        FROM   receivable rec
               LEFT JOIN tblInvoicePayment inpay
                      ON rec.RecID = inpay.ID
               LEFT JOIN tbl_employeeMaster emp
                      ON emp.id = inpay.ModifiedBy
        --WHERE  (( ( Isnull(@Status, '') = ''
        --               AND rec.Status IN(SELECT ID
        --                                 FROM   tbl_CodeMaster
        --                                 WHERE  [Type] IN( 'Open', 'Partial')
        --                                        AND IsDeleted = 0) )
        --              OR ( Isnull(@Status, '') != ''
        --                   AND rec.Status  IN(SELECT ID
        --                                     FROM   tbl_CodeMaster
        --                                     WHERE  [Type] = @Status
        --                                            AND IsDeleted = 0) ) )
        --               AND rec.Status IN(SELECT ID
        --                                 FROM   tbl_CodeMaster
        --                                 WHERE  [Type] = @Status
        --                                        AND IsDeleted = 0) ) 
        GROUP  BY rec.RecID,
                  inpay.PaymentDate,
                  inpay.ModifiedOn,
                  emp.FirstName,
                  inpay.ID

        --ORDER  BY inpay.ModifiedOn DESC
        COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION
        DECLARE @ErrorMsg    VARCHAR(100),
                @ErrSeverity TINYINT
        SELECT @ErrorMsg = Error_message(),
               @ErrSeverity = Error_severity()
        RAISERROR(@ErrorMsg,@ErrSeverity,1)
    END CATCH
END
