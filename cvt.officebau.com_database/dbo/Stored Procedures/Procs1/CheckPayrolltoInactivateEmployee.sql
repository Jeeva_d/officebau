﻿/****************************************************************************     
CREATED BY   :   Naneeshwar.M  
CREATED DATE  :   02-Nov-2017  
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [CheckPayrolltoInactivateEmployee] 106,'02-Oct-2017'  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[CheckPayrolltoInactivateEmployee] (@EmployeeID   INT,  
                                                          @InactiveForm DATETIME)  
AS  
  BEGIN  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          DECLARE @MonthName VARCHAR(50),  
                  @OutPut    VARCHAR(200)  
  
          SET @MonthName =(SELECT Datename(month, Dateadd(month, Month(@InactiveForm) - 1, Cast('2000-01-01' AS DATETIME))))  
  
          IF EXISTS(SELECT 1  
                    FROM   tbl_Pay_EmployeePayroll  
                    WHERE  EmployeeId = @EmployeeID  
                           AND MonthId = Month(@InactiveForm)  
                           AND YearId = Year(@InactiveForm)  
                           AND IsDeleted = 0  
                           AND IsProcessed = 1)  
            BEGIN  
                SET @OutPut= 'Payroll has been Processed for the month of '  
                             + @MonthName + ' '  
                             + Cast (Year(@InactiveForm) AS VARCHAR)  
                             + '. So you cannot Inactivate the seleted Employee.'  
            END  
  
          IF EXISTS(SELECT 1  
                    FROM   tbl_Pay_EmployeePayroll  
                    WHERE  EmployeeId = @EmployeeID  
                           AND MonthId = Month(@InactiveForm)  
                           AND YearId = Year(@InactiveForm)  
                           AND IsDeleted = 0  
                           AND IsProcessed <> 1)  
            BEGIN  
                UPDATE tbl_Pay_EmployeePayroll  
                SET    IsDeleted = 1  
                WHERE  EmployeeId = @EmployeeID  
                       AND MonthId = Month(@InactiveForm)  
                       AND YearId = Year(@InactiveForm)  
                       AND IsDeleted = 0  
  
                UPDATE tbl_TDS  
                SET    IsDeleted = 1  
                WHERE  EmployeeId = @EmployeeID  
                       AND MonthId = Month(@InactiveForm)  
                       AND YearId = (SELECT ID  
                                     FROM   tbl_FinancialYear  
                                     WHERE  NAME = Year(@InactiveForm)  
                                            AND DomainID = (SELECT DomainID  
                                                            FROM   tbl_EmployeeMaster  
                                                            WHERE  id = @EmployeeID))  
                       AND IsDeleted = 0  
  
                SET @OutPut= 'Updated.'  
            END  
  
          SELECT @OutPut AS Result  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
