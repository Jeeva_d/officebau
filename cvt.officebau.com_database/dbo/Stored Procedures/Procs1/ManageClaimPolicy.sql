﻿/****************************************************************************     
CREATED BY   : Naneeshwar.M    
CREATED DATE  : 28-NOV-2017    
MODIFIED BY   : Ajith N    
MODIFIED DATE  : 01 Dec 2017    
 <summary>     
 [Manageclaimpolicy] 97,1,3,1,5,1,1,0,1,1    
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageClaimPolicy] (@ID             INT,
                                           @ExpenseTypeID  INT,
                                           @BandID         INT,
                                           @LevelID        INT,
                                           @DesignationID  INT,
                                           @MetroAmount    MONEY,
                                           @NonMetroAmount MONEY,
                                           @IsDeleted      BIT,
                                           @SessionID      INT,
                                           @DomainID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output               VARCHAR(100) = 'Operation Failed!',
              @DesignationMappingID INT=0

      SET @DesignationMappingID= (SELECT ID
                                  FROM   tbl_EmployeeDesignationMapping
                                  WHERE  BandID = @BandID
                                         AND GradeID = @LevelID
                                         AND DesignationID = @DesignationID
                                         AND IsDeleted = 0)

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @IsDeleted = 1 )
            BEGIN
                IF NOT EXISTS(SELECT 1
                              FROM   tbl_ClaimsRequest
                              WHERE  IsDeleted = 0
                                     AND CategoryID = @ExpenseTypeID
                                     AND DestinationID = @DesignationID
                                     AND DomainID = @DomainID)
                  BEGIN
                      UPDATE tbl_ClaimPolicy
                      SET    IsDeleted = 1,
                             ModifiedBY = @SessionID
                      WHERE  ID = @ID
                             AND DomainID = @DomainID

                      SET @Output = 'Deleted Successfully.'
                  END
                ELSE
                  SET @Output = 'Record referred.'

                GOTO Finish
            END
          ELSE
            BEGIN
                IF( @DesignationMappingID <> 0 )
                  BEGIN
                      IF( @ID = 0 )
                        BEGIN
                            IF( (SELECT Count(1)
                                 FROM   tbl_ClaimPolicy
                                 WHERE  DesignationMappingID = @DesignationMappingID
                                        AND ExpenseType = @ExpenseTypeID
                                        AND DomainID = @DomainID
                                        AND IsDeleted = 0) = 0 )
                              BEGIN
                                  INSERT INTO tbl_ClaimPolicy
                                              (DesignationMappingID,
                                               ExpenseType,
                                               MetroAmount,
                                               NonMetroAmount,
                                               CreatedBy,
                                               CreatedOn,
                                               ModifiedBy,
                                               ModifiedOn,
                                               DomainID)
                                  VALUES      (@DesignationMappingID,
                                               @ExpenseTypeID,
                                               @MetroAmount,
                                               @NonMetroAmount,
                                               @SessionID,
                                               Getdate(),
                                               @SessionID,
                                               Getdate(),
                                               @DomainID)

                                  SET @Output = 'Inserted Successfully.'
                              END
                            ELSE
                              SET @Output = 'Already Exists.'
                        END
                      ELSE
                        BEGIN
                            IF( (SELECT Count(1)
                                 FROM   tbl_ClaimPolicy
                                 WHERE  DomainID = @DomainID
                                        AND DesignationMappingID = @DesignationMappingID
                                        AND ExpenseType = @ExpenseTypeID
                                        AND ID <> @ID
                                        AND IsDeleted = 0) = 0 )
                              BEGIN
                                  UPDATE tbl_ClaimPolicy
                                  SET    DesignationMappingID = @DesignationMappingID,
                                         ExpenseType = @ExpenseTypeID,
                                         MetroAmount = @MetroAmount,
                                         NonMetroAmount = @NonMetroAmount,
                                         ModifiedBY = @SessionID,
                                         ModifiedOn = Getdate()
                                  WHERE  ID = @ID
                                         AND DomainID = @DomainID

                                  SET @Output = 'Updated Successfully.'
                              END
                            ELSE
                              BEGIN
                                  SET @Output = 'Already Exists.'
                              END
                        END
                  END
                ELSE
                  BEGIN
                      SET @Output = 'Please Map the Band and Grade for the selected Designation.'

                      GOTO Finish
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
