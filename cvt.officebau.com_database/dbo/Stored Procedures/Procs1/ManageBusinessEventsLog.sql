﻿/**************************************************************************** 
CREATED BY			:
CREATED DATE		:
MODIFIED BY			:
MODIFIED DATE		:
 <summary>        
		
 </summary>                         
 *****************************************************************************/
CREATE PROC [dbo].[ManageBusinessEventsLog] (@GUID   UNIQUEIDENTIFIER,
                                      @COL_NM VARCHAR(500),
                                      @TBL_NM VARCHAR(200))
AS
  BEGIN
      /*====To Pick Existing Table Column Values=====*/
      DECLARE @TypeCastColumns VARCHAR(MAX),
              @TraceQuery      VARCHAR(MAX)

      SET @TypeCastColumns = (SELECT Substring((SELECT ',CONVERT(varchar(MAX),' + Splitdata + ') AS '
                                                       + Splitdata
                                                FROM   dbo.Fnsplitstring(@COL_NM, ',')
                                                FOR XML PATH('')), 2, 80000))
      SET @TraceQuery = 'SELECT ColumnName,ColumnValue,ModifiedBy,ModifiedOn FROM (SELECT '
                        + @TypeCastColumns
                        + ',ModifiedBy,ModifiedOn FROM ' + @TBL_NM
                        + ' TBL WHERE TBL.HistoryID = '''
                        + CONVERT(VARCHAR(50), @GUID) + ''') AS T'
                        + ' unpivot (ColumnValue for ColumnName in ('
                        + @COL_NM + ')) UNP'

      DECLARE @ExistingTableValues TABLE
        (
           Column_Name  VARCHAR(MAX),
           Column_Value VARCHAR(MAX),
           ModifiedBy   INT,
           ModifiedOn   DATETIME
        )

      INSERT INTO @ExistingTableValues
      EXEC(@TraceQuery)

  /*=============================================*/
      /*===Columns Traced Newly(OR)First Entry=======*/
      INSERT INTO tblBusinessEventsLog
                  ([GUID],
                   ColumnName,
                   ColumnValue,
                   ModifiedBy,
                   ModifiedOn,
                   ChangeType,
                   IsLatest)
      SELECT @GUID,
             Column_Name,
             Column_Value,
             ModifiedBy,
             ModifiedOn,
             ( CASE
                 WHEN Column_Name = 'IsDeleted' THEN 'DELETE'
                 ELSE 'INSERT'
               END ),
             1
      FROM   @ExistingTableValues ETV
      WHERE  (SELECT Count(1)
              FROM   tblBusinessEventsLog HBL
              WHERE  HBL.[GUID] = @GUID
                     AND HBL.ColumnName = ETV.Column_Name) = 0

  /*=============================================*/
      /*===Check for record change and insert into logs===*/
      DECLARE @BusinessEventsLog TABLE
        (
           [GUID]      UNIQUEIDENTIFIER,
           ColumnName  VARCHAR(200),
           ColumnValue VARCHAR(MAX),
           ModifiedBy  INT,
           ModifiedOn  DATETIME,
           ChangeType  VARCHAR(100),
           IsLatest    BIT
        )

      INSERT INTO @BusinessEventsLog
                  ([GUID],
                   ColumnName,
                   ColumnValue,
                   ModifiedBy,
                   ModifiedOn,
                   ChangeType,
                   IsLatest)
      SELECT @GUID,
             HBL.ColumnName,
             ETV.Column_Value,
             ETV.ModifiedBy,
             ETV.ModifiedOn,
             'UPDATE',
             1
      FROM   tblBusinessEventsLog HBL
             JOIN dbo.Fnsplitstring(@COL_NM, ',') AS COL
               ON HBL.ColumnName = COL.Splitdata
             JOIN @ExistingTableValues ETV
               ON ETV.Column_Name = HBL.ColumnName
                  AND Isnull(ETV.Column_Value, '') <> Isnull(HBL.ColumnValue, '')
      WHERE  HBL.GUID = @GUID
             AND HBL.IsLatest = 1

      UPDATE HBL
      SET    IsLatest = 0
      FROM   tblBusinessEventsLog HBL
             JOIN @BusinessEventsLog LL
               ON LL.GUID = HBL.GUID
                  AND LL.ColumnName = HBL.ColumnName

      INSERT INTO tblBusinessEventsLog
      SELECT *
      FROM   @BusinessEventsLog
  /*==================================================*/
  END
