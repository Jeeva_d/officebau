﻿

/****************************************************************************               
CREATED BY  :           
CREATED DATE :              
MODIFIED BY  :            
MODIFIED DATE :            
<summary>            
[Pay_paystubconfiguration] 12,2018,5,1      
</summary>                                       
*****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_PaystubConfiguration] (@MonthId    INT,
                                                  @Year       INT,
                                                  @EmployeeID INT,
                                                  @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @CompanyName VARCHAR(100)= (SELECT Isnull(FullName, '')
             FROM   tbl_company
             WHERE  id = @DomainID)
          DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'
            + CONVERT(VARCHAR(10), @Year)
          DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth))

          SELECT @EmployeeID                                    AS EmployeeID,
                 EMP.FirstName + ' ' + Isnull(EMP.LastName, '') AS [EmployeeName],
                 EMP.Code                                       AS [EmployeeCode],
                 EMP.EmailID                                    AS [EmailID],
                 Isnull(DE.NAME, '') + '-'
                 + Isnull(DEPART.NAME, '')                      AS [DesignationName],
                 Isnull(EST.PFNo, '-')                          AS [PFNo],
                 Isnull(EST.ESINo, '-')                         AS [ESINo],
                 Isnull(EST.UANNo, '-')                         AS [UAN],
                 Isnull(EST.PAN_No, '-')                        AS [PANNo],
                 Isnull(EST.AadharID, '-')                      AS [AadharID],
                 Isnull(EST.AccountNo, '-')                     AS [AccountNo],
                 CASE
                   WHEN ( EST.AccountNo <> '' ) THEN
                     'Bank'
                   ELSE
                     'Cash / Cheque'
                 END                                            AS [Mode of Pay],
                 pd.Amount,
                 pc.Description + '/' + p.Type                  AS Description,
                 Cast(@WorkDays AS INT)                         AS [WorkingDays],
                 @CompanyName                                   AS [CompanyName],
                 Isnull(BU.Address, '')                         AS [BusinessUnitAddress],
                 CONVERT(VARCHAR(50), EMP.DOJ, 106)             AS [DOJ],
                 Isnull(CONVERT(VARCHAR(50), COM.Logo), '')     AS LOGO,
                 @WorkDays - Isnull(lop.[LopDays], 0)           AS PresentDays,
                 CONVERT(VARCHAR(50), mon.Code) + ' - '
                 + CONVERT(VARCHAR(50), YearID )                AS MonthYear,
                 paycom.PayStub
          INTO   #tempPayStub
          FROM   tbl_pay_paystubConfiguration p
                 JOIN tbl_Pay_PayrollCompontents pc
                   ON pc.ID = p.ComponentID
                 JOIN tbl_Pay_EmployeePayrollDetails pd
                   ON pd.ComponentId = pc.id
                 JOIN tbl_Pay_EmployeePayroll epay
                   ON epay.ID = pd.PayrollId
                 JOIN tbl_EmployeeMaster emp
                   ON emp.ID = @EmployeeID
                 LEFT JOIN tbl_EmployeeStatutoryDetails EST
                        ON EST.EmployeeID = EMP.ID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = emp.BaseLocationID
                 LEFT JOIN tbl_lopdetails lop
                        ON lop.EmployeeId = @Employeeid
                           AND lop.Monthid = @MonthId
                           AND lop.year = @Year
                 LEFT JOIN tbl_Company COM
                        ON COM.ID = epay.DomainId
                 LEFT JOIN tbl_FileUpload FU
                        ON FU.Id = COM.Logo
                 LEFT JOIN tbl_Designation DE
                        ON DE.ID = EMP.DesignationID
                 LEFT JOIN tbl_Department DEPART
                        ON DEPART.ID = EMP.DepartmentID
                 JOIN tbl_month mon
                   ON mon.id = @MonthID
                 JOIN tbl_pay_employeepaystructure emppay
                   ON emppay.ID = epay.EmployeePaystructureID
                 JOIN tbl_Pay_PayrollCompanyPayStructure paycom
                   ON paycom.ComponentID = pd.ComponentId
                      AND paycom.companypaystructureID = emppay.companypaystructureID
          WHERE  epay.monthid = @MonthId
                 AND [yearid] = @Year
                 AND epay.EmployeeId = @Employeeid
                 AND epay.IsProcessed = 1
                 AND epay.DomainID = @DomainID
                 AND epay.Isdeleted = 0
                 AND p.isdeleted = 0
          ORDER  BY [Type]DESC,
                    Ordinal ASC

          DELETE FROM #tempPayStub
          WHERE  PayStub = 1
                 AND amount = 0

          UPDATE #tempPayStub
          SET    PayStub = 0
          WHERE  PayStub = 1

          DECLARE @header VARCHAR(max)=''

          SET @header= Substring((SELECT ',[' + Description + ']'
                                  FROM   #tempPayStub
                                  FOR xml Path('')), 2, 20000)

          IF( (SELECT Count(1)
               FROM   #tempPayStub) > 0 )
            BEGIN
                EXEC (' SELECT        
      EmployeeID,      
      [EmployeeName],      
      [EmployeeCode],      
      [EmailID],      
      [DesignationName],      
      [PFNo],      
      [ESINo],      
      [UAN],      
      [PANNo],      
      [AadharID],      
      [AccountNo],      
      [Mode of Pay],      
      [WorkingDays],      
      [CompanyName],      
      [BusinessUnitAddress],      
      [DOJ],      
      LOGO,      
      [PresentDays],      
      MonthYear,      
       '+@header+'      
   FROM   (SELECT *      
     FROM   #tempPayStub ) AS SourceTable      
       PIVOT ( Max(Amount)      
       FOR [Description] IN ('+@header+') ) AS PivotTable; ')
            END
          ELSE
            BEGIN
                SELECT *
                FROM   #tempPayStub
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 
