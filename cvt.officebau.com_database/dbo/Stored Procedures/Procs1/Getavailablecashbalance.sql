﻿/****************************************************************************   
CREATED BY   : Naneeshwar  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 Getavailablecashbalance 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getavailablecashbalance] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          --DECLARE @ID VARCHAR(max)

          --SET @ID=(SELECT Stuff((SELECT ',' + CONVERT(VARCHAR, b.ID)
          --                       FROM   tblBank b
          --                              LEFT JOIN tbl_CodeMaster cd
          --                                     ON cd.ID = b.AccountTypeID
          --                       WHERE  b.IsDeleted = 0
          --                              AND b.DomainID = @DomainID
          --                       FOR XML PATH('')), 1, 1, '') AS ID)

          SELECT CONVERT(VARCHAR, AvailableCash) + '/'
                 --+ CONVERT(VARCHAR, @ID)
				  AS Amount
          --SELECT AvailableCash AS Amount
          FROM   tblCashBucket
          WHERE  DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
