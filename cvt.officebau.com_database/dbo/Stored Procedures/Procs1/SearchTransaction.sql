﻿/****************************************************************************             
CREATED BY  : Naneeshwar            
CREATED DATE : 14-Nov-2016          
MODIFIED BY  : JENNIFER.S           
MODIFIED DATE : 04-MAY-2017        
 <summary>          
 [SearchTransaction] 1,'','','','All','All','EqualTo',NULL,0,''        
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchTransaction] (@DomainID INT,
@StartDate VARCHAR(50),
@EndDate VARCHAR(50),
@VendorName VARCHAR(50),
@Status VARCHAR(50),
@ScreenType VARCHAR(50),
@Notations VARCHAR(50),
@Amount MONEY,
@CostCenterID INT = 0,
@LedgerName VARCHAR(50))
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		DECLARE @Count INT
			   ,@NoOfRecords INT = (SELECT
						value
					FROM tblApplicationConfiguration
					WHERE Isdeleted = 0
					AND DomainId = @DomainID
					AND Code = 'NRECORD')
		DECLARE @table TABLE (
			DetailDescription VARCHAR(8000)
		   ,EID INT
		)

		IF (@StartDate != '')
			SET @StartDate = CAST(@StartDate AS DATETIME)

		IF (@EndDate != '')
			SET @EndDate = CAST(@EndDate AS DATETIME)

		INSERT INTO @table
			SELECT
				(SELECT
						SUBSTRING((SELECT
								+',' + ed.Remarks
							FROM tblExpenseDetails ed
							WHERE ed.Isdeleted = 0
							AND ex.Id = ed.ExpenseID
							FOR XML PATH (''))
						, 2, 200000))
			   ,ex.Id
			FROM tblExpense ex;

		WITH CTE
		AS
		(SELECT DISTINCT
				ex.Id AS Id
			   ,ex.type AS [type]
			   ,ex.DATE AS BillDate
			   ,ex.DueDate AS DueDate
			   ,ex.BillNo AS BillNo
			   ,ex.VendorID AS VendorID
			   ,ex.CostCenterID AS CostCenterID
			   ,cc.NAME AS CostCenterName
			   ,vd.NAME AS VendorName
			   ,(SELECT
						ISNULL(DetailDescription, '')
					FROM @table d
					WHERE d.EID = ex.Id)
				AS [Description]
			   ,((SELECT
						((ISNULL(SUM(QTY * Amount), 0)) + ISNULL(SUM(CGST), 0) + ISNULL(SUM(SGST), 0))
					FROM tblExpenseDetails
					WHERE ExpenseID = ex.Id
					AND ISNULL(Isdeleted, 0) = 0
					AND DomainId = @domainID)
				) AS TotalAmount
			   ,cd.Code AS [Status]
			   ,ex.CreatedOn
			   ,ex.ModifiedOn,
			   ex.IsApprovalRequired,
							 ex.IsApproved 
			   ,CAST(fu.Id AS VARCHAR(50)) AS HistoryId,
			   CASE WHEN ((SELECT COUNT(*) FROM tblExpenseDetails  WHERE  isdeleted =0 AND ExpenseID = ex.Id AND isLedger =1) !=0)
			   THEN (SELECT Name FROM tblLedger WHERE id = (SELECT TOP 1 ledgerID FROM tblExpenseDetails  WHERE  isdeleted =0 AND ExpenseID = ex.Id AND isLedger =1) )
			   ELSE (SELECT Name FROM tblProduct_v2 AS p WHERE ID  =(SELECT TOP 1 ledgerID FROM tblExpenseDetails  WHERE  isdeleted =0 AND ExpenseID = ex.Id AND isLedger =0) )
	END AS Ledger
			FROM tblExpense ex
			JOIN tblExpenseDetails ed
				ON ed.ExpenseID = ex.Id
			LEFT JOIN tblLedger Ld
				ON Ld.Id = ed.LedgerID
			LEFT JOIN tblVendor vd
				ON vd.Id = ex.VendorID
			LEFT JOIN tbl_CodeMaster cd
				ON cd.Id = ex.StatusID
			LEFT JOIN tblCostCenter cc
				ON cc.Id = ex.CostCenterID
			LEFT JOIN tbl_FileUpload fu
				ON fu.Id = ex.HistoryId
			WHERE ISNULL(ex.Isdeleted, 0) = 0
			AND ex.DomainId = @domainID
			AND ((ISNULL(@Status, '') = ''
			AND ex.StatusID IN (SELECT
					Id
				FROM tbl_CodeMaster
				WHERE [type] != 'Close'
				AND Isdeleted = 0)
			)
			OR (ISNULL(@Status, '') != 'All'
			AND ex.StatusID IN (SELECT
					Id
				FROM tbl_CodeMaster
				WHERE [type] = @Status
				AND Isdeleted = 0)
			)
			OR (ISNULL(@Status, '') = 'All'
			AND ex.StatusID IN (SELECT
					Id
				FROM tbl_CodeMaster
				WHERE [type] IN ('Open', 'Partial', 'Close')
				AND Isdeleted = 0)
			))
			AND ((ISNULL(@ScreenType, '') = ''
			AND ex.type LIKE '%' + @ScreenType + '%')
			OR (ISNULL(@ScreenType, '') = 'All'
			AND ex.type IN ('Expense', 'Bill'))
			OR (ISNULL(@ScreenType, '') != 'All'
			AND ex.type IN (@ScreenType)))
			AND (ISNULL(@VendorName, '') = ''
			OR vd.NAME LIKE '%' + @VendorName + '%')
			AND (ISNULL(@LedgerName, '') = ''
			OR Ld.NAME LIKE '%' + @LedgerName + '%')
			AND ((@StartDate IS NULL
			OR @StartDate = '')
			OR [DATE] >= @StartDate)
			AND ((@EndDate IS NULL
			OR @EndDate = '')
			OR [DATE] <= @EndDate)
			AND ((ISNULL(@CostCenterID, '') = ''
			OR ex.CostCenterID = @CostCenterID)))
		SELECT
			* INTO #TempExpense
		FROM CTE
		WHERE (ISNULL(@Amount, 0) = 0)
		OR (ISNULL(@Notations, '') = 'EqualTo'
		AND TotalAmount = ISNULL(@Amount, 0))
		OR (ISNULL(@Notations, '') = 'GreaterThanEqual'
		AND TotalAmount >= @Amount)
		OR (ISNULL(@Notations, '') = 'LessThanEqual'
		AND TotalAmount <= @Amount)

		SET @Count = (SELECT
				COUNT(1)
			FROM #TempExpense)

		SELECT TOP (ISNULL(@NoOfRecords, @Count))
			*
		FROM #TempExpense
		ORDER BY BillDate DESC
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMsg VARCHAR(100)
			   ,@ErrSeverity TINYINT
		SELECT
			@ErrorMsg = ERROR_MESSAGE()
		   ,@ErrSeverity = ERROR_SEVERITY()
		RAISERROR (@ErrorMsg, @ErrSeverity, 1)
	END CATCH
END

