﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	28-NOV-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary> 
 [Deletecfsentry] 1,1,1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteCFSEntry] (@ID        INT,
                                        @SessionID INT,
                                        @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output      VARCHAR(100) = 'Operation Failed!',
              @ContainerNo VARCHAR(50)

      SET @ContainerNo =(SELECT ContainerName
                         FROM   tbl_CFSEntry
                         WHERE  id = @ID)

      BEGIN TRY
          BEGIN TRANSACTION

          BEGIN
              IF NOT EXISTS(SELECT 1
                            FROM   tbl_cfsOperation
                            WHERE  ContainerNo = @ContainerNo)
                BEGIN
                    UPDATE tbl_CFSEntry
                    SET    IsDeleted = 1,
                           ModifiedBY = @SessionID
                    WHERE  ID = @ID
                           AND DomainID = @DomainID

                    SET @Output = 'Deleted Successfully'

                    GOTO Finish
                END
              ELSE
                BEGIN
                    SET @Output = 'The Container No has been used in CFS Operation.'

                    GOTO Finish
                END
          END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
