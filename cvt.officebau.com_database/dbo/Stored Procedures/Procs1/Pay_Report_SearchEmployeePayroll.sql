﻿/****************************************************************************             
CREATED BY  :  Priya K          
CREATED DATE :            
MODIFIED BY  :             
MODIFIED DATE :   12-NOV-2018          
<summary>          
 [Pay_Report_SearchEmployeePayroll] 1, '1,2,3,4,5,6,7,8,9,10,11,12', 0,1054,4          
</summary>                                     
*****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_Report_SearchEmployeePayroll] (@DomainID        INT,
                                                        @Location        VARCHAR(100),
                                                        @DepartmentID    INT,
                                                        @ComponentID     INT,
                                                        @FinancialYearID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @getDate DATE,
                  @MonthID INT = 3,
                  @YearID  INT,
                  @cols    AS NVARCHAR(MAX),
                  @query   AS NVARCHAR(MAX)

          SET @YearID = (SELECT Cast(NAME AS INT) + 1
                         FROM   tbl_FinancialYear
                         WHERE  ID = @FinancialYearID
                                AND Isnull(IsDeleted, 0) = 0
                                AND DomainID = @DomainID)

          CREATE TABLE #MonthTable
            (
               ID      TINYINT IDENTITY(1, 1),
               Months  VARCHAR(10),
               MonthID INT,
               Years   SMALLINT
            )

          CREATE TABLE #DataSource
            (
               [Value] NVARCHAR(128)
            )

          INSERT INTO #DataSource
                      ([Value])
          SELECT Item
          FROM   dbo.Splitstring (@Location, ',')
          WHERE  Isnull(Item, '') <> ''

          SET @getDate = Cast(CONVERT(VARCHAR, @YearID) + '-'
                              + CONVERT(VARCHAR, @MonthID) + '-01' AS DATETIME);

          WITH DateRange
               AS (SELECT @getDate Dates
                   UNION ALL
                   SELECT Dateadd(mm, -1, Dates)
                   FROM   DateRange
                   WHERE  Dates > Dateadd(mm, -11, @getDate))
          INSERT INTO #MonthTable
          SELECT CONVERT(CHAR(3), Datename(m, Dates), 0) Months,
                 Month(Dates)                            AS MonthID,
                 Year(Dates)                             Years
          FROM   DateRange

          SELECT emp.ID                                         AS EmployeeId,
                 Isnull(emp.EmpCodePattern, '') + emp.Code      AS [Emp_Code],
                 emp.FirstName + ' ' + Isnull(emp.LastName, '') AS [Emp Name],
                 CONVERT(VARCHAR(20), emp.DOJ, 106)             AS JoiningDate,
                 BU.NAME                                        AS Business_Unit,
                 emp.BaseLocationID                             AS BaseLocationID,
                 DEP.NAME                                       AS [Department_Name],
                 ep.DomainId                                    AS DomainId,
                 MT.Months                                      AS [Month],
                 ( Amount )                                     AS Total_Amount
          INTO   #Result
          FROM   #MonthTable MT
                 LEFT JOIN tbl_pay_employeepayroll ep
                        ON ep.MonthId = MT.MonthId
                           AND MT.Years = ep.YearID
                           AND ep.isdeleted = 0
                 LEFT JOIN tbl_EmployeeMaster emp
                        ON emp.ID = ep.EmployeeId
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = emp.BaseLocationID
                 LEFT JOIN tbl_Department DEP
                        ON DEP.ID = EMP.DepartmentID
                 LEFT JOIN tbl_Pay_EmployeePayrollDetails pd
                        ON pd.payrollID = ep.ID
          WHERE  pd.ComponentID = @ComponentID
                 AND ep.IsProcessed = 1
                 AND ep.IsDeleted = 0
                 AND ep.DomainId = @DomainID
                 AND ( ( @DepartmentID IS NULL
                          OR Isnull(@DepartmentID, 0) = 0 )
                        OR emp.DepartmentID = @DepartmentID )
                 AND ( ( @Location = Cast(0 AS VARCHAR) )
                        OR emp.BaseLocationID IN (SELECT [Value]
                                                  FROM   #DataSource) )
                 AND ( emp.BaseLocationID <> 0
                        OR emp.BaseLocationID <> '' )
          ORDER  BY MT.ID

          DECLARE @cols1 VARCHAR(Max)

          SET @cols = Stuff((SELECT ',' + Quotename(Months)
                             FROM   #MonthTable
                             ORDER  BY ID DESC
                             FOR XML path('')), 1, 1, ' ')
          SET @cols1 = Stuff((SELECT '+(ISNULL(' + Quotename(Months) + ',0))'
                              FROM   #MonthTable
                              ORDER  BY ID DESC
                              FOR XML path('')), 1, 1, ' ')
          SET @query = 'SELECT  Emp_Code,[Emp Name],JoiningDate as Joining_Date,Business_Unit,[Department_Name],'
                       + @cols1 + ' AS Total_Amount,' + @cols
                       + ' from       
             (      
                select  Emp_Code,[Emp Name],JoiningDate,Business_Unit,[Department_Name],[Month],Total_Amount      
                from #Result      
            ) x      
            pivot       
            (      
                sum(Total_Amount)      
                for [Month] in (' + @cols
                       + ')      
            ) p '

          EXECUTE(@query);
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
