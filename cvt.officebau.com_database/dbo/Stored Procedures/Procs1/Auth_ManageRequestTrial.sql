﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	05-01-2016
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
    [Auth_ManageRequestTrial] 'nupro marketing limited','rajbharath_n@sbn.com','des','','',''

	select * from Auth_RequestTrial
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Auth_ManageRequestTrial] (@CompanyName     VARCHAR(100),
                                                 @EmailID         VARCHAR(100),
                                                 @Description     VARCHAR(MAX),
                                                 @Location        VARCHAR(100),
                                                 @DefaultPassword VARCHAR(100),
                                                 @ContactNo       VARCHAR(100),
                                                 @Modules         VARCHAR(100),
												 @IndustryID      INT,
												 @TeamSize INT)
AS
  BEGIN
      SET NOCOUNT ON

      BEGIN TRY
          SET NOCOUNT ON;

          DECLARE @Output   VARCHAR(100) = 'Operation Failed!',
                  @DomainID INT
         
			
          BEGIN TRANSACTION

		 IF EXISTS ( SELECT 1 
		             FROM Auth_RequestTrial 
					 WHERE IsDeleted = 0
					 AND CompanyName = @CompanyName )
		 BEGIN
				SET @Output = 'Company name already exist !'
		 END
         else
		 BEGIN
		  IF NOT EXISTS (SELECT 1
                         FROM   Auth_RequestTrial
                         WHERE  EmailID = @EmailID
                                AND IsDeleted = 0)
            BEGIN
                IF NOT EXISTS (SELECT 1
                               FROM   tbl_EmployeeMaster
                               WHERE  LoginCode = @EmailID
                                      AND IsDeleted = 0)
                  BEGIN
                      INSERT INTO Auth_RequestTrial
                                  (CompanyName,
                                   EmailID,
                                   [Description],
								   IndustryID,
								   TeamSize)
                      VALUES      ( @CompanyName,
                                    @EmailID,
                                    @Description,
									@IndustryID,
									@TeamSize )

                      EXEC Auth_Createbusinessaccess
                        @CompanyName,
                        @Location,
                        @EmailID,
                        @DefaultPassword,
                        @ContactNo,
                        NULL,
                        @Modules

                      SET @DomainID = (SELECT TOP 1 ID
                                       FROM   tbl_Company
                                       WHERE  NAME = @CompanyName
                                       ORDER  BY ModifiedOn DESC)
                      SET @Output = 'OK/' + Cast(@DomainID AS VARCHAR)
                  END
                ELSE
                  BEGIN
                      SET @Output = 'This Email Already Requested !'
                  END
            END
          ELSE
            BEGIN
                SET @Output = 'This Email Already Requested !'
            END
 END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
