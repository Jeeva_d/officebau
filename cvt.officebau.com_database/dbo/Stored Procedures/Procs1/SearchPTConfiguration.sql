﻿
/****************************************************************************   
CREATED BY   :  Naneeshwar.M
CREATED DATE  :   20-SEP-2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          [Searchptconfiguration] 2,3,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchPTConfiguration] (@BusinessUnitID  INT,
                                               @FinancialYearID INT,
                                               @DomainID        INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @FinancialYearName INT

      SET @FinancialYearName=(SELECT NAME
                              FROM   tbl_FinancialYear
                              WHERE  id = @FinancialYearID
                                     AND IsDeleted = 0
                                     AND DomainID = @DomainID)

      BEGIN TRY
          BEGIN
              SELECT PT.ID          AS ID,
                     MinGross       AS MinGross,
                     MaxGross       AS MaxGross,
                     Amount         AS Amount,
                     bu.NAME        AS Businessunit,
                     fy.DisplayName AS FinancialYear
              FROM   tbl_PTConfiguration pt
                     LEFT JOIN tbl_BusinessUnit BU
                            ON bu.ID = pt.BusinessUnitID
                     LEFT JOIN tbl_FinancialYear FY
                            ON FY.ID = @FinancialYearID
              WHERE  BusinessUnitID = @BusinessUnitID
                     AND pt.IsDeleted = 0
                     AND FYID = @FinancialYearName
					 and pt.domainid = @DomainID
          END
      END TRY
      BEGIN CATCH         
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
