﻿/****************************************************************************               
CREATED BY   : Priya K              
CREATED DATE  :   21-Nov-2018            
MODIFIED BY   :              
 <summary>            
 [GetStockAdjustmentDetails] 2,1            
 </summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetStockAdjustmentDetails] (@ID       INT,
                                   @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT tisa.ID                                                                  AS ID,
                 tisa.LedgerID                                                            AS LedgerID,
				 ldg.Name                                                                 AS LedgerName,
                 tisa.AdjustedDate                                                        AS AdjustedDate,
                 tisa.ReasonID                                                            AS ReasonID,
                 tir.Name                                                                 AS ReasonName,
                 tisa.Remarks                                                             AS Remarks,
                 tisa.ModifiedOn                                                           AS ModifiedOn,
                 EMP.FullName                                            AS ModifiedBy
          FROM   tbl_InventoryStockAdjustment tisa
                 LEFT JOIN tblLedger ldg
                        ON ldg.ID = tisa.LedgerID
                 LEFT JOIN tbl_InventoryReason tir
                        ON tir.ID = tisa.ReasonID
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = tisa.ModifiedBy
          WHERE  tisa.IsDeleted = 0
                 AND ( tisa.ID = @ID
                        OR @ID IS NULL )
                 AND tisa.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
