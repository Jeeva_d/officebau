﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	01-04-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
     Search Packages, based on country 
	 [Auth_SearchPackages] 'IN'

	 SELECT * FROM Auth_Package WHERE CountryCode = 'IN'
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_SearchPackages]
 (
	@CountryCode	VARCHAR(10)
 )
 AS
 BEGIN
	BEGIN TRY		
		SELECT 
			ID				AS ID				,
			PackageName		AS PackageName		,
			UP1				AS UP1				,
			UP2				AS UP2				,
			UP3				AS UP3				,
			UP4				AS UP4				,
			CurrencyCode	AS CurrencyCode		
		FROM 
			Auth_Package 
		WHERE 
			CountryCode = @CountryCode AND IsDeleted = 0
		ORDER BY OrderNumber

	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
