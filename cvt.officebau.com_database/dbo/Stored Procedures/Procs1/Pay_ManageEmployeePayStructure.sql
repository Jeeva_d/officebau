﻿/****************************************************************************  
CREATED BY   :  
CREATED DATE  :  
MODIFIED BY   :  
MODIFIED DATE  :  
 <summary>  
 </summary>  
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_ManageEmployeePayStructure]  
(  
     @ID INT,  
     @EffectiveFrom DATETIME,  
     @CompanyPayStructureId INT,  
     @EmployeeID INT,  
     @SessionID INT,  
     @IsDeleted BIT,  
     @DomainID INT  
)  
AS  
     BEGIN  
          SET NOCOUNT ON;  
            
          DECLARE @Output VARCHAR(8000)  
            
          BEGIN TRY  
               BEGIN TRANSACTION  
			    IF EXISTS(SELECT TOP 1 Id FROM tbl_Pay_EmployeePayStructure WHERE @ID <> ID and EffectiveFrom = @EffectiveFrom  and  EmployeeID = @EmployeeID 
			                                   AND IsDeleted = 0 AND CompanyPayStructureID = @CompanyPayStructureID AND DomainID=@DomainID) 
		    BEGIN
				SET @Output = (SELECT
						[Message]
					FROM tblErrorMessage
					WHERE [Type] = 'Warning'
					AND Code = 'RCD_EXIST'
					AND IsDeleted = 0)
				GOTO FINISH;
			END


               IF EXISTS  
               (SELECT  
                    TOP 1 ID  
               FROM  
                    tbl_Pay_EmployeePayroll  
               WHERE  
                    EmployeePayStructureID = @ID AND IsDeleted = 0 AND DomainID = @DomainID)  
               BEGIN  
                    SET @Output =  
                    (SELECT  
                         [Message]  
                    FROM  
                         tblErrorMessage  
                    WHERE  
                         [type] = 'Warning' AND Code = 'RCD_REF' AND IsDeleted = 0)  
                      
                    GOTO FINISH;  
               END  
               IF Exists  
               (Select  
                    Top 1 Id  
               from  
                    tbl_Pay_EmployeePayStructure  
               WHERE  
                    EffectiveFrom =@EffectiveFrom And ID <>@ID and EmployeeID=@EmployeeID AND IsDeleted = 0)  
               BEGIN  
                    SET @Output = 'Already Paystructure configured for the selected date'  
                      
                    GOTO FINISH;  
               END  
                 
               IF   
               ( SELECT  
                    DATEADD(day,-1, pcps.EffectiveFrom)  
               FROM  
                    tbl_Pay_CompanyPayStructure pcps  
             where pcps.ID=@CompanyPayStructureId ) >=  @EffectiveFrom
                 
               BEGIN  
                    SET @Output = 'Effective from should be greater than Company Paystructure Effective from'  
                      
                    GOTO FINISH;  
               END  
                  IF   
               ( SELECT  
                    DATEADD(day,-1, pcps.DOJ)  
               FROM  
                    tbl_Employeemaster pcps  
             where pcps.ID=@EmployeeID ) >= @EffectiveFrom  
                 
               BEGIN  
                    SET @Output = 'Effective from should be greater than Date of joining'  
                      
                    GOTO FINISH;  
               END  
                 
                 
               --------------Delete --------------  
               IF @IsDeleted = 1  
               BEGIN  
                    UPDATE  
                         tbl_Pay_EmployeePayStructure  
                    SET  IsDeleted  = 1          ,  
                         ModifiedBy = @SessionID ,  
                         ModifiedOn = GETDATE()  
                    WHERE  
                         ID = @ID  
                      
                    UPDATE  
                         tbl_Pay_EmployeePayStructureDetails  
                    SET  IsDeleted  = 1          ,  
                         ModifiedBy = @SessionID ,  
                         ModifiedOn = GETDATE()  
                    WHERE  
                         tbl_Pay_EmployeePayStructureDetails.PayStructureId = @ID  
                      
                    SET @Output =  
                    (SELECT  
                         [Message]  
                    FROM  
                         tblErrorMessage  
                    WHERE  
                         [type] = 'Information' AND Code = 'RCD_DEL' AND IsDeleted = 0)  

                    GOTO FINISH;  
               END  
                 
               ---------------------------Update ----------------------------  
                 
               IF @ID <>0  
               BEGIN  
                    UPDATE  
                         tbl_Pay_EmployeePayStructure  
                    SET  CompanyPayStructureID = @CompanyPayStructureID,  
                         EffectiveFrom         = @EffectiveFrom        ,  
                         EmployeeID            = @EmployeeID           ,  
                         DomainID              = @DomainID             ,  
           ModifiedBy            = @SessionID            ,  
                         ModifiedOn            = GETDATE()  
                    WHERE  
                         ID = @ID  
                      
                      
                    SET @Output =  
                    ( SELECT  
                         [Message]  
                    FROM  
                         tblErrorMessage  
                    WHERE  
                         [Type] = 'Information' AND Code = 'RCD_UPD' AND IsDeleted = 0)  
                    GOTO FINISH;  
               END  
                 
                 
                 
               ---------------------------Insert ----------------------------  
               IF( Isnull(@ID, 0) = 0 ) 
                    BEGIN  
                         INSERT INTO tbl_Pay_EmployeePayStructure  
                              (  
                                   CompanyPayStructureID,  
                                   EmployeeID           ,  
                                   EffectiveFrom        ,  
                                   IsDeleted            ,  
                                   DomainID             ,  
                                   CreatedBy            ,  
                                   CreatedOn            ,  
                                   ModifiedBy           ,  
                                   ModifiedOn           ,  
                                   HistoryID  
                              )  
                         SELECT  
                              @CompanyPayStructureId ,  
                              @EmployeeID            ,  
                              @EffectiveFrom ,
							  0,  
                              @DomainID  ,  
                              @SessionID ,  
                              GETDATE()  ,  
                              @SessionID ,  
                              GETDATE()  ,  
                              NEWID()  
                         ;  
                           
                         SET @Output =  
                         (SELECT  
                              [Message]  
                         FROM  
                              tblErrorMessage  
                         WHERE  
                              [type] = 'Information' AND Code = 'RCD_INS' AND IsDeleted = 0)  
                         + '/' + CAST(@@identity AS VARCHAR)  
                         GOTO FINISH;  
                    END  
                
                 
               FINISH:  
               SELECT @Output COMMIT TRANSACTION  
          END TRY  
          BEGIN CATCH  
               ROLLBACK TRANSACTION  
               DECLARE @ErrorMsg VARCHAR(100) ,  
                    @ErrSeverity TINYINT  
               SELECT  
                    @ErrorMsg    = ERROR_MESSAGE() ,  
                    @ErrSeverity = ERROR_SEVERITY()  
               RAISERROR (@ErrorMsg, @ErrSeverity, 1)  
          END CATCH  
     END
