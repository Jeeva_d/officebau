﻿/****************************************************************************             
CREATED BY   : DHANALAKSHMI.S            
CREATED DATE  :04-FEB-2019      
MODIFIED BY   :             
MODIFIED DATE  :             
 <summary>          
 [GetBankBasedonPaymentMode]  'Credit Card' ,1    
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetBankBasedonPaymentMode] (@PaymentMode VARCHAR(50),
                                                    @DomainID    INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          IF ( Isnull(@PaymentMode, '') = 'Credit Card' )
            BEGIN
                SELECT b.ID   AS ID,
                       b.NAME AS NAME
                FROM   tblBank b
                WHERE  b.DomainID = @DomainID
                       AND b.AccountTypeID = (SELECT ID
                                              FROM   tbl_CodeMaster
                                              WHERE  Code IN ( 'Credit Card' )
                                                     AND [Type] = 'Account')
                       AND b.IsDeleted = 0
                ORDER  BY NAME
            END
          ELSE
            BEGIN
                SELECT b.ID   AS ID,
                       b.NAME AS NAME
                FROM   tblBank b
                WHERE  b.DomainID = @DomainID
                       AND b.AccountTypeID NOT IN (SELECT ID
                                                   FROM   tbl_CodeMaster
                                                   WHERE  Code IN ( 'Credit Card' )
                                                          AND [Type] = 'Account')
                       AND b.IsDeleted = 0
                ORDER  BY NAME
            END

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
