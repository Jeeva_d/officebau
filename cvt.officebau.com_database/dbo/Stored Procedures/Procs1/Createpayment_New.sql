﻿/****************************************************************************                 
CREATED BY    :               
CREATED DATE  :             
MODIFIED BY   :             
MODIFIED DATE :              
<summary>               
[Createpayment_New] '12-12-2017','','',20,5,2,29,1,1           
</summary>                                         
*****************************************************************************/              
CREATE PROCEDURE [dbo].[Createpayment_New] (@Date            DATETIME,              
                                       @Description     VARCHAR(1000),              
                                       @PartyName       VARCHAR(100),              
                                       @TotalAmount     MONEY,              
                                       @PaymentModeID   INT,              
                                       @BankID          INT,              
                                       @LedgerID        INT,              
                                       @SessionID       INT,              
                                       @DomainID        INT    ,        
              @Payment PAYMENT readonly              
            )              
AS              
  BEGIN              
      SET NOCOUNT ON;              
              
      BEGIN TRY              
          BEGIN TRANSACTION              
              
          DECLARE @OUTPUT            VARCHAR(100)            
              
          IF( (SELECT Count(1)              
               FROM   tblCashBucket              
               WHERE  DomainID = @DomainID) = 0 )              
            INSERT INTO tblCashBucket              
                        (AvailableCash,              
                         DomainID)              
            VALUES      (0,              
                         @DomainID)              
              
          INSERT INTO tbl_payment            
                      (PartyName,              
                       [PaymentDate],              
                       [Description],              
                       ModeID,              
                       BankID,              
                       LedgerID,              
                       DomainID,              
                       CreatedBy,              
                       ModifiedBy,TotalAmount)              
          VALUES      (@PartyName,              
                       @Date,              
                       @Description,              
                       @PaymentModeID,              
                       @BankID,              
                       @LedgerID,              
                       @DomainID,              
                       @SessionID,              
                       @SessionID,@TotalAmount)              
              
          DECLARE @SourceID INT = @@IDENTITY              
            
    Insert Into tbl_paymentMapping (PaymentID,RefID,Amount,Isdeleted,DomainId,CreatedBy,ModifiedBy)            
    Select @SourceID ,RefID,amount,Isdeleted,@DomainID,@SessionID,@SessionID from @PAYMENT            
              update tbl_paymentMapping set Amount=p.Amount from tbl_paymentMapping join @Payment p on p.ID=0 
          IF( Isnull(@BankID, 0) <> 0 )              
            BEGIN              
                INSERT INTO tblBRS              
                            (BankID,              
                             SourceID,              
                             SourceDate,              
                             SourceType,              
                             Amount,              
                             DomainID,              
                             BRSDescription,              
                             CreatedBy,              
                             ModifiedBy)              
                VALUES      (@BankID,              
                             @SourceID,              
                             @Date,              
                             (SELECT ID              
                                       FROM   tblMasterTypes              
                                WHERE  NAME = 'Make Payment')             
                              ,              
                             @TotalAmount,              
             @DomainID,              
                   @Description,              
                             @SessionID,              
                             @SessionID)              
              
                INSERT INTO tblBookedBankBalance              
                            (BRSID,              
                             Amount,              
            DomainID,              
                             CreatedBy,              
                             ModifiedBy)              
                VALUES      (@@IDENTITY,              
          @TotalAmount,              
                             @DomainID,              
                             @SessionID,              
                             @SessionID)              
              
                EXEC Managesystembankbalance              
                  @DomainID              
            END              
              
          IF( @PaymentModeID = (SELECT ID              
                                FROM   tbl_CodeMaster              
                                WHERE [Type] = 'PaymentType' and Code = 'Cash' and IsDeleted = 0) )              
            BEGIN              
                UPDATE tblCashBucket              
                SET    AvailableCash = AvailableCash - @TotalAmount,              
                       ModifiedOn = Getdate()              
                WHERE  DomainID = @DomainID              
              
                INSERT INTO tblVirtualCash              
                            (SourceID,              
                             Amount,              
                             Date,              
                             SourceType,              
                             DomainID,              
                             CreatedBy,              
                             ModifiedBy)              
                VALUES      (@SourceID,              
                             0 - @TotalAmount ,              
                             @Date,              
                            (SELECT ID              
                                       FROM   tblMasterTypes              
                                       WHERE  NAME = 'Make Payment'),              
                             @DomainID,              
                             @SessionID,              
                             @SessionID)              
            END              
              
          SET @Output = (SELECT [Message]              
                         FROM   tblErrorMessage              
                         WHERE  [Type] = 'Information'              
                                AND Code = 'RCD_INS'              
                                AND IsDeleted = 0) --'Inserted Successfully'              
              
          SELECT @Output              
              
          COMMIT TRANSACTION              
      END TRY              
      BEGIN CATCH              
          ROLLBACK TRANSACTION              
          DECLARE @ErrorMsg    VARCHAR(100),              
                  @ErrSeverity TINYINT              
          SELECT @ErrorMsg = Error_message(),              
                 @ErrSeverity = Error_severity()              
          RAISERROR(@ErrorMsg,@ErrSeverity,1)              
      END CATCH              
  END
