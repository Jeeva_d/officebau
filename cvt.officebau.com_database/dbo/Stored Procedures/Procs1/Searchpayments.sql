﻿      
/****************************************************************************             
CREATED BY   :             
CREATED DATE  : 
MODIFIED DATE  :   
 <summary>          
 </summary>                                     
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Searchpayments] (@DomainID  INT,      
                                               @StartDate VARCHAR(50),      
                                               @EndDate   VARCHAR(50),      
                                               @PartyName VARCHAR(50),      
                                               @Notations VARCHAR(50),      
                                               @Amount    MONEY,       
                                               @LedgerName VARCHAR(50))      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          IF( @StartDate != '' )      
            SET @StartDate = Cast(@StartDate AS DATETIME)      
      
          IF( @EndDate != '' )      
            SET @EndDate = Cast(@EndDate AS DATETIME)      
      
          SELECT ar.ID            AS ID,      
                 ar.PartyName     AS PartyName,      
                 ar.PaymentDate          AS Date,     
                 ld.name AS LedgerName,
                 TotalAmount     AS Amount,      
                 ar.[Description] AS [Description],
				  bk.Name AS BankName,
				 cm.Code AS PaymentMode       
          INTO   #TempPayments      
          FROM   tbl_Payment ar   
     LEFT JOIN tblLedger ld      
                        ON ar.LedgerID = ld.ID    
	 LEFT JOIN tblBank bk      
                        ON ar.BankID = bk.ID 
	 LEFT JOIN tbl_CodeMaster cm      
                        ON ar.ModeID = cm.ID 					 	  
          WHERE  ar.IsDeleted = 0      
                    
                 AND ar.DomainID = @domainID      
                 AND ( ( @StartDate IS NULL      
                          OR @StartDate = '' )      
                        OR PaymentDate >= @StartDate )      
                 AND ( ( @EndDate IS NULL      
                          OR @EndDate = '' )      
                        OR PaymentDate <= @EndDate )      
                 AND ( Isnull(@PartyName, '') = ''      
                        OR ar.PartyName LIKE '%' + @PartyName + '%' )      
                 AND ( Isnull(@LedgerName, '') = ''      
                        OR ld.NAME LIKE '%' + @LedgerName + '%' )
          ORDER  BY ar.ModifiedOn DESC 

          SELECT *      
          FROM   #TempPayments      
          WHERE  ( Isnull(@Amount, 0) = 0 )      
                  OR ( Isnull(@Notations, '') = 'EqualTo'      
                       AND Amount = @Amount )      
                  OR ( Isnull(@Notations, '') = 'GreaterThanEqual'      
                       AND Amount >= @Amount )      
                  OR ( Isnull(@Notations, '') = 'LessThanEqual'      
                       AND Amount <= @Amount ) 
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
