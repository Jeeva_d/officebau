﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	06-01-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	Update Isemail send set true

	select * from Auth_RequestTrial
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_SendRequestTrial]
 (
	@IsReject		INT					,
	@ID				INT					,
	@Email			VARCHAR(100)		,
	@AppPassword	VARCHAR(10)		
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY		
	SET NOCOUNT ON;
		DECLARE @Output						VARCHAR(100)				
		BEGIN TRANSACTION	
			IF(@IsReject = 0)
				BEGIN
					UPDATE
						Auth_RequestTrial
					SET
						IsEmailSend = 1	,
						AppPassword = @AppPassword
					WHERE
						EmailID = @Email AND ID = @ID AND IsDeleted = 0
				END
			ELSE
				BEGIN
					UPDATE
						Auth_RequestTrial
					SET
						IsReject = 1	
					WHERE
						EmailID = @Email AND ID = @ID AND IsDeleted = 0
				END

				SET @Output = 'OK'

			SELECT @Output
		COMMIT TRANSACTION      
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
