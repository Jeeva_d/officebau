﻿/****************************************************************************               
CREATED BY   :               
CREATED DATE  :               
MODIFIED BY   :               
MODIFIED DATE  :               
<summary>                      
[Rpt_TDSReport]   1,'all','10-10-2016','10-10-2019'       
</summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_TDSReport] (@domainID      INT,
                                        @SearchTextbox VARCHAR(MAX),
                                        @FromDate      DATE,
                                        @ToDate        DATE)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @LedgerIDs NVARCHAR(MAX);

          IF( @SearchTextbox = 'ALL' )
            BEGIN
                SELECT @LedgerIDs = COALESCE(@LedgerIDs + ',', '')
                                    + Cast(ID AS VARCHAR(5))
                FROM   tblLedger
                WHERE  IsDeleted = 0
                       AND DomainID = @domainID;
            END
          ELSE
            BEGIN
                SELECT @LedgerIDs = COALESCE(@LedgerIDs + ',', '')
                                    + Cast(ID AS VARCHAR(5))
                FROM   tblLedger l
                       INNER JOIN dbo.FnSplitString(@SearchTextbox, ',') s
                               ON l.NAME LIKE '%' + s.Splitdata + '%'
                WHERE  IsDeleted = 0
                       AND DomainID = @domainID;
            END

          SELECT v.NAME                                      AS Party,
                 l.NAME                                      AS Ledger,
                 RIGHT(CONVERT(VARCHAR, 0) + CONVERT(VARCHAR, Datepart(DAY, Date)), 2)
                 + '-'
                 + CONVERT(VARCHAR(3), Datename(Month, Date))
                 + '-'
                 + CONVERT(VARCHAR(4), Datepart(YEAR, Date)) AS Date,
                 ed.Amount                                   AS Debit,
                 NULL                                        AS Credit,
                 ed.Remarks                                  AS Description
          FROM   tblExpenseDetails ed
                 JOIN tblExpense e
                   ON e.ID = ed.ExpenseID
                 JOIN tblVendor v
                   ON v.ID = e.VendorID
                 JOIN tblLedger l
                   ON l.ID = ed.LedgerID
                 INNER JOIN dbo.FnSplitString(@LedgerIDs, ',') s
                         ON l.ID = Cast(ISNULL(s.Splitdata, 0) AS INT)
          WHERE  ed.DomainID = @domainID
                 AND ed.IsDeleted = 0
                 AND e.Date BETWEEN @FromDate AND @ToDate
          --ORDER BY v.Name asc, date DESC;  
          UNION ALL
          SELECT PartyName,
                 l.NAME,
                 RIGHT(CONVERT(VARCHAR, 0) + CONVERT(VARCHAR, Datepart(DAY, PaymentDate)), 2)
                 + '-'
                 + CONVERT(VARCHAR(3), Datename(Month, PaymentDate))
                 + '-'
                 + CONVERT(VARCHAR(4), Datepart(YEAR, PaymentDate)) AS Date,
                 NULL,
                 TotalAmount,
                 Description
          FROM   tbl_Receipts ed
                 JOIN tblLedger l
                   ON l.ID = ed.LedgerID
                 JOIN dbo.FnSplitString(@LedgerIDs, ',') s
                   ON l.ID = Cast(ISNULL(s.Splitdata, 0) AS INT)
          WHERE  ed.DomainID = @domainID
                 AND ed.Isdeleted = 0
                 AND ed.PaymentDate BETWEEN @FromDate AND @ToDate
          UNION ALL
          SELECT PartyName,
                 l.NAME,
                 RIGHT(CONVERT(VARCHAR, 0) + CONVERT(VARCHAR, Datepart(DAY, PaymentDate)), 2)
                 + '-'
                 + CONVERT(VARCHAR(3), Datename(Month, PaymentDate))
                 + '-'
                 + CONVERT(VARCHAR(4), Datepart(YEAR, PaymentDate)) AS Date,
                 TotalAmount,
                 NULL,
                 Description
          FROM   tbl_Payment ed
                 JOIN tblLedger l
                   ON l.ID = ed.LedgerID
                 JOIN dbo.FnSplitString(@LedgerIDs, ',') s
                   ON l.ID = Cast(ISNULL(s.Splitdata, 0) AS INT)
          WHERE  ed.DomainID = @domainID
                 AND ed.Isdeleted = 0
                 AND ed.PaymentDate BETWEEN @FromDate AND @ToDate
          ORDER  BY Party ASC,
                    date DESC;
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = ERROR_MESSAGE(),
                 @ErrSeverity = ERROR_SEVERITY();
          RAISERROR(@ErrorMsg,@ErrSeverity,1);
      END CATCH;
  END;
