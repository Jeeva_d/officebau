﻿/****************************************************************************                                       
CREATED BY  :                                    
CREATED DATE :                                 
MODIFIED BY  :                          
MODIFIED DATE :                        
<summary>                                    
  [RPT_CustomerStatement] '01-Jan-2017','01-Jan-2019',1,'PeterJackson'                              
</summary>                                                               
*****************************************************************************/                
CREATE PROCEDURE RPT_CustomerStatement(@StartDate  DATETIME,                
                                      @EndDate    DATETIME,                
                                      @DomainID   INT,                
                                      @LedgerName VARCHAR(100) = NULL)                
AS                
  BEGIN                
      SET NOCOUNT ON;                
                
      BEGIN TRY                
          DECLARE @LedgerID INT                
                
          IF( @LedgerName IS NULL                
               OR @LedgerName = '' )                
            SET @LedgerID = 0                
          ELSE                
            SET @LedgerID = (SELECT TOP 1 ID                
                             FROM   tblCustomer                
                             WHERE  NAME = @LedgerName                
                                    AND DomainID = @DomainID);                
                
          WITH CTE                
               AS (Select i.ID ,i.Date,i.Date OrderID,'Invoice' Type,(QTY*it.Rate)+(CGSTAmount+SGSTAmount) AS Debit,null AS Credit,      
p.Name As Product,it.ItemDescription Description,'A' As OrderName from tblInvoiceItem It       
Join tblProduct_v2 p on it.ProductID=p.ID      
Join tblInvoice i on it.InvoiceID = i.ID      
Where it.IsDeleted=0      
  AND it.DomainID = @DomainID                
                        AND ( i.CustomerID = @LedgerID )                
                          AND ( @StartDate IS NULL                
                                 OR i.Date >= @StartDate )                
                          AND ( @EndDate IS NULL                
                                 OR i.Date <= @EndDate )        
Union All      
Select ip.ID,ip.PaymentDate,i.Date,'Payment',null,ipm.Amount,Case When (ip.BankID=0) Then 'Cash' Else b.DisplayName End,ip.PaymentDescription,'B' from tblInvoicePaymentMapping ipm      
Join tblInvoicePayment ip on ipm.InvoicePaymentID=ip.ID      
Join tblInvoice i on i.ID = ipm.InvoiceID      
Join tblBank b on b.ID=ip.BankID      
Where ipm.IsDeleted=0      
  AND ip.DomainID = @DomainID                
                        AND ( i.CustomerID = @LedgerID )                
                          AND ( @StartDate IS NULL                
                                 OR i.Date >= @StartDate )                
                          AND ( @EndDate IS NULL                
                                 OR i.Date <= @EndDate )      
Union All       
Select i.ID,ih.Date,i.Date,'Invoice',null,ih.Amount,l.Name,null,'C' from tblInvoiceHoldings ih      
Join tblLedger l on l.ID = ih.LedgerId      
Join tblInvoice i on i.ID=ih.InvoiceId      
Where ih.Isdeleted=0      
  AND ih.DomainID = @DomainID                
                        AND ( i.CustomerID = @LedgerID )                
                          AND ( @StartDate IS NULL                
                                 OR i.Date >= @StartDate )                
                          AND ( @EndDate IS NULL                
                                 OR i.Date <= @EndDate )      
Union All       
Select p.Id,p.PaymentDate,p.PaymentDate,'Receipts',TotalAmount,null,l.Name,Description,'D' from tbl_Receipts p       
Join tblLedger l on p.LedgerId=l.ID      
Where p.Isdeleted=0 AND p.DomainID = @DomainID                
                        AND ( p.PartyName = @LedgerName )       
                          AND ( @StartDate IS NULL                
                                 OR p.PaymentDate>= @StartDate )             
                          AND ( @EndDate IS NULL                
                                 OR p.PaymentDate <= @EndDate )      
Union All       
Select p.Id,p.PaymentDate,p.PaymentDate,'Receipts',null,TotalAmount,Case When(p.BankID=0) Then 'Cash' Else  l.Name END,p.Description,      
'E' from tbl_Receipts p       
Join tblBank l on p.BankID=l.ID      
Where p.Isdeleted=0 AND p.DomainID = @DomainID                
                        AND ( p.PartyName = @LedgerName )                
                          AND ( @StartDate IS NULL                
                                 OR p.PaymentDate>= @StartDate )                
                          AND ( @EndDate IS NULL                
                                 OR p.PaymentDate <= @EndDate )      
     UNION ALL
 Select eh.ID,eh.JournalDate,Eh.JournalDate AS OrderByID,'Journal' ,Debit,Credit,eh.JournalNo,eh.Description ,'F' orderbyName from 
 tblJournal Eh          
 Join tblCustomer l on l.ID = eh.LedgerProductID and eh.type='Customer'          
 where eh.Isdeleted=0  AND eh.DomainID = @DomainID                
                        AND ( eh.LedgerProductID = @LedgerID )                
                          AND ( @StartDate IS NULL                
                                 OR eh.JournalDate>= @StartDate )                
                          AND ( @EndDate IS NULL                
                                 OR eh.JournalDate <= @EndDate )     )                
          SELECT*             
          INTO   #TempResult                
          FROM   CTE                
          ORDER  BY OrderID ASC                
                
          -- OpeningBalance and OutstandingBalance Calculation                              
          DECLARE @OpeningBalance MONEY                
          DECLARE @OutstandingBalance MONEY                
                
          SET @OutstandingBalance = ( (SELECT Sum(Isnull(Credit, 0)) - Sum(Isnull(Debit, 0))                
                                       FROM   #TempResult TEMP                
                                              )                
                                      + ISNULL(@OpeningBalance, 0) )                
                
          SELECT @OpeningBalance     AS OpeningBalance,                
                 @OutstandingBalance AS OutstandingBalance                
                
                SELECT ID,                
                 Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-') [Date],                
                 [Type],                
     Product Particulars,                
               Case When(  Sum(Debit) =0 )then null Else Sum(Debit) END                                          Debit,                
              Case When(  Sum(Credit) =0 )then null Else Sum(Credit) END                                              Credit,                
                   Description             [Description]              
     FROM   #TempResult V                
          GROUP  BY ID,                
                    Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-'),                
                    [Type],                
                    Description,                
                    Product,                
                    OrderID,OrderName                
          ORDER  BY OrderID,                
                    OrderName                
                
          DROP TABLE #TempResult                
      END TRY                
      BEGIN CATCH                
          DECLARE @ErrorMsg    VARCHAR(100),                
                  @ErrSeverity TINYINT                
          SELECT @ErrorMsg = Error_message(),                
                 @ErrSeverity = Error_severity()                
          RAISERROR(@ErrorMsg,@ErrSeverity,1)                
      END CATCH                
  END
