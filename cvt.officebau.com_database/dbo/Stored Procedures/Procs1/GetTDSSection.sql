﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>     
		select * from TDSSection
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetTDSSection] (@ID       INT,
                                       @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT sec.ID            ID,
                 sec.Code          Code,
                 sec.[Description] [Description]
          FROM   TDSSection sec
          WHERE  sec.IsDeleted = 0
                 AND sec.ID = @ID
                 --AND sec.DomainID = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
