﻿/****************************************************************************                         
CREATED BY   : Ajith                       
CREATED DATE  : 21-MAY-2019                        
MODIFIED BY   :                       
MODIFIED DATE  :                      
 <summary>                         
      GetFandFDaysPaid 2,'10 may 2009', '10 feb 2019', 2, 1                
 </summary>                                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetFandFDaysPaid] (@EmployeeID      INT,
                                           @DateOfJoining   DATETIME,
                                           @LastWorkingDate DATETIME,
                                           @SessionID       INT,
                                           @DomainID        INT)
AS
  BEGIN
      BEGIN TRY
          DECLARE @DaysPaid                 INT,
                  @Years                    INT,
                  @TotalDaysInMonth         INT,
                  @YearsOfService           VARCHAR(100),
                  @totalServiceDays         INT,
                  @LastWorkingDateStartDate DATETIME

          SET @LastWorkingDateStartDate = Cast(Year(@LastWorkingDate) AS VARCHAR)
                                          + ' - '
                                          + Cast(Month(@LastWorkingDate) AS VARCHAR)
                                          + ' - 1'
          SET @totalServiceDays = Datediff(DAY, @DateOfJoining, @LastWorkingDate)
          SET @TotalDaysInMonth = Datediff(DAY, @LastWorkingDateStartDate, Dateadd(MONTH, 1, @LastWorkingDateStartDate))
          SET @Years = ( @totalServiceDays / 365 );
          SET @YearsOfService = Cast(@Years AS VARCHAR) + ' Year(s) '
                                + Cast(((@totalServiceDays % 365) / 30) AS VARCHAR)
                                + ' Month(s) '
                                + Cast(((@totalServiceDays % 365) % 30) AS VARCHAR)
                                + ' Day(s)';

          SELECT @DaysPaid = Count(1)
          FROM   tbl_Attendance A
          WHERE  A.IsDeleted = 0
                 AND A.EmployeeID = @EmployeeID
                 AND Year(a.LogDate) = Year(@LastWorkingDate)
                 AND Month(a.LogDate) = Month(@LastWorkingDate)

          SELECT @TotalDaysInMonth                 AS TotalDaysInMonth,
                 @DaysPaid                         AS DaysPaid,
                 @YearsOfService                   AS YearsOfService,
                 ( @TotalDaysInMonth - @DaysPaid ) AS LOPDays
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
