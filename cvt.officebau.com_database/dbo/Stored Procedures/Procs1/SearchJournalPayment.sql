﻿/****************************************************************************               
CREATED BY   :               
CREATED DATE  :               
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>            
 SearchJournalPayment null,29,1            
 </summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchJournalPayment] (@ID INT, @LedgerID INT, @DomainID INT)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION
		IF (ISNULL(@ID, 0) <> 0)
		BEGIN
			SELECT
				p.ID
			   ,j.ID AS RefId
			   ,j.JournalNo
			   ,j.Credit AS Amount
			   ,(SELECT
						ISNULL(SUM(Amount), 0)
					FROM tbl_PaymentMapping
					WHERE IsDeleted = 0
					AND DomainID = @DomainID
					AND p.RefId = RefId
					AND PaymentId <> p.PaymentId)
				AS PaidAmount
			   ,ISNULL(p.Amount, 0) AS updateAmount
			FROM tblJournal j
			JOIN tbl_PaymentMapping p
				ON p.RefId = j.ID
					AND p.IsDeleted = 0
			WHERE (ISNULL(@ID, 0) = 0
			OR ISNULL(@ID, 0) = p.PaymentId)
			AND (ISNULL(@LedgerID, 0) = 0
			OR ISNULL(@LedgerID, 0) = j.LedgerProductID)
			AND j.IsDeleted = 0
			AND j.Type = 'Ledger'
		END
		ELSE
		BEGIN
			SELECT
				0 AS ID
			   ,j.ID AS RefId
			   ,j.JournalNo
			   ,j.Credit AS Amount
			   ,(SELECT
						ISNULL(SUM(Amount), 0)
					FROM tbl_PaymentMapping
					WHERE IsDeleted = 0
					AND DomainID = @DomainID
					AND j.ID = RefId)
				AS PaidAmount
			   ,NULL AS updateAmount
			FROM tblJournal j
			WHERE (ISNULL(@LedgerID, 0) = 0
			OR ISNULL(@LedgerID, 0) = j.LedgerProductID)
			AND j.IsDeleted = 0
			AND j.Type='ledger'
			AND j.Credit > (SELECT
					ISNULL(SUM(Amount), 0)
				FROM tbl_PaymentMapping
				WHERE IsDeleted = 0
				AND DomainID = @DomainID
				AND j.ID = RefId
				AND j.Type = 'Ledger')

		END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMsg VARCHAR(100)
			   ,@ErrSeverity TINYINT
		SELECT
			@ErrorMsg = ERROR_MESSAGE()
		   ,@ErrSeverity = ERROR_SEVERITY()
		RAISERROR (@ErrorMsg, @ErrSeverity, 1)
	END CATCH
END
