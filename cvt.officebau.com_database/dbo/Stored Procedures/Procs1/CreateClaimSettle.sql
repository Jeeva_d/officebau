﻿/****************************************************************************     
CREATED BY   : Naneeshwar.S    
CREATED DATE  : 20-Jun-2017    
MODIFIED BY   : Ajith N    
MODIFIED DATE  : 29 Nov 2017    
 <summary>            
    [CreateClaimSettle] 0,2,1,'6',28,'12','',1,0,1,1,1    
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[CreateClaimSettle] (@ID              INT,    
                                           @ApproverID      INT,    
                                           @RequesterItemID INT,    
                                           @SettlerAmount   MONEY,    
                                           @PaymentDate     DATETIME,    
                                           @PaymentTypeID   INT,    
                                           @Status          VARCHAR(50),    
                                           @Remarks         VARCHAR(8000),    
                                           @CreatedBy       INT,    
                                           @IsDeleted       INT,    
                                           @ModifiedBy      INT,    
                                           @DomainID        INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(100)    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          BEGIN    
              IF( @ID = 0    
                  AND (SELECT Count(1)    
                       FROM   tbl_ClaimsSettle    
                       WHERE  ClaimItemID = @RequesterItemID) = 0 )    
                BEGIN    
                    INSERT INTO dbo.tbl_ClaimsSettle    
                                (ApproverID,    
                                 SettlerAmount,    
                                 PaymentTypeID,    
                                 PaymentDate,    
                                 ClaimItemID,    
                                 Remarks,    
                                 CreatedBy,  
                                 CreatedOn,  
                                 ModifiedBy,  
                                 ModifiedOn,   
                                 DomainID)    
                    VALUES      ( @ApproverID,    
                                  @SettlerAmount,    
                                  @PaymentTypeID,    
                                  @PaymentDate,    
                                  @RequesterItemID,    
                                  @Remarks,    
                                  @CreatedBy,  
                                  Getdate(),    
                                  @ModifiedBy,  
                                  Getdate(),    
                                  @DomainID )    
    
                    SET @ID = Ident_current('tbl_ClaimsSettle')    
                    SET @Output = 'Settled Successfully.'    
                END    
              ELSE    
                BEGIN    
                    UPDATE tbl_ClaimsSettle    
                    SET    SettlerAmount = @SettlerAmount,    
                           Remarks = @Remarks,    
                           PaymentTypeID = @PaymentTypeID,  
                           ModifiedOn = Getdate()   
                    WHERE  id = @ID    
    
                    SET @Output='Updated Successfully'    
    
                    GOTO FINISH    
                END    
          END    
    
          FINISH:    
    
          IF EXISTS(SELECT 1    
                    FROM   tbl_ClaimsSettle    
                    WHERE  ClaimItemID = @RequesterItemID    
                           AND IsDeleted = 0)    
            BEGIN    
                -- Update Claims Settle Status    
                UPDATE tbl_ClaimsSettle    
                SET    StatusID = (SELECT ID    
                                   FROM   tbl_Status    
                                   WHERE  code = 'Settled'    
                                          AND Type = 'Claims')    
                --SET    StatusID = ( CASE    
                --                      WHEN( (SELECT Sum(ApprovedAmount)    
                --                             FROM   tbl_ClaimsApprove    
                --                             WHERE  ClaimItemID = @RequesterItemID    
                --                                    AND IsDeleted = 0) > (SELECT Sum(SettlerAmount)    
                --       FROM   tbl_ClaimsSettle    
              --                                                          WHERE  ClaimItemID = @RequesterItemID    
                --                                                                 AND IsDeleted = 0) ) THEN    
                --                        ((SELECT ID    
                --                          FROM   tbl_Status    
                --                          WHERE  code = 'Partial'    
                --                                 AND Type = 'Claims'))    
                --                      WHEN( (SELECT Sum(ApprovedAmount)    
                --                             FROM   tbl_ClaimsApprove    
                --                             WHERE  ClaimItemID = @RequesterItemID    
                --                                    AND IsDeleted = 0) = (SELECT Sum(SettlerAmount)    
                --                                                          FROM   tbl_ClaimsSettle    
                --                                                          WHERE  ClaimItemID = @RequesterItemID    
                --                                                                 AND IsDeleted = 0) ) THEN    
                --                        ((SELECT ID    
                --                          FROM   tbl_Status    
                --                          WHERE  code = 'Settled'    
                --                                 AND Type = 'Claims'))    
                --                    END )    
                WHERE  id = @ID    
    
                -- Update Claims Request Status              
                UPDATE tbl_ClaimsRequest    
                SET    StatusID = (SELECT ID    
                                   FROM   tbl_Status    
                                   WHERE  code = 'Settled'    
                                          AND Type = 'Claims')    
                --SET    StatusID = CASE    
                --                    WHEN( (SELECT Sum(ApprovedAmount)    
                --                           FROM   tbl_ClaimsApprove    
                --                           WHERE  ClaimItemID = @RequesterItemID    
                --                                  AND IsDeleted = 0) > (SELECT Sum(SettlerAmount)    
                --                                                        FROM   tbl_ClaimsSettle    
                --                                                        WHERE  ClaimItemID = @RequesterItemID    
                --                                                               AND IsDeleted = 0) ) THEN((SELECT ID    
                --                                                                                          FROM   tbl_Status    
                --                                                                                          WHERE  code = 'Partial'    
                --                                                                                                 AND Type = 'Claims'))    
                --                    WHEN( (SELECT Sum(ApprovedAmount)    
                --                           FROM   tbl_ClaimsApprove    
                --                           WHERE  ClaimItemID = @RequesterItemID    
                --                                  AND IsDeleted = 0) = (SELECT Sum(SettlerAmount)    
                --                                                        FROM   tbl_ClaimsSettle    
                --                                                        WHERE  ClaimItemID = @RequesterItemID    
                --                                            AND IsDeleted = 0) ) THEN((SELECT ID    
                --                                                                                          FROM   tbl_Status    
                --                                                                                          WHERE  code = 'Settled'    
                --                                                                                                 AND Type = 'Claims'))    
                --        END    
                WHERE  id = @RequesterItemID    
    
                -- Update Claims Financial Approve Status    
                UPDATE tbl_ClaimsFinancialApprove    
                SET    SettlerStatusID = (SELECT ID    
                                   FROM   tbl_Status    
                                   WHERE  code = 'Settled'    
                                          AND Type = 'Claims')    
                --SET    SettlerStatusID = ( CASE    
                --                             WHEN( (SELECT Sum(ApprovedAmount)    
                --                                    FROM   tbl_ClaimsApprove    
                --                                    WHERE  ClaimItemID = @RequesterItemID    
                --                                           AND IsDeleted = 0) > (SELECT Sum(SettlerAmount)    
                --                                                                 FROM   tbl_ClaimsSettle    
                --                                                                 WHERE  ClaimItemID = @RequesterItemID    
                --                                                                        AND IsDeleted = 0) ) THEN    
                --                               ((SELECT ID    
                --                                 FROM   tbl_Status    
                --                                 WHERE  code = 'Partial'    
                --                                        AND Type = 'Claims'))    
                --                             WHEN( (SELECT Sum(ApprovedAmount)    
                --                                    FROM   tbl_ClaimsApprove    
                --                                    WHERE  ClaimItemID = @RequesterItemID    
                --                                           AND IsDeleted = 0) = (SELECT Sum(SettlerAmount)    
                --                                                                 FROM   tbl_ClaimsSettle    
                --                                                                 WHERE  ClaimItemID = @RequesterItemID    
                --                                                                        AND IsDeleted = 0) ) THEN    
                --                               ((SELECT ID    
                --                                 FROM   tbl_Status    
                --                                 WHERE  code = 'Settled'    
                --                                        AND Type = 'Claims'))    
                --                           END )    
                WHERE  ClaimItemID = @RequesterItemID    
            END    
    
          SELECT @Output AS UserMessage    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
