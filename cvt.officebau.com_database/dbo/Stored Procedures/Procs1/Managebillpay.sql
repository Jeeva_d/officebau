﻿/****************************************************************************                             
CREATED BY  : Jeeva                            
CREATED DATE :                             
MODIFIED BY  :                      
MODIFIED DATE :                         
<summary>                                    
sELECT * FROM TBLEXPENSE                          
</summary>                                                     
*****************************************************************************/                
CREATE PROCEDURE [dbo].[Managebillpay] (@ID            INT,                
                                       @Description   VARCHAR(1000),                
                                       @Date          DATETIME,                
                                       @VendorID      INT,                
                                       @TotalAmount   MONEY,                
                                       @SessionID     INT,                
                                       @DomainID      INT,                
                                       @PaymentModeID INT,                
                                       @BankID        INT,                
                                       @IsDeleted     BIT,                
                                       @Reference     VARCHAR(1000),                
                                       @BillPay       BILLPAY readonly)                
AS                
  BEGIN                
      SET NOCOUNT ON;                
                
      BEGIN TRY                
          BEGIN TRANSACTION                
                
          DECLARE @OUTPUT                 VARCHAR(100),                
                  @ExpensePaymentID       INT,                
                  @cash                   MONEY,                
                  @ExpensePayHistoryID    VARCHAR(MAX),                
                  @ExpensePayMapHistoryID VARCHAR(MAX),                
                  @PreviousCash           MONEY = (SELECT Sum(EM.Amount)                
                     FROM   tblexpensepayment EP                
                            LEFT JOIN tblExpensepaymentmapping EM                
                                   ON EP.id = EM.expensePaymentID                
                     WHERE  EP.isdeleted = 0                
                            AND EP.ID = @id                
                            AND EP.DomainID = @DomainID),                
                  @PreviousMode           INT = (SELECT TOP 1 PaymentModeID                
                     FROM   tblexpensepayment EP                
                            LEFT JOIN tblExpensepaymentmapping EM                
                                   ON EP.id = EM.expensePaymentID                
                     WHERE  EP.isdeleted = 0                
                            AND EP.ID = @id                
                            AND EP.DomainID = @DomainID),                
                  @ExpensePayColumn       VARCHAR(MAX) = ( CASE                
                        WHEN @IsDeleted = 1 THEN                
                          'IsDeleted'                
                        ELSE                
                          'PaymentModeID,BankID'                
                      END ),                
                  @ExpensePayMapColumn    VARCHAR(MAX) = ( CASE                
                        WHEN @IsDeleted = 1 THEN                
                          'IsDeleted'                
                        ELSE                
                          'Amount'                
                      END )                
          DECLARE @PayHistoryIDTable TABLE                
            (                
               ID        INT IDENTITY(1, 1),                
               HistoryID VARCHAR(Max)                
            )                
          DECLARE @PayMapHistoryIDTable TABLE                
            (                
      ID        INT IDENTITY(1, 1),                
               HistoryID VARCHAR(Max)                
            )                
           
          IF( (SELECT Count(1)                
               FROM   tblCashBucket                
               WHERE  DomainID = @DomainID) = 0 )                
            INSERT INTO tblCashBucket                
                        (AvailableCash,                
                         DomainID)                
            VALUES      (0,                
                         @DomainID)                
                
          IF( @IsDeleted = 1 )                
            BEGIN                
                IF EXISTS (SELECT 1                
                           FROM   tblbrs                
                           WHERE  SourceID = @ID                
                                  AND sourcetype = (SELECT ID                
                                                    FROM   tblMasterTypes                
                                           WHERE  NAME = 'PayBill')                
                                  AND IsReconsiled = 1                
                                  AND IsDeleted = 0                
                                  AND DomainID = @DomainID)                
                  BEGIN                
                      SET @Output = (SELECT [Message]                
                                     FROM   tblErrorMessage                
                                     WHERE  [Type] = 'Information'                
                                            AND Code = 'RCD_RECON'                
                                            AND IsDeleted = 0) --'The record has been Reconciled.'                        
                
                      GOTO finish                
                  END                
                ELSE                
                  BEGIN                
                      IF( @PaymentModeID = (SELECT ID                
                                            FROM   tbl_CodeMaster                
                                            WHERE  Code = 'Cash'                
                                                   AND IsDeleted = 0) )                
                        BEGIN                
                            UPDATE tblCashBucket                
                            SET    AvailableCash = AvailableCash                
                                                   + (SELECT Sum(Amount)                
                                                      FROM   tblExpensePaymentmapping                
                                                      WHERE  ExpensePaymentID = @ID                
                                                             AND IsDeleted = 0                
                                                             AND DomainID = @DomainID)                
                            WHERE  @DomainID = DomainID                
                
                            UPDATE tblVirtualCash                
                            SET    IsDeleted = 1                
                            WHERE  SourceID = @ID                
                                   AND SourceType = (SELECT ID                
                                                     FROM   tblMasterTypes                
                                                     WHERE  NAME = 'PayBill')                
                                   AND IsDeleted = 0                
                                   AND DomainID = @DomainID                
                        END                
                
                      INSERT INTO @PayMapHistoryIDTable                
                                  (HistoryID)                
                      SELECT HistoryID                
                      FROM   tblExpensePaymentMapping                
                      WHERE  ExpensePaymentID = @ID                
                AND DomainID = @DomainID                
                
                      INSERT INTO @PayHistoryIDTable                
                                  (HistoryID)                
                      SELECT HistoryID                
            FROM   tblExpensePayment                
                      WHERE  ID = @ID                
                             AND DomainID = @DomainID                
                
                      UPDATE tblExpensePaymentmapping                
                      SET    IsDeleted = 1                
                      WHERE  @ID = ExpensePaymentID                
                
                      UPDATE tblExpensePayment                
                      SET    IsDeleted = 1                
                      WHERE  ID = @ID                
                
                      UPDATE tblBookedBankBalance                
                      SET    IsDeleted = 1                
                      WHERE  BRSID = (SELECT ID                
                                      FROM   tblBRS                
                                      WHERE  SourceID = @ID                
                AND SourceType = (SELECT ID                
                                                               FROM   tblMasterTypes                
                                                               WHERE  NAME = 'PayBill')                
 AND IsDeleted = 0)                
                             AND DomainID = @DomainID                
                
                      UPDATE tblBRS                
                      SET    IsDeleted = 1                
                      WHERE  SourceID = @ID                
                             AND sourcetype = (SELECT ID                
                      FROM   tblMasterTypes                
                                               WHERE  NAME = 'PayBill')                
                
                      EXEC Managesystembankbalance                
                        @DomainID                
                  END                
                
                SET @Output = (SELECT [Message]                
                               FROM   tblErrorMessage                
                               WHERE  [Type] = 'Information'                
                                      AND Code = 'RCD_DEL'                
                                      AND IsDeleted = 0) --'Deleted Successfully'                        
                
                GOTO finish                
            END                
          ELSE                
            BEGIN                
                IF( @ID = 0 )                
                  BEGIN                
                      INSERT INTO tblExpensePayment                
                                  (PaymentModeID,                
                                   Date,                
                                   BankID,                
                                   Reference,                
                                   DomainID,                
                                   CreatedBy,                
                                   CreatedOn,                
                                   ModifiedBy,                
                                   ModifiedOn)                
                      OUTPUT      INSERTED.HistoryID                
                      INTO @PayHistoryIDTable(HistoryID)                
                      VALUES      (@PaymentModeID,                
                                   @Date,                
                                   @BankID,                
                                   @Reference,                
                                   @DomainID,                
                                   @SessionID,                
                                   Getdate(),                
                                   @SessionID,                
                          Getdate())                
                
                      SET @ExpensePaymentID = Ident_current('tblExpensePayment')                
                
                      INSERT INTO tblExpensePaymentMapping                
                                  (ExpensePaymentID,                
                                   ExpenseID,                
                 Amount,                
                                   DomainID,                
                                   CreatedBy,                
                                   ModifiedBy,                
                                   ModifiedOn)                
                      OUTPUT      INSERTED.HistoryID                
                      INTO @PayMapHistoryIDTable(HistoryID)                
      SELECT @ExpensePaymentID,                
                             ExpenseID,                
                             PayAmount,                
                             @DomainID,                
                             @SessionID,                
                             @SessionID,                
                             Getdate()                
                      FROM   @BillPay                
                      WHERE  PayAmount <> 0             
                
                      IF( Isnull(@BankID, 0) <> 0 )                
                        BEGIN                
                            INSERT INTO tblBRS                
                                        (BankID,                
                                         SourceID,                
                                         SourceDate,                
                                         SourceType,                
                    Amount,                
                                         DomainID,                
                                         BRSDescription,                
                                         CreatedBy,                
                                         CreatedOn,                
                                         ModifiedBy,                
                                         ModifiedOn)                
                 VALUES      (@BankID,                
                                         @ExpensePaymentID,                
                                         @Date,                
                                         (SELECT ID                
                                          FROM   tblMasterTypes                
                                          WHERE  NAME = 'PayBill'),                
                                         (SELECT Sum(Amount)                
                    FROM   tblExpensePaymentMapping                
                                          WHERE  IsDeleted = 0                
                                                 AND ExpensePaymentID = @ExpensePaymentID                
                                                 AND DomainID = @DomainID),                
                                         @DomainID,                
                                         @Reference,                
                                         @SessionID,                
                                         Getdate(),                
                                       @SessionID,                
                                         Getdate())                
                
                            INSERT INTO tblBookedBankBalance                
                                        (BRSID,                
                                         Amount,                
                                         DomainID,                
                                         CreatedBy,                
                                         ModifiedBy)                
                            VALUES      (@@IDENTITY,                
                                         @TotalAmount,     
                                         @DomainID,                
                                         @SessionID,                
                                         @SessionID)                
                
                            EXEC Managesystembankbalance                
                              @DomainID                
                        END                
                
                      IF( @PaymentModeID = (SELECT ID                
       FROM   tbl_CodeMaster                
                                            WHERE  Code = 'Cash'                
                                                   AND IsDeleted = 0) )                
                        BEGIN                
                            UPDATE tblCashBucket                
                            SET    AvailableCash = AvailableCash - @TotalAmount                
                            WHERE  @DomainID = DomainID                
                
                            INSERT INTO tblVirtualCash         
                                        (SourceID,                
                                         Amount,                
                                         Date,                
                                         SourceType,                
                                         DomainID,                
                                         CreatedBy,                
                                         CreatedOn)                
                            VALUES      (@ExpensePaymentID,                
                                         0 - @TotalAmount,                
                                     @Date,                
                                         (SELECT ID                
                                          FROM   tblMasterTypes                
                                          WHERE  NAME = 'PayBill'),                
                                         @DomainID,                
                                         @SessionID,                
                                         Getdate())                
                        END                
                
                      DECLARE @FileID VARCHAR(50) = (SELECT top 1 Cast(E.HistoryID AS VARCHAR(50))                
                         FROM   tblExpensePaymentMapping PM                
                                JOIN tblExpense E                
                                       ON E.ID = PM.ExpenseID                
            join tbl_FileUpload FU ON FU.Id = E.HistoryID                
                         WHERE  PM.ExpensePaymentID = @ExpensePaymentID)                
                
                      SET @Output = (SELECT [Message]                
                                     FROM   tblErrorMessage                
                       WHERE  [Type] = 'Information'                
                                            AND Code = 'RCD_INS'                
                                            AND IsDeleted = 0)                
                                    + '/' + ISNULL(@FileID,'') --'Inserted Successfully.'                        
                
                      GOTO finish                
                  END                
                ELSE                
                  BEGIN                
                      IF EXISTS (SELECT 1                
                                 FROM   tblbrs                
                                 WHERE  SourceID = @ID                
                                        AND sourcetype = (SELECT ID                
                                                          FROM   tblMasterTypes                
                                                          WHERE  NAME = 'PayBill')                
                                        AND IsReconsiled = 1                
                                        AND IsDeleted = 0                
                       AND DomainID = @DomainID)                
                        BEGIN                
                            SET @Output = (SELECT [Message]                
                                           FROM   tblErrorMessage                
                                           WHERE  [Type] = 'Information'                
                                                  AND Code = 'RCD_RECON'                
                                                  AND IsDeleted = 0) --'The record has been Reconciled.'                        
                            UPDATE tblExpensePayment                
                            SET    Reference = @Reference,                
                                   ModifiedBy = @SessionID,                
                                   ModifiedOn = Getdate()                
                            WHERE  ID = @ID                
                                   AND DomainID = @DomainID                
                
                            UPDATE tblBRS                
                            SET    BRSDescription = @Reference,                
                                   ModifiedBy = @SessionID                
                            WHERE  SourceID = @ID                
                                AND sourcetype = (SELECT ID                
                                                     FROM   tblMasterTypes                
                                                     WHERE  NAME = 'PayBill')                
                                   AND IsDeleted = 0                
                                   AND DomainID = @DomainID                
                
                            GOTO finish                
                        END                
                      ELSE                
                        BEGIN                
                            UPDATE tblExpensePayment                
                            SET    PaymentModeID = @PaymentModeID,                
                                Date = @Date,                
                                   BankID = @BankID,                
                                   Reference = @Reference,                
                                   DomainID = @DomainID,                
                                   ModifiedBy = @SessionID,                
                                   ModifiedOn = Getdate()                
                            WHERE  ID = @ID                
                                   AND DomainID = @DomainID                
                
                            UPDATE tblExpensePaymentMapping                
                            SET    tblExpensePaymentMapping.Amount = B.PayAmount,                
                            tblExpensePaymentMapping.ModifiedBy = @SessionID                
                            FROM   tblExpensePaymentMapping                
                                   INNER JOIN @BillPay AS B                
                                           ON tblExpensePaymentMapping.ID = B.ID                
                
                            INSERT INTO @PayMapHistoryIDTable                
                                        (HistoryID)                
                            SELECT HistoryID                
                            FROM   tblExpensePaymentMapping                
                                   INNER JOIN @BillPay AS B                
            ON tblExpensePaymentMapping.ID = B.ID                
                
                            INSERT INTO tblExpensePaymentMapping                
                                        (ExpensePaymentID,                
                                         ExpenseID,                
                                         Amount,                
                                         DomainID,                
                                         CreatedBy,                
                             ModifiedBy,                
                                         ModifiedOn)                
                            OUTPUT      INSERTED.HistoryID                
                            INTO @PayMapHistoryIDTable(HistoryID)                
                            SELECT @ID,                
                                   ExpenseID,                
                                   PayAmount,                
                                   @DomainID,                
                                   @SessionID,                
                                   @SessionID,                
                                   Getdate()                
     FROM   @BillPay                
                            WHERE  PayAmount <> 0                
                                   AND ID = 0                
                
                            IF( Isnull(@BankID, 0) <> 0 )                
                              BEGIN                
                                  IF EXISTS(SELECT 1                
                                            FROM   tblBRS                
                                            WHERE  SourceID = @ID                
                                                   AND sourcetype = (SELECT ID                
                                                              FROM   tblMasterTypes                
                                                                     WHERE  NAME = 'PayBill')               
                                                   AND DomainID = @DomainID                
                                                   AND IsDeleted = 0)                
                                    BEGIN                
                                        UPDATE tblBRS                
                                        SET    BankID = @BankID,                
                                               SourceDate = @Date,                
                                               Amount = (SELECT Sum(Amount)                
                                                         FROM   tblExpensePaymentMapping                
                                                         WHERE  IsDeleted = 0                
                                                                AND ExpensePaymentID = @ID                
                                                                AND DomainID = @DomainID),                
                                               DomainID = @DomainID,                
                                               BRSDescription = @Reference,                
                                               ModifiedBy = @SessionID                
                                        WHERE  SourceID = @ID                
                                               AND sourcetype = (SELECT ID                
                                                                 FROM   tblMasterTypes                
                                                                 WHERE  NAME = 'PayBill')                
                                               AND IsDeleted = 0                
               AND DomainID = @DomainID                
                
                                        UPDATE tblBookedBankBalance                
                                        SET    Amount = @TotalAmount                
                                        WHERE  BRSID = (SELECT ID                
                                                        FROM   tblBRS                
                                                        WHERE  SourceID = @ID                
                                                               AND SourceType = (SELECT ID                
                                                                                 FROM   tblMasterTypes                
       WHERE  NAME = 'PayBill')                
                                                               AND IsDeleted = 0)                
                                               AND DomainID = @DomainID                
                
                                        EXEC Managesystembankbalance                
                                          @DomainID                
                                    END                
                                  ELSE                
                                    BEGIN                
                                        INSERT INTO tblBRS                
                                                    (BankID,                
                                                     SourceID,                
                                        SourceDate,                
                                                     SourceType,                
                       Amount,                
                                                     BRSDescription)                
                                        VALUES      (@BankID,                
                                                     @ID,                
                                                     @Date,                
                                                     (SELECT ID                
                                                      FROM   tblMasterTypes                
                                                      WHERE  NAME = 'PayBill'),                
                                                     @TotalAmount,                
                                                     @Reference)               
                
                                        INSERT INTO tblBookedBankBalance                
                                                    (BRSID,                
                                                     Amount,                
                                                     DomainID,                
                                      CreatedBy,                
                                                     ModifiedBy)                
                                        VALUES      (@@IDENTITY,                
                                                     @TotalAmount,                
                                                     @DomainID,                
                                                     @SessionID,                
                                                     @SessionID)                
                
                                        EXEC Managesystembankbalance                
                                  @DomainID                
                
                                        UPDATE tblCashBucket                
                                        SET    AvailableCash = ( AvailableCash + @TotalAmount )                
                                        WHERE  @DomainID = DomainID                
                
                                        UPDATE tblVirtualCash                
                                        SET    IsDeleted = 1                
                                        WHERE  SourceID = @ID                
                                               AND SourceType = (SELECT ID                
                                                                 FROM   tblMasterTypes                
                                                                 WHERE  NAME = 'PayBill')                
                                               AND IsDeleted = 0            
                                               AND DomainID = @DomainID                
                                    END                
                              END                
                            ELSE                
                              BEGIN                
                                  IF( (SELECT 1                
                                       FROM   tblBRS                
                                       WHERE  SourceID = @ID                
                                              AND sourcetype = (SELECT ID                
                                                                FROM   tblMasterTypes                
                                                                WHERE  NAME = 'PayBill')                
                                              AND DomainID = @DomainID                
                                              AND IsDeleted = 0) = 1 )                
                        UPDATE tblBookedBankBalance                
                                    SET    IsDeleted = 1                
                                    WHERE  BRSID = (SELECT ID                
                                                    FROM   tblBRS                
                                                    WHERE  SourceID = @ID                
                                                           AND SourceType = (SELECT ID                
                                                                             FROM   tblMasterTypes                
                                                                             WHERE  NAME = 'PayBill')                
                                                           AND IsDeleted = 0)                
                                           AND DomainID = @DomainID                
                
                                  UPDATE tblBRS                
                                  SET    IsDeleted = 1                
                                 WHERE  SourceID = @ID                
                                         AND sourcetype = (SELECT ID                
                                                           FROM   tblMasterTypes                
                                            WHERE  NAME = 'PayBill')                
                                         AND DomainID = @DomainID                
                                         AND IsDeleted = 0                
                
                                  EXEC Managesystembankbalance                
                                    @DomainID                
                              END                
                
                            IF( @PaymentModeID = (SELECT ID                
                                                  FROM   tbl_CodeMaster                
                                                  WHERE  Code = 'Cash'                
                                                         AND IsDeleted = 0) )                
                              BEGIN                
                                  IF ( @PreviousMode = (SELECT ID                
                                                        FROM   tbl_CodeMaster                
                                                        WHERE  Code = 'Cash'            
                                                               AND IsDeleted = 0) )                
                                    BEGIN                
                                        UPDATE tblCashBucket                
                                        SET    AvailableCash = AvailableCash + @PreviousCash - @TotalAmount                
                                        WHERE  @DomainID = DomainID                
                
                                        UPDATE tblVirtualCash                
                                        SET    Amount = 0 - @TotalAmount                
                                        WHERE  SourceID = @ID                
                                               AND SourceType = (SELECT ID                
                                                                 FROM   tblMasterTypes                
                                                                 WHERE  NAME = 'PayBill')                
                                               AND IsDeleted = 0                                                       AND DomainID = @DomainID                
                                    END                
                                  ELSE                
                                    BEGIN                
                                        UPDATE tblCashBucket                
                                        SET    AvailableCash = AvailableCash - @TotalAmount                
                                        WHERE  @DomainID = DomainID                
                
                                        INSERT INTO tblVirtualCash                
                                                    (SourceID,                
                                                     Amount,                
                                                     Date,                
                                                     SourceType,               
                                                     DomainID,                
                                                     CreatedBy,                
                                  ModifiedBy)                
                                        VALUES      (@ID,                
                                                     0 - @TotalAmount,                
                                                     @Date,                
                                                     (SELECT ID                
                                                      FROM   tblMasterTypes                
                                                      WHERE  NAME = 'PayBill'),                
                                                     @DomainID,                
                                                     @SessionID,                
                                                     @SessionID)                
                                    END                
                              END                
                
                            SET @Output = (SELECT [Message]                
                                           FROM   tblErrorMessage                
                                           WHERE  [Type] = 'Information'                
             AND Code = 'RCD_UPD'                
                                                  AND IsDeleted = 0) --'Updated Successfully.'                        
                
                            GOTO finish                
                        END                
                  END                
            END                
                
          FINISH:                
                
          DECLARE @TempCount INT = 1,                
                  @Count     INT = (SELECT Count(1)                
                     FROM   @PayHistoryIDTable)                
                
          WHILE @Count >= @TempCount                
            BEGIN                
                SET @ExpensePayHistoryID= (SELECT HistoryID                
                                           FROM   @PayHistoryIDTable                
                                           WHERE  ID = @TempCount)                
                
                EXEC Managebusinesseventslog                
                  @ExpensePayHistoryID,                
             @ExpensePayColumn,                
                  'tblExpensePayment'                
                
                SET @TempCount = @TempCount + 1                
            END                
                
          SET @TempCount = 1                
          SET @Count = (SELECT Count(1)                
                        FROM   @PayMapHistoryIDTable)                
                
          WHILE @Count >= @TempCount                
            BEGIN                
                SET @ExpensePayMapHistoryID= (SELECT HistoryID                 FROM   @PayMapHistoryIDTable                
                                              WHERE  ID = @TempCount)                
                
                EXEC Managebusinesseventslog                
                  @ExpensePayMapHistoryID,                
                  @ExpensePayMapColumn,                
                  'tblExpensePaymentMapping'                
                
                SET @TempCount = @TempCount + 1                
            END                
                
          UPDATE tblExpense                
          SET    tblExpense.PaidAmount = (SELECT Sum(Amount)                
                                          FROM   tblExpensePaymentMapping               
                                          WHERE  IsDeleted = 0                
                                                 AND ExpenseID = S.ExpenseID                
                                                 AND DomainID = @DomainID),                
                 tblExpense.StatusID =CASE          
                              WHEN ( ((SELECT Sum(Amount)          
                                      FROM tblExpensePaymentMapping          
                                      WHERE  IsDeleted = 0          
                                             AND ExpenseID = s.ExpenseID          
                                             AND DomainID = @DomainID)+(Select ISNULL(SUM(AMOUNT),0) from tblExpenseHoldings   
            Where ExpenseId=s.ExpenseID And Isdeleted=0 and DomainID=@DomainID)) >= (SELECT ( ( Isnull(Sum(QTY * Amount), 0) ) + Isnull(Sum(CGST), 0) + Isnull(Sum(SGST), 0) )          
                                                                          FROM   tblExpenseDetails          
                                                                          WHERE  ExpenseID = s.ExpenseID          
                                                                                 AND IsDeleted = 0          
                                                                                 AND DomainID = @DomainID)          
                                                                         ) THEN          
                                (SELECT ID          
                         FROM   tbl_CodeMaster          
                                 WHERE  [Type] = 'Close'          
                                        AND IsDeleted = 0)          
                              WHEN ((( Isnull((SELECT Sum(Amount)          
                                              FROM   tblExpensePaymentMapping          
                                              WHERE  IsDeleted = 0          
                                                     AND ExpenseID = s.ExpenseID          
                                                     AND DomainID = @DomainID), 0)+(Select ISNULL(SUM(AMOUNT),0) from tblExpenseHoldings   
            Where ExpenseId=s.ExpenseID And Isdeleted=0 and DomainID=@DomainID)) = 0 )) THEN          
                                (SELECT ID          
                                 FROM   tbl_CodeMaster          
                                 WHERE  [Type] = 'Open'          
                                        AND IsDeleted = 0)          
                              WHEN (( Isnull((SELECT Sum(Amount)          
                                             FROM   tblExpensePaymentMapping          
                                             WHERE  IsDeleted = 0          
                                                    AND ExpenseID = s.ExpenseID          
                                                    AND DomainID = @DomainID), 0)+(Select ISNULL(SUM(AMOUNT),0) from tblExpenseHoldings   
            Where ExpenseId=s.ExpenseID And Isdeleted=0 and DomainID=@DomainID)) < (SELECT ( ( Isnull(Sum(QTY * Amount), 0) ) + Isnull(Sum(CGST), 0) + Isnull(Sum(SGST), 0) )          
                                                                          FROM   tblExpenseDetails          
                                                                          WHERE  ExpenseID = s.ExpenseID          
                                                                                 AND IsDeleted = 0          
                                                                                 AND DomainID = @DomainID)          
                                                                         )   THEN          
                                (SELECT ID          
                                 FROM   tbl_CodeMaster          
                                 WHERE  [Type] = 'Partial'          
                           AND IsDeleted = 0)       
          END            
          FROM   tblExpense                
                 JOIN @BillPay AS S                
                   ON tblExpense.ID = S.ExpenseID                
                
          SELECT @Output                
                
          COMMIT TRANSACTION                
      END TRY                
      BEGIN CATCH                
          ROLLBACK TRANSACTION                
          DECLARE @ErrorMsg    VARCHAR(100),                
                  @ErrSeverity TINYINT                
          SELECT @ErrorMsg = Error_message(),                
                 @ErrSeverity = Error_severity()                
          RAISERROR(@ErrorMsg,@ErrSeverity,1)                
      END CATCH                
  END
