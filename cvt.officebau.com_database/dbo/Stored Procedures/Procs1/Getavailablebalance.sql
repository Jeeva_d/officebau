﻿/****************************************************************************       
CREATED BY   : Naneeshwar      
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>    
 [Getavailablebalance] 1,1    
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Getavailablebalance] (@ID       INT,  
                                             @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT ( (SELECT Isnull(Sum(amount), 0)  
                    FROM   tblbrs  
                    WHERE  isdeleted = 0  
                           AND SourceType IN((SELECT ID  
                                              FROM   tblMasterTypes mt  
                                              WHERE  BankID = tbrs.BankID  
                                                     AND NAME IN ( 'InvoiceReceivable', 'Receive Payment', 'ToBank' )))) - (SELECT Isnull(Sum(amount), 0)  
                                                                                                                            FROM   tblbrs  
                                                                                                                            WHERE  isdeleted = 0  
                                                                                                                                   AND SourceType IN((SELECT ID  
                                                                                                                                                      FROM   tblMasterTypes mt  
                                                                                                                                                      WHERE  BankID = tbrs.BankID  
                                                                                                                                                             AND NAME IN ( 'PayBill', 'FromBank', 'Bill', 'Expense', 'Make Payment' )))) ) + bk.OpeningBalance 
AS OpeningBalance  
          FROM   tblBank bk  
                 LEFT JOIN TblBrs tbrs  
                        ON tbrs.bankID = bk.ID  
          WHERE  bk.IsDeleted = 0  
                 AND bk.ID = @ID  
                 AND bk.DomainID = @DomainID  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
