﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_ManagePayrollActivity] (@MonthId INT,  
@Year INT,  
@DomainID INT,  
@Location VARCHAR(100),  
@SessionID INT,  
@StatusId int)  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 DECLARE @Output VARCHAR(8000)  
 SET @Year = ( SELECT  
  Name  
 FROM tbl_FinancialYear  
 WHERE Id = @Year  
 AND IsDeleted = 0  
 AND DomainID = @DomainID)  
  
 DECLARE @DataSource TABLE (  
  Id INT IDENTITY (1, 1)  
    ,[Value] NVARCHAR(128)  
 )  
  
 INSERT INTO @DataSource ([Value])  
  SELECT  
   Item  
  FROM dbo.SplitString(@Location, ',')  
  WHERE ISNULL(Item, '') <> ''  
 BEGIN TRY  
  BEGIN TRANSACTION  
  IF @StatusId =1  
  BEGIN   
  UPDATE tbl_Pay_EmployeePayroll  
  SET ApproverId = @SessionID  
     ,IsApproved = 1  
     ,ModifiedBy = @SessionID  
     ,ModifiedOn = GETDATE()  
  FROM tbl_Pay_EmployeePayroll AS p  
  LEFT JOIN tbl_EmployeeMaster emp  
   ON p.EmployeeId = emp.Id  
  LEFT JOIN tbl_BusinessUnit BU  
   ON BU.Id = emp.BaseLocationID  
  WHERE p.MonthId = @MonthId  
  AND p.YearId = @Year  
  AND ((@Location = CAST(0 AS VARCHAR))  
  OR emp.BaseLocationID IN (SELECT  
    [Value]  
   FROM @DataSource)  
  )  
  AND (emp.BaseLocationID <> 0  
  OR emp.BaseLocationID <> '');  
  SET @Output = 'Payroll Approved'  
 END  
 IF @StatusId =2  
 BEGIN
  UPDATE tbl_Pay_EmployeePayroll  
     SET IsProcessed = 1  
     ,ModifiedBy = @SessionID  
     ,ModifiedOn = GETDATE()  
  FROM tbl_Pay_EmployeePayroll AS p  
  LEFT JOIN tbl_EmployeeMaster emp  
   ON p.EmployeeId = emp.Id  
  LEFT JOIN tbl_BusinessUnit BU  
   ON BU.Id = emp.BaseLocationID  
  WHERE p.MonthId = @MonthId  
  AND p.YearId = @Year AND p.IsApproved=1  
  AND ((@Location = CAST(0 AS VARCHAR))  
  OR emp.BaseLocationID IN (SELECT  
    [Value]  
   FROM @DataSource)  
  )  
  AND (emp.BaseLocationID <> 0  
  OR emp.BaseLocationID <> '');  
  SET @Output = 'Payroll Processed'  
  END
 FINISH:  
   SELECT @Output  
  COMMIT TRANSACTION  
 END TRY  
 BEGIN CATCH  
  ROLLBACK TRANSACTION  
  DECLARE @ErrorMsg VARCHAR(100)  
      ,@ErrSeverity TINYINT  
  SELECT  
   @ErrorMsg = ERROR_MESSAGE()  
     ,@ErrSeverity = ERROR_SEVERITY()  
  RAISERROR (@ErrorMsg, @ErrSeverity, 1)  
 END CATCH  
END
