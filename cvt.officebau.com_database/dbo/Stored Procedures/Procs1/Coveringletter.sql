﻿/**************************************************************************** 
CREATED BY    		:	Naneeshwar.M
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
	[Coveringletter] 8,3,1,'17,20,19'
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Coveringletter] (@MonthId  INT,
                                        @Year     INT,
                                        @DomainID INT,
                                        @Location VARCHAR(100))
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @DataSource TABLE
        (
           [Value] NVARCHAR(128)
        )

      INSERT INTO @DataSource
                  ([Value])
      SELECT Item
      FROM   dbo.Splitstring (@Location, ',')
      WHERE  Isnull(Item, '') <> ''

      BEGIN TRY
          BEGIN
              SET @Year = (SELECT NAME
                           FROM   tbl_FinancialYear
                           WHERE  id = @Year)

              SELECT Code                                   AS EmployeeCode,
                     FirstName + ' ' + Isnull(LastName, '') AS EmployeeName,
                     BankName                               AS BankName,
                     AccountNo                              AS AccountNo,
                     Round(NetSalary, 0)                    AS [NetPayable],
                     IFSCCode                               AS IFSCCode
              FROM   tbl_EmployeePayroll ep
                     LEFT JOIN tbl_EmployeeMaster emp
                            ON emp.ID = ep.EmployeeID
                     LEFT JOIN tbl_EmployeeStatutoryDetails eb
                            ON emp.id = eb.EmployeeID
              WHERE  ep.IsProcessed = 1
                     AND ep.IsDeleted = 0
                     AND ep.MonthId = @monthID
                     AND YearId = @Year
                     AND emp.DomainID = @domainID
                     AND ( ( @Location = Cast(0 AS VARCHAR) )
                            OR ( emp.BaseLocationID IN (SELECT [Value]
                                                        FROM   @DataSource
                                                        WHERE  emp.IsDeleted = 0) ) )
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
