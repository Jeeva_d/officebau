﻿/****************************************************************************     
CREATED BY   :   
CREATED DATE  : 
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [Rpt_GetLoanDetails] 191
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_GetLoanDetails](@ID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          --		  SELECT Date,
          --       Amount,
          --       Description,
          --       (SELECT NAME
          --        FROM   tblMasterTypes
          --        WHERE  ID = TransactionType) Type
          --FROM   tblReceipts
          --WHERE  LedgerID = @ID
          --       AND IsDeleted = 0
          --ORDER  BY date DESC 
          CREATE TABLE #result
            (
               [Date] DATE,
               Amount MONEY,
               Description VARCHAR(max),
               [Type]      VARCHAR(100)
            )

          INSERT INTO #result
          SELECT paymentDate,
                 totalAmount,
                 Description,
                 'Receive Payment' AS [Type]
          FROM   tbl_Receipts
          WHERE  LedgerID = @ID
                 AND IsDeleted = 0
          UNION ALL
          SELECT paymentDate,
                 totalAmount,
                 Description,
                 'Make Payment' AS [Type]
          FROM   tbl_payment
          WHERE  LedgerID = @ID
                 AND IsDeleted = 0

          SELECT *
          FROM   #result
          ORDER  BY [Date] DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
