﻿
/****************************************************************************   
CREATED BY		:   Naneeshwar.M
CREATED DATE	:   
MODIFIED BY		:   
MODIFIED DATE	:   
<summary>
	[Getresetpasswordadmin] 1,224,224,'5mfmc1FOPK4ukjiQM1hAeLR/g7pPqP07mmKAJXhL2ao='
</summary>                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[Getresetpasswordadmin] (@DomainID   INT,
                                               @EmployeeID INT,
                                               @SessionID  INT,
                                               @Password   VARCHAR(50))
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(500)

      BEGIN TRY
          BEGIN TRANSACTION

          IF(SELECT Count(1)
             FROM   tbl_Login
             WHERE  EmployeeID = @EmployeeID
                    AND DomainID = @DomainID) = 0
            BEGIN
                INSERT dbo.tbl_Login
                       (EmployeeID,
                        Password,
                        RecentPasswordChangeDate,
                        IsChanged,
                        CreatedBy,
                        CreatedOn)
                VALUES (@EmployeeID,
                        @Password,
                        Getdate(),
                        0,
                        @SessionID,
                        Getdate())
            END
          ELSE
            BEGIN
                UPDATE tbl_Login
                SET    IsChanged = 0,
                       Password = @Password,
                       CreatedBy = @SessionID
                WHERE  EmployeeID = @EmployeeID
                       AND DomainID = @DomainID
            END

          SET @Output = 'Valid/'
                        + (SELECT Isnull(EmailID, '')
                           FROM   tbl_EmployeeMaster
                           WHERE  id = @EmployeeID)
                        + '/'
                        + (SELECT LoginCode + ' - ' + FirstName
                           FROM   tbl_EmployeeMaster
                           WHERE  id = @EmployeeID)
                        + '/'

          OUTPUTRESULT:

          SELECT @Output AS UserMessage

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END

