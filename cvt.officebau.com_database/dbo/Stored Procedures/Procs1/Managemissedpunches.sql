﻿/****************************************************************************   
CREATED BY		:   Dhanalakshmi. S
CREATED DATE	:   10-OCT-2017
MODIFIED BY		:   Ajith N
MODIFIED DATE	:   19 Jan 2018
<summary>
[Managemissedpunches] 0,106,1,'2017-06-06','00:00',null,0,1,1,0
</summary>                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[Managemissedpunches] (@ID           INT,
                                             @EmployeeID   INT,
                                             @PunchTypeID  INT,
                                             @PunchDate    DATE,
                                             @PunchTime    DATETIME,
                                             @Remarks      VARCHAR(8000),
                                             @SessionID    INT,
                                             @DomainID     INT,
                                             @IsDeleted    BIT,
                                             @HRApproverID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output        VARCHAR(100),
              @PunchStatusID INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = 'Pending'
                        AND [Type] = 'Leave'
                        AND IsDeleted = 0),
              @RegionIDs     VARCHAR(20) = (SELECT Cast(RegionID AS VARCHAR(10))
                 FROM   tbl_EmployeeMaster EM
                 WHERE  EM.ID = @SessionID
                 FOR XML path('')),
              @BiometricCode VARCHAR(50) = (SELECT BiometricCode
                 FROM   tbl_EmployeeMaster
                 WHERE  ID = @EmployeeID)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF( @IsDeleted != 0 )
            BEGIN
                IF NOT EXISTS (SELECT 1
                               FROM   tbl_EmployeeMissedPunches
                               WHERE  ID = @ID
                                      AND StatusID = (SELECT ID
                                                      FROM   tbl_Status
                                                      WHERE  Code = 'Approved'
                                                             AND [Type] = 'Leave'
                                                             AND IsDeleted = 0))
                  BEGIN
                      UPDATE tbl_EmployeeMissedPunches
                      SET    IsDeleted = @IsDeleted,
                             ModifiedBy = @SessionID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID

                      SET @Output = 'Request Deleted Successfully.'
                  END
                ELSE
                  SET @Output = 'Cannot Delete Request.'
            END
          ELSE
            BEGIN
                IF EXISTS(SELECT 1
                          FROM   tbl_Attendance A
                          WHERE  A.EmployeeID = @EmployeeID
                                 AND A.LogDate = @PunchDate
                                 AND A.IsDeleted = 0
                                 AND A.DomainID = @DomainID)
                  BEGIN
                      IF NOT EXISTS(SELECT 1
                                    FROM   tbl_Holidays
                                    WHERE  IsDeleted = 0
                                           AND DomainID = @DomainID
                                           AND RegionID LIKE '%,' + @RegionIDs + ',%'
                                           AND [Date] = @PunchDate)
                        BEGIN
                            IF EXISTS(SELECT 1
                                      FROM   tbl_Attendance
                                      WHERE  EmployeeID = @EmployeeID
                                             AND LogDate = @PunchDate
                                             AND IsDeleted = 0
                                             AND DomainID = @DomainID
                                             AND Duration IS NOT NULL)
                              BEGIN
                                  IF NOT EXISTS (SELECT 1
                                                 FROM   tbl_pay_EmployeePayroll
                                                 WHERE  IsDeleted = 0
                                                        AND DomainId = @DomainID
                                                        AND EmployeeId = @EmployeeID
                                                        AND MonthId = Datepart(MM, @PunchDate)
                                                        AND YearId = Datepart(YYYY, @PunchDate))
                                    BEGIN
                                        IF NOT EXISTS(SELECT 1
                                                      FROM   tbl_EmployeeMissedPunches
                                                      WHERE  IsDeleted = 0
                                                             AND DomainID = @DomainID
                                                             AND PunchDate = @PunchDate
                                                             AND PunchTypeID = @PunchTypeID
                                                             AND EmployeeID = @EmployeeID
                                                             AND PunchTime = Cast(@PunchTime AS TIME)
                                                             AND ID <> @ID
                                                             AND StatusID <> (SELECT ID
                                                                              FROM   tbl_Status
                                                                              WHERE  IsDeleted = 0
                                                                                     AND [type] = 'Leave'
                                                                                     AND code = 'Rejected'))
                                          BEGIN
                                              IF( @ID = 0 )
                                                BEGIN
                                                    INSERT INTO tbl_EmployeeMissedPunches
                                                                (EmployeeID,
                                                                 PunchTypeID,
                                                                 PunchDate,
                                                                 PunchTime,
                                                                 HRApproverID,
                                                                 Remarks,
                                                                 StatusID,
                                                                 DomainID,
                                                                 CreatedBy,
                                                                 ModifiedBy)
                                                    VALUES      (@EmployeeID,
                                                                 @PunchTypeID,
                                                                 @PunchDate,
                                                                 @PunchTime,
                                                                 @HRApproverID,
                                                                 @Remarks,
                                                                 @PunchStatusID,
                                                                 @DomainID,
                                                                 @SessionID,
                                                                 @SessionID)

                                                    SET @Output = 'Request Created Successfully.'
                                                END
                                              ELSE
                                                BEGIN
                                                    UPDATE tbl_EmployeeMissedPunches
                                                    SET    PunchTypeID = @PunchTypeID,
                                                           PunchDate = @PunchDate,
                                                           PunchTime = @PunchTime,
                                                           Remarks = @Remarks,
                                                           StatusID = @PunchStatusID,
                                                           DomainID = @DomainID,
                                                           ModifiedBy = @SessionID
                                                    WHERE  ID = @ID

                                                    SET @Output = 'Updated Successfully.'
                                                END
                                          END
                                        ELSE
                                          SET @Output = 'Record already exists.'
                                    END
                                  ELSE
                                    SET @Output = 'Payroll has already processed for this selected month and year.'
                              END
                            ELSE
                              SET @Output = 'Request cannot be raised for Absent days.'
                        END
                      ELSE
                        SET @Output = 'Request cannot be raised for Holidays and Week off.'
                  END
                ELSE
                  SET @Output = 'Record is not synchronized.'
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
