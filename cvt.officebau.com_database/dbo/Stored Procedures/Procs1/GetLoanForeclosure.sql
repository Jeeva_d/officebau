﻿/****************************************************************************   
CREATED BY		:  JENNIFER.S
CREATED DATE	:  01-AUG-2017
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary> 
          [GetLoanForeclosure] 0,1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetLoanForeclosure] (@ID       INT,
                                            @LoanID   INT,
                                            @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT LNR.ID                             LoanID,
                 LNR.LoanTypeID                     TypeID,
                 REQ.Code + ' - ' + REQ.FullName    Requester,
                 CASE
                   WHEN Isnull(LNF.ID, 0) <> 0 THEN
                     Isnull(LNF.Amount, 0)
                   ELSE
                     (SELECT Sum(Amount) Amount
                      FROM   tbl_LoanAmortization
                      WHERE  LoanRequestID = LNR.ID
                             AND StatusID = (SELECT ID
                                             FROM   tbl_Status
                                             WHERE  [Type] = 'Amortization'
                                                    AND Code = 'Open'))
                 END                                Amount,
                 LNF.ID                             ID,
                 LNF.InterestRate                   RateofInterest,
                 LNF.TotalAmount                    TotalAmount,
                 LNF.PaymentModeID                  PaymentTypeID,
                 LNF.PaymentDate,
                 LNF.ReferenceNo                    ReferenceNo,
                 --LNF.ModifiedBy                   ModifiedBy,
                 LNF.ModifiedOn                     ModifiedOn,
                 BU.NAME                            BaseLocation,
                 EMF.FullName                       ModifiedBy,
                 Isnull(L.ForecloseInterestRate, 0) ForeCloseInterestRate,
                 (SELECT Count(1)
                  FROM   tbl_LoanAmortization AM
                  WHERE  AM.LoanRequestID = LNR.ID
                         --and AM.StatusID = (SELECT ID
                         --                                          FROM   tbl_Status
                         --                                          WHERE  [Type] = 'Amortization'
                         --                                                 AND Code = 'Open')
                         AND ( DueBalance <> 0
                                OR PaidAmount = 0 )
                         AND AM.IsDeleted = 0)      AS ForeclosureMonth,
                 LNF.[Description]                  AS [Description],
                 LNR.EmployeeID                     AS EmployeeID,
                 LNS.DateOfRealization              AS [Date]
          FROM   tbl_LoanSettle LNS
                 JOIN tbl_LoanRequest LNR
                   ON LNS.LoanRequestID = LNR.ID
                 JOIN tbl_EmployeeMaster REQ
                   ON LNR.EmployeeID = REQ.ID
                 LEFT JOIN tbl_LoanForeclosure LNF
                        ON LNS.LoanRequestID = LNF.LoanRequestID
                           AND ( ( Isnull(@ID, 0) <> 0
                                   AND LNF.ID = @ID )
                                  OR Isnull(@ID, 0) = 0 )
                 LEFT JOIN tbl_BusinessUnit BU
                        ON REQ.BaseLocationID = BU.ID
                 LEFT JOIN tbl_EmployeeDesignationMapping Em
                        ON REQ.DesignationID = Em.ID
                 LEFT JOIN tbl_LoanConfiguration L
                        ON L.DesignationMappingID = Em.ID
                           AND L.IsDeleted = 0
                 LEFT JOIN tbl_EmployeeMaster EMF
                        ON EMF.ID = LNF.ModifiedBy
          WHERE  LNS.LoanRequestID = @LoanID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
