﻿/****************************************************************************                     
CREATED BY   :                  
CREATED DATE  :                     
MODIFIED BY   :                   
MODIFIED DATE  :                 
<summary>                  
[Rpt_daybookbyBank] 1,NULL,NULL,1            
</summary>                                             
*****************************************************************************/    
CREATE PROCEDURE [dbo].[Rpt_daybookbyBank] (@BankID    INT,    
                                           @StartDate DATETIME = NULL,    
                                           @EndDate   DATETIME = NULL,    
                                           @DomainID  INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          DECLARE @FinancialYear INT;    
          DECLARE @StartFinancialMonth INT;    
          DECLARE @EndFinancialMonth INT;    
    
          IF ( @StartDate IS NULL    
                OR @StartDate = '' )    
              OR ( @EndDate IS NULL    
                    OR @EndDate = '' )    
            BEGIN    
                SET @StartFinancialMonth = (SELECT Value    
                                            FROM   tblApplicationConfiguration    
                                            WHERE  Code = 'STARTMTH'    
                                                   AND DomainID = @DomainID)    
                SET @EndFinancialMonth = ( ( (SELECT Value    
                                              FROM   tblApplicationConfiguration    
                                              WHERE  Code = 'STARTMTH'    
                                                     AND DomainID = @DomainID)    
                                             + 11 ) % 12 )    
    
                IF( Month(Getdate()) <= ( ( (SELECT Value    
                                             FROM   tblApplicationConfiguration    
                                             WHERE  Code = 'STARTMTH'    
                                                    AND DomainID = @DomainID)    
                                            + 11 ) % 12 ) )    
                  SET @FinancialYear = Year(Getdate()) - 1    
                ELSE    
                  SET @FinancialYear = Year(Getdate())    
    
                SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date              
                SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date             
            END;    
    
          WITH CTE    
               AS (SELECT DISTINCT ip.ID                                     AS ID,    
                                   ip.PaymentDate                            [Date],    
                                   cd.NAME                                   Party,    
                                   (SELECT Sum(AMOUNT)    
                                    FROM   tblInvoicePaymentMapping ipm    
                                    WHERE  ipm.isdeleted = 0    
                                           AND ipm.DomainID = @DomainID    
                                           AND ipm.InvoicePaymentID = ip.ID) Receipts,    
                                   NULL                                      Payments,    
                                   ip.CreatedOn                              modifiedOn,    
                                   ip.BankID,    
                                   (SELECT NAME    
                                    FROM   tblMasterTypes    
                                    WHERE  NAME = 'InvoiceReceivable'    
                                           AND Code = 'INVREC')              [Type]    
                   FROM   tblInvoice i    
                          LEFT JOIN tblInvoicePaymentMapping im    
                                 ON im.InvoiceID = i.ID    
                                    AND im.IsDeleted = 0    
                          LEFT JOIN tblInvoicePayment ip    
                                 ON ip.ID = im.InvoicePaymentID    
                          AND ip.IsDeleted = 0    
                          LEFT JOIN tblCustomer cd    
                                 ON cd.ID = i.CustomerID    
                          LEFT JOIN tblBRS brs    
                                 ON brs.SourceID = im.ID    
                                    AND brs.SourceType = (SELECT id    
                                                          FROM   tblMasterTypes    
                                                          WHERE  NAME = 'InvoiceReceivable')    
                   WHERE  ip.PaymentModeID <> (SELECT ID    
                                               FROM   tbl_CodeMaster    
                                               WHERE  Code = 'Cash'    
                                                      AND [Type] = 'PaymentType'    
                                                      AND IsDeleted = 0)    
                          AND i.IsDeleted = 0    
                          AND im.IsDeleted = 0    
                          AND i.DomainID = @DomainID    
                          AND ( ip.BankID = @BankID    
                                 OR '' = Isnull(@BankID, '') )    
                          AND ( @StartDate = ''    
                                 OR i.Date >= @StartDate )    
                          AND ( @EndDate = ''    
                                 OR i.Date <= @EndDate )    
                   UNION ALL    
                   SELECT DISTINCT e.ID                      AS ID,    
                                   e.[Date],    
                                   vd.NAME                   Party,    
                                   NULL                      Receipts,    
                                   em.Amount                 Payments,    
                                   e.CreatedOn               modifiedOn,    
                                   ep.BankID,    
                                   (SELECT NAME    
                                    FROM   tblMasterTypes    
                                    WHERE  NAME = ( 'Expense' )    
                                           AND Code = 'EXP') [Type]    
                   FROM   tblExpense e    
                          LEFT JOIN tblExpensePaymentMapping em    
                                 ON em.ExpenseID = e.ID    
                                    AND em.IsDeleted = 0    
                          LEFT JOIN tblExpensePayment ep    
                                 ON ep.ID = em.ExpensePaymentID    
                                    AND ep.IsDeleted = 0    
                          LEFT JOIN tblVendor vd    
                                 ON vd.ID = e.VendorID    
                          LEFT JOIN tblBRS brs    
                                 ON brs.SourceID = em.ID    
                   WHERE  ep.PaymentModeID <> (SELECT ID    
                                               FROM   tbl_CodeMaster    
                                               WHERE  Code = 'Cash'    
                                                      AND [Type] = 'PaymentType'    
                                                      AND IsDeleted = 0)    
                          AND e.IsDeleted = 0    
                          AND e.Type = 'Expense'    
                          AND e.DomainID = @DomainID    
                          AND ( ep.BankID = @BankID    
                                 OR '' = Isnull(@BankID, '') )    
                          AND ( @StartDate = ''    
                                 OR e.Date >= @StartDate )    
                          AND ( @EndDate = ''    
                                 OR e.Date <= @EndDate )    
                   UNION ALL    
                   SELECT DISTINCT ep.ID                                     AS ID,    
                                   ep.[Date],    
                                   vd.NAME        Party,    
                                   NULL                                      Receipts,    
                                   (SELECT Sum(AMOUNT)    
                                    FROM   tblExpensePaymentMapping epm    
          WHERE  epm.isdeleted = 0    
                                           AND epm.DomainID = @DomainID    
                                           AND epm.ExpensePaymentID = ep.ID) Payments,    
                                   ep.CreatedOn                              modifiedOn,    
                                   ep.BankID,    
                                   (SELECT NAME    
                                    FROM   tblMasterTypes    
                                    WHERE  NAME = 'PayBill'    
                                           AND CODE = 'BILL')                [Type]    
                   FROM   tblExpense e    
                          LEFT JOIN tblExpensePaymentMapping em    
                                 ON em.ExpenseID = e.ID    
                                    AND em.IsDeleted = 0    
                          LEFT JOIN tblExpensePayment ep    
                                 ON ep.ID = em.ExpensePaymentID    
                                    AND ep.IsDeleted = 0    
                          LEFT JOIN tblVendor vd    
                                 ON vd.ID = e.VendorID    
                          LEFT JOIN tblBRS brs    
                                 ON brs.SourceID = em.ID    
                   WHERE  ep.PaymentModeID <> (SELECT ID    
                                               FROM   tbl_CodeMaster    
                                               WHERE  Code = 'Cash'    
                                                      AND [Type] = 'PaymentType'    
                                                      AND IsDeleted = 0)    
                          AND e.Type <> 'Expense'    
                          AND e.IsDeleted = 0    
                          AND e.DomainID = @DomainID    
                          AND ( ep.BankID = @BankID    
                                 OR '' = Isnull(@BankID, '') )    
                          AND ( @StartDate = ''    
                                 OR e.Date >= @StartDate )    
                          AND ( @EndDate = ''    
                                 OR e.Date <= @EndDate )    
                   UNION ALL    
                   --SELECT DISTINCT r.ID                            AS ID,    
                   --                r.[Date],    
                   --                PartyName                       Party,    
                   --                ( CASE    
                   --                    WHEN ( TransactionType = (SELECT ID    
                   --                                              FROM   tblMasterTypes    
                   --                                              WHERE  NAME = 'Receive Payment') ) THEN    
                   --                      ( r.Amount )    
                   --                    ELSE    
                   --                      NULL    
                   --                  END )                         Receipts,    
                   --                NULL                            Payments,    
                   --                r.CreatedOn                     modifiedOn,    
                   --                r.BankID,    
                   --                (SELECT NAME    
                   --                 FROM   tblMasterTypes    
                   --                 WHERE  NAME = 'Receive Payment'    
                   --                        AND CODE = 'TransType') [Type]    
                   --FROM   tblReceipts r    
                   --       LEFT JOIN tblBRS brs    
                   --              ON brs.SourceID = r.ID    
                   --WHERE  r.PaymentMode <> (SELECT ID    
                   --                         FROM   tbl_CodeMaster    
                   --                         WHERE  Code = 'Cash'    
                --                                AND [Type] = 'PaymentType'    
                   --                                AND IsDeleted = 0)    
                   --       AND r.IsDeleted = 0    
                   --       AND brs.SourceType = (SELECT id    
                   --                             FROM   tblMasterTypes    
                   --                           WHERE  NAME = 'Receive Payment')    
                   --       AND r.DomainID = @DomainID    
                   --       AND ( r.BankID = @BankID    
                   --              OR '' = Isnull(@BankID, '') )    
                   --       AND ( @StartDate = ''    
                   --              OR r.Date >= @StartDate )    
                   --       AND ( @EndDate = ''    
                   --              OR r.Date <= @EndDate )    
                   --UNION ALL    
                   --SELECT DISTINCT r.ID                            AS ID,    
                   --                r.[Date],    
                   --                PartyName                       Party,    
                   --                NULL                            Receipts,    
                   --                ( CASE    
                   --                    WHEN ( TransactionType = (SELECT ID    
                   --                                              FROM   tblMasterTypes    
                   --                                              WHERE  NAME = 'Make Payment') ) THEN    
                   --                      ( r.Amount )    
                   --                    ELSE    
                   --                      NULL    
                   --                  END )                         Payments,    
                   --                r.CreatedOn                     modifiedOn,    
                   --                r.BankID,    
                   --                (SELECT NAME    
                   --                 FROM   tblMasterTypes    
                   --                 WHERE  NAME = 'Make Payment'    
                   --                        AND CODE = 'TransType') [Type]    
                   --FROM   tblReceipts r    
                   --       LEFT JOIN tblBRS brs    
                   --              ON brs.SourceID = r.ID    
                   --WHERE  r.PaymentMode <> (SELECT ID    
                   --                         FROM   tbl_CodeMaster    
                   --                         WHERE  Code = 'Cash'    
                   --                                AND [Type] = 'PaymentType'    
                   --                                AND IsDeleted = 0)    
                   --       AND r.IsDeleted = 0    
                   --       AND r.DomainID = @DomainID    
                   --       AND brs.SourceType = (SELECT id    
                   --                             FROM   tblMasterTypes    
                   --                             WHERE  NAME = 'Make Payment')    
                   --       AND ( r.BankID = @BankID    
                   --              OR '' = Isnull(@BankID, '') )    
                   --       AND ( @StartDate = ''    
                   --              OR r.Date >= @StartDate )    
                   --       AND ( @EndDate = ''    
                   --              OR r.Date <= @EndDate )    
                   --------------------------------------------    
                   SELECT DISTINCT r.ID                            AS ID,    
                                   r.[PaymentDate],    
                                   PartyName                       Party,    
                                   r.TotalAmount                   Receipts,    
                                   NULL                            Payments,    
                                   r.CreatedOn                     modifiedOn,    
                                   r.BankID,    
                                   (SELECT NAME    
                                    FROM   tblMasterTypes    
                                    WHERE  NAME = 'Receive Payment'    
                                           AND CODE = 'TransType') [Type]    
                   FROM   tbl_Receipts r    
                          LEFT JOIN tblBRS brs    
                                 ON brs.SourceID = r.ID    
                   WHERE  r.ModeID <> (SELECT ID    
                                       FROM   tbl_CodeMaster    
                                       WHERE  Code = 'Cash'    
                                        AND [Type] = 'PaymentType'    
                                              AND IsDeleted = 0)    
                          AND r.IsDeleted = 0    
                          AND brs.SourceType = (SELECT id    
                                                FROM   tblMasterTypes    
                                                WHERE  NAME = 'Receive Payment')    
                          AND r.DomainID = @DomainID    
                          AND ( r.BankID = @BankID    
                                 OR '' = Isnull(@BankID, '') )    
                          AND ( @StartDate = ''    
                                 OR r.PaymentDate >= @StartDate )    
                          AND ( @EndDate = ''    
                                 OR r.PaymentDate <= @EndDate )    
                   UNION ALL    
                   SELECT DISTINCT r.ID                            AS ID,    
                                   r.[PaymentDate],    
                                   PartyName                       Party,    
                                   NULL                            Receipts,    
                                   r.TotalAmount                   Payments,    
                                   r.CreatedOn                     modifiedOn,    
                                   r.BankID,    
                                   (SELECT NAME    
                                    FROM   tblMasterTypes    
                                    WHERE  NAME = 'Make Payment'    
                                           AND CODE = 'TransType') [Type]    
                   FROM   tbl_Payment r    
                          LEFT JOIN tblBRS brs    
                                 ON brs.SourceID = r.ID    
                   WHERE  r.ModeID <> (SELECT ID    
                                       FROM   tbl_CodeMaster    
                                       WHERE  Code = 'Cash'    
                                              AND [Type] = 'PaymentType'    
                                              AND IsDeleted = 0)    
                          AND r.IsDeleted = 0    
                          AND r.DomainID = @DomainID    
                          AND brs.SourceType = (SELECT id    
                                                FROM   tblMasterTypes    
                                                WHERE  NAME = 'Make Payment')    
                          AND ( r.BankID = @BankID    
                                 OR '' = Isnull(@BankID, '') )    
                          AND ( @StartDate = ''    
                                 OR r.PaymentDate >= @StartDate )    
                          AND ( @EndDate = ''    
                                 OR r.PaymentDate <= @EndDate )    
                   ---------------------------------------------------    
                   UNION ALL    
                   SELECT DISTINCT t.ID                      AS ID,    
                                   t.VoucherDate             [Date],    
                                  Case WHen (t.FromID = 0) Then 'Cash' Else b1.DisplayName END                   Party,    
                                   ( CASE    
                                       WHEN ( ToID <> 0 ) THEN    
                                         ( t.Amount )    
                                       ELSE    
                                         NULL    
                                     END )                   Receipts,    
                                   NULL                Payments,    
                                   t.CreatedOn               modifiedOn,    
                                   t.ToID                    BankID,    
                                   (SELECT NAME    
                                    FROM   tblMasterTypes    
                                    WHERE  NAME = 'ToBank'    
                                           AND CODE = 'EXP') [Type]    
                   FROM   tblTransfer t    
                          LEFT JOIN tblBank b    
                                 ON b.ID = t.ToID    
           LEFT Join tblBank b1 on b1.ID = t.FromID  
                          LEFT JOIN tblBRS brs    
                                 ON brs.SourceID = t.ID                     WHERE  t.IsDeleted = 0    
                          AND t.DomainID = @DomainID    
                          AND brs.SourceType = (SELECT id    
                                                FROM   tblMasterTypes    
                                                WHERE  NAME = 'ToBank')    
                          AND ( t.ToID = @BankID    
                                 OR '' = Isnull(@BankID, '') )    
                          AND ( @StartDate = ''    
                                 OR t.VoucherDate >= @StartDate )    
                          AND ( @EndDate = ''    
                                 OR t.VoucherDate <= @EndDate )    
                   UNION ALL    
                   SELECT DISTINCT t.ID                      AS ID,    
                                   t.VoucherDate             [Date],    
                                  Case WHen (t.ToID = 0) Then 'Cash' Else b1.DisplayName END             Party,    
                                   NULL                      Receipts,    
                                   ( CASE    
                                       WHEN ( FromID <> 0 ) THEN    
                                         ( t.Amount )    
                                       ELSE    
                                         NULL    
                                     END )                   Payments,    
                                   t.CreatedOn               modifiedOn,    
                                   t.FromID                  BankID,    
                                   (SELECT NAME    
                                    FROM   tblMasterTypes    
                                    WHERE  NAME = 'FromBank'    
                                           AND CODE = 'EXP') [Type]    
                   FROM   tblTransfer t    
                          LEFT JOIN tblBank b    
                                 ON b.ID = t.FromID    
        LEFT Join tblBank b1 on b1.ID = t.ToID  
                          LEFT JOIN tblBRS brs    
                                 ON brs.SourceID = t.ID    
                   WHERE  t.IsDeleted = 0    
                          AND t.DomainID = @DomainID    
                          AND brs.SourceType = (SELECT id    
                                                FROM   tblMasterTypes    
                                                WHERE  NAME = 'FromBank')    
                          AND ( t.FromID = @BankID    
                                 OR '' = Isnull(@BankID, '') )    
                          AND ( @StartDate = ''    
                                 OR t.VoucherDate >= @StartDate )    
                          AND ( @EndDate = ''    
                                 OR t.VoucherDate <= @EndDate ))    
          SELECT ID,    
                 [Date],    
                 Party,    
                 Receipts AS Debit,    
                 Payments As credit,    
                 CASE    
                   WHEN Receipts IS NULL THEN    
                     -1 * Payments    
                   ELSE    
                     Receipts    
                 END                            Amount,    
                 CONVERT (DECIMAL(16, 2), 0.00) ClosingBalance,    
                 modifiedOn,    
                 BankID,    
                 [Type],    
                 Row_number()    
                   OVER(    
                     PARTITION BY BankID    
                     ORDER BY [Date])           RowOrder    
          INTO   #TempResult    
          FROM   CTE    
          WHERE  [Date] >= @StartDate    
                 AND [Date] <= @EndDate    
    
          UPDATE rst    
          SET    ClosingBalance = rst.Amount    
          FROM   #TempResult rst    
          WHERE  RowOrder = 1    
    
          UPDATE rst    
          SET    ClosingBalance = (SELECT Sum([AMOUNT])    
                                   FROM   #TempResult t    
                                   WHERE  rst.BankID = t.BankID    
                                          AND t.RowOrder <= rst.RowOrder)    
          FROM   #TempResult rst    
    
          SELECT ID,    
                 Replace(CONVERT(VARCHAR(11), [Date], 106), ' ', '-') [Date],    
                 Party,    
                 Debit ,    
                 Credit,    
                 ClosingBalance                                       AS [Closing Balance],    
                 [Type]    
          --  Replace(CONVERT(VARCHAR(20), modifiedOn, 113), ' ', '-') modifiedOn            
          FROM   #TempResult    
          UNION ALL    
          SELECT NULL,    
                 NULL,    
                 'Total Amount',    
                 Sum(Debit),    
                 Sum(Credit),    
                 NULL,--Sum(Receipts) - Sum(Payments),      
                 NULL    
          --   NULL            
          FROM   #TempResult    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
