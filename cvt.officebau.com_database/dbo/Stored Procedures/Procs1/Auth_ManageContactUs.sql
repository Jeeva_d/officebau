﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	06-01-2016
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
      Manage Contact us
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_ManageContactUs]
 (
	@FirstName			VARCHAR(50)		,
	@LastName			VARCHAR(50)		,
	@Email				VARCHAR(50)		,
	@Phone				VARCHAR(15)		,
	@CompanyName		VARCHAR(150)	,
	@Message			VARCHAR(MAX)	
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY					
		SET NOCOUNT ON;
		DECLARE @Output						VARCHAR(100)				
		BEGIN TRANSACTION
			INSERT INTO Auth_ContactUs
			(
				FirstName		,
				LastName		,
				Email			,
				Phone			,
				CompanyName		,
				[Message]
			)
			VALUES
			(
				@FirstName		,
				@LastName		,
				@Email			,
				@Phone			,
				@CompanyName	,
				@Message		
			)
			SET @Output = 'OK'
		SELECT @Output
		COMMIT TRANSACTION      
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
