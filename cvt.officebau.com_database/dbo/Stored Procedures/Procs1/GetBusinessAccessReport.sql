﻿/**************************************************************************** 
CREATED BY			:	Priya K
CREATED DATE		:	20-Mar-2018
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	 [GetBusinessAccessReport] 2010,4
 </summary>                         
 *****************************************************************************/

CREATE PROCEDURE [dbo].[GetBusinessAccessReport] ( @EmployeeID INT,
	                                              @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
         
 
		  declare @BusinessUnit varchar(MAX), @BusinessUnitHeader varchar(MAX),
		          @sql nvarchar(max)
		          

		  SET @BusinessUnit = STUFF((SELECT ',[' + Name  + ']' 
		                             FROM tbl_BusinessUnit
									       where IsDeleted = 0 and ParentID <> 0 and DomainID = @DomainID  FOR XML PATH('')),1,1,'')
			

		  SET @BusinessUnitHeader = STUFF((SELECT ',ISNULL([' + Name  + '], 0) AS [' + Name  + ']'
		                             FROM tbl_BusinessUnit
									       where IsDeleted = 0 and ParentID <> 0 and DomainID = @DomainID  FOR XML PATH('')),1,1,'')
										   			 
          set @sql = '
		 select EmployeeCode, EmployeeName, ' + @BusinessUnitHeader + ' from ( select   
		                  EM.ID  AS ID, 
						  EM.Code AS EmployeeCode,
		                  EM.FirstName + '' '' + Isnull(EM.Lastname, '''')        AS  EmployeeName,
						  BU.Name  as BusinessUnitName 
						, (case when isnull(bu.id, 0) = 0 then 0 else 1 end) as Access
		         from tbl_EmployeeMaster EM
          LEFT JOIN tbl_BusinessUnit BU
		          ON  BU.ID in (select item from SplitString(EM.BusinessUnitID, '',''))
				  where EM.domainid = bu.domainid
				  and  EM.IsDeleted = 0
				  and isNull(EM.IsActive, 0) = 0
				  and ('+ cast(@EmployeeID as varchar(max))+ ' = 0 or EM.ID = '+ cast(@EmployeeID as varchar(max))+')  
				) as x
				pivot(max(Access) for BusinessUnitName in(' + @BusinessUnit + ')) As Bpivot'
				print @sql
				exec(@sql)
			 
			end try

		  BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
	  end
