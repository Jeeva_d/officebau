﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	Naneeshwar.M
MODIFIED DATE		:	
 <summary>        
	GetTDSConfiguration 1
	SELECT * FROM TDSConfiguration
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetTDSConfiguration] (@DomainID INT,
                                              @RegionID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT tds.Code,
                 Value,
                 emp.FullName   AS ModifiedBy,
                 tds.ModifiedOn AS ModifiedOn,
                 BU.NAME        AS BaseLocation
          FROM   TDSConfiguration tds
                 LEFT JOIN tbl_EmployeeMaster emp
                        ON emp.ID = tds.ModifiedBy
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = emp.BaseLocationID
          WHERE  tds.IsDeleted = 0
                 AND tds.DomainID = @DomainID
                 AND @RegionID = tds.RegionID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
