﻿    
/****************************************************************************     
CREATED BY   : Naneeshwar.M    
CREATED DATE  : 20-SEP-2017    
MODIFIED BY   :     
MODIFIED DATE  :     
<summary>            
    
</summary>                             
*****************************************************************************/    
CREATE PROCEDURE [dbo].[ManagePTConfiguration] (@ID              INT,    
                                               @MinGross        MONEY,    
                                               @MaxGross        MONEY,    
                                               @Amount          MONEY,    
                                               @BusinessUnitID  INT,    
                                               @FinancialYearID INT,    
                                               @SessionID       INT,    
                                               @IsDeleted       BIT,    
                                               @DomainID        INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(8000)    
    
      SET @FinancialYearID=(SELECT NAME    
                            FROM   tbl_FinancialYear    
                            WHERE  id = @FinancialYearID    
                                   AND IsDeleted = 0    
                                   AND DomainID = @DomainID)    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SET @Output = 'Operation Failed!'    
    
          IF( Isnull(@ID, 0) != 0 )    
            BEGIN    
                IF( @IsDeleted = 1 )    
                  BEGIN    
                      UPDATE tbl_PTConfiguration    
                      SET    IsDeleted = @IsDeleted,    
                             ModifiedBy = @SessionID,    
                             ModifiedOn = GETDATE()    
                      WHERE  ID = @ID    
                             AND DomainID = @DomainID    
    
                      SET @Output = 'Deleted Successfully.'    
    
                      GOTO Finish    
                  END    
    
                IF( @IsDeleted = 0 )    
                  BEGIN    
                      --IF EXISTS (SELECT 1    
                      --           FROM   tbl_PTConfiguration    
                      --           WHERE  IsDeleted = 0    
                      --                  AND FYID = @FinancialYearID    
                      --                  AND BusinessUnitID = @BusinessUnitID    
                      --                  AND ID = @ID)    
                      BEGIN    
                          UPDATE tbl_PTConfiguration    
                          SET    FYID = @FinancialYearID,    
                                 MinGross = @MinGross,    
                                 MaxGross = @MaxGross,    
                                 Amount = @Amount,    
                                 BusinessUnitID = @BusinessUnitID,    
                                 ModifiedBy = @SessionID,    
                                 ModifiedOn = GETDATE()    
                          WHERE  ID = @ID    
                                 AND DomainID = @DomainID    
    
                          SET @Output = 'Updated Successfully.'    
    
                          GOTO Finish    
                      END    
                  --ELSE    
                  --  BEGIN    
                  --      SET @Output = 'Already Exists!'    
                  --      GOTO Finish    
                  --  END    
                  END    
            END    
    
          IF( Isnull(@ID, 0) = 0 )    
            BEGIN    
                --IF NOT EXISTS (SELECT 1    
                --               FROM   tbl_PTConfiguration    
                --               WHERE  FYID = @FinancialYearID    
                --                      AND BusinessUnitID = @BusinessUnitID    
                --                      AND IsDeleted = 0)    
                --  BEGIN    
                BEGIN    
                    INSERT INTO tbl_PTConfiguration    
                     (FYID,    
                                 MinGross,    
                                 MaxGross,    
                                 Amount,    
                                 BusinessUnitID,    
                                 CreatedBy,    
                                 CreatedOn,    
                                 ModifiedBy,    
                                 ModifiedOn,    
                                 DomainID)    
                    VALUES      ( @FinancialYearID,    
                                  @MinGross,    
                                  @MaxGross,    
                                  @Amount,    
                                  @BusinessUnitID,    
                                  @SessionID,    
                                  GETDATE(),    
                                  @SessionID,    
                                  GETDATE(),    
                                  @DomainID)    
    
                    SET @Output = 'Inserted Successfully.'    
    
                    GOTO Finish    
                END    
            --  END    
            --ELSE    
            --  BEGIN    
            --      SET @Output = 'Already Exists!'    
            --      GOTO Finish    
            -- END    
            END    
    
          FINISH:    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
