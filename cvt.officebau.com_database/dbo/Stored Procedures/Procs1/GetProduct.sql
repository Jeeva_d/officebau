﻿/****************************************************************************                     
CREATED BY   : Jeeva                    
CREATED DATE  :                     
MODIFIED BY   : SIVA.B                  
MODIFIED DATE  : 28-11-2016                    
 <summary>                  
 [Getproduct] 1,1,1                    
 select * from tblproduct                  
 </summary>                                             
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[GetProduct] (@ID       INT,      
                                    @DomainID INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT pd.ID                                                                                   AS ID,      
                 pd.NAME                                                                                    AS NAME,      
                 Rate                                                                                    AS Rate,      
                 ProductDescription                                                                      AS Remarks,      
                 TransactionTypeID                                                                       AS TransactionTypeID,      
                 TypeID                                                                                  AS TypeID,      
                 Manufacturer                                                                            AS Manufacturer,      
                 pd.ModifiedOn                                                                           AS ModifiedOn,      
                 EMP.FullName                                                                            AS ModifiedBy,      
                 OpeningStock                                                                            AS OpeningStock,      
                 ( (SELECT Isnull(Sum(QTY), 0)      
                    FROM   tblExpenseDetails eit      
                    WHERE  pd.ID = eit.LedgerID and IsLedger=0      
                           AND DomainID = @DomainID      
                           AND IsDeleted = 0) - (SELECT Isnull(Sum(QTY), 0)      
                                                 FROM   tblInvoiceItem it      
                                                 WHERE  pd.ID = it.ProductID      
                                                        AND DomainID = @DomainID      
                                                        AND IsDeleted = 0) + Isnull(pd.OpeningStock, 0) )AS AvailableStock,      
                 GstHSNID                                                                                AS HSNID,      
                 GH.Code                                                                                 AS HsnCode,      
                 GH.SGST,      
                 GH.CGST,      
                 GH.IGST,      
                 pd.LedgerId,    
                 pd.UOMID,
                 U.Name AS UOM   
          FROM   tblproduct pd      
                 LEFT JOIN tbl_EmployeeMaster EMP      
                        ON EMP.ID = pd.ModifiedBy      
                 LEFT JOIN tblGstHSN GH      
                        ON pd.GstHSNID = GH.ID  
                  LEFT JOIN tbl_UOM U      
                        ON U.ID = pd.UOMID      
          WHERE  ( pd.ID = @ID      
                    OR @ID IS NULL )      
                 AND pd.DomainID = @DomainID      
                 AND pd.IsDeleted = 0      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
