﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_ManageEmployeePayStructureDetails] (@ID                     INT,  
                                                               @Value                  DECIMAL(18, 2),  
                                                               @ComponentId            INT,  
                                                               @EmployeePayStructureID INT,  
                                                               @SessionID              INT,  
                                                               @IsDeleted              BIT,  
                                                               @DomainID               INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(8000)  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          IF EXISTS (SELECT TOP 1 ID  
                     FROM   tbl_Pay_EmployeePayroll  
                     WHERE  EmployeePayStructureID = @EmployeePayStructureId  
                            AND IsDeleted = 0  
                            AND DomainID = @DomainID)  
            BEGIN  
                SET @Output = (SELECT [Message]  
                               FROM   tblErrorMessage  
                               WHERE  [Type] = 'Warning'  
                                      AND Code = 'RCD_REF'  
                                      AND IsDeleted = 0)  
  
                GOTO FINISH;  
            END  
  
          --IF Exists(Select Top 1 Id from tbl_Pay_EmployeePayStructure Where ComponentID = @ComponentId and EffectiveFrom =@EffectiveFrom And ID <>@ID and EmployeeID=@EmployeeID)  
          --BEGIN   
          --  SET @Output = 'Already PayStructure Configured for the select Date'  
          -- GOTO FINISH;  
          --END  
          --------------Delete --------------  
          ---------------------------Update ---------------------------  
          IF (select Count(*) from tbl_Pay_EmployeePayStructureDetails WHERE  PayStructureId = @EmployeePayStructureID and  ComponentId=@ComponentId and Isdeleted=0 )>0
            BEGIN  
                UPDATE tbl_Pay_EmployeePayStructureDetails  
                SET    Amount = @Value,  
                       ModifiedBy = @SessionID,  
                       ModifiedOn = Getdate()  
                WHERE  PayStructureId = @EmployeePayStructureID and  ComponentId=@ComponentId and Isdeleted=0  
  
                SET @Output = (SELECT [Message]  
                               FROM   tblErrorMessage  
                               WHERE  [Type] = 'Information'  
                                      AND Code = 'RCD_UPD'  
                                      AND IsDeleted = 0)  
  
                GOTO FINISH;  
            END  
  
          ---------------------------Insert ----------------------------  
          IF (select Count(*) from tbl_Pay_EmployeePayStructureDetails WHERE  PayStructureId = @EmployeePayStructureID and  ComponentId=@ComponentId and Isdeleted=0 )=0 
            BEGIN  
                INSERT INTO tbl_Pay_EmployeePayStructureDetails  
                            (PayStructureId,  
                             ComponentId,  
                             Amount,  
                             IsDeleted,  
                             DomainID,  
                             CreatedBy,  
                             CreatedOn,  
                             ModifiedBy,  
                             ModifiedOn,  
                             HistoryID)  
                SELECT @EmployeePayStructureId,  
                       @ComponentId,  
                       @Value,  
                       0,  
                       @DomainID,  
                       @SessionID,  
                       Getdate(),  
                       @SessionID,  
                       Getdate(),  
                       Newid();  
  
                SET @Output = (SELECT [Message]  
                               FROM   tblErrorMessage  
                               WHERE  [Type] = 'Information'  
                                      AND Code = 'RCD_INS'  
                                      AND IsDeleted = 0)  
  
            GOTO FINISH;  
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = ERROR_MESSAGE(),  
                 @ErrSeverity = ERROR_SEVERITY()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
