﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	30-12-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
     User subscribe from website
	 [Auth_ManageSubscribe] 'rajbharath@gmail.com'
 </summary>                         
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Auth_ManageSubscribe]
(
	@EmailID		Varchar(100)				
)
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY					
		SET NOCOUNT ON;
		DECLARE @Output						VARCHAR(100)				
		BEGIN TRANSACTION

		IF NOT EXISTS (SELECT 1 FROM Auth_Subscribe WHERE EmailID = @EmailID)
		BEGIN
			INSERT INTO Auth_Subscribe
			(
				EmailID
			)
			VALUES
			(
				@EmailID
			)
			SET @Output = 'OK'
		END
		ELSE
			SET @Output = 'This Email-id already subscribed!'
		SELECT @Output
		COMMIT TRANSACTION      
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
