﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	06-01-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	Search Requested user for trail version\
	[Auth_SearchRequestTrial]

	select * from Auth_RequestTrial
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_SearchRequestTrial]
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY				
		SELECT 
			ID				AS ID				,
			CompanyName		AS CompanyName		,
			EmailID			AS Email			,
			[Description]	AS [Description]	,
			CreatedDate		AS RequestedDate	,
			ISNULL(IsEmailSend,0) AS IsEmailSend,
			ISNULL(IsReject,0) AS IsReject
		FROM 
			Auth_RequestTrial 
		WHERE 
			IsDeleted = 0 
			-- AND ISNULL(IsEmailSend,0) = 0 AND ISNULL(IsReject,0) = 0
			ORDER BY ID Desc
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
