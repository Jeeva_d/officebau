﻿      
/****************************************************************************               
CREATED BY  :  Ajith N          
CREATED DATE :  13 Nov 2017           
MODIFIED BY  :              
MODIFIED DATE   :         
 <summary>                      
    Missedcheckincheckoutreportlist 4,1, 1, 2,'0,2,4',2   
 </summary>                                       
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[MissedCheckInCheckOutReportList] (@Year         INT,      
                                                         @MonthID      INT,      
                                                         @DomainID     INT,      
                                                         @UserID       INT,      
                                                         @BusinessUnit VARCHAR(50),    
                                                         @StatusID   INT)      
AS      
    
  BEGIN      
      BEGIN TRY      
          DECLARE @StartDate       VARCHAR(20) = 1,      
                  @MonthCode       VARCHAR(20),      
                  @BusinessUnitIDs VARCHAR(50),      
                  @YearID          INT = (SELECT NAME      
                     FROM   Tbl_FinancialYear      
                     WHERE  ID = @Year      
                            AND IsDeleted = 0      
                            AND DomainID = @DomainID)      
      
          SET @MonthCode = (SELECT Code      
                            FROM   Tbl_Month      
                            WHERE  Id = @MonthID      
                                   AND IsDeleted = 0)      
      
          IF( Isnull(@BusinessUnit, '') = '' )      
            SET @BusinessUnitIDs = (SELECT BusinessUnitID      
                                    FROM   tbl_EmployeeMaster      
                                    WHERE  ID = @UserID      
                                           AND DomainID = @DomainID)      
          ELSE      
            SET @BusinessUnitIDs =@BusinessUnit      
      
          DECLARE @BusinessUnitTable TABLE      
            (      
               BusinessUnit INT      
            )      
      
          INSERT INTO @BusinessUnitTable      
                      (BusinessUnit)      
          SELECT Item      
          FROM   Splitstring(@BusinessUnitIDs, ',')      
          WHERE  Isnull(Item, '') <> ''      
      
          DECLARE @PunchTime DATETIME = @MonthCode + ' ' + @StartDate + ' '      
            + Cast(@YearID AS VARCHAR);      
      
          SELECT Isnull(Em.EmpCodePattern, '') + Em.Code                                                         AS EmployeeCode,      
                 Em.Id                                                           AS EmployeeId,      
                 Em.FirstName + ' ' + Isnull(Em.LastName, '')                    AS EmployeeName,      
                 (SELECT Count(1)      
                  FROM   Tbl_EmployeeMissedPunches Cin      
                  WHERE  Cin.Id = Ms.Id      
                         AND Cin.PunchTypeId = (SELECT Id      
                                                FROM   Tbl_CodeMaster      
                                                WHERE  Type = 'PunchType'      
                                                       AND Code = 'Check In'))   AS [CheckIN],      
                 (SELECT Count(1)      
                  FROM   Tbl_EmployeeMissedPunches Cout      
                  WHERE  Cout.Id = Ms.Id      
                         AND Cout.PunchTypeId = (SELECT Id      
                                                 FROM   Tbl_CodeMaster      
                                                 WHERE  Type = 'PunchType'      
                                                        AND Code = 'Check Out')) AS [CheckOut],      
                 @PunchTime      AS PunchTime,      
                 Em.BaseLocationID                                               AS BaseLocationID,      
                 BT.NAME                                 AS BusinessUnit,      
                 REG.NAME                                                        AS Region,    
             ST.Code                        AS [Status]    
          INTO   #temp      
          FROM   Tbl_EmployeeMissedPunches Ms      
                 LEFT JOIN Tbl_EmployeeMaster Em      
                        ON Em.Id = Ms.EmployeeId      
                           AND Em.IsDeleted = 0      
                 LEFT JOIN tbl_BusinessUnit BT      
                        ON Em.BaseLocationID = BT.ID      
                 LEFT JOIN tbl_BusinessUnit REG      
                        ON REG.ID = BT.ParentID     
                  LEFT JOIN tbl_Status ST      
                        ON ST.ID = Ms.StatusID    
          WHERE  Ms.DomainID = @DomainID      
                 AND (ISNULL(@YearID,0) = 0 OR Year(Ms.PunchDate) = @YearID)     
                 AND (ISNULL(@MonthID,0) = 0 OR Month(Ms.PunchDate) = @MonthID)      
                 --AND (Ms.StatusId = (SELECT Id      
                 --                   FROM   Tbl_Status      
                 --                   WHERE  Type = 'Leave'      
                 --                          AND Code = 'Approved') )    
                 AND (ISNull(@StatusID, 0) = 0     
                          OR Ms.StatusID = @StatusID )    
                 AND Ms.IsDeleted =0  
          SELECT EmployeeCode,      
                 EmployeeName,      
                 EmployeeId,      
                 Sum([CheckIN])  AS [CheckIN],      
                 Sum([CheckOut]) AS [CheckOut],      
                 PunchTime,      
                 BusinessUnit,      
                 Region,    
                 [Status]      
          FROM   #temp      
          WHERE  BaseLocationID IN (SELECT BusinessUnit      
                                    FROM   @BusinessUnitTable)      
          GROUP  BY EmployeeCode,      
                    EmployeeName,      
                    EmployeeId,      
                    PunchTime,      
                    BusinessUnit,      
                    Region,    
                    [Status]      
      END TRY      
      BEGIN CATCH      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END 
