﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :           
MODIFIED BY   :           
MODIFIED DATE  :           
<summary>                  
 [Rpt_costcentertransactiondetail] '','','Project','Expense',2        
</summary>                                   
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Rpt_costcentertransactiondetail] (@StartDate       DATETIME = NULL,      
                                                          @EndDate         DATETIME = NULL,      
                                                          @SearchParameter VARCHAR(100),      
                                                          @Type            VARCHAR(50),      
                                                          @DomainID        INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          DECLARE @FinancialYear INT;      
          DECLARE @StartFinancialMonth INT;      
          DECLARE @EndFinancialMonth INT;      
      
          IF ( @StartDate IS NULL      
                OR @StartDate = '' )      
              OR ( @EndDate IS NULL      
                    OR @EndDate = '' )      
            BEGIN      
                SET @StartFinancialMonth = (SELECT Value      
                                            FROM   tblApplicationConfiguration      
                                            WHERE  Code = 'STARTMTH'      
                                                   AND DomainID = @DomainID)      
                SET @EndFinancialMonth = ( ( (SELECT Value      
                                              FROM   tblApplicationConfiguration      
                                              WHERE  Code = 'STARTMTH'      
                                                     AND DomainID = @DomainID)      
                                             + 11 ) % 12 )      
      
                IF( Month(Getdate()) <= ( ( (SELECT Value      
                                             FROM   tblApplicationConfiguration      
                                             WHERE  Code = 'STARTMTH'      
                                                    AND DomainID = @DomainID)      
                                            + 11 ) % 12 ) )      
                  SET @FinancialYear = Year(Getdate()) - 1      
                ELSE      
                  SET @FinancialYear = Year(Getdate())      
      
                SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date            
                SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date           
            END      
      
          IF( @Type = 'Expense' )      
            BEGIN      
                SELECT e.ID                                    AS ID,      
                       e.Type                                  AS Type,      
                       ( (SELECT ( Isnull(Sum(Amount*Qty), 0) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0))      
                          FROM   tblExpenseDetails      
                          WHERE  ExpenseID = e.ID      
                                 AND IsDeleted = 0      
                                 AND DomainID = @DomainID)      
                          ) AS Amount,      
                       BillNo                                  AS BillNo,      
                       Date                                    AS Date,      
                       v.NAME                                  AS VendorName      
                FROM   tblExpense e      
                       LEFT JOIN tblVendor v      
                              ON v.ID = e.VendorID      
                WHERE  Date >= @StartDate      
                       AND Date <= @EndDate      
                       AND e.IsDeleted = 0      
                       AND e.DomainID = @DomainID      
   AND e.CostCenterID = (SELECT ID      
                                             FROM   tblCostCenter      
                                             WHERE  @SearchParameter = NAME      
                                                    AND IsDeleted = 0      
                                                    AND DomainID = @DomainID)      
                GROUP  BY e.ID,      
                          e.Type,      
                          v.NAME,      
                          BillNo,      
                          Date      
                ORDER  BY Date DESC      
            END      
          ELSE      
            BEGIN      
                SELECT i.ID      AS ID,      
                       i.Type    AS Type,      
                       ( CASE      
                           WHEN ( i.DiscountPercentage <> 0 ) THEN (SELECT ( Isnull(Sum(Qty * Rate)      
                                                                                    + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum(Qty * Rate) * i.DiscountPercentage ), 0) ), 0) )      
                                                                    FROM   tblInvoiceItem      
                                                                    WHERE  InvoiceID = i.ID      
                                                                           AND IsDeleted = 0      
                                                                           AND DomainID = @DomainID)      
                           ELSE (SELECT ( Isnull(( Sum(Qty * Rate)      
                                                   + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(i.DiscountValue, 0) ), 0) )      
                                 FROM   tblInvoiceItem      
                                 WHERE  InvoiceID = i.ID      
                                        AND IsDeleted = 0      
                                        AND DomainID = @DomainID)      
                         END )   AS Amount,      
                       InvoiceNo AS BillNo,      
                       Date      AS Date,      
                       c.NAME    AS VendorName      
                FROM   tblInvoice i      
                       LEFT JOIN tblCustomer c      
                              ON c.ID = i.CustomerID      
                       LEFT JOIN tbl_CodeMaster cm      
                              ON cm.ID = i.StatusID      
                WHERE  Date >= @StartDate      
                       AND Date <= @EndDate      
                       AND i.IsDeleted = 0      
                       AND i.DomainID = @DomainID      
                       AND i.RevenueCenterID = (SELECT ID      
                                                FROM   tblCostCenter      
                                                WHERE  @SearchParameter = NAME      
                                                       AND IsDeleted = 0      
                                                       AND DomainID = @DomainID)      
                GROUP  BY i.ID,      
                          i.Type,      
                          c.NAME,      
                          cm.Code,      
                          InvoiceNo,      
                          Date,      
                          i.DiscountPercentage,      
                          i.DiscountValue      
                ORDER  BY Date DESC      
            END      
      END TRY      
      BEGIN CATCH      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
