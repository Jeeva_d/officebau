﻿/****************************************************************************           
CREATED BY  : Naneeshwar          
CREATED DATE :           
MODIFIED BY  : JENNIFER S  
MODIFIED DATE : 12-FEB-2018        
 <summary>                  
  [Manageaccountreceipts] 1,2,3,'aa','a',8,122,1,2,2,1,'sd',2,1,1,1,1    
 </summary>                                   
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Manageaccountreceipts] (@ID              INT,  
                                                @Date            DATETIME,  
                                                @ReceiptNO       VARCHAR(100),  
                                                @Description     VARCHAR(1000),  
                                                @PartyName       VARCHAR(100),  
                                                @TransactionType VARCHAR(100),  
                                                @TotalAmount     MONEY,  
                                                @PaymentModeID   INT,  
                                                @BankID          INT,  
                                                @Reference       VARCHAR(1000),  
                                                @LedgerID        INT,  
                                                @IsDeleted       BIT,  
                                                @SessionID       INT,  
                                                @DomainID        INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          DECLARE @OUTPUT            VARCHAR(100),  
                  @InvoicePaymentID  INT,  
                  @InvoiceID         INT,  
                  @cash              MONEY,  
                  @TransactionTypeID INT =(SELECT ID  
                    FROM   tblMasterTypes  
                    WHERE  NAME = @TransactionType),  
                  @PreviousCash      MONEY = (SELECT Amount  
                     FROM   tblReceipts  
                     WHERE  ID = @ID  
                            AND DomainID = @DomainID),  
                  @PreviousMode      INT = (SELECT PaymentMode  
                     FROM   tblReceipts  
                     WHERE  isdeleted = 0  
                            AND ID = @ID  
                            AND DomainID = @DomainID)  
  
          IF( (SELECT Count(1)  
               FROM   tblCashBucket  
               WHERE  DomainID = @DomainID) = 0 )  
            INSERT INTO tblCashBucket  
                        (AvailableCash,  
                         DomainID)  
            VALUES      (0,  
                         @DomainID)  
  
          IF( @IsDeleted = 1 )  
            BEGIN  
                IF EXISTS (SELECT 1  
                           FROM   tblbrs  
                           WHERE  SourceID = @ID  
                                  AND SourceType = @TransactionTypeID  
                                  AND IsDeleted = 0  
                                  AND IsReconsiled = 1  
                                  AND DomainID = @DomainID)  
                  BEGIN  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Information'  
                                            AND Code = 'RCD_RECON'  
                                            AND IsDeleted = 0) --'The record has been Reconciled.'  
  
                      GOTO finish  
                  END  
                ELSE  
                  BEGIN  
                      IF( @PaymentModeID = (SELECT ID  
                                            FROM   tbl_CodeMaster  
                                            WHERE  Code = 'Cash'  
                                                   AND IsDeleted = 0) )  
                        BEGIN  
                            UPDATE tblCashBucket  
                            SET    AvailableCash = ( CASE  
             WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                         ( AvailableCash - @TotalAmount )  
                                                       ELSE  
                                                         ( AvailableCash + @TotalAmount )  
                                                     END ),  
                                   ModifiedOn = Getdate()  
                            WHERE  @DomainID = DomainID  
  
                            UPDATE tblVirtualCash  
                            SET    IsDeleted = 1  
                            WHERE  SourceID = @ID  
                                   AND SourceType = ( CASE  
                                                        WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                          (SELECT ID  
                                                           FROM   tblMasterTypes  
                                                           WHERE  NAME = 'Receive Payment')  
                                                        ELSE  
                                                          ((SELECT ID  
                                                            FROM   tblMasterTypes  
                                                            WHERE  NAME = 'Make Payment'))  
                                                      END )  
                                   AND IsDeleted = 0  
                        END  
  
                      UPDATE tblReceipts  
                      SET    IsDeleted = 1  
                      WHERE  ID = @ID  
  
                      UPDATE tblBookedBankBalance  
                      SET    IsDeleted = 1  
                      WHERE  BRSID = (SELECT ID  
                                      FROM   tblBRS  
                                      WHERE  SourceID = @ID  
                                             AND SourceType = @TransactionTypeID  
                                             AND IsDeleted = 0  
                                             AND DomainID = @DomainID)  
  
                      UPDATE tblBRS  
                      SET    IsDeleted = 1  
                      WHERE  SourceID = @ID  
                             AND SourceType = @TransactionTypeID  
                             AND DomainID = @DomainID  
  
                      EXEC Managesystembankbalance  
                        @DomainID  
                  END  
  
                SET @Output = (SELECT [Message]  
                               FROM   tblErrorMessage  
                               WHERE  [Type] = 'Information'  
                                      AND Code = 'RCD_DEL'  
                                      AND IsDeleted = 0) -- 'Deleted Successfully'  
  
                GOTO finish  
            END  
          ELSE  
            BEGIN  
                IF( @ID = 0 )  
                  BEGIN  
                      INSERT INTO tblReceipts  
                                  (PartyName,  
                                   [Date],  
                                   ReceiptNo,  
                                   TransactionType,  
                                   [Description],  
                                   Amount,  
                                   PaymentMode,  
                                   BankID,  
                                   Reference,  
                                   LedgerID,  
                                   DomainID,  
                                   CreatedBy,  
                                   ModifiedBy)  
                      VALUES      (@PartyName,  
                                   @Date,  
                                   Isnull(@ReceiptNO, ''),  
                                   @TransactionTypeID,  
                                   @Description,  
                                   @TotalAmount,  
                                   @PaymentModeID,  
                                   @BankID,  
                    @Reference,  
                                   @LedgerID,  
                                   @DomainID,  
                                   @SessionID,  
                                   @SessionID)  
  
                      DECLARE @SourceID INT =@@IDENTITY  
  
                      IF( Isnull(@BankID, 0) <> 0 )  
                        BEGIN  
                            INSERT INTO tblBRS  
                                        (BankID,  
                                         SourceID,  
                                         SourceDate,  
                                         SourceType,  
                                         Amount,  
                                         DomainID,  
                                         BRSDescription,  
                                         CreatedBy,  
                                         ModifiedBy)  
                            VALUES      (@BankID,  
                                         @SourceID,  
                                         @Date,  
                                         ( CASE  
                                             WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                               (SELECT ID  
                                                FROM   tblMasterTypes  
                                                WHERE  NAME = 'Receive Payment')  
                                             ELSE  
                                               ((SELECT ID  
                                                 FROM   tblMasterTypes  
                                                 WHERE  NAME = 'Make Payment'))  
                                           END ),  
                                         @TotalAmount,  
                                         @DomainID,  
                                         @Description,  
                                         @SessionID,  
                                         @SessionID)  
  
                            INSERT INTO tblBookedBankBalance  
                                        (BRSID,  
                                         Amount,  
                                         DomainID,  
                                         CreatedBy,  
                                         ModifiedBy)  
                            VALUES      (@@IDENTITY,  
                                         @TotalAmount,  
                                         @DomainID,  
                                         @SessionID,  
                                         @SessionID)  
  
                            EXEC Managesystembankbalance  
                              @DomainID  
                        END  
  
                      IF( @PaymentModeID = (SELECT ID  
                                            FROM   tbl_CodeMaster  
                                            WHERE  Code = 'Cash'  
                                                   AND IsDeleted = 0) )  
                        BEGIN  
                            UPDATE tblCashBucket  
                            SET    AvailableCash = ( CASE  
                                                       WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                         ( AvailableCash + @TotalAmount )  
                                                       ELSE  
                                                         ( AvailableCash - @TotalAmount )  
                                                     END ),  
                                   ModifiedOn = Getdate()  
                            WHERE  @DomainID = DomainID  
  
                            INSERT INTO tblVirtualCash  
                                        (SourceID,  
                                         Amount,  
                                         [Date],  
                                         SourceType,  
                                         DomainID,  
  CreatedBy,  
                                         ModifiedBy)  
                            VALUES      (@SourceID,  
                                         ( CASE  
                                             WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                               ( @TotalAmount )  
                                             ELSE  
                                               ( 0 - @TotalAmount )  
                                           END ),  
                                         @Date,  
                                         ( CASE  
                                             WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                               (SELECT ID  
                                                FROM   tblMasterTypes  
                                                WHERE  NAME = 'Receive Payment')  
                                             ELSE  
                                               ((SELECT ID  
                                                 FROM   tblMasterTypes  
                                                 WHERE  NAME = 'Make Payment'))  
                                           END ),  
                                         @DomainID,  
                                         @SessionID,  
                                         @SessionID)  
                        END  
  
                      SET @Output = (SELECT [Message]  
                                     FROM   tblErrorMessage  
                                     WHERE  [Type] = 'Information'  
                                            AND Code = 'RCD_INS'  
                                            AND IsDeleted = 0) --'Inserted Successfully'  
                  END  
                ELSE  
                  BEGIN  
                      --'The record has been Reconciled.'  
                      IF EXISTS (SELECT 1  
                                 FROM   tblbrs  
                                 WHERE  SourceID = @ID  
                                        AND SourceType = @TransactionTypeID  
                                        AND IsDeleted = 0  
                                        AND IsReconsiled = 1  
                                        AND DomainID = @DomainID)  
                        BEGIN  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Information'  
                                                  AND Code = 'RCD_UPD_DES'  
                                                  AND IsDeleted = 0) --'The record is referred. Description is Updated.'  
  
                            UPDATE tblReceipts  
                            SET    ReceiptNo = @ReceiptNO,  
                                   Reference = @Reference,  
                                   [Description] = @Description,  
                                   ModifiedBy = @SessionID,  
                                   LedgerID = @LedgerID,  
                                   ModifiedOn = Getdate()  
                            WHERE  ID = @ID  
                                   AND DomainID = @DomainID  
  
                            UPDATE tblbrs  
                            SET    BRSDescription = @Description  
                            WHERE  SourceID = @ID  
                                   AND SourceType = @TransactionTypeID  
                                   AND DomainID = @DomainID  
  
                            GOTO finish  
                        END  
                      ELSE  
                        BEGIN  
                            UPDATE tblReceipts  
                            SET    PartyName = @PartyName,  
                                   Date = @Date,  
                                   ReceiptNO = @ReceiptNO,  
                                   TransactionType = @TransactionTypeID,  
     Description = @Description,  
                                   Amount = @TotalAmount,  
                                   PaymentMode = @PaymentModeID,  
                                   BankID = Isnull(@BankID, 0),  
                                   Reference = @Reference,  
                                   LedgerID = @LedgerID,  
                                   ModifiedBy = @SessionID,  
                                   ModifiedOn = Getdate()  
                            WHERE  ID = @ID  
                                   AND DomainID = @DomainID  
  
                            IF( Isnull(@BankID, 0) <> 0 )  
                              BEGIN  
                                  IF EXISTS(SELECT 1  
                                            FROM   tblBRS  
                                            WHERE  SourceID = @ID  
                                                   AND SourceType = @TransactionTypeID  
                                                   AND DomainID = @DomainID  
                                                   AND IsDeleted = 0)  
                                    BEGIN  
                                        UPDATE tblbrs  
                                        SET    Amount = @TotalAmount,  
                                               BRSDescription = @Description,  
                                               BankID = @BankID,  
                                               SourceType = ( CASE  
                                                                WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                                  (SELECT ID  
                                                                   FROM   tblMasterTypes  
                                                                   WHERE  NAME = 'Receive Payment')  
                                                                ELSE  
                                                                  ((SELECT ID  
                                                                    FROM   tblMasterTypes  
                                                                    WHERE  NAME = 'Make Payment'))  
                                                              END )  
                                        WHERE  SourceID = @ID  
                                               AND SourceType = @TransactionTypeID  
                                               AND DomainID = @DomainID  
  
                                        UPDATE tblBookedBankBalance  
                                        SET    Amount = @TotalAmount  
                                        WHERE  BRSID = (SELECT ID  
                                                        FROM   tblBRS  
                                                        WHERE  SourceID = @ID  
                                                               AND SourceType = @TransactionTypeID  
                                                               AND IsDeleted = 0  
                                                               AND DomainID = @DomainID)  
                                               AND DomainID = @DomainID  
  
                                        EXEC Managesystembankbalance  
                                          @DomainID  
                                    END  
                                  ELSE  
                                    BEGIN  
                                        INSERT INTO tblBRS  
                                                    (BankID,  
                                                     SourceID,  
                                                     SourceDate,  
                                                     SourceType,  
                                                     Amount,  
                                                     BRSDescription)  
                                        VALUES      (@BankID,  
                                                     @ID,  
                                                     @Date,  
                                                     ( CASE  
                                                         WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                           (SELECT ID  
                                                            FROM   tblMasterTypes  
                                                            WHERE  NAME = 'Receive Payment')  
                                                         ELSE  
                                                           ((SELECT ID  
                                                             FROM   tblMasterTypes  
                                                             WHERE  NAME = 'Make Payment'))  
                                                       END ),  
                                                     @TotalAmount,  
                                                     @Description)  
  
                                        INSERT INTO tblBookedBankBalance  
                                                    (BRSID,  
                                                     Amount,  
                                                     DomainID,  
                                                     CreatedBy,  
                                                     ModifiedBy)  
                                        VALUES      (@@IDENTITY,  
                                                     @TotalAmount,  
                                                     @DomainID,  
                                                     @SessionID,  
                                                     @SessionID)  
  
                                        EXEC Managesystembankbalance  
                                          @DomainID  
  
                                        UPDATE tblCashBucket  
                                        SET    AvailableCash = ( CASE  
                                                                   WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                                     ( AvailableCash - @TotalAmount )  
                                                                   ELSE  
                                                                     ( AvailableCash + @TotalAmount )  
                                                                 END ),  
                                               ModifiedOn = Getdate()  
                                        WHERE  @DomainID = DomainID  
  
                                        UPDATE tblVirtualCash  
                                        SET    IsDeleted = 1  
                                        WHERE  SourceID = @ID  
                                               AND SourceType = ( CASE  
                                                                    WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                                      (SELECT ID  
                                                                       FROM   tblMasterTypes  
                                                                       WHERE  NAME = 'Receive Payment')  
                                                                    ELSE  
                                                                      ((SELECT ID  
                                                                        FROM   tblMasterTypes  
                                                                        WHERE  NAME = 'Make Payment'))  
                                                                  END )  
                                               AND IsDeleted = 0  
                                               AND DomainID = @DomainID  
                                    END  
                              END  
                            ELSE  
                              BEGIN  
   IF( (SELECT 1  
                                       FROM   tblBRS  
                                       WHERE  SourceID = @ID  
                                              AND SourceType = @TransactionTypeID  
                                              AND DomainID = @DomainID  
                                              AND IsDeleted = 0) = 1 )  
                                    BEGIN  
                                        UPDATE tblBookedBankBalance  
                                        SET    IsDeleted = 1  
                                        WHERE  BRSID = (SELECT ID  
                                                        FROM   tblBRS  
                                                        WHERE  SourceID = @ID  
                                                               AND SourceType = @TransactionTypeID  
                                                               AND IsDeleted = 0)  
                                               AND DomainID = @DomainID  
  
                                        UPDATE tblBRS  
                                        SET    IsDeleted = 1  
                                        WHERE  SourceID = @ID  
                                               AND SourceType = @TransactionTypeID  
                                               AND DomainID = @DomainID  
                                               AND IsDeleted = 0  
  
                                        EXEC Managesystembankbalance  
                                          @DomainID  
                                    END  
                              END  
  
                            IF( @PaymentModeID = (SELECT ID  
                                                  FROM   tbl_CodeMaster  
                                                  WHERE  Code = 'Cash'  
                                                         AND IsDeleted = 0) )  
                              BEGIN  
                                  IF ( @PreviousMode = (SELECT ID  
                                                        FROM   tbl_CodeMaster  
                                                        WHERE  Code = 'Cash'  
                                                               AND IsDeleted = 0) )  
                                    BEGIN  
                                        UPDATE tblCashBucket  
                                        SET    AvailableCash = ( CASE  
                                                                   WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                                     ( AvailableCash - @PreviousCash + @TotalAmount )  
                                                                   ELSE  
                                                                     ( AvailableCash + @PreviousCash - @TotalAmount )  
                                                                 END ),  
                                               ModifiedOn = Getdate()  
                                        WHERE  @DomainID = DomainID  
  
                                        UPDATE tblVirtualCash  
                                        SET    Amount = ( CASE  
                                                            WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                              ( @TotalAmount )  
                                                            ELSE  
                                                              ( 0 - @TotalAmount )  
                                                          END )  
                                        WHERE  SourceID = @ID  
                                               AND SourceType = ( CASE  
                                                                    WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                                      (SELECT ID  
          FROM   tblMasterTypes  
                                                                       WHERE  NAME = 'Receive Payment')  
                                                                    ELSE  
                                                                      ((SELECT ID  
                                                                        FROM   tblMasterTypes  
                                                                        WHERE  NAME = 'Make Payment'))  
                                                                  END )  
                                               AND IsDeleted = 0  
                                               AND DomainID = @DomainID  
                                    END  
                                  ELSE  
                                    BEGIN  
                                        UPDATE tblCashBucket  
                                        SET    AvailableCash = ( CASE  
                                                                   WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                                     ( AvailableCash + @TotalAmount )  
                                                                   ELSE  
                                                                     ( AvailableCash - @TotalAmount )  
                                                                 END ),  
                                               ModifiedOn = Getdate()  
                                        WHERE  @DomainID = DomainID  
  
                                        INSERT INTO tblVirtualCash  
                                                    (SourceID,  
                                                     Amount,  
                                                     Date,  
                                                     SourceType,  
                                                     DomainID,  
                                                     CreatedBy,  
                                                     ModifiedBy)  
                                        VALUES      (@ID,  
                                                     ( CASE  
                                                         WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                           ( @TotalAmount )  
                                                         ELSE  
                                                           ( 0 - @TotalAmount )  
                                                       END ),  
                                                     @Date,  
                                                     ( CASE  
                                                         WHEN ( @TransactionType = 'Receive Payment' ) THEN  
                                                           (SELECT ID  
                                                            FROM   tblMasterTypes  
                                                            WHERE  NAME = 'Receive Payment')  
                                                         ELSE  
                                                           ((SELECT ID  
                                                             FROM   tblMasterTypes  
                                                             WHERE  NAME = 'Make Payment'))  
                                                       END ),  
                                                     @DomainID,  
                                                     @SessionID,  
                                                     @SessionID)  
                                    END  
                              END  
  
                            SET @Output = (SELECT [Message]  
                                           FROM   tblErrorMessage  
                                           WHERE  [Type] = 'Information'  
                                                  AND Code = 'RCD_UPD'  
                                                  AND IsDeleted = 0) --'Updated Successfully.'  
  
                            GOTO finish  
                        END  
                  END  
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
