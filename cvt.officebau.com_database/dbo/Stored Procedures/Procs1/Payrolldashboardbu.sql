﻿/****************************************************************************     
CREATED BY      : Naneeshwar.M    
CREATED DATE  : 05-JULY-2017    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>      
 [Payrolldashboardbu] null,3,0,1 ,1   
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Payrolldashboardbu] (@MonthId    INT,    
                                            @Year       INT,    
                                            @Type       Varchar(100),    
                                            @DomainID   INT,    
                                            @EmployeeID INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
      SET @Year = (SELECT NAME    
                   FROM   tbl_FinancialYear    
                   WHERE  id = @Year    
                          AND DomainID = @DomainID)    
    
      DECLARE @DataSource TABLE    
        (    
           [Value] NVARCHAR(128)    
        )    
    
      INSERT INTO @DataSource    
                  ([Value])    
      SELECT Item    
      FROM   dbo.Splitstring ((SELECT businessunitid    
                               FROM   tbl_employeemaster    
                               WHERE  id = @EmployeeID    
                                      AND DomainID = @DomainID), ',')    
      WHERE  Isnull(Item, '') <> ''    
    
      BEGIN TRY    
          BEGIN    
              IF( @Type = 'BUSINESS UNIT' )    
                BEGIN    
                    SELECT DISTINCT bu.NAME                  AS BusinessUnit,    
                                    Round(Sum(p.Amount), 0) AS NetSalary    
                    FROM   tbl_Pay_EmployeePayroll ep    
     LEFT JOIN tbl_Pay_EmployeePayrollDetails p on p.PayrollID =ep.ID and p.Isdeleted=0  
     AND p.ComponentID   =(Select Value from tbl_DashBoardConfiguration where [Key]='Tile1ID'  and DomainID =@DomainID)    
                           LEFT JOIN tbl_EmployeeMaster emp    
                                  ON emp.ID = ep.EmployeeId    
                           LEFT JOIN tbl_BusinessUnit bu    
                                  ON bu.ID = emp.BaseLocationID    
                    WHERE  ep.IsProcessed = 1    
                           AND ep.IsDeleted = 0    
                           AND ( ( @MonthId IS NULL    
                                    OR @MonthId = '' )    
                                  OR ( ep.monthid = @MonthId ) )    
                           AND [yearid] = @Year    
                           AND ep.DomainId = @DomainID    
                           AND emp.BaselocationID IN(SELECT *    
                                                    FROM   @DataSource)    
                    GROUP  BY bu.NAME    
                END    
              ELSE  IF ( @Type = 'Designation' ) 
                BEGIN    
                    SELECT DISTINCT Isnull(de.NAME, 'Others') AS BusinessUnit,    
                                    Round(Sum(p.Amount), 0)  AS NetSalary    
                    FROM   tbl_pay_EmployeePayroll ep    
					 LEFT JOIN tbl_Pay_EmployeePayrollDetails p on p.PayrollID =ep.ID and p.Isdeleted=0  
					 AND p.ComponentID   =(Select Value from tbl_DashBoardConfiguration where [Key]='Tile1ID'  and DomainID =@DomainID)   
                           LEFT JOIN tbl_EmployeeMaster emp    
                                  ON emp.ID = ep.EmployeeId    
                           LEFT JOIN tbl_Designation de    
                                  ON de.ID = emp.DesignationID    
                    WHERE  ep.IsProcessed = 1    
                           AND ep.IsDeleted = 0    
                           AND ( ( @MonthId IS NULL    
                                    OR @MonthId = '' )    
                                  OR ( ep.monthid = @MonthId ) )    
                           AND [yearid] = @Year    
                           AND ep.DomainId = @DomainID    
                           AND emp.BaselocationID IN(SELECT *    
                                                    FROM   @DataSource)    
           GROUP  BY de.NAME    
                END   
				   ELSE  
                BEGIN    
                    SELECT DISTINCT Isnull(de.NAME, 'Others') AS BusinessUnit,    
                                    Round(Sum(p.Amount), 0)  AS NetSalary    
                    FROM   tbl_pay_EmployeePayroll ep    
					 LEFT JOIN tbl_Pay_EmployeePayrollDetails p on p.PayrollID =ep.ID and p.Isdeleted=0  
					 AND p.ComponentID   =(Select Value from tbl_DashBoardConfiguration where [Key]='Tile1ID'  and DomainID =@DomainID)   
                           LEFT JOIN tbl_EmployeeMaster emp    
                                  ON emp.ID = ep.EmployeeId    
                           LEFT JOIN tbl_Department de    
                                  ON de.ID = emp.DepartmentID    
                    WHERE  ep.IsProcessed = 1    
                           AND ep.IsDeleted = 0    
                           AND ( ( @MonthId IS NULL    
                                    OR @MonthId = '' )    
                                  OR ( ep.monthid = @MonthId ) )    
                           AND [yearid] = @Year    
                           AND ep.DomainId = @DomainID    
                           AND emp.BaselocationID IN(SELECT *    
                                                    FROM   @DataSource)    
           GROUP  BY de.NAME    
                END    
          END    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR (@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
