﻿/**************************************************************************** 
CREATED BY    		:	Ajith N
CREATED DATE		:	26-JULY-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
	[SearchHeadcount]  1, 106
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchHeadcount] (@DomainID INT,
                                         @UserID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT *
          INTO   #tmpAccess
          FROM   Splitstring((SELECT BusinessUnitID
                              FROM   tbl_EmployeeMaster
                              WHERE  ID = @UserID), ',')

          SELECT Isnull(BU.NAME, 'Others') AS BusinessUnit,
                 Count(1)                  AS Headcount
          FROM   tbl_EmployeeMaster EM
                 JOIN tbl_BusinessUnit BU
                   ON BU.ID = EM.BaseLocationID
          WHERE  EM.IsDeleted = 0
                 AND Isnull(EM.IsActive, 0) = 0
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND EM.DomainID = @DomainID
                 AND EM.BaseLocationID IN (SELECT item
                                           FROM   #tmpAccess)
          GROUP  BY BU.NAME
      END TRY
      BEGIN CATCH         
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
