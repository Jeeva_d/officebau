﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	05-JUN-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
   [Searchrbsmenuforuser] 5,null,4
 </summary>                          
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchrbsmenuforuser] (@UserID   INT,
                                              @ModuleID INT,
                                              @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT RSM.SubModule          AS Module,
                 RBS.Menu               AS Menu,
                 Isnull(RUM.MRead, 0)   AS MenuRead,
                 Isnull(RUM.MWrite, 0)  AS MenuWrite,
                 Isnull(RUM.MEdit, 0)   AS MenuEdit,
                 Isnull(RUM.MDelete, 0) AS MenuDelete,
                 RBS.ID                 AS MenuID,
                 RSM.ID                 AS ModuleID
          FROM   tbl_RBSMenu RBS
                 LEFT JOIN tbl_RBSSubModule RSM
                        ON RSM.ID = RBS.SubModuleID
                 LEFT JOIN tbl_RBSUserMenuMapping RUM
                        ON RUM.MenuID = RBS.ID
                           AND RUM.EmployeeID = Isnull(@UserID, RUM.EmployeeID)
                           AND RUM.DomainID = @DomainID
          WHERE  RSM.ID = Isnull(@ModuleID, RSM.ID)
                 AND RBS.IsSubmenu = 0
                 AND RBS.menucode <> 'COMPANY'
				 and RBS.DomainID=@DomainID AND RBS.IsDeleted=0
          ORDER  BY RSM.SubModule ASC,
		            RBS.Menu ASC,
                    RBS.MenuOrderID ASC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
