﻿/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  : 03-Sep-2018  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
   [SearchRBSSubmoduleMenuforUser] 2,null,1  
 </summary>                            
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchRBSSubmoduleMenuforUser] (@UserID   INT,
                                                       @ModuleID INT,
                                                       @DomainID INT,
                                                       @Type     VARCHAR(20))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF( @Type = 'REPORT' )
            BEGIN
                SELECT RBS.[Description]      AS Module,
                       --SUB.SubModule          AS Module,
                       RBS.Menu               AS Menu,
                       Isnull(RUM.MRead, 0)   AS MenuRead,
                       Isnull(RUM.MWrite, 0)  AS MenuWrite,
                       Isnull(RUM.MEdit, 0)   AS MenuEdit,
                       Isnull(RUM.MDelete, 0) AS MenuDelete,
                       RBS.ID                 AS MenuID,
                       RBS.SubModuleID        AS ModuleID
                FROM   tbl_RBSMenu RBS
                       JOIN tbl_RBSMenu SUBRBS
                         ON SUBRBS.ID = RBS.SubModuleID
                       LEFT JOIN tbl_RBSSubModule SUB
                              ON SUB.ID = SUBRBS.SubModuleID
                       LEFT JOIN tbl_RBSUserMenuMapping RUM
                              ON RUM.MenuID = RBS.ID
                                 AND RUM.EmployeeID = Isnull(@UserID, RUM.EmployeeID)
                                 AND RUM.DomainID = @DomainID
                WHERE  SUBRBS.ID = Isnull(@ModuleID, SUBRBS.ID)
                       AND RBS.ISDELETED = 0
                       AND RBS.IsSubmenu = 1
                       AND RBS.RootConfig = 'REPORT'
                       AND RBS.DomainID = @DomainID
                ORDER  BY RBS.[Description],
                          --SUB.SubModule ASC,
                          SUBRBS.ID ASC,
                          RBS.Menu ASC,
                          RBS.MenuOrderID ASC
            END
          ELSE
            BEGIN
                SELECT RBS.[Description]      AS Module,
                       --SUB.SubModule          AS Module,
                       RBS.Menu               AS Menu,
                       Isnull(RUM.MRead, 0)   AS MenuRead,
                       Isnull(RUM.MWrite, 0)  AS MenuWrite,
                       Isnull(RUM.MEdit, 0)   AS MenuEdit,
                       Isnull(RUM.MDelete, 0) AS MenuDelete,
                       RBS.ID                 AS MenuID,
                       RBS.SubModuleID        AS ModuleID
                FROM   tbl_RBSMenu RBS
                       JOIN tbl_RBSMenu SUBRBS
                         ON SUBRBS.ID = RBS.SubModuleID
                       LEFT JOIN tbl_RBSSubModule SUB
                              ON SUB.ID = SUBRBS.SubModuleID
                       LEFT JOIN tbl_RBSUserMenuMapping RUM
                              ON RUM.MenuID = RBS.ID
                                 AND RUM.EmployeeID = Isnull(@UserID, RUM.EmployeeID)
                                 AND RUM.DomainID = @DomainID
                WHERE  SUBRBS.ID = Isnull(@ModuleID, SUBRBS.ID)
                       AND RBS.ISDELETED = 0
                       AND RBS.IsSubmenu = 1
                       AND RBS.RootConfig = 'CONFIG'
                       AND RBS.DomainID = @DomainID
                ORDER  BY RBS.[Description],
                          --SUB.SubModule ASC,
                          SUBRBS.ID ASC,
                          RBS.Menu ASC,
                          RBS.MenuOrderID ASC
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
