﻿/**************************************************************************** 
CREATED BY    		:	Ajith N
CREATED DATE		:	26-JULY-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
	[SearchAttrition]  1,106
 </summary>                         
 *****************************************************************************/

CREATE PROCEDURE [dbo].[SearchAttrition] (@DomainID INT,
                                         @UserID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT *
          INTO   #tmpAccess
          FROM   SplitString((SELECT BusinessUnitID
                              FROM   tbl_EmployeeMaster
                              WHERE  ID = @UserID), ',')

          -- Attrition = ((No of Notactive Employee(Current year) * 100.00)) / ((No of Total Employee (Except Current Year)) + (No of New Join Employee(Current Year)))) / 100.00
          SELECT ISNULL(BU.NAME, 'Others')                                                                                                                         AS BusinessUnit,
                 ( ( ( (SELECT CASE
                                 WHEN ISNULL(COUNT(1), 0) = 0 THEN 1
                                 ELSE COUNT(1)
                               END
                        FROM   tbl_EmployeeMaster EMN
                        WHERE  EMN.IsDeleted = 0
                               AND ISNULL(EMN.IsActive, 0) = 1
                               AND YEAR(EMN.DOJ) = ( YEAR(GETDATE()) )
                               AND EMN.BaseLocationID = BU.ID
                               AND EMN.Code NOT LIKE 'TMP_%'
                               AND EMN.DomainID = @DomainID
                               AND EMN.BaseLocationID IN (SELECT item
                                                         FROM   #tmpAccess)) * 100.00 ) / ( (SELECT ISNULL(COUNT(1), 0)
                                                                                             FROM   tbl_EmployeeMaster EMA
                                                                                             WHERE  EMA.IsDeleted = 0
                                                                                                    AND ISNULL(EMA.IsActive, 0) = 0
                                                                                                    AND YEAR(EMA.DOJ) < ( YEAR(GETDATE()) )
                                                                                                    AND EMA.BaseLocationID = BU.ID
                                                                                                    AND EMA.Code NOT LIKE 'TMP_%'
                                                                                                    AND EMA.DomainID = @DomainID
                                                                                                    AND EMA.BaseLocationID IN (SELECT item
                                                                                                                              FROM   #tmpAccess))
                                                                                            + (SELECT ISNULL(COUNT(1), 0)
                                                                                               FROM   tbl_EmployeeMaster EMA
                                                                                               WHERE  EMA.IsDeleted = 0
                                                                                                      AND ISNULL(EMA.IsActive, 0) = 0
                                                                                                      AND YEAR(EMA.DOJ) = ( YEAR(GETDATE()) )
                                                                                                      AND EMA.BaseLocationID = BU.ID
                                                                                                      AND EMA.Code NOT LIKE 'TMP_%'
                                                                                                      AND EMA.DomainID = @DomainID
                                                                                                      AND EMA.BaseLocationID IN (SELECT item
                                                                                                                                FROM   #tmpAccess)) ) ) / 100.00 ) AS Attrition,
                 BU.ID
          FROM   tbl_BusinessUnit BU
                 JOIN tbl_EmployeeMaster EM
                   ON BU.ID = EM.BaseLocationID
                      AND BU.ParentID <> 0
          WHERE  EM.IsDeleted = 0
                 AND ISNULL(EM.IsActive, 0) = 0
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND EM.DomainID = @DomainID
                 AND EM.BaseLocationID IN (SELECT item
                                           FROM   #tmpAccess)
          GROUP  BY BU.NAME,
                    BU.ID
          ORDER  BY Attrition DESC,
                    BusinessUnit

      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
