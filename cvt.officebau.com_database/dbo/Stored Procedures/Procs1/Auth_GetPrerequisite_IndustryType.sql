﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	24-12-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
      
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_GetPrerequisite_IndustryType]
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY					
	
		SELECT ID AS ID ,IndustryName As Code,'Industry' AS [Type] from Auth_IndustryType WHERE IsDeleted=0 ORDER BY Code
		
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
