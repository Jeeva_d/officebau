﻿/****************************************************************************               
CREATED BY   : Dhanalakshmi. S              
CREATED DATE  :     03-12-2018          
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>            
[Searchinventorydashboardlist]    1, 'OutofStock'       
 </summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchInventoryDashboardList] (@DomainID   INT,
                                                       @ScreenType VARCHAR(20))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @ScreenType = 'LowThresold' )
            BEGIN
                SELECT P.NAME                      AS [Product Name],
                       UM.NAME                     AS UOM,
                       MFG                         AS Manufacturer,
                       GH.Code                     AS [HSN Code],
                       Isnull(LowStockThresold, 0) AS [Low Stock Threshold],
                       Isnull(PurchasePrice, 0)               AS [Purchase Price],
                       PL.NAME                     AS [Purchase Ledger],
                       PurchaseDescription         AS [Purchase Description],
                       Isnull(SalesPrice, 0)                  AS [Sales Price],
                       SL.NAME                     AS [Sales Ledger],
                       SalesDescription            AS [Sales Description],
                       OpeningStock                AS [Opening Stock],
                       VW.StockonHand              AS [Stock on Hand],
                       VW.AvailableStock           AS [Available Stock]
                FROM   tblProduct_V2 P
                       LEFT JOIN VW_Inventory VW
                              ON P.ID = VW.ProductID
                       LEFT JOIN tbl_UOM UM
                              ON UM.ID = P.UOMId
                       LEFT JOIN tblGSTHSN GH
                              ON GH.ID = P.HSNId
                       LEFT JOIN tblLedger PL
                              ON PL.ID = P.PurchaseLedgerId
                       LEFT JOIN tblLedger SL
                              ON SL.ID = P.SalesLedgerId
                WHERE  P.IsDeleted = 0
                       AND P.DomainID = @DomainID
                       AND p.NAME IS NOT NULL
                       AND P.IsInventroy = 1
                       AND Isnull(LowStockThresold, 0) <> 0
                       AND Isnull(LowStockThresold, 0) >= StockonHand
                ORDER  BY P.NAME ASC
            END
          ELSE IF ( @ScreenType = 'OutofStock' )
            BEGIN
                SELECT P.NAME                      AS [Product Name],
                       UM.NAME                     AS UOM,
                       MFG                         AS Manufacturer,
                       GH.Code                     AS [HSN Code],
                       ISNULL(PurchasePrice, 0)               AS [Purchase Price],
                       PL.NAME                     AS [Purchase Ledger],
                       PurchaseDescription         AS [Purchase Description],
                       ISNULL(SalesPrice ,0)                 AS [Sales Price],
                       SL.NAME                     AS [Sales Ledger],
                       SalesDescription            AS [Sales Description],
                       OpeningStock                AS [Opening Stock],
                       VW.StockonHand              AS [Stock on Hand],
                       Isnull(LowStockThresold, 0) AS [Low Stock Threshold]
                FROM   tblProduct_V2 P
                       LEFT JOIN VW_Inventory VW
                              ON P.ID = VW.ProductID
                       LEFT JOIN tbl_UOM UM
                              ON UM.ID = P.UOMId
                       LEFT JOIN tblGSTHSN GH
                              ON GH.ID = P.HSNId
                       LEFT JOIN tblLedger PL
                              ON PL.ID = P.PurchaseLedgerId
                       LEFT JOIN tblLedger SL
                              ON SL.ID = P.SalesLedgerId
                WHERE  StockonHand <= 0
                       AND P.IsInventroy = 1
                ORDER  BY P.NAME ASC
            END
          ELSE IF ( @ScreenType = 'PO' )
            BEGIN
                SELECT P.NAME                                              AS [Product Name],
                       UM.NAME                                             AS UOM,
                       MFG                                                 AS Manufacturer,
                       GH.Code                                             AS [HSN Code],
                       OpeningStock                                        AS [Opening Stock],
                       ( Isnull(Sum(QTY), 0) - Isnull(Sum(BilledQTY), 0) ) AS Quantity
                FROM   tblPODetails POD
                       LEFT JOIN tblProduct_V2 P
                              ON P.ID = POD.LedgerID
                       LEFT JOIN tbl_UOM UM
                              ON UM.ID = P.UOMId
                       LEFT JOIN tblGSTHSN GH
                              ON GH.ID = P.HSNId
                       LEFT JOIN tblLedger PL
                              ON PL.ID = P.PurchaseLedgerId
                       LEFT JOIN tblLedger SL
                              ON SL.ID = P.SalesLedgerId
                WHERE  POD.IsDeleted = 0
                       AND POD.DomainID = @DomainID
                       AND POD.IsLedger = 0
                       AND ( BilledQTY = 0
                              OR BilledQTY <> QTY )
                       AND P.IsInventroy = 1
                GROUP  BY P.NAME,
                          UM.NAME,
                          MFG,
                          OpeningStock,
                          GH.Code
                ORDER  BY P.NAME ASC
            END
          ELSE
            BEGIN
                SELECT P.NAME                                                AS [Product Name],
                       UM.NAME                                               AS UOM,
                       MFG                                                   AS Manufacturer,
                       GH.Code                                               AS [HSN Code],
                       OpeningStock                                          AS [Opening Stock],
                       ( Isnull(Sum(QTY), 0) - Isnull(Sum(InvoicedQTY), 0) ) AS Quantity
                FROM   tblSoItem SOT
                       LEFT JOIN tblProduct_V2 P
                              ON P.ID = SOT.ProductID
                       LEFT JOIN tbl_UOM UM
                              ON UM.ID = P.UOMId
                       LEFT JOIN tblGSTHSN GH
                              ON GH.ID = P.HSNId
                       LEFT JOIN tblLedger PL
                              ON PL.ID = P.PurchaseLedgerId
                       LEFT JOIN tblLedger SL
                              ON SL.ID = P.SalesLedgerId
                WHERE  SOT.IsDeleted = 0
                       AND SOT.DomainID = @DomainID
                       AND SOT.IsProduct = 0
                       AND ( InvoicedQTY = 0
                              OR InvoicedQTY <> QTY )
                       AND P.IsInventroy = 1
                GROUP  BY P.NAME,
                          UM.NAME,
                          MFG,
                          OpeningStock,
                          GH.Code
                ORDER  BY P.NAME ASC
            END

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
