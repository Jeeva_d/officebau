﻿
/****************************************************************************   
CREATED BY    : 
CREATED DATE  : 
MODIFIED BY   : 
MODIFIED DATE : 
 <summary> 
		[Getbalancesheet] 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getbalancesheet] (@DomainID INT)
AS
  BEGIN
      BEGIN TRY
          ------- P & l --------------
          DECLARE @PL TABLE
            (
               ID          INT IDENTITY(1, 1),
               NAME        VARCHAR(100),
               AMOUNT      MONEY,
               lEDGER      VARCHAR(100),
               GROUPLEDGER VARCHAR(100),
               CHATAGERY   VARCHAR(100)
            )
          DECLARE @plvALUE MONEY,
                  @pltypee VARCHAR(100)='P&L Profit'

          INSERT INTO @PL
          SELECT tblledger.NAME,
                 openingBalance,
                 tblledger.NAME,
                 s.NAME,
                 'EXPENSE'
          FROM   tblledger
                 JOIN tblGroupLedger s
                   ON tblledger.GroupID = s.ID
                      AND tblledger.DomainID = @DomainID
          WHERE  isexpense = 1
                 AND tblledger.isdeleted = 0
          UNION
          SELECT NAME,
                 AMOUNT,
                 Ledger,
                 Groups,
                 'EXPENSE'
          FROM   VW_expense
          UNION
          SELECT CUSTOMER,
                 AMOUNT,
                 'ACCOUNT RESIVABLES',
                 'RECEIVABLES',
                 'INCOME'
          FROM   VW_INCOME
          WHERE  Type <> 'AccountReceipts'
          UNION
          SELECT tblledger.NAME,
                 openingBalance,
                 tblledger.NAME,
                 s.NAME,
                 'INCOME'
          FROM   tblledger
                 JOIN tblGroupLedger s
                   ON tblledger.GroupID = s.ID
                      AND tblledger.DomainID = @DomainID
          WHERE  isexpense = 0
                 AND tblledger.isdeleted = 0

          SET @plvALUE= ( (SELECT Isnull(Sum(AMOUNT), 0)
                           FROM   @PL
                           WHERE  CHATAGERY = 'INCOME') - (SELECT Isnull(Sum(AMOUNT), 0)
                                                           FROM   @PL
                                                           WHERE  CHATAGERY = 'EXPENSE') )

          IF( @plvALUE < 0 )
            BEGIN
                SET @pltypee='P&L Loss'
                SET @plvALUE=( @plvALUE * ( -1 ) )
            END

          --Balance Sheet--
          --select * from @PL
          DELETE @PL

          INSERT INTO @PL
          SELECT tblledger.NAME,
                 openingBalance,
                 tblledger.NAME,
                 s.NAME,
                 'EXPENSE'
          FROM   tblledger
                 JOIN tblGroupLedger s
                   ON tblledger.GroupID = s.ID
                      AND tblledger.DomainID = @DomainID
          WHERE  isexpense = 1
                 AND tblledger.isdeleted = 0
          UNION
          SELECT NAME,
                 Amount - Isnull(PaidAmount, 0),
                 NAME,
                 'Accounts Payable',
                 'EXPENSE'
          FROM   VW_expense
          UNION
          SELECT CUSTOMER,
                 AMOUNT,
                 CUSTOMER,
                 L.NAME,
                 'EXPENSE'
          FROM   VW_INCOME
                 LEFT JOIN TBLLEDGER L
                        ON L.id = VW_INCOME.lEDGERid
                           AND L.IsExpense = 1
          WHERE  Type = 'AccountReceipts'
          UNION
          SELECT CUSTOMER,
                 AMOUNT - Isnull(Received, 0),
                 CUSTOMER,
                 'RECEIVABLES',
                 'INCOME'
          FROM   VW_INCOME
          UNION
          SELECT tblledger.NAME,
                 openingBalance,
                 tblledger.NAME,
                 s.NAME,
                 'INCOME'
          FROM   tblledger
                 JOIN tblGroupLedger s
                   ON tblledger.GroupID = s.ID
                      AND tblledger.DomainID = @DomainID
          WHERE  isexpense = 0
                 AND tblledger.isdeleted = 0
          UNION ALL
          SELECT 'pl parameter',
                 @plvALUE,
                 @pltypee,
                 @pltypee,
                 CASE
                   WHEN ( @pltypee = 'P&L Loss' ) THEN 'INCOME'
                   ELSE 'EXPENSE'
                 END

          SELECT NAME,
                 Isnull(AMOUNT, 0) AS Amount,
                 lEDGER,
                 GROUPLEDGER,
                 CHATAGERY
          FROM   @pl
          WHERE  Amount <> 0
          UNION
          SELECT DISTINCT b.NAME,
                          Isnull(( (SELECT Isnull(Sum(amount), 0)
                                    FROM   tblbrs
                                    WHERE  isdeleted = 0
                                           AND SourceType IN((SELECT ID
                                                              FROM   tblMasterTypes
                                                              WHERE  BankID = tbrs.BankID
                                                                     AND NAME IN ( 'InvoiceReceivable', 'AccountReceipts', 'ToBank' )))) - (SELECT Isnull(Sum(amount), 0)
                                                                                                                                            FROM   tblbrs
                                                                                                                                            WHERE  isdeleted = 0
                                                                                                                                                   AND SourceType IN((SELECT ID
                                                                                                                                                                      FROM   tblMasterTypes
                                                                                                                                                                      WHERE  BankID = tbrs.BankID
                                                                                                                                                                             AND NAME IN ( 'PayBill', 'FromBank', 'Bill', 'Expense' )))) ) + b.OpeningBalance, 0),
                          b.NAME,
                          'Bank',
                          'INCOME'
          FROM   tblBank b
                 LEFT JOIN TblBrs tbrs
                        ON tbrs.bankID = b.ID
          WHERE  b.isdeleted = 0
          UNION
          SELECT 'Cash',
                 AvailableCash,
                 'CASH',
                 'Bank',
                 'INCOME'
          FROM   tblcashBucket
          UNION
          SELECT NAME,
                 OpeningBalance,
                 NAME,
                 'Bank',
                 'EXPENSE'
          FROM   tblBank
          WHERE  isdeleted = 0
                 AND openingbalance <> 0
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END


