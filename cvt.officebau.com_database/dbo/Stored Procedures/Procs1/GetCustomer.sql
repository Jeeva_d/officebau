﻿/****************************************************************************         
CREATED BY   : Dhanalakshmi        
CREATED DATE  : 14/11/2016        
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>      
 [GetCustomer] 3,1,1        
 select * from tblCustomer      
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[GetCustomer] (@ID       INT,      
                                      @DomainID INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT Cus.ID              AS ID,      
                 Cus.NAME            AS NAME,      
                 Cus.EmailID         AS EmailID,      
                 Cus.CustomerEmailID AS CustomerEmailID,      
                 Cus.ContactNo       AS ContactNo,      
                 Cus.ContactPerson   AS ContactPerson,      
                 Cus.ContactPersonNo AS ContactPersonNo,      
                 Cus.CityID          AS CityID,      
                 CITY.NAME           AS CityName,      
                 Cus.[Address]       AS [Address],      
                 Cus.BillingAddress  AS BillingAddress,      
                 Cus.CurrencyID      AS CurrencyID,      
                 Cus.PaymentTerms    AS PaymentTerms,      
                 Cus.PanNo           AS PanNo,      
                 Cus.TanNo           AS TanNo,      
                 Cus.Remarks         AS Remarks,      
                 Cus.GSTNo           AS GSTNo,      
                 Cus.ModifiedOn      AS ModifiedOn,      
                 EMP.FullName       AS ModifiedBy  
                  
          FROM   tblCustomer Cus      
                 LEFT JOIN tbl_City CITY      
                        ON CITY.ID = Cus.CityID      
                 LEFT JOIN tbl_EmployeeMaster EMP      
                        ON EMP.ID = Cus.ModifiedBy      
          WHERE  Cus.ID = @ID      
                 AND Cus.DomainID = @DomainID      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
