﻿/****************************************************************************     
CREATED BY   : Ajith N    
CREATED DATE  : 08 Aug 2017    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
   [ExportEmployeeMasterList]  '' , '', '', '', 'false', 1,'123'    
 </summary>                              
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ExportEmployeeMasterList] (@EmployeeCode     VARCHAR(20),    
                                                   @Name             VARCHAR(100),    
                                                   @DepartmentID     INT,    
                                                   @DesignationID    INT,    
                                                   @FunctionalID     INT,    
                                                   @BusinessUnit     VARCHAR(20),    
                                                   @ContactNo        VARCHAR(20),    
                                                   @EmailID          VARCHAR(50),    
                                                   @EmploymentTypeID INT,    
                                                   @ReportingTo      VARCHAR(100),    
                                                   @PAN              VARCHAR(50),    
                                                   @PFNo             VARCHAR(50),    
                                                   @EEESI            VARCHAR(50),    
                                                   @AadharID         VARCHAR(50),    
                                                   @UAN              VARCHAR(50),    
                                                   @PolicyNo         VARCHAR(50),    
                  
                                                   @IsActive         BIT,    
                                                   @EmployeeID       INT,    
                                                   @DomainID         INT )    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          DECLARE @BusinessUnitIDs VARCHAR(50)    
    
          IF( Isnull(@BusinessUnit, 0) = 0 )    
            SET @BusinessUnitIDs = (SELECT BusinessUnitID    
                                    FROM   tbl_EmployeeMaster    
                                    WHERE  ID = @EmployeeID    
                                           AND DomainID = @DomainID)    
          ELSE    
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','    
    
          DECLARE @BusinessUnitTable TABLE    
            (    
               BusinessUnit VARCHAR(100)    
            )    
    
          INSERT INTO @BusinessUnitTable    
          SELECT @BusinessUnitIDs    
    
          SELECT EM.Code                                           AS [Employee Code],  
                 EM.FirstName + ' ' + Isnull(EM.LastName, '')      AS [Name],    
                 DEP.NAME                                          AS [Department],    
                 DESI.NAME                                         AS [Designation],    
                 FUN.NAME                                          AS [Functional],    
                 BU.NAME                                           AS [Business Unit],    
                 Isnull(EM.ContactNo, '')                          AS [Contact No],    
                 Isnull(EM.EmergencyContactNo, '')                 AS [Emergency Mobile No],    
                 EM.EmailID                                        AS [Email ID],    
                 EMPLOYEETYPE.NAME                                 AS [Employment Type],    
                 Isnull(REPORTING.Code, '')+ ' - ' +REPORTING.FirstName + ' '    
                 + Isnull(REPORTING.LastName, '')                  AS [Reporting To],    
                 STATUTORY.PAN_No                                  AS [PAN],    
                 STATUTORY.PFNo                                    AS [PF No],    
                 STATUTORY.ESINo                                   AS [ESI No],    
                 STATUTORY.AadharID                                AS [Aadhar No],    
                 STATUTORY.UANNo                                   AS [UAN],    
                 STATUTORY.PolicyNo                                AS [Personal Medical Insurance No],    
     STATUTORY.CompanyPolicyNo AS [Company Policy No],    
     STATUTORY.TPI AS [Third Party Insurance]    
          FROM   tbl_EmployeeMaster EM    
                 LEFT JOIN tbl_Department DEP    
                        ON DEP.ID = EM.DepartmentID    
                 LEFT JOIN tbl_Designation DESI    
                        ON DESI.ID = EM.DesignationID    
                 LEFT JOIN tbl_Functional FUN    
                        ON FUN.ID = EM.FunctionalID    
                 LEFT JOIN tbl_BusinessUnit BU    
                        ON BU.ID = EM.BaseLocationID    
                 LEFT JOIN tbl_BusinessUnit REGION    
                        ON REGION.ID = EM.RegionID    
                 LEFT JOIN tbl_EmployementType EMPLOYEETYPE    
                        ON EMPLOYEETYPE.ID = EM.EmploymentTypeID    
                 LEFT JOIN tbl_EmployeeMaster REPORTING    
                        ON REPORTING.ID = EM.ReportingToID    
                 LEFT JOIN tbl_EmployeeStatutoryDetails STATUTORY    
                        ON STATUTORY.EmployeeID = EM.ID    
                 LEFT JOIN tbl_EmployeeMaster REPORTINGTO    
                        ON REPORTINGTO.ID = EM.ReportingToID    
                 JOIN @BusinessUnitTable TBU    
                   ON TBU.BusinessUnit LIKE '%,'    
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))    
                                            + ',%'    
          WHERE  EM.IsDeleted = 0    
                 AND ( Isnull(@EmployeeCode, '') = ''    
                        OR ( EM.Code LIKE '%' + @EmployeeCode + '%' ) )    
                 AND ( Isnull(@Name, '') = ''    
                        OR ( ( EM.FirstName + ' ' + Isnull(EM.LastName, '') ) LIKE '%' + @Name + '%' ) )    
                 AND ( Isnull(@ContactNo, '') = ''    
                        OR ( EM.ContactNo LIKE '%' + @ContactNo + '%' ) )    
                 AND ( @BusinessUnit = 0    
                        OR EM.BaseLocationID = @BusinessUnit )    
                 AND ( Isnull(EM.IsActive, '0') = @IsActive )    
                 AND EM.Code NOT LIKE 'TMP_%'    
                 AND EM.domainID = @DomainID    
                 AND ( Isnull(@DepartmentID, '') = ''    
                        OR DEP.ID = @DepartmentID )    
                 AND ( Isnull(@DesignationID, '') = ''    
                        OR DESI.ID = @DesignationID )    
                 AND ( Isnull(@FunctionalID, '') = ''    
                        OR FUN.ID = @FunctionalID )    
                 AND ( Isnull(@EmailID, '') = ''    
                        OR ( EM.EmailID LIKE '%' + @EmailID + '%' ) )    
                 AND ( Isnull(@EmploymentTypeID, '') = ''    
                        OR EM.EmploymentTypeID = @EmploymentTypeID )    
                 AND ( Isnull(@ReportingTo, '') = ''    
                        OR ( ( Isnull(REPORTINGTO.FirstName, '') + ' '    
                               + Isnull(REPORTINGTO.LastName, '') ) LIKE '%' + @ReportingTo + '%' ) )    
                 AND ( Isnull(@PAN, '') = ''    
                        OR ( STATUTORY.PAN_No LIKE '%' + @PAN + '%' ) )    
                 AND ( Isnull(@PFNo, '') = ''    
                        OR ( STATUTORY.PFNo LIKE '%' + @PFNo + '%' ) )    
                 AND ( Isnull(@EEESI, '') = ''    
                        OR ( STATUTORY.ESINo LIKE '%' + @EEESI + '%' ) )    
                 AND ( Isnull(@AadharID, '') = ''    
                        OR ( STATUTORY.AadharID LIKE '%' + @AadharID + '%' ) )    
                 AND ( Isnull(@UAN, '') = ''    
                        OR ( STATUTORY.UANNo LIKE '%' + @UAN + '%' ) )    
                 AND ( Isnull(@PolicyNo, '') = ''    
                        OR ( STATUTORY.PolicyNo LIKE '%' + @PolicyNo + '%' ) )    
        
          ORDER  BY EM.FirstName + ' ' + Isnull(EM.LastName, '')    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT;    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
