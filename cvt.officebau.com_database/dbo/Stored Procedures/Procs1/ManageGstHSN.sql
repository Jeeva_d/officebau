﻿/****************************************************************************     
CREATED BY   : Dhanalakshmi.S    
CREATED DATE  : 26 July 2018    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>     
  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[ManageGstHSN] (@ID             INT,  
                                      @Code           VARCHAR(100),  
                                      @GSTDescription VARCHAR(1000),  
                                      @CGST           DECIMAL(18, 2),  
                                      @SGST           DECIMAL(18, 2),  
                                      @IGST           DECIMAL(18, 2),  
                                      @SessionID      INT,  
                                      @IsDeleted      BIT,  
                                      @DomainID       INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(100) = 'Operation Failed!'  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          IF( @IsDeleted = 1 )  
            BEGIN  
                IF( ( (SELECT Count(1)  
                       FROM   tblProduct_V2  
                       WHERE  HSNId = @ID  
                              AND DomainID = @DomainID  
                              AND IsDeleted = 0) > 0 )  
                     OR ( (SELECT Count(1)  
                           FROM   tblLedger  
                           WHERE  GstHSNID = @ID  
                                  AND DomainID = @DomainID  
                                  AND IsDeleted = 0) > 0 ) )  
                  BEGIN  
                      SET @Output = 'The record is referred.'  
  
                      GOTO Finish  
                  END  
                ELSE  
                  BEGIN  
                      UPDATE tblGstHSN  
                      SET    IsDeleted = 1,  
                             ModifiedBY = @SessionID,  
                             ModifiedOn = Getdate()  
                      WHERE  ID = @ID  
                             AND DomainID = @DomainID  
  
                      SET @Output = 'Deleted Successfully.'  
  
                      GOTO Finish  
                  END  
            END  
          ELSE  
            BEGIN  
                IF( @ID = 0 )  
                  BEGIN  
                      IF NOT EXISTS(SELECT 1  
                                    FROM   tblGstHSN  
                                    WHERE  IsDeleted = 0  
                                           AND Code = @Code)  
                        BEGIN  
                            INSERT INTO tblGstHSN  
                                        (Code,  
                                         GSTDescription,  
                                         CGST,  
                                         SGST,  
                                         IGST,  
                                         ModifiedBy,  
                                         ModifiedOn,  
                                         DomainID)  
                            VALUES      (@Code,  
                                         @GSTDescription,  
                                         @CGST,  
                                         @SGST,  
                                         @IGST,  
                                         @SessionID,  
                                         Getdate(),  
                                         @DomainID)  
  
                            SET @Output = 'Inserted Successfully'  
                        END  
                      ELSE  
                        BEGIN  
                            SET @Output = 'Already HSN Code Exists.'  
                        END  
                  END  
                ELSE  
                  BEGIN  
                      IF NOT EXISTS(SELECT 1  
                                    FROM   tblGstHSN  
                                    WHERE  IsDeleted = 0  
                                           AND Code = @Code  
                        AND ID <> @ID)  
                        BEGIN  
                            UPDATE tblGstHSN  
                            SET    Code = @Code,  
                                   GSTDescription = @GSTDescription,  
                                   CGST = @CGST,  
                                   SGST = @SGST,  
                                   IGST = @IGST,  
                                   ModifiedBY = @SessionID,  
                                   ModifiedOn = Getdate()  
                            WHERE  ID = @ID  
  
                            SET @Output = 'Updated Successfully'  
                        END  
                      ELSE  
                        BEGIN  
                            SET @Output = 'Already HSN Code Exists.'  
                        END  
                  END  
            END  
  
          FINISH:  
  
          SELECT @Output  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
