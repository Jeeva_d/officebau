﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :        
MODIFIED BY   :           
MODIFIED DATE  :        ,   
 <summary> 
 [SearchPOBillDetailList] ',' ,1      
 </summary>                                   
 *****************************************************************************/        
Create PROCEDURE [dbo].[SearchPOBillDetailList] (@POID varchar(Max),        
                                                 @DomainID  INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          BEGIN TRANSACTION        
        
          SELECT ed.ID       AS ID,        
                 ed.LedgerID AS LedgerID,        
                 ed.Remarks  AS [Description],
				   QTY-BilledQTY AS QTY,IsLedger AS IsProduct,      
                 ed.Amount   AS Amount,0 CGST,Isnull(CGSTPercent,0) CGSTPercent,0 SGST,      
     Isnull(SGSTPercent,0) SGSTPercent        
          FROM   tblPODetails ed        
          WHERE  ed.IsDeleted = 0        
                 AND  ed.POID IN (Select * from dbo.FnSplitString(@POID,',')) And ed.DomainID=@DomainID        
        
          COMMIT TRANSACTION        
      END TRY        
      BEGIN CATCH        
          ROLLBACK TRANSACTION        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
