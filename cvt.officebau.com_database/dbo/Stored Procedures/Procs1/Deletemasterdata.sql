﻿/****************************************************************************     
CREATED BY   : Naneeshwar.M    
CREATED DATE  :   06-JUN-2017  
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>   
          [Deletemasterdata] 1,NULL,'SKIlls', 0,'SKIllsID', 1, 224, 1  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Deletemasterdata] (@ID               INT,  
                                          @DependantTable   VARCHAR(200),  
                                          @MainTable        VARCHAR(200),  
                                          @DependantTableID INT,  
                                          @COLUMNNAME       VARCHAR(100),  
                                          @IsDeleted        BIT,  
                                          @SessionID        INT,  
                                          @DomainID         INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          DECLARE @sql       VARCHAR(MAX),  
                  @TABLEQURY VARCHAR(MAX),  
                  @count     INT,  
                  @OUTPUT    VARCHAR(100)  
          DECLARE @Table TABLE  
            (  
               COUN INT  
            )  
  
          SET @MainTable = 'tbl_' + @MainTable  
  
          IF( Isnull(@DependantTable, '') = '' )  
            BEGIN  
                SET @DependantTable = @DependantTable + 'ID'  
            END  
  
          IF( Isnull(@DependantTable, '') = '' )  
            BEGIN  
                IF( @MainTable = 'tbl_Designation'  
                     OR @MainTable = 'tbl_Department' )  
                  SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE '  
                                           + @COLUMNNAME + ' = ' + CONVERT(VARCHAR(10), @ID)  
                                           + ' and IsDeleted = 0  and DomainID='  
                                           + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                    FROM   INFORMATION_SCHEMA.COLUMNS  
                                    WHERE  COLUMN_NAME = @COLUMNNAME  
                                           AND TABLE_NAME <> 'tbl_Company'  
                                    FOR XML PATH(''))  
                ELSE IF( @MainTable = 'tbl_BusinessUnit' )  
                  BEGIN  
                      SET @COLUMNNAME = 'ParentID'  
                      SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME  
                                               + ' WHERE ParentID LIKE ''%'  
                                               + CONVERT(VARCHAR(10), @ID)  
                                               + '%'' and IsDeleted = 0  and DomainID='  
                                               + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                        FROM   INFORMATION_SCHEMA.COLUMNS  
                                        WHERE  COLUMN_NAME = @COLUMNNAME  
                                               AND TABLE_NAME <> 'tbl_Company'  
                                               AND TABLE_NAME <> 'tbl_LeavePolicy'  
                                               AND TABLE_NAME <> 'tbl_PayrollConfiguration'  
                                        FOR XML PATH(''))  
                      SET @COLUMNNAME = 'RegionID'  
                      SET @TABLEQURY += (SELECT 'SELECT 1 FROM ' + TABLE_NAME  
                                                + ' WHERE RegionID ='  
                                                + CONVERT(VARCHAR(10), @ID)  
                                                + ' and IsDeleted = 0  and DomainID='  
                                                + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                         FROM   INFORMATION_SCHEMA.COLUMNS  
                                         WHERE  COLUMN_NAME = @COLUMNNAME  
                                                AND TABLE_NAME = 'tbl_EmployeeMaster'  
                                      FOR XML PATH(''))  
                  END  
                ELSE IF( @MainTable = 'tbl_EmployeeGrade' )  
                  SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME  
                                           + ' WHERE GradeID = '  
                                           + CONVERT(VARCHAR(10), @ID)  
                                           + ' and IsDeleted = 0  and DomainID='  
                                           + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                    FROM   INFORMATION_SCHEMA.COLUMNS  
                                    WHERE  COLUMN_NAME LIKE '%GradeID%'  
                                    FOR XML PATH(''))  
                ELSE IF( @MainTable = 'tbl_AssetType' )  
                  SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE '  
                                           + @COLUMNNAME + ' = ' + CONVERT(VARCHAR(10), @ID)  
                                           + ' and IsDeleted = 0  and DomainID='  
                                           + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                    FROM   INFORMATION_SCHEMA.COLUMNS  
                                    WHERE  COLUMN_NAME = @COLUMNNAME  
                                    FOR XML PATH(''))  
                ELSE IF( @MainTable = 'tbl_ExitReason' )  
                  BEGIN  
                      SET @COLUMNNAME = 'RelivingReason'  
                      SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE '  
                                               + @COLUMNNAME + ' = ' + CONVERT(VARCHAR(10), @ID)  
                                               + ' and IsDeleted = 0  and DomainID='  
                                               + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                        FROM   INFORMATION_SCHEMA.COLUMNS  
                                        WHERE  COLUMN_NAME = @COLUMNNAME  
                                        FOR XML PATH(''))  
                  END  
                ELSE  
                  SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE '  
                                           + @COLUMNNAME + ' = ' + CONVERT(VARCHAR(10), @ID)  
                                           + ' and IsDeleted = 0 and DomainID='  
                                           + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                    FROM   INFORMATION_SCHEMA.COLUMNS  
                                    WHERE  COLUMN_NAME LIKE '%' + @COLUMNNAME + '%'  
                                           AND TABLE_NAME <> 'tbl_Company'  
                                    FOR XML PATH(''))  
  print @TABLEQURY
                INSERT INTO @Table  
                EXEC(@TABLEQURY)  
  
                SET @count=(SELECT Count(*)  
                            FROM   @Table)  
  
                IF( @count = 0 )  
                  BEGIN  
                      SET @sql= ( 'UPDATE ' + @MainTable  
                                  + ' SET IsDeleted = 1  
                                   WHERE ID = '  
                                  + CONVERT(VARCHAR(10), @ID) + 'and DomainID='  
                                  + CONVERT(VARCHAR(10), @DomainID) )  
                      SET @OUTPUT='Deleted Successfully'  
                  END  
                ELSE  
                  SET @OUTPUT = 'Record referred'  
            END  
          ELSE  
            BEGIN  
                IF( @MainTable = 'tbl_Designation'  
                     OR @MainTable = 'tbl_Department' )  
                  SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE '  
                                           + @COLUMNNAME + ' = ' + CONVERT(VARCHAR(10), @ID)  
                                           + ' and IsDeleted = 0  and DomainID='  
                                           + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                    FROM   INFORMATION_SCHEMA.COLUMNS  
                  WHERE  COLUMN_NAME = @COLUMNNAME  
                                           AND TABLE_NAME <> 'tbl_Company'  
                                    FOR XML PATH(''))  
                ELSE IF( @MainTable = 'tbl_BusinessUnit' )  
                  BEGIN  
                      SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME  
                                               + ' WHERE BusinessUnitID LIKE ''%'  
                                               + CONVERT(VARCHAR(10), @ID)  
                                               + '%'' and IsDeleted = 0  and DomainID='  
                                               + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                        FROM   INFORMATION_SCHEMA.COLUMNS  
                                        WHERE  COLUMN_NAME = @COLUMNNAME  
                                               AND TABLE_NAME <> 'tbl_Company'  
                                               AND TABLE_NAME <> 'tbl_LeavePolicy'  
                                               AND TABLE_NAME <> 'tbl_PayrollConfiguration'  
                                        FOR XML PATH(''))  
                      SET @TABLEQURY += (SELECT 'SELECT 1 FROM ' + TABLE_NAME  
                                                + ' WHERE BaseLocationID ='  
                                                + CONVERT(VARCHAR(10), @ID)  
                                                + ' and IsDeleted = 0  and DomainID='  
                                                + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                         FROM   INFORMATION_SCHEMA.COLUMNS  
                                         WHERE  COLUMN_NAME = @COLUMNNAME  
                                                AND TABLE_NAME = 'tbl_EmployeeMaster'  
                                         FOR XML PATH(''))  
                  END  
                ELSE IF( @MainTable = 'tbl_EmployeeGrade' )  
                  SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME  
                                           + ' WHERE GradeID = '  
                                           + CONVERT(VARCHAR(10), @ID)  
                                           + ' and IsDeleted = 0  and DomainID='  
                                           + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                    FROM   INFORMATION_SCHEMA.COLUMNS  
                                    WHERE  COLUMN_NAME LIKE '%GradeID%'  
                                    FOR XML PATH(''))  
                ELSE IF( @MainTable = 'tbl_AssetType' )  
                  SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE '  
                                           + @COLUMNNAME + ' = ' + CONVERT(VARCHAR(10), @ID)  
                                           + ' and IsDeleted = 0  and DomainID='  
                                           + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                    FROM   INFORMATION_SCHEMA.COLUMNS  
                                    WHERE  COLUMN_NAME = @COLUMNNAME  
                                    FOR XML PATH(''))  
                ELSE IF( @MainTable = 'tbl_ExitReason' )  
                  BEGIN  
                      SET @COLUMNNAME = 'RelivingReason'  
                      SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE '  
                                               + @COLUMNNAME + ' = ' + CONVERT(VARCHAR(10), @ID)  
                                               + ' and IsDeleted = 0  and DomainID='  
                                               + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                        FROM   INFORMATION_SCHEMA.COLUMNS  
                                        WHERE  COLUMN_NAME = @COLUMNNAME  
                                        FOR XML PATH(''))  
                  END  
                ELSE  
                  SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE '  
    + @COLUMNNAME + ' = ' + CONVERT(VARCHAR(10), @ID)  
                                           + ' and IsDeleted = 0  and DomainID='  
                                           + CONVERT(VARCHAR(10), @DomainID) + '; '  
                                    FROM   INFORMATION_SCHEMA.COLUMNS  
                                    WHERE  COLUMN_NAME LIKE '%' + @COLUMNNAME + '%'  
                                           AND TABLE_NAME <> 'tbl_Company'  
                                    FOR XML PATH(''))  
  
                INSERT INTO @Table  
                EXEC(@TABLEQURY)  
  
                SET @count=(SELECT Count(*)  
                            FROM   @Table)  
  
                IF( @count = 0 )  
                  BEGIN  
                      SET @sql=( 'UPDATE ' + @MainTable  
                                 + ' SET IsDeleted = 1  
                                  WHERE ID = '  
                                 + CONVERT(VARCHAR(10), @ID) + 'and DomainID='  
                                 + CONVERT(VARCHAR(10), @DomainID) )  
                      SET @OUTPUT = 'Deleted Successfully'  
                  END  
                ELSE  
                  SET @OUTPUT = 'Record referred'  
            END  
  
          EXEC(@sql)  
  
          SELECT @OUTPUT  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
