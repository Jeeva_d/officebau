﻿/****************************************************************************       
CREATED BY   : Naneeshwar      
CREATED DATE  :   08/11/2016       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>       
        
 </summary>                               
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Managebank] (@ID             INT,    
                                    @BankName       VARCHAR(100),    
                                    @AccountNo      VARCHAR(1000),    
                                    @AccountTypeID  INT,    
                                    @OpeningBalance MONEY,    
                                    @OpeningBalDate DATETIME,    
                                    @Limit          MONEY,    
                                    @DisplayName    VARCHAR(100),    
                                    @Description    VARCHAR(1000),    
                                    @BranchName     VARCHAR(100),    
                                    @IFSCCode       VARCHAR(100),    
                                    @SWIFTCode      VARCHAR(100),    
                                    @MICRCode       VARCHAR(100),    
                                    @IsDeleted      BIT,    
                                    @SessionID      INT,    
                                    @DomainID       INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(1000)    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          IF( @IsDeleted = 1 )    
            BEGIN    
                IF EXISTS (SELECT 1    
                           FROM   tblExpensePayment    
                           WHERE  BankID = @ID    
                                  AND IsDeleted = 0    
                                  AND DomainID = @DomainID)    
                  BEGIN    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Warning'    
                                            AND Code = 'RCD_REF'    
                                            AND IsDeleted = 0) --'The record is referred.'      
    
                      GOTO finish    
                  END    
    
                IF EXISTS (SELECT 1    
                           FROM   tblInvoicePayment    
                           WHERE  BankID = @ID    
                                  AND IsDeleted = 0    
                                  AND DomainID = @DomainID)    
                  BEGIN    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Warning'    
                                            AND Code = 'RCD_REF'    
                                            AND IsDeleted = 0) --'The record is referred.'      
    
                      GOTO finish    
                  END    
    
                IF EXISTS (SELECT 1    
                           FROM   tblTransfer    
                           WHERE  FromID = @ID    
                                   OR ToID = @ID    
                                      AND IsDeleted = 0    
                                      AND DomainID = @DomainID)    
                  BEGIN    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Warning'    
                                            AND Code = 'RCD_REF'    
                                            AND IsDeleted = 0) --'The record is referred.'      
                      GOTO finish    
                  END    
    
                IF EXISTS (SELECT 1    
                           FROM  tbl_Receipts    
                           WHERE  BankID = @ID    
                                  AND IsDeleted = 0    
                                  AND DomainID = @DomainID)    
                  BEGIN    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                         WHERE  [Type] = 'Warning'    
                                            AND Code = 'RCD_REF'    
                                            AND IsDeleted = 0) --'The record is referred.'      
                      GOTO finish    
                  END    
                ELSE    
                  BEGIN    
                      UPDATE tblBank    
                      SET    IsDeleted = 1    
                      WHERE  @ID = ID    
                             AND DomainID = @DomainID    
    
                      SET @Output = (SELECT [Message]    
                                     FROM   tblErrorMessage    
                                     WHERE  [Type] = 'Information'    
                                            AND Code = 'RCD_DEL'    
                                            AND IsDeleted = 0) --'Deleted Successfully'      
                  END    
            END    
          ELSE    
            BEGIN    
                IF( @ID = 0 )    
                  BEGIN    
                      IF( (SELECT Count(1)    
                           FROM   tblBank    
                           WHERE  AccountNo = @AccountNo    
                                  AND DomainID = @DomainID    
                                  AND IsDeleted = 0) = 0 )    
                        BEGIN    
                            INSERT INTO tblBank    
                                        (NAME,    
                                         AccountNo,    
                                         AccountTypeID,    
                                         OpeningBalance,    
                                         OpeningBalDate,    
                                         Limit,    
                                         DisplayName,    
                                         [Description],    
                                         BranchName,    
                                         IFSCCode,    
                                         SWIFTCode,    
                                         MICRCode,    
                                         CreatedBy,    
                                         CreatedOn,    
                                         ModifiedBy,    
                                         ModifiedOn,    
                                         DomainID,    
                                         HistoryID,    
                                         IsDeleted)    
                            VALUES      (@BankName,    
                                         @AccountNo,    
                                         @AccountTypeID,    
                                         @OpeningBalance,    
                                         @OpeningBalDate,    
                                         @Limit,    
                                         @DisplayName,    
                                         @Description,    
                                         @BranchName,    
                                         @IFSCCode,    
                                         @SWIFTCode,    
                                         @MICRCode,    
                                         @SessionID,    
                                         Getdate(),    
                                         @SessionID,    
                                         Getdate(),    
                                         @DomainID,    
                                         Newid(),    
                                         0)    
    
    SET @Output = (SELECT [Message]    
                                           FROM   tblErrorMessage    
                                           WHERE  [Type] = 'Information'    
                                                  AND Code = 'RCD_INS'    
                                                  AND IsDeleted = 0) --'Inserted Successfully'      
                        END    
                      ELSE    
                        BEGIN    
                            SET @Output = (SELECT [Message]    
                                           FROM   tblErrorMessage    
                                           WHERE  [Type] = 'Warning'    
                                                  AND Code = 'RCD_EXIST'    
                                                  AND IsDeleted = 0) --'Already Exists'      
                        END    
                  END    
                ELSE    
                  BEGIN    
                      IF( (SELECT Count(1)    
                           FROM   tblBank    
                           WHERE  AccountNo = @AccountNo    
                                  AND DomainID = @DomainID    
                                  AND ID <> @ID    
                                  AND IsDeleted = 0) = 0 )    
                        BEGIN    
                            UPDATE tblBank    
                            SET    NAME = @BankName,    
                                   AccountNo = @AccountNo,    
                                   AccountTypeID = @AccountTypeID,    
                                   OpeningBalance = @OpeningBalance,    
                                   OpeningBalDate = @OpeningBalDate,    
                                   Limit = @Limit,    
                                   DisplayName = @DisplayName,    
                                   [Description] = @Description,    
                                   BranchName = @BranchName,    
                                   IFSCCode = @IFSCCode,    
                                   SWIFTCode = @SWIFTCode,    
                                   MICRCode = @MICRCode,    
                                   ModifiedBY = @SessionID,    
                                   ModifiedOn = GETDATE()    
                            WHERE  ID = @ID    
                                   AND DomainID = @DomainID    
    
                            SET @Output = (SELECT [Message]    
                                           FROM   tblErrorMessage    
                                           WHERE  [Type] = 'Information'    
                                                  AND Code = 'RCD_UPD'    
                                                  AND IsDeleted = 0) --'Updated Successfully'      
                        END    
                      ELSE    
                        BEGIN    
                            SET @Output = (SELECT [Message]    
                                           FROM   tblErrorMessage    
                                           WHERE  [Type] = 'Warning'    
                                                  AND Code = 'RCD_EXIST'    
                                                  AND IsDeleted = 0) --'Already Exists'      
                        END    
                  END    
            END    
    
          FINISH:    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
