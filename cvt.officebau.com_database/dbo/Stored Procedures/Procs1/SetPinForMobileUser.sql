﻿/****************************************************************************       
CREATED BY   :     
CREATED DATE  :     
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>              
 </summary>                               
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[SetPinForMobileUser] (@UserID   INT,      
                                        @DeviceInfo VARCHAR(Max),    
										@Pin int,      
                                        @DomainID INT,
										@OverwriteDevice varchar(10))      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
   Declare @Output Varchar(500) ='' 
   IF (@OverwriteDevice = 'Yes')
       BEGIN    
    Update tbl_Login set DeviceInfo=@DeviceInfo,PIN =@Pin    
     Where EmployeeID=@UserID    
      set @Output ='VALID';    
     Goto Finish;    
    END 
  IF Exists(Select 1 from tbl_login where DeviceInfo =@DeviceInfo and EmployeeID <>@UserID)    
    Begin     
        set @Output ='Device already registered with another user';    
     Goto Finish;    
    End    
    ELSE IF (Select DeviceInfo from tbl_login  Where EmployeeID=@UserID) IS not null    
    BEGIN    
     set @Output ='Already registered with PIN';    
     Goto Finish;    
    END    
  ELSE    
    BEGIN    
        Update tbl_Login set DeviceInfo=@DeviceInfo,PIN =@Pin    
     Where EmployeeID=@UserID    
      set @Output ='VALID';    
     Goto Finish;    
    END    
    
  FINISH:    
          SELECT @Output  AS UserMessage      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
