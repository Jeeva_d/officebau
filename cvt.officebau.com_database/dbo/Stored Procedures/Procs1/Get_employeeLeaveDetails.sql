﻿/****************************************************************************                         
CREATED BY    :                        
CREATED DATE  :                         
MODIFIED BY   :                       
MODIFIED DATE :                      
<summary>       
</summary>                                                 
*****************************************************************************/  
CREATE PROCEDURE [dbo].[Get_employeeLeaveDetails](@EmployeeID int,@Year int)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN  
             select e.Name,ep.TotalLeave,ep.AvailedLeave,ep.TotalLeave-ep.AvailedLeave As Balance from tbl_EmployeeAvailedLeave ep
				Join tbl_LeaveTypes e on ep.LeaveTypeID =e.ID
				Where ep.EmployeeID = @EmployeeID and ep.isdeleted =0 and year=@Year
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
