﻿/****************************************************************************   
CREATED BY	:	Ajith. N
CREATED DATE  :   19-JUN-2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          [DeleteCustomerContact] 56, 1,1 	  
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteCustomerContact] (@ID       INT,
                                               @UserID   INT,
                                               @DomainID INT)
AS
  BEGIN
      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @TABLEQURY VARCHAR(MAX),
                  @count     INT,
                  @OUTPUT    VARCHAR(100)

          DECLARE @Table TABLE
            (
               COUN INT
            )

          BEGIN
              SET @TABLEQURY = (SELECT 'SELECT 1 FROM ' + TABLE_NAME
                                       + ' WHERE ContactID= '
                                       + CONVERT(VARCHAR(10), @ID)
                                       + ' AND IsDeleted = 0  AND DomainID = '
                                       + CONVERT(VARCHAR(10), @DomainID) + '; '
                                FROM   INFORMATION_SCHEMA.COLUMNS
                                WHERE  COLUMN_NAME = 'ContactID'
                                FOR XML PATH(''))
								 
              INSERT INTO @Table
              EXEC(@TABLEQURY)

              SET @count = (SELECT COUNT(1)
                            FROM   @Table)

              IF( @count = 0 )
                BEGIN
                    UPDATE tbl_CustomerContact
                    SET    IsDeleted = 1,
                           ModifiedBy = @UserID,
                           ModifiedOn = GETDATE()
                    WHERE  ID = @ID
                           AND DomainID = @DomainID

                    SET @OUTPUT = 'Deleted Successfully'
                END
              ELSE
                SET @OUTPUT = 'Record referred'
          END

          SELECT @OUTPUT AS UserMessage

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
