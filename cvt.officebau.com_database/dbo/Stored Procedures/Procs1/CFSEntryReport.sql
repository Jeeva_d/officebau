﻿/**************************************************************************** 
CREATED BY    		:	Naneeshwar M
CREATED DATE		:	21-DEC-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
		 [Cfsentryreport] null,'03-dec-2017',NULL,null,2,1 
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CFSEntryReport] (@FromDate       DATE,
                                        @ToDate         DATE,
                                        @EmployeeID     INT,
                                        @BusinessUnitID INT,
                                        @Type           INT,
                                        @DomainID       INT)
AS
  BEGIN
      BEGIN TRY
          SET NOCOUNT ON;

          BEGIN TRANSACTION

          SELECT Cast(Row_number()
                        OVER (
                          ORDER BY cfe.ModifiedOn DESC) AS INT) AS Rowno,
                 CFE.ID                                         AS ID,
                 CFE.ContainerName                              AS ContainerName,
                 CFE.CreatedOn                                  AS InwardDate,
                 EMP.FirstName + ' ' + Isnull(EMP.LastName, '') AS SalesPerson,
                 CHANo                                          AS CHANo,
                 Size                                           AS Size,
                 ExporterName                                   AS ExporterName,
                 CASE
                   WHEN( InwardTypeID = 1 ) THEN 'Import'
                   ELSE 'Export'
                 END                                            AS InwardTypeName,
                 cfo.GateInDate                                 AS GateInDate,
                 cfo.BOEDate                                    AS BOEDate,
                 CFE.CreatedBy                                  AS CreatedBy
          INTO   #CFSTemp
          FROM   tbl_cfsEntry cfe
                 LEFT JOIN tbl_CFSOperation cfo
                        ON cfo.ContainerNo = cfe.ContainerName
                 LEFT JOIN tbl_employeemaster emp
                        ON emp.ID = cfe.CreatedBy
          WHERE  CFE.IsDeleted = 0

          IF ( @Type = 1 )
            BEGIN
                SELECT *
                FROM   #CFSTemp cfe
                WHERE  ( @EmployeeID IS NULL
                          OR cfe.CreatedBy = @EmployeeID )
                       AND ( ( @FromDate IS NULL
                               AND @ToDate IS NULL )
                              OR ( Cast( GateInDate  AS DATE) >= Isnull(@FromDate, '01-Jan-1990')
                                   AND Cast( GateInDate  AS DATE) <= Isnull(@ToDate, Getdate()) ) )
                       AND cfe.GateInDate IS NOT NULL
            END
          ELSE
            BEGIN
                SELECT *
                FROM   #CFSTemp cfe
                WHERE  ( @EmployeeID IS NULL
                          OR cfe.CreatedBy = @EmployeeID )
                       AND ( ( @FromDate IS NULL
                               AND @ToDate IS NULL )
                              OR ( Cast( BOEDate  AS DATE) >= Isnull(@FromDate, '01-Jan-1990')
                                   AND Cast( BOEDate  AS DATE) <= Isnull(@ToDate, Getdate()) ) )
                       AND cfe.BOEDate IS NOT NULL
            END

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
