﻿
/****************************************************************************   
CREATED BY		: K.SASIREKHA
CREATED DATE	: 20-10-2017 
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
	[Searchccrecpts] 0,1,106
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchccrecpts] (@BusinessUnitID INT,
                                         @DomainID       INT,
										  @UserID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

		            CREATE TABLE #tmpAccess  
            (  
               ID   INT IDENTITY(1, 1),  
               item INT  
            )  

		  IF( @BusinessUnitID <> 0 )  
            BEGIN  
                INSERT INTO #tmpAccess  
                SELECT @BusinessUnitID AS item  
            END  
          ELSE  
            BEGIN  
                INSERT INTO #tmpAccess  
                SELECT item  
                FROM   Splitstring((SELECT BusinessUnitID  
                                    FROM   tbl_EmployeeMaster  
                                    WHERE  ID = @UserID), ',')  
            END  

               SELECT EMR.ID             AS ID,
                 EMR.KeyID          AS eventID,
                 EMR.BusinessUnitID AS BusinessUnitID,
                 BUS.NAME           AS BusinessUnit,
                 MEK.NAME           AS eventName,
                 EMR.CCEmailID      AS CCrecpts
             FROM   tbl_EmailRecipients EMR
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = EMR.ModifiedBy
			     --AND EMR.BusinessUnitID=(SELECT  BusinessUnitID
        --                                           FROM   tbl_EmployeeMaster EM
        --                                           WHERE  EM.ID = @UserID
        --                                                  AND IsDeleted = 0)
			                 	--LEFT JOIN tbl_EmployeeMaster EM
     --                   ON EM.ID =  @SessionEmployeeID
	                   LEFT JOIN tbl_MailerEventKeys MEK
                        ON MEK.ID = EMR.KeyID
                 LEFT JOIN tbl_BusinessUnit BUS
                        ON BUS.ID = EMR.BusinessUnitID
          WHERE  EMR.IsDeleted = 0
                 AND EMR.DomainID = @DomainID
				 --OR EM.BusinessUnitID=EMR.BusinessUnitID
                 --AND (ISNULL(@BusinessUnitID, 0) = 0 OR EMR.BusinessUnitID = @BusinessUnitID  ) 
				AND EMR.BusinessUnitID in (select item from #tmpAccess)
			ORDER BY EMR.ModifiedOn DESC

   COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
