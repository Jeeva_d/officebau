﻿
/****************************************************************************   
CREATED BY    : Dhanalakshmi. S
CREATED DATE  : 28 June 2017
MODIFIED BY   : 
MODIFIED DATE : 
 <summary> 
 	[Deleteemployeepaystructure] 5,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DeleteEmployeepaystructure] (@ID       INT,
                                                    @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @OUTPUT VARCHAR(100)

          BEGIN
              IF EXISTS(SELECT 1
                        FROM   tbl_EmployeeApproval EA
                               LEFT JOIN tbl_EmployeePayStructure EPS
                                      ON EPS.Id = EA.EmployeePaystructureID
                        WHERE  EA.EmployeePaystructureID = @ID
                               AND EPS.isdeleted = 0
                               AND EPS.DomainId = @DomainID)
                BEGIN
                    SET @Output = 'Onboard Approval has been done for the Employee. So you cannot Delete'

                    GOTO finish
                END
              ELSE IF NOT EXISTS(SELECT 1
                            FROM   tbl_EmployeePayroll
                            WHERE  EmployeePayStructureID = @ID
                                   AND IsDeleted = 0)
                BEGIN
                    UPDATE tbl_EmployeePayStructure
                    SET    IsDeleted = 1
                    WHERE  @ID = ID
                           AND DomainId = @DomainID

                    SET @Output = 'Deleted Successfully'

                    GOTO finish
                END
              ELSE
                BEGIN
                    SET @Output = 'Payroll has been processed using the Paystructure. So you cannot Delete'

                    GOTO finish
                END
          END

          FINISH:

          SELECT @OUTPUT

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
