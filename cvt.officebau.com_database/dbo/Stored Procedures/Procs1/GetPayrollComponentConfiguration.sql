﻿/**************************************************************************** 
CREATED BY			:	Priya K
CREATED DATE		:	6-Mar-2018
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	 [GetPayrollComponentConfiguration] 1
 </summary>                         
 *****************************************************************************/
 
CREATE PROCEDURE [dbo].[GetPayrollComponentConfiguration] (@BusinessUnitID INT)
AS
  BEGIN
      SET NOCOUNT ON;

	  BEGIN TRY
	    SELECT ISNULL(pc.ID, 0)                                       AS ID,
				ISNULL(PCM.ID, 0)                                       AS ComponentID,
		       PCM.Code						                           AS PayrollCode,
			   PCM.[Description]				                       AS PayrollDescription,
			   PCM.ControlType										   AS PayrollControlType,
			   PC.ConfigValue                                          AS PayrollConfigValue,
			   PC.BusinessUnitID                                       AS BusinessUnitID,
			   em.FirstName + ' ' + Isnull(em.lastname, '')            AS ModifiedBy,
			   PC.ModifiedOn                                           AS ModifiedOn,
               BU.NAME                                                 AS BusinessUnit
			   INTO #temp
			   FROM tbl_PayrollComponentMaster pcm
				   LEFT JOIN tbl_PayrollComponentConfiguration pc
					 ON pcm.ID = pc.PayrollComponentID
					   AND pc.BusinessUnitID = @BusinessUnitID
				   LEFT JOIN tbl_EmployeeMaster em
					ON em.ID = pc.ModifiedBy
				   LEFT JOIN tbl_BusinessUnit bu
					ON bu.ID = pc.BusinessUnitID

			IF EXISTS (SELECT 1 
			           from tbl_PayrollComponentConfiguration 
					   where BusinessUnitID = @BusinessUnitID)
			     
			 BEGIN
			 
			 SELECT ID                      AS ID,
					ComponentID,
				   PayrollCode,
				   PayrollDescription,
				   PayrollControlType,
				   ISNULL(PayrollConfigValue, 0)        AS PayrollConfigValue,
				   @BusinessUnitID               AS BusinessUnitID,
				   ISNULL(ModifiedBy, '')           AS  ModifiedBy,
				   ISNULL(ModifiedOn, GetDate())    AS ModifiedOn,
				   ISNULL(BusinessUnit, '')        AS BusinessUnit
				   FROM #temp
				   Where BusinessUnitID = @BusinessUnitID
				   ORDER BY PayrollDescription ASC
			 END
			     
			 ELSE
			   
			   BEGIN
			    
			     SELECT  0               AS ID,
						ComponentID,
				        PayrollCode,
			            PayrollDescription,
			            PayrollControlType,
			                     0                 AS PayrollConfigValue,
			        @BusinessUnitID               AS BusinessUnitID,
			                     NULL             AS  ModifiedBy,
			                     NULL             AS ModifiedOn,
			                     NULL             AS BusinessUnit
			   FROM #temp
			   ORDER BY PayrollDescription ASC
			   END

	  END TRY
	  BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
END
