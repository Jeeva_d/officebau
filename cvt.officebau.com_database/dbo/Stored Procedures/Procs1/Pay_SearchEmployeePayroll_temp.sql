﻿/****************************************************************************                                
CREATED BY      :                                
CREATED DATE  :                                
MODIFIED BY   :                                
MODIFIED DATE  :                                
<summary>                                
        [Pay_SearchEmployeePayroll_temp] 8,6,1,'2',0,9                                
</summary>                                
*****************************************************************************/      
CREATE PROCEDURE [dbo].[Pay_SearchEmployeePayroll_temp] (@MonthId  INT,      
                                                   @Year     INT,      
                                                   @DomainID INT,      
                                                   @Location VARCHAR(100),      
                                                   @StatusID INT,      
                                                   @Proceed  INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      SET @Year = (SELECT NAME      
                   FROM   tbl_FinancialYear      
                   WHERE  ID = @Year      
                          AND IsDeleted = 0      
                          AND DomainID = @DomainID)      
      
      DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'      
        + CONVERT(VARCHAR(10), @Year)      
      DECLARE @DataSource TABLE      
        (      
           ID      INT IDENTITY (1, 1),      
           [Value] NVARCHAR(128)      
        )      
      
      INSERT INTO @DataSource      
                  ([value])      
      SELECT Item      
      FROM   dbo.Splitstring(@Location, ',')      
      WHERE  Isnull(Item, '') <> ''      
      
      DECLARE @Workdays INT = Datediff(DAY, @CurrentMonth, Dateadd(MONTH, 1, @CurrentMonth));      
      
      BEGIN TRY      
          BEGIN      
              DECLARE @t TABLE      
                (      
                   id          INT,      
                   code        VARCHAR(50),      
                   description VARCHAR(1000),      
                   ordinal     INT,      
                   componentid INT,      
                   value       VARCHAR(max),      
                   amount      DECIMAL(18, 2),      
                   iseditable  BIT,      
                   [Type]      VARCHAR(50)      
                )      
      
       --       SELECT ( effectivefrom ) AS EffectiveFrom,      
       --              employeeid,      
       --              E.id,      
       --              companypaystructureid      
       --       INTO   #maintable      
       --       FROM   tbl_pay_employeepaystructure E      
       --              JOIN tbl_EmployeeMaster EM      
       --                ON E.EmployeeID = EM.ID      
       --       WHERE
			    --effectivefrom <= Dateadd(d, -1, Dateadd(m, Datediff(m, 0, Dateadd(year, @Year - 2000, Dateadd( month, @MonthId - 1, '20000101')))      
       --                                                         + 1, 0))      
       --              AND E.effectivefrom = (SELECT Max(EffectiveFrom)      
       --                                     FROM   tbl_pay_employeepaystructure      
       --                                     WHERE  employeeID = E.Employeeid      
       --                                            AND isdeleted = 0      
       --                                            AND effectivefrom <= Dateadd(d, -1, Dateadd(m, Datediff(m, 0, Dateadd(year, @Year - 2000, Dateadd( month, @MonthId - 1, '20000101')))      
       --                                                                                           + 1, 0)))      
       --              AND E.Isdeleted = 0      
       --              AND E.DomainID = @DomainID      
       --              AND EM.Isdeleted = 0      
       --              AND Em.IsActive = 0        
    --GROUP  BY employeeid,                           
              --          id,                           
              --          companypaystructureid   
			      
			   
	   SELECT ( effectivefrom ) AS EffectiveFrom,      
                     employeeid,      
                     E.id,      
                     companypaystructureid
					 INTO   #maintable        
              FROM   tbl_pay_employeepaystructure E      
                     JOIN tbl_EmployeeMaster EM      
                       ON E.EmployeeID = EM.ID      
              WHERE
			    effectivefrom <=  Dateadd(d, -1, Dateadd(m, Datediff(m, 0, Dateadd(year, @Year - 2000, Dateadd( month, @MonthId - 1, '20000101')))      
                                                               , 0) + 1)      
                     AND E.effectivefrom = (SELECT Max(EffectiveFrom)      
                                            FROM   tbl_pay_employeepaystructure      
                                            WHERE  employeeID = E.Employeeid      
                                                   AND isdeleted = 0      
                                                   AND effectivefrom <= Dateadd(d, -1, Dateadd(m, Datediff(m, 0, Dateadd(year, @Year - 2000, Dateadd( month, @MonthId - 1, '20000101')))      
                                                                                                  , 0)+1))      
                     AND E.Isdeleted = 0      
                     AND E.DomainID = 1      
                     AND EM.Isdeleted = 0      
                     AND Em.IsActive = 0
					 Insert INTO   #maintable  
		SELECT ( effectivefrom ) AS EffectiveFrom,      
                     employeeid,      
                     E.id,      
                     companypaystructureid      
              FROM   tbl_pay_employeepaystructure E      
                     JOIN tbl_EmployeeMaster EM      
                       ON E.EmployeeID = EM.ID      
              WHERE
			    effectivefrom between  Dateadd(d, -1, Dateadd(m, Datediff(m, 0, Dateadd(year, @Year - 2000, Dateadd( month, @MonthId - 1, '20000101')))      
                                                               , 0) + 2)    and Dateadd(d, -1, Dateadd(m, Datediff(m, 0, Dateadd(year, @Year - 2000, Dateadd( month, @MonthId - 1, '20000101')))      
                                                               +1, 0))         
                     AND E.Isdeleted = 0      
                     AND E.DomainID = 1      
                     AND EM.Isdeleted = 0      
                     AND Em.IsActive = 0       

              DECLARE @ID INT      
              DECLARE @payroll PAYROLL      
              WHILE ( (SELECT Count(1)      
       FROM   #maintable) > 0 )      
                BEGIN      
                    SET @ID = (SELECT TOP 1 id      
                               FROM   #maintable)      
      
         DELETE @payroll      
    Declare @Employeecount int =( Select Count(*) from #maintable Where EmployeeID = ( Select EmployeeID from #maintable where Id =@ID))
                    INSERT INTO @payroll      
                    SELECT e.id,      
                           e.componentid,      
                           paystructureid,      
                           CASE      
                             WHEN ( p.description = 'Tax' ) THEN      
                               Isnull(tds.TDSAmount, 0)      / @Employeecount
                             WHEN ( p.code = 'LOANDED' ) THEN      
                               Isnull(Isnull(LA.Amount, 0) + ( CASE      
                                                                 WHEN Isnull(LA.InterestAmount, 0) = 0 THEN      
                                                                   0      
                                                                 ELSE      
                                                                   Isnull(LA.InterestAmount, 0)      
                                                               END ) + Isnull((SELECT ob.DueBalance      
                                                                               FROM   tbl_LoanAmortization ob      
                                                                               WHERE  ob.LoanRequestID = l.ID      
                                                                                      AND ob.DomainID = @DomainID      
                                                                                      AND Month(ob.DueDate) = Month(Dateadd(month, -1, @CurrentMonth))      
                                                                                      AND Year(ob.DueDate) = Year(Dateadd(month, -1, @CurrentMonth))), 0), 0)   /@Employeecount   
                             ELSE      
                               e.amount      
                           END AS Amount,      
                           m.EmployeeID      
                    FROM   tbl_pay_employeepaystructuredetails e      
                           JOIN #maintable m      
                             ON m.id = e.paystructureid      
                                AND m.id = @ID      
                           JOIN tbl_pay_payrollcompontents p      
                             ON p.id = e.componentid      
                           LEFT JOIN tbl_LoanRequest l      
                                  ON l.EmployeeID = m.EmployeeID      
                                     AND l.StatusID = (SELECT ID      
                                                       FROM   tbl_Status      
                                                       WHERE  Type = 'Loan'      
                                                              AND Code = 'Settled')      
                           LEFT JOIN tbl_LoanAmortization la      
                                  ON la.LoanRequestID = l.ID      
                                     AND Month(la.DueDate) = Month(@CurrentMonth)      
                                     AND Year(la.DueDate) = Year(@CurrentMonth)      
                                     AND la.StatusID = (SELECT ID      
                                                        FROM   tbl_Status      
                                                        WHERE  Type = 'Amortization'      
                                                               AND Code = 'Open')      
                           LEFT JOIN tbl_tds TDS      
                                  ON TDS.employeeid = m.employeeid  
                AND tds.monthid = @MonthId      
                                     AND tds.yearid = (SELECT id      
                                                       FROM   tbl_financialyear      
      WHERE  isdeleted = 0      
                                                              AND NAME = @YEAR      
                                                              AND domainid = @DomainID)      
                                     AND TDS.isdeleted = 0     
                    WHERE  e.isdeleted = 0      
      
                    DECLARE @ComID INT =(SELECT companypaystructureid      
                      FROM   #maintable      
                      WHERE  ID = @ID)      
                    DECLARE @Gross DECIMAL(18, 2) = Isnull((SELECT Amount      
                              FROM   @payroll      
                              WHERE  ComponentId = (SELECT ID      
                                                    FROM   tbl_Pay_PayrollCompontents      
                                                    WHERE  Description = 'Gross'      
                                                           AND DomainID = @DomainID      
                                                           AND IsDeleted = 0)), 0)      
                    DECLARE @LOP DECIMAL=(SELECT ( CASE      
                                 WHEN ( Datepart(MM, DOJ) = @MonthId      
                                        AND Datepart(YYYY, DOJ) = @Year ) THEN      
                                   ( @Workdays - ( Datepart(D, DOJ) - 1 ) - Isnull(LopDays, 0) )      
                                 ELSE      
                                   @Workdays - Isnull(LopDays, 0)      
                               END ) - ( CASE      
                                           WHEN ( Datepart(MM, InactiveFrom) = @MonthId      
                                                  AND Datepart(YYYY, InactiveFrom) = @Year ) THEN      
                                             ( ( @Workdays ) - Day(InactiveFrom) )      
                                           ELSE      
                                             0      
                                         END )      
                      FROM   tbl_employeemaster ep      
                             LEFT JOIN tbl_LopDetails P      
                                    ON P.EmployeeID = ep.ID      
                                       AND P.DomainID = @DomainID      
                                       AND P.MonthId = @MonthId      
                                       AND P.year = @Year      
                      WHERE  ep.ID = (SELECT EmployeeID      
                                      FROM   #maintable      
                                      WHERE  ID = @ID))      
      
                    INSERT INTO @t      
                    EXEC Pay_getemployeepaystructurecomponents_temp      
                      @ComID,      
                      @Gross,      
                      @payroll,      
                      @Workdays,      
                      @lop      
      
                    DELETE #maintable      
                    WHERE  id = @ID      
                END      
      
              SELECT tpeps.id AS EmployeePayStructure,      
                     tpeps.CompanyPayStructureID,      
                     tpeps.EmployeeID,      
                     tpepsd.Description,      
                     CASE      
                       WHEN ( tpepsd.Code = 'LOANDED' ) THEN      
                         Isnull(Isnull(LA.Amount, 0) + ( CASE      
                                                           WHEN Isnull(LA.InterestAmount, 0) = 0 THEN      
                                                             0      
                                                           ELSE      
                                                             Isnull(LA.InterestAmount, 0)  
       END ) + Isnull((SELECT ob.DueBalance      
                                                                         FROM   tbl_LoanAmortization ob      
                                                                         WHERE  ob.LoanRequestID = l.ID      
                                                          AND ob.DomainID = @DomainID      
                                                                                AND Month(ob.DueDate) = Month(Dateadd(month, -1, @CurrentMonth))      
                                                                                AND Year(ob.DueDate) = Year(Dateadd(month, -1, @CurrentMonth))), 0), 0)      
                       ELSE      
                         Cast(tpepsd.Amount AS DECIMAL(18, 2))      
                     END      value,      
                     tpeps.EffectiveFrom,      
                     tpeps.DomainID,      
                     tpepsd.IsEditable,      
                     0        AS IsProcessed,      
                     0        AS IsApproved ,Convert(varchar(max) ,'') As Remarks     
              INTO   #EmployeePay      
              FROM   @t tpepsd      
                     JOIN tbl_Pay_EmployeePayStructureDetails AS tpepsds      
                       ON tpepsds.ID = tpepsd.id      
                     JOIN tbl_Pay_EmployeePayStructure AS tpeps      
                       ON tpepsds.PayStructureId = tpeps.ID      
                     LEFT JOIN tbl_employeemaster em      
                            ON em.ID = tpeps.EmployeeID      
                     LEFT JOIN tbl_LoanRequest l      
                            ON l.EmployeeID = tpeps.EmployeeID      
                               AND l.StatusID = (SELECT ID      
                                                 FROM   tbl_Status      
                                                 WHERE  Isdeleted = 0      
                                                        AND Type = 'Loan'      
                                                        AND Code = 'Settled')      
                     LEFT JOIN tbl_LoanAmortization la      
                            ON la.LoanRequestID = l.ID      
                               AND Month(la.DueDate) = Month(@CurrentMonth)      
                               AND Year(la.DueDate) = Year(@CurrentMonth)      
                               AND la.StatusID = (SELECT ID      
                                                  FROM   tbl_Status      
                                                  WHERE  Isdeleted = 0      
                                                         AND Type = 'Amortization'      
                                                         AND Code = 'Open')      
                     LEFT JOIN tbl_LopDetails AS tld      
                            ON tld.EmployeeId = tpeps.EmployeeID      
                               AND tld.DomainId = tpeps.DomainId      
                               AND tld.MonthID = @MonthId      
                               AND tld.year = @Year      
              WHERE  tpeps.DomainID = @DomainID      
                     AND tpeps.IsDeleted = 0      
      
              DROP TABLE #maintable      
      
              DECLARE @col VARCHAR(1000)      
              DECLARE @sql NVARCHAR(MAX)      
              SELECT @col = COALESCE(@col + ', ', '')      
                            + Quotename(Description)      
              FROM   tbl_Pay_PayrollCompontents AS tppc      
              WHERE  tppc.IsDeleted = 0      
                     AND tppc.DomainID = @DomainID      
              ORDER  BY tppc.Ordinal ASC      
              SET @sql = 'SELECT * INTO ##PivotTable FROM (SELECT 0 As Id, EmployeePayStructure,Description,                                   
        EmployeeID,Value,EffectiveFrom, IsProcessed,IsApproved ,Remarks FROM #EmployeePay) src       
  PIVOT (Max(Value) FOR Description  IN ('     
                         + @col + ')) pvt'      
      
              EXECUTE Sp_executesql      
                @sql      
      
              SELECT Isnull(emp.EmpCodePattern, '') + emp.Code AS Code,      
                     emp.FirstName                             AS EmployeeName,      
                     Row_number()      
                       OVER (      
 PARTITION BY ep.EmployeeID      
                         ORDER BY EffectiveFrom DESC)          AS ORDERNUMBER,      
                     ( CASE      
                         WHEN ( Datepart(MM, DOJ) = @MonthId      
                                AND Datepart(YYYY, DOJ) = @Year ) THEN      
                           ( @Workdays - ( Datepart(D, DOJ) - 1 ) - Isnull(LopDays, 0) )      
                         ELSE      
                           @Workdays - Isnull(LopDays, 0)      
                       END ) - ( CASE      
                  WHEN ( Datepart(MM, InactiveFrom) = @MonthId      
                                          AND Datepart(YYYY, InactiveFrom) = @Year ) THEN      
                                     ( ( @Workdays ) - Day(InactiveFrom) )      
                                   ELSE      
                                     0      
                                 END )                         AS Workdays,      
                     ep.*,      
                     Isnull(ESD.AccountNo, '')                 AS [Account No],      
                     Isnull(ESD.BankName, '')             AS [Bank Name],      
                     Isnull(ESD.IFSCCode, '')                  AS [IFSC Code],      
                     ESD.BeneID                                AS [Beneficiary ID],      
                     CONVERT(VARCHAR(15), emp.DOJ, 106)        AS [Date Of Joining],      
                     Isnull(EDT.NAME, '')                      AS Department,      
                     Isnull(EDN.NAME, '')                      AS Designation     
              INTO   #ResultTable      
              FROM   ##PivotTable ep      
                     LEFT JOIN tbl_LopDetails P      
                            ON P.EmployeeID = ep.EmployeeID      
                               AND P.DomainID = @DomainID      
                               AND P.MonthId = @MonthId      
                               AND P.year = @Year      
                     LEFT JOIN tbl_EmployeeMaster emp      
                            ON ep.EmployeeID = emp.id      
                     LEFT JOIN tbl_BusinessUnit BU      
                            ON BU.id = emp.BaseLocationID      
                     LEFT JOIN tbl_EmployeeStatutoryDetails ESD      
                            ON ESD.EmployeeID = ep.EmployeeID      
                     LEFT JOIN tbl_Department EDT      
                            ON EDT.ID = emp.DepartmentID      
                     LEFT JOIN tbl_Designation EDN      
                            ON EDN.ID = emp.DesignationID      
              WHERE  EffectiveFrom <= Dateadd(D, -1, Dateadd(M, Datediff(M, 0, Dateadd(YEAR, @Year - 2000, Dateadd(MONTH, @MonthId - 1, '20000101')))      
                                                                + 1, 0))      
                     AND ep.EmployeeID NOT IN (SELECT EmployeeID      
                                               FROM   tbl_Pay_EmployeePayroll      
                                               WHERE  MonthId = @MonthId      
                                                      AND [YearID] = @Year      
                                                      AND IsDeleted = 0      
                                                      AND DomainID = @DomainID)      
                     AND ( Isnull(emp.IsActive, '') = 0      
                            OR ( Month(InactiveFrom) = @MonthId      
                                 AND Year(InactiveFrom) = @Year ) )      
    AND ( ( @Location = Cast(0 AS VARCHAR) )      
                            OR emp.BaseLocationID IN (SELECT [value]      
                                                      FROM   @DataSource) )      
                     AND ( emp.BaseLocationID <> 0      
                            OR emp.BaseLocationID <> '' )      
                     AND emp.HasAccess = 1      
                     AND emp.IsDeleted = 0      
                     AND emp.EmploymentTypeID = (SELECT id      
           FROM   tbl_EmployementType      
                                                 WHERE  NAME = 'Direct'      
                                                        AND DomainID = @DomainID);      
              SELECT tpep.id,      
                     tpep.EmployeePayStructureID,      
                     tppd.ComponentId,      
                     tppd.Amount,      
                     tppc.Description,      
                     tpep.EmployeeId,      
                     tpeps.EffectiveFrom,      
                     tpep.IsProcessed,      
                     tpep.IsApproved  ,    
					 tpep.Remarks
              INTO   #SavedPayroll      
      FROM   tbl_Pay_EmployeePayroll AS tpep      
                     LEFT JOIN tbl_Pay_EmployeePayrollDetails AS tppd      
                            ON tppd.PayrollId = tpep.id      
                     JOIN tbl_Pay_EmployeePayStructure AS tpeps      
                       ON tpeps.id = tpep.EmployeePayStructureID      
                     JOIN tbl_Pay_PayrollCompontents AS tppc      
                       ON tppc.id = tppd.ComponentID      
                          AND tppc.IsDeleted = 0      
                          AND tpep.DomainId = @DomainID      
              WHERE  tpep.MonthId = @MonthId      
                     AND tpep.YearId = @Year      
                     AND tpep.isdeleted = 0      
      
         DROP TABLE ##PivotTable      
      
              SET @sql = 'SELECT * INTO ##PivotTable FROM (SELECT ID, EmployeePayStructureID,Description,                                  
                          EmployeeID,Amount,EffectiveFrom,IsProcessed,IsApproved,Remarks FROM #SavedPayroll) src                                
        PIVOT (Max(Amount) FOR Description IN ('      
                         + @col + ')) pvt'      
      
              EXECUTE Sp_executesql      
                @sql      
      
              SELECT *      
              INTO   #Result      
              FROM   #ResultTable      
              --WHERE  ORDERNUMBER = 1      
      
              INSERT INTO #Result      
              SELECT Isnull(emp.EmpCodePattern, '') + emp.Code AS Code,      
                     emp.FirstName                             AS EmployeeName,      
                     0                                         AS ORDERNUMBER,      
                     ( CASE      
                         WHEN ( Datepart(MM, DOJ) = @MonthId      
                                AND Datepart(YYYY, DOJ) = @Year ) THEN      
                           ( @Workdays - ( Datepart(D, DOJ) - 1 ) - Isnull(LopDays, 0) )      
                         ELSE      
                           @Workdays - Isnull(LopDays, 0)      
                       END ) - ( CASE      
                                   WHEN ( Datepart(MM, InactiveFrom) = @MonthId      
                                          AND Datepart(YYYY, InactiveFrom) = @Year ) THEN      
                                     ( ( @Workdays ) - Day(InactiveFrom) )      
                                   ELSE      
                                     0      
                                 END )                         AS Workdays,      
                     ep.*,      
                     Isnull(ESD.AccountNo, '')                 AS [Account No],      
                     Isnull(ESD.BankName, '')                  AS [Bank Name],   
  Isnull(ESD.IFSCCode, '')                  AS [IFSC Code],      
                    ESD.BeneID                                AS [Beneficiary ID],      
                     CONVERT(VARCHAR(15), emp.DOJ, 106)        AS [Date Of Joining],      
                     Isnull(EDT.NAME, '')                      AS Department,      
                     Isnull(EDN.NAME, '')                      AS Designation   
              FROM   ##PivotTable ep      
                     LEFT JOIN tbl_LopDetails P      
                            ON P.EmployeeID = ep.EmployeeID      
                               AND P.DomainID = @DomainID      
                        AND P.MonthId = @MonthId      
                               AND P.year = @Year      
                     LEFT JOIN tbl_EmployeeMaster emp      
                            ON ep.EmployeeID = emp.id      
                     LEFT JOIN tbl_BusinessUnit BU      
                            ON BU.id = emp.BaseLocationID      
                     LEFT JOIN tbl_EmployeeStatutoryDetails ESD      
                            ON ESD.EmployeeID = ep.EmployeeID      
                     LEFT JOIN tbl_Department EDT      
                            ON EDT.ID = emp.DepartmentID      
                     LEFT JOIN tbl_Designation EDN      
                            ON EDN.ID = emp.DesignationID      
              WHERE        
                      ( ( @Location = Cast(0 AS VARCHAR) )      
                            OR emp.BaseLocationID IN (SELECT [value]      
                                                      FROM   @DataSource) )      
                     AND ( emp.BaseLocationID <> 0      
                            OR emp.BaseLocationID <> '' )      
                     AND emp.HasAccess = 1      
                     AND emp.IsDeleted = 0      
                     AND emp.EmploymentTypeID = (SELECT ID      
                                                 FROM   tbl_EmployementType      
                                                 WHERE  NAME = 'Direct'      
                                                        AND DomainID = @DomainID);      
      
              IF( @StatusID = 0 )      
                SELECT *      
                FROM   #Result AS r      
                WHERE  r.id = 0      
      
              IF( @StatusID = 1 )      
                SELECT *      
                FROM   #Result AS r      
                WHERE  r.id <> 0      
                       AND Isprocessed = 0      
                       AND IsApproved = 0      
      
              IF( @StatusID = 2 )      
                SELECT *      
                FROM   #Result AS r      
                WHERE  IsApproved = 1      
                       AND Isprocessed = 0      
      
              IF( @StatusID = 3 )      
                SELECT *      
                FROM   #Result AS r      
                WHERE  IsApproved = 1      
                       AND Isprocessed = 1      
      
              IF( @StatusID = 9 )      
                SELECT *      
                FROM   #Result      
      
              DROP TABLE ##PivotTable      
          END      
      END TRY      
      BEGIN CATCH      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR (@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END 


