﻿
/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  : 10-JULY-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>          
 [Getemployeepayrollhistory] 1,18  
 </summary>                            
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Getemployeepayrollhistory] (@DomainID   INT,  
                                                   @EmployeeID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
      
       SELECT BusinessUnitID,  
             Cast(EducationAllow AS BIT) AS IsEducationAllow,  
             Cast(OverTime AS BIT)       AS IsOverTime,  
             Cast(EEPF AS BIT)           AS IsEEPF,  
             Cast(EEESI AS BIT)          AS IsEEESI,  
             Cast(PT AS BIT)             AS IsPT,  
             Cast(TDS AS BIT)            AS IsTDS,  
             Cast(ERPF AS BIT)           AS IsERPF,  
             Cast(ERESI AS BIT)          AS IsERESI,  
             Cast(GRATUITY AS BIT)       AS IsGratuity,  
             Cast(PLI AS BIT)            AS IsPLI,  
             Cast(MEDICLAIM AS BIT)      AS IsMediclaim,  
             Cast(MOBILEDATACARD AS BIT) AS IsMobileDatacard,  
             Cast(OTHERPERKS AS BIT)     AS IsOtherPerks  
      INTO   #tmpParollConfigValue  
      FROM   (SELECT code,  
                     ISNULL(pcc.ConfigValue, 0) AS ConfigValue,  
                     BusinessUnitID  
              FROM   tbl_PayrollComponentConfiguration pcc  
                     LEFT JOIN tbl_PayrollComponentMaster pcm  
                            ON pcc.PayrollComponentID = pcm.ID) x  
             PIVOT( Max(ConfigValue)  
                  FOR code IN ( EducationAllow,  
                                OverTime,  
                                EEPF,  
                                EEESI,  
                                PT,  
                                TDS,  
                                ERPF,  
                                ERESI,  
                                GRATUITY,  
                                PLI,  
                                MEDICLAIM,  
                                MOBILEDATACARD,  
                                OTHERPERKS)) p  
                                
                                
          SELECT EMP.FullName                                         AS EmployeeName,  
                 EMP.Code                                              AS EmployeeCode,  
                 LEFT(Datename(month, Dateadd(month, MonthId, -1)), 3) AS [Month],  
                 Monthid                                               AS Monthid,  
                 YearId                                                AS YearId,  
                 GrossEarning                                          AS Gross,  
                 Basic                                                 AS Basic,  
                 HRA                                                   AS HRA,  
                 MedicalAllowance                                      AS MedicalAllowance,  
                 Conveyance                                            AS Conveyance,  
                 SplAllow                                              AS SplAllow,  
                 EducationAllow                                        AS EducationAllowance,  
                 MagazineAllow                                         AS PaperMagazine,  
                 MonsoonALlow                                          AS MonsoonAllow,  
                 OverTime                                              AS OverTime,  
                 EEPF                                                  AS EEPF,  
                 EEESI                                                 AS EEESI,  
                 ERPF                                                  AS ERPF,  
                ERESI                                                 AS ERESI,  
                 Gratuity                                              AS Gratuity,  
                 PLI                                                   AS PLI,  
                 PT                                                    AS PT,  
                 bonus                                                 AS Bonus,  
                 Mediclaim                                             AS Mediclaim,  
                 MobileDataCard                                        AS MobileDataCard,  
                 OtherEarning                                          AS [Other Earning],  
                 OtherPerks                                            AS OtherPerks,  
                 Loans                                                 AS Loans,  
                 TDS                                                   AS TDS,  
                 OtherDeduction                                        AS [Other Deductions],  
                 NetSalary                                             AS NetSalary,  
                 CTC                                                   AS CTC,
                 IsEducationAllow,  
                IsOverTime,  
                IsEEPF,  
                IsEEESI,  
                IsPT,  
                IsTDS,  
                IsERPF,  
                IsERESI,  
                IsGratuity,  
                IsPLI,  
                IsMediclaim,  
                IsMobileDatacard,  
                IsOtherPerks  
          FROM   tbl_EmployeePayroll e  
                 LEFT JOIN tbl_EmployeeMaster emp  
                        ON emp.ID = e.EmployeeId 
                 LEFT JOIN #tmpParollConfigValue tpc  
                         ON tpc.BusinessUnitID = e.BaselocationID 
          WHERE  IsProcessed = 1  
                 AND EmployeeId = @EmployeeID  
                 AND e.IsDeleted = 0  
          ORDER  BY  YearId DESC ,Monthid DESC
                      
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
