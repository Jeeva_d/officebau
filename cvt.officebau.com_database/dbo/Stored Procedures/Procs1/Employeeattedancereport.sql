﻿/****************************************************************************             
CREATED BY  : Anto          
CREATED DATE :  25-SEP-2017          
MODIFIED BY  : Ajith          
MODIFIED DATE : 07 Dec 2017          
 <summary>                    
          [Employeeattedancereport] 1,4,12,1           
          
  To be Rework based on Employee ID          
          
 </summary>                                     
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Employeeattedancereport] (@MonthID    INT,      
                                                 @Year       INT,      
                                                 @EmployeeID INT,      
                                                 @DomainID   INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY    
          DECLARE @DynamicColumn      VARCHAR(1000) = '',      
                  @DynamicHoursHeader VARCHAR(MAX) = '',      
                  @DynamicColumnalter VARCHAR(MAX) = '',      
                  @YearCode           VARCHAR(10),      
                  @BiometricCode      VARCHAR(20)      
      
          IF Object_id('tempdb..##TEMPCHECKIN') IS NOT NULL      
            BEGIN      
                DROP TABLE ##TEMPCHECKIN      
            END      
  
           
    SET @YearCode = (SELECT FinancialYear      
                           FROM   tbl_FinancialYear      
                           WHERE  ID = @Year      
                                  AND IsDeleted = 0)      
      
          --drop table #tempCurrentDays, ##TEMPCHECKIN,##tblTempAttedance,#NORMALCOUNT,#TempValue          
          DECLARE @FROMDATE DATETIME = Cast(CONVERT(VARCHAR(20), @MonthID) + '-1-'      
                 + CONVERT(VARCHAR(20), @YearCode) AS DATETIME)      
          DECLARE @ENDDATE DATETIME = Cast(CONVERT(VARCHAR(20), @MonthID) + '-'      
                 + CONVERT(VARCHAR(20), Day(dbo.Eomonth(@FROMDATE, 0)))      
                 + '-' + CONVERT(VARCHAR(20), @YearCode) AS DATETIME)      
          DECLARE @FULLDAY TIME,      
                  @HALFDAY TIME      
      
          SELECT @FULLDAY = CONVERT(TIME, Replace(FullDay, '.', ':')),      
                 @HALFDAY = CONVERT(TIME, Replace(HalfDay, '.', ':'))      
          FROM   tbl_BusinessHours BL      
                 JOIN tbl_EmployeeMaster EM      
                   ON EM.RegionID = BL.RegionID      
                      AND BL.IsDeleted = 0      
          WHERE  EM.ID = @EmployeeID;      
      
          WITH AllDays      
               AS (SELECT @FROMDATE               AS [Date],      
                          1                       AS [level],      
                          1                       AS ID,      
                          Datename(dw, @FROMDATE) AS [days]      
                   UNION ALL      
                   SELECT Dateadd(DAY, 1, [Date]),      
                          [level] + 1,      
                          ID + 1,      
                          Datename(DW, [Date]) AS [days]      
                   FROM   AllDays      
                   WHERE  [Date] < @ENDDATE)      
          SELECT AL.[Date],      
                 AL.ID,      
                 CASE      
                   WHEN HL.[Type] = 'Weekly' THEN      
                     'WO ' + LEFT( HL.[Day], 3) + '/' +      
                     + CONVERT(VARCHAR, [level])      
                   WHEN HL.[Type] = 'Yearly' THEN      
                     'H ' + LEFT( HL.[Day], 3) + '/' +      
                     + CONVERT(VARCHAR, [level])      
                   ELSE      
                     CONVERT(VARCHAR, [level]) + '/'      
                 END                     [level],      
                 --[level],          
                 [days],      
                 ROW_NUMBER()      
                   OVER(      
                     PARTITION BY AL.[Date]      
                     ORDER BY AL.[Date]) AS Rowno      
          INTO   #tempAllDays      
          FROM   AllDays AL      
                 LEFT JOIN tbl_Holidays HL      
                        ON CONVERT(DATE, HL.[Date]) = CONVERT(DATE, AL.[Date])      
                           AND HL.IsDeleted = 0      
                           AND HL.RegionID LIKE '%'      
                                                + (SELECT ',' + CONVERT(VARCHAR, RegionID) + ','      
                                           FROM   tbl_EmployeeMaster      
                                                   WHERE  ID = @EmployeeID)      
                                                + '%'      
          OPTION (MAXRECURSION 0)      
      
          SELECT *      
          INTO   #tempCurrentDays      
          FROM   #tempAllDays      
          WHERE  Rowno = 1      
      
          SELECT @DynamicColumn = Substring((SELECT ',[' + CONVERT(VARCHAR(100), level) + ']'      
                                             FROM   #tempCurrentDays      
                                             FOR XML PATH('')), 2, 200000)      
      
          SELECT @DynamicHoursHeader = Substring((SELECT (SELECT ',CASE WHEN (['      
                                                                 + CONVERT(NVARCHAR(100), T.level) + '] >= '''      
                                                                 + CONVERT(NVARCHAR(5), @FULLDAY, 108)      
                                                                 + ''') THEN ''P'' WHEN (['      
                                                                 + CONVERT(NVARCHAR(100), T.level) + '] >= '''      
                                                                 + CONVERT(NVARCHAR(5), @HALFDAY, 108)      
                                                                 + ''') AND (['      
                                                                 + CONVERT(NVARCHAR(100), T.level) + '] < '''      
                                                                 + CONVERT(NVARCHAR(5), @FULLDAY, 108)      
                                                                 + ''') THEN ''HD'' ELSE ''A'' END AS ['      
                                                                 + CONVERT(NVARCHAR(100), T.level) + ']'      
                                                          FROM   #tempCurrentDays T      
                                                          --LEFT JOIN tbl_Holidays H          
                                                          --       ON DAY(T.[Date]) = DAY(H.[Date])          
                                                          --          AND YEAR(H.[Date]) = @YearCode          
                                                          --          AND MONTH(H.[Date]) = @MonthID          
                                                          FOR XML PATH(''), type).value('.', 'varchar(max)')), 2, 200000)      
      
          SET @BiometricCode = (SELECT BiometricCode      
                                FROM   tbl_EmployeeMaster      
                                WHERE  ID = @EmployeeID)      
      
          SELECT A.EmployeeID,      
                 A.[LogDate]              AS PunchDate,      
                 A.IsPermission           AS IsPermission,      
                 A.IsManual               AS IsManual,      
                 CONVERT(TIME, punchTime) AS punchTime,      
                 CONVERT(TIME, Duration)  AS Duration      
          INTO   #AttendencePunchDetails      
          FROM   (SELECT TAT.EmployeeID,      
                         TAT.[LogDate],      
                         Dateadd(second, Datediff(SECOND, ISNULL(TAT.LeaveDuration, '00:00:00.000'), ISNULL(TAT.Duration, '00:00:00.000')), 0) AS Duration,      
                         0                                                                                                                     AS IsPermission,      
                         (CASE when EMP.ID IS NOT NULL then 1 ELSE 0 END)      AS IsManual,      
                         BL.punchTime                                                                                                          AS punchTime      
                  FROM   tbl_Attendance TAT      
  JOIN tbl_BiometricLogs BL      
                           ON BL.AttendanceId = TAT.ID      
                         LEFT JOIN tbl_EmployeeMissedPunches EMP      
                                ON EMP.IsDeleted = 0      
                                   AND EMP.EmployeeID = TAT.EmployeeID      
       AND emp.PunchDate = TAT.LogDate      
                                   AND EMP.PunchTime = BL.PunchTime      
                                   AND EMP.StatusID = (SELECT ID      
                                                       FROM   tbl_Status      
                                                       WHERE  IsDeleted = 0      
                                                              AND [Type] = 'Leave'      
                                                              AND Code = 'Approved')      
                  WHERE  Year(TAT.LogDate) = @YearCode      
                         AND Month(TAT.LogDate) = @MonthID      
                         AND TAT.EmployeeID = @EmployeeID      
                         AND TAT.LeaveDuration IS NULL) AS A      
          -- WHERE  A.Duration > 0      
          UNION ALL      
          ------ from Employee Leave      
          SELECT EL.EmployeeID,      
                 ELM.LeaveDate AS PunchDate,      
                 2             AS IsPermission,      
                 NULL          AS IsManual,      
                 NULL          AS punchTime,      
                 CASE      
                   WHEN ( (SELECT Count(1)      
                           FROM   tbl_BusinessHours BH      
                           WHERE  BH.IsDeleted = 0      
                                  AND BH.RegionID = ISNULL(EM.RegionID, 0)) = 1 ) THEN      
                     ( CASE      
                         WHEN ( ISNULL(EL.IsHalfDay, 'false') = 'true' ) THEN      
                           --(SELECT TOP 1 Dateadd(MINUTE, Cast(Rtrim(Ltrim(Datediff(MINUTE, 0, Substring(Cast(ISNULL(TA.Duration, '00:00:00') AS VARCHAR(20)), 1, 8)))) AS INT), CONVERT(TIME, Replace(H.HalfDay, '.', ':')))      
                           (SELECT TOP 1 CASE      
                                           WHEN TA.Duration IS NULL THEN      
                                             CONVERT(TIME, Replace(H.HalfDay, '.', ':'))      
                                           ELSE      
                                             Substring(Cast(ISNULL(TA.Duration, '00:00:00') AS VARCHAR(20)), 1, 8)      
                                         END      
                            FROM   tbl_BusinessHours H      
                            WHERE  H.IsDeleted = 0      
                                   AND H.RegionID = ISNULL(EM.RegionID, 0))      
                         ELSE      
                           (SELECT TOP 1 CONVERT(TIME, Replace(F.FullDay, '.', ':'))      
                            FROM   tbl_BusinessHours F      
                            WHERE  F.IsDeleted = 0      
                                   AND F.RegionID = ISNULL(EM.RegionID, 0))      
                       END )      
                   ELSE      
                     NULL      
                 END           AS Duration      
          FROM   tbl_EmployeeLeave EL      
                 LEFT JOIN tbl_EmployeeLeaveDateMapping ELM      
                        ON EL.ID = ELM.LeaveRequestID      
                 LEFT JOIN tbl_EmployeeMaster EM      
                        ON EM.ID = EL.EmployeeID      
                 LEFT JOIN tbl_Attendance TA      
                        ON ta.EmployeeID = EM.ID      
                           AND ELM.LeaveDate = TA.LogDate      
          -- AND TA.LeaveDuration IS NOT NULL      
          WHERE  Year(LeaveDate) = @YearCode      
                 AND Month(LeaveDate) = @MonthID      
 AND EM.BiometricCode = @BiometricCode      
                 AND EL.Duration IS NULL      
                 AND HRStatusID = (SELECT ID      
                                   FROM   tbl_Status      
                                   WHERE  IsDeleted = 0      
                                          AND [Type] = 'Leave'      
  AND Code = 'Approved')      
                 AND EL.IsDeleted = 0      
          UNION ALL      
          ------ from Employee Permission      
          SELECT EL.EmployeeID,      
                 ELM.LeaveDate AS PunchDate,      
                 1             AS IsPermission,      
                 NULL          AS IsManual,      
                 NULL          AS punchTime,      
                 TA.Duration   AS Duration      
          FROM   tbl_EmployeeLeave EL      
     LEFT JOIN tbl_EmployeeLeaveDateMapping ELM      
                        ON EL.ID = ELM.LeaveRequestID      
                 LEFT JOIN tbl_EmployeeMaster EM      
                        ON EM.ID = EL.EmployeeID      
                 JOIN tbl_Attendance TA      
                   ON ta.EmployeeID = EM.ID      
                      AND ELM.LeaveDate = TA.LogDate      
                      AND TA.LeaveDuration IS NOT NULL      
          WHERE  Year(LeaveDate) = @YearCode      
                 AND Month(LeaveDate) = @MonthID      
                 AND EM.BiometricCode = @BiometricCode      
                 AND EL.Duration IS NOT NULL      
                 AND HRStatusID = (SELECT ID      
                                   FROM   tbl_Status      
                                   WHERE  IsDeleted = 0      
                                          AND [Type] = 'Leave'      
                                          AND Code = 'Approved')      
                 AND EL.IsDeleted = 0      
          ORDER  BY punchTime      
      
          --select * from #AttendencePunchDetails   where PunchDate = '2018-11-15'      
          IF EXISTS(SELECT Count(1)      
                    FROM   #AttendencePunchDetails)      
            BEGIN      
                EXEC('          
     SELECT  *          
     INTO ##TEMPCHECKIN          
     FROM (          
      SELECT ROW_NUMBER() OVER(ORDER BY(select 1)) AS Row1          
      ,''PUNCH-1000'' AS Days, TC.level,CONVERT(varchar(5), ta.punchTime, 108) AS punchTime          
      FROM #tempCurrentDays TC            
      LEFT JOIN #AttendencePunchDetails TA ON CAST(TC.[Date] AS DATE) = TA.PunchDate AND  TA.EmployeeID = '+ @EmployeeID +'              
      --LEFT JOIN tbl_EmployeeMaster EM ON EM.ID = ISNULL(TA.EmployeeID, '+ @EmployeeID +')               
     ) as s          
     PIVOT          
     (          
      MIN(punchTime)          
      FOR [level] IN ('+ @DynamicColumn + ')          
     )AS pvt')      
                DECLARE @COUNT            INT = 1,      
                        @TOTALCOUNT       INT = (SELECT Count(LEVEL)      
                           FROM   #tempCurrentDays),      
                        @MAXCOUNT         INT= 0,      
                        @TempData         NVARCHAR(MAX) = '',      
                        @TempDurationData NVARCHAR(MAX) = '',      
                        @InsertData       NVARCHAR(MAX) = '',      
                        @ColName          VARCHAR(100)      
      
                SELECT *      
                INTO   #tblTempAttedance      
                FROM   ##TEMPCHECKIN      
      
                DELETE FROM #tblTempAttedance      
      
                IF EXISTS(SELECT 1      
                          FROM   #AttendencePunchDetails      
                          WHERE  EmployeeID = @EmployeeID      
                                 AND Year(PunchDate) = @YearCode      
                                 AND Month(PunchDate) = @MonthID)      
                  BEGIN      
                      CREATE TABLE #NORMALCOUNT      
                        (      
                           COUNT INT      
                        )      
      
                      CREATE TABLE #TempValue      
                        (      
                           Value    VARCHAR(100),      
                           MaxValue VARCHAR(100)      
                        )      
      
                      WHILE( @COUNT <= @TOTALCOUNT )      
                        BEGIN      
                            DELETE FROM #NORMALCOUNT      
      
                            SET @ColName = (SELECT [LEVEL]      
                                            FROM   #tempCurrentDays      
                             WHERE  ID = @COUNT)      
      
                            INSERT INTO #NORMALCOUNT      
                            EXEC('SELECT  COUNT(['+ @ColName + ']) FROM ##TEMPCHECKIN WHERE ['+ @ColName + '] IS NOT NULL')      
      
                            IF( @MAXCOUNT < (SELECT [COUNT]      
                                             FROM   #NORMALCOUNT) )      
                              BEGIN      
                                  SET @MAXCOUNT = (SELECT *      
                                                   FROM   #NORMALCOUNT)      
                              END      
      
                      TRUNCATE TABLE #TempValue      
      
                            INSERT INTO #TempValue      
                                        (Value)      
                            EXEC('SELECT  ['+ @ColName + '] FROM ##TEMPCHECKIN WHERE ['+ @ColName + '] IS NOT NULL')      
      
                            SELECT @TempData = @TempData + ' '      
                                               + 'UPDATE #tblTempAttedance SET ['      
                                               + CONVERT(VARCHAR(100), @ColName) + '] = '''      
                                               + CONVERT(VARCHAR(100), VALUE)      
                                               + '''WHERE row1 = '      
                                               + CONVERT(VARCHAR(10), Row_number() OVER(ORDER BY(SELECT 1)) )      
                                               + ';'      
                            FROM   #TempValue      
      
                            TRUNCATE TABLE #TempValue      
      
                            INSERT INTO #TempValue      
                                        (MaxValue,      
                                         Value)      
                            EXEC('SELECT  MAX(['+ @ColName + ']) AS Value , MIN(['+ @ColName + ']) AS MaxValue FROM ##TEMPCHECKIN')      
      
                            SET @DynamicColumnalter = @DynamicColumnalter      
                                                      + ' ALTER TABLE #tblTempAttedance ALTER COLUMN ['      
                                                      + CONVERT(VARCHAR(100), @ColName)      
                                                      + '] VARCHAR(100);'      
      
                            IF EXISTS(SELECT Count(DISTINCT PunchDate)      
                                      FROM   #AttendencePunchDetails      
                                      WHERE  Day(PunchDate) = @COUNT)      
                              BEGIN      
                                  SELECT @TempDurationData = @TempDurationData      
                                                             + 'update #tblTempAttedance set ['      
                                                             + CONVERT(VARCHAR(100), @ColName)      
                                                             + ']= (select top 1 CONVERT(VARCHAR(5), duration) + ''/'' + CAST(ISNULL(IsManual, 0) AS VARCHAR) + 
															 ''/'' + CAST(ISNULL(IsPermission, 0) AS VARCHAR) from #AttendencePunchDetails where DAY(PunchDate) = '      
                                                             + CONVERT(VARCHAR(100), @COUNT)      
                                                             + ' ORDER BY IsPermission DESC,IsManual DESC) WHERE row1 = 0;'      
                              END      
      
                            SET @COUNT = @COUNT + 1      
                        END;      
      
                      WITH AllDays      
                           AS (SELECT 1 AS [level]      
                               UNION ALL      
                               SELECT [level] + 1      
                               FROM   AllDays      
                               WHERE  [level] < @MAXCOUNT)      
                      SELECT @InsertData = @InsertData + ' '      
                                           + 'insert into  #tblTempAttedance (row1,[Days]) values ('      
                                           + CONVERT(VARCHAR(10), [level] ) + ','''');'      
                      FROM   AllDays      
                      OPTION (MAXRECURSION 0)      
      
                      EXEC(@InsertData)      
      
                      EXEC(@TempData)      
      
                      EXEC('UPDATE #tblTempAttedance SET [Days] = ''Punch-'' + CONVERT(VARCHAR(10), ISNULL(ROW1, 0))')      
      
                      EXEC('insert into  #tblTempAttedance (row1,[Days]) values(0,''Total Hrs'')')      
      
                      EXEC(@DynamicColumnalter)      
      
                      EXEC ( @TempDurationData )      
      
                      EXEC('SELECT * FROM #tblTempAttedance UNION ALL SELECT ''-1'' as row1 , ''Status'',' + @DynamicHoursHeader +' 
					   FROM #tblTempAttedance where row1 = 0  ORDER BY row1 ASC')      
                  END      
                ELSE      
                  BEGIN      
                SELECT *      
    FROM   #tblTempAttedance     
       Exec UpdateBiometricWorkedHours 
    
                  END      
            END      
      END TRY      
      BEGIN CATCH      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
