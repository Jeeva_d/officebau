﻿/****************************************************************************                   
CREATED BY   :                 
CREATED DATE  :                   
MODIFIED BY   :     Ajith N              
MODIFIED DATE  :    12 May 2017               
 <summary>                
 [Rpt_IncomeCustomer] 2017,1,1                
 [Rpt_IncomeCustomer] '',' ',1              
[Rpt_BankDetails] null,null, 3          
 </summary>                                           
 *****************************************************************************/              
CREATE PROCEDURE [dbo].[Rpt_BankDetails] (@StartDate DATETIME=null ,              
                                            @EndDate   DATETIME=null,              
                                            @DomainID  INT )              
AS              
  BEGIN              
      SET NOCOUNT ON;              
              
      BEGIN TRY              
          BEGIN TRANSACTION              
              
         Declare @ID int,@BankName Varchar(200)      
  select ID , Name INTO #tempBank From tblBank where DomainID=@DomainID and isdeleted=0      
  Select * from #tempBank      
While((Select Count(*) from #tempBank)>0)      
Begin       
   Select Top 1 @ID=ID,@BankName=Name from #tempBank      
   Delete #tempBank where ID =@ID      
   SELECT BRSDescription Description,cast(Amount as numeric(10,2)) As Amount,ReconsiledDate [Reconciled date],t1.Name Type,C1.Code PaymentMode      
   ,Em.FirstName CreatedBy, b.CreatedOn ,EM1.FirstName AS ModifiedBy ,b.ModifiedOn      
    FROM tblBRS B      
   Left  Join tblExpensePayment E on E.ID = b.SourceID        
   Left Join tblMasterTypes t1 on t1.ID =b.SourceType       
   Left Join tbl_CodeMaster C1 on c1.ID=E.PaymentModeID      
    Join tbl_EmployeeMaster em on em.ID =b.CreatedBy      
   Join tbl_EmployeeMaster em1 on em1.ID =b.ModifiedBy      
   where b.IsDeleted=0 and b.DomainID=@DomainID  and b.BankID=@ID and t1.Name IN ('Expense','PayBill')      
    AND  ( @StartDate IS NULL        
    OR ReconsiledDate >= @StartDate )        
    AND ( @EndDate IS NULL        
    OR ReconsiledDate <= @EndDate )        
   Union All      
   SELECT BRSDescription,cast(b.Amount as numeric(10,2)),ReconsiledDate,t1.Name,C1.Code      
     ,Em.FirstName CreatedBy, b.CreatedOn ,EM1.FirstName AS ModifiedBy ,b.ModifiedOn      
      
    FROM tblBRS B      
   Left  Join tblInvoicePayment E on E.ID = b.SourceID        
   Left Join tblMasterTypes t1 on t1.ID =b.SourceType       
   Left Join tbl_CodeMaster C1 on c1.ID=E.PaymentModeID      
    Left Join tbl_EmployeeMaster em on em.ID =b.CreatedBy      
   Left Join tbl_EmployeeMaster em1 on em1.ID =b.ModifiedBy      
   where b.IsDeleted=0 and b.DomainID=@DomainID  and b.BankID=@ID and t1.Name IN ('InvoiceReceivable')      
    AND  ( @StartDate IS NULL        
    OR ReconsiledDate >= @StartDate )        
    AND ( @EndDate IS NULL        
    OR ReconsiledDate <= @EndDate )       
    Union All      
   SELECT BRSDescription,cast(b.Amount as numeric(10,2)),ReconsiledDate,t1.Name,C1.Code      
      ,Em.FirstName CreatedBy, b.CreatedOn ,EM1.FirstName AS ModifiedBy ,b.ModifiedOn      
      
    FROM tblBRS B      
   Left  Join tblReceipts E on E.ID = b.SourceID        
   Left Join tblMasterTypes t1 on t1.ID =b.SourceType       
   Left Join tbl_CodeMaster C1 on c1.ID=E.PaymentMode      
    Left Join tbl_EmployeeMaster em on em.ID =b.CreatedBy      
Left Join tbl_EmployeeMaster em1 on em1.ID =b.ModifiedBy      
   where b.IsDeleted=0 and b.DomainID=@DomainID  and b.BankID=@ID and t1.Name IN ('Receive Payment',      
   'Make Payment')      
    AND  ( @StartDate IS NULL        
    OR ReconsiledDate >= @StartDate )        
    AND ( @EndDate IS NULL        
    OR ReconsiledDate <= @EndDate )      
     Union All      
   SELECT BRSDescription,cast(b.Amount as numeric(10,2)),ReconsiledDate,t1.Name,''      
     ,Em.FirstName CreatedBy, b.CreatedOn ,EM1.FirstName AS ModifiedBy ,b.ModifiedOn      
      
    FROM tblBRS B      
   Left  Join tblTransfer E on E.ID = b.SourceID        
   Left Join tblMasterTypes t1 on t1.ID =b.SourceType       
   Left Join tbl_EmployeeMaster em on em.ID =b.CreatedBy      
   Left Join tbl_EmployeeMaster em1 on em1.ID =b.ModifiedBy      
   where b.IsDeleted=0 and b.DomainID=@DomainID  and b.BankID=@ID and t1.Name IN ('FromBank',      
   'ToBank')      
    AND  ( @StartDate IS NULL        
    OR ReconsiledDate >= @StartDate )        
    AND ( @EndDate IS NULL        
    OR ReconsiledDate <= @EndDate )      
End      
      
      
Drop table #tempBank      
          COMMIT TRANSACTION              
      END TRY              
      BEGIN CATCH              
          ROLLBACK TRANSACTION              
          DECLARE @ErrorMsg    VARCHAR(100),              
                  @ErrSeverity TINYINT              
          SELECT @ErrorMsg = Error_message(),              
                 @ErrSeverity = Error_severity()              
          RAISERROR(@ErrorMsg,@ErrSeverity,1)              
      END CATCH              
  END
