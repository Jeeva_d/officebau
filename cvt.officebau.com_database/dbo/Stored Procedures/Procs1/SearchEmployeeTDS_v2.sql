﻿/****************************************************************************       
CREATED BY  : JENNIFER.S    
CREATED DATE : 03-Jul-2017    
MODIFIED BY  : Naneeshwar.M    
MODIFIED DATE : 22-SEP-2017    
<summary>    
 [SearchEmployeeTDS] 4 ,4,'4',2     
</summary>                               
*****************************************************************************/    
Create PROCEDURE [dbo].[SearchEmployeeTDS_v2] (@MonthID      TINYINT,    
                                           @Year         INT,    
                                           @BusinessUnit VARCHAR(1000),    
                                           @DomainID     INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SET @Year =(SELECT NAME    
                      FROM   tbl_FinancialYear    
                      WHERE  DomainID = @DomainID    
                             AND id = @Year    
                             AND IsDeleted = 0)    
    
          DECLARE @PayFY INT    
    
          IF( @MonthID BETWEEN 1 AND 3 )    
            SET @PayFY=( (SELECT FY.NAME    
                          FROM   tbl_FinancialYear FY    
                          WHERE  FY.NAME = @Year    
                                 AND FY.IsDeleted = 0    
                                 AND FY.DomainID = @DomainID)    
                         + 1 )    
          ELSE    
            SET @PayFY =(SELECT FY.NAME    
                         FROM   tbl_FinancialYear FY    
                         WHERE  FY.NAME = @Year    
                                AND FY.IsDeleted = 0    
                                AND FY.DomainID = @DomainID)    
    
          DECLARE @Date           DATE = (SELECT Dateadd(day, -1, Dateadd(month, @MonthID, Dateadd(year, (SELECT NAME    
                                                                                     FROM   tbl_FinancialYear    
                                                                                     WHERE  NAME = @PayFY    
                                                                                            AND IsDeleted = 0    
                                                                                            AND DomainID = @DomainID) - 1900, 0)))),    
                  @PayrollMonthID INT,    
                  @ParollYearId   INT,    
                  @count          INT =1,    
                  @RunTDS         INT    
          DECLARE @DataSource TABLE    
            (    
               ID      INT IDENTITY(1, 1),    
               [Value] NVARCHAR(128)    
            )    
    
          INSERT INTO @DataSource    
                      ([Value])    
          SELECT Item    
          FROM   dbo.Splitstring (@BusinessUnit, ',')    
          WHERE  Isnull(Item, '') <> ''    
    
          DECLARE @TDSTable TABLE    
            (    
               ID             INT,    
               RunTDS         INT,    
               BaseLocationID INT    
            )    
    
          WHILE ( @count <= (SELECT Count(*)    
                             FROM   @DataSource) )    
            BEGIN    
                SELECT TOP 1 @ParollYearId = YearId,    
                             @PayrollMonthID = MonthId    
                FROM   tbl_pay_EmployeePayroll ep
				Join tbl_EmployeeMaster e on ep.EmployeeId = e.ID   
                WHERE  IsProcessed = 1    
                       AND ep.IsDeleted = 0    
                       AND BaseLocationID = (SELECT Value    
                                             FROM   @DataSource    
                                             WHERE  ID = @count)    
                       AND ep.DomainID = @DomainID    
                ORDER  BY YearId DESC,    
                          MonthId DESC    
    
                SET @ParollYearId = (SELECT NAME    
                                     FROM   tbl_FinancialYear    
                                     WHERE  FinancialYear = @ParollYearId    
                                            AND IsDeleted = 0    
                                            AND DomainID = @DomainID)    
                SET @RunTDS= (SELECT CASE    
                                       WHEN ( @MonthID = 1 ) THEN    
                                         CASE                                             WHEN ( 12 = @PayrollMonthID    
                                                  AND @ParollYearId = @Year ) THEN    
                                             1    
                                           WHEN ( 12 = @PayrollMonthID    
                                                  AND @ParollYearId = @Year - 1 ) THEN    
                                             1--if passed month is jan i.e. 1 then it will check the last processed month in payroll and year.     
                                           ELSE    
                                             0    
                                         END    
                                       WHEN ( @MonthID = 4 ) THEN    
                                         1--if the passed month is April then system will take as not processed.     
                                       ELSE    
                                         CASE    
                                           WHEN ( @MonthID - 1 = @PayrollMonthID    
                                                  AND @ParollYearId = @Year ) THEN    
                                             1--if the passed month is other than april then last processed month and year is check in payroll.    
                                           WHEN ( @MonthID - 1 = @PayrollMonthID    
                                                  AND @ParollYearId - 1 = @Year ) THEN    
                                             1    
                                           ELSE    
                                             0    
                                         END    
                                     END)    
    
                INSERT INTO @TDSTable    
                            (ID,    
                             RunTDS,    
                             BaseLocationID)    
                VALUES      (@count,    
                             @RunTDS,    
                             (SELECT Value    
                              FROM   @DataSource    
                              WHERE  ID = @count))    
    
                SET @count=@count + 1    
            END    
    
          SELECT DISTINCT EMP.ID                            EmployeeID,    
                          dp.NAME                           DepartmentName,    
                          ISNULL(EMP.EmpCodePattern, '') +  EMP.Code + ' - ' + EMP.FullName  EmployeeName,    
                          BUU.NAME                          BusinessUnit,    
                          EMP.DOJ,    
                          (SELECT Count(1)    
                           FROM   tbl_pay_EmployeePayroll    
                           WHERE  employeeid = EMP.ID    
                                  AND monthID = @MonthID    
                                  AND YearId = @Year    
                                  AND IsProcessed = 1    
                                  AND IsDeleted = 0    
                                  AND DomainID = @DomainID) AS Counts,    
                          ts.TDSAmount                      AS TDS,    
                          Isnull(pan.PAN_No, '')            AS PANNO,    
                          @RunTDS                           AS RunTDS,    
                          @Year                             AS YearName    
          FROM   tbl_EmployeeMaster EMP    
                 JOIN tbl_BusinessUnit BUU    
                   ON EMP.BaseLocationID = BUU.ID    
                 LEFT JOIN tbl_TDS ts    
                        ON emp.ID = ts.employeeid    
                           AND ts.MonthID = @MonthID    
                           AND YearId = (SELECT ID    
                                         FROM   tbl_FinancialYear    
                                         WHERE  NAME = @Year    
   AND DomainId = @DomainID    
                                                AND IsDeleted = 0)    
                           AND ts.IsDeleted = 0    
                           AND ts.BaseLocationID = emp.BaseLocationID    
                 LEFT JOIN tbl_Department dp    
                        ON dp.ID = EMP.DepartmentID    
                 LEFT JOIN tbl_EmployeeStatutoryDetails pan    
                        ON EMP.ID = pan.Employeeid    
                 LEFT JOIN @TDSTable runtds    
                        ON runtds.BaseLocationID = BUU.ID    
                 JOIN tbl_pay_EmployeePayStructure eps    
                   ON eps.EmployeeId = emp.ID    
          WHERE  EMP.DomainID = @DomainID    
                 AND EMP.IsDeleted = 0    
                 AND ( Isnull(EMP.IsActive, 0) = 0    
                        OR ( Isnull(EMP.IsActive, 0) = 1    
                             AND EMP.InactiveFrom >= Getdate() ) )    
                 AND EMP.DOJ < = @Date    
                 AND ( Isnull(@BusinessUnit, '') <> ''    
                       AND EMP.BaseLocationID IN (SELECT Item    
                                                  FROM   dbo.Splitstring (@BusinessUnit, ',')    
                                                  WHERE  Isnull(Item, '') <> '') )    
    
          -- ORDER  BY EMP.Code + ' (' + EMP.FirstName + ')'    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
