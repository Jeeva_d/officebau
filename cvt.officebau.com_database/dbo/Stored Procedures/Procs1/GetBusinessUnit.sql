﻿/****************************************************************************   
CREATED BY		: K.SASIREKHA 
CREATED DATE	: 04-10-2017 
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
 [GetBusinessUnit] 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetBusinessUnit] (@ID       INT,
                                          @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT BUS.ID,
                 BUS.ParentID,
                 BUS.NAME,
                 BUS.Remarks,
                 BUS.Address,
                 BUS.ModifiedOn,
                 EMP.FullName AS ModifiedBy,
                 BU.NAME      AS BaseLocation
          FROM   tbl_BusinessUnit BUS
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = BUS.ModifiedBy
                 LEFT JOIN tbl_BusinessUnit BU
                        ON EMP.BaseLocationID = BU.ID
          WHERE  BUS.IsDeleted = 0
                 AND BUS.ID = @ID
                 AND BUS.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
