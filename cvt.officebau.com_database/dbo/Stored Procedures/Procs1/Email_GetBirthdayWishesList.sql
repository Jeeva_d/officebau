﻿

/****************************************************************************     
CREATED BY  :  Naneeshwar.M  
CREATED DATE :  20-SEP-2017  
MODIFIED BY  :  Jennifer.S  
MODIFIED DATE :  16-OCT-2017  
 <summary>   
          Email_GetBirthdayWishesList  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Email_GetBirthdayWishesList]
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN
              SELECT *
              FROM   (SELECT emp.ID,
                             emp.EmailID                                                                                                                                                                                                                       
                                                                                  AS EmailID,
                             emp.FirstName + ' ' + Isnull(emp.LastName, '')                                                                                                                                                                                    
                                                                                  AS EmployeeName,
                             com.FullName                                                                                                                                                                                                                      
                                                                                  AS CompanyName,
                             ISNULL(ER.CCEmailID,'') + Isnull((SELECT DISTINCT ',' + CCM.EmailID FROM tbl_EmployeeMaster CCM WHERE CCM.BaseLocationID = emp.BaseLocationID AND Isnull(EmailID, '') != '' AND CCM.EmailID != (ISNULL(ER.CCEmailID,'')) AND CCM.EmailID LIKE '%@%' AND ISnull(CCM.IsDeleted, 0) = 0 AND Isnull(CCM.IsActive, 0) = 0 FOR XML PATH('')), '') AS CC,
                             emp.BaseLocationID,
                                          emp.Code                                                                                                                                                                                                                                               
                      AS EmployeeCode
                      FROM   tbl_EmployeeMaster emp
                             LEFT JOIN tbl_EmployeePersonalDetails epd
                                    ON epd.EmployeeID = emp.ID
                             LEFT JOIN tbl_Company com
                                    ON com.ID = emp.DomainID
                             LEFT JOIN tbl_BusinessUnit BU
                                    ON bu.ID = emp.BaseLocationID
                             LEFT JOIN tbl_EmailRecipients ER
                                    ON ER.BusinessUnitID = EMP. BaseLocationID
                                       AND ER.KeyID = (SELECT ID
                                                       FROM   tbl_MailerEventKeys
                                                       WHERE  NAME = 'BIRTHDAY'
                                                              AND Isdeleted = 0)
                             LEFT JOIN tbl_EmployeeMaster EM
                                    ON EM.ID = emp.ID
                      WHERE  emp.IsDeleted = 0
                             AND emp.IsActive = 0
                             AND emp.HasAccess = 1
                             AND Month(epd.DateOfBirth) = Month(Getdate())
                             AND Day(epd.DateOfBirth) = Day(Getdate())) X
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
