﻿/****************************************************************************   
CREATED BY		: Ajith
CREATED DATE	: 17 Jun 2019
MODIFIED BY		: 
MODIFIED DATE	:  
 <summary>
		DeleteLOPDetails @MonthID = 7,@Year = 8,@BusinessUnit = '4,',@UserID = 2,@DomainID = 1
 </summary>                           
 *****************************************************************************/
create PROCEDURE [dbo].[DeleteLOPDetails] (@MonthId      INT,
                                          @Year         INT,
                                          @BusinessUnit VARCHAR(100),
                                          @UserID       INT,
                                          @DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Ouput     VARCHAR(100) = 'Operation Failed !!!',
                  @MonthName VARCHAR(50),
                  @YearName  VARCHAR(50)

          SELECT TOP 1 @MonthName = M.Code,
                       @YearName = FY.NAME
          FROM   tbl_LopDetails LOP
                 JOIN tbl_FinancialYear FY
                   ON FY.IsDeleted = 0
                      AND FY.ID = @Year
                      AND FY.DomainID = @DomainID
                 JOIN tbl_Month M
                   ON M.IsDeleted = 0
                      AND M.ID = @MonthId
          WHERE  LOP.DomainId = @DomainID

          IF NOT EXISTS(SELECT 1
                        FROM   tbl_Pay_EmployeePayroll
                        WHERE  IsDeleted = 0
                               AND MonthId = @MonthId
                               AND YearId = @YearName
                               AND DomainId = @DomainID
                               AND EmployeeId IN (SELECT id
                                                  FROM   tbl_EmployeeMaster EM
                                                  WHERE  EM.IsDeleted = 0
                                                         AND EM.DomainID = @DomainID
                                                         AND EM.BaseLocationID IN (SELECT item
                                                                                   FROM   dbo.SplitString(@BusinessUnit, ','))))
            --AND IsProcessed = 1)
            BEGIN
                DELETE FROM tbl_LopDetails
                WHERE  MonthId = @MonthId
                       AND [Year] = @YearName
                       AND DomainId = @DomainID
                       AND EmployeeId IN (SELECT id
                                          FROM   tbl_EmployeeMaster EM
                                          WHERE  EM.IsDeleted = 0
                                                 AND EM.DomainID = @DomainID
                                                 AND EM.BaseLocationID IN (SELECT item
                                                                           FROM   dbo.SplitString(@BusinessUnit, ',')))

                SET @Ouput = 'Deleted Successfully.'
            END
          ELSE
            BEGIN
                SET @Ouput = 'The LOP details cannot be deleted as the payroll has been already processed for '
                             + @MonthName + ' ' + @YearName + '.'
            END

          SELECT @Ouput AS [Message]

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 

