﻿/****************************************************************************   
CREATED BY   :   Naneeshwar.M
CREATED DATE  :   16-OCT-2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [Checkforpaystructureconfiguration] '',1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CHECKFORPAYSTRUCTURECONFIGURATION] (@BusinessUnitID VARCHAR(50),
                                                           @DomainID       INT)
AS
  BEGIN
      BEGIN TRY
          SELECT DISTINCT Split.a.value('.', 'VARCHAR(100)') AS BusinessUnitID,
                          ID,
                          Code
          INTO   #Temptable
          FROM   (SELECT Cast ('<B>'
                               + Replace([BusinessUnitIDs], ',', '</B><B>')
                               + '</B>' AS XML) AS BusinessUnitID,
                         ID,
                         Code
                  FROM   tbl_PaystructureConfiguration) AS A
                 CROSS APPLY BusinessUnitID.nodes ('/B') AS Split(a);

          SELECT Code
          FROM   #Temptable
          WHERE  BusinessUnitID = @BusinessUnitID
		  and (ISNULL(@BusinessUnitID,'') <>'' )
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
