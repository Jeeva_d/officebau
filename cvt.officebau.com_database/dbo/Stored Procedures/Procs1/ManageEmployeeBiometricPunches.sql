﻿ /****************************************************************************                                   
CREATED BY  :   Ajith N                                
CREATED DATE : 06 Feb 2019                          
MODIFIED BY  :                             
MODIFIED DATE :                         
 <summary>   
     ManageEmployeeBiometricPunches 1 
 </summary>                                                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageEmployeeBiometricPunches] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION DML

          DECLARE @Output            VARCHAR(100)= 'Operation Filed!',
                  @UserID            INT = 1,
                  @Count             INT = 1,
                  @AttendanceId      INT = 0,
                  @BusinessStartTime TIME,
                  @BusinessEndTime   TIME,
                  @Duration          TIME,
                  @Minutes           INT,
                  @LogDate           DATE = Dateadd(DAY, -1, Getdate()),
                  @CurrentDate       DATETIME

          SET @CurrentDate = Cast(( Cast(@LogDate AS VARCHAR) + ' 09:00' ) AS DATETIME)

          SELECT top 1 @Duration = Cast(Replace(Cast(B.FullDay AS VARCHAR(5)), '.', ':') AS TIME),
                 @Minutes = B.FullDay * 60
          FROM   tbl_BusinessHours B
          WHERE  B.IsDeleted = 0
                 AND B.DomainID = @DomainID

          SET @BusinessStartTime = Min(@CurrentDate)
          SET @BusinessEndTime = Min(Dateadd(MINUTE, @Minutes, @CurrentDate))

          CREATE TABLE #tmpEmployeeList
            (
               ID                        INT IDENTITY(1, 1),
               EmployeeId                INT,
               LogDate                   DATE,
               Duration                  TIME,
               RegionID                  INT,
               IsBiometricAccessRequired BIT
            )

          INSERT INTO #tmpEmployeeList
          SELECT Em.ID                                   AS EmployeeId,
                 @LogDate                                AS LogDate,
                 @Duration                               AS Duration,
                 EM.RegionID                             AS RegionID,
                 ISNULL(OD.IsBiometricAccessRequired, 1) AS IsBiometricAccessRequired
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_Attendance A
                        ON A.IsDeleted = 0
                           AND A.EmployeeID = EM.ID
                           AND A.LogDate = @LogDate
                 LEFT JOIN tbl_EmployeeOtherDetails OD
                        ON OD.IsDeleted = 0
                           AND OD.EmployeeID = EM.ID
          WHERE  em.IsDeleted = 0
                 AND EM.IsActive = 0
                 AND EM.DomainID = @DomainID
                 AND ISNULL(OD.IsBiometricAccessRequired, 1) = 0

          WHILE( @Count <= (SELECT Count(1)
                            FROM   #tmpEmployeeList) )
            BEGIN
                IF NOT EXISTS(SELECT 1
                              FROM   #tmpEmployeeList EL
                                     JOIN tbl_Holidays H
                                       ON H.IsDeleted = 0
                                          AND H.[Date] = EL.LogDate
                                          AND H.[Year] = Year(@LogDate)
                                          AND H.DomainID = @DomainID
                                          AND H.RegionID LIKE '%,' + Cast(EL.RegionID AS VARCHAR) + ',%'
                              WHERE  EL.LogDate = @LogDate
                                     AND EL.ID = @Count)
                  BEGIN
                      IF NOT EXISTS(SELECT 1
                                    FROM   tbl_Attendance a
                                           JOIN #tmpEmployeeList te
                                             ON te.EmployeeId = a.EmployeeID
                                                AND te.LogDate = a.LogDate
                                    WHERE  a.IsDeleted = 0
                                           AND te.ID = @Count)
                        BEGIN
                            INSERT INTO tbl_Attendance
                                        (EmployeeID,
                                         LogDate,
                                         Duration,
                                         DomainID,
                                         HistoryID,
                                         CreatedBy,
                                         CreatedOn,
                                         ModifiedBy,
                                         ModifiedOn,
                                         IsDeleted,
                                         LeaveDuration)
                            SELECT EmployeeId,
                                   LogDate,
                                   Duration,
                                   @DomainID,
                                   Newid(),
                                   @UserID,
                                   Getdate(),
                                   @UserID,
                                   Getdate(),
                                   0,
                                   NULL
                            FROM   #tmpEmployeeList
                            WHERE  ID = @Count

                            SET @AttendanceId = IDENT_CURRENT('tbl_Attendance')
                        END
                      ELSE
                        BEGIN
                            SET @AttendanceId = (SELECT a.ID
                                                 FROM   tbl_Attendance a
                                                        JOIN #tmpEmployeeList te
                                                          ON te.EmployeeId = a.EmployeeID
                                                             AND te.LogDate = a.LogDate
                                                 WHERE  a.IsDeleted = 0
                                                        AND te.ID = @Count)

                            UPDATE tbl_Attendance
                            SET    Duration = @Duration
                            WHERE  ID = @AttendanceId
                        END

                      IF ( @AttendanceId > 0 )
                        BEGIN
                            IF NOT EXISTS(SELECT 1
                                          FROM   tbl_BiometricLogs
                                          WHERE  IsDeleted = 0
                                                 AND AttendanceId = @AttendanceId
                                                 AND Direction = 'IN'
                                                 AND LogType = 'OfficeBAU')
                              BEGIN
                                  INSERT INTO tbl_BiometricLogs
                                              (AttendanceId,
                                               PunchTime,
                                               Direction,
                                               LogType,
                                               CreatedBy,
                                               CreatedOn,
                                               ModifiedBy,
                                               ModifiedOn,
                                               IsDeleted)
                                  VALUES      (@AttendanceId,
                                               @BusinessStartTime,
                                               'IN',
                                               'OfficeBAU',
                                               @UserID,
                                               Getdate(),
          @UserID,
              Getdate(),
                                               0)
                              END
                            ELSE
                              BEGIN
                                  UPDATE tbl_BiometricLogs
                                  SET    PunchTime = @BusinessStartTime
                                  WHERE  IsDeleted = 0
                                         AND AttendanceId = @AttendanceId
                                         AND Direction = 'IN'
                                         AND LogType = 'OfficeBAU'
                              END

                            IF NOT EXISTS(SELECT 1
                                          FROM   tbl_BiometricLogs
                                          WHERE  IsDeleted = 0
                                                 AND AttendanceId = @AttendanceId
                                                 AND Direction = 'OUT'
                                                 AND LogType = 'OfficeBAU')
                              BEGIN
                                  INSERT INTO tbl_BiometricLogs
                                              (AttendanceId,
                                               PunchTime,
                                               Direction,
                                               LogType,
                                               CreatedBy,
                                               CreatedOn,
                                               ModifiedBy,
                                               ModifiedOn,
                                               IsDeleted)
                                  VALUES      (@AttendanceId,
                                               @BusinessEndTime,
                                               'OUT',
                                               'OfficeBAU',
                                               @UserID,
                                               Getdate(),
                                               @UserID,
                                               Getdate(),
                                               0)
                              END
                            ELSE
                              BEGIN
                                  UPDATE tbl_BiometricLogs
                                  SET    PunchTime = @BusinessEndTime
                                  WHERE  IsDeleted = 0
                                         AND AttendanceId = @AttendanceId
                                         AND Direction = 'OUT'
                                         AND LogType = 'OfficeBAU'
                              END
                        END
                  END

                SET @Count = @Count + 1
                SET @AttendanceId = 0
            END

          SET @Output = 'Updated Successfully'

          SELECT @Output

          COMMIT TRANSACTION DML
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION DML
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
