﻿/****************************************************************************     
CREATED BY   : Dhanalakshmi    
CREATED DATE  :   13/03/2017     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>     
  [ManageCompany] 1,'abc','abc','addr',1,'a','abc@abc.com','qe',1,1,1,'sh',0,1,1    
  select * from tblcompany;    
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ManageCompany] (@ID            INT,    
                                       @Code          VARCHAR(100),    
                                       @Name          VARCHAR(100),    
                                       @Address       VARCHAR(1000),    
                                       @ContactNo     VARCHAR(50),    
                                       @ContactPerson VARCHAR(50),    
                                       @EmailID       VARCHAR(100),    
                                       @Logo          VARCHAR(100),    
                                       @CityID        INT,    
                                       @StateID       INT,    
                                       @CountryID     INT,    
                                       @PanNo         VARCHAR(100),    
                                       @TanNo         VARCHAR(100),    
                                       @CSTNo         VARCHAR(100),    
                                       @SessionID     INT,  
                                        @PaymentFavor          VARCHAR(100)='')    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(1000)    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          BEGIN    
              UPDATE tblCompany    
              SET    NAME = @Name,    
                     Code = @Code,    
                     [Address] = @Address,    
                     ContactNo = @ContactNo,    
                     ContactPerson = @ContactPerson,    
                     EmailID = @EmailID,    
                     Logo = @Logo,    
                     CityID = @CityID,    
                     StateID = @StateID,    
                     CountryID = @CountryID,    
                     PanNo = @PanNo,    
                     TanNo = @TanNo,    
                     CSTNo = @CSTNo,   
                     PaymentFavor=@PaymentFavor,   
                     ModifiedBY = @SessionID,    
                     ModifiedOn = Getdate()    
              WHERE  ID = @ID    
    
              SET @Output = (SELECT [Message]    
                             FROM   tblErrorMessage    
                             WHERE  [Type] = 'Information'    
                                    AND Code = 'RCD_UPD'    
                                    AND IsDeleted = 0) --'Updated Successfully'    
          END    
    
          FINISH:    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END    
    
ROLLBACK
