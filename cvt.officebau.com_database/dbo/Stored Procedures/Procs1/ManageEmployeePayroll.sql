﻿/**************************************************************************** 
CREATED BY    		:	
CREATED DATE		:	
MODIFIED BY			:	Naneeshwar.M
MODIFIED DATE		:	
 <summary>  
 [ManageEmployeePayroll]
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageEmployeePayroll] (@Id                     INT,
                                               @EmployeeId             INT,
                                               @MonthId                INT,
                                               @Year                   INT,
                                               @EmployeePayStructureID INT,
                                               @GrossEarning           MONEY,
                                               @Basic                  MONEY,
                                               @HRA                    MONEY,
                                               @MedicalAllowance       MONEY,
                                               @Conveyance             MONEY,
                                               @EducationAllowance     MONEY,
                                               @PaperMagazine          MONEY,
                                               @MonsoonAllow           MONEY,
                                               @OverTime               MONEY,
                                               @SplAllow               MONEY,
                                               @EEPF                   MONEY,
                                               @EEESI                  MONEY,
                                               @PT                     MONEY,
                                               @TDS                    MONEY,
                                               @ERPF                   MONEY,
                                               @ERESI                  MONEY,
                                               @Bonus                  MONEY,
                                               @Gratuity               MONEY,
                                               @PLI                    MONEY,
                                               @Mediclaim              MONEY,
                                               @MobileDataCard         MONEY,
                                               @OtherEarning           MONEY,
                                               @OtherPerks             MONEY,
                                               @OtherDeduction         MONEY,
                                               @Loans                  MONEY,
                                               @IsProcessed            BIT,
                                               @DomainID               INT,
                                               @LoginID                INT,
                                               @IsApproved             BIT,
                                               @NetSalary              MONEY,
                                               @CTC                    MONEY,
                                               @BaseLocationID         INT,
                                               @NoofDaysPayable        MONEY)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output          VARCHAR(100) = '',
              @RegionID        INT = (SELECT ParentID
                 FROM   tbl_BusinessUnit
                 WHERE  id = @BaseLocationID),
              @PAYEESILimit    INT,
              @PAYEESIValue    MONEY,
              @PAYEESIPercent  DECIMAL(18, 2),
              @PAYERESILimit   INT,
              @PAYERESIValue   MONEY,
              @PAYERESIPercent DECIMAL(18, 2),
              @YEARID          INT =(SELECT ID
                FROM   tbl_FinancialYear
                WHERE  NAME = @Year
                       AND IsDeleted = 0
                       AND DomainID = @DomainID)
      DECLARE @TDSYear INT

      IF( @MonthID BETWEEN 1 AND 3 )
        SET @TDSYear=(SELECT NAME
                      FROM   tbl_FinancialYear
                      WHERE  NAME = @Year - 1
                             AND DomainID = @DomainID)
      ELSE
        SET @TDSYear =(SELECT NAME
                       FROM   tbl_FinancialYear
                       WHERE  NAME = @Year
                              AND DomainID = @DomainID)

      SET @PAYEESILimit=(SELECT Limit
                         FROM   tbl_PayrollConfiguration
                         WHERE  Components = 'EmployeeESI'
                                AND BusinessUnitID = @RegionID)
      SET @PAYEESIValue=(SELECT Value
                         FROM   tbl_PayrollConfiguration
                         WHERE  Components = 'EmployeeESI'
                                AND BusinessUnitID = @RegionID)
      SET @PAYEESIPercent=(SELECT Percentage
                           FROM   tbl_PayrollConfiguration
                           WHERE  Components = 'EmployeeESI'
                                  AND BusinessUnitID = @RegionID)
      SET @PAYERESILimit=(SELECT Limit
                          FROM   tbl_PayrollConfiguration
                          WHERE  Components = 'EmployerESI'
                                 AND BusinessUnitID = @RegionID)
      SET @PAYERESIValue=(SELECT Value
                          FROM   tbl_PayrollConfiguration
                          WHERE  Components = 'EmployerESI'
                                 AND BusinessUnitID = @RegionID)
      SET @PAYERESIPercent=(SELECT Percentage
                            FROM   tbl_PayrollConfiguration
                            WHERE  Components = 'EmployerESI'
                                   AND BusinessUnitID = @RegionID)

      DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'
        + CONVERT(VARCHAR(10), @Year)
      DECLARE @Workdays INT = Datediff(day, @CurrentMonth, Dateadd(month, 1, @CurrentMonth))

      BEGIN TRY
          BEGIN TRANSACTION

          BEGIN
              IF Isnull(@Id, 0) = 0
                BEGIN
                    INSERT INTO tbl_EmployeePayroll
                                (EmployeeId,
                                 MonthId,
                                 YearId,
                                 EmployeePayStructureID,
                                 GrossEarning,
                                 Basic,
                                 HRA,
                                 MedicalAllowance,
                                 Conveyance,
                                 EducationAllow,
                                 MagazineAllow,
                                 MonsoonAllow,
                                 OverTime,
                                 SplAllow,
                                 EEPF,
                                 EEESI,
                                 PT,
                                 TDS,
                                 ERPF,
                                 ERESI,
                                 Bonus,
                                 Gratuity,
                                 PLI,
                                 Mediclaim,
                                 MobileDataCard,
                                 OtherEarning,
                                 OtherPerks,
                                 OtherDeduction,
                                 Loans,
                                 NetSalary,
                                 CTC,
                                 BaseLocationID,
                                 NoofDaysPayable,
                                 IsProcessed,
                                 DomainId,
                                 CreatedBy,
                                 ModifiedBy)
                    SELECT @EmployeeId,
                           @MonthId,
                           @Year,
                           @EmployeePayStructureID,
                           ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                             + @SplAllow + @EducationAllowance
                             + @PaperMagazine ),
                           @Basic,
                           @HRA,
                           @MedicalAllowance,
                           @Conveyance,
                           @EducationAllowance,
                           @PaperMagazine,
                           @MonsoonAllow,
                           @OverTime,
                           @SplAllow,
                           @EEPF,
                           --CASE
                           --  WHEN ( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                           --           + @SplAllow + @EducationAllowance
                           --           + @PaperMagazine + @OverTime ) >= @PAYEESILimit ) THEN @PAYEESIValue
                           --  ELSE Ceiling(( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                           --                   + @SplAllow + @EducationAllowance
                           --                   + @PaperMagazine + @OverTime ) * @PAYEESIPercent / 100 ))
                           --END,
                           @EEESI,
                           @PT,
                           @TDS,
                           @ERPF,
                           --CASE
                           --  WHEN ( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                           --           + @SplAllow + @EducationAllowance
                           --           + @PaperMagazine + @OverTime ) >= @PAYERESILimit ) THEN @PAYERESIValue
                           --  ELSE Ceiling(( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                           --                   + @SplAllow + @EducationAllowance
                           --                   + @PaperMagazine + @OverTime ) * @PAYERESIPercent / 100 ))
                           --END,
                           @ERESI,
                           @Bonus,
                           @Gratuity,
                           @PLI,
                           @Mediclaim,
                           @MobileDataCard,
                           @OtherEarning,
                           @OtherPerks,
                           @OtherDeduction,
                           @Loans,
                           @NetSalary,
                           @CTC,
                           @BaseLocationID,
                           @NoofDaysPayable,
                           @IsProcessed,
                           @DomainID,
                           @LoginID,
                           @LoginID

                    SET @Output ='Inserted Successfully !'

                    GOTO FINISH
                END
              ELSE IF @IsProcessed = 1
                BEGIN
                    UPDATE tbl_EmployeePayroll
                    SET    IsProcessed = @IsProcessed,
                           NetSalary = @NetSalary,
                           CTC = @CTC
                    WHERE  Id = @Id

                    EXEC Managepayrollloanamortization
                      @EmployeeId,
                      @MonthId,
                      @Year,
                      @Loans,
                      @DomainID,
                      @LoginID

                    SET @Output ='Processed Successfully !'

                    GOTO FINISH
                END
              ELSE IF( @IsApproved = 1 )
                BEGIN
                    UPDATE tbl_EmployeePayroll
                    SET    IsApproved = @IsApproved,
                           ApproverId = @LoginID,
                           NetSalary = @NetSalary,
                           CTC = @CTC
                    WHERE  Id = @Id

                    SET @Output ='Approved Successfully !'

                    GOTO FINISH
                END
              ELSE
                BEGIN
                    UPDATE tbl_EmployeePayroll
                    SET    GrossEarning = ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                                            + @SplAllow + @EducationAllowance
                                            + @PaperMagazine ),
                           EmployeePayStructureID = @EmployeePayStructureID,
                           Basic = @Basic,
                           HRA = @HRA,
                           MedicalAllowance = @MedicalAllowance,
                           Conveyance = @Conveyance,
                           EducationAllow = @EducationAllowance,
                           MagazineAllow = @PaperMagazine,
                           MonsoonAllow = @MonsoonAllow,
                           OverTime = @OverTime,
                           SplAllow = @SplAllow,
                           EEPF = @EEPF,
                           --EEESI = CASE
                           --          WHEN ( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                           --                   + @SplAllow + @EducationAllowance
                           --                   + @PaperMagazine + @OverTime ) >= @PAYEESILimit ) THEN @PAYEESIValue
                           --          ELSE Ceiling(( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                           --                           + @SplAllow + @EducationAllowance
                           --                           + @PaperMagazine + @OverTime ) * @PAYEESIPercent / 100 ))
                           --        END,
                           EEESI = @EEESI,
                           PT = @PT,
                           TDS = @TDS,
                           ERPF = @ERPF,
                           --ERESI = CASE
                           --          WHEN ( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                           --                   + @SplAllow + @EducationAllowance
                           --                   + @PaperMagazine + @OverTime ) >= @PAYERESILimit ) THEN @PAYERESIValue
                           --          ELSE Ceiling(( ( @Basic + @HRA + @MedicalAllowance + @Conveyance
                           --                           + @SplAllow + @EducationAllowance
                           --                           + @PaperMagazine + @OverTime ) * @PAYERESIPercent / 100 ))
                           --        END,
                           ERESI = @ERESI,
                           Bonus = @Bonus,
                           Gratuity = @Gratuity,
                           PLI = @PLI,
                           Mediclaim = @Mediclaim,
                           MobileDataCard = @MobileDataCard,
                           OtherEarning = @OtherEarning,
                           OtherPerks = @OtherPerks,
                           OtherDeduction = @OtherDeduction,
                           Loans = @Loans,
                           NetSalary = @NetSalary,
                           CTC = @CTC,
                           ModifiedBy = @LoginID
                    WHERE  Id = @Id

                    SET @Output ='Updated Successfully !'

                    GOTO FINISH
                END
          END

          FINISH:

          IF EXISTS(SELECT 1
                    FROM   tbl_TDS
                    WHERE  EmployeeId = @EmployeeId
                           AND MonthId = @MonthId
                           AND YearID = (SELECT ID
                                         FROM   tbl_financialYear
                                         WHERE  NAME = @TDSYear
                                                AND DomainID = @DomainID)
                           AND IsDeleted = 0)
            BEGIN
                UPDATE tbl_TDS
                SET    TDSAmount = @TDS
                WHERE  MonthId = @MonthId
                       AND YearID = (SELECT ID
                                     FROM   tbl_financialYear
                                     WHERE  NAME = @TDSYear
                                            AND DomainID = @DomainID)
                       AND EmployeeId = @EmployeeId
            END

          IF NOT EXISTS(SELECT 1
                        FROM   tbl_TDS
                        WHERE  EmployeeId = @EmployeeId
                               AND MonthId = @MonthId
                               AND YearID = (SELECT ID
                                             FROM   tbl_financialYear
                                             WHERE  NAME = @TDSYear
                                                    AND DomainID = @DomainID)
                               AND IsDeleted = 0)
            BEGIN
                INSERT INTO tbl_TDS
                            (EmployeeId,
                             MonthId,
                             YearID,
                             TDSAmount,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             DomainId,
                             BaseLocationID)
                VALUES      (@EmployeeId,
                             @MonthId,
                             (SELECT ID
                              FROM   tbl_financialYear
                              WHERE  NAME = @TDSYear
                                     AND DomainID = @DomainID),
                             Isnull(@TDS, 0),
                             @LoginID,
                             Getdate(),
                             @LoginID,
                             Getdate(),
                             @DomainID,
                             @BaseLocationID)
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
