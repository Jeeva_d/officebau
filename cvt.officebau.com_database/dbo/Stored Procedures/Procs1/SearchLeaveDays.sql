﻿/****************************************************************************       
CREATED BY   : K.SASIREKHA      
CREATED DATE  : 29-09-2017      
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>              
  [SearchLeaveDays]  '',6,2,1,1    
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchLeaveDays] (@EmployeeCodes  VARCHAR(500),
                                         @Year           INT,
                                         @BusinessUnitID INT,
                                         @LeaveTypeID    INT,
                                         @DomainID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @yearname VARCHAR(80)= (SELECT NAME
         FROM   tbl_FinancialYear
         WHERE  ID = @Year
                AND IsDeleted = 0
                AND DomainID = @DomainID)
      DECLARE @EmployeeList TABLE
        (
           EmpCode VARCHAR(500)
        )

      INSERT INTO @EmployeeList
                  (EmpCode)
      SELECT DISTINCT Replace(Substring(Item, 0, Charindex('-', Item, 0)), ' ', '')
      FROM   dbo.Splitstring (@EmployeeCodes, ',')
      WHERE  Isnull(Item, '') <> ''

      BEGIN TRY
          SELECT EM.ID                     AS EmployeeID,
                 EM.FullName               AS EmployeeName,
                 ( CASE
                     WHEN EM.Code LIKE 'TMP_%' THEN EM.Code
                     ELSE Isnull(EM.EmpCodePattern, '') + EM.Code
                   END )                   AS EmployeeNo,
                 @yearname                 AS FinancialYear,
                 Isnull(EAL.TotalLeave, 0) AS LOPDays
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_EmployeeAvailedLeave EAL
                        ON EAL.EmployeeID = EM.ID
                           AND EAL.Year = @yearname
                           AND EAL.LeaveTypeID = @LeaveTypeID
          WHERE  EM. BaseLocationID = @BusinessUnitID
                 AND EM.IsDeleted = 0
                 AND ( EM.IsActive = 0
                        OR ( EM.IsActive = 1
                             AND Year(Em.InactiveFrom) = Cast(@yearname AS INT) ) )
                 AND Year(Em.DOJ) <= @yearname
                 AND ( Isnull(@EmployeeCodes, '') = ''
                        OR EM.Code IN (SELECT Replace(EmpCode, EM.EmpCodePattern, '')
                                       FROM   @EmployeeList) )
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END


