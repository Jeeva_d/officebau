﻿/****************************************************************************     
CREATED BY   : Naneeshwar.M    
CREATED DATE  : 14-Jun-2017    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
    [Manageclaimapprove] 0,4,1,4,12,'12',1,0,1,1,1    
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Manageclaimapprove] (@ID                  INT,
                                            @RequesterID         INT,
                                            @RequesterItemID     INT,
                                            @Status              VARCHAR(50),
                                            @FinancialApproverID INT,
                                            @ApprovalAmount      MONEY,
                                            @Remarks             VARCHAR(8000),
                                            @CreatedBy           INT,
                                            @IsDeleted           INT,
                                            @ModifiedBy          INT,
                                            @DomainID            INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output            VARCHAR(1000) = 'Operation Failed!',
              @ClaimStatusID     INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = @Status
                        AND Type = 'Claims'
                        AND IsDeleted = 0),
              @SubmittedStatusID INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = 'Submitted'
                        AND Type = 'Claims'
                        AND IsDeleted = 0),
              @RejectedStatusID  INT = (SELECT ID
                 FROM   tbl_Status
                 WHERE  Code = 'Rejected'
                        AND Type = 'Claims'
                        AND IsDeleted = 0)

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @ID = 0 )
            BEGIN
                INSERT INTO dbo.tbl_ClaimsApprove
                            (RequesterID,
                             FinancialApproverID,
                             ApprovedAmount,
                             StatusID,
                             FinApproverStatusID,
                             ClaimItemID,
                             Comments,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             DomainID)
                VALUES      ( @RequesterID,
                              CASE
                                WHEN ( Isnull(@FinancialApproverID, 0) = 0 ) THEN ( 0 )
                                ELSE ( @FinancialApproverID )
                              END,
                              @ApprovalAmount,
                              @ClaimStatusID,
                              CASE
                                WHEN( @Status = 'Rejected' )THEN @RejectedStatusID
                                ELSE @SubmittedStatusID
                              END,
                              @RequesterItemID,
                              @Remarks,
                              @CreatedBy,
                              Getdate(),
                              @ModifiedBy,
                              Getdate(),
                              @DomainID )

                UPDATE tbl_ClaimsRequest
                SET    StatusID = @ClaimStatusID
                WHERE  id = @RequesterItemID

                IF( @Status = 'Rejected' )
                  SET @Output='Rejected Successfully.'
                ELSE
                  SET @Output='Approved Successfully.'
            END
          ELSE
            BEGIN
                UPDATE tbl_ClaimsApprove
                SET    RequesterID = @RequesterID,
                       FinancialApproverID = CASE
                                               WHEN( Isnull(@FinancialApproverID, 0) = 0 )THEN ( 0 )
                                               ELSE( @FinancialApproverID )
                                             END,
                       ApprovedAmount = @ApprovalAmount,
                       StatusID = @ClaimStatusID,
                       FinApproverStatusID = CASE
                                               WHEN ( @Status = 'Rejected' )THEN @RejectedStatusID
                                               ELSE @SubmittedStatusID
                                             END,
                       Comments = @Remarks,
                       ModifiedBy = @ModifiedBy,
                       ModifiedOn = Getdate()
                WHERE  id = @ID

                UPDATE tbl_ClaimsRequest
                SET    StatusID = @ClaimStatusID
                WHERE  id = @RequesterItemID

                IF( @Status = 'Rejected' )
                  SET @Output='Rejected Successfully.'
                ELSE
                  SET @Output='Approved Successfully.'
            END

          COMMIT TRANSACTION

          SELECT @Output AS UserMessage
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
