﻿
/****************************************************************************         
CREATED BY  :         
CREATED DATE :         
MODIFIED BY  :   JENNIFER S
MODIFIED DATE :  02-SEP-2017
 <summary>                
   
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchBRS] (@BankID       INT,
                                   @StartDate    VARCHAR(50),
                                   @EndDate      VARCHAR(50),
                                   @IsReconsiled BIT,
                                   @DomainID     INT,
                                   @Notations    VARCHAR(50),
                                   @Amount       MONEY)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          IF( @StartDate != '' )
            SET @StartDate = Cast(@StartDate AS DATETIME)

          IF( @EndDate != '' )
            SET @EndDate = Cast(@EndDate AS DATETIME)

          SELECT bk.ID               AS ID,
                 bk.SourceDate       AS SDATE,
                 bk.SourceID         AS SourceID,
                 bk.ReconsiledDate   AS ReconsiledDate,
                 mt.NAME             AS SourceName,
                 b.NAME              AS BankName,
                 bk.Amount           AS Amount,
                 bk.AvailableBalance AS ClosingAmount,
                 bk.IsReconsiled     AS IsReconsiled,
                 bk.BRSDescription   AS [Description]
          INTO   #TempBRS
          FROM   tblBRS bk
                 LEFT JOIN tblBank b
                        ON bk.BankID = b.ID
                 LEFT JOIN tblMasterTypes mt
                        ON bk.SourceType = mt.ID
          WHERE  bk.IsDeleted = 0
                 AND bk.IsReconsiled = @IsReconsiled
                 AND ( Isnull(@BankID, '') = ''
                        OR bk.BankID = @BankID )
                 AND bk.DomainID = @DomainID
                 AND ( ( @StartDate IS NULL
                          OR @StartDate = '' )
                        OR SourceDate >= @StartDate )
                 AND ( ( @EndDate IS NULL
                          OR @EndDate = '' )
                        OR SourceDate <= @EndDate )
          ORDER  BY bk.SourceDate ASC

          SELECT *
          FROM   #TempBRS
          WHERE  ( Isnull(@Amount, 0) = 0 )
                  OR ( Isnull(@Notations, '') = 'EqualTo'
                       AND Amount = @Amount )
                  OR ( Isnull(@Notations, '') = 'GreaterThanEqual'
                       AND Amount >= @Amount )
                  OR ( Isnull(@Notations, '') = 'LessThanEqual'
                       AND Amount <= @Amount )

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END

