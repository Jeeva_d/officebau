﻿/****************************************************************************   
CREATED BY		:   JENNIFER.S
CREATED DATE	:   06-Jun-2017
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary> 
			CreateLeaveType 1, 9, 1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [CreateLeaveType](@ID       INT,
                                   @Name     VARCHAR(100),
                                   @Remarks  VARCHAR(max),
                                   @Code     VARCHAR(100),
                                   @UserID   INT,
                                   @DomainID INT)
AS
  BEGIN
      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Output VARCHAR(50) = 'Record referred'

          IF( @ID = 0 )
            BEGIN
                INSERT INTO tbl_LeaveTypes
                            (NAME,
                             Remarks,
                             DomainID,
                             HistoryID,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             IsDeleted,
                             Code)
                VALUES      ( @Name,
                              @Remarks,
                              @DomainID,
                              Newid(),
                              @UserID,
                              Getdate(),
                              @UserID,
                              Getdate(),
                              0,
                              @Code)

                SET @ID = @@IDENTITY

                INSERT INTO tbl_EmployeeAvailedLeave
                            (EmployeeID,
                             LeaveTypeID,
                             TotalLeave,
                             AvailedLeave,
                             [Year],
                             DomainID,
                             HistoryID,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             IsDeleted)
                SELECT EM.ID           AS EmployeeID,
                       @ID             AS LeaveTypeID,
                       0               AS TotalLeave,
                       0               AS AvailedLeave,
                       Year(Getdate()) AS [Year],
                       @DomainID       AS DomainID,
                       Newid()         AS HistoryID,
                       @UserID         AS CreatedBy,
                       Getdate()       AS CreatedOn,
                       @UserID         AS ModifiedBy,
                       Getdate()       AS ModifiedOn,
                       0               AS IsDeleted
                FROM   tbl_EmployeeMaster EM
                WHERE  EM.IsDeleted = 0
                       AND Em.IsActive = 0
                       AND EM.DomainID = @DomainID

                SET @Output = 'Inserted Successfully.'
            END

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
