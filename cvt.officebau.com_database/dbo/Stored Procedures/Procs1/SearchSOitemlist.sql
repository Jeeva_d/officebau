﻿/****************************************************************************         
CREATED BY   :         
CREATED DATE  :      
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>      
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[SearchSOitemlist] (@InvoiceID INT,      
                                                @DomainID  INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT it.ID              AS ID,      
                 it.ProductID       AS ProductID,      
                 it.ItemDescription AS [Description],      
                 it.Qty             AS Qty,      
                 it.Rate            AS Rate,      
     it.SGST            AS SGST,      
     it.SGSTAMOUNT,      
     it.CGST,      
     it.CGSTAMOUNT,      
     p.Name AS ProductName,      
     it.IsProduct,      
     g.Code  AS HsnCode    
          FROM   tblSOItem it      
	   LEFT JOin tblProduct_v2 p on it.ProductID=p.ID      
	   Left Join tblGstHSN g on p.HSNID =g.ID    
          WHERE  it.IsDeleted = 0      
                 AND @InvoiceID = it.SOID And it.DomainID=@DomainID      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
