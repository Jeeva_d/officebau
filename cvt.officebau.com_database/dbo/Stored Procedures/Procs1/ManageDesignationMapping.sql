﻿/****************************************************************************       
CREATED BY   : DHANALAKSHMI.S      
CREATED DATE  : 08-JAN-2018      
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>              
  [ManageDesignationMapping]       
 </summary>                               
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ManageDesignationMapping] (@ID            INT,    
                                                  @DesignationID INT,    
                                                  @BandID        INT,    
                                                  @GradeID       INT,    
                                                  @SessionID     INT,    
                                                  @IsDeleted     INT,    
                                                  @DomainID      INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(8000)    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SET @Output = 'Operation Failed!'    
    
          IF( @IsDeleted = 1 )    
            BEGIN    
                IF( (SELECT Count(1)    
                     FROM   tbl_LoanConfiguration    
                     WHERE  DesignationMappingID = @ID    
                            AND DomainID = @DomainID    
                            AND IsDeleted = 0) = 0    
                    AND (SELECT Count(1)    
                         FROM   tbl_ClaimPolicy    
                         WHERE  DesignationMappingID = @ID    
                                AND DomainID = @DomainID    
                                AND IsDeleted = 0) = 0 )    
                  BEGIN    
                      UPDATE tbl_EmployeeDesignationMapping    
                      SET    IsDeleted = @IsDeleted,    
                             ModifiedBy = @SessionID,    
                             ModifiedOn = Getdate()    
                      WHERE  ID = @ID    
                             AND DomainID = @DomainID    
    
                      SET @Output = 'Deleted Successfully'    
    
                      GOTO Finish    
                  END    
                ELSE    
                  BEGIN    
                      SET @Output = 'The record is referred.'    
    
                      GOTO Finish    
                  END    
            END    
          ELSE IF( Isnull(@ID, 0) = 0 )    
            BEGIN    
                IF EXISTS(SELECT 1    
                          FROM   tbl_EmployeeDesignationMapping    
                          WHERE  DesignationID = @DesignationID    
                                 AND DomainID = @DomainID    
                                 AND IsDeleted = 0)    
                  BEGIN    
                      SET @Output = 'Already Exists.'    
    
                      GOTO Finish    
                  END    
                ELSE    
                  INSERT INTO tbl_EmployeeDesignationMapping    
                              (DesignationID,    
                               BandID,    
                               GradeID,    
                               CreatedBy,    
                               CreatedOn,    
                               ModifiedBy,    
                               ModifiedOn,    
                               DomainID)    
                  VALUES      ( @DesignationID,    
                                @BandID,    
                                @GradeID,    
                                @SessionID,    
                                Getdate(),    
                                @SessionID,    
                                Getdate(),    
                                @DomainID)    
    
                SET @Output = 'Inserted Successfully.'    
    
                GOTO Finish    
            END    
          ELSE    
            BEGIN    
                IF (SELECT Count(1)    
                    FROM   tbl_EmployeeDesignationMapping    
                    WHERE  DesignationID = @DesignationID    
                           AND DomainID = @DomainID    
                           AND IsDeleted = 0    
                           AND ID <> @ID) = 0    
                  BEGIN    
             IF( (SELECT Count(1)    
                           FROM   tbl_LoanConfiguration    
                           WHERE  DesignationMappingID = @ID    
                                  AND DomainID = @DomainID    
                                  AND IsDeleted = 0) = 0    
                          AND (SELECT Count(1)    
                               FROM   tbl_ClaimPolicy    
                               WHERE  DesignationMappingID = @ID    
                                      AND DomainID = @DomainID    
                                      AND IsDeleted = 0) = 0 )    
                        BEGIN    
                            UPDATE tbl_EmployeeDesignationMapping    
                            SET    DesignationID = @DesignationID,    
                                   BandID = @BandID,    
                                   GradeID = @GradeID,    
                                   ModifiedBy = @SessionID,    
                                   ModifiedOn = Getdate()    
                            WHERE  ID = @ID    
                                   AND DomainID = @DomainID    
    
                            SET @Output = 'Updated Successfully.'    
    
                            GOTO Finish    
                        END    
                      ELSE    
                        BEGIN    
                            SET @Output = 'The record is referred.'    
    
                            GOTO Finish    
                        END    
                  END    
                ELSE    
                  BEGIN    
                      SET @Output = 'Already Exists.'    
    
                      GOTO Finish    
                  END    
            END    
    
          FINISH:    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
