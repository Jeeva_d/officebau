﻿/****************************************************************************     
CREATED BY   :     
CREATED DATE  :    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GenerateForHolidays] (@Year AS INT = 2019,
@DomainID INT = 1,
@RegionIDS Varchar(Max) =',1,',@DaysToInculede Varchar(Max))
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  Declare
@FirstDateOfYear DATETIME,
@LastDateOfYear DATETIME
   
SET @FirstDateOfYear = DATEADD(yyyy, @Year - 1900, 0)
SET @LastDateOfYear = DATEADD(yyyy, @Year - 1900 + 1, 0)

;WITH cte AS (
	SELECT 1 AS DayID,
	@FirstDateOfYear AS FromDate,
	DATENAME(dw, @FirstDateOfYear) AS Dayname
	UNION ALL
	SELECT cte.DayID + 1 AS DayID,
	DATEADD(d, 1 ,cte.FromDate),
	DATENAME(dw, DATEADD(d, 1 ,cte.FromDate)) AS Dayname
	FROM cte
	WHERE DATEADD(d,1,cte.FromDate) < @LastDateOfYear
)
--INSERT INTO tbl_Holidays (Year,date,Day,Type,IsOptional,DomainID,RegionID)
SELECT @Year,FromDate, Dayname,'Weekly',0,@DomainID,@RegionIDS
FROM CTE
WHERE DayName IN ( SELECT * FROM dbo.FnSplitString(@DaysToInculede,','))
OPTION (MaxRecursion 370)
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
