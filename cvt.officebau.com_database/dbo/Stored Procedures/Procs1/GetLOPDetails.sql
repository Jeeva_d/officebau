﻿/****************************************************************************       
CREATED BY  :      
CREATED DATE :       
MODIFIED BY  :      
MODIFIED DATE :      
<summary>     
         GetLOPDetails 10,4,null,3, 1        
</summary>                               
*****************************************************************************/    
CREATE PROCEDURE [dbo].[GetLOPDetails] (@MonthID      INT,    
                                       @Year         INT,    
                                       @BusinessUnit VARCHAR(50),    
                                       @UserID       INT,    
                                       @DomainID     INT)    
AS    
  BEGIN    
      BEGIN TRY    
          DECLARE @StatusID          INT = (SELECT ID    
                     FROM   tbl_Status    
                     WHERE  IsDeleted = 0    
                            AND Code = 'Approved'    
                            AND [Type] = 'Leave'),    
                  @LeaveTypeIDs      VARCHAR(50) = Isnull((SELECT ID    
                            FROM   tbl_LeaveTypes    
                            WHERE  IsDeleted = 0    
                                   AND NAME IN ( 'Permission' ) AND DomainID = @DomainID), 0),    
                  @LeaveTypeOD       VARCHAR(50) = Isnull((SELECT ID    
                            FROM   tbl_LeaveTypes    
                            WHERE  IsDeleted = 0    
                                   AND NAME IN ( 'On Duty' ) AND DomainID = @DomainID), 0),    
                  @ApplicationRoleID VARCHAR(20) = (SELECT TOP 1 ID    
                     FROM   tbl_ApplicationRole    
                     WHERE  NAME LIKE '%HR%' AND DomainID = @DomainID),    
                  @BusinessUnitIDs   VARCHAR(50)    
    
          IF( Isnull(@BusinessUnit, '') = '' )    
            SET @BusinessUnitIDs = (SELECT BusinessUnitID    
                                    FROM   tbl_EmployeeMaster    
                                    WHERE  Isnull(IsDeleted, 0) = 0    
                                           AND Isnull(IsActive, 0) = 0    
                                           AND ID = @UserID    
                                           AND DomainID = @DomainID)    
          ELSE    
            SET @BusinessUnitIDs = Cast(@BusinessUnit AS VARCHAR(50))    
    
          DECLARE @count               INT = 1,    
                  @DayCount            INT = 1,    
                  @EmployeeID          INT,    
                  @RowValueUpdateQuery VARCHAR(max),    
                  @DynamicSQL          VARCHAR(MAX),    
                  @ColumnName          VARCHAR(50),    
                  @LeaveType           VARCHAR(30),    
                  @LeaveOn             VARCHAR(30),    
                  @Salarydate          DATE,    
                  @SalaryStartDate     VARCHAR(20) = 1,    
                  @SalaryMonth         VARCHAR(10),    
                  @SalaryYear          VARCHAR(10),    
                  @SalaryDayCount      INT,    
                  @day                 DATE,    
                  @Allday              VARCHAR(max) = '',    
                  @DynamicColumnName   VARCHAR(MAX) = '',    
                  @IsHalfDay           BIT = 'false',    
                  @NoOfHours           INT = 0    
          DECLARE @tempEmp TABLE    
            (    
               ID           INT IDENTITY(1, 1),    
               EmployeeID   INT,    
               [Name]       VARCHAR(100),    
               [Date]       VARCHAR(30),    
               NoOfHours    INT,    
               LeaveType    VARCHAR(30),    
               BusinessUnit INT,    
               IsHalfDay    BIT    
            )    
    
          CREATE TABLE #ResultTable    
            (    
               ID         INT IDENTITY(1, 1),    
               EmployeeID INT,    
               [Name]     VARCHAR(100),    
               [Total]    DECIMAL(32, 1) NULL    
            )    
    
          CREATE TABLE #TempDates    
            (    
               ID     INT IDENTITY(1, 1),    
               [date] VARCHAR(10)    
            )    
    
          -- Set Salary Start Date    
          SET @SalaryMonth = (SELECT Code    
                              FROM   tbl_Month    
                              WHERE  ID = @MonthID    
                                     AND IsDeleted = 0)    
          SET @SalaryYear = (SELECT FinancialYear    
                             FROM   tbl_FinancialYear    
                             WHERE  ID = @Year    
                                    AND IsDeleted = 0 AND DomainID = @DomainID)    
          SET @SalaryDate = @SalaryMonth + ' ' + @SalaryStartDate + ' '    
                            + @SalaryYear;    
          --SET @Salarydate = 'jun 1 2017';             
          SET @day = @SalaryDate;    
    
          WITH CTE    
               AS (SELECT @SalaryDate AS CurrentDate    
                   UNION ALL    
                   SELECT Dateadd(DAY, 1, CurrentDate) AS CurrentDate    
                   FROM   CTE    
                   WHERE  Dateadd(DD, 1, CurrentDate) < Dateadd(MONTH, 1, @SalaryDate))    
          INSERT INTO #TempDates    
          SELECT *    
          FROM   CTE    
          ORDER  BY CurrentDate    
    
          SET @SalaryDayCount = (SELECT Count(1)    
                                 FROM   #TempDates)    
    
          IF EXISTS(SELECT 1    
                    FROM   tbl_EmployeeMaster EM    
                           LEFT JOIN tbl_EmployeeOtherDetails OD    
                                  ON OD.EmployeeID = EM.ID    
                    WHERE  Isnull(EM.IsDeleted, 0) = 0    
                           AND Isnull(EM.IsActive, 0) = 0    
                           AND EM.ID = @UserID AND EM.DomainID = @DomainID)    
            --AND Isnull(OD.ApplicationRoleID, 0) IN ( @ApplicationRoleID ))    
            BEGIN    
                INSERT INTO @tempEmp    
                SELECT EM.ID                                              AS EmployeeID,    
                       ISNULL(EM.EmpCodePattern, '') + '' + EM.Code + ' - ' + EM.FullName                     AS NAME,    
                       Substring(CONVERT(VARCHAR(20), LM.LeaveDate, 109), 1, 3)    
                       + ' '    
                       + Cast(Datepart(DAY, LM.LeaveDate) AS VARCHAR(50)) AS [Date],    
                       Sum(EL.Duration)                                   AS Duration,    
                       LT.Code                                            AS LeaveType,    
                       EM.BaseLocationID                                  AS BusinessUnit,    
                       Isnull(EL.IsHalfDay, 'false')                      AS IsHalfDay    
                FROM   tbl_EmployeeLeave EL    
                       LEFT JOIN tbl_EmployeeLeaveDateMapping LM    
                              ON EL.ID = LM.LeaveRequestID    
                       LEFT JOIN tbl_EmployeeMaster EM    
                              ON EL.EmployeeID = EM.ID    
                       LEFT JOIN tbl_LeaveTypes LT    
                              ON EL.LeaveTypeID = LT.ID    
                WHERE  EL.Duration IS NOT NULL    
                       AND EL.IsDeleted = 0    
                       AND Isnull(EM.IsDeleted, 0) = 0    
                       AND Isnull(EL.HRStatusID, 0) = @StatusID    
                       AND EM.BaseLocationID IN (SELECT Item    
                                                 FROM   DBO.Splitstring(@BusinessUnitIDs, ','))    
                       AND EL.LeaveTypeID NOT IN ( @LeaveTypeIDs )    
                       AND EL.DomainID = @DomainID    
                       AND LM.LeaveDate >= @SalaryDate    
                       AND LM.LeaveDate <= Dateadd(DAY, @SalaryDayCount, @SalaryDate)    
                GROUP  BY EM.ID,    
                          ISNULL(EM.EmpCodePattern, '') + '' +EM.Code + ' - ' + EM.FullName,    
                          LM.LeaveDate,    
                          LT.Code,    
                          EM.BaseLocationID,    
                          EL.IsHalfDay    
                UNION ALL    
                SELECT EM.ID                                              AS EmployeeID,    
                       ISNULL(EM.EmpCodePattern, '') + '' + EM.Code + ' - ' + EM.FullName                     AS NAME,    
                       Substring(CONVERT(VARCHAR(20), LM.LeaveDate, 109), 1, 3)    
                       + ' '    
                       + Cast(Datepart(DAY, LM.LeaveDate) AS VARCHAR(50)) AS [Date],    
                       Count(1) * 8                                       AS Duration,    
         ( CASE    
                           WHEN Isnull(EL.IsHalfDay, 'false') = 'false' THEN LT.Code    
                           ELSE LT.Code + '(H)'    
                         END )                                            AS LeaveType,    
                       EM.BaseLocationID                                  AS BusinessUnit,    
                       Isnull(EL.IsHalfDay, 'false')                      AS IsHalfDay    
                FROM   tbl_EmployeeLeaveDateMapping LM    
                       LEFT JOIN tbl_EmployeeLeave EL    
                              ON EL.ID = LM.LeaveRequestID    
                       LEFT JOIN tbl_EmployeeMaster EM    
                              ON LM.EmployeeID = EM.ID    
                       LEFT JOIN tbl_LeaveTypes LT    
                              ON EL.LeaveTypeID = LT.ID    
                WHERE  EL.Duration IS NULL    
                       AND LM.IsDeleted = 0    
                       AND Isnull(EM.IsDeleted, 0) = 0    
                       AND EM.BaseLocationID IN (SELECT Item    
                                                 FROM   DBO.Splitstring(@BusinessUnitIDs, ','))    
                       AND Isnull(EL.HRStatusID, 0) = @StatusID    
                       AND EL.LeaveTypeID NOT IN ( @LeaveTypeIDs )    
                       AND EL.LeaveTypeID NOT IN ( @LeaveTypeOD )    
                       AND EL.DomainID = @DomainID    
                       AND LM.LeaveDate >= @SalaryDate    
                       AND LM.LeaveDate <= Dateadd(DAY, @SalaryDayCount, @SalaryDate)    
                GROUP  BY EM.ID,    
                          ISNULL(EM.EmpCodePattern, '') + '' +EM.Code + ' - ' + EM.FullName,    
                          LM.LeaveDate,    
                          LT.Code,    
                          EM.BaseLocationID,    
                          EL.IsHalfDay    
            ----Get All Permission     
            --      UNION ALL    
            --      SELECT EM.ID                                              AS EmployeeID,    
            --             EM.Code + ' - ' + EM.FirstName               AS NAME,    
            --             SUBSTRING(CONVERT(VARCHAR(20), LM.LeaveDate, 109), 1, 3)    
            --             + ' '    
            --             + CAST(DATEPART(DAY, LM.LeaveDate) AS VARCHAR(50)) AS [Date],    
            --             0                                                  AS Duration,    
            --             LT.Code + '(' + CAST(EL.Duration AS VARCHAR)    
            --             + ')'                                              AS LeaveType,    
            --             EM.BaseLocationID                                  AS BusinessUnit,    
            --             isnull(EL.IsHalfDay, 'false')                      AS IsHalfDay    
            --      FROM   tbl_EmployeeLeaveDateMapping LM    
            --             LEFT JOIN tbl_EmployeeLeave EL    
            --                    ON EL.ID = LM.LeaveRequestID    
            --             LEFT JOIN tbl_EmployeeMaster EM    
            --                    ON LM.EmployeeID = EM.ID    
            --             LEFT JOIN tbl_LeaveTypes LT    
            --                    ON EL.LeaveTypeID = LT.ID    
            --      WHERE  EL.Duration IS NOT NULL    
            --             AND LM.IsDeleted = 0    
            --             AND Isnull(EM.IsDeleted, 0) = 0    
            --             AND EM.BaseLocationID IN (SELECT Item    
            --                                       FROM   DBO.Splitstring(@BusinessUnitIDs, ','))    
            --             AND Isnull(EL.HRStatusID, 0) = @StatusID    
            --             AND EL.LeaveTypeID IN ( @LeaveTypeIDs )    
            --             AND EL.DomainID = @DomainID    
            --             AND LM.LeaveDate >= @SalaryDate    
            --             AND LM.LeaveDate <= DATEADD(DAY, @SalaryDayCount, @SalaryDate)    
            --      GROUP  BY EM.ID,    
            --                EM.Code + ' - ' + EM.FirstName,    
            --                LM.LeaveDate,    
            --                LT.Code,    
            --                EM.BaseLocationID,    
       --          EL.IsHalfDay,    
            --                EL.Duration    
            END    
    
          INSERT INTO #ResultTable    
                      (EmployeeID,    
                       NAME)    
          SELECT DISTINCT EmployeeID,    
                          NAME    
          FROM   @tempEmp    
    
          -- Get Dynamic Column    
          SET @DynamicColumnName = (SELECT ',['    
                                           + Substring(CONVERT(VARCHAR(20), Cast([date] AS DATETIME), 109), 1, 3)    
                                           + ' '    
                                           + Cast(Datepart(DAY, [date]) AS VARCHAR)    
                                           + '] AS ['    
                                           + Cast(Datepart(DAY, [date]) AS VARCHAR)    
                                           + ']'    
                                    FROM   #TempDates    
                                    FOR XML PATH(''))    
          -- Get Dynamic alter Query    
          SET @DynamicSQL = (SELECT 'ALTER TABLE #ResultTable ADD ['    
                                    + Substring(CONVERT(VARCHAR(20), Cast([date] AS DATETIME), 109), 1, 3)    
                                    + ' '    
                                    + Cast(Datepart(DAY, [date]) AS VARCHAR)    
                                    + '] VARCHAR(20) NULL;'    
                             FROM   #TempDates    
                             FOR XML PATH('')) -- Add dynamic column    
          EXEC(@DynamicSQL) -- Add dynamic column         
          WHILE( @Count <= (SELECT Count(1)    
                            FROM   @tempEmp) )    
            BEGIN    
                SET @EmployeeID = (SELECT EmployeeID    
                                   FROM   @tempEmp    
                                   WHERE  ID = @Count)    
                SET @LeaveType = (SELECT LeaveType    
                                  FROM   @tempEmp    
                                  WHERE  ID = @Count)    
                SET @LeaveOn = (SELECT [Date]    
                                FROM   @tempEmp    
                                WHERE  ID = @Count)    
                SET @IsHalfDay = (SELECT IsHalfDay    
                                  FROM   @tempEmp    
                                  WHERE  ID = @Count)    
                SET @NoOfHours = (SELECT NoOfHours    
                                  FROM   @tempEmp    
                                  WHERE  ID = @Count)    
    
                WHILE( @DayCount <= (SELECT Count(1)    
                                     FROM   #TempDates) )    
                  BEGIN    
                      SET @ColumnName = (SELECT Substring(CONVERT(VARCHAR(20), Cast([date] AS DATETIME), 109), 1, 3)    
                                                + ' '    
                                                + Cast(Datepart(DAY, [date]) AS VARCHAR)    
                                         FROM   #TempDates    
                                         WHERE  ID = @DayCount)    
    
                      IF( @LeaveOn = @ColumnName )    
                        BEGIN    
                            IF( @LeaveType = (SELECT code    
                                              FROM   tbl_LeaveTypes    
                                              WHERE  NAME LIKE '%Permission%'    
                                                     AND IsDeleted = 0    
                                          AND DomainID = @DomainID) )    
                              BEGIN    
                                  SET @RowValueUpdateQuery = Isnull('UPDATE #ResultTable SET [' + @ColumnName +'] = ''' + @LeaveType + ''' WHERE EmployeeID = ''' + Cast(@EmployeeID AS VARCHAR) + ''';', '')    
                                                             + Isnull(@RowValueUpdateQuery, '') --Update Row value     
                              END    
                            ELSE    
                              BEGIN    
                                  SET @RowValueUpdateQuery = Isnull('UPDATE #ResultTable SET [' + @ColumnName +'] = ''' + @LeaveType + ''' WHERE EmployeeID = ''' + Cast(@EmployeeID AS VARCHAR) + ''';', '')    
                                                             + Isnull(@RowValueUpdateQuery, '') --Update Row value     
                              END    
    
                            UPDATE #ResultTable    
                            SET    [Total] = (SELECT ( CASE    
                                                         WHEN Isnull(@IsHalfDay, 'false') = 'false'    
                                                              AND @NoOfHours <> 0 THEN Isnull([Total], 0) + 1    
                                                         WHEN Isnull(@IsHalfDay, 'false') = 'false'    
                                                              AND @NoOfHours = 0 THEN Isnull([Total], 0) + 0    
                                                         ELSE Isnull([Total], 0) + 0.5    
                                                       END )    
                                              FROM   #ResultTable    
                                              WHERE  EmployeeID = @EmployeeID)    
                            WHERE  EmployeeID = @EmployeeID    
                        END    
    
                      SET @DayCount = @DayCount + 1    
                  END    
    
                SET @DayCount = 0    
                -- SET @LeaveOn = NULL    
                SET @Count = @Count + 1    
            END    
    
          EXEC(@RowValueUpdateQuery)    
    
          EXEC('SELECT Name, [Total]' + @DynamicColumnName + '    
          FROM   #ResultTable')    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
