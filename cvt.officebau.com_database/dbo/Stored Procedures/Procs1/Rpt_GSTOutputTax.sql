﻿/****************************************************************************               
CREATED BY   : Ajith N          
CREATED DATE  : 27 Jul 2018              
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>            
	[Rpt_GSTOutputTax] 4,1 ,'','true' 
 </summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_GSTOutputTax] (@FinancialYearID INT,
                                          @DomainID        INT,
                                          @ReportType      VARCHAR(50),
                                          @IsIncludeZero   BIT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @FinancialYear INT;
          DECLARE @StartFinancialMonth INT;
          DECLARE @EndFinancialMonth INT;
          DECLARE @Result TABLE
            (
               [Supplier] VARCHAR(200),
               Total      MONEY
            )

          SET @StartFinancialMonth = (SELECT Value
                                      FROM   tblApplicationConfiguration
                                      WHERE  Code = 'STARTMTH'
                                             AND DomainID = @DomainID)
          SET @EndFinancialMonth = ( ( (SELECT Value
                                        FROM   tblApplicationConfiguration
                                        WHERE  Code = 'STARTMTH'
                                               AND DomainID = @DomainID)
                                       + 11 ) % 12 )

          IF( @FinancialYearID = 0 )
            BEGIN
                IF( Month(Getdate()) <= ( ( (SELECT Value
                                             FROM   tblApplicationConfiguration
                                             WHERE  Code = 'STARTMTH'
                                                    AND DomainID = @DomainID)
                                            + 11 ) % 12 ) )
                  SET @FinancialYear = Year(Getdate()) - 1
                ELSE
                  SET @FinancialYear = Year(Getdate())
            END
          ELSE
            BEGIN
                SET @FinancialYear = Substring((SELECT FinancialYear
                                                FROM   tbl_FinancialYear
                                                WHERE  ID = @FinancialYearID
                                                       AND DomainID = @DomainID), 0, 5)
            END

          DECLARE @StartDate DATE = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date          
          DECLARE @EndDate DATE = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date         
          SELECT im.InvoiceID                                                AS ID,
                 c.NAME                                                      AS Customer,
                 Isnull(G.Code, '')                                          AS [HSN Code],
                 Isnull(c.GSTNo, '')                                         AS [GST No],
                 i.Type                                                      AS [Type],
                 i.InvoiceNo                                                 AS [Invoice No],
                 ( im.qty * im.Rate )                                        AS [Gross Amount],
                 Isnull(im.CGSTAmount, 0)                                    AS CGST,
                 Cast(Isnull(im.CGST, 0) AS VARCHAR)                         AS [CGST %],
                 Isnull(im.SGSTAmount, 0)                                    AS SGST,
                 Cast(Isnull(im.SGST, 0) AS VARCHAR)                         AS [SGST %],
                 0                                                           AS IGST,
                 Cast(0 AS VARCHAR)                                          AS [IGST %],
                 ( Isnull(im.CGSTAmount, 0) ) + ( Isnull(im.SGSTAmount, 0) ) AS [Total GST],
                 ( ( im.Qty * im.Rate ) + ( Isnull(im.CGSTAmount, 0)
                                            + Isnull(im.SGSTAmount, 0) ) )   AS [Net Amount],
                 Row_number()
                   OVER (
                     ORDER BY c.NAME ASC, G.Code ASC)                        AS RNo
          INTO   #tmpResult
          FROM   tblInvoiceItem im
                 LEFT JOIN tblInvoice I
                        ON I.ID = im.InvoiceID
                 LEFT JOIN tblCustomer c
                        ON c.ID = I.CustomerID
                 LEFT JOIN tblProduct_v2 P
                        ON P.ID = im.ProductID
                 LEFT JOIN tblGstHSN G
                        ON G.ID = P.hsnID
          WHERE  im.IsDeleted = 0
                 AND im.DomainID = @DomainID
                 AND I.[Date] >= @StartDate
                 AND I.[Date] <= @EndDate
          ORDER  BY c.NAME ASC,
                    Isnull(G.Code, '');

          IF( @ReportType = 'CompactView' )
            BEGIN ;
                WITH GstOutput
                     AS (SELECT Customer,
                                [GST No],
                                Sum([Gross Amount]) AS [Gross Amount],
                                Sum(CGST)           AS CGST,
                                Sum(SGST)           AS SGST,
                                Sum(IGST)           AS IGST,
                                Sum([Total GST])    AS [Total GST],
                                Sum([Net Amount])   AS [Net Amount]
                         FROM   #tmpResult
                         WHERE  ( @IsIncludeZero = 'true' )
                                 OR ( ( @IsIncludeZero = 'false' )
                                      AND ( CGST > 0
                                             OR SGST > 0
                                             OR IGST > 0 ) )
                         GROUP  BY Customer,
                                   [GST No]
                         UNION ALL
                         SELECT 'TOTAL',
                                NULL,
                                Sum([Gross Amount]),
                                Sum(CGST),
                                Sum(SGST),
                                Sum(IGST),
                                Sum([Total GST]),
                                Sum([Net Amount])
                         FROM   #tmpResult
                         WHERE  ( @IsIncludeZero = 'true' )
                                 OR ( ( @IsIncludeZero = 'false' )
                                      AND ( CGST > 0
                                             OR SGST > 0
                                             OR IGST > 0 ) ))
                SELECT *
                FROM   GstOutput
                WHERE  [Gross Amount] > 0
            END
          ELSE
            BEGIN ;
                WITH GstOutput
                     AS (SELECT ID,
                                Customer,
                                [HSN Code],
                                [Type],
                                [Invoice No],
                                [Gross Amount],
                                CGST,
                                [CGST %],
                                SGST,
                                [SGST %],
                                IGST,
                                [IGST %],
                                [Total GST],
                                [Net Amount],
                                RNo
                         FROM   #tmpResult
                         WHERE  ( @IsIncludeZero = 'true' )
                                 OR ( ( @IsIncludeZero = 'false' )
                                      AND ( CGST > 0
                                             OR SGST > 0
                                             OR IGST > 0 ) )
                         UNION ALL
                         SELECT NULL,
                                'TOTAL',
                                NULL,
                                NULL,
                                NULL,
                                Sum([Gross Amount]),
                                Sum(CGST),
                                NULL,
                                Sum(SGST),
                                NULL,
                                Sum(IGST),
                                NULL,
                                Sum([Total GST]),
                                Sum([Net Amount]),
                                NULL
                         FROM   #tmpResult
                         WHERE  ( @IsIncludeZero = 'true' )
                                 OR ( ( @IsIncludeZero = 'false' )
                                      AND ( CGST > 0
                                             OR SGST > 0
                                             OR IGST > 0 ) ))
                SELECT ID,
                       Customer,
                       [HSN Code],
                       [Invoice No],
                       Sum([Gross Amount]) AS [Gross Amount],
                       [CGST %],
                       Sum(CGST)           AS CGST,
                       [SGST %],
                       Sum(SGST)           AS SGST,
                       [IGST %],
                       Sum(IGST)           AS IGST,
                       Sum([Total GST])    AS [Total GST],
                       Sum([Net Amount])   AS [Net Amount]
                FROM   GstOutput
                WHERE  [Gross Amount] > 0
                GROUP  BY ID,
                          Customer,
                          [HSN Code],
                          [Invoice No],
                          [CGST %],
                          [SGST %],
                          [IGST %],
                          RNo
                ORDER  BY CASE
                            WHEN ( RNo IS NULL ) THEN
                              9999
                            ELSE
                              RNo
                          END
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
