﻿/****************************************************************************                   
CREATED BY   : Naneeshwar                  
CREATED DATE  :   21-Nov-2016                
MODIFIED BY   :                  
 <summary>                
 [Getincome] 1133,1                
 </summary>                                           
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Getincome] (@ID       INT,    
                                   @DomainID INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SELECT inv.ID                                                                   AS ID,    
                 inv.Date                                                                 AS InvoiceDate,    
                 inv.DueDate                                                              AS DueDate,    
                 inv.CustomerID                                                           AS CustomerID,    
                 cus.NAME                                                                 AS CustomerName,    
                 inv.BillAddress                                                          AS BillingAddress,    
                 cus.PaymentTerms                                                         AS PaymentTerms,    
                 inv.Description                                                          AS [Description],    
                 inv.RevenueCenterID                                                      AS RevenueCenterID,    
                 cc.NAME                                                                  AS RevenueCenterName,    
                 inv.InvoiceNo                                                            AS InvoiceNo,    
                 inv.Type                                                                 AS ScreenType,    
                 ( CASE    
                     WHEN ( inv.DiscountPercentage <> 0 ) THEN    
                       (SELECT ( ISNULL(Sum(QTY * Rate)    
                                        + Sum(ISNULL(SGSTAmount, 0) + ISNULL(CGSTAmount, 0)) - ( ISNULL(( Sum(QTY * Rate) * inv.DiscountPercentage ), 0) ), 0) )    
                        FROM   tblInvoiceItem    
                        WHERE  InvoiceID = @ID    
                               AND IsDeleted = 0    
                               AND DomainID = @DomainID)    
                     ELSE    
                       (SELECT ( ISNULL(( Sum(QTY * Rate)    
                                          + Sum(ISNULL(SGSTAmount, 0) + ISNULL(CGSTAmount, 0)) - ISNULL(inv.DiscountValue, 0) ), 0) )    
                        FROM   tblInvoiceItem    
                        WHERE  InvoiceID = @ID    
                               AND IsDeleted = 0    
                               AND DomainID = @DomainID)    
                   END )                                                                  AS TotalAmount,    
                 inv.ReceivedAmount    
                 + (SELECT ISNULL(Sum(Amount), 0)    
                    FROM   tblInvoiceHoldings    
                    WHERE  IsDeleted = 0    
                           AND InvoiceID = inv.ID)                                        AS ReceivedAmount,    
                 inv.DiscountID                                                           AS DiscountID,    
                 cd.Code                                                                  AS DiscountType,    
                 ( inv.DiscountPercentage * 100 )                                         AS DiscountPercentage,    
                 inv.DiscountValue                                                        AS DiscountValue,    
                 inv.CurrencyRate                                                         AS CurrencyRate,    
                 cur.NAME                                                                 AS Currency,    
                 inv.ModifiedOn                                                           AS ModifiedOn,    
                 EMP.FullName                                                             AS ModifiedBy,    
				 FIUP.OriginalFileName                                                    As [FileName],  
				Cast(FIUP.Id AS VARCHAR(50))											  AS HistoryID,   
                 C.FullName                                                               AS CompanyName,    
                 Cast(C.Logo AS VARCHAR(50))                                              AS Logo,    
                 C.Address                                                                AS CompanyAddress,    
                 ''                                                                       AS STATENAME,    
                 c.EmailID                                                                AS CompanyEmaild,    
                 c.GSTNo                                                                  AS CompanyGSTNO,    
                 c.ContactNo                                                              AS CompanyContactNo,    
                 cus.GSTNo                                                                AS CustomerGSTNo,    
                 ISNULL(s1.NAME, 'N/A')                                                   AS CustomerState,    
                 ISNULL((SELECT Toid FROM tblEmailConfig WHERE DomainID = @DomainID AND [key] = 'SendInvoice'), '')    
                 + ',' + ISNULL(cus.CustomerEmailid, '') + ','    
                 + ISNULL(cus.EmailId, '')                                                AS ToEmail,    
                 ISNULL((SELECT CC FROM tblEmailConfig WHERE DomainID = @DomainID AND [key] = 'SendInvoice'), '')    
                 + ',' + c.EmailId                                                        AS CCEmail,    
                 (SELECT BCC    
                  FROM   tblEmailConfig    
                  WHERE  DomainID = @DomainID    
                         AND [key] = 'SendInvoice')                                       AS BCCEmail,    
                 C.PaymentFavour                                                          AS PaymentFavour,    
                 Stuff((SELECT ',' + so.SONo    
                        FROM   tblSalesOrder AS so    
                        WHERE  so.IsDeleted = 0    
                               AND so.ID IN ((SELECT s.SOID    
                                              FROM   tblSOItem AS s    
                                              WHERE  IsDeleted = 0    
                                                     AND Id IN ((SELECT ii.SoItemID    
                                                                 FROM   tblInvoiceItem AS ii    
                                                                 WHERE  IsDeleted = 0    
                                                                        AND ii.InvoiceID = inv.Id))))    
                        FOR XML PATH (''), TYPE) .value ('.', 'NVARCHAR(MAX)'), 1, 1, '') AS SoNos  ,inv.HistoryID  
          FROM   tblInvoice inv    
                 LEFT JOIN tblCustomer cus    
                        ON cus.ID = inv.CustomerID    
                 LEFT JOIN tblCurrency cur    
                        ON cur.ID = cus.CurrencyID    
                 LEFT JOIN tbl_CodeMaster cd    
                        ON cd.ID = inv.DiscountID    
                 LEFT JOIN tblCostCenter cc    
                        ON cc.ID = inv.RevenueCenterID    
                 LEFT JOIN tbl_EmployeeMaster EMP    
                        ON EMP.ID = inv.ModifiedBy    
                 LEFT JOIN tbl_Company C    
                        ON C.ID = INV.DomainID    
                 LEFT JOIN tbl_FileUpload fu    
                        ON fu.ID = c.Logo   
                 LEFT JOIN tbl_FileUpload FIUP         
                     ON FIUP.Id = inv.HistoryID      
                 --Left join tbl_State s on s.ID=c.StateID                
                 LEFT JOIN tbl_City ss    
                        ON ss.ID = cus.cityid    
 LEFT JOIN tbl_State s1    
                        ON s1.ID = ss.StateID    
          WHERE  inv.IsDeleted = 0    
                 AND ( inv.ID = @ID    
                        OR @ID IS NULL )    
                 AND inv.DomainID = @DomainID    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = ERROR_MESSAGE(),    
                 @ErrSeverity = ERROR_SEVERITY()    
          RAISERROR (@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END 