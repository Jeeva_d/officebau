﻿/****************************************************************************         
CREATED BY   :   Naneeshwar.M      
CREATED DATE  :   06-Jun-2017      
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>      
 Searchrbsmenu 2,1 ,"HRMS"     
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchRBSMenu] (@UserID     INT,
                                       @DomainID   INT,
                                       @RootConfig VARCHAR(20))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT RMO.Module                AS Module,
                 RSM.SubModule             AS SubModule,
                 RSM.menuicon              AS SubMenuIcon,
                 RBS.Menu                  AS Menu,
                 RBS.MenuURL               AS MenuURL,
                 RBS.MenuCode              AS MenuCode,
                 RBS.MenuOrderID           AS MenuORDERID,
                 RMO.ModuleOrderID         AS MOrderID,
                 RSM.SubModuleOrderID      AS SMOrderID,
                 Isnull(RBS.IsMobility, 0) AS IsMobility 
          FROM   tbl_RBSMenu RBS
                 JOIN tbl_RBSSubModule RSM
                   ON RBS.SubModuleID = RSM.ID
                      AND Isnull(RSM.IsDeleted, 0) = 0
                 JOIN tbl_RBSModule RMO
                   ON RSM.ModuleID = RMO.ID
                 LEFT JOIN tbl_RBSUserMenuMapping RUM
                        ON RUM.MenuID = RBS.ID
          WHERE  RUM.MRead = 1
                 AND RUM.EmployeeID = @UserID
                 AND RUM.DomainID = @DomainID
                 AND RBS.IsSubmenu = 0
                 AND RBS.MenuCode NOT IN ( 'COMPANY' )
                 AND RBS.RootConfig IN ( Isnull(@RootConfig, RBS.RootConfig), 'Common'  )
                 AND RBS.DomainID = @DomainID AND RBS.ISDELETED =0
          ORDER  BY SMOrderID,
                    MenuOrderID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
