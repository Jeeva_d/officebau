﻿/****************************************************************************     
CREATED BY  : Priya K  
CREATED DATE : 30-Oct-2018  
MODIFIED BY  :  
MODIFIED DATE :   
 <summary>  
  [Pay_ManageTDSDetails]   
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_ManageTDSDetails] (@SessionID  INT,
                                              @DomainID   INT,
                                              @TDSDetails TDSDETAILS READONLY)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Ouput VARCHAR(100) = 'Operation Failed!'

          IF EXISTS(SELECT 1
                    FROM   tbl_Pay_EmployeePayroll Pay
                           JOIN @TDSDetails det
                             ON Pay.EmployeeId = DET.EmployeeID
                                AND Pay.MonthID = DET.MonthID
                                AND PAY.YearID = DET.[YEAR]
                                AND PAY.IsProcessed = 0
                                AND PAY.IsDeleted = 0
                                AND PAY.DomainID = @DomainID
                           LEFT JOIN tbl_TDS tds
                                  ON tds.Id = det.ID
                                     AND det.MonthId = tds.MonthId
                                     AND det.Year = tds.YearID
                                     AND DET.TDSAmount = tds.TDSAmount
                                     AND tds.EmployeeId = det.EmployeeID)
            BEGIN
                UPDATE Pay
                SET    Pay.IsDeleted = 1
                FROM   tbl_Pay_EmployeePayroll Pay
                       JOIN @TDSDetails det
                         ON Pay.EmployeeId = det.EmployeeID
                            AND Pay.MonthID = DET.MonthID
                            AND PAY.YearID = DET.[YEAR]
                            AND PAY.IsProcessed = 0
                            AND PAY.IsDeleted = 0
                            AND PAY.DomainID = @DomainID
                       LEFT JOIN tbl_TDS tds
                              ON tds.Id = det.ID
                                 AND tds.EmployeeId = det.EmployeeID
                                 AND det.MonthId = tds.MonthId
                                 AND det.Year = tds.YearID
            -- WHERE  Isnull(det.LOPDays, 0) <> Isnull(lop.LOPDays, 0)  
            END

          -- Insert TDS Details  
          INSERT INTO tbl_TDS
                      (EmployeeId,
                       MonthID,
                       YearID,
                       TDSAmount,
					   BaseLocationID,
                       CreatedBy,
                       ModifiedBy,
                       DomainId)
          SELECT EmployeeID,
                 MonthID,
                 [Year],
                 Isnull(TDSAmount, 0),
				 BaseLocationID,
                 @SessionID,
                 @SessionID,
                 @DomainID
          FROM   @TDSDetails
          WHERE  Isnull(ID, 0) = 0

          --    AND PayrollStatus = 'Open'  
          -- AND Isnull(LOPDays, 0) <> 0  
          -- Update TDS Details  
          UPDATE tbl_TDS
          SET    TDSAmount = Isnull(det.TDSAmount, 0),
                 ModifiedBy = @SessionID,
                 ModifiedOn = Getdate()
          FROM   tbl_TDS r
                 JOIN @TDSDetails det
                   ON r.Id = det.ID

          SET @Ouput = 'Updated Successfully'

          SELECT @Ouput

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
