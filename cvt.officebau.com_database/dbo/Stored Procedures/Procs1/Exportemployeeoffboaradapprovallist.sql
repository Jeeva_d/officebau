﻿/****************************************************************************     
CREATED BY   : DHANALAKSHMI S    
CREATED DATE  : 16 Aug 2017    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
   [Exportemployeeoffboaradapprovallist]  '','', 22, 436,1    
 </summary>                              
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[Exportemployeeoffboaradapprovallist] (@EmployeeCode VARCHAR(20),    
                                                             @Name         VARCHAR(100),    
                                                             @StatusID     INT,    
                                                             @ApproverID   INT,    
                                                             @DomainID     INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          DECLARE @BusinessUnitIDs VARCHAR(50) = (SELECT BusinessUnitID    
             FROM   tbl_EmployeeMaster    
             WHERE  ID = @ApproverID    
                    AND DomainID = @DomainID)    
          DECLARE @BusinessUnitTable TABLE    
            (    
               BusinessUnit VARCHAR(100)    
            )    
    
          INSERT INTO @BusinessUnitTable    
          SELECT @BusinessUnitIDs    
    
          SELECT ISNULL(EM.EmpCodePattern, '') + '' + EM.Code                                           AS [Employee Code],  
                 EM.FirstName + ' ' + Isnull(EM.LastName, '')      AS [Employee Name],    
                 BU.NAME                                           AS [Business Unit],    
                 DEP.NAME                                          AS [Department],    
                 OFP.InitiatedDate                                 AS [Initiated Date],    
                 OFP.RelievingDate                                 AS [Exp. Relieving Date]    
          FROM   tbl_OffBoardProcess OFP    
                 LEFT JOIN tbl_EmployeeMaster EM    
                        ON EM.ID = OFP.EmployeeID    
                 LEFT JOIN tbl_Department DEP    
                        ON DEP.ID = EM.DepartmentID    
                 LEFT JOIN tbl_BusinessUnit BU    
                        ON BU.ID = EM.BaseLocationID    
                 LEFT JOIN tbl_BusinessUnit REGION    
                        ON REGION.ID = EM.RegionID    
                 JOIN @BusinessUnitTable TBU    
                   ON TBU.BusinessUnit LIKE '%,'    
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))    
                                            + ',%'    
          WHERE  OFP.ApproverID = @ApproverID    
                 AND ( Isnull(@EmployeeCode, '') = ''    
                        OR ( EM.Code LIKE '%' + @EmployeeCode + '%' ) )    
                 AND ( Isnull(@Name, '') = ''    
                        OR ( ( EM.FirstName + ' ' + Isnull(EM.LastName, '') ) LIKE '%' + @Name + '%' ) )    
                 AND ( ( Isnull(@StatusID, 0) = 0    
                         AND Isnull(@StatusID, 0) = 0 )    
                        OR OFP.StatusID = @StatusID )    
                 AND OFP.domainID = @DomainID    
          ORDER  BY EM.FirstName + ' ' + Isnull(EM.LastName, '')    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT;    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
