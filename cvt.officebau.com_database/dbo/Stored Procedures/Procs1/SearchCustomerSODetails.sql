﻿
/****************************************************************************                   
CREATED BY    :                   
CREATED DATE  :                   
MODIFIED BY   :                
MODIFIED DATE :                   
 <summary>     
 [SearchCustomerSODetails] 9 ,1   
 </summary>                                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchCustomerSODetails] (@CustomerID INT,
@DomainID INT)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION

		SELECT
			p.id
		   ,SONo AS InvoiceNO
		   ,((QTY - ISNULL(InvoicedQTY, 0)) * Rate) + (((((QTY - ISNULL(InvoicedQTY, 0)) * Rate) / 100) * SGST) +
			((((QTY - ISNULL(InvoicedQTY, 0)) * Rate) / 100) * CGST)) Amount INTO #Result
		FROM tblSalesOrder p
		JOIN tblSOItem po
			ON po.SOID = p.id
			and po.IsDeleted = 0
		WHERE p.IsDeleted=0 and CustomerID = @CustomerID
		AND StatusID <> (SELECT
				id
			FROM tbl_CodeMaster
			WHERE IsDeleted = 0
			AND [type] = 'Close')

		SELECT
			id
		   ,InvoiceNO
		   ,SUM(Amount) AS Amount
		FROM #Result
	--	WHERE #Result.Amount <>0
		GROUP BY id
				,InvoiceNO
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMsg VARCHAR(100)
			   ,@ErrSeverity TINYINT
		SELECT
			@ErrorMsg = ERROR_MESSAGE()
		   ,@ErrSeverity = ERROR_SEVERITY()
		RAISERROR (@ErrorMsg, @ErrSeverity, 1)
	END CATCH
END
