﻿/**************************************************************************** 
CREATED BY			:	
CREATED DATE		:	
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	ManageTDSComponent
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageTDSComponent] (@ID                INT,
                                            @SectionID         INT,
                                            @Code              VARCHAR(100),
                                            @Rules             VARCHAR(20),
                                            @Value             DECIMAL(18, 2),
                                            @Description       VARCHAR(8000),
                                            @HasDeleted        BIT,
                                            @SessionEmployeeID INT,
                                            @DomainID          INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(8000),
	  @FYID INT = 0

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          IF( Isnull(@ID, 0) != 0 )
            BEGIN
                IF( @HasDeleted = 1 )
                  BEGIN
                      UPDATE TDSComponent
                      SET    IsDeleted = 1,
                             ModifiedBy = @SessionEmployeeID,
                             ModifiedOn = Getdate()
                      WHERE  ID = @ID

                      SET @Output = 'Deleted Successfully.'

                      GOTO Finish
                  END

                IF( @HasDeleted = 0 )
                  BEGIN
                      IF NOT EXISTS (SELECT 1
                                     FROM   TDSComponent
                                     WHERE  ID <> @ID
                                            AND SectionID = @SectionID
                                            AND Code = @Code
                                            AND DomainID = @DomainID
											AND FYID = @FYID
                                            AND IsDeleted = 0)
                        BEGIN
                            UPDATE TDSComponent
                            SET    SectionID = @SectionID,
                                   Code = @Code,
                                   Rules = @Rules,
                                   Value = @Value,
                                   [Description] = @Description,
                                   ModifiedBy = @SessionEmployeeID,
                                   ModifiedOn = Getdate(),
								   FYID = @FYID
                            WHERE  ID = @ID

                            SET @Output = 'Updated Successfully.'

                            GOTO Finish
                        END
                      ELSE
                        BEGIN
                            SET @Output = 'Already Exists!'

                            GOTO Finish
                        END
                  END
            END

          IF( Isnull(@ID, 0) = 0 )
            BEGIN
                IF NOT EXISTS (SELECT 1
                               FROM   TDSComponent
                               WHERE  SectionID = @SectionID
                                      AND Code = @Code
                                      AND DomainID = @DomainID
									  AND FYID = @FYID
                                      AND IsDeleted = 0)
                  BEGIN
                      INSERT INTO TDSComponent
                                  (SectionID,
                                   Code,
                                   Rules,
                                   Value,
                                   [Description],
                                   CreatedBy,
                                   ModifiedBy,
                                   DomainID,
								   FYID)
                      VALUES      ( @SectionID,
                                    @Code,
                                    @Rules,
                                  @Value,
                                    @Description,
                                    @SessionEmployeeID,
                                    @SessionEmployeeID,
                                    @DomainID,
									@FYID)

                      SET @Output = 'Inserted Successfully.'

                      GOTO Finish
                  END
                ELSE
                  BEGIN
                      SET @Output = 'Already Exists!'

                      GOTO Finish
                  END
            END

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
