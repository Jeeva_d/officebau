﻿/****************************************************************************               
CREATED BY    :               
CREATED DATE  :               
MODIFIED BY   :             
MODIFIED DATE :              
 <summary>      tblvendor            
     [Searchautocompleteparameter] 'Customer','Name','A' ,1              
 </summary>                                       
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchautocompleteparameter] (@TableName       VARCHAR(200),
                                                      @columnName      VARCHAR(200),
                                                      @SearchParameter VARCHAR(200),
                                                      @DomainID        INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @sql VARCHAR(MAX)

          SET @TableName='tbl' + @TableName

          IF( @TableName = 'tblLedger' )
            BEGIN
                SET @sql= ( 'Select A.ID, A.Name, '''' AS Currency, '''' AS Address, '''' AS PaymentTerms,  '''' AS InvoiceNo,'''' SOCount From '
                            + @TableName + ' AS A Where A.' + @columnName
                            + ' like''%' + @SearchParameter
                            + '%'' and A.IsDeleted = 0 and A.DomainID = '
                            + CONVERT(VARCHAR(10), @DomainID) )
            END
          ELSE IF ( @TableName = 'tblCustomer' )
            BEGIN
                SET @sql= ( 'SELECT A.ID,            
                                A.NAME,            
                                B.NAME                AS Currency,            
                                A.[Billingaddress] AS address,            
                                A.PaymentTerms        AS PaymentTerms,            
                                (SELECT TOP 1 InvoiceNo            
                                 FROM   tblInvoice            
                                 WHERE  CustomerID = A.ID            
                                 ORDER  BY Date DESC) InvoiceNo ,
								 CAST((Select Count(*) from tblSalesOrder  
     where isdeleted =0 and CustomerID=A.ID And StatusID =(Select ID from tbl_codemaster where isdeleted=0 and type=''open'')) AS Varchar(10)) AS SOCount          
                         FROM   tblCustomer AS A            
                                LEFT JOIN tblcurrency B            
                                       ON B.ID = A.CurrencyID WHERE  A.'
                            + @columnName + ' LIKE''%' + @SearchParameter
                            + '%'' AND A.IsDeleted = 0            
                                AND A.DomainID = '
                            + CONVERT(VARCHAR(10), @DomainID) )
            END
          ELSE IF ( @TableName = 'tblVendor' )
            BEGIN
                SET @sql= ( 'Select A.ID, A.Name,B.Name AS Currency,A.Address,A.PaymentTerms AS PaymentTerms , 
							 CAST((Select Count(*) from tblpurchaseorder 
									where isdeleted =0 and VendorID=A.ID 
										And StatusID =(Select ID from tbl_codemaster where isdeleted=0 and type=''open'')) AS Varchar(10)) AS InvoiceNo ,'''' SOCount 
							From '
                            + @TableName
                            + ' AS A LEFT Join tblcurrency B on B.ID =A.CurrencyID Where A.'
                            + @columnName + ' like''%' + @SearchParameter
                            + '%'' and A.IsDeleted = 0 and A.DomainID = '
                            + CONVERT(VARCHAR(10), @DomainID) )
            END
          ELSE
            BEGIN
                SET @sql= ( 'Select A.ID, A.Name,B.Name AS Currency,A.Address,A.PaymentTerms AS PaymentTerms ,  '''' AS InvoiceNo ,'''' SOCount From '
                            + @TableName
                            + ' AS A LEFT Join tblcurrency B on B.ID =A.CurrencyID Where A.'
                            + @columnName + ' like''%' + @SearchParameter
                            + '%'' and A.IsDeleted = 0 and A.DomainID = '
                            + CONVERT(VARCHAR(10), @DomainID) )
            END

          PRINT @sql

          EXEC(@sql)

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
