﻿/****************************************************************************   
CREATED BY		:  Naneeshwar.M
CREATED DATE	:  22-feb-2018
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
	[Gettdslimitforcalculation] 2,'80TTB',3,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetTDSLimitforCalculation] (@EmployeeID INT,
                                                   @section    VARCHAR(50),
                                                   @FYID       INT,
                                                   @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Limit MONEY
      DECLARE @SectionID INT =(SELECT ID
        FROM   TDSSection
        WHERE  Code = @section
               --AND DomainID = @DomainID
               AND IsDeleted = 0)
      DECLARE @RegionID INT =(SELECT RegionID
        FROM   tbl_EmployeeMaster
        WHERE  id = @EmployeeID)

      BEGIN TRY
          IF ( @section = '80c' )
            BEGIN
                SELECT @Limit = Limit
                FROM   tbl_TDSLimitConfiguration
                WHERE  SectionID IN(SELECT ID
                                    FROM   TDSSection
                                    WHERE  Code LIKE '%' + @section + '%'
                                           --AND DomainID = @DomainID
                                           AND IsDeleted = 0)
                       AND FYID = @FYID
                       AND IsDeleted = 0
                       AND regionID = @RegionID
            END
          ELSE
            BEGIN
                SELECT @Limit = Limit
                FROM   tbl_TDSLimitConfiguration
                WHERE  SectionID = @SectionID
                       AND FYID = @FYID
                       AND IsDeleted = 0
                       AND regionID = @RegionID
            END

          SELECT ISNULL(@Limit, 0) AS Limit
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
