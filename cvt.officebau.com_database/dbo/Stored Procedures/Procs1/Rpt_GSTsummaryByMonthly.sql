﻿/****************************************************************************                   
CREATED BY   : Ajith N              
CREATED DATE  : 01 Aug 2018                  
MODIFIED BY   :                   
MODIFIED DATE  :                   
 <summary>                
  [Rpt_GSTsummaryByMonthly] 4,1 ,''           
 </summary>                                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_GSTsummaryByMonthly] (@FinancialYearID INT,
                                                 @DomainID        INT,
                                                 @ReportType      VARCHAR(50))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @FinancialYear INT;
          DECLARE @StartFinancialMonth INT;
          DECLARE @EndFinancialMonth INT;
          DECLARE @Query            VARCHAR(MAX),
                  @ColumnName       VARCHAR(MAX),
                  @ColumnHeaderName VARCHAR(MAX)

          CREATE TABLE #tmpResult
            (
               --id      INT IDENTITY(1, 1),      
               [Type]    VARCHAR(100),
               NAME      VARCHAR(100),
               Total     MONEY,
               [date]    VARCHAR(100),
               [RNumber] INT
            )

          SET @StartFinancialMonth = (SELECT Value
                                      FROM   tblApplicationConfiguration
                                      WHERE  Code = 'STARTMTH'
                                             AND DomainID = @DomainID)
          SET @EndFinancialMonth = ( ( (SELECT Value
                                        FROM   tblApplicationConfiguration
                                        WHERE  Code = 'STARTMTH'
                                               AND DomainID = @DomainID)
                                       + 11 ) % 12 )

          IF( @FinancialYearID = 0 )
            BEGIN
                IF( Month(Getdate()) <= ( ( (SELECT Value
                                             FROM   tblApplicationConfiguration
                                             WHERE  Code = 'STARTMTH'
                                                    AND DomainID = @DomainID)
                                            + 11 ) % 12 ) )
                  SET @FinancialYear = Year(Getdate()) - 1
                ELSE
                  SET @FinancialYear = Year(Getdate())
            END
          ELSE
            BEGIN
                SET @FinancialYear = Substring((SELECT FinancialYear
                                                FROM   tbl_FinancialYear
                                                WHERE  ID = @FinancialYearID
                                                       AND DomainID = @DomainID), 0, 5)
            END

          DECLARE @StartDate DATE = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date              
          DECLARE @EndDate DATE = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date             
          ;

          WITH cteDate
               AS (SELECT 1          AS DayID,
                          @StartDate AS FromDate
                   --Datename(dw, @StartDate) AS Dayname  
                   UNION ALL
                   SELECT cteDate.DayID + 1 AS DayID,
                          Dateadd(MONTH, 1, cteDate.FromDate)
                   --Datename(dw, Dateadd(d, 1, cte.FromDate)) AS Dayname  
                   FROM   cteDate
                   WHERE  Dateadd(MONTH, 1, cteDate.FromDate) < @EndDate)
          SELECT Substring(Datename(month, FromDate), 1, 3 )
                 + '_'
                 + Substring(Cast(Year(FromDate) AS VARCHAR), 3, 2) AS [Date],
                 -- Dayname,  
                 Row_number()
                   OVER (
                     partition BY Datepart( month, FromDate ), Datename( weekday, FromDate )
                     ORDER BY FromDate )                            AS RowID
          INTO   #tmpDate
          FROM   cteDate
          ORDER  BY FromDate
          OPTION (MaxRecursion 370)

          SELECT --im.InvoiceID                                                AS ID,  
          --c.NAME                                                      AS Customer,  
          --Isnull(G.Code, '')                                          AS [HSN Code],  
          --Isnull(c.GSTNo, '')                                         AS [GST No],  
          --i.Type                                                      AS [Type],  
          --i.InvoiceNo                                                 AS [Invoice No],  
          --( im.qty * im.Rate )                                        AS [Total Amount],  
          Isnull(im.CGSTAmount, 0)                                    AS CGST,
          Cast(Isnull(im.CGST, 0) AS VARCHAR)                         AS [CGST %],
          Isnull(im.SGSTAmount, 0)                                    AS SGST,
          Cast(Isnull(im.SGST, 0) AS VARCHAR)                         AS [SGST %],
          0                                                           AS IGST,
          Cast(0 AS VARCHAR)                                          AS [IGST %],
          ( Isnull(im.CGSTAmount, 0) ) + ( Isnull(im.SGSTAmount, 0) ) AS [Total GST],
          -- CONVERT(VARCHAR, i.[Date], 106)                             AS [Date],  
          Substring(Datename(month, i.[Date]), 1, 3 )
          + '_'
          + Substring(Cast(Year(i.[Date]) AS VARCHAR), 3, 2)          AS [Date],
          'Output'                                                    AS [Type]
          INTO   #tmpGstOutput
          FROM   tblInvoiceItem im
                 LEFT JOIN tblInvoice I
                        ON I.ID = im.InvoiceID
                 LEFT JOIN tblCustomer c
                        ON c.ID = I.CustomerID
                 LEFT JOIN tblProduct_v2 P
                        ON P.ID = im.ProductID
                 LEFT JOIN tblGstHSN G
                        ON G.ID = P.HsNId
          WHERE  im.IsDeleted = 0
                 AND im.DomainID = @DomainID
                 AND I.[Date] >= @StartDate
                 AND I.[Date] <= @EndDate
          ORDER  BY c.NAME ASC,
                    Isnull(G.Code, '')

          SELECT --EI.ExpenseID                               AS ID,  
          --v.NAME                                     AS Vendor,  
          --Isnull(v.GSTNo, '')                        AS [GST No],  
          --Isnull(G.Code, '')                         AS [HSN Code],  
          --E.[Type]                                   AS [Type],  
          --E.billNo                                   AS [Bill No],  
          --( EI.Qty * EI.Amount )                     AS [Total Amount],  
          Isnull(EI.CGST, 0)                                 AS CGST,
          Cast(Isnull(EI.CGSTPercent, 0) AS VARCHAR)         AS [CGST %],
          Isnull(EI.SGST, 0)                                 AS SGST,
          Cast(Isnull(EI.SGSTPercent, 0) AS VARCHAR)         AS [SGST %],
          Isnull(EI.IGST, 0)                                 AS IGST,
          Cast(Isnull(EI.IGSTPercent, 0) AS VARCHAR)         AS [IGST %],
          ( Isnull(EI.CGST, 0) + Isnull(EI.SGST, 0)
            + Isnull(EI.IGST, 0) )                           AS [Total GST],
          -- CONVERT(VARCHAR, E.[Date], 106)            AS [Date],  
          Substring(Datename(month, e.[Date]), 1, 3 )
          + '_'
          + Substring(Cast(Year(E.[Date]) AS VARCHAR), 3, 2) AS [Date],
          'Input'                                            AS [Type]
          INTO   #tmpGstInput
          FROM   tblExpenseDetails EI
                 LEFT JOIN tblExpense E
                        ON E.ID = EI.ExpenseID
                 LEFT JOIN tblVendor v
                        ON v.ID = E.VendorID
                 LEFT JOIN tblLedger L
                        ON L.ID = EI.LedgerID
                 LEFT JOIN tblGstHSN G
                        ON G.ID = L.GstHSNID
          WHERE  EI.IsDeleted = 0
                 AND EI.DomainID = @DomainID
                 AND E.[Date] >= @StartDate
                 AND E.[Date] <= @EndDate
          ORDER  BY v.NAME ASC,
                    E.[Type] ASC,
                    Isnull(G.Code, '')

          --SELECT *  
          --FROM   #tmpGstOutput  
          --SELECT *  
          --FROM   #tmpGstInput  
          ----- GST Output      
          INSERT INTO #tmpResult
          SELECT 'GST Output Tax' AS [Type],
                 'CGST'           AS NAME,
                 Sum(CGST)        AS CGST,
                 [Date],
                 1                AS RNumber
          FROM   #tmpGstOutput
          GROUP  BY [date]
          UNION ALL
          SELECT 'GST Output Tax' AS [Type],
                 'SGST'           AS NAME,
                 Sum(SGST)        AS SGST,
                 [Date],
                 2                AS RNumber
          FROM   #tmpGstOutput
          GROUP  BY [date]
          UNION ALL
          SELECT 'GST Output Tax' AS [Type],
                 'IGST'           AS NAME,
                 Sum(IGST)        AS IGST,
                 [Date],
                 3                AS RNumber
          FROM   #tmpGstOutput
          GROUP  BY [date]
          UNION ALL
          SELECT 'GST Output Tax'                      AS [Type],
                 'Total'                               AS NAME,
                 ( Sum(CGST) + Sum(SGST) + Sum(IGST) ) AS Total,
                 [Date],
                 4                                     AS RNumber
          FROM   #tmpGstOutput
          GROUP  BY [date]
          UNION ALL
          ----- GST Input      
          SELECT 'GST Input Tax' AS [Type],
                 'CGST'          AS NAME,
                 Sum(CGST)       AS CGST,
                 [Date],
                 1               AS RNumber
          FROM   #tmpGstInput
          GROUP  BY [date]
          UNION ALL
          SELECT 'GST Input Tax' AS [Type],
                 'SGST'          AS NAME,
                 Sum(SGST)       AS SGST,
                 [Date],
                 2               AS RNumber
          FROM   #tmpGstInput
          GROUP  BY [date]
          UNION ALL
          SELECT 'GST Input Tax' AS [Type],
                 'IGST'          AS NAME,
                 Sum(IGST)       AS IGST,
                 [Date],
                 3               AS RNumber
          FROM   #tmpGstInput
          GROUP  BY [date]
          UNION ALL
          SELECT 'GST Input Tax'                       AS [Type],
                 'Total'                               AS NAME,
                 ( Sum(CGST) + Sum(SGST) + Sum(IGST) ) AS Total,
                 [Date],
                 4                                     AS RNumber
          FROM   #tmpGstInput
          GROUP  BY [date]
          ---- GST to be Paid  
          UNION ALL
          SELECT ''                                                                                    AS [Type],
                 'GST to be Paid'                                                                      AS NAME,
                 Isnull((SELECT ( Sum(op.CGST) + Sum(op.SGST) + Sum(op.IGST) )
                         FROM   #tmpGstOutput op
                         WHERE  op.[Date] = td.[Date]), 0) - Isnull((SELECT ( Sum(ip.CGST) + Sum(ip.SGST) + Sum(ip.IGST) )
                                                                     FROM   #tmpGstInput ip
                                                                     WHERE  ip.[Date] = td.[Date]), 0) AS Total,
                 td.[Date],
                 1                                                                                     AS RNumber
          FROM   #tmpDate td
          GROUP  BY td.[date]

          SET @ColumnHeaderName = Substring((SELECT ',[' + Cast([date] AS VARCHAR(15)) + '] AS ['
                                                    -- + CONVERT(VARCHAR, [date], 106) + ']'  
                                                    + Cast([date] AS VARCHAR(15)) + ']'
                                             FROM   #tmpDate
                                             FOR xml path('')), 2, 2000)
          SET @ColumnName = Substring((SELECT ',[' + Cast([date] AS VARCHAR(15)) + ']'
                                       FROM   #tmpDate
                                       FOR xml path('')), 2, 2000)
          SET @Query = ( 'SELECT [Type] , NAME, '
                         + @ColumnHeaderName
                         + '  
    FROM   (SELECT *  
                 FROM   #tmpResult) AS src  
                 PIVOT ( Max(total)  
                       FOR [date] IN ( '
                         + @ColumnName
                         + ') ) piv  
          ORDER  BY Type desc, RNumber' )

          EXEC(@Query)
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
