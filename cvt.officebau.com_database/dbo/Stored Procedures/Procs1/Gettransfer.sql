﻿/****************************************************************************     
CREATED BY   : Dhanalakshmi    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [GetContraVoucher] Null,1,1    
 select * from tblContraVoucher  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Gettransfer] (@ID       INT,  
                                      @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT con.ID             AS ID,  
                 FromID             AS FromID,  
                 ToID               AS ToID,  
                 Amount             AS Amount,  
                 VoucherDate        AS VoucherDate,  
                 VoucherDescription AS VoucherDescription,  
                 con.ModifiedOn     AS ModifiedOn,  
                 EMP.FullName      AS ModifiedBy  
          FROM   tblTransfer con  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = con.ModifiedBy  
          WHERE  ( con.ID = @ID  
                    OR @ID IS NULL )  
                 AND con.DomainID = @DomainID  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
