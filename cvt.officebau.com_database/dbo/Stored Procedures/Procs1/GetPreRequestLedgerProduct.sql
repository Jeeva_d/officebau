﻿/****************************************************************************               
CREATED BY   : Jeeva              
CREATED DATE  :               
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>            
 [GetPreRequestLedgerProduct] 1            
 </summary>                                       
 *****************************************************************************/            
CREATE PROCEDURE [dbo].[GetPreRequestLedgerProduct] (            
                                              @DomainID INT)            
AS            
  BEGIN            
      SET NOCOUNT ON;            
            
      BEGIN TRY            
          BEGIN TRANSACTION            
            
          SELECT pd.ID                            AS ID,            
                 SUBSTRING(pd.NAME,0,20) + '(Product) '  AS NAME            
          FROM   tblProduct_V2 pd            
                 JOIN tbl_CodeMaster cd            
                   ON cd.ID = pd.TypeID            
          WHERE  pd.DomainID = @DomainID            
         and pd.TypeID in  (SELECT ID            
                                              FROM   tbl_CodeMaster            
                                              WHERE  Code IN (  'Product' ))             
                 AND pd.IsPurchase=1           
              and pd.IsDeleted=0               
            
  Union All            
   Select ID , SUBSTRING(Name,0,20)+'(Ledger) ' from tblLedger where IsDeleted =0 and DomainID=@DomainID            
  Order By Name          
          COMMIT TRANSACTION            
      END TRY            
      BEGIN CATCH            
          ROLLBACK TRANSACTION            
          DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT            
          SELECT @ErrorMsg = Error_message(),            
                 @ErrSeverity = Error_severity()            
          RAISERROR(@ErrorMsg,@ErrSeverity,1)            
      END CATCH            
  END
