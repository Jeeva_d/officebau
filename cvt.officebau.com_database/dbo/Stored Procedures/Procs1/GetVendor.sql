﻿/****************************************************************************       
CREATED BY   : Jeeva      
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>    
 [GetVendor] 2003,1,1      
 select * from tblEmployeeMaster    
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetVendor] (@ID       INT,  
                                   @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT VEN.ID              AS ID,  
                 VEN.NAME            AS NAME,  
                 VEN.EmailID         AS EmailID,  
                 VEN.VendorEmailID   AS VendorEmailID,  
                 VEN.ContactNo       AS ContactNo,  
                 VEN.ContactPerson   AS ContactPerson,  
                 VEN.CityID          AS CityID,  
                 CITY.NAME           AS CityName,  
                 VEN.[Address]       AS Address,  
                 VEN.BillingAddress  AS BillingAddress,  
                 VEN.CurrencyID      AS CurrencyID,  
                 VEN.PaymentTerms    AS PaymentTerms,  
                 VEN.PanNo           AS PanNo,  
                 VEN.TanNo           AS TanNo,  
                 VEN.Remarks         AS Remarks,  
                 VEN.ContactPersonNo AS ContactPersonNo,  
                 VEN.TinNo           AS TinNo,  
                 VEN.CSTNo           AS CSTNo,  
                 VEN.ModifiedOn      AS ModifiedOn,  
                 EMP.FullName        AS ModifiedBy,
                 VEN.GSTNo           AS GSTNo  
          FROM   tblvendor VEN  
                 LEFT JOIN tbl_City CITY  
                        ON CITY.ID = VEN.CityID  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = VEN.ModifiedBy  
          WHERE  VEN.ID = @ID  
                 AND VEN.DomainID = @DomainID  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
