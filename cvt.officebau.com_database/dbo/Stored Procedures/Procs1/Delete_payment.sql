﻿/****************************************************************************         
CREATED BY    : Ajith N      
CREATED DATE  : 31 May 2017      
MODIFIED BY   :       
MODIFIED DATE :       
 <summary>        
 </summary>                                 
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Delete_payment] (@ID              INT,      
                                        @TotalAmount     MONEY,      
                                        @PaymentModeID   INT,      
                                        @SessionID       INT,      
                                        @DomainID        INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          DECLARE @OUTPUT            VARCHAR(100),      
                  @TransactionTypeID INT =(SELECT ID      
                    FROM   tblMasterTypes      
                    WHERE  NAME = 'Make Payment')      
      Set @TotalAmount =(select totalAmount from tbl_Payment where id=@ID)
          IF EXISTS (SELECT 1      
                     FROM   tblbrs      
                     WHERE  SourceID = @ID      
                            AND SourceType = @TransactionTypeID      
                            AND IsDeleted = 0      
                            AND IsReconsiled = 1      
                            AND DomainID = @DomainID)      
            BEGIN      
                SET @Output = (SELECT [Message]      
                               FROM   tblErrorMessage      
                               WHERE  [Type] = 'Information'      
                                      AND Code = 'RCD_RECON'      
                                      AND IsDeleted = 0) --'The record has been Reconciled.'      
                GOTO finish      
            END      
          ELSE      
            BEGIN      
                IF( @PaymentModeID = (SELECT ID      
                                      FROM   tbl_CodeMaster      
                                      WHERE  Code = 'Cash' and IsDeleted = 0) )      
                  BEGIN      
                      UPDATE tblCashBucket      
                      SET    AvailableCash =  AvailableCash + @TotalAmount,      
                             ModifiedOn = Getdate()      
                      WHERE  DomainID = @DomainID      
      
                      UPDATE tblVirtualCash      
                      SET    IsDeleted = 1,      
                             ModifiedBy = @SessionID      
                      WHERE  SourceID = @ID      
                             AND SourceType =(SELECT ID      
                                                        FROM   tblMasterTypes      
                                                        WHERE  NAME = 'Make Payment')      
                             AND IsDeleted = 0      
                  END      
      
                UPDATE tbl_Payment      
                SET    IsDeleted = 1,      
                       ModifiedBy = @SessionID      
                WHERE  ID = @ID      
  Update tbl_PaymentMapping SET    IsDeleted = 1,      
                       ModifiedBy = @SessionID      
        Where PaymentId=@ID    
                UPDATE tblBookedBankBalance      
                SET    IsDeleted = 1,      
                       ModifiedBy = @SessionID      
                WHERE  BRSID = (SELECT ID      
                                FROM   tblBRS      
                                WHERE  SourceID = @ID      
                                       AND SourceType = @TransactionTypeID      
                                       AND IsDeleted = 0      
 AND DomainID = @DomainID)      
      
                UPDATE tblBRS      
                SET    IsDeleted = 1,      
                       ModifiedBy = @SessionID      
                WHERE  SourceID = @ID      
                       AND SourceType = @TransactionTypeID      
                       AND DomainID = @DomainID      
      
                EXEC Managesystembankbalance      
                  @DomainID      
      
                SET @Output = (SELECT [Message]      
                               FROM   tblErrorMessage      
                               WHERE  [Type] = 'Information'      
                                      AND Code = 'RCD_DEL'      
                                      AND IsDeleted = 0) -- 'Deleted Successfully'      
            END      
      
          FINISH:      
      
          SELECT @Output      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
