﻿/****************************************************************************     
CREATED BY   : Naneeshwar.M    
CREATED DATE  : 19-JAN-2018    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
      
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ManageApplicationConfiguration] (@ID             INT,    
                                                        @BusinessUnitID INT,    
                                                        @ConfigCode     VARCHAR(100),    
                                                        @ConfigValue    VARCHAR(100),    
                                                        @SessionID      INT,    
                                                        @DomainID       INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output   VARCHAR(8000),    
              @ConfigID INT=(SELECT ID    
                FROM   tbl_ConfigurationMaster    
                WHERE  Code = @ConfigCode)    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SET @Output = 'Operation Failed!'    
    
          IF( Isnull(@ID, 0) != 0 )    
            BEGIN    
                BEGIN    
                    UPDATE tbl_ApplicationConfiguration    
                    SET    ConfigValue = @ConfigValue,    
                           ModifiedBy = @SessionID,    
                           ModifiedOn = Getdate()    
                    WHERE  ID = @ID    
                           AND ConfigurationID = @ConfigID    
                           AND BusinessUnitID = @BusinessUnitID    
    
                    SET @Output = 'Updated Successfully.'    
    
                    GOTO Finish    
                END    
            END    
    
          IF( Isnull(@ID, 0) = 0 )    
            BEGIN    
                IF EXISTS(SELECT 1    
                          FROM   tbl_ApplicationConfiguration    
                          WHERE  @ID <> ID    
                                 AND ConfigurationID = @ConfigID    
                                 AND BusinessUnitID = @BusinessUnitID)    
                  BEGIN    
                      SET @Output = 'Already Exists.'    
    
                      GOTO Finish    
                  END    
                ELSE    
                  BEGIN    
                      INSERT INTO tbl_ApplicationConfiguration    
                                  (ConfigurationID,    
                                   BusinessUnitID,    
                                   ConfigValue,    
                                   CreatedBy,    
                                   CreatedOn,    
                                   ModifiedBy,    
                                   ModifiedOn)    
                      VALUES      ( @ConfigID,    
                                    @BusinessUnitID,    
                                    @ConfigValue,    
                                    @SessionID,    
                                    Getdate(),    
                                    @SessionID,    
                                    Getdate() )    
    
                      SET @Output = 'Inserted Successfully.'    
    
                      GOTO Finish    
                  END    
            END    
    
          FINISH:    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
