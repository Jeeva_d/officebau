﻿/**************************************************************************** 
CREATED BY			:	JENNIFER.S
CREATED DATE		:	07/06/2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
    [SearchRequestedLeaves] '2018-02-04','2018-02-05',4, 5
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchRequestedLeaves] (@FromDate DATE,
                                               @ToDate   DATE,
                                               @DomainID INT,
                                               @UserID   INT,
                                               @Type     VARCHAR(50) = NULL)
AS
  BEGIN
      SET NOCOUNT ON

      BEGIN TRY
          DECLARE @BaseLocationID INT,
                  @AppConfigValue INT,
                  @FromDay        VARCHAR(20) = (SELECT Datename(dw, @FromDate) DayofWeek),
                  @ToDay          VARCHAR(20) = (SELECT Datename(dw, @ToDate) DayofWeek),
                  @RegionIDs      VARCHAR(20) = (SELECT Cast(RegionID AS VARCHAR(10))
                     FROM   tbl_EmployeeMaster EM
                     WHERE  EM.ID = @UserID
                     FOR XML path(''))

          SELECT @BaseLocationID = BaseLocationID
          FROM   tbl_EmployeeMaster EM
          WHERE  EM.ID = @UserID

          SET @AppConfigValue = ISNULL((SELECT ISNULL(ConfigValue, 0) AS Value
                                        FROM   tbl_ApplicationConfiguration AC
                                               JOIN tbl_ConfigurationMaster CM
                                                 ON CM.ID = AC.ConfigurationID
                                                    AND cm.Code = 'LEVWKEND'
                                        WHERE  AC.BusinessUnitID = @BaseLocationID), 0)

          DECLARE @DateTable TABLE
            (
               [Date] DATE,
               [days] VARCHAR(20),
               [Type] VARCHAR(50)
            )
          DECLARE @Year INT = Year(Getdate());

          WITH CTE
               AS (SELECT @FromDate               AS All_Month_Date,
                          Datename(dw, @FromDate) AS [days]
                   UNION ALL
                   SELECT Dateadd(DD, 1, All_Month_Date),
                          Datename(dw, Dateadd(DD, 1, All_Month_Date)) AS [days]
                   FROM   CTE
                   WHERE  Dateadd(DD, 1, All_Month_Date) <= @ToDate)
          SELECT *
          INTO   #tempDates
          FROM   CTE
          OPTION( MAXRECURSION 0)

          IF( ISNULL(@Type, '') = 'view' )
            BEGIN
                INSERT INTO @DateTable
                SELECT t.*,
                       ( CASE
                           WHEN H.[Type] = 'Yearly' THEN
                             'Holiday'
                           WHEN H.[Type] = 'Weekly' THEN
                             'Week Off'
                           ELSE
                             H.[Type]
                         END ) AS [Type]
                FROM   #tempDates t
                       LEFT JOIN tbl_Holidays H
                              ON H.[YEAR] = @Year
                                 AND H.IsDeleted = 0
                                 AND H.RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%'
                                 AND H.DomainID = @DomainID
                                 AND t.All_Month_Date = H.[Date]
            END
          ELSE
            BEGIN
                IF EXISTS (SELECT [days]
                           FROM   #tempDates
                           WHERE  [days] IN ( 'Sunday', 'Saturday' ))
                  BEGIN
                      IF( @AppConfigValue = 1 )
                        BEGIN
                            INSERT INTO @DateTable
                            SELECT *,
                                   '' AS [Type]
                            FROM   #tempDates
                            WHERE  All_Month_Date NOT IN (SELECT [Date]
                                                          FROM   tbl_Holidays
                                                          WHERE  [YEAR] = @Year
                                                                 AND [Type] = 'Yearly'
                                                                 AND DomainID = @DomainID
                                                                 AND RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%'
                                                                 AND IsDeleted = 0)
                        END
                      ELSE
                        BEGIN
                            INSERT INTO @DateTable
                            SELECT *,
                                   '' AS [Type]
                            FROM   #tempDates
                            WHERE  All_Month_Date NOT IN (SELECT [Date]
                                                          FROM   tbl_Holidays
                                                          WHERE  [YEAR] = @Year
                                                                 --AND [Type] = 'Yearly'
                                                                 AND DomainID = @DomainID
                                                                 AND RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%'
                                                                 AND IsDeleted = 0)
                        END
                  END
                ELSE
                  BEGIN
                      INSERT INTO @DateTable
                      SELECT *,
                             '' AS [Type]
                      FROM   #tempDates
                      WHERE  All_Month_Date NOT IN (SELECT [Date]
                                                    FROM   tbl_Holidays
                                                    WHERE  [YEAR] = @Year
                                                           AND DomainID = @DomainID
                                                           AND RegionID LIKE '%,' + Cast(@RegionIDs AS VARCHAR(20)) + ',%'
                                                           AND IsDeleted = 0)
                  END
            END

          SELECT [Date]               RequestedDate,
                 Datename(dw, [Date]) RequestedDays,
                 (SELECT Count(1)
                  FROM   @DateTable)  NoOfDays,
                 [Type]
          FROM   @DateTable
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
