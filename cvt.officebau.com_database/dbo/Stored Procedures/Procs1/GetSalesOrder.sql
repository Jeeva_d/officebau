﻿/****************************************************************************                 
CREATED BY   :                 
CREATED DATE  :                
MODIFIED BY   :     
MODIFIED DATE  :              
 <summary>              
 [Getincome] 2,1              
 </summary>                                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetSalesOrder] (@ID       INT,
                                       @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT inv.ID                              AS ID,
                 inv.Date                            AS InvoiceDate,
                 inv.CustomerID                      AS CustomerID,
                 cus.NAME                            AS CustomerName,
                 inv.BillAddress                     AS BillingAddress,
                 cus.PaymentTerms                    AS PaymentTerms,
                 inv.Description                     AS [Description],
                 inv.SONo                            AS InvoiceNo,
                 ((SELECT ( Isnull(( Sum(Qty * Rate)
                                     + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) ), 0) )
                   FROM   tblSOItem
                   WHERE  SOID = @ID
                          AND IsDeleted = 0
                          AND DomainID = @DomainID)) AS TotalAmount,
                 cur.NAME                            AS Currency,
                 inv.CurrencyRate,
                 inv.ModifiedOn                      AS ModifiedOn,
                 EMP.FullName                        AS ModifiedBy,
                 C.FullName                          AS CompanyName,
                 Cast(C.Logo AS VARCHAR(50))         AS Logo,
                 C.Address                           AS CompanyAddress,
                 ''                                  AS STATENAME,
                 c.EmailID                           AS CompanyEmaild,
                 c.GSTNo                             AS CompanyGSTNO,
                 c.ContactNo                         AS CompanyContactNo,
                 cus.GSTNo                           AS CustomerGSTNo,
                 Isnull(s1.NAME, 'N/A')              AS CustomerState,
                 Isnull( (SELECT Toid FROM tblemailConfig WHERE DomainID = @DomainID AND [Key] = 'SendInvoice'), '')
                 + ',' + Isnull(cus.CustomerEmailid, '') + ','
                 + Isnull(cus.EmailId, '')           AS ToEmail,
                 Isnull( (SELECT CC FROM tblemailConfig WHERE DomainID = @DomainID AND [Key] = 'SendInvoice'), '')
                 + ',' + c.EmailId                   AS CCEmail,
                 (SELECT BCC
                  FROM   tblemailConfig
                  WHERE  DomainID = @DomainID
                         AND [Key] = 'SendInvoice')  AS BCCEmail,
                 C.PaymentFavour                     AS PaymentFavour
          FROM   tblSalesOrder inv
                 LEFT JOIN tblCustomer cus
                        ON cus.ID = inv.CustomerID
                 LEFT JOIN tblCurrency cur
                        ON cur.id = cus.CurrencyID
                 LEFT JOIN tbl_EmployeeMaster EMP
                        ON EMP.ID = inv.ModifiedBy
                 LEFT JOIN tbl_Company C
                        ON C.ID = INV.DomainID
                 LEFT JOIN tbl_FileUpload fu
                        ON fu.Id = c.Logo
                 LEFT JOIN tbl_city ss
                        ON ss.ID = cus.cityid
                 LEFT JOIN tbl_State s1
                        ON s1.ID = ss.StateID
          WHERE  inv.IsDeleted = 0
                 AND ( inv.ID = @ID
                        OR @ID IS NULL )
                 AND inv.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
