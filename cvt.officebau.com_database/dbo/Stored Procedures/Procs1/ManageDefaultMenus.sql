﻿/**************************************************************************** 
CREATED BY			:	Ajith N
CREATED DATE		:	16 Oct 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
		ManageDefaultMenus 1771, 1769, 6
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageDefaultMenus] (@EmployeeID INT,
                                            @ModifiedBy INT,
                                            @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(50) = 'Operation Failed!'

      BEGIN TRY
          BEGIN TRANSACTION

          INSERT INTO tbl_RBSUserMenuMapping
                      (EmployeeID,
                       MenuID,
                       MRead,
                       MWrite,
                       MEdit,
                       MDelete,
                       DomainID,
                       CreatedBy,
                       ModifiedBy)
          SELECT @EmployeeID,
                 ID,
                 1,
                 1,
                 1,
                 1,
                 @DomainID,
                 @ModifiedBy,
                 @ModifiedBy
          FROM   tbl_RBSMenu
          WHERE  Isdeleted = 0
                 AND DomainID = @DomainID
                 AND MenuCode IN ( 'PROFILE', 'MISSEDPUNCHREQ', 'LEAVEMYATTEN', 'REQLEAVE',
                                   'HRPOLICY', 'COMPOFFREQ', 'CLAIMREQ', 'HRADEC',
                                   'TDSDEC', 'TDSEMP', 'HOLIDYLIST', 'MYSKILL' )

          SET @Output = 'RBS Menu Inserted Successfully.'

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
