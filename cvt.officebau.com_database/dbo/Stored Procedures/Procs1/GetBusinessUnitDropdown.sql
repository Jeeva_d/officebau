﻿/****************************************************************************         
CREATED BY		: Jennifer.S    
CREATED DATE	: 21-JUN-2017
MODIFIED BY		:     
MODIFIED DATE   :       
 <summary>                
  [GetBusinessUnitDropdown] 1,1
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetBusinessUnitDropdown] (@UserID   INT,
                                                 @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT BUS.ID   ID,
                 BUS.NAME NAME
          FROM   tbl_BusinessUnit BUS
                 JOIN (SELECT *
                       FROM   dbo.Splitstring ((SELECT BusinessUnitID
                                                FROM   tbl_EmployeeMaster
                                                WHERE  ID = @UserID
                                                       AND DomainID = @DomainID), ',')
                       WHERE  Isnull(Item, '') <> '') i
                   ON i.Item = BUS.ID
          WHERE  BUS.IsDeleted = 0
                 AND BUS.DomainID = @DomainID
                 AND Isnull(BUS.ParentID, 0) <> 0
          ORDER  BY BUS.NAME

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END

