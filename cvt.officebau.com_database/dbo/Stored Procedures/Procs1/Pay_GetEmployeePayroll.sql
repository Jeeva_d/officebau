﻿ 
 
 
  /****************************************************************************                                           
CREATED BY      :                                                  
CREATED DATE  :                                                   
MODIFIED BY   :                                                 
MODIFIED DATE  :                                                
<summary>                                                    
[Pay_GetEmployeePayroll_temp] 0,96,1,2,1                           
[Pay_GetEmployeePayroll] 0,96,1,2,1                                                
                                              
</summary>                                                                           
*****************************************************************************/          
CREATE PROCEDURE [dbo].[Pay_GetEmployeePayroll] (@ID                     INT = 16,          
                                                @EmployeePayStructureId INT = 61,          
                                                @MonthId                INT = 10,          
                                                @Year                   INT = 4,          
                                                @DomainID               INT = 1)          
AS          
  BEGIN          
      SET NOCOUNT ON;          
          
      BEGIN TRY          
          BEGIN          
              DECLARE @Formula VARCHAR(MAX)          
          
                     
          
              SET @Year = (SELECT NAME          
                           FROM   tbl_FinancialYear          
                           WHERE  Id = @Year          
                                  AND Isdeleted = 0          
                                  AND DomainId = @DomainID);          
          
              DECLARE @CurrentMonth DATE = CONVERT(VARCHAR(10), @MonthId) + '-' + '1' + '-'          
                + CONVERT(VARCHAR(10), @Year);          
              DECLARE @Workdays INT = Datediff(DAY, @CurrentMonth, Dateadd(MONTH, 1, @CurrentMonth));          
          
              IF @ID = 0          
                BEGIN          
                    DECLARE @Payroll PAYROLL          
          
                    INSERT INTO @Payroll          
                    SELECT 0        AS Id,          
                           tppc.Id  ComponentId,          
                           tpeps.Id AS EmployeePayStructureID,          
                           CASE          
                             WHEN ( tppc.Description <> 'Gross' ) THEN          
                               ( CASE          
                                   WHEN ( tppc.Description = 'TAX' ) THEN          
                                     tds.TDSAmount          
                                   WHEN ( tppc.Code = 'LOANDED' ) THEN          
                                     Isnull(Isnull(LA.Amount, 0) + ( CASE          
                       WHEN Isnull(LA.InterestAmount, 0) = 0 THEN          
                                                                         0          
                                                                   ELSE          
                                                                         Isnull(LA.InterestAmount, 0)          
                                                                     END ) + Isnull((SELECT ob.DueBalance          
                                                                                     FROM   tbl_LoanAmortization ob          
                                                                                     WHERE  ob.LoanRequestID = l.ID          
                       AND ob.IsDeleted = 0          
                                                                                            AND ob.DomainID = @DomainID          
                                                                                            AND Month(ob.DueDate) = Month(Dateadd(month, -1, @CurrentMonth))          
                                                            AND Year(ob.DueDate) = Year(Dateadd(month, -1, @CurrentMonth))), 0), 0)          
                                   ELSE          
                                     Cast(tpepsd.Amount AS DECIMAL(18, 2))          
                                 END )          
                             ELSE          
                               Cast(tpepsd.Amount AS DECIMAL(18, 2))          
                           END      Amount,          
                           tpeps.EmployeeId          
                    FROM   tbl_Pay_EmployeePayStructureDetails AS tpepsd          
                           JOIN tbl_Pay_EmployeePayStructure AS tpeps          
                             ON tpeps.Id = tpepsd.PayStructureId          
                           JOIN tbl_Pay_PayrollCompontents AS tppc          
                             ON tppc.Id = tpepsd.ComponentId          
                           LEFT JOIN tbl_LoanRequest l          
                                  ON l.EmployeeID = tpeps.EmployeeID          
                                     AND l.StatusID = (SELECT ID          
                                                       FROM   tbl_Status          
                                                       WHERE  Type = 'Loan'          
                                                              AND Code = 'Settled')          
                           LEFT JOIN tbl_LoanAmortization la          
                                  ON la.LoanRequestID = l.ID          
                                     AND Month(la.DueDate) = Month(@CurrentMonth)          
                                     AND Year(la.DueDate) = Year(@CurrentMonth)          
          AND la.IsDeleted = 0          
                                     AND la.StatusID = (SELECT ID          
                                                        FROM   tbl_Status          
                                                        WHERE  Type = 'Amortization'          
                                                               AND Code = 'Open')          
                           LEFT JOIN tbl_tds TDS          
                                  ON TDS.employeeid = tpeps.employeeid          
                                     AND tds.monthid = @MonthId          
                                     AND tds.yearid = (SELECT id          
                                                       FROM   tbl_financialyear          
                                                       WHERE  isdeleted = 0          
                                                              AND NAME = @YEAR          
                                                              AND domainid = @DomainID)          
                                     AND TDS.isdeleted = 0          
                    WHERE  tpeps.DomainId = @DomainID          
                           AND tpepsd.PayStructureId = @EmployeePayStructureId          
                           AND tpeps.Isdeleted = 0          
                           AND tpepsd.ISdeleted = 0;          
          
                    SELECT tppc.Id,          
                           tppc.Code,          
                           tppc.Description,          
                           tppc.Ordinal,          
                           tppc.Id  ComponentId,          
                           tppcps.value,          
                           CASE          
                             WHEN ( tppc.Description = 'Gross' ) THEN          
                               Cast(tpepsd.Amount AS VARCHAR(MAX))          
                             ELSE          
                               CASE          
                                 WHEN ( tppcps.IsEditable = 1 ) THEN          
                                   Cast(ISNULL(p.Amount, 0) AS VARCHAR(MAX))          
                                 WHEN ( tppc.Code = 'LOANDED' ) THEN          
                                   Cast(ISNULL(p.Amount, 0) AS VARCHAR(MAX))          
                                 ELSE          
                                   ISNULL(tppcps.value, '')          
                               END          
                           END      AS Amount,          
                        0        AS HasUpdated,          
                           tppcps.IsEditable,          
                           tppcps.Fixed,          
                           tpeps.EmployeeID,          
                           tpeps.ID AS EmployeePayStructureID          
                    INTO   #tblcompanypay          
                    FROM   tbl_Pay_PayrollCompontents AS tppc          
                           LEFT JOIN @Payroll p          
                                  ON tppc.Id = p.ComponentId          
                           LEFT JOIN tbl_Pay_EmployeePayStructure AS tpeps          
                                  ON tpeps.Id = p.EmployeePayStructureID          
                           LEFT JOIN tbl_Pay_PayrollCompanyPayStructure AS tppcps          
                                  ON tppc.Id = tppcps.ComponentId          
                                     AND tppcps.IsDeleted = 0          
                                     AND tppcps.CompanyPayStructureID = tpeps.CompanyPayStructureID          
                           JOIN tbl_Pay_EmployeePayStructureDetails AS tpepsd          
                             ON tpeps.Id = tpepsd.PayStructureId          
                                AND tpepsd.ComponentId = tppcps.ComponentId          
                                AND tpepsd.Isdeleted = 0          
                    WHERE  tppc.Isdeleted = 0          
                           AND tppc.DomainId = @DomainID          
          
                    DECLARE @LOP DECIMAL =(SELECT ( CASE          
                                 WHEN ( Datepart(MM, DOJ) = @MonthId          
                                        AND Datepart(YYYY, DOJ) = @Year ) THEN          
                                   ( @Workdays - ( Datepart(D, DOJ) - 1 ) - Isnull(LopDays, 0) )          
                                 ELSE          
                                   @Workdays - Isnull(LopDays, 0)          
                               END ) - ( CASE          
                                           WHEN ( Datepart(MM, InactiveFrom) = @MonthId          
                                                  AND Datepart(YYYY, InactiveFrom) = @Year ) THEN          
                                             ( ( @Workdays ) - Day(InactiveFrom) )          
                                           ELSE          
                                             0          
                                         END )          
                      FROM   tbl_employeemaster ep          
                             LEFT JOIN tbl_LopDetails P          
                                    ON P.EmployeeID = ep.ID          
                                       AND P.DomainID = @DomainID          
                                       AND P.MonthId = @MonthId          
                                       AND P.year = @Year          
                      WHERE  ep.ID = (SELECT TOP 1 employeeID          
                                      FROM   #tblcompanypay))          
                  DECLARE @IDs   INT,          
                            @count INT = (SELECT Count(1)          
                               FROM   #tblcompanypay);          
          
                    --UPDATE #tblcompanypay          
                    --SET    amount = '(' + Cast(amount AS VARCHAR(max)) + '*('          
                    --           + Cast(@LOP AS VARCHAR) + '/'          
                    --                + Cast(Cast(@Workdays AS DECIMAL(16, 1))AS VARCHAR)          
                    --                + '))'          
                    --WHERE  Fixed = 1          
          
                    ----------------------------------------                                      
                    WHILE ( @count <> 0 )          
                      BEGIN          
             SET @IDs = (SELECT TOP 1 Id          
                                      FROM   #tblcompanypay          
                                      WHERE  hasupdated = 0          
                                      ORDER  BY Ordinal);          
          
                          -----------------------------------------                                      
                          UPDATE #tblcompanypay          
                          SET    hasupdated = 1          
                          WHERE  Id = @IDs;          
          
                          UPDATE p          
                          SET    Amount = Replace(P.Amount, (SELECT Code          
                                                             FROM   #tblcompanypay          
                                                             WHERE  Id = @IDs), (SELECT Amount          
                                                                                 FROM   #tblcompanypay          
                                                                                 WHERE  Id = @IDs))          
                          FROM   #tblcompanypay P Where p.Fixed = 1;          
          
                          -----------------------                                      
                          SET @count = @count - 1;          
                      END;          
          
                    SET @count = (SELECT Count(1)          
                                  FROM   #tblcompanypay);          
          
                    WHILE ( @count <> 0 )          
                      BEGIN          
                          SET @IDs = (SELECT TOP 1 Id          
                                      FROM   #tblcompanypay          
                                      WHERE  hasupdated = 1          
                                        and Amount NOT LIKE '%[a-zA-Z][a-zA-Z]%');          
      IF @IDs is null  
        Set @IDs= (Select ID FROM   #tblcompanypay      
                               WHERE  hasupdated = 1 and amount like '%Case %')  
                   

                          UPDATE #tblcompanypay          
                          SET    hasupdated = 0          
                          WHERE  Id = @IDs;          
          
                          SET @Formula = (SELECT Amount          
                                          FROM   #tblcompanypay          
                                          WHERE  Id = @IDs);          
          
                          DECLARE @Input VARCHAR(MAX) = '(' + @Formula + ')';          
                          DECLARE @output        VARCHAR(MAX),          
                                  @resultdecimal NVARCHAR(MAX);          
                          DECLARE @i INT = 0;          
          
                          WHILE @i < Len(@Input)          
                            BEGIN          
                                SET @i = @i + 1;          
          
                                IF ( dbo.isReallyNumeric(Substring(@Input, @i, 1)) IS NOT NULL )          
                                  BEGIN          
                                      SET @output = ISNULL(@output, '') + Substring(@Input, @i, 1);          
                                  END;          
                                ELSE          
                                  BEGIN          
                                      SET @resultdecimal = ISNULL(@resultdecimal, '')          
                                                           + ISNULL(@output, '') + Substring(@Input, @i, 1);          
                                      SET @output = '';          
                                  END;          
  END;          
          
                          SET @Formula = @resultdecimal;          
                          SET @resultdecimal = '';          
          
                          DECLARE @sql NVARCHAR(MAX);          
                          DECLARE @result DECIMAL(18, 2);          
          
                          SET @sql = N'set @result = ' + @Formula;          
          
                          EXEC sp_executesql          
                            @sql,          
                            N'@result DECIMAL(16,3) output',          
                            @result OUT;          
          
                          UPDATE #tblcompanypay          
                          SET    Amount =Case when (fixed=1 and @WorkDays<>0) Then @result * @lop/@WorkDays Else @result END                
                          WHERE  Id = @IDs;        
                
          UPDATE p          
                    SET    p.amount = Replace(P.amount, (SELECT Code          
                                                      FROM   #tblcompanypay          
                                                       WHERE  ID = @IDs), (SELECT amount          
                                                             FROM   #tblcompanypay          
                                                                          WHERE  ID = @IDs))          
                    FROM   #tblcompanypay P where p.Fixed = 0;            
          
                          SET @count = @count - 1;          
                      END;          
          
                    SELECT 0                              ID,          
                           EmployeeID,          
                           EmployeePayStructureID,          
                           a.ComponentId,          
                           Description,          
                           IsEditable,          
                          Round( Cast(Amount AS DECIMAL(18, 2)),0) AS Amount,          
                           b.Type,          
                           a.Code,          
                           ''                             AS Remarks,          
                           ( CASE          
                               WHEN a.Code = 'LOANDED' THEN          
                                 ISNULL(AC.ConfigValue, '0')          
                               ELSE          
                                 '0'          
                             END )                        AS ConfigValue          
                    FROM   #tblcompanypay a          
                           LEFT JOIN tbl_Pay_PaystubConfiguration b          
                                  ON a.ComponentId = b.ComponentID          
                                     AND b.IsDeleted = 0          
                           JOIN tbl_EmployeeMaster EM          
                             ON EM.ID = a.EmployeeID          
                           LEFT JOIN tbl_ApplicationConfiguration AC          
                                  ON AC.BusinessUnitID = EM.BaseLocationID          
                                     AND AC.ConfigurationID = (SELECT ID          
                                                               FROM   tbl_ConfigurationMaster          
                                                               WHERE  Code = 'LOANPAYR')          
                    ORDER  BY a.Ordinal ASC,          
                              b.Type DESC;          
                END          
          
              IF @ID != 0          
                BEGIN          
                    SELECT tppd.ID                     AS Id,          
                           tpep.EmployeePayStructureID AS EmployeePayStructureID,          
                           tpep.EmployeeId,          
                           tppc.Id                     ComponentId,          
                           tppc.Description,          
                           Round(tppd.Amount ,0)                AS Amount,          
                           tppcps.IsEditable,          
                           b.Type,          
                           tppc.Code,          
                           tpep.Remarks,          
                           ( CASE          
                               WHEN tppc.Code = 'LOANDED' THEN          
                                 ISNULL(AC.ConfigValue, '0')          
                               ELSE          
                                 '0'          
                             END )     AS ConfigValue          
                    FROM   tbl_Pay_EmployeePayroll AS tpep          
                           JOIN tbl_Pay_EmployeePayrollDetails AS tppd          
                             ON tppd.PayrollId = tpep.Id          
                           JOIN tbl_Pay_PayrollCompontents AS tppc          
                             ON tppc.Id = tppd.ComponentId          
                           JOIN tbl_Pay_PayrollCompanyPayStructure AS tppcps          
     ON tppcps.ComponentId = tppc.Id          
                                AND tppcps.isdeleted = 0          
                                AND tppcps.CompanyPayStructureID = (SELECT tpeps.CompanyPayStructureID          
                                                                    FROM   tbl_Pay_EmployeePayStructure AS tpeps          
                                                                    WHERE  Id = tpep.EmployeePayStructureID)          
                           LEFT JOIN tbl_Pay_PaystubConfiguration b          
                                  ON tppc.Id = b.ComponentID          
                                     AND b.IsDeleted = 0          
                           JOIN tbl_EmployeeMaster EM          
                             ON EM.ID = tpep.EmployeeID          
                           LEFT JOIN tbl_ApplicationConfiguration AC          
                                  ON AC.BusinessUnitID = EM.BaseLocationID          
                                     AND AC.ConfigurationID = (SELECT ID          
                                                               FROM   tbl_ConfigurationMaster          
                                                               WHERE  Code = 'LOANPAYR')          
                    WHERE  tpep.Id = @ID          
                    ORDER  BY tppc.Ordinal ASC,          
                              b.Type DESC;          
                END          
          END          
      END TRY          
      BEGIN CATCH          
          DECLARE @ErrorMsg    VARCHAR(100),          
                  @ErrSeverity TINYINT          
          SELECT @ErrorMsg = ERROR_MESSAGE(),          
                 @ErrSeverity = ERROR_SEVERITY()          
          RAISERROR (@ErrorMsg,@ErrSeverity,1)          
      END CATCH          
  END
