﻿/****************************************************************************       
CREATED BY   :       
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
<summary>              
 [Getcomparecostrevenuecenter] null, null, null, 1    
</summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Getcomparecostrevenuecenter] (@StartDate DATETIME = NULL,  
                                                     @EndDate   DATETIME = NULL,  
                                                     @Name      VARCHAR(100)= NULL,  
                                                     @DomainID  INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @FinancialYear INT;  
          DECLARE @StartFinancialMonth INT;  
          DECLARE @EndFinancialMonth INT;  
  
          IF ( @StartDate IS NULL  
                OR @StartDate = '' )  
              OR ( @EndDate IS NULL  
                    OR @EndDate = '' )  
            BEGIN  
                SET @StartFinancialMonth = (SELECT Value  
                                            FROM   tblApplicationConfiguration  
                                            WHERE  Code = 'STARTMTH'  
                                                   AND DomainID = @DomainID)  
                SET @EndFinancialMonth = ( ( (SELECT Value  
                                              FROM   tblApplicationConfiguration  
                                              WHERE  Code = 'STARTMTH'  
                                                     AND DomainID = @DomainID)  
                                             + 11 ) % 12 )  
  
                IF( Month(Getdate()) <= ( ( (SELECT Value  
                                             FROM   tblApplicationConfiguration  
                                             WHERE  Code = 'STARTMTH'  
                                                    AND DomainID = @DomainID)  
                                            + 11 ) % 12 ) )  
                  SET @FinancialYear = Year(Getdate()) - 1  
                ELSE  
                  SET @FinancialYear = Year(Getdate())  
  
                SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date        
                SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date       
            END  
  
          SELECT ct.NAME                                                                                                   AS [Cost Center],  
                 (SELECT Sum(Isnull(VI.Amount, 0))  
                       --  + Sum(Isnull(VI.SGSTAmount, 0))  
                       --  + Sum(Isnull(VI.CGSTAmount, 0))  
                  FROM   Vw_Income VI  
                  WHERE  vi.RevenueCenterID = ct.ID  
                         AND VI.IsDeleted = 0  
                         AND ( @StartDate = ''  
                                OR VI.Date >= @StartDate )  
                         AND ( @EndDate = ''  
                                OR VI.Date <= @EndDate ))                                                                  AS Receivables,  
                 (SELECT Sum(Isnull(Amount*QTY, 0))+Sum(SGST+CGST)  
                  FROM   tblExpenseDetails ev 
				  Join tblExpense e on ev.ExpenseID = e.ID 
                  WHERE  e.costcenterID = ct.ID  
                         AND ev.IsDeleted = 0  
                         AND ( @StartDate = ''  
                                OR e.Date >= @StartDate )  
                         AND ( @EndDate = ''  
                                OR e.Date <= @EndDate ))                                                                  AS Payable,  
                 Isnull((SELECT Sum(Isnull(VI.Amount, 0))  
                            --    + Sum(Isnull(VI.SGSTAmount, 0))  
                            --   + Sum(Isnull(VI.CGSTAmount, 0))  
                         FROM   Vw_Income VI  
     WHERE  vi.RevenueCenterID = ct.ID  
                                AND VI.IsDeleted = 0  
                                AND ( @StartDate = ''  
                                       OR VI.Date >= @StartDate )  
                                AND ( @EndDate = ''  
                                       OR VI.Date <= @EndDate )), 0) - Isnull((SELECT Sum(ev.Amount)  
                                                                               FROM   Vw_Expense ev  
                                                                               WHERE  ev.costcenterID = ct.ID  
                                                                                      AND ev.IsDeleted = 0  
                                                                                      AND ( @StartDate = ''  
                                                                                             OR ev.Date >= @StartDate )  
                                                                                      AND ( @EndDate = ''  
                                                                                             OR ev.Date <= @EndDate )), 0) AS TotalAmount  
          INTO   #temp  
          FROM   tblCostCenter ct  
                 LEFT JOIN tblExpense e  
                        ON e.CostCenterID = ct.ID  
                           AND e.IsDeleted = 0  
                 LEFT JOIN tblInvoice i  
                        ON ct.ID = i.RevenueCenterID  
                           AND i.IsDeleted = 0  
          WHERE  ct.DomainID = @DomainID  
                 AND ( ( @Name IS NULL  
                          OR @Name = '' )  
                        OR ct.NAME = @Name )  
                 AND ct.IsDeleted = 0  
          GROUP  BY ct.NAME,  
                    ct.ID  
  
          SELECT *  
          FROM   #temp  
          WHERE  TotalAmount <> 0  
                  OR ( Payable <> NULL  
                       AND Receivables <> NULL )  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
