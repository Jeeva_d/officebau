﻿/****************************************************************************                             
CREATED BY   : Dhanalakshmi. S                            
CREATED DATE  :   20-11-2018                           
MODIFIED BY   :                         
MODIFIED DATE  :                            
 <summary>                          
      [SearchStockMaintenance] 0, 1                  
 </summary>                                                     
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[SearchStockMaintenance] (@ProductID INT,      
                                                 @DomainID  INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          SELECT P.ID                        AS ProductID,      
                 P.NAME                      AS ProductName,      
                 Isnull(I.StockonHand, 0)  AS StockonHand,      
                 Isnull(I.CommitedStock, 0) AS CommitedStock,      
                 Isnull(I.AvailableStock, 0)  AS AvailableStock,     
                 U.NAME                      AS UOM,      
                 MFG  AS Manufacturer      
          FROM   tblProduct_V2 P      
                 LEFT JOIN VW_Inventory I      
                        ON I.ProductID = P.ID      
                 LEFT JOIN tbl_UOM U      
                        ON U.ID = p.UOMID      
          WHERE  P.IsDeleted = 0      
                 AND P.DomainID = @DomainID    
                 AND P.IsInventroy = 1   
                 AND ( @ProductID = 0      
                        OR P.ID = @ProductID )      
          ORDER  BY P.NAME ASC      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
