﻿/****************************************************************************             
CREATED BY  :  Jeeva D    
CREATED DATE :      
MODIFIED BY  :           
MODIFIED DATE   :            
 <summary>          
 [ManageCompanyPayStructure] 0 ,'Com1',40,20,12.5,16,11.5,1,1             
 </summary>                                     
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManageCompanyPayStructure](@ID                 INT,
                                                  @BusinessUnitID     INT,
                                                  @Name               VARCHAR(100),
                                                  @Basic              DECIMAL(5, 2),
                                                  @HRA                DECIMAL(5, 2),
                                                  @MedicalAllowance   MONEY,
                                                  @Conveyance         MONEY,
                                                  @EducationAllowance MONEY,
                                                  @PaperMagazine      MONEY,
                                                  @MonsoonAllow       MONEY,
                                                  @EffectiveFrom      DATE,
                                                  @DomainID           INT,
                                                  @LoginId            INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Result VARCHAR(100) = 'Operation Failed!'

          IF( Isnull(@ID, 0) = 0 )
            BEGIN
                IF( (SELECT Count(1)
                     FROM   tbl_CompanyPayStructure
                     WHERE  NAME = @Name
                            AND IsDeleted = 0
                            AND BusinessUnitID = @BusinessUnitID) = 1 )
                  BEGIN
                      SET @Result = 'Paystructure Name Already Exists!'

                      GOTO OutPutResult
                  END
                ELSE
                  BEGIN
                      INSERT INTO tbl_CompanyPayStructure
                                  (NAME,
                                   Basic,
                                   HRA,
                                   MedicalAllowance,
                                   Conveyance,
                                   EducationAllow,
                                   MagazineAllow,
                                   MonsoonAllow,
                                   BusinessUnitID,
                                   DomainId,
                                   CreatedBy,
                                   CreatedOn,
                                   ModifiedBy,
                                   ModifiedOn,
                                   EffectiveFrom)
                      SELECT @Name,
                             @Basic,
                             @HRA,
                             @MedicalAllowance,
                             @Conveyance,
                             @EducationAllowance,
                             @PaperMagazine,
                             @MonsoonAllow,
                             @BusinessUnitID,
                             @DomainID,
                             @LoginId,
                             Getdate(),
                             @LoginId,
                             Getdate(),
                             @EffectiveFrom

                      SET @Result = 'Inserted Successfully.'

                      GOTO OutPutResult
                  END
            END
          ELSE
            BEGIN
                -- TODO : Need to check with Employee PS.    
                IF( (SELECT Count(1)
                     FROM   tbl_CompanyPayStructure
                     WHERE  NAME = @Name
                            AND Id <> @ID
                            AND IsDeleted = 0
                            AND BusinessUnitID = @BusinessUnitID) = 1 )
                  BEGIN
                      SET @Result = 'Paystructure Name Already Exists!'

                      GOTO OutPutResult
                  END
                ELSE
                  BEGIN
                      IF EXISTS(SELECT 1
                                FROM   tbl_EmployeePayStructure
                                WHERE  CompanyPayStubId = @ID
                                       AND IsDeleted = 0)
                        BEGIN
                            SET @Result = 'Paystructure has begin used in the Employee Paystructure!'

                            GOTO OutPutResult
                        END
                      ELSE
                        BEGIN
                            UPDATE tbl_CompanyPayStructure
                            SET    NAME = @Name,
                                   Basic = @Basic,
                                   HRA = @HRA,
                                   MedicalAllowance = @MedicalAllowance,
                                   Conveyance = @Conveyance,
                                   EducationAllow = @EducationAllowance,
                                   MagazineAllow = @PaperMagazine,
                                   MonsoonAllow = @MonsoonAllow,
                                   BusinessUnitID = @BusinessUnitID,
                                   DomainId = @DomainID,
                                   ModifiedBy = @LoginId,
                                   ModifiedOn = Getdate(),
                                   EffectiveFrom = @EffectiveFrom
                            WHERE  ID = @ID

                            SET @Result = 'Updated Successfully.'

                            GOTO OutPutResult
                        END
                  END
            END

          OUTPUTRESULT:

          SELECT @Result AS Result

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
