﻿/****************************************************************************               
CREATED BY   : Ajith N              
CREATED DATE  : 31 Aug 2018              
MODIFIED BY   :               
MODIFIED DATE  :               
 <summary>                      
   [Rpt_NewlyHireEmployeeList]  '' , '', '', 1,1              
 </summary>                                        
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[Rpt_NewlyHireEmployeeList] (@DepartmentID  INT,        
                                                   @DesignationID INT,        
                                                   @BusinessUnit  VARCHAR(20),        
                                                   @UserID        INT,        
                                                   @DomainID      INT)        
AS        
  BEGIN        
      SET NOCOUNT ON;        
        
      BEGIN TRY        
          DECLARE @BusinessUnitIDs VARCHAR(50)        
        
          IF( Isnull(@BusinessUnit, 0) = 0 )        
            SET @BusinessUnitIDs = (SELECT BusinessUnitID        
                                    FROM   tbl_EmployeeMaster        
                                    WHERE  ID = @UserID        
                                           AND DomainID = @DomainID)        
          ELSE        
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','        
        
          DECLARE @BusinessUnitTable TABLE        
            (        
               BusinessUnit VARCHAR(100)        
            )        
        
          INSERT INTO @BusinessUnitTable        
          SELECT @BusinessUnitIDs        
        
          SELECT EA.TempEmployeeCode                        AS [Temporary No],        
                 EM.FullName                                AS [Employee Name],        
                 REGION.NAME                                AS [Region],        
                 DEP.NAME                                   AS [Department],        
                 DESI.NAME                                  AS [Designation],        
                 EM.EmailID                                 AS [Email ID],        
                 Isnull(EM.ContactNo, '')                   AS [Contact No],        
                 Isnull(EM.EmergencyContactNo, '')          AS [Emergency Mobile No],        
                 Cast(em.doj AS DATE)                                     AS [Date of Joining],        
                 GEN.Code                                   AS [Gender],        
                 PER.DateOfBirth AS [Date of Birth],        
                 BAND.NAME                                  AS [Band],        
                 GRADE.NAME                                 AS [Grade],        
                 EMPLOYEETYPE.NAME                          AS [Employment Type],        
                 EM.Qualification                           AS [Educational Qualification],        
                  (SELECT TOP 1 PSD.Amount      
                      FROM  tbl_pay_employeepaystructure EPS 
					  join tbl_Pay_EmployeePayStructureDetails PSD
						on psd.PayStructureId = eps.ID and PSD.DomainId = EPS.DomainID and PSD.Isdeleted = 0
					  join    tbl_Pay_PayrollCompontents PC 
						on PSD.ComponentId = pc.ID AND PSD.DomainID = pc.DomainID and pc.IsDeleted = 0
                      WHERE  EPS.employeeID = EM.ID   AND
					     EPS.IsDeleted = 0 and pc.Code = 'GRO'
                      ORDER  BY EPS.EffectiveFrom DESC)                                   AS [Gross Salary],  
                 BU.NAME                                    AS [Business Unit]        
          FROM   tbl_EmployeeMaster EM        
                 JOIN tbl_EmployeeApproval EA        
                   ON EA.EmployeeID = EM.ID        
                      AND EA.SecondApproverId IS NULL        
                 LEFT JOIN tbl_Department DEP        
                        ON DEP.ID = EM.DepartmentID        
                 LEFT JOIN tbl_Designation DESI        
                        ON DESI.ID = EM.DesignationID        
                 LEFT JOIN tbl_BusinessUnit BU        
                        ON BU.ID = EM.BaseLocationID        
                 LEFT JOIN tbl_BusinessUnit REGION        
                        ON REGION.ID = EM.RegionID        
                 LEFT JOIN tbl_EmployementType EMPLOYEETYPE        
                        ON EMPLOYEETYPE.ID = EM.EmploymentTypeID        
                 LEFT JOIN tbl_EmployeeMaster REPORTING        
      ON REPORTING.ID = EM.ReportingToID        
                 LEFT JOIN tbl_EmployeeMaster REPORTINGTO        
 ON REPORTINGTO.ID = EM.ReportingToID        
                 LEFT JOIN tbl_EmployeePersonalDetails PER        
                        ON PER.EmployeeID = EM.ID        
                 LEFT JOIN tbl_CodeMaster GEN        
                        ON GEN.ID = EM.GenderID        
                 LEFT JOIN tbl_Band BAND        
                        ON BAND.ID = EM.BandID        
                 LEFT JOIN tbl_EmployeeGrade GRADE        
                        ON GRADE.ID = em.GradeID        
                 --LEFT JOIN tbl_employeepaystructure PS        
                 --       ON PS.EmployeeId = EM.ID        
                 --          AND PS.Id = (SELECT TOP 1 ID        
                 --                       FROM   tbl_employeepaystructure        
                 --                       WHERE  EmployeeId = EM.ID        
                 --                              AND IsDeleted = 0        
                 --                       ORDER  BY EffectiveFrom DESC)        
                 LEFT JOIN tbl_BloodGroup BG        
                        ON BG.ID = PER.BloodGroupID        
                 LEFT JOIN tbl_CodeMaster MS        
                        ON MS.ID = PER.MaritalStatusID        
                 LEFT JOIN tbl_EmployeeOtherDetails OTHER        
                        ON OTHER.EmployeeID = em.ID        
                 JOIN @BusinessUnitTable TBU        
                   ON TBU.BusinessUnit LIKE '%,'        
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))        
                                            + ',%'        
          WHERE  EM.IsDeleted = 0        
                 AND EM.IsActive = 1        
                 AND ( @BusinessUnit = 0        
                        OR EM.BaseLocationID = @BusinessUnit )        
                 AND EM.domainID = @DomainID        
                 AND ( Isnull(@DepartmentID, '') = ''        
                        OR DEP.ID = @DepartmentID )        
                 AND ( Isnull(@DesignationID, '') = ''        
                        OR DESI.ID = @DesignationID )        
          ORDER  BY EM.FirstName + ' ' + Isnull(EM.LastName, '')        
      END TRY        
      BEGIN CATCH        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT;        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
