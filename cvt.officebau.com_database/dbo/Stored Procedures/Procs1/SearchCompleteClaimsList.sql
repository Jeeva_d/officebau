﻿  
/****************************************************************************     
CREATED BY   : Ajith N  
CREATED DATE  :   01 Aug 2017  
MODIFIED BY   :   Ajith N  
MODIFIED DATE  :   29 Nov 2017  
 <summary>   
          [SearchCompleteClaimsList] 1, '', '', '', '', 0, 0  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchCompleteClaimsList] (@DomainID       INT,  
                                                  @StatusID       INT,  
                                                  @FromDate       DATETIME,  
                                                  @ToDate         DATETIME,  
                                                  @BusinessUnitID INT,  
                                                  @EmployeeID     INT,  
                                                  @CategoryID     INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN  
              SELECT CR.ID                                        AS ID,  
                     Isnull(EM.EmpCodePattern, '') + EM.Code                                      AS EmployeeCode,  
                     EM.FullName                                 AS FullName,  
                     CR.ClaimDate                                 AS RequestedDate,  
                     CR.Amount                                    AS Amount,  
                     ( CASE  
                         WHEN TS.ID <> (SELECT ID  
                                        FROM   tbl_Status  
                                        WHERE  Code = 'Rejected'  
                                               AND Type = 'Claims'  
                                               AND IsDeleted = 0) THEN  
                           ( CASE  
                               WHEN CFA.FinApprovedAmount IS NULL THEN  
                                 CA.ApprovedAmount  
                               ELSE  
                                 CFA.FinApprovedAmount  
                             END )  
                         ELSE  
                           NULL  
                       END )                                      AS ApprovalAmount,  
                     CS.SettlerAmount                             AS SettlerAmount,  
                     BU.NAME                                      AS BusinessUnit,  
                     TS.Code                                      AS [Status],  
                     Dep.NAME                                     AS DepartmentName,  
                     CC.NAME                                      AS CategoryName,  
                     Cast(ROW_NUMBER()  
                            OVER(  
                              ORDER BY CR.ClaimDate DESC) AS INT) AS RowNo  
              FROM   tbl_ClaimsRequest CR  
                     LEFT JOIN tbl_EmployeeMaster EM  
                            ON EM.ID = CR.CreatedBy  
                     LEFT JOIN tbl_BusinessUnit BU  
                            ON BU.ID = EM.BaseLocationID  
                     LEFT JOIN tbl_Status TS  
                            ON TS.ID = CR.StatusID  
                     LEFT JOIN tbl_Department Dep  
                            ON Dep.ID = EM.DepartmentID  
                     LEFT JOIN tbl_ClaimCategory CC  
                            ON CC.ID = CR.CategoryID  
                     LEFT JOIN tbl_ClaimsFinancialApprove CFA  
                            ON CFA.ClaimItemID = CR.ID  
                     LEFT JOIN tbl_ClaimsSettle CS  
                            ON CS.ClaimItemID = CR.ID  
                     LEFT JOIN tbl_ClaimsApprove CA  
                            ON CA.ClaimItemID = CR.ID  
              WHERE  CR.IsDeleted = 0  
                     AND CR.DomainID = @DomainID  
                     AND ( Isnull(@BusinessUnitID, 0) = 0  
                            OR ( EM.BaseLocationID = @BusinessUnitID ) )  
                     AND ( Isnull(@StatusID, 0) = 0  
                            OR ( CR.StatusID = @StatusID ) )  
AND ( Isnull(@FromDate, '') = ''  
                            OR ( CR.ClaimDate >= @FromDate ) )  
                     AND ( Isnull(@ToDate, '') = ''  
                            OR ( CR.ClaimDate <= @ToDate ) )  
                     AND ( Isnull(@EmployeeID, 0) = 0  
                            OR ( EM.ID = @EmployeeID ) )  
                     AND ( Isnull(@CategoryID, 0) = 0  
                            OR ( CR.CategoryID = @CategoryID ) )  
              ORDER  BY CR.ClaimDate DESC,  
                        EM.FullName DESC  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
