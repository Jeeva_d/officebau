﻿/****************************************************************************     
CREATED BY   : Ajith    
CREATED DATE  :   12 Feb 2019
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
	[GetLoanAmortizationList] 4,1,1  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetLoanAmortizationList] (@ID       INT,
                                                 @UserID   INT,
                                                 @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT em.FullName                  AS Requester,
                 ISNULL( la.Amount, 0)
                 + ISNULL( la.InterestAmount, 0)
                 + ISNULL( due.DueBalance, 0) AS TotalAmount,
                 la.DueDate                   AS PaymentDate,
                 ISNULL(la.PaidAmount, 0)     AS PaidAmount,
				 ISNULL( due.DueBalance, 0) AS DueBalance
          FROM   tbl_LoanRequest L
                 LEFT JOIN tbl_EmployeeMaster em
                        ON em.ID = l.EmployeeID
                 JOIN tbl_LoanAmortization la
                   ON la.LoanRequestID = l.ID
                      AND ISNULL(la.isdeleted, 0) = 0
                 LEFT JOIN tbl_LoanAmortization due
                        ON due.LoanRequestID = la.LoanRequestID
                           AND ISNULL(due.isdeleted, 0) = 0
                           AND ISNULL(due.IsBalance, 0) = 1
                           AND Year(Dateadd(MONTH, 1, due.DueDate)) = Year(la.DueDate)
                           AND Month(Dateadd(MONTH, 1, due.DueDate)) = Month(la.DueDate)
          WHERE  L.ID = @ID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
