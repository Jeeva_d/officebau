﻿/****************************************************************************   
CREATED BY   :  Ajith N  
CREATED DATE  : 23 May 2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetSearchHistory] (@FormName        VARCHAR(200),
                                          @SearchParameter VARCHAR(200),
                                          @UserID          INT,
                                          @DomainID        INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT Value
          FROM   tblSearchHistory
          WHERE  FormName = @FormName
                 AND Parameter = @SearchParameter
                 AND UserID = @UserID
                 AND DomainID = @DomainID
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
