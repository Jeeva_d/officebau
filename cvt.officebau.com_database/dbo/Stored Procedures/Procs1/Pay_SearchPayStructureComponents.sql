﻿/****************************************************************************           
CREATED BY   :           
CREATED DATE  :           
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>        
 [Pay_SearchPayStructureComponents] 1,1        
 </summary>                                   
 *****************************************************************************/        
CREATE PROCEDURE [dbo].[Pay_SearchPayStructureComponents] (@ID       INT,        
                                                          @DomainID INT)        
AS        
  BEGIN        
      BEGIN TRY        
          SELECT tppc.ID AS ComponentID,        
                 tppcps.ID,        
                 tppc.Code,        
                 tppc.Description,        
                 tppc.Ordinal,        
                 tppcps.Value,        
                 tppcps.CompanyPayStructureID,        
                 tppcps.IsEditable ,      
                 tppcps.ShowinPaystructure ,       
                 tppcps.PayStub    ,  
     tppcps.Fixed    
          FROM   tbl_Pay_PayrollCompanyPayStructure AS tppcps        
                 LEFT JOIN tbl_Pay_PayrollCompontents AS tppc        
                        ON tppcps.ComponentID = tppc.ID        
                           AND tppc.IsDeleted = 0        
          WHERE  tppcps.CompanyPayStructureID = @ID        
                 AND tppcps.IsDeleted = 0        
                 AND tppcps.DomainID = @DomainID        
          UNION ALL        
          SELECT tppc.ID AS ComponentID,        
                 0,        
                 tppc.Code,        
                 tppc.Description,        
                 tppc.Ordinal,        
                 '',        
                 @ID,        
                 tppc.IsDeleted,      
     'true','false' ,'false'       
          FROM   tbl_Pay_PayrollCompontents AS tppc        
          WHERE  tppc.IsDeleted = 0        
                 AND ID NOT IN (SELECT ComponentID        
                                FROM   tbl_Pay_PayrollCompanyPayStructure        
                                WHERE  IsDeleted = 0        
                                       AND CompanyPayStructureID = @ID)        
                 AND tppc.DomainID = @DomainID        
          ORDER  BY tppc.Ordinal        
      END TRY        
      BEGIN CATCH        
          DECLARE @ErrorMsg    VARCHAR(100),        
                  @ErrSeverity TINYINT        
          SELECT @ErrorMsg = Error_message(),        
                 @ErrSeverity = Error_severity()        
          RAISERROR(@ErrorMsg,@ErrSeverity,1)        
      END CATCH        
  END
