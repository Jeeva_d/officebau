﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [SearchNewsAndEvents] 1,71
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchNewsAndEvents] (@DomainID   INT,
                                             @EmployeeID INT)
AS
  BEGIN
      BEGIN TRY
          SELECT ne.ID,
                 NAME,
                 Description,
                 PublishedOn,
                 EndDate,
                 em.FullName  AS PublishedBy,
                 em1.FullName AS CreatedBy,
                 ne.HistoryID,
                 StartDate
          FROM   tbl_NewsAndEvents ne
                 LEFT JOIN tbl_EmployeeMaster em
                        ON em.ID = ne.PublishedBy
                 JOIN tbl_EmployeeMaster em1
                   ON em1.ID = ne.CreatedBy
          WHERE  ne.DomainID = @DomainID
                 AND ne.IsDeleted = 0
                 AND ne.CreatedBy = @EmployeeID
          ORDER  BY ne.CreatedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
