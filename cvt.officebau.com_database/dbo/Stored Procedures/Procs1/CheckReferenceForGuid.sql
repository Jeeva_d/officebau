﻿  
/****************************************************************************                   
CREATED BY    :                 
CREATED DATE  :                 
MODIFIED BY   :              
MODIFIED DATE :            
 <summary>                 
       CheckReferenceForGuid '9F6EAB5F-5D2F-4EA7-8D6C-4F7B482F148E'      
 </summary>                                           
 *****************************************************************************/  
  
CREATE PROCEDURE [dbo].[CheckReferenceForGuid]  
(@SearchStr NVARCHAR(MAX),   
 @OutPut    INT OUTPUT  
)  
AS  
     BEGIN  
         BEGIN TRY  
             CREATE TABLE #Results  
             (ColumnName  NVARCHAR(370),   
              ColumnValue NVARCHAR(3630)  
             );  
             SET NOCOUNT ON;  
             DECLARE @TableName NVARCHAR(256), @ColumnName NVARCHAR(128), @SearchStr2 NVARCHAR(110);  
             SET @TableName = '';  
             SET @SearchStr2 = QUOTENAME('%'+@SearchStr+'%', '''');  
             WHILE @TableName IS NOT NULL  
                 BEGIN  
                     SET @ColumnName = '';  
                     SET @TableName =  
                     (  
                         SELECT MIN(QUOTENAME(TABLE_SCHEMA)+'.'+QUOTENAME(table_name))  
                         FROM INFORMATION_SCHEMA.TABLES  
                         WHERE TABLE_TYPE = 'BASE TABLE'  
                               AND table_name IN('tblExpense', 
							   'tbl_EmployeeMaster', 'tbl_ClaimsRequest', 
							   'tbl_EmployeeDocuments', 'tbl_Form16', 
							   'tbl_NewsAndEvents', 'tbl_company','tblPurchaseOrder')  
                         AND table_name NOT LIKE '%_TriggerAudit%'  
                         AND QUOTENAME(TABLE_SCHEMA)+'.'+QUOTENAME(table_name) > @TableName  
                         AND OBJECTPROPERTY(OBJECT_ID(QUOTENAME(TABLE_SCHEMA)+'.'+QUOTENAME(table_name)), 'IsMSShipped') = 0  
                     );  
                     WHILE(@TableName IS NOT NULL)  
                          AND (@ColumnName IS NOT NULL)  
                         BEGIN  
                             SET @ColumnName =  
                             (  
                                 SELECT MIN(QUOTENAME(column_name))  
                                 FROM INFORMATION_SCHEMA.COLUMNS  
                                 WHERE TABLE_SCHEMA = PARSENAME(@TableName, 2)  
                                       AND table_name = PARSENAME(@TableName, 1)  
                                       AND data_type IN('uniqueidentifier')  
                                 AND QUOTENAME(column_name) > @ColumnName  
                             );  
                             -- Select @TableName,@ColumnName        
                             IF @ColumnName IS NOT NULL  
                                 BEGIN  
                                     INSERT INTO #Results  
                                     EXEC ('SELECT '''+@TableName+'.'+@ColumnName+''', LEFT('+@ColumnName+', 3630) FROM '+@TableName+' (NOLOCK) '+' WHERE '+@ColumnName+' LIKE '+@SearchStr2);  
                                 END;  
                         END;  
                 END;  
             SET @OutPut =  
             (  
                 SELECT COUNT(*)  
                 FROM #Results  
             );  
         END TRY  
         BEGIN CATCH  
             ROLLBACK TRANSACTION;  
             DECLARE @ErrorMsg VARCHAR(100), @ErrSeverity TINYINT;  
             SELECT @ErrorMsg = ERROR_MESSAGE(),   
                    @ErrSeverity = ERROR_SEVERITY();  
             RAISERROR(@ErrorMsg, @ErrSeverity, 1);  
         END CATCH;  
     END;
