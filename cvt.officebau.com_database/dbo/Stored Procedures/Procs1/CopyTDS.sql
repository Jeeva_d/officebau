﻿/****************************************************************************           
CREATED BY  :       
CREATED DATE :          
MODIFIED BY  :        
MODIFIED DATE :        
<summary>        
 [CopyTDS] 6,5,2,1 ,1      
</summary>                                   
*****************************************************************************/
CREATE PROCEDURE [dbo].[CopyTDS] (@MonthID      INT,
                                 @Year         INT,
                                 @BusinessUnit INT,
                                 @SessionID    INT,
                                 @DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @Result   VARCHAR(500),
                  @PreMonth INT,
                  @PreYear  INT
          DECLARE @YearName INT = (SELECT NAME
             FROM   tbl_FinancialYear
             WHERE   id = @Year
                    AND DomainID = @DomainID)
          DECLARE @CustomDate DATETIME = Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @YearName - 2000, Dateadd(MONTH, @MonthID - 1, '20000101')))
                                      + 1, 0))

          SET @PreYear=(SELECT TOP 1 T.YearID
                        FROM   tbl_TDS T
                               JOIN tbl_FinancialYear FY
                                 ON FY.ID = T.YearID
                        WHERE  T.BaseLocationID = @BusinessUnit
                               AND T.IsDeleted = 0
                               AND T.DomainId = @DomainID
                        ORDER  BY FY.NAME DESC)

          SET @PreMonth=(SELECT TOP 1 MonthId
                         FROM   tbl_TDS
                         WHERE  BaseLocationID = @BusinessUnit
                                AND IsDeleted = 0
                                AND DomainId = @DomainID
                                AND YearID = @PreYear
                         ORDER  BY MonthId DESC)

          IF( (SELECT Count(1)
               FROM   tbl_TDS
               WHERE  MonthId = @MonthID
                      AND YearID = @Year
                      AND IsDeleted = 0
                      AND BaseLocationID = @BusinessUnit
                      AND DomainId = @DomainID) = 0 )
            BEGIN
                ------------------------------------Insert---------------------  
                INSERT INTO tbl_TDS
                            (EmployeeId,
                             MonthId,
                             YearID,
                             TDSAmount,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             DomainId,
                             BaseLocationID,
                             IsDeleted)
                SELECT TDS.EmployeeId,
                       @MonthID,
                       @Year,
                       TDS.TDSAmount,
                       @SessionID,
                       Getdate(),
                       @SessionID,
                       Getdate(),
                       @DomainID,
                       @BusinessUnit,
                       0
                FROM   tbl_EmployeeMaster EMP
                       JOIN tbl_BusinessUnit BUU
                         ON EMP.BaseLocationID = BUU.ID
                       LEFT JOIN tbl_TDS TDS
                              ON EMP.ID = TDS.EmployeeId
                                 AND TDS.MonthId = @PreMonth
                                 AND TDS.IsDeleted = 0
                                 AND TDS.YearID = @PreYear
                WHERE  EMP.DomainID = @DomainID
                       AND EMP.IsDeleted = 0
                       AND ( Isnull(emp.IsActive, '') = 0
                              OR ( Month(InactiveFrom) = @MonthID
     AND Year(InactiveFrom) = @Year ) )
                       AND ( ( Isnull(@BusinessUnit, '') <> ''
                               AND EMP.BaseLocationID IN (SELECT Item
                                                          FROM   dbo.Splitstring (@BusinessUnit, ',')
                                                          WHERE  Isnull(Item, '') <> '') )
                              OR Isnull(@BusinessUnit, '') = '' )
                       AND Cast(EMP.DOJ AS DATE) <= Cast (@CustomDate AS DATE)
                       AND EMP.IsDeleted = 0
                       AND EMP.HasAccess = 1
                       AND EMP.EmploymentTypeID = (SELECT ID
                                                   FROM   tbl_EmployementType
                                                   WHERE  NAME = 'Direct'
                                                          AND DomainID = @DomainID)
                       AND MonthId = @PreMonth
                       AND YearID = @PreYear

                SET @Result='TDS Copied Successfully!'

                GOTO Finish
            END
          ELSE
            BEGIN
                SET @Result='TDS for selected Business Unit''s Month and Year already Exists!'

                GOTO Finish
            END

          FINISH:

          SELECT @Result

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 
