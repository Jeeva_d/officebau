﻿/****************************************************************************   
CREATED BY   : Dhanalakshmi  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [SearchGroupLedger] 1,1 
 
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchgroupledger] ( @DomainID INT
                                            )
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION
          

          SELECT pd.ID      AS ID,
                 pd.NAME    AS NAME,
                 pd.Remarks   AS Remarks,
                 pd.ReportType AS ReportType
          FROM   tblGroupLedger pd
          
          WHERE  pd.DomainID = @DomainID
                 AND pd.IsDeleted = 0
          ORDER  BY pd.ModifiedON DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END










