﻿/****************************************************************************         
CREATED BY  : Naneeshwar        
CREATED DATE : 21-Nov-2016      
MODIFIED BY  : JENNIFER.S    
MODIFIED DATE : 09-10-17  
 <summary>      
 [Searchincomepayable] 'All','','','',1      
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchincomepayable] (@Status VARCHAR(100),
@StartDate VARCHAR(50),
@EndDate VARCHAR(50),
@CustomerName VARCHAR(50),
@DomainID INT,
@Notations VARCHAR(50),
@Amount MONEY)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		IF (@StartDate != '')
			SET @StartDate = CAST(@StartDate AS DATETIME)

		IF (@EndDate != '')
			SET @EndDate = CAST(@EndDate AS DATETIME)

		IF (@Status = 'All')
			SET @Status = '';

		WITH cte
		AS
		(SELECT
				C.Name AS Customer
			   ,C.ID AS CustomerID
			   ,i.ReceivedAmount AS ReceivedAmount
			   ,(CASE
					WHEN (i.DiscountPercentage <> 0) THEN (SELECT
								(ISNULL(SUM(Qty * Rate)
								+ SUM(ISNULL(SGSTAmount, 0) + ISNULL(CGSTAmount, 0)) - (ISNULL((SUM(Qty * Rate) * i.DiscountPercentage), 0)), 0))
							FROM tblInvoiceItem
							WHERE InvoiceID = i.ID
							AND DomainID = @DomainID
							AND IsDeleted = 0)
					ELSE (SELECT
								(ISNULL((SUM(Qty * Rate)
								+ SUM(ISNULL(SGSTAmount, 0) + ISNULL(CGSTAmount, 0)) - ISNULL(i.DiscountValue, 0)), 0))
							FROM tblInvoiceItem
							WHERE InvoiceID = i.ID
							AND DomainID = @DomainID
							AND IsDeleted = 0)
				END) AS Booked
				,(SELECT SUM(ih.Amount) FROM tblInvoiceHoldings AS ih WHERE ih.Isdeleted=0 AND ih.InvoiceId=i.ID) AS Holdings
			   ,cd.[Type] AS [Status]
			   ,i.AutoGeneratedNo
			FROM tblInvoice i
			LEFT JOIN tblCustomer c
				ON i.CustomerID = c.ID
			JOIN tbl_CodeMaster cd
				ON cd.ID = i.StatusID
			WHERE i.[Type] = 'Invoice'
			AND i.DomainID = @DomainID
			AND i.IsDeleted = 0
			AND ((ISNULL(@Status, '') = ''
			AND i.StatusID IN (SELECT
					ID
				FROM tbl_CodeMaster
				WHERE [Type] IN ('Open', 'Partial')
				AND IsDeleted = 0)
			)
			OR (ISNULL(@Status, '') != ''
			AND i.StatusID IN (SELECT
					ID
				FROM tbl_CodeMaster
				WHERE [Type] = @Status
				AND IsDeleted = 0)
			))
			AND (ISNULL(@CustomerName, '') = ''
			OR c.Name LIKE '%' + @CustomerName + '%')
			AND ((@StartDate IS NULL
			OR @StartDate = '')
			OR i.Date >= @StartDate)
			AND ((@EndDate IS NULL
			OR @EndDate = '')
			OR i.Date <= @EndDate))
		SELECT
			Customer
		   ,CustomerID
		   ,ReceivedAmount
		   ,SUM(Booked) AS Booked
		   ,SUM(cte.Holdings) AS Holdings
		   ,[Status]
		   ,AutoGeneratedNo INTO #tmpReceiveble
		FROM cte
		GROUP BY Customer
				,CustomerID
				,ReceivedAmount
				,[Status]
				,AutoGeneratedNo
		ORDER BY Customer ASC,
		[Status] ASC

		SELECT
			Customer
		   ,CustomerID
		   ,ReceivedAmount
		   ,Booked
		   ,[Status]
		   ,Holdings
		   ,AutoGeneratedNo
		FROM #tmpReceiveble
		WHERE (ISNULL(@Amount, 0) = 0)
		OR (ISNULL(@Notations, '') = 'EqualTo'
		AND Booked = ISNULL(@Amount, 0))
		OR (ISNULL(@Notations, '') = 'GreaterThanEqual'
		AND Booked >= @Amount)
		OR (ISNULL(@Notations, '') = 'LessThanEqual'
		AND Booked <= @Amount)
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMsg VARCHAR(100)
			   ,@ErrSeverity TINYINT
		SELECT
			@ErrorMsg = ERROR_MESSAGE()
		   ,@ErrSeverity = ERROR_SEVERITY()
		RAISERROR (@ErrorMsg, @ErrSeverity, 1)
	END CATCH
END
