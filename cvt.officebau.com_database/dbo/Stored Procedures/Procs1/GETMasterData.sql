﻿/****************************************************************************   
CREATED BY   : Naneeshwar.M
CREATED DATE  :   06-JUN-2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>   
 [GETMasterData] 'null','destinationcities',1,1        
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GETMasterData] (@DependantTable VARCHAR(200),
                                       @MainTable      VARCHAR(200),
                                       @ID             INT,
                                       @DomainID       INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          DECLARE @sql VARCHAR(MAX)

          IF( Isnull(@DependantTable, '') = ''
               OR Isnull(@DependantTable, '') = 'Null' )
            BEGIN
                IF( @MainTable = 'Destinationcities' )
                  BEGIN
                      SET @sql =( 'Select ID,Name,Remarks,cast(IsMetro as INT) AS DRPID from tbl_'
                                  + @MainTable + ' Where ID='
                                  + CONVERT(VARCHAR(10), @ID) + 'and DomainID='
                                  + CONVERT(VARCHAR(10), @domainID) )
                  END
                ELSE
                  BEGIN
                      SET @sql =( 'Select ID,Name,Remarks ,0 AS DRPID from tbl_'
                                  + @MainTable + ' WHERE ID= '
                                  + CONVERT(VARCHAR(10), @ID) + 'and DomainID='
                                  + CONVERT(VARCHAR(10), @domainID) )
                  END
            END
          ELSE IF( @DependantTable = 'BusinessUnit' )
            BEGIN
                SET @sql =( 'Select ID,Name,Remarks,ParentID AS DRPID from tbl_'
                            + @MainTable + ' Where ID='
                            + CONVERT(VARCHAR(10), @ID) + 'and DomainID='
                            + CONVERT(VARCHAR(10), @domainID) )
            END
          ELSE
            BEGIN
                SET @sql =( 'Select ID,Name,Remarks,' + @DependantTable
                            + 'ID AS DRPID from tbl_' + @MainTable
                            + ' Where ID=' + CONVERT(VARCHAR(10), @ID)
                            + 'and DomainID='
                            + CONVERT(VARCHAR(10), @domainID) )
            END

          EXEC(@sql)

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
