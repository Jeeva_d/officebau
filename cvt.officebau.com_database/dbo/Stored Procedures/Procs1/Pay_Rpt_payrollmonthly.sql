﻿  
  
/****************************************************************************     
CREATED BY  :  Priya K  
CREATED DATE :    
MODIFIED BY  :     
MODIFIED DATE :   13-JUL-2017  
<summary>  
 [Pay_Rpt_payrollmonthly] 1, '1,2,3,4,5,6,7,8,9,10,11,12', 0,1054,4  
</summary>                             
*****************************************************************************/  
CREATE PROCEDURE [dbo].[Pay_Rpt_payrollmonthly] (@DomainID        INT,  
                                            @Location        VARCHAR(100),  
                                            @DepartmentID    INT,  
                                            @ComponentID      INT,  
                                            @FinancialYearID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @DynamicQuery VARCHAR(MAX) = '',  
                  @getDate      DATE,  
                  @MonthID      INT = 3,  
                  @YearID       INT  
      set @YearID = (SELECT Cast(NAME AS INT) + 1  
                     FROM   tbl_FinancialYear  
                     WHERE  ID = @FinancialYearID  
                            AND Isnull(IsDeleted, 0) = 0  
                            AND DomainID = @DomainID)  
  
          CREATE TABLE #MonthTable  
            (  
               ID      TINYINT IDENTITY(1, 1),  
               Months  VARCHAR(10),  
               MonthID INT,  
               Years   SMALLINT  
            )  
  
          CREATE TABLE #DataSource  
            (  
               [Value] NVARCHAR(128)  
            )  
  
          INSERT INTO #DataSource  
                      ([Value])  
          SELECT Item  
          FROM   dbo.Splitstring (@Location, ',')  
          WHERE  Isnull(Item, '') <> ''  
           
          SELECT @getDate = Cast(CONVERT(VARCHAR, @YearID) + '-'  
                                 + CONVERT(VARCHAR, @MonthID) + '-01' AS DATETIME);  
   
          WITH DateRange  
               AS (SELECT @getDate Dates  
                   UNION ALL  
                   SELECT Dateadd(mm, -1, Dates)  
                   FROM   DateRange  
                   WHERE  Dates > Dateadd(mm, -11, @getDate))  
          INSERT INTO #MonthTable  
          SELECT CONVERT(CHAR(3), Datename(m, Dates), 0) Months,  
                 Month(Dates)                            AS MonthID,  
                 Year(Dates)                             Years  
          FROM   DateRange  
     
			  Select MT.Months + ' ' + Cast( MT.Years AS VARCHAR) AS [Month] ,			
			  Sum(Amount) AS ComponentValues from #MonthTable MT
			  Left join tbl_pay_employeepayroll ep on
			   ep.MonthId = MT.MonthID and ep.YearId = MT.Years and ep.isdeleted =0 
			 LEFT JOIN tbl_EmployeeMaster emp
			  ON emp.ID = ep.EmployeeId
			 AND ( ( @DepartmentID IS NULL
                       OR Isnull(@DepartmentID, 0) = 0 )
                      OR emp.DepartmentID = @DepartmentID )
					  and	(	(@Location = CAST(0 AS VARCHAR))          
                              OR emp.BaseLocationID IN          
                              (SELECT          
                                   [Value]          
                              FROM          
                                   #DataSource) )      
                         AND          
                         (emp.BaseLocationID <> 0 OR emp.BaseLocationID <> '')
			 LEFT  Join tbl_Pay_EmployeePayrollDetails pd on pd.payrollID = ep.ID 
			   where pd.ComponentID = @ComponentID and
			         ep.IsProcessed = 1
					 AND ep.IsDeleted = 0
					 AND ep.DomainId = @DomainID
			   Group by MT.Months,MT.Years,MT.ID
			   Order by MT.ID



          
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
