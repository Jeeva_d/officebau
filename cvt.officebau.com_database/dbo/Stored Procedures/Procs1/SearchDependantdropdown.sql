﻿/****************************************************************************     
CREATED BY   : Jeeva    
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
     [Searchdependantdropdown] 0,'codemaster','PaystubType','Type', 1    
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchDependantdropdown] (@ID             INT,  
                                                 @MainTable      VARCHAR(100),  
                                                 @Type           VARCHAR(100),  
                                                 @DependantTable VARCHAR(200),  
                                                 @DomainID       INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
  
          IF( Isnull(@Type, '') <> '' )  
            SET @ID = (SELECT ID  
                       FROM   tblMasterTypes  
                       WHERE  NAME = @Type)  
  
          DECLARE @sql VARCHAR(MAX)  
  
          SET @DependantTable = @DependantTable + 'ID'  
          SET @MainTable = 'tbl_' + @MainTable  
          SET @sql = ( 'Select ID, Code AS Name From '  
                       + @MainTable + ' WHERE IsDeleted = 0 AND '  
                       + @DependantTable + ' = '  
                       + CONVERT(VARCHAR(10), @ID) )  
  
          PRINT @sql  
  
          EXEC(@sql)  
  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
