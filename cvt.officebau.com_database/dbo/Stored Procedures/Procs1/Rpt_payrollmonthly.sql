﻿

/****************************************************************************   
CREATED BY		:   Naneeshwar.M
CREATED DATE	:   07-JULY-2017
MODIFIED BY		:   Naneeshwar.M
MODIFIED DATE	:   13-JUL-2017
<summary>
	[Rpt_payrollmonthly] 1, '1,2,3,4,5,6,7,8,9,10,11,12', 0,'netsalary',3
</summary>                           
*****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_payrollmonthly] (@DomainID        INT,
                                            @Location        VARCHAR(100),
                                            @DepartmentID    INT,
                                            @Component       VARCHAR(20),
                                            @FinancialYearID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @DynamicQuery VARCHAR(MAX) = '',
                  @getDate      DATE,
                  @MonthID      INT = 3,
                  @YearID       INT
				  set @YearID = (SELECT Cast(NAME AS INT) + 1
                     FROM   tbl_FinancialYear
                     WHERE  ID = @FinancialYearID
                            AND Isnull(IsDeleted, 0) = 0
                            AND DomainID = @DomainID)

          CREATE TABLE #MonthTable
            (
               ID      TINYINT IDENTITY(1, 1),
               Months  VARCHAR(10),
               MonthID INT,
               Years   SMALLINT
            )

          CREATE TABLE #DataSource
            (
               [Value] NVARCHAR(128)
            )

          INSERT INTO #DataSource
                      ([Value])
          SELECT Item
          FROM   dbo.Splitstring (@Location, ',')
          WHERE  Isnull(Item, '') <> ''

          -- DECLARE @MonthID INT,       
          --SELECT @MonthID = Max(MonthID),
          --       @YearID
          --FROM   tbl_EmployeePayroll EP
          --       LEFT JOIN tbl_EmployeeMaster EMP
          --              ON EMP.ID = EP.EmployeeId
          --       LEFT JOIN tbl_BusinessUnit BU
          --              ON BU.ID = EMP.BusinessUnitID
          --WHERE  EP.IsDeleted = 0
          --       AND EP.IsProcessed = 1
          --       AND ( ( @Location = Cast(0 AS VARCHAR)
          --                OR @Location IS NULL )
          --              OR ( EP.BaseLocationID IN (SELECT [Value]
          --                                         FROM   #DataSource
          --                                         WHERE  EMP.IsDeleted = 0) ) )
          --       AND ( ( @DepartmentID IS NULL
          --                OR Isnull(@DepartmentID, 0) = 0 )
          --              OR EMP.DepartmentID = @DepartmentID );
          SELECT @getDate = Cast(CONVERT(VARCHAR, @YearID) + '-'
                                 + CONVERT(VARCHAR, @MonthID) + '-01' AS DATETIME);
 
          WITH DateRange
               AS (SELECT @getDate Dates
                   UNION ALL
                   SELECT Dateadd(mm, -1, Dates)
                   FROM   DateRange
                   WHERE  Dates > Dateadd(mm, -11, @getDate))
          INSERT INTO #MonthTable
          SELECT CONVERT(CHAR(3), Datename(m, Dates), 0) Months,
                 Month(Dates)                            AS MonthID,
                 Year(Dates)                             Years
          FROM   DateRange

          SET @DynamicQuery = '; WITH PayrollCTE
								   AS (SELECT CAST(ep.MonthId AS VARCHAR(20))  AS MonthId, CAST(ep.YearId AS VARCHAR(20)) AS YearId,
											  EP.' + @Component
                              + ' AS Component,
											  emp.DepartmentID,
											  EP.BaseLocationID,EmployeeID
									   FROM   tbl_EmployeePayroll ep
											  LEFT JOIN tbl_EmployeeMaster emp
													 ON emp.ID = ep.EmployeeId
														AND ( (''' + @Location
                              + '''= Cast(0 AS VARCHAR) OR ''' + @Location
                              + ''' IS NULL)
															   OR ( ep.BaseLocationID IN (SELECT [Value]
																						   FROM   #DataSource
																						   WHERE  ep.IsDeleted = 0) ) )
									   WHERE  ep.IsProcessed = 1
											  AND ep.IsDeleted = 0
											  AND ep.DomainId = ' + CAST(@DomainID AS VARCHAR) + '
									   GROUP  BY ep.MonthId,
												 emp.DepartmentID,
												 ep.BaseLocationID,
												 ep.YearId,
												 ep.' + @Component
                              + ',
												 EmployeeID)
							  SELECT MT.Months + '' '' + Cast( MT.Years AS VARCHAR) AS [Month],
									 Sum(Component)                                   Component
							  FROM   #MonthTable MT
									 LEFT JOIN tbl_month MON
											ON MT.Months  COLLATE DATABASE_DEFAULT  = MON.Code  COLLATE DATABASE_DEFAULT 
									 LEFT JOIN PayrollCTE pay
											ON pay.MonthId = MON.ID
											   AND ( ('
                              + CONVERT(VARCHAR(30), @DepartmentID)
                              + ' IS NULL
														OR Isnull('
                              + CONVERT(VARCHAR(30), @DepartmentID)
                              + ', 0) = 0 )
													  OR PAY.DepartmentID = '
                              + CONVERT(VARCHAR(30), @DepartmentID)
                              + ')
											   AND (  PAY.BaseLocationID IN(SELECT [Value]
																		   FROM   #DataSource) )
							  WHERE MT.MonthID = pay.MonthId AND MT.Years = pay.YearId 
							  GROUP  BY MT.Months + '' '' + Cast( MT.Years AS VARCHAR),
										MT.ID
							  ORDER  BY MT.ID DESC'

          EXEC (@DynamicQuery)
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
