﻿/****************************************************************************                 
CREATED BY   : DhanaLakshmi. S                
CREATED DATE  :   30-Nov-2018              
MODIFIED BY   :                 
MODIFIED DATE  :                 
 <summary>                 
 [GetInventoryDashBoardTilesDetails] 1              
 </summary>                                         
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[GetInventoryDashBoardTilesDetails] (@DomainID INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN TRANSACTION      
      
          DECLARE @LowThreshold  INT = 0,      
                  @OutofStock   INT = 0,      
                  @POQuantity   DECIMAL(32, 2) = 0,      
                  @SOQuantity   DECIMAL(32, 2) = 0      
      
          SELECT @LowThreshold = Isnull(Count(1), 0)      
          FROM   tblProduct_V2 P      
                 LEFT JOIN VW_Inventory VW      
                        ON P.ID = VW.ProductID      
          WHERE  P.IsDeleted = 0      
                 AND P.DomainID = @DomainID      
                 AND P.IsInventroy = 1      
                 AND Isnull(LowStockThresold, 0) <> 0      
                 AND Isnull(LowStockThresold, 0) >= StockonHand      
      
          SELECT @OutofStock = Isnull(Count(1), 0)      
          FROM   VW_Inventory VW      
                 LEFT JOIN tblProduct_V2 P      
                        ON P.ID = VW.ProductID      
          WHERE  StockonHand <= 0      
                 AND P.IsDeleted = 0      
                 AND P.DomainID = @DomainID      
                 AND P.IsInventroy = 1      
      
          SELECT @POQuantity = (Isnull(Sum(POD.Qty), 0)  - Isnull(Sum(POD.BilledQTY), 0))    
          FROM   tblPODetails POD      
                 LEFT JOIN tblProduct_V2 P      
                        ON P.ID = POD.LedgerID      
                WHERE  POD.IsDeleted = 0    
                       AND POD.DomainID = @DomainID    
                       AND POD.IsLedger = 0    
                       AND ( POD.BilledQTY = 0    
                              OR POD.BilledQTY <> QTY )    
                       AND P.IsInventroy = 1    
      
          SELECT @SOQuantity =  (Isnull(Sum(QTY), 0) - Isnull(Sum(InvoicedQTY), 0))      
          FROM   tblSoItem SOT      
                 LEFT JOIN tblProduct_V2 P      
                        ON P.ID = SOT.ProductID      
          WHERE  SOT.IsDeleted = 0      
                 AND SOT.DomainID = @DomainID      
                 AND SOT.IsProduct = 0      
                 AND (InvoicedQTY = 0 OR InvoicedQTY <> QTY)    
                 AND P.IsInventroy = 1      
      
          SELECT @LowThreshold AS LowStockThreshold,      
                 @OutofStock   AS OutofStock,      
                 @POQuantity   AS POQuantity,      
                 @SOQuantity   AS SOQuantity      
      
          COMMIT TRANSACTION      
      END TRY      
      BEGIN CATCH      
          ROLLBACK TRANSACTION      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
