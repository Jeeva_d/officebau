﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	27-JUN-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
  [Getemployeepayhistory] 1,1
 </summary>                          
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getemployeepayhistory] (@DomainID   INT,
                                               @EmployeeID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @ROWCOLUMN   VARCHAR(MAX),
                  @COLUMNQUERY VARCHAR(MAX);

      -- GET Employee pay history
      ;
          WITH CTE
               AS (SELECT Cast(( CONVERT(VARCHAR(100), CPS.NAME) + ' ('
                                -- + CONVERT(VARCHAR(100), EPS.Gross, 106) + ') ('
                                 + CONVERT(VARCHAR(100), EPS.EffectiveFrom, 106)
                                 + ')' ) AS VARCHAR(100))               AS [CompanyPayStucName],
                          CONVERT(VARCHAR(100), EPS.EffectiveFrom, 106) AS [Effective From],
                          CONVERT(VARCHAR(100), EPS.Gross)              AS [Gross],
                          CONVERT(VARCHAR(100), EPS.[Basic])            AS [Basic],
                          CONVERT(VARCHAR(100), EPS.HRA)                AS [HRA],
                          CONVERT(VARCHAR(100), EPS.MedicalAllowance)   AS [Medical Allowance],
                          CONVERT(VARCHAR(100), EPS.Conveyance)         AS [Conveyance],
                          CONVERT(VARCHAR(100), SplAllow)               AS [Special Allowance],
                          CONVERT(VARCHAR(100), EPS.EducationAllow)     AS [Education Allowance],
                          CONVERT(VARCHAR(100), EPS.MagazineAllow)      AS [Paper Magazine],
                          CONVERT(VARCHAR(100), EPS.EEPF)               AS [Employee PF],
                          CONVERT(VARCHAR(100), EPS.ERPF)               AS [Employer PF],
                          CONVERT(VARCHAR(100), EPS.EEESI)              AS [Employee ESI],
                          CONVERT(VARCHAR(100), EPS.ERESI)              AS [Employer ESI],
                          --CONVERT(VARCHAR(100), EPS.PT)               AS [Professional Tax],
                          --CONVERT(VARCHAR(100), TDS)                  AS [Tax Deducted at Source (TDS)],
                          CONVERT(VARCHAR(100), EPS.PLI)                AS [Performance Linked Incentive],
                          CONVERT(VARCHAR(100), EPS.Mediclaim)          AS [Mediclaim],
                          CONVERT(VARCHAR(100), EPS.Bonus)              AS [Bonus],
                          CONVERT(VARCHAR(100), Gratuity)               AS [Gratuity],
                          CONVERT(VARCHAR(100), EPS.MobileDataCard)     AS [Mobile Datacard],
                          CONVERT(VARCHAR(100), EPS.OtherPerks)         AS [Other Perks],
                          CONVERT(VARCHAR(100), EPS.CTC)                AS [CTC]
                   FROM   tbl_EmployeePayStructure EPS
                          LEFT JOIN tbl_CompanyPayStructure CPS
                                 ON CPS.Id = EPS.CompanyPayStubId
                   WHERE  EPS.IsDeleted = 0
                          AND EPS.EmployeeId = @EmployeeID
                          AND EPS.DomainId = @DomainID)
          SELECT *
          INTO   #EmployeePayStructure
          FROM   CTE

          -- Get all row Name
          SELECT @ROWCOLUMN = Substring((SELECT ',[' + Isnull(NAME, '') + ']'
                                         FROM   tempdb.sys.columns
                                         WHERE  object_id = Object_id('tempdb..#EmployeePayStructure')
                                                AND NAME NOT IN ( 'CompanyPayStucName' )
                                         ORDER  BY column_id
                                         FOR XML PATH('')), 2, 20000)

          -- Get all Column Name
          SELECT @COLUMNQUERY = Substring((SELECT ',[' + Isnull(CompanyPayStucName, '') + ']'
 FROM   #EmployeePayStructure
                                           FOR XML PATH('')), 2, 20000)

          -- Interchange the table using Pivot and UnPivot 
          EXEC('SELECT PIV.NAME as [Pay Components], '+ @COLUMNQUERY + '
					FROM
					(
					  SELECT * 
					  FROM   #EmployeePayStructure e     
					  unpivot
					  (
						value for name in (' + @ROWCOLUMN +')
					  ) unpiv
					) src
					pivot
					(
					  max(value)
					  for CompanyPayStucName in (' + @COLUMNQUERY +')
					) piv
					JOIN tempdb.sys.columns T
						ON T.name COLLATE DATABASE_DEFAULT = piv.name COLLATE DATABASE_DEFAULT
					WHERE  object_id = Object_id(''tempdb..#EmployeePayStructure'') 
					ORDER BY T.column_id	
					')
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(8000),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
