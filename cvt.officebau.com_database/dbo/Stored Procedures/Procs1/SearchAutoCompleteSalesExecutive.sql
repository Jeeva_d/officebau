﻿/****************************************************************************   
CREATED BY   :  Ajith N
CREATED DATE  : 27 Dec 2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary> 
          [SearchAutoCompleteSalesExecutive] '',1,106
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchAutoCompleteSalesExecutive] (@SalesPerson VARCHAR(200),
                                                           @DomainID    INT,
                                                           @EmployeeID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @applicationRoleID INT = (SELECT ID
             FROM   tbl_ApplicationRole
             WHERE  NAME = 'Sales Executive'
                    AND DomainID = @DomainID
                    AND Isdeleted = 0)

          SELECT EM.ID       AS ID,
                 EM.FullName AS [Name]
          FROM   tbl_EmployeeMaster EM
                 JOIN tbl_EmployeeOtherDetails TOD
                   ON EM.ID = TOD.EmployeeID
                 CROSS APPLY dbo.Splitstring((SELECT BusinessUnitID
                                              FROM   tbl_EmployeeMaster
                                              WHERE  ID = @EmployeeID
                                                     AND Isdeleted = 0
                                                     AND ISNULL(IsActive, 0) = 0
                                                     AND DomainID = @DomainID), ',') S
          WHERE  Patindex('%,' + S.Item + ',%', BusinessUnitID) > 0
                 AND TOD.ApplicationRoleID = @applicationRoleID
                 AND EM.Isdeleted = 0
                 AND EM.DomainID = @DomainID
                 AND ( @SalesPerson = ''
                        OR EM.FullName LIKE '%' + @SalesPerson + '%' )
          ORDER  BY EM.ModifiedOn DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
