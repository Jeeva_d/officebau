﻿/****************************************************************************                     
CREATED BY   :                  
CREATED DATE  :   14-Nov-2016                  
MODIFIED BY   :                     
MODIFIED DATE  :                     
 <summary>                  
 [Gettransactiondetail] 1,1,1                  
 </summary>                                             
 *****************************************************************************/                  
CREATE PROCEDURE [dbo].[Gettransactiondetail] (@ID       INT,                  
                                              @DomainID INT)                  
AS                  
  BEGIN                  
      SET NOCOUNT ON;                  
                  
      BEGIN TRY                  
          BEGIN TRANSACTION                  
                  
          DECLARE @sum MONEY =( (SELECT ( (Isnull(Sum(QTY*Amount), 0)) +Isnull(Sum(CGST), 0)+Isnull(Sum(SGST), 0) )                 
              FROM   tblExpenseDetails                  
              WHERE  ExpenseID = @ID                  
                     AND ISNULL(IsDeleted, 0) = 0                  
                     AND DomainID = @DomainID)                  
             )                  
                  
          SELECT ex.ID                      AS ID,                  
                 ex.Date                    AS BillDate,                  
                 ex.DueDate                 AS DueDate,                  
                 ep.Date                    AS PaymentDate,                  
                 ex.VendorID                AS VendorID,                  
                 vd.NAME                    AS VendorName,                  
                 vd.BillingAddress          AS BillingAddress,                  
                 vd.PaymentTerms            AS PaymentTerms,                  
                 ex.Remarks                 AS [Description],                  
                 ex.BillNo                  AS BillNo,                  
                 @sum                       AS TotalAmount,                  
                 ex.PaidAmount+(Select ISNULL(SUM(AMOUNT),0) from tblExpenseHoldings   
            Where ExpenseId=ex.ID And Isdeleted=0 and DomainID=@DomainID)              AS PaidAmount,                  
                 ex.Type                    AS ScreenType,                  
                 ep.PaymentModeID           AS PaymentModeID,                  
                 cd.Code                    AS PaymentMode,                  
                 ep.BankID                  AS BankID,                  
                 bk.Name             AS BankName,                  
                 ex.CostCenterID            AS CostCenterID,                  
                 cc.NAME                    AS CostCenterName,                  
                 ep.reference               AS Reference,                  
                 fu.OriginalFileName                AS [FileName],                  
                 Cast(fu.Id AS VARCHAR(50)) AS HistoryID,                  
                 ex.ModifiedOn              AS ModifiedOn,                  
                 ex.createdOn               AS CreatedOn,                  
                 EMP.FullName              AS ModifiedBy ,

				 
				 Stuff((Select ',' + PONO from tblPurchaseOrder 
				 Where isdeleted =0 and ID IN ( (Select POID from tblPODetails where IsDeleted =0 And ID in (
				 (SElect poitemid from tblexpenseDetails where IsDeleted=0 and ExpenseID = ex.ID))))        
                             FOR xml PATH (''), TYPE) .value('.', 'NVARCHAR(MAX)'), 1, 1, '')    As PONos ,
							 ex.IsApprovalRequired,
							 ex.IsApproved              
          FROM   tblExpense ex                  
                 LEFT JOIN tblVendor vd                  
                        ON vd.ID = ex.VendorID                  
                 LEFT JOIN tbl_EmployeeMaster EMP                  
              ON EMP.ID = ex.ModifiedBy                  
                 LEFT JOIN tblExpensePaymentMapping expm                  
                        ON expm.ExpenseID = @ID                  
                 LEFT JOIN tblExpensePayment ep                  
                        ON ep.ID = expm.ExpensePaymentID                  
                 LEFT JOIN tbl_CodeMaster cd                  
                        ON cd.ID = ep.PaymentModeID                  
       LEFT JOIN tblBank bk                  
                        ON bk.ID = ep.BankID       
                 LEFT JOIN tblCostCenter cc                  
                        ON cc.ID = ex.CostCenterID                  
  LEFT JOIN tbl_FileUpload fu     
                     ON fu.Id = ex.HistoryID                  
          --AND fu.IsDeleted = 0                
          WHERE  isnull(ex.IsDeleted, 0) = 0                  
                 AND ( ex.ID = @ID                  
                        OR @ID IS NULL )                
                 AND ex.DomainID = @DomainID                  
                  
          COMMIT TRANSACTION                  
      END TRY                  
      BEGIN CATCH                  
          ROLLBACK TRANSACTION                  
          DECLARE @ErrorMsg    VARCHAR(100),                  
                  @ErrSeverity TINYINT                  
          SELECT @ErrorMsg = Error_message(),                  
                 @ErrSeverity = Error_severity()                  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)                  
      END CATCH                  
  END

