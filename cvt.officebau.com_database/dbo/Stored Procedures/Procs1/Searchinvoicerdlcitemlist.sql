﻿/****************************************************************************   
CREATED BY   : Naneeshwar  
CREATED DATE  :   14-Nov-2016
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [Searchinvoicerdlcitemlist]  39,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchinvoicerdlcitemlist] (@InvoiceID INT,
                                                    @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

         Select p.Name AS ProductName,it.ItemDescription as Description, it.QTY,it.Rate,(it.QTY*it.Rate) AS Amount from tblInvoiceItem it
Join tblProduct_v2 p on it.Productid =p.ID
 WHERE  it.IsDeleted = 0
                 AND @InvoiceID = it.InvoiceID
                 AND it.DomainID = @DomainID
				 Union ALL
				    Select 'Total' AS ProductName,'',0,0,Sum(it.QTY*it.Rate) AS Amount from tblInvoiceItem it
Join tblProduct_v2 p on it.Productid =p.ID
 WHERE  it.IsDeleted = 0
  AND @InvoiceID = it.InvoiceID
                 AND it.DomainID = @DomainID
				  Union ALL
				    Select 'Taxable Value' AS ProductName,'',0,0,Sum(it.QTY*it.Rate) AS Amount from tblInvoiceItem it
Join tblProduct_v2 p on it.Productid =p.ID
 WHERE  it.IsDeleted = 0
  AND @InvoiceID = it.InvoiceID
                 AND it.DomainID = @DomainID
Union All
Select 'ADD SGST ' +convert(varchar,SGST) + ' %','',null,SGST,SUM( ISNULL(SGSTAmount,0)) from tblInvoiceItem it
 WHERE  it.IsDeleted = 0
                 AND @InvoiceID = it.InvoiceID
                 AND it.DomainID = @DomainID and iSNULL(SGST,0)<>0 
group by SGST
Union All
Select 'ADD CGST ' +convert(varchar,CGST) + ' %','',null,CGST,SUM(ISNULL(cGSTAmount,0)) from tblInvoiceItem it
 WHERE  it.IsDeleted = 0
AND @InvoiceID = it.InvoiceID
 AND it.DomainID = @DomainID and iSNULL(CGST,0)<>0 
group by CGST
         

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
