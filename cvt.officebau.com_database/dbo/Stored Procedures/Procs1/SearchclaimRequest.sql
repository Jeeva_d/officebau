﻿  
/****************************************************************************     
CREATED BY   :  Naneeshwar.M  
CREATED DATE  :     
MODIFIED BY   :   Ajith N  
MODIFIED DATE  :   07 Dec 2014  
 <summary>   
          [SearchclaimRequest] 1,0,106,''  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchclaimRequest] (@DomainID      INT,  
                                            @StatusID      INT,  
                                            @EmployeeID    INT,  
                                            @OperationType VARCHAR(20))  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Status VARCHAR(100)  
  
      IF( @OperationType = 'pageload' )  
        SET @Status = 'Rejected,Open'  
      ELSE  
        SET @Status = 'Approved,Submitted,Open,Rejected,Partial,Settled,Resubmitted'  
  
      BEGIN TRY  
          BEGIN ;  
              WITH CTE  
                   AS (SELECT CLR.ID         AS ID,  
                              CLC.NAME       AS CategoryName,  
                              CLR.ApproverID AS ApprovalID,  
                              CASE  
                                WHEN ( CLR.StatusID IN (SELECT ID  
                                                        FROM   tbl_Status  
                                                        WHERE  Code NOT IN( 'Submitted' )  
                                                               AND [Type] = 'Claims') ) THEN  
                              Isnull(EMP.EmpCodePattern, '') +  Isnull(EMP.Code, '') + ' - ' + EMP.FullName  
                              END            AS ApprovarName,  
                              CLR.claimdate  AS ClaimedDate,  
                              CLA.CreatedOn  AS ApprovedDate,  
                              Amount         AS Amount,  
                              CLR.StatusID   AS StatusID,  
                              STA.Code       AS [Status],  
                              CLR.ModifiedOn AS ModifiedOn,  
                              DC.NAME        AS DestinationCity,  
                              ( CASE  
                                  WHEN CFA.FinApprovedAmount IS NULL THEN  
                                    CLA.ApprovedAmount  
                                  ELSE  
                                    CFA.FinApprovedAmount  
                                END )        AS ApprovalAmount  
                       FROM   tbl_ClaimsRequest CLR  
                              LEFT JOIN tbl_ClaimsApprove CLA  
                                     ON CLA.ClaimItemID = CLR.ID  
                              LEFT JOIN tbl_ClaimCategory CLC  
                                     ON CLC.ID = CLR.CategoryID  
                              LEFT JOIN tbl_EmployeeMaster CRE  
                                     ON CRE.ID = CLR.CreatedBy  
                              LEFT JOIN tbl_EmployeeMaster EMP  
                                     ON emp.ID = CLR.ApproverID  
                              LEFT JOIN tbl_Status STA  
                                     ON STA.ID = CLR.StatusID  
                              LEFT JOIN tbl_DestinationCities DC  
                                     ON DC.ID = CLR.DestinationID  
                              LEFT JOIN tbl_ClaimsFinancialApprove CFA  
                                     ON CFA.ClaimItemID = CLR.ID  
                       WHERE  CLR.CreatedBy = @EmployeeID  
                              AND CLR.IsDeleted = 0  
                              AND ( ( Isnull(@StatusID, 0) = 0  
                                      AND CLR.StatusID IN((SELECT ID  
                                                           FROM   tbl_Status  
                                                           WHERE  Code IN(SELECT Splitdata  
                                                                          FROM   FnSplitString(@Status, ','))  
                                                                  AND [Type] = 'Claims'  
AND IsDeleted = 0)) )  
                             OR CLR.StatusID = @StatusID )  
                              AND CLR.DomainId = @DomainID)  
              SELECT ID                                         AS ID,  
                     CategoryName                               AS CategoryName,  
                     ApprovalID                                 AS ApprovalID,  
                     ISNULL(ApprovarName, '')                   AS ApprovarName,  
                     ClaimedDate                                AS ClaimedDate,  
                     ApprovedDate                               AS ApprovedDate,  
                     Sum(amount)                                AS Amount,  
                     StatusID                                   AS StatusID,  
                     [Status]                                   AS [Status],  
                     ModifiedOn,  
                     DestinationCity,  
                     -- ApprovalAmount,  
                     ( CASE  
                         WHEN StatusID = (SELECT ID  
                                          FROM   tbl_Status  
                                          WHERE  Code = 'Rejected'  
                                                 AND [Type] = 'Claims'  
                                                 AND IsDeleted = 0) THEN  
                           NULL  
                         ELSE  
                           ApprovalAmount  
                       END )                                    AS ApprovalAmount,  
                     Cast(ROW_NUMBER()  
                            OVER(  
                              ORDER BY ModifiedOn DESC) AS INT) AS RowNo  
              FROM   CTE  
              GROUP  BY ID,  
                        CategoryName,  
                        ApprovalID,  
                        ApprovarName,  
                        ClaimedDate,  
                        ApprovedDate,  
                        StatusID,  
                        ModifiedOn,  
                        [Status],  
                        DestinationCity,  
                        ApprovalAmount  
              ORDER  BY ModifiedOn DESC  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
