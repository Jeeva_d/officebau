﻿/****************************************************************************   
CREATED BY   :  Ajith N  
CREATED DATE  : 23 May 2017
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
	DMLSearchHistory 'BRS', 'StartDate', '1/1/2017 12:00:00 AM', 13, 8
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[DMLSearchHistory] (@FormName        VARCHAR(200),
                                          @Event           VARCHAR(20),
                                          @SearchParameter VARCHAR(200),
                                          @Value           VARCHAR(200),
                                          @UserID          INT,
                                          @DomainID        INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION DML

          IF( @Event = 'pageload' )
            BEGIN
                IF( (SELECT COUNT(1)
                     FROM   tblSearchHistory
                     WHERE  FormName = @FormName
                            AND Parameter = @SearchParameter
                            AND UserID = @UserID
                            AND DomainID = @DomainID) = 0 )
                  BEGIN
                      INSERT INTO tblSearchHistory
                                  (FormName,
                                   Parameter,
                                   Value,
                                   UserID,
                                   DomainID)
                      VALUES      (@FormName,
                                   @SearchParameter,                                  								  
								   @Value,
                                   @UserID,
                                   @DomainID)
                  END
            END
          ELSE
            BEGIN
                UPDATE tblSearchHistory
                SET    Value = @Value
                WHERE  FormName = @FormName
                       AND Parameter = @SearchParameter
                       AND UserID = @UserID
                       AND DomainID = @DomainID
            END

          SELECT Value
          FROM   tblSearchHistory
          WHERE  FormName = @FormName
                 AND Parameter = @SearchParameter
                 AND UserID = @UserID
                 AND DomainID = @DomainID

          COMMIT TRANSACTION DML
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION DML
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
