﻿/**************************************************************************** 
CREATED BY			:	DhanaLakshmi.S
CREATED DATE		:	05 Jun 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
     
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Getemployee] (@EmployeeID INT,
                                     @DomainID   INT,
                                     @CALID      INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          SELECT EMP.ID                    AS EmployeeID,
                 EMP.Code                  AS EmployeeCode,
                 EMP.FirstName             AS FirstName,
                 EMP.LastName              AS LastName,
                 EMP.FatherName            AS FatherName,
                 EMP.Gender                AS Gender,
                 EMP.Qualification         AS Qualification,
                 EMP.EmailID               AS EmailID,
                 EMP.ContactNo             AS ContactNo,
                 EMP.DOJ                   AS DOJ,
                 EMP.DOC                   AS DOC,
                 EMP.EmploymentTypeID      AS EmploymentTypeID,
                 EMP.DesignationID         AS DesignationID,
                 DEG.Code                  AS Designation,
                 EMP.DepartmentID          AS DepartmentID,
                 DEP.Code                  AS Department,
                 Personal.StateID          AS StateID,
                 STA.Code                  AS [State],
                 Personal.CountryID        AS CountryID,
                 CON.Code                  AS Country,
                 Personal.DateOfBirth      AS DateOfBirth,
                 Personal.BloodGroupID     AS BloodGroupID,
                 Personal.PresentAddress   AS PresentAddress,
                 Personal.PermanentAddress AS PermanentAddress,
                 Personal.StateID          AS StateID,
                 Personal.Anniversary      AS Anniversary,
                 Personal.SpouseName       AS SpouseName,
                 bank.BankName             AS BankName,
                 bank.BranchName           AS BranchName,
                 bank.AccountNo            AS AccountNo,
                 bank.AccountTypeID        AS AccountTypeID,
                 bank.PANCard              AS PANCard,
                 PAS.TypeID                AS PassportTypeID,
                 PAS.PassportNo            AS PassportNo,
                 PAS.PassportExpiryDate    AS PassportExpiryDate,
                 PAS.IssueDate             AS IssueDate,
                 PAS.IssuedPlace           AS IssuedPlace,
                 PAS.Remarks               AS Remarks
          FROM   tbl_EmployeeMaster EMP
                 JOIN Designation DEG
                   ON EMP.DesignationID = DEG.ID
                 JOIN Department DEP
                   ON EMP.DepartmentID = DEP.ID
                 LEFT JOIN tbl_EmployeePersonalDetails Personal
                        ON EMP.ID = Personal.EmployeeID
                 LEFT JOIN tbl_EmployeeStatutoryDetails bank
                        ON EMP.ID = bank.EmployeeID
                           AND bank.IsActive = 1
                 LEFT JOIN tbl_EmployeePassportDetails PAS
                        ON EMP.ID = PAS.EmployeeID
                           AND PAS.IsDeleted = 0
                 LEFT JOIN [State] STA
                        ON Personal.StateID = STA.ID
                 LEFT JOIN Country CON
                        ON Personal.CountryID = CON.ID
          WHERE  EMP.ID = @EmployeeID
                 AND EMP.IsDeleted = 0
				  AND ISNULL(EMP.IsActive, 0) = 0
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
