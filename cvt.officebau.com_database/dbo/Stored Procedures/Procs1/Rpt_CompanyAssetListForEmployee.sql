﻿/****************************************************************************           
CREATED BY   : Ajith N          
CREATED DATE  : 31 Aug 2018          
MODIFIED BY   :           
MODIFIED DATE  :           
 <summary>                  
   [Rpt_CompanyAssetListForEmployee]  '' , '', '', 'false', 2,1          
 </summary>                                    
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Rpt_CompanyAssetListForEmployee] (@DepartmentID  INT,      
                                                         @DesignationID INT,      
                                                         @BusinessUnit  VARCHAR(100),      
                                                         @IsActive      BIT,      
                                                         @UserID        INT,      
                                                         @DomainID      INT)      
AS      
      
      
BEGIN      
    SET NOCOUNT ON;      
      
    BEGIN TRY      
        DECLARE @BusinessUnitIDs VARCHAR(50)      
      
        IF( Isnull(@BusinessUnit, 0) = 0 )      
          SET @BusinessUnitIDs = (SELECT BusinessUnitID      
                                  FROM   tbl_EmployeeMaster      
                                  WHERE  ID = @UserID      
                                         AND DomainID = @DomainID)      
        ELSE      
          SET @BusinessUnitIDs = ',' + @BusinessUnit + ','      
      
        DECLARE @BusinessUnitTable TABLE      
          (      
             BusinessUnit VARCHAR(100)      
          )      
      
        INSERT INTO @BusinessUnitTable      
        SELECT @BusinessUnitIDs      
      
        SELECT Isnull(EM.EmpCodePattern, '') + EM.Code                     AS [Employee Code],      
               EM.FullName                 AS [Employee Name],      
               --REGION.NAME                                            AS [Region],          
               --DEP.NAME                                               AS [Department],          
               --DESI.NAME                                              AS [Designation],          
               AT.NAME                     AS [Asset Type],      
               CA.Make                     AS [Make],      
               CA.SerialNo                 AS [Serial No],      
               CA.QTY                      AS [Quantity],      
               Cast(CA.HandoverOn AS DATE) AS [Given On],      
               Cast(ca.ReturnedOn AS DATE) AS [Returned On],      
               CA.ReturnedRemarks          AS [Returned Remarks],      
               ( CASE      
                   WHEN CA.ReturnStatus = 0 THEN 'Issued'      
                   WHEN AT.NAME IS NULL THEN ''      
                   ELSE 'Returned'      
                 END )                     AS [Returned Status],      
               ( CASE      
                   WHEN EM.IsActive = 0 THEN 'Active'      
                   ELSE 'Inactive'      
                 END )                     AS [Status],      
               BU.NAME                     AS [Business Unit]      
        FROM   tbl_CompanyAssets CA   
         LEFT JOIN tbl_EmployeeMaster EM      
                      ON CA.EmployeeID = EM.ID    
               LEFT JOIN tbl_Department DEP      
                      ON DEP.ID = EM.DepartmentID      
               LEFT JOIN tbl_Designation DESI      
                      ON DESI.ID = EM.DesignationID      
               LEFT JOIN tbl_Functional FUN      
                      ON FUN.ID = EM.FunctionalID      
               LEFT JOIN tbl_BusinessUnit BU      
                      ON BU.ID = EM.BaseLocationID      
               LEFT JOIN tbl_BusinessUnit REGION      
                      ON REGION.ID = EM.RegionID      
               LEFT JOIN tbl_AssetType AT      
                      ON AT.ID = CA.AssetTypeID      
               JOIN @BusinessUnitTable TBU      
                 ON TBU.BusinessUnit LIKE '%,'      
                          + Cast(EM.BaseLocationID AS VARCHAR(10))      
                                          + ',%'      
        WHERE  EM.IsDeleted = 0  
        AND CA.IsDeleted = 0     
               AND ( @BusinessUnit = 0      
                      OR EM.BaseLocationID = @BusinessUnit )      
               AND ( EM.IsActive IS NULL      
                      OR EM.IsActive = @IsActive )      
               AND CA.domainID = @DomainID      
               AND ( Isnull(@DepartmentID, '') = ''      
                      OR DEP.ID = @DepartmentID )      
               AND ( Isnull(@DesignationID, '') = ''      
                      OR DESI.ID = @DesignationID )      
         ORDER  BY EM.Code      
    END TRY      
    BEGIN CATCH      
        DECLARE @ErrorMsg    VARCHAR(100),      
                @ErrSeverity TINYINT;      
        SELECT @ErrorMsg = Error_message(),      
               @ErrSeverity = Error_severity()      
        RAISERROR(@ErrorMsg,@ErrSeverity,1)      
    END CATCH      
END
