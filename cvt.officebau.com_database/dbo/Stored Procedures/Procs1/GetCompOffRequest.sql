﻿/****************************************************************************       
CREATED BY  : K.SASIREKHA     
CREATED DATE : 04-10-2017     
MODIFIED BY  :       
MODIFIED DATE :       
 <summary>    
  [GetCompOffRequest] 1,1    
 </summary>                               
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetCompOffRequest] (@ID       INT,  
                                           @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT ECO.ID                           AS ID,  
                 ECO.Date                         AS Date,  
                 ATT.Duration                     AS Duration,  
                 IsNull(ECO.RequesterRemarks, '') AS Remarks,  
                 IsNull(EMR.EmpCodePattern, '') + '' + EMR.Code + ' - ' + EMR.FullName  AS ApprovarName,  
                 ECO.ModifiedOn                   AS ModifiedOn,  
                 EM.FullName                      AS ModifiedBy,  
                 EMA.FullName                     AS EmployeeName,  
                 STA.Code                         AS Status,  
                 ECO.ApproverRemarks              AS ApproverRemarks,  
                 ECO.ApprovedDate                 AS ApprovedDate,  
                 ECO.ApproverID                   AS ApproverID,  
                 ECO.CreatedBy                    AS RequesterID,  
                 ECO.HRApproverRemarks            AS HRRemarks,  
                 AD.NAME                          AS ApproverDesignation,  
                 AP.Code                          AS ApproverPrefix,  
                 ED.NAME                          AS Designation,  
                 EP.Code                          AS Prefix,  
                 EMA.EmailID                      AS EmployeeEmail,  
                 EMR.EmailID                      AS ApproverEmail,  
                 HR.EmailID                       AS HREmail,  
                 HR.FullName                      AS HRName,  
                 HRP.Code                         AS HRPrefix,  
                 HRD.NAME                         AS HRDesignation,  
                 STHR.Code                        AS HRStatus,  
                 ECO.HRApprovedDate               AS HRApprovedDate  
          FROM   tbl_EmployeeCompOff ECO  
                 LEFT JOIN tbl_Attendance ATT  
                        ON ECO.CreatedBy = ATT.EmployeeID  
                           AND ECO.Date = ATT.LogDate  
                 LEFT JOIN tbl_EmployeeMaster EM  
                        ON EM.ID = ECO.ModifiedBy  
                 LEFT JOIN tbl_EmployeeMaster EMA  
                        ON EMA.ID = ECO.CreatedBy  
                 LEFT JOIN tbl_EmployeeMaster EMR  
                        ON EMR.ID = ECO.ApproverID  
                 LEFT JOIN tbl_Status STA  
                        ON STA.ID = ECO.StatusID  
                 LEFT JOIN tbl_Designation AD  
                        ON AD.ID = EMR.DesignationID  
                 LEFT JOIN tbl_CodeMaster AP  
                        ON AP.ID = EMR.PrefixID  
                 LEFT JOIN tbl_Designation ED  
                        ON ED.ID = EMA.DesignationID  
                 LEFT JOIN tbl_CodeMaster EP  
                        ON EP.ID = EMA.PrefixID  
                 LEFT JOIN tbl_EmployeeMaster HR  
                        ON HR.ID = ECO.HRApproverID  
                 LEFT JOIN tbl_Designation HRD  
                        ON HRD.ID = HR.DesignationID  
                 LEFT JOIN tbl_CodeMaster HRP  
                        ON HRP.ID = HR.PrefixID  
                 LEFT JOIN tbl_Status STHR  
                        ON STHR.ID = ECO.HRStatusID  
          WHERE  ECO.ID = @ID  
                 AND ECO.DomainID = @DomainID  
                 AND ECO.IsDeleted = 0  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
