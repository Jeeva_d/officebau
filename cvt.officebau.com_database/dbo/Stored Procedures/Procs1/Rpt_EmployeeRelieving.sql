﻿/****************************************************************************             
CREATED BY  :  JENNIFER.S          
CREATED DATE :  18-AUG-2017          
MODIFIED BY  :             
MODIFIED DATE :             
 <summary>           
          [Rpt_EmployeeRelieving] '','23-AUG-2017',null,null,1,421          
 </summary>                                     
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Rpt_EmployeeRelieving] (@FromDate       DATE,      
                                                @ToDate         DATE,      
                                                @RegionID       INT,      
                                                @BusinessUnitID INT,      
                                                @DomainID       INT,      
                                                @SessionID      INT)      
AS      
  BEGIN      
      SET NOCOUNT ON;      
      
      BEGIN TRY      
          BEGIN      
              SELECT EMP.ID,      
                     Isnull(EMP.EmpCodePattern, '') + EMP.Code                                                                  EmployeeCode,      
                     EMP.FirstName + ' ' + Isnull(EMP.LastName, '')                            NAME,      
                     REG.NAME                                                                  Region,      
                     BST.NAME                                                                  BusinessUnit,      
                     DEP.NAME                                                                  DepartmentName,      
                     DEG.NAME                                                                  DesignationName,      
                     EMP.DOJ,      
                     GEN.Code                                                                  Gender,      
                     EMP.InactiveFrom                                                          RelievingDate,      
                     MST.Code                                                                  MaritalStatus,      
                     EPD.PresentAddress,      
                     EMP.ContactNo,      
                     EMP.EmailID,      
                     PERSONAL.FatherName                                                       AS FatherName,      
                     STATUTORY.PFNo                                                            AS PFNo,      
                     STATUTORY.ESINo                                                           AS ESINo,      
                     (SELECT TOP 1 PSD.Amount      
                      FROM  tbl_pay_employeepaystructure EPS 
					  join tbl_Pay_EmployeePayStructureDetails PSD
						on psd.PayStructureId = eps.ID and PSD.DomainId = EPS.DomainID and PSD.Isdeleted = 0
					  join    tbl_Pay_PayrollCompontents PC 
						on PSD.ComponentId = pc.ID AND PSD.DomainID = pc.DomainID and pc.IsDeleted = 0
                      WHERE  EPS.employeeID = EMP.ID   AND
					     EPS.IsDeleted = 0 and pc.Code = 'GRO'
                      ORDER  BY EPS.EffectiveFrom DESC)                                        AS GrossSalary,      
                     STATUTORY.UANNo                                                           AS UAN,      
                     STATUTORY.IFSCCode                                                        AS IFSCCode,      
                     STATUTORY.BankName                                                        AS BankName,      
                     STATUTORY.AadharID                                                        AS AadharID,      
                     STATUTORY.PAN_No                                                          AS PANNO,      
                     EPD.DateOfBirth                                                           AS DOB,      
                     STATUTORY.AccountNo                                                       AS AccountNo,      
                     ( CASE      
                         WHEN Isnull(emp.IsActive, 0) = 0 THEN 'Active'      
                         ELSE 'Inactive'      
                       END )                                                                   AS [status],      
                     Cast(Row_number()      
                            OVER(      
            ORDER BY EMP.FirstName + ' ' + Isnull(EMP.LastName, '')) AS INT) AS RowNo      
              FROM   tbl_EmployeeMaster EMP      
              LEFT JOIN tbl_BusinessUnit BST      
                            ON EMP.BaseLocationID = BST.ID      
             LEFT JOIN tbl_BusinessUnit REG      
                            ON EMP.RegionID = REG.ID      
                     LEFT JOIN tbl_EmployeePersonalDetails EPD      
                            ON EMP.ID = EPD.EmployeeID      
                     LEFT JOIN tbl_Department DEP      
                            ON EMP.DepartmentID = DEP.ID      
                     LEFT JOIN tbl_Designation DEG      
                            ON EMP.DesignationID = DEG.ID      
                     LEFT JOIN tbl_CodeMaster GEN      
                            ON EMP.GenderID = GEN.ID      
                               AND GEN.[Type] = 'Gender'      
                     LEFT JOIN tbl_CodeMaster MST      
                            ON EPD.MaritalStatusID = MST.ID      
                               AND MST.[Type] = 'MaritalStatus'      
                     LEFT JOIN tbl_EmployeePersonalDetails PERSONAL      
                            ON EMP.id = PERSONAL.EmployeeID      
                               AND PERSONAL.IsDeleted = 0      
                     LEFT JOIN tbl_EmployeeStatutoryDetails STATUTORY      
                            ON EMP.id = STATUTORY.EmployeeID      
                               AND STATUTORY.IsDeleted = 0      
              WHERE  EMP.IsDeleted = 0      
                     AND Isnull(EMP.IsActive, 0) = 1      
                     AND EMP.Code NOT LIKE 'TMP_%'      
                     AND EMP.DomainID = @DomainID      
                     AND ( Isnull(@RegionID, '') = ''      
                            OR ( Isnull(@RegionID, '') <> ''      
                                 AND EMP.RegionID = @RegionID ) )      
                     AND ( ( Isnull(@FromDate, '') = ''      
                             AND Isnull(@ToDate, '') = '' )      
                            OR ( Isnull(@FromDate, '') <> ''      
                                 AND Isnull(@ToDate, '') = ''      
                                 AND EMP.InactiveFrom >= @FromDate )      
                            OR ( Isnull(@FromDate, '') = ''      
                                 AND Isnull(@ToDate, '') <> ''      
                                 AND EMP.InactiveFrom <= @ToDate )      
                            OR ( Isnull(@FromDate, '') <> ''      
                                 AND Isnull(@ToDate, '') <> ''      
                                 AND EMP.InactiveFrom BETWEEN @FromDate AND @ToDate ) )      
                     AND ( ( Isnull(@BusinessUnitID, '') = ''      
                             AND EMP.BaseLocationID IN (SELECT Splitdata      
                                                        FROM   dbo.Fnsplitstring ((SELECT BusinessUnitID      
                                                                                   FROM   tbl_EmployeeMaster      
                                                                                   WHERE  ID = @SessionID), ',')      
                                                        WHERE  Isnull(Splitdata, '') <> '') )      
                            OR ( Isnull(@BusinessUnitID, '') <> ''      
                                 AND EMP.BaseLocationID = @BusinessUnitID ) )      
              ORDER  BY EMP.Code     
          END      
      END TRY      
      BEGIN CATCH      
          DECLARE @ErrorMsg    VARCHAR(100),      
                  @ErrSeverity TINYINT      
          SELECT @ErrorMsg = Error_message(),      
                 @ErrSeverity = Error_severity()      
          RAISERROR(@ErrorMsg,@ErrSeverity,1)      
      END CATCH      
  END
