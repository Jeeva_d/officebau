﻿/****************************************************************************     
CREATED BY   :   Dhanalakshmi  
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [GetGroupLedger] 1011,1,1  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Getgroupledger] (@ID       INT,  
                                         @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT gl.ID         AS ID,  
                 NAME          AS NAME,  
                 Remarks       AS Remarks,  
                 ReportType    AS ReportType,  
                 gl.ModifiedOn AS ModifiedOn,  
                 EMP.FullName AS ModifiedBy  
          FROM   tblGroupLedger gl  
                 LEFT JOIN tbl_EmployeeMaster EMP  
                        ON EMP.ID = gl.ModifiedBy  
          WHERE  gl.IsDeleted = 0  
                 AND gl.ID = @ID AND gl.DomainID = @DomainID  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
