﻿
/**************************************************************************** 
CREATED BY    		:	Dhanalakshmi. S
CREATED DATE		:	20 MAR 2018
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
	[ReportDashoardTiles]  1
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ReportDashoardTiles] (@DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT (SELECT   Count(1)      
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
                 LEFT JOIN tbl_Designation DESI
                        ON DESI.ID = EM.DesignationID
          WHERE  EM.IsDeleted = 0
                 AND Isnull(EM.IsActive, 0) = 0
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND EM.DomainID = @DomainID ) AS HeadcountforDesignation,


				(SELECT   Count(1)                
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
                 LEFT JOIN tbl_Department DEP
                        ON DEP.ID = EM.DesignationID
          WHERE  EM.IsDeleted = 0
                 AND Isnull(EM.IsActive, 0) = 0
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND EM.DomainID = @DomainID) As HeadcountforDepartment,

				 (SELECT   Sum(PS.CTC)                
          FROM   tbl_EmployeeMaster EM
		         LEFT JOIN tbl_EmployeePayStructure PS
						ON PS.EmployeeId = EM.ID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
                 LEFT JOIN tbl_Designation DEP
                        ON DEP.ID = EM.DesignationID
          WHERE  EM.IsDeleted = 0
                 AND Isnull(EM.IsActive, 0) = 0
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND EM.DomainID = @DomainID) As CompensationforDesignation,

				  (SELECT   Sum(PS.CTC)                 
          FROM   tbl_EmployeeMaster EM
		         LEFT JOIN tbl_EmployeePayStructure PS
						ON PS.EmployeeId = EM.ID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
                 LEFT JOIN tbl_Department DEP
                        ON DEP.ID = EM.DesignationID
          WHERE  EM.IsDeleted = 0
                 AND Isnull(EM.IsActive, 0) = 0
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND EM.DomainID = @DomainID) As CompensationforDepartment



				 Into #temptable
				 Select * from #temptable

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
