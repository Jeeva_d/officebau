﻿/****************************************************************************   
CREATED BY   :	Ajith N
CREATED DATE  : 16 Feb 2018
MODIFIED BY   :	 
MODIFIED DATE  :  
 <summary> 
		CreateMenuAccess 1,1 
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[CreateMenuAccess] (@EmployeeID INT,
                                          @DomainID   INT,
                                          @IsManual   BIT = 0)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          --BEGIN TRANSACTION DML_RBS
          DECLARE @count       INT = 0,
                  @IsDeleted   BIT = 0,
                  @IsActive    BIT = 0,
                  @CurrentDate DATETIME = Getdate(),
                  @UserID      INT = 1,
                  @SubModuleID INT = 0,
                  @MenuID      INT = 0,
                  @MenuCode    VARCHAR(20) = '',
                  @Output      VARCHAR(100) = 'Operation Failed!'

          SELECT ID,
                 ModuleID,
                 SubModule,
                 Description,
                 SubModuleOrderID,
                 CreatedBy,
                 CreatedOn,
                 ModifiedBy,
                 ModifiedOn,
                 IsDeleted,
                 Newid() AS HistoryID,
                 MenuIcon
          INTO   #tmpSubModule
          FROM   tbl_SubModule
          WHERE  IsDeleted = 0
          ORDER  BY ID ASC

          SELECT ID,
                 MenuCode,
                 Menu,
                 Description,
                 MenuURL,
                 SubModuleID,
                 MenuOrderID,
                 CreatedBy,
                 CreatedOn,
                 ModifiedBy,
                 ModifiedOn,
                 HistoryID,
                 IsSubmenu,
                 MenuIcon,
                 RootConfig,
                 IsMobility
          INTO   #tmpMenu
          FROM   tbl_Menu
          ORDER  BY SubModuleID ASC

          IF NOT EXISTS(SELECT 1
                        FROM   tbl_RBSSubModule
                        WHERE  DomainID = @DomainID)
            BEGIN
                ------------------------ Sub-Module & Menus ------------------------
                WHILE( @count <= (SELECT Max(ID)
                                  FROM   #tmpSubModule) )
                  BEGIN
                      IF EXISTS(SELECT 1
                                FROM   #tmpSubModule
                                WHERE  ID = @count)
                        BEGIN
                            INSERT INTO tbl_RBSSubModule
                                        (ModuleID,
                                         SubModule,
                                         Description,
                                         SubModuleOrderID,
                                         CreatedBy,
                                         CreatedOn,
                                         ModifiedBy,
                                         ModifiedOn,
                                         IsDeleted,
                                         HistoryID,
                                         MenuIcon,
                                         DomainID)
                            SELECT ModuleID,
                                   SubModule,
                                   Description,
                                   SubModuleOrderID,
                                   @UserID,
                                   @CurrentDate,
                                   @UserID,
                                   @CurrentDate,
                                   IsDeleted,
                                   Newid(),
                                   MenuIcon,
                                   @DomainID
                            FROM   #tmpSubModule
                            WHERE  ID = @count

                            SET @SubModuleID = Ident_current('tbl_RBSSubModule')

                            INSERT INTO tbl_RBSMenu
                                        (MenuCode,
                                         Menu,
                                         Description,
                                         MenuURL,
                                         SubModuleID,
                                         MenuOrderID,
                                         CreatedBy,
                                         CreatedOn,
                                         ModifiedBy,
                                         ModifiedOn,
                                         HistoryID,
                                         IsSubmenu,
                                         MenuIcon,
                                         DomainID,
                                         RootConfig,
                                         Isdeleted,
                                         IsMobility)
                            SELECT MenuCode,
                                   Menu,
                                   Description,
                                   MenuURL,
                                   CASE
                                     WHEN @count = 0 THEN
                                       @count
                                     ELSE
                                       @SubModuleID
                                   END,
                                   MenuOrderID,
                                   @UserID,
                                   @CurrentDate,
                                   @UserID,
                                   @CurrentDate,
                                   Newid(),
                                   IsSubmenu,
                                   MenuIcon,
                                   @DomainID,
                                   RootConfig,
                                   0,
                                   IsMobility
                            FROM   #tmpMenu
                            WHERE  SubModuleID = @count
                                   AND RootConfig NOT IN ( 'REPORT', 'CONFIG' )
                        END
                      ELSE
                        BEGIN
                            SET @SubModuleID = 0

                            IF( @count = 0 )
                              BEGIN
                                  INSERT INTO tbl_RBSMenu
                                              (MenuCode,
                                               Menu,
                                               Description,
                                               MenuURL,
                                               SubModuleID,
                                               MenuOrderID,
                                               CreatedBy,
                                               CreatedOn,
                                               ModifiedBy,
                                               ModifiedOn,
                                               HistoryID,
                                               IsSubmenu,
                                               MenuIcon,
                                               DomainID,
                                               RootConfig,
                                               Isdeleted,
                                               IsMobility)
                                  SELECT MenuCode,
                                         Menu,
                                         Description,
                                         MenuURL,
                                         CASE
                                           WHEN @count = 0 THEN
                                             @count
                                           ELSE
                                             @SubModuleID
                                         END,
                                         MenuOrderID,
                                         @UserID,
                                         @CurrentDate,
                                         @UserID,
                                         @CurrentDate,
                                         Newid(),
                                         IsSubmenu,
                                         MenuIcon,
                                         @DomainID,
                                         RootConfig,
                                         0,
                                         IsMobility
                                  FROM   #tmpMenu
                                  WHERE  SubModuleID = @count
                                         AND RootConfig NOT IN ( 'REPORT', 'CONFIG' )
                              END
                        END

                      SET @count = @count + 1
                  END

                ------------------------ Configuration & Report ------------------------
                INSERT INTO tbl_RBSMenu
                            (MenuCode,
                             Menu,
                             Description,
                             MenuURL,
                             SubModuleID,
                             MenuOrderID,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             HistoryID,
                             IsSubmenu,
                             MenuIcon,
                             DomainID,
                             RootConfig,
                             Isdeleted,
                             IsMobility)
                SELECT MenuCode,
                       Menu,
                       Description,
                       MenuURL,
                       SubModuleID,
                       MenuOrderID,
                       @UserID,
                       @CurrentDate,
                       @UserID,
                       @CurrentDate,
                       Newid(),
                       IsSubmenu,
                       MenuIcon,
                       @DomainID,
                       RootConfig,
                       1,
                       IsMobility
                FROM   #tmpMenu
                WHERE  RootConfig IN ( 'CONFIG', 'REPORT' )

                SET @count = (SELECT ( Max(ID) + 1 )
                              FROM   tbl_RBSMenu
                              WHERE  DomainID = @DomainID
                                     AND RootConfig NOT IN ( 'CONFIG', 'REPORT' ))

                DECLARE @MaxSubModuleID INT = (SELECT ( Max(ID) + 1 )
                   FROM   tbl_RBSMenu
                   WHERE  DomainID = @DomainID
                          AND RootConfig NOT IN ( 'CONFIG', 'REPORT' ))

                WHILE( @count <= (SELECT Max(ID)
                                  FROM   tbl_RBSMenu
                                  WHERE  DomainID = @DomainID
                                         AND RootConfig IN ( 'CONFIG', 'REPORT' )) )
                  BEGIN
                      IF EXISTS(SELECT 1
                                FROM   tbl_RBSMenu
                                WHERE  IsSubmenu = 1
                                       AND DomainID = @DomainID
                                       AND ID = @count)
                        BEGIN
                            SET @MenuID = (SELECT ID
                                           --CASE
                                           --                                         WHEN ID > @MaxSubModuleID THEN
                                           --                                           ID
                                           --                                         ELSE
                                           --                                           NULL
                                           --                                       END
                                           FROM   tbl_RBSMenu
                                           WHERE  DomainID = @DomainID
                                                  AND MenuCode = (SELECT MenuCode
                                                                  FROM   #tmpMenu
                                                                  WHERE  ID = (SELECT SubModuleID
                                                                               FROM   tbl_RBSMenu
                                                                               WHERE  DomainID = @DomainID
                                                                                      AND ID = @count)))

                            UPDATE tbl_RBSMenu
                            SET    SubModuleID = @MenuID,
                                   Isdeleted = 0
                            WHERE  IsSubmenu = 1
                                   AND DomainID = @DomainID
                                   AND ID = @count
                                   AND @MenuID IS NOT NULL
                        END

                      SET @count = @count + 1
                  END

                -- Insert Menu Permission	 
                INSERT INTO tbl_RBSUserMenuMapping
                            (EmployeeID,
                             MenuID,
                             MRead,
                             MWrite,
                             MEdit,
                             MDelete,
                             CreatedBy,
                             CreatedOn,
                             ModifiedBy,
                             ModifiedOn,
                             IsDeleted,
                             DomainID,
                             HistoryID)
                SELECT @EmployeeID,
                       ID,
                       1,
                       1,
                       1,
                       1,
                       @UserID,
                       @CurrentDate,
                       @UserID,
                       @CurrentDate,
                       @IsDeleted,
                       @DomainID,
                       Newid()
                FROM   tbl_RBSMenu
                WHERE  DomainID = @DomainID

                SET @Output = 'Inserted Successfully.'
            END
          ELSE
            BEGIN
                SET @Output = 'Already Exist.'
            END

          IF( @IsManual = 1 )
            BEGIN
                SELECT @Output AS Output
            END
      -- COMMIT TRANSACTION DML_RBS
      END TRY
      BEGIN CATCH
          -- ROLLBACK TRANSACTION DML_RBS
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
