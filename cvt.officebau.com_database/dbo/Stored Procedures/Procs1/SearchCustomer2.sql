﻿/****************************************************************************     
CREATED BY   : Dhanalakshmi    
CREATED DATE  :  14/11/2016   
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>  
 [SearchVendor] '',1,1    
 select * from tblvendor  
  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchCustomer2] (@Name     VARCHAR(100),  
                                          @DomainID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SELECT cs.ID           AS ID,  
                 cs.NAME         AS NAME,  
                 ContactNo       AS ContactNo,  
                 ContactPerson   AS ContactPerson,  
                 ContactPersonNo AS ContactPersonNo,  
                 cty.NAME        AS CityName,  
                 cur.NAME        AS CurrencyName,  
                 cs.CurrencyID   AS CurrencyID,  
                 PANNo           AS PanNO,
                 cs.GSTNo        AS GSTNo  
          FROM   tblCustomer cs  
                 LEFT JOIN tbl_City cty  
                        ON cty.ID = cs.CityID  
                 LEFT JOIN tblCurrency cur  
                        ON cur.ID = cs.CurrencyID  
          WHERE  cs.NAME LIKE '%' + Isnull(@Name, '') + '%'  
                 AND cs.DomainID = @DomainID  
                 AND cs.IsDeleted = 0  
          ORDER  BY cs.ModifiedOn DESC  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
