﻿/****************************************************************************                     
CREATED BY   : Priya. K                    
CREATED DATE  :   20-NOV-2018                  
MODIFIED BY   :                   
MODIFIED DATE  :                  
 <summary>                  
 [SearchStockAdjustment] 1,0,0, ''            
 </summary>                                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchStockAdjustment] (@DomainID      INT,
                                               @ProductID     INT,
                                               @LedgerID      INT,
                                               @OperationType VARCHAR(20))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF( @OperationType = 'StockMaintenance' )
            BEGIN
                SELECT isa.ID                                    AS ID,
                       isa.AdjustedDate                          AS AdjustedDate,
                       isa.LedgerID                              AS LedgerID,
                       ldg.NAME                                  AS LedgerName,
                       pt.NAME                                   AS ProductName,
                       isa.ReasonID                              AS ReasonID,
                       rsn.NAME                                  AS ReasonName,
                       isa.Remarks                               AS Description,
                       isai.AdjustedQty                          AS AdjustedQty,
                       Isnull(em.Code, '') + ' - ' + em.FullName AS AdjustedBy,
                       isa.ModifiedOn                            AS ModifiedOn,
                       EM.FullName                               AS ModifiedBy
                FROM   tbl_InventoryStockAdjustment isa
                       LEFT JOIN tbl_InventoryStockAdjustmentItems isai
                              ON isa.ID = isai.AdjustmentID
                       LEFT JOIN tblLedger ldg
                              ON ldg.ID = isa.LedgerID
                       LEFT JOIN tblProduct_V2 pt
                              ON pt.ID = isai.ProductID
                       LEFT JOIN tbl_InventoryReason rsn
                              ON rsn.ID = isa.ReasonID
                       LEFT JOIN tbl_EmployeeMaster EM
                              ON EM.ID = isa.ModifiedBy
                WHERE  isa.IsDeleted = 0
                       AND isa.DomainID = @DomainID
                       AND ( @ProductID = 0
                              OR isai.ProductID = @ProductID )
                ORDER  BY isa.ModifiedOn DESC
            END
          ELSE
            BEGIN
                SELECT DISTINCT isa.ID                                    AS ID,
                                isa.AdjustedDate                          AS AdjustedDate,
                                rsn.NAME                                  AS ReasonName,
                                --pt.NAME                                   AS ProductName,    
                                isa.Remarks                               AS Description,
                                -- (select AdjustedQty from tbl_InventoryStockAdjustmentItems isai where isa.ID = isai.AdjustmentID)  as AdjustedQty,        
                                0                                         AS AdjustedQty,
                                isa.LedgerID                              AS LedgerID,
                                ldg.NAME                                  AS LedgerName,
                                Isnull(em.Code, '') + ' - ' + em.FullName AS AdjustedBy,
                                isa.ModifiedOn                            AS ModifiedOn,
                                EM.FullName                               AS ModifiedBy
                FROM   tbl_InventoryStockAdjustment isa
                       LEFT JOIN tbl_InventoryStockAdjustmentItems isai
                              ON isa.ID = isai.AdjustmentID
                       LEFT JOIN tblProduct_V2 pt
                              ON pt.ID = isai.ProductID
                       LEFT JOIN tblLedger ldg
                              ON ldg.ID = isa.LedgerID
                       LEFT JOIN tbl_InventoryReason rsn
                              ON rsn.ID = isa.ReasonID
                       LEFT JOIN tbl_EmployeeMaster EM
                              ON EM.ID = isa.ModifiedBy
                WHERE  isa.IsDeleted = 0
                       AND isa.DomainID = @DomainID
                       AND ( @ProductID = 0
                              OR isai.ProductID = @ProductID )
                       AND ( @LedgerID = 0
                              OR isa.LedgerID = @LedgerID )
                ORDER  BY isa.ModifiedOn DESC
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
