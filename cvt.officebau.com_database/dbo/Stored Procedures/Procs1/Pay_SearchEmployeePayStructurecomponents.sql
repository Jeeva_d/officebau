﻿/****************************************************************************                       
CREATED BY   :                       
CREATED DATE  :                       
MODIFIED BY   :                       
MODIFIED DATE  :                       
 <summary>              
 [Pay_SearchEmployeePayStructurecomponents] 4210             
 </summary>                                                   
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Pay_SearchEmployeePayStructurecomponents] (
  --DECLARE     
  @ID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT pepsd.ID                                  AS ID,
                 tpeps.EmployeeId                          AS EmployeeID,
                 Isnull(em.Code, '') + ' - ' + em.FullName AS EmployeeName,
                 tpeps.CompanyPayStructureID               AS CompanyPayStucId,
                 tpcps.PaystructureName                    AS CompanyPayStucName,
                 tpcps.EffectiveFrom                       AS EffectiveFrom,
                 pepsd.Amount                              AS Gross,
                 ----              
                 pc.Code                                   AS Code,
                 pc.[Description]                          AS [Description],
                 pc.Ordinal                                AS Ordinal,
                 pepsd.ComponentId                         AS ComponentId,
                 ''                                        AS value,
                 pepsd.Amount                              AS Amount,
                 c.IsEditable                              AS IsEditable,
                 ( ppc.Type )                              AS Type
          FROM   tbl_Pay_EmployeePayStructure tpeps
                 JOIN tbl_Pay_CompanyPayStructure tpcps
                   ON tpcps.ID = tpeps.CompanyPayStructureID
                 JOIN tbl_EmployeeMaster em
                   ON em.ID = tpeps.EmployeeID
                 JOIN tbl_Pay_EmployeePayStructureDetails pepsd
                   ON pepsd.PayStructureId = tpeps.ID
                      AND pepsd.isdeleted = 0
                 JOIN tbl_Pay_PayrollCompontents pc
                   ON pepsd.ComponentId = pc.ID
                 JOIN tbl_Pay_PayrollCompanyPayStructure c
                   ON pc.ID = c.ComponentId
                      AND c.CompanyPayStructureID = tpcps.ID
                      AND c.Isdeleted = 0
                 LEFT JOIN tbl_Pay_PaystubConfiguration ppc
                        ON ppc.ComponentID = pc.ID
                           AND ppc.IsDeleted = 0
          WHERE  tpeps.ID = @ID
          ORDER  BY --ppc.Type DESC,
                    pc.Ordinal ASC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
