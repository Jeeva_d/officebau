﻿/**************************************************************************** 
CREATED BY			:	Jeeva
CREATED DATE		:	31-12-2015
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
		Check Emailid / Username exsists
		select * from Auth_WebLogin
		select * from Auth_Users
		[Auth_ValidateUsernameAndEmailId] 0,'rajbharath.in@gmail.com'
 </summary>                         
 *****************************************************************************/  
 CREATE PROCEDURE [dbo].[Auth_ValidateUsernameAndEmailId]
 (
		@IsUserName		INT			,
		@Input			VARCHAR(50)
 )
 AS
 BEGIN
	SET NOCOUNT ON
	BEGIN TRY		
	DECLARE @Output INT = 0
	
	IF	(@IsUserName = 1)
		BEGIN
			IF EXISTS(SELECT 1 FROM Auth_Users WHERE Username = @Input)
				SET @Output = 1
			ELSE
				SET @Output = 0
		END
	ELSE
		BEGIN
			IF EXISTS(SELECT 1 FROM Auth_WebLogin WHERE Email = @Input)
				SET @Output = 1
			ELSE
				SET @Output = 0
		END
	SELECT	@Output AS Result
	END TRY
	BEGIN CATCH        
		DECLARE @ErrorMsg VARCHAR(100),		@ErrSeverity TINYINT        
		SELECT	@ErrorMsg=ERROR_MESSAGE(),	@ErrSeverity=ERROR_SEVERITY()        
		RAISERROR(@ErrorMsg,@ErrSeverity,1)  		
	END CATCH 
 END
