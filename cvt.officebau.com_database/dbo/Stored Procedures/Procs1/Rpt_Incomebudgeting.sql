﻿/****************************************************************************                                    
CREATED BY    :                                  
CREATED DATE  :                                  
MODIFIED BY   :                               
MODIFIED DATE :                             
[Rpt_Incomebudgeting] 1009,3,'booked'                                
*****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_Incomebudgeting] (@FinancialYearID INT,
                                              @DomainID        INT,
                                              @Type            VARCHAR(100))
AS
  BEGIN
      BEGIN try
          DECLARE @FYIDvalue INT = (SELECT financialyear
                     FROM   tbl_financialyear
                     WHERE  id = @FinancialYearID),
                  @Month     INT = (SELECT value
                     FROM   tblapplicationconfiguration
                     WHERE  code = 'STARTMTH'
                            AND domainid = @DomainID)
          DECLARE @StartDate DATETIME = Dateadd(month, @Month - 1, Dateadd(year, @FYIDvalue - 1900, 0)),
                  @EndDate   DATETIME = Dateadd(month, CASE
                                     WHEN ( @Month = 1 ) THEN
                                       12 - 1
                                     ELSE
                                       @Month - 1 - 1
                                   END, Dateadd(year, @FYIDvalue + 1 - 1900, 0))

          SELECT l.NAME                                                                                                                   AS [Name],
                 ( ( it.rate * Qty ) + ( CGSTAmount + SGSTAmount ) ) / ( 100 / ( ( epm.Amount / (SELECT Sum(Rate*Qty)
                                                                                                        + Sum(CGSTAmount+SGSTAmount)
                                                                                                 FROM   tblinvoiceitem
                                                                                                 WHERE  IsDeleted = 0
                                                                                                        AND InvoiceId = e.ID) ) * 100 ) ) TotalAmount,
                 Datename(month, e.date)                                                                                                  MonthNames,
                 b.amount,
                 'Payment'                                                                                                                Typed
          INTO   #Income
          FROM   tblbudget b
                 LEFT JOIN tblledger l
                        ON b.budgetid = l.id
                 LEFT JOIN tblProduct_v2 v
                        ON l.id = v.SalesLedgerId
                 JOIN tblinvoiceitem it
                   ON v.ID = it.ProductID
                 LEFT JOIN tblInvoice e
                        ON it.InvoiceId = e.ID
                           AND b.BudgetType = 'InFlow'
                           AND e.isdeleted = 0
                 LEFT JOIN tblInvoicePaymentMapping epm
                        ON epm.InvoiceID = e.id
                           AND epm.isdeleted = 0
                 JOIN tblInvoicePayment ep
                   ON epm.InvoicePaymentID = ep.id
                      AND b.fyid = @FinancialYearID
                      AND e.date > @StartDate
                      AND e.date < @EndDate
          WHERE  b.BudgetType = 'Inflow'
                 AND b.isdeleted = 0
                 AND b.domainid = @DomainID

          INSERT INTO #Income
          SELECT l.NAME AS [Name],
                 Sum(Isnull(it.CGSTAmount, 0))
                 + Sum(Isnull(it.SGSTAmount, 0))
                 + Sum((it.QTY*it.Rate)),
                 Datename(month, e.Date),
                 b.amount,
                 'Booked'
          FROM   tblbudget b
                 LEFT JOIN tblledger l
                        ON b.budgetid = l.id
                 LEFT JOIN tblProduct_v2 v
                        ON l.id = v.SalesLedgerId
                 LEFT JOIN tblinvoiceitem it
                        ON v.ID = it.ProductID
                 LEFT JOIN tblInvoice e
                        ON it.InvoiceId = e.ID
                           AND b.BudgetType = 'InFlow'
                           AND e.isdeleted = 0
                           AND b.fyid = @FinancialYearID
                           AND e.Date > @StartDate
                           AND e.Date < @EndDate
          WHERE  b.BudgetType = 'Inflow'
                 AND b.isdeleted = 0
                 AND b.domainid = @DomainID
          GROUP  BY v.NAME,
                    l.NAME,
                    b.Type,
                    Datename(month, e.Date),
                    b.amount

          --INSERT INTO #Income
          --SELECT v.NAME                  AS NAME,
          --       Sum(e.amount)           TotalAmount,
          --       Datename(month, e.date) MonthNames,
          --       b.amount,
          --       @Type                   AS Typed
          --FROM   tblbudget b
          --       LEFT JOIN tblReceipts e
          --              ON b.budgetid = e.LedgerID
          --                 AND b.BudgetType = 'INFlow'
          --                 AND e.isdeleted = 0
          --                 AND e.TransactionType = (SELECT Id
          --                                          FROM   tblMasterTypes
          --                                          WHERE  NAME = 'Receive Payment')
          --                 AND e.date > @StartDate
          --                 AND e.date < @EndDate
          --       LEFT JOIN tblledger v
          --              ON b.budgetid = v.id
          --                 AND b.Type = 'Ledger'
          --WHERE  b.BudgetType = 'INFlow'
          --       AND b.isdeleted = 0
          --       AND b.domainid = @DomainID
          --GROUP  BY v.NAME,
          --          Datename(month, e.date),
          --          b.amount

		  INSERT INTO #Income
          SELECT v.NAME                  AS NAME,
                 Sum(e.totalamount)           TotalAmount,
                 Datename(month, e.paymentdate) MonthNames,
                 b.amount,
                 @Type                   AS Typed
          FROM   tblbudget b
                 LEFT JOIN tbl_Receipts e
                        ON b.budgetid = e.LedgerID
                           AND b.BudgetType = 'INFlow'
                           AND e.isdeleted = 0                         
                           AND e.paymentdate > @StartDate
                           AND e.paymentdate < @EndDate
                 LEFT JOIN tblledger v
                        ON b.budgetid = v.id
                           AND b.Type = 'Ledger'
          WHERE  b.BudgetType = 'INFlow'
                 AND b.isdeleted = 0
                 AND b.domainid = @DomainID
          GROUP  BY v.NAME,
                    Datename(month, e.paymentdate),
                    b.amount

          IF( @Type = 'Payment' )
            DELETE #Income
            WHERE  Typed <> 'Payment'
          ELSE
            DELETE #Income
            WHERE  Typed = 'Payment'

          SELECT Datename(month, '2012-' + Cast(number AS VARCHAR(2)) + '-1') monthname,
                 amount,
                 l.NAME                                                       AS [Name]
          INTO   #month
          FROM   master..spt_values s
                 JOIN tblbudget b
                   ON b.id = b.id
                      AND b.isdeleted = 0
                      AND b.fyid = @FinancialYearID
                      AND b.domainid = @DomainID
                      AND b.BudgetType = 'Inflow'
                 LEFT JOIN tblLedger l
                        ON b.budgetid = l.id
                           AND b.type = 'Ledger'
          WHERE  s.type = 'P'
                 AND number BETWEEN 1 AND 12
          ORDER  BY number

          CREATE TABLE #months
            (
               rowid     INT IDENTITY (1, 1) NOT NULL,
               monthname VARCHAR(50) NOT NULL
            )

          SELECT m.monthname,
                 m.NAME                   NAME,
                 Isnull(e.totalamount, 0) Amount,
                 m.amount                 pro
          INTO   #pivottable
          FROM   #month m
                 LEFT JOIN #Income e
                        ON m.monthname = e.monthnames
                           AND m.NAME = e.NAME

          INSERT INTO #months
                      (monthname)
          SELECT Datename(month, '2012-' + Cast(number AS VARCHAR(2)) + '-1') monthname
          FROM   master..spt_values s
          WHERE  s.type = 'P'
                 AND number BETWEEN 4 AND 12
          UNION ALL
          SELECT Datename(month, '2012-' + Cast(number AS VARCHAR(2)) + '-1') monthname
          FROM   master..spt_values s
          WHERE  s.type = 'P'
                 AND number BETWEEN 1 AND 3

          DECLARE @cols  AS NVARCHAR(max),
                  @cols2 AS NVARCHAR(max),
                  @query AS NVARCHAR(max);

          SET @cols = Stuff((SELECT ',' + monthname
                             FROM   #months
                             FOR xml path (''), type) .value('.', 'NVARCHAR(MAX)'), 1, 1, '')
          SET @cols2 = Stuff((SELECT '+' + monthname
                              FROM   #months
                              FOR xml path (''), type) .value('.', 'NVARCHAR(MAX)' ), 1, 1, '')
          SET @query = 'SELECT Name AS Particulars, pro AS Planned ,('
                       + @cols2 + ') As Actual, CASE WHEN((' + @cols2
                       + ')=0) then 0 Else ((' + @cols2
                       + ')*100)/pro END AS [Percent]   ,' + @cols
                       + ' from              (                 select Name                     , amount                     , monthname ,pro                          
 from #PivotTable           ) x             pivot              (                sum(amount)                                     
               for monthname in (' + @cols
                       + ')             ) p ORDER BY PRo '

          PRINT @query

          EXECUTE (@query)

          DROP TABLE #month, #Income, #pivottable, #months
      END try
      BEGIN catch
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END catch
  END
