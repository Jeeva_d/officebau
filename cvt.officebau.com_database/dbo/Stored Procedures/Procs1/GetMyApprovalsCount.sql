﻿/****************************************************************************   
CREATED BY		: DHANALAKSHMI. S
CREATED DATE	: 18-11-2017 
MODIFIED BY		:   
MODIFIED DATE	:   
 <summary>
	[GetMyApprovalsCount] 5, 4
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetMyApprovalsCount] (@EmployeeID INT,
                                             @DomainID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @PendingStatusID INT= (SELECT ID
                     FROM   tbl_Status
                     WHERE  Code = 'Pending'
                            AND [Type] = 'Leave'
                            AND IsDeleted = 0),
                  @ClaimsStatusID  INT = (SELECT ID
                     FROM   tbl_Status
                     WHERE  Code IN ( 'Submitted' )
                            AND [Type] = 'Claims')

          SELECT RBS.ID
          INTO   #TempRBS
          FROM   tbl_RBSMenu RBS
                 LEFT JOIN tbl_RBSUserMenuMapping RUM
                        ON RUM.MenuID = RBS.ID
          WHERE  RUM.EmployeeID = @EmployeeID
                 AND RUM.DomainID = @DomainID
				 and RBS.DomainID=@DomainID
                 AND RBS.IsSubmenu = 0
                 AND MRead = 1
                 AND RUM.IsDeleted = 0  And RBS.isdeleted=0
				
          SELECT (SELECT Count(1)
                  FROM   tbl_EmployeeLeave
                  WHERE  ApproverID = @EmployeeID
                         AND IsDeleted = 0
                         AND DomainID = @DomainID
                         AND ApproverStatusID = @PendingStatusID)                      AS LeavesCount,
                 (SELECT Count(1)
                  FROM   tbl_ClaimsRequest CLR
                         LEFT JOIN tbl_ClaimsApprove CLA
                                ON CLA.ClaimItemID = CLR.ID
                  WHERE  ApproverID = @EmployeeID
                         AND CLR.IsDeleted = 0
                         AND CLR.DomainID = @DomainID
                         AND CLR.StatusID IN((SELECT ID
                                              FROM   tbl_Status
                                              WHERE  Code IN( 'Submitted', 'Resubmitted' )
                                                     AND [Type] = 'Claims'))
                          OR CLA.StatusID = @ClaimsStatusID)                           AS ClaimsCount,
                 (SELECT Count(1)
                  FROM   tbl_EmployeeMissedPunches
                  WHERE  IsDeleted = 0
                         AND DomainID = @DomainID
                         AND StatusID = @PendingStatusID
						 AND HRApproverID = @EmployeeID)                              AS PunchCount,
                 (SELECT Count(1)
                  FROM   tbl_EmployeeLeave
                  WHERE  HRApproverID = @EmployeeID
                         AND IsDeleted = 0
                         AND DomainID = @DomainID
                         AND HRStatusID = @PendingStatusID)                            AS LeavesHRApprovalCount,
                 (SELECT Count(1)
                  FROM   tbl_ClaimsRequest CLR
                         LEFT JOIN tbl_ClaimsApprove CLA
                                ON CLA.ClaimItemID = CLR.ID
                         LEFT JOIN tbl_ClaimsFinancialApprove CFA
                                ON CLR.id = CFA.ClaimItemID
                  WHERE  FinancialApproverID = @EmployeeID
                         AND CLR.IsDeleted = 0
                         AND CLR.DomainID = @DomainID
                         AND CLA.FinApproverStatusID = (SELECT ID
                                                        FROM   tbl_Status
                                                        WHERE  Code IN( 'Submitted' )
                                                               AND [Type] = 'Claims')) AS ClaimsFinancialCount
          INTO   #TempCount
		
        CREATE TABLE #FinalTable
         (
               [Count]     VARCHAR(50),
               Description VARCHAR(500)
            )

          INSERT INTO #FinalTable
          SELECT CASE
                   WHEN (SELECT 1
                         FROM   #TempRBS
                         WHERE  ID = (SELECT ID
                                      FROM   tbl_RBSMenu
                                      WHERE  MenuCode = 'APPLEAVE' and DomainID=@DomainID and isdeleted=0 )) <> 0 THEN 'Leave Approval' + ' - '
                                                                               + Cast(LeavesCount AS VARCHAR(50))
                   ELSE NULL
                 END AS Counts,
                 '/LeaveManagement/LeaveApprove?menuCode=APPLEAVE'
          FROM   #TempCount
          UNION ALL
          SELECT CASE
                   WHEN (SELECT 1
                         FROM   #TempRBS
                         WHERE  ID = (SELECT ID
                                      FROM   tbl_RBSMenu
                                      WHERE  MenuCode = 'LEAVEHRAPP' and DomainID=@DomainID and isdeleted=0 )) <> 0 THEN 'Leave HR Approval' + ' - '
                                                                                 + Cast( LeavesHRApprovalCount AS VARCHAR(50))
                   ELSE NULL
                 END AS Counts,
                 '/LeaveManagement/HRApproval?menuCode=LEAVEHRAPP'
          FROM   #TempCount
          UNION ALL
          SELECT CASE
                   WHEN (SELECT 1
                         FROM   #TempRBS
                         WHERE  ID = (SELECT ID
                                      FROM   tbl_RBSMenu
                                      WHERE  MenuCode = 'CLAIMAPP' and DomainID=@DomainID and isdeleted=0 )) <> 0 THEN 'Claims Approval' + ' - '
                                                                               + Cast(ClaimsCount AS VARCHAR(50))
                   ELSE NULL
                 END AS Counts,
                 '/EmployeeClaims/SearchClaimsApproval?menuCode=CLAIMAPP'
          FROM   #TempCount
          UNION ALL
          SELECT CASE
                   WHEN (SELECT 1
                         FROM   #TempRBS
                         WHERE  ID = (SELECT ID
                                      FROM   tbl_RBSMenu
                                      WHERE  MenuCode = 'CLAIMFINAPP' and DomainID=@DomainID and isdeleted=0 )) <> 0 THEN 'Claims Finance Approval' + ' - '
                                                                                  + Cast(ClaimsFinancialCount AS VARCHAR(50))
                   ELSE NULL
                 END AS Counts,
                 '/EmployeeClaims/SearchClaimsFinancialApproval?menuCode=CLAIMFINAPP'
          FROM   #TempCount
          UNION ALL
          SELECT CASE
                   WHEN (SELECT 1
                         FROM   #TempRBS
                         WHERE  ID = (SELECT ID
                                      FROM   tbl_RBSMenu
                                      WHERE  MenuCode = 'MISSEDPUNCHAPP' and DomainID=@DomainID and isdeleted=0 )) <> 0 THEN 'Missed Punches Approval' + ' - '
                                                                                     + Cast(PunchCount AS VARCHAR(50))
                   ELSE NULL
                 END AS Counts,
                 '/LeaveManagement/SearchApprovePunches?menuCode=MISSEDPUNCHAPP'
          FROM   #TempCount

          SELECT *
          FROM   #FinalTable
          WHERE  Count IS NOT NULL
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
