﻿/****************************************************************************         
CREATED BY   : Ajith N        
CREATED DATE  : 31 Aug 2018        
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>                
   [Rpt_EmployeeSkillMatrix]  2 , '', 'false', 1,1        
 </summary>                                  
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_EmployeeSkillMatrix] (@SkillsID     INT,
                                                 @BusinessUnit VARCHAR(100),
                                                 @IsActive     BIT,
                                                 @UserID       INT,
                                                 @DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @BusinessUnitIDs VARCHAR(50)

          IF( Isnull(@BusinessUnit, 0) = 0 )
            SET @BusinessUnitIDs = (SELECT BusinessUnitID
                                    FROM   tbl_EmployeeMaster
                                    WHERE  ID = @UserID
                                           AND DomainID = @DomainID)
          ELSE
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','

          DECLARE @BusinessUnitTable TABLE
            (
               BusinessUnit VARCHAR(100)
            )

          INSERT INTO @BusinessUnitTable
          SELECT @BusinessUnitIDs

          SELECT DISTINCT SK.ID                          AS SkillsID,
                          SK.NAME                        AS [Skills],
                          (SELECT Count(1)
                           FROM   tbl_EmployeeSkills ES
						   LEFT JOIN tbl_EmployeeMaster EMP
                                  ON EMP.ID = ES.EmployeeID
                           WHERE  ES.IsDeleted = 0
                                  AND ES.DomainID = @DomainID
                                  AND ES.LevelID = (SELECT ID
                                                 FROM   tbl_CodeMaster
                                                 WHERE IsDeleted = 0
                                  AND Code = 'Beginner')
                                  AND SkillsID = Sk.ID 
								  AND EMP.IsActive = @IsActive)  AS [Beginner],
                          (SELECT ID
                           FROM   tbl_CodeMaster
                           WHERE IsDeleted = 0
                                  AND Code = 'Beginner')     AS BeginnerLevelID,
                          (SELECT Count(1)
                           FROM   tbl_EmployeeSkills ES
						   LEFT JOIN tbl_EmployeeMaster EMP
                                  ON EMP.ID = ES.EmployeeID
                           WHERE  ES.IsDeleted = 0
                                  AND ES.DomainID = @DomainID
                                  AND ES.LevelID = (SELECT ID
                                                 FROM   tbl_CodeMaster
                                                 WHERE  IsDeleted = 0
                                  AND Code = 'Intermediate')
                                  AND SkillsID = Sk.ID
								  AND EMP.IsActive = @IsActive)  AS [Intermediate],
                          (SELECT ID
                           FROM   tbl_CodeMaster
                           WHERE  IsDeleted = 0
                                  AND Code = 'Intermediate') AS IntermediateLevelID,
                          (SELECT Count(1)
                           FROM   tbl_EmployeeSkills ES
						   LEFT JOIN tbl_EmployeeMaster EMP
                                  ON EMP.ID = ES.EmployeeID
                           WHERE  ES.IsDeleted = 0
                                  AND ES.DomainID = @DomainID
                                  AND ES.LevelID = (SELECT ID
                                                 FROM   tbl_CodeMaster
                                                 WHERE IsDeleted = 0
                                  AND Code = 'Expert')
                                  AND SkillsID = Sk.ID
								  AND EMP.IsActive = @IsActive)  AS [Experience],
                          (SELECT ID
                           FROM   tbl_CodeMaster
                           WHERE IsDeleted = 0
                                  AND Code = 'Expert')       AS ExpertLevelID
          FROM   tbl_Skills SK
                 LEFT JOIN tbl_EmployeeSkills ES
                        ON ES.SkillsID = Sk.ID
                           AND ES.IsDeleted = 0
                           AND Es.DomainID = @DomainID
                 LEFT JOIN tbl_EmployeeMaster EM
                        ON EM.ID = ES.EmployeeID
                 LEFT JOIN tbl_CodeMaster CM
                        ON CM.ID = ES.LevelID
                JOIN @BusinessUnitTable TBU
                   ON TBU.BusinessUnit LIKE '%,'
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))
                                            + ',%'
          WHERE  SK.IsDeleted = 0
                 AND ( @BusinessUnit = 0
                        OR EM.BaseLocationID = @BusinessUnit )
                 AND EM.IsActive = @IsActive 
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND SK.domainID = @DomainID
                 AND EM.domainID = @DomainID
                 AND ( Isnull(@SkillsID, '') = ''
                        OR SK.ID = @SkillsID )
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
