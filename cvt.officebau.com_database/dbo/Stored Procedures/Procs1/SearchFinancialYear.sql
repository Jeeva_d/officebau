﻿/****************************************************************************   
CREATED BY   : Dhanalakshmi  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [SearchFinancialYear]1 
 
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchFinancialYear] (@DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT ID            AS ID,
                 FinancialYear AS FinancialYear,
                 fyDescription   AS Description
          FROM   tbl_FinancialYear
          WHERE  DomainID = @DomainID
                 AND IsDeleted = 0
          ORDER  BY ModifiedON DESC

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
