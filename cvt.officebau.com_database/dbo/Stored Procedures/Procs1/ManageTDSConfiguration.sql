﻿/****************************************************************************     
CREATED BY   :     
CREATED DATE  :     
MODIFIED BY   : Naneeshwar.M    
MODIFIED DATE  :     
 <summary>            
 ManageTDSCofiguration    
 SELECT * FROM TDSConfiguration    
 </summary>                             
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[ManageTDSConfiguration] (@Code              VARCHAR(100),    
                                                @Value             VARCHAR(100),    
                                                @RegionID          INT,    
                                                @SessionEmployeeID INT,    
                                                @DomainID          INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      DECLARE @Output VARCHAR(8000)    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          IF EXISTS (SELECT 1    
                     FROM   TDSConfiguration    
                     WHERE  Code = @Code    
                            AND DomainID = @DomainID    
                            AND IsDeleted = 0    
                            AND RegionID = @RegionID)    
            BEGIN    
                UPDATE TDSConfiguration    
                SET    Value = @Value,    
                       ModifiedBy = @SessionEmployeeID,    
                       ModifiedOn = Getdate()    
                WHERE  Code = @Code    
                       AND DomainID = @DomainID    
                       AND RegionID = @RegionID    
    
                SET @Output = 'Updated Successfully.'    
    
                GOTO RESULT    
            END    
          ELSE    
            BEGIN    
                INSERT INTO TDSConfiguration    
                            (Code,    
                             Description,    
                             Value,    
                             RegionID,    
                             CreatedBy,    
                             CreatedOn,    
                             ModifiedBy,    
                             ModifiedOn,    
                             DomainID)    
                VALUES      (@Code,    
                             @Code,    
                             @Value,    
                             @RegionID,    
                             @SessionEmployeeID,    
                             Getdate(),    
                             @SessionEmployeeID,    
                             Getdate(),    
                             @DomainID)    
    
                SET @Output = 'Updated Successfully.'    
            END    
    
          RESULT:    
    
          SELECT @Output    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
