﻿/****************************************************************************   
CREATED BY   : Naneeshwar.M  
CREATED DATE  : 22-JAN-2018  
MODIFIED BY   : Ajith N  
MODIFIED DATE  : 02 Feb 2018  
 <summary>          
     [GetApplicationConfigValue] 'LOPUPDAT','11',106  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetApplicationConfigValue] (@Code         VARCHAR(50),  
                                                   @BusinessUnit VARCHAR(25),  
                                                   @UserID       INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @DataSource TABLE  
            (  
               ID      INT IDENTITY(1, 1),  
               [Value] NVARCHAR(128)  
            )  
  
          IF( ISNULL(@BusinessUnit, '') <> '' )  
            BEGIN  
                INSERT INTO @DataSource  
                            ([Value])  
                SELECT Item  
                FROM   dbo.Splitstring (@BusinessUnit, ',')  
                WHERE  Isnull(Item, '') <> ''  
            END  
          ELSE  
            BEGIN  
                INSERT INTO @DataSource  
                            ([Value])  
                SELECT BaseLocationID  
                FROM   tbl_EmployeeMaster  
                WHERE  IsDeleted = 0  
                       AND ID = @UserID  
            END  
  
          SELECT Isnull(AC.ConfigValue, '0') AS ConfigValue  
          FROM   tbl_ConfigurationMaster cm  
                 LEFT JOIN tbl_ApplicationConfiguration ac  
                        ON cm.ID = ac.ConfigurationID  
                 JOIN @DataSource BU  
                   ON Bu.value = ac.BusinessUnitID  
          WHERE  cm.Code = @code  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
