﻿/****************************************************************************         
CREATED BY		: Ajith N   
CREATED DATE	: 26 Sep 2017
MODIFIED BY		:  
MODIFIED DATE   :   
 <summary>                
    [GetEmployeeDropDown] 'BusinessUnit', 'false',2,1 
	[GetEmployeeDropDown] 'EmployeeMaster', 'false',2,1 
 </summary>                                 
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetEmployeeDropDown] (@Type              VARCHAR(50),
                                             @IsIncludeEmployee BIT,
                                             @EmployeeID        INT,
                                             @DomainID          INT)
AS
  BEGIN
      BEGIN try
          IF( ( isnull(@Type, '') = 'BusinessUnit' )
              AND ( isnull(@IsIncludeEmployee, 'true') = 'false' ) )
            BEGIN
                SELECT EM.ID         AS ID,
                       ISNULL(comp.EmpCodePattern, '') + Code + ' - '
                       + EM.FullName AS NAME
                FROM   tbl_EmployeeMaster EM
                       LEFT JOIN tbl_Company COMP
                              ON comp.ID = EM.DomainID
                WHERE  EM.IsDeleted = 0
                       AND isnull(EM.IsActive, 0) = 0
                       AND BaseLocationID IN (SELECT Item
                                              FROM   dbo.Splitstring ((SELECT BaseLocationID
                                                                       FROM   tbl_EmployeeMaster
                                                                       WHERE  id = @EmployeeID
                                                                              AND DomainID = @DomainID), ',')
                                              WHERE  Isnull(Item, '') <> '')
                       AND DomainID = @DomainID
                       AND EM.ID <> @EmployeeID
                ORDER  BY EM.FullName ASC
            END
          ELSE IF ( ISNULL(@Type, '') = 'EmployeeMaster' )
            BEGIN
                SELECT EM.ID         AS ID,
                       ISNULL(comp.EmpCodePattern, '') + Code + ' - '
                       + EM.FullName AS NAME
                FROM   tbl_EmployeeMaster EM
                       LEFT JOIN tbl_Company COMP
                              ON comp.ID = EM.DomainID
                WHERE  EM.IsDeleted = 0
                       AND isnull(EM.IsActive, 0) = 0
                       AND BaseLocationID IN (SELECT Item
                                              FROM   dbo.Splitstring ((SELECT BaseLocationID
                                                                       FROM   tbl_EmployeeMaster
                                                                       WHERE  id = @EmployeeID
                                                                              AND DomainID = @DomainID), ',')
                                              WHERE  Isnull(Item, '') <> '')
                       AND DomainID = @DomainID
                       AND EM.ID <> @EmployeeID
                ORDER  BY EM.FullName ASC
            END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
