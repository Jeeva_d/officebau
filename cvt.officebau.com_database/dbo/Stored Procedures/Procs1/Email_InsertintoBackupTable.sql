﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	20-SEP-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
	 
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Email_InsertintoBackupTable] (@FromID  VARCHAR(200),
                                                     @ToID    VARCHAR(max),
                                                     @cc      VARCHAR(max),
                                                     @Bcc     VARCHAR(max),
                                                     @Subject VARCHAR(max),
                                                     @content VARCHAR(max))
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(8000)

      BEGIN TRY
          BEGIN TRANSACTION

          SET @Output = 'Operation Failed!'

          INSERT INTO tbl_EmailProcessor_Backup
                      (ID,
                       fromID,
                       ToID,
                       CC,
                       BCC,
                       Subject,
                       Content)
          VALUES      (0,
                       @FromID,
                       @ToID,
                       @cc,
                       @Bcc,
                       @Subject,
                       @content)

          FINISH:

          SELECT @Output

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
