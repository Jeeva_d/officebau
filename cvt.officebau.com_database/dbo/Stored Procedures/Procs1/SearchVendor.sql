﻿/****************************************************************************         
CREATED BY   : Jeeva        
CREATED DATE  :         
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>      
 [SearchVendor] '',1,1        
 select * from tblvendor      
      
 </summary>                                 
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[SearchVendor] (@Name     VARCHAR(100),    
                                      @DomainID INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN TRANSACTION    
    
          SELECT vd.ID           AS ID,    
                 vd.NAME         AS NAME,    
                 ContactNo       AS ContactNo,    
                 ContactPersonNo AS ContactPersonNo,    
                 ContactPerson   AS ContactPerson,    
                 cty.NAME        AS CityName,    
                 cur.NAME        AS CurrencyName,    
                 vd.CurrencyID   AS CurrencyID,    
                 PANNo           AS PanNO,  
                 vd.GSTNo        AS GSTNo,
                 vd.PaymentTerms AS PaymentTerms   
          FROM   tblVendor vd    
                 LEFT JOIN tbl_City cty    
                        ON cty.ID = vd.CityID    
                 LEFT JOIN tblCurrency cur    
                        ON cur.ID = vd.CurrencyID    
          WHERE  vd.NAME LIKE '%' + Isnull(@Name, '') + '%'    
                 AND vd.DomainID = @DomainID    
                 AND vd.IsDeleted = 0    
          ORDER  BY vd.ModifiedON DESC    
    
          COMMIT TRANSACTION    
      END TRY    
      BEGIN CATCH    
          ROLLBACK TRANSACTION    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR(@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
