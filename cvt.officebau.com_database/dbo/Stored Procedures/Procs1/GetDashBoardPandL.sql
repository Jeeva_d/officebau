﻿/****************************************************************************   
CREATED BY     :   
CREATED DATE   :   
MODIFIED BY    : JENNIFER.S  
MODIFIED DATE  : 12-SEP-2017
<summary>          
 [GetDashBoardPandL] '',1 
</summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetDashBoardPandL] (@Date     DATETIME,
                                           @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF( Isnull(@Date, '') = '' )
            SET @Date=Getdate();

          WITH Expense
               AS (SELECT ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50)) AS ExpenseMonthYear,
                          Sum(Amount)                                                                          AS ExpenseAmount,
                          Sum(Isnull(PaidAmount, 0))                                                           AS ExpenseReceivedAmount
                   FROM   VW_NewExpense
                   WHERE  DomainID = @DomainID
                          --AND ReportType IN( 0, 2 )
                   GROUP  BY ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50))),
               Income
               AS (SELECT ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50)) AS MonthYear,
                          Sum(Amount)                                                                          AS Amount,
                          Sum(Isnull(Received, 0))                                                             AS ReceivedAmount
                   FROM   Vw_Income
                   WHERE  DomainID = @DomainID
                   GROUP  BY ( LEFT(Datename(MONTH, Date), 3) ) + ' ' + Cast(Datepart(YEAR, Date) AS VARCHAR(50))),
               MonthYear
               AS (--SELECT MonthYear
                  --FROM   dbo.Fngetmonthyear (Datepart(Month, @Date), Datepart(YEAR, @Date))
                  SELECT 1                                                 AS mnum,
                         LEFT(Datename(MM, Getdate()), 3) + ' '
                         + CONVERT(VARCHAR(10), Datepart(YEAR, Getdate())) AS MonthYear
                   UNION ALL
                   SELECT 1 + mnum,
                          LEFT(Datename(MM, Dateadd(month, -(mnum), Getdate())), 3)
                          + ' '
                          + CONVERT(VARCHAR(10), Datepart(YEAR, Dateadd(month, -(mnum), Getdate())))
                   FROM   MonthYear
                   WHERE  mnum < 6)
          SELECT tem.MonthYear                         AS MonthYear,
                 Isnull(expe.ExpenseAmount, 0)         AS ExpenseAmount,
                 Isnull(expe.ExpenseReceivedAmount, 0) AS ExpenseReceivedAmount,
                 Isnull(inc.Amount, 0)                 AS Amount,
                 Isnull(inc.ReceivedAmount, 0)         AS ReceivedAmount
          FROM   MonthYear tem
                 LEFT JOIN Expense expe
                        ON tem.MonthYear = expe.ExpenseMonthYear
                 LEFT JOIN Income inc
                        ON tem.MonthYear = inc.MonthYear
          ORDER  BY mnum DESC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END 
