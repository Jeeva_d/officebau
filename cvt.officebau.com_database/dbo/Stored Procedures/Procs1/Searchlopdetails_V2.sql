﻿/****************************************************************************       
CREATED BY  : JENNIFER.S    
CREATED DATE : 03-Jul-2017    
MODIFIED BY  : Naneeshwar.M    
MODIFIED DATE : 16 AUG 2017    
<summary>    
 [Searchlopdetails_V2] 12 ,2018,'4',0  ,1    
</summary>                               
*****************************************************************************/
CREATE PROCEDURE [dbo].[Searchlopdetails_V2] (@MONTHID      TINYINT,
                                             @Year         SMALLINT,
                                             @BusinessUnit VARCHAR(1000),
                                             @HasLopDays   BIT,
                                             @DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @CustomDate DATETIME = Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @year - 2000, Dateadd(MONTH, @monthID - 1, '20000101')))
                                      + 1, 0))
          DECLARE @Workdays INT = Day(@CustomDate)

          SELECT Row_number()
                   OVER (
                     PARTITION BY emp.ID
                     ORDER BY EffectiveFrom DESC)          AS ORDERNUMBER,
                 Isnull(LOP.ID, 0)                         AS ID,
                 EMP.ID                                    AS EmployeeID,
                 EMP.FullName                              AS EmployeeName,
                 Isnull(EMP.EmpCodePattern, '') + EMP.Code AS EmployeeNo,
                 Isnull(DEP.NAME, '')                      AS DepartmentName,
                 @MONTHID                                  AS MonthID,
                 @Year                                     AS YearId,
                 Isnull(LOP.LopDays, 0)                    AS LOPDays,
                 ( CASE
                     WHEN ( Datepart(MM, DOJ) = @MonthId
                            AND Datepart(YYYY, DOJ) = @year ) THEN
                       ( @Workdays - ( Datepart(D, DOJ) - 1 ) )
                     ELSE
                       @Workdays
                   END ) - ( CASE
                               WHEN ( Datepart(MM, InactiveFrom) = @MonthId
                                      AND Datepart(YYYY, InactiveFrom) = @year ) THEN
                                 ( Day(@CustomDate) - Day(InactiveFrom) )
                               ELSE
                                 0
                             END )                         AS NoOfDays,
                 DOJ                                       AS DOJ,
                 CASE
                   WHEN LOP.IsManual = 0 THEN
                     'Biometric'
                   ELSE
                     'Manual'
                 END                                       AS [Type],
                 CASE
                   WHEN Isnull(PAY.ID, 0) <> 0 THEN
                     'Processed'
                   ELSE
                     'Open'
                 END                                       AS [Status],
                 BUU.NAME                                  AS BusinessUnit
          INTO   #tmplop
          FROM   tbl_EmployeeMaster EMP
                 JOIN tbl_BusinessUnit BUU
                   ON EMP.BaseLocationID = BUU.ID
                 LEFT JOIN tbl_LopDetails LOP
                        ON EMP.ID = LOP.EmployeeId
                           AND LOP.MonthID = @MONTHID
                           AND LOP.[Year] = @Year
                 LEFT JOIN tbl_pay_EmployeePayroll PAY
                        ON PAY.EmployeeId = EMP.ID
                           AND PAY.MonthID = @MONTHID
                           AND PAY.[YearId] = @Year
                           AND PAY.IsDeleted = 0
                           AND PAY.IsProcessed = 1
                 LEFT JOIN tbl_department DEP
                        ON DEP.ID = EMP.DepartmentID
                 JOIN tbl_Pay_EmployeePayStructure ep
                   ON ep.EmployeeId = EMP.ID
          WHERE  EMP.DomainID = @DomainID
                 AND EMP.IsDeleted = 0
                 AND ( Isnull(emp.IsActive, '') = 0
                        OR ( Month(InactiveFrom) = @MonthId
                             AND Year(InactiveFrom) = @Year ) )
                 AND ( ( Isnull(@BusinessUnit, '') <> ''
                         AND EMP.BaseLocationID IN (SELECT Item
                                                    FROM   dbo.Splitstring (@BusinessUnit, ',')
                                                    WHERE  Isnull(Item, '') <> '') )
                        OR Isnull(@BusinessUnit, '') = '' )
                 AND ( ( @HasLopDays = 1
                         AND Isnull(LOP.LopDays, 0) <> 0 )
                        OR ( @HasLopDays = 0 ) )
                 AND Cast(EMP.DOJ AS DATE) <= Cast (@CustomDate AS DATE)
                 AND EMP.IsDeleted = 0
                 --AND Isnull(EMP.IsActive, '') = 0    
                 AND EMP.HasAccess = 1
                 AND EMP.EmploymentTypeID = (SELECT ID
                                             FROM   tbl_EmployementType
                                             WHERE  NAME = 'Direct'
                                                    AND DomainID = @DomainID)
                 AND ep.EffectiveFrom < Dateadd(D, -1, Dateadd(m, Datediff(m, 0, Dateadd(YEAR, @Year - 2000, Dateadd(MONTH, @MonthId - 1, '20000101')))
                                                                  + 1, 0))
          ORDER  BY EMP.Code + ' - ' + EMP.FullName

          SELECT *
          FROM   #tmplop
          WHERE  ORDERNUMBER <= 1
          ORDER  BY EmployeeName ASC
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
