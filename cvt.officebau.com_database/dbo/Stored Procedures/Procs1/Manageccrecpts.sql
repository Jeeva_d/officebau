﻿/****************************************************************************     
CREATED BY   : K.SASIREKHA    
CREATED DATE  : 20-10-2017    
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
  Manageccrecpts 15, '',3, 1, 106, 0, 1    
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Manageccrecpts] (@ID             INT,  
                                         @CCrecpts       VARCHAR(1000),  
                                         @eventID        INT,  
                                         @BusinessUnitID INT,  
                                         @SessionID      INT,  
                                         @IsDeleted      BIT,  
                                         @DomainID       INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(8000)  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          SET @Output = 'Operation Failed!'  
  
          IF( Isnull(@ID, 0) != 0 )  
            BEGIN  
                IF( @IsDeleted = 1 )  
                  BEGIN  
                      UPDATE tbl_EmailRecipients  
                      SET    IsDeleted = @IsDeleted,  
                             ModifiedBy = @SessionID,  
                             ModifiedOn = Getdate()  
                      WHERE  ID = @ID  
                             AND DomainID = @DomainID  
  
                      SET @Output = 'Deleted Successfully.'  
  
                      GOTO Finish  
                  END  
            --IF( @IsDeleted = 0 )    
            --  BEGIN    
            --      BEGIN    
            --          UPDATE tbl_EmailRecipients    
            --          SET     KeyID= @eventID,    
            --                 BusinessUnitID = @BusinessUnitID,    
            --                 CCEmailID = @CCrecpts,    
            --                 ModifiedBy = @SessionID,    
            --                 ModifiedOn = Getdate()    
            --          WHERE  ID = @ID    
            --                 AND DomainID = @DomainID    
            --          SET @Output = 'Updated Successfully.'    
            --          GOTO Finish    
            --      END    
            --  END    
            END  
  
          IF( Isnull(@ID, 0) = 0 )  
            BEGIN  
                IF EXISTS(SELECT 1  
                          FROM   tbl_EmailRecipients  
                          WHERE  @ID <> ID  
                                 AND KeyID = @eventID  
                                 AND BusinessUnitID = @BusinessUnitID  
                                 --AND CCEmailID = @CCrecpts    
                                 AND IsDeleted = 0)  
                  BEGIN  
                      SET @Output = 'Already Exists.'  
  
                      GOTO Finish  
                  END  
                ELSE  
                  BEGIN  
                      INSERT INTO tbl_EmailRecipients  
                                  (KeyID,  
                                   BusinessUnitID,  
                                   CCEmailID,  
                                   CreatedBy,  
                                   CreatedOn,  
                                   ModifiedBy,  
                                   ModifiedOn,  
                                   DomainID)  
                      VALUES      ( @eventID,  
                                    @BusinessUnitID,  
                                    replace(@CCrecpts,',', ';'), 
                                    @SessionID,  
                                    Getdate(),  
                                    @SessionID,  
                                    Getdate(),  
                                    @DomainID)  
  
                      SET @Output = 'Inserted Successfully.'  
  
                      GOTO Finish  
                  END  
            END  
          ELSE  
            BEGIN  
                IF EXISTS(SELECT 1  
                          FROM   tbl_EmailRecipients  
                          WHERE  @ID <> ID  
                                 AND KeyID = @eventID  
                                 AND BusinessUnitID = @BusinessUnitID  
                                 --AND CCEmailID = @CCrecpts    
                                 AND IsDeleted = 0)  
                  BEGIN  
                      SET @Output = 'Already Exists.'  
  
                      GOTO Finish  
                  END  
                ELSE  
                  BEGIN  
                      UPDATE tbl_EmailRecipients  
                      SET    KeyID = @eventID,  
                             BusinessUnitID = @BusinessUnitID,  
                             CCEmailID = replace(@CCrecpts,',', ';'),  
                             ModifiedBy = @SessionID,  
                             ModifiedOn = Getdate()  
                      WHERE  ID = @ID  
                             AND DomainID = @DomainID  
  
                      SET @Output = 'Updated Successfully.'  
  
                      FINISH:  
                  END  
            END  
  
          SELECT @Output  
  
          SELECT *  
          FROM   tbl_EmailRecipients  
          WHERE  @ID <> ID  
                 AND KeyID = @eventID  
                 AND BusinessUnitID = @BusinessUnitID  
                 --AND CCEmailID = @CCrecpts    
                 AND IsDeleted = 0  
  
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
