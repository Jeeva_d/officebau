﻿/****************************************************************************     
CREATED BY      : Ajith N    
CREATED DATE  :  11 Jan 2018    
MODIFIED BY   :      
MODIFIED DATE  :      
<summary>      
 [SearchAutocompleteEmployeeBasedonAccess] 0 ,rama,1,106    
</summary>                             
*****************************************************************************/    
CREATE PROCEDURE [dbo].[SearchAutocompleteEmployeeBasedonAccess] (@EmployeeID      INT,    
                                                                 @SearchParameter VARCHAR(50),    
                                                                 @DomainID        INT,    
                                                                 @UserID          INT)    
AS    
  BEGIN    
      SET NOCOUNT ON;    
    
      BEGIN TRY    
          BEGIN    
              SELECT DISTINCT TOP 50 ID                      AS ID,    
                                  ISNULL(EmpCodePattern, '')+ Code + ' - ' + FullName AS NAME    
              FROM   tbl_EmployeeMaster em    
                     JOIN (SELECT Item    
                           FROM   dbo.Splitstring ((SELECT BusinessUnitID    
                                                    FROM   tbl_EmployeeMaster    
                                                    WHERE  id = @UserID    
                                                           AND DomainID = @DomainID), ',')    
                           WHERE  Isnull(Item, '') <> '') x    
                       ON BusinessUnitID LIKE '%,' + Item + ',%'    
              WHERE  IsDeleted = 0    
                     AND ReasonID IS NULL    
                     AND HasAccess = 1    
                     AND DomainID = @DomainID    
                     AND id <> @EmployeeID    
                     AND FullName LIKE '%' + @SearchParameter + '%'    
                     AND ISNULL(Code, '') NOT LIKE 'TMP_%'    
          END    
      END TRY    
      BEGIN CATCH    
          DECLARE @ErrorMsg    VARCHAR(100),    
                  @ErrSeverity TINYINT    
          SELECT @ErrorMsg = Error_message(),    
                 @ErrSeverity = Error_severity()    
          RAISERROR (@ErrorMsg,@ErrSeverity,1)    
      END CATCH    
  END
