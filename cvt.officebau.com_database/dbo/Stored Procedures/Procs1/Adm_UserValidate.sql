﻿/****************************************************************************   
CREATED BY   : Jeeva  
CREATED DATE  :   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [Adm_UserValidate] 'Jeeva@j.com','1'
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Adm_UserValidate] (@UserName VARCHAR(100),
                                                 @Password      Varchar(100))
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION
		  dECLARE @OUTPUT VARCHAR(10)
         if EXISTS(select 1 from Adm_User where UserName=@UserName and Password=@Password)
		   SET @OUTPUT='Valid'
		 eLSE
		 SET @OUTPUT='FAILED'
		 Select @OUTPUT
          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END




