﻿/**************************************************************************** 
CREATED BY    		:	Naneeshwar.M
CREATED DATE		:	06-JULY-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>  
 [Payrolldashboard_emplist] null,2017,'BU',1,'Hyderabad'
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Payrolldashboard_emplist] (@MonthId      INT,
                                                  @Year         INT,
                                                  @Type         VARCHAR(50),
                                                  @DomainID     INT,
                                                  @BusinessUnit VARCHAR(100),
                                                  @EmployeeID   INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN
              DECLARE @DataSource TABLE
                (
                   [Value] NVARCHAR(128)
                )

              INSERT INTO @DataSource
                          ([Value])
              SELECT Item
              FROM   dbo.Splitstring ((SELECT businessunitid
                                       FROM   tbl_employeemaster
                                       WHERE  id = @EmployeeID and DomainID=@DomainID), ',')
              WHERE  Isnull(Item, '') <> ''

              SELECT DISTINCT EMP.Code                 AS EmployeeCode,
                              EMP.FullName            AS EmployeeName,
                              Emp.ID                   AS ID,
                              Round(Sum(NetSalary), 0) AS NetSalary,
                              Round(Sum(CTC), 0)       AS CTC
              FROM   tbl_EmployeePayroll EPR
                     JOIN tbl_EmployeeMaster EMP
                       ON EPR.EmployeeId = EMP.ID
                          AND EMP.IsDeleted = 0
                     LEFT JOIN tbl_BusinessUnit bu
                            ON bu.ID = EPR.BaseLocationID
                     LEFT JOIN tbl_Designation de
                            ON de.ID = emp.DesignationID
              WHERE  ( ( @MonthId IS NULL
                          OR Isnull(@MonthId, 0) = 0 )
                        OR ( EPR.monthid = @MonthId ) )
                     AND [yearid] = @Year
                     AND EPR.isdeleted = 0
                     AND EPR.DomainId = @DomainID
                     AND EPR.IsProcessed = 1
                     AND ( @Type = 'BU'
                           AND bu.NAME = @BusinessUnit
                            OR @Type = 'Designation'
                               AND Isnull(de.NAME, 'Others') = @BusinessUnit )
                     AND EPR.BaselocationID IN(SELECT *
                                               FROM   @DataSource)
              GROUP  BY Code,
                        FullName,
                        EMP.ID,
                        EPR.ID
              ORDER  BY EMP.Code,
                        EMP.FullName
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR (@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
