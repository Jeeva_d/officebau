﻿/****************************************************************************         
CREATED BY   :  Dhanalakshmi     
CREATED DATE  : 13-03-2017    
MODIFIED BY   :         
MODIFIED DATE  :         
 <summary>      
 [Rpt_IncomeCustomer] 0,1      
 [Rpt_RevenueCenter] null, null, 2    
 </summary>                                 
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Rpt_RevenueCenter] (@StartDate DATETIME = NULL,  
                                           @EndDate   DATETIME = NULL,  
                                           @DomainID  INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN TRANSACTION  
  
          DECLARE @FinancialYear INT;  
          DECLARE @StartFinancialMonth INT;  
          DECLARE @EndFinancialMonth INT;  
  
          IF ( @StartDate IS NULL  
                OR @StartDate = '' )  
              OR ( @EndDate IS NULL  
                    OR @EndDate = '' )  
            BEGIN  
                SET @StartFinancialMonth = (SELECT Value  
                                            FROM   tblApplicationConfiguration  
                                            WHERE  Code = 'STARTMTH'  
                                                   AND DomainID = @DomainID)  
                SET @EndFinancialMonth = ( ( (SELECT Value  
                                              FROM   tblApplicationConfiguration  
                                              WHERE  Code = 'STARTMTH'  
                                                     AND DomainID = @DomainID)  
                                             + 11 ) % 12 )  
  
                IF( Month(Getdate()) <= ( ( (SELECT Value  
                                             FROM   tblApplicationConfiguration  
                                             WHERE  Code = 'STARTMTH'  
                                                    AND DomainID = @DomainID)  
                                            + 11 ) % 12 ) )  
                  SET @FinancialYear = Year(Getdate()) - 1  
                ELSE  
                  SET @FinancialYear = Year(Getdate())  
  
                SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date        
                SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date       
            END  
  
          DECLARE @MONTHCOUNT          INT = 0,  
                  @currentmonth        INT,  
                  @currentyear         INT,  
                  @RevenueCenterID     VARCHAR(100) = '',  
                  @AMOUNT              VARCHAR(100),  
                  @RowValueUpdateQuery VARCHAR(MAX)= '',  
                  @Count               INT = 0,  
                  @QueryResult         NVARCHAR(MAX),  
                  @ColumnName          VARCHAR(10),  
                  @DynamicSQL          VARCHAR(MAX),  
                  @SumOfColumnQuery    VARCHAR(MAX),  
                  @SumOfRowQuery       VARCHAR(MAX),  
                  @RowTotalInsertQuery VARCHAR(MAX),  
                  @OutputQuery         VARCHAR(MAX)  
  
          CREATE TABLE #Result  
            (  
               ID                INT PRIMARY KEY IDENTITY(1, 1),  
               [RevenueCenterID] VARCHAR(200),  
               [RevenueCenter]   VARCHAR(200),  
               Total             MONEY NULL  
            )  
  
          SELECT Datename(MONTH, Dateadd(MONTH, X.number, @StartDate)) AS [MonthName],  
                 Datename(YY, Dateadd(MONTH, X.number, @StartDate))    [YEAR],  
                 Datepart(MM, Dateadd(MM, X.number, @StartDate))       [MONTH],  
                 X.number                                              AS RowNumber  
          INTO   #TempMonth  
          FROM   master.dbo.spt_values X  
          WHERE  X.type = 'P'  
                 AND X.number <= Datediff(MONTH, @StartDate, @EndDate);  
  
      SET @SumOfColumnQuery = (SELECT Substring((SELECT '+ ISNULL( ([' + Substring([MonthName], 1, 3 )  
                                                            + '_' + Substring([YEAR], 3, 2) + ']), 0)'  
                                                     FROM   #TempMonth  
                                                     FOR XML PATH('')), 2, 20000)) --Update Sum Of Column  value    
          SET @SumOfRowQuery = (SELECT Substring((SELECT ',  NULLIF(SUM(ISNULL(['  
                                                         + Substring([MonthName], 1, 3 ) + '_'  
                                                         + Substring([YEAR], 3, 2) + '], 0)), 0) '  
                                                  FROM   #TempMonth  
                                                  FOR XML PATH('')), 2, 20000)) --Update Sum Of Row value    
          SET @OutputQuery = (SELECT Substring((SELECT ',[' + Substring([MonthName], 1, 3 ) + '_'  
                                                       + Substring([YEAR], 3, 2) + ']'  
                                                FROM   #TempMonth  
                                                FOR XML PATH('')), 2, 20000)) -- Final Output Table Fields    
          SET @DynamicSQL =(SELECT 'ALTER TABLE #Result ADD ['  
                                   + Substring([MonthName], 1, 3 ) + '_'  
                                   + Substring([YEAR], 3, 2) + '] MONEY NULL;'  
                            FROM   #TempMonth  
                            FOR XML PATH('')) -- Add dynamic column    
          EXEC(@DynamicSQL) -- Add dynamic column    
          INSERT INTO #Result  
                      ([RevenueCenterID],  
                       [RevenueCenter])  
          SELECT DISTINCT ct.ID,  
                          ct.NAME  
          FROM   tblInvoice i  
                 JOIN tblCostCenter ct  
                   ON ct.ID = i.RevenueCenterID  
          WHERE  i.IsDeleted = 0  
                 AND i.DomainID = @DomainID  
          ORDER  BY ct.NAME ASC  
  
          WHILE( @Count < (SELECT Count(1)  
                           FROM   #Result) )  
            BEGIN  
                SET @RevenueCenterID = (SELECT RevenueCenterID  
                                        FROM   #Result  
                                        WHERE  ID = ( @Count + 1 ))  
  
                WHILE( @MONTHCOUNT < (SELECT Count(1)  
                                      FROM   #TempMonth) )  
                  BEGIN  
                      SELECT @currentmonth = [MONTH],  
                             @currentyear = [year],  
                             @ColumnName = Substring([MonthName], 1, 3 ) + '_'  
                                           + Substring([YEAR], 3, 2) -- Display Column Name    
                      FROM   #TempMonth  
                      WHERE  RowNumber = @MONTHCOUNT  
  
                      SET @AMOUNT = (SELECT Sum(CASE  
                                                  WHEN ( I.DiscountPercentage <> 0 ) THEN  
                                                    ( Isnull(( IT.Qty * IT.Rate ) + ISNULL(IT.SGSTAMOUNT, 0) + ISNULL(IT.cGSTAMOUNT, 0) - ( Isnull(( ( IT.Qty * IT.Rate ) * I.DiscountPercentage ), 0) ), 0) )  
                                                  ELSE  
                                                    Isnull(( ( IT.Qty * IT.Rate ) + ISNULL(IT.SGSTAMOUNT, 0) + ISNULL(IT.cGSTAMOUNT, 0) - Isnull(I.DiscountValue, 0) ), 0)  
                                                END) ProcessedAmount  
                                     FROM   tblInvoice I  
                                            JOIN tblInvoiceItem IT  
                                              ON IT.InvoiceID = I.ID  
                                                 AND IT.IsDeleted = 0  
                                            JOIN tblCostCenter ct  
                                              ON ct.ID = i.RevenueCenterID  
                                     WHERE  Datepart(MONTH, I.Date) = @currentmonth  
                                  AND Datepart(YEAR, I.Date) = @currentyear  
                                            AND ct.ID = @RevenueCenterID  
                                            AND ( @StartDate = ''  
                                                   OR I.Date >= @StartDate )  
                                            AND ( @EndDate = ''  
                                                   OR I.Date <= @EndDate ))  
                      SET @RowValueUpdateQuery = Isnull('UPDATE #Result SET [' + @ColumnName +'] = ' + @AMOUNT + ' WHERE RevenueCenterID = ''' + @RevenueCenterID + ''';', '')  
                                                 + Isnull(@RowValueUpdateQuery, '') --Update Row value    
                      SET @MONTHCOUNT = @MONTHCOUNT + 1  
                  END  
  
                SET @QueryResult = Isnull('UPDATE #Result SET Total = ' + @SumOfColumnQuery + ' WHERE RevenueCenterID = ''' + @RevenueCenterID + ''';', '')  
                                   + Isnull(@QueryResult, '') -- Result  table    
                SET @MONTHCOUNT = 0  
                SET @Count = @Count + 1  
            END  
  
          SET @RowTotalInsertQuery = ' insert into #result SELECT '''',''Total Amount'',NULLIF(Sum(Isnull(Total, 0)), 0),'  
                                     + @SumOfRowQuery + 'FROM   #Result' --Insert Row Total value    
  
          EXEC(@RowValueUpdateQuery ) --Update Row value    
  
          EXEC(@QueryResult) -- Result  table    
  
          EXEC(@RowTotalInsertQuery) --Insert Row Total value    
  
          EXEC( 'SELECT RevenueCenter AS [Revenue Center], Total, '+ @OutputQuery + ' FROM #Result Where Total <>0') -- Final Output table    
          COMMIT TRANSACTION  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
