﻿/****************************************************************************   
CREATED BY   : Naneeshwar  
CREATED DATE  :   18/11/2016   
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>   
  update tblbrs set isreconsiled=0, reconsileddate=null  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Managebrs] (@ID        INT,  
                                   @Date      DATE,  
                                   @Reconsile BIT,  
                                   @SessionID INT,  
                                   @DomainID  INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      DECLARE @Output VARCHAR(1000)  
  
      BEGIN TRY  
          BEGIN  
              UPDATE tblBRS  
              SET    ReconsiledDate = @Date,  
                     IsReconsiled = @Reconsile,  
                     ModifiedBy = @SessionID,  
                     ModifiedOn = Getdate()  
              WHERE  @ID = ID  
                     AND IsDeleted = 0  
                     AND DomainID = @DomainID  
  
              SET @Output = (SELECT [Message]  
                             FROM   tblErrorMessage  
                             WHERE  [Type] = 'Information'  
                                    AND Code = 'RCD_UPD'  
                                    AND IsDeleted = 0) --'Updated Successfully'  
          END  
  
          SELECT @Output  
      END TRY  
      BEGIN CATCH  
          ROLLBACK TRANSACTION  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
