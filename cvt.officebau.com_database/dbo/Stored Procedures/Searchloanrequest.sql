﻿  
/****************************************************************************     
CREATED BY   :  Anto.A  
CREATED DATE  :     
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>   
          [SearchLoanRequest] 1,0,0,106  
 </summary>                             
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[Searchloanrequest] (@DomainID   INT,  
                                           @StatusID   INT,  
                                           @Type       INT,  
                                           @EmployeeID INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          BEGIN ;  
              WITH CTE  
                   AS (SELECT LRE.ID                                       AS ID,  
                              LRE.LoanTypeID                               AS TypeID,  
                              LRE.LoanAmount                               AS Amount,  
                              CASE  
                                WHEN FC.LoanRequestID IS NOT NULL THEN  
                                  (SELECT Sum(ISNULL(Amount, 0))  
                                          + Sum(ISNULL(InterestAmount, 0))  
                                   FROM   tbl_LoanAmortization  
                                   WHERE  LoanRequestID = LRE.ID  
                                          AND IsDeleted = 0  
                                          AND DomainID = @DomainID  
                                          AND StatusID IN (SELECT ID  
                                                           FROM   tbl_Status  
                                                           WHERE  [Type] = 'Amortization'  
                                                                  AND Code != 'Open'))  
                                ELSE  
                                  LRE.TotalAmount  
                              END                                          AS TotalAmount,  
                              Isnull(EMP.EmpCodePattern, '') + Isnull(EMP.Code, '') + ' - ' + EMP.FullName AS ApprovarName,  
                              LRE.ApprovedDate                             AS ApprovedDate,  
                              --LRE.StatusID                                 AS StatusID,  
                              CASE  
                                WHEN FC.LoanRequestID IS NOT NULL THEN  
                                  (SELECT id  
                                   FROM   tbl_Status  
                                   WHERE  IsDeleted = 0  
                                          AND Type = 'Amortization'  
                                          AND Code = 'Foreclosed')  
                                ELSE  
                                  LRE.StatusID  
                              END                                          AS StatusID,  
                              CASE  
                                WHEN FC.LoanRequestID IS NOT NULL THEN  
                                  (SELECT Code  
                                   FROM   tbl_Status  
                                   WHERE  IsDeleted = 0  
                                          AND Type = 'Amortization'  
                                          AND Code = 'Foreclosed')  
                                ELSE  
                                  (SELECT Code  
                                   FROM   tbl_Status  
                                   WHERE  IsDeleted = 0  
                                          AND Type = 'Loan'  
                                          AND ID = LRE.StatusID)  
                              END                                          AS [Status],  
                              LRE.ModifiedOn                               AS ModifiedOn,  
                              LRE.CreatedOn                                AS RequestedDate  
                       FROM   tbl_LoanRequest LRE  
                              LEFT JOIN tbl_EmployeeMaster CRE  
                                     ON CRE.ID = LRE.CreatedBy  
                   LEFT JOIN tbl_EmployeeMaster EMP  
                                     ON emp.ID = LRE.ApproverID  
                              LEFT JOIN tbl_Status STA  
                                     ON STA.ID = LRE.StatusID  
                                        AND STA.Type = 'LOAN'  
                              LEFT JOIN tbl_LoanForeclosure FC  
                                     ON FC.LoanRequestID = LRE.ID  
                       WHERE  LRE.EmployeeID = @EmployeeID  
                              AND LRE.IsDeleted = 0  
                              AND LRE.DomainID = @DomainID  
                              AND (( Isnull(@StatusID, 0) = 0  
                                      OR LRE.StatusID = @StatusID )))  
              SELECT ID           AS ID,  
                     TypeID       AS Type,  
                     Amount       AS Amount,  
                     TotalAmount  AS TotalAmount,  
                     ApprovarName AS ApprovarName,  
                     ApprovedDate AS ApprovedDate,  
                     StatusID     AS StatusID,  
                     [Status]     AS [Status],  
                     ModifiedOn,  
                     RequestedDate  
              FROM   CTE  
              ORDER  BY ModifiedOn DESC  
          END  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
