﻿
/****************************************************************************   
CREATED BY    : 
CREATED DATE  : 
MODIFIED BY   : 
MODIFIED DATE : 
 <summary> 
		[GetPANDL] 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetPANDL] (@DomainID INT)
AS
  BEGIN
      BEGIN TRY
          ------- P & l --------------
          DECLARE @PL TABLE
            (
               ID          INT IDENTITY(1, 1),
               NAME        VARCHAR(100),
               AMOUNT      MONEY,
               lEDGER      VARCHAR(100),
               GROUPLEDGER VARCHAR(100),
               CHATAGERY   VARCHAR(100)
            )
          DECLARE @plvALUE MONEY,
                  @pltypee VARCHAR(100)='EXPENSE'

          INSERT INTO @PL
          SELECT tblledger.NAME,
                 openingBalance,
                 tblledger.NAME,
                 s.NAME,
                 'EXPENSE'
          FROM   tblledger
                 JOIN tblGroupLedger s
                   ON tblledger.GroupID = s.ID
          WHERE  isexpense = 1
                 AND tblledger.DomainID = @DomainID
                 AND tblledger.isdeleted = 0
          UNION
          SELECT e.NAME,
                 e.AMOUNT,
                 L.NAME,
                 e.Groups,
                 'EXPENSE'
          FROM   VW_expense e
                 JOIN tblLedger l
                   ON e.Ledger = l.NAME
          UNION
          SELECT p.NAME,
                 ( i.QTY * i.Rate ),
                 p.NAME,
                 'Revenue',
                 'INCOME'
          FROM   tblinvoiceitem i
                 JOIN tblProduct_V2 p
                   ON i.ProductID = p.ID
          WHERE  i.isdeleted = 0
                 AND i.DomainID = @DomainID
          UNION
          SELECT tblledger.NAME,
                 openingBalance,
                 tblledger.NAME,
                 s.NAME,
                 'INCOME'
          FROM   tblledger
                 JOIN tblGroupLedger s
                   ON tblledger.GroupID = s.ID
          WHERE  isexpense = 0
                 AND tblledger.DomainID = @DomainID
                 AND tblledger.isdeleted = 0

          SET @plvALUE= ( (SELECT Sum(AMOUNT)
                           FROM   @PL
                           WHERE  CHATAGERY = 'INCOME') - (SELECT Sum(AMOUNT)
                                                           FROM   @PL
                                                           WHERE  CHATAGERY = 'EXPENSE') )

          SELECT NAME,
                 Isnull(AMOUNT, 0) AS Amount,
                 lEDGER,
                 GROUPLEDGER,
                 CHATAGERY
          FROM   @pl
          WHERE  Amount <> 0
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
