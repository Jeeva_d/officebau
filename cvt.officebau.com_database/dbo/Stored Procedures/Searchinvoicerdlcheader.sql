﻿/****************************************************************************   
CREATED BY   : Naneeshwar  
CREATED DATE  :   21-Nov-2016
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>
 [Searchinvoicerdlcheader] 1,1
 </summary>                           
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Searchinvoicerdlcheader] (@ID       INT,
                                                 @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          BEGIN TRANSACTION

          SELECT inv.ID                           AS ID,
                 inv.Date                         AS InvoiceDate,
                 inv.DueDate                      AS DueDate,
                 cus.NAME                         AS CustomerName,
                 inv.BillAddress                  AS BillingAddress,
                 cus.PaymentTerms                 AS PaymentTerms,
                 inv.InvoiceNo                    AS InvoiceNo,
                 ( inv.DiscountPercentage * 100 ) AS DiscountPercentage,
                 --CONVERT(VARCHAR(10), ( inv.DiscountPercentage * 100 ))
                 --+ ' %'             AS DiscountPercentage,
                 inv.DiscountValue                AS DiscountValue,
                 cur.NAME                         AS Currency,
                 com.FullName                  AS CompanyName
          FROM   tblInvoice inv
                 LEFT JOIN tblCustomer cus
                        ON cus.ID = inv.CustomerID
                 LEFT JOIN tblCurrency cur
                        ON cur.id = cus.CurrencyID
                 LEFT JOIN tbl_CodeMaster cd
                        ON cd.ID = inv.DiscountID
                 LEFT JOIN tbl_Company com
                        ON com.id = inv.DomainID
          WHERE  inv.IsDeleted = 0
                 AND ( inv.ID = @ID
                        OR @ID IS NULL )
                 AND inv.DomainID = @DomainID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
