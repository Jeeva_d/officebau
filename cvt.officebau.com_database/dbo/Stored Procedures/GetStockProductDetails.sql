﻿/****************************************************************************                           
CREATED BY   : Dhanalakshmi. S                          
CREATED DATE  :   20-Nov-2018                        
MODIFIED BY   :                       
MODIFIED DATE  :                         
 <summary>                        
      [GetStockProductDetails] 22,1                 
 </summary>                                                   
 *****************************************************************************/            
CREATE PROCEDURE [dbo].[GetStockProductDetails] (@ProductID       INT,            
                                                 @DomainID INT)            
AS            
  BEGIN            
      SET NOCOUNT ON;            
            
      BEGIN TRY            
          BEGIN TRANSACTION            
            
          SELECT pd.ID                                                                                   AS ID,            
                 pd.NAME                                                                                 AS NAME,            
                 pd.TypeID                                                                                  AS TypeID,            
                 MFG                                                                            AS Manufacturer,            
                 pd.ModifiedOn                                                                           AS ModifiedOn,            
                 EMP.FullName                                                                            AS ModifiedBy,            
                 HSNId                                                                                   AS HSNID,            
                 GH.Code                                                                                 AS HsnCode,            
                 pd.UOMID,      
                 U.Name AS UOM,    
                 pd.OpeningStock,    
                 pd.LowStockThresold,    
                 pd.IsPurchase,    
                  PurchasePrice,    
                  PurchaseLedgerId,    
                  PurchaseDescription,    
                  IsSales,    
                  SalesPrice,     
                  SalesLedgerId,    
                  SalesDescription,  
                  IsInventroy,  
                  CM.Code As [Type],  
                  PL.Name AS PurchaseLedgerName,  
                  SL.Name AS SalesLedgerName,
                  GH.SGST,  
                 GH.CGST,  
                 GH.IGST  
          FROM   tblProduct_V2 pd            
                 LEFT JOIN tbl_EmployeeMaster EMP            
                        ON EMP.ID = pd.ModifiedBy            
                 LEFT JOIN tblGstHSN GH            
                        ON pd.HSNId = GH.ID        
                  LEFT JOIN tbl_UOM U            
                        ON U.ID = pd.UOMId  
                 LEFT JOIN tbl_CodeMaster CM            
                        ON CM.ID = pd.TypeID     
              LEFT JOIN tblLedger PL            
                        ON PL.ID = pd.PurchaseLedgerId      
               LEFT JOIN tblLedger SL            
                        ON SL.ID = pd.SalesLedgerId           
          WHERE  ( pd.ID = @ProductID            
                    OR @ProductID IS NULL )            
                 AND pd.DomainID = @DomainID            
                 AND pd.IsDeleted = 0            
            
          COMMIT TRANSACTION            
      END TRY            
      BEGIN CATCH            
          ROLLBACK TRANSACTION            
          DECLARE @ErrorMsg    VARCHAR(100),            
                  @ErrSeverity TINYINT            
          SELECT @ErrorMsg = Error_message(),            
  @ErrSeverity = Error_severity()            
          RAISERROR(@ErrorMsg,@ErrSeverity,1)            
      END CATCH            
  END
