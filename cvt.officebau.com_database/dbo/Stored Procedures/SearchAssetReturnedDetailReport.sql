﻿/**************************************************************************** 
CREATED BY			:	DHANALAKSHMI S
CREATED DATE		:	16 Aug 2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
  [SearchAssetReturnedDetailReport]   1,1, 1, 0,106,0,106
 </summary>                          
 *****************************************************************************/
CREATE PROCEDURE [dbo].[SearchAssetReturnedDetailReport] (@CompanyAssetID INT,
                                                         @DomainID       INT,
                                                         @IsActive       BIT,
                                                         @BusinessUnit   VARCHAR(20),
                                                         @UserID         INT,
                                                         @ReturnStatus   BIT,
                                                         @EmployeeID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @BusinessUnitIDs VARCHAR(50)

          IF( ISNULL(@BusinessUnit, 0) = 0 )
            SET @BusinessUnitIDs = (SELECT BusinessUnitID
                                    FROM   tbl_EmployeeMaster
                                    WHERE  ID = @UserID
                                           AND DomainID = @DomainID)
          ELSE
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','

          DECLARE @BusinessUnitTable TABLE
            (
               BusinessUnit VARCHAR(100)
            )

          INSERT INTO @BusinessUnitTable
          SELECT @BusinessUnitIDs

          SELECT Em.ID                                        AS EmployeeID,
                 EM.Code                                      AS EmployeeCode,
                 EM.FirstName + ' ' + ISNULL(EM.LastName, '') AS FirstName,
                 CA.AssetTypeID                               AS AssetTypeID,
                 AT.NAME                                      AS NAME,
                 CA.QTY                                       AS QTY,
                 CA.Make                                      AS Make,
                 CA.SerialNo                                  AS SerialNumber,
                 CA.HandoverOn                                AS HandOverOn,
                 CA.ReturnedOn                                AS ReturnedOn,
                 CA.ReturnedRemarks                           AS ReturnedRemarks,
                 CA.ReturnStatus                              AS ReturnStatus,
                 Cast(ROW_NUMBER()
                        OVER(
                          ORDER BY FirstName ASC) AS INT)     AS RowNo,
                 BU.NAME                                      AS BusinessUnit,
                 Em.IsActive                                  AS IsActive,
                 Dep.NAME                                     AS DepartmentName
          FROM   tbl_CompanyAssets CA
                 LEFT JOIN tbl_EmployeeMaster EM
                        ON EM.ID = CA.EmployeeID
                 LEFT JOIN tbl_AssetType AT
                        ON AT.ID = CA.AssetTypeID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
				 LEFT JOIN tbl_Department Dep
                        ON Dep.ID = EM.DepartmentID
                 JOIN @BusinessUnitTable TBU
                   ON TBU.BusinessUnit LIKE '%,'
                                            + Cast(EM.BaseLocationID AS VARCHAR(10))
                                            + ',%'
          WHERE  ( @CompanyAssetID = 0
                    OR CA.AssetTypeID = @CompanyAssetID )
                 AND CA.IsDeleted = 0
                 AND CA.DomainID = @DomainID
                 AND ( @IsActive IS NULL
                        OR EM.IsActive = @IsActive )
                 AND ( @BusinessUnit = 0
                        OR EM.BaseLocationID = @BusinessUnit )
                 AND ( @ReturnStatus IS NULL
                        OR CA.ReturnStatus = @ReturnStatus )
                 AND ( @EmployeeID = 0
                        OR Em.ID = @EmployeeID )
          AND EM.Code NOT LIKE 'TMP_%'
      END TRY
      BEGIN CATCH
        DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT;
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
