﻿/****************************************************************************         
CREATED BY  : Naneeshwar.M      
CREATED DATE :         
MODIFIED BY   :  Naneeshwar. M        
MODIFIED DATE  :  05-May-2017    
<summary>      
 
 [Rpt_daybookbycash] NULL,NULL,1  
</summary>                                 
*****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_daybookbycash] (@StartDate DATETIME = NULL,
                                           @EndDate   DATETIME = NULL,
                                           @DomainID  INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @FinancialYear INT;
          DECLARE @StartFinancialMonth INT;
          DECLARE @EndFinancialMonth INT;

          IF ( @StartDate IS NULL
                OR @StartDate = '' )
              OR ( @EndDate IS NULL
                    OR @EndDate = '' )
            BEGIN
                SET @StartFinancialMonth = (SELECT Value
                                            FROM   tblApplicationConfiguration
                                            WHERE  Code = 'STARTMTH'
                                                   AND DomainID = @DomainID)
                SET @EndFinancialMonth = ( ( (SELECT Value
                                              FROM   tblApplicationConfiguration
                                              WHERE  Code = 'STARTMTH'
                                                     AND DomainID = @DomainID)
                                             + 11 ) % 12 )

                IF( Month(Getdate()) <= ( ( (SELECT Value
                                             FROM   tblApplicationConfiguration
                                             WHERE  Code = 'STARTMTH'
                                                    AND DomainID = @DomainID)
                                            + 11 ) % 12 ) )
                  SET @FinancialYear = Year(Getdate()) - 1
                ELSE
                  SET @FinancialYear = Year(Getdate())

                SET @StartDate = Dateadd(MONTH, -1, Dateadd(MONTH, @StartFinancialMonth, Dateadd(YEAR, @FinancialYear - 1900, 0))) -- Fiscal Year Start date    
                SET @EndDate = Dateadd(DAY, -1, Dateadd(month, @EndFinancialMonth, Dateadd(YEAR, ( @FinancialYear + 1 ) - 1900, 0))) -- Fiscal Year End date   
            END;

          WITH CTE
               AS (SELECT DISTINCT ip.ID                                     AS ID,
                                   ip.PaymentDate                            AS [Date],
                                   cd.NAME                                   AS Party,
                                   (SELECT Sum(AMOUNT)
                                    FROM   tblInvoicePaymentMapping ipm
                                    WHERE  ipm.isdeleted = 0
                                           AND ipm.DomainID = @DomainID
                                           AND ipm.InvoicePaymentID = ip.ID) AS Receipts,
                                   NULL                                      AS Payments,
                                   (SELECT Sum(AMOUNT)
                                    FROM   tblInvoicePaymentMapping ipm
                                    WHERE  ipm.isdeleted = 0
                                           AND ipm.DomainID = @DomainID
                                           AND ipm.InvoicePaymentID = ip.ID) AS Amount,
                                   (SELECT NAME
                                    FROM   tblMasterTypes
                                    WHERE  NAME = 'InvoiceReceivable'
                                           AND Code = 'INVREC')              [Type]
                   FROM   tblInvoice i
                          LEFT JOIN tblInvoicePaymentMapping im
                                 ON im.InvoiceID = i.ID
                                    AND im.IsDeleted = 0
                          LEFT JOIN tblInvoicePayment ip
                                 ON ip.ID = im.InvoicePaymentID
                                    AND ip.IsDeleted = 0
                          LEFT JOIN tblCustomer cd
                                 ON cd.ID = i.CustomerID
                          LEFT JOIN tblVirtualCash vc
                                 ON vc.SourceID = im.ID
                                    AND SourceType = (SELECT id
                                                      FROM   tblMasterTypes
                                                      WHERE  NAME = 'InvoiceReceivable')
                   WHERE  ip.PaymentModeID = (SELECT ID
                                              FROM   tbl_CodeMaster
                                              WHERE  Code = 'Cash'
                                                     AND [Type] = 'PaymentType'
                                                     AND IsDeleted = 0)
                          AND i.IsDeleted = 0
                          AND i.DomainID = @DomainID
                          AND ( @StartDate = ''
                                 OR i.Date >= @StartDate )
                          AND ( @EndDate = ''
                                 OR i.Date <= @EndDate )
                   UNION ALL
                   SELECT DISTINCT ( CASE
                                       WHEN ( e.Type = 'Expense' ) THEN
                                         ( e.ID )
                                       ELSE
                                         ( ep.ID )
                                     END )                                            AS ID,
                                   ( CASE
                                       WHEN ( e.Type = 'Expense' ) THEN
                                         ( e.Date )
                                       ELSE
                                         (SELECT Date
                                          FROM   tblExpensePayment
                                          WHERE  ID = em.ExpensePaymentID)
                                     END )                                            AS [Date],
                                   vd.NAME                                            AS Party,
                                   NULL                                               AS Receipts,
                                   (SELECT Sum(AMOUNT)
                                    FROM   tblExpensePaymentMapping epm
                                    WHERE  epm.isdeleted = 0
                                           AND epm.DomainID = @DomainID
                                           AND epm.ExpensePaymentID = ep.ID)          AS Payments,
                                   ( -1 ) * (SELECT Sum(AMOUNT)
                                             FROM   tblExpensePaymentMapping epm
                                             WHERE  epm.isdeleted = 0
                                                    AND epm.DomainID = @DomainID
                                                    AND epm.ExpensePaymentID = ep.ID) AS Amount,
                                   ( CASE
                                       WHEN ( e.Type = 'Expense' ) THEN
                                         (SELECT NAME
                                          FROM   tblMasterTypes
                                          WHERE  NAME = ( 'Expense' )
                                                 AND Code = 'EXP')
                                       ELSE
                                         (SELECT NAME
                                          FROM   tblMasterTypes
                                          WHERE  NAME = 'PayBill'
                                                 AND CODE = 'BILL')
                                     END )                                            AS [Type]
                   FROM   tblExpense e
                          LEFT JOIN tblExpensePaymentMapping em
                                 ON em.ExpenseID = e.ID
                          LEFT JOIN tblExpensePayment ep
                                 ON ep.ID = em.ExpensePaymentID
                                    AND ep.IsDeleted = 0
                          LEFT JOIN tblVendor vd
                                 ON vd.ID = e.VendorID
                          LEFT JOIN tblVirtualCash vc
                                 ON vc.SourceID = em.ID
                                    AND SourceType IN (SELECT id
                                                       FROM   tblMasterTypes
                                                       WHERE  NAME IN( 'Expense', 'PayBill' ))
                                    AND vc.IsDeleted = 0
                   WHERE  ep.PaymentModeID = (SELECT ID
                                              FROM   tbl_CodeMaster
                                              WHERE  code = 'Cash'
                                                     AND [Type] = 'PaymentType'
                                                     AND IsDeleted = 0)
                          AND e.IsDeleted = 0
                          AND e.DomainID = @DomainID
                          AND ( @StartDate = ''
                                 OR e.Date >= @StartDate )
                          AND ( @EndDate = ''
                                 OR e.Date <= @EndDate )
                   UNION ALL
                   --SELECT DISTINCT r.ID      AS ID,
                   --                r.[Date]  AS Date,
                   --                PartyName AS Party,
                   --                ( CASE
                   --                    WHEN ( TransactionType = (SELECT ID
                   --                                              FROM   tblMasterTypes
                   --                                              WHERE  NAME = 'Receive Payment') ) THEN
                   --                      ( r.Amount )
                   --                    ELSE
                   --                      NULL
                   --                  END )   AS Receipts,
                   --                ( CASE
                   --                    WHEN ( TransactionType = (SELECT ID
                   --                                              FROM   tblMasterTypes
                   --                                              WHERE  NAME = 'Make Payment') ) THEN
                   --                      ( r.Amount )
                   --                    ELSE
                   --                      NULL
                   --                  END )   AS Payments,
                   --                ( CASE
                   --                    WHEN ( TransactionType = (SELECT ID
                   --                                              FROM   tblMasterTypes
                   --                                              WHERE  NAME = 'Make Payment') ) THEN
                   --                      ( ( -1 ) * r.Amount )
                   --                    ELSE
                   --                      r.Amount
                   --                  END )   AS Amount,
                   --                ( CASE
                   --                    WHEN ( TransactionType = (SELECT ID
                   --                                              FROM   tblMasterTypes
                   --                                              WHERE  NAME = 'Receive Payment') ) THEN
                   --                      ((SELECT NAME
                   --                        FROM   tblMasterTypes
                   --                        WHERE  NAME = 'Receive Payment'
                   --                               AND CODE = 'TransType'))
                   --                    ELSE
                   --                      (SELECT NAME
                   --                       FROM   tblMasterTypes
                   --                       WHERE  NAME = 'Make Payment'
                   --                              AND CODE = 'TransType')
                   --                  END )   AS [Type]
                   --FROM   tblReceipts r
                   --       LEFT JOIN tblVirtualCash vc
                   --              ON vc.SourceID = r.ID
                   --                 AND SourceType = TransactionType
                   --WHERE  r.PaymentMode = (SELECT ID
                   --                        FROM   tbl_CodeMaster
                   --                        WHERE  Code = 'Cash'
                   --                               AND [Type] = 'PaymentType'
                   --                               AND IsDeleted = 0)
                   --       AND r.IsDeleted = 0
                   --       AND r.DomainID = @DomainID
                   --       AND ( @StartDate = ''
                   --              OR r.Date >= @StartDate )
                   --       AND ( @EndDate = ''
                   --              OR r.Date <= @EndDate )
                   -------------------------------------------
                   SELECT DISTINCT r.ID                              AS ID,
                                   r.[PaymentDate]                   AS Date,
                                   PartyName                         AS Party,
                                   ( r.TotalAmount )                 AS Receipts,
                                   NULL                              AS Payments,
                                   r.TotalAmount                     AS Amount,
                                   ((SELECT NAME
                                     FROM   tblMasterTypes
                                     WHERE  NAME = 'Receive Payment'
                                            AND CODE = 'TransType')) AS [Type]
                   FROM   tbl_Receipts r
                          LEFT JOIN tblVirtualCash vc
                                 ON vc.SourceID = r.ID
                                    AND SourceType = (SELECT Id
                                                      FROM   tblMasterTypes
                                                      WHERE  NAME = 'Receive Payment'
                                                             AND CODE = 'TransType')
                   WHERE  r.ModeID = (SELECT ID
                                      FROM   tbl_CodeMaster
                                      WHERE  Code = 'Cash'
                                             AND [Type] = 'PaymentType'
                                             AND IsDeleted = 0)
                          AND r.IsDeleted = 0
                          AND r.DomainID = @DomainID
                          AND ( @StartDate = ''
                                 OR r.[PaymentDate] >= @StartDate )
                          AND ( @EndDate = ''
                                 OR r.[PaymentDate] <= @EndDate )
                   UNION ALL
                   SELECT DISTINCT r.ID                            AS ID,
                                   r.[PaymentDate]                 AS Date,
                                   PartyName                       AS Party,
                                   NULL                            AS Receipts,
                                   r.TotalAmount                   AS Payments,
                                   ( ( -1 ) * r.TotalAmount )      AS Amount,
                                   (SELECT NAME
                                    FROM   tblMasterTypes
                                    WHERE  NAME = 'Make Payment'
                                           AND CODE = 'TransType') AS [Type]
                   FROM   tbl_Payment r
                          LEFT JOIN tblVirtualCash vc
                                 ON vc.SourceID = r.ID
                                    AND SourceType = (SELECT Id
                                                      FROM   tblMasterTypes
                                                      WHERE  NAME = 'Make Payment'
                                                             AND CODE = 'TransType')
                   WHERE  r.ModeID = (SELECT ID
                                      FROM   tbl_CodeMaster
                                      WHERE  Code = 'Cash'
                                             AND [Type] = 'PaymentType'
                                             AND IsDeleted = 0)
                          AND r.IsDeleted = 0
                          AND r.DomainID = @DomainID
                          AND ( @StartDate = ''
                                 OR r.[PaymentDate] >= @StartDate )
                          AND ( @EndDate = ''
                                 OR r.[PaymentDate] <= @EndDate )
                   -----------------------------------------
                   UNION ALL
                   SELECT DISTINCT t.ID          AS ID,
                                   t.VoucherDate AS Date,
                                   b.DisplayName AS Party,
                                   ( CASE
                                       WHEN ( ToID = 0 ) THEN
                                         ( t.Amount )
                                       ELSE
                                         NULL
                                     END )       AS Receipts,
                                   ( CASE
                                       WHEN ( FromID = 0 ) THEN
                                         ( t.Amount )
                                       ELSE
                                         NULL
                                     END )       AS Payments,
                                   ( CASE
                                       WHEN ( FromID = 0 ) THEN
                                         ( ( -1 ) * t.Amount )
                                       ELSE
                                         t.Amount
                                     END )       AS Amount,
                                   ( CASE
                                       WHEN ( ToID = 0 ) THEN
                                         ((SELECT NAME
                                           FROM   tblMasterTypes
                                           WHERE  NAME = 'FromBank'
                                                  AND CODE = 'EXP'))
                                       ELSE
                                         (SELECT NAME
                                          FROM   tblMasterTypes
                                          WHERE  NAME = 'ToBank'
                                                 AND CODE = 'EXP')
                                     END )       AS [Type]
                   FROM   tblTransfer t
                          LEFT JOIN tblBank b
                                 ON b.ID = t.ToID
                                     OR b.ID = t.FromID
                          LEFT JOIN tblVirtualCash vc
                                 ON vc.SourceID = t.ID
                   WHERE  t.IsDeleted = 0
                          AND ( t.FromID = 0
                                 OR t.ToID = 0 )
                          AND t.DomainID = @DomainID
                          AND ( @StartDate = ''
                                 OR t.VoucherDate >= @StartDate )
                          AND ( @EndDate = ''
                                 OR t.VoucherDate <= @EndDate ))
          SELECT ID,
                 [Date],
                 Party,
                 Receipts,
                 Payments,
                 Amount,
                 [Type],
                 CONVERT (DECIMAL(16, 2), 0.00)[Closing Balance],
                 Row_number()
                   OVER(
                     ORDER BY [Date])          RowOrder
          INTO   #TempCash
          FROM   CTE
          WHERE  [Date] >= @StartDate
                 AND [Date] <= @EndDate

          UPDATE csh
          SET    [Closing Balance] = csh.Amount
          FROM   #TempCash csh
          WHERE  RowOrder = 1

          UPDATE csh
          SET    [Closing Balance] = (SELECT Sum([AMOUNT])
                                      FROM   #TempCash t
                                      WHERE  t.RowOrder <= csh.RowOrder)
          FROM   #TempCash csh

          INSERT INTO #TempCash
          SELECT NULL,
                 NULL,
                 'Total Amount',
                 Sum(Receipts),
                 Sum(Payments),
                 NULL,
                 NULL,
                 NULL,
                 NULL
          FROM   #TempCash

          SELECT ID,
                 RIGHT(CONVERT(VARCHAR, 0) + CONVERT(VARCHAR, Datepart(DAY, [Date])), 2)
                 + '-'
                 + CONVERT(VARCHAR(3), Datename(Month, [Date]))
                 + '-'
                 + CONVERT(VARCHAR(4), Datepart(YEAR, [Date])) AS Date,
                 Party,
                 Receipts,
                 Payments,
                 [Closing Balance],
                 [Type]
          FROM   #TempCash
          ORDER  BY CASE
                      WHEN( Date IS NULL ) THEN
                        '1-1-2999'
                      ELSE
                        DATE
                    END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
