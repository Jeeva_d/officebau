﻿/****************************************************************************                       
CREATED BY   :                       
CREATED DATE  :                       
MODIFIED BY   :                       
MODIFIED DATE  :                       
 <summary>                              
   select * from tblinvoice                    
 </summary>                                               
 *****************************************************************************/
CREATE	 PROCEDURE [dbo].[Managejournal] (@ID INT,
@JournalNo VARCHAR(50),
@JournalDate DATETIME,
@Description VARCHAR(500),
@IsDeleted BIT,
@SessionID INT,
@DomainID INT,
@JOURNAL Journal READONLY)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE @OUTPUT VARCHAR(100)
			   ,@JournalID INT
			   ,@AutogenPaddingLength INT
			   ,@AutogeneratePrefix VARCHAR(100)
			   ,@AutogenNo VARCHAR(100)

		SET @AutogeneratePrefix = 'JOUR'
		SET @AutogenPaddingLength = '1'
		SET @AutogenNo = @AutogeneratePrefix +
		+RIGHT('0000000000000001', @AutogenPaddingLength)

		IF (@IsDeleted = 1)
			IF ((SELECT
						COUNT(*)
					FROM tbl_PaymentMapping
					WHERE Isdeleted = 0
					AND DomainId = @DomainID
					AND RefId IN (SELECT
							id
						FROM @JOURNAL))
				+ (SELECT
						COUNT(*)
					FROM tbl_ReceiptsMapping
					WHERE Isdeleted = 0
					AND DomainId = @DomainID
					AND RefId IN (SELECT
							id
						FROM @JOURNAL))
				<> 0)
			BEGIN
				SET @OUTPUT = (SELECT
						[Message]
					FROM tblErrorMessage
					WHERE [type] = 'Warning'
					AND Code = 'RCD_REF'
					AND IsDeleted = 0)
			END
			ELSE
			BEGIN

				UPDATE tblJournal
				SET IsDeleted = 1
				WHERE @JournalNo = JournalNo
				AND @DomainID = DomainID

				SET @Output = (SELECT
						[Message]
					FROM tblErrorMessage
					WHERE [type] = 'Information'
					AND Code = 'RCD_DEL'
					AND IsDeleted = 0) --'Deleted Successfully'                  
			END
		ELSE
		BEGIN
			IF (@ID = 0)
			BEGIN
				IF EXISTS (SELECT
							1
						FROM tblJournal
						WHERE IsDeleted = 0
						AND ISNULL(AutoJournalNo, NULL) <> ''
						AND DomainID = @DomainID)
					SET @AutogenNo = @AutogeneratePrefix +
					+(SELECT
							RIGHT('0000000'
							+ CAST(MAX(RIGHT(AutoJournalNo, @AutogenPaddingLength)) + 1 AS VARCHAR(1000)), @AutogenPaddingLength)
						FROM tblJournal
						WHERE LEFT(AutoJournalNo, (LEN(AutoJournalNo) - @AutogenPaddingLength)) = @AutogeneratePrefix
						AND ISNULL(AutoJournalNo, '') <> ''
						AND DomainID = @DomainID)

			BEGIN
				INSERT INTO tblJournal (JournalNo,
				JournalDate,
				Debit,
				Credit,
				Description,
				AutoJournalNo,
				LedgerProductID,
				DomainID,
				CreatedBy,
				ModifiedBy,
				type)
					SELECT
						@JournalNo
					   ,@JournalDate
					   ,Debit
					   ,Credit
					   ,@Description
					   ,@AutogenNo
					   ,LedgerProductID
					   ,@DomainID
					   ,@SessionID
					   ,@SessionID
					   ,type
					FROM @JOURNAL
					WHERE IsDeleted = 0
			END

				SET @OUTPUT = (SELECT
						[Message]
					FROM tblErrorMessage
					WHERE [type] = 'Information'
					AND Code = 'RCD_INS'
					AND IsDeleted = 0) --'Inserted Successfully'                  
			END
			ELSE
			BEGIN
				SET @JournalID = @ID
				IF ((SELECT
							COUNT(*)
						FROM tbl_PaymentMapping
						WHERE Isdeleted = 0
						AND DomainId = @DomainID
						AND RefId IN (SELECT
								id
							FROM @JOURNAL))
					+ (SELECT
							COUNT(*)
						FROM tbl_ReceiptsMapping
						WHERE Isdeleted = 0
						AND DomainId = @DomainID
						AND RefId IN (SELECT
								id
							FROM @JOURNAL))
					<> 0)
				BEGIN
					SET @OUTPUT = (SELECT
							[Message]
						FROM tblErrorMessage
						WHERE [type] = 'Warning'
						AND Code = 'RCD_REF'
						AND IsDeleted = 0)
				END
				ELSE
				BEGIN
					SET @AutogenNo = (SELECT TOP 1
							AutoJournalNo
						FROM tblJournal
						WHERE id IN ((SELECT
								id
							FROM @JOURNAL)))

					UPDATE tblJournal
					SET JournalNo = @JournalNo
					   ,JournalDate = @JournalDate
					   ,tblJournal.Debit = I.Debit
					   ,tblJournal.Credit = I.Credit
					   ,tblJournal.LedgerProductID = I.LedgerProductID
					   ,tblJournal.type = I.type
					   ,tblJournal.Description = @Description
					   ,tblJournal.IsDeleted = I.IsDeleted
					   ,ModifiedBy = @SessionID
					   ,ModifiedOn = GETDATE()
					FROM tblJournal t
					INNER JOIN @JOURNAL AS I
						ON t.id = I.id
						AND DomainID = @DomainID

					SET @OUTPUT = (SELECT
							[Message]
						FROM tblErrorMessage
						WHERE [type] = 'Information'
						AND Code = 'RCD_UPD'
						AND IsDeleted = 0) --'Updated Successfully'                  
				
					IF ((SELECT
								COUNT(1)
							FROM @JOURNAL
							)
						> 0)
					BEGIN
						INSERT INTO tblJournal (JournalNo,
						JournalDate,
						Debit,
						Credit,
						Description,
						AutoJournalNo,
						LedgerProductID,
						DomainID,
						CreatedBy,
						ModifiedBy, type)
							SELECT
								@JournalNo
							   ,@JournalDate
							   ,Debit
							   ,Credit
							   ,@Description
							   ,@AutogenNo
							   ,LedgerProductID
							   ,@DomainID
							   ,@SessionID
							   ,@SessionID
							   ,type
							FROM @JOURNAL
							WHERE id = 0
							AND IsDeleted = 0
					END
				END


			END
		END

	FINISH:
		SELECT
			@Output

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMsg VARCHAR(100)
			   ,@ErrSeverity TINYINT
		SELECT
			@ErrorMsg = ERROR_MESSAGE()
		   ,@ErrSeverity = ERROR_SEVERITY()
		RAISERROR (@ErrorMsg, @ErrSeverity, 1)
	END CATCH
END
