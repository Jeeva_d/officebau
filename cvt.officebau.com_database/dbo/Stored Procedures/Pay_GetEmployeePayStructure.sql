﻿/****************************************************************************                 
CREATED BY   :                 
CREATED DATE  :                 
MODIFIED BY   :                 
MODIFIED DATE  :                 
 <summary>        
 [Pay_GetEmployeePayStructure] 1020      
 </summary>                                             
 *****************************************************************************/      
CREATE PROCEDURE [dbo].[Pay_GetEmployeePayStructure] (@ID INT)      
AS      
BEGIN      
 SET NOCOUNT ON;      
      
 BEGIN TRY      
  BEGIN TRANSACTION      
      
  SELECT      
   tpeps.ID AS ID,      
      tpeps.EmployeeId                               AS EmployeeID,      
            Isnull(em.Code, '') + ' - ' + em.FullName      AS EmployeeName,      
      tpeps.CompanyPayStructureID AS CompanyPayStucId,      
      tpeps.EffectiveFrom AS EffectiveFrom,      
  CAST(0 AS Decimal) AS Gross         
  FROM tbl_Pay_EmployeePayStructure tpeps      
  JOIN tbl_EmployeeMaster em      
   ON em.ID = tpeps.EmployeeID      
 --JOIN tbl_Pay_EmployeePayStructureDetails pepsd      
 --  ON pepsd.PayStructureId = tpeps.ID      
    
  WHERE tpeps.ID = @ID       
  COMMIT TRANSACTION      
 END TRY      
 BEGIN CATCH      
  ROLLBACK TRANSACTION      
  DECLARE @ErrorMsg VARCHAR(100)      
      ,@ErrSeverity TINYINT      
  SELECT      
   @ErrorMsg = ERROR_MESSAGE()      
     ,@ErrSeverity = ERROR_SEVERITY()      
  RAISERROR (@ErrorMsg, @ErrSeverity, 1)      
 END CATCH      
END
