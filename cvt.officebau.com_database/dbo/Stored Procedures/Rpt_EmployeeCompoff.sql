﻿/****************************************************************************     
CREATED BY   : DHANALAKSHMI S   
CREATED DATE  : 18 SEP 2018   
MODIFIED BY   :     
MODIFIED DATE  :     
 <summary>            
 [Rpt_EmployeeCompoff]  
 </summary>                             
 *****************************************************************************/
CREATE PROCEDURE [dbo].[Rpt_EmployeeCompoff] (@Year         INT,
                                             @MonthID      INT,
                                             @StatusID     INT,
                                             @BusinessUnit VARCHAR(100),
                                             @UserID       INT,
                                             @DomainID     INT)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          DECLARE @BusinessUnitIDs VARCHAR(50),
                  @YearID          INT = (SELECT NAME
                     FROM   Tbl_FinancialYear
                     WHERE  ID = @Year
                            AND IsDeleted = 0
                            AND DomainID = @DomainID)

          IF( Isnull(@BusinessUnit, 0) = 0 )
            SET @BusinessUnitIDs = (SELECT BusinessUnitID
                                    FROM   tbl_EmployeeMaster
                                    WHERE  ID = @UserID
                                           AND DomainID = @DomainID)
          ELSE
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','

          DECLARE @BusinessUnitTable TABLE
            (
               BusinessUnit VARCHAR(100)
            )

          INSERT INTO @BusinessUnitTable
          SELECT @BusinessUnitIDs

          SELECT Isnull(EM.EmpCodePattern, '') + EM.Code AS [Employee Code],
                 EM.FullName                             AS [Employee Name],
                 ECO.[Date]                              AS [Comp off Date],
                 ATT.Duration                            AS Duration,
                 ECO.RequesterRemarks                    AS Remarks,
                 STA.Code                                AS Status,
                 BU.NAME                                 AS [Business Unit]
          FROM   tbl_EmployeeMaster EM
                 LEFT JOIN tbl_EmployeeCompOff ECO
                        ON ECO.CreatedBy = EM.ID
                 LEFT JOIN tbl_BusinessUnit BU
                        ON BU.ID = EM.BaseLocationID
                 LEFT JOIN tbl_Attendance ATT
                        ON ECO.CreatedBy = ATT.EmployeeID
                           AND ECO.Date = ATT.LogDate
                 LEFT JOIN tbl_Status STA
                        ON STA.ID = ECO.StatusID
                 LEFT JOIN tbl_EmployeeMaster EMA
                        ON EMA.ID = ECO.ApproverID
          WHERE  ECO.Isdeleted = 0
                 AND EM.IsDeleted = 0
                 AND EM.DomainID = @DomainID
                 AND ECO.DomainID = @DomainID
                 AND ( Isnull(@StatusID, 0) = 0
                        OR eco.StatusID = @StatusID )
                 AND ( @BusinessUnit = 0
                        OR EM.BaseLocationID = @BusinessUnit )
                 AND Isnull(EM.IsActive, '') = 0
                 AND EM.Code NOT LIKE 'TMP_%'
                 AND (ISNULL(@MonthID, 0) = 0 OR Datepart(MM, ECO.[Date]) = @MonthID)
                 AND (ISNULL(@YearID, 0) = 0 OR Datepart(YYYY, ECO.[Date]) = @YearID)
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
