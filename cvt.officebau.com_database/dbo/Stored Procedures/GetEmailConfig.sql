﻿/****************************************************************************       
CREATED BY   :       
CREATED DATE  :       
MODIFIED BY   :       
MODIFIED DATE  :       
 <summary>    
	[GetEmailConfig] 'Expense',1,'cc'
 </summary>                               
 *****************************************************************************/
CREATE PROCEDURE [dbo].[GetEmailConfig] (@Key      VARCHAR(20),
                                        @DomainID INT,
                                        @Type     VARCHAR(50) = NULL)
AS
  BEGIN
      SET NOCOUNT ON;

      BEGIN TRY
          IF( ISNULL(@Type, '') = '' )
            BEGIN
                SELECT cc.ID           AS ID,
                       Toid            AS Toid,
                       cc.CC           AS CC,
                       BCC             AS BCC,
                       cc.IsAttachment AS IsAttachment,
                       [Key]           AS EmailKey,
                       cc.ModifiedOn,
                       e.FullName      AS ModifiedBy
                FROM   tblemailConfig cc
                       JOIN tbl_EmployeeMaster e
                         ON e.id = cc.ModifiedBy
                WHERE  cc.DomainID = @DomainID
                       AND [Key] = @Key
            END
		ELSE
          BEGIN
              DECLARE @Query VARCHAR(max) = ''

              SET @Query = 'SELECT cc.ID       AS ID, ' + CASE @Type WHEN 'TO' THEN 'toid' ELSE @Type END
                           + '  AS EmailID 
				  FROM   tblemailConfig cc  
						 JOIN tbl_EmployeeMaster e  
						   ON e.id = cc.ModifiedBy  
				  WHERE  cc.DomainID = '
                           + Cast(@DomainID AS VARCHAR)
                           + '
						 AND [Key] = ''' + @Key + ''''

              EXEC(@Query)
          END
      END TRY
      BEGIN CATCH
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
