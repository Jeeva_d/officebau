﻿/****************************************************************************   
CREATED BY   :   
CREATED DATE  : 21/03/2016  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>    
    SearchEmployeeHRA 0,1  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[SearchTDSemployee] (@EmployeeID INT,  
                                           @DomainID   INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          SELECT DISTINCT tds.EmployeeID                                 EmployeeID,  
                          emp.FirstName + ' ' + Isnull(emp.LastName, '') EmployeeName,  
                           ISNULL(emp.EmpCodePattern, '') + ''+ emp.Code                                       EmployeeCode  
          FROM   tbl_TDS tds  
                 JOIN tbl_EmployeeMaster emp  
                   ON tds.EmployeeID = emp.ID  
          WHERE   tds.DomainID = @DomainID  
                 AND ( Isnull(@EmployeeID, 0) = 0  
                        OR tds.EmployeeID = Isnull(@EmployeeID, 0) )  
          ORDER  BY emp.FirstName + ' ' + Isnull(emp.LastName, '')  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
