﻿/****************************************************************************                 
CREATED BY  :       
CREATED DATE        
MODIFIED BY         
MODIFIED DATE      
 <summary>  
 [SearchReceivablesTileDetails]             
 </summary>                                         
 *****************************************************************************/    
CREATE PROCEDURE [dbo].[SearchReceivablesTileDetails] (  
@TileType Varchar(100) = 'Receivables', @DomainID INT =1  
)    
AS    
BEGIN    
 SET NOCOUNT ON;    
    
 BEGIN TRY    
 select c.Name AS CustomerName,Sum(QTY*Rate)+Sum(SGSTAmount+CGSTAmount) AS Booked ,ISNULL(ReceivedAmount,0)+ISNULL(Sum(ih.Amount),0) AS ReceivedAmount ,  
(SELECT Substring((SELECT + ',' + it1.ItemDescription    
                                    FROM   tblInvoiceItem it1    
                                    WHERE  it1.IsDeleted = 0    
                                           AND i.ID = it1.InvoiceID    
                                    FOR XML PATH('')), 2, 200000)) AS [Description]  
         ,  
         (Sum(QTY*Rate)+Sum(SGSTAmount+CGSTAmount)) - (ISNULL((ReceivedAmount),0)+ISNULL(Sum(ih.Amount),0)) AS Receivables,  
         DueDate    
         INTO #Result  
 from tblinvoice i   
LEFT join tblInvoiceItem it on i.ID = it.InvoiceID and it.isdeleted=0  
LEFT Join tblInvoiceHoldings ih on i.ID =ih.InvoiceId and ih.Isdeleted=0  
Join tblCustomer c on c.ID =i.CustomerID  
Where i.IsDeleted = 0 AND i.DomainID  = @DomainID  
Group by  c.Name  ,ReceivedAmount,i.ID,DueDate  
  
IF @TileType = 'Receivables'  
Select * from #Result where Receivables<>0  
ELSE  
Select * from #Result where Receivables<>0 ANd DueDate <getdate()  
 END TRY    
 BEGIN CATCH    
  DECLARE @ErrorMsg VARCHAR(100)    
      ,@ErrSeverity TINYINT    
  SELECT    
   @ErrorMsg = ERROR_MESSAGE()    
     ,@ErrSeverity = ERROR_SEVERITY()    
  RAISERROR (@ErrorMsg, @ErrSeverity, 1)    
 END CATCH    
END
