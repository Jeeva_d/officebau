﻿/**************************************************************************** 
CREATED BY			:	Naneeshwar.M
CREATED DATE		:	06-Jun-2017
MODIFIED BY			:	
MODIFIED DATE		:	
 <summary>        
    [ManagePasswordAudit] 'naneeshwar_m@skybasenet.com','0125624'
 </summary>                         
 *****************************************************************************/
CREATE PROCEDURE [dbo].[ManagePasswordAudit] (@UserName VARCHAR(100),
                                             @GUID     VARCHAR(10),
                                             @DomainID INT)
AS
  BEGIN
      SET NOCOUNT ON;

      DECLARE @Output VARCHAR(1000) = 'Operation Failed!'
      --SET @UserName= CASE
      --                 WHEN( Substring(@UserName, 1, 3) = 'PNP' ) THEN( 'PAN' + Substring(@UserName, 4, 4) )
      --                 WHEN ( Substring(@UserName, 1, 3) = 'AHD' ) THEN( 'AHM' + Substring(@UserName, 4, 4) )
      --                 ELSE @UserName
      --               END
      DECLARE @UserLoginID INT=(SELECT ID
        FROM   tbl_EmployeeMaster
        WHERE  Isnull(IsActive, 0) = 0
               AND IsDeleted = 0
               AND LoginCode = @UserName
               AND DomainID = @DomainID)

      BEGIN TRY
          BEGIN TRANSACTION

          IF( (SELECT GUID
               FROM   tbl_PasswordReset
               WHERE  EmployeeID = @UserLoginID
                      AND IsExpired = 0
                      AND DomainID = @DomainID) = @GUID )
            BEGIN
                SET @Output = 'Correct Code'
            END
          ELSE
            SET @Output = 'Incorrect Code/Code Expired'

          SELECT @Output      AS UserMessage,
                 @UserLoginID AS UserID

          COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
          ROLLBACK TRANSACTION
          DECLARE @ErrorMsg    VARCHAR(100),
                  @ErrSeverity TINYINT
          SELECT @ErrorMsg = Error_message(),
                 @ErrSeverity = Error_severity()
          RAISERROR(@ErrorMsg,@ErrSeverity,1)
      END CATCH
  END
