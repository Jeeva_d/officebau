﻿/****************************************************************************   
CREATED BY      : Ajith N  
CREATED DATE  : 26-JULY-2017  
MODIFIED BY   :   
MODIFIED DATE  :   
 <summary>    
 [GetCompensationByBU]  '',1,2  
 </summary>                           
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetCompensationByBU] (@BusinessUnit VARCHAR(50),  
                                             @DomainID     INT,  
                                             @UserID       INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          SELECT *  
          INTO   #tmpAccess  
          FROM   Splitstring((SELECT BusinessUnitID  
                              FROM   tbl_EmployeeMaster  
                              WHERE  ID = @UserID), ',')  
  
          SELECT Isnull(DEP.NAME, 'Others')          AS BusinessUnit,  
                 ( Isnull(d.Amount, 0) )               AS Compensation,  
                 Row_number()  
                   OVER (  
                     PARTITION BY PS.EmployeeId  
                     ORDER BY PS.EffectiveFrom DESC) AS Rowno  
          INTO   #temp  
          FROM   tbl_EmployeeMaster EM  
                 LEFT JOIN tbl_pay_EmployeePayStructure PS  
                        ON EM.ID = PS.EmployeeId  
                           AND Isnull(PS.IsDeleted, 0) = 0  
				LEFT JOIN tbl_Pay_EmployeePayStructureDetails d on d.PayStructureId = PS.ID
				 And d.ISdeleted=0 and d.ComponentId = (Select Value from tbl_DashBoardConfiguration where [Key]='HRMSCOM' and DomainID =@DomainID)		   
				LEFT JOIN tbl_BusinessUnit BU  
                        ON BU.ID = EM.BaseLocationID  
                 LEFT JOIN tbl_Department DEP  
                        ON DEP.ID = EM.DepartmentID  
          WHERE  EM.IsDeleted = 0  
                 AND Isnull(EM.IsActive, 0) = 0  
                 AND EM.Code NOT LIKE 'TMP_%'  
                 AND EM.DomainID = @DomainID  
                 AND Isnull(d.Amount, 0) <> 0  
                 AND ( @BusinessUnit = ''  
                        OR Isnull(BU.NAME, 'Others') = @BusinessUnit )  
                 AND EM.BaseLocationID IN (SELECT item  
                                           FROM   #tmpAccess)  
  
          SELECT BusinessUnit,  
                 Sum(Compensation) AS Compensation  
          FROM   #temp  
          WHERE  rowno = 1  
          GROUP  BY BusinessUnit  
          ORDER  BY Compensation DESC,  
                    BusinessUnit  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR (@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
