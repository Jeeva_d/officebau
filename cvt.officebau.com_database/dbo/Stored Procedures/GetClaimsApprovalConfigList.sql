﻿/****************************************************************************                         
CREATED BY   : Dhanalakshmi S                      
CREATED DATE  : 3 Oct 2018                        
MODIFIED BY   :                         
MODIFIED DATE  :                         
 <summary>                                
     [GetClaimsApprovalConfigList] '',2,1                   
 </summary>                                                  
 *****************************************************************************/  
CREATE PROCEDURE [dbo].[GetClaimsApprovalConfigList] (@BusinessUnit VARCHAR(100),  
                                                     @UserID       INT,  
                                                     @DomainID     INT)  
AS  
  BEGIN  
      SET NOCOUNT ON;  
  
      BEGIN TRY  
          DECLARE @BusinessUnitIDs VARCHAR(50)  
  
          IF( Isnull(@BusinessUnit, 0) = 0 )  
            SET @BusinessUnitIDs = (SELECT BusinessUnitID  
                                    FROM   tbl_EmployeeMaster  
                                    WHERE  ID = @UserID  
                                           AND DomainID = @DomainID)  
          ELSE  
            SET @BusinessUnitIDs = ',' + @BusinessUnit + ','  
  
          DECLARE @BusinessUnitTable TABLE  
            (  
               BusinessUnit VARCHAR(100)  
            )  
  
          INSERT INTO @BusinessUnitTable  
          SELECT @BusinessUnitIDs  
  
          SELECT CA.ID,  
                 EM.ID                         AS EmployeeID,  
                 Isnull(EM.EmpCodePattern, '') + EM.Code + ' - ' + EM.FullName AS EmployeeName,  
                 EM.ReportingToID,  
                 Ap.FullName,  
                 CASE  
                   WHEN ( AP.FullName IS NULL ) THEN (  Isnull(RT.EmpCodePattern, '') + RT.Code + ' - ' + RT.FullName )  
                   ELSE ( Isnull(AP.EmpCodePattern, '') + AP.Code + ' - ' + AP.FullName )  
                 END                           AS ReportingToName  
          FROM   tbl_EmployeeMaster EM  
                 LEFT JOIN tbl_EmployeeMaster RT  
                        ON RT.ID = EM.ReportingToID  
                 LEFT JOIN tbl_ClaimsApproverConfiguration CA  
                        ON CA.EmployeeID = EM.ID  
                 LEFT JOIN tbl_EmployeeMaster AP  
                        ON AP.ID = CA.ApproverID  
          WHERE  Em.IsDeleted = 0  
                 AND EM.DomainID = @DomainID  
                 AND ( @BusinessUnit = 0  
                        OR EM.BaseLocationID = @BusinessUnit )  
                 AND ( Isnull(EM.IsActive, 0) = 0 )  
                 AND EM.Code NOT LIKE 'TMP_%'  
          ORDER  BY EM.Code  
      END TRY  
      BEGIN CATCH  
          DECLARE @ErrorMsg    VARCHAR(100),  
                  @ErrSeverity TINYINT;  
          SELECT @ErrorMsg = Error_message(),  
                 @ErrSeverity = Error_severity()  
          RAISERROR(@ErrorMsg,@ErrSeverity,1)  
      END CATCH  
  END
