﻿CREATE VIEW [dbo].[VW_NewExpense]            
             
AS            
SELECT expdet.ExpenseID,            
       V.NAME,            
       E.VendorID,            
       E.CreatedBy,            
       E.[Date],            
       E.BillNo,            
       E.[Type],            
    ISNULL(CGST,0)As CGSTAMOUNT ,ISNULL(SGST,0) AS SGSTAMOUNT,      
       expdet.Amount*QTY + ISNULL(CGST,0)+ISNULL(SGST,0) AS Amount,            
     Case When (IsLedger=1) Then  l.NAME  Else p.Name End                                                                                AS Ledger,            
     Case When (IsLedger=1) Then  GL.NAME  Else p.Name End                                                                               AS Groups,            
       C.Code                                                                                 AS StatusName,            
       ( ( (expdet.Amount*QTY + ISNULL(CGST,0)+ISNULL(SGST,0)) * ( Isnull(E.PaidAmount, 0) ) ) / ( (SELECT Isnull(Sum(amount*Qty), 0) +SUM(ISNULL(SGST,0)) +SUM(ISNULL(CGST,0))          
                                                              FROM   dbo.tblExpenseDetails            
                                                              WHERE  ExpenseID = expdet.ExpenseID            
                                                                     AND IsDeleted = 0)            
                                                             ) ) AS PaidAmount,            
       E.DueDate                                                                              AS DueDate,            
       E.ModifiedOn,            
       GL.ReportType,            
       L.ID                                                                                   AS LedgerID,          
       expdet.DomainID   AS   DomainID,          
    E.CostCenterID,l.IsTaxComponent  AS    IsTaxComponent          
FROM   dbo.tblExpenseDetails expdet            
     Left  JOIN dbo.tblLedger AS l            
         ON expdet.LedgerID = l.ID  And IsLedger=1       
  Left  JOIN dbo.tblProduct_v2 AS p            
         ON expdet.LedgerID = p.ID  And IsLedger=0        
      Left JOIN dbo.tblGroupLedger AS GL            
         ON l.GroupID = GL.ID            
     Left  JOIN dbo.tblexpense AS E            
         ON expdet.ExpenseID = E.ID            
      Left JOIN dbo.tblVendor AS v            
         ON E.VendorID = V.ID            
      Left JOIN dbo.tbl_CodeMaster AS C            
         ON c.ID = E.StatusID            
WHERE  expdet.IsDeleted = 0            
GROUP  BY expdet.ExpenseID,            
          expdet.Amount,            
          l.NAME,            
          GL.NAME,            
          expdet.ID,            
          V.NAME,            
          E.[Date],            
          E.PaidAmount,            
          E.BillNo,            
          E.[Type],            
          C.Code,            
          E.VendorID,            
          E.CreatedBy,            
          E.DueDate,            
          E.ModifiedOn,            
          GL.ReportType,            
          L.ID,          
          expdet.DomainID,          
    E.CostCenterID  ,IsTaxComponent  ,QTY,SGST,CGST,p.Name,IsLedger
