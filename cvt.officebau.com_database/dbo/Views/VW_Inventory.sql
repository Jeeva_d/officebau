﻿CREATE VIEW [dbo].[VW_Inventory]              
AS              
  WITH CTE              
       AS (SELECT P.ID                                                            AS ProductID,              
                  P.NAME                                                          AS ProductName,              
                  P.OpeningStock                                   AS OpeningStock,              
                  ai.RemainingQty                                                 AS RemainingQty,              
                  ai.ModifiedOn                                                   AS AdjustDate,              
                  ai.AdjustedQty                                                  AS AdjustedQty,              
                  (SELECT TOP 1 ModifiedOn              
                   FROM   tbl_Inventory              
                   WHERE  Stocktype = 'Purchase'              
                          AND IsDeleted = 0              
                          AND ProductID = P.ID              
                   ORDER  BY ModifiedOn DESC)                                     AS PurchaseDate,              
                  (SELECT TOP 1 ModifiedOn              
                   FROM   tbl_Inventory              
                   WHERE  Stocktype = 'Sales'              
                          AND IsDeleted = 0              
                          AND ProductID = P.ID              
                   ORDER  BY ModifiedOn DESC)                                     AS SalesDate,              
                  (SELECT TOP 1 ModifiedOn              
                   FROM   tbl_Inventory              
                   WHERE  Stocktype = 'SO'              
                          AND IsDeleted = 0              
                          AND ProductID = P.ID              
                   ORDER  BY ModifiedOn DESC)                                     AS SODate,              
                  (SELECT TOP 1 ModifiedOn              
                   FROM   tbl_InventorystockDistribution              
                   WHERE  IsDeleted = 0              
                          AND ProductID = P.ID)                                   AS DistributionDate,              
                  ( Isnull((SELECT Isnull(Sum(QTY), 0)              
                            FROM   tbl_Inventory              
                            WHERE  Stocktype = 'Purchase'              
                                   AND IsDeleted = 0              
                                   AND ProductID = P.ID              
                                   AND ( ai.ModifiedOn IS NULL              
                                          OR ModifiedOn >= ai.ModifiedOn )), 0) ) AS PurchaseQty,              
                  ( Isnull((SELECT Isnull(Sum(QTY), 0)              
                            FROM   tbl_Inventory              
                            WHERE  Stocktype = 'Sales'              
                                   AND IsDeleted = 0              
                                   AND ProductID = P.ID              
                                   AND ( ai.ModifiedOn IS NULL              
                                          OR ModifiedOn >= ai.ModifiedOn )), 0) ) AS SalesQty,              
                  ( Isnull((SELECT Isnull(Sum(QTY), 0)              
                            FROM   tbl_Inventory              
                            WHERE  Stocktype = 'SO'              
                                   AND IsDeleted = 0              
                                   AND ProductID = P.ID              
                                   --AND ( ai.ModifiedOn IS NULL              
                                   --       OR ModifiedOn >= ai.ModifiedOn )      
                                          ), 0) ) AS SOQty,              
                  ( Isnull((SELECT Isnull(Sum(QTY), 0)              
                            FROM   tbl_InventorystockDistribution              
                            WHERE  IsDeleted = 0              
                                   AND ProductID = P.ID              
               AND ( ai.ModifiedOn IS NULL              
                                          OR ModifiedOn >= ai.ModifiedOn )), 0) ) AS DistributionQty            
           FROM   tblProduct_V2 P              
                  LEFT JOIN tbl_InventoryStockAdjustMentItems ai              
                    ON ai.ProductID = p.ID              
                            AND ai.IsDeleted = 0              
                         AND ai.ModifiedOn = (SELECT TOP 1 ModifiedOn              
                                                 FROM   tbl_InventoryStockAdjustMentItems              
                                                 WHERE  ProductID = p.ID              
                                              AND IsDeleted = 0              
                                                 ORDER  BY Modifiedon DESC)              
           WHERE  P.IsDeleted = 0              
                  AND p.NAME IS NOT NULL              
                  AND P.IsInventroy = 1              
                  --AND P.OpeningStock <> 0              
          --GROUP  BY p.NAME,                  
          --               P.ID,                  
          --               P.IsDeleted,                  
          --               P.OpeningStock,                  
          --               ai.RemainingQty                  
          )              
  SELECT ProductID,              
         ProductName,              
         AdjustDate,              
         PurchaseDate,              
         ( CASE              
             WHEN AdjustDate IS NOT NULL THEN ( Isnull(RemainingQty, 0)              
                                                + Isnull(Sum(PurchaseQty), 0) - Isnull(Sum(SalesQty), 0) - Isnull(Sum(DistributionQty), 0) )              
             ELSE ( CASE              
                      WHEN RemainingQty IS NOT NULL THEN ( ( Isnull(OpeningStock, 0)              
                                                             + Isnull(AdjustedQty, 0) ) + Isnull(Sum(PurchaseQty), 0) - Isnull(Sum(SalesQty), 0) - Isnull(Sum(DistributionQty), 0) )              
                      ELSE ( ( ( Isnull(OpeningStock, 0)              
                                 + Isnull(Sum(PurchaseQty), 0) ) - Isnull(Sum(SalesQty), 0) ) - Isnull(Sum(DistributionQty), 0) )              
                    END )              
           END )               AS [StockonHand],              
         Isnull(Sum(SOQty), 0) AS CommitedStock,              
         ( CASE              
             WHEN AdjustDate IS NOT NULL THEN ( Isnull(RemainingQty, 0) + ( Isnull(Sum(PurchaseQty), 0) - Isnull(Sum(SOQty), 0) - Isnull(Sum(SalesQty), 0) ) - Isnull(Sum(DistributionQty), 0) )              
             ELSE ( CASE              
                      WHEN RemainingQty IS NOT NULL THEN ( ( Isnull(OpeningStock, 0)              
                                                             + Isnull(AdjustedQty, 0) ) + ( Isnull(Sum(PurchaseQty), 0) - Isnull(Sum(SOQty), 0) - Isnull(Sum(SalesQty), 0) ) - Isnull(Sum(DistributionQty), 0) )              
                      ELSE ( ( ( ( Isnull(OpeningStock, 0) ) + Isnull(Sum(PurchaseQty), 0) ) - Isnull(Sum(SOQty), 0) - Isnull(Sum(SalesQty), 0) ) - Isnull(Sum(DistributionQty), 0) )              
                    END )              
           END )               AS AvailableStock            
  FROM   CTE              
  GROUP  BY ProductID,              
            ProductName,              
            RemainingQty,              
            OpeningStock,              
            AdjustDate,              
            PurchaseDate,              
            AdjustedQty
