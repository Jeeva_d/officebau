﻿CREATE VIEW [dbo].[VW_Expense]    
AS    
  SELECT EXPDET.ExpenseID,    
         V.NAME,    
         E.VendorID,    
         E.CreatedBy,    
         E.Date,    
         E.BillNo,    
         E.Type,    
         ( ( EXPDET.Amount * QTY ) + Isnull(EXPDET.CGST, 0) + Isnull(EXPDET.SGST, 0) )                                                                          AS Amount,    
         L.NAME                                                                                                                                                 AS Ledger,    
         GL.NAME                                                                                                                                                AS Groups,    
         C.code                                                                                                                                                 AS StatusName,    
         ( ( ( ( EXPDET.Amount * QTY ) + Isnull(EXPDET.CGST, 0) + Isnull(EXPDET.SGST, 0) ) * ( Isnull(E.PaidAmount, 0) ) ) / ( (SELECT ( Isnull(Sum(amount*Qty), 0)    
                                                                                                                                         + Sum(Isnull( CGST, 0)) + Sum(ISnull(SGST, 0)) )    
                                                                                                                                FROM   dbo.tblExpenseDetails    
                                                                                                                                WHERE  ExpenseID = EXPDET.ExpenseID    
                                                                                                                                       AND IsDeleted = 0)    
                                                                                                                               ) ) AS PaidAmount,    
         E.DueDate                                                                                                                                              AS DueDate,    
         E.ModifiedOn,    
         EXPDET.DomainID,    
         GL.ReportType,    
         E.CostCenterID,    
         E.IsDeleted,    
         EXPDET.SGST                                                                                                                                            AS SGSTAmount,    
         EXPDET.IGST                                                                                                                                            AS IGSTAmount,    
         EXPDET.CGST                                                                                                                                            AS CGSTAmount    
  FROM   dbo.tblExpenseDetails EXPDET    
         JOIN dbo.tblLedger AS L    
           ON EXPDET.LedgerID = L.ID    
         JOIN dbo.tblGroupLedger AS GL    
           ON L.GroupID = GL.ID    
         JOIN dbo.tblexpense AS E    
           ON EXPDET.ExpenseID = E.ID    
         JOIN dbo.tblVendor AS V    
           ON E.VendorID = V.ID    
         JOIN dbo.tbl_CodeMaster AS C    
           ON C.ID = E.StatusID    
  WHERE  EXPDET.IsDeleted = 0    
         AND EXPDET.IsLedger = 1    
  GROUP  BY EXPDET.ExpenseID,    
            EXPDET.Amount,    
            L.NAME,    
            GL.NAME,    
            EXPDET.ID,    
            V.NAME,    
            E.Date,    
            E.PaidAmount,    
            E.BillNo,    
            E.Type,    
            C.Code,    
            E.VendorID,    
            E.CreatedBy,    
            E.DueDate,    
     E.ModifiedOn,    
            EXPDET.DomainID,    
            GL.ReportType,    
            E.CostCenterID,    
            E.IsDeleted,    
            EXPDET.Qty,    
            EXPDET.CGST,    
            EXPDET.SGST,    
            EXPDET.IGST    
  UNION    
  SELECT EXPDET.ExpenseID,    
         V.NAME,    
         E.VendorID,    
         E.CreatedBy,    
         E.Date,    
         E.BillNo,    
         E.Type,    
         ( ( EXPDET.Amount * QTY ) + Isnull(EXPDET.CGST, 0) + Isnull(EXPDET.SGST, 0) )                                                                          AS Amount,    
         L.NAME                                                                                                                                                 AS Ledger,    
         GL.NAME                                                                                                                                                AS Groups,    
         C.code                                                                                                                                                 AS StatusName,    
         ( ( ( ( EXPDET.Amount * QTY ) + Isnull(EXPDET.CGST, 0) + Isnull(EXPDET.SGST, 0) ) * ( Isnull(E.PaidAmount, 0) ) ) / ( (SELECT ( Isnull(Sum(amount*Qty), 0)    
                                                                                                                                         + Sum(Isnull( CGST, 0)) + Sum(ISnull(SGST, 0)) )    
                                                                                                                                FROM   dbo.tblExpenseDetails    
                                                                                                                                WHERE  ExpenseID = EXPDET.ExpenseID    
                                                                                                                                       AND IsDeleted = 0)    
                                                                                                                                ) ) AS PaidAmount,    
         E.DueDate                                                                                                                                              AS DueDate,    
         E.ModifiedOn,    
         EXPDET.DomainID,    
         GL.ReportType,    
         E.CostCenterID,    
         E.IsDeleted,    
         EXPDET.SGST                                                                                                                                            AS SGSTAmount,    
         EXPDET.IGST                                                                                                                                            AS IGSTAmount,    
         EXPDET.CGST                                                                                                                                            AS CGSTAmount    
  FROM   dbo.tblExpenseDetails EXPDET    
  JOIN dbo.tblProduct_v2 AS P    
           ON p.ID = EXPDET.LedgerID    
         JOIN dbo.tblLedger AS L    
           ON   L.ID = p.PurchaseLedgerId    
         JOIN dbo.tblGroupLedger AS GL    
           ON L.GroupID = GL.ID    
         JOIN dbo.tblexpense AS E    
           ON EXPDET.ExpenseID = E.ID    
         JOIN dbo.tblVendor AS V    
           ON E.VendorID = V.ID    
         JOIN dbo.tbl_CodeMaster AS C    
           ON C.ID = E.StatusID        
  WHERE  EXPDET.IsDeleted = 0    
         AND EXPDET.IsLedger = 0    
  GROUP  BY EXPDET.ExpenseID,    
            EXPDET.Amount,    
          L.NAME,    
            GL.NAME,    
            EXPDET.ID,    
            V.NAME,    
            E.Date,    
            E.PaidAmount,    
            E.BillNo,    
            E.Type,    
            C.Code,    
            E.VendorID,    
            E.CreatedBy,    
            E.DueDate,    
            E.ModifiedOn,    
            EXPDET.DomainID,    
            GL.ReportType,    
            E.CostCenterID,    
            E.IsDeleted,    
            EXPDET.Qty,    
            EXPDET.CGST,    
            EXPDET.SGST,    
            EXPDET.IGST
