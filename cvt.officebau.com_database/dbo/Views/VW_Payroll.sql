﻿
CREATE VIEW [dbo].[VW_Payroll]
AS
 SELECT PPD.PayrollId                             AS PayrollId,
        PPD.ComponentId                           AS ComponentId,
        PPD.Amount                                AS Amount,
        PPC.Description                           AS [Description],
        Emp.ID                                    AS EmployeeID,
        Emp.FullName                              AS Employee_Name,
        CONVERT(VARCHAR(20), emp.DOJ, 106)        AS Joining_Date,
        Isnull(Emp.EmpCodePattern, '') + Emp.Code + '_'
        + d.NAME                                  AS Emp_Code,
        Isnull(Emp.EmpCodePattern, '') + Emp.Code AS Employee_Code,
        b.NAME,
        d.NAME                                    AS Department,
        ep.MonthId                                AS MonthId,
        ep.YearId                                 AS YearId,
        ep.DomainId                               AS DomainId,
        b.Id                                      AS BuID,
        d.ID                                      AS DepID,
        m.Code                                    AS Month,
        de.NAME                                   AS Designation,
        de.ID                                     AS DesignationId,
       ISnull( lop.LopDays  ,0) AS LopDays,
        ppc.Ordinal
 FROM   dbo.tbl_Pay_EmployeePayrollDetails PPD
        JOIN dbo.tbl_Pay_PayrollCompontents AS PPC
          ON PPD.ComponentId = PPC.ID
        JOIN tbl_Pay_EmployeePayroll ep
          ON ep.ID = PPD.PayrollId
             AND ep.IsDeleted = 0
             AND ep.IsProcessed = 1
        JOIN tbl_EmployeeMaster emp
          ON emp.ID = ep.EmployeeId
        JOIN tbl_Department d
          ON d.ID = emp.DepartmentID
        JOIN tbl_BusinessUnit b
          ON b.ID = emp.BaseLocationID
        JOIN tbl_Month m
          ON m.ID = ep.MonthId
        JOIN tbl_Designation de
          ON de.ID = emp.DesignationID
       Left JOIN tbl_LopDetails lop
          ON lop.EmployeeId = ep.EmployeeId
             AND ep.MonthId = lop.MonthID
             AND ep.YearId = lop.year
 WHERE  PPD.IsDeleted = 0
