﻿  
CREATE VIEW [dbo].Vw_OpenInvoices      
AS      
   
Select C.Name AS Customer,InvoiceNo ,cur.Name as Currency,Date as [Invoice Date],DueDate,cc.Name as [Revenue center],  
      (SELECT Substring((SELECT + ',' + it.ItemDescription    
                                    FROM   tblInvoiceItem it    
                                    WHERE  it.IsDeleted = 0    
                                           AND i.ID = it.InvoiceID    
                                    FOR XML PATH('')), 2, 200000)) AS [Description],    
                 ( CASE    
                     WHEN ( i.DiscountPercentage <> 0 ) THEN    
                       (SELECT ( Isnull(Sum(Qty * Rate)    
                                        + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - ( Isnull(( Sum(Qty * Rate) * i.DiscountPercentage ), 0) ), 0) )    
                        FROM   tblInvoiceItem    
                        WHERE  InvoiceID = i.ID    
                               AND IsDeleted = 0    
                              )    
                     ELSE    
                       (SELECT ( Isnull(( Sum(Qty * Rate)    
                                          + Sum(Isnull(SGSTAmount, 0)+Isnull(CGSTAmount, 0)) - Isnull(i.DiscountValue, 0) ), 0) )    
                        FROM   tblInvoiceItem    
                        WHERE  InvoiceID = i.ID    
                               AND IsDeleted = 0    
                               )    
                   END )                                           AS TotalAmount,s.Code as Status  
 from tblinvoice i  
Join tblCustomer c on i.CustomerID = c.id  
jOIN tblCurrency cur on c.CurrencyID = cur.ID  
Left join tblCostCenter cc on cc.ID = i.RevenueCenterID  
join tbl_CodeMaster s on s.id= i.StatusID and s.Type = 'Open'  
Where i.IsDeleted=0
