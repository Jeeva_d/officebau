﻿CREATE VIEW [dbo].[VW_Income]    
AS    
  SELECT INV.ID,    
         INV.CustomerID,    
         INV.CreatedBy,    
         INV.InvoiceNo,    
         INV.[Date],    
         INV.DueDate,    
         CASE    
           WHEN ( INVITE.IsProduct = 0 ) THEN    
             C.NAME    
           ELSE    
             LED.NAME    
         END                                                                            AS Customer,    
         CD.Code                                                                        AS StatusName,    
         CASE    
           WHEN ( INV.DiscountPercentage <> 0 ) THEN    
             ( Isnull((( Qty * Rate )+ISNULL( INVITE.SGSTAmount,0)+ISNULL( INVITE.CGSTAmount,0)) - 
			 ( Isnull(( ( Qty * Rate +ISNULL( INVITE.SGSTAmount,0)+ISNULL( INVITE.CGSTAmount,0) ) * INV.DiscountPercentage ), 0) ), 0) )    
           ELSE    
             ( Isnull(( ( Qty * Rate +ISNULL( INVITE.SGSTAmount,0)+ISNULL( INVITE.CGSTAmount,0) ) -
			  Isnull(INV.DiscountValue, 0) ), 0) )    
         END                                                                            Amount,    
         (( INVITE.QTY * INVITE.Rate )+ISNULL( INVITE.SGSTAmount,0)+ISNULL( INVITE.CGSTAmount,0) ) * 
(Select Sum(amount) from tblInvoicePaymentMapping where InvoiceID=  INV.ID and IsDeleted=0) / (SELECT Sum(( Isnull(QTY, 0) * Isnull(Rate, 0) ))  +
		 ISNULL( SUM(SGSTAmount),0)+ISNULL(SUM( CGSTAmount),0)  
                                                              FROM   Dbo.tblInvoiceItem    
                                                              WHERE  InvoiceID = INV.ID    
                                                                     AND IsDeleted = 0) AS Received,    
         INV.ModifiedOn,    
         INV.Type,    
         (SELECT LedgerID    
          FROM   tblInvoicePayment    
          WHERE  ID = (SELECT TOP 1 InvoicePaymentID    
                       FROM   tblInvoicePaymentMapping    
                       WHERE  InvoiceID = INV.ID    
                              AND IsDeleted = 0))                                       AS LedgerID,    
         INVITE.ProductID,    
         INVITE.DomainID,    
         INV.RevenueCenterID,    
         INVITE.IsProduct,    
         INV.IsDeleted,    
         INVITE.SGSTAmount,    
         INVITE.CGSTAmount,    
         5                                                                              AS ReportType --  --Let Assume ReportType = 5(income) in tblInvoiceItem table     
  FROM   dbo.tblInvoiceItem AS INVITE    
         LEFT JOIN dbo.tblInvoice AS INV    
                ON INV.ID = INVITE.InvoiceID    
                   AND INVITE.IsDeleted = 0    
         LEFT JOIN dbo.tblCustomer AS C    
                ON INV.CustomerID = C.ID    
         LEFT JOIN dbo.tblLedger AS LED    
                ON INVITE.ProductID = LED.ID    
         JOIN dbo.tbl_CodeMaster AS CD    
           ON INV.StatusID = CD.ID    
  WHERE  INV.IsDeleted = 0    and INVITE.IsDeleted = 0

  GROUP  BY INV.ID,    
            INV.[Date],    
            INV.DueDate,    
            C.NAME,    
            CD.Code,    
            INVITE.QTY,    
            INVITE.Rate,    
            INV.ReceivedAmount,    
            INV.InvoiceNo,    
            INV.CustomerID,    
            INV.CreatedBy,    
            INV.ModifiedOn,    
            INV.[Type],    
            INV.DiscountPercentage,    
            INV.DiscountValue,    
            INVITE.ProductID,    
            INVITE.DomainID,    
            INV.RevenueCenterID,    
            INVITE.IsProduct,    
            LED.NAME,    
            INV.IsDeleted,    
            INVITE.SGSTAmount,    
            INVITE.CGSTAmount
