﻿CREATE TABLE [dbo].[tbl_OffBoardHistory] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]    INT              NOT NULL,
    [WithdrawDate]  DATE             NOT NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tbl_tbl_OffBoardHistory_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tbl_OffBoardHistory_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tbl_tbl_OffBoardHistory_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tbl_OffBoardHistory_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]     BIT              CONSTRAINT [DF_tbl_OffBoardHistory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]      INT              NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_OffBoardHistory_HistoryID] DEFAULT (newid()) NULL,
    [RelievingDate] DATE             NULL,
    [Reason]        VARCHAR (100)    NULL,
    [RequestedDate] DATE             NULL,
    [Status]        VARCHAR (50)     NULL,
    CONSTRAINT [PK_tbl_OffBoardHistory] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

