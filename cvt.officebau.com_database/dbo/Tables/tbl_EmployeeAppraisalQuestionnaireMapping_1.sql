﻿CREATE TABLE [dbo].[tbl_EmployeeAppraisalQuestionnaireMapping] (
    [EmployeeAppraisalID] INT              NOT NULL,
    [QuestionID]          INT              NOT NULL,
    [Rating]              INT              NOT NULL,
    [Remarks]             VARCHAR (1000)   NULL,
    [AppraiserRating]     INT              NOT NULL,
    [AppraiserRemarks]    VARCHAR (1000)   NULL,
    [IsDeleted]           BIT              CONSTRAINT [DF_tbl_EmployeeAppraisalQuestionnaireMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]           DATETIME         CONSTRAINT [DF_tbl_EmployeeAppraisalQuestionnaireMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           INT              CONSTRAINT [DF_tbl_EmployeeAppraisalQuestionnaireMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]          DATETIME         CONSTRAINT [DF_tbl_EmployeeAppraisalQuestionnaireMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]          INT              CONSTRAINT [DF_tbl_EmployeeAppraisalQuestionnaireMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]           UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeAppraisalQuestionnaireMapping_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]            INT              CONSTRAINT [DF_tbl_EmployeeAppraisalQuestionnaireMapping_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [FK_tbl_EmployeeAppraisalQuestionnaireMapping_tbl_EmployeeAppraisal] FOREIGN KEY ([EmployeeAppraisalID]) REFERENCES [dbo].[tbl_EmployeeAppraisal] ([ID]),
    CONSTRAINT [FK_tbl_EmployeeAppraisalQuestionnaireMapping_tbl_Questionnaire] FOREIGN KEY ([QuestionID]) REFERENCES [dbo].[tbl_Questionnaire] ([ID])
);

