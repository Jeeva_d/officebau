﻿CREATE TABLE [dbo].[tbl_EmailRecipients] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [KeyID]          INT              NOT NULL,
    [BusinessUnitID] INT              NOT NULL,
    [CCEmailID]      VARCHAR (1000)   NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_EmailRecipients_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_EmailRecipients_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_EmailRecipients_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_EmailRecipients_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tbl_EmailRecipients_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmailRecipients_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]       INT              CONSTRAINT [DF_tbl_EmailRecipients_DomainID] DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_EmailRecipients_tbl_MailerEventKeys] FOREIGN KEY ([KeyID]) REFERENCES [dbo].[tbl_MailerEventKeys] ([ID])
);

