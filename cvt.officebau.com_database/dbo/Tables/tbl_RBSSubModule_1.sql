﻿CREATE TABLE [dbo].[tbl_RBSSubModule] (
    [ID]               INT              IDENTITY (1, 1) NOT NULL,
    [ModuleID]         INT              NOT NULL,
    [SubModule]        VARCHAR (100)    NOT NULL,
    [Description]      VARCHAR (8000)   NULL,
    [SubModuleOrderID] INT              NULL,
    [CreatedBy]        INT              CONSTRAINT [DF_tbl_RBSSubModule_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_tbl_RBSSubModule_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       INT              CONSTRAINT [DF_tbl_RBSSubModule_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_tbl_RBSSubModule_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_tbl_RBSSubModule_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_RBSSubModule_HistoryID] DEFAULT (newid()) NULL,
    [MenuIcon]         VARCHAR (50)     NULL,
    [DomainID]         INT              CONSTRAINT [DF_tbl_RBSSubModule_DomainID] DEFAULT ((1)) NULL
);

