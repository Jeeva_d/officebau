﻿CREATE TABLE [dbo].[tblCostCenter] (
    [ID]               INT              IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (100)    NOT NULL,
    [Remarks]          VARCHAR (1000)   NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_tblCostCenter_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_tblCostCenter_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        INT              CONSTRAINT [DF_tblCostCenter_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_tblCostCenter_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       INT              CONSTRAINT [DF_tblCostCenter_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]         INT              CONSTRAINT [DF_tblCostCenter_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tblCostCenter_HistoryID] DEFAULT (newid()) NOT NULL,
    [CostcentertypeID] INT              NULL,
    CONSTRAINT [PK_tblCostCenter] PRIMARY KEY CLUSTERED ([ID] ASC)
);

