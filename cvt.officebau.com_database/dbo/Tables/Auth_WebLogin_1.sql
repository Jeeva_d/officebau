﻿CREATE TABLE [dbo].[Auth_WebLogin] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [FullName]     VARCHAR (100)    NOT NULL,
    [Company]      VARCHAR (150)    NOT NULL,
    [Email]        VARCHAR (50)     NOT NULL,
    [TeamSize]     INT              NOT NULL,
    [IndustryType] INT              NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_Auth_WebLogin_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedDate]  DATETIME         CONSTRAINT [DF_Auth_WebLogin_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_Auth_WebLogin_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedDate] DATETIME         CONSTRAINT [DF_Auth_WebLogin_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_Auth_WebLogin_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    CONSTRAINT [PK_Auth_WebLogin] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

