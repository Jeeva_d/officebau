﻿CREATE TABLE [dbo].[tbl_Notifications] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [FromID]           INT           NOT NULL,
    [FromName]         VARCHAR (200) NOT NULL,
    [ImagePath]        VARCHAR (MAX) NULL,
    [ToID]             INT           NOT NULL,
    [Message]          VARCHAR (500) NOT NULL,
    [OldMessage]       VARCHAR (500) NULL,
    [SenderIsViewed]   BIT           NULL,
    [ReceiverIsViewed] BIT           NULL,
    [IsViewed]         BIT           NULL,
    [CreatedOn]        DATETIME      CONSTRAINT [DF_tbl_Notifications_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        INT           CONSTRAINT [DF_tbl_Notifications_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedBy]       INT           CONSTRAINT [DF_tbl_Notifications_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME      CONSTRAINT [DF_tbl_Notifications_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [WishType]         VARCHAR (10)  NULL,
    CONSTRAINT [PK_tbl_Notifications] PRIMARY KEY CLUSTERED ([Id] ASC)
);

