﻿CREATE TABLE [dbo].[tbl_Skills] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (200)    NULL,
    [Remarks]    VARCHAR (500)    NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_Skills_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_Skills_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_Skills_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_Skills_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_Skills_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_Skills_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Skills_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_Skills] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

