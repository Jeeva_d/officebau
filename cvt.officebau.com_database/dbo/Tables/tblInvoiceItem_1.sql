﻿CREATE TABLE [dbo].[tblInvoiceItem] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [InvoiceID]       INT              NOT NULL,
    [ProductID]       INT              NULL,
    [ItemDescription] VARCHAR (1000)   NULL,
    [QTY]             INT              NOT NULL,
    [Rate]            MONEY            NOT NULL,
    [SGST]            DECIMAL (18, 2)  NULL,
    [SGSTAmount]      MONEY            NULL,
    [CGST]            DECIMAL (18, 2)  NULL,
    [CGSTAmount]      MONEY            NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tblInvoiceItem_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tblInvoiceItem_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tblInvoiceItem_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tblInvoiceItem_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tblInvoiceItem_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]        INT              CONSTRAINT [DF_tblInvoiceItem_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tblInvoiceItem_HistoryID] DEFAULT (newid()) NOT NULL,
    [IsProduct]       INT              CONSTRAINT [DF_tblInvoiceItem_IsProduct] DEFAULT ((0)) NOT NULL,
    [SoItemID]        INT              NULL,
    CONSTRAINT [PK_tblInvoiceItem] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tblInvoiceItem_tblInvoice] FOREIGN KEY ([InvoiceID]) REFERENCES [dbo].[tblInvoice] ([ID])
);

