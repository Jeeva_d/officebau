﻿CREATE TABLE [dbo].[tbl_Country] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_Country_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_Country_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_Country_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_Country_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_Country_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Country_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_Country_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_Country] PRIMARY KEY CLUSTERED ([ID] ASC)
);

