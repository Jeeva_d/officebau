﻿CREATE TABLE [dbo].[tbl_FandF] (
    [ID]                INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]        INT              NOT NULL,
    [LastWorkingDate]   DATETIME         NOT NULL,
    [YearsOfService]    VARCHAR (150)    NULL,
    [TotalDaysInMonth]  INT              NOT NULL,
    [DaysPaid]          INT              NOT NULL,
    [ReasonForLeaving]  VARCHAR (1000)   NULL,
    [NoticeDays]        INT              NOT NULL,
    [LeaveEncashedDays] INT              NOT NULL,
    [LOPDays]           INT              NOT NULL,
    [NetAmountPayable]  MONEY            NOT NULL,
    [Remarks]           VARCHAR (1000)   NULL,
    [StatusID]          INT              NOT NULL,
    [HRApproverId]      INT              NOT NULL,
    [HRApprovedDate]    DATETIME         NULL,
    [HRStatusID]        INT              NULL,
    [HRRemarks]         VARCHAR (1000)   NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_tbl_FAndF_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]         DATETIME         CONSTRAINT [DF_tbl_FAndF_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         INT              CONSTRAINT [DF_tbl_FAndF_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]        DATETIME         CONSTRAINT [DF_tbl_FAndF_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]        INT              CONSTRAINT [DF_tbl_FAndF_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]         UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_FAndF_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]          INT              CONSTRAINT [DF_tbl_FAndF_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_FAndF] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_FAndF_tbl_EmployeeMaster_EmployeeID] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID])
);

