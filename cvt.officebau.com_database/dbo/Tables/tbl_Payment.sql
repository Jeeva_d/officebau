﻿CREATE TABLE [dbo].[tbl_Payment] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [LedgerId]    INT              NOT NULL,
    [PaymentDate] DATE             NOT NULL,
    [ModeID]      INT              NOT NULL,
    [BankID]      INT              NOT NULL,
    [PartyName]   VARCHAR (100)    NULL,
    [Description] VARCHAR (MAX)    NULL,
    [TotalAmount] MONEY            NOT NULL,
    [Isdeleted]   BIT              CONSTRAINT [DF_tbl_Payment_Isdeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              CONSTRAINT [DF_tbl_Payment_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Payment_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Payment_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Payment_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Payment_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryId]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Payment_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_Payment] PRIMARY KEY CLUSTERED ([Id] ASC)
);

