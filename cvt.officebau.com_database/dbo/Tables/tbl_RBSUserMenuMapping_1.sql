﻿CREATE TABLE [dbo].[tbl_RBSUserMenuMapping] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID] INT              NOT NULL,
    [MenuID]     INT              NOT NULL,
    [MRead]      BIT              CONSTRAINT [DF_tbl_RBSUserMenuMapping_MRead] DEFAULT ((0)) NOT NULL,
    [MWrite]     BIT              CONSTRAINT [DF_tbl_RBSUserMenuMapping_MWrite] DEFAULT ((0)) NOT NULL,
    [MEdit]      BIT              CONSTRAINT [DF_tbl_RBSUserMenuMapping_MEdit] DEFAULT ((0)) NOT NULL,
    [MDelete]    BIT              CONSTRAINT [DF_tbl_RBSUserMenuMapping_MDelete] DEFAULT ((0)) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_RBSUserMenuMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_RBSUserMenuMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_RBSUserMenuMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_RBSUserMenuMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_RBSUserMenuMapping_IsDeleted] DEFAULT ((0)) NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_RBSUserMenuMapping_DomainID] DEFAULT ((1)) NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_RBSUserMenuMapping_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_RBSUserMenuMapping] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

