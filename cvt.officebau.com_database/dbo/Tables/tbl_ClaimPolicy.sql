﻿CREATE TABLE [dbo].[tbl_ClaimPolicy] (
    [ID]                   INT              IDENTITY (1, 1) NOT NULL,
    [DesignationMappingID] INT              NOT NULL,
    [ExpenseType]          INT              NOT NULL,
    [MetroAmount]          MONEY            NULL,
    [NonMetroAmount]       MONEY            NULL,
    [IsDeleted]            BIT              CONSTRAINT [DF_tbl_ClaimPolicy_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]            DATETIME         CONSTRAINT [DF_tbl_ClaimPolicy_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            INT              CONSTRAINT [DF_tbl_ClaimPolicy_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]           DATETIME         CONSTRAINT [DF_tbl_ClaimPolicy_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]           INT              CONSTRAINT [DF_tbl_ClaimPolicy_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]            UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ClaimPolicy_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]             INT              CONSTRAINT [DF_tbl_ClaimPolicy_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_ClaimPolicy] PRIMARY KEY CLUSTERED ([ID] ASC)
);

