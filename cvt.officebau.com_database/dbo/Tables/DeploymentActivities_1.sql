﻿CREATE TABLE [dbo].[DeploymentActivities] (
    [ID]              INT           IDENTITY (1, 1) NOT NULL,
    [BrodcastMessage] VARCHAR (MAX) NULL,
    [IsActive]        BIT           CONSTRAINT [DF_DeploymentActivities_IsActive] DEFAULT ((0)) NOT NULL,
    [CreatedBy]       INT           CONSTRAINT [DF_DeploymentActivities_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME      CONSTRAINT [DF_DeploymentActivities_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT           CONSTRAINT [DF_DeploymentActivities_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME      CONSTRAINT [DF_DeploymentActivities_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [FromDate]        DATETIME      NULL,
    [ToDate]          DATETIME      NULL,
    [DomainID]        VARCHAR (500) NULL,
    [IsDeleted]       BIT           CONSTRAINT [DF_DeploymentActivities_IsDeleted] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_DeploymentActivities] PRIMARY KEY CLUSTERED ([ID] ASC)
);

