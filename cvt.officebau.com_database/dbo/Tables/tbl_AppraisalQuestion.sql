﻿CREATE TABLE [dbo].[tbl_AppraisalQuestion] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [QuestionID]    INT              NOT NULL,
    [DepartmentID]  INT              NOT NULL,
    [DesignationID] INT              NOT NULL,
    [IsDeleted]     BIT              CONSTRAINT [DF_tbl_AppraisalQuestion_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tbl_AppraisalQuestion_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tbl_AppraisalQuestion_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tbl_AppraisalQuestion_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tbl_AppraisalQuestion_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_AppraisalQuestion_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]      INT              CONSTRAINT [DF_tbl_AppraisalQuestion_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_AppraisalQuestion] PRIMARY KEY CLUSTERED ([ID] ASC)
);

