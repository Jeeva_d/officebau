﻿CREATE TABLE [dbo].[TDSDeclaration] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]      INT              NOT NULL,
    [ComponentID]     INT              NOT NULL,
    [Declaration]     MONEY            NULL,
    [Cleared]         MONEY            NULL,
    [Rejected]        MONEY            NULL,
    [Remarks]         VARCHAR (8000)   NULL,
    [Submitted]       MONEY            NULL,
    [ApproverRemarks] VARCHAR (8000)   NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_TDSDeclaration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_TDSDeclaration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_TDSDeclaration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_TDSDeclaration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_TDSDeclaration_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]        INT              NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_TDSDeclaration_HistoryID] DEFAULT (newid()) NOT NULL,
    [FinancialYearID] INT              NULL,
    CONSTRAINT [PK_TDSDeclaration] PRIMARY KEY CLUSTERED ([ID] ASC)
);

