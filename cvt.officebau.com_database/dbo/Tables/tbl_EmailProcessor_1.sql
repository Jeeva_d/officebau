﻿CREATE TABLE [dbo].[tbl_EmailProcessor] (
    [ID]             BIGINT           IDENTITY (1, 1) NOT NULL,
    [FromID]         VARCHAR (200)    NULL,
    [ToID]           VARCHAR (MAX)    NULL,
    [Cc]             VARCHAR (MAX)    NULL,
    [Bcc]            VARCHAR (MAX)    NULL,
    [Subject]        VARCHAR (MAX)    NULL,
    [Content]        TEXT             NULL,
    [Attachment]     TEXT             NULL,
    [AttachmentPath] TEXT             NULL,
    [IsPolled]       INT              CONSTRAINT [DF_tbl_EmailProcessor_IsPolled] DEFAULT ((0)) NULL,
    [SentDate]       DATETIME         NULL,
    [RetryCount]     INT              NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_EmailProcessor_CreatedOn] DEFAULT (getdate()) NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_EmailProcessor_CreatedBy] DEFAULT ((1)) NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_EmailProcessor_ModifiedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_EmailProcessor_ModifiedBy] DEFAULT ((1)) NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tbl_EmailProcessor_IsDeleted] DEFAULT ((0)) NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmailProcessor_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_EmailProcessor] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

