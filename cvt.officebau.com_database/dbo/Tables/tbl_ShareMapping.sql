﻿CREATE TABLE [dbo].[tbl_ShareMapping] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [EmployeeId] INT             NOT NULL,
    [IsCustomer] BIT             NOT NULL,
    [CustomerId] INT             NULL,
    [VendorId]   INT             NULL,
    [Percentage] DECIMAL (16, 4) NULL,
    [CreatedBy]  INT             CONSTRAINT [DF_tbl_ShareMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME        CONSTRAINT [DF_tbl_ShareMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT             CONSTRAINT [DF_tbl_ShareMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME        CONSTRAINT [DF_tbl_ShareMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]   INT             NOT NULL,
    [IsDeleted]  BIT             CONSTRAINT [DF_tbl_ShareMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_ShareMapping] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tbl_ShareMapping_tbl_EmployeeMaster] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID]),
    CONSTRAINT [FK_tbl_ShareMapping_tblCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[tblCustomer] ([ID]),
    CONSTRAINT [FK_tbl_ShareMapping_tblVendor] FOREIGN KEY ([VendorId]) REFERENCES [dbo].[tblVendor] ([ID])
);

