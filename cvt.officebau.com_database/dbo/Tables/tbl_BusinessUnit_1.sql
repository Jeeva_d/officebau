﻿CREATE TABLE [dbo].[tbl_BusinessUnit] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (100)    NOT NULL,
    [Remarks]         VARCHAR (1000)   NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tbl_BusinessUnit_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tbl_BusinessUnit_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tbl_BusinessUnit_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tbl_BusinessUnit_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tbl_BusinessUnit_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]        INT              CONSTRAINT [DF_tbl_BusinessUnit_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_BusinessUnit_HistoryID] DEFAULT (newid()) NOT NULL,
    [ParentID]        INT              CONSTRAINT [DF_tbl_BusinessUnit_ParentID] DEFAULT ((0)) NULL,
    [Address]         VARCHAR (5000)   NULL,
    [PayslipDuration] INT              NULL,
    CONSTRAINT [PK_tbl_BusinessUnit] PRIMARY KEY CLUSTERED ([ID] ASC)
);

