﻿CREATE TABLE [dbo].[tbl_Year] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NULL,
    [Remarks]    VARCHAR (8000)   NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_Year_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_Year_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_Year_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_Year_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_Year_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Year_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_Year] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

