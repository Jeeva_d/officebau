﻿CREATE TABLE [dbo].[tbl_BusinessHours] (
    [ID]                           INT              IDENTITY (1, 1) NOT NULL,
    [RegionID]                     INT              NOT NULL,
    [FullDay]                      DECIMAL (5, 2)   NOT NULL,
    [HalfDay]                      DECIMAL (5, 2)   NOT NULL,
    [DomainID]                     INT              NOT NULL,
    [HistoryID]                    UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_BusinessHours_HistoryID] DEFAULT (newid()) NOT NULL,
    [IsDeleted]                    BIT              CONSTRAINT [DF_tbl_BusinessHours_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]                    INT              CONSTRAINT [DF_tbl_BusinessHours_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]                    DATETIME         CONSTRAINT [DF_tbl_BusinessHours_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]                   INT              CONSTRAINT [DF_tbl_BusinessHours_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]                   DATETIME         CONSTRAINT [DF_tbl_BusinessHours_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [BusinessStartHour]            TIME (7)         NULL,
    [BusinessEndHour]              TIME (7)         NULL,
    [IsBasedOnLastCheckout]        BIT              CONSTRAINT [DF_tbl_BusinessHours_IsBasedOnLastCheckout] DEFAULT ((0)) NOT NULL,
    [IncludeLeaveDetailsInPaystub] BIT              CONSTRAINT [DF_tbl_BusinessHours_IncludeLeaveDetailsInPaystub] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_tbl_BusinessHours] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_BusinessHours_tbl_BusinessUnit] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[tbl_BusinessUnit] ([ID])
);

