﻿CREATE TABLE [dbo].[tbl_LoanForeclosure] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [LoanRequestID] INT              NOT NULL,
    [Amount]        MONEY            NOT NULL,
    [InterestRate]  DECIMAL (5, 2)   NULL,
    [TotalAmount]   MONEY            NOT NULL,
    [PaymentModeID] INT              NOT NULL,
    [PaymentDate]   DATE             NOT NULL,
    [Description]   VARCHAR (500)    NULL,
    [DomainID]      INT              NOT NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tbl_LoanForeclosure_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tbl_LoanForeclosure_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tbl_LoanForeclosure_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tbl_LoanForeclosure_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]     BIT              CONSTRAINT [DF_tbl_LoanForeclosure_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_LoanForeclosure_HistoryID] DEFAULT (newid()) NOT NULL,
    [ReferenceNo]   VARCHAR (50)     NULL,
    CONSTRAINT [PK_tbl_LoanForeclosure] PRIMARY KEY CLUSTERED ([ID] ASC)
);

