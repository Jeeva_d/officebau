﻿CREATE TABLE [dbo].[tbl_EmployeeSkills] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]      INT              NOT NULL,
    [SkillsID]        INT              NOT NULL,
    [LevelID]         INT              NOT NULL,
    [Year]            INT              NOT NULL,
    [Month]           INT              NOT NULL,
    [LastUsedVersion] VARCHAR (50)     NULL,
    [LastUsedYearID]  INT              NULL,
    [Description]     VARCHAR (500)    NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tbl_EmployeeSkills_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tbl_EmployeeSkills_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tbl_EmployeeSkills_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tbl_EmployeeSkills_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tbl_EmployeeSkills_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeSkills_HistoryID] DEFAULT (newid()) NULL,
    [DomainID]        INT              CONSTRAINT [DF_tbl_EmployeeSkills_DomainID] DEFAULT ((1)) NOT NULL,
    [IsLocked]        BIT              CONSTRAINT [DF_tbl_EmployeeSkills_IsLocked] DEFAULT ((0)) NOT NULL,
    [DateofExpiry]    DATETIME         NULL,
    [FileID]          UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_tbl_EmployeeSkills] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

