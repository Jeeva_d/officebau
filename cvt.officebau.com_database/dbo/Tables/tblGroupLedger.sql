﻿CREATE TABLE [dbo].[tblGroupLedger] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tblGroupLedger_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tblGroupLedger_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tblGroupLedger_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tblGroupLedger_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tblGroupLedger_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tblGroupLedger_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tblGroupLedger_HistoryID] DEFAULT (newid()) NOT NULL,
    [ReportType] INT              CONSTRAINT [DF_tblGroupLedger_ReportType] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tblGroupLedger] PRIMARY KEY CLUSTERED ([ID] ASC)
);

