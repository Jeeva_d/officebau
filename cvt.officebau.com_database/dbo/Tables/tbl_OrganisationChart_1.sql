﻿CREATE TABLE [dbo].[tbl_OrganisationChart] (
    [ID]                   INT              IDENTITY (1, 1) NOT NULL,
    [DesignationID]        INT              NULL,
    [MappingID]            INT              NULL,
    [RoleResponsibilityID] INT              NULL,
    [IsDeleted]            BIT              CONSTRAINT [DF_tbl_OrganisationChart_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]            DATETIME         CONSTRAINT [DF_tbl_OrganisationChart_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            INT              CONSTRAINT [DF_tbl_OrganisationChart_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]           DATETIME         CONSTRAINT [DF_tbl_OrganisationChart_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]           INT              CONSTRAINT [DF_tbl_OrganisationChart_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]            UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_OrganisationChart_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]             INT              CONSTRAINT [DF_tbl_OrganisationChart_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_OrganisationChart] PRIMARY KEY CLUSTERED ([ID] ASC)
);

