﻿CREATE TABLE [dbo].[tbl_EmployeeTask] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [TaskId]      INT              NOT NULL,
    [HoursWorked] TIME (7)         NOT NULL,
    [StatusId]    INT              NOT NULL,
    [Description] NVARCHAR (MAX)   NOT NULL,
    [CreatedBy]   INT              NOT NULL,
    [CreatedOn]   DATETIME         NOT NULL,
    [ModifiedBy]  INT              NOT NULL,
    [ModifiedOn]  DATETIME         NOT NULL,
    [DomainId]    INT              NOT NULL,
    [HistoryId]   UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK__tbl_Empl__3214EC2738EE6921] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tbl_EmployeeTask_tbl_EmployeeMaster] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID]),
    CONSTRAINT [FK_tbl_EmployeeTask_tbl_InternalMeeting_Task] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[tbl_InternalMeeting_Task] ([Id]),
    CONSTRAINT [FK_tbl_EmployeeTask_tbl_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[tbl_Status] ([ID])
);

