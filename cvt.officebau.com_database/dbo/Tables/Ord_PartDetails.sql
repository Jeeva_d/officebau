﻿CREATE TABLE [dbo].[Ord_PartDetails] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (100)    NOT NULL,
    [Vendor]      VARCHAR (500)    NOT NULL,
    [Description] VARCHAR (500)    NULL,
    [Amount]      DECIMAL (16, 4)  NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_Ord_PartDetails_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_Ord_PartDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_Ord_PartDetails_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_Ord_PartDetails_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_Ord_PartDetails_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryId]   UNIQUEIDENTIFIER CONSTRAINT [DF_Ord_PartDetails_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_Ord_PartDetails] PRIMARY KEY CLUSTERED ([Id] ASC)
);

