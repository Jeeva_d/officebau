﻿CREATE TABLE [dbo].[tbl_FileUpload] (
    [Id]               UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_FileUpload_Id] DEFAULT (newid()) NOT NULL,
    [Path]             VARCHAR (MAX)    NOT NULL,
    [ReferenceTable]   VARCHAR (50)     NULL,
    [FileName]         VARCHAR (500)    NULL,
    [FileType]         VARCHAR (100)    NULL,
    [FileContent]      VARBINARY (MAX)  NULL,
    [DomainID]         INT              CONSTRAINT [DF_tbl_FileUpload_DomainID] DEFAULT ((1)) NOT NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_tbl_FileUpload_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]        INT              CONSTRAINT [DF_tbl_FileUpload_CreateBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_tbl_FileUpload_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       INT              CONSTRAINT [DF_tbl_FileUpload_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_tbl_FileUpload_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [OriginalFileName] VARCHAR (100)    NULL,
    CONSTRAINT [PK_tbl_FileUpload] PRIMARY KEY CLUSTERED ([Id] ASC)
);

