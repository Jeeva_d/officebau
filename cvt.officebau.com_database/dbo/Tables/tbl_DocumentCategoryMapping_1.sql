﻿CREATE TABLE [dbo].[tbl_DocumentCategoryMapping] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [EmployeeId]  INT           NOT NULL,
    [CategoryIds] VARCHAR (MAX) NOT NULL,
    [IsDeleted]   BIT           CONSTRAINT [DF_tbl_DocumentCategoryMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]   DATETIME      CONSTRAINT [DF_tbl_DocumentCategoryMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   INT           CONSTRAINT [DF_tbl_DocumentCategoryMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME      CONSTRAINT [DF_tbl_DocumentCategoryMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT           CONSTRAINT [DF_tbl_DocumentCategoryMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainId]    INT           CONSTRAINT [DF_tbl_DocumentCategoryMapping_DomainId] DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

