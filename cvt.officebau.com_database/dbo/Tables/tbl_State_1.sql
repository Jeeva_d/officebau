﻿CREATE TABLE [dbo].[tbl_State] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [CountryID]  INT              NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_State_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_State_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_State_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_State_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_State_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_State_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_State_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_State] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_State_tbl_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[tbl_Country] ([ID])
);

