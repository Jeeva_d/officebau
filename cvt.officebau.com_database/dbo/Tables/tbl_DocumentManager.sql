﻿CREATE TABLE [dbo].[tbl_DocumentManager] (
    [Id]                 INT              IDENTITY (1, 1) NOT NULL,
    [DocumentCategoryId] INT              NOT NULL,
    [ExpiryDate]         DATETIME         NULL,
    [Active]             BIT              NOT NULL,
    [Description]        VARCHAR (MAX)    NULL,
    [CreatedOn]          DATETIME         CONSTRAINT [DF_tbl_DocumentManager_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          INT              CONSTRAINT [DF_tbl_DocumentManager_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]         DATETIME         CONSTRAINT [DF_tbl_DocumentManager_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]         INT              CONSTRAINT [DF_tbl_DocumentManager_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainId]           INT              CONSTRAINT [DF_tbl_DocumentManager_DomainId] DEFAULT ((1)) NOT NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_tbl_DocumentManager_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryId]          UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_DocumentManager_HistoryId] DEFAULT (newid()) NOT NULL,
    [DocumentName]       VARCHAR (500)    NOT NULL,
    CONSTRAINT [PK_tbl_DocumentManager] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tbl_DocumentManager_tbl_DocumentCategory] FOREIGN KEY ([DocumentCategoryId]) REFERENCES [dbo].[tbl_DocumentCategory] ([Id])
);

