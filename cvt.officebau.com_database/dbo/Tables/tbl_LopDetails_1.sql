﻿CREATE TABLE [dbo].[tbl_LopDetails] (
    [Id]         INT             IDENTITY (1, 1) NOT NULL,
    [EmployeeId] INT             NOT NULL,
    [MonthID]    INT             NOT NULL,
    [Year]       INT             NOT NULL,
    [LopDays]    DECIMAL (18, 1) NOT NULL,
    [CreatedBy]  INT             CONSTRAINT [DF_tbl_Lop_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME        CONSTRAINT [DF_tbl_Lop_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT             CONSTRAINT [DF_tbl_Lop_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME        CONSTRAINT [DF_tbl_Lop_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]   INT             CONSTRAINT [DF_tbl_Lop_DomainId] DEFAULT ((1)) NOT NULL,
    [IsManual]   BIT             CONSTRAINT [DF_tbl_tbl_LopDetails_IsManual] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_Lop] PRIMARY KEY CLUSTERED ([Id] ASC)
);

