﻿CREATE TABLE [dbo].[tbl_DashBoardConfiguration] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [Key]        VARCHAR (10)  NOT NULL,
    [DomainID]   INT           CONSTRAINT [DF_tbl_DashBoardConfiguration_DomainID] DEFAULT ((1)) NOT NULL,
    [Value]      VARCHAR (MAX) NULL,
    [CreatedBy]  INT           CONSTRAINT [DF_tbl_DashBoardConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME      CONSTRAINT [DF_tbl_DashBoardConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT           CONSTRAINT [DF_tbl_DashBoardConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME      CONSTRAINT [DF_tbl_DashBoardConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tbl_DashBoardConfiguration] PRIMARY KEY CLUSTERED ([Id] ASC)
);

