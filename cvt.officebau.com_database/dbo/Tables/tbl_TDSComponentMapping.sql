﻿CREATE TABLE [dbo].[tbl_TDSComponentMapping] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [TDSKey]      VARCHAR (50) NOT NULL,
    [ComponentId] INT          NOT NULL,
    [DomainId]    INT          CONSTRAINT [DF_tbl_TDSComponentMapping_DomainId] DEFAULT ((1)) NOT NULL,
    [CreatedBy]   INT          CONSTRAINT [DF_tbl_TDSComponentMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME     CONSTRAINT [DF_tbl_TDSComponentMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT          CONSTRAINT [DF_tbl_TDSComponentMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME     CONSTRAINT [DF_tbl_TDSComponentMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_tbl_TDSComponentMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

