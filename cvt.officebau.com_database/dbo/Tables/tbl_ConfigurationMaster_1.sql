﻿CREATE TABLE [dbo].[tbl_ConfigurationMaster] (
    [ID]                   INT              IDENTITY (1, 1) NOT NULL,
    [Code]                 CHAR (50)        NOT NULL,
    [Description]          VARCHAR (1000)   NOT NULL,
    [ControlType]          VARCHAR (100)    NOT NULL,
    [AssessLevel]          BIT              CONSTRAINT [DF_tbl_ConfigurationMaster_AssessLevel] DEFAULT ((0)) NOT NULL,
    [CreatedBy]            INT              CONSTRAINT [DF_tbl_ConfigurationMaster_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]            DATETIME         CONSTRAINT [DF_tbl_ConfigurationMaster_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]           INT              CONSTRAINT [DF_tbl_ConfigurationMaster_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]           DATETIME         CONSTRAINT [DF_tbl_ConfigurationMaster_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]            UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ConfigurationMaster_HistoryID] DEFAULT (newid()) NULL,
    [ConfigurationOrderID] INT              NULL,
    [Module]               VARCHAR (50)     NULL,
    CONSTRAINT [PK_tbl_ConfigurationMaster] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [UC_tbl_ConfigurationMaster_Code] UNIQUE NONCLUSTERED ([Code] ASC)
);

