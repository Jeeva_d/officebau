﻿CREATE TABLE [dbo].[tbl_UOM] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (800)    NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_UOM_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_UOM_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_UOM_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_UOM_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_UOM_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_UOM_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_UOM_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_UOM] PRIMARY KEY CLUSTERED ([ID] ASC)
);

