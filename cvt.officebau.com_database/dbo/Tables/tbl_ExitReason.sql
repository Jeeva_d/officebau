﻿CREATE TABLE [dbo].[tbl_ExitReason] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_ExitReason_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_ExitReason_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_ExitReason_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_ExitReason_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_ExitReason_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_ExitReason_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ExitReason_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_ExitReason] PRIMARY KEY CLUSTERED ([ID] ASC)
);

