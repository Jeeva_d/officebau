﻿CREATE TABLE [dbo].[Auth_RequestTrial] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [CompanyName]  VARCHAR (150)    NOT NULL,
    [EmailID]      VARCHAR (100)    NOT NULL,
    [Description]  VARCHAR (MAX)    NULL,
    [ActivatedOn]  DATETIME         NULL,
    [IsEmailSend]  BIT              CONSTRAINT [DF_Auth_RequestTrial_IsEmailSend] DEFAULT ((0)) NULL,
    [AppPassword]  VARCHAR (10)     NULL,
    [IsReject]     BIT              CONSTRAINT [DF_Auth_RequestTrial_IsReject] DEFAULT ((0)) NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_Auth_RequestTrial_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedDate]  DATETIME         CONSTRAINT [DF_Auth_RequestTrial_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_Auth_RequestTrial_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedDate] DATETIME         CONSTRAINT [DF_Auth_RequestTrial_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_Auth_RequestTrial_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [IndustryID]   INT              NULL,
    [TeamSize]     INT              NULL,
    CONSTRAINT [PK_Auth_RequestTrial] PRIMARY KEY CLUSTERED ([ID] ASC)
);

