﻿CREATE TABLE [dbo].[tbl_LoanAmortization] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [LoanRequestID]  INT              NOT NULL,
    [Amount]         MONEY            NOT NULL,
    [InterestAmount] MONEY            NOT NULL,
    [DueDate]        DATETIME         NOT NULL,
    [StatusID]       INT              NOT NULL,
    [DomainID]       INT              NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_LoanAmortization_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_LoanAmortization_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_LoanAmortization_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_LoanAmortization_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tbl_LoanAmortization_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_LoanAmortization_HistoryID] DEFAULT (newid()) NOT NULL,
    [IsBalance]      BIT              CONSTRAINT [DF_tbl_LoanAmortization_IsBalance] DEFAULT ((0)) NULL,
    [PaidAmount]     MONEY            NULL,
    [DueBalance]     MONEY            NULL,
    CONSTRAINT [PK_tbl_LoanAmortization] PRIMARY KEY CLUSTERED ([ID] ASC)
);

