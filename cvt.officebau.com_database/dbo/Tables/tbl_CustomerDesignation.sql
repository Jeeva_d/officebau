﻿CREATE TABLE [dbo].[tbl_CustomerDesignation] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_CustomerDesignation_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_CustomerDesignation_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_CustomerDesignation_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_CustomerDesignation_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_CustomerDesignation_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_CustomerDesignation_HistoryID] DEFAULT (newid()) NOT NULL,
    [Remarks]    VARCHAR (800)    NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_CustomerDesignation_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_CustomerDesignation] PRIMARY KEY CLUSTERED ([ID] ASC)
);

