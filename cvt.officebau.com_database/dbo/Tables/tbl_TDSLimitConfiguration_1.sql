﻿CREATE TABLE [dbo].[tbl_TDSLimitConfiguration] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [FYID]       INT              NOT NULL,
    [SectionID]  INT              NOT NULL,
    [Limit]      MONEY            NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_TDSLimitConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATE             CONSTRAINT [DF_tbl_TDSLimitConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_TDSLimitConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_TDSLimitConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_TDSLimitConfiguration_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_TDSLimitConfiguration_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_TDSLimitConfiguration_HistoryID] DEFAULT (newid()) NOT NULL,
    [RegionID]   INT              NULL,
    CONSTRAINT [PK_tbl_TDSLimitConfiguration] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

