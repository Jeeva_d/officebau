﻿CREATE TABLE [dbo].[tbl_LoginForms] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [LoginHistoryID] INT              NOT NULL,
    [FormName]       VARCHAR (100)    NOT NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tbl_LoginForms_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_LoginForms_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_LoginForms_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_LoginForms_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_LoginForms_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_LoginForms_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]       INT              CONSTRAINT [DF_tbl_LoginForms_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_LoginForms] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_LoginForms_tbl_LoginHistory] FOREIGN KEY ([LoginHistoryID]) REFERENCES [dbo].[tbl_LoginHistory] ([ID])
);

