﻿CREATE TABLE [dbo].[tblPurchaseOrder] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [Date]         DATETIME         NOT NULL,
    [VendorID]     INT              NOT NULL,
    [Remarks]      VARCHAR (1000)   NULL,
    [PONo]         VARCHAR (50)     NULL,
    [StatusID]     INT              NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_tblPurchaseOrder_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tblPurchaseOrder_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_tblPurchaseOrder_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tblPurchaseOrder_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tblPurchaseOrder_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]     INT              CONSTRAINT [DF_tblPurchaseOrder_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER CONSTRAINT [DF_tblPurchaseOrder_HistoryID] DEFAULT (newid()) NOT NULL,
    [Reference]    VARCHAR (1000)   NULL,
    [CostCenterID] INT              NULL,
    CONSTRAINT [PK_tblPurchaseOrder] PRIMARY KEY CLUSTERED ([ID] ASC)
);

