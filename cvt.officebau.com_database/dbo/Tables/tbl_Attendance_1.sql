﻿CREATE TABLE [dbo].[tbl_Attendance] (
    [ID]            BIGINT           IDENTITY (1, 1) NOT NULL,
    [EmployeeID]    INT              NOT NULL,
    [LogDate]       DATE             NOT NULL,
    [Duration]      TIME (7)         NULL,
    [DomainID]      INT              NOT NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Attendance_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tbl_Attendance_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tbl_Attendance_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tbl_Attendance_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tbl_Attendance_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]     BIT              CONSTRAINT [DF_tbl_Attendance_IsDeleted] DEFAULT ((0)) NOT NULL,
    [LeaveDuration] TIME (7)         NULL,
    CONSTRAINT [PK_tbl_Attendance] PRIMARY KEY CLUSTERED ([ID] ASC)
);

