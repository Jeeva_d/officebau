﻿CREATE TABLE [dbo].[tbl_EmployeeAvailedLeave] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]   INT              NOT NULL,
    [LeaveTypeID]  INT              NOT NULL,
    [TotalLeave]   DECIMAL (16, 2)  NOT NULL,
    [AvailedLeave] DECIMAL (16, 1)  NOT NULL,
    [Year]         INT              NOT NULL,
    [DomainID]     INT              NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeAvailedLeave_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_tbl_EmployeeAvailedLeave_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tbl_EmployeeAvailedLeave_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tbl_EmployeeAvailedLeave_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tbl_EmployeeAvailedLeave_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_tbl_EmployeeAvailedLeave_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_EmployeeAvailedLeave] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ tbl_EmployeeAvailedLeave_tbl_EmployeeMaster] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID])
);

