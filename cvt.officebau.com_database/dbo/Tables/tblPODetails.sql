﻿CREATE TABLE [dbo].[tblPODetails] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [POID]        INT              NOT NULL,
    [LedgerID]    INT              NOT NULL,
    [Remarks]     VARCHAR (1000)   NULL,
    [SGSTPercent] DECIMAL (18, 2)  NULL,
    [CGSTPercent] DECIMAL (18, 2)  NULL,
    [IGSTPercent] DECIMAL (18, 2)  NULL,
    [SGST]        MONEY            NULL,
    [IGST]        MONEY            NULL,
    [CGST]        MONEY            NULL,
    [Amount]      MONEY            NOT NULL,
    [Qty]         INT              CONSTRAINT [DF_tblPODetails_Qty] DEFAULT ((1)) NOT NULL,
    [BilledQTY]   INT              NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tblPODetails_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tblPODetails_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tblPODetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tblPODetails_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tblPODetails_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]    INT              CONSTRAINT [DF_tblPODetails_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tblPODetails_HistoryID] DEFAULT (newid()) NOT NULL,
    [IsLedger]    BIT              CONSTRAINT [DF_tblPODetails_IsLedger] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tblPODetails] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tblPODetails_tblPO] FOREIGN KEY ([POID]) REFERENCES [dbo].[tblPurchaseOrder] ([ID])
);

