﻿CREATE TABLE [dbo].[tbl_EmployeeTravelRequest] (
    [ID]                   INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]           INT              NOT NULL,
    [TravelTypeID]         INT              NOT NULL,
    [ModeOfTransportID]    INT              NOT NULL,
    [PurposeOfTravel]      VARCHAR (500)    NOT NULL,
    [TravelFrom]           VARCHAR (100)    NOT NULL,
    [TravelTo]             VARCHAR (100)    NOT NULL,
    [TravelDate]           DATE             NOT NULL,
    [ReturnDate]           DATE             NULL,
    [CurrencyID]           INT              NOT NULL,
    [TotalAmount]          MONEY            NOT NULL,
    [Notes]                VARCHAR (MAX)    NULL,
    [AttachmentFileID]     UNIQUEIDENTIFIER NULL,
    [StatusID]             INT              NOT NULL,
    [IsDeleted]            BIT              CONSTRAINT [DF_tbl_EmployeeTravelRequest_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]            DATETIME         CONSTRAINT [DF_tbl_EmployeeTravelRequest_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            INT              CONSTRAINT [DF_tbl_EmployeeTravelRequest_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]           DATETIME         CONSTRAINT [DF_tbl_EmployeeTravelRequest_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]           INT              CONSTRAINT [DF_tbl_EmployeeTravelRequest_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]            UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeTravelRequest_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]             INT              CONSTRAINT [DF_tbl_EmployeeTravelRequest_DomainID] DEFAULT ((1)) NOT NULL,
    [IsBillableToCustomer] BIT              NULL,
    [TravelReqNo]          VARCHAR (20)     NULL,
    CONSTRAINT [PK_tbl_EmployeeTravelRequest] PRIMARY KEY CLUSTERED ([ID] ASC)
);

