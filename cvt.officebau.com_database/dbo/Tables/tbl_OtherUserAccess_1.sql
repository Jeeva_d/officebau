﻿CREATE TABLE [dbo].[tbl_OtherUserAccess] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [ScreenName]     VARCHAR (100)    NOT NULL,
    [DesignationIds] VARCHAR (MAX)    NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_OtherUserAcess_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_OtherUserAcess_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_OtherUserAcess_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_OtherUserAcess_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryId]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_OtherUserAcess_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_OtherUserAcess] PRIMARY KEY CLUSTERED ([Id] ASC)
);

