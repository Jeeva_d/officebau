﻿CREATE TABLE [dbo].[TDSHouseTax] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]      INT              NOT NULL,
    [Component]       VARCHAR (100)    NOT NULL,
    [Type]            VARCHAR (20)     NOT NULL,
    [Declaration]     MONEY            NULL,
    [Submitted]       MONEY            NULL,
    [Remarks]         VARCHAR (8000)   NULL,
    [Cleared]         MONEY            NULL,
    [Rejected]        MONEY            NULL,
    [ApproverRemarks] VARCHAR (8000)   NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_TDSHouseTax_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_TDSHouseTax_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_TDSHouseTax_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_TDSHouseTax_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_TDSHouseTax_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]        INT              NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_TDSHouseTax_HistoryID] DEFAULT (newid()) NOT NULL,
    [FinancialYearID] INT              NULL,
    CONSTRAINT [PK_TDSHouseTax] PRIMARY KEY CLUSTERED ([ID] ASC)
);

