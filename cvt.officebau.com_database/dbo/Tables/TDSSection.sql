﻿CREATE TABLE [dbo].[TDSSection] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (100)    NOT NULL,
    [Description] VARCHAR (8000)   NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_TDSSection_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_TDSSection_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_TDSSection_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_TDSSection_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_TDSSection_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              NOT NULL,
    [CALID]       INT              NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_TDSSection_HistoryID] DEFAULT (newid()) NOT NULL,
    [ordinal]     INT              NULL,
    CONSTRAINT [PK_TDSSection] PRIMARY KEY CLUSTERED ([ID] ASC)
);

