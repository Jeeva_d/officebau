﻿CREATE TABLE [dbo].[tbl_Pay_EmployeePayroll] (
    [Id]                     INT            IDENTITY (1, 1) NOT NULL,
    [EmployeeId]             INT            NOT NULL,
    [EmployeePayStructureID] INT            NOT NULL,
    [MonthId]                INT            NOT NULL,
    [YearId]                 INT            NOT NULL,
    [ApproverId]             INT            NULL,
    [ApprovedOn]             DATETIME       CONSTRAINT [DF_tbl_Pay_EmployeePayroll_ApprovedOn] DEFAULT (getdate()) NOT NULL,
    [IsProcessed]            BIT            CONSTRAINT [DF_tbl_Pay_EmployeePayroll_IsProcessed] DEFAULT ((0)) NOT NULL,
    [IsApproved]             BIT            CONSTRAINT [DF_tbl_Pay_EmployeePayroll_IsApproved] DEFAULT ((0)) NOT NULL,
    [IsDeleted]              BIT            CONSTRAINT [DF_tbl_Pay_EmployeePayroll_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]              INT            CONSTRAINT [DF_tbl_Pay_EmployeePayroll_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]              DATETIME       CONSTRAINT [DF_tbl_Pay_EmployeePayroll_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]             INT            CONSTRAINT [DF_tbl_Pay_EmployeePayroll_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]             DATETIME       CONSTRAINT [DF_tbl_Pay_EmployeePayroll_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]               INT            CONSTRAINT [DF_tbl_Pay_EmployeePayroll_DomainId] DEFAULT ((1)) NOT NULL,
    [Remarks]                VARCHAR (1000) NULL,
    CONSTRAINT [PK_tbl_Pay_EmployeePayroll] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tbl_Pay_EmployeePayroll_tbl_Pay_EmployeePayStructure] FOREIGN KEY ([EmployeePayStructureID]) REFERENCES [dbo].[tbl_Pay_EmployeePayStructure] ([ID])
);

