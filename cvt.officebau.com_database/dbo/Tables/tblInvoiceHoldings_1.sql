﻿CREATE TABLE [dbo].[tblInvoiceHoldings] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [InvoiceId]  INT              NOT NULL,
    [Date]       DATE             NOT NULL,
    [LedgerId]   INT              NOT NULL,
    [Amount]     MONEY            NOT NULL,
    [Isdeleted]  BIT              CONSTRAINT [DF_tblInvoiceHoldings_Isdeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tblInvoiceHoldings_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tblInvoiceHoldings_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tblInvoiceHoldings_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tblInvoiceHoldings_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tblInvoiceHoldings_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tblInvoiceHoldings_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblInvoiceHoldings] PRIMARY KEY CLUSTERED ([ID] ASC)
);

