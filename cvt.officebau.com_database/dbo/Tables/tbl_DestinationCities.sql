﻿CREATE TABLE [dbo].[tbl_DestinationCities] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (800)    NULL,
    [IsMetro]    BIT              CONSTRAINT [DF_tbl_DestinationCities_IsMetro] DEFAULT ((0)) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_DestinationCities_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_DestinationCities_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_DestinationCities_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_DestinationCities_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_DestinationCities_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_DestinationCities_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_DestinationCities_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_DestinationCities] PRIMARY KEY CLUSTERED ([ID] ASC)
);

