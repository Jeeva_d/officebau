﻿CREATE TABLE [dbo].[tbl_LoginHistory] (
    [ID]                INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]        INT              NOT NULL,
    [IPAddress]         VARCHAR (20)     NULL,
    [LoginTime]         DATETIME         NULL,
    [IsActive]          BIT              CONSTRAINT [DF_tbl_LoginHistory_IsActive] DEFAULT ((0)) NULL,
    [PhysicalAddress]   VARCHAR (100)    NULL,
    [BrowserType]       VARCHAR (50)     NULL,
    [Device]            VARCHAR (50)     NULL,
    [LogOutTime]        DATETIME         NULL,
    [LogOutType]        VARCHAR (50)     NULL,
    [CreatedOn]         DATETIME         CONSTRAINT [DF_tbl_LoginHistory_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]          INT              CONSTRAINT [DF_tbl_LoginHistory_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]         UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_LoginHistory_HistoryID] DEFAULT (newid()) NOT NULL,
    [Location]          VARCHAR (500)    NULL,
    [LatitudeLongitude] VARCHAR (100)    NULL,
    [ClientHostName]    VARCHAR (100)    NULL,
    CONSTRAINT [PK_tbl_LoginHistory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

