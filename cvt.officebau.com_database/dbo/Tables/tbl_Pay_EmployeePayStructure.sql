﻿CREATE TABLE [dbo].[tbl_Pay_EmployeePayStructure] (
    [ID]                    INT              IDENTITY (1, 1) NOT NULL,
    [CompanyPayStructureID] INT              NOT NULL,
    [EmployeeID]            INT              NOT NULL,
    [EffectiveFrom]         DATETIME         NOT NULL,
    [IsDeleted]             BIT              CONSTRAINT [DF_tbl_Pay_EmployeePayStructure_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]              INT              CONSTRAINT [DF_tbl_Pay_EmployeePayStructure_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]             INT              CONSTRAINT [DF_tbl_Pay_EmployeePayStructure_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]             DATETIME         CONSTRAINT [DF_tbl_Pay_EmployeePayStructure_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            INT              CONSTRAINT [DF_tbl_Pay_EmployeePayStructure_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]            DATETIME         CONSTRAINT [DF_tbl_Pay_EmployeePayStructure_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]             UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Pay_EmployeePayStructure_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK__tbl_Pay___3214EC272B1085DD] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_Pay_EmployeePayStructure_tbl_Pay_CompanyPayStructure] FOREIGN KEY ([CompanyPayStructureID]) REFERENCES [dbo].[tbl_Pay_CompanyPayStructure] ([ID])
);

