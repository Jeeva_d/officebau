﻿CREATE TABLE [dbo].[tblInvoicePaymentMapping] (
    [ID]               INT              IDENTITY (1, 1) NOT NULL,
    [InvoicePaymentID] INT              NOT NULL,
    [InvoiceID]        INT              NOT NULL,
    [Amount]           MONEY            NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_tblInvoicePaymentMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_tblInvoicePaymentMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        INT              CONSTRAINT [DF_tblInvoicePaymentMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_tblInvoicePaymentMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       INT              CONSTRAINT [DF_tblInvoicePaymentMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]         INT              CONSTRAINT [DF_tblInvoicePaymentMapping_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tblInvoicePaymentMapping_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblInvoicePaymentMapping] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tblInvoicePaymentMapping_tblInvoice] FOREIGN KEY ([InvoiceID]) REFERENCES [dbo].[tblInvoice] ([ID]),
    CONSTRAINT [FK_tblInvoicePaymentMapping_tblInvoicePayment] FOREIGN KEY ([InvoicePaymentID]) REFERENCES [dbo].[tblInvoicePayment] ([ID])
);

