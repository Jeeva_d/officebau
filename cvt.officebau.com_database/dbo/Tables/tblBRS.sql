﻿CREATE TABLE [dbo].[tblBRS] (
    [ID]               INT              IDENTITY (1, 1) NOT NULL,
    [BankID]           INT              NULL,
    [SourceID]         INT              NULL,
    [SourceDate]       DATETIME         NULL,
    [SourceType]       INT              NULL,
    [IsReconsiled]     BIT              CONSTRAINT [DF_tblBRS_IsReconsiled] DEFAULT ((0)) NULL,
    [BRSDescription]   VARCHAR (1000)   NULL,
    [Amount]           MONEY            NULL,
    [ReconsiledDate]   DATETIME         NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_tblBRS_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_tblBRS_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        INT              CONSTRAINT [DF_tblBRS_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_tblBRS_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       INT              CONSTRAINT [DF_tblBRS_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]         INT              CONSTRAINT [DF_tblBRS_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tblBRS_HistoryID] DEFAULT (newid()) NOT NULL,
    [AvailableBalance] MONEY            NULL,
    CONSTRAINT [PK_tblBRS] PRIMARY KEY CLUSTERED ([ID] ASC)
);

