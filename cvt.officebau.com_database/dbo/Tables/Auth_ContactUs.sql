﻿CREATE TABLE [dbo].[Auth_ContactUs] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [FirstName]   VARCHAR (50)     NOT NULL,
    [LastName]    VARCHAR (50)     NOT NULL,
    [Email]       VARCHAR (50)     NOT NULL,
    [Phone]       VARCHAR (15)     NULL,
    [CompanyName] VARCHAR (150)    NULL,
    [Message]     VARCHAR (MAX)    NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_Auth_ContactUs_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_Auth_ContactUs_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_Auth_ContactUs_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_Auth_ContactUs_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_Auth_ContactUs_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    CONSTRAINT [PK_Auth_ContactUs] PRIMARY KEY CLUSTERED ([ID] ASC)
);

