﻿CREATE TABLE [dbo].[tbl_InventoryStockAdjustment] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [LedgerID]     INT              NOT NULL,
    [AdjustedDate] DATETIME         NOT NULL,
    [ReasonID]     INT              NOT NULL,
    [Remarks]      VARCHAR (1000)   NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_tbl_InventoryStockAdjustment_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]     INT              CONSTRAINT [DF_tbl_InventoryStockAdjustment_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_tbl_InventoryStockAdjustment_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tbl_InventoryStockAdjustment_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tbl_InventoryStockAdjustment_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tbl_InventoryStockAdjustment_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_InventoryStockAdjustment_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_InventoryStockAdjustment] PRIMARY KEY CLUSTERED ([ID] ASC)
);

