﻿CREATE TABLE [dbo].[tbl_EmployeeAppraisal] (
    [ID]                       INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]               INT              NOT NULL,
    [AppraisalConfigurationID] INT              NOT NULL,
    [AppraiserID]              INT              NOT NULL,
    [SubmittedOn]              DATETIME         NULL,
    [AppraisedOn]              DATETIME         NULL,
    [IsDeleted]                BIT              CONSTRAINT [DF_tbl_EmployeeAppraisal_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]                DATETIME         CONSTRAINT [DF_tbl_EmployeeAppraisal_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                INT              CONSTRAINT [DF_tbl_EmployeeAppraisal_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]               DATETIME         CONSTRAINT [DF_tbl_EmployeeAppraisal_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]               INT              CONSTRAINT [DF_tbl_EmployeeAppraisal_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]                UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeAppraisal_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]                 INT              CONSTRAINT [DF_tbl_EmployeeAppraisal_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_EmployeeAppraisal] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_EmployeeAppraisal_tbl_AppraisalConfiguration] FOREIGN KEY ([AppraisalConfigurationID]) REFERENCES [dbo].[tbl_AppraisalConfiguration] ([ID])
);

