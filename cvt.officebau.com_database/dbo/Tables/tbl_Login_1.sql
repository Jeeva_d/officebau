﻿CREATE TABLE [dbo].[tbl_Login] (
    [ID]                       INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]               INT              NOT NULL,
    [Password]                 VARCHAR (500)    NOT NULL,
    [RecentPasswordChangeDate] DATETIME         CONSTRAINT [DF_tbl_Login_RecentPasswordChangeDate] DEFAULT (getdate()) NULL,
    [CreatedOn]                DATETIME         CONSTRAINT [DF_tbl_Login_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                INT              CONSTRAINT [DF_tbl_Login_CreatedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]                UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Login_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]                 INT              CONSTRAINT [DF_tbl_Login_DomainID] DEFAULT ((1)) NOT NULL,
    [IsChanged]                BIT              CONSTRAINT [tbl_Login_IsChanged] DEFAULT ((0)) NOT NULL,
    [DeviceInfo]               VARCHAR (MAX)    NULL,
    [PIN]                      INT              NULL,
    CONSTRAINT [PK_tbl_Login] PRIMARY KEY CLUSTERED ([ID] ASC)
);

