﻿CREATE TABLE [dbo].[tblInvoicePayment] (
    [ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [PaymentDate]        DATETIME         NOT NULL,
    [PaymentModeID]      INT              NOT NULL,
    [BankID]             INT              NOT NULL,
    [Reference]          VARCHAR (1000)   NULL,
    [LedgerID]           INT              NULL,
    [PaymentDescription] VARCHAR (1000)   NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_tblInvoicePayment_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]          DATETIME         CONSTRAINT [DF_tblInvoicePayment_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          INT              CONSTRAINT [DF_tblInvoicePayment_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]         DATETIME         CONSTRAINT [DF_tblInvoicePayment_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]         INT              CONSTRAINT [DF_tblInvoicePayment_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]           INT              CONSTRAINT [DF_tblInvoicePayment_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]          UNIQUEIDENTIFIER CONSTRAINT [DF_tblInvoicePayment_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblInvoicePayment] PRIMARY KEY CLUSTERED ([ID] ASC)
);

