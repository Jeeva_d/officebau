﻿CREATE TABLE [dbo].[tbl_AssetType] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_AssetType_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_AssetType_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_AssetType_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_AssetType_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_AssetType_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_AssetType_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_AssetType_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_AssetType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

