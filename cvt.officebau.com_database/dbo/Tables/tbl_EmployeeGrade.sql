﻿CREATE TABLE [dbo].[tbl_EmployeeGrade] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NULL,
    [Remarks]    VARCHAR (100)    NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_EmployeeGrade_DomainID] DEFAULT ((1)) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_EmployeeGrade_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_EmployeeGrade_CreatedOn] DEFAULT (getdate()) NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_EmployeeGrade_CreatedBy] DEFAULT ((1)) NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_EmployeeGrade_ModifiedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_EmployeeGrade_ModifiedBy] DEFAULT ((1)) NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeGrade_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_EmployeeGrade] PRIMARY KEY CLUSTERED ([ID] ASC)
);

