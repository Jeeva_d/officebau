﻿CREATE TABLE [dbo].[tbl_Pay_EmployeePayrollDetails] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [PayrollId]   INT              NOT NULL,
    [ComponentId] INT              NOT NULL,
    [Amount]      DECIMAL (18, 2)  NULL,
    [Isdeleted]   BIT              CONSTRAINT [DF_tbl_Pay_EmployeePayrollDetails_Isdeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Pay_EmployeePayrollDetails_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Pay_EmployeePayrollDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Pay_EmployeePayrollDetails_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Pay_EmployeePayrollDetails_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryId]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Pay_EmployeePayrollDetails_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_Pay_EmployeePayrollDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tbl_Pay_EmployeePayrollDetails_tbl_Pay_EmployeePayroll] FOREIGN KEY ([PayrollId]) REFERENCES [dbo].[tbl_Pay_EmployeePayroll] ([Id]),
    CONSTRAINT [FK_tbl_Pay_EmployeePayrollDetails_tbl_Pay_PayrollCompontents] FOREIGN KEY ([ComponentId]) REFERENCES [dbo].[tbl_Pay_PayrollCompontents] ([ID])
);

