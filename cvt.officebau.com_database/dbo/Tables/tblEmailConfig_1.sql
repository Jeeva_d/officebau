﻿CREATE TABLE [dbo].[tblEmailConfig] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [Key]          VARCHAR (20)  CONSTRAINT [DF_tblEmailConfig_Key] DEFAULT ('ExpEmail') NULL,
    [Toid]         VARCHAR (MAX) NULL,
    [BCC]          VARCHAR (MAX) NULL,
    [CC]           VARCHAR (MAX) NULL,
    [DomainID]     INT           CONSTRAINT [DF_tblEmailConfig_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]    INT           CONSTRAINT [DF_tblEmailConfig_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME      CONSTRAINT [DF_tblEmailConfig_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT           CONSTRAINT [DF_tblEmailConfig_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME      CONSTRAINT [DF_tblEmailConfig_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsAttachment] BIT           CONSTRAINT [DF_tblEmailConfig_IsAttachment] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tblEmailConfig] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UK_tblEmailConfig] UNIQUE NONCLUSTERED ([Key] ASC, [DomainID] ASC)
);

