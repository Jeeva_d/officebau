﻿CREATE TABLE [dbo].[tbl_EmployeeBilling] (
    [Id]                     INT             IDENTITY (1, 1) NOT NULL,
    [EmployeeId]             INT             NOT NULL,
    [CustomerId]             INT             NOT NULL,
    [Fixed]                  BIT             NOT NULL,
    [HourlyRate]             MONEY           NULL,
    [FixedRate]              MONEY           NULL,
    [IsActive]               BIT             CONSTRAINT [tbl_EmployeeBilling_IsActive] DEFAULT ((0)) NOT NULL,
    [BillingPercentage]      DECIMAL (16, 4) NULL,
    [RemunerationPercentage] DECIMAL (16, 4) NULL,
    [Isdeleted]              BIT             CONSTRAINT [tbl_EmployeeBilling_Isdeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]              INT             CONSTRAINT [tbl_EmployeeBilling_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]              DATETIME        CONSTRAINT [tbl_EmployeeBilling_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]             INT             CONSTRAINT [tbl_EmployeeBilling_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]             DATETIME        CONSTRAINT [tbl_EmployeeBilling_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]               INT             NOT NULL,
    [Description]            NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK__tbl_EmployeeBilling] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tbl_EmployeeBilling_tbl_Company] FOREIGN KEY ([DomainId]) REFERENCES [dbo].[tbl_Company] ([ID]),
    CONSTRAINT [FK_tbl_EmployeeBilling_tbl_EmployeeMaster] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID]),
    CONSTRAINT [FK_tbl_EmployeeBilling_tblCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[tblCustomer] ([ID])
);

