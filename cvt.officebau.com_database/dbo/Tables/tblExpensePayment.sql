﻿CREATE TABLE [dbo].[tblExpensePayment] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [Date]          DATETIME         NOT NULL,
    [PaymentModeID] INT              NOT NULL,
    [BankID]        INT              NOT NULL,
    [Reference]     VARCHAR (1000)   NULL,
    [IsDeleted]     BIT              CONSTRAINT [DF_tblExpensePayment_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tblExpensePayment_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tblExpensePayment_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tblExpensePayment_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tblExpensePayment_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]      INT              CONSTRAINT [DF_tblExpensePayment_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tblExpensePayment_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblExpensePayment] PRIMARY KEY CLUSTERED ([ID] ASC)
);

