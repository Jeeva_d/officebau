﻿CREATE TABLE [dbo].[tbl_MasterMenuforDomain] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [Name]           VARCHAR (100)    NOT NULL,
    [OrderID]        INT              NULL,
    [Remarks]        VARCHAR (1000)   NULL,
    [MenuParameters] VARCHAR (200)    NULL,
    [RootConfig]     VARCHAR (20)     NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_MasterMenuforDomain_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_MasterMenuforDomain_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_MasterMenuforDomain_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_MasterMenuforDomain_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_MasterMenuforDomain_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_MasterMenuforDomain] PRIMARY KEY CLUSTERED ([ID] ASC)
);

