﻿CREATE TABLE [dbo].[tbl_RolesResponsibility] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (8000)   NULL,
    [Description] VARCHAR (8000)   NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_RolesResponsibility_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_RolesResponsibility_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_RolesResponsibility_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_RolesResponsibility_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_RolesResponsibility_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_RolesResponsibility_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]    INT              CONSTRAINT [DF_tbl_RolesResponsibility_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_RolesResponsibility] PRIMARY KEY CLUSTERED ([ID] ASC)
);

