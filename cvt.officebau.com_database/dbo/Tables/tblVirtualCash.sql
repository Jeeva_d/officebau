﻿CREATE TABLE [dbo].[tblVirtualCash] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [SourceID]   INT              NOT NULL,
    [Amount]     MONEY            NOT NULL,
    [Date]       DATETIME         CONSTRAINT [DF_tblVirtualCash_Date] DEFAULT (getdate()) NOT NULL,
    [SourceType] INT              NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tblVirtualCash_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tblVirtualCash_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tblVirtualCash_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tblVirtualCash_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tblVirtualCash_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tblVirtualCash_DomainID] DEFAULT ((1)) NOT NULL,
    [CALID]      INT              CONSTRAINT [DF_tblVirtualCash_CALID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tblVirtualCash_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblVirtualCash] PRIMARY KEY CLUSTERED ([ID] ASC)
);

