﻿CREATE TABLE [dbo].[tbl_ClaimsSettle] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [ApproverID]    INT              NOT NULL,
    [SettlerAmount] MONEY            NULL,
    [Remarks]       VARCHAR (8000)   NULL,
    [PaymentTypeID] INT              NULL,
    [ClaimItemID]   INT              NULL,
    [StatusID]      INT              NULL,
    [IsDeleted]     BIT              CONSTRAINT [DF_tbl_ClaimsSettle_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tbl_ClaimsSettle_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tbl_ClaimsSettle_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tbl_ClaimsSettle_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tbl_ClaimsSettle_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]      INT              CONSTRAINT [DF_tbl_ClaimsSettle_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ClaimsSettle_HistoryID] DEFAULT (newid()) NOT NULL,
    [PaymentDate]   DATETIME         NULL,
    CONSTRAINT [PK_tbl_ClaimsSettle] PRIMARY KEY CLUSTERED ([ID] ASC)
);

