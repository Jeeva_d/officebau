﻿CREATE TABLE [dbo].[tblBank] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [Name]           VARCHAR (100)    NOT NULL,
    [AccountNo]      VARCHAR (100)    NOT NULL,
    [AccountTypeID]  INT              NULL,
    [OpeningBalance] MONEY            NULL,
    [OpeningBalDate] DATETIME         NULL,
    [Description]    VARCHAR (1000)   NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tblBank_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tblBank_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tblBank_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tblBank_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tblBank_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]       INT              CONSTRAINT [DF_tblBank_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tblBank_HistoryID] DEFAULT (newid()) NOT NULL,
    [Limit]          MONEY            NULL,
    [DisplayName]    VARCHAR (100)    NULL,
    [BranchName]     VARCHAR (100)    NULL,
    [IFSCCode]       VARCHAR (100)    NULL,
    [SWIFTCode]      VARCHAR (100)    NULL,
    [MICRCode]       VARCHAR (100)    NULL,
    CONSTRAINT [PK_tblBank] PRIMARY KEY CLUSTERED ([ID] ASC)
);

