﻿CREATE TABLE [dbo].[tblCurrency] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (100)    NULL,
    [Code]         VARCHAR (100)    NULL,
    [Remarks ]     VARCHAR (1000)   NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_tblCurrency_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tblCurrency_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_tblCurrency_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tblCurrency_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tblCurrency_ModifedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER CONSTRAINT [DF_tblCurrency_HistoryID] DEFAULT (newid()) NOT NULL,
    [CurrencyCode] VARCHAR (50)     NULL,
    CONSTRAINT [PK_tblCurrency] PRIMARY KEY CLUSTERED ([ID] ASC)
);

