﻿CREATE TABLE [dbo].[tbl_Pay_PaystubConfiguration] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [ComponentID] INT              NOT NULL,
    [Type]        VARCHAR (200)    NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_Pay_PaystubConfiguration_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Pay_PaystubConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Pay_PaystubConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Pay_PaystubConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Pay_PaystubConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Pay_PaystubConfiguration_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_Pay_PaystubConfiguration] PRIMARY KEY CLUSTERED ([ID] ASC)
);

