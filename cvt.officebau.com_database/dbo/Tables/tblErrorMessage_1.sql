﻿CREATE TABLE [dbo].[tblErrorMessage] (
    [Code]        VARCHAR (25)     NOT NULL,
    [Type]        VARCHAR (25)     NOT NULL,
    [Message]     VARCHAR (100)    NOT NULL,
    [Description] VARCHAR (200)    NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tblErrorMessage_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tblErrorMessage_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   INT              NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tblErrorMessage_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tblErrorMessage_HistoryID] DEFAULT (newid()) NULL,
    PRIMARY KEY CLUSTERED ([Code] ASC)
);

