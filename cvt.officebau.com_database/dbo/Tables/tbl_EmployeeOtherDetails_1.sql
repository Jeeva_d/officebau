﻿CREATE TABLE [dbo].[tbl_EmployeeOtherDetails] (
    [ID]                        INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]                INT              NOT NULL,
    [IsClaimRequest]            BIT              NOT NULL,
    [IsDeleted]                 BIT              CONSTRAINT [DF_tbl_EmployeeOtherDetails_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]                 DATETIME         CONSTRAINT [DF_tbl_EmployeeOtherDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                 INT              CONSTRAINT [DF_tbl_EmployeeOtherDetails_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]                DATETIME         CONSTRAINT [DF_tbl_EmployeeOtherDetails_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]                INT              CONSTRAINT [DF_tbl_EmployeeOtherDetails_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]                 UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeOtherDetails_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]                  INT              CONSTRAINT [DF_tbl_EmployeeOtherDetails_DomainID] DEFAULT ((1)) NOT NULL,
    [ApplicationRoleID]         INT              NULL,
    [StartMenuURL]              VARCHAR (100)    NULL,
    [IsBiometricAccessRequired] BIT              NULL,
    [IsSuperUser]               BIT              NULL,
    [IPAddress]                 VARCHAR (50)     NULL,
    CONSTRAINT [PK_tbl_EmployeeOtherDetails] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_EmployeeOtherDetails_tbl_EmployeeMaster] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID])
);

