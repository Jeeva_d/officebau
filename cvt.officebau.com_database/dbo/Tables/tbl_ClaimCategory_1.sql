﻿CREATE TABLE [dbo].[tbl_ClaimCategory] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_ClaimCategory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_ClaimCategory_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_ClaimCategory_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_ClaimCategory_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_ClaimCategory_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ClaimCategory_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_ClaimCategory_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_ClaimCategory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

