﻿CREATE TABLE [dbo].[tbl_Status] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (100)    NOT NULL,
    [Description] VARCHAR (8000)   NOT NULL,
    [Type]        VARCHAR (100)    NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Status_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Status_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Status_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Status_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Status_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_Status_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_Status] PRIMARY KEY CLUSTERED ([ID] ASC)
);

