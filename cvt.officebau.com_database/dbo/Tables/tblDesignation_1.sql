﻿CREATE TABLE [dbo].[tblDesignation] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [DepartmentID] INT              NULL,
    [Name]         VARCHAR (100)    NOT NULL,
    [Remarks]      VARCHAR (1000)   NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_tblDesignation_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tblDesignation_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_tblDesignation_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tblDesignation_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tblDesignation_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]     INT              CONSTRAINT [DF_tblDesignation_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER CONSTRAINT [DF_tblDesignation_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblDesignation] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tblDesignation_tblDepartment] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[tblDepartment] ([ID])
);

