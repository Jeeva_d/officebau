﻿CREATE TABLE [dbo].[tbl_LopDetails_Process] (
    [Id]         INT      IDENTITY (1, 1) NOT NULL,
    [EmployeeId] INT      NOT NULL,
    [MonthID]    INT      NOT NULL,
    [Year]       INT      NOT NULL,
    [Absent]     INT      NULL,
    [Present]    INT      NULL,
    [Halfday]    INT      NULL,
    [CreatedBy]  INT      CONSTRAINT [DF_tbl_LopDetails_Process_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME CONSTRAINT [DF_tbl_LopDetails_Process_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT      CONSTRAINT [DF_tbl_LopDetails_Process_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME CONSTRAINT [DF_tbl_LopDetails_Process_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]   INT      CONSTRAINT [DF_tbl_LopDetails_Process_DomainId] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_LopDetails_Process] PRIMARY KEY CLUSTERED ([Id] ASC)
);

