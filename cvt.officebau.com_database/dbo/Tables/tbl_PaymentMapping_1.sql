﻿CREATE TABLE [dbo].[tbl_PaymentMapping] (
    [Id]         INT              IDENTITY (1, 1) NOT NULL,
    [PaymentId]  INT              NOT NULL,
    [RefId]      INT              NOT NULL,
    [Amount]     MONEY            NOT NULL,
    [Isdeleted]  BIT              CONSTRAINT [DF_tbl_PaymentMapping_Isdeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_PaymentMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_PaymentMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_PaymentMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_PaymentMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]   INT              NOT NULL,
    [HistoryId]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_PaymentMapping_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_PaymentMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

