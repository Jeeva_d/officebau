﻿CREATE TABLE [dbo].[tbl_InternalMeeting_Task] (
    [Id]           INT              IDENTITY (1, 1) NOT NULL,
    [IMId]         INT              NOT NULL,
    [AssignedToId] INT              NOT NULL,
    [DueDate]      DATE             NOT NULL,
    [StatusId]     INT              NOT NULL,
    [Description]  NVARCHAR (MAX)   NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_tbl_InternalMeeting_Task_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tbl_InternalMeeting_Task_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tbl_InternalMeeting_Task_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tbl_InternalMeeting_Task_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]     INT              CONSTRAINT [DF_tbl_InternalMeeting_Task_DomainId] DEFAULT ((1)) NOT NULL,
    [HistoryId]    UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_InternalMeeting_Task_HistoryId] DEFAULT (newid()) NOT NULL,
    [IsDeleted]    BIT              NULL,
    CONSTRAINT [PK_tbl_InternalMeeting_Task] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tbl_InternalMeeting_Task_Modified_By] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID]),
    CONSTRAINT [FK_tbl_InternalMeeting_Task_tbl_EmployeeMaster] FOREIGN KEY ([AssignedToId]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID]),
    CONSTRAINT [FK_tbl_InternalMeeting_Task_tbl_InternalMeeting] FOREIGN KEY ([IMId]) REFERENCES [dbo].[tbl_InternalMeeting] ([Id]),
    CONSTRAINT [FK_tbl_InternalMeeting_Task_tbl_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[tbl_Status] ([ID])
);

