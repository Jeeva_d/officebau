﻿CREATE TABLE [dbo].[tbl_TDSConfig] (
    [ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [Values]             VARCHAR (100)    NOT NULL,
    [PayrollComponentID] INT              NOT NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_tbl_TDSConfig_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]           INT              CONSTRAINT [DF_tbl_TDSConfig_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]          INT              CONSTRAINT [DF_tbl_TDSConfig_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]          DATETIME         CONSTRAINT [DF_tbl_TDSConfig_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]         INT              CONSTRAINT [DF_tbl_TDSConfig_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]         DATETIME         CONSTRAINT [DF_tbl_TDSConfig_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]          UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_TDSConfig_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_TDSConfig] PRIMARY KEY CLUSTERED ([ID] ASC)
);

