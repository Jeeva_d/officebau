﻿CREATE TABLE [dbo].[tblJournal] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [JournalNo]       VARCHAR (100)    NULL,
    [JournalDate]     DATETIME         NOT NULL,
    [Debit]           MONEY            NULL,
    [Credit]          MONEY            NULL,
    [Description]     VARCHAR (MAX)    NULL,
    [AutoJournalNo]   VARCHAR (100)    NULL,
    [LedgerProductID] INT              NOT NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tblJournal_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tblJournal_CreatedOn] DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tblJournal_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tblJournal_ModifiedOn] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tblJournal_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]        INT              CONSTRAINT [DF_tblJournal_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tblJournal_HistoryID] DEFAULT (newid()) NOT NULL,
    [Type]            VARCHAR (20)     NULL,
    CONSTRAINT [PK_tblJournal] PRIMARY KEY CLUSTERED ([ID] ASC)
);

