﻿CREATE TABLE [dbo].[tblBusinessEventsLog] (
    [GUID]        UNIQUEIDENTIFIER NOT NULL,
    [ColumnName]  VARCHAR (200)    NOT NULL,
    [ColumnValue] VARCHAR (MAX)    CONSTRAINT [DF_HistoricalBusinessDataLogs_ColumnValue] DEFAULT ('') NOT NULL,
    [ModifiedBy]  INT              NOT NULL,
    [ModifiedOn]  DATETIME         NOT NULL,
    [ChangeType]  VARCHAR (100)    NOT NULL,
    [IsLatest]    BIT              NOT NULL
);

