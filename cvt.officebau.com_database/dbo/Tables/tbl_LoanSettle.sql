﻿CREATE TABLE [dbo].[tbl_LoanSettle] (
    [ID]                INT              IDENTITY (1, 1) NOT NULL,
    [LoanRequestID]     INT              NOT NULL,
    [SettlerID]         INT              NOT NULL,
    [Amount]            MONEY            NOT NULL,
    [DateOfRealization] DATETIME         NOT NULL,
    [PaymentModeID]     INT              NOT NULL,
    [ReferenceNo]       VARCHAR (50)     NULL,
    [Description]       VARCHAR (500)    NULL,
    [DomainID]          INT              NOT NULL,
    [CreatedBy]         INT              CONSTRAINT [DF_tbl_LoanSettle_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]         DATETIME         CONSTRAINT [DF_tbl_LoanSettle_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]        INT              CONSTRAINT [DF_tbl_LoanSettle_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]        DATETIME         CONSTRAINT [DF_tbl_LoanSettle_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_tbl_LoanSettle_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]         UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_LoanSettle_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_LoanSettle] PRIMARY KEY CLUSTERED ([ID] ASC)
);

