﻿CREATE TABLE [dbo].[tbl_CarryForwardLeave] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]     INT              NOT NULL,
    [LeaveTypeID]    INT              NOT NULL,
    [TotalLeave]     DECIMAL (16, 1)  NOT NULL,
    [Year]           INT              NOT NULL,
    [IsCarryForward] BIT              CONSTRAINT [DF_tbl_CarryForwardLeave_IsCarryForward] DEFAULT ((0)) NOT NULL,
    [IsEncashment]   BIT              CONSTRAINT [DF_tbl_CarryForwardLeave_IsEncashment] DEFAULT ((0)) NOT NULL,
    [DomainID]       INT              NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_CarryForwardLeave_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_CarryForwardLeave_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_CarryForwardLeave_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_CarryForwardLeave_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_CarryForwardLeave_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tbl_CarryForwardLeave_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_CarryForwardLeave] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ tbl_CarryForwardLeave_tbl_EmployeeMaster] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID])
);

