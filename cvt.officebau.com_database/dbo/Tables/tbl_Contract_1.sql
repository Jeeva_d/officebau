﻿CREATE TABLE [dbo].[tbl_Contract] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (200)    NOT NULL,
    [EmailID]     VARCHAR (50)     NULL,
    [ContactNo1]  VARCHAR (20)     NULL,
    [ContactNo2]  VARCHAR (20)     NULL,
    [Address1]    VARCHAR (500)    NULL,
    [Address2]    VARCHAR (500)    NULL,
    [Description] VARCHAR (8000)   NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_Contract_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Contract_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Contract_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Contract_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Contract_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Contract_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]    INT              CONSTRAINT [DF_tbl_Contract_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_Contract] PRIMARY KEY CLUSTERED ([ID] ASC)
);

