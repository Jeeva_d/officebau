﻿CREATE TABLE [dbo].[tbl_Questionnaire] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Question]   VARCHAR (1000)   NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_Questionnaire_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_Questionnaire_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_Questionnaire_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_Questionnaire_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_Questionnaire_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Questionnaire_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_Questionnaire_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_Questionnaire] PRIMARY KEY CLUSTERED ([ID] ASC)
);

