﻿CREATE TABLE [dbo].[tbl_Month] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (100)    NULL,
    [Description] VARCHAR (8000)   NULL,
    [MapID]       TINYINT          NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Month_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Month_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Month_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Month_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_Month_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Month_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_Month] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

