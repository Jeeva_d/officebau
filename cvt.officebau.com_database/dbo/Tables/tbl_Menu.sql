﻿CREATE TABLE [dbo].[tbl_Menu] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [MenuCode]    VARCHAR (50)     NOT NULL,
    [Menu]        VARCHAR (100)    NOT NULL,
    [Description] VARCHAR (8000)   NULL,
    [MenuURL]     VARCHAR (500)    NOT NULL,
    [SubModuleID] INT              NOT NULL,
    [MenuOrderID] INT              NULL,
    [RootConfig]  VARCHAR (20)     NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Menu_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Menu_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Menu_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Menu_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Menu_HistoryID] DEFAULT (newid()) NULL,
    [IsSubmenu]   BIT              CONSTRAINT [DF_tbl_Menu_IsSubmenu] DEFAULT ((0)) NULL,
    [MenuIcon]    VARCHAR (50)     NULL,
    [IsMobility]  BIT              NULL,
    CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    CONSTRAINT [UNIQUE_MenuCode] UNIQUE NONCLUSTERED ([MenuCode] ASC)
);

