﻿CREATE TABLE [dbo].[tblBudget] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [BudgetId]   INT           NOT NULL,
    [Type]       VARCHAR (10)  NOT NULL,
    [TagName]    VARCHAR (100) NULL,
    [Amount]     MONEY         NOT NULL,
    [FyId]       INT           NOT NULL,
    [Freeze]     BIT           CONSTRAINT [DF_tblBudget_Freeze] DEFAULT ((0)) NOT NULL,
    [IsDeleted]  BIT           CONSTRAINT [DF_tblBudget_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]  INT           CONSTRAINT [DF_tblBudget_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME      CONSTRAINT [DF_tblBudget_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT           CONSTRAINT [DF_tblBudget_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME      CONSTRAINT [DF_tblBudget_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]   INT           CONSTRAINT [DF_tblBudget_DomainId] DEFAULT ((1)) NOT NULL,
    [BudgetType] VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK_tblBudget] PRIMARY KEY CLUSTERED ([Id] ASC)
);

