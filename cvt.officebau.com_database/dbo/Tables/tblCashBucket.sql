﻿CREATE TABLE [dbo].[tblCashBucket] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [AvailableCash] MONEY            CONSTRAINT [DF_tblCashBucket_AvailableCash] DEFAULT ((0)) NOT NULL,
    [DomainID]      INT              CONSTRAINT [DF_tblCashBucket_DomainID] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tblCashBucket_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tblCashBucket_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tblCashBucket_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblCashBucket] PRIMARY KEY CLUSTERED ([ID] ASC)
);

