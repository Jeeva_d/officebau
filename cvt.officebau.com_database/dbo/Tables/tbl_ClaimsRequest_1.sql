﻿CREATE TABLE [dbo].[tbl_ClaimsRequest] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [ClaimDate]     DATETIME         NOT NULL,
    [Amount]        MONEY            NOT NULL,
    [Remarks]       VARCHAR (8000)   NULL,
    [CategoryID]    INT              NOT NULL,
    [BillNo]        VARCHAR (50)     NULL,
    [StatusID]      INT              NOT NULL,
    [SubmittedDate] DATETIME         NULL,
    [ApproverID]    INT              NOT NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tbl_ClaimsRequest_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tbl_ClaimsRequest_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tbl_ClaimsRequest_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tbl_ClaimsRequest_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]     BIT              CONSTRAINT [DF_tbl_ClaimsRequest_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]      INT              NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ClaimsRequest_HistoryID] DEFAULT (newid()) NULL,
    [FileUpload]    UNIQUEIDENTIFIER NULL,
    [DestinationID] INT              NULL,
    [IsTravelClaim] BIT              NULL,
    [TravelReqNo]   VARCHAR (20)     NULL,
    CONSTRAINT [PK_tbl_ClaimsRequest] PRIMARY KEY CLUSTERED ([ID] ASC)
);

