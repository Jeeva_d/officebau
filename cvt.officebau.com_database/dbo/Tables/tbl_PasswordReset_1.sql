﻿CREATE TABLE [dbo].[tbl_PasswordReset] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]  INT              NOT NULL,
    [GUID]        VARCHAR (10)     NULL,
    [IsUsed]      BIT              CONSTRAINT [DF_tbl_PasswordReset_IsUsed] DEFAULT ((0)) NULL,
    [RequestedOn] DATETIME         CONSTRAINT [DF_tbl_PasswordReset_RequestedOn] DEFAULT (getdate()) NULL,
    [IsExpired]   BIT              CONSTRAINT [DF_tbl_PasswordReset_IsExpired] DEFAULT ((0)) NULL,
    [DomainID]    INT              CONSTRAINT [DF_tbl_PasswordReset_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_PasswordReset_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_PasswordReset] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_PasswordReset_tbl_EmployeeMaster] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID])
);

