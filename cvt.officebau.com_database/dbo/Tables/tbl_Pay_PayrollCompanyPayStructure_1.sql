﻿CREATE TABLE [dbo].[tbl_Pay_PayrollCompanyPayStructure] (
    [ID]                    INT              IDENTITY (1, 1) NOT NULL,
    [CompanyPayStructureID] INT              NULL,
    [ComponentID]           INT              NULL,
    [Value]                 VARCHAR (500)    NULL,
    [IsEditable]            BIT              CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_IsEditable] DEFAULT ((0)) NULL,
    [IsDeleted]             BIT              CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]              INT              CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]             INT              CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]             DATETIME         CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            INT              CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]            DATETIME         CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]             UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_HistoryID] DEFAULT (newid()) NOT NULL,
    [ShowinPaystructure]    BIT              CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_ShowinPaystructure] DEFAULT ((1)) NOT NULL,
    [PayStub]               BIT              CONSTRAINT [DF_tbl_Pay_PayrollCompanyPayStructure_PayStub] DEFAULT ((0)) NOT NULL,
    [Fixed]                 BIT              NULL,
    CONSTRAINT [PK__tbl_Pay___3214EC27F04AE527] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_Pay_PayrollCompanyPayStructure_tbl_Pay_CompanyPayStructure] FOREIGN KEY ([CompanyPayStructureID]) REFERENCES [dbo].[tbl_Pay_CompanyPayStructure] ([ID]),
    CONSTRAINT [FK_tbl_Pay_PayrollCompanyPayStructure_tbl_Pay_PayrollCompontents] FOREIGN KEY ([ComponentID]) REFERENCES [dbo].[tbl_Pay_PayrollCompontents] ([ID])
);

