﻿CREATE TABLE [dbo].[tbl_MailerEventKeys] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_MailerEventKeys_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_MailerEventKeys_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_MailerEventKeys_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_MailerEventKeys_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_MailerEventKeys_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_MailerEventKeys_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_MailerEventKeys_DomainID] DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

