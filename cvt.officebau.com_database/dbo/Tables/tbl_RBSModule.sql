﻿CREATE TABLE [dbo].[tbl_RBSModule] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [Module]        VARCHAR (100)    NOT NULL,
    [Description]   VARCHAR (8000)   NULL,
    [Icon]          VARCHAR (1000)   NULL,
    [ModuleOrderID] INT              NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tbl_RBSModule_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tbl_RBSModule_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tbl_RBSModule_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tbl_RBSModule_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_RBSModule_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_RBSModule] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

