﻿CREATE TABLE [dbo].[Auth_Package] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [Country]      VARCHAR (100)    NOT NULL,
    [CountryCode]  VARCHAR (10)     NULL,
    [PackageName]  VARCHAR (100)    NOT NULL,
    [UP1]          INT              NOT NULL,
    [UP2]          INT              NOT NULL,
    [UP3]          INT              NOT NULL,
    [UP4]          INT              NOT NULL,
    [CurrencyCode] VARCHAR (10)     NOT NULL,
    [OrderNumber]  INT              NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_Auth_Package_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedDate]  DATETIME         CONSTRAINT [DF_Auth_Package_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_Auth_Package_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedDate] DATETIME         CONSTRAINT [DF_Auth_Package_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_Auth_Package_IsDelete] DEFAULT ((0)) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    CONSTRAINT [PK_Auth_Package] PRIMARY KEY CLUSTERED ([ID] ASC)
);

