﻿CREATE TABLE [dbo].[tbl_ClaimsApproverConfiguration] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID] INT              NOT NULL,
    [ApproverID] INT              NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_ClaimsApproverConfiguration_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_ClaimsApproverConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_ClaimsApproverConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_ClaimsApproverConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_ClaimsApproverConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ClaimsApproverConfiguration_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_ClaimsApproverConfiguration] PRIMARY KEY CLUSTERED ([ID] ASC)
);

