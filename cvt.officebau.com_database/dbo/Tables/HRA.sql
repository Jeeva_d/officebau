﻿CREATE TABLE [dbo].[HRA] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]  INT              NOT NULL,
    [MonthID]     INT              NOT NULL,
    [YearID]      INT              NOT NULL,
    [Declaration] MONEY            NULL,
    [Submitted]   MONEY            NULL,
    [Cleared]     MONEY            NULL,
    [Rejected]    MONEY            NULL,
    [Remarks]     VARCHAR (8000)   NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_HRA_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_HRA_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_HRA_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_HRA_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_HRA_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_HRA_HistoryID] DEFAULT (newid()) NOT NULL,
    [StartYearID] INT              NULL,
    CONSTRAINT [PK_TDS_HRA] PRIMARY KEY CLUSTERED ([ID] ASC)
);

