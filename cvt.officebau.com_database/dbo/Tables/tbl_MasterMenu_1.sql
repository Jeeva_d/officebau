﻿CREATE TABLE [dbo].[tbl_MasterMenu] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [Name]           VARCHAR (100)    NOT NULL,
    [OrderID]        INT              NULL,
    [Remarks]        VARCHAR (1000)   NULL,
    [MenuParameters] VARCHAR (200)    NULL,
    [RootConfig]     VARCHAR (20)     NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_MasterMenu_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_MasterMenu_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_MasterMenu_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_MasterMenu_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]       INT              CONSTRAINT [DF_tbl_MasterMenu_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_MasterMenu_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_MasterMenu] PRIMARY KEY CLUSTERED ([ID] ASC)
);

