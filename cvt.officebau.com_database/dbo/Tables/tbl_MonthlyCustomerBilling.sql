﻿CREATE TABLE [dbo].[tbl_MonthlyCustomerBilling] (
    [Id]           INT      IDENTITY (1, 1) NOT NULL,
    [CustomerId]   INT      NOT NULL,
    [Year]         INT      NOT NULL,
    [MonthId]      INT      NOT NULL,
    [CurrencyRate] MONEY    NULL,
    [Isdeleted]    BIT      CONSTRAINT [DF__tbl_Month__Isdel__3FF6031B] DEFAULT ((0)) NOT NULL,
    [CreatedBy]    INT      CONSTRAINT [DF__tbl_Month__Creat__40EA2754] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME CONSTRAINT [DF__tbl_Month__Creat__41DE4B8D] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT      CONSTRAINT [DF__tbl_Month__Modif__42D26FC6] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME CONSTRAINT [DF__tbl_Month__Modif__43C693FF] DEFAULT (getdate()) NOT NULL,
    [DomainId]     INT      CONSTRAINT [DF__tbl_Month__Domai__44BAB838] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__tbl_Mont__3214EC071658D61E] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__tbl_Month__Month__3F01DEE2] FOREIGN KEY ([MonthId]) REFERENCES [dbo].[tbl_Month] ([ID]),
    CONSTRAINT [FK_tbl_MonthlyCustomerBilling_tblCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[tblCustomer] ([ID])
);

