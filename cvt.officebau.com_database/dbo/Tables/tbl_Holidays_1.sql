﻿CREATE TABLE [dbo].[tbl_Holidays] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Year]        SMALLINT         NOT NULL,
    [Date]        DATE             NOT NULL,
    [Day]         VARCHAR (20)     NOT NULL,
    [Type]        VARCHAR (50)     NOT NULL,
    [Description] VARCHAR (8000)   NULL,
    [IsOptional]  BIT              CONSTRAINT [DF_tbl_Holidays_IsOptional] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Holidays_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Holidays_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Holidays_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Holidays_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Holidays_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_Holidays_IsDeleted] DEFAULT ((0)) NOT NULL,
    [RegionID]    VARCHAR (1000)   NOT NULL,
    CONSTRAINT [PK_tbl_Holidays] PRIMARY KEY CLUSTERED ([ID] ASC)
);

