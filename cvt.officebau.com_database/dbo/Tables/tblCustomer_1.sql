﻿CREATE TABLE [dbo].[tblCustomer] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (100)    NOT NULL,
    [Address]         VARCHAR (1000)   NULL,
    [CityID]          INT              NULL,
    [PaymentTerms]    VARCHAR (1000)   NULL,
    [ContactPerson]   VARCHAR (50)     NULL,
    [ContactNo]       VARCHAR (20)     NULL,
    [ContactPersonNo] VARCHAR (20)     NULL,
    [EmailID]         VARCHAR (100)    NULL,
    [CustomerEmailID] VARCHAR (100)    NULL,
    [BillingAddress]  VARCHAR (1000)   NULL,
    [PanNo]           VARCHAR (500)    NULL,
    [TanNo]           VARCHAR (500)    NULL,
    [GSTNo]           VARCHAR (500)    NULL,
    [Remarks]         VARCHAR (1000)   NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tblCustomer_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tblCustomer_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tblCustomer_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tblCustomer_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tblCustomer_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]        INT              CONSTRAINT [DF_tblCustomer_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tblCustomer_HistoryID] DEFAULT (newid()) NOT NULL,
    [CurrencyID]      INT              NULL,
    [BankID]          INT              NULL,
    CONSTRAINT [PK_tblCustomer] PRIMARY KEY CLUSTERED ([ID] ASC)
);

