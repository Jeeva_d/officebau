﻿CREATE TABLE [dbo].[tblMasterTypes] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (100)    NOT NULL,
    [Code]        VARCHAR (10)     NOT NULL,
    [Description] VARCHAR (1000)   NULL,
    [Type]        VARCHAR (20)     NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tblMasterTypes_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tblMasterTypes_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tblMasterTypes_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tblMasterTypes_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tblMasterTypes_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tblMasterTypes_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblMasterTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

