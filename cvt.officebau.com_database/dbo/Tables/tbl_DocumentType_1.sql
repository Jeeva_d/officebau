﻿CREATE TABLE [dbo].[tbl_DocumentType] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [DomainID]   INT              CONSTRAINT [DF_DocumentType_DomainID] DEFAULT ((1)) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_DocumentType_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_DocumentType_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_DocumentType_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_DocumentType_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_DocumentType_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_DocumentType_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_DocumentType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

