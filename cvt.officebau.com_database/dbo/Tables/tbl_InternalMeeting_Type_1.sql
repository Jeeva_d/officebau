﻿CREATE TABLE [dbo].[tbl_InternalMeeting_Type] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (100)    NOT NULL,
    [Description] VARCHAR (500)    NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_InternalMeeting_Type_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_InternalMeeting_Type_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_InternalMeeting_Type_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_InternalMeeting_Type_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]    INT              CONSTRAINT [DF_tbl_InternalMeeting_Type_DomainId] DEFAULT ((1)) NOT NULL,
    [HistoryId]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_InternalMeeting_Type_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_InternalMeeting_Type] PRIMARY KEY CLUSTERED ([Id] ASC)
);

