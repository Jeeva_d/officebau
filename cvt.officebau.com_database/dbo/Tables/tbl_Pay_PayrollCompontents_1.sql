﻿CREATE TABLE [dbo].[tbl_Pay_PayrollCompontents] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (12)     NOT NULL,
    [Description] VARCHAR (500)    NULL,
    [Ordinal]     INT              NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_Pay_PayrollCompontents_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              CONSTRAINT [DF_tbl_Pay_PayrollCompontents_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Pay_PayrollCompontents_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Pay_PayrollCompontents_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Pay_PayrollCompontents_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Pay_PayrollCompontents_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Pay_PayrollCompontents_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK__tbl_Pay___3214EC27D7E9D6EC] PRIMARY KEY CLUSTERED ([ID] ASC)
);

