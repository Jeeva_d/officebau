﻿CREATE TABLE [dbo].[tbl_EmploymentDetails] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]      INT              NOT NULL,
    [CompanyName]     VARCHAR (500)    NOT NULL,
    [YearID]          INT              NOT NULL,
    [LastDesignation] VARCHAR (100)    NOT NULL,
    [Code]            VARCHAR (20)     NOT NULL,
    [ReportingTo]     VARCHAR (100)    NULL,
    [WorkLocation]    VARCHAR (100)    NULL,
    [Remarks]         VARCHAR (8000)   NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tbl_EmploymentDetails_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tbl_EmploymentDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tbl_EmploymentDetails_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tbl_EmploymentDetails_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tbl_EmploymentDetails_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmploymentDetails_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]        INT              CONSTRAINT [DF_tbl_EmploymentDetails_DomainID] DEFAULT ((1)) NOT NULL,
    [MonthID]         INT              NULL,
    [ToMonthID]       INT              NULL,
    [ToYearID]        INT              NULL,
    CONSTRAINT [PK_tbl_EmploymentDetails] PRIMARY KEY CLUSTERED ([ID] ASC)
);

