﻿CREATE TABLE [dbo].[tblExpenseHoldings] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [ExpenseId]  INT              NOT NULL,
    [Date]       DATE             NOT NULL,
    [LedgerId]   INT              NOT NULL,
    [Amount]     MONEY            NOT NULL,
    [Isdeleted]  BIT              CONSTRAINT [DF_tblExpenseHoldings_Isdeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tblExpenseHoldings_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tblExpenseHoldings_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tblExpenseHoldings_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tblExpenseHoldings_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tblExpenseHoldings_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tblExpenseHoldings_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblExpenseHoldings] PRIMARY KEY CLUSTERED ([ID] ASC)
);

