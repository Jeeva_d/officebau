﻿CREATE TABLE [dbo].[Auth_Subscribe] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [EmailId]      VARCHAR (100)    NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_Auth_Subscribe_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedDate]  DATETIME         CONSTRAINT [DF_Auth_Subscribe_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_Auth_Subscribe_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedDate] DATETIME         CONSTRAINT [DF_Auth_Subscribe_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_Auth_Subscribe_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    CONSTRAINT [PK_Auth_Subscribe] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

