﻿CREATE TABLE [dbo].[tblProduct] (
    [ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [Name]               VARCHAR (200)    NOT NULL,
    [Code]               VARCHAR (50)     NULL,
    [Rate]               MONEY            NULL,
    [ProductDescription] VARCHAR (1000)   NULL,
    [GstHSNID]           INT              NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_tblProduct_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]          DATETIME         CONSTRAINT [DF_tblProduct_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          INT              CONSTRAINT [DF_tblProduct_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]         DATETIME         CONSTRAINT [DF_tblProduct_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]         INT              CONSTRAINT [DF_tblProduct_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]           INT              CONSTRAINT [DF_tblProduct_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]          UNIQUEIDENTIFIER CONSTRAINT [DF_tblProduct_HistoryID] DEFAULT (newid()) NOT NULL,
    [TransactionTypeID]  INT              NULL,
    [Manufacturer]       VARCHAR (200)    NULL,
    [TypeID]             INT              NULL,
    [OpeningStock]       DECIMAL (18, 2)  NULL,
    [OpeningBalance]     MONEY            NULL,
    [LedgerId]           INT              NULL,
    [UOMID]              INT              NULL,
    CONSTRAINT [PK_tblProduct] PRIMARY KEY CLUSTERED ([ID] ASC)
);

