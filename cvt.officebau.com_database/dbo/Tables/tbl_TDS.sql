﻿CREATE TABLE [dbo].[tbl_TDS] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeId]     INT              NOT NULL,
    [MonthId]        INT              NOT NULL,
    [YearID]         INT              NOT NULL,
    [TDSAmount]      MONEY            NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_TDS_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_TDS_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_TDS_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_TDS_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]       INT              CONSTRAINT [DF_tbl_TDS_DomainId] DEFAULT ((1)) NOT NULL,
    [BaseLocationID] INT              NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tbl_TDS_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_TDS_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_TDS] PRIMARY KEY CLUSTERED ([Id] ASC)
);

