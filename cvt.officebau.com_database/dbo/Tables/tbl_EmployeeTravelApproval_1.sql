﻿CREATE TABLE [dbo].[tbl_EmployeeTravelApproval] (
    [ID]               INT              IDENTITY (1, 1) NOT NULL,
    [TravelRequestID]  INT              NOT NULL,
    [ApproverID]       INT              NOT NULL,
    [ApproverStatusID] INT              NULL,
    [ApproverRemarks]  VARCHAR (500)    NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_tbl_EmployeeTravelApproval_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_tbl_EmployeeTravelApproval_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        INT              CONSTRAINT [DF_tbl_EmployeeTravelApproval_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_tbl_EmployeeTravelApproval_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       INT              CONSTRAINT [DF_tbl_EmployeeTravelApproval_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeTravelApproval_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]         INT              CONSTRAINT [DF_tbl_EmployeeTravelApproval_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_EmployeeTravelApproval] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_EmployeeTravelApproval_tbl_EmployeeTravelRequest] FOREIGN KEY ([TravelRequestID]) REFERENCES [dbo].[tbl_EmployeeTravelRequest] ([ID])
);

