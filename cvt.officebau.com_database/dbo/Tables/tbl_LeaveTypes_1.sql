﻿CREATE TABLE [dbo].[tbl_LeaveTypes] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (8000)   NULL,
    [DomainID]   INT              NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_LeaveTypes_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_LeaveTypes_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_LeaveTypes_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_LeaveTypes_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_LeaveTypes_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_LeaveTypes_IsDeleted] DEFAULT ((0)) NOT NULL,
    [Code]       VARCHAR (20)     NOT NULL,
    CONSTRAINT [PK_tbl_LeaveTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

