﻿CREATE TABLE [dbo].[tbl_EmployeeMissedPunches] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]   INT              NOT NULL,
    [PunchTypeID]  INT              NOT NULL,
    [PunchDate]    DATE             NOT NULL,
    [PunchTime]    TIME (7)         NOT NULL,
    [Remarks]      VARCHAR (8000)   NULL,
    [StatusID]     INT              NOT NULL,
    [HRApproverID] INT              NULL,
    [HRApprovedOn] DATETIME         NULL,
    [HRRemarks]    VARCHAR (8000)   NULL,
    [DomainID]     INT              NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeMissedPunches_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_tbl_EmployeeMissedPunches_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tbl_EmployeeMissedPunches_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tbl_EmployeeMissedPunches_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tbl_EmployeeMissedPunches_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_tbl_EmployeeMissedPunches_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_EmployeeMissedPunches] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

