﻿CREATE TABLE [dbo].[Auth_Feedback] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (150)    NOT NULL,
    [Email]        VARCHAR (150)    NOT NULL,
    [FeedbackMsg]  VARCHAR (MAX)    NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_Auth_Feedback_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedDate]  DATETIME         CONSTRAINT [DF_Auth_Feedback_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_Auth_Feedback_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedDate] DATETIME         CONSTRAINT [DF_Auth_Feedback_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_Auth_Feedback_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    CONSTRAINT [PK_Auth_Feedback] PRIMARY KEY CLUSTERED ([ID] ASC)
);

