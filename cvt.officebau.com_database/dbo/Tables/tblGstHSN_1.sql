﻿CREATE TABLE [dbo].[tblGstHSN] (
    [ID]             INT             IDENTITY (1, 1) NOT NULL,
    [Code]           VARCHAR (20)    NOT NULL,
    [GSTDescription] VARCHAR (500)   NOT NULL,
    [CGST]           DECIMAL (18, 2) NULL,
    [SGST]           DECIMAL (18, 2) NULL,
    [IGST]           DECIMAL (18, 2) NULL,
    [ModifiedBy]     INT             NOT NULL,
    [ModifiedOn]     DATETIME        NULL,
    [IsDeleted]      BIT             CONSTRAINT [DF_tblGstHSN_IsDeleted] DEFAULT ((0)) NULL,
    [DomainID]       INT             CONSTRAINT [DF_tblGstHSN_DomainID] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_tblGstHSN] PRIMARY KEY CLUSTERED ([ID] ASC)
);

