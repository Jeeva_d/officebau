﻿CREATE TABLE [dbo].[tbl_EmployeeCompOff] (
    [ID]                INT              IDENTITY (1, 1) NOT NULL,
    [Date]              DATE             NOT NULL,
    [RequesterRemarks]  VARCHAR (8000)   NULL,
    [StatusID]          INT              NOT NULL,
    [ApproverID]        INT              NOT NULL,
    [ApprovedDate]      DATETIME         NULL,
    [ApproverRemarks]   VARCHAR (8000)   NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_tbl_EmployeeCompOff_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]         INT              CONSTRAINT [DF_tbl_CompOff_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]         DATETIME         CONSTRAINT [DF_tbl_EmployeeCompOff_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]        INT              CONSTRAINT [DF_tbl_CompOff_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]        DATETIME         CONSTRAINT [DF_tbl_EmployeeCompOff_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]          INT              CONSTRAINT [DF_tbl_EmployeeCompOff_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]         UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeCompOff_HistoryID] DEFAULT (newid()) NOT NULL,
    [HRApproverID]      INT              NULL,
    [HRApproverRemarks] VARCHAR (500)    NULL,
    [HRApprovedDate]    DATETIME         NULL,
    [HRStatusID]        INT              NULL,
    [ApproverStatusID]  INT              NULL,
    CONSTRAINT [PK_tbl_EmployeeCompOff] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

