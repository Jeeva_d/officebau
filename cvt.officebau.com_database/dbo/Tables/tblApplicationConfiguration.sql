﻿CREATE TABLE [dbo].[tblApplicationConfiguration] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (100)    NOT NULL,
    [Description] VARCHAR (8000)   NULL,
    [Value]       VARCHAR (100)    NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_Configuration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_Configuration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_Configuration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_Configuration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_Configuration_IsDelated] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              CONSTRAINT [DF_tblApplicationConfiguration_DomainID] DEFAULT ((1)) NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_Configuration_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_ApplicationConfiguration] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

