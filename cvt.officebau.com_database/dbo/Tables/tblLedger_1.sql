﻿CREATE TABLE [dbo].[tblLedger] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [GroupID]        INT              NULL,
    [Name]           VARCHAR (100)    NOT NULL,
    [Remarks]        VARCHAR (1000)   NULL,
    [IsExpense]      BIT              CONSTRAINT [DF_tblLedger_IsExpense] DEFAULT ((0)) NULL,
    [OpeningBalance] MONEY            CONSTRAINT [DF_tblLedger_OpeningBalance] DEFAULT ((0)) NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tblLedger_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tblLedger_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tblLedger_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tblLedger_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tblLedger_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]       INT              CONSTRAINT [DF_tblLedger_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tblLedger_HistoryID] DEFAULT (newid()) NOT NULL,
    [IsTaxComponent] BIT              CONSTRAINT [DF_tblLedger_IsTaxComponent] DEFAULT ((0)) NOT NULL,
    [LedgerType]     VARCHAR (100)    NULL,
    [GstHSNID]       INT              NULL,
    [Isdefault]      BIT              CONSTRAINT [DF_tblLedger_Isdefault] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tblLedger] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tblLedger_tblGroupLedger] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[tblGroupLedger] ([ID])
);

