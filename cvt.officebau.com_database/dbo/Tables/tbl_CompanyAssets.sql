﻿CREATE TABLE [dbo].[tbl_CompanyAssets] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]      INT              NOT NULL,
    [AssetTypeID]     INT              NOT NULL,
    [QTY]             INT              NULL,
    [Make]            VARCHAR (100)    NULL,
    [SerialNo]        VARCHAR (100)    NULL,
    [HandoverOn]      DATETIME         NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tbl_CompanyAssets_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tbl_CompanyAssets_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tbl_CompanyAssets_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tbl_CompanyAssets_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tbl_CompanyAssets_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_CompanyAssets_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]        INT              CONSTRAINT [DF_tbl_CompanyAssets_DomainID] DEFAULT ((1)) NOT NULL,
    [ReturnedOn]      DATETIME         NULL,
    [ReturnedRemarks] VARCHAR (8000)   NULL,
    [Remarks]         VARCHAR (8000)   NULL,
    [ReturnStatus]    BIT              CONSTRAINT [DF_tbl_CompanyAssets_ReturnStatus] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_tbl_CompanyAssets] PRIMARY KEY CLUSTERED ([ID] ASC)
);

