﻿CREATE TABLE [dbo].[tbl_DocumentCategory] (
    [Id]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (MAX)    NULL,
    [DomainId]   INT              CONSTRAINT [DF_tbl_DocumentCategory_DomainId] DEFAULT ((1)) NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_DocumentCategory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_DocumentCategory_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_DocumentCategory_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_DocumentCategory_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_DocumentCategory_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryId]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_DocumentCategory_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_DocumentCategory] PRIMARY KEY CLUSTERED ([Id] ASC)
);

