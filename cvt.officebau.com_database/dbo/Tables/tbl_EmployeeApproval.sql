﻿CREATE TABLE [dbo].[tbl_EmployeeApproval] (
    [ID]                     INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]             INT              NOT NULL,
    [TempEmployeeCode]       VARCHAR (20)     NULL,
    [FirstApproverId]        INT              NULL,
    [FirstApproverdOn]       DATETIME         NULL,
    [FirstApproverRemarks]   VARCHAR (500)    NULL,
    [SecondApproverId]       INT              NULL,
    [SecondApproverdOn]      DATETIME         NULL,
    [SecondApproverRemarks]  VARCHAR (500)    NULL,
    [IsDeleted]              BIT              CONSTRAINT [DF_tbl_EmployeeApproval_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]              DATETIME         CONSTRAINT [DF_tbl_EmployeeApproval_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              INT              CONSTRAINT [DF_tbl_EmployeeApproval_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]             DATETIME         CONSTRAINT [DF_tbl_EmployeeApproval_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]             INT              CONSTRAINT [DF_tbl_EmployeeApproval_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]              UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeApproval_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]               INT              CONSTRAINT [DF_tbl_EmployeeApproval_DomainID] DEFAULT ((1)) NOT NULL,
    [EmployeePaystructureID] INT              NULL,
    CONSTRAINT [PK_tbl_EmployeeApproval] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_EmployeeApproval_tbl_EmployeeMaster] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID])
);

