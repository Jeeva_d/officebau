﻿CREATE TABLE [dbo].[tbl_MonthlyCustomerBillingDetails] (
    [Id]                     INT             IDENTITY (1, 1) NOT NULL,
    [MonthlyID]              INT             NOT NULL,
    [EmployeeID]             INT             NOT NULL,
    [HourlyRate]             MONEY           NULL,
    [HoursLogged]            INT             NULL,
    [FixedRate]              MONEY           NULL,
    [Total]                  MONEY           NOT NULL,
    [BillingPercentage]      DECIMAL (16, 4) NULL,
    [RemunerationPercentage] DECIMAL (16, 4) NULL,
    [EmployeeBillingId]      INT             NULL,
    [Isdeleted]              BIT             CONSTRAINT [DF__tbl_Month__Isdel__497F6D55] DEFAULT ((0)) NOT NULL,
    [CreatedBy]              INT             CONSTRAINT [DF__tbl_Month__Creat__4A73918E] DEFAULT ((1)) NOT NULL,
    [CreatedOn]              DATETIME        CONSTRAINT [DF__tbl_Month__Creat__4B67B5C7] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]             INT             CONSTRAINT [DF__tbl_Month__Modif__4C5BDA00] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]             DATETIME        CONSTRAINT [DF__tbl_Month__Modif__4D4FFE39] DEFAULT (getdate()) NOT NULL,
    [DomainId]               INT             CONSTRAINT [DF__tbl_Month__Domai__4E442272] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK__tbl_Mont__3214EC0703CD6097] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__tbl_Month__Emplo__488B491C] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID]),
    CONSTRAINT [FK__tbl_Month__Month__479724E3] FOREIGN KEY ([MonthlyID]) REFERENCES [dbo].[tbl_MonthlyCustomerBilling] ([Id]),
    CONSTRAINT [FK__tbl_MonthlyCustomerBillingDetails__tbl_EmployeeBilling] FOREIGN KEY ([EmployeeBillingId]) REFERENCES [dbo].[tbl_EmployeeBilling] ([Id])
);

