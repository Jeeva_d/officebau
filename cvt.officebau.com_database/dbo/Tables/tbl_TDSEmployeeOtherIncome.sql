﻿CREATE TABLE [dbo].[tbl_TDSEmployeeOtherIncome] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]  INT              NOT NULL,
    [Year]        INT              NOT NULL,
    [Bonus]       MONEY            NULL,
    [PLI]         MONEY            NULL,
    [OtherIncome] MONEY            NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_TDSEmployeeOtherIncome_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_TDSEmployeeOtherIncome_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_TDSEmployeeOtherIncome_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_TDSEmployeeOtherIncome_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_TDSEmployeeOtherIncome_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              CONSTRAINT [DF_tbl_TDSEmployeeOtherIncome_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_TDSEmployeeOtherIncome_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_TDSEmployeeOtherIncome] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_TDSEmployeeOtherIncome_tbl_EmployeeMaster] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID])
);

