﻿CREATE TABLE [dbo].[tbl_FandFDetails] (
    [ID]                INT              IDENTITY (1, 1) NOT NULL,
    [FAndFid]           INT              NOT NULL,
    [Type]              VARCHAR (100)    NOT NULL,
    [Description]       VARCHAR (500)    NOT NULL,
    [Amount]            MONEY            NULL,
    [IsEditableAmount]  BIT              NOT NULL,
    [Remarks]           VARCHAR (250)    NULL,
    [IsEditableRemarks] BIT              NOT NULL,
    [Ordinal]           INT              NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_tbl_FandFDetails_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]         DATETIME         CONSTRAINT [DF_tbl_FandFDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         INT              CONSTRAINT [DF_tbl_FandFDetails_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]        DATETIME         CONSTRAINT [DF_tbl_FandFDetails_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]        INT              CONSTRAINT [DF_tbl_FandFDetails_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]         UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_FandFDetails_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]          INT              CONSTRAINT [DF_tbl_FandFDetails_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_FandFDetails] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_FandFDetails_tbl_FandF_ID] FOREIGN KEY ([FAndFid]) REFERENCES [dbo].[tbl_FandF] ([ID])
);

