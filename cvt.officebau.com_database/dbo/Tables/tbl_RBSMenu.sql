﻿CREATE TABLE [dbo].[tbl_RBSMenu] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [MenuCode]    VARCHAR (50)     NOT NULL,
    [Menu]        VARCHAR (100)    NOT NULL,
    [Description] VARCHAR (8000)   NULL,
    [MenuURL]     VARCHAR (500)    NOT NULL,
    [SubModuleID] INT              NOT NULL,
    [MenuOrderID] INT              NULL,
    [RootConfig]  VARCHAR (20)     NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_RBSMenu_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_RBSMenu_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_RBSMenu_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_RBSMenu_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_RBSMenu_HistoryID] DEFAULT (newid()) NULL,
    [IsSubmenu]   BIT              CONSTRAINT [DF_tbl_RBSMenu_IsSubmenu] DEFAULT ((0)) NULL,
    [MenuIcon]    VARCHAR (50)     NULL,
    [DomainID]    INT              CONSTRAINT [DF_tbl_RBSMenu_DomainID] DEFAULT ((1)) NULL,
    [IsMobility]  BIT              CONSTRAINT [DF_tbl_RBSMenu_IsMobility] DEFAULT ((0)) NULL,
    [Isdeleted]   BIT              CONSTRAINT [DF_tbl_RBSMenu_Isdeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_RBSMenu] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

