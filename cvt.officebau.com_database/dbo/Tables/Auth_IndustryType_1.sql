﻿CREATE TABLE [dbo].[Auth_IndustryType] (
    [ID]                  INT              IDENTITY (1, 1) NOT NULL,
    [IndustryName]        VARCHAR (250)    NOT NULL,
    [IndustryDescription] VARCHAR (500)    NULL,
    [CreatedBy]           INT              CONSTRAINT [DF_Auth_IndustryType_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedDate]         DATETIME         CONSTRAINT [DF_Auth_IndustryType_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]          INT              CONSTRAINT [DF_Auth_IndustryType_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedDate]        DATETIME         CONSTRAINT [DF_Auth_IndustryType_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]           BIT              CONSTRAINT [DF_Auth_IndustryType_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]           UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    CONSTRAINT [PK_Auth_IndustryType] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

