﻿CREATE TABLE [dbo].[tbl_MultipleFileUpload] (
    [RowID]            INT              IDENTITY (1, 1) NOT NULL,
    [ID]               UNIQUEIDENTIFIER NOT NULL,
    [Path]             VARCHAR (MAX)    NOT NULL,
    [CreatedBy]        INT              CONSTRAINT [DF_tbl_MultipleFileUpload1_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_tbl_MultipleFileUpload1_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       INT              CONSTRAINT [DF_tbl_MultipleFileUpload1_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_tbl_MultipleFileUpload1_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [FileName]         VARCHAR (200)    NULL,
    [OriginalFileName] VARCHAR (200)    NULL,
    CONSTRAINT [PK_tbl_MultipleFileUpload] PRIMARY KEY CLUSTERED ([RowID] ASC)
);

