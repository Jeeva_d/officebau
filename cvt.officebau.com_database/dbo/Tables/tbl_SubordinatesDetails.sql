﻿CREATE TABLE [dbo].[tbl_SubordinatesDetails] (
    [TravelRequestID] INT              NOT NULL,
    [Name]            VARCHAR (200)    NOT NULL,
    [Remarks]         VARCHAR (1000)   NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tbl_SubordinatesDetails_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tbl_SubordinatesDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tbl_SubordinatesDetails_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tbl_SubordinatesDetails_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tbl_SubordinatesDetails_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_SubordinatesDetails_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]        INT              CONSTRAINT [DF_tbl_SubordinatesDetails_DomainID] DEFAULT ((1)) NOT NULL
);

