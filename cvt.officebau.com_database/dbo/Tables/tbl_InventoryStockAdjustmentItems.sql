﻿CREATE TABLE [dbo].[tbl_InventoryStockAdjustmentItems] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [AdjustmentID] INT              NOT NULL,
    [ProductID]    INT              NOT NULL,
    [AdjustedQty]  INT              NULL,
    [Remarks]      VARCHAR (1000)   NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_tbl_InventoryStockAdjustmentItems_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]     INT              CONSTRAINT [DF_tbl_InventoryStockAdjustmentItems_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_tbl_InventoryStockAdjustmentItems_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tbl_InventoryStockAdjustmentItems_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tbl_InventoryStockAdjustmentItems_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tbl_InventoryStockAdjustmentItems_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_InventoryStockAdjustmentItems_HistoryID] DEFAULT (newid()) NOT NULL,
    [RemainingQty] INT              NULL,
    CONSTRAINT [PK_tbl_InventoryStockAdjustmentItems] PRIMARY KEY CLUSTERED ([ID] ASC)
);

