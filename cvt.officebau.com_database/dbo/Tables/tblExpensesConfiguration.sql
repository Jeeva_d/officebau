﻿CREATE TABLE [dbo].[tblExpensesConfiguration] (
    [Id]           INT              IDENTITY (1, 1) NOT NULL,
    [GLedgerId]    INT              NOT NULL,
    [ScreenType]   VARCHAR (10)     NOT NULL,
    [Limit]        MONEY            NOT NULL,
    [Remarks]      VARCHAR (MAX)    NULL,
    [Approvers]    VARCHAR (500)    NULL,
    [ApproverType] VARCHAR (50)     NULL,
    [CreatedById]  INT              CONSTRAINT [DF_tblExpensesConfiguration_CreatedById] DEFAULT ((1)) NOT NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tblExpensesConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tblExpensesConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tblExpensesConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [Isdeleted]    BIT              CONSTRAINT [DF_tblExpensesConfiguration_Isdeleted] DEFAULT ((0)) NOT NULL,
    [DomainId]     INT              CONSTRAINT [DF_tblExpensesConfiguration_DomainId] DEFAULT ((1)) NOT NULL,
    [HistoryId]    UNIQUEIDENTIFIER CONSTRAINT [DF_tblExpensesConfiguration_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblExpensesConfiguration] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblExpensesConfiguration_tbl_EmployeeMaster] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[tbl_EmployeeMaster] ([ID]),
    CONSTRAINT [FK_tblExpensesConfiguration_tblGroupLedger] FOREIGN KEY ([GLedgerId]) REFERENCES [dbo].[tblGroupLedger] ([ID])
);

