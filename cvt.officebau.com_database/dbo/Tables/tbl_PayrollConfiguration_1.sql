﻿CREATE TABLE [dbo].[tbl_PayrollConfiguration] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [Components]     VARCHAR (100)    NULL,
    [Percentage]     DECIMAL (5, 2)   NULL,
    [Limit]          MONEY            NULL,
    [Value]          MONEY            NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_PayrollConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_PayrollConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_PayrollConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_PayrollConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_PayrollConfiguration_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]       INT              CONSTRAINT [DF_tbl_PayrollConfiguration_DomainID] DEFAULT ((1)) NULL,
    [BusinessUnitID] INT              NULL,
    CONSTRAINT [PK_tbl_PayrollConfiguration] PRIMARY KEY CLUSTERED ([ID] ASC)
);

