﻿CREATE TABLE [dbo].[tblTransfer] (
    [ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [FromID]             INT              NOT NULL,
    [ToID]               INT              NULL,
    [Amount]             MONEY            NULL,
    [VoucherDate]        DATETIME         NULL,
    [VoucherDescription] VARCHAR (1000)   NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_tblTransfer_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]          DATETIME         CONSTRAINT [DF_tblTransfer_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          INT              CONSTRAINT [DF_tblTransfer_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]         DATETIME         CONSTRAINT [DF_tblTransfer_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]         INT              CONSTRAINT [DF_tblTransfer_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]           INT              CONSTRAINT [DF_tblTransfer_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]          UNIQUEIDENTIFIER CONSTRAINT [DF_tblTransfer_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblTransfer] PRIMARY KEY CLUSTERED ([ID] ASC)
);

