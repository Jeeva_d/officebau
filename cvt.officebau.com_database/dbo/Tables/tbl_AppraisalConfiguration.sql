﻿CREATE TABLE [dbo].[tbl_AppraisalConfiguration] (
    [ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [AppraisalName]      VARCHAR (500)    NOT NULL,
    [BUID]               INT              NOT NULL,
    [AppraiseeStartDate] DATETIME         NOT NULL,
    [AppraiseeEndDate]   DATETIME         NOT NULL,
    [AppraiserStartDate] DATETIME         NOT NULL,
    [AppraiserEndDate]   DATETIME         NOT NULL,
    [EligibilityDate]    DATETIME         NOT NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_tbl_AppraisalConfiguration_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]          DATETIME         CONSTRAINT [DF_tbl_AppraisalConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          INT              CONSTRAINT [DF_tbl_AppraisalConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]         DATETIME         CONSTRAINT [DF_tbl_AppraisalConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]         INT              CONSTRAINT [DF_tbl_AppraisalConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]          UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_AppraisalConfiguration_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]           INT              CONSTRAINT [DF_tbl_AppraisalConfiguration_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_AppraisalConfiguration] PRIMARY KEY CLUSTERED ([ID] ASC)
);

