﻿CREATE TABLE [dbo].[tbl_InventoryStockDistribution] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]      INT              NOT NULL,
    [ProductID]       INT              NOT NULL,
    [Qty]             INT              NOT NULL,
    [CostCenterID]    INT              NOT NULL,
    [DistributedDate] DATETIME         NOT NULL,
    [Remarks]         VARCHAR (1000)   NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tbl_InventoryStockDistribution_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]        INT              CONSTRAINT [DF_tbl_InventoryStockDistribution_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tbl_InventoryStockDistribution_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tbl_InventoryStockDistribution_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tbl_InventoryStockDistribution_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tbl_InventoryStockDistribution_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_InventoryStockDistribution_HistoryID] DEFAULT (newid()) NOT NULL,
    [Party]           VARCHAR (100)    NULL,
    [IsInternal]      BIT              NULL,
    CONSTRAINT [PK_tbl_InventoryStockDistribution] PRIMARY KEY CLUSTERED ([ID] ASC)
);

