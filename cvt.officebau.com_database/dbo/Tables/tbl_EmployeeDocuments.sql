﻿CREATE TABLE [dbo].[tbl_EmployeeDocuments] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]     INT              NOT NULL,
    [Title]          VARCHAR (100)    NOT NULL,
    [FileName]       VARCHAR (500)    NULL,
    [FilePath]       VARCHAR (1000)   NULL,
    [Remarks]        VARCHAR (MAX)    NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_EmployeeDocuments_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_EmployeeDocuments_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_EmployeeDocuments_ModifeiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_EmployeeDocuments_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tbl_EmployeeDocuments_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]       INT              NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeDocuments_HistoryID] DEFAULT (newid()) NOT NULL,
    [DocumentTypeId] INT              NULL,
    [FileID]         UNIQUEIDENTIFIER NULL,
    [DateofExpiry]   DATETIME         NULL,
    CONSTRAINT [PK_tbl_EmployeeDocuments] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

