﻿CREATE TABLE [dbo].[tblDepartment] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tblDepartment_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tblDepartment_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tblDepartment_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tblDepartment_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tblDepartment_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tblDepartment_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tblDepartment_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblDepartment] PRIMARY KEY CLUSTERED ([ID] ASC)
);

