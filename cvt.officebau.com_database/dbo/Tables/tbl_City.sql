﻿CREATE TABLE [dbo].[tbl_City] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [StateID]    INT              NULL,
    [Remarks]    VARCHAR (1000)   NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_City_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_City_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_City_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_City_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_City_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_City_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_City_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_City] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_City_tbl_State] FOREIGN KEY ([StateID]) REFERENCES [dbo].[tbl_State] ([ID])
);

