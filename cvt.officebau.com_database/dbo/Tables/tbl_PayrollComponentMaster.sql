﻿CREATE TABLE [dbo].[tbl_PayrollComponentMaster] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (50)     NOT NULL,
    [Description] VARCHAR (1000)   NOT NULL,
    [ControlType] VARCHAR (100)    NOT NULL,
    [AssessLevel] BIT              CONSTRAINT [DF_tbl_PayrollComponentMaster_AssessLevel] DEFAULT ((0)) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_PayrollComponentMaster_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_PayrollComponentMaster_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_PayrollComponentMaster_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_PayrollComponentMaster_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_PayrollComponentMaster_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_PayrollComponentMaster] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80),
    UNIQUE NONCLUSTERED ([Code] ASC)
);

