﻿CREATE TABLE [dbo].[tbl_NewsAndEvents] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (100)    NOT NULL,
    [Description]  VARCHAR (MAX)    NOT NULL,
    [BusinessUnit] VARCHAR (200)    NULL,
    [PublishedBy]  INT              NULL,
    [PublishedOn]  DATETIME         NULL,
    [EndDate]      DATETIME         NULL,
    [HistoryID]    UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_NewsAndEvents_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [DF_tbl_NewsAndEvents_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_tbl_NewsAndEvents_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [DF_tbl_NewsAndEvents_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_tbl_NewsAndEvents_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]     INT              CONSTRAINT [DF_tbl_NewsAndEvents_DomainID] DEFAULT ((1)) NOT NULL,
    [StartDate]    DATETIME         NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_tbl_NewsAndEvents_IsDeleted] DEFAULT ((0)) NOT NULL,
    [EventDate]    DATETIME         NULL,
    CONSTRAINT [PK_tbl_NewsAndEvents] PRIMARY KEY CLUSTERED ([ID] ASC)
);

