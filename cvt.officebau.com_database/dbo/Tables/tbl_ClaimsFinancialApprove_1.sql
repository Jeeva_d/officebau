﻿CREATE TABLE [dbo].[tbl_ClaimsFinancialApprove] (
    [ID]                INT              IDENTITY (1, 1) NOT NULL,
    [ApproverID]        INT              NULL,
    [Remarks]           VARCHAR (8000)   NULL,
    [StatusID]          INT              NULL,
    [ClaimItemID]       INT              NOT NULL,
    [SettlerStatusID]   INT              NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_tbl_ClaimsFinancialApprove_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]         DATETIME         CONSTRAINT [DF_tbl_ClaimsFinancialApprove_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]         INT              CONSTRAINT [DF_tbl_ClaimsFinancialApprove_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]        DATETIME         CONSTRAINT [DF_tbl_ClaimsFinancialApprove_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]        INT              CONSTRAINT [DF_tbl_ClaimsFinancialApprove_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]          INT              CONSTRAINT [DF_tbl_ClaimsFinancialApprove_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]         UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ClaimsFinancialApprove_HistoryID] DEFAULT (newid()) NOT NULL,
    [FinApprovedAmount] MONEY            NULL,
    CONSTRAINT [PK_tbl_ClaimsFinanacialApprove] PRIMARY KEY CLUSTERED ([ID] ASC)
);

