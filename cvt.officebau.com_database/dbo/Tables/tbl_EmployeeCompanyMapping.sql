﻿CREATE TABLE [dbo].[tbl_EmployeeCompanyMapping] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID] INT              NOT NULL,
    [CompanyIDs] VARCHAR (50)     NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_EmployeeCompanyMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_EmployeeCompanyMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_EmployeeCompanyMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_EmployeeCompanyMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_EmployeeCompanyMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeCompanyMapping_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_EmployeeCompanyMapping] PRIMARY KEY CLUSTERED ([ID] ASC)
);

