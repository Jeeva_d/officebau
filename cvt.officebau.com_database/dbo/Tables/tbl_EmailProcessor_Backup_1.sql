﻿CREATE TABLE [dbo].[tbl_EmailProcessor_Backup] (
    [ID]              BIGINT           NULL,
    [FromID]          VARCHAR (200)    NULL,
    [ToID]            VARCHAR (MAX)    NULL,
    [Cc]              VARCHAR (MAX)    NULL,
    [Bcc]             VARCHAR (MAX)    NULL,
    [Subject]         VARCHAR (MAX)    NULL,
    [Content]         TEXT             NULL,
    [Attachment]      TEXT             NULL,
    [AttachmentPath]  TEXT             NULL,
    [IsPolled]        INT              NULL,
    [SentDate]        DATETIME         NULL,
    [RetryCount]      INT              NULL,
    [ProcessedStatus] INT              NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tbl_EmailProcessor_Backup_CreatedOn] DEFAULT (getdate()) NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tbl_EmailProcessor_Backup_CreatedBy] DEFAULT ((1)) NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tbl_EmailProcessor_Backup_ModifiedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tbl_EmailProcessor_Backup_ModifiedBy] DEFAULT ((1)) NULL,
    [IsDeleted]       BIT              NULL,
    [History]         UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmailProcessor_Backup_History] DEFAULT (newid()) NULL
);

