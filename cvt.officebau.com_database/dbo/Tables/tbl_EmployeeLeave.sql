﻿CREATE TABLE [dbo].[tbl_EmployeeLeave] (
    [ID]                       INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]               INT              NOT NULL,
    [LeaveTypeID]              INT              NOT NULL,
    [FromDate]                 DATE             NOT NULL,
    [ToDate]                   DATE             NOT NULL,
    [Duration]                 TINYINT          NULL,
    [PurposeID]                INT              NULL,
    [RequesterRemarks]         VARCHAR (8000)   NULL,
    [AlternativeContactNo]     VARCHAR (50)     NULL,
    [ApproverID]               INT              NOT NULL,
    [RequestedDate]            DATETIME         CONSTRAINT [DF_tbl_EmployeeLeave_RequestedDate] DEFAULT (getdate()) NOT NULL,
    [StatusID]                 INT              NOT NULL,
    [ApproverRemarks]          VARCHAR (8000)   NULL,
    [ApprovedDate]             DATETIME         CONSTRAINT [DF_tbl_EmployeeLeave_ApprovedDate] DEFAULT (getdate()) NULL,
    [HRApproverID]             INT              NULL,
    [HRStatusID]               INT              NULL,
    [HRApprovedDate]           DATETIME         NULL,
    [HRRemarks]                VARCHAR (8000)   NULL,
    [DomainID]                 INT              NOT NULL,
    [HistoryID]                UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeLeave_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]                INT              CONSTRAINT [DF_tbl_EmployeeLeave_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]                DATETIME         CONSTRAINT [DF_tbl_EmployeeLeave_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]               INT              CONSTRAINT [DF_tbl_EmployeeLeave_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]               DATETIME         CONSTRAINT [DF_tbl_EmployeeLeave_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]                BIT              CONSTRAINT [DF_tbl_EmployeeLeave_IsDeleted] DEFAULT ((0)) NOT NULL,
    [ApproverStatusID]         INT              NULL,
    [IsHalfDay]                BIT              NULL,
    [Reason]                   VARCHAR (300)    NULL,
    [RevertRemarks]            VARCHAR (300)    NULL,
    [ReplacementEmployeeID]    INT              NULL,
    [IsAssetClearanceRequired] BIT              NULL,
    CONSTRAINT [PK_tbl_EmployeeLeave] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 90)
);

