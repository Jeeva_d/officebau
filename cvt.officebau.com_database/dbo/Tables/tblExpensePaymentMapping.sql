﻿CREATE TABLE [dbo].[tblExpensePaymentMapping] (
    [ID]               INT              IDENTITY (1, 1) NOT NULL,
    [ExpensePaymentID] INT              NULL,
    [ExpenseID]        INT              NULL,
    [Amount]           MONEY            NOT NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_tblExpensePaymentMapping_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_tblExpensePaymentMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        INT              CONSTRAINT [DF_tblExpensePaymentMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_tblExpensePaymentMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       INT              CONSTRAINT [DF_tblExpensePaymentMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]         INT              CONSTRAINT [DF_tblExpensePaymentMapping_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tblExpensePaymentMapping_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblExpensePaymentMapping] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tblExpensePaymentMapping_tblExpense] FOREIGN KEY ([ExpenseID]) REFERENCES [dbo].[tblExpense] ([ID]),
    CONSTRAINT [FK_tblExpensePaymentMapping_tblExpensePayment] FOREIGN KEY ([ExpensePaymentID]) REFERENCES [dbo].[tblExpensePayment] ([ID])
);

