﻿CREATE TABLE [dbo].[TDSComponent] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [SectionID]   INT              NOT NULL,
    [Code]        VARCHAR (1000)   NOT NULL,
    [Description] VARCHAR (8000)   NOT NULL,
    [Rules]       VARCHAR (20)     NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_TDSComponent_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_TDSComponent_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_TDSComponent_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_TDSComponent_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_TDSComponent_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              NOT NULL,
    [CALID]       INT              NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_TDSComponent_HistoryID] DEFAULT (newid()) NOT NULL,
    [Value]       DECIMAL (18, 2)  NULL,
    [FYID]        INT              NULL,
    CONSTRAINT [PK_TDSComponent] PRIMARY KEY CLUSTERED ([ID] ASC)
);

