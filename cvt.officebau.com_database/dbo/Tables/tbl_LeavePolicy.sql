﻿CREATE TABLE [dbo].[tbl_LeavePolicy] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [LeaveTypeID] INT              NOT NULL,
    [RegionID]    INT              NOT NULL,
    [NoOfLeaves]  TINYINT          NOT NULL,
    [Description] VARCHAR (8000)   NULL,
    [DomainID]    INT              NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_LeavePolicy_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_LeavePolicy_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_LeavePolicy_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_LeavePolicy_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_LeavePolicy_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_LeavePolicy_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tbl_LeavePolicy] PRIMARY KEY CLUSTERED ([ID] ASC)
);

