﻿CREATE TABLE [dbo].[tbl_ApplicationConfiguration] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [ConfigurationID] INT              NOT NULL,
    [BusinessUnitID]  INT              NOT NULL,
    [ConfigValue]     VARCHAR (100)    NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tbl_ApplicationConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tbl_ApplicationConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tbl_ApplicationConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tbl_ApplicationConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ApplicationConfiguration_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_ApplicationConfiguration] PRIMARY KEY CLUSTERED ([ConfigurationID] ASC, [BusinessUnitID] ASC) WITH (FILLFACTOR = 80)
);

