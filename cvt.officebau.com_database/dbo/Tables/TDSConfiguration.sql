﻿CREATE TABLE [dbo].[TDSConfiguration] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (50)     NOT NULL,
    [Description] VARCHAR (500)    NOT NULL,
    [Value]       VARCHAR (50)     NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_TDSConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_TDSConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_TDSConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_TDSConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_TDSConfiguration_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_TDSConfiguration_HistoryID] DEFAULT (newid()) NOT NULL,
    [RegionID]    INT              NULL,
    CONSTRAINT [PK_TDSConfiguration] PRIMARY KEY CLUSTERED ([ID] ASC)
);

