﻿CREATE TABLE [dbo].[tbl_ClaimsApprove] (
    [ID]                  INT              IDENTITY (1, 1) NOT NULL,
    [RequesterID]         INT              NOT NULL,
    [ApprovedAmount]      MONEY            NULL,
    [Comments]            VARCHAR (500)    NULL,
    [StatusID]            INT              NOT NULL,
    [ClaimItemID]         INT              NULL,
    [FinancialApproverID] INT              NULL,
    [FinApproverStatusID] INT              NULL,
    [IsDeleted]           BIT              CONSTRAINT [DF_tbl_ClaimsApprove_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]           INT              CONSTRAINT [DF_tbl_ClaimsApprove_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]           DATETIME         CONSTRAINT [DF_tbl_ClaimsApprove_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]          INT              CONSTRAINT [DF_tbl_ClaimsApprove_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]          DATETIME         CONSTRAINT [DF_tbl_ClaimsApprove_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]            INT              CONSTRAINT [DF_tbl_ClaimsApprove_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]           UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_ClaimsApprove_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_ClaimsApprove] PRIMARY KEY CLUSTERED ([ID] ASC)
);

