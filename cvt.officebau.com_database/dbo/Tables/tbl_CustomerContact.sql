﻿CREATE TABLE [dbo].[tbl_CustomerContact] (
    [ID]                    INT              IDENTITY (1, 1) NOT NULL,
    [FirstName]             VARCHAR (100)    NULL,
    [LastName]              VARCHAR (100)    NULL,
    [CustomerID]            INT              NOT NULL,
    [CustomerDesignationID] INT              NULL,
    [CustomerDepartmentID]  INT              NULL,
    [EmailID]               VARCHAR (200)    NULL,
    [ContactNo1]            VARCHAR (200)    NULL,
    [ContactNo2]            VARCHAR (200)    NULL,
    [IsDeleted]             BIT              CONSTRAINT [DF_tbl_CustomerContact_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]             DATETIME         CONSTRAINT [DF_tbl_CustomerContact_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             INT              CONSTRAINT [DF_tbl_CustomerContact_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]            DATETIME         CONSTRAINT [DF_tbl_CustomerContact_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            INT              CONSTRAINT [DF_tbl_CustomerContact_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]             UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_CustomerContact_HistoryID] DEFAULT (newid()) NOT NULL,
    [DomainID]              INT              CONSTRAINT [DF_tbl_CustomerContact_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_CustomerContact] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tbl_CustomerContact_tbl_Customer] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[tbl_Customer] ([ID])
);

