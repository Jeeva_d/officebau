﻿CREATE TABLE [dbo].[tbl_Pay_CompanyPayStructure] (
    [ID]               INT              IDENTITY (1, 1) NOT NULL,
    [PaystructureName] VARCHAR (100)    NULL,
    [EffectiveFrom]    DATETIME         NULL,
    [IsDeleted]        BIT              CONSTRAINT [DF_tbl_Pay_CompanyPayStructure_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]         INT              CONSTRAINT [DF_tbl_Pay_CompanyPayStructure_DomainID] DEFAULT ((0)) NOT NULL,
    [CreatedBy]        INT              CONSTRAINT [DF_tbl_Pay_CompanyPayStructure_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_tbl_Pay_CompanyPayStructure_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       INT              CONSTRAINT [DF_tbl_Pay_CompanyPayStructure_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_tbl_Pay_CompanyPayStructure_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]        UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Pay_CompanyPayStructure_HistoryID] DEFAULT (newid()) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

