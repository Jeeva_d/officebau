﻿CREATE TABLE [dbo].[Auth_Users] (
    [ID]           INT              IDENTITY (1, 1) NOT NULL,
    [WebLoginID]   INT              NOT NULL,
    [Username]     VARCHAR (50)     NOT NULL,
    [Password]     VARCHAR (100)    NOT NULL,
    [SysAdmin]     BIT              CONSTRAINT [DF_Auth_Users_SysAdmin] DEFAULT ((0)) NULL,
    [IsActive]     BIT              CONSTRAINT [DF_Auth_Users_IsActive] DEFAULT ((0)) NOT NULL,
    [RandomKey]    VARCHAR (150)    NOT NULL,
    [CreatedBy]    INT              CONSTRAINT [DF_Auth_Users_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedDate]  DATETIME         CONSTRAINT [DF_Auth_Users_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   INT              CONSTRAINT [DF_Auth_Users_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedDate] DATETIME         CONSTRAINT [DF_Auth_Users_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]    BIT              CONSTRAINT [DF_Auth_Users_IsDeleted] DEFAULT ((0)) NOT NULL,
    [HistoryID]    UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    CONSTRAINT [PK_Auth_Users] PRIMARY KEY CLUSTERED ([ID] ASC)
);

