﻿CREATE TABLE [dbo].[tbl_BiometricLogs] (
    [Id]           INT          IDENTITY (1, 1) NOT NULL,
    [AttendanceId] INT          NOT NULL,
    [PunchTime]    TIME (7)     NOT NULL,
    [Direction]    VARCHAR (10) NOT NULL,
    [LogType]      VARCHAR (10) NOT NULL,
    [CreatedBy]    INT          CONSTRAINT [DF_tbl_BiometricLogs_CreatedBy] DEFAULT ((1)) NULL,
    [CreatedOn]    DATETIME     CONSTRAINT [DF_tbl_BiometricLogs_CreatedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]   INT          CONSTRAINT [DF_tbl_BiometricLogs_ModifiedBy] DEFAULT ((1)) NULL,
    [ModifiedOn]   DATETIME     CONSTRAINT [DF_tbl_BiometricLogs_ModifiedOn] DEFAULT (getdate()) NULL,
    [IsDeleted]    BIT          CONSTRAINT [DF_tbl_BiometricLogs_IsDeleted] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_tbl_BiometricLogs] PRIMARY KEY CLUSTERED ([Id] ASC)
);

