﻿CREATE TABLE [dbo].[tblBookedBankBalance] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [BRSID]          INT              NOT NULL,
    [Amount]         MONEY            NULL,
    [ClosingBalance] MONEY            NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tblBookedBankBalance_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tblBookedBankBalance_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tblBookedBankBalance_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tblBookedBankBalance_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tblBookedBankBalance_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]       INT              CONSTRAINT [DF_tblBookedBankBalance_DomainID] DEFAULT ((1)) NOT NULL,
    [CALID]          INT              CONSTRAINT [DF_tblBookedBankBalance_CALID] DEFAULT ((1)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tblBookedBankBalance_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblBookedBankBalance] PRIMARY KEY CLUSTERED ([ID] ASC)
);

