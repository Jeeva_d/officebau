﻿CREATE TABLE [dbo].[Auth_GetQuote] (
    [ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [Client_Name]        VARCHAR (50)     NULL,
    [Client_Email]       VARCHAR (150)    NULL,
    [Client_Expectation] VARCHAR (MAX)    NULL,
    [Quote_Date]         DATETIME         NULL,
    [IsRead]             BIT              CONSTRAINT [DF_Auth_GetQuote_IsRead] DEFAULT ((1)) NULL,
    [IsDeleted]          BIT              CONSTRAINT [DF_Auth_GetQuote_IsDeleted] DEFAULT ((0)) NULL,
    [HistoryID]          UNIQUEIDENTIFIER DEFAULT (newid()) NULL,
    [IsEmailSend]        BIT              NULL,
    [AppPassword]        VARCHAR (10)     NULL,
    [IsReject]           BIT              NULL,
    CONSTRAINT [PK_Auth_GetQuote] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

