﻿CREATE TABLE [dbo].[tbl_Inventory] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [ReferenceID] INT              NOT NULL,
    [StockType]   VARCHAR (10)     NOT NULL,
    [ProductID]   INT              NOT NULL,
    [Qty]         INT              NOT NULL,
    [ItemID]      INT              NOT NULL,
    [Remarks]     VARCHAR (1000)   NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_Inventory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DomainID]    INT              CONSTRAINT [DF_tbl_Inventory_DomainID] DEFAULT ((1)) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_Inventory_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_Inventory_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_Inventory_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_Inventory_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Inventory_HistoryID] DEFAULT (newid()) NOT NULL,
    [MapId]       INT              NULL,
    [MapItemId]   INT              NULL,
    CONSTRAINT [PK_tbl_Inventory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

