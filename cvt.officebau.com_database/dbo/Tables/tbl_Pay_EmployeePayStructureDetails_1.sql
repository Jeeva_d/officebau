﻿CREATE TABLE [dbo].[tbl_Pay_EmployeePayStructureDetails] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [PayStructureId] INT              NOT NULL,
    [ComponentId]    INT              NOT NULL,
    [Amount]         DECIMAL (18, 2)  NULL,
    [Isdeleted]      BIT              CONSTRAINT [DF_tbl_Pay_EmployeePayStructureDetails_Isdeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_Pay_EmployeePayStructureDetails_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_Pay_EmployeePayStructureDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_Pay_EmployeePayStructureDetails_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_Pay_EmployeePayStructureDetails_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainId]       INT              CONSTRAINT [DF_tbl_Pay_EmployeePayStructureDetails_DomainId] DEFAULT ((1)) NOT NULL,
    [HistoryId]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Pay_EmployeePayStructureDetails_HistoryId] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_Pay_EmployeePayStructureDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tbl_Pay_EmployeePayStructureDetails_tbl_Pay_PayrollCompontents] FOREIGN KEY ([ComponentId]) REFERENCES [dbo].[tbl_Pay_PayrollCompontents] ([ID])
);

