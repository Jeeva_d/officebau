﻿CREATE TABLE [dbo].[tbl_ShareHolderAnalysisMapping] (
    [Id]               INT           NOT NULL,
    [EmployeeId]       INT           NOT NULL,
    [SalesLedgerIds]   VARCHAR (200) NULL,
    [ExpenceLedgerIds] VARCHAR (200) NULL,
    [CreatedOn]        DATETIME      NOT NULL,
    [CreatedBy]        INT           NOT NULL,
    [ModifiedOn]       DATETIME      NOT NULL,
    [ModifiedBy]       INT           NOT NULL,
    [DomainId]         INT           NOT NULL
);

