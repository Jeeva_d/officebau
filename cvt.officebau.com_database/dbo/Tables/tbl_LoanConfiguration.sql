﻿CREATE TABLE [dbo].[tbl_LoanConfiguration] (
    [ID]                    INT              IDENTITY (1, 1) NOT NULL,
    [DesignationMappingID]  INT              NOT NULL,
    [Amount]                MONEY            NOT NULL,
    [Tenure]                INT              NOT NULL,
    [InterestRate]          DECIMAL (32, 2)  NOT NULL,
    [DomainID]              INT              NOT NULL,
    [IsDeleted]             BIT              CONSTRAINT [DF_tbl_LoanConfiguration_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]             INT              CONSTRAINT [DF_tbl_LoanConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]             DATETIME         CONSTRAINT [DF_tbl_LoanConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            INT              CONSTRAINT [DF_tbl_LoanConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]            DATETIME         CONSTRAINT [DF_tbl_LoanConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]             UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_LoanConfiguration_HistoryID] DEFAULT (newid()) NOT NULL,
    [ForecloseInterestRate] DECIMAL (32, 2)  NULL,
    CONSTRAINT [PK_LoanConfiguration] PRIMARY KEY CLUSTERED ([ID] ASC)
);

