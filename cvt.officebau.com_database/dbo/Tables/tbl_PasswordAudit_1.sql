﻿CREATE TABLE [dbo].[tbl_PasswordAudit] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [LoginHistoryID] INT              NOT NULL,
    [ChangedOn]      DATETIME         NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_PasswordAudit_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]       INT              CONSTRAINT [DF_tbl_PasswordAudit_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_PasswordAudit_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_PasswordAudit] PRIMARY KEY CLUSTERED ([ID] ASC)
);

