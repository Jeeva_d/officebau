﻿CREATE TABLE [dbo].[tbl_FinancialYear] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [Name]          VARCHAR (100)    NOT NULL,
    [DisplayName]   VARCHAR (20)     NULL,
    [FinancialYear] VARCHAR (100)    NOT NULL,
    [FYDescription] VARCHAR (100)    NULL,
    [StartMonth]    INT              NOT NULL,
    [EndMonth]      INT              NOT NULL,
    [DomainID]      INT              CONSTRAINT [DF_tbl_FinancialYear_DomainID] DEFAULT ((1)) NOT NULL,
    [IsDeleted]     BIT              CONSTRAINT [DF_tbl_FinancialYear_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tbl_FinancialYear_CreatedOn] DEFAULT (getdate()) NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tbl_FinancialYear_CreatedBy] DEFAULT ((1)) NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tbl_FinancialYear_ModifiedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tbl_FinancialYear_ModifiedBy] DEFAULT ((1)) NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_FinancialYear_HistoryID] DEFAULT (newid()) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

