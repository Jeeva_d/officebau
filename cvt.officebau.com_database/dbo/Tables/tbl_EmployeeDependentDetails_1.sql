﻿CREATE TABLE [dbo].[tbl_EmployeeDependentDetails] (
    [ID]             INT              IDENTITY (1, 1) NOT NULL,
    [EmployeeID]     INT              NOT NULL,
    [Name]           VARCHAR (100)    NOT NULL,
    [GenderID]       INT              NOT NULL,
    [RelationshipID] INT              NOT NULL,
    [DOB]            DATETIME         NOT NULL,
    [Remarks]        VARCHAR (8000)   NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tbl_EmployeeDependentDetails_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_EmployeeDependentDetails_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_EmployeeDependentDetails_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_EmployeeDependentDetails_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_EmployeeDependentDetails_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]       INT              CONSTRAINT [DF_tbl_EmployeeDependentDetails_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeDependentDetails_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_EmployeeDependentDetails] PRIMARY KEY CLUSTERED ([ID] ASC)
);

