﻿CREATE TABLE [dbo].[tbl_CodeMaster] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Code]        VARCHAR (100)    NOT NULL,
    [Description] VARCHAR (8000)   NULL,
    [Type]        VARCHAR (100)    NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_CodeMaster_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_CodeMaster_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_CodeMaster_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_CodeMaster_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_CodeMaster_IsDeteted] DEFAULT ((0)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_CodeMaster_HistoryID] DEFAULT (newid()) NULL,
    [TypeID]      INT              NULL,
    CONSTRAINT [PK_tbl_CodeMaster] PRIMARY KEY CLUSTERED ([ID] ASC) WITH (FILLFACTOR = 80)
);

