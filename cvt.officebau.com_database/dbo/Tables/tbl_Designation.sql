﻿CREATE TABLE [dbo].[tbl_Designation] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100)    NOT NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tbl_Designation_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tbl_Designation_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tbl_Designation_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tbl_Designation_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tbl_Designation_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_Designation_HistoryID] DEFAULT (newid()) NOT NULL,
    [Remarks]    VARCHAR (800)    NULL,
    [DomainID]   INT              CONSTRAINT [DF_tbl_Designation_DomainID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_tbl_Designation] PRIMARY KEY CLUSTERED ([ID] ASC)
);

