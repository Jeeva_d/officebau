﻿CREATE TABLE [dbo].[tbl_EmployeeLeaveDateMapping] (
    [LeaveRequestID] INT              NOT NULL,
    [EmployeeID]     INT              NOT NULL,
    [LeaveDate]      DATE             NOT NULL,
    [DomainID]       INT              NOT NULL,
    [HistoryID]      UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeLeaveDateMapping_HistoryID] DEFAULT (newid()) NOT NULL,
    [CreatedBy]      INT              CONSTRAINT [DF_tbl_EmployeeLeaveDateMapping_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]      DATETIME         CONSTRAINT [DF_tbl_EmployeeLeaveDateMapping_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     INT              CONSTRAINT [DF_tbl_EmployeeLeaveDateMapping_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]     DATETIME         CONSTRAINT [DF_tbl_EmployeeLeaveDateMapping_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]      BIT              CONSTRAINT [DF_tbl_EmployeeLeaveDateMapping_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [FK_tbl_EmployeeLeaveDateMapping_tbl_EmployeeLeave] FOREIGN KEY ([LeaveRequestID]) REFERENCES [dbo].[tbl_EmployeeLeave] ([ID])
);

