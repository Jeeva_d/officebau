﻿CREATE TABLE [dbo].[tbl_EmployeeDesignationMapping] (
    [ID]            INT              IDENTITY (1, 1) NOT NULL,
    [DesignationID] INT              NOT NULL,
    [BandID]        INT              NOT NULL,
    [GradeID]       INT              NOT NULL,
    [DomainID]      INT              CONSTRAINT [DF_tbl_EmployeeDesignationMapping_DomainID] DEFAULT ((1)) NOT NULL,
    [IsDeleted]     BIT              CONSTRAINT [DF_tbl_EmployeeDesignationMapping_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]     DATETIME         CONSTRAINT [DF_tbl_EmployeeDesignationMapping_CreatedOn] DEFAULT (getdate()) NULL,
    [CreatedBy]     INT              CONSTRAINT [DF_tbl_EmployeeDesignationMapping_CreatedBy] DEFAULT ((1)) NULL,
    [ModifiedOn]    DATETIME         CONSTRAINT [DF_tbl_EmployeeDesignationMapping_ModifiedOn] DEFAULT (getdate()) NULL,
    [ModifiedBy]    INT              CONSTRAINT [DF_tbl_EmployeeDesignationMapping_ModifiedBy] DEFAULT ((1)) NULL,
    [HistoryID]     UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployeeDesignationMapping_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_EmployeeDesignationMapping] PRIMARY KEY CLUSTERED ([ID] ASC)
);

