﻿CREATE TABLE [dbo].[tbl_PayrollComponentConfiguration] (
    [ID]                 INT              IDENTITY (1, 1) NOT NULL,
    [PayrollComponentID] INT              NOT NULL,
    [BusinessUnitID]     INT              NOT NULL,
    [ConfigValue]        VARCHAR (100)    NULL,
    [CreatedBy]          INT              CONSTRAINT [DF_tbl_PayrollComponentConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]          DATETIME         CONSTRAINT [DF_tbl_PayrollComponentConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]         INT              CONSTRAINT [DF_tbl_PayrollComponentConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]         DATETIME         CONSTRAINT [DF_tbl_PayrollComponentConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [HistoryID]          UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_PayrollComponentConfiguration_HistoryID] DEFAULT (newid()) NULL,
    CONSTRAINT [PK_tbl_PayrollComponentConfiguration] PRIMARY KEY CLUSTERED ([PayrollComponentID] ASC, [BusinessUnitID] ASC) WITH (FILLFACTOR = 80)
);

