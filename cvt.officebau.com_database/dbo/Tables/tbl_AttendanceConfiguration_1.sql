﻿CREATE TABLE [dbo].[tbl_AttendanceConfiguration] (
    [ID]                INT              IDENTITY (1, 1) NOT NULL,
    [BusinessUnitID]    INT              NOT NULL,
    [BusinessStartTime] TIME (7)         NOT NULL,
    [BusinessEndTime]   TIME (7)         NOT NULL,
    [HalfDay]           TINYINT          NOT NULL,
    [FullDay]           TINYINT          NOT NULL,
    [DomainID]          INT              NOT NULL,
    [HistoryID]         UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_AttendanceConfiguration_HistoryID] DEFAULT (newid()) NOT NULL,
    [IsDeleted]         BIT              CONSTRAINT [DF_tbl_AttendanceConfiguration_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]         INT              CONSTRAINT [DF_tbl_AttendanceConfiguration_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]         DATETIME         CONSTRAINT [DF_tbl_AttendanceConfiguration_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]        INT              CONSTRAINT [DF_tbl_AttendanceConfiguration_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]        DATETIME         CONSTRAINT [DF_tbl_AttendanceConfiguration_ModifiedOn] DEFAULT (getdate()) NOT NULL
);

