﻿CREATE TABLE [dbo].[tblSOItem] (
    [ID]              INT              IDENTITY (1, 1) NOT NULL,
    [SOID]            INT              NOT NULL,
    [ProductID]       INT              NULL,
    [ItemDescription] VARCHAR (1000)   NULL,
    [QTY]             INT              NOT NULL,
    [InvoicedQTY]     INT              NULL,
    [Rate]            MONEY            NOT NULL,
    [SGST]            DECIMAL (18, 2)  NULL,
    [SGSTAmount]      MONEY            NULL,
    [CGST]            DECIMAL (18, 2)  NULL,
    [CGSTAmount]      MONEY            NULL,
    [IsDeleted]       BIT              CONSTRAINT [DF_tblSOItem_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]       DATETIME         CONSTRAINT [DF_tblSOItem_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       INT              CONSTRAINT [DF_tblSOItem_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]      DATETIME         CONSTRAINT [DF_tblSOItem_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      INT              CONSTRAINT [DF_tblSOItem_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]        INT              CONSTRAINT [DF_tblSOItem_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]       UNIQUEIDENTIFIER CONSTRAINT [DF_tblSOItem_HistoryID] DEFAULT (newid()) NOT NULL,
    [IsProduct]       INT              CONSTRAINT [DF_tblSOItem_IsProduct] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_tblSOItem] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_tblSOItem_tblSalesOrder] FOREIGN KEY ([SOID]) REFERENCES [dbo].[tblSalesOrder] ([ID])
);

