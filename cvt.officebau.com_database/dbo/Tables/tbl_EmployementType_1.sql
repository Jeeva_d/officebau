﻿CREATE TABLE [dbo].[tbl_EmployementType] (
    [ID]          INT              IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (50)     NOT NULL,
    [Description] VARCHAR (1000)   NULL,
    [IsDeleted]   BIT              CONSTRAINT [DF_tbl_EmployementType_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedBy]   INT              CONSTRAINT [DF_tbl_EmployementType_CreatedBy] DEFAULT ((1)) NOT NULL,
    [CreatedOn]   DATETIME         CONSTRAINT [DF_tbl_EmployementType_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  INT              CONSTRAINT [DF_tbl_EmployementType_ModifiedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn]  DATETIME         CONSTRAINT [DF_tbl_EmployementType_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [DomainID]    INT              CONSTRAINT [DF_tbl_EmployementType_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]   UNIQUEIDENTIFIER CONSTRAINT [DF_tbl_EmployementType_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tbl_EmployementType] PRIMARY KEY CLUSTERED ([ID] ASC)
);

