﻿CREATE TABLE [dbo].[tblSearchHistory] (
    [ID]         INT              IDENTITY (1, 1) NOT NULL,
    [UserID]     INT              NULL,
    [FormName]   VARCHAR (100)    NULL,
    [Parameter]  VARCHAR (100)    NULL,
    [Value]      VARCHAR (100)    NULL,
    [IsDeleted]  BIT              CONSTRAINT [DF_tblSearchHistory_IsDeleted] DEFAULT ((0)) NULL,
    [CreatedOn]  DATETIME         CONSTRAINT [DF_tblSearchHistory_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]  INT              CONSTRAINT [DF_tblSearchHistory_CreatedBy] DEFAULT ((1)) NOT NULL,
    [ModifiedOn] DATETIME         CONSTRAINT [DF_tblSearchHistory_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy] INT              CONSTRAINT [DF_tblSearchHistory_ModifedBy] DEFAULT ((1)) NOT NULL,
    [DomainID]   INT              CONSTRAINT [DF_tblSearchHistory_DomainID] DEFAULT ((1)) NOT NULL,
    [HistoryID]  UNIQUEIDENTIFIER CONSTRAINT [DF_tblSearchHistory_HistoryID] DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_tblSearchHistory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

