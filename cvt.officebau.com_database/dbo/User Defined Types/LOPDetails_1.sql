﻿CREATE TYPE [dbo].[LOPDetails] AS TABLE (
    [ID]            INT            NULL,
    [MonthID]       TINYINT        NULL,
    [YEAR]          SMALLINT       NULL,
    [EmployeeID]    INT            NULL,
    [LOPDays]       DECIMAL (5, 2) NULL,
    [PayrollStatus] VARCHAR (20)   NULL);

