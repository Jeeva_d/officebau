﻿CREATE TYPE [dbo].[FandFComponentDetailsList] AS TABLE (
    [id]                INT           NULL,
    [FandFId]           INT           NULL,
    [Type]              VARCHAR (100) NULL,
    [Description]       VARCHAR (500) NULL,
    [Amount]            MONEY         NULL,
    [IsEditableAmount]  BIT           NULL,
    [Remarks]           VARCHAR (500) NULL,
    [IsEditableRemarks] BIT           NULL,
    [IsDeleted]         BIT           NULL,
    [Ordinal]           INT           NULL);

