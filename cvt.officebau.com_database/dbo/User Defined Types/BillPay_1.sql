﻿CREATE TYPE [dbo].[BillPay] AS TABLE (
    [ID]        INT   NULL,
    [ExpenseID] INT   NULL,
    [PayAmount] MONEY NULL,
    [IsDeleted] BIT   NULL);

