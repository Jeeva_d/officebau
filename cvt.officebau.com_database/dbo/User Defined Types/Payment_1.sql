﻿CREATE TYPE [dbo].[Payment] AS TABLE (
    [ID]        INT   NULL,
    [Amount]    MONEY NULL,
    [IsDeleted] BIT   NULL,
    [RefID]     INT   NULL);

