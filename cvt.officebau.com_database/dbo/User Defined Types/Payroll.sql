﻿CREATE TYPE [dbo].[Payroll] AS TABLE (
    [ID]                     INT          NULL,
    [ComponentId]            INT          NULL,
    [EmployeePayStructureId] INT          NULL,
    [Amount]                 DECIMAL (18) NULL,
    [EmployeeId]             INT          NULL);

