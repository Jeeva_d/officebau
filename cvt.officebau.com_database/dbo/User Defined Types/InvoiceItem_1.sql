﻿CREATE TYPE [dbo].[InvoiceItem] AS TABLE (
    [ID]          INT             NULL,
    [ProductID]   INT             NULL,
    [ItemRemarks] VARCHAR (1000)  NULL,
    [Qty]         INT             NULL,
    [Rate]        MONEY           NULL,
    [SGST]        DECIMAL (18, 2) NULL,
    [SGSTAmount]  MONEY           NULL,
    [CGST]        DECIMAL (18, 2) NULL,
    [CGSTAmount]  MONEY           NULL,
    [IsDeleted]   BIT             NULL,
    [InvoiceID]   INT             NULL,
    [IsProduct]   INT             NULL,
    [SoItemId]    INT             NULL);

