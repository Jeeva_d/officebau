﻿CREATE TYPE [dbo].[TDSDetails] AS TABLE (
    [ID]             INT             NULL,
    [MonthId]        INT             NULL,
    [YEAR]           INT             NULL,
    [EmployeeID]     INT             NULL,
    [TDSAmount]      DECIMAL (18, 5) NULL,
    [BaseLocationID] INT             NULL);

