﻿CREATE TYPE [dbo].[CFSUpload] AS TABLE (
    [PayerName]       VARCHAR (200)  NULL,
    [ShippingBillNo]  VARCHAR (200)  NULL,
    [ContainerNo]     VARCHAR (200)  NULL,
    [Size]            INT            NULL,
    [ExporterName]    VARCHAR (800)  NULL,
    [ContainerStatus] VARCHAR (200)  NULL,
    [GateInDate]      DATETIME       NULL,
    [LinerAgent]      VARCHAR (800)  NULL,
    [ConsigneeName]   VARCHAR (800)  NULL,
    [TransporterName] VARCHAR (800)  NULL,
    [TrailerNo]       VARCHAR (800)  NULL,
    [CargoDesciption] VARCHAR (8000) NULL,
    [Scanning]        VARCHAR (800)  NULL,
    [BOENo]           VARCHAR (500)  NULL,
    [BOEDate]         DATETIME       NULL,
    [IGMNo]           VARCHAR (200)  NULL,
    [VesselNo]        VARCHAR (400)  NULL,
    [CHANo]           VARCHAR (400)  NULL);

