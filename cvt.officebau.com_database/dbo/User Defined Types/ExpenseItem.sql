﻿CREATE TYPE [dbo].[ExpenseItem] AS TABLE (
    [ID]                 INT            NULL,
    [ProductID]          INT            NULL,
    [ExpenseItemRemarks] VARCHAR (1000) NULL,
    [QTY]                INT            NULL,
    [Amount]             MONEY          NULL,
    [IsDeleted]          BIT            NULL,
    [ExpenseID]          INT            NULL,
    [TypeID]             INT            NULL);

