﻿CREATE TYPE [dbo].[InvoiceReceivable] AS TABLE (
    [ID]             INT   NULL,
    [InvoiceID]      INT   NULL,
    [ReceivedAmount] MONEY NULL,
    [IsDeleted]      BIT   NULL);

