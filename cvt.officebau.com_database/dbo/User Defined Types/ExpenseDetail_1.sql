﻿CREATE TYPE [dbo].[ExpenseDetail] AS TABLE (
    [ID]                   INT             NULL,
    [LedgerID]             INT             NULL,
    [ExpenseDetailRemarks] VARCHAR (1000)  NULL,
    [SGST]                 DECIMAL (18, 2) NULL,
    [SGSTAmount]           MONEY           NULL,
    [CGST]                 DECIMAL (18, 2) NULL,
    [CGSTAmount]           MONEY           NULL,
    [Qty]                  INT             NULL,
    [Amount]               MONEY           NULL,
    [IsDeleted]            BIT             NULL,
    [ExpenseID]            INT             NULL,
    [IsProduct]            BIT             NULL,
    [POItemID]             INT             NULL);

