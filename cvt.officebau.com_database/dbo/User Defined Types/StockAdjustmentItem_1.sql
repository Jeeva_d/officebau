﻿CREATE TYPE [dbo].[StockAdjustmentItem] AS TABLE (
    [ID]              INT             NULL,
    [AdjustmentID]    INT             NOT NULL,
    [ItemProductID]   INT             NOT NULL,
    [AdjustedQty]     INT             NOT NULL,
    [ItemStockonHand] DECIMAL (18, 2) NOT NULL,
    [RemainingQty]    INT             NOT NULL,
    [IsDeleted]       BIT             NOT NULL);

