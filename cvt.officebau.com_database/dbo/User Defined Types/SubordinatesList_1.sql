﻿CREATE TYPE [dbo].[SubordinatesList] AS TABLE (
    [Name]      VARCHAR (200) NULL,
    [IsDeleted] BIT           NULL);

