﻿CREATE TYPE [dbo].[TimeWatchUpload] AS TABLE (
    [BiometricID] VARCHAR (20) NULL,
    [Date]        DATE         NULL,
    [Punch1]      VARCHAR (20) NULL,
    [Punch2]      VARCHAR (20) NULL,
    [Punch3]      VARCHAR (20) NULL,
    [Punch4]      VARCHAR (20) NULL);

