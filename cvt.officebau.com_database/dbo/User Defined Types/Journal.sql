﻿CREATE TYPE [dbo].[Journal] AS TABLE (
    [ID]              INT            NULL,
    [Credit]          MONEY          NULL,
    [Debit]           MONEY          NULL,
    [LedgerProductID] INT            NULL,
    [IsDeleted]       BIT            NULL,
    [Type]            VARCHAR (1000) NULL);

