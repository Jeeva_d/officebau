﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class PayableRepository : BaseRepository
    {
        #region Search

        #region ExpensePayable

        public List<ExpenseBo> SearchExpensePayable(SearchParamsBo searchParamsBo)
        {
            List<ExpenseBo> transactionList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EXPENSEPAYABLE);
                Database.AddInParameter(dbCommand, DBParam.Input.VendorName, DbType.String, searchParamsBo.VendorName);
                Database.AddInParameter(dbCommand, DBParam.Input.LedgerName, DbType.String, searchParamsBo.LedgerName);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, searchParamsBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        transactionList.Add(
                            new ExpenseBo
                            {
                                VendorId = Utility.CheckDbNull<int>(reader[DBParam.Output.VendorID]),
                                VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.BilledAmount]),
                                PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaidAmount]),
                                Amount = Utility.CheckDbNull<decimal>(reader["Holdings"]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status])
                            });
                    }
                }
            }

            return transactionList;
        }

        #endregion ExpensePayable

        #region IncomePayable

        public List<IncomeBo> SearchIncomePayable(SearchParamsBo searchParamsBo)
        {
            List<IncomeBo> incomeList = new List<IncomeBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_INCOMEPAYABLE);

                Database.AddInParameter(dbCommand, DBParam.Input.CustomerName, DbType.String, searchParamsBo.CustomerName);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, searchParamsBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeList.Add(
                            new IncomeBo
                            {
                                CustomerId = Utility.CheckDbNull<int>(reader[DBParam.Output.CustomerID]),
                                CustomerName = Utility.CheckDbNull<string>(reader[DBParam.Output.Customer]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Booked]),
                                ReceivedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ReceivedAmount]),
                                Amount = Utility.CheckDbNull<decimal>(reader["Holdings"]),

                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status])
                            });
                    }
                }
            }

            return incomeList;
        }

        #endregion IncomePayable

        #region EXPENSE PAID BY VENDOR

        public List<ExpenseBo> SearchExpensePaidbyVendor(ExpenseBo expenseBo)
        {
            List<ExpenseBo> transactionList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EXPENSEPAIDBYVENDOR);

                Database.AddInParameter(dbCommand, DBParam.Input.VendorID, DbType.Int32, expenseBo.VendorId);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, expenseBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, expenseBo.StartDate);
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, expenseBo.EndDate);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        transactionList.Add(
                            new ExpenseBo
                            {
                                ExpensePayId = Utility.CheckDbNull<int>(reader[DBParam.Output.PayId]),
                                DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PayDate]),
                                PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaidAmount]),
                                Reference = Utility.CheckDbNull<string>(reader[DBParam.Output.Reference]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.Bill]),
                                ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                                ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                                VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName])
                            });
                    }
                }
            }

            return transactionList;
        }

        #endregion EXPENSE PAID BY VENDOR

        #region Received from Customer

        public List<IncomeBo> SearchReceivablebyCustomer(IncomeBo incomeBo)
        {
            List<IncomeBo> searchList = new List<IncomeBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_RECEIVABLEBYCUSTOMER);

                Database.AddInParameter(dbCommand, DBParam.Input.CustomerID, DbType.Int32, incomeBo.CustomerId);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, incomeBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, incomeBo.StartDate);
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, incomeBo.EndDate);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        searchList.Add(
                            new IncomeBo
                            {
                                InvoicePaymentId = Utility.CheckDbNull<int>(reader[DBParam.Output.RecID]),
                                DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PaymentDate]),
                                ReceivedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.RecAmount]),
                                InvoiceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo]),
                                ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                                ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy])
                            });
                    }
                }
            }

            return searchList;
        }

        #endregion Received from Customer

        #region RPT

        public List<ExpenseBo> SearchRptParty()
        {
            List<ExpenseBo> transactionList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SearchRPTParty);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        transactionList.Add(
                            new ExpenseBo
                            {
                                VendorId = Utility.CheckDbNull<int>(reader[DBParam.Output.CustomerID]),
                                VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.Customer]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Booked]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Type])
                            });
                    }
                }
            }

            return transactionList;
        }

        public List<ExpenseBo> Searchrptpartydetails(int vendorId, string screenType)
        {
            List<ExpenseBo> transactionList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.Searchrptpartydetails);
                Database.AddInParameter(dbCommand, DBParam.Input.PartyID, DbType.Int32, vendorId);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, screenType);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        transactionList.Add(
                            new ExpenseBo
                            {
                                ExpensePayId = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.Type])
                            });
                    }
                }
            }

            return transactionList;
        }

        #endregion RPT

        #endregion Search
    }
}