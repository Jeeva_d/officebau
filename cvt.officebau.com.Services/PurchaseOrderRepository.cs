﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace cvt.officebau.com.Services
{
    public class PurchaseOrderRepository : BaseRepository
    {
        #region Search PurchaseOrder

        public List<ExpenseBo> SearchPurchaseOrder(SearchParamsBo searchParamsBo)
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchPurchaseOrder");
                Database.AddInParameter(dbCommand, DBParam.Input.VendorName, DbType.String, searchParamsBo.VendorName);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, searchParamsBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount);
                Database.AddInParameter(dbCommand, DBParam.Input.LedgerName, DbType.String, searchParamsBo.LedgerName);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBoList.Add(
                            new ExpenseBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                BillDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.BillDate]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                                VendorId = Utility.CheckDbNull<int>(reader[DBParam.Output.VendorID]),
                                VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName]),
                                Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                FileId = Utility.CheckDbNull<string>(reader[DBParam.Output.HistoryID]),
                                PoRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                                CreatedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.CreatedBy]),
                                CostCenterId = Utility.CheckDbNull<int>(reader[DBParam.Output.CostCenterID])
                            });
                    }
                }
            }

            return expenseBoList;
        }

        public ExpenseBo GetPoDetails(int id)
        {
            ExpenseBo expenseBo = new ExpenseBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetPOdetail");
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBo = new ExpenseBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            BillDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.BillDate]),
                            VendorId = Utility.CheckDbNull<int>(reader[DBParam.Output.VendorID]),
                            VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName].ToString()),
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.BillingAddress].ToString()),
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms].ToString()),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description].ToString()),
                            BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo].ToString()),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            FileUpload = Utility.CheckDbNull<string>(reader[DBParam.Output.FileName]),
                            CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.CreatedOn]),
                            FileId = Utility.CheckDbNull<string>(reader[DBParam.Output.HistoryID]),
                            Reference = Utility.CheckDbNull<string>(reader[DBParam.Output.Reference]),
                            CostCenterId = Utility.CheckDbNull<int>(reader[DBParam.Output.CostCenterID])
                        };
                    }
                }
            }

            expenseBo.ExpenseDetail = SearchPoDetailsList(id);
            return expenseBo;
        }

        private List<ExpenseDetailsBO> SearchPoDetailsList(int id)
        {
            List<ExpenseDetailsBO> expenseDetailsBoList = new List<ExpenseDetailsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchPODetailList");

                Database.AddInParameter(dbCommand, "@POID", DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseDetailsBoList.Add(new ExpenseDetailsBO
                        {
                            LedgerID = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID]),
                            ExpenseDetailRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            ID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Qty = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            IsProduct = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsProduct]),
                            SGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTPERCENT]),
                            CGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGSTPERCENT]),
                            SGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            CGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            TotalAmount = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) * Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]) + Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]) + Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger")
                        });
                        expenseDetailsBoList[expenseDetailsBoList.Count - 1].LedgerList.Find(a => a.Value == expenseDetailsBoList[expenseDetailsBoList.Count - 1].LedgerID.ToString()).Selected = true;
                    }
                }

                if (expenseDetailsBoList.Count == 0)
                {
                    expenseDetailsBoList.Add(new ExpenseDetailsBO
                    {
                        LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger")
                    });
                }
            }

            return expenseDetailsBoList;
        }

        public List<ExpenseDetailsBO> SearchPoBillDetailList(string id)
        {
            List<ExpenseDetailsBO> expenseDetailsBoList = new List<ExpenseDetailsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchPOBillDetailList");

                Database.AddInParameter(dbCommand, "@POID", DbType.String, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseDetailsBoList.Add(new ExpenseDetailsBO
                        {
                            LedgerID = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID]),
                            ExpenseDetailRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            POIDItemID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Qty = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            IsProduct = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsProduct]),
                            SGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTPERCENT]),
                            CGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGSTPERCENT]),

                            SGSTAmount = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                         Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]) / 100 *
                                         Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTPERCENT]),

                            CGSTAmount = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                         Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]) / 100 *
                                         Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGSTPERCENT]),

                            TotalAmount = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                          Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]) + Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                          Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]) / 100 *
                                          Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTPERCENT]) +
                                          Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                          Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]) / 100 *
                                          Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTPERCENT]),

                            LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger")
                        });
                        expenseDetailsBoList[expenseDetailsBoList.Count - 1].LedgerList.Find(a => a.Value == expenseDetailsBoList[expenseDetailsBoList.Count - 1].LedgerID.ToString()).Selected = true;
                    }
                }

                if (expenseDetailsBoList.Count == 0)
                {
                    expenseDetailsBoList.Add(new ExpenseDetailsBO
                    {
                        LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger")
                    });
                }
            }

            return expenseDetailsBoList;
        }

        public string ManagePo(ExpenseBo expenseBo)
        {
            string userMessage = string.Empty;
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);

            switch (expenseBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    userMessage = CreatePo(expenseBo, con);
                    break;

                case Operations.Update:
                    userMessage = UpdatePo(expenseBo, con);
                    break;

                case Operations.Delete:
                    userMessage = DeleteBill(expenseBo, con);
                    break;
            }

            return userMessage;
        }

        private string CreatePo(ExpenseBo expenseBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand("CreatePO") { CommandType = CommandType.StoredProcedure, Connection = con };
            FillBillParameters(expenseBo, cmd);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string UpdatePo(ExpenseBo expenseBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand("UpdatePO") { CommandType = CommandType.StoredProcedure, Connection = con };
            FillBillParameters(expenseBo, cmd);
            cmd.Parameters.AddWithValue(DBParam.Input.ID, expenseBo.Id);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string DeleteBill(ExpenseBo expenseBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand("DeletePO") { CommandType = CommandType.StoredProcedure, Connection = con };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, expenseBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private void FillBillParameters(ExpenseBo expenseBo, SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(expenseBo.Remarks));
            cmd.Parameters.AddWithValue(DBParam.Input.Date, expenseBo.BillDate);
            cmd.Parameters.AddWithValue(DBParam.Input.TotalAmount, expenseBo.TotalAmount);
            cmd.Parameters.AddWithValue(DBParam.Input.BillNo, ValueOrDBNullIfZero(expenseBo.BillNo));
            cmd.Parameters.AddWithValue(DBParam.Input.ExpenseDetail, Utility.ToDataTable(expenseBo.ExpenseDetail, "ExpenseDetail"));
            cmd.Parameters.AddWithValue(DBParam.Input.UploadFile, ValueOrDBNullIfZero(expenseBo.FileUpload));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.FileID, ValueOrDBNullIfZero(expenseBo.FileId));
            cmd.Parameters.AddWithValue(DBParam.Input.Reference, ValueOrDBNullIfZero(expenseBo.Reference));
            cmd.Parameters.AddWithValue(DBParam.Input.VendorID, expenseBo.VendorId != 0 ? expenseBo.VendorId : 0);
            cmd.Parameters.AddWithValue(DBParam.Input.CostCenterID, expenseBo.CostCenterId);
        }

        private object ValueOrDBNullIfZero(string val)
        {
            if (string.IsNullOrEmpty(val))
            {
                return DBNull.Value;
            }

            return val;
        }

        #endregion Search PurchaseOrder
    }
}