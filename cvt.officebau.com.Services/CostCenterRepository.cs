﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class CostCenterRepository : BaseRepository
    {
        #region Get CostCenter

        public CostCenterBo GetCostCenter(int id)
        {
            CostCenterBo costCenterBo = new CostCenterBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_COSTCENTER);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        costCenterBo = new CostCenterBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Code = Utility.CheckDbNull<string>(reader[DBParam.Output.Name].ToString()),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks].ToString())
                        };
                    }
                }
            }

            return costCenterBo;
        }

        #endregion Get CostCenter

        #region Manage CostCenter

        public string ManageCostCenter(CostCenterBo costCenterBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_COSTCENTER);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, costCenterBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, costCenterBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, costCenterBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, costCenterBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                costCenterBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return costCenterBo.UserMessage;
        }

        #endregion Manage CostCenter

        #region Get DashBoard Payable

        public List<ExpenseBo> GetCompareCostRevenue(SearchParamsBo searchParamsBo)
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETCOMPARECOSTREVENUECENTER);

                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.DateTime, searchParamsBo.StartDate);
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.DateTime, searchParamsBo.EndDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, searchParamsBo.LedgerName);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
               
                    using (IDataReader reader = Database.ExecuteReader(dbCommand))
                    {
                        while (reader.Read())
                        {
                            expenseBoList.Add(new ExpenseBo
                            {
                                BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.CostCenter]),
                                PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Payable]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Receivables]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount])
                            });
                        }
                    }
            }

            return expenseBoList;
        }

        #endregion Get DashBoard Payable

        public List<CostCenterBo> SearchCostCenterList(CostCenterBo costCenterBo)
        {
            List<CostCenterBo> costCenterBoList = new List<CostCenterBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_COSTCENTERLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, costCenterBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        costCenterBoList.Add(
                            new CostCenterBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks])
                            });
                    }
                }
            }

            return costCenterBoList;
        }
    }
}