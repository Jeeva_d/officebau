﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class JournelRepository : BaseRepository
    {
        #region Manage Journal

        public string ManageJournel(JournelBO journelBo)
        {
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);
            string usermessage;

            SqlCommand cmd = new SqlCommand(Constants.MANAGE_JOURNAL)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, journelBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.JournalNo, ValueOrDBNullIfZero(journelBo.JournelNo));
            cmd.Parameters.AddWithValue(DBParam.Input.JournalDate, journelBo.Date);
            cmd.Parameters.AddWithValue(DBParam.Input.IsDeleted, journelBo.IsDeleted);
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(journelBo.Description));
            cmd.Parameters.AddWithValue(DBParam.Input.Journal, Utility.ToDataTable(journelBo.JournelDetail, "Journal"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            con.Open();
            usermessage = cmd.ExecuteScalar().ToString();

            return usermessage;
        }

        #endregion Manage Journal

        #region Search Journal

        public List<JournelBO> SearchJournal(JournelBO journelBo)
        {
            List<JournelBO> journalList = new List<JournelBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_JOURNAL);
                Database.AddInParameter(dbCommand, DBParam.Input.ScreenType, DbType.String, 'S');
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        journalList.Add(
                            new JournelBO
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                JournelNo = Utility.CheckDbNull<string>(reader[DBParam.Output.JournalNo]),
                                UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.LEDGER]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.JournalDate]),
                                CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.CreatedOn]),
                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description])
                            });
                    }
                }
            }

            return journalList;
        }

        #endregion Search Journal

        #region Supporting Function

        private object ValueOrDBNullIfZero(string val)
        {
            if (string.IsNullOrEmpty(val))
            {
                return DBNull.Value;
            }

            return val;
        }

        #endregion Supporting Function

        #region Get Dropdown for Ledger and Product

        public List<SelectListItem> Getdropdown_ledgerproduct()
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETDROPDOWN_LEDGERPRODUCT);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Code]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList.OrderBy(a => a.Text).ToList();
        }

        #endregion Get Dropdown for Ledger and Product

        #region Get Party Name

        public List<SelectListItem> GetPartyName()
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETPARTYNAME);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #endregion Get Party Name

        #region Get Journal

        public JournelBO GetJournal(int id)
        {
            JournelBO journelBo = new JournelBO();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_JOURNAL);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        journelBo = new JournelBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.JournalDate]),
                            JournelNo = Utility.CheckDbNull<string>(reader[DBParam.Output.JournalNo]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.AutoJournalNo]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description])
                        };
                    }
                }
            }

            journelBo.JournelDetail = SearchJournalDetailList(journelBo.UserMessage);
            return journelBo;
        }

        public List<JournelDetailBO> SearchJournalDetailList(string journelNo)
        {
            List<JournelDetailBO> journelDetailList = new List<JournelDetailBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_JOURNALDETAILLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.AutoJournelNo, DbType.String, journelNo);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        JournelDetailBO journelDetailBo = new JournelDetailBO
                        {
                            ID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            LedgerProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerProductID]),
                            Debit = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Debit]),
                            Credit = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Credit]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            LedgerProductList = Getdropdown_ledgerproduct(),
                            NameList = GetPartyName(),
                            Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type])
                        };

                        journelDetailList.Add(journelDetailBo);
                        if (journelDetailList[journelDetailList.Count - 1].LedgerProductID != 0)
                        {
                            journelDetailList[journelDetailList.Count - 1].LedgerProductList.AddRange(journelDetailList[journelDetailList.Count - 1].NameList);
                            journelDetailList[journelDetailList.Count - 1]
                                .LedgerProductList.Find(a => a.Value == journelDetailList[journelDetailList.Count - 1].LedgerProductID.ToString() && a.Text.Contains(journelDetailList[journelDetailList.Count - 1].Type)).Selected = true;
                        }
                    }
                }

                if (journelDetailList.Count == 0)
                {
                    journelDetailList.Add(new JournelDetailBO
                    {
                        LedgerProductList = Getdropdown_ledgerproduct(),
                        NameList = GetPartyName()
                    });
                }
            }

            return journelDetailList;
        }

        #endregion Get Journal
    }
}