﻿using cvt.officebau.com.MessageConstants;
using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class EmployeeLoanRepository : BaseRepository
    {
        #region Supporting Functions

        public int GetLoanStatusId(string type, string statusName)
        {
            int statusId;

            using (FSMEntities context = new FSMEntities())
            {
                statusId = Convert.ToInt32(context.tbl_Status.Where(b => !b.IsDeleted && b.Type == type && b.Code == statusName).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
            }

            return statusId;
        }

        #endregion Supporting Functions

        #region GetBaseLocationID

        public int GetBaseLocationId(int employeeId)
        {
            FSMEntities context = new FSMEntities();
            int baseLocationId = Convert.ToInt32(context.tbl_EmployeeMaster.Where(b => b.ID == employeeId && !b.IsDeleted && !(b.IsActive ?? false)).OrderByDescending(b => b.ID).Select(b => b.BaseLocationID).FirstOrDefault());
            return baseLocationId;
        }

        #endregion GetBaseLocationID

        public List<SelectListItem> GetStatusCode()
        {
            List<SelectListItem> dependanceDropDownList = new List<SelectListItem>();
            using (FSMEntities context = new FSMEntities())
            {
                List<cvt.officebau.com.ViewModels.Entity> list = (from s in context.tbl_Status
                                                                  where !s.IsDeleted && (s.Type == "Amortization" || s.Type == "Loan")
                                                                  select new cvt.officebau.com.ViewModels.Entity
                                                                  {
                                                                      Name = s.Code,
                                                                      Id = s.ID
                                                                  }).ToList();

                dependanceDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                foreach (cvt.officebau.com.ViewModels.Entity item in list)
                {
                    dependanceDropDownList.Add(new SelectListItem
                    {
                        Text = item.Name,
                        Value = item.Id.ToString()
                    });
                }

                return dependanceDropDownList;
            }
        }

        #region Loan Request

        public List<EmployeeLoanBo> GetLoanRequestList(EmployeeLoanBo employeeLoanBo)
        {
            List<EmployeeLoanBo> employeeLoanBoList = new List<EmployeeLoanBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_LOANREQUEST);

                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.Int32, employeeLoanBo.TypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeLoanBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeLoanBoList.Add(
                            new EmployeeLoanBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                TypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.Type]),
                                Amount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]), 0),
                                TotalAmount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]), 0),
                                ApproverName = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovarName]),
                                ApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                                LoanStatusId = new cvt.officebau.com.ViewModels.Entity
                                {
                                    Id = Utility.CheckDbNull<int>(reader[DBParam.Output.StatusID]),
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Status])
                                },
                                RequestedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.RequestedDate])
                            });
                    }
                }
            }

            return employeeLoanBoList;
        }

        public EmployeeLoanBo GetLoanRequest(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeLoanBo result = new EmployeeLoanBo();
                if (id != 0)
                {
                    result = (from c in context.tbl_LoanRequest
                              join st in context.tbl_Status on c.StatusID equals st.ID
                              join appst in context.tbl_Status on c.ApproverStatusID equals appst.ID into appsta
                              from appstatus in appsta.DefaultIfEmpty()
                              join finst in context.tbl_Status on c.FinancialStatusID equals finst.ID into finsta
                              from finstatus in finsta.DefaultIfEmpty()
                              join emp in context.tbl_EmployeeMaster on c.CreatedBy equals emp.ID
                              join mod in context.tbl_EmployeeMaster on c.EmployeeID equals mod.ID
                              join empca in context.tbl_EmployeeMaster on c.ApproverID equals empca.ID into empca
                              from empmca in empca.DefaultIfEmpty()
                              join empcs in context.tbl_EmployeeMaster on c.FinancialApproverID equals empcs.ID into empcs
                              from empmcs in empcs.DefaultIfEmpty()
                              join bu in context.tbl_BusinessUnit on mod.BaseLocationID equals bu.ID into business
                              from bUnit in business.DefaultIfEmpty()
                              where c.ID == id
                              select new EmployeeLoanBo
                              {
                                  Id = c.ID,
                                  TypeId = c.LoanTypeID,
                                  Amount = Math.Round(c.LoanAmount, 0),
                                  TotalAmount = Math.Round(c.TotalAmount, 0),
                                  RateofInterest = c.InterestRate,
                                  Tenure = c.Tenure,
                                  Emi = c.EMI,
                                  Reason = c.Purpose,
                                  LoanStatusId = new cvt.officebau.com.ViewModels.Entity
                                  {
                                      Id = c.StatusID,
                                      Name = st.Code
                                  },
                                  ExpectedDate = c.ExpectedDate,
                                  BusinessUnitId = c.BusinessUnitID,
                                  ApproverName = (empmca.EmpCodePattern ?? "") + (empmca.Code ?? "") + " - " + empmca.FullName,
                                  ApproverId = c.ApproverID,
                                  ApproverStatus = appstatus.Code,
                                  ApprovedDate = c.ApprovedDate,
                                  ApprovedReason = c.ApproverRemarks,
                                  FinApproverName = (empmcs.EmpCodePattern ?? "") + (empmcs.Code ?? "") + " - " + empmcs.FullName,
                                  FinApproverId = c.FinancialApproverID,
                                  FinApprovedDate = c.ApprovedDate,
                                  FinApprovedReason = c.FinancialRemarks,
                                  FinApproverStatus = finstatus.Code,
                                  IsDeleted = c.IsDeleted,
                                  CreatedOn = c.CreatedOn,
                                  ModifiedByName = mod.FullName,
                                  ModifiedOn = c.ModifiedOn,
                                  BaseLocation = bUnit.Name,
                                  StatusName = st.Code ?? "",
                                  EmployeeId = c.EmployeeID
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public decimal GetFixedGrossForLoan()
        {
            decimal gross = 0;
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_FIXEDGROSSFORLOAN);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross]);
                    }
                }
            }

            return gross;
        }

        /// <summary>
        /// </summary>
        /// <param name="employeeLoanBo"></param>
        /// <returns></returns>
        public string CreateLoanRequest(EmployeeLoanBo employeeLoanBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();
                int employeeId = Utility.UserId();
                List<int> checkStatus = (from r in context.tbl_LoanRequest
                                         from s in context.tbl_Status
                                            .Where(s => !s.IsDeleted && s.Type == "Loan" && (s.Code == "Open" || s.Code == "Approved" || s.Code == "Submitted" || s.Code == "Settled"))
                                         where !r.IsDeleted
                                              && r.EmployeeID == employeeLoanBo.EmployeeId
                                              && r.StatusID == s.ID
                                         select r.ID).ToList();
                //if (context.tbl_LoanRequest.Where(l => l.EmployeeID == employeeLoanBo.EmployeeId && !l.IsDeleted && l.DomainID == domainId
                //                        && context.tbl_Status.Where(s => !s.IsDeleted && s.Type == "Loan" && (s.Code == "Approved" || s.Code == "Submitted" || s.Code == "Settled")).Select(s => s.ID).Contains(l.StatusID)).Count() == 0)
                if (checkStatus.Count() == 0)
                {
                    tbl_LoanRequest loanRequest = new tbl_LoanRequest
                    {
                        LoanTypeID = Convert.ToInt32(employeeLoanBo.TypeId),
                        EmployeeID = Utility.UserId(),
                        LoanAmount = employeeLoanBo.Amount,
                        InterestRate = employeeLoanBo.RateofInterest,
                        TotalAmount = Convert.ToDecimal(employeeLoanBo.TypeId == 0 ? employeeLoanBo.Amount : employeeLoanBo.TotalAmount),
                        EMI = Convert.ToDecimal(employeeLoanBo.Emi),
                        Tenure = employeeLoanBo.Tenure == 0 ? 1 : employeeLoanBo.Tenure,
                        Purpose = employeeLoanBo.Reason,
                        ExpectedDate = employeeLoanBo.ExpectedDate,
                        StatusID = GetLoanStatusId("Loan", "Submitted"),
                        ApproverID = employeeLoanBo.ApproverId,
                        ApproverStatusID = GetLoanStatusId("Loan", "Submitted"),
                        BusinessUnitID = employeeLoanBo.BusinessUnitId,
                        IsDeleted = employeeLoanBo.IsDeleted,
                        CreatedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = Utility.UserId(),
                        DomainID = Utility.DomainId(),
                        HistoryID = Guid.NewGuid()
                    };

                    context.tbl_LoanRequest.Add(loanRequest);
                    context.SaveChanges();

                    employeeLoanBo.Id = Convert.ToInt32(context.tbl_LoanRequest.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                    return BOConstants.Submitted;
                }

                return Notification.AlreadyExists;
            }
        }

        public string UpdateLoanRequest(EmployeeLoanBo employeeLoanBo)
        {
            FSMEntities context = new FSMEntities();

            if (employeeLoanBo.Id != 0)
            {
                tbl_LoanRequest loanRequest = context.tbl_LoanRequest.FirstOrDefault(b => b.ID == employeeLoanBo.Id);

                if (loanRequest != null)
                {
                    loanRequest.LoanTypeID = Convert.ToInt32(employeeLoanBo.TypeId);
                    loanRequest.LoanAmount = employeeLoanBo.Amount;
                    loanRequest.InterestRate = employeeLoanBo.RateofInterest;
                    loanRequest.TotalAmount = Convert.ToDecimal(employeeLoanBo.TypeId == 0 ? employeeLoanBo.Amount : employeeLoanBo.TotalAmount);
                    loanRequest.EMI = Convert.ToDecimal(employeeLoanBo.Emi);
                    loanRequest.Tenure = employeeLoanBo.Tenure == 0 ? 1 : employeeLoanBo.Tenure;
                    loanRequest.Purpose = employeeLoanBo.Reason;
                    loanRequest.ExpectedDate = employeeLoanBo.ExpectedDate;
                    loanRequest.BusinessUnitID = employeeLoanBo.BusinessUnitId;
                    loanRequest.StatusID = GetLoanStatusId("Loan", "Submitted");
                    loanRequest.ApproverID = employeeLoanBo.ApproverId;
                    loanRequest.ApproverStatusID = GetLoanStatusId("Loan", "Submitted");
                    loanRequest.IsDeleted = employeeLoanBo.IsDeleted;
                    loanRequest.ModifiedOn = DateTime.Now;
                    loanRequest.ModifiedBy = Utility.UserId();
                    loanRequest.DomainID = Utility.DomainId();
                    loanRequest.HistoryID = Guid.NewGuid();

                    context.Entry(loanRequest).State = EntityState.Modified;
                }

                context.SaveChanges();

                employeeLoanBo.Id = employeeLoanBo.Id;
                return Notification.Updated;
            }

            return Notification.AlreadyExists;
        }

        public string DeleteLoanRequest(EmployeeLoanBo employeeLoanBo)
        {
            FSMEntities context = new FSMEntities();
            tbl_LoanRequest employee = context.tbl_LoanRequest.FirstOrDefault(b => b.ID == employeeLoanBo.Id);

            if (employee != null)
            {
                employee.IsDeleted = employeeLoanBo.IsDeleted;
                employee.ModifiedOn = DateTime.Now;
                employee.ModifiedBy = Utility.UserId();
            }

            context.SaveChanges();
            return Notification.Deleted;
        }

        public string GetEmployeeConfiguration(int employeeId)
        {
            FSMEntities context = new FSMEntities();

            int bandId = Convert.ToInt32(context.tbl_EmployeeMaster.Where(b => b.ID == employeeId && !b.IsDeleted && !(b.IsActive ?? false)).OrderByDescending(b => b.ID).Select(b => b.BandID).FirstOrDefault());
            int designationId = Convert.ToInt32(context.tbl_EmployeeMaster.Where(b => b.ID == employeeId && !b.IsDeleted && !(b.IsActive ?? false)).OrderByDescending(b => b.ID).Select(b => b.DesignationID).FirstOrDefault());
            int gradeId = Convert.ToInt32(context.tbl_EmployeeMaster.Where(b => b.ID == employeeId && !b.IsDeleted && !(b.IsActive ?? false)).OrderByDescending(b => b.ID).Select(b => b.GradeID).FirstOrDefault());
            int designationMappingId = Convert.ToInt32(context.tbl_EmployeeDesignationMapping.Where(s => s.BandID == bandId && s.GradeID == gradeId && s.DesignationID == designationId && !(s.IsDeleted ?? false)).Select(s => s.ID).FirstOrDefault());

            int check = Convert.ToInt32(context.tbl_LoanConfiguration.Count(s => !s.IsDeleted && s.DesignationMappingID == designationMappingId));
            if (check != 0)
            {
                EmployeeLoanBo configuration = (from config in context.tbl_LoanConfiguration
                                                join emp in context.tbl_EmployeeDesignationMapping on config.DesignationMappingID equals emp.DesignationID into empba
                                                from empband in empba.DefaultIfEmpty()
                                                join em in context.tbl_EmployeeMaster on config.ModifiedBy equals em.ID into employee
                                                from empmas in employee.DefaultIfEmpty()
                                                where config.DesignationMappingID == designationMappingId && !config.IsDeleted
                                                select new EmployeeLoanBo
                                                {
                                                    Tenure = config.Tenure,
                                                    InterestRate = config.InterestRate,
                                                    MaxAmount = config.Amount
                                                }).FirstOrDefault();
                return configuration == null ? "" : configuration.Tenure + "/" + configuration.InterestRate + "/" + configuration.MaxAmount;
            }

            return BOConstants.Configure_Loan;
        }

        public List<EmployeeLoanBo> GetLoanAmortizationList(int? id)
        {
            List<EmployeeLoanBo> resultList = new List<EmployeeLoanBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_LOANAMORTIZATIONLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.String, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        resultList.Add(new EmployeeLoanBo
                        {
                            Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                            PaymentDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PaymentDate]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaidAmount]),
                            DueBalance = Utility.CheckDbNull<decimal>(reader["DueBalance"]),
                        });
                    }
                }
            }

            return resultList;
        }

        #endregion Loan Request

        #region Loan Approval

        public List<EmployeeLoanBo> GetEmployeeLoanApproveList(EmployeeLoanBo employeeLoanBo)
        {
            List<EmployeeLoanBo> employeeLoanBoList = new List<EmployeeLoanBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHLOANAPPROVE);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeLoanBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeLoanBoList.Add(
                            new EmployeeLoanBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                RequestedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.RequestedDate]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                                RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                                StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                StatusId = Utility.CheckDbNull<int>(reader[DBParam.Output.StatusID]),
                                TypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.Type]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount])
                            });
                    }
                }
            }

            return employeeLoanBoList;
        }

        public List<EmployeeLoanBo> SearchFinanceApproval(EmployeeLoanBo employeeLoanBo)
        {
            List<EmployeeLoanBo> employeeLoanBoList = new List<EmployeeLoanBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHLOANFINAPPROVE);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeLoanBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeLoanBoList.Add(
                            new EmployeeLoanBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                RequestedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.RequestedDate]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                                StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                TypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.Type]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount])
                            });
                    }
                }
            }

            return employeeLoanBoList;
        }

        public EmployeeLoanBo GetLoanApproveDetails(EmployeeLoanBo employeeLoanBo)
        {
            EmployeeLoanBo employeeBo = new EmployeeLoanBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETLOANAPPROVALDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeLoanBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeLoanBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeBo = new EmployeeLoanBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            StatusId = Utility.CheckDbNull<int>(reader[DBParam.Output.StatusID]),
                            RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                            Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                            StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            ExpectedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ExpectedDate]),
                            Reason = Utility.CheckDbNull<string>(reader[DBParam.Output.Purpose]),
                            TypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.TypeID]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            Tenure = Utility.CheckDbNull<int>(reader[DBParam.Output.Tenure]),
                            RateofInterest = Utility.CheckDbNull<decimal>(reader[DBParam.Output.RateofInterest]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                            ApprovedReason = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovedReason]),
                            ApproverId = Utility.CheckDbNull<int>(reader[DBParam.Output.ApproverID]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]),
                            ApproverStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.ApproverStatus])
                        };
                    }
                }
            }

            employeeBo.ExpectedDate = employeeBo.ExpectedDate == DateTime.MinValue ? null : employeeBo.ExpectedDate;

            return employeeBo;
        }

        public string ManageLoanApprovel(int? financialApproverId, string remarks, string status, int id, int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                tbl_LoanRequest loanApprove = context.tbl_LoanRequest.FirstOrDefault(b => b.ID == id);

                if (loanApprove != null)
                {
                    loanApprove.StatusID = GetLoanStatusId("Loan", status);
                    loanApprove.ApproverStatusID = GetLoanStatusId("Loan", status);
                    if (status.ToUpper() != "REJECTED")
                    {
                        loanApprove.FinancialApproverID = financialApproverId;
                        loanApprove.FinancialStatusID = GetLoanStatusId("Loan", "Submitted");
                    }

                    loanApprove.ApproverRemarks = remarks;
                    loanApprove.ApprovedDate = DateTime.Now;
                    loanApprove.ModifiedBy = employeeId;

                    context.Entry(loanApprove).State = EntityState.Modified;
                }

                context.SaveChanges();

                return status + " Successfully";
            }
        }

        public EmployeeLoanBo ApproveLoanFianance(int loanRequestId)
        {
            EmployeeLoanBo employeeBo = new EmployeeLoanBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETFINAPPROVALDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.LoanRequestID, DbType.Int32, loanRequestId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeBo = new EmployeeLoanBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            StatusId = Utility.CheckDbNull<int>(reader[DBParam.Output.StatusID]),
                            RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                            Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                            StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            ExpectedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ExpectedDate]),
                            TypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.TypeID]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            Tenure = Utility.CheckDbNull<int>(reader[DBParam.Output.Tenure]),
                            RateofInterest = Utility.CheckDbNull<decimal>(reader[DBParam.Output.RateofInterest]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                            ApprovedReason = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovedReason]),
                            ApproverId = Utility.CheckDbNull<int>(reader[DBParam.Output.ApproverID]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            FinApproverStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.FinApproverStatus]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]),
                            Reason = Utility.CheckDbNull<string>(reader[DBParam.Output.Purpose])
                        };
                    }
                }
            }

            employeeBo.ExpectedDate = employeeBo.ExpectedDate == DateTime.MinValue ? null : employeeBo.ExpectedDate;

            return employeeBo;
        }

        public string ManageLoanFinanceApproval(string remarks, string status, int loanRequestId, int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                tbl_LoanRequest loanApprove = context.tbl_LoanRequest.FirstOrDefault(b => b.ID == loanRequestId);

                if (loanApprove != null)
                {
                    loanApprove.FinancialRemarks = remarks;
                    loanApprove.FinancialApprovedDate = DateTime.Now;
                    loanApprove.StatusID = GetLoanStatusId("Loan", status);
                    loanApprove.FinancialStatusID = GetLoanStatusId("Loan", status);
                    loanApprove.ModifiedBy = employeeId;
                    context.Entry(loanApprove).State = EntityState.Modified;
                }

                context.SaveChanges();

                return status + " Successfully";
            }
        }

        #endregion Loan Approval

        #region Loan Settlement

        public List<EmployeeLoanBo> SearchLoanSettlement(int? statusId)
        {
            List<EmployeeLoanBo> settlementList = new List<EmployeeLoanBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHLOANSETTLE);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, statusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        settlementList.Add(
                            new EmployeeLoanBo
                            {
                                LoanId = Utility.CheckDbNull<int>(reader[DBParam.Output.LoanID]),
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Amount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]), 0),
                                Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                                CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.CreatedOn]),
                                ApproverName = Utility.CheckDbNull<string>(reader[DBParam.Output.Approver]),
                                ApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                                StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                TotalAmount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]), 0)
                            });
                    }
                }
            }

            return settlementList;
        }

        public EmployeeLoanBo GetLoanSettlement(int id, int loanId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                return (from r in context.tbl_LoanRequest
                        join e in context.tbl_EmployeeMaster on r.EmployeeID equals e.ID
                        join s in context.tbl_LoanSettle on r.ID equals s.LoanRequestID into loanSettle
                        from t in loanSettle.DefaultIfEmpty()
                        join sett in context.tbl_Status on r.StatusID equals sett.ID into settsta
                        from settstatus in settsta.DefaultIfEmpty()
                        join mod in context.tbl_EmployeeMaster on t.ModifiedBy equals mod.ID into emod
                        from settlemod in emod.DefaultIfEmpty()
                        join bu in context.tbl_BusinessUnit on settlemod.BaseLocationID equals bu.ID into business
                        from bUnit in business.DefaultIfEmpty()
                        where r.ID == loanId && (id != 0 && t.ID == id || id == 0)
                        select new EmployeeLoanBo
                        {
                            LoanId = r.ID,
                            Id = id,
                            Amount = r.LoanAmount,
                            PaymentMode = new cvt.officebau.com.ViewModels.Entity
                            {
                                Id = t.PaymentModeID
                            },
                            ReferenceNo = t.ReferenceNo,
                            DateOfRealization = t.DateOfRealization,
                            Description = t.Description,
                            StatusName = settstatus.Code,
                            Requester = e.Code + " - " + e.FullName,
                            TypeName = r.LoanTypeID == 0 ? "Salary Advance" : "Soft Loan",
                            ModifiedByName = settlemod.FullName,
                            ModifiedOn = t.ModifiedOn,
                            BaseLocation = bUnit.Name,
                            RequestedDate = r.CreatedOn
                        }).FirstOrDefault();
            }
        }

        public string ManageLoanSettlement(EmployeeLoanBo employeeLoanBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGELOANSETTLEMENT);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeLoanBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.LoanID, DbType.Int32, employeeLoanBo.LoanId);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, employeeLoanBo.Amount);
                Database.AddInParameter(dbCommand, DBParam.Input.DateofRealization, DbType.Date, employeeLoanBo.DateOfRealization);
                Database.AddInParameter(dbCommand, DBParam.Input.PaymentModeID, DbType.Int32, employeeLoanBo.PaymentMode.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.Reference, DbType.String, employeeLoanBo.ReferenceNo);
                Database.AddInParameter(dbCommand, DBParam.Input.Description, DbType.String, employeeLoanBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                return Database.ExecuteScalar(dbCommand).ToString();
            }
        }

        #endregion Loan Settlement

        #region Loan Foreclosure

        public List<EmployeeLoanBo> SearchLoanForeclosure(int? statusId)
        {
            List<EmployeeLoanBo> settlementList = new List<EmployeeLoanBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_LOANFORECLOSURE);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, statusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        settlementList.Add(
                            new EmployeeLoanBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                LoanId = Utility.CheckDbNull<int>(reader[DBParam.Output.LoanID]),
                                Amount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]), 0),
                                Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                                CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.SettlementOn]),
                                TypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.TypeID]),
                                StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                                TotalAmount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]), 0)
                            });
                    }
                }
            }

            return settlementList;
        }

        public EmployeeLoanBo GetLoanForeclosure(int id, int loanId)
        {
            EmployeeLoanBo employeeBo = new EmployeeLoanBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_LOANFORECLOSURE);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.LoanID, DbType.Int32, loanId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeBo = new EmployeeLoanBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            LoanId = Utility.CheckDbNull<int>(reader[DBParam.Output.LoanID]),
                            Amount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]), 0),
                            Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                            TypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.TypeID]),
                            RateofInterest = Utility.CheckDbNull<decimal>(reader[DBParam.Output.RateofInterest]),
                            TotalAmount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]), 0),
                            PaymentMode = new cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.PaymentTypeID])
                            },
                            PaymentDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PaymentDate]),
                            ReferenceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ReferenceNo]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]),
                            ForeCloseMonth = Utility.CheckDbNull<int>(reader[DBParam.Output.ForeclosureMonth]),
                            ForecloseInterestRate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ForeCloseInterestRate]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            DateOfRealization = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date])
                        };
                    }
                }
            }

            return employeeBo;
        }

        public string ManageLoanForeclosure(EmployeeLoanBo employeeLoanBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_LOANFORECLOSURE);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeLoanBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.LoanID, DbType.Int32, employeeLoanBo.LoanId);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, employeeLoanBo.Amount);
                Database.AddInParameter(dbCommand, DBParam.Input.PaymentModeID, DbType.Int32, employeeLoanBo.PaymentMode.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.PaymentDate, DbType.Date, employeeLoanBo.PaymentDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Interest, DbType.String, employeeLoanBo.RateofInterest);
                Database.AddInParameter(dbCommand, DBParam.Input.Description, DbType.String, employeeLoanBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.Reference, DbType.String, employeeLoanBo.ReferenceNo);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeLoanBo.EmployeeId);
                return Database.ExecuteScalar(dbCommand).ToString();
            }
        }

        #endregion Loan Foreclosure

        #region Loan Configuration

        /// <summary>
        ///     Method to get Loan Configuration List.
        /// </summary>
        /// <returns>Loan Configuration List</returns>
        public List<EmployeeLoanBo> SearchEmployeeLoanConfiguration()
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();
                List<EmployeeLoanBo> configurationList = (from config in context.tbl_LoanConfiguration
                                                          join em in context.tbl_EmployeeDesignationMapping on config.DesignationMappingID equals em.ID
                                                          join ban in context.tbl_Band on em.BandID equals ban.ID
                                                          join gr in context.tbl_EmployeeGrade on em.GradeID equals gr.ID
                                                          join des in context.tbl_Designation on em.DesignationID equals des.ID
                                                          where !config.IsDeleted && config.DomainID == domainId
                                                          orderby config.ModifiedOn descending
                                                          select new EmployeeLoanBo
                                                          {
                                                              ConfigurationId = config.ID,
                                                              BandId = em.BandID,
                                                              Amount = config.Amount,
                                                              InterestRate = config.InterestRate,
                                                              Tenure = config.Tenure,
                                                              DesignationId = em.DesignationID,
                                                              GradeId = em.GradeID,
                                                              BandName = ban.Name,
                                                              GradeName = gr.Name,
                                                              DesignationName = des.Name,
                                                              ForecloseInterestRate = config.ForecloseInterestRate
                                                          }).ToList();
                return configurationList;
            }
        }

        /// <summary>
        ///     Method to get the Selected Loan Configuration Data.
        /// </summary>
        /// <param name="configurationId">ConfigurationID</param>
        /// <returns>Selected Loan Configuration Data</returns>
        public EmployeeLoanBo GetEmployeeLoanConfiguration(int configurationId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeLoanBo employeeLoanBo = (from config in context.tbl_LoanConfiguration
                                                 join em in context.tbl_EmployeeDesignationMapping on config.DesignationMappingID equals em.ID
                                                 join emp in context.tbl_EmployeeMaster on config.ModifiedBy equals emp.ID
                                                 join bu in context.tbl_BusinessUnit on emp.BaseLocationID equals bu.ID into business
                                                 from bUnit in business.DefaultIfEmpty()
                                                 where !config.IsDeleted && config.ID == configurationId
                                                 orderby config.ModifiedOn descending
                                                 select new EmployeeLoanBo
                                                 {
                                                     ConfigurationId = config.ID,
                                                     Amount = config.Amount,
                                                     InterestRate = config.InterestRate,
                                                     Tenure = config.Tenure,
                                                     ModifiedByName = emp.FullName,
                                                     ModifiedOn = config.ModifiedOn,
                                                     BaseLocation = bUnit.Name,
                                                     BandId = em.BandID,
                                                     DesignationId = em.DesignationID,
                                                     GradeId = em.GradeID,
                                                     ForecloseInterestRate = config.ForecloseInterestRate
                                                 }).FirstOrDefault();
                return employeeLoanBo;
            }
        }

        /// <summary>
        ///     Method to Save or Update Loan Configuration
        /// </summary>
        /// <param name="employeeLoanBo">EmployeeLoanBO</param>
        /// <returns>Success Message</returns>
        public string ManageEmployeeLoanConfiguration(EmployeeLoanBo employeeLoanBo)
        {
            string successMessage;
            using (FSMEntities context = new FSMEntities())
            {
                int designationMappingId = Convert.ToInt32(context.tbl_EmployeeDesignationMapping.Where(s => s.BandID == employeeLoanBo.BandId
                                                                                                          && s.GradeID == employeeLoanBo.GradeId
                                                                                                          && s.DesignationID == employeeLoanBo.DesignationId
                                                                                                          && !(s.IsDeleted ?? false)).Select(s => s.ID).FirstOrDefault());

                int requestCount = (from lc in context.tbl_LoanRequest
                                    join emp in context.tbl_EmployeeMaster on lc.EmployeeID equals emp.ID into empl
                                    from empdm in empl.DefaultIfEmpty()
                                    join em in context.tbl_EmployeeDesignationMapping on empdm.DesignationID equals em.ID into empmas
                                    from empmaster in empmas.DefaultIfEmpty()
                                    join cmp in context.tbl_LoanConfiguration on empmaster.ID equals cmp.DesignationMappingID
                                    where !lc.IsDeleted && lc.LoanTypeID != 0 && cmp.DesignationMappingID == designationMappingId && !cmp.IsDeleted
                                    select lc).Count();

                if (designationMappingId != 0)
                {
                    if (employeeLoanBo.ConfigurationId.Equals(0))
                    {
                        if (context.tbl_LoanConfiguration.Where(a => a.DesignationMappingID == designationMappingId && !a.IsDeleted).Count() != 0)
                        {
                            successMessage = Notification.AlreadyExists;
                        }
                        else
                        {
                            tbl_LoanConfiguration tblLoanConfiguration = new tbl_LoanConfiguration();
                            FillClassDetails(tblLoanConfiguration, employeeLoanBo);
                            tblLoanConfiguration.ID = employeeLoanBo.ConfigurationId;
                            tblLoanConfiguration.CreatedBy = Utility.UserId();
                            tblLoanConfiguration.DesignationMappingID = designationMappingId;
                            tblLoanConfiguration.CreatedOn = DateTime.Now;
                            tblLoanConfiguration.HistoryID = Guid.NewGuid();
                            tblLoanConfiguration.ModifiedOn = DateTime.Now;
                            tblLoanConfiguration.ModifiedBy = Utility.UserId();

                            context.tbl_LoanConfiguration.Add(tblLoanConfiguration);
                            context.SaveChanges();

                            successMessage = Notification.Inserted;
                        }
                    }
                    else if (employeeLoanBo.IsDeleted != true)
                    {
                        int oldDesignationMappingId = Convert.ToInt32(context.tbl_LoanConfiguration.Where(a => a.ID == employeeLoanBo.ConfigurationId && !a.IsDeleted).Select(a => a.DesignationMappingID).FirstOrDefault());

                        requestCount = (from lc in context.tbl_LoanRequest
                                        join emp in context.tbl_EmployeeMaster on lc.EmployeeID equals emp.ID into empl
                                        from empdm in empl.DefaultIfEmpty()
                                        join em in context.tbl_EmployeeDesignationMapping on empdm.DesignationID equals em.ID into empmas
                                        from empmaster in empmas.DefaultIfEmpty()
                                        join cmp in context.tbl_LoanConfiguration on empmaster.ID equals cmp.DesignationMappingID
                                        where !lc.IsDeleted && lc.LoanTypeID != 0 && cmp.DesignationMappingID == oldDesignationMappingId && !cmp.IsDeleted
                                        select lc).Count();

                        int configCount = context.tbl_LoanConfiguration.Count(a => a.DesignationMappingID == designationMappingId && !a.IsDeleted && a.ID != employeeLoanBo.ConfigurationId);
                        if (requestCount == 0)
                        {
                            if (configCount == 0)
                            {
                                tbl_LoanConfiguration loanConfiguration = context.tbl_LoanConfiguration.FirstOrDefault(b => b.ID == employeeLoanBo.ConfigurationId && !b.IsDeleted);
                                FillClassDetails(loanConfiguration, employeeLoanBo);
                                if (loanConfiguration != null)
                                {
                                    loanConfiguration.DesignationMappingID = designationMappingId;
                                    loanConfiguration.ModifiedOn = DateTime.Now;
                                    loanConfiguration.ModifiedBy = Utility.UserId();

                                    context.Entry(loanConfiguration).State = EntityState.Modified;
                                }

                                context.SaveChanges();

                                successMessage = Notification.Updated;
                            }
                            else
                            {
                                successMessage = Notification.AlreadyExists;
                            }
                        }
                        else
                        {
                            successMessage = Notification.AlreadyExists;
                        }
                    }
                    else
                    {
                        if (requestCount != 0)
                        {
                            successMessage = Notification.Record_Referred;
                        }
                        else
                        {
                            tbl_LoanConfiguration loanConfiguration = context.tbl_LoanConfiguration.FirstOrDefault(b => b.ID == employeeLoanBo.ConfigurationId && !b.IsDeleted);
                            if (loanConfiguration != null)
                            {
                                loanConfiguration.IsDeleted = employeeLoanBo.IsDeleted;
                                loanConfiguration.ModifiedOn = DateTime.Now;
                                loanConfiguration.ModifiedBy = Utility.UserId();
                            }

                            context.SaveChanges();

                            successMessage = Notification.Deleted;
                        }
                    }
                }
                else
                {
                    successMessage = BOConstants.Map_Selected_Desigination;
                }
            }

            return successMessage;
        }

        /// <summary>
        ///     Common Method to Fill Class Details
        /// </summary>
        /// <param name="tblLoanConfiguration">Table Class</param>
        /// <param name="employeeLoanBo">Model Class</param>
        private void FillClassDetails(tbl_LoanConfiguration tblLoanConfiguration, EmployeeLoanBo employeeLoanBo)
        {
            tblLoanConfiguration.Amount = employeeLoanBo.Amount;
            tblLoanConfiguration.Tenure = employeeLoanBo.Tenure;
            tblLoanConfiguration.InterestRate = employeeLoanBo.InterestRate;
            tblLoanConfiguration.ForecloseInterestRate = employeeLoanBo.ForecloseInterestRate;
            tblLoanConfiguration.ModifiedBy = Utility.UserId();
            tblLoanConfiguration.ModifiedOn = DateTime.Now;
            tblLoanConfiguration.IsDeleted = false;
            tblLoanConfiguration.DomainID = Utility.DomainId();
        }

        #endregion Loan Configuration

        #region Report

        public List<cvt.officebau.com.ViewModels.Entity> GetLoanApproverList(string approverName)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();

                List<cvt.officebau.com.ViewModels.Entity> list = (from t in context.tbl_LoanRequest
                                                                  join e in context.tbl_EmployeeMaster on t.FinancialApproverID equals e.ID into finApp
                                                                  from f in finApp.DefaultIfEmpty()
                                                                  where !t.IsDeleted && t.DomainID == domainId && f.FullName.Contains(approverName)
                                                                  orderby f.FullName
                                                                  select new cvt.officebau.com.ViewModels.Entity
                                                                  {
                                                                      Name = (f.EmpCodePattern ?? "") + (f.Code ?? "") + " - " + (f.FullName ?? ""),
                                                                      Id = f.ID
                                                                  }).Distinct().ToList();
                return list;
            }
        }

        public List<EmployeeLoanReportBo> GetCompleteLoanList(EmployeeLoanReportBo employeeLoanReportBo)
        {
            List<EmployeeLoanReportBo> employeeLoanReportBoList = new List<EmployeeLoanReportBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_COMPLETELOANLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeLoanReportBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.DateTime, employeeLoanReportBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.DateTime, employeeLoanReportBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, employeeLoanReportBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, employeeLoanReportBo.ApproverId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeLoanReportBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.LoanTypeID, DbType.Int32, employeeLoanReportBo.TypeId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeLoanReportBoList.Add(
                            new EmployeeLoanReportBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.FullName]),
                                EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                                RequestedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.RequestedDate]),
                                Amount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]), 0),
                                LoanType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                                OutstandingAmount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OutstandingAmount]), 0),
                                BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                ApproverName = Utility.CheckDbNull<string>(reader[DBParam.Output.Approver]),
                                FinApproverName = Utility.CheckDbNull<string>(reader[DBParam.Output.FinApproverName]),
                                RepaymentStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.RepaymentStatus]),
                                ApproverStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.ApproverStatus]),
                                FinApproverStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.FinApproverStatus]),
                                SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName])
                            });
                    }
                }
            }

            return employeeLoanReportBoList;
        }

        public List<EmployeeLoanReportBo> GetRepaymentDetailsList(int id)
        {
            List<EmployeeLoanReportBo> employeeLoanReportBoList = new List<EmployeeLoanReportBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_LOANREPAYMENTDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.LoanRequestID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeLoanReportBoList.Add(
                            new EmployeeLoanReportBo
                            {
                                Amount = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]), 0),
                                DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                                PaidDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PaidDate]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status])
                            });
                    }
                }
            }

            return employeeLoanReportBoList;
        }

        #endregion Report
    }
}