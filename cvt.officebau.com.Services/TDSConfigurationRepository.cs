﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class TdsConfigurationRepository : BaseRepository
    {
        public List<TdsConfigurationBo> SearchTdsLimitConfiguration(TdsConfigurationBo tdsConfigurationBo)
        {
            List<TdsConfigurationBo> tdsConfigurationBoList = new List<TdsConfigurationBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_TDSLIMITCONFIGURATION);

                Database.AddInParameter(dbCommand, DBParam.Input.FYID, DbType.Int32, tdsConfigurationBo.FinancialYearId);
                Database.AddInParameter(dbCommand, DBParam.Input.RegionID, DbType.Int32, tdsConfigurationBo.RegionId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        TdsConfigurationBo tdsBo = new TdsConfigurationBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            SectionId = Utility.CheckDbNull<int>(reader[DBParam.Output.SectionID]),
                            Section = Utility.CheckDbNull<string>(reader[DBParam.Output.SectionName]),
                            FinancialYear = Utility.CheckDbNull<string>(reader[DBParam.Output.FinancialYear]),
                            Limit = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Limit]),
                            FinancialYearId = Utility.CheckDbNull<int>(reader[DBParam.Output.FYID]),
                            Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region])
                        };
                        tdsConfigurationBoList.Add(tdsBo);
                    }
                }
            }

            return tdsConfigurationBoList;
        }

        public TdsConfigurationBo GetTdsLimitConfiguration(int id)
        {
            TdsConfigurationBo tdsConfigurationBo = new TdsConfigurationBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_TDSLIMITCONFIGURATION);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        tdsConfigurationBo.Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]);
                        tdsConfigurationBo.SectionId = Utility.CheckDbNull<int>(reader[DBParam.Output.SectionID]);
                        tdsConfigurationBo.Limit = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Limit]);
                        tdsConfigurationBo.FinancialYearId = Utility.CheckDbNull<int>(reader[DBParam.Output.FYID]);
                        tdsConfigurationBo.ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]);
                        tdsConfigurationBo.ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]);
                        tdsConfigurationBo.BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]);
                        tdsConfigurationBo.RegionId = Utility.CheckDbNull<int>(reader[DBParam.Output.RegionID]);
                    }
                }
            }

            return tdsConfigurationBo;
        }

        public string ManageTdsLimitConfiguration(TdsConfigurationBo tdsConfigurationBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_TDSLIMITCONFIGURATION);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, tdsConfigurationBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SectionID, DbType.Int32, tdsConfigurationBo.SectionId);
                Database.AddInParameter(dbCommand, DBParam.Input.Limit, DbType.Decimal, tdsConfigurationBo.Limit);
                Database.AddInParameter(dbCommand, DBParam.Input.FYID, DbType.Int32, tdsConfigurationBo.FinancialYearId);
                Database.AddInParameter(dbCommand, DBParam.Input.RegionID, DbType.Int32, tdsConfigurationBo.RegionId);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, tdsConfigurationBo.IsDeleted);
                tdsConfigurationBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return tdsConfigurationBo.UserMessage;
        }

        #region TDS Configuration

        public TdsConfigurationBo GetTdsConfiguration()
        {
            using (Database.CreateConnection())
            {
                TdsConfigurationBo tdsConfigurationBo = new TdsConfigurationBo();

                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_TDSCONFIG);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        if (reader[DBParam.Output.Value].ToString() == "CTC")
                        {
                            tdsConfigurationBo.CTCComponentID = Convert.ToInt32(reader[DBParam.Output.PayrollComponentId]);
                        }

                        if (reader[DBParam.Output.Value].ToString() == "BASIC")
                        {
                            tdsConfigurationBo.BasicComponentID = Convert.ToInt32(reader[DBParam.Output.PayrollComponentId]);
                        }

                        if (reader[DBParam.Output.Value].ToString() == "MEDICAL")
                        {
                            tdsConfigurationBo.MedicalComponentID = Convert.ToInt32(reader[DBParam.Output.PayrollComponentId]);
                        }

                        if (reader[DBParam.Output.Value].ToString() == "CONVEYANCE")
                        {
                            tdsConfigurationBo.ConveyanceComponentID = Convert.ToInt32(reader[DBParam.Output.PayrollComponentId]);
                        }

                        if (reader[DBParam.Output.Value].ToString() == "HRA")
                        {
                            tdsConfigurationBo.HRAComponentID = Convert.ToInt32(reader[DBParam.Output.PayrollComponentId]);
                        }
                        tdsConfigurationBo.ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]);
                        tdsConfigurationBo.ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]);
                    }
                }

                return tdsConfigurationBo;
            }
        }

        public TdsConfigurationBo ManageTdsConfiguration(List<TdsConfigurationBo> list)
        {
            TdsConfigurationBo tdsConfigurationBo = new TdsConfigurationBo();
            using (DbConnection dbCon = this.Database.CreateConnection())
            {
                foreach (var item in list)
                {
                    DbCommand dbCmd = this.Database.GetStoredProcCommand(Constants.MANAGE_TDSCONFIG);
                    this.Database.AddInParameter(dbCmd, DBParam.Input.ID, DbType.Int32, item.Id);
                    this.Database.AddInParameter(dbCmd, DBParam.Input.Value, DbType.String, item.Values);
                    this.Database.AddInParameter(dbCmd, DBParam.Input.PayrollComponentID, DbType.Int32, item.PayrollComponentID);
                    this.Database.AddInParameter(dbCmd, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                    this.Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    tdsConfigurationBo.UserMessage = this.Database.ExecuteScalar(dbCmd).ToString();
                }
            }
            return tdsConfigurationBo;
        }

        #endregion TDS Configuration
    }
}