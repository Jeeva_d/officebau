﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace cvt.officebau.com.Services
{
    public class StudentHub : Hub
    {
        [HubMethodName("sendUptodateInformation")]
        public static void SendUptodateInformation(string action)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StudentHub>();

            // the updateStudentInformation method will update the connected client about any recent changes in the server data
            context.Clients.All.updateStudentInformation(action);
        }

        [HubMethodName("BuildActivity")]
        public static void BuildActivity(string action)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StudentHub>();

            // the updateStudentInformation method will update the connected client about any recent changes in the server data
            context.Clients.All.BuildActivityInformation(action);
        }
    }
}