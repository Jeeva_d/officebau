﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class CustomerContactRepository : BaseRepository
    {
        public List<CustomerContactBo> GetCustomerContactList(CustomerContactBo customerContactBo)
        {
            List<CustomerContactBo> customerContactBoList = new List<CustomerContactBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_CUSTOMERCONTACT);

                Database.AddInParameter(dbCommand, DBParam.Input.CustomerName, DbType.String, customerContactBo.CustomerName);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactName, DbType.String, customerContactBo.FullName);
                Database.AddInParameter(dbCommand, DBParam.Input.EmailID, DbType.String, customerContactBo.EmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, customerContactBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        customerContactBoList.Add(
                            new CustomerContactBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactName]),
                                CustomerName = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName]),
                                EmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                DesignationName = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]),
                                ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                                AlternateNo = Utility.CheckDbNull<string>(reader[DBParam.Output.AlternateNo])
                            });
                    }
                }
            }

            return customerContactBoList;
        }
    }
}