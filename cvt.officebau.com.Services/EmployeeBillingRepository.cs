﻿using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class EmployeeBillingRepository
    {

        #region Variables

        FSMEntities datacontext = new FSMEntities();
        EmployeeBillingBO employeeBillingBO = new EmployeeBillingBO();
        int userId = Utility.UserId();
        DateTime dateNow = DateTime.Now;
        int domainId = Utility.DomainId();

        #endregion

        #region Employee Billing Search

        public List<EmployeeBillingBO> SearchEmployee(string employeeName, string customerName, bool IsActive)
        {
            List<EmployeeBillingBO> result = (from c in datacontext.tbl_EmployeeBilling
                                              where c.tbl_EmployeeMaster.FullName.ToUpper().Contains(string.IsNullOrEmpty(employeeName) ? c.tbl_EmployeeMaster.FullName.ToUpper() : employeeName.ToUpper())
                                              && c.tblCustomer.Name.ToUpper().Contains(string.IsNullOrEmpty(customerName) ? c.tblCustomer.Name.ToUpper() : customerName.ToUpper())
                                              && (IsActive ? true == c.IsActive : false == c.IsActive)
                                               && !c.Isdeleted
                                              orderby c.EmployeeId 
                                              select new EmployeeBillingBO
                                              {
                                                  EmployeeBillingId = c.Id,
                                                  EmployeeId = c.EmployeeId,
                                                  CustomerId = c.CustomerId,
                                                  CustomerName = c.tblCustomer.Name,
                                                  EmployeeName = c.tbl_EmployeeMaster.FullName,
                                                  EmployeeCode = c.tbl_EmployeeMaster.EmpCodePattern + c.tbl_EmployeeMaster.Code,
                                                  HourlyRate = c.HourlyRate,
                                                  FixedRate = c.FixedRate,
                                                  CurrencyCode = datacontext.tblCurrencies.FirstOrDefault(a => a.ID == c.tblCustomer.CurrencyID).Code,
                                                  Active = c.IsActive,
                                                  BillingPercentage = c.BillingPercentage,
                                                  RemunerationPercentage = c.RemunerationPercentage,
                                                  Description= c.Description
                                              }).ToList();
            return result;
        }

        #endregion

        #region GetemployeeBilling

        public EmployeeBillingBO GetemployeeBilling(int? id)
        {
            var c = datacontext.tbl_EmployeeBilling.FirstOrDefault(a => a.Id == id && !a.Isdeleted);
            var result = new EmployeeBillingBO()
            {
                EmployeeBillingId = c.Id,
                EmployeeId = c.EmployeeId,
                CustomerId = c.CustomerId,
                CustomerName = c.tblCustomer.Name,
                EmployeeName = c.tbl_EmployeeMaster.FullName,
                Fixed = c.Fixed,
                FixedRate = c.FixedRate,
                HourlyRate = c.HourlyRate,
                CurrencyCode = datacontext.tblCurrencies.FirstOrDefault(a => a.ID == c.tblCustomer.CurrencyID).Code,
                BillingPercentage =c.BillingPercentage,
                RemunerationPercentage = c.RemunerationPercentage,
                ModifiedByName = datacontext.tbl_EmployeeMaster.FirstOrDefault(a => a.ID == c.ModifiedBy)?.FirstName,
                Active = c.IsActive,
                Description = c.Description,
                ModifiedOn = c.ModifiedOn
            };

            return result;
        }

        #endregion

        #region ManageEmployeeBilling

        public string ManageEmployeeBilling(EmployeeBillingBO employeeBillingBO)
        {
            var Data = datacontext.tbl_EmployeeBilling.Count(a => a.Id != employeeBillingBO.EmployeeBillingId && a.EmployeeId == employeeBillingBO.EmployeeId && a.CustomerId == employeeBillingBO.CustomerId && a.IsActive == employeeBillingBO.Active && !a.Isdeleted);
            if (Data != 0)
                return Notification.AlreadyExists;

            if (employeeBillingBO.EmployeeBillingId == 0)
            {
                tbl_EmployeeBilling BillingDetail = new tbl_EmployeeBilling
                {
                    Id = employeeBillingBO.EmployeeBillingId,
                    EmployeeId = employeeBillingBO.EmployeeId,
                    CustomerId = employeeBillingBO.CustomerId,
                    Fixed = employeeBillingBO.Fixed,
                    FixedRate = employeeBillingBO.FixedRate,
                    HourlyRate = employeeBillingBO.HourlyRate,
                    IsActive = employeeBillingBO.Active,
                    BillingPercentage = employeeBillingBO.BillingPercentage,
                    RemunerationPercentage = employeeBillingBO.RemunerationPercentage,
                    Description = employeeBillingBO.Description,
                    CreatedBy = userId,
                    CreatedOn = dateNow,
                    ModifiedBy = userId,
                    ModifiedOn = dateNow,
                    DomainId = domainId
                };
                datacontext.tbl_EmployeeBilling.Add(BillingDetail);
                datacontext.SaveChanges();
                return Notification.Inserted;
            }
            else
            {
                tbl_EmployeeBilling BillingDetail = datacontext.tbl_EmployeeBilling.FirstOrDefault(b => b.Id == employeeBillingBO.EmployeeBillingId);
                {
                    BillingDetail.EmployeeId = employeeBillingBO.EmployeeId;
                    BillingDetail.CustomerId = employeeBillingBO.CustomerId;
                    BillingDetail.Fixed = employeeBillingBO.Fixed;
                    BillingDetail.FixedRate = employeeBillingBO.FixedRate;
                    BillingDetail.HourlyRate = employeeBillingBO.HourlyRate;
                    BillingDetail.IsActive = employeeBillingBO.Active;
                    BillingDetail.BillingPercentage = employeeBillingBO.BillingPercentage;
                    BillingDetail.RemunerationPercentage = employeeBillingBO.RemunerationPercentage;
                    BillingDetail.Description = employeeBillingBO.Description;
                    BillingDetail.ModifiedBy = userId;
                    BillingDetail.ModifiedOn = dateNow;
                    BillingDetail.DomainId = domainId;
                };
                datacontext.Entry(BillingDetail).State = EntityState.Modified;
                datacontext.SaveChanges();
                return Notification.Updated;
            }
        }

        #endregion

        #region DeleteEmployeeBilling

        public string DeleteEmployeeBilling(int id, int employeeId)
        {
            tbl_EmployeeBilling BillingDetail = datacontext.tbl_EmployeeBilling.FirstOrDefault(b => b.Id == id);
            var EmployeeBillingId = datacontext.tbl_EmployeeBilling.FirstOrDefault(w => w.EmployeeId == employeeId)?.EmployeeId;
            var employeeinvoice = datacontext.tbl_MonthlyCustomerBillingDetails.FirstOrDefault(q => q.EmployeeID == employeeId)?.EmployeeID;
            if (EmployeeBillingId == employeeinvoice)
            {
                return Notification.Already_Referred;
            }
            if (BillingDetail != null)
            {
                BillingDetail.Isdeleted = true;
                BillingDetail.ModifiedOn = dateNow;
                BillingDetail.ModifiedBy = userId;
            }
            datacontext.Entry(BillingDetail).State = EntityState.Modified;
            datacontext.SaveChanges();
            return Notification.Deleted;
        }

        #endregion

    }
}

