﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class EmployeePayStructureRepositoryV2 : BaseRepository
    {
        public List<EmployeePayStructureBo_V2> SearchEmployee(EmployeePayStructureBo_V2 employeePayStructureBo)
        {
            List<EmployeePayStructureBo_V2> employeePayStructureBoList = new List<EmployeePayStructureBo_V2>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_SearchEmployeePayStructure");

                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, employeePayStructureBo.BusinessUnitIDs);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeePayStructureBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, employeePayStructureBo.IsActive);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeePayStructureBoList.Add(new EmployeePayStructureBo_V2
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            EffectiveFrom = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EffectiveFrom]),
                            Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross]),
                            Basic = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]),
                            CompanyPayStucture = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CompanyPayStucId]),
                                Name = reader[DBParam.Output.CompanyPayStucName].ToString()
                            },
                        });
                    }
                }
            }

            return employeePayStructureBoList;
        }

        public EmployeePayStructureBo_V2 GetEmployeePayStructureDetails(int id)
        {
            EmployeePayStructureBo_V2 employeePayStructureBo = new EmployeePayStructureBo_V2();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_GetEmployeePayStructure");

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeePayStructureBo = new EmployeePayStructureBo_V2
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            CompanyPayStructureId = Utility.CheckDbNull<int>(reader[DBParam.Output.CompanyPayStucId]),
                            EffectiveFrom = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EffectiveFrom]),
                            Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross])
                        };
                    }
                }
            }

            employeePayStructureBo.CompanyPayStructureBo = new List<CompanyPayStructureBo_V2>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_SearchEmployeePayStructureComponents");

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeePayStructureBo.CompanyPayStructureBo.Add(new CompanyPayStructureBo_V2()
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Code]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            ComponentId = Utility.CheckDbNull<int>(reader[DBParam.Output.ComponentId]),
                            Formula = Utility.CheckDbNull<string>(reader[DBParam.Output.Value]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            IsEditable = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEditable]),
                            PayType = Utility.CheckDbNull<string>(reader["Type"]),
                        });
                    }
                }
            }
            return employeePayStructureBo;
        }

        public EmployeePayStructureBo_V2 GetEmployeePayStructure(int payStructureId, decimal gross, List<EmployeePayrollBo_v2> list)
        {
            EmployeePayStructureBo_V2 employeePayStructureBoV2 = new EmployeePayStructureBo_V2
            {
                CompanyPayStructureBo = new List<CompanyPayStructureBo_V2>()
            };

            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);
            SqlCommand cmd = new SqlCommand("Pay_GetEmployeePayStructureComponents_temp")
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            }; con.Open();
            cmd.Parameters.AddWithValue("@Payroll", Utility.ToDataTable(list, "Payroll"));
            cmd.Parameters.AddWithValue(DBParam.Input.CompanyPayStructureId, payStructureId);
            cmd.Parameters.AddWithValue(DBParam.Input.Gross, gross);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    employeePayStructureBoV2.CompanyPayStructureBo.Add(new CompanyPayStructureBo_V2()
                    {
                        Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                        Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Code]),
                        Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                        ComponentId = Utility.CheckDbNull<int>(reader[DBParam.Output.ComponentId]),
                        Formula = Utility.CheckDbNull<string>(reader[DBParam.Output.Value]),
                        Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                        IsEditable = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEditable]),
                        PayType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),

                    });
                }
            }
            return employeePayStructureBoV2;
        }

        public List< cvt.officebau.com.ViewModels.Entity> SearchAutoCompleteEmployee(string employeeName)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List< cvt.officebau.com.ViewModels.Entity> list = (from t in context.tbl_EmployeeMaster
                                        where !t.IsDeleted && t.DomainID == domainId && t.FullName.Contains(employeeName) && !(t.IsActive ?? false)
                                                                   orderby t.FullName
                                        select new  cvt.officebau.com.ViewModels.Entity
                                        {
                                            Name = (t.Code ?? "") + " - " + (t.FullName ?? ""),
                                            Id = t.ID
                                        }).ToList();
                return list;
            }
        }

        public string ManageEmployeePayStructure(EmployeePayStructureBo_V2 employeePayStructureBo)
        {
            DbCommand dbCmd = Database.GetStoredProcCommand("Pay_ManageEmployeePayStructure");
            Database.AddInParameter(dbCmd, DBParam.Input.ID, DbType.Int32, employeePayStructureBo.Id);
            Database.AddInParameter(dbCmd, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
            Database.AddInParameter(dbCmd, DBParam.Input.EffectiveFrom, DbType.DateTime, employeePayStructureBo.EffectiveFrom);
            Database.AddInParameter(dbCmd, DBParam.Input.CompanyPayStructureId, DbType.Int32, employeePayStructureBo.CompanyPayStructureId);
            Database.AddInParameter(dbCmd, DBParam.Input.EmployeeID, DbType.Int32, employeePayStructureBo.EmployeeId);
            Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
            Database.AddInParameter(dbCmd, DBParam.Input.IsDeleted, DbType.Boolean, employeePayStructureBo.IsDeleted);
            employeePayStructureBo.UserMessage = Database.ExecuteScalar(dbCmd).ToString();
            if ((!employeePayStructureBo.UserMessage.Split('/')[0].ToUpper().Contains("SUCCESSFULLY")) || (employeePayStructureBo.UserMessage.Split('/')[0].ToUpper().Contains("DELETED SUCCESSFULLY")))
            {
                return employeePayStructureBo.UserMessage.Split('/')[0];
            }
            else if (employeePayStructureBo.Id == 0)
            {
                employeePayStructureBo.Id = Convert.ToInt32(employeePayStructureBo.UserMessage.Split('/')[1]);
            }
            foreach (CompanyPayStructureBo_V2 n in employeePayStructureBo.CompanyPayStructureBo)
            {
                using (Database.CreateConnection())
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand("Pay_ManageEmployeePayStructureDetails");
                    Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, n.Id);
                    Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.Value, DbType.String, n.Amount);
                    Database.AddInParameter(dbCommand, DBParam.Input.ComponentId, DbType.Int32, n.ComponentId);
                    Database.AddInParameter(dbCommand, DBParam.Input.EmployeePayStructureID, DbType.Int32, employeePayStructureBo.Id);
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, employeePayStructureBo.IsDeleted);
                    employeePayStructureBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
                }
            }
            return employeePayStructureBo.UserMessage.Split('/')[0];
        }

        public List<EmployeePayStructureBo_V2> GetPayStubComponentsList()
        {
            List<EmployeePayStructureBo_V2> employeePayStructureBoV2List = new List<EmployeePayStructureBo_V2>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetPayStubComponentsList");

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeePayStructureBoV2List.Add(new EmployeePayStructureBo_V2
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ComponentID = Utility.CheckDbNull<int>(reader[DBParam.Output.ComponentID]),
                            TypeID = Utility.CheckDbNull<int>(reader[DBParam.Output.TypeID]),
                            ComponentCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            TypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "PayStubType"),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn])
                        });
                        if (employeePayStructureBoV2List[employeePayStructureBoV2List.Count - 1].TypeID != 0)
                        {
                            employeePayStructureBoV2List[employeePayStructureBoV2List.Count - 1].TypeList.Find(a => a.Value == employeePayStructureBoV2List[employeePayStructureBoV2List.Count - 1].TypeID.ToString()).Selected = true;
                        }
                    }
                }
            }
            return employeePayStructureBoV2List;
        }

        public EmployeePayStructureBo_V2 ManagePayStubConfiguration(List<EmployeePayStructureBo_V2> list)
        {
            EmployeePayStructureBo_V2 employeePayStructureBoV2 = new EmployeePayStructureBo_V2();

            using (DbConnection dbCon = Database.CreateConnection())
            {
                dbCon.Open();
                foreach (EmployeePayStructureBo_V2 item in list)
                {
                        DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_PAYSTUBCONFIGURATION);
                        Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, item.Id == 0 ? 0 : item.Id);
                        Database.AddInParameter(dbCommand, DBParam.Input.ComponentId, DbType.Int32, item.ComponentID);
                        Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, item.Type);
                        Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                        employeePayStructureBoV2.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
                }
            }

            return employeePayStructureBoV2;
        }
    }
}