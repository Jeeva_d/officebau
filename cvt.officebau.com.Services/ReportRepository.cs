﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace cvt.officebau.com.Services
{
    public class ReportRepository : BaseRepository
    {
        #region GetBusinessUnitSalesGrid

        public List<ReportBo> GetBusinessUnitSalesGrid(ReportBo searchBo)
        {
            List<ReportBo> boList = new List<ReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.BusinessUnitSalesGRID);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, searchBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, searchBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, searchBo.BusinessUnitId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ReportBo reportBo = new ReportBo
                        {
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            SalesExecutive = Utility.CheckDbNull<string>(reader[DBParam.Output.SalesPerson]),
                            Customer = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName]),
                            Achieved = Utility.CheckDbNull<int>(reader[DBParam.Output.Achieved]),
                            Expected = Utility.CheckDbNull<int>(reader[DBParam.Output.Expected])
                        };
                        boList.Add(reportBo);
                    }
                }
            }

            return boList;
        }

        #endregion GetBusinessUnitSalesGrid

        #region Expense Reports

        public List<ReportsBo> SearchExpenseReport(ReportsBo reportBo)
        {
            List<ReportsBo> expenseReportBo = new List<ReportsBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EXPENSESUPPLIER);

                Database.AddInParameter(dbCommand, DBParam.Input.Days, DbType.Int32, reportBo.YearlyType);
                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.DateTime, reportBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.DateTime, reportBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.VendorID, DbType.Int32, reportBo.VendorId);
                Database.AddInParameter(dbCommand, DBParam.Input.CreatedBy, DbType.String, null);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, null);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseReportBo.Add(
                            new ReportsBo
                            {
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                                FromDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                                Outstanding = Utility.CheckDbNull<decimal>(reader[DBParam.Output.outstanding])
                            });
                    }
                }
            }

            return expenseReportBo;
        }

        #endregion Expense Reports

        #region Income Reports

        public List<ReportsBo> SearchIncomeReport(ReportsBo reportBo)
        {
            List<ReportsBo> incomeReportBo = new List<ReportsBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_INCOME);

                Database.AddInParameter(dbCommand, DBParam.Input.Days, DbType.Int32, reportBo.YearlyType);
                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.Date, reportBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.Date, reportBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.VendorID, DbType.Int32, reportBo.VendorId);
                Database.AddInParameter(dbCommand, DBParam.Input.CreatedBy, DbType.String, null);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeReportBo.Add(
                            new ReportsBo
                            {
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Customer]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo]),
                                FromDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Outstanding = Utility.CheckDbNull<decimal>(reader[DBParam.Output.outstanding])
                            });
                    }
                }
            }

            return incomeReportBo;
        }

        #endregion Income Reports

        public List<ReportsBo> SearchLoanDetails()
        {
            List<ReportsBo> reportBo = new List<ReportsBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_LOANDETAILS);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        reportBo.Add(
                            new ReportsBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.NAME]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount])
                            });
                    }
                }
            }

            return reportBo;
        }

        public List<ReportsBo> GetLoanDetails(int id)
        {
            List<ReportsBo> reportBo = new List<ReportsBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("[Rpt_GetLoanDetails]");
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        reportBo.Add(
                            new ReportsBo
                            {
                                FromDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type])
                            });
                    }
                }
            }

            return reportBo;
        }

        #region GST Summary Report

        public List<GstReportBo> GstSummaryReportList(int yearId, string reportType)
        {
            List<GstReportBo> gstReportBoList = new List<GstReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_GSTSUMMARY);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.ReportType, DbType.String, reportType);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        gstReportBoList.Add(new GstReportBo
                        {
                            Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Total = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Total])
                        });
                    }
                }
            }

            return gstReportBoList;
        }

        #endregion GST Summary Report

        #region GST Summary Monthly Report

        public DataSet GstSummaryMonthlyReportList(int yearId, string reportType)
        {
            string constr = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;

            DataSet ds = new DataSet();
            DataTable dtOutputTax = new DataTable("GSTsummaryByMonthly");
            ds.Tables.Add(dtOutputTax);
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_GSTSUMMARYBYMONTHLY, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.FinancialYearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.ReportType, reportType);
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtOutputTax);
                    }
                }
            }

            return ds;
        }

        #endregion GST Summary Monthly Report

        #region Sales Report - Chart

        public List<ReportBo> GetBusinessUnitSalesTarget(ReportBo searchBo)
        {
            List<ReportBo> boList = new List<ReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.BUSINESSUNITSALESTARGETREPORT);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, searchBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, searchBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, searchBo.BusinessUnitId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ReportBo reportBo = new ReportBo
                        {
                            Expected = Utility.CheckDbNull<int>(reader[DBParam.Output.Expected]),
                            Actual = Utility.CheckDbNull<int>(reader[DBParam.Output.Actual]),
                            TradeType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type])
                        };
                        boList.Add(reportBo);
                    }
                }
            }

            return boList;
        }

        public List<ReportBo> GetTargetReportData(int monthId, int yearId)
        {
            List<ReportBo> boList = new List<ReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MONTHLYTARGETREPORT);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ReportBo reportBo = new ReportBo
                        {
                            Expected = Utility.CheckDbNull<int>(reader[DBParam.Output.Expected]),
                            Actual = Utility.CheckDbNull<int>(reader[DBParam.Output.Actual]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit])
                        };
                        boList.Add(reportBo);
                    }
                }
            }

            return boList;
        }

        public List<ReportBo> GetSalesExecutiveReportData(ReportBo searchBo)
        {
            List<ReportBo> boList = new List<ReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SALESEXECUTIVEMONTHLYTARGET);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, searchBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, searchBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, searchBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, searchBo.TradeType);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ReportBo reportBo = new ReportBo
                        {
                            Target = Utility.CheckDbNull<int>(reader[DBParam.Output.Target]),
                            Achieved = Utility.CheckDbNull<int>(reader[DBParam.Output.Achieved]),
                            Committed = Utility.CheckDbNull<int>(reader[DBParam.Output.Committed]),
                            SalesExecutive = Utility.CheckDbNull<string>(reader[DBParam.Output.SalesPerson])
                        };
                        boList.Add(reportBo);
                    }
                }
            }

            return boList;
        }

        public List<ReportBo> SearchActualSalesReport(ReportBo searchBo)
        {
            List<ReportBo> boList = new List<ReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHACTUALSALESREPORT);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, searchBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, searchBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.SalesPerson, DbType.String, searchBo.SalesExecutive);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, searchBo.TradeType);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, searchBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ReportBo reportBo = new ReportBo
                        {
                            Achieved = Utility.CheckDbNull<int>(reader[DBParam.Output.Actual]),
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            RakeNo = reader[DBParam.Output.RakeNo].ToString(),
                            ExportDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ExportDate]),
                            Customer = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName]),
                            SalesExecutive = Utility.CheckDbNull<string>(reader[DBParam.Output.SalesPerson])
                        };
                        boList.Add(reportBo);
                    }
                }
            }

            return boList;
        }

        public List<ReportBo> SearchCommittedSalesReport(ReportBo searchBo)
        {
            List<ReportBo> boList = new List<ReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHCOMMITTEDSALESREPORT);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, searchBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, searchBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.SalesPerson, DbType.String, searchBo.SalesExecutive);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, searchBo.TradeType);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, searchBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ReportBo reportBo = new ReportBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Committed = Utility.CheckDbNull<int>(reader[DBParam.Output.Expected]),
                            RakeNo = reader[DBParam.Output.RakeNo].ToString(),
                            ExportDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ExportDate]),
                            Customer = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName]),
                            SalesExecutive = Utility.CheckDbNull<string>(reader[DBParam.Output.SalesPerson])
                        };
                        boList.Add(reportBo);
                    }
                }
            }

            return boList;
        }


        #endregion Sales Report - Chart

        #region Activity Count Report

        public List<ReportBo> GetActivityCountReport(ReportBo searchBo)
        {
            List<ReportBo> boList = new List<ReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHACTIVITYCOUNTREPORT);

                Database.AddInParameter(dbCommand, DBParam.Input.Date, DbType.Date, searchBo.VisitDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, searchBo.BusinessUnit);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ReportBo reportBO;
                    while (reader.Read())
                    {
                        reportBO = new ReportBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            Count = Utility.CheckDbNull<int>(reader[DBParam.Output.Counts]),
                            SalesExecutive = Utility.CheckDbNull<string>(reader[DBParam.Output.SalesPerson]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            VisitDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.VisitDate]),
                            LastVisit = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.LastVisit])
                        };
                        boList.Add(reportBO);
                    }
                }
            }

            return boList;
        }


        #endregion Activity Count Report

        #region Sales Compliance Report

        public DataTable SearchSalesComplianceReport(ReportBo searchBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.REPORT_SALESCOMPLIANCE);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, searchBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, searchBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, searchBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable();
                dataSet.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        public DataTable GetSalesComplianceReportDetails(int? monthId, int? yearId, int employeeId, int dateValue)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.REPORT_SALESCOMPLIANCEDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.Value, DbType.Int32, dateValue);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable();
                dataSet.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion Sales Compliance Report

        #region Party Detail

        public List<ReportsBo> SearchExpenseTransactiondetail(string searchParameter, string partyName, string type)
        {
            List<ReportsBo> reportBo = new List<ReportsBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EXPENSETRANSACTIONDETAIL);

                Database.AddInParameter(dbCommand, DBParam.Input.SearchParameter, DbType.String, searchParameter);
                Database.AddInParameter(dbCommand, DBParam.Input.PartyName, DbType.String, partyName);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        reportBo.Add(
                            new ReportsBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                                FromDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName])
                            });
                    }
                }
            }

            return reportBo;
        }

        public List<ReportsBo> SearchIncomeTransactiondetail(string searchParameter, string partyName, string type)
        {
            List<ReportsBo> reportBo = new List<ReportsBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_INCOMETRANSACTIONDETAIL);

                Database.AddInParameter(dbCommand, DBParam.Input.SearchParameter, DbType.String, searchParameter);
                Database.AddInParameter(dbCommand, DBParam.Input.PartyName, DbType.String, partyName);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        reportBo.Add(
                            new ReportsBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                                FromDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName])
                            });
                    }
                }
            }

            return reportBo;
        }

        public List<ReportsBo> SearchCostcenterTransactiondetail(string searchParameter, string startDate, string endDate, string type)
        {
            List<ReportsBo> reportBo = new List<ReportsBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_COSTCENTERTRANSACTIONDETAIL);

                Database.AddInParameter(dbCommand, DBParam.Input.SearchParameter, DbType.String, searchParameter);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, startDate);
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, endDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        reportBo.Add(
                            new ReportsBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                                FromDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName])
                            });
                    }
                }
            }

            return reportBo;
        }

        #endregion Party Detail
    }
}