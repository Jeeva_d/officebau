﻿using cvt.officebau.com.ViewModels;
using System.Collections.Generic;

namespace cvt.officebau.com.Services
{
    public interface IAnnouncements
    {
        List<CustomBo> GetStudentDetails();

        CustomBo GetBuildNotification();
    }
}