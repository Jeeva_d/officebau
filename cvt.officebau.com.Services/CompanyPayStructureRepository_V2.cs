﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class CompanyPayStructureRepositoryV2 : BaseRepository
    {
        public List<CompanyPayStructureBo_V2> SearchCompanyPayStructure()
        {
            List<CompanyPayStructureBo_V2> companyPayStructureList = new List<CompanyPayStructureBo_V2>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_SearchCompanyPayStructure");

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.String, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        companyPayStructureList.Add(
                            new CompanyPayStructureBo_V2
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                EffectiveFrom = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EffectiveFrom]),
                                ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.FullName]),
                                ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                                UserId = Utility.CheckDbNull<int>(reader[DBParam.Output.Count]),
                            });
                    }
                }
            }

            return companyPayStructureList;
        }

        public CompanyPayStructureBo_V2 GetCompanyPayStructure(int id)
        {
            CompanyPayStructureBo_V2 companyPayStructureBo = new CompanyPayStructureBo_V2();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_GetCompanyPayStructure");

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        companyPayStructureBo = new CompanyPayStructureBo_V2
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            EffectiveFrom = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EffectiveFrom]),
                        };
                    }
                }
            }

            return companyPayStructureBo;
        }

        public string ManageCompanyPayStructure(CompanyPayStructureBo_V2 companyPayStructureBoV2)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_ManageCompanyPayStructure");
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, companyPayStructureBoV2.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, companyPayStructureBoV2.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.EffectiveFrom, DbType.DateTime, companyPayStructureBoV2.EffectiveFrom);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, companyPayStructureBoV2.IsDeleted);
                companyPayStructureBoV2.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }
            return companyPayStructureBoV2.UserMessage;
        }

        public List<CompanyPayStructureBo_V2> SearchCompanyPayStructureComponents(int id)
        {
            List<CompanyPayStructureBo_V2> companyPayStructureList = new List<CompanyPayStructureBo_V2>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_SearchPayStructureComponents");
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.String, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.String, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        companyPayStructureList.Add(
                            new CompanyPayStructureBo_V2
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                ComponentId = Utility.CheckDbNull<int>(reader[DBParam.Output.ComponentId]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Code]),
                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                Formula = Utility.CheckDbNull<string>(reader[DBParam.Output.Value]),
                                CompanyPayStructureId = id,
                                IsEditable = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEditable]),
                                ShowinPaystructure = Utility.CheckDbNull<bool>(reader["ShowinPaystructure"]),
                                PayStub = Utility.CheckDbNull<bool>(reader["PayStub"]),
                                FixedComponent = Utility.CheckDbNull<bool>(reader["Fixed"]),


                            });
                    }
                }
            }

            return companyPayStructureList;
        }

        public string ManageCompanyPayStructureComponents(IList<CompanyPayStructureBo_V2> list)
        {
            string message = string.Empty;
            foreach (CompanyPayStructureBo_V2 companyPayStructureBoV2 in list)
            {
                if (companyPayStructureBoV2.Name == "GRO" || companyPayStructureBoV2.Name == "TAX")
                {
                    companyPayStructureBoV2.IsActive = true;
                    if (companyPayStructureBoV2.Name == "TAX")
                    {
                        companyPayStructureBoV2.IsEditable = true;
                    }
                }
                if (companyPayStructureBoV2.Id == 0 && !companyPayStructureBoV2.IsActive)
                {
                }
                else
                {
                    using (Database.CreateConnection())
                    {
                        DbCommand dbCommand = Database.GetStoredProcCommand("Pay_ManageCompanyPayStructureComponents");
                        Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, companyPayStructureBoV2.Id);
                        Database.AddInParameter(dbCommand, DBParam.Input.CompanyPayStructureId, DbType.Int32,
                            companyPayStructureBoV2.CompanyPayStructureId);
                        Database.AddInParameter(dbCommand, DBParam.Input.ComponentId, DbType.Int32,
                            companyPayStructureBoV2.ComponentId);
                        Database.AddInParameter(dbCommand, DBParam.Input.Formula, DbType.String,
                            companyPayStructureBoV2.Formula);
                        Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                        Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                        Database.AddInParameter(dbCommand, DBParam.Input.IsEditable, DbType.Boolean,
                            companyPayStructureBoV2.IsEditable);
                        Database.AddInParameter(dbCommand, "@PayStub", DbType.Boolean,
                           companyPayStructureBoV2.PayStub);
                        Database.AddInParameter(dbCommand, "@Fixed", DbType.Boolean,
                           companyPayStructureBoV2.FixedComponent);
                        Database.AddInParameter(dbCommand, "@ShowinPaystructure", DbType.Boolean,
                           companyPayStructureBoV2.ShowinPaystructure);
                        Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean,
                              !companyPayStructureBoV2.IsActive );
                        message = Database.ExecuteScalar(dbCommand).ToString();
                    }
                }
            }

            return message;
        }
    }
}