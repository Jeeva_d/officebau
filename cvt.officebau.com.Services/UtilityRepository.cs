﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class UtilityRepository : BaseRepository
    {
        public void SaveMailToBackupTable(string fromEmailID, string toEmailID, string ccEmailID, string subject, string content)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Conn"].ToString();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = Constants.EMAIL_INSERTINTOBACKUPTABLE;
                command.Parameters.Add(DBParam.Input.FromID, SqlDbType.NVarChar).Value = fromEmailID;
                command.Parameters.Add(DBParam.Input.ToID, SqlDbType.NVarChar).Value = toEmailID;
                command.Parameters.Add(DBParam.Input.CC, SqlDbType.NVarChar).Value = ccEmailID;
                command.Parameters.Add(DBParam.Input.BCC, SqlDbType.NVarChar).Value = string.Empty;
                command.Parameters.Add(DBParam.Input.Subject, SqlDbType.NVarChar).Value = subject;
                command.Parameters.Add(DBParam.Input.Content, SqlDbType.NVarChar).Value = content;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }

                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
            }
        }

        public void SendEmail(string toEmailID, string ccEmailID, string content, string subject, string AttachetmentPath = "", string Bcc = "", string fileid = "")
        {
            string fromEmailID = string.IsNullOrEmpty(Utility.GetSession("CommunicationEmailId"))
                ? System.Web.Configuration.WebConfigurationManager.AppSettings["EmailAddress"]
                : Utility.GetSession("CommunicationEmailId");

            using (FSMEntities context = new FSMEntities())
            {
                tbl_EmailProcessor emailProcessor = new tbl_EmailProcessor
                {
                    FromID = fromEmailID,
                    ToID = toEmailID,
                    Cc = ccEmailID,
                    Bcc = Bcc,
                    AttachmentPath = AttachetmentPath,
                    Subject = subject,
                    Content = content,
                    Attachment = fileid
                };
                context.tbl_EmailProcessor.Add(emailProcessor);
                context.SaveChanges();
            }
        }

        public  void TrackFormNavigation(string formName)
        {
            using (var context = new FSMEntities())
            {
                context.tbl_LoginForms.Add(new tbl_LoginForms
                {
                    LoginHistoryID = Utility.LoginId(),
                    FormName = formName,
                    CreatedBy = Utility.UserId(),
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now,
                    ModifiedBy = Utility.UserId(),
                    DomainID = Utility.DomainId(),
                    HistoryID = Guid.NewGuid()
                });
                context.SaveChanges();
            }
        }

        public List<SelectListItem> GetFinancialYearText()
        {
            var masterDataRepository = new MasterDataRepository();

            var dependantDropDownList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            dependantDropDownList.Find(a => a.Text == DateTime.Now.Year.ToString()).Selected = true;
            foreach (var n in dependantDropDownList)
            {
                if (n.Text != "-- Select --")
                {
                    n.Text = n.Text + " - " + (Convert.ToInt32(n.Text.Remove(0, 2)) + 1);
                }
            }

            return dependantDropDownList;
        }
    }
}