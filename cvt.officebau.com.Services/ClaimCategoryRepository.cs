﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class ClaimCategoryRepository : BaseRepository
    {
        public List<MasterDataBo> GetClaimsCatergoryList(MasterDataBo masterDataBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<MasterDataBo> list = (from c in context.tbl_ClaimCategory
                                           where !c.IsDeleted && c.DomainID == masterDataBo.DomainId
                                           select new MasterDataBo
                                           {
                                               Id = c.ID,
                                               Code = c.Name,
                                               Description = c.Remarks,
                                               IsDeleted = c.IsDeleted
                                           }).ToList();
                return list;
            }
        }

        public MasterDataBo GetClaimsCategory(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                MasterDataBo result = new MasterDataBo();
                int domainId = Utility.DomainId();

                if (id != null)
                {
                    result = (from c in context.tbl_ClaimCategory
                              where c.DomainID == domainId && c.ID == id
                              select new MasterDataBo
                              {
                                  Id = c.ID,
                                  Code = c.Name,
                                  Description = c.Remarks,
                                  IsDeleted = c.IsDeleted
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public string ManageClaimCategory(MasterDataBo masterDataBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_CLAIMCATEGORY);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, masterDataBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, masterDataBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, masterDataBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, masterDataBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                masterDataBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return masterDataBo.UserMessage;
        }

        public List<EmployeeClaimsBo> SearchClaimsEligibilityList(int employeeId)
        {
            List<EmployeeClaimsBo> claimsPolicyList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_CLAIMSELIGIBILITYLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        claimsPolicyList.Add(new EmployeeClaimsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ExpenseType = Utility.CheckDbNull<string>(reader[DBParam.Output.ExpenseType]),
                            Band = Utility.CheckDbNull<string>(reader[DBParam.Output.Band]),
                            Level = Utility.CheckDbNull<string>(reader[DBParam.Output.Level]),
                            Designation = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]),
                            MetroAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MetroAmount]),
                            NonMetroAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.NonMetroAmount])
                        });
                    }
                }
            }

            return claimsPolicyList;
        }
    }
}