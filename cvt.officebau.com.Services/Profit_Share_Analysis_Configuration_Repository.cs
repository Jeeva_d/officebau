﻿using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class Profit_Share_Analysis_Configuration_Repository
    {
        public readonly FSMEntities datacontext = new FSMEntities();

        public List<Profit_Share_Analysis_ConfigurationBo> ProfitShareAnalysisConfigurationSearch(string employeeName)
        {
            var result = (from c in datacontext.tbl_ShareMapping
                          where c.tbl_EmployeeMaster.FullName.ToUpper().Contains(string.IsNullOrEmpty(employeeName) ? c.tbl_EmployeeMaster.FullName.ToUpper() : employeeName.ToUpper())
                           && !c.IsDeleted
                          orderby c.ModifiedOn descending
                          select new Profit_Share_Analysis_ConfigurationBo
                          {
                              Id = c.Id,
                              EmployeeName = c.tbl_EmployeeMaster.FullName,
                              CustomerName = c.tblCustomer.Name,
                              VendorName = c.tblVendor.Name,
                              Percentage = c.Percentage
                          }).ToList();
            return result;
        }

        public Profit_Share_Analysis_ConfigurationBo GetProfitShareAnalysisConfiguration(int id)
        {
            var Share = datacontext.tbl_ShareMapping.FirstOrDefault(a => a.Id == id);
            Profit_Share_Analysis_ConfigurationBo profit_Share_Analysis_ConfigurationBo = new Profit_Share_Analysis_ConfigurationBo
            {
                Id = Share.Id,
                EmployeeId = Share.EmployeeId,
                IsCustomer = Share.IsCustomer,
                CustomerId = Share.CustomerId,
                VendorId = Share.VendorId,
                VendorName = Share.tblVendor?.Name,
                EmployeeName = Share.tbl_EmployeeMaster?.FullName,
                CustomerName = Share.tblCustomer?.Name,
                Percentage = Share.Percentage
            };

            return profit_Share_Analysis_ConfigurationBo;
        }

        public string ManageProfitShareAnalysisConfiguration(Profit_Share_Analysis_ConfigurationBo profit_Share_Analysis_ConfigurationBo)
        {
            tbl_ShareMapping tbl_ShareMapping = new tbl_ShareMapping();
            string message = Notification.Inserted;
            if (profit_Share_Analysis_ConfigurationBo.Id != 0)
            {
                tbl_ShareMapping = datacontext.tbl_ShareMapping.FirstOrDefault(a => a.Id == profit_Share_Analysis_ConfigurationBo.Id && !a.IsDeleted);
                message = Notification.Updated;
            }
            else
            {
                tbl_ShareMapping.CreatedBy = Utility.UserId();
                tbl_ShareMapping.CreatedOn = Utility.GetCurrentDate();
                tbl_ShareMapping.IsDeleted = false;
            }
            if (profit_Share_Analysis_ConfigurationBo.IsDeleted)
            {
                tbl_ShareMapping = datacontext.tbl_ShareMapping.FirstOrDefault(a => a.Id == profit_Share_Analysis_ConfigurationBo.Id && !a.IsDeleted);
                tbl_ShareMapping.IsDeleted = true;
                datacontext.Entry(tbl_ShareMapping).State = EntityState.Modified;
                datacontext.SaveChanges();

                return Notification.Deleted;
            }

            tbl_ShareMapping.EmployeeId = profit_Share_Analysis_ConfigurationBo.EmployeeId;
            tbl_ShareMapping.CustomerId = profit_Share_Analysis_ConfigurationBo.CustomerId;
            tbl_ShareMapping.VendorId = profit_Share_Analysis_ConfigurationBo.VendorId;
            tbl_ShareMapping.IsCustomer = profit_Share_Analysis_ConfigurationBo.IsCustomer;
            tbl_ShareMapping.Percentage = profit_Share_Analysis_ConfigurationBo.Percentage;
            tbl_ShareMapping.ModifiedOn = Utility.GetCurrentDate();
            tbl_ShareMapping.ModifiedBy = Utility.UserId();

            if (profit_Share_Analysis_ConfigurationBo.Id == 0)
                datacontext.tbl_ShareMapping.Add(tbl_ShareMapping);
            else
                datacontext.Entry(tbl_ShareMapping).State = EntityState.Modified;
            datacontext.SaveChanges();
            return message;

        }
    }
}
