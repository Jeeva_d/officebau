﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class PayrollDashboadRepository : BaseRepository
    {
        public PayrollBO GetLastMonthYear()
        {
            int monthId, yearId, baselocationId, userId = Utility.UserId(), domainId = Utility.DomainId();
            PayrollBO payrollbo = new PayrollBO();
            using (FSMEntities context = new FSMEntities())
            {
                baselocationId = Convert.ToInt32(context.tbl_EmployeeMaster.Where(emp => !emp.IsDeleted && emp.ID == userId).Select(b => b.BaseLocationID).FirstOrDefault());
                yearId = Convert.ToInt32(context.tbl_Pay_EmployeePayroll.Where(pay => !pay.IsDeleted && pay.IsProcessed && pay.DomainId == domainId).OrderByDescending(b => b.YearId).Select(b => b.YearId).FirstOrDefault());
                monthId = Convert.ToInt32(context.tbl_Pay_EmployeePayroll.Where(pay => !pay.IsDeleted && pay.IsProcessed && pay.DomainId == domainId && pay.YearId == yearId).OrderByDescending(b => b.MonthId).Select(b => b.MonthId).FirstOrDefault());
            }

            payrollbo.MonthID = monthId;
            payrollbo.YearID = yearId;
            return payrollbo;
        }

        public PayrollBO GetDashboardTiles(int? monthId, int yearId)
        {
            PayrollBO payrollBo = new PayrollBO();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PAYROLLDASHBOARDTILES);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        payrollBo = new PayrollBO
                        {
                            NetSalary = Utility.CheckDbNull<decimal>(reader[DBParam.Output.NetSalary]),
                            EEPF = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PF]),
                            EEESI = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ESI]),
                            TDS = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS])
                        };
                    }
                }
            }

            return payrollBo;
        }

        public List<PayrollBO> GetbuWise(string type, int? monthId, int yearId)
        {
            List<PayrollBO> boList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PAYROLLDASHBOARDBU);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            NetSalary = Utility.CheckDbNull<decimal>(reader[DBParam.Output.NetSalary])
                        };
                        boList.Add(payrollBo);
                    }
                }
            }

            return boList;
        }

        public List<PayrollBO> GetProjectedNetPay(string bu, string type, int? monthId, int yearId)
        {
            List<PayrollBO> boList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETPROJECTEDNETPAY_DASHBOARD);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, bu);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.String, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.String, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.MonthYear]),
                            NetSalary = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Net])
                        };
                        boList.Add(payrollBo);
                    }
                }
            }

            return boList;
        }

        public DataTable GetEmployeeList(string bu, string type, int? monthId, int yearId)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Payrolldashboard_emplist_v2");

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, bu);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                DataSet ds = new DataSet();
                DataTable employeepayrollReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(employeepayrollReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, employeepayrollReport);
                }

                return employeepayrollReport;
            }
        }
    }
}