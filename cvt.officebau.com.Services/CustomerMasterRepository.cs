﻿using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class CustomerMasterRepository : BaseRepository
    {
        public List<CustomerBo> GetCustomerList(CustomerBo customerBo)
        {
            List<CustomerBo> customerBoList = new List<CustomerBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_CUSTOMER);

                Database.AddInParameter(dbCommand, DBParam.Input.City, DbType.String, customerBo.CityName);
                Database.AddInParameter(dbCommand, DBParam.Input.OrderName, DbType.String, customerBo.OrderName);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, customerBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.SalesPersonID, DbType.Int32, customerBo.SalesPersonId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                bool excelExport = customerBo.Operation.ToLower() == "excelexport";

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        customerBoList.Add(
                            new CustomerBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                EnquiryName = Utility.CheckDbNull<string>(reader[DBParam.Output.EnquiryName]),
                                OrderName = Utility.CheckDbNull<string>(reader[DBParam.Output.OrderName]),
                                CountryName = Utility.CheckDbNull<string>(reader[DBParam.Output.Country]),
                                CityName = Utility.CheckDbNull<string>(reader[DBParam.Output.City]),
                                MonthlyBusinessVolume = Utility.CheckDbNull<string>(reader[DBParam.Output.MonthlyBusinessVolume]),
                                TypeofBusiness = Utility.CheckDbNull<string>(reader[DBParam.Output.TypeofBusiness]),
                                CustomerCount = Utility.CheckDbNull<int>(reader[DBParam.Output.Count]),
                                SalesPersonName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),

                                // For excel export
                                EmailId = excelExport ? Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]) : string.Empty,
                                ContactNo = excelExport ? Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]) : string.Empty,
                                StateName = excelExport ? Utility.CheckDbNull<string>(reader[DBParam.Output.State]) : string.Empty,
                                StatusName = excelExport ? Utility.CheckDbNull<string>(reader[DBParam.Output.Status]) : string.Empty,
                                Address1 = excelExport ? Utility.CheckDbNull<string>(reader[DBParam.Output.Address1]) : string.Empty,
                                Address2 = excelExport ? Utility.CheckDbNull<string>(reader[DBParam.Output.Address2]) : string.Empty,
                                CreatedByName = excelExport ? Utility.CheckDbNull<string>(reader[DBParam.Output.CreatedBy]) : string.Empty,
                                CreatedOn = excelExport ? Utility.CheckDbNull<DateTime>(reader[DBParam.Output.CreatedOn]) : DateTime.MinValue
                            });
                    }
                }
            }

            return customerBoList;
        }

        public CustomerBo GetCustomer(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                CustomerBo result = new CustomerBo();
                int domainId = Utility.DomainId();

                if (id != null)
                {
                    result = (from c in context.tbl_Customer
                              join e in context.tbl_EmployeeMaster on c.ModifiedBy equals e.ID
                              join bu in context.tbl_BusinessUnit on e.BaseLocationID equals bu.ID into business
                              from bUnit in business.DefaultIfEmpty()
                              where c.DomainID == domainId && c.ID == id
                              select new CustomerBo
                              {
                                  Id = c.ID,
                                  EnquiryName = c.EnquiryName,
                                  OrderName = c.OrderName,
                                  CountryId = c.CountryID,
                                  StateId = c.StateID,
                                  CityId = c.CityID,
                                  Address1 = c.Address1,
                                  Address2 = c.Address2,
                                  IsDeleted = c.IsDeleted,
                                  StatusId = c.StatusID,
                                  TypeofBusiness = c.TypeofBusiness,
                                  MonthlyBusinessVolume = c.MonthlyBusinessVolume,
                                  SalesPersonId = c.SalesPersonID,
                                  ContactNo = c.ContactNo,
                                  EmailId = c.EmailID,
                                  ModifiedByName = e.FullName,
                                  ModifiedOn = c.ModifiedOn,
                                  BaseLocation = bUnit.Name
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public string CreateCustomerMaster(CustomerBo customerBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int check = 0;
                int domainId = Utility.DomainId();

                if (customerBo.EnquiryName != null)
                {
                    check = context.tbl_Customer.Count(s => s.EnquiryName == customerBo.EnquiryName && !s.IsDeleted && s.DomainID == domainId);
                }

                if (check == 0)
                {
                    tbl_Customer customer = new tbl_Customer
                    {
                        EnquiryName = customerBo.EnquiryName,
                        OrderName = customerBo.OrderName,
                        CountryID = customerBo.CountryId,
                        StateID = customerBo.StateId,
                        CityID = customerBo.CityId,
                        Address1 = customerBo.Address1,
                        Address2 = customerBo.Address2,
                        StatusID = customerBo.StatusId,
                        TypeofBusiness = customerBo.TypeofBusiness,
                        MonthlyBusinessVolume = customerBo.MonthlyBusinessVolume,
                        SalesPersonID = customerBo.SalesPersonId,
                        ContactNo = customerBo.ContactNo,
                        EmailID = customerBo.EmailId,
                        IsDeleted = customerBo.IsDeleted,
                        CreatedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = Utility.UserId(),
                        DomainID = Utility.DomainId(),
                        HistoryID = Guid.NewGuid()
                    };

                    context.tbl_Customer.Add(customer);
                    context.SaveChanges();

                    customerBo.Id = Convert.ToInt32(context.tbl_Customer.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                    customerBo.EnquiryName = context.tbl_Customer.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.EnquiryName).FirstOrDefault();
                    return Notification.Inserted+"/" + customerBo.EnquiryName + "/" + customerBo.Id;
                }

                return Notification.AlreadyExists;
            }
        }

        public string UpdateCustomerMaster(CustomerBo customerBo)
        {
            FSMEntities context = new FSMEntities();
            int check = 0;
            int domainId = Utility.DomainId();

            if (customerBo.EnquiryName != null)
            {
                check = context.tbl_Customer.Count(s => s.EnquiryName == customerBo.EnquiryName && !s.IsDeleted && s.ID != customerBo.Id && s.DomainID == domainId);
            }

            if (check == 0)
            {
                tbl_Customer customer = context.tbl_Customer.FirstOrDefault(b => b.ID == customerBo.Id);
                if (customer != null)
                {
                    customer.ID = customerBo.Id;
                    customer.EnquiryName = customerBo.EnquiryName;
                    customer.OrderName = customerBo.OrderName;
                    customer.CountryID = customerBo.CountryId;
                    customer.StateID = customerBo.StateId;
                    customer.CityID = customerBo.CityId;
                    customer.Address1 = customerBo.Address1;
                    customer.Address2 = customerBo.Address2;
                    customer.StatusID = customerBo.StatusId;
                    customer.TypeofBusiness = customerBo.TypeofBusiness;
                    customer.MonthlyBusinessVolume = customerBo.MonthlyBusinessVolume;
                    customer.SalesPersonID = customerBo.SalesPersonId;
                    customer.ContactNo = customerBo.ContactNo;
                    customer.EmailID = customerBo.EmailId;
                    customer.IsDeleted = customerBo.IsDeleted;
                    customer.ModifiedOn = DateTime.Now;
                    customer.ModifiedBy = Utility.UserId();
                    context.Entry(customer).State = EntityState.Modified;
                }

                context.SaveChanges();
                customerBo.Id = customerBo.Id;
                return Notification.Updated;
            }

            return Notification.AlreadyExists;
        }

        public string DeleteCustomerMaster(CustomerBo customerBo)
        {
            string message = string.Empty;
            DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DELETE_CUSTOMERMASTER);

            Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, customerBo.Id);
            Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, customerBo.UserId);
            Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, customerBo.DomainId);

            using (IDataReader reader = Database.ExecuteReader(dbCommand))
            {
                while (reader.Read())
                {
                    message = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage]);
                }
            }

            return message;
        }

        public List<CustomerBo> GetCountryList()
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<CustomerBo> list = (from t in context.tbl_Country
                                         where !t.IsDeleted && t.DomainID == domainId
                                         orderby t.Name
                                         select new CustomerBo
                                         {
                                             Name = t.Name,
                                             Id = t.ID
                                         }).ToList();
                return list;
            }
        }

        public List<CustomerBo> GetStateList(int? countryId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<CustomerBo> list = (from t in context.tbl_State
                                         where !t.IsDeleted && t.DomainID == domainId && t.CountryID == countryId
                                         orderby t.Name
                                         select new CustomerBo
                                         {
                                             Name = t.Name,
                                             Id = t.ID
                                         }).ToList();
                return list;
            }
        }

        public List<CustomerBo> GetCityList(int? stateId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<CustomerBo> list = (from t in context.tbl_City
                                         where !t.IsDeleted && t.DomainID == domainId && t.StateID == stateId
                                         orderby t.Name
                                         select new CustomerBo
                                         {
                                             Name = t.Name,
                                             Id = t.ID
                                         }).ToList();
                return list;
            }
        }
    }

    public class CustomerRepository : BaseRepository
    {
        #region Get Customer

        public CustomerDetailsBo GetCustomer(int customerId)
        {
            CustomerDetailsBo customerDetailsBo = new CustomerDetailsBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_CUSTOMER);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, customerId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        customerDetailsBo = new CustomerDetailsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Address = Utility.CheckDbNull<string>(reader[DBParam.Output.Address]),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms]),
                            ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                            ContactPerson = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactPerson]),
                            EmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]),
                            ContactPersonNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactPersonNo]),
                            CustomerEmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerEmailID]),
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.BillingAddress]),
                            City = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CityID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.CityName])
                            },
                            Currency = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CurrencyID])
                            },
                            BankID = new cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.BankID])                               
                            },
                            PanNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PanNO]),
                            TanNo = Utility.CheckDbNull<string>(reader[DBParam.Output.TanNo]),
                            GstNo = Utility.CheckDbNull<string>(reader[DBParam.Output.GSTNo]),                           
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy])
                        };
                    }
                }
            }

            return customerDetailsBo;
        }

        #endregion Get Customer

        #region Search Customer

        public List<CustomerDetailsBo> SearchCustomer(CustomerDetailsBo customerDetailsBo)
        {
            List<CustomerDetailsBo> customerDetailsBoList = new List<CustomerDetailsBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_CUSTOMER2);

                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, customerDetailsBo.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        customerDetailsBoList.Add(
                            new CustomerDetailsBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                                ContactPersonNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactPersonNo]),
                                ContactPerson = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactPerson]),
                                City = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.CityName])
                                },
                                Currency = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.CurrencyName])
                                },
                                PanNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PANNo]),
                                GstNo = Utility.CheckDbNull<string>(reader[DBParam.Output.GSTNo]),
                                BankID = new cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName])
                                },
                            });
                    }
                }
            }

            return customerDetailsBoList;
        }

        #endregion Search Customer

        #region Manage Customer

        public string ManageCustomer(CustomerDetailsBo customerDetailsBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_CUSTOMER2);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, customerDetailsBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.CityID, DbType.String, customerDetailsBo.City == null ? null : customerDetailsBo.City.Id == 0 ? null : customerDetailsBo.City.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.CurrencyID, DbType.String, customerDetailsBo.Currency == null ? null : customerDetailsBo.Currency.Id == 0 ? null : customerDetailsBo.Currency.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, customerDetailsBo.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.Address, DbType.String, customerDetailsBo.Address);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, customerDetailsBo.Remarks);
                Database.AddInParameter(dbCommand, DBParam.Input.PaymentTerms, DbType.String, customerDetailsBo.PaymentTerms);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, customerDetailsBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.contactPerson, DbType.String, customerDetailsBo.ContactPerson);
                Database.AddInParameter(dbCommand, DBParam.Input.EmailID, DbType.String, customerDetailsBo.EmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.contactPersonNo, DbType.String, customerDetailsBo.ContactPersonNo);
                Database.AddInParameter(dbCommand, DBParam.Input.CustomerEmailID, DbType.String, customerDetailsBo.CustomerEmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.BillingAddress, DbType.String, customerDetailsBo.BillingAddress);
                Database.AddInParameter(dbCommand, DBParam.Input.PanNo, DbType.String, customerDetailsBo.PanNo);
                Database.AddInParameter(dbCommand, DBParam.Input.TanNo, DbType.String, customerDetailsBo.TanNo);
                Database.AddInParameter(dbCommand, DBParam.Input.GSTNo, DbType.String, customerDetailsBo.GstNo);
                Database.AddInParameter(dbCommand, DBParam.Input.BankID, DbType.String, customerDetailsBo.BankID == null ? null : customerDetailsBo.BankID.Id == 0 ? null : customerDetailsBo.BankID.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, customerDetailsBo.IsDeleted);

                customerDetailsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return customerDetailsBo.UserMessage;
        }

        #endregion Manage Customer
    }
}