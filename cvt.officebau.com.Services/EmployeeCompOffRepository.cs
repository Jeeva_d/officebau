﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;

namespace cvt.officebau.com.Services
{
    public class EmployeeCompOffRepository : BaseRepository
    {
        UtilityRepository email = new UtilityRepository();
        #region Comp Off Request

        public string GetApprover(int employeeId, string type)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int reportingId = Convert.ToInt32(context.tbl_EmployeeMaster.Where(b => b.ID == employeeId && !b.IsDeleted && !(b.IsActive ?? false)).OrderByDescending(b => b.ID).Select(b => b.ReportingToID).FirstOrDefault());

                if (reportingId != 0)
                {
                    var result = (from e in context.tbl_EmployeeMaster
                                  where e.ID == reportingId && !e.IsDeleted && !(e.IsActive ?? false)
                                  select new
                                  {
                                      Approver = e.FullName,
                                      ApproverID = e.ID,
                                      e.Code,
                                      e.EmpCodePattern
                                  }).FirstOrDefault();
                    if (type.ToUpper() == "NAME")
                    {
                        return result == null ? "" : (result.EmpCodePattern ?? "") + (result.Code ?? "") + " - " + result.Approver;
                    }

                    return result?.ApproverID.ToString();
                }

                return null;
            }
        }

        public string CalculateDuration(int employeeId, DateTime date)
        {
            using (FSMEntities context = new FSMEntities())
            {
                string duration = Convert.ToString(context.tbl_Attendance.Where(c => c.EmployeeID == employeeId && !c.IsDeleted && c.LogDate == date).Select(c => c.Duration).FirstOrDefault());
                return string.IsNullOrEmpty(duration) ? "00:00" : duration.Substring(0, 5);
            }
        }

        public string ManageCompOffRequest(EmployeeCompOffBo employeeCompOffBo)
        {
            string outputMessage;
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_COMPOFFREQUEST);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeCompOffBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeCompOffBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.Date, DbType.Date, employeeCompOffBo.Date);
                Database.AddInParameter(dbCommand, DBParam.Input.RequesterRemarks, DbType.String, employeeCompOffBo.RequesterRemarks);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, employeeCompOffBo.ApproverId);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, employeeCompOffBo.IsDeleted);

                outputMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            if (outputMessage.ToLower().Contains("successfully") && !outputMessage.ToLower().Contains("deleted"))
            {
                
                int compOffRequestId = Convert.ToInt32(outputMessage.Split('/')[1]);
                employeeCompOffBo = GetCompOffRequest(compOffRequestId);
                email.SendEmail(employeeCompOffBo.ApproverEmail, employeeCompOffBo.EmployeeEmail,
                    PopulateCompOffMailBody(employeeCompOffBo, "Request", string.Empty),
                    "Comp off - Request : " + employeeCompOffBo.EmployeeEmail + " ( " + employeeCompOffBo.EmployeeName + " )");
            }

            return outputMessage.Split('/')[0];
        }

        public EmployeeCompOffBo GetCompOffRequest(int id)
        {
            EmployeeCompOffBo employeeCompOffBo = new EmployeeCompOffBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_COMPOFFREQUEST);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeCompOffBo = new EmployeeCompOffBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                            Duration = Utility.CheckDbNull<TimeSpan>(reader[DBParam.Output.Duration]),
                            RequesterRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                            ApproverRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.ApproverRemarks]),
                            ApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                            ApproverId = Utility.CheckDbNull<int>(reader[DBParam.Output.ApproverID]),
                            ApproverName = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovarName]),
                            StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ApproverDesignation = Utility.CheckDbNull<string>(reader[DBParam.Output.ApproverDesignation]),
                            ApproverPrefix = Utility.CheckDbNull<string>(reader[DBParam.Output.ApproverPrefix]),
                            Designation = Utility.CheckDbNull<string>(reader[DBParam.Output.Designation]),
                            Prefix = Utility.CheckDbNull<string>(reader[DBParam.Output.Prefix]),
                            EmployeeEmail = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeEmail]),
                            ApproverEmail = Utility.CheckDbNull<string>(reader[DBParam.Output.ApproverEmail]),
                            HrEmail = Utility.CheckDbNull<string>(reader[DBParam.Output.HREmail]),
                            HrRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.HRRemarks]),
                            HrName = Utility.CheckDbNull<string>(reader[DBParam.Output.HRName]),
                            HrDesignation = Utility.CheckDbNull<string>(reader[DBParam.Output.HRDesignation]),
                            HrPrefix = Utility.CheckDbNull<string>(reader[DBParam.Output.HRPrefix]),
                            HRApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.HRApprovedDate]),
                            HrStatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.HRStatus]),
                        };
                    }
                }
            }

            return employeeCompOffBo;
        }

        public List<EmployeeCompOffBo> SearchCompOffRequest(EmployeeCompOffBo employeeCompOffBo)
        {
            List<EmployeeCompOffBo> employeeCompOffBoList = new List<EmployeeCompOffBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_COMPOFFREQUEST);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeCompOffBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeCompOffBo.StatusId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeCompOffBoList.Add(
                            new EmployeeCompOffBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                Duration = Utility.CheckDbNull<TimeSpan>(reader[DBParam.Output.Duration]),
                                RequesterRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                                ApproverName = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovarName]),
                                ApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                                StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                HrName = Utility.CheckDbNull<string>(reader[DBParam.Output.HRName]),
                                HRApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.HRApprovedDate]),
                            });
                    }
                }

                return employeeCompOffBoList;
            }
        }

        public int GetStatusId(string type, string codeName)
        {
            using (FSMEntities context = new FSMEntities())
            {
                return context.tbl_Status.FirstOrDefault(s => !s.IsDeleted && s.Type == type && s.Code == codeName).ID;
            }
        }

        private string PopulateCompOffMailBody(EmployeeCompOffBo employeeCompOffBo, string type, string remarks)
        {
            string content = string.Empty;
            string regards;
            string designation;

            if (type == "Request")
            {
                content = @"I would like to request a comp off for the below mentioned dates.
                            I would be glad to help with a plan to cover my workload in my absence.
                            Thank you for your consideration of my request.";
            }
            else if (type.Contains("Approved"))
            {
                content = "Your comp off request has been approved. <br/>";
            }
            else if (type.Contains("Rejected"))
            {
                content = "Your comp off request has been rejected. <br/>";
            }

            if (type.Contains("Manager"))
            {
                regards = employeeCompOffBo.ApproverName;
                designation = employeeCompOffBo.ApproverDesignation;
            }
            else if (type.Contains("HR"))
            {
                regards = employeeCompOffBo.HrName;
                designation = employeeCompOffBo.HrDesignation;
            }
            else
            {
                regards = employeeCompOffBo.EmployeeName;
                designation = employeeCompOffBo.Designation;
            }

            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/CompOff.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Name}", type == "Request" ? employeeCompOffBo.ApproverName : employeeCompOffBo.EmployeeName);
            body = body.Replace("{Content}", content);
            body = body.Replace("{Date}", Convert.ToDateTime(employeeCompOffBo.Date).ToString("dd-MMM-yyyy"));
            body = body.Replace("{LeaveType}", "Comp Off");
            body = body.Replace("{Reason}", employeeCompOffBo.RequesterRemarks);
            body = body.Replace("{Remarks}", type == "Request" ? string.Empty : "<b>Remarks : </b>" + remarks);
            body = body.Replace("{Regards}", regards);
            body = body.Replace("{Prefix}", type == "Request" ? employeeCompOffBo.ApproverPrefix : employeeCompOffBo.Prefix);
            body = body.Replace("{Designation}", designation);
            return body;
        }

        #endregion Comp Off Request

        #region Comp Off Approval

        public List<EmployeeCompOffBo> SearchCompOffApproval(EmployeeCompOffBo employeeCompOffBo)
        {
            List<EmployeeCompOffBo> employeeCompOffBoList = new List<EmployeeCompOffBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_COMPOFFAPPROVAL);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeCompOffBo.StatusId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeCompOffBoList.Add(
                            new EmployeeCompOffBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                                Duration = Utility.CheckDbNull<TimeSpan>(reader[DBParam.Output.Duration]),
                                CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.CreatedOn]),
                                RequesterRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                                ApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                                StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status])
                            });
                    }
                }

                return employeeCompOffBoList;
            }
        }

        public string ManageCompOffApproval(EmployeeCompOffBo employeeCompOffBo)
        {
            string outputMessage;
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_COMPOFFAPPROVAL);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeCompOffBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.RequesterID, DbType.Int32, employeeCompOffBo.RequesterId);
                Database.AddInParameter(dbCommand, DBParam.Input.Date, DbType.Date, employeeCompOffBo.Date);
                Database.AddInParameter(dbCommand, DBParam.Input.RequesterRemarks, DbType.String, employeeCompOffBo.RequesterRemarks ?? "");
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverRemarks, DbType.String, employeeCompOffBo.ApproverRemarks);
                Database.AddInParameter(dbCommand, DBParam.Input.HRApproverID, DbType.Int32, employeeCompOffBo.HrApproverId);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, employeeCompOffBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, employeeCompOffBo.StatusCode);
                Database.AddInParameter(dbCommand, DBParam.Input.ModifiedBy, DbType.Int32, Utility.UserId());

                outputMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            if (outputMessage.ToLower().Contains("successfully"))
            {
                
                int compOffRequestId = Convert.ToInt32(outputMessage.Split('/')[1]);
                if (outputMessage.ToLower().Contains("approved"))
                {
                    employeeCompOffBo = GetCompOffRequest(compOffRequestId);
                    email.SendEmail(employeeCompOffBo.EmployeeEmail, employeeCompOffBo.ApproverEmail + ";" + employeeCompOffBo.HrEmail,
                        PopulateCompOffMailBody(employeeCompOffBo, "Manager Approved", employeeCompOffBo.ApproverRemarks),
                        "Comp off - Approved (Manager) : " + employeeCompOffBo.ApproverEmail + " ( " + employeeCompOffBo.ApproverName + " )");
                }
                else if (outputMessage.ToLower().Contains("rejected"))
                {
                    employeeCompOffBo = GetCompOffRequest(compOffRequestId);
                    email.SendEmail(employeeCompOffBo.EmployeeEmail, employeeCompOffBo.ApproverEmail,
                        PopulateCompOffMailBody(employeeCompOffBo, "Manager Rejected", employeeCompOffBo.ApproverRemarks),
                        "Comp off - Rejected (Manager) : " + employeeCompOffBo.ApproverEmail + " ( " + employeeCompOffBo.ApproverName + " )");
                }
            }

            return outputMessage.Split('/')[0];
        }

        #endregion Comp Off Approval

        #region Comp off HR Approval

        public List<EmployeeCompOffBo> CompOffHrApprovalList(EmployeeCompOffBo employeeCompOffBo)
        {
            List<EmployeeCompOffBo> employeeCompOffList = new List<EmployeeCompOffBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_COMPOFFHRAPPROVAL);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeCompOffBo.StatusId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeCompOffList.Add(
                            new EmployeeCompOffBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                                Duration = Utility.CheckDbNull<TimeSpan>(reader[DBParam.Output.Duration]),
                                CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.CreatedOn]),
                                RequesterRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                                ApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                                StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status])
                            });
                    }
                }

                return employeeCompOffList;
            }
        }

        public string ManageCompOffHrApproval(EmployeeCompOffBo employeeCompOffBo)
        {
            string outputMessage;
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_COMPOFFHRAPPROVAL);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeCompOffBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.RequesterID, DbType.Int32, employeeCompOffBo.RequesterId);
                Database.AddInParameter(dbCommand, DBParam.Input.Date, DbType.Date, employeeCompOffBo.Date);
                Database.AddInParameter(dbCommand, DBParam.Input.RequesterRemarks, DbType.String, employeeCompOffBo.RequesterRemarks ?? "");
                Database.AddInParameter(dbCommand, DBParam.Input.HRRemarks, DbType.String, employeeCompOffBo.HrRemarks);
                Database.AddInParameter(dbCommand, DBParam.Input.HRApproverID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, employeeCompOffBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, employeeCompOffBo.StatusCode);
                Database.AddInParameter(dbCommand, DBParam.Input.ModifiedBy, DbType.Int32, Utility.UserId());

                outputMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            if (outputMessage.ToLower().Contains("successfully"))
            {
                int compOffRequestId = Convert.ToInt32(outputMessage.Split('/')[1]);
                if (outputMessage.ToLower().Contains("approved"))
                {
                    employeeCompOffBo = GetCompOffRequest(compOffRequestId);
                    email.SendEmail(employeeCompOffBo.EmployeeEmail, employeeCompOffBo.HrEmail,
                        PopulateCompOffMailBody(employeeCompOffBo, "HR Approved", employeeCompOffBo.ApproverRemarks),
                        "Comp off - Approved (HR) : " + employeeCompOffBo.HrEmail + " ( " + employeeCompOffBo.HrName + " )");
                }
                else if (outputMessage.ToLower().Contains("rejected"))
                {
                    employeeCompOffBo = GetCompOffRequest(compOffRequestId);
                    email.SendEmail(employeeCompOffBo.EmployeeEmail, employeeCompOffBo.HrEmail,
                        PopulateCompOffMailBody(employeeCompOffBo, "HR Rejected", employeeCompOffBo.ApproverRemarks),
                        "Comp off - Rejected (HR) : " + employeeCompOffBo.HrEmail + " ( " + employeeCompOffBo.HrName + " )");
                }
            }

            return outputMessage.Split('/')[0];
        }

        #endregion Comp off HR Approval
    }
}