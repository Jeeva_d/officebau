﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class HolidayRepository : BaseRepository
    {
        #region Holiday

        public List<HolidayBo> SearchHoliday(HolidayBo searchBo)
        {
            List<HolidayBo> holidayList = new List<HolidayBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHHOLIDAY);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, 0);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, searchBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, searchBo.HolidayType);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.RegionID, DbType.Int32, searchBo.RegionId);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        HolidayBo holidayBo = new HolidayBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            HolidayDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.HolidayDate]),
                            Holiday = Utility.CheckDbNull<string>(reader[DBParam.Output.Holiday]),
                            HolidayType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            IsOptional = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOptional]),
                            Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region])
                        };
                        holidayList.Add(holidayBo);
                    }
                }
            }

            return holidayList;
        }

        public HolidayBo GetHoliday(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                return (from h in context.tbl_Holidays
                        join m in context.tbl_EmployeeMaster on h.ModifiedBy equals m.ID
                        join bu in context.tbl_BusinessUnit on m.BaseLocationID equals bu.ID into business
                        from bUnit in business.DefaultIfEmpty()
                        where !h.IsDeleted && h.ID == id
                        select new HolidayBo
                        {
                            Id = h.ID,
                            HolidayDate = h.Date,
                            HolidayType = h.Type,
                            Year = h.Year,
                            BusinessUnit = h.RegionID,
                            Description = h.Description,
                            ModifiedByName = m.FullName,
                            ModifiedOn = h.ModifiedOn,
                            BaseLocation = bUnit.Name
                        }).FirstOrDefault();
            }
        }

        public string ManageHoliday(HolidayBo holidayBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_HOLIDAYS);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, holidayBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.HolidayDate, DbType.Date, holidayBo.HolidayDate);
                Database.AddInParameter(dbCommand, DBParam.Input.HolidayType, DbType.String, holidayBo.HolidayType);
                Database.AddInParameter(dbCommand, DBParam.Input.Description, DbType.String, holidayBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.IsOptional, DbType.Boolean, holidayBo.IsOptional);
                Database.AddInParameter(dbCommand, DBParam.Input.RegionIDs, DbType.String, holidayBo.BusinessUnit);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, holidayBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, holidayBo.Year);
                return Database.ExecuteScalar(dbCommand).ToString();
            }
        }

        #endregion Holiday

        #region Business Hours

        public List<HolidayBo> SearchBusinessHours(int domainId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<HolidayBo> policyList = (from h in context.tbl_BusinessHours
                                              join b in context.tbl_BusinessUnit on h.RegionID equals b.ID
                                              where !h.IsDeleted && h.DomainID == domainId
                                              orderby h.ModifiedOn descending
                                              select new HolidayBo
                                              {
                                                  Id = h.ID,
                                                  FullDay = h.FullDay,
                                                  HalfDay = h.HalfDay,
                                                  Region = b.Name,
                                                  BusinessStartHour = h.BusinessStartHour,
                                                  BusinessEndHour = h.BusinessEndHour,
                                                  IsBasedOnLastCheckout = h.IsBasedOnLastCheckout,
                                                  IsIncludeLeaveDetailsInPaystub = h.IncludeLeaveDetailsInPaystub ?? false
                                              }).ToList();
                return policyList;
            }
        }

        public HolidayBo GetBusinessHours(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                HolidayBo policyDetails = (from h in context.tbl_BusinessHours
                                           where !h.IsDeleted && h.ID == id
                                           select new HolidayBo
                                           {
                                               Id = h.ID,
                                               FullDay = h.FullDay,
                                               HalfDay = h.HalfDay,
                                               RegionId = h.RegionID,
                                               BusinessStartHour = h.BusinessStartHour,
                                               BusinessEndHour = h.BusinessEndHour,
                                               IsBasedOnLastCheckout = h.IsBasedOnLastCheckout,
                                               IsIncludeLeaveDetailsInPaystub = h.IncludeLeaveDetailsInPaystub ?? false
                                           }).FirstOrDefault();
                return policyDetails;
            }
        }

        public string ManageBusinessHours(HolidayBo businessHoursBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int check = context.tbl_BusinessHours.Count(h => !h.IsDeleted && h.DomainID == businessHoursBo.DomainId
                                                                              && h.ID != businessHoursBo.Id && h.RegionID == businessHoursBo.RegionId);

                if (businessHoursBo.IsDeleted)
                {
                    var checkBh = (from a in context.tbl_Attendance
                                   from e in context.tbl_EmployeeMaster
                                                    .Where(em => em.ID == a.EmployeeID && !em.IsDeleted)
                                   where (e.RegionID ?? 0) == businessHoursBo.RegionId
                                   select new { a.EmployeeID }).ToList();
                    if (!checkBh.Any())
                    {
                        tbl_BusinessHours deleteBh = context.tbl_BusinessHours.FirstOrDefault(p => !p.IsDeleted && p.ID == businessHoursBo.Id);
                        if (deleteBh != null)
                        {
                            deleteBh.IsDeleted = businessHoursBo.IsDeleted;
                            deleteBh.ModifiedBy = Utility.UserId();
                            deleteBh.ModifiedOn = DateTime.Now;

                            context.Entry(deleteBh).State = EntityState.Modified;
                        }

                        context.SaveChanges();

                        return Notification.Deleted;
                    }

                    return Notification.Record_Referred;
                }

                if (check == 0)
                {
                    if (businessHoursBo.Id != 0)
                    {
                        tbl_BusinessHours updateBh = context.tbl_BusinessHours.FirstOrDefault(p => !p.IsDeleted && p.ID == businessHoursBo.Id);

                        if (updateBh != null)
                        {
                            updateBh.RegionID = businessHoursBo.RegionId;
                            updateBh.FullDay = (decimal)businessHoursBo.FullDay;
                            updateBh.HalfDay = (decimal)businessHoursBo.HalfDay;
                            updateBh.BusinessStartHour = (TimeSpan)businessHoursBo.BusinessStartHour;
                            updateBh.BusinessEndHour = (TimeSpan)businessHoursBo.BusinessEndHour;
                            updateBh.IsBasedOnLastCheckout = businessHoursBo.IsBasedOnLastCheckout;
                            updateBh.IncludeLeaveDetailsInPaystub = businessHoursBo.IsIncludeLeaveDetailsInPaystub;
                            updateBh.ModifiedBy = Utility.UserId();
                            updateBh.ModifiedOn = DateTime.Now;

                            context.Entry(updateBh).State = EntityState.Modified;
                        }

                        context.SaveChanges();

                        return Notification.Updated;
                    }

                    tbl_BusinessHours insertBh = new tbl_BusinessHours
                    {
                        RegionID = businessHoursBo.RegionId,
                        FullDay = (decimal)businessHoursBo.FullDay,
                        HalfDay = (decimal)businessHoursBo.HalfDay,
                        BusinessStartHour = (TimeSpan)businessHoursBo.BusinessStartHour,
                        BusinessEndHour = (TimeSpan)businessHoursBo.BusinessEndHour,
                        IsBasedOnLastCheckout = businessHoursBo.IsBasedOnLastCheckout,
                        IncludeLeaveDetailsInPaystub = businessHoursBo.IsIncludeLeaveDetailsInPaystub,
                        DomainID = Utility.DomainId(),
                        HistoryID = Guid.NewGuid(),
                        CreatedBy = Utility.UserId(),
                        ModifiedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now
                    };

                    context.tbl_BusinessHours.Add(insertBh);
                    context.SaveChanges();

                    return Notification.Inserted;
                }

                return Notification.AlreadyExists;
            }
        }

        #endregion Business Hours
    }
}