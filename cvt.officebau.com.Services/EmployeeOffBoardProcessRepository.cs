﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Services
{
    public class EmployeeOffBoardProcessRepository : BaseRepository
    {
        #region Asset Report

        public List<CompanyAssetsBo> SearchAssetReturnDetails(CompanyAssetsBo companyAssetsBo)
        {
            List<CompanyAssetsBo> companyAssetsBoList = new List<CompanyAssetsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_ASSETRETURNEDDETAILREPORT);

                Database.AddInParameter(dbCommand, DBParam.Input.CompanyAssetID, DbType.String, companyAssetsBo.AssetTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, companyAssetsBo.BaseLocationId);
                Database.AddInParameter(dbCommand, DBParam.Input.ReturnStatus, DbType.Boolean, companyAssetsBo.ReturnAssetStatus);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, companyAssetsBo.EmployeeStatus);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, companyAssetsBo.EmployeeId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        companyAssetsBoList.Add(
                            new CompanyAssetsBo
                            {
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                                EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                Make = Utility.CheckDbNull<string>(reader[DBParam.Output.Make]),
                                Qty = Utility.CheckDbNull<int>(reader[DBParam.Output.QTY]),
                                SerialNumber = Utility.CheckDbNull<string>(reader[DBParam.Output.SerialNumber]),
                                HandoverOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.HandOverOn]),
                                ReturnedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ReturnedOn]),
                                ReturnedRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.ReturnedRemarks]),
                                ReturnStatus = Utility.CheckDbNull<bool>(reader[DBParam.Output.ReturnStatus]),
                                SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo]),
                                BusinessUnitName = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                IsActive = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsActive]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName])
                            });
                    }

                    return companyAssetsBoList;
                }
            }
        }

        #endregion Asset Report

        #region OffBoard Request

        public string GetOffboardRequestId(int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                if (context.tbl_OffBoardProcess.Count(o => o.EmployeeID == employeeId) != 0)
                {
                    return context.tbl_OffBoardProcess.FirstOrDefault(o => o.EmployeeID == employeeId)?.ID.ToString();
                }

                return null;
            }
        }

        public EmployeeOffBoardBo GetOffBoardRequest(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeOffBoardBo employeeOffBoardBo = (from l in context.tbl_OffBoardProcess
                                                         join s in context.tbl_Status on l.StatusID equals s.ID
                                                         join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                                         join m in context.tbl_EmployeeMaster on l.ModifiedBy equals m.ID
                                                         join a in context.tbl_EmployeeMaster on l.ApproverID equals a.ID
                                                         join bu in context.tbl_BusinessUnit on m.BaseLocationID equals bu.ID into business
                                                         from bUnit in business.DefaultIfEmpty()
                                                         where l.ID == id
                                                         select new EmployeeOffBoardBo
                                                         {
                                                             Id = l.ID,
                                                             EmployeeId = l.EmployeeID,
                                                             EmployeeName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                                             RelievingDate = l.RelievingDate,
                                                             InitiatedDate = l.InitiatedDate,
                                                             Reason = l.Reason,
                                                             TentativeRelievingDate = l.TentativeRelievingDate,
                                                             RequesterRemarks = l.Remarks,
                                                             ApproverId = l.ApproverID,
                                                             Approver = (e.EmpCodePattern ?? "") + (a.Code ?? "") + " - " + a.FullName,
                                                             StatusCode = s.Code,
                                                             ApproverRemarks = l.ApproverRemarks,
                                                             ApprovedDate = l.ApprovedOn,
                                                             ModifiedByName = e.FullName,
                                                             ModifiedByUser = a.FullName,
                                                             ModifiedOn = l.ModifiedOn,
                                                             BaseLocation = bUnit.Name,
                                                             WithdrawDate = context.tbl_OffBoardHistory.Where(h => h.EmployeeID == l.EmployeeID).Select(h => h.WithdrawDate).FirstOrDefault()
                                                         }).FirstOrDefault();

                if (employeeOffBoardBo != null)
                {
                    employeeOffBoardBo.WithdrawDate = employeeOffBoardBo.WithdrawDate == DateTime.MinValue ? null : employeeOffBoardBo.WithdrawDate;
                }
                return employeeOffBoardBo;
            }
        }

        public string ManageOffBoardRequest(EmployeeOffBoardBo employeeOffBoardBo)
        {
            string outputMessage;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_OFFBOARDREQUEST);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeOffBoardBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeOffBoardBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.RelievingDate, DbType.Date, employeeOffBoardBo.RelievingDate);
                Database.AddInParameter(dbCommand, DBParam.Input.TentativeRelievingDate, DbType.Date, employeeOffBoardBo.TentativeRelievingDate);
                Database.AddInParameter(dbCommand, DBParam.Input.InitiatedDate, DbType.Date, employeeOffBoardBo.InitiatedDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Reason, DbType.String, employeeOffBoardBo.Reason);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, employeeOffBoardBo.RequesterRemarks);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeOffBoardBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, employeeOffBoardBo.ApproverId);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, employeeOffBoardBo.IsDeleted);
                outputMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return outputMessage;
        }

        public string DeleteOffBoardRequest(EmployeeOffBoardBo employeeOffBoardBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                context.tbl_OffBoardHistory.Where(a => a.EmployeeID == employeeOffBoardBo.EmployeeId)
                       .ToList().ForEach(a =>
                        {
                            a.IsDeleted = true;
                            a.ModifiedOn = DateTime.Now;
                            a.ModifiedBy = employeeOffBoardBo.ModifiedBy;
                            context.Entry(a).State = EntityState.Modified;
                            context.SaveChanges();
                        });
                tbl_OffBoardProcess employee = context.tbl_OffBoardProcess.FirstOrDefault(b => b.ID == employeeOffBoardBo.Id && b.EmployeeID == employeeOffBoardBo.EmployeeId);

                if (employee != null)
                {
                    employee.ModifiedOn = DateTime.Now;
                    employee.ModifiedBy = Utility.UserId();

                    context.Entry(employee).State = EntityState.Deleted;
                }

                context.SaveChanges();

                return Notification.Deleted;
            }
        }

        public string WithdrawOffBoardRequest(EmployeeOffBoardBo employeeOffBoardBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                if (context.tbl_OffBoardProcess.Where(l => l.ID == employeeOffBoardBo.Id
                                                           && l.StatusID == context.tbl_Status.FirstOrDefault(s => !s.IsDeleted && s.Type == "OffBoard" && s.Code == "Initiated").ID).Count() != 0)
                {
                    tbl_OffBoardProcess employee = context.tbl_OffBoardProcess.FirstOrDefault(b => b.ID == employeeOffBoardBo.Id);

                    if (employee != null)
                    {
                        employee.IsDeleted = employeeOffBoardBo.IsDeleted;
                        employee.StatusID = GetStatusId("OffBoard", "Saved");
                        employee.ModifiedOn = DateTime.Now;
                        employee.ModifiedBy = Utility.UserId();
                    }

                    context.SaveChanges();

                    OffBoardHistory(employeeOffBoardBo);

                    return BOConstants.Withdraw;
                }

                return BOConstants.Cannot_Withdraw;
            }
        }

        private void OffBoardHistory(EmployeeOffBoardBo employeeOffBoardBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                tbl_OffBoardHistory offBoardHistory = new tbl_OffBoardHistory
                {
                    EmployeeID = employeeOffBoardBo.EmployeeId,
                    WithdrawDate = DateTime.Now,
                    RelievingDate = employeeOffBoardBo.RelievingDate,
                    Reason = employeeOffBoardBo.Reason,
                    RequestedDate = employeeOffBoardBo.InitiatedDate,
                    CreatedBy = Utility.UserId(),
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now,
                    ModifiedBy = Utility.UserId(),
                    DomainID = Utility.DomainId(),
                    HistoryID = Guid.NewGuid(),
                    Status = employeeOffBoardBo.Code
                };

                context.tbl_OffBoardHistory.Add(offBoardHistory);
                context.SaveChanges();
            }
        }

        public List<EmployeeOffBoardBo> GetOffBoardHistory(int employeeId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<EmployeeOffBoardBo> list = (from h in context.tbl_OffBoardHistory
                                                 join e in context.tbl_EmployeeMaster on h.EmployeeID equals e.ID
                                                 where !h.IsDeleted && h.DomainID == domainId && h.EmployeeID == employeeId
                                                 orderby h.RequestedDate descending
                                                 select new EmployeeOffBoardBo
                                                 {
                                                     Id = h.ID,
                                                     EmployeeId = h.EmployeeID,
                                                     WithdrawDate = h.WithdrawDate,
                                                     Code = e.Code,
                                                     EmployeeName = e.FullName,
                                                     RelievingDate = h.RelievingDate,
                                                     Reason = h.Reason,
                                                     InitiatedDate = h.RequestedDate,
                                                     StatusCode = h.Status ?? ""
                                                 }).ToList();
                return list;
            }
        }

        #endregion OffBoard Request

        #region OffBoard Approval

        public List<EmployeeOffBoardBo> SearchOffBoardApproval(EmployeeOffBoardBo employeeOffBoardBo)
        {
            List<EmployeeOffBoardBo> employeeOffBoardBoList = new List<EmployeeOffBoardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEEOFFBOARDAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeOffBoardBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeOffBoardBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeOffBoardBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeOffBoardBoList.Add(
                            new EmployeeOffBoardBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                                BusinessUnitName = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                RelievingDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.RelievingDate]),
                                InitiatedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.InitiatedDate]),
                                EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID])
                            });
                    }

                    return employeeOffBoardBoList;
                }
            }
        }

        public int GetStatusId(string type, string codeName)
        {
            using (FSMEntities context = new FSMEntities())
            {
                return context.tbl_Status.Where(s => !s.IsDeleted && s.Type == type && s.Code == codeName).Select(s => s.ID).FirstOrDefault();
            }
        }

        public string ApproveOffBoardRequest(int id, string status, string remarks, int approverId, int domainId, int? hrApproverId, DateTime? tentativeRelievingDate, int employeeId, DateTime? initiatedDate, DateTime? relievingDate)
        {
            string outputMessage;

            using (FSMEntities context = new FSMEntities())
            {
                if (context.tbl_OffBoardProcess.Where(l => l.ID == id
                                                           && l.StatusID == context.tbl_Status.Where(s => !s.IsDeleted && s.Type == "OffBoard" && s.Code == "Initiated").Select(s => s.ID).FirstOrDefault()).Count() != 0)
                {
                    int statusId = GetStatusId("OffBoard", status);

                    tbl_OffBoardProcess approverAction = context.tbl_OffBoardProcess.FirstOrDefault(l => l.ID == id);
                    if (approverAction != null)
                    {
                        approverAction.ApproverRemarks = remarks;
                        approverAction.TentativeRelievingDate = tentativeRelievingDate;
                        approverAction.StatusID = statusId;
                        approverAction.HRApproverID = hrApproverId;
                        approverAction.ApproverStatusID = status.ToUpper().Contains("APPROVE")
                            ? (int?)GetStatusId("OffBoard", "Pending")
                            : null;
                        approverAction.AssetStatusID = GetStatusId("OffBoard", "Pending");
                        approverAction.ApprovedOn = DateTime.Now;
                        approverAction.ModifiedBy = approverId;
                        approverAction.ModifiedOn = DateTime.Now;
                        approverAction.DomainID = Utility.DomainId();

                        context.Entry(approverAction).State = EntityState.Modified;
                        context.SaveChanges();

                        if (status.ToUpper() == "REJECTED")
                        {
                            EmployeeOffBoardBo employeeOffBoardBo = new EmployeeOffBoardBo
                            {
                                EmployeeId = employeeId,
                                RelievingDate = relievingDate,
                                Reason = remarks,
                                InitiatedDate = initiatedDate,
                                CreatedBy = Utility.UserId(),
                                ModifiedBy = Utility.UserId(),
                                DomainId = Utility.DomainId()
                            };

                            approverAction.ModifiedOn = DateTime.Now;
                            employeeOffBoardBo.Code = "Rejected";

                            OffBoardHistory(employeeOffBoardBo);
                        }
                    }

                    outputMessage = status + " Successfully";
                }
                else
                {
                    outputMessage = BOConstants.cannot_Approve_Reject;
                }
            }

            return outputMessage;
        }

        public DataTable SearchEmployeeOffBoardApprovalListForExport(EmployeeOffBoardBo employeeOffBoardBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORT_EMPLOYEEOFFBOARADAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeOffBoardBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeOffBoardBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeOffBoardBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable();
                dataSet.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion OffBoard Approval

        #region HR Approval

        public List<EmployeeOffBoardBo> SearchOffBoardHrApproval(EmployeeOffBoardBo employeeOffBoardBo)
        {
            List<EmployeeOffBoardBo> employeeOffBoardBoList = new List<EmployeeOffBoardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEEOFFBOARDHRAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeOffBoardBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeOffBoardBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeOffBoardBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.HRApproverID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeOffBoardBoList.Add(
                            new EmployeeOffBoardBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                                EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                                BusinessUnitName = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                ApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                                ApproverName = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovarName])
                            });
                    }

                    return employeeOffBoardBoList;
                }
            }
        }

        public EmployeeOffBoardBo GetOffBoardHrApprove(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeOffBoardBo employeeOffBoardBo = (from l in context.tbl_OffBoardProcess
                                                         join s in context.tbl_Status on l.StatusID equals s.ID into status
                                                         from st in status.DefaultIfEmpty()
                                                         join hr in context.tbl_Status on l.HRStatusID equals hr.ID into hrs
                                                         from hrStatus in hrs.DefaultIfEmpty()
                                                         join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                                         join m in context.tbl_EmployeeMaster on l.ModifiedBy equals m.ID
                                                         join a in context.tbl_EmployeeMaster on l.ApproverID equals a.ID
                                                         join h in context.tbl_EmployeeMaster on l.HRApproverID equals h.ID
                                                         join bu in context.tbl_BusinessUnit on m.BaseLocationID equals bu.ID into business
                                                         from bUnit in business.DefaultIfEmpty()
                                                         where l.ID == id
                                                         select new EmployeeOffBoardBo
                                                         {
                                                             Id = l.ID,
                                                             EmployeeId = l.EmployeeID,
                                                             EmployeeName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                                             RelievingDate = l.RelievingDate,
                                                             InitiatedDate = l.InitiatedDate,
                                                             Reason = l.Reason,
                                                             TentativeRelievingDate = l.TentativeRelievingDate,
                                                             RequesterRemarks = l.Remarks,
                                                             ApproverId = l.ApproverID,
                                                             Approver = (e.EmpCodePattern ?? "") + (a.Code ?? "") + " - " + a.FullName,
                                                             StatusCode = st.Code,
                                                             HrStatus = hrStatus.Code,
                                                             ApproverRemarks = l.ApproverRemarks,
                                                             ApprovedDate = l.ApprovedOn,
                                                             ModifiedByName = h.FullName,
                                                             ModifiedOn = l.ModifiedOn,
                                                             BaseLocation = bUnit.Name
                                                         }).FirstOrDefault();

                return employeeOffBoardBo;
            }
        }

        public List<CompanyAssetsBo> SearchCompanyAssetDetail(EmployeeOffBoardBo employeeOffBoardBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<CompanyAssetsBo> list = (from e in context.tbl_CompanyAssets
                                              join em in context.tbl_EmployeeMaster on e.ModifiedBy equals em.ID
                                              join a in context.tbl_AssetType on e.AssetTypeID equals a.ID
                                              join bu in context.tbl_BusinessUnit on em.BaseLocationID equals bu.ID into business
                                              from bUnit in business.DefaultIfEmpty()
                                              from off in context.tbl_OffBoardProcess
                                                .Where(off => !(off.IsDeleted) && off.EmployeeID == e.EmployeeID && off.ID == employeeOffBoardBo.Id).DefaultIfEmpty()
                                              join hr in context.tbl_Status on off.AssetStatusID equals hr.ID into hrs
                                              from hrStatus in hrs.DefaultIfEmpty()
                                              where !e.IsDeleted && e.DomainID == domainId && e.EmployeeID == employeeOffBoardBo.EmployeeId
                                              orderby a.Name
                                              select new CompanyAssetsBo
                                              {
                                                  CompanyAssetId = e.ID,
                                                  Make = e.Make,
                                                  AssetTypeId = e.AssetTypeID,
                                                  SerialNumber = e.SerialNo,
                                                  Qty = e.QTY,
                                                  HandoverOn = e.HandoverOn,
                                                  EmployeeId = e.EmployeeID,
                                                  AssetsName = new  cvt.officebau.com.ViewModels.Entity
                                                  {
                                                      Name = a.Name
                                                  },
                                                  ModifiedOn = e.ModifiedOn,
                                                  ModifiedByName = em.FullName,
                                                  Remarks = e.Remarks,
                                                  ReturnedDate = e.ReturnedOn,
                                                  ReturnedRemarks = e.ReturnedRemarks,
                                                  BaseLocation = bUnit.Name,
                                                  AssetStatus = hrStatus.Code,
                                                  OffboardProcessID = employeeOffBoardBo.Id
                                              }).ToList();
                return list;
            }
        }

        public string UpdateReturnedDetails(CompanyAssetsBo companyAssetsBo)
        {
            string outputMessage;
            using (FSMEntities context = new FSMEntities())
            {
                {
                    tbl_CompanyAssets approverAction = context.tbl_CompanyAssets.FirstOrDefault(l => l.ID == companyAssetsBo.Id && !l.IsDeleted);
                    if (approverAction != null)
                    {
                        approverAction.ReturnedOn = companyAssetsBo.ReturnedDate;
                        approverAction.ReturnedRemarks = companyAssetsBo.ReturnedRemarks;
                        approverAction.ReturnStatus = true;
                        approverAction.IsDeleted = approverAction.IsDeleted;
                        approverAction.ModifiedOn = DateTime.Now;
                        approverAction.ModifiedBy = Utility.UserId();

                        context.Entry(approverAction).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    outputMessage = Notification.Updated;

                    if (context.tbl_CompanyAssets.Count(a => !a.ReturnedOn.HasValue && a.EmployeeID == companyAssetsBo.EmployeeId && !a.IsDeleted) == 0)
                    {
                        tbl_OffBoardProcess assetStatus = context.tbl_OffBoardProcess.FirstOrDefault(l => l.EmployeeID == companyAssetsBo.EmployeeId && l.ID == companyAssetsBo.OffboardProcessID);
                        if (assetStatus != null)
                        {
                            assetStatus.AssetStatusID = GetStatusId("OffBoard", "Cleared");
                            assetStatus.ModifiedOn = DateTime.Now;
                            assetStatus.ModifiedBy = Utility.UserId();

                            context.Entry(assetStatus).State = EntityState.Modified;
                        }

                        context.SaveChanges();
                    }
                }
            }

            return outputMessage;
        }

        public CompanyAssetsBo GetReturnDetail(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                CompanyAssetsBo companyAssetsBo = (from l in context.tbl_CompanyAssets
                                                   join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                                   join m in context.tbl_EmployeeMaster on l.ModifiedBy equals m.ID
                                                   where !l.IsDeleted && l.ID == id
                                                   select new CompanyAssetsBo
                                                   {
                                                       Id = l.ID,
                                                       EmployeeId = l.EmployeeID,
                                                       ReturnedDate = l.ReturnedOn,
                                                       ReturnedRemarks = l.ReturnedRemarks,
                                                       Remarks = l.Remarks,
                                                       HandoverOn = l.HandoverOn,
                                                   }).FirstOrDefault();

                return companyAssetsBo;
            }
        }

        public string UpdateExitInterviewDetails(EmployeeOffBoardBo employeeOffBoardBo)
        {
            string outputMessage = string.Empty;

            using (FSMEntities context = new FSMEntities())
            {
                int assetCount = context.tbl_CompanyAssets.Count(a => !a.IsDeleted && a.EmployeeID == employeeOffBoardBo.EmployeeId);
                int assetReturned = context.tbl_CompanyAssets.Count(a => !a.IsDeleted && a.EmployeeID == employeeOffBoardBo.EmployeeId && !a.ReturnedOn.HasValue);

                if (assetCount == 0 || assetReturned == 0)
                {
                    if (context.tbl_OffBoardProcess.Where(l => l.ID == employeeOffBoardBo.Id
                                                               && l.ApproverStatusID == context.tbl_Status.Where(s => !s.IsDeleted && s.Type == "OffBoard" && s.Code == "Pending").Select(s => s.ID).FirstOrDefault()).Count() != 0)
                    {
                        int statusId = GetStatusId("OffBoard", "Approved");
                        tbl_OffBoardProcess approverAction = context.tbl_OffBoardProcess.FirstOrDefault(l => l.ID == employeeOffBoardBo.Id);
                        if (approverAction != null)
                        {
                            approverAction.ActualRelievedDate = employeeOffBoardBo.ActualRelievedDate;
                            approverAction.RelievingComments = employeeOffBoardBo.RelievingComments;
                            approverAction.RelivingReason = employeeOffBoardBo.RelivingReasonId;
                            approverAction.HRApprovedOn = DateTime.Now;
                            approverAction.StatusID = statusId;
                            approverAction.ApproverStatusID = statusId;
                            approverAction.HRStatusID = statusId;
                            approverAction.IsDeleted = employeeOffBoardBo.IsDeleted;
                            approverAction.ModifiedOn = DateTime.Now;
                            approverAction.ModifiedBy = Utility.UserId();

                            context.Entry(approverAction).State = EntityState.Modified;
                        }

                        context.SaveChanges();

                        tbl_EmployeeMaster employee = context.tbl_EmployeeMaster.FirstOrDefault(l => l.ID == employeeOffBoardBo.EmployeeId);

                        if (employee != null)
                        {
                            employee.InactiveFrom = employeeOffBoardBo.ActualRelievedDate;
                            employee.ReasonID = employeeOffBoardBo.RelivingReasonId;
                            employee.IsDeleted = employeeOffBoardBo.IsDeleted;
                            employee.ModifiedOn = DateTime.Now;
                            employee.ModifiedBy = Utility.UserId();

                            context.Entry(employee).State = EntityState.Modified;
                        }

                        context.SaveChanges();

                        outputMessage = Notification.Updated;
                    }
                }
                else
                {
                    outputMessage = BOConstants.Return_Company_Assets;
                }
            }

            return outputMessage;
        }

        public EmployeeOffBoardBo GetExitInterviewDetail(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeOffBoardBo employeeOffBoardBo = (from l in context.tbl_OffBoardProcess
                                                         join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                                         where l.ID == id
                                                         select new EmployeeOffBoardBo
                                                         {
                                                             Id = l.ID,
                                                             EmployeeId = l.EmployeeID,
                                                             ActualRelievedDate = l.ActualRelievedDate,
                                                             RelievingComments = l.RelievingComments,
                                                             RelivingReasonId = l.RelivingReason,
                                                             Doj = e.DOJ
                                                         }).FirstOrDefault();

                return employeeOffBoardBo;
            }
        }

        public DataTable SearchEmployeeOffBoardHrApprovalListForExport(EmployeeOffBoardBo employeeOffBoardBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORT_EMPLOYEEOFFBOARADHRAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeOffBoardBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeOffBoardBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeOffBoardBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.HRApproverID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dataTable = new DataTable();
                ds.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion HR Approval

        #region AssetClearance

        public List<EmployeeOffBoardBo> SearchEmployeeAssetsList(EmployeeOffBoardBo employeeOffBoardBo)
        {
            List<EmployeeOffBoardBo> employeeOffBoardBoList = new List<EmployeeOffBoardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHEMPLOYEEASSETSLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeOffBoardBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeOffBoardBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeOffBoardBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.HRApproverID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeOffBoardBoList.Add(
                            new EmployeeOffBoardBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                                EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                                BusinessUnitName = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                ApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                                ApproverName = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovarName]),
                                AssetStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.ReturnStatus])
                            });
                    }

                    return employeeOffBoardBoList;
                }
            }
        }

        public DataTable SearchAssetListExcelExport(EmployeeOffBoardBo employeeOffBoardBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORTEMPLOYEEASSETLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeOffBoardBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeOffBoardBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeOffBoardBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.HRApproverID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dataTable = new DataTable();
                ds.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion AssetClearance

        #region OffBoard Request HR

        public List<EmployeeOffBoardBo> GetOffboardRequestHRList(EmployeeOffBoardBo employeeOffBoardBO)
        {
            List<EmployeeOffBoardBo> employeeOffBoardBOList = new List<EmployeeOffBoardBo>();

            using (DbConnection dbCon = this.Database.CreateConnection())
            {
                DbCommand dbcmd = this.Database.GetStoredProcCommand(Constants.SEARCH_OFFBOARDREQUESTHRLIST);

                this.Database.AddInParameter(dbcmd, DBParam.Input.EmployeeCode, DbType.String, employeeOffBoardBO.Code);
                this.Database.AddInParameter(dbcmd, DBParam.Input.Name, DbType.String, employeeOffBoardBO.FirstName);
                this.Database.AddInParameter(dbcmd, DBParam.Input.StatusID, DbType.Int32, employeeOffBoardBO.StatusId);
                this.Database.AddInParameter(dbcmd, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                this.Database.AddInParameter(dbcmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = this.Database.ExecuteReader(dbcmd))
                {
                    while (reader.Read())
                    {
                        employeeOffBoardBOList.Add(
                            new EmployeeOffBoardBo
                            {
                                Id = Utility.CheckDbNull<Int32>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                FirstName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                                BusinessUnitName = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                RelievingDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.RelievingDate]),
                                InitiatedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.InitiatedDate]),
                                EmployeeId = Utility.CheckDbNull<Int32>(reader[DBParam.Output.EmployeeID]),
                                StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status])
                            });
                    }
                    return employeeOffBoardBOList;
                }
            }
        }

        #endregion OffBoard Request HR

    }
}