﻿using cvt.officebau.com.Utilities;
using System.Data;

namespace cvt.officebau.com.Services
{
    public class GetApplicationConfiguration : BaseRepository
    {
        public string GetApplicationConfigValue(string code, string businessUnit, int userId)
        {
            var userMessage = string.Empty;

            using (Database.CreateConnection())
            {
                var dbCommand = Database.GetStoredProcCommand(Constants.GET_APPLICATIONCONFIGVALUE);

                Database.AddInParameter(dbCommand, DBParam.Input.Code, DbType.String, code);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, businessUnit);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, userId);

                using (var reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.ConfigValue]);
                    }
                }
            }

            return userMessage;
        }
    }
}