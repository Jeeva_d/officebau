﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class CarryForwardLeaveRepository : BaseRepository
    {
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;

        #region Carry Forward Leave

        public DataSet GetCarryforwardSummary(CarryForwardLeaveBO carryForwardLeaveBo)
        {
            DataSet ds = new DataSet();
            DataTable dtCarryforward = new DataTable("CarryforwardLeave");
            ds.Tables.Add(dtCarryforward);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.GET_CARRYFORWARDLEAVESUMMARY, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtCarryforward);
                    }
                }
            }
            return ds;
        }

        public List<CarryForwardLeaveBO> GetCarryForwardDetails(int employeeID, string leaveType)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainID = Utility.DomainId();
                int leaveTypeID = Convert.ToInt32(context.tbl_LeaveTypes.Where(s => s.Name == leaveType && s.DomainID == domainID
                                                                                                         && !(s.IsDeleted)).Select(s => s.ID).FirstOrDefault());
                List<CarryForwardLeaveBO> CarryForwardLeaveList = (from t in context.tbl_CarryForwardLeave
                                                                   where !t.IsDeleted && t.DomainID == domainID && t.EmployeeID == employeeID && t.LeaveTypeID == leaveTypeID
                                                                   orderby t.ModifiedOn descending
                                                                   select new CarryForwardLeaveBO
                                                                   {
                                                                       TotalLeave = t.TotalLeave,
                                                                       Year = t.Year
                                                                   }).ToList();
                return CarryForwardLeaveList;
            }
        }

        public List<CarryForwardLeaveBO> SearchCarryForwardDetails(CarryForwardLeaveBO carryForwardLeaveBO)
        {
            List<CarryForwardLeaveBO> carryForwardLeaveBoList = new List<CarryForwardLeaveBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_CARRYFORWARDLEAVEDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, carryForwardLeaveBO.Year);
                Database.AddInParameter(dbCommand, DBParam.Input.LeaveTypeID, DbType.Int32, carryForwardLeaveBO.LeaveTypeID);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        carryForwardLeaveBoList.Add(new CarryForwardLeaveBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            FirstName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            LastName = Utility.CheckDbNull<string>(reader[DBParam.Output.LastName]),
                            TotalLeave = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalLeave]),
                            LeaveTypeID = Utility.CheckDbNull<int>(reader[DBParam.Output.LeaveTypeID]),
                            LeaveType = Utility.CheckDbNull<string>(reader[DBParam.Output.LeaveType]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            Year = Utility.CheckDbNull<int>(reader[DBParam.Output.Year])
                        });
                    }
                }
            }
            return carryForwardLeaveBoList;
        }

        public CarryForwardLeaveBO ManageCarryForwardLeave(List<CarryForwardLeaveBO> list)
        {
            CarryForwardLeaveBO carryForwardLeaveBo = new CarryForwardLeaveBO();
            using (DbConnection dbCon = this.Database.CreateConnection())
            {
                foreach (var item in list)
                {
                    if (item.TotalLeave != 0)
                    {
                        DbCommand dbCmd = this.Database.GetStoredProcCommand(Constants.MANAGE_CARRYFORWARDLEAVE);
                        this.Database.AddInParameter(dbCmd, DBParam.Input.ID, DbType.Int32, item.Id);
                        this.Database.AddInParameter(dbCmd, DBParam.Input.EmployeeID, DbType.Int32, item.EmployeeId);
                        this.Database.AddInParameter(dbCmd, DBParam.Input.LeaveTypeID, DbType.Int32, item.LeaveTypeID);
                        this.Database.AddInParameter(dbCmd, DBParam.Input.TotalLeave, DbType.Decimal, item.TotalLeave);
                        this.Database.AddInParameter(dbCmd, DBParam.Input.YearID, DbType.Int32, item.Year);
                        this.Database.AddInParameter(dbCmd, DBParam.Input.IsCarryForward, DbType.Boolean, 0);
                        this.Database.AddInParameter(dbCmd, DBParam.Input.IsEncashment, DbType.Boolean, 0);
                        this.Database.AddInParameter(dbCmd, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                        this.Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                        carryForwardLeaveBo.UserMessage = this.Database.ExecuteScalar(dbCmd).ToString();
                    }
                }
            }
            return carryForwardLeaveBo;
        }

        #endregion Carry Forward Leave
    }
}