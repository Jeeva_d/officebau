﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace cvt.officebau.com.Services
{
    public class ExpenseRepository : BaseRepository
    {
        #region DuplicateCheck

        public ExpenseBo DuplicateCheck(string name, string table)
        {
            ExpenseBo expenseBo = new ExpenseBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DUPLICATENAMECHECK2);

                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, name);
                Database.AddInParameter(dbCommand, DBParam.Input.Table, DbType.String, table);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBo = new ExpenseBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader["Output"])
                        };
                    }
                }

                return expenseBo;
            }
        }

        #endregion DuplicateCheck

        #region Email

        public string PopulateMailBodyForUpdated(string vendor, decimal amount, string account, DateTime? date, string desc, string payMode, string ledger, DateTime? createdDate, string mod)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/UpdateExpensePayment.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{PartyName}", vendor);
            body = body.Replace("{Ledger}", ledger);
            body = body.Replace("{Amount}", "INR" + " " + Utility.PrecisionForDecimal(amount, 2));
            body = body.Replace("{desc}", desc);
            body = body.Replace("{Date}", Convert.ToDateTime(date).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{account}", account);
            body = body.Replace("{Mode}", payMode);
            body = body.Replace("{Createdby}", mod + " - On " + Convert.ToDateTime(createdDate).ToString("dd-MMMM-yyyy h:mm tt") + " IST");
            return body;
        }

        #endregion Email

        public List<ExpenseHoldingBO> SearchVendorPoDetails(int id, string screenName)
        {
            List<ExpenseHoldingBO> invoiceReceivableBoList = new List<ExpenseHoldingBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchVendorPODetails");
                Database.AddInParameter(dbCommand, DBParam.Input.VendorID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        invoiceReceivableBoList.Add(new ExpenseHoldingBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            CostCenterId = Utility.CheckDbNull<int>(reader[DBParam.Output.CostCenterID]),
                            ScreenName = screenName
                        });
                    }
                }
            }

            return invoiceReceivableBoList;
        }

        #region Search Bill and Expense

        public List<ExpenseBo> SearchExpenseTransactionDetails(SearchParamsBo searchParamsBo)
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_TRANSACTION);
                Database.AddInParameter(dbCommand, DBParam.Input.VendorName, DbType.String, searchParamsBo.VendorName);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, searchParamsBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.ScreenType, DbType.String, searchParamsBo.ScreenType);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount);
                Database.AddInParameter(dbCommand, DBParam.Input.Tag, DbType.String, searchParamsBo.Tag);
                Database.AddInParameter(dbCommand, DBParam.Input.CostCenterID, DbType.Decimal, searchParamsBo.CostCenterId);
                Database.AddInParameter(dbCommand, DBParam.Input.LedgerName, DbType.String, searchParamsBo.LedgerName);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBoList.Add(
                            new ExpenseBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                ScreenType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                                BillDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.BillDate]),
                                DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                                CostCenter = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CostCenterID]),
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.CostCenterName])
                                },
                                VendorId = Utility.CheckDbNull<int>(reader[DBParam.Output.VendorID]),
                                VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName]),
                                Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                Tag = Utility.CheckDbNull<string>(reader[DBParam.Output.Tag]),
                                FileId = Utility.CheckDbNull<string>(reader[DBParam.Output.HistoryID]),
                                UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.LEDGER]),
                            });
                    }
                }
            }

            return expenseBoList;
        }

        public List<ExpenseBo> SearchExpenseDuplicatedRecords(SearchParamsBo searchParamsBo)
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EXPENSECHECKDUPLICATION);
                Database.AddInParameter(dbCommand, DBParam.Input.ColumnName, DbType.String, searchParamsBo.ColumnName);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBoList.Add(
                            new ExpenseBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                ScreenType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                                BillDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.BillDate]),
                                DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                                BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                                CostCenter = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CostCenterID]),
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.CostCenterName])
                                },
                                VendorId = Utility.CheckDbNull<int>(reader[DBParam.Output.VendorID]),
                                VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName]),
                                Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                Tag = Utility.CheckDbNull<string>(reader[DBParam.Output.Tag]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.LEDGER])
                            });
                    }
                }
            }

            return expenseBoList;
        }

        #endregion Search Bill and Expense

        #region Get Expense and Bill Transactions

        public ExpenseBo GetExpenseTransactionDetails(int id)
        {
            ExpenseBo expenseBo = new ExpenseBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_TRANSACTION);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBo = new ExpenseBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            BillDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.BillDate]),
                            DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                            PaymentDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PaymentDate]),
                            VendorId = Utility.CheckDbNull<int>(reader[DBParam.Output.VendorID]),
                            Tag = Utility.CheckDbNull<string>(reader[DBParam.Output.Tag].ToString()),
                            VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName].ToString()),
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.BillingAddress].ToString()),
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms].ToString()),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description].ToString()),
                            BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo].ToString()),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                            PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaidAmount]),
                            ScreenType = Utility.CheckDbNull<string>(reader[DBParam.Output.ScreenType].ToString()),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            PaymentMode = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.PaymentModeID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentMode])
                            },
                            Bank = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.BankID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName])
                            },
                            CostCenter = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CostCenterID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.CostCenterName])
                            },
                            Reference = Utility.CheckDbNull<string>(reader[DBParam.Output.Reference]),
                            FileUpload = Utility.CheckDbNull<string>(reader[DBParam.Output.FileName]),
                            CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.CreatedOn]),

                            FileId = Utility.CheckDbNull<string>(reader[DBParam.Output.HistoryID]),
                            PoNos = Utility.CheckDbNull<string>(reader["PONos"])
                        };
                    }
                }
            }

            expenseBo.ExpenseDetail = SearchExpenseDetailsList(id);
            expenseBo.ExpenseHoldingBOs = SearchExpenseHoldingList(id);
            if (id != 0)
            {
                MasterDataRepository masterDataRepository = new MasterDataRepository();
                ExpenseDetailsBO expenseDetailsBo = new ExpenseDetailsBO
                {
                    LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger")
                };
                expenseBo.Emailhtml = PopulateMailBodyForUpdated(expenseBo.VendorName, expenseBo.TotalAmount, expenseBo.Bank.Name, expenseBo.PaymentDate,
                    expenseBo.ExpenseDetail.Find(a => a.ExpenseDetailRemarks != "").ExpenseDetailRemarks,
                    expenseBo.PaymentMode.Name,
                    expenseDetailsBo.LedgerList.Find(a => a.Value == expenseBo.ExpenseDetail.Find(m => m.LedgerID != 0).LedgerID.ToString()).Text
                  , expenseBo.ModifiedOn, expenseBo.ModifiedByName);
            }

            return expenseBo;
        }

        private List<ExpenseDetailsBO> SearchExpenseDetailsList(int id)
        {
            List<ExpenseDetailsBO> expenseDetailsBoList = new List<ExpenseDetailsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EXPENSEDETAILSLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.ExpenseID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseDetailsBoList.Add(new ExpenseDetailsBO
                        {
                            LedgerID = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID]),
                            ExpenseDetailRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            ID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            POIDItemID = Utility.CheckDbNull<int>(reader["Poid"]),
                            Qty = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            IsProduct = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsProduct]),
                            SGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTPERCENT]),
                            CGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGSTPERCENT]),
                            SGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            CGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            TotalAmount = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) * Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]) + Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]) + Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger")
                        });
                        expenseDetailsBoList[expenseDetailsBoList.Count - 1].LedgerList.Find(a => a.Value == expenseDetailsBoList[expenseDetailsBoList.Count - 1].LedgerID.ToString()).Selected = true;
                    }
                }

                if (expenseDetailsBoList.Count == 0)
                {
                    expenseDetailsBoList.Add(new ExpenseDetailsBO
                    {
                        LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger")
                    });
                }
            }

            return expenseDetailsBoList;
        }

        #endregion Get Expense and Bill Transactions

        #region BILL Management

        public string ManageBill(ExpenseBo expenseBo)
        {
            string userMessage = string.Empty;
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);

            switch (expenseBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    userMessage = CreateBill(expenseBo, con);
                    break;

                case Operations.Update:
                    userMessage = UpdateBill(expenseBo, con);
                    break;

                case Operations.Delete:
                    userMessage = DeleteBill(expenseBo, con);
                    break;
            }

            return userMessage;
        }

        private string DeleteBill(ExpenseBo expenseBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.DELETE_BILL)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, expenseBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string UpdateBill(ExpenseBo expenseBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.UPDATE_BILL)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            FillBillParameters(expenseBo, cmd);
            cmd.Parameters.AddWithValue(DBParam.Input.ID, expenseBo.Id);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string CreateBill(ExpenseBo expenseBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.CREATE_BILL)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            FillBillParameters(expenseBo, cmd);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private void FillBillParameters(ExpenseBo expenseBo, SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(expenseBo.Description));
            cmd.Parameters.AddWithValue(DBParam.Input.Date, expenseBo.BillDate);
            cmd.Parameters.AddWithValue(DBParam.Input.TotalAmount, expenseBo.TotalAmount);
            cmd.Parameters.AddWithValue(DBParam.Input.DueDate, expenseBo.DueDate);
            cmd.Parameters.AddWithValue(DBParam.Input.BillNo, ValueOrDBNullIfZero(expenseBo.BillNo));
            cmd.Parameters.AddWithValue(DBParam.Input.Type, expenseBo.ScreenType);
            cmd.Parameters.AddWithValue(DBParam.Input.Reference, ValueOrDBNullIfZero(expenseBo.Reference));
            cmd.Parameters.AddWithValue(DBParam.Input.ExpenseDetail, Utility.ToDataTable(expenseBo.ExpenseDetail, "ExpenseDetail"));
            //cmd.Parameters.AddWithValue(DBParam.Input.ExpenseItem, Utility.ToDataTable(expenseBO.ExpenseItems, "ExpenseItems"));
            cmd.Parameters.AddWithValue(DBParam.Input.UploadFile, ValueOrDBNullIfZero(expenseBo.FileUpload));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.BankID, 0);
            cmd.Parameters.AddWithValue(DBParam.Input.FileID, ValueOrDBNullIfZero(expenseBo.FileId));

            cmd.Parameters.AddWithValue(DBParam.Input.VendorID, expenseBo.VendorId != 0 ? expenseBo.VendorId : 0);

            if (expenseBo.IsCostCenter != 0)
            {
                cmd.Parameters.AddWithValue(DBParam.Input.CostCenterID, expenseBo.CostCenter == null ? 0 : expenseBo.CostCenter.Id);
            }
            else
            {
                cmd.Parameters.AddWithValue(DBParam.Input.CostCenterID, 0);
            }

            if (expenseBo.PaymentMode != null)
            {
                cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, expenseBo.PaymentMode == null ? 0 : expenseBo.PaymentMode.Id);
            }
            else
            {
                cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, 0);
            }
        }

        #endregion BILL Management

        #region Expense Management

        public string ManageExpense(ExpenseBo expenseBo)
        {
            string userMessage = string.Empty;
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);

            switch (expenseBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    userMessage = CreateExpense(expenseBo, con);
                    break;

                case Operations.Update:
                    userMessage = UpdateExpense(expenseBo, con);
                    break;

                case Operations.Delete:
                    userMessage = DeleteExpense(expenseBo, con);
                    break;
            }

            return userMessage;
        }

        private string CreateExpense(ExpenseBo expenseBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.CREATE_EXPENSE)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            con.Open();
            FillExpenseParameters(expenseBo, cmd);
            var msg = cmd.ExecuteScalar().ToString();
            string bank = "Cash";
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            if (expenseBo.Bank.Id != null)
            {
                expenseBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
                bank = expenseBo.BankList.Find(a => a.Value == expenseBo.Bank.Id.ToString()).Text;
            }

            CommonRepository commonRepository = new CommonRepository();
            ExpenseDetailsBO expenseDetailsBo = new ExpenseDetailsBO();

            expenseBo.PaymentModeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
            expenseDetailsBo.LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger");
            PopulateMailBody(expenseBo.VendorName, expenseBo.TotalAmount, bank,
                expenseBo.PaymentDate, expenseBo.ExpenseDetail.Find(a => a.ExpenseDetailRemarks != "").ExpenseDetailRemarks,
                expenseBo.PaymentModeList.Find(a => a.Value == expenseBo.PaymentMode.Id.ToString()).Text
              , expenseDetailsBo.LedgerList.Find(a => a.Value == expenseBo.ExpenseDetail.Find(m => m.LedgerID != 0).LedgerID.ToString()).Text
              , expenseBo.FileId);
            return msg;
        }

        private string UpdateExpense(ExpenseBo expenseBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.UPDATE_EXPENSE)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            FillExpenseParameters(expenseBo, cmd);
            cmd.Parameters.AddWithValue(DBParam.Input.ID, expenseBo.Id);
            con.Open();
            string bank = "Cash";
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            if (expenseBo.Bank.Id != null)
            {
                expenseBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
                bank = expenseBo.BankList.Find(a => a.Value == expenseBo.Bank.Id.ToString()).Text;
            }

            CommonRepository commonRepository = new CommonRepository();
            ExpenseDetailsBO expenseDetailsBo = new ExpenseDetailsBO();

            expenseBo.PaymentModeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
            expenseDetailsBo.LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger");
            PopulateMailBodyForExpenseUpdated(expenseBo.VendorName, expenseBo.TotalAmount, bank,
                expenseBo.PaymentDate, expenseBo.ExpenseDetail.Find(a => a.ExpenseDetailRemarks != "").ExpenseDetailRemarks,
                expenseBo.PaymentModeList.Find(a => a.Value == expenseBo.PaymentMode.Id.ToString()).Text
              , expenseDetailsBo.LedgerList.Find(a => a.Value == expenseBo.ExpenseDetail.Find(m => m.LedgerID != 0).LedgerID.ToString()).Text
              , expenseBo.Emailhtml, expenseBo.FileId);
            return cmd.ExecuteScalar().ToString();
        }

        private void FillExpenseParameters(ExpenseBo expenseBo, SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(expenseBo.Remarks));
            cmd.Parameters.AddWithValue(DBParam.Input.Date, expenseBo.PaymentDate);
            cmd.Parameters.AddWithValue(DBParam.Input.TotalAmount, expenseBo.TotalAmount);
            cmd.Parameters.AddWithValue(DBParam.Input.DueDate, expenseBo.DueDate);
            cmd.Parameters.AddWithValue(DBParam.Input.BillNo, ValueOrDBNullIfZero(expenseBo.BillNo));
            cmd.Parameters.AddWithValue(DBParam.Input.Type, expenseBo.ScreenType);
            cmd.Parameters.AddWithValue(DBParam.Input.Tag, expenseBo.Tag);
            cmd.Parameters.AddWithValue(DBParam.Input.Reference, ValueOrDBNullIfZero(expenseBo.Reference));
            cmd.Parameters.AddWithValue(DBParam.Input.ExpenseDetail, Utility.ToDataTable(expenseBo.ExpenseDetail, "ExpenseDetail"));
            //cmd.Parameters.AddWithValue(DBParam.Input.ExpenseItem, Utility.ToDataTable(expenseBO.ExpenseItems, "ExpenseItems"));
            cmd.Parameters.AddWithValue(DBParam.Input.UploadFile, ValueOrDBNullIfZero(expenseBo.FileUpload));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.FileID, ValueOrDBNullIfZero(expenseBo.FileId));

            cmd.Parameters.AddWithValue(DBParam.Input.VendorID, expenseBo.VendorId != 0 ? expenseBo.VendorId : 0);

            if (expenseBo.IsCostCenter != 0)
            {
                cmd.Parameters.AddWithValue(DBParam.Input.CostCenterID, expenseBo.CostCenter == null ? 0 : expenseBo.CostCenter.Id);
            }
            else
            {
                cmd.Parameters.AddWithValue(DBParam.Input.CostCenterID, 0);
            }

            if (expenseBo.PaymentMode != null)
            {
                cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, expenseBo.PaymentMode == null ? 0 : expenseBo.PaymentMode.Id);
            }
            else
            {
                cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, 0);
            }

            cmd.Parameters.AddWithValue(DBParam.Input.BankID, expenseBo.Bank.Id ?? 0);
        }

        private static string DeleteExpense(ExpenseBo expenseBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.DELETE_EXPENSE)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, expenseBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());

            if (expenseBo.PaymentMode != null)
            {
                cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, expenseBo.PaymentMode == null ? 0 : expenseBo.PaymentMode.Id);
            }
            else
            {
                cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, 0);
            }

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        #endregion Expense Management

        #region BillPay

        public List<BillPayBo> SearchBillDetailsList(int? id, int vendorId, string vName)
        {
            List<BillPayBo> billPayBoList = new List<BillPayBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_BILLPAYLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.VendorID, DbType.Int32, vendorId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        billPayBoList.Add(new BillPayBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ExpensePaymentID]),
                            ExpenseId = Utility.CheckDbNull<int>(reader[DBParam.Output.ExpenseID]),
                            BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            BillDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.BillDate]),
                            DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                            ExpenseAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaidAmount]),
                            OutstandingAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OutstandingAmount]),
                            PayAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.updateAmount]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.LedgerName]),
                            HoldingsCount = Utility.CheckDbNull<int>(reader["HoldingsCount"]),
                            HoldingAmount = Utility.CheckDbNull<decimal>(reader["HoldingAmount"])
                        });
                    }
                }
            }

            return billPayBoList;
        }

        public string ManageBillPay(ExpenseBo expenseBo)
        {
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);

            SqlCommand cmd = new SqlCommand(Constants.MANAGE_BILLPAY)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, expenseBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(expenseBo.Description));
            cmd.Parameters.AddWithValue(DBParam.Input.Date, expenseBo.PaymentDate);
            cmd.Parameters.AddWithValue(DBParam.Input.VendorID, expenseBo.VendorId);
            cmd.Parameters.AddWithValue(DBParam.Input.TotalAmount, expenseBo.TotalAmount);
            cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, expenseBo.PaymentMode == null ? 0 : expenseBo.PaymentMode.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.BankID, expenseBo.Bank.Id ?? 0);

            cmd.Parameters.AddWithValue(DBParam.Input.Reference, ValueOrDBNullIfZero(expenseBo.Reference));
            cmd.Parameters.AddWithValue(DBParam.Input.IsDeleted, expenseBo.IsDeleted);
            cmd.Parameters.AddWithValue(DBParam.Input.BillPay, Utility.ToDataTable(expenseBo.BillPay, "BillPay"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            con.Open();
            string bank = "Cash";
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            if (expenseBo.Bank.Id != null)
            {
                expenseBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
                bank = expenseBo.BankList.Find(a => a.Value == expenseBo.Bank.Id.ToString()).Text;
            }

            CommonRepository commonRepository = new CommonRepository();
            expenseBo.PaymentModeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
            string userMessage = cmd.ExecuteScalar().ToString();
            if (userMessage.Split('/')[0] == Notification.Inserted)
            {
                expenseBo.FileId = userMessage.Split('/')[1];
                PopulateMailBody(expenseBo.VendorName, expenseBo.TotalAmount, bank, expenseBo.PaymentDate, expenseBo.Reference,
                    expenseBo.PaymentModeList.Find(a => a.Value == expenseBo.PaymentMode.Id.ToString()).Text
                  , expenseBo.BillPay.Find(m => !string.IsNullOrWhiteSpace(m.UserMessage)).UserMessage, expenseBo.FileId);
            }

            if (userMessage.Split('/')[0] == Notification.Updated)
            {
                PopulateMailBodyForExpenseUpdated(expenseBo.VendorName, expenseBo.TotalAmount, bank, expenseBo.PaymentDate, expenseBo.Reference,
                    expenseBo.PaymentModeList.Find(a => a.Value == expenseBo.PaymentMode.Id.ToString()).Text
                  , expenseBo.BillPay.Find(m => !string.IsNullOrWhiteSpace(m.UserMessage)).UserMessage, expenseBo.Emailhtml, expenseBo.FileId);
            }

            return userMessage.Split('/')[0];
        }

        public string PopulateMailBody(string vendor, decimal amount, string account, DateTime? date, string desc, string payMode, string ledger, string fileId)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/ExpensePayment.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{PartyName}", vendor);
            body = body.Replace("{Ledger}", ledger);
            body = body.Replace("{Amount}", "INR" + " " + Utility.PrecisionForDecimal(amount, 2));
            body = body.Replace("{desc}", desc);            
            body = body.Replace("{Date}", Convert.ToDateTime(date).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{account}", account);            
            body = body.Replace("{Mode}", payMode);
            body = body.Replace("{Createdby}", Utility.GetSession("FirstName") + " - On " + DateTime.Now.ToString("dd-MMMM-yyyy h:mm tt") + " IST");
            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();
            ApplicationConfigBo applicationConfigBo = applicationConfigurationRepository.GetEmailConfig("Expense");
            CommonRepository commonRepository = new CommonRepository();
            commonRepository.ManageEmail_InsertintoProcessorTable(applicationConfigBo.To, applicationConfigBo.Cc, applicationConfigBo.Description, body,
                "OfficeBAU Expense - " + ledger + " - INR " + Utility.PrecisionForDecimal(amount, 2), "Expense", fileId);
            return body;
        }

        public string PopulateMailBodyForExpenseUpdated(string vendor, decimal amount, string account, DateTime? date, string desc, string payMode, string ledger, string body, string fileId)
        {
            body = body.Replace("{PartyNameUpdated}", vendor);
            body = body.Replace("{LedgerUpdated}", ledger);
            body = body.Replace("{AmountUpdated}", "INR" + " " + Utility.PrecisionForDecimal(amount, 2));
            body = body.Replace("{descUpdated}", desc);
            body = body.Replace("{DateUpdated}", Convert.ToDateTime(date).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{accountUpdated}", account);           
            body = body.Replace("{ModeUpdated}", payMode);
            body = body.Replace("{CreatedbyUpdated}", Utility.GetSession("FirstName") + " - On " + DateTime.Now.ToString("dd-MMMM-yyyy h:mm tt") + " IST");
            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();
            ApplicationConfigBo applicationConfigBo = applicationConfigurationRepository.GetEmailConfig("Expense");
            CommonRepository commonRepository = new CommonRepository();
            commonRepository.ManageEmail_InsertintoProcessorTable(applicationConfigBo.To, applicationConfigBo.Cc, applicationConfigBo.Description, body,
                "OfficeBAU Expense Updated- " + ledger + " - INR " + Utility.PrecisionForDecimal(amount, 2), "Expense", fileId);
            return body;
        }

        public ExpenseBo GetBillPayDetails(int id)
        {
            ExpenseBo expenseBo = new ExpenseBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_BILLPAYDETAIL);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBo = new ExpenseBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            PaymentDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PaymentDate]),
                            VendorId = Utility.CheckDbNull<int>(reader[DBParam.Output.VendorID]),
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms]),
                            VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName].ToString()),
                            ScreenType = Utility.CheckDbNull<string>(reader[DBParam.Output.ScreenType].ToString()),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            PaymentMode = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.PaymentModeID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentMode])
                            },
                            Bank = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.BankID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName])
                            },
                            Reference = Utility.CheckDbNull<string>(reader[DBParam.Output.Reference].ToString()),
                            FileId = Utility.CheckDbNull<string>(reader[DBParam.Output.FileUploadID])
                        };
                    }
                }
            }

            if (expenseBo.VendorId != 0)
            {
                expenseBo.BillPay = SearchBillDetailsList(expenseBo.Id, expenseBo.VendorId, expenseBo.VendorName);
            }
            expenseBo.Emailhtml = PopulateMailBodyForUpdated(expenseBo.VendorName, expenseBo.BillPay?.Sum(a => a.PayAmount) ?? 0,
                expenseBo.Bank.Name, expenseBo.PaymentDate,
                expenseBo.Reference,
                expenseBo.PaymentMode.Name,
                expenseBo.BillPay == null ? "" : expenseBo.BillPay.FirstOrDefault()?.UserMessage
              , expenseBo.ModifiedOn, expenseBo.ModifiedByName);
            return expenseBo;
        }

        public ExpenseBo GetVendorDetails(int id)
        {
            ExpenseBo expenseBo = new ExpenseBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_VENDOR);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBo = new ExpenseBo
                        {
                            VendorId = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms])
                        };
                    }
                }
            }

            return expenseBo;
        }

        #endregion BillPay

        #region Supporting Functions

        private object ValueOrDBNullIfZero(string val)
        {
            if (string.IsNullOrEmpty(val))
            {
                return DBNull.Value;
            }

            return val;
        }

        public ExpenseBo GetAvailableCashBalance()
        {
            ExpenseBo expenseBo = new ExpenseBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_AVAILABLECASHBALANCE);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBo = new ExpenseBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.Amount])
                        };
                    }
                }
            }

            return expenseBo;
        }

        #endregion Supporting Functions

        #region HoldingsDetails

        public List<ExpenseHoldingBO> SearchExpenseHoldingList(int id)
        {
            List<ExpenseHoldingBO> invoiceReceivableBoList = new List<ExpenseHoldingBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchExpenseHoldingList");

                Database.AddInParameter(dbCommand, DBParam.Input.ExpenseID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        invoiceReceivableBoList.Add(new ExpenseHoldingBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            BillNo = Utility.CheckDbNull<string>(reader[DBParam.Output.BillNo]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.LedgerName]),
                            ExpenseID = Utility.CheckDbNull<int>(reader[DBParam.Output.ExpenseID]),
                            Date = Utility.CheckDbNull<DateTime?>(reader[DBParam.Output.Date]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            OutstandingAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OutstandingAmount]),
                            BillAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ExpenseAmount]),
                            LedgerID = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID]),
                            LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger")
                        });
                        if (invoiceReceivableBoList[invoiceReceivableBoList.Count - 1].LedgerID != 0)
                        {
                            invoiceReceivableBoList[invoiceReceivableBoList.Count - 1].LedgerList.Find(a => a.Value == invoiceReceivableBoList[invoiceReceivableBoList.Count - 1].LedgerID.ToString()).Selected = true;
                        }
                    }
                }
            }

            return invoiceReceivableBoList;
        }

        public string ManageExpenseHoldings(IList<ExpenseHoldingBO> expenseHoldingBOs)
        {
            string usermessage = string.Empty;

            foreach (ExpenseHoldingBO b in expenseHoldingBOs)
            {
                if (b.Date.HasValue)
                {
                    if (b.LedgerID != 0)
                    {
                        if (b.Amount != 0)
                        {
                            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                            SqlConnection con = new SqlConnection(consString);

                            SqlCommand cmd = new SqlCommand("ManageExpenseHolding", con)
                            {
                                CommandType = CommandType.StoredProcedure,
                                Connection = con
                            };
                            cmd.Parameters.AddWithValue(DBParam.Input.ID, b.Id);
                            cmd.Parameters.AddWithValue(DBParam.Input.ExpenseID, expenseHoldingBOs[0].ExpenseID);
                            cmd.Parameters.AddWithValue(DBParam.Input.Date, b.Date);
                            cmd.Parameters.AddWithValue(DBParam.Input.Amount, b.Amount);
                            cmd.Parameters.AddWithValue(DBParam.Input.IsDeleted, b.IsDeleted);
                            cmd.Parameters.AddWithValue(DBParam.Input.LedgerID, b.LedgerID);
                            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
                            con.Open();
                            usermessage = cmd.ExecuteScalar().ToString();
                        }
                    }
                }
            }

            return usermessage;
        }

        #endregion HoldingsDetails
    }
}