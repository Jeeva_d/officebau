﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class ProductMasterRepository : BaseRepository
    {
        #region Get Product

        public ProductMasterBo GetProduct(int productId)
        {
            ProductMasterBo productMasterBo = new ProductMasterBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_PRODUCT);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, productId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        productMasterBo = new ProductMasterBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            SaleRate = Utility.CheckDbNull<decimal>(reader["SalesPrice"]),
                            PurchaseRate = Utility.CheckDbNull<decimal>(reader["PurchasePrice"]),
                            PurchaseDescription = Utility.CheckDbNull<string>(reader["PurchaseDescription"]),
                            SalesDescription = Utility.CheckDbNull<string>(reader["SalesDescription"]),
                            SalesLedgerId = Utility.CheckDbNull<int>(reader["SalesLedgerId"]),
                            PurchaseLedgerId = Utility.CheckDbNull<int>(reader["PurchaseLedgerId"]),
                            IsInventory = Utility.CheckDbNull<bool>(reader["IsInventroy"]),
                            IsPurchase = Utility.CheckDbNull<bool>(reader["IsPurchase"]),
                            IsSales = Utility.CheckDbNull<bool>(reader["IsSales"]),
                            HsnCode = Utility.CheckDbNull<string>(reader[DBParam.Output.HsnCode]),
                            TypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.TypeID]),
                            Manufacturer = Utility.CheckDbNull<string>(reader[DBParam.Output.Manufacturer]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            LowStockThresold = Utility.CheckDbNull<decimal>(reader["LowStockThresold"]),
                            OpeningStock = Utility.CheckDbNull<int>(reader[DBParam.Output.OpeningStock]),
                            Sgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            Cgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            Igst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.IGST]),
                            Hsnid = Utility.CheckDbNull<int>(reader[DBParam.Output.HSNID]),
                            Uomid = Utility.CheckDbNull<int>(reader[DBParam.Output.UOMID]),
                            SalesCount = Utility.CheckDbNull<int>(reader[DBParam.Output.SalesCount]),
                            PurchaseCount = Utility.CheckDbNull<int>(reader[DBParam.Output.PurchaseCount]),
                            InventoryCount = Utility.CheckDbNull<int>(reader[DBParam.Output.InventoryCount])
                        };
                    }
                }
            }

            return productMasterBo;
        }

        #endregion Get Product

        #region Search Product

        public List<ProductMasterBo> SearchProduct(ProductMasterBo productMasterBo)
        {
            List<ProductMasterBo> productMasterBoList = new List<ProductMasterBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_PRODUCT);

                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, productMasterBo.Name);
                //Database.AddInParameter(dbCommand, DBParam.Input.TransactionType, DbType.Int32, productMasterBo.TransactionTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.TypeID, DbType.Int32, productMasterBo.TypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        productMasterBoList.Add(
                            new ProductMasterBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                Type = Utility.CheckDbNull<string>(reader[DBParam.Output.TypeName]),
                                Manufacturer = Utility.CheckDbNull<string>(reader[DBParam.Output.Manufacturer]),
                                Cgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                                Sgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                                Igst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.IGST]),
                                HsnCode = Utility.CheckDbNull<string>(reader[DBParam.Output.HsnCode]),
                                SalesLedgerName = Utility.CheckDbNull<string>(reader["SalesLedgerName"]),
                                PurchaseLedgerName = Utility.CheckDbNull<string>(reader["PurchaseLedgerName"]),
                                IsInventory = Utility.CheckDbNull<bool>(reader["IsInventroy"]),
                                IsPurchase = Utility.CheckDbNull<bool>(reader["IsPurchase"]),
                                PurchaseRate = Utility.CheckDbNull<decimal>(reader["PurchasePrice"]),
                                PurchaseDescription = Utility.CheckDbNull<string>(reader["PurchaseDescription"]),
                                IsSales = Utility.CheckDbNull<bool>(reader["IsSales"]),
                                SaleRate = Utility.CheckDbNull<decimal>(reader["SalesPrice"]),
                                SalesDescription = Utility.CheckDbNull<string>(reader["SalesDescription"]),
                                LowStockThresold = Utility.CheckDbNull<decimal>(reader["LowStockThresold"]),
                                OpeningStock = Utility.CheckDbNull<int>(reader[DBParam.Output.OpeningStock]),
                            });
                    }
                }
            }

            return productMasterBoList;
        }

        #endregion Search Product

        #region Manage Product

        public string ManageProduct(ProductMasterBo productMasterBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_PRODUCT);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, productMasterBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, productMasterBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.TypeID, DbType.Int32, productMasterBo.TypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, productMasterBo.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.UOMID, DbType.Int32, productMasterBo.Uomid);
                Database.AddInParameter(dbCommand, DBParam.Input.Manufacturer, DbType.String, productMasterBo.Manufacturer);
                Database.AddInParameter(dbCommand, DBParam.Input.OpeningStock, DbType.Int32, productMasterBo.OpeningStock);
                Database.AddInParameter(dbCommand, DBParam.Input.HsnID, DbType.String, productMasterBo.Hsnid);
                Database.AddInParameter(dbCommand, "@LowStock", DbType.Decimal, productMasterBo.LowStockThresold);
                Database.AddInParameter(dbCommand, "@IsInventory", DbType.Boolean, productMasterBo.IsInventory);
                Database.AddInParameter(dbCommand, "@IsPurchase", DbType.Boolean, productMasterBo.IsPurchase);
                Database.AddInParameter(dbCommand, "@IsSales", DbType.Boolean, productMasterBo.IsSales);
                Database.AddInParameter(dbCommand, "@PurchasePrice", DbType.Decimal, productMasterBo.PurchaseRate);
                Database.AddInParameter(dbCommand, "@SalesPrice", DbType.Decimal, productMasterBo.SaleRate);
                Database.AddInParameter(dbCommand, "@PurchaseDescription", DbType.String, productMasterBo.PurchaseDescription);
                Database.AddInParameter(dbCommand, "@SaleseDescription", DbType.String, productMasterBo.SalesDescription);
                Database.AddInParameter(dbCommand, "@PurchaseLedgerID", DbType.Int32, productMasterBo.PurchaseLedgerId);
                Database.AddInParameter(dbCommand, "@SalesLedgerID", DbType.Int32, productMasterBo.SalesLedgerId);
                productMasterBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return productMasterBo.UserMessage;
        }

        #endregion Manage Product

        public List<CustomBo> AutoCompleteHsnCode(string hsnCode)
        {
            int domainId = Utility.DomainId();
            using (FSMEntities context = new FSMEntities())
            {
                List<CustomBo> list = (from t in context.tblGstHSNs
                                       where t.Code.Contains(hsnCode) && !(t.IsDeleted ?? false) && t.DomainID == domainId
                                       orderby t.Code
                                       select new CustomBo
                                       {
                                           Name = t.Code,
                                           Id = t.ID
                                       }).ToList();
                return list;
            }
        }

        public string GetGstHsnCode(int? hsnId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                ProductMasterBo result = (from e in context.tblGstHSNs
                                          where e.ID == hsnId && !(e.IsDeleted ?? false) && e.DomainID == domainId
                                          select new ProductMasterBo
                                          {
                                              Id = e.ID,
                                              Sgst = e.SGST,
                                              Cgst = e.CGST,
                                              Igst = e.IGST,
                                              Name = e.Code
                                          }).FirstOrDefault();
                if (result != null)
                {
                    result.UserMessage = result.Id + "/" + result.Sgst + "/" + result.Cgst + "/" + result.Igst;
                    return result.UserMessage;
                }
                return string.Empty;
            }
        }

        #region GstHSN Master

        public List<ProductMasterBo> GetGstHsnList(ProductMasterBo productMasterBo)
        {
            int domainId = Utility.DomainId();
            using (FSMEntities context = new FSMEntities())
            {
                List<ProductMasterBo> list = (from c in context.tblGstHSNs
                                              where !(c.IsDeleted ?? false) && c.DomainID == domainId
                                              orderby c.ModifiedOn descending
                                              select new ProductMasterBo
                                              {
                                                  Id = c.ID,
                                                  GsthsnCode = c.Code,
                                                  GstDescription = c.GSTDescription,
                                                  Sgst = c.SGST,
                                                  Cgst = c.CGST,
                                                  Igst = c.IGST
                                              }).ToList();
                return list;
            }
        }

        public string ManageGstConfiguration(ProductMasterBo productMasterBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_GSTHSN);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, productMasterBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.Code, DbType.String, productMasterBo.GsthsnCode);
                Database.AddInParameter(dbCommand, DBParam.Input.GSTDescription, DbType.String, productMasterBo.GstDescription);
                Database.AddInParameter(dbCommand, DBParam.Input.CGST, DbType.Decimal, productMasterBo.Cgst);
                Database.AddInParameter(dbCommand, DBParam.Input.SGST, DbType.Decimal, productMasterBo.Sgst);
                Database.AddInParameter(dbCommand, DBParam.Input.IGST, DbType.Decimal, productMasterBo.Igst);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, productMasterBo.IsDeleted);
                productMasterBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return productMasterBo.UserMessage;
        }

        #endregion GstHSN Master
    }
}