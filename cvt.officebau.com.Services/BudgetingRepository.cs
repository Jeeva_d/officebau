﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class BudgetingRepository : BaseRepository
    {
        public string ManageBudgeting(BudgetingBo budgetingBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("ManageBudgeting");
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, budgetingBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.BudgetID, DbType.Int32, budgetingBo.BudgetNameId);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, budgetingBo.Type);
                Database.AddInParameter(dbCommand, DBParam.Input.BudgetType, DbType.String, budgetingBo.BudgetType);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, budgetingBo.TagName);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.String, budgetingBo.BudgetingAmount);
                Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, budgetingBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, budgetingBo.FinancialYearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                budgetingBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return budgetingBo.UserMessage;
        }

        public List<BudgetingBo> SearchBudgetingList(int fyId)
        {
            List<BudgetingBo> budgetingBOlist = new List<BudgetingBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("[SearchBudgeting]");

                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.String, fyId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        budgetingBOlist.Add(
                            new BudgetingBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                BudgetingAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                TagName = Utility.CheckDbNull<string>(reader[DBParam.Output.NAME]),
                                Name = Utility.CheckDbNull<string>(reader["BudgetName"]),
                                Type = Utility.CheckDbNull<string>(reader["Type"]),
                                BudgetNameId = Utility.CheckDbNull<int>(reader["BudgetId"]),
                                BudgetType = Utility.CheckDbNull<string>(reader["BudgetType"]),
                                FinancialYearId = fyId,
                                ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                                ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn])
                            });
                    }
                }
            }

            return budgetingBOlist;
        }

        public List<BudgetingBo> SearchBudgetingReport(int fyId)
        {
            List<BudgetingBo> budgetingBoList = new List<BudgetingBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("[RPT_Budgeting]");

                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.String, fyId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        budgetingBoList.Add(
                            new BudgetingBo
                            {
                                BudgetingAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                                TagName = Utility.CheckDbNull<string>(reader[DBParam.Output.NAME]),
                                Type = Utility.CheckDbNull<string>(reader["Type"]),
                                FinancialYearId = fyId
                            });
                    }
                }
            }

            return budgetingBoList;
        }
    }
}