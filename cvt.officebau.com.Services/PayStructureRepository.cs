﻿//using cvt.officebau.com.Utilities;
//using cvt.officebau.com.Repository;
//using cvt.officebau.com.ViewModels;
//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Data;
//using System.Data.Common;
//using System.Data.SqlClient;
//using System.Linq;

//namespace cvt.officebau.com.Services
//{
//    public class PayStructureRepository : BaseRepository
//    {
//        #region DuplicateCheck

//        public PayStructureBO DuplicateCheck(string name, string table, int bu)
//        {
//            PayStructureBO payStructureBo = new PayStructureBO();

//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DUPLICATENAMECHECK);

//                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, name);
//                Database.AddInParameter(dbCommand, DBParam.Input.Table, DbType.String, table);
//                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, bu);
//                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

//                using (IDataReader reader = Database.ExecuteReader(dbCommand))
//                {
//                    while (reader.Read())
//                    {
//                        payStructureBo = new PayStructureBO
//                        {
//                            UserMessage = Utility.CheckDbNull<string>(reader["Output"])
//                        };
//                    }
//                }

//                return payStructureBo;
//            }
//        }

//        #endregion DuplicateCheck

//        #region CheckforAdditionComponent

//        public List<PayStructureBO> CheckforAdditionComponentsPaystructure(string bu, string type)
//        {
//            List<PayStructureBO> listOfCode = new List<PayStructureBO>();

//            if (type == "Employee")
//            {
//                bu = GetRegionID(bu);
//            }

//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CHECKFORPAYSTRUCTURECONFIGURATION);

//                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.String, bu);
//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

//                using (IDataReader reader = Database.ExecuteReader(dbCommand))
//                {
//                    while (reader.Read())
//                    {
//                        listOfCode.Add(new PayStructureBO
//                        {
//                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Code])
//                        });
//                    }
//                }

//                return listOfCode;
//            }
//        }

//        #endregion CheckforAdditionComponent

//        #region GetRegion

//        private string GetRegionID(string bu)
//        {
//            int domainId = Utility.DomainId();

//            using (FSMEntities context = new FSMEntities())
//            {
//                string regionId = context.tbl_BusinessUnit.FirstOrDefault(s => s.Name.Equals(bu.Replace(" )", "").Trim()) && !(s.IsDeleted ?? false) && s.DomainID == domainId)?.ID.ToString();
//                return regionId;
//            }
//        }

//        #endregion GetRegion

//        #region Company Pay Structure

//        public string ManageCompanyPayStructure(PayStructureBO payStructureBo)
//        {
//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_COMPANYPAYSTRUCTURE);
//                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, payStructureBo.Id);
//                Database.AddInParameter(dbCommand, DBParam.Input.LoginId, DbType.Int32, Utility.UserId());
//                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, payStructureBo.Name);
//                Database.AddInParameter(dbCommand, DBParam.Input.Basic, DbType.Decimal, payStructureBo.Basic);
//                Database.AddInParameter(dbCommand, DBParam.Input.HRA, DbType.Decimal, payStructureBo.HRA);
//                Database.AddInParameter(dbCommand, DBParam.Input.MedicalAllowance, DbType.Decimal, payStructureBo.MedicalAllowance);
//                Database.AddInParameter(dbCommand, DBParam.Input.Conveyance, DbType.Decimal, payStructureBo.Conveyance);
//                Database.AddInParameter(dbCommand, DBParam.Input.EducationAllowance, DbType.Decimal, payStructureBo.EducationAllowance);
//                Database.AddInParameter(dbCommand, DBParam.Input.PaperMagazine, DbType.Decimal, payStructureBo.PaperMagazine);
//                Database.AddInParameter(dbCommand, DBParam.Input.MonsoonAllow, DbType.Decimal, payStructureBo.MonsoonAllow);
//                Database.AddInParameter(dbCommand, DBParam.Input.EffectiveFrom, DbType.DateTime, payStructureBo.EffictiveFrom);
//                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, payStructureBo.BusinessUnitID);
//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
//                payStructureBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
//            }

//            return payStructureBo.UserMessage;
//        }

//        public List<PayStructureBO> SearchCompanyPaystructure(PayStructureBO payStructureBo)
//        {
//            List<PayStructureBO> payStructureBoList = new List<PayStructureBO>();

//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_COMPANYPAYSTRUCTURE);

//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
//                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

//                using (IDataReader reader = Database.ExecuteReader(dbCommand))
//                {
//                    while (reader.Read())
//                    {
//                        payStructureBoList.Add(
//                            new PayStructureBO
//                            {
//                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
//                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
//                                Basic = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]),
//                                HRA = Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]),
//                                MedicalAllowance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]),
//                                Conveyance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]),
//                                EffictiveFrom = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EffictiveFrom]),
//                                BusinessUnitIDs = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit])
//                            });
//                    }
//                }
//            }

//            return payStructureBoList;
//        }

//        public PayStructureBO GetCompanyPayStructure(int id)
//        {
//            PayStructureBO payStructureBo = new PayStructureBO();

//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_COMPANYPAYSTRUCTURE);

//                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

//                using (IDataReader reader = Database.ExecuteReader(dbCommand))
//                {
//                    while (reader.Read())
//                    {
//                        payStructureBo = new PayStructureBO
//                        {
//                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
//                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
//                            Basic = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]),
//                            HRA = Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]),
//                            MedicalAllowance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]),
//                            Conveyance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]),
//                            EducationAllowance =
//                                Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]),
//                            PaperMagazine = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]),
//                            MonsoonAllow = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]),
//                            EffictiveFrom = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EffictiveFrom]),
//                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
//                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
//                            BusinessUnitID = Utility.CheckDbNull<int>(reader[DBParam.Output.BusinessUnit]),
//                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation])
//                        };
//                    }
//                }
//            }

//            return payStructureBo;
//        }

//        #endregion Company Pay Structure

//        #region Employee Pay Structure

//        public PayStructureBO GetPayStructureList(int id)
//        {
//            PayStructureBO payStructureBo = new PayStructureBO();

//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEEPAYLIST);

//                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

//                using (IDataReader reader = Database.ExecuteReader(dbCommand))
//                {
//                    while (reader.Read())
//                    {
//                        payStructureBo = new PayStructureBO
//                        {
//                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
//                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
//                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
//                            Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross]),
//                            EffictiveFrom = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EffictiveFrom]),
//                            CompanyPayStucture = new  cvt.officebau.com.ViewModels.Entity
//                            {
//                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CompanyPayStucId]),
//                                Name = reader[DBParam.Output.CompanyPayStucName].ToString()
//                            },
//                            CompanyPayStuctureID = Utility.CheckDbNull<int>(reader[DBParam.Output.CompanyPayStucId]),
//                            IsPFRequired = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPFRequired]),
//                            IsESIRequired = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsESIRequired]),
//                            DOJ = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]),
//                            MedicalAllowance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]),
//                            Conveyance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]),
//                            EducationAllowance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]),
//                            PaperMagazine = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]),
//                            MonsoonAllow = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]),
//                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
//                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
//                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation])
//                        };
//                    }
//                }
//            }

//            return payStructureBo;
//        }

//        public List<PayStructureBO> SearchEmployee(PayStructureBO payStructureBo)
//        {
//            List<PayStructureBO> listOfMenu = new List<PayStructureBO>();

//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHEMPLOYEEPAYSTRUCTURE);

//                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, payStructureBo.BusinessUnitIDs);
//                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, payStructureBo.EmployeeId);
//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
//                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, payStructureBo.IsActive);

//                using (IDataReader reader = Database.ExecuteReader(dbCommand))
//                {
//                    while (reader.Read())
//                    {
//                        listOfMenu.Add(new PayStructureBO
//                        {
//                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
//                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
//                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
//                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
//                            Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross]),
//                            EffictiveFrom = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EffictiveFrom]),
//                            CompanyPayStucture = new  cvt.officebau.com.ViewModels.Entity
//                            {
//                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CompanyPayStucId]),
//                                Name = reader[DBParam.Output.CompanyPayStucName].ToString()
//                            },
//                            Counts = Utility.CheckDbNull<int>(reader[DBParam.Output.Counts])
//                        });
//                    }
//                }
//            }

//            return listOfMenu;
//        }

//        public List<PayStructureBO> SearchUser(PayStructureBO payStructureBo)
//        {
//            List<PayStructureBO> userList = new List<PayStructureBO>();

//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHUSER);

//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

//                using (IDataReader reader = Database.ExecuteReader(dbCommand))
//                {
//                    while (reader.Read())
//                    {
//                        userList.Add(
//                            new PayStructureBO
//                            {
//                                UserId = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
//                                UserName = reader[DBParam.Output.Name].ToString(),
//                                EmployeeName = reader[DBParam.Output.EmployeeName].ToString()
//                            });
//                    }
//                }
//            }

//            return userList;
//        }

//        public PayStructureBO SearchEmployeePayStructure(PayStructureBO payStructureBo)
//        {
//            PayStructureBO payBo = new PayStructureBO();

//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_EMPLOYEEPAYSTRUCTURE);

//                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, payStructureBo.Id);
//                Database.AddInParameter(dbCommand, DBParam.Input.CompanyPayStubId, DbType.Int32, payStructureBo.CompanyPayStuctureID);
//                Database.AddInParameter(dbCommand, DBParam.Input.Gross, DbType.Decimal, payStructureBo.Gross);
//                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, payStructureBo.EmployeeId);
//                Database.AddInParameter(dbCommand, DBParam.Input.IsPFRequired, DbType.Boolean, payStructureBo.IsPFRequired);
//                Database.AddInParameter(dbCommand, DBParam.Input.IsESIRequired, DbType.Boolean, payStructureBo.IsESIRequired);
//                Database.AddInParameter(dbCommand, DBParam.Input.MedicalAllowance, DbType.Decimal, payStructureBo.MedicalAllowance);
//                Database.AddInParameter(dbCommand, DBParam.Input.Conveyance, DbType.Decimal, payStructureBo.Conveyance);
//                Database.AddInParameter(dbCommand, DBParam.Input.EducationAllowance, DbType.Decimal, payStructureBo.EducationAllowance);
//                Database.AddInParameter(dbCommand, DBParam.Input.PaperMagazine, DbType.Decimal, payStructureBo.PaperMagazine);
//                Database.AddInParameter(dbCommand, DBParam.Input.MonsoonAllow, DbType.Decimal, payStructureBo.MonsoonAllow);

//                using (IDataReader reader = Database.ExecuteReader(dbCommand))
//                {
//                    while (reader.Read())
//                    {
//                        payBo = new PayStructureBO
//                        {
//                            Basic = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]),
//                            HRA = Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]),
//                            MedicalAllowance =
//                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance])),
//                            Conveyance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]),
//                            EducationAllowance =
//                                Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]),
//                            PaperMagazine = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]),
//                            MonsoonAllow = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]),
//                            SplAllow = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SplAllow]),
//                            EPF = Utility.CheckDbNull<decimal>(reader[DBParam.Output.EPF]),
//                            PT = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PT]),
//                            TDS = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS]),
//                            EESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EESI])),
//                            ERESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERESI])),
//                            ERPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERPF])),
//                            Bonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Bonus])),
//                            Gratuity = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gratuity])),
//                            Medicalclaim = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MEDICLAIM]),
//                            PLI = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PLI]),
//                            MobileDataCard = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MobileDataCard]),
//                            OtherPerks = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherPerks]),
//                            Counts = Utility.CheckDbNull<int>(reader[DBParam.Output.CheckConfig]),
//                            CheckCompanyPS = Utility.CheckDbNull<int>(reader[DBParam.Output.CheckCompanyPS])
//                        };
//                    }
//                }

//                return payBo;
//            }
//        }

//        public DataSet GetPayHistoryList(PayStructureBO payStructureBo)
//        {
//            string connectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
//            DataSet ds = new DataSet();
//            DataTable dtExpenseSupplier = new DataTable("EmployeePayHistory");
//            ds.Tables.Add(dtExpenseSupplier);

//            using (SqlConnection con = new SqlConnection(connectionString))
//            {
//                using (SqlCommand cmd = new SqlCommand(Constants.GET_EMPLOYEEPAYHISTORY, con))
//                {
//                    cmd.CommandType = CommandType.StoredProcedure;
//                    cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, payStructureBo.EmployeeId);
//                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
//                    con.Open();

//                    using (SqlDataReader dr = cmd.ExecuteReader())
//                    {
//                        ds.Load(dr, LoadOption.OverwriteChanges, dtExpenseSupplier);
//                    }
//                }
//            }

//            return ds;
//        }

//        public string ManageEmployeePayStructure(PayStructureBO payStructureBo)
//        {
//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_EMPLOYEEPAYSTRUCTURE);
//                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, payStructureBo.Id);
//                Database.AddInParameter(dbCommand, DBParam.Input.LoginId, DbType.Int32, Utility.UserId());
//                Database.AddInParameter(dbCommand, DBParam.Input.CompanyPayStubId, DbType.Int32, payStructureBo.CompanyPayStuctureID);
//                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, payStructureBo.EmployeeId);
//                Database.AddInParameter(dbCommand, DBParam.Input.Gross, DbType.Decimal, payStructureBo.Gross);
//                Database.AddInParameter(dbCommand, DBParam.Input.EffictiveFrom, DbType.DateTime, payStructureBo.EffictiveFrom);
//                Database.AddInParameter(dbCommand, DBParam.Input.Bonus, DbType.Decimal, payStructureBo.Bonus);
//                Database.AddInParameter(dbCommand, DBParam.Input.Mediclaim, DbType.Decimal, payStructureBo.Medicalclaim);
//                Database.AddInParameter(dbCommand, DBParam.Input.PLI, DbType.Decimal, payStructureBo.PLI);
//                Database.AddInParameter(dbCommand, DBParam.Input.MobileDataCard, DbType.Decimal, payStructureBo.MobileDataCard);
//                Database.AddInParameter(dbCommand, DBParam.Input.OtherPerks, DbType.Decimal, payStructureBo.OtherPerks);
//                Database.AddInParameter(dbCommand, DBParam.Input.CTC, DbType.Decimal, payStructureBo.CTC);
//                Database.AddInParameter(dbCommand, DBParam.Input.IsPFRequired, DbType.Boolean, payStructureBo.IsPFRequired);
//                Database.AddInParameter(dbCommand, DBParam.Input.IsESIRequired, DbType.Boolean, payStructureBo.IsESIRequired);
//                Database.AddInParameter(dbCommand, DBParam.Input.MedicalAllowance, DbType.Decimal, payStructureBo.MedicalAllowance);
//                Database.AddInParameter(dbCommand, DBParam.Input.Conveyance, DbType.Decimal, payStructureBo.Conveyance);
//                Database.AddInParameter(dbCommand, DBParam.Input.EducationAllowance, DbType.Decimal, payStructureBo.EducationAllowance);
//                Database.AddInParameter(dbCommand, DBParam.Input.PaperMagazine, DbType.Decimal, payStructureBo.PaperMagazine);
//                Database.AddInParameter(dbCommand, DBParam.Input.MonsoonAllow, DbType.Decimal, payStructureBo.MonsoonAllow);
//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
//                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, payStructureBo.IsDeleted);
//                payStructureBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
//            }

//            return payStructureBo.UserMessage;
//        }

//        public string DeleteCompanyPayStructure(PayStructureBO payStructureBo)
//        {
//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DELETE_COMPANYPAYSTRUCTURE);

//                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, payStructureBo.Id);
//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

//                payStructureBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
//            }

//            return payStructureBo.UserMessage;
//        }

//        public string DeleteEmployeePayStructure(PayStructureBO payStructureBo)
//        {
//            using (Database.CreateConnection())
//            {
//                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DELETE_EMPLOYEEPAYSTRUCTURE);

//                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, payStructureBo.Id);
//                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

//                payStructureBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
//            }

//            return payStructureBo.UserMessage;
//        }

//        #endregion Employee Pay Structure

//        #region PayStructure Configuration

//        public string GetPaystructureConfiguration(PayStructureBO paystructure)
//        {
//            using (FSMEntities context = new FSMEntities())
//            {
//                PayStructureBO list = (from t in context.tbl_PaystructureConfiguration
//                                       join emp in context.tbl_EmployeeMaster on t.ModifiedBy equals emp.ID
//                                       join bu in context.tbl_BusinessUnit on emp.BaseLocationID equals bu.ID into business
//                                       from bUnit in business.DefaultIfEmpty()
//                                       where t.DomainID == paystructure.DomainId && t.Code == paystructure.Code
//                                       select new PayStructureBO
//                                       {
//                                           Code = t.Code,
//                                           BusinessUnitIDs = t.BusinessUnitIDs,
//                                           ModifiedByName = emp.FullName,
//                                           ModifiedOn = t.ModifiedOn,
//                                           BaseLocation = bUnit.Name
//                                       }).FirstOrDefault();
//                return list == null ? "" : list.BusinessUnitIDs + '/' + list.ModifiedByName + '/' + Convert.ToDateTime(list.ModifiedOn).ToString("dd-MMM-yyyy HH:mm") + '/' + list.BaseLocation;
//            }
//        }

//        public string ManagePayStructureConfiguration(PayStructureBO managePaystructConfig)
//        {
//            using (FSMEntities context = new FSMEntities())
//            {
//                string successMessage;
//                if (!context.tbl_PaystructureConfiguration.Any(a => a.DomainID == managePaystructConfig.DomainId && a.Code == managePaystructConfig.Code))
//                {
//                    tbl_PaystructureConfiguration paystructureConfiguration = new tbl_PaystructureConfiguration
//                    {
//                        BusinessUnitIDs = managePaystructConfig.BusinessUnitIDs,
//                        Code = managePaystructConfig.Code,
//                        CreatedBy = Utility.UserId(),
//                        CreatedOn = DateTime.Now,
//                        ModifiedBy = Utility.UserId(),
//                        ModifiedOn = DateTime.Now,
//                        HistoryID = Guid.NewGuid(),
//                        DomainID = Utility.DomainId(),
//                        IsDeleted = false
//                    };
//                    context.tbl_PaystructureConfiguration.Add(paystructureConfiguration);
//                    context.SaveChanges();
//                    successMessage = "Inserted Successfully";
//                }
//                else
//                {
//                    tbl_PaystructureConfiguration paystructureConfiguration = context.tbl_PaystructureConfiguration.FirstOrDefault(a => a.DomainID == managePaystructConfig.DomainId && a.Code == managePaystructConfig.Code);
//                    //paystructureConfiguration.ID = managePaystructConfig.ID;
//                    if (paystructureConfiguration != null)
//                    {
//                        paystructureConfiguration.BusinessUnitIDs = managePaystructConfig.BusinessUnitIDs;
//                        paystructureConfiguration.Code = managePaystructConfig.Code;
//                        paystructureConfiguration.ModifiedBy = Utility.UserId();
//                        paystructureConfiguration.ModifiedOn = DateTime.Now;
//                        paystructureConfiguration.HistoryID = Guid.NewGuid();
//                        paystructureConfiguration.IsDeleted = false;

//                        context.Entry(paystructureConfiguration).State = EntityState.Modified;
//                    }

//                    managePaystructConfig.Id = managePaystructConfig.Id;
//                    context.SaveChanges();
//                    successMessage = "Updated Successfully";
//                }

//                return successMessage;
//            }
//        }

//        #endregion PayStructure Configuration
//    }
//}