﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class CompanyMasterRepository : BaseRepository
    {
        public List<CompanyMasterBo> GetCompanyMasterList(CompanyMasterBo companyMasterBo)
        {
            List<CompanyMasterBo> companyMasterBoList = new List<CompanyMasterBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHCOMPANYMASTER);

                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, companyMasterBo.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, companyMasterBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.EmailID, DbType.String, companyMasterBo.EmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.GSTNO, DbType.String, companyMasterBo.Gstno);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        companyMasterBoList.Add(new CompanyMasterBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            FullName = reader[DBParam.Output.FullName].ToString(),
                            Name = reader[DBParam.Output.Name].ToString(),
                            ContactNo = reader[DBParam.Output.ContactNo].ToString(),
                            Address = reader[DBParam.Output.Address1].ToString(),
                            EmailId = reader[DBParam.Output.EmailID].ToString(),
                            Panno = reader[DBParam.Output.PANNO].ToString(),
                            Tanno = reader[DBParam.Output.TANNO].ToString(),
                            Gstno = reader[DBParam.Output.GSTNO].ToString(),
                            Website = reader[DBParam.Output.Website].ToString()
                        });
                    }
                }
            }

            return companyMasterBoList;
        }

        public CompanyMasterBo GetCompanyMasterDetail(int id)
        {
            CompanyMasterBo companyMasterBo = new CompanyMasterBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETCOMPANYMASTERDETAIL);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        companyMasterBo = new CompanyMasterBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                            Address = Utility.CheckDbNull<string>(reader[DBParam.Output.Address1]),
                            EmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]),
                            Panno = Utility.CheckDbNull<string>(reader[DBParam.Output.PANNO]),
                            Tanno = Utility.CheckDbNull<string>(reader[DBParam.Output.TANNO]),
                            Gstno = Utility.CheckDbNull<string>(reader[DBParam.Output.GSTNO]),
                            FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.FullName]),
                            Website = Utility.CheckDbNull<string>(reader[DBParam.Output.Website]),
                            Logo = Utility.CheckDbNull<string>(reader[DBParam.Output.Logo]),
                            FileUpload = Utility.CheckDbNull<string>(reader[DBParam.Output.Logo]),
                            FileUploadId = Utility.CheckDbNull<string>(reader[DBParam.Output.FileUploadID]) == null ? Guid.Empty : Guid.Parse(reader[DBParam.Output.FileUploadID].ToString()),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            PaymentFavour = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentFavour]),
                            CommunicationEmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.CommunicationEmailId]),
                            EmpCodePattern = Utility.CheckDbNull<string>(reader[DBParam.Output.EmpCodePattern])
                        };
                    }
                }
            }

            return companyMasterBo;
        }

        public string ManageCompanyMaster(CompanyMasterBo companyMasterBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGECOMPANYMASTER);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, companyMasterBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, companyMasterBo.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.FullName, DbType.String, companyMasterBo.FullName);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, companyMasterBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.Address, DbType.String, companyMasterBo.Address);
                Database.AddInParameter(dbCommand, DBParam.Input.EmailID, DbType.String, companyMasterBo.EmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.PANNO, DbType.String, companyMasterBo.Panno);
                Database.AddInParameter(dbCommand, DBParam.Input.TANNO, DbType.String, companyMasterBo.Tanno);
                Database.AddInParameter(dbCommand, DBParam.Input.GSTNO, DbType.String, companyMasterBo.Gstno);
                Database.AddInParameter(dbCommand, DBParam.Input.Website, DbType.String, companyMasterBo.Website);
                Database.AddInParameter(dbCommand, DBParam.Input.PaymentFavour, DbType.String, companyMasterBo.PaymentFavour);
                Database.AddInParameter(dbCommand, DBParam.Input.CommunicationEmailId, DbType.String, companyMasterBo.CommunicationEmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.Logo, DbType.Guid, companyMasterBo.FileUploadId);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, companyMasterBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.EmpCodePattern, DbType.String, companyMasterBo.EmpCodePattern);
                companyMasterBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return companyMasterBo.UserMessage;
        }
    }
}