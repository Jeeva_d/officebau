﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class MasterDataRepository : BaseRepository
    {
        #region Manage MasterData

        public string ManageMasterData(MasterDataBo masterDataBo)
        {
            using (Database.CreateConnection())
            {
                if ((masterDataBo.DependantTable == "Null" ? "" : masterDataBo.DependantTable.ToUpper()) == "BUSINESSUNIT" && masterDataBo.MainTable.ToUpper() == "BUSINESSUNIT")
                {
                    int domainId = Utility.DomainId();

                    using (FSMEntities context = new FSMEntities())
                    {
                        int check;
                        if (masterDataBo.Id == 0)
                        {
                            check = context.tbl_BusinessUnit.Count(s => s.Name == masterDataBo.Code && s.ParentID == masterDataBo.DependantDropDown.Id && (s.ParentID ?? 0) != 0 && !(s.IsDeleted ?? false) && s.DomainID == domainId);

                            if (check == 0)
                            {
                                tbl_BusinessUnit businessUnit = new tbl_BusinessUnit
                                {
                                    Name = masterDataBo.Code,
                                    Remarks = masterDataBo.Description,
                                    ParentID = masterDataBo.DependantDropDown.Id,
                                    IsDeleted = masterDataBo.IsDeleted,
                                    CreatedBy = Utility.UserId(),
                                    CreatedOn = DateTime.Now,
                                    ModifiedOn = DateTime.Now,
                                    ModifiedBy = Utility.UserId(),
                                    HistoryID = Guid.NewGuid(),
                                    DomainID = Utility.DomainId()
                                };

                                context.tbl_BusinessUnit.Add(businessUnit);
                                context.SaveChanges();

                                masterDataBo.UserMessage = Notification.Inserted;
                            }
                            else
                            {
                                masterDataBo.UserMessage = Notification.AlreadyExists;
                            }
                        }
                        else
                        {
                            check = context.tbl_BusinessUnit.Count(s => s.Name == masterDataBo.Code && s.ParentID == masterDataBo.DependantDropDown.Id && (s.ParentID ?? 0) != 0 && s.ID != masterDataBo.Id && !(s.IsDeleted ?? false) && s.DomainID == domainId);

                            if (check == 0)
                            {
                                tbl_BusinessUnit businessUnit = context.tbl_BusinessUnit.FirstOrDefault(b => b.ID == masterDataBo.Id);
                                if (businessUnit != null)
                                {
                                    businessUnit.Name = masterDataBo.Code;
                                    businessUnit.Remarks = masterDataBo.Description;
                                    businessUnit.ParentID = masterDataBo.DependantDropDown.Id;
                                    businessUnit.IsDeleted = masterDataBo.IsDeleted;
                                    businessUnit.ModifiedOn = DateTime.Now;
                                    businessUnit.ModifiedBy = Utility.UserId();
                                    businessUnit.HistoryID = Guid.NewGuid();
                                    businessUnit.DomainID = Utility.DomainId();

                                    context.Entry(businessUnit).State = EntityState.Modified;
                                }

                                context.SaveChanges();

                                masterDataBo.UserMessage = Notification.Updated;
                            }
                            else
                            {
                                masterDataBo.UserMessage = Notification.AlreadyExists;
                            }
                        }
                    }
                }
                else
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_MASTERDATA);

                    Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, masterDataBo.Id);
                    Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.Code, DbType.String, masterDataBo.Code);
                    Database.AddInParameter(dbCommand, DBParam.Input.Description, DbType.String, masterDataBo.Description);
                    Database.AddInParameter(dbCommand, DBParam.Input.DependantTable, DbType.String, masterDataBo.DependantTable.ToUpper() == "NULL" ? null : masterDataBo.DependantTable);
                    Database.AddInParameter(dbCommand, DBParam.Input.MainTable, DbType.String, masterDataBo.MainTable);
                    if (masterDataBo.MainTable.ToUpper() == "DESTINATIONCITIES")
                    {
                        Database.AddInParameter(dbCommand, DBParam.Input.DependantTableID, DbType.Int32, masterDataBo.TypeId);
                    }
                    else
                    {
                        Database.AddInParameter(dbCommand, DBParam.Input.DependantTableID, DbType.Int32, masterDataBo.DependantDropDown?.Id);
                    }

                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                    masterDataBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
                }
            }

            return masterDataBo.UserMessage;
        }

        #endregion Manage MasterData

        #region Delete Master Data

        public string DeleteMasterData(MasterDataBo masterDataBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DELETE_MASTERDATA);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, masterDataBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DependantTable, DbType.String, masterDataBo.DependantTable == "Null" ? null : masterDataBo.DependantTable);
                Database.AddInParameter(dbCommand, DBParam.Input.MainTable, DbType.String, masterDataBo.MainTable);
                Database.AddInParameter(dbCommand, DBParam.Input.DependantTableID, DbType.Int32, masterDataBo.DependantDropDown?.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.ColumnName, DbType.String, masterDataBo.Parameters);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, masterDataBo.IsDeleted);
                masterDataBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return masterDataBo.UserMessage;
        }

        #endregion Delete Master Data

        #region EmployeeDropDownCode

        public List<SelectListItem> SearchEmpDropDownForCode(string type, int employeeId)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHEMPDROPDOWNFORCODE);

                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId != 0 ? employeeId : Utility.UserId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #endregion EmployeeDropDownCode

        #region Base Location

        public int GetBaseLocation(int userId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                return context.tbl_EmployeeMaster.Where(e => !e.IsDeleted && !(e.IsActive ?? false) && e.DomainID == domainId && e.ID == userId).Select(e => e.BaseLocationID).FirstOrDefault();
            }
        }

        #endregion Base Location

        #region Get Current Year ID

        public int GetCurrentYearId(string currentYear)
        {
            int result;
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                result = (from c in context.tbl_FinancialYear
                          where c.IsDeleted != true && c.DomainID == domainId && c.FinancialYear.Contains(currentYear)
                          select c.ID).FirstOrDefault();
            }

            return result;
        }

        #endregion Get Current Year ID

        #region Get Status ID Based On Type & Code

        public int GetStatusId(string type, string codeName)
        {
            using (FSMEntities context = new FSMEntities())
            {
                return context.tbl_Status.Where(s => !s.IsDeleted && s.Type.ToLower().Trim() == type.ToLower() && s.Code.ToLower().Trim() == codeName.ToLower()).Select(s => s.ID).FirstOrDefault();
            }
        }

        #endregion Get Status ID Based On Type & Code

        #region Product List

        public List<SelectListItem> GetPreRequestProduct(string type)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_PREREQUESTPRODUCT);

                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #endregion Product List

        #region Product/Ledger List

        public List<SelectListItem> GetPreRequestProductLedger(string type)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETPREREQUESTLEDGERPRODUCT);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #endregion Product/Ledger List

        #region Approver Details

        public string GetApprover(int employeeId, string type)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int reportingId = Convert.ToInt32(context.tbl_EmployeeMaster.Where(b => b.ID == employeeId && !b.IsDeleted && !(b.IsActive ?? false)).OrderByDescending(b => b.ID).Select(b => b.ReportingToID).FirstOrDefault());

                if (reportingId != 0)
                {
                    var result = (from e in context.tbl_EmployeeMaster
                                  where e.ID == reportingId && !e.IsDeleted && !(e.IsActive ?? false)
                                  select new
                                  {
                                      Approver = e.FullName,
                                      ApproverID = e.ID,
                                      e.Code,
                                      e.EmpCodePattern
                                  }).FirstOrDefault();
                    if (type.ToUpper() == "NAME")
                    {
                        return result == null ? "" : (result.EmpCodePattern ?? "") + (result.Code ?? "") + " - " + result.Approver;
                    }

                    return result?.ApproverID.ToString();
                }

                return null;
            }
        }

        #endregion Approver Details

        #region Search

        #region MasterData Menu List

        public List<MasterDataBo> SearchMasterDataMenuList(string menuCode)
        {
            List<MasterDataBo> masterDataBoList = new List<MasterDataBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_MASTERDATAMENU);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, "@Rootconfig", DbType.String, Utility.GetSession("ModuleName"));
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        masterDataBoList.Add(
                            new MasterDataBo
                            {
                                Menu = Utility.CheckDbNull<string>(reader[DBParam.Output.Menu]),
                                Parameters = Utility.CheckDbNull<string>(reader[DBParam.Output.MenuParameters]),
                                MenuCode = menuCode
                            });
                    }
                }
            }

            return masterDataBoList;
        }

        #endregion MasterData Menu List

        #region MasterData

        public List<MasterDataBo> SearchMasterData(string mainTable, string depandantTable)
        {
            List<MasterDataBo> masterDataBoList = new List<MasterDataBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_MASTERDATA);

                Database.AddInParameter(dbCommand, DBParam.Input.DependantTable, DbType.String, depandantTable == "Null" ? null : depandantTable);
                Database.AddInParameter(dbCommand, DBParam.Input.MainTable, DbType.String, mainTable);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        MasterDataBo masterDataBo = new MasterDataBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Code = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            DependantDropDown = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.DRPName])
                            },
                            MainTable = mainTable,
                            DependantTable = depandantTable
                        };
                        if (mainTable.ToUpper() == "DESTINATIONCITIES")
                        {
                            masterDataBo.TypeName = Utility.CheckDbNull<string>(reader[DBParam.Output.DRPName]);
                        }

                        masterDataBoList.Add(masterDataBo);
                    }
                }
            }

            return masterDataBoList;
        }

        #endregion MasterData

        #region MasterData DropDown

        public List<SelectListItem> SearchMasterDataDropDown(string dependantTable, string type)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_MASTERDATADROPDOWN);

                Database.AddInParameter(dbCommand, DBParam.Input.DependantTable, DbType.String, dependantTable);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        public List<SelectListItem> SearchMasterDataDropDown(string dependantTable)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_MASTERDATADROPDOWN2);

                Database.AddInParameter(dbCommand, DBParam.Input.DependantTable, DbType.String, dependantTable);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        public List<SelectListItem> SearchMasterDataDropDownByCode(string dependantTable, string type)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_MASTERDATADROPDOWN);

                Database.AddInParameter(dbCommand, DBParam.Input.DependantTable, DbType.String, dependantTable);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<string>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #endregion MasterData DropDown

        #endregion Search

        #region GET MasterData

        #region MasterData

        public MasterDataBo GetMasterData(string mainTable, string depandantTable, int? id)
        {
            MasterDataBo masterDataBo = new MasterDataBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_MASTERDATA);

                Database.AddInParameter(dbCommand, DBParam.Input.DependantTable, DbType.String, depandantTable == "Null" ? null : depandantTable);
                Database.AddInParameter(dbCommand, DBParam.Input.MainTable, DbType.String, mainTable);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        masterDataBo = new MasterDataBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Code = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            DependantDropDown = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.DRPID])
                            },
                            MainTable = mainTable,
                            DependantTable = depandantTable
                        };
                        if (mainTable.ToUpper() == "DESTINATIONCITIES")
                        {
                            masterDataBo.TypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.DRPID]);
                        }
                    }
                }
            }

            return masterDataBo;
        }

        public int GetCodeMasterDataId(string type, string codeName)
        {
            int result;

            using (FSMEntities context = new FSMEntities())
            {
                result = (from c in context.tbl_CodeMaster
                          where !c.IsDeleted && c.Type == type && c.Code == codeName
                          select c.ID).FirstOrDefault();
            }

            return result;
        }

        #endregion MasterData

        #endregion GET MasterData

        #region AutoComplete

        public List< cvt.officebau.com.ViewModels.Entity> SearchAutoCompleteEmployee(string searchName)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List< cvt.officebau.com.ViewModels.Entity> list = (from t in context.tbl_EmployeeMaster
                                        where !t.IsDeleted && t.DomainID == domainId && !(t.IsActive ?? false) && t.FullName.Contains(searchName)
                                        orderby t.FullName
                                        select new  cvt.officebau.com.ViewModels.Entity
                                        {
                                            Name = (t.EmpCodePattern ?? "") + (t.Code ?? "") + " - " + (t.FullName ?? ""),
                                            Id = t.ID
                                        }).ToList();
                return list;
            }
        }

        #endregion AutoComplete

        #region EmployeeDropDown

        //Based on Access
        public List<cvt.officebau.com.ViewModels.Entity> SearchAutoCompleteEmployeebasedonAccess(string searchName, int? employeeId)
        {
            List<cvt.officebau.com.ViewModels.Entity> list = new List<cvt.officebau.com.ViewModels.Entity>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_AUTOCOMPLETE_EMPLOYEE_BASED_ON_ACCESS);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId ?? 0);
                Database.AddInParameter(dbCommand, DBParam.Input.SearchParameter, DbType.String, searchName);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, employeeId ?? Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        list.Add(new  cvt.officebau.com.ViewModels.Entity
                        {
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return list;
        }

        //Based on BU Access
        public List< cvt.officebau.com.ViewModels.Entity> SearchAutoCompleteEmployeebasedonBu(string searchName, int? employeeId = 0, string type = "")
        {
            List< cvt.officebau.com.ViewModels.Entity> list = new List< cvt.officebau.com.ViewModels.Entity>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_AUTOCOMPLETE_EMPLOYEE_BASED_ON_BU);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId ?? 0);
                Database.AddInParameter(dbCommand, DBParam.Input.SearchParameter, DbType.String, searchName);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type ?? "");

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        list.Add(new  cvt.officebau.com.ViewModels.Entity
                        {
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return list;
        }

        //Employee Search for Autocomplete with all active and inactive employees
        public List<CustomBo> AutoCompleteEmployeeWithInActive(string employeeName)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<CustomBo> list = (from t in context.tbl_EmployeeMaster
                                       where !t.IsDeleted && t.DomainID == domainId && t.FullName.Contains(employeeName) && t.HasAccess
                                       orderby t.FullName
                                       select new CustomBo
                                       {
                                           Name = (t.EmpCodePattern ?? "") + (t.Code ?? "") + " - " + (t.FullName ?? ""),
                                           Id = t.ID
                                       }).ToList();
                return list;
            }
        }

        //Based on Region or Based on BU or include or exclude login Employee
        public List<SelectListItem> GetEmployeeDropDown(string type, int employeeId, bool isIncludeEmployee)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_EMPLOYEEDROPDOWN);

                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.IsIncludeEmployee, DbType.Boolean, isIncludeEmployee);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #endregion EmployeeDropDown

        #region Business Unit Dropdown

        public List<SelectListItem> GetBusinessUnitDropDown()
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETBUSINESSUNITDROPDOWN);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        public List<SelectListItem> GetRegionBasedOnEmployeeAccess(int employeeId, int? regionId, string type)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_REGIONBASEDONEMPLOYEEACCESS);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.RegionID, DbType.Int32, regionId);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        public List<SelectListItem> GetBusinessUnitBasedSalesExecutives(int id, string type)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETBUBASEDSALESEXECUTIVEDROPDOWN);

                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        public List< cvt.officebau.com.ViewModels.Entity> GetBusinessUnitDependOnRegion(int? regionId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List< cvt.officebau.com.ViewModels.Entity> list = (from t in context.tbl_BusinessUnit
                                        where !(t.IsDeleted ?? false) && t.DomainID == domainId && (t.ParentID ?? 0) != 0 && (t.ParentID ?? 0) == regionId
                                        orderby t.Name
                                        select new  cvt.officebau.com.ViewModels.Entity
                                        {
                                            Name = t.Name,
                                            Id = t.ID
                                        }).ToList();
                return list;
            }
        }

        public List<SelectListItem> GetRegionDropDownDependOnEmployee(int employeeId)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List< cvt.officebau.com.ViewModels.Entity> list = (from t in context.tbl_BusinessUnit
                                        join e in context.tbl_EmployeeMaster on t.ID equals e.RegionID
                                        where !(t.IsDeleted ?? false) && t.DomainID == domainId && (t.ParentID ?? 0) == 0 && e.ID == employeeId
                                        orderby t.Name
                                        select new  cvt.officebau.com.ViewModels.Entity
                                        {
                                            Name = t.Name,
                                            Id = t.ID
                                        }).ToList();
                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                foreach ( cvt.officebau.com.ViewModels.Entity item in list)
                {
                    dependantDropDownList.Add(new SelectListItem
                    {
                        Text = item.Name,
                        Value = item.Id.ToString()
                    });
                }

                return dependantDropDownList;
            }
        }

        public List<SelectListItem> GetEmployeeNameBasedonBu(int employeeId)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_EMPLOYEENAMEBASEDONBU);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #endregion Business Unit Dropdown

        #region Generate Employee Code

        // Temporary Employee Code
        public string AutoGenerateEmployeeCode()
        {
            string result;
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                result = (from c in context.tbl_EmployeeApproval
                          where c.DomainID == domainId && c.TempEmployeeCode.ToUpper().StartsWith("TMP_")
                          orderby c.CreatedOn descending
                          select c.TempEmployeeCode).FirstOrDefault();
            }

            if (result != null)
            {
                result = result.ToUpper().Split('_')[0] + "_" + (Convert.ToInt32(result.ToUpper().Split('_')[1]) + 1);
            }
            else
            {
                result = Constants.Default_Temp_EmployeeCode;
            }

            return result;
        }

        // Generate Employee Code Based On Base Location
        public string GenerateEmployeeCode(int baseLocationId)
        {
            string employeeCode;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GENERATE_EMPLOYEECODE);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, baseLocationId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                employeeCode = Database.ExecuteScalar(dbCommand).ToString();
            }

            return employeeCode;
        }

        #endregion Generate Employee Code

        public List<SelectListItem> GetBankNameBasedonPaymentMode(string paymentMode)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_BANKBASEDONPAYMENTMODE);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, "PaymentMode", DbType.String, paymentMode);

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #region Employee Details

        public EmployeeMasterBo GetEmployeeDetails(int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                var result = (from e in context.tbl_EmployeeMaster
                              where e.ID == employeeId
                              select new EmployeeMasterBo
                              {
                                  Name = e.FullName,
                                  FullName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                  Code = e.Code ?? "",
                                  EmpCodePattern = e.EmpCodePattern ?? "",

                              }).FirstOrDefault();


                return result;
            }
        }

        #endregion Employee Details

        #region Application Role

        public bool HasApplicationRole(int employeeId, string applicationRole)
        {
            bool HasApplicationRole = false;
            using (FSMEntities context = new FSMEntities())
            {
                var domainId = Utility.DomainId();

                var applicationRoleID = context.tbl_ApplicationRole.Where(ar => !ar.IsDeleted && ar.DomainID == domainId && ar.Name.Replace(" ", "").Contains(applicationRole.Replace(" ", ""))).Select(e => e.ID).FirstOrDefault();
                var result = context.tbl_EmployeeOtherDetails.Where(od => !od.IsDeleted && od.DomainID == domainId && od.EmployeeID == employeeId && od.ApplicationRoleID == applicationRoleID).Select(e => e.ID).Count();

                if (result > 0)
                {
                    HasApplicationRole = true;
                }
            }
            return HasApplicationRole;
        }

        #endregion Application Role

        #region Other User Access

        public bool HasOtherUserAccess(int employeeId, string screenName)
        {
            bool HasOtherUserAccess = false;
            int rowCount = 0;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_OTHERUSERACCESS);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.ScreenName, DbType.String, screenName);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {

                        rowCount = Utility.CheckDbNull<int>(reader[DBParam.Output.Count]);
                    }
                }
                if (rowCount > 0)
                {
                    HasOtherUserAccess = true;
                }
            }

            return HasOtherUserAccess;
        }

        #endregion Other User Access

        #region Application Config Value

        public string GetApplicationConfigValue(string code, string businessUnit, int userId)
        {
            GetApplicationConfiguration getApplicationConfiguration = new GetApplicationConfiguration();
            var userMessage = getApplicationConfiguration.GetApplicationConfigValue(code, businessUnit, userId);

            return userMessage;
        }

        #endregion Application Config Value
    }
}