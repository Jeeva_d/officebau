﻿using cvt.officebau.com.MessageConstants;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class BrsRepository : BaseRepository
    {
        #region Manage BRS

        public string ManageBrs(Brsbo brsBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_BRS);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, brsBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.Date, DbType.DateTime, brsBo.ReconsiledDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Reconsile, DbType.Boolean, brsBo.IsReconciled);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                brsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return brsBo.UserMessage;
        }

        #endregion Manage BRS

        #region Manage Reconciliation

        public string ManageReconciliation(Brsbo brsBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_RECONCILIATION);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                brsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return brsBo.UserMessage;
        }

        #endregion Manage Reconciliation

        public Brsbo GetSourceScreen(int id, string sourceType)
        {
            Brsbo brsBo = new Brsbo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHSOURCESCREEN);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.SourceType, DbType.String, sourceType);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        brsBo = new Brsbo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        };
                    }
                }
            }

            return brsBo;
        }

        #region Search BRS

        public List<Brsbo> SearchBrsList(SearchParamsBo searchParamsBo)
        {
            List<Brsbo> brsBoList = new List<Brsbo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_BRS);

                Database.AddInParameter(dbCommand, DBParam.Input.BankID, DbType.Int32, searchParamsBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.IsReconsiled, DbType.Boolean, searchParamsBo.IsReconciled);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount == "" ? 0 : Convert.ToDecimal(searchParamsBo.Amount));

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        brsBoList.Add(
                            new Brsbo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName]),
                                Source = Utility.CheckDbNull<string>(reader[DBParam.Output.SourceName]),
                                SourceId = Utility.CheckDbNull<int>(reader[DBParam.Output.SourceID]),
                                Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.SDATE]),
                                ReconsiledDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ReconsiledDate]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                ClosingAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ClosingAmount]),
                                IsReconciled = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsReconsiled]),
                                Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description])
                            });
                    }
                }
            }

            return brsBoList;
        }

        public string GetBankAccountNumber(int? id)
        {
            string bankName = string.Empty;
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_BANKACCOUNTNUMBER);
                Database.AddInParameter(dbCommand, DBParam.Input.BankID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        bankName = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName]);
                    }
                }
            }

            return bankName;
        }

        [HttpPost]
        public string DeleteBRS(Brsbo brsbo)
        {

            using (var context = new Repository.FSMEntities())
            {
                context.Database.ExecuteSqlCommand("TRUNCATE TABLE [tblBRS]");
            }
            return BOConstants.Successfully_Cleared;
        }
        #endregion Search BRS
    }
}