﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class BusinessUnitRepository : BaseRepository
    {
        public List<BusinessUnitBo> SearchBusinessUnit(BusinessUnitBo businessUnitBo)
        {
            List<BusinessUnitBo> businessBoList = new List<BusinessUnitBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_BUSINESSUNIT);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        businessBoList.Add(
                            new BusinessUnitBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                                Address = Utility.CheckDbNull<string>(reader[DBParam.Output.Address]),
                                BusinessUnit = Utility.CheckDbNull<int>(reader[DBParam.Output.ParentID]),
                                BusinessUnitName = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit])
                            });
                    }
                }

                return businessBoList;
            }
        }

        public BusinessUnitBo GetBusinessUnit(int id)
        {
            BusinessUnitBo businessUnitBo = new BusinessUnitBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_BUSINESSUNIT);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        businessUnitBo = new BusinessUnitBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            BusinessUnit = Utility.CheckDbNull<int>(reader[DBParam.Output.ParentID]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            Code = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Address = Utility.CheckDbNull<string>(reader[DBParam.Output.Address]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            BusinessUnitName = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation])
                        };
                    }
                }
            }

            return businessUnitBo;
        }

        public string ManageBusinessUnit(BusinessUnitBo businessUnitBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_BUSINESSUNIT);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, businessUnitBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.ParentID, DbType.Int32, businessUnitBo.BusinessUnit);
                Database.AddInParameter(dbCommand, DBParam.Input.Address, DbType.String, businessUnitBo.Address);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, businessUnitBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, businessUnitBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, businessUnitBo.IsDeleted);
                businessUnitBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return businessUnitBo.UserMessage;
        }
    }
}