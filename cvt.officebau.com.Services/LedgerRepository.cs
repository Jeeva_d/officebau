﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class LedgerRepository : BaseRepository
    {
        #region Search Ledger

        public List<LedgerBo> SearchLedger(LedgerBo ledgerBo)
        {
            List<LedgerBo> ledgerBoList = new List<LedgerBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_LEDGER);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ledgerBoList.Add(
                            new LedgerBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                Group = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.GroupName])
                                },
                                IsExpense = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsExpense]),
                                OpeningBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OpeningBalance]),
                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                                LedgerType = Utility.CheckDbNull<string>(reader[DBParam.Output.LedgerType]),
                                Cgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                                Sgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                                Igst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.IGST])
                            });
                    }
                }
            }

            return ledgerBoList;
        }

        #endregion Search Ledger

        #region Get Ledger

        public LedgerBo GetLedger(int ledgerId)
        {
            LedgerBo ledgerBo = new LedgerBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_LEDGER);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, ledgerId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ledgerBo = new LedgerBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Group = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.GroupID])
                            },
                            IsExpense = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsExpense]),
                            OpeningBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OpeningBalance]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            LedgerType = Utility.CheckDbNull<string>(reader[DBParam.Output.LedgerType]),
                            HsnCode = Utility.CheckDbNull<string>(reader[DBParam.Output.HsnCode]),
                            Sgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            Cgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            Igst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.IGST]),
                            Hsnid = Utility.CheckDbNull<int>(reader[DBParam.Output.HSNID]),
                            IsActive = Utility.CheckDbNull<bool>(reader["ISdefault"])
                        };
                    }
                }
            }

            return ledgerBo;
        }

        #endregion Get Ledger

        #region Manage Ledger

        public string ManageLedger(LedgerBo ledgerBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_LEDGER);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, ledgerBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, ledgerBo.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, ledgerBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.GroupID, DbType.Int32, ledgerBo.Group?.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.OpeningBalance, DbType.Decimal, ledgerBo.OpeningBalance);
                Database.AddInParameter(dbCommand, DBParam.Input.IsExpense, DbType.Boolean, ledgerBo.IsExpense);
                Database.AddInParameter(dbCommand, DBParam.Input.LedgerType, DbType.String, ledgerBo.LedgerType);
                Database.AddInParameter(dbCommand, DBParam.Input.HsnID, DbType.String, ledgerBo.Hsnid);
                Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, ledgerBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                ledgerBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return ledgerBo.UserMessage;
        }

        #endregion Manage Ledger
    }
}