﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Services
{
    public class AttendanceRepository : BaseRepository
    {
        #region LOP Details

        public DataTable ExportLopDetails(int month, int year, string location)
        {
            DataSet ds = new DataSet();
            DataTable dtAttendance = new DataTable("ExportLOPDetails");
            ds.Tables.Add(dtAttendance);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORT_LOPDETAILS);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, month);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, year);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, location);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtAttendance);
                }

                return dtAttendance;
            }
        }

        public DataTable ExportLopDetails_V2(int month, int year, string location)
        {
            DataSet ds = new DataSet();
            DataTable dtAttendance = new DataTable("ExportLOPDetails");
            ds.Tables.Add(dtAttendance);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORT_LOPDETAILS_V2);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, month);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, year);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, location);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtAttendance);
                }

                return dtAttendance;
            }
        }

        #endregion LOP Details

        #region Employee Attendance

        public List<AttendanceBo> SearchDailyAttendance(AttendanceBo attendanceBo)
        {
            List<AttendanceBo> attendanceList = new List<AttendanceBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHATTENDANCE);

                Database.AddInParameter(dbCommand, DBParam.Input.CurrentDate, DbType.Date, attendanceBo.AttendanceDate);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        attendanceBo = new AttendanceBo
                        {
                            AttendanceId = Utility.CheckDbNull<long>(reader[DBParam.Output.AttendanceID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Code]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            AttendanceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.AttendanceDate]),
                            Punches = Utility.CheckDbNull<string>(reader[DBParam.Output.Punches]) == null ? "" : Utility.CheckDbNull<string>(reader[DBParam.Output.Punches]).Replace(",,", ","),
                            PunchTime = Utility.CheckDbNull<string>(reader[DBParam.Output.PunchTime]),
                            IsManual = Utility.CheckDbNull<string>(reader[DBParam.Output.IsManual])
                        };
                        attendanceList.Add(attendanceBo);
                    }
                }
            }

            return attendanceList;
        }

        public string ManageDailyAttendance(AttendanceBo attendance)
        {
            using (Database.CreateConnection())
            {
                try
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGEATTENDANCE);
                    Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, attendance.AttendanceId);
                    Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, attendance.EmployeeId);
                    Database.AddInParameter(dbCommand, DBParam.Input.LogDate, DbType.Date, attendance.LogDate);
                    Database.AddInParameter(dbCommand, DBParam.Input.PunchType, DbType.String, attendance.Punches);
                    Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    string outputMessage = Database.ExecuteScalar(dbCommand).ToString();
                    return outputMessage;
                }
                catch (Exception)
                {
                    return BOConstants.ValidPunches;
                }
            }
        }

        public DataTable SearchMonthlyAttendance(AttendanceBo attendanceBo)
        {
            DataSet ds = new DataSet();
            DataTable dtAttendance = new DataTable("Monthlyattedancereport");
            ds.Tables.Add(dtAttendance);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MONTHLY_ATTEDANCEREPORT);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, attendanceBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, attendanceBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.RegionIDs, DbType.String, attendanceBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtAttendance);
                }

                return dtAttendance;
            }
        }

        public string CheckBusinessHoursBasedOnBusinessUnit(int? businessUnitId)
        {
            bool result = false;
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                if (businessUnitId == null)
                {
                    return false.ToString();
                }

                List< cvt.officebau.com.ViewModels.Entity> queryResult = (from tbu in context.tbl_BusinessUnit
                                               join tbh in context.tbl_BusinessHours on tbu.ParentID equals tbh.RegionID
                                               where tbh.DomainID == domainId && tbu.ID == businessUnitId && !tbh.IsDeleted
                                               select new  cvt.officebau.com.ViewModels.Entity
                                               {
                                                   Name = tbu.Name,
                                                   Id = tbu.ID
                                               }).ToList();

                if (queryResult.Count > 0)
                {
                    result = true;
                }
            }

            return result.ToString();
        }

        #endregion Employee Attendance

        #region My Attendance

        public DataTable SearchEmployeeAttendance(AttendanceBo attendanceBo)
        {
            DataSet ds = new DataSet();
            DataTable dtAttendance = new DataTable("Employeeattedancereport");
            ds.Tables.Add(dtAttendance);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EMPLOYEE_ATTEDANCEREPORT);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, attendanceBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, attendanceBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.String, attendanceBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtAttendance);
                }

                return dtAttendance;
            }
        }

        public List<AttendanceBo> GetMyAttendance(int employeeId, DateTime logDate)
        {
            List<AttendanceBo> attendanceList = new List<AttendanceBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand;
                if (employeeId == 0)
                {
                    dbCommand = Database.GetStoredProcCommand(Constants.SEARCHMYATTENDANCE);
                    Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                }
                else
                {
                    dbCommand = Database.GetStoredProcCommand(Constants.SEARCHMYATTENDANCE);
                    Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                }

                Database.AddInParameter(dbCommand, DBParam.Input.LogDate, DbType.DateTime, logDate);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        AttendanceBo attendanceBo = new AttendanceBo
                        {
                            PunchTime = reader[DBParam.Output.PunchTime].ToString(),
                            UserMessage = reader[DBParam.Output.Description].ToString()
                        };
                        attendanceList.Add(attendanceBo);
                    }
                }
            }

            return attendanceList;
        }

        public string CheckBusinessHoursBasedOnEmployeeId(int employeeId)
        {
            bool result = false;
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List< cvt.officebau.com.ViewModels.Entity> queryResult = (from tem in context.tbl_EmployeeMaster
                                               join tbh in context.tbl_BusinessHours on tem.RegionID equals tbh.RegionID
                                               where tem.DomainID == domainId && tem.ID == employeeId && !tbh.IsDeleted
                                               select new  cvt.officebau.com.ViewModels.Entity
                                               {
                                                   Name = tem.FullName,
                                                   Id = tem.ID
                                               }).ToList();

                if (queryResult.Count > 0)
                {
                    result = true;
                }
            }

            return result.ToString();
        }

        #endregion My Attendance

        #region Biometric Synchronize

        public string Synchronization(AttendanceBo attendanceBo)
        {
            DataTable dt;
            try
            {
                dt = GetBiometricAttendanceDetails(attendanceBo);
            }
            catch (Exception ex)
            {
                return ex.Message.ToLower().Contains("invalid") ? "No records found for the selected month and year." : "Please check the server";
            }

            if (dt.Rows.Count > 0)
            {
                string consString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        sqlBulkCopy.DestinationTableName = Constants.Table_Attendance_Process;
                        sqlBulkCopy.ColumnMappings.Add(DBParam.Output.EmployeeID, DBParam.Output.EmployeeID);
                        sqlBulkCopy.ColumnMappings.Add(DBParam.Output.AttendanceDate, DBParam.Output.AttendanceDate);
                        sqlBulkCopy.ColumnMappings.Add(DBParam.Output.InTime, DBParam.Output.InTime);
                        sqlBulkCopy.ColumnMappings.Add(DBParam.Output.OutTime, DBParam.Output.OutTime);
                        sqlBulkCopy.ColumnMappings.Add(DBParam.Output.Duration, DBParam.Output.Duration);
                        sqlBulkCopy.ColumnMappings.Add(DBParam.Output.PunchRecords, DBParam.Output.PunchRecords);
                        con.Open();
                        sqlBulkCopy.WriteToServer(dt);
                        con.Close();
                            using (Database.CreateConnection())
                            {
                                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.BIOMETRICSYNCHRONIZE);
                                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, attendanceBo.MonthId);
                                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, attendanceBo.YearId);
                                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                                attendanceBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
                            }
                    }
                }
            }
            else
            {
                attendanceBo.UserMessage = BOConstants.No_Records_Found;
            }

            return attendanceBo.UserMessage;
        }

        public DataTable GetBiometricAttendanceDetails(AttendanceBo attendanceBo)
        {
            DataTable dt = new DataTable();
            string consString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

            using (SqlConnection con = new SqlConnection(consString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.GET_BIOMETRICATTENDANCEDETAILS, con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue(DBParam.Input.MonthID, attendanceBo.MonthId);
                        cmd.Parameters.AddWithValue(DBParam.Input.YearID, attendanceBo.YearId);
                        cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                        da.Fill(dt);
                        con.Close();
                    }
                }
            }

            return dt;
        }

        #endregion Biometric Synchronize
    }
}