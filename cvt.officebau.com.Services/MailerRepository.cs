﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class MailerRepository : BaseRepository
    {
        #region Mail Receipts

        public List<MailerBo> SearchCcReceipts(MailerBo mailerBo)
        {
            List<MailerBo> mailerBoList = new List<MailerBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchCCrecpts");

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, mailerBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        MailerBo mailBo = new MailerBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EventId = Utility.CheckDbNull<int>(reader[DBParam.Output.eventID]),
                            Event = Utility.CheckDbNull<string>(reader[DBParam.Output.eventName]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            CCrecpts = Utility.CheckDbNull<string>(reader[DBParam.Output.CCrecpts]),
                            BusinessUnitId = Utility.CheckDbNull<int>(reader[DBParam.Output.BusinessUnitID])
                        };
                        mailerBoList.Add(mailBo);
                    }
                }
            }

            return mailerBoList;
        }

        public MailerBo GetCcReceipts(int id)
        {
            MailerBo mailerBo = new MailerBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetCCrecpts");

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        mailerBo.Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]);
                        mailerBo.BusinessUnitId = Utility.CheckDbNull<int>(reader[DBParam.Output.BusinessUnitID]);
                        mailerBo.CCrecpts = Utility.CheckDbNull<string>(reader[DBParam.Output.CCrecpts]);
                        mailerBo.EventId = Utility.CheckDbNull<int>(reader[DBParam.Output.eventID]);
                        mailerBo.ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]);
                        mailerBo.ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]);
                    }
                }
            }

            return mailerBo;
        }

        public string ManageCcReceipts(MailerBo mailerBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Manageccrecpts");
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, mailerBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.eventID, DbType.Int32, mailerBo.EventId);
                Database.AddInParameter(dbCommand, DBParam.Input.CCrecpts, DbType.String, mailerBo.CCrecpts);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, mailerBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, mailerBo.IsDeleted);
                mailerBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return mailerBo.UserMessage;
        }

        #endregion Mail Receipts
    }
}