﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Threading;
using System.Web.Configuration;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class CommonRepository : BaseRepository
    {
        public List<SelectListItem> SearchDependantDropDown(int id, string mainTable, string type, string dependant)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_DEPENDANTDROPDOWN);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DependantTable, DbType.String, dependant);
                Database.AddInParameter(dbCommand, DBParam.Input.MainTable, DbType.String, mainTable);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        public List<SelectListItem> SearchAutoComplete(string tableName, string columnName, string searchParameter)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_AUTOCOMPLETE);

                Database.AddInParameter(dbCommand, DBParam.Input.TableName, DbType.String, tableName);
                Database.AddInParameter(dbCommand, DBParam.Input.columnName, DbType.String, columnName);
                Database.AddInParameter(dbCommand, DBParam.Input.SearchParameter, DbType.String, searchParameter);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        public List<CustomBo> SearchAutoCompleteParameter(string tableName, string columnName, string searchParameter)
        {
            List<CustomBo> dependantDropDownList = new List<CustomBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_AUTOCOMPLETEPARAMETER);

                Database.AddInParameter(dbCommand, DBParam.Input.TableName, DbType.String, tableName);
                Database.AddInParameter(dbCommand, DBParam.Input.columnName, DbType.String, columnName);
                Database.AddInParameter(dbCommand, DBParam.Input.SearchParameter, DbType.String, searchParameter);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new CustomBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Parameter = Utility.CheckDbNull<string>(reader[DBParam.Output.Currency]),
                            Parameter1 = Utility.CheckDbNull<string>(reader[DBParam.Output.Address]),
                            Parameter2 = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms]),
                            Parameter3 = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo]),
                            Latitude = Utility.CheckDbNull<string>(reader["SOCount"]),
                            Designation = Utility.CheckDbNull<string>(reader["CurrencyCode"])

                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #region Common Functions

        public DateTime GetFinancialYear(int startMonth)
        {
            CultureInfo customCulture = new CultureInfo("en-US", true) { DateTimeFormat = { ShortDatePattern = "MM/dd/yyyy" } };

            Thread.CurrentThread.CurrentCulture = customCulture;
            Thread.CurrentThread.CurrentUICulture = customCulture;

            int year = DateTime.Now.Year;

            if (DateTime.Now.Month < startMonth)
            {
                year = year - 1;
            }

            return new DateTime(year, startMonth, 1);
        }

        #endregion Common Functions

        public string ManageEmail_InsertintoProcessorTable(string to, string cc, string bcc, string content, string subject, string screenName, string fileId)
        {
            string result;
            using (DbConnection dbCon = Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Email_InsertintoProcessorTable");
                dbCon.Open();
                Database.AddInParameter(dbCommand, "@ToID", DbType.String, to);
                Database.AddInParameter(dbCommand, "@cc", DbType.String, cc);
                Database.AddInParameter(dbCommand, "@BCC", DbType.String, bcc);
                Database.AddInParameter(dbCommand, "@FromID", DbType.String, WebConfigurationManager.AppSettings["EmailAddress"]);
                Database.AddInParameter(dbCommand, "@content", DbType.String, content);
                Database.AddInParameter(dbCommand, "@Subject", DbType.String, subject);
                Database.AddInParameter(dbCommand, DBParam.Input.ScreenType, DbType.String, screenName);
                Database.AddInParameter(dbCommand, DBParam.Input.FileID, DbType.String, fileId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.String, Utility.DomainId());
                result = Database.ExecuteScalar(dbCommand).ToString();
                dbCon.Close();
            }

            return result;
        }

        #region Get Search History Parameter Value

        public object GetSearchHistoryParameterValue(string formName, string Event, string searchParameter, object value)
        {
            string outputValue = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DML_SEARCHHISTORY);

                Database.AddInParameter(dbCommand, DBParam.Input.FormName, DbType.String, formName);
                Database.AddInParameter(dbCommand, DBParam.Input.Event, DbType.String, Event);
                Database.AddInParameter(dbCommand, DBParam.Input.SearchParameter, DbType.String, searchParameter);
                Database.AddInParameter(dbCommand, DBParam.Input.Value, DbType.String, value);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        outputValue = Utility.CheckDbNull<string>(reader[DBParam.Output.Value]);
                    }
                }
            }

            return outputValue;
        }

        public void GetSearchDataHistory(SearchParamsBo searchParamsBo, string Event, string formName, int financialYearStartMonth)
        {
            object bankId, costCenterId;
            DateTime financialYear = GetFinancialYear(financialYearStartMonth);

            if (searchParamsBo.StartDate == null)
            {
                searchParamsBo.StartDate = financialYear;
            }

            if (searchParamsBo.EndDate == null)
            {
                searchParamsBo.EndDate = searchParamsBo.StartDate.Value.AddDays(365 - 1);
            }

            searchParamsBo.StartDate = Convert.ToDateTime(GetSearchHistoryParameterValue(formName, Event, "StartDate", searchParamsBo.StartDate));
            searchParamsBo.EndDate = Convert.ToDateTime(GetSearchHistoryParameterValue(formName, Event, "EndDate", searchParamsBo.EndDate));

            switch (formName.ToLower())
            {
                case "transfer":
                    bankId = GetSearchHistoryParameterValue(formName, Event, "BankID", searchParamsBo.Id);
                    searchParamsBo.Id = bankId == null ? 0 : Convert.ToInt32(bankId);
                    searchParamsBo.Bank = new  cvt.officebau.com.ViewModels.Entity { Id = searchParamsBo.Id };
                    searchParamsBo.Notations = (string)GetSearchHistoryParameterValue(formName, Event, "Notations", searchParamsBo.Notations);
                    searchParamsBo.Amount = (string)GetSearchHistoryParameterValue(formName, Event, "Amount", searchParamsBo.Amount);
                    break;

                case "ledgerdetails":
                    searchParamsBo.LedgerName = (string)GetSearchHistoryParameterValue(formName, Event, "LedgerName", searchParamsBo.LedgerName);
                    break;

                case "vendorstatement":
                    searchParamsBo.VendorName = (string)GetSearchHistoryParameterValue(formName, Event, "VendorName", searchParamsBo.VendorName);
                    break;

                case "customerstatement":
                    searchParamsBo.VendorName = (string)GetSearchHistoryParameterValue(formName, Event, "CustomerName", searchParamsBo.VendorName);
                    break;

                case "daybookbybank":
                    bankId = GetSearchHistoryParameterValue(formName, Event, "BankID", searchParamsBo.Id);
                    searchParamsBo.Id = bankId == null ? 0 : Convert.ToInt32(bankId);
                    searchParamsBo.Bank = new  cvt.officebau.com.ViewModels.Entity { Id = searchParamsBo.Id };
                    break;

                case "expense":
                    searchParamsBo.VendorName = (string)GetSearchHistoryParameterValue(formName, Event, "VendorName", searchParamsBo.VendorName);
                    searchParamsBo.Status = (string)GetSearchHistoryParameterValue(formName, Event, "StatusType", searchParamsBo.Status);
                    searchParamsBo.StatusType = new  cvt.officebau.com.ViewModels.Entity { Name = searchParamsBo.Status };
                    searchParamsBo.ScreenType = (string)GetSearchHistoryParameterValue(formName, Event, "Type", searchParamsBo.ScreenType);
                    searchParamsBo.Type = new  cvt.officebau.com.ViewModels.Entity { Name = searchParamsBo.ScreenType };
                    searchParamsBo.Notations = (string)GetSearchHistoryParameterValue(formName, Event, "Notations", searchParamsBo.Notations);
                    searchParamsBo.Amount = (string)GetSearchHistoryParameterValue(formName, Event, "Amount", searchParamsBo.Amount);
                    costCenterId = GetSearchHistoryParameterValue(formName, Event, "CostCenterID", searchParamsBo.CostCenterId);
                    searchParamsBo.CostCenter = new  cvt.officebau.com.ViewModels.Entity { Id = costCenterId == null ? 0 : Convert.ToInt32(costCenterId) };
                    searchParamsBo.LedgerName = (string)GetSearchHistoryParameterValue(formName, Event, "LedgerName", searchParamsBo.LedgerName);
                    break;

                case "income":
                    searchParamsBo.CustomerName = (string)GetSearchHistoryParameterValue(formName, Event, "CustomerName", searchParamsBo.CustomerName);
                    searchParamsBo.Status = (string)GetSearchHistoryParameterValue(formName, Event, "StatusType", searchParamsBo.Status);
                    searchParamsBo.StatusType = new  cvt.officebau.com.ViewModels.Entity { Name = searchParamsBo.Status };
                    searchParamsBo.Notations = (string)GetSearchHistoryParameterValue(formName, Event, "Notations", searchParamsBo.Notations);
                    searchParamsBo.Amount = (string)GetSearchHistoryParameterValue(formName, Event, "Amount", searchParamsBo.Amount);
                    costCenterId = GetSearchHistoryParameterValue(formName, Event, "CostCenterID", searchParamsBo.CostCenterId);
                    searchParamsBo.CostCenter = new  cvt.officebau.com.ViewModels.Entity { Id = costCenterId == null ? 0 : Convert.ToInt32(costCenterId) };
                    break;

                case "accountpayable":
                    searchParamsBo.VendorName = (string)GetSearchHistoryParameterValue(formName, Event, "VendorName", searchParamsBo.VendorName);
                    searchParamsBo.Status = (string)GetSearchHistoryParameterValue(formName, Event, "StatusType", searchParamsBo.Status);
                    searchParamsBo.StatusType = new  cvt.officebau.com.ViewModels.Entity { Name = searchParamsBo.Status };
                    searchParamsBo.Notations = (string)GetSearchHistoryParameterValue(formName, Event, "Notations", searchParamsBo.Notations);
                    searchParamsBo.Amount = (string)GetSearchHistoryParameterValue(formName, Event, "Amount", searchParamsBo.Amount);
                    searchParamsBo.LedgerName = (string)GetSearchHistoryParameterValue(formName, Event, "LedgerName", searchParamsBo.LedgerName);
                    break;

                case "accountreceivable":
                    searchParamsBo.CustomerName = (string)GetSearchHistoryParameterValue(formName, Event, "CustomerName", searchParamsBo.CustomerName);
                    searchParamsBo.Status = (string)GetSearchHistoryParameterValue(formName, Event, "StatusType", searchParamsBo.Status);
                    searchParamsBo.StatusType = new  cvt.officebau.com.ViewModels.Entity { Name = searchParamsBo.Status };
                    searchParamsBo.Notations = (string)GetSearchHistoryParameterValue(formName, Event, "Notations", searchParamsBo.Notations);
                    searchParamsBo.Amount = (string)GetSearchHistoryParameterValue(formName, Event, "Amount", searchParamsBo.Amount);
                    break;

                case "receipts":
                case "payments":
                    searchParamsBo.PartyName = (string)GetSearchHistoryParameterValue(formName, Event, "PartyName", searchParamsBo.PartyName);
                    searchParamsBo.Notations = (string)GetSearchHistoryParameterValue(formName, Event, "Notations", searchParamsBo.Notations);
                    searchParamsBo.Amount = (string)GetSearchHistoryParameterValue(formName, Event, "Amount", searchParamsBo.Amount);
                    searchParamsBo.LedgerName = (string)GetSearchHistoryParameterValue(formName, Event, "LedgerName", searchParamsBo.LedgerName);
                    break;

                case "pandl":
                    costCenterId = GetSearchHistoryParameterValue(formName, Event, "CostCenterID", searchParamsBo.CostCenterId);
                    searchParamsBo.CostCenter = new  cvt.officebau.com.ViewModels.Entity { Id = costCenterId == null ? 0 : Convert.ToInt32(costCenterId) };
                    break;

                case "trailbalance":
                    searchParamsBo.FinancialYear = (string)GetSearchHistoryParameterValue(formName, Event, "FinancialYear", searchParamsBo.FinancialYear);
                    searchParamsBo.FinancialYearType = new  cvt.officebau.com.ViewModels.Entity { Name = searchParamsBo.FinancialYear };
                    break;

                case "brs":
                    bankId = GetSearchHistoryParameterValue(formName, Event, "BankID", searchParamsBo.Id);
                    searchParamsBo.Id = bankId == null ? 0 : Convert.ToInt32(bankId);
                    searchParamsBo.Bank = new  cvt.officebau.com.ViewModels.Entity { Id = searchParamsBo.Id };
                    searchParamsBo.Notations = (string)GetSearchHistoryParameterValue(formName, Event, "Notations", searchParamsBo.Notations);
                    searchParamsBo.Amount = (string)GetSearchHistoryParameterValue(formName, Event, "Amount", searchParamsBo.Amount);
                    searchParamsBo.IsReconciled = Convert.ToBoolean(GetSearchHistoryParameterValue(formName, Event, "IsReconciled", searchParamsBo.IsReconciled));
                    break;

                case "openinvoice":
                    searchParamsBo.CustomerName = (string)GetSearchHistoryParameterValue(formName, Event, "CustomerName", searchParamsBo.CustomerName);
                    break;
            }
        }

        #endregion Get Search History Parameter Value
    }
}