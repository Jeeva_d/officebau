﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class TdsDeclarationRepository : BaseRepository
    {
        #region MANAGE

        public string ManageTdsDeclaration(List<TdsDeclarationBo> declarationBoList)
        {
            string userMessage = string.Empty;

            using (DbConnection dbCon = Database.CreateConnection())
            {
                foreach (TdsDeclarationBo declarationBo in declarationBoList)
                {
                    declarationBo.IsApprover = false;

                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_TDSDECLARATION);
                    Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, declarationBo.Id);
                    Database.AddInParameter(dbCommand, DBParam.Input.ComponentID, DbType.Int32, declarationBo.ComponentId);
                    Database.AddInParameter(dbCommand, DBParam.Input.Declaration, DbType.Currency, declarationBo.Declaration);
                    Database.AddInParameter(dbCommand, DBParam.Input.Submitted, DbType.Currency, declarationBo.Submitted);
                    Database.AddInParameter(dbCommand, DBParam.Input.Cleared, DbType.Currency, declarationBo.Cleared);
                    Database.AddInParameter(dbCommand, DBParam.Input.Rejected, DbType.Currency, declarationBo.Rejected);
                    Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, declarationBo.Description);
                    Database.AddInParameter(dbCommand, DBParam.Input.ApproverRemarks, DbType.String, declarationBo.ApproverRemarks);
                    Database.AddInParameter(dbCommand, DBParam.Input.IsApprover, DbType.Boolean, declarationBo.IsApprover);
                    Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, declarationBo.FinancialYearId);
                    Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, declarationBo.HasDeleted);
                    Database.AddInParameter(dbCommand, DBParam.Input.SessionEmployeeID, DbType.Int32, declarationBo.EmployeeId != 0 ? declarationBo.EmployeeId : Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    userMessage = Database.ExecuteScalar(dbCommand).ToString();
                }
            }

            return userMessage;
        }

        public string ProcessTdsDeclaration(List<TdsDeclarationBo> declarationBoList)
        {
            string userMessage = string.Empty;

            using (DbConnection dbCon = Database.CreateConnection())
            {
                foreach (TdsDeclarationBo declarationBo in declarationBoList)
                {
                    declarationBo.IsApprover = true;

                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_TDSDECLARATION);
                    dbCon.Open();
                    Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, declarationBo.Id);
                    Database.AddInParameter(dbCommand, DBParam.Input.ComponentID, DbType.Int32, declarationBo.ComponentId);
                    Database.AddInParameter(dbCommand, DBParam.Input.Declaration, DbType.Currency, declarationBo.Declaration);
                    Database.AddInParameter(dbCommand, DBParam.Input.Submitted, DbType.Currency, declarationBo.Submitted);
                    Database.AddInParameter(dbCommand, DBParam.Input.Cleared, DbType.Currency, declarationBo.Cleared);
                    Database.AddInParameter(dbCommand, DBParam.Input.Rejected, DbType.Currency, declarationBo.Rejected);
                    Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, declarationBo.Description);
                    Database.AddInParameter(dbCommand, DBParam.Input.ApproverRemarks, DbType.String, declarationBo.ApproverRemarks);
                    Database.AddInParameter(dbCommand, DBParam.Input.IsApprover, DbType.Boolean, declarationBo.IsApprover);
                    Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, declarationBo.FinancialYearId);
                    Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, declarationBo.HasDeleted);
                    Database.AddInParameter(dbCommand, DBParam.Input.SessionEmployeeID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    userMessage = Database.ExecuteScalar(dbCommand).ToString();
                }
            }

            return userMessage;
        }

        /// <summary>
        ///     To Declare House Tax
        /// </summary>
        /// <param name="declarationBo"></param>
        /// <returns>Success or Error Message</returns>
        public string ManageTdsHouseTax(TdsDeclarationBo declarationBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_TDSHOUSETAX);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, declarationBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.Component, DbType.String, declarationBo.Component);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, declarationBo.IncomeType);
                Database.AddInParameter(dbCommand, DBParam.Input.Declaration, DbType.Currency, declarationBo.Declaration);
                Database.AddInParameter(dbCommand, DBParam.Input.Submitted, DbType.Currency, declarationBo.Submitted);
                Database.AddInParameter(dbCommand, DBParam.Input.Cleared, DbType.Currency, declarationBo.Cleared);
                Database.AddInParameter(dbCommand, DBParam.Input.Rejected, DbType.Currency, declarationBo.Rejected);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, declarationBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverRemarks, DbType.String, declarationBo.ApproverRemarks);
                Database.AddInParameter(dbCommand, DBParam.Input.IsApprover, DbType.Boolean, declarationBo.IsApprover);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, declarationBo.FinancialYearId);
                Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, declarationBo.HasDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionEmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                declarationBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return declarationBo.UserMessage;
        }

        public string ManageTdsConfiguration(List<TdsDeclarationBo> configurationList, int? regionId)
        {
            TdsDeclarationBo configurationBo = new TdsDeclarationBo();
            using (DbConnection dbCon = Database.CreateConnection())
            {
                foreach (TdsDeclarationBo item in configurationList)
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_TDSCONFIGURATION);
                    dbCon.Open();
                    Database.AddInParameter(dbCommand, DBParam.Input.Code, DbType.String, item.Code);
                    Database.AddInParameter(dbCommand, DBParam.Input.Value, DbType.String, item.Value);
                    Database.AddInParameter(dbCommand, DBParam.Input.RegionID, DbType.Int32, regionId);
                    Database.AddInParameter(dbCommand, DBParam.Input.SessionEmployeeID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    configurationBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
                    dbCon.Close();
                }
            }

            return configurationBo.UserMessage;
        }

        #endregion MANAGE

        #region SEARCH

        public List<TdsDeclarationBo> SearchTdsDeclaration(int employeeId, bool isApprover, int? startYear, string type = "")
        {
            List<TdsDeclarationBo> declarationList = new List<TdsDeclarationBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_TDSDECLARATION);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.IsApprover, DbType.Boolean, isApprover);
                Database.AddInParameter(dbCommand, DBParam.Input.StartYear, DbType.Int32, startYear);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        TdsDeclarationBo declarationBo = new TdsDeclarationBo
                        {
                            Id = Convert.ToInt32(reader[DBParam.Output.ID]),
                            ComponentId = Utility.CheckDbNull<int>(reader[DBParam.Output.ComponentID]),
                            Component = reader[DBParam.Output.Component].ToString(),
                            Declaration = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Declaration]),
                            Submitted = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Submitted]),
                            Cleared = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Cleared]),
                            Rejected = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rejected]),
                            Description = reader[DBParam.Output.Remarks].ToString(),
                            ApproverRemarks = reader[DBParam.Output.ApproverRemarks].ToString(),
                            IsCleared = Convert.ToBoolean(reader[DBParam.Output.IsCleared]),
                            IsOpen = Convert.ToBoolean(reader[DBParam.Output.IsOpen]),
                            IsProofOpen = Convert.ToBoolean(reader[DBParam.Output.IsProofOpen]),
                            IsCurrentFy = Convert.ToBoolean(reader[DBParam.Output.IsCurrentFY]),
                            IncomeTypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.TypeID]),
                            IncomeType = reader[DBParam.Output.Type].ToString(),
                            Section = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Convert.ToInt32(reader[DBParam.Output.SectionID]),
                                Name = reader[DBParam.Output.Section].ToString()
                            },
                            EmployeeId = employeeId
                        };
                        declarationList.Add(declarationBo);
                    }
                }
            }

            return declarationList;
        }

        /// <summary>
        ///     Employee List
        /// </summary>
        /// <param name="declarationBo"></param>
        /// <returns></returns>
        public List<TdsDeclarationBo> SearchEmployee(TdsDeclarationBo declarationBo)
        {
            List<TdsDeclarationBo> employeeList = new List<TdsDeclarationBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEETDSDECLARATION);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, declarationBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.StartYear, DbType.Int32, declarationBo.FinancialYearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, declarationBo.BusinessUnitId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        declarationBo = new TdsDeclarationBo
                        {
                            EmployeeId = Convert.ToInt32(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = reader[DBParam.Output.EmployeeName].ToString(),
                            EmployeeCode = reader[DBParam.Output.EmployeeCode].ToString(),
                            NoOfRecords = Convert.ToInt32(reader[DBParam.Output.NoOfRecords])
                        };
                        employeeList.Add(declarationBo);
                    }
                }
            }

            return employeeList;
        }

        #endregion SEARCH

        #region GET

        public TdsDeclarationBo GetTdsConfiguration(int regionId)
        {
            using (Database.CreateConnection())
            {
                TdsDeclarationBo configurationBo = new TdsDeclarationBo();

                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_TDSCONFIGURATION);
                Database.AddInParameter(dbCommand, DBParam.Input.RegionID, DbType.Int32, regionId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        if (reader["Code"].ToString() == "TDSMOD")
                        {
                            configurationBo.MonthOpenDate = Convert.ToInt32(reader["Value"]);
                        }

                        if (reader["Code"].ToString() == "TDSMCD")
                        {
                            configurationBo.MonthCloseDate = Convert.ToInt32(reader["Value"]);
                        }

                        if (reader["Code"].ToString() == "TDSPOD")
                        {
                            configurationBo.ProofOpenDate = Convert.ToDateTime(reader["Value"]);
                        }

                        if (reader["Code"].ToString() == "TDSPCD")
                        {
                            configurationBo.ProofCloseDate = Convert.ToDateTime(reader[DBParam.Output.Value]);
                            configurationBo.ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]);
                            configurationBo.ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]);
                            configurationBo.BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]);
                        }
                    }
                }

                return configurationBo;
            }
        }

        #endregion GET
    }
}