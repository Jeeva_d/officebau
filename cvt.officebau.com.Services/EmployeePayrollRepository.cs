﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;


namespace cvt.officebau.com.Services
{
    public class EmployeePayrollRepository : BaseRepository
    {
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString; //Anbu

        #region Employee Payroll

        public DataSet SearchEmployeePayroll(int monthId, int yearId, string businessUnitList, int? statusId, int? proceed, string screentype = "")
        {
            DataSet ds = new DataSet();
            DataTable dtPayroll = new DataTable("PayrollDatatable");
            ds.Tables.Add(dtPayroll);
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Pay_SearchEmployeePayroll", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.MonthID, monthId);
                    cmd.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.Location, businessUnitList);
                    cmd.Parameters.AddWithValue(DBParam.Input.StatusID, statusId ?? 9);
                    cmd.Parameters.AddWithValue(DBParam.Input.Proceed, proceed ?? 2);
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtPayroll);
                    }
                }
            }
            //if (screentype != "savepayroll")
            //    if (ds.Tables["PayrollDatatable"].Rows.Count > 0)
            //    {
            //        dtPayroll = Utility.SupressEmptyColumns(ds.Tables["PayrollDatatable"]);
            //        ds.Tables.RemoveAt(0);
            //        ds.Tables.Add(dtPayroll);
            //    }
            return ds;
        }

        public string SearchPayrollForTdsLop(int monthId, int yearId, string businessUnitList)
        {
            PayrollBO payrollBoList = new PayrollBO();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_SearchPayrollforTDSLOP");

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        payrollBoList.UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage]);
                    }
                }
            }

            return payrollBoList.UserMessage;
        }

        public string SavePayroll(int monthId, int yearId, string businessUnitList, int statusId)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand;
                if (statusId != 0)
                {
                    dbCommand = Database.GetStoredProcCommand("Pay_ManagePayrollActivity");
                    Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, statusId);
                }
                else
                {
                    dbCommand = Database.GetStoredProcCommand("Pay_ManageEmployeePayroll");
                }
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                return Database.ExecuteScalar(dbCommand).ToString();
            }
        }

        public List<EmployeePayrollBo_v2> GetPayrollDetails(int id, int employeePayStructureId, int monthId, int yearId)
        {
            List<EmployeePayrollBo_v2> companyPayStructureBo = new List<EmployeePayrollBo_v2>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_GetEmployeePayroll");

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeePayStructureID, DbType.Int32, employeePayStructureId);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        companyPayStructureBo.Add(new EmployeePayrollBo_v2()
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeePayStructureId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeePaystructureID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            ComponentId = Utility.CheckDbNull<int>(reader[DBParam.Output.ComponentId]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            IsEditable = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEditable]),
                            PayType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            ComponentCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Code]),
                            Note = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            ConfigValue = Utility.CheckDbNull<string>(reader[DBParam.Output.ConfigValue]),
                        });
                    }
                }
            }

            return companyPayStructureBo;
        }

        public string ManageSinglePayroll(List<EmployeePayrollBo_v2> list, int monthId, int yearId, int employeeId)
        {
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);
            SqlCommand cmd = new SqlCommand("Pay_ManageSinglePayroll")
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            }; con.Open();
            cmd.Parameters.AddWithValue("@Payroll", Utility.ToDataTable(list, "Payroll"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
            cmd.Parameters.AddWithValue(DBParam.Input.MonthID, monthId);
            cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, employeeId);
            cmd.Parameters.AddWithValue(DBParam.Input.Remarks, list[0].Note);

            return cmd.ExecuteScalar().ToString();
        }

        public string ManageRevertPayroll(PayrollBo_V2 payrollBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PAY_REVERTPAYROLL);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeName, DbType.String, payrollBo.EmployeeName);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, payrollBo.BusinessUnitIds);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, payrollBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, payrollBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                payrollBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return payrollBo.UserMessage;
        }

        public List<SelectListItem> GetEmployeeList(int monthId, int yearId, string businessUnit)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_GetEmployeelistforEmail");

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, businessUnit);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        list.Add(new SelectListItem
                        {
                            Text = reader[DBParam.Output.ID].ToString(),
                            Value = reader[DBParam.Output.EmailID].ToString()
                        });
                    }
                }
            }
            return list;
        }

        public DataSet GetPayStub(int employeeId, int monthId, int yearId)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            ds.Tables.Add(dt);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_PaystubConfiguration");

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dt);
                }
            }
            return ds;
        }

        #endregion Employee Payroll

        #region PayrollHistory

        public DataSet GetPayrollHistory(int employeeId)
        {
            DataSet ds = new DataSet();
            DataTable dtPayroll = new DataTable("PayrollHistory");
            ds.Tables.Add(dtPayroll);
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Getemployeepayrollhistory_V2", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, employeeId);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtPayroll);
                        if (ds.Tables["PayrollHistory"].Rows.Count > 0)
                        {
                            dtPayroll = Utility.SupressEmptyColumns(ds.Tables["PayrollHistory"]);
                            ds.Tables.RemoveAt(0);
                            ds.Tables.Add(dtPayroll);
                        }

                    }
                }
            }

            return ds;
        }

        public DataSet GetPayrollSummaryList(int monthId, int yearId, string businessUnitList)
        {
            DataSet ds = new DataSet();
            DataTable dtPayroll = new DataTable("PayrollSummary");
            ds.Tables.Add(dtPayroll);
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("GetPayrollSummaryList_V2", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.MonthID, monthId);
                    cmd.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.BusinessUnit, businessUnitList);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtPayroll);
                    }
                }
            }

            return ds;
        }

        #endregion PayrollHistory

        #region LOP Details

        public List<LopDetailsBo> SearchLopDetails(LopDetailsBo lopDetailsBo)
        {
            List<LopDetailsBo> list = new List<LopDetailsBo>();

            using (Database.CreateConnection())
            {
                string menuCode = lopDetailsBo.MenuCode;

                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_LOPDETAILS_V2);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, lopDetailsBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, lopDetailsBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, lopDetailsBo.BusinessUnitIdList);
                Database.AddInParameter(dbCommand, DBParam.Input.HasLopDays, DbType.Boolean, lopDetailsBo.HasLopDays);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        lopDetailsBo = new LopDetailsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeNo]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                            MonthId = Utility.CheckDbNull<byte>(reader[DBParam.Output.MonthID]),
                            YearId = Utility.CheckDbNull<short>(reader[DBParam.Output.YearId]),
                            LopDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.LOPDays]),
                            StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            Location = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            LopType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            ActualWorkDays = Utility.CheckDbNull<int>(reader[DBParam.Output.NoOfDays]),
                            Doj = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]),
                            MenuCode = menuCode
                        };
                        list.Add(lopDetailsBo);
                    }
                }
                return list;
            }
        }

        public string ManageLopDetails(List<LopDetailsBo> lopDetailsBoList)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);

            SqlCommand cmd = new SqlCommand(Constants.MANAGE_LOPDETAILS_V2)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.LOPDetails, ConvertToDataTable(lopDetailsBoList, "LOPDetails"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        public string DeleteLopDetails(LopDetailsBo LopDetailsBo)
        {
            string message = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DELETE_LOPDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, LopDetailsBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, LopDetailsBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, LopDetailsBo.BusinessUnitIdList);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        message = Utility.CheckDbNull<string>(reader[DBParam.Output.Message]);
                    }
                }
                return message;
            }
        }

        public static DataTable ConvertToDataTable<T>(List<T> items, string name)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            foreach (T item in items)
            {
                object[] values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            dataTable.Columns.Remove("entity");
            dataTable.Columns.Remove("UserMessage");
            dataTable.Columns.Remove("CreatedOn");
            dataTable.Columns.Remove("CreatedBy");
            dataTable.Columns.Remove("ModifiedOn");
            dataTable.Columns.Remove("ModifiedBy");
            dataTable.Columns.Remove("UserID");
            dataTable.Columns.Remove("UserName");
            dataTable.Columns.Remove("MenuCode");
            dataTable.Columns.Remove("ModifiedByName");
            dataTable.Columns.Remove("Operation");
            dataTable.Columns.Remove("DomainID");
            dataTable.Columns.Remove("GUID");
            dataTable.Columns.Remove("Designation");
            dataTable.Columns.Remove("Latitude");
            dataTable.Columns.Remove("longitude");
            dataTable.Columns.Remove("Location");
            dataTable.Columns.Remove("LoginID");
            dataTable.Columns.Remove("IsActive");
            dataTable.Columns.Remove("Name");
            dataTable.Columns.Remove("DOJ");
            dataTable.Columns.Remove("SNo");
            dataTable.Columns.Remove("Parameter");
            dataTable.Columns.Remove("Parameter1");
            dataTable.Columns.Remove("Parameter2");
            dataTable.Columns.Remove("Parameter3");
            dataTable.Columns.Remove("Emailhtml");

            if (name == "LOPDetails")
            {
                dataTable.Columns.Remove("EmployeeName");
                dataTable.Columns.Remove("BusinessUnitIDList");
                dataTable.Columns.Remove("MonthList");
                dataTable.Columns.Remove("YearList");
                dataTable.Columns.Remove("IsDeleted");
                dataTable.Columns.Remove("LOPType");
                dataTable.Columns.Remove("HasLopDays");
                dataTable.Columns.Remove("BusinessUnitList");
                dataTable.Columns.Remove("ActualWorkDays");
                dataTable.Columns["ID"].SetOrdinal(0);
                dataTable.Columns["MonthID"].SetOrdinal(1);
                dataTable.Columns["YearID"].SetOrdinal(2);
                dataTable.Columns["EmployeeID"].SetOrdinal(3);
                dataTable.Columns["LOPDays"].SetOrdinal(4);
                dataTable.Columns["StatusCode"].SetOrdinal(5);
            }
            return dataTable;
        }

        #endregion LOP Details

        public List<PayrollBO> RPT_SearchPayrollByMonthly(int monthId, int yearId, string businessUnitList, int? department)
        {
            List<PayrollBO> payrollBoList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_SEARCHPAYROLLBYMONTHLY);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, department);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            EmployeeName = Utility.CheckDbNull<string>(reader["Employee_Name"]),
                            OtherEarnings = Math.Round(Convert.ToDecimal(reader["Other Earnings"]), 0),
                            TDS = Math.Round(Utility.CheckDbNull<decimal>(reader["TAX"]), 0),
                            Deduction = Math.Round(Utility.CheckDbNull<decimal>(reader["Other Deductions"]),
                                0),
                            NetSalary = Math.Round(Utility.CheckDbNull<decimal>(reader["Net Pay"]), 0),
                            CTC = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.CTC]), 0),
                            LOPDays = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.LOPDays]), 0),
                        };
                        //=======================
                        //=======================
                        payrollBoList.Add(payrollBo);
                    }
                }
            }

            return payrollBoList;
        }
        #region Populate MailBody for Payroll

        public string PopulateMailBody(string monthName, string yearName, int monthId, int yearId, string businessUnitList, int? department)
        {
            string body;
            StringBuilder strContent = new StringBuilder();

            List<PayrollBO> payrollBoList = RPT_SearchPayrollByMonthly(monthId, yearId, businessUnitList, department);

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/PayrollDetails.html")))
            {
                body = reader.ReadToEnd();
            }

            decimal totalnetpay = 0, totalCtc = 0, totalTds = 0, totalLoans = 0, totalDeduction = 0, totalOtherEarnings = 0, totalLop = 0;
            foreach (PayrollBO item in payrollBoList)
            {
                strContent.AppendLine(@"<tr>");
                strContent.AppendLine(@"<td width='20%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt'>
                                         <p class=MsoNormal><span>" + item.EmployeeName +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span> " + item.NetSalary.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span>  " + item.CTC.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span> " + item.TDS.ToString("#,###,##0.00") +
                                      "</span></p></td>");

                strContent.AppendLine(@"<td width='13%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span> " + item.Deduction.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='13%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span> " + item.OtherEarnings.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span>  " + item.LOPDays.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"</tr>");

                totalnetpay = totalnetpay + item.NetSalary;
                totalCtc = totalCtc + item.CTC;
                totalTds = totalTds + item.TDS;
                totalLoans = totalLoans + item.Loans;
                totalDeduction = totalDeduction + item.Deduction;
                totalOtherEarnings = totalOtherEarnings + item.OtherEarnings;
                totalLop = totalLop + item.LOPDays;
            }

            strContent.AppendLine(@"<tr>");
            strContent.AppendLine(@"<td width='20%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt'>
                                           <p class=MsoNormal><span><b>Total</b>
                                        </span></p></td>");
            strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span><b> " + totalnetpay.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span> <b>" + totalCtc.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span><b> " + totalTds.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");

            strContent.AppendLine(@"<td width='13%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span><b>" + totalDeduction.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='13%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span> <b>" + totalOtherEarnings.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span><b>" + totalLop.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"</tr>");

            body = body.Replace("{List}", strContent.ToString());
            body = body.Replace("{Month}", monthName);
            body = body.Replace("{Year}", yearName);
            body = body.Replace("{Date}", DateTime.Now.ToString("dd-MMM-yyyy"));

            return body;
        }

        #endregion Populate MailBody for Payroll

        public string CheckPayrollProcessed(int monthId, int yearId, string businessUnitList, int? department)
        {
            string userMessage = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CHECKPAYROLLBYMONTHLY);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, department);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage]);
                    }
                }
            }

            if (userMessage.Split('!')[0] != "0")
            {
                return userMessage.Replace(userMessage.Split('!')[0], "");
            }

            return string.Empty;
        }
    }
}