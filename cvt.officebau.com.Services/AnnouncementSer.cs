﻿using cvt.officebau.com.ViewModels;
using System.Collections.Generic;

namespace cvt.officebau.com.Services
{
    public class AnnouncementSer : IAnnouncements
    {
        public List<CustomBo> GetStudentDetails()
        {
            return NotificationRepository.GetAnnouncements();
        }

        public CustomBo GetBuildNotification()
        {
            return NotificationRepository.GetBuildNotification();
        }
    }
}