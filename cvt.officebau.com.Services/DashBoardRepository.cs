﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class DashBoardRepository : BaseRepository
    {
        #region Get Accounts DashBoard TileDetails

        public IncomeBo GetAccountsDashBoardTileDetails()
        {
            IncomeBo incomeBo = new IncomeBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GetAccountsDashBoardTileDetails);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBo = new IncomeBo
                        {
                            ReceivedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Payable]),
                            CurrencyRate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PayableOverDue]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Recivables]),
                            DiscountValue = Utility.CheckDbNull<decimal>(reader[DBParam.Output.RecivablesOverDue])
                        };
                    }
                }
            }

            return incomeBo;
        }

        #endregion Get Accounts DashBoard TileDetails

        #region Get DashBoard Payable

        public List<ExpenseBo> GetDashBoardPayable()
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GetDashBoardPayable);

                Database.AddInParameter(dbCommand, DBParam.Input.Date, DbType.Int32, null);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBoList.Add(new ExpenseBo
                        {
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.MonthYear]),
                            PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Payable]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Receivables])
                        });
                    }
                }
            }

            return expenseBoList;
        }

        public List<ExpenseBo> SearchPayablesTileDetails(string tileType)
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchPayablesTileDetails");

                Database.AddInParameter(dbCommand, "@TileType", DbType.String, tileType);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBoList.Add(new ExpenseBo
                        {
                            VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorName]),
                            PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaidAmount]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Booked]),
                            DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Payable]),
                        });
                    }
                }
            }

            return expenseBoList;
        }

        public List<ExpenseBo> SearchReceivablesTileDetails(string tileType)
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchReceivablesTileDetails");

                Database.AddInParameter(dbCommand, "@TileType", DbType.String, tileType);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBoList.Add(new ExpenseBo
                        {
                            VendorName = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName]),
                            PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ReceivedAmount]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Booked]),
                            DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Receivables]),
                        });
                    }
                }
            }

            return expenseBoList;
        }

        #endregion Get DashBoard Payable

        #region Get DashBoard Receivables

        public List<ExpenseBo> GetDashBoardReceivables()
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GetDashBoardreceivables);

                Database.AddInParameter(dbCommand, DBParam.Input.Date, DbType.Int32, null);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBoList.Add(new ExpenseBo
                        {
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.MonthYear]),
                            PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Receivables]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount])
                        });
                    }
                }
            }

            return expenseBoList;
        }

        #endregion Get DashBoard Receivables

        #region Get DashBoard PandL

        public List<ExpenseBo> GetDashBoardPandL()
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GetDashBoardPandL);

                Database.AddInParameter(dbCommand, DBParam.Input.Date, DbType.Int32, null);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBoList.Add(new ExpenseBo
                        {
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.MonthYear]),
                            PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ExpenseAmount]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount])
                        });
                    }
                }
            }

            return expenseBoList;
        }

        #endregion Get DashBoard PandL

        #region Get DashBoard Agging Expense

        public List<ExpenseBo> GetDashBoardAggingExpense()
        {
            List<ExpenseBo> expenseBoList = new List<ExpenseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GetDashBoardAggingExpense);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        expenseBoList.Add(new ExpenseBo
                        {
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.Value]),
                            PaidAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAP]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAR])
                        });
                    }
                }
            }

            return expenseBoList;
        }

        #endregion Get DashBoard Agging Expense

        #region Get DashBoard Recent Modified

        public List<BankBo> GetDashBoardRecentModified()
        {
            List<BankBo> bankList = new List<BankBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GetDashBoardRecentModified);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        bankList.Add(new BankBo
                        {
                            BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.PartyName]),
                            OpeningBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return bankList;
        }

        #endregion Get DashBoard Recent Modified

        #region Get Dashboard Bank Details

        public List<BankBo> GetDashboardBankDetails()
        {
            List<BankBo> bankList = new List<BankBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.Getdashboardbankdetails);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        bankList.Add(new BankBo
                        {
                            BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.NAME]),
                            OpeningBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.AvailableBalance])
                        });
                    }
                }
            }

            return bankList;
        }

        public List<BankBo> GetDashboardSystemBankDetails()
        {
            List<BankBo> bankList = new List<BankBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETDASHBOARDSYSTEMBANKDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        bankList.Add(new BankBo
                        {
                            BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.NAME]),
                            OpeningBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.AvailableBalance])
                        });
                    }
                }
            }

            return bankList;
        }

        #endregion Get Dashboard Bank Details

        #region Get BalanceSheet

        public List<BankBo> GetBalanceSheet(string screen, SearchParamsBo searchParamsBo)
        {
            List<BankBo> balanceSheetList = new List<BankBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand;
                switch (screen)
                {
                    case "BalanceSheet":
                        dbCommand = Database.GetStoredProcCommand("NewBalancesheet");
                        break;

                    case "PAndL":
                        dbCommand = Database.GetStoredProcCommand("nEWpandl");
                        Database.AddInParameter(dbCommand, DBParam.Input.CostCenterID, DbType.Int32, searchParamsBo.CostCenterId);
                        break;

                    default:
                        dbCommand = Database.GetStoredProcCommand(Constants.GetTrailBalance);
                        break;
                }

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SearchStartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.SearchEndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        if (Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]) != 0)
                        {
                            balanceSheetList.Add(new BankBo
                            {
                                BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.NAME]),
                                OpeningBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.LEDGER]),
                                AccountNo = Utility.CheckDbNull<string>(reader[DBParam.Output.GROUPLEDGER]),
                                UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.CATEGORY]),
                                BranchName = Utility.CheckDbNull<string>(reader[DBParam.Output.BranchName]),
                                IfscCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                                DisplayName = Utility.CheckDbNull<string>(reader[DBParam.Output.ID])
                            });
                        }
                    }
                }
            }

            return balanceSheetList;
        }

        public List<BankBo> GetAccDetails(SearchParamsBo searchParamsBo)
        {
            List<BankBo> bankList = new List<BankBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("AccReportdetails");
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, searchParamsBo.CustomerName);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, searchParamsBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SearchStartDate, DbType.DateTime, searchParamsBo.StartDate);
                Database.AddInParameter(dbCommand, DBParam.Input.SearchEndDate, DbType.DateTime, searchParamsBo.EndDate);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        bankList.Add(new BankBo
                        {
                            Description = Utility.CheckDbNull<string>(reader["Name"]),
                            AccountNo = Utility.CheckDbNull<string>(reader["Ledger"]),
                            OpeningBalance = Utility.CheckDbNull<decimal>(reader["credit"]),
                            ClosingBalance = Utility.CheckDbNull<decimal>(reader["Debit"]),
                            OpeningBalanceDate = Utility.CheckDbNull<DateTime>(reader["Date"])
                        });
                    }
                }
            }

            return bankList;
        }

        #endregion Get BalanceSheet
    }
}