﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class GroupLedgerRepository : BaseRepository
    {
        #region Search GroupLedger

        public List<GroupLedgerBo> SearchGroupLedger(GroupLedgerBo groupLedgerBo)

        {
            List<GroupLedgerBo> groupLedgerBoList = new List<GroupLedgerBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_GROUPLEDGER);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        groupLedgerBoList.Add(
                            new GroupLedgerBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Group = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                ReportType = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ReportType])
                                },

                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks])
                            });
                    }
                }
            }

            return groupLedgerBoList;
        }

        #endregion Search GroupLedger

        #region Get GroupLedger

        public GroupLedgerBo GetGroupLedger(int groupLedgerId)
        {
            GroupLedgerBo groupLedgerBo = new GroupLedgerBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_GROUPLEDGER);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, groupLedgerId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        groupLedgerBo = new GroupLedgerBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Group = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            ReportType = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ReportType])
                            },
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy])
                        };
                    }
                }
            }

            return groupLedgerBo;
        }

        #endregion Get GroupLedger

        #region Manage GroupLedger

        public string ManageGroupLedger(GroupLedgerBo groupLedgerBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_GROUPLEDGER);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, groupLedgerBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, groupLedgerBo.Group);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, groupLedgerBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.ReportType, DbType.Int32, groupLedgerBo.ReportType?.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, groupLedgerBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                groupLedgerBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return groupLedgerBo.UserMessage;
        }

        #endregion Manage GroupLedger
    }
}