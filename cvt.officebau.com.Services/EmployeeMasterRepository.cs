﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Objects.SqlClient;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Services
{
    public class EmployeeMasterRepository : BaseRepository
    {
        #region variable

        private int _check;

        #endregion variable

        #region Duplicate LoginCode Check

        public string EmployeeLoginCodeCheck(string loginCode, string table, int employeeId)
        {
            string userMessage = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DUPLICATENAMECHECK);

                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, loginCode);
                Database.AddInParameter(dbCommand, DBParam.Input.Table, DbType.String, table);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.String, "");
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userMessage = Utility.CheckDbNull<string>(reader["Output"]);
                    }
                }

                return userMessage;
            }
        }

        #endregion Duplicate LoginCode Check

        #region Employee Complete Report

        public DataTable SearchEmployeeCompleteReport(ReportInputParamBo reportInputParamBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EMPLOYEEMASTERLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, reportInputParamBo.DepartmentId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, reportInputParamBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, reportInputParamBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, reportInputParamBo.IsActiveRecords);
                Database.AddInParameter(dbCommand, "@ColNames", DbType.String, reportInputParamBo.ColName);

                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable("EmployeeCompleteReport");
                dataSet.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion Employee Complete Report

        #region Employee DependentList Report

        public DataTable SearchEmployeeDependentListReport(ReportInputParamBo reportInputParamBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EMPLOYEEDEPENDENTLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, reportInputParamBo.DepartmentId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, reportInputParamBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, reportInputParamBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, reportInputParamBo.IsActiveRecords);

                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable("EmployeeDependentListReport");
                dataSet.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion Employee DependentList Report

        #region Employee Asset Report

        public DataTable SearchEmployeeAssetReport(ReportInputParamBo reportInputParamBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_COMPANYASSETLISTFOREMPLOYEE);

                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, reportInputParamBo.DepartmentId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, reportInputParamBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, reportInputParamBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, reportInputParamBo.IsActiveRecords);

                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable("EmployeeCompanyAssetReport");
                dataSet.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion Employee Asset Report

        #region Employee New Hires Report

        public DataTable SearchEmployeeNewHiresReport(ReportInputParamBo reportInputParamBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_NEWLYHIREEMPLOYEELIST);

                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, reportInputParamBo.DepartmentId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, reportInputParamBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, reportInputParamBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable("EmployeeNewHiresReport");
                dataSet.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion Employee New Hires Report

        #region Employee Asset Defaulters Report

        public DataTable SearchEmployeeDefaultersReport(ReportInputParamBo reportInputParamBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_COMPANYASSETDEFAULTERSLISTFOREMPLOYEE);

                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, reportInputParamBo.DepartmentId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, reportInputParamBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, reportInputParamBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable("EmployeeAssetDefaultersReport");
                dataSet.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion Employee Asset Defaulters Report

        #region Employee Skill Set Report

        public DataTable SearchEmployeeSkillSetReport(ReportInputParamBo reportInputParamBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EMPLOYEESKILLSET);

                Database.AddInParameter(dbCommand, DBParam.Input.SkillsID, DbType.Int32, reportInputParamBo.SkillsId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, reportInputParamBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, reportInputParamBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, reportInputParamBo.IsActiveRecords);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                DataSet dataSet = new DataSet();
                DataTable dataTable = new DataTable("EmployeeSkillSetReport");
                dataSet.Tables.Add(dataTable);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }

                return dataTable;
            }
        }

        #endregion Employee Skill Set Report

        #region Travel Details Report

        public DataTable TravelDetailsReportList(ReportInputParamBo reportInputParamBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_TRAVELDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, reportInputParamBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, reportInputParamBo.DepartmentId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, reportInputParamBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, reportInputParamBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dtTravelList = new DataTable();
                ds.Tables.Add(dtTravelList);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtTravelList);
                }

                return dtTravelList;
            }
        }

        #endregion Travel Details Report

        #region Employee Master Details

        public List<EmployeeMasterBo> SearchEmployeeMasterList(EmployeeMasterBo employeeMasterBo)
        {
            List<EmployeeMasterBo> employeeMasterBoList = new List<EmployeeMasterBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEEMASTERLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeMasterBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeMasterBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, employeeMasterBo.DepartmentId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, employeeMasterBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.FunctionalID, DbType.Int32, employeeMasterBo.FunctionalId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, employeeMasterBo.BaseLocationId);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, employeeMasterBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.EmailID, DbType.String, employeeMasterBo.EmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmploymentTypeID, DbType.Int32, employeeMasterBo.EmploymentTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.ReportingTo, DbType.String, employeeMasterBo.ReportingToName);
                Database.AddInParameter(dbCommand, DBParam.Input.PAN, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.PanNo);
                Database.AddInParameter(dbCommand, DBParam.Input.PFNo, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.PfNo);
                Database.AddInParameter(dbCommand, DBParam.Input.EEESI, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.EsiNo);
                Database.AddInParameter(dbCommand, DBParam.Input.AadharID, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.AadharId);
                Database.AddInParameter(dbCommand, DBParam.Input.UAN, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.Uan);
                Database.AddInParameter(dbCommand, DBParam.Input.PolicyNo, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.PolicyNo);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, employeeMasterBo.IsActive);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeMasterBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeMasterBoList.Add(
                            new EmployeeMasterBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                DesignationName = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]),
                                ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                                IsActive = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsApproved])
                            });
                    }

                    return employeeMasterBoList;
                }
            }
        }

        public DataTable SearchEmployeeMasterListForExport(EmployeeMasterBo employeeMasterBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORT_EMPLOYEEMASTERLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeMasterBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeMasterBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, employeeMasterBo.DepartmentId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, employeeMasterBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.FunctionalID, DbType.Int32, employeeMasterBo.FunctionalId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, employeeMasterBo.BaseLocationId);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, employeeMasterBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.EmailID, DbType.String, employeeMasterBo.EmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmploymentTypeID, DbType.Int32, employeeMasterBo.EmploymentTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.ReportingTo, DbType.String, employeeMasterBo.ReportingToName);
                Database.AddInParameter(dbCommand, DBParam.Input.PAN, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.PanNo);
                Database.AddInParameter(dbCommand, DBParam.Input.PFNo, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.PfNo);
                Database.AddInParameter(dbCommand, DBParam.Input.EEESI, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.EsiNo);
                Database.AddInParameter(dbCommand, DBParam.Input.AadharID, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.AadharId);
                Database.AddInParameter(dbCommand, DBParam.Input.UAN, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.Uan);
                Database.AddInParameter(dbCommand, DBParam.Input.PolicyNo, DbType.String, employeeMasterBo.StatutoryDetailsBo == null ? "" : employeeMasterBo.StatutoryDetailsBo.PolicyNo);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, employeeMasterBo.IsActive);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeMasterBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dtEmployeeMasterList = new DataTable();
                ds.Tables.Add(dtEmployeeMasterList);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtEmployeeMasterList);
                }

                return dtEmployeeMasterList;
            }
        }

        public EmployeeMasterBo GetEmployeeMaster(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeMasterBo result = new EmployeeMasterBo();

                if (id != null)
                {
                    result = (from e in context.tbl_EmployeeMaster
                              join em in context.tbl_EmployeeMaster on e.ModifiedBy equals em.ID
                              join rt in context.tbl_EmployeeMaster on e.ReportingToID equals rt.ID into report
                              from reportingToId in report.DefaultIfEmpty()
                              join fileUpload in context.tbl_FileUpload on e.FileID equals fileUpload.Id into fileUpload
                              from files in fileUpload.DefaultIfEmpty()
                              join g in context.tbl_EmployeeGrade on e.GradeID equals g.ID into grd
                              from grade in grd.DefaultIfEmpty()
                              join b in context.tbl_Band on e.BandID equals b.ID into bnd
                              from brand in bnd.DefaultIfEmpty()
                              join a in context.tbl_EmployeeApproval on e.ID equals a.EmployeeID into app
                              from employeeApproval in app.DefaultIfEmpty()
                              join p in context.tbl_EmployeePersonalDetails on e.ID equals p.EmployeeID into personal
                              from personalDetails in personal.DefaultIfEmpty()
                              join bu in context.tbl_BusinessUnit on em.BaseLocationID equals bu.ID into business
                              from businessUnit in business.DefaultIfEmpty()
                              join g in context.tbl_CodeMaster on e.GenderID equals g.ID into gen
                              from gender in gen.DefaultIfEmpty()
                              where e.ID == id
                              select new EmployeeMasterBo
                              {
                                  Id = e.ID,
                                  EmployeeId = e.ID,
                                  Code = e.Code,
                                  TempEmployeeCode = employeeApproval.TempEmployeeCode ?? "",
                                  LoginCode = e.LoginCode,
                                  FirstName = e.FirstName,
                                  MiddleName = e.MiddleName,
                                  LastName = e.LastName,
                                  DepartmentId = e.DepartmentID,
                                  DesignationId = e.DesignationID,
                                  ContactNo = e.ContactNo,
                                  EmergencyContactNo = e.EmergencyContactNo,
                                  GenderId = e.GenderID,
                                  EmailId = e.EmailID,
                                  PrefixId = e.PrefixID,
                                  ReportingToId = e.ReportingToID,
                                  ReportingToName = reportingToId.Code + " - " + reportingToId.FullName,
                                  EmploymentTypeId = e.EmploymentTypeID,
                                  GradeId = e.GradeID,
                                  GradeName = grade.Name,
                                  Qualification = e.Qualification,
                                  JobTypeId = e.JobTypeID,
                                  Doc = e.DOC,
                                  DateOFJoin = e.DOJ,
                                  ContractExpiryDate = e.ContractExpiryDate,
                                  ModifiedByName = em.FullName,
                                  ModifiedOn = e.ModifiedOn,
                                  BusinessUnitIDs = e.BusinessUnitID,
                                  FileName = files.FileName,
                                  FileUploadId = files.Id,
                                  FunctionalId = e.FunctionalID,
                                  BandId = e.BandID,
                                  BandName = brand.Name,
                                  BaseLocationId = e.BaseLocationID,
                                  IsActive = e.IsActive ?? false,
                                  ReasonId = e.ReasonID,
                                  InactiveFrom = e.InactiveFrom,
                                  ContractorId = e.ContractorId,
                                  RegionId = e.RegionID,
                                  Dob = personalDetails.DateOfBirth,
                                  BaseLocation = businessUnit.Name,
                                  BiometricCode = e.BiometricCode,
                                  Gender = gender.Code,
                                  EmpCodePattern = em.EmpCodePattern
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public string CreateEmployee(EmployeeMasterBo employeeMasterBo)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            using (FSMEntities context = new FSMEntities())
            {
                if (employeeMasterBo.EmailId != null)
                {
                    _check = CheckDuplicateNameWithDob(employeeMasterBo);
                }

                if (_check == 0)
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {
                        int domainId = Convert.ToInt32(Utility.DomainId());
                        string empCodePattern = context.tbl_Company.Where(b => b.ID == domainId).Select(b => b.EmpCodePattern).FirstOrDefault();
                        try
                        {
                            tbl_EmployeeMaster employee = new tbl_EmployeeMaster
                            {
                                Code = masterDataRepository.AutoGenerateEmployeeCode(),
                                EmpCodePattern = empCodePattern
                            };
                            employeeMasterBo.TempEmployeeCode = employee.Code;
                            employeeMasterBo.LoginCode = employee.LoginCode;
                            employee.FirstName = employeeMasterBo.FirstName;
                            employee.MiddleName = employeeMasterBo.MiddleName;
                            employee.LastName = employeeMasterBo.LastName;
                            employee.FullName = employeeMasterBo.FirstName + " " + (employeeMasterBo.MiddleName ?? "") + " " + (employeeMasterBo.LastName ?? "");
                            employee.DepartmentID = employeeMasterBo.DepartmentId;
                            employee.DesignationID = employeeMasterBo.DesignationId;
                            employee.ContactNo = employeeMasterBo.ContactNo;
                            employee.EmergencyContactNo = employeeMasterBo.EmergencyContactNo;
                            employee.GenderID = employeeMasterBo.GenderId;
                            employee.EmailID = employeeMasterBo.EmailId;
                            employee.PrefixID = employeeMasterBo.PrefixId;
                            employee.ReportingToID = employeeMasterBo.ReportingToId;
                            employee.EmploymentTypeID = employeeMasterBo.EmploymentTypeId;
                            employee.GradeID = employeeMasterBo.GradeId;
                            employee.Qualification = employeeMasterBo.Qualification;
                            employee.JobTypeID = employeeMasterBo.JobTypeId;
                            employee.DOC = employeeMasterBo.Doc;
                            employee.DOJ = employeeMasterBo.DateOFJoin;
                            employee.ContractExpiryDate = employeeMasterBo.ContractExpiryDate;
                            employee.HasAccess = employeeMasterBo.HasAccess;
                            employee.BusinessUnitID = employeeMasterBo.BusinessUnitIDs;
                            employee.FunctionalID = employeeMasterBo.FunctionalId;
                            employee.BandID = employeeMasterBo.BandId;
                            employee.BaseLocationID = employeeMasterBo.BaseLocationId;
                            employee.ApprovedBy = employeeMasterBo.ApprovedBy;
                            employee.ApprovedOn = employeeMasterBo.ApprovedOn;
                            employee.ReasonID = employeeMasterBo.ReasonId;
                            employee.InactiveFrom = employeeMasterBo.InactiveFrom;
                            employee.ContractorId = employeeMasterBo.ContractorId;
                            employee.RegionID = employeeMasterBo.RegionId;

                            if (!string.IsNullOrWhiteSpace(employeeMasterBo.FileId))
                            {
                                employee.FileID = new Guid(employeeMasterBo.FileId);
                            }

                            employee.IsActive = employeeMasterBo.IsActive;
                            employee.IsDeleted = employeeMasterBo.IsDeleted;
                            employee.CreatedBy = Utility.UserId();
                            employee.CreatedOn = DateTime.Now;
                            employee.ModifiedOn = DateTime.Now;
                            employee.ModifiedBy = Utility.UserId();
                            employee.DomainID = Utility.DomainId();
                            employee.HistoryID = Guid.NewGuid();

                            context.tbl_EmployeeMaster.Add(employee);
                            context.SaveChanges();

                            employeeMasterBo.Id = Convert.ToInt32(context.tbl_EmployeeMaster.OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                            employeeMasterBo.EmployeeId = employeeMasterBo.Id;
                            UpdateDateOfBirth(employeeMasterBo);

                            string defaultPassword = ConfigurationManager.AppSettings["DefaultPassword"];

                            tbl_Login login = new tbl_Login
                            {
                                EmployeeID = employeeMasterBo.Id,
                                Password =  EncryptionUtility.GetHashedValue(defaultPassword),
                                RecentPasswordChangeDate = DateTime.Now,
                                CreatedBy = Utility.UserId(),
                                CreatedOn = DateTime.Now,
                                DomainID = Utility.DomainId(),
                                HistoryID = Guid.NewGuid()
                            };

                            context.tbl_Login.Add(login);
                            context.SaveChanges();

                            tbl_EmployeeApproval approval = new tbl_EmployeeApproval
                            {
                                EmployeeID = employeeMasterBo.Id,
                                TempEmployeeCode = employeeMasterBo.TempEmployeeCode,
                                IsDeleted = employeeMasterBo.IsDeleted,
                                CreatedBy = Utility.UserId(),
                                CreatedOn = DateTime.Now,
                                ModifiedOn = DateTime.Now,
                                ModifiedBy = Utility.UserId(),
                                HistoryID = Guid.NewGuid(),
                                DomainID = Utility.DomainId()
                            };

                            context.tbl_EmployeeApproval.Add(approval);
                            context.SaveChanges();

                            if (employeeMasterBo.FileId != null)
                            {
                                tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == new Guid(employeeMasterBo.FileId));
                                if (fileUpload != null)
                                {
                                    fileUpload.ReferenceTable = "tbl_EmployeeMaster";
                                    context.Entry(fileUpload).State = EntityState.Modified;
                                }

                                context.SaveChanges();
                            }

                            context.ManageEmployeeLeaveDays(employeeMasterBo.Id, DateTime.Now, Utility.UserId(), Utility.DomainId());
                            context.SaveChanges();

                            transaction.Complete();
                            return "Inserted Successfully/" + employeeMasterBo.EmployeeId;
                        }
                        catch(Exception)
                        {
                            transaction.Dispose();
                            return "Operation Failed!";
                        }
                    }
                }

                return "Already Exists";
            }
        }

        public int CheckDuplicateNameWithDob(EmployeeMasterBo employeeMasterBo)
        {
            int count = 0;
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CHECK_DUPLICATE_NAMEWITHDOB);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeMasterBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeMasterBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.FullName, DbType.String, employeeMasterBo.FirstName + "" + employeeMasterBo.LastName);
                Database.AddInParameter(dbCommand, DBParam.Input.EmailID, DbType.String, employeeMasterBo.EmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.DateofBirth, DbType.DateTime, employeeMasterBo.Dob);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        count = Utility.CheckDbNull<int>(reader[DBParam.Output.Count]);
                    }
                }

                return count;
            }
        }

        public string UpdateEmployee(EmployeeMasterBo employeeMasterBo)
        {
            FSMEntities context = new FSMEntities();
            string message = string.Empty;

            if (employeeMasterBo.EmailId != null)
            {
                _check = CheckDuplicateNameWithDob(employeeMasterBo);
            }

            if (_check == 0)
            {
                if (employeeMasterBo.IsActive)
                {
                    if (employeeMasterBo.IsUhApprovalRequired != "0")
                    {
                        message = CheckPayrollToInactivateEmployee(employeeMasterBo.Id, employeeMasterBo.InactiveFrom);
                    }

                    if (string.IsNullOrEmpty(message) || message.Contains("Updated"))
                    {
                        using (TransactionScope transaction = new TransactionScope())
                        {
                            try
                            {
                                tbl_EmployeeMaster employee = context.tbl_EmployeeMaster.FirstOrDefault(b => b.ID == employeeMasterBo.Id);
                                if (employee != null)
                                {
                                    employee.Code = employeeMasterBo.Code;
                                    employee.BiometricCode =
                                        employeeMasterBo.BiometricCode ?? employeeMasterBo.Id.ToString();
                                    employee.LoginCode = employeeMasterBo.LoginCode;
                                    employee.FirstName = employeeMasterBo.FirstName;
                                    employee.MiddleName = employeeMasterBo.MiddleName;
                                    employee.LastName = employeeMasterBo.LastName;
                                    employee.FullName = employeeMasterBo.FirstName + " " + (employeeMasterBo.MiddleName ?? "") + " " + (employeeMasterBo.LastName ?? "");
                                    employee.DepartmentID = employeeMasterBo.DepartmentId;
                                    employee.DesignationID = employeeMasterBo.DesignationId;
                                    employee.ContactNo = employeeMasterBo.ContactNo;
                                    employee.EmergencyContactNo = employeeMasterBo.EmergencyContactNo;
                                    employee.GenderID = employeeMasterBo.GenderId;
                                    employee.EmailID = employeeMasterBo.EmailId;
                                    employee.PrefixID = employeeMasterBo.PrefixId;
                                    employee.ReportingToID = employeeMasterBo.ReportingToId;
                                    employee.EmploymentTypeID = employeeMasterBo.EmploymentTypeId;
                                    employee.GradeID = employeeMasterBo.GradeId;
                                    employee.Qualification = employeeMasterBo.Qualification;
                                    employee.JobTypeID = employeeMasterBo.JobTypeId;
                                    employee.DOC = employeeMasterBo.Doc;
                                    employee.DOJ = employeeMasterBo.DateOFJoin;
                                    employee.ContractExpiryDate = employeeMasterBo.ContractExpiryDate;
                                    employee.BusinessUnitID = employeeMasterBo.BusinessUnitIDs;
                                    employee.FunctionalID = employeeMasterBo.FunctionalId;
                                    employee.BandID = employeeMasterBo.BandId;
                                    employee.BaseLocationID = employeeMasterBo.BaseLocationId;
                                    employee.ApprovedBy = employeeMasterBo.ApprovedBy;
                                    employee.ApprovedOn = employeeMasterBo.ApprovedOn;
                                    employee.ContractorId = employeeMasterBo.ContractorId;
                                    employee.RegionID = employeeMasterBo.RegionId;

                                    if (!string.IsNullOrWhiteSpace(employeeMasterBo.FileId))
                                    {
                                        employee.FileID = new Guid(employeeMasterBo.FileId);
                                    }

                                    employee.HasAccess = employeeMasterBo.HasAccess;
                                    employee.IsActive = employeeMasterBo.IsActive;
                                    employee.InactiveFrom = employeeMasterBo.InactiveFrom;
                                    employee.ReasonID = employeeMasterBo.ReasonId;
                                    employee.IsDeleted = employeeMasterBo.IsDeleted;
                                    employee.ModifiedOn = DateTime.Now;
                                    employee.ModifiedBy = Utility.UserId();
                                    context.Entry(employee).State = EntityState.Modified;
                                }

                                context.SaveChanges();
                                employeeMasterBo.Id = employeeMasterBo.Id;
                                employeeMasterBo.EmployeeId = employeeMasterBo.Id;

                                if (employeeMasterBo.ApprovedBy != null || employeeMasterBo.SecondApprovedBy != null)
                                {
                                    EmployeeApproval(employeeMasterBo);
                                }

                                UpdateDateOfBirth(employeeMasterBo);
                                UpdatePayStructureEffectiveDate(employeeMasterBo);
                                if (employeeMasterBo.FileId != null)
                                {
                                    tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == new Guid(employeeMasterBo.FileId));
                                    if (fileUpload != null)
                                    {
                                        fileUpload.ReferenceTable = "tbl_EmployeeMaster";
                                        context.Entry(fileUpload).State = EntityState.Modified;
                                    }

                                    context.SaveChanges();
                                }

                                transaction.Complete();
                                return Notification.Updated+"/" + Convert.ToSingle(employeeMasterBo.EmployeeId);
                            }
                            catch (Exception)
                            {
                                transaction.Dispose();
                                return Notification.Default_Error_Message;
                            }
                        }
                    }

                    return message;
                }
                else
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {
                        try
                        {
                            tbl_EmployeeMaster employee = context.tbl_EmployeeMaster.FirstOrDefault(b => b.ID == employeeMasterBo.Id);
                            if (employee != null)
                            {
                                employee.Code = employeeMasterBo.Code;
                                employee.BiometricCode = employeeMasterBo.BiometricCode ?? employeeMasterBo.Id.ToString();
                                employee.LoginCode = employeeMasterBo.LoginCode;
                                employee.FirstName = employeeMasterBo.FirstName;
                                employee.MiddleName = employeeMasterBo.MiddleName;
                                employee.LastName = employeeMasterBo.LastName;
                                employee.FullName = employeeMasterBo.FirstName + " " + (employeeMasterBo.MiddleName ?? "") + " " + (employeeMasterBo.LastName ?? "");
                                employee.DepartmentID = employeeMasterBo.DepartmentId;
                                employee.DesignationID = employeeMasterBo.DesignationId;
                                employee.ContactNo = employeeMasterBo.ContactNo;
                                employee.EmergencyContactNo = employeeMasterBo.EmergencyContactNo;
                                employee.GenderID = employeeMasterBo.GenderId;
                                employee.EmailID = employeeMasterBo.EmailId;
                                employee.PrefixID = employeeMasterBo.PrefixId;
                                employee.ReportingToID = employeeMasterBo.ReportingToId;
                                employee.EmploymentTypeID = employeeMasterBo.EmploymentTypeId;
                                employee.GradeID = employeeMasterBo.GradeId;
                                employee.Qualification = employeeMasterBo.Qualification;
                                employee.JobTypeID = employeeMasterBo.JobTypeId;
                                employee.DOC = employeeMasterBo.Doc;
                                employee.DOJ = employeeMasterBo.DateOFJoin;
                                employee.ContractExpiryDate = employeeMasterBo.ContractExpiryDate;
                                employee.BusinessUnitID = employeeMasterBo.BusinessUnitIDs;
                                employee.FunctionalID = employeeMasterBo.FunctionalId;
                                employee.BandID = employeeMasterBo.BandId;
                                employee.BaseLocationID = employeeMasterBo.BaseLocationId;
                                employee.ApprovedBy = employeeMasterBo.ApprovedBy;
                                employee.ApprovedOn = employeeMasterBo.ApprovedOn;
                                employee.ContractorId = employeeMasterBo.ContractorId;
                                employee.RegionID = employeeMasterBo.RegionId;

                                if (!string.IsNullOrWhiteSpace(employeeMasterBo.FileId))
                                {
                                    employee.FileID = new Guid(employeeMasterBo.FileId);
                                }

                                employee.HasAccess = employeeMasterBo.HasAccess;
                                employee.IsActive = employeeMasterBo.IsActive;
                                employee.InactiveFrom = employeeMasterBo.InactiveFrom;
                                employee.ReasonID = employeeMasterBo.ReasonId;
                                employee.IsDeleted = employeeMasterBo.IsDeleted;
                                employee.ModifiedOn = DateTime.Now;
                                employee.ModifiedBy = Utility.UserId();
                                context.Entry(employee).State = EntityState.Modified;
                            }

                            context.SaveChanges();
                            employeeMasterBo.Id = employeeMasterBo.Id;
                            employeeMasterBo.EmployeeId = employeeMasterBo.Id;

                            if (employeeMasterBo.ApprovedBy != null || employeeMasterBo.SecondApprovedBy != null)
                            {
                                EmployeeApproval(employeeMasterBo);
                            }

                            UpdateDateOfBirth(employeeMasterBo);
                            UpdatePayStructureEffectiveDate(employeeMasterBo);
                            if (employeeMasterBo.FileId != null)
                            {
                                tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == new Guid(employeeMasterBo.FileId));
                                if (fileUpload != null)
                                {
                                    fileUpload.ReferenceTable = "tbl_EmployeeMaster";
                                    context.Entry(fileUpload).State = EntityState.Modified;
                                }

                                context.SaveChanges();
                            }

                            transaction.Complete();
                            return Notification.Updated+"/" + Convert.ToSingle(employeeMasterBo.EmployeeId);
                        }
                        catch (Exception)
                        {
                            transaction.Dispose();
                            return Notification.Default_Error_Message;
                        }
                    }
                }
            }

            return Notification.AlreadyExists;
        }

        public string CheckPayrollToInactivateEmployee(int employeeId, DateTime? inactiveForm)
        {
            string result = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CHECKPAYROLLTOINACTIVATEEMPLOYEE);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.InactiveForm, DbType.DateTime, inactiveForm);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        result = Utility.CheckDbNull<string>(reader["Result"]);
                    }
                }
            }

            return result;
        }

        private void EmployeeApproval(EmployeeMasterBo employeeMasterBo)
        {
            FSMEntities context = new FSMEntities();

            _check = context.tbl_EmployeeApproval.Count(s => !s.IsDeleted && s.EmployeeID == employeeMasterBo.Id);
            if (_check != 0)
            {
                tbl_EmployeeApproval approval = context.tbl_EmployeeApproval.FirstOrDefault(b => b.EmployeeID == employeeMasterBo.Id);
                if (employeeMasterBo.ApprovedBy != null)
                {
                    if (approval != null)
                    {
                        approval.FirstApproverId = employeeMasterBo.ApprovedBy;
                        approval.FirstApproverdOn = DateTime.Now;
                        approval.FirstApproverRemarks = employeeMasterBo.ApproverRemarks;
                        approval.EmployeePaystructureID = employeeMasterBo.PaystructureId;
                    }
                }

                if (employeeMasterBo.SecondApprovedBy != null)
                {
                    if (approval != null)
                    {
                        approval.SecondApproverId = employeeMasterBo.SecondApprovedBy;
                        approval.SecondApproverdOn = DateTime.Now;
                        approval.SecondApproverRemarks = employeeMasterBo.SecondApproverRemarks;
                    }
                }

                if (approval != null)
                {
                    approval.ModifiedOn = DateTime.Now;
                    approval.ModifiedBy = Utility.UserId();
                    approval.HistoryID = Guid.NewGuid();
                    approval.DomainID = Utility.DomainId();

                    context.Entry(approval).State = EntityState.Modified;
                }

                context.SaveChanges();
            }
        }

        private void UpdatePayStructureEffectiveDate(EmployeeMasterBo employeeMasterBo)
        {
            switch (employeeMasterBo.MenuCode)
            {
                case MenuCode.Onboard:
                case MenuCode.Onboard_Approval_HR:
                case MenuCode.Onboard_Approval_UnitHead:
                    FSMEntities context = new FSMEntities();

                    _check = context.tbl_Pay_EmployeePayStructure.Count(s => !(s.IsDeleted) && s.EmployeeID == employeeMasterBo.Id);
                    if (_check != 0)
                    {
                        tbl_Pay_EmployeePayStructure employeePayStructure = context.tbl_Pay_EmployeePayStructure.OrderByDescending(e => e.EffectiveFrom).FirstOrDefault(b => !(b.IsDeleted) && b.EmployeeID == employeeMasterBo.Id);

                        if (employeePayStructure != null)
                        {
                            employeePayStructure.EffectiveFrom = employeeMasterBo.DateOFJoin;
                            employeePayStructure.ModifiedOn = DateTime.Now;
                            employeePayStructure.ModifiedBy = Utility.UserId();
                            employeePayStructure.DomainID = Utility.DomainId();

                            context.Entry(employeePayStructure).State = EntityState.Modified;
                        }

                        context.SaveChanges();
                    }

                    break;
            }
        }

        public void ManageMenu(EmployeeMasterBo employeeMasterBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_DEFAULTMENUS);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeMasterBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.ModifiedBy, DbType.Int32, employeeMasterBo.ModifiedBy);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.ExecuteScalar(dbCommand);
            }
        }

        public string DeleteEmployee(EmployeeMasterBo employeeMasterBo)
        {
            FSMEntities context = new FSMEntities();
            tbl_EmployeeMaster employee = context.tbl_EmployeeMaster.FirstOrDefault(b => b.ID == employeeMasterBo.Id);

            if (employee != null)
            {
                employee.IsDeleted = employeeMasterBo.IsDeleted;
                employee.ModifiedOn = DateTime.Now;
                employee.ModifiedBy = Utility.UserId();

                context.Entry(employee).State = EntityState.Modified;
            }

            context.SaveChanges();

            _check = context.tbl_Pay_EmployeePayStructure.Count(s => s.EmployeeID == employeeMasterBo.Id);
            if (_check != 0)
            {
                tbl_Pay_EmployeePayStructure payStructure = context.tbl_Pay_EmployeePayStructure.FirstOrDefault(b => b.EmployeeID == employeeMasterBo.Id);
                context.Entry(payStructure).State = EntityState.Deleted;
                context.SaveChanges();
            }

            return Notification.Deleted;
        }

        public string DuplicateCheck(string employeeCode, int bu, int employeeId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                _check = context.tbl_EmployeeMaster.Count(s => ((s.EmpCodePattern ?? "") + "" + s.Code) == employeeCode && !s.IsDeleted && s.DomainID == domainId && s.BaseLocationID == bu && s.ID != employeeId);
                return _check.ToString();
            }
        }

        public void UploadEmployeeProfileImage(string fileId, int employeeId, int userId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                tbl_EmployeeMaster employee = context.tbl_EmployeeMaster.FirstOrDefault(b => b.ID == employeeId);

                if (employee != null)
                {
                    employee.FileID = new Guid(fileId);
                    employee.ModifiedOn = DateTime.Now;
                    employee.ModifiedBy = userId;

                    context.Entry(employee).State = EntityState.Modified;
                }

                context.SaveChanges();

                if (!string.IsNullOrWhiteSpace(fileId))
                {
                    tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == new Guid(fileId));
                    if (fileUpload != null)
                    {
                        fileUpload.ReferenceTable = "tbl_EmployeeMaster";
                        context.Entry(fileUpload).State = EntityState.Modified;
                    }

                    context.SaveChanges();
                }
            }
        }

        public EmployeeMasterBo GetDesignationBandGrade(int designationId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeMasterBo list = (from t in context.tbl_EmployeeDesignationMapping
                                         join g in context.tbl_EmployeeGrade on t.GradeID equals g.ID into grade
                                         from tg in grade.DefaultIfEmpty()
                                         join b in context.tbl_Band on t.BandID equals b.ID into band
                                         from tb in band.DefaultIfEmpty()
                                         where !t.IsDeleted == true && t.DesignationID == designationId
                                         select new EmployeeMasterBo
                                         {
                                             GradeId = t.GradeID,
                                             GradeName = tg.Name,
                                             BandId = t.BandID,
                                             BandName = tb.Name
                                         }).FirstOrDefault();
                return list;
            }
        }

        private void UpdateDateOfBirth(EmployeeMasterBo employeeMasterBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                _check = context.tbl_EmployeePersonalDetails.Count(s => s.EmployeeID == employeeMasterBo.EmployeeId && !s.IsDeleted);
                if (_check == 0)
                {
                    tbl_EmployeePersonalDetails personalDetails = new tbl_EmployeePersonalDetails
                    {
                        EmployeeID = employeeMasterBo.EmployeeId,
                        DateOfBirth = employeeMasterBo.Dob,
                        CreatedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = Utility.UserId(),
                        DomainID = Utility.DomainId(),
                        HistoryID = Guid.NewGuid()
                    };

                    context.tbl_EmployeePersonalDetails.Add(personalDetails);
                    context.SaveChanges();
                }
                else
                {
                    tbl_EmployeePersonalDetails updatePersonalDetails = context.tbl_EmployeePersonalDetails.FirstOrDefault(b => b.EmployeeID == employeeMasterBo.EmployeeId && !b.IsDeleted);

                    if (updatePersonalDetails != null)
                    {
                        updatePersonalDetails.DateOfBirth = employeeMasterBo.Dob;
                        updatePersonalDetails.ModifiedOn = DateTime.Now;
                        updatePersonalDetails.ModifiedBy = Utility.UserId();

                        context.Entry(updatePersonalDetails).State = EntityState.Modified;
                    }

                    context.SaveChanges();
                }
            }
        }

        #endregion Employee Master Details

        #region Email

        public EmployeeMasterBo GetEmailContent(int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeMasterBo result = (from e in context.tbl_EmployeeMaster
                                           join c in context.tbl_Company on e.DomainID equals c.ID
                                           where !e.IsDeleted && e.ID == employeeId
                                           select new EmployeeMasterBo
                                           {
                                               EmailId = e.EmailID,
                                               Name = c.FullName
                                           }).FirstOrDefault();

                return result;
            }
        }

        public string PopulateOnBoardApprovalMailBody(string mailToName, string companyName, string loginCode, string defaultPassword, string url)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/WelcomeAboard.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Name}", mailToName);
            body = body.Replace("{CompanyName}", companyName);
            body = body.Replace("{URL}", url);
            body = body.Replace("{LoginCode}", loginCode);
            body = body.Replace("{DefaultPassword}", defaultPassword);
            return body;
        }

        #endregion Email

        #region PersonalDetails

        public PersonalDetailsBo GetEmployeeDetails(int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                PersonalDetailsBo result = new PersonalDetailsBo();

                _check = context.tbl_EmployeePersonalDetails.Count(s => s.EmployeeID == employeeId && !s.IsDeleted);
                if (_check != 0)
                {
                    result = (from e in context.tbl_EmployeePersonalDetails
                              join em in context.tbl_EmployeeMaster on e.ModifiedBy equals em.ID
                              join bu in context.tbl_BusinessUnit on em.BaseLocationID equals bu.ID into business
                              from bUnit in business.DefaultIfEmpty()
                              where e.EmployeeID == employeeId
                              select new PersonalDetailsBo
                              {
                                  Id = employeeId,
                                  PersonalDetailId = e.ID,
                                  EmployeeId = e.ID,
                                  Dob = e.DateOfBirth,
                                  BloodGroupId = e.BloodGroupID,
                                  PresentAddress = e.PresentAddress,
                                  PermanentAddress = e.PermanentAddress,
                                  CountryId = e.CountryID,
                                  StateId = e.StateID,
                                  MaritalStatusId = e.MaritalStatusID,
                                  Anniversary = e.Anniversary,
                                  SpouseName = e.SpouseName,
                                  FatherName = e.FatherName,
                                  CityId = e.CityID,
                                  ModifiedByName = em.FullName,
                                  ModifiedOn = e.ModifiedOn,
                                  BaseLocation = bUnit.Name,
                                  MotherName = e.MotherName,
                                  EthnicityId = e.EthniCityID
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public string CreateEmployeePersonalDetails(PersonalDetailsBo personalDetailsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                _check = context.tbl_EmployeePersonalDetails.Count(s => s.EmployeeID == personalDetailsBo.EmployeeId && !s.IsDeleted && s.DomainID == domainId);
                if (_check == 0)
                {
                    tbl_EmployeePersonalDetails personalDetails = new tbl_EmployeePersonalDetails
                    {
                        EmployeeID = personalDetailsBo.EmployeeId,
                        DateOfBirth = personalDetailsBo.Dob,
                        BloodGroupID = personalDetailsBo.BloodGroupId,
                        PresentAddress = personalDetailsBo.PresentAddress,
                        PermanentAddress = personalDetailsBo.PermanentAddress,
                        CountryID = personalDetailsBo.CountryId,
                        StateID = personalDetailsBo.StateId,
                        MaritalStatusID = personalDetailsBo.MaritalStatusId,
                        Anniversary = personalDetailsBo.Anniversary,
                        SpouseName = personalDetailsBo.SpouseName,
                        FatherName = personalDetailsBo.FatherName,
                        CityID = personalDetailsBo.CityId,
                        IsDeleted = personalDetailsBo.IsDeleted,
                        CreatedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = Utility.UserId(),
                        DomainID = Utility.DomainId(),
                        HistoryID = Guid.NewGuid(),
                        MotherName = personalDetailsBo.MotherName,
                        EthniCityID = personalDetailsBo.EthnicityId
                    };

                    context.tbl_EmployeePersonalDetails.Add(personalDetails);
                    context.SaveChanges();

                    personalDetailsBo.PersonalDetailId = Convert.ToInt32(context.tbl_EmployeePersonalDetails.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                    return Notification.Inserted+"/" + personalDetailsBo.PersonalDetailId;
                }

                return Notification.AlreadyExists;
            }
        }

        public string UpdateEmployeePersonalDetails(PersonalDetailsBo personalDetailsBo)
        {
            FSMEntities context = new FSMEntities();
            int domainId = Utility.DomainId();

            _check = context.tbl_EmployeePersonalDetails.Count(s => s.ID != personalDetailsBo.PersonalDetailId && s.EmployeeID == personalDetailsBo.EmployeeId && !s.IsDeleted && s.DomainID == domainId);
            if (_check == 0)
            {
                tbl_EmployeePersonalDetails personalDetails = context.tbl_EmployeePersonalDetails.FirstOrDefault(b => b.ID == personalDetailsBo.PersonalDetailId && b.EmployeeID == personalDetailsBo.EmployeeId);

                if (personalDetails != null)
                {
                    personalDetails.DateOfBirth = personalDetailsBo.Dob;
                    personalDetails.BloodGroupID = personalDetailsBo.BloodGroupId;
                    personalDetails.PresentAddress = personalDetailsBo.PresentAddress;
                    personalDetails.PermanentAddress = personalDetailsBo.PermanentAddress;
                    personalDetails.CountryID = personalDetailsBo.CountryId;
                    personalDetails.StateID = personalDetailsBo.StateId;
                    personalDetails.MaritalStatusID = personalDetailsBo.MaritalStatusId;
                    personalDetails.Anniversary = personalDetailsBo.Anniversary;
                    personalDetails.SpouseName = personalDetailsBo.SpouseName;
                    personalDetails.FatherName = personalDetailsBo.FatherName;
                    personalDetails.CityID = personalDetailsBo.CityId;
                    personalDetails.IsDeleted = personalDetailsBo.IsDeleted;
                    personalDetails.ModifiedOn = DateTime.Now;
                    personalDetails.ModifiedBy = Utility.UserId();
                    personalDetails.MotherName = personalDetailsBo.MotherName;
                    personalDetails.EthniCityID = personalDetailsBo.EthnicityId;

                    context.Entry(personalDetails).State = EntityState.Modified;
                }

                context.SaveChanges();

                personalDetailsBo.PersonalDetailId = personalDetailsBo.PersonalDetailId;
                return Notification.Updated+"/" + personalDetailsBo.PersonalDetailId;
            }

            return Notification.AlreadyExists;
        }

        #endregion PersonalDetails

        #region Bank Details

        public StatutoryDetailsBo GetEmployeeBankDetails(int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                StatutoryDetailsBo result = new StatutoryDetailsBo();

                _check = context.tbl_EmployeeStatutoryDetails.Count(s => s.EmployeeID == employeeId && !s.IsDeleted);
                if (_check != 0)
                {
                    result = (from e in context.tbl_EmployeeStatutoryDetails
                              join em in context.tbl_EmployeeMaster on e.ModifiedBy equals em.ID
                              join bu in context.tbl_BusinessUnit on em.BaseLocationID equals bu.ID into business
                              from bUnit in business.DefaultIfEmpty()
                              where e.EmployeeID == employeeId
                              select new StatutoryDetailsBo
                              {
                                  Id = employeeId,
                                  StatutoryDetailId = e.ID,
                                  EmployeeId = e.ID,
                                  BankName = e.BankName,
                                  BranchName = e.BranchName,
                                  AccountNo = e.AccountNo,
                                  AccountTypeId = e.AccountTypeID,
                                  PanNo = e.PAN_No,
                                  PfNo = e.PFNo,
                                  IfscCode = e.IFSCCode,
                                  EsiNo = e.ESINo,
                                  Uan = e.UANNo,
                                  PolicyName = e.PolicyName,
                                  PolicyNo = e.PolicyNo,
                                  ModifiedByName = em.FullName,
                                  ModifiedOn = e.ModifiedOn,
                                  AadharId = e.AadharID,
                                  BaseLocation = bUnit.Name,
                                  CompanyPolicyNo = e.CompanyPolicyNo,
                                  TpInsurance = e.TPI,
                                  BeneId = e.BeneID
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public string CreateEmployeeBankDetails(StatutoryDetailsBo statutoryDetailsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                _check = context.tbl_EmployeeStatutoryDetails.Count(s => s.EmployeeID == statutoryDetailsBo.EmployeeId && !s.IsDeleted && s.DomainID == domainId);
                if (_check == 0)
                {
                    tbl_EmployeeStatutoryDetails statutoryDetails = new tbl_EmployeeStatutoryDetails
                    {
                        EmployeeID = statutoryDetailsBo.EmployeeId,
                        BankName = statutoryDetailsBo.BankName,
                        BranchName = statutoryDetailsBo.BranchName,
                        AccountNo = statutoryDetailsBo.AccountNo,
                        AccountTypeID = statutoryDetailsBo.AccountTypeId,
                        PAN_No = statutoryDetailsBo.PanNo,
                        PFNo = statutoryDetailsBo.PfNo,
                        IFSCCode = statutoryDetailsBo.IfscCode,
                        ESINo = statutoryDetailsBo.EsiNo,
                        UANNo = statutoryDetailsBo.Uan,
                        PolicyName = statutoryDetailsBo.PolicyName,
                        PolicyNo = statutoryDetailsBo.PolicyNo,
                        AadharID = statutoryDetailsBo.AadharId,
                        IsDeleted = statutoryDetailsBo.IsDeleted,
                        CreatedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = Utility.UserId(),
                        HistoryID = Guid.NewGuid(),
                        DomainID = Utility.DomainId(),
                        CompanyPolicyNo = statutoryDetailsBo.CompanyPolicyNo,
                        TPI = statutoryDetailsBo.TpInsurance,
                        BeneID = statutoryDetailsBo.BeneId
                    };

                    context.tbl_EmployeeStatutoryDetails.Add(statutoryDetails);
                    context.SaveChanges();

                    statutoryDetailsBo.StatutoryDetailId = Convert.ToInt32(context.tbl_EmployeeStatutoryDetails.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                    return Notification.Inserted+"/" + statutoryDetailsBo.StatutoryDetailId;
                }

                return Notification.AlreadyExists;
            }
        }

        public string UpdateEmployeeBankDetails(StatutoryDetailsBo statutoryDetailsBo)
        {
            FSMEntities context = new FSMEntities();
            int domainId = Utility.DomainId();

            _check = context.tbl_EmployeeStatutoryDetails.Count(s => s.ID != statutoryDetailsBo.StatutoryDetailId && s.EmployeeID == statutoryDetailsBo.EmployeeId && !s.IsDeleted && s.DomainID == domainId);
            if (_check == 0)
            {
                tbl_EmployeeStatutoryDetails statutoryDetails = context.tbl_EmployeeStatutoryDetails.FirstOrDefault(b => b.ID == statutoryDetailsBo.StatutoryDetailId && b.EmployeeID == statutoryDetailsBo.EmployeeId);

                if (statutoryDetails != null)
                {
                    statutoryDetails.BankName = statutoryDetailsBo.BankName;
                    statutoryDetails.BranchName = statutoryDetailsBo.BranchName;
                    statutoryDetails.AccountNo = statutoryDetailsBo.AccountNo;
                    statutoryDetails.AccountTypeID = statutoryDetailsBo.AccountTypeId;
                    statutoryDetails.PAN_No = statutoryDetailsBo.PanNo;
                    statutoryDetails.PFNo = statutoryDetailsBo.PfNo;
                    statutoryDetails.IFSCCode = statutoryDetailsBo.IfscCode;
                    statutoryDetails.ESINo = statutoryDetailsBo.EsiNo;
                    statutoryDetails.UANNo = statutoryDetailsBo.Uan;
                    statutoryDetails.PolicyName = statutoryDetailsBo.PolicyName;
                    statutoryDetails.PolicyNo = statutoryDetailsBo.PolicyNo;
                    statutoryDetails.AadharID = statutoryDetailsBo.AadharId;
                    statutoryDetails.IsDeleted = statutoryDetailsBo.IsDeleted;
                    statutoryDetails.ModifiedOn = DateTime.Now;
                    statutoryDetails.ModifiedBy = Utility.UserId();
                    statutoryDetails.CompanyPolicyNo = statutoryDetailsBo.CompanyPolicyNo;
                    statutoryDetails.TPI = statutoryDetailsBo.TpInsurance;
                    statutoryDetails.BeneID = statutoryDetailsBo.BeneId;

                    context.Entry(statutoryDetails).State = EntityState.Modified;
                }

                context.SaveChanges();

                statutoryDetailsBo.StatutoryDetailId = statutoryDetailsBo.StatutoryDetailId;
                return Notification.Updated+"/" + statutoryDetailsBo.StatutoryDetailId;
            }

            return Notification.AlreadyExists;
        }

        #endregion Bank Details

        #region Documents

        public List<DocumentDetailsBo> GetEmployeeDocumentsList(DocumentDetailsBo documentDetailsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<DocumentDetailsBo> list = (from employeeDocuments in context.tbl_EmployeeDocuments
                                                join modifiedBy in context.tbl_EmployeeMaster on employeeDocuments.ModifiedBy equals modifiedBy.ID
                                                join createdBy in context.tbl_EmployeeMaster on employeeDocuments.CreatedBy equals createdBy.ID
                                                join d in context.tbl_DocumentType on employeeDocuments.DocumentTypeId equals d.ID into dt
                                                from documentType in dt.DefaultIfEmpty()
                                                where !employeeDocuments.IsDeleted && employeeDocuments.DomainID == domainId && employeeDocuments.EmployeeID == documentDetailsBo.EmployeeId
                                                orderby employeeDocuments.ModifiedOn descending
                                                select new DocumentDetailsBo
                                                {
                                                    Id = employeeDocuments.ID,
                                                    Title = employeeDocuments.Title,
                                                    Description = employeeDocuments.Remarks,
                                                    FileName = employeeDocuments.FileName,
                                                    FilePath = employeeDocuments.FilePath,
                                                    FileUploadId = employeeDocuments.FileID,
                                                    FileCreateDate = employeeDocuments.CreatedOn,
                                                    EmployeeId = employeeDocuments.EmployeeID,
                                                    ModifiedOn = employeeDocuments.ModifiedOn,
                                                    ModifiedByName = modifiedBy.FullName,
                                                    DocumentTypeId = employeeDocuments.DocumentTypeId,
                                                    Document = documentType.Name,
                                                    DateOfExpiry = employeeDocuments.DateofExpiry,
                                                    CreatedByName = createdBy.FullName
                                                }).ToList();
                return list;
            }
        }

        public string CreateEmployeeDocumentDetails(DocumentDetailsBo documentDetailsBo)
        {
            FSMEntities context = new FSMEntities();

            _check = context.tbl_EmployeeDocuments.Count(s => s.ID == documentDetailsBo.Id);
            if (_check == 1)
            {
                tbl_EmployeeDocuments employeeDocuments = context.tbl_EmployeeDocuments.FirstOrDefault(b => b.ID == documentDetailsBo.Id);
                if (employeeDocuments != null)
                {
                    employeeDocuments.EmployeeID = documentDetailsBo.EmployeeId;
                    employeeDocuments.Title = documentDetailsBo.Title;
                    employeeDocuments.FileID = documentDetailsBo.FileUploadId;
                    employeeDocuments.Remarks = documentDetailsBo.Description;
                    employeeDocuments.DocumentTypeId = documentDetailsBo.DocumentTypeId;
                    employeeDocuments.DateofExpiry = documentDetailsBo.DateOfExpiry;
                    employeeDocuments.DomainID = documentDetailsBo.DomainId;
                    employeeDocuments.IsDeleted = documentDetailsBo.IsDeleted;
                    employeeDocuments.HistoryID = Guid.NewGuid();
                    employeeDocuments.ModifiedOn = DateTime.Now;
                    employeeDocuments.ModifiedBy = Utility.UserId();

                    context.Entry(employeeDocuments).State = EntityState.Modified;
                }

                context.SaveChanges();

                if (documentDetailsBo.FileUploadId != new Guid() && documentDetailsBo.FileUploadId != null)
                {
                    tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == documentDetailsBo.FileUploadId);
                    if (fileUpload != null)
                    {
                        fileUpload.ReferenceTable = "tbl_EmployeeDocuments";
                        context.Entry(fileUpload).State = EntityState.Modified;
                    }

                    context.SaveChanges();
                }

                return Notification.Updated;
            }
            else
            {
                tbl_EmployeeDocuments employeeDocuments = new tbl_EmployeeDocuments
                {
                    EmployeeID = documentDetailsBo.EmployeeId,
                    Title = documentDetailsBo.Title,
                    FileID = documentDetailsBo.FileUploadId,
                    Remarks = documentDetailsBo.Description,
                    DocumentTypeId = documentDetailsBo.DocumentTypeId,
                    DateofExpiry = documentDetailsBo.DateOfExpiry,
                    IsDeleted = documentDetailsBo.IsDeleted,
                    HistoryID = Guid.NewGuid(),
                    CreatedOn = DateTime.Now,
                    CreatedBy = Utility.UserId(),
                    ModifiedOn = DateTime.Now,
                    ModifiedBy = Utility.UserId(),
                    DomainID = Utility.DomainId()
                };

                context.tbl_EmployeeDocuments.Add(employeeDocuments);
                context.SaveChanges();

                if (documentDetailsBo.FileUploadId != new Guid() && documentDetailsBo.FileUploadId != null)
                {
                    tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == documentDetailsBo.FileUploadId);
                    if (fileUpload != null)
                    {
                        fileUpload.ReferenceTable = "tbl_EmployeeDocuments";
                        context.Entry(fileUpload).State = EntityState.Modified;
                    }

                    context.SaveChanges();
                }

                return Notification.Inserted;
            }
        }

        public string DeleteEmployeeDocumentDetails(DocumentDetailsBo documentDetailsBo)
        {
            FSMEntities context = new FSMEntities();

            _check = context.tbl_EmployeeDocuments.Count(s => s.ID == documentDetailsBo.Id);
            if (_check != 0)
            {
                tbl_EmployeeDocuments employeeDocuments = context.tbl_EmployeeDocuments.FirstOrDefault(b => b.ID == documentDetailsBo.Id);

                if (employeeDocuments != null)
                {
                    employeeDocuments.IsDeleted = documentDetailsBo.IsDeleted;
                    employeeDocuments.ModifiedOn = DateTime.Now;
                    employeeDocuments.ModifiedBy = Utility.UserId();

                    context.Entry(employeeDocuments).State = EntityState.Modified;
                }

                context.SaveChanges();

                return Notification.Deleted;
            }

            return Notification.AlreadyExists;
        }

        #endregion Documents

        #region Other Details

        public EmployeeMasterBo GetEmployeeOtherDetails(int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeMasterBo result;

                _check = context.tbl_EmployeeOtherDetails.Count(s => s.EmployeeID == employeeId && !s.IsDeleted);
                if (_check != 0)
                {
                    result = (from otherDetails in context.tbl_EmployeeOtherDetails
                              join modifiedBy in context.tbl_EmployeeMaster on otherDetails.ModifiedBy equals modifiedBy.ID
                              join employee in context.tbl_EmployeeMaster on otherDetails.EmployeeID equals employee.ID
                              join bu in context.tbl_BusinessUnit on modifiedBy.BaseLocationID equals bu.ID into business
                              from baseLocation in business.DefaultIfEmpty()
                              where otherDetails.EmployeeID == employeeId
                                 && !otherDetails.IsDeleted
                              select new EmployeeMasterBo
                              {
                                  Id = employeeId,
                                  OtherDetailId = otherDetails.ID,
                                  IsClaimRequest = false,
                                  ApplicationRoleId = otherDetails.ApplicationRoleID,
                                  ModifiedByName = modifiedBy.FullName,
                                  ModifiedOn = otherDetails.ModifiedOn,
                                  StartScreen = otherDetails.StartMenuURL,
                                  BiometricCode = employee.BiometricCode,
                                  LoginCode = employee.LoginCode,
                                  BaseLocation = baseLocation.Name,
                                  IsBiometricAccessRequired = otherDetails.IsBiometricAccessRequired ?? false,
                                  IsSuperUser = otherDetails.IsSuperUser ?? false,
                                  IPAddress = otherDetails.IPAddress
                              }).FirstOrDefault();
                }
                else
                {
                    result = (from employee in context.tbl_EmployeeMaster
                              from otherDetails in context.tbl_EmployeeOtherDetails.Where(od => od.EmployeeID == employee.ID).DefaultIfEmpty()
                              where employee.ID == employeeId && !employee.IsDeleted
                              select new EmployeeMasterBo
                              {
                                  Id = employeeId,
                                  OtherDetailId = 0,
                                  IsClaimRequest = false,
                                  BiometricCode = employee.BiometricCode,
                                  LoginCode = employee.LoginCode,
                                  IsBiometricAccessRequired = true
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public string CreateEmployeeOtherDetails(EmployeeMasterBo employeeMasterBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                _check = context.tbl_EmployeeOtherDetails.Count(s => s.EmployeeID == employeeMasterBo.EmployeeId && !s.IsDeleted && s.DomainID == domainId);
                if (_check == 0)
                {
                    int checkBioCode = context.tbl_EmployeeMaster.Count(x => x.BiometricCode.Substring(SqlFunctions.PatIndex("%[^0]%", x.BiometricCode).Value - 1) == employeeMasterBo.BiometricCode.Substring(SqlFunctions.PatIndex("%[^0]%", employeeMasterBo.BiometricCode).Value - 1) && x.ID != employeeMasterBo.EmployeeId && x.DomainID == domainId);
                    if (checkBioCode == 0)
                    {
                        tbl_EmployeeOtherDetails otherDetails = new tbl_EmployeeOtherDetails
                        {
                            EmployeeID = employeeMasterBo.EmployeeId,
                            IsClaimRequest = employeeMasterBo.IsClaimRequest,
                            ApplicationRoleID = employeeMasterBo.ApplicationRoleId,
                            StartMenuURL = employeeMasterBo.StartScreen,
                            IsBiometricAccessRequired = employeeMasterBo.IsBiometricAccessRequired,
                            IsSuperUser = employeeMasterBo.IsSuperUser,
                            IsDeleted = employeeMasterBo.IsDeleted,
                            CreatedBy = Utility.UserId(),
                            CreatedOn = DateTime.Now,
                            ModifiedOn = DateTime.Now,
                            ModifiedBy = Utility.UserId(),
                            HistoryID = Guid.NewGuid(),
                            DomainID = Utility.DomainId(),
                            IPAddress =employeeMasterBo.IPAddress
                        };

                        context.tbl_EmployeeOtherDetails.Add(otherDetails);
                        context.SaveChanges();

                        tbl_EmployeeMaster employeeMaster = context.tbl_EmployeeMaster.FirstOrDefault(b => b.ID == employeeMasterBo.EmployeeId);
                        if (employeeMaster != null)
                        {
                            employeeMaster.LoginCode = employeeMasterBo.LoginCode;
                        }

                        context.Entry(employeeMaster).State = EntityState.Modified;
                        context.SaveChanges();

                        int checkOtherDetails = context.tbl_EmployeeOtherDetails.Count(s => s.EmployeeID == employeeMasterBo.EmployeeId);
                        if (checkOtherDetails != 0)
                        {
                            employeeMaster = context.tbl_EmployeeMaster.FirstOrDefault(b => b.ID == employeeMasterBo.EmployeeId);

                            if (employeeMaster != null)
                            {
                                employeeMaster.BiometricCode = employeeMasterBo.BiometricCode;
                            }
                            context.Entry(employeeMaster).State = EntityState.Modified;
                            context.SaveChanges();
                        }

                        employeeMasterBo.OtherDetailId = Convert.ToInt32(context.tbl_EmployeeOtherDetails.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                        return Notification.Inserted+"/" + employeeMasterBo.OtherDetailId;
                    }

                    return BOConstants.Biometric_Code_already_mapped;
                }

                return Notification.AlreadyExists;
            }
        }

        public string UpdateEmployeeOtherDetails(EmployeeMasterBo employeeMasterBo)
        {
            FSMEntities context = new FSMEntities();
            int domainId = Utility.DomainId();

            _check = context.tbl_EmployeeOtherDetails.Count(s => s.ID != employeeMasterBo.OtherDetailId && s.EmployeeID == employeeMasterBo.EmployeeId && !s.IsDeleted && s.DomainID == domainId);
            if (_check == 0)
            {
                tbl_EmployeeOtherDetails otherDetails = context.tbl_EmployeeOtherDetails.FirstOrDefault(b => b.ID == employeeMasterBo.OtherDetailId && b.EmployeeID == employeeMasterBo.EmployeeId);

                if (otherDetails != null)
                {
                    otherDetails.IsClaimRequest = false;
                    otherDetails.ApplicationRoleID = employeeMasterBo.ApplicationRoleId;
                    otherDetails.StartMenuURL = employeeMasterBo.StartScreen;
                    otherDetails.IsBiometricAccessRequired = employeeMasterBo.IsBiometricAccessRequired;
                    otherDetails.IsSuperUser = employeeMasterBo.IsSuperUser;
                    otherDetails.IsDeleted = employeeMasterBo.IsDeleted;
                    otherDetails.ModifiedOn = DateTime.Now;
                    otherDetails.ModifiedBy = Utility.UserId();
                    context.Entry(otherDetails).State = EntityState.Modified;
                    otherDetails.IPAddress = employeeMasterBo.IPAddress;
                }

                context.SaveChanges();

                tbl_EmployeeMaster employeeMaster = context.tbl_EmployeeMaster.FirstOrDefault(b => b.ID == employeeMasterBo.EmployeeId);
                if (employeeMaster != null)
                {
                    employeeMaster.LoginCode = employeeMasterBo.LoginCode;
                }

                context.Entry(employeeMaster).State = EntityState.Modified;
                context.SaveChanges();

                int checkBioCode = context.tbl_EmployeeMaster.Count(x => x.BiometricCode.Substring(SqlFunctions.PatIndex("%[^0]%", x.BiometricCode).Value - 1) == employeeMasterBo.BiometricCode.Substring(SqlFunctions.PatIndex("%[^0]%", employeeMasterBo.BiometricCode).Value - 1) && x.ID != employeeMasterBo.EmployeeId && x.DomainID == domainId);
                if (checkBioCode == 0)
                {
                    employeeMaster = context.tbl_EmployeeMaster.FirstOrDefault(b => b.ID == employeeMasterBo.EmployeeId);

                    if (employeeMaster != null)
                    {
                        employeeMaster.BiometricCode = employeeMasterBo.BiometricCode;
                    }
                    context.Entry(employeeMaster).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else
                {
                    return BOConstants.Biometric_Code_already_mapped;
                }

                employeeMasterBo.OtherDetailId = employeeMasterBo.OtherDetailId;
                return Notification.Updated+"/" + employeeMasterBo.OtherDetailId;
            }

            return Notification.AlreadyExists;
        }

        public string DeleteEmployeeOtherDetails(EmployeeMasterBo employeeMasterBo)
        {
            FSMEntities context = new FSMEntities();

            _check = context.tbl_EmployeeOtherDetails.Count(s => s.EmployeeID == employeeMasterBo.Id);
            if (_check != 0)
            {
                tbl_EmployeeOtherDetails employee = context.tbl_EmployeeOtherDetails.FirstOrDefault(b => b.EmployeeID == employeeMasterBo.Id);
                if (employee != null)
                {
                    employee.IsDeleted = employeeMasterBo.IsDeleted;
                    employee.ModifiedOn = DateTime.Now;
                    employee.ModifiedBy = Utility.UserId();
                }
                context.Entry(employee).State = EntityState.Modified;
                context.SaveChanges();

                return Notification.Deleted;
            }

            return string.Empty;
        }

        #endregion Other Details

        #region Employee Paystructure

        public EmployeeMasterBo GetEmployeePayStructure(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeMasterBo result = new EmployeeMasterBo();

                if (id != 0)
                {
                    result = (from employee in context.tbl_EmployeeMaster
                              from modifiedBy in context.tbl_EmployeeMaster
                                                .Where(emm => emm.ID == employee.ModifiedBy).DefaultIfEmpty()
                              from reportingTo in context.tbl_EmployeeMaster
                                               .Where(rtm => rtm.ID == employee.ReportingToID).DefaultIfEmpty()
                              from files in context.tbl_FileUpload
                                                   .Where(fum => fum.Id == employee.FileID).DefaultIfEmpty()
                              from pay in context.tbl_Pay_EmployeePayStructure
                                                 .Where(epm => epm.EmployeeID == employee.ID && !(epm.IsDeleted)).DefaultIfEmpty()
                              from grade in context.tbl_EmployeeGrade
                                                .Where(gm => gm.ID == employee.GradeID).DefaultIfEmpty()
                              from band in context.tbl_Band
                                                .Where(bm => bm.ID == employee.BandID).DefaultIfEmpty()
                              from approval in context.tbl_EmployeeApproval
                                                .Where(epm => epm.EmployeeID == employee.ID).DefaultIfEmpty()
                              from firstApprover in context.tbl_EmployeeMaster
                                                .Where(fam => fam.ID == approval.FirstApproverId).DefaultIfEmpty()
                              from secondApprover in context.tbl_EmployeeMaster
                                                .Where(sam => sam.ID == approval.SecondApproverId).DefaultIfEmpty()
                              from personalDetails in context.tbl_EmployeePersonalDetails
                                                .Where(pdm => pdm.EmployeeID == employee.ID && !pdm.IsDeleted).DefaultIfEmpty()
                              from bu in context.tbl_BusinessUnit
                                                .Where(bum => bum.ID == modifiedBy.BaseLocationID).DefaultIfEmpty()
                              from bus in context.tbl_BusinessUnit
                                                 .Where(b => b.ID == firstApprover.BaseLocationID).DefaultIfEmpty()
                              from bUnit in context.tbl_BusinessUnit
                                                   .Where(baseLocation => baseLocation.ID == secondApprover.BaseLocationID).DefaultIfEmpty()
                              where employee.ID == id && !employee.IsDeleted
                              select new EmployeeMasterBo
                              {
                                  Id = employee.ID,
                                  EmployeeId = employee.ID,
                                  Code = employee.Code,
                                  TempEmployeeCode = approval.TempEmployeeCode ?? "",
                                  LoginCode = employee.LoginCode,
                                  FirstName = employee.FirstName,
                                  LastName = employee.LastName,
                                  DepartmentId = employee.DepartmentID,
                                  DesignationId = employee.DesignationID,
                                  ContactNo = employee.ContactNo,
                                  EmergencyContactNo = employee.EmergencyContactNo,
                                  GenderId = employee.GenderID,
                                  EmailId = employee.EmailID,
                                  PrefixId = employee.PrefixID,
                                  ReportingToId = employee.ReportingToID,
                                  ReportingToName = reportingTo.Code + " - " + reportingTo.FullName,
                                  EmploymentTypeId = employee.EmploymentTypeID,
                                  GradeId = employee.GradeID,
                                  GradeName = grade.Name,
                                  Qualification = employee.Qualification,
                                  JobTypeId = employee.JobTypeID,
                                  Doc = employee.DOC,
                                  DateOFJoin = employee.DOJ,
                                  ContractExpiryDate = employee.ContractExpiryDate,
                                  ModifiedByName = modifiedBy.FullName,
                                  ModifiedOn = employee.ModifiedOn,
                                  BusinessUnitIDs = employee.BusinessUnitID,
                                  FileName = files.Path,
                                  FunctionalId = employee.FunctionalID,
                                  BandId = employee.BandID,
                                  BandName = band.Name,
                                  BaseLocationId = employee.BaseLocationID,
                                  BaseLocation = bu.Name,
                                  IsActive = employee.IsActive ?? false,
                                  ContractorId = employee.ContractorId,
                                  ApproverRemarks = approval.FirstApproverRemarks ?? "",
                                  ApprovedOn = approval.FirstApproverdOn ?? employee.ModifiedOn,
                                  ApproverName = firstApprover.FullName ?? modifiedBy.FullName,
                                  SecondApproverRemarks = approval.SecondApproverRemarks ?? "",
                                  SecondApprovedOn = approval.SecondApproverdOn ?? employee.ModifiedOn,
                                  SecondApproverName = secondApprover.FullName ?? modifiedBy.FullName,
                                  RegionId = employee.RegionID,
                                  Dob = personalDetails.DateOfBirth,
                                  PaystructureId = pay.ID,
                                  FirstApproverBaseLocation = bus.Name ?? bu.Name,
                                  SecondApproverBaseLocation = bUnit.Name ?? bu.Name,
                                  BiometricCode = employee.BiometricCode
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        // All Employees(Active & Inactive) Reg. Payroll Configuration Only
        public List<CustomBo> SearchAutoCompleteEmployee(string employeeName, string bu)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                int regionId = context.tbl_BusinessUnit.Where(s => s.Name.Equals(bu.Replace(" )", "").Trim()) && !(s.IsDeleted ?? false) && s.DomainID == domainId).Select(s => s.ID).FirstOrDefault();
                List<CustomBo> list = (from t in context.tbl_EmployeeMaster
                                       where !t.IsDeleted && t.DomainID == domainId && t.FullName.Contains(employeeName) && t.HasAccess && t.RegionID == regionId && t.ReasonID == null
                                       orderby t.FullName
                                       select new CustomBo
                                       {
                                           Name = (t.Code ?? "") + " - " + (t.FullName ?? ""),
                                           Id = t.ID,
                                           Doj = t.DOJ
                                       }).ToList();
                return list;
            }
        }

        #endregion Employee Paystructure

        #region Employee Request & HR Approval

        public List<EmployeeMasterBo> SearchEmployeeRequestApprovalList(EmployeeMasterBo employeeMasterBo)
        {
            List<EmployeeMasterBo> employeeMasterBoList = new List<EmployeeMasterBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEEREQUESTAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeMasterBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.TempEmployeeCode, DbType.String, employeeMasterBo.TempEmployeeCode);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeMasterBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, employeeMasterBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, employeeMasterBo.IsApproved);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, employeeMasterBo.ApprovedBy);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeMasterBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeMasterBoList.Add(
                            new EmployeeMasterBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                TempEmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.TempEmployeeCode]),
                                FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                DesignationName = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]),
                                ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                                BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]),
                                IsActive = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsApproved]),
                                DateOFJoin = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]),
                                IsApproved = employeeMasterBo.IsApproved
                            });
                    }

                    return employeeMasterBoList;
                }
            }
        }

        public DataTable SearchEmployeeRequestApprovalListForExport(EmployeeMasterBo employeeMasterBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORT_EMPLOYEEREQUESTAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeMasterBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.TempEmployeeCode, DbType.String, employeeMasterBo.TempEmployeeCode);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeMasterBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, employeeMasterBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, employeeMasterBo.IsApproved);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, employeeMasterBo.ApprovedBy);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeMasterBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet dataSet = new DataSet();
                DataTable dtEmployeeMasterList = new DataTable();
                dataSet.Tables.Add(dtEmployeeMasterList);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dtEmployeeMasterList);
                }

                return dtEmployeeMasterList;
            }
        }

        #endregion Employee Request & HR Approval

        #region Company Assets

        public List<CompanyAssetsBo> SearchCompanyAssets(CompanyAssetsBo companyAssetsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<CompanyAssetsBo> list = (from companyAssets in context.tbl_CompanyAssets
                                              join em in context.tbl_EmployeeMaster on companyAssets.ModifiedBy equals em.ID into emp
                                              from employee in emp.DefaultIfEmpty()
                                              join a in context.tbl_AssetType on companyAssets.AssetTypeID equals a.ID into at
                                              from asset in at.DefaultIfEmpty()
                                              where !companyAssets.IsDeleted && companyAssets.DomainID == domainId && companyAssets.EmployeeID == companyAssetsBo.EmployeeId
                                              orderby companyAssets.ModifiedOn descending
                                              select new CompanyAssetsBo
                                              {
                                                  CompanyAssetId = companyAssets.ID,
                                                  Make = companyAssets.Make,
                                                  AssetTypeId = companyAssets.AssetTypeID,
                                                  SerialNumber = companyAssets.SerialNo,
                                                  Qty = companyAssets.QTY,
                                                  HandoverOn = companyAssets.HandoverOn,
                                                  EmployeeId = companyAssets.EmployeeID,
                                                  AssetsName = new  cvt.officebau.com.ViewModels.Entity
                                                  {
                                                      Name = asset.Name
                                                  },
                                                  ModifiedOn = companyAssets.ModifiedOn,
                                                  ModifiedByName = employee.FullName
                                              }).ToList();
                return list;
            }
        }

        public string CreateCompanyAssets(CompanyAssetsBo companyAssetsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                _check = context.tbl_CompanyAssets.Count(s => s.ID == companyAssetsBo.CompanyAssetId && !s.IsDeleted && s.DomainID == domainId);
                if (_check == 0)
                {
                    tbl_CompanyAssets companyAssets = new tbl_CompanyAssets
                    {
                        EmployeeID = companyAssetsBo.EmployeeId,
                        Make = companyAssetsBo.Make,
                        Remarks = companyAssetsBo.Remarks,
                        AssetTypeID = companyAssetsBo.AssetTypeId,
                        SerialNo = companyAssetsBo.SerialNumber,
                        QTY = companyAssetsBo.Qty,
                        HandoverOn = companyAssetsBo.HandoverOn,
                        ReturnStatus = companyAssetsBo.ReturnStatus,
                        IsDeleted = companyAssetsBo.IsDeleted,
                        CreatedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = Utility.UserId(),
                        DomainID = Utility.DomainId(),
                        HistoryID = Guid.NewGuid()
                    };

                    context.tbl_CompanyAssets.Add(companyAssets);
                    context.SaveChanges();

                    companyAssetsBo.CompanyAssetId = Convert.ToInt32(context.tbl_CompanyAssets.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                    return Notification.Inserted+"/" + companyAssetsBo.CompanyAssetId;
                }
                else
                {
                    return Notification.AlreadyExists;
                }
            }
        }

        public string UpdateCompanyAssets(CompanyAssetsBo companyAssetsBo)
        {
            FSMEntities context = new FSMEntities();
            _check = context.tbl_CompanyAssets.Count(s => s.ID == companyAssetsBo.CompanyAssetId && s.EmployeeID == companyAssetsBo.EmployeeId && !s.IsDeleted);

            if (_check != 0)
            {
                tbl_CompanyAssets companyAssets = context.tbl_CompanyAssets.FirstOrDefault(b => b.ID == companyAssetsBo.CompanyAssetId && b.EmployeeID == companyAssetsBo.EmployeeId);

                if (companyAssets != null)
                {
                    companyAssets.Make = companyAssetsBo.Make;
                    companyAssets.Remarks = companyAssetsBo.Remarks;
                    companyAssets.AssetTypeID = companyAssetsBo.AssetTypeId;
                    companyAssets.SerialNo = companyAssetsBo.SerialNumber;
                    companyAssets.QTY = companyAssetsBo.Qty;
                    companyAssets.HandoverOn = companyAssetsBo.HandoverOn;
                    companyAssets.ReturnStatus = companyAssetsBo.ReturnStatus;
                    companyAssets.IsDeleted = companyAssetsBo.IsDeleted;
                    companyAssets.ModifiedOn = DateTime.Now;
                    companyAssets.ModifiedBy = Utility.UserId();
                }
                context.Entry(companyAssets).State = EntityState.Modified;
                context.SaveChanges();

                companyAssetsBo.CompanyAssetId = companyAssetsBo.CompanyAssetId;
                return Notification.Updated+"/" + companyAssetsBo.CompanyAssetId;
            }
            else
            {
                return Notification.AlreadyExists;
            }
        }

        public string DeleteCompanyAssets(CompanyAssetsBo companyAssetsBo)
        {
            FSMEntities context = new FSMEntities();
            _check = context.tbl_CompanyAssets.Count(s => s.ID == companyAssetsBo.CompanyAssetId);

            int assetReturned = context.tbl_CompanyAssets.Count(a => !a.IsDeleted && a.ID == companyAssetsBo.CompanyAssetId && a.EmployeeID == companyAssetsBo.EmployeeId && !a.ReturnedOn.HasValue);

            if (assetReturned != 0)
            {
                tbl_CompanyAssets companyAssets = context.tbl_CompanyAssets.FirstOrDefault(b => b.ID == companyAssetsBo.CompanyAssetId);
                if (companyAssets != null)
                {
                    companyAssets.IsDeleted = companyAssetsBo.IsDeleted;
                    companyAssets.ModifiedOn = DateTime.Now;
                    companyAssets.ModifiedBy = Utility.UserId();
                }
                context.Entry(companyAssets).State = EntityState.Modified;
                context.SaveChanges();

                return Notification.Deleted;
            }
            else
            {
                return Notification.Record_Referred;
            }
        }

        public CompanyAssetsBo GetCompanyAsset(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                CompanyAssetsBo companyAssetsBo = (from companyAssets in context.tbl_CompanyAssets
                                                   join employee in context.tbl_EmployeeMaster on companyAssets.EmployeeID equals employee.ID
                                                   join m in context.tbl_EmployeeMaster on companyAssets.ModifiedBy equals m.ID
                                                   join a in context.tbl_AssetType on companyAssets.AssetTypeID equals a.ID
                                                   where !companyAssets.IsDeleted && companyAssets.ID == id
                                                   select new CompanyAssetsBo
                                                   {
                                                       Id = companyAssets.EmployeeID,
                                                       CompanyAssetId = companyAssets.ID,
                                                       Make = companyAssets.Make,
                                                       Remarks = companyAssets.Remarks,
                                                       AssetTypeId = companyAssets.AssetTypeID,
                                                       SerialNumber = companyAssets.SerialNo,
                                                       Qty = companyAssets.QTY,
                                                       HandoverOn = companyAssets.HandoverOn,
                                                       EmployeeId = companyAssets.EmployeeID,
                                                       AssetsName = new  cvt.officebau.com.ViewModels.Entity
                                                       {
                                                           Name = a.Name
                                                       },
                                                       ModifiedOn = companyAssets.ModifiedOn,
                                                       ModifiedByName = employee.FullName
                                                   }).FirstOrDefault();
                return companyAssetsBo;
            }
        }

        #endregion Company Assets

        #region Dependent Details

        public List<DependentDetailsBo> SearchDependentDetails(DependentDetailsBo dependentDetailsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<DependentDetailsBo> list = (from dependentDetails in context.tbl_EmployeeDependentDetails
                                                 //join modifiedBy in context.tbl_EmployeeMaster on dependentDetails.ModifiedBy equals modifiedBy.ID
                                                 join gender in context.tbl_CodeMaster on dependentDetails.GenderID equals gender.ID
                                                 where !(dependentDetails.IsDeleted ?? false) && dependentDetails.DomainID == domainId && dependentDetails.EmployeeID == dependentDetailsBo.EmployeeId
                                                 orderby dependentDetails.ModifiedOn descending
                                                 select new DependentDetailsBo
                                                 {
                                                     DependentDetailId = dependentDetails.ID,
                                                     Name = dependentDetails.Name,
                                                     Gender = gender.Code,
                                                     GenderId = dependentDetails.GenderID,
                                                     Dob = dependentDetails.DOB,
                                                     RelationshipId = dependentDetails.RelationshipID,
                                                     Remarks = dependentDetails.Remarks,
                                                     EmployeeId = dependentDetails.EmployeeID,
                                                     ModifiedOn = dependentDetails.ModifiedOn,
                                                     //ModifiedByName = modifiedBy.FullName
                                                 }).ToList();
                return list;
            }
        }

        public string CreateDependentDetails(DependentDetailsBo dependentDetailsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                _check = context.tbl_EmployeeDependentDetails.Count(s => s.ID == dependentDetailsBo.DependentDetailId && !(s.IsDeleted ?? false) && s.DomainID == domainId);

                if (_check == 0)
                {
                    tbl_EmployeeDependentDetails dependentDetails = new tbl_EmployeeDependentDetails
                    {
                        EmployeeID = dependentDetailsBo.EmployeeId,
                        Name = dependentDetailsBo.Name,
                        GenderID = dependentDetailsBo.GenderId,
                        DOB = dependentDetailsBo.Dob,
                        RelationshipID = dependentDetailsBo.RelationshipId,
                        Remarks = dependentDetailsBo.Remarks,
                        IsDeleted = dependentDetailsBo.IsDeleted,
                        CreatedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = Utility.UserId(),
                        DomainID = Utility.DomainId(),
                        HistoryID = Guid.NewGuid()
                    };

                    context.tbl_EmployeeDependentDetails.Add(dependentDetails);
                    context.SaveChanges();

                    dependentDetailsBo.DependentDetailId = Convert.ToInt32(context.tbl_EmployeeDependentDetails.Where(b => !(b.IsDeleted ?? false) && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                    return Notification.Inserted+"/" + dependentDetailsBo.DependentDetailId;
                }
                else
                {
                    return Notification.AlreadyExists;
                }
            }
        }

        public string UpdateDependentDetails(DependentDetailsBo dependentDetailsBo)
        {
            FSMEntities context = new FSMEntities();
            _check = context.tbl_EmployeeDependentDetails.Count(s => s.ID == dependentDetailsBo.DependentDetailId && s.EmployeeID == dependentDetailsBo.EmployeeId && !(s.IsDeleted ?? false));

            if (_check != 0)
            {
                tbl_EmployeeDependentDetails dependentDetails = context.tbl_EmployeeDependentDetails.FirstOrDefault(b => b.ID == dependentDetailsBo.DependentDetailId && b.EmployeeID == dependentDetailsBo.EmployeeId);

                if (dependentDetails != null)
                {
                    dependentDetails.Name = dependentDetailsBo.Name;
                    dependentDetails.GenderID = dependentDetailsBo.GenderId;
                    dependentDetails.DOB = dependentDetailsBo.Dob;
                    dependentDetails.RelationshipID = dependentDetailsBo.RelationshipId;
                    dependentDetails.Remarks = dependentDetailsBo.Remarks;
                    dependentDetails.ModifiedOn = DateTime.Now;
                    dependentDetails.ModifiedBy = Utility.UserId();
                    dependentDetails.HistoryID = Guid.NewGuid();
                }
                context.Entry(dependentDetails).State = EntityState.Modified;
                context.SaveChanges();

                return Notification.Updated+"/" + dependentDetailsBo.DependentDetailId;
            }
            else
            {
                return Notification.AlreadyExists;
            }
        }

        public string DeleteDependentDetails(DependentDetailsBo dependentDetailsBo)
        {
            FSMEntities context = new FSMEntities();
            _check = context.tbl_EmployeeDependentDetails.Count(s => s.ID == dependentDetailsBo.DependentDetailId);

            if (_check != 0)
            {
                tbl_EmployeeDependentDetails dependentDetails = context.tbl_EmployeeDependentDetails.FirstOrDefault(b => b.ID == dependentDetailsBo.DependentDetailId);

                if (dependentDetails != null)
                {
                    dependentDetails.IsDeleted = dependentDetailsBo.IsDeleted;
                    dependentDetails.ModifiedOn = DateTime.Now;
                    dependentDetails.ModifiedBy = Utility.UserId();
                }
                context.Entry(dependentDetails).State = EntityState.Modified;
                context.SaveChanges();

                return Notification.Deleted;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion Dependent Details

        #region Report

        public List<EmployeeReportBo> SearchEmployeeJoinReport(EmployeeReportBo employeeReportBo)
        {
            List<EmployeeReportBo> employeeLoanBoList = new List<EmployeeReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EMPLOYEEJOINING);

                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.DateTime, employeeReportBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.DateTime, employeeReportBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.RegionID, DbType.Int32, employeeReportBo.RegionId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, employeeReportBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.String, employeeReportBo.IsActiveRecords);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeLoanBoList.Add(
                            new EmployeeReportBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region]),
                                BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                Department = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                Designation = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]),
                                Doj = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]),
                                Gender = Utility.CheckDbNull<string>(reader[DBParam.Output.Gender]),
                                Dob = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOB]),
                                MaritalStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.MaritalStatus]),
                                Address = Utility.CheckDbNull<string>(reader[DBParam.Output.PresentAddress]),
                                ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                                EmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]),
                                FatherName = Utility.CheckDbNull<string>(reader[DBParam.Output.FatherName]),
                                PfNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PFNo]),
                                EsiNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ESINo]),
                                GrossSalary = Utility.CheckDbNull<decimal>(reader[DBParam.Output.GrossSalary]),
                                Uan = Utility.CheckDbNull<string>(reader[DBParam.Output.UAN]),
                                IfscCode = Utility.CheckDbNull<string>(reader[DBParam.Output.IFSCCode]),
                                BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName]),
                                AadharId = Utility.CheckDbNull<string>(reader[DBParam.Output.AadharID]),
                                PanNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PANNO]),
                                AccountNo = Utility.CheckDbNull<string>(reader[DBParam.Output.AccountNo]),
                                IsActiveRecords = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo])
                            });
                    }
                }
            }

            return employeeLoanBoList;
        }

        public List<EmployeeReportBo> SearchEmployeeRelieveReport(EmployeeReportBo employeeReportBo)
        {
            List<EmployeeReportBo> employeeLoanBoList = new List<EmployeeReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EMPLOYEERELIEVING);

                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.DateTime, employeeReportBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.DateTime, employeeReportBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.RegionID, DbType.Int32, employeeReportBo.RegionId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, employeeReportBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeLoanBoList.Add(
                            new EmployeeReportBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region]),
                                BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                Department = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                Designation = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]),
                                Doj = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]),
                                Gender = Utility.CheckDbNull<string>(reader[DBParam.Output.Gender]),
                                RelievingDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.RelievingDate]),
                                MaritalStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.MaritalStatus]),
                                Address = Utility.CheckDbNull<string>(reader[DBParam.Output.PresentAddress]),
                                ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                                EmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]),
                                FatherName = Utility.CheckDbNull<string>(reader[DBParam.Output.FatherName]),
                                PfNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PFNo]),
                                EsiNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ESINo]),
                                GrossSalary = Utility.CheckDbNull<decimal>(reader[DBParam.Output.GrossSalary]),
                                Uan = Utility.CheckDbNull<string>(reader[DBParam.Output.UAN]),
                                IfscCode = Utility.CheckDbNull<string>(reader[DBParam.Output.IFSCCode]),
                                BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName]),
                                AadharId = Utility.CheckDbNull<string>(reader[DBParam.Output.AadharID]),
                                PanNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PANNO]),
                                Dob = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOB]),
                                AccountNo = Utility.CheckDbNull<string>(reader[DBParam.Output.AccountNo]),
                                IsActiveRecords = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo])
                            });
                    }
                }
            }

            return employeeLoanBoList;
        }

        #endregion Report

        #region Unit Head Approval

        public List<EmployeeMasterBo> SearchEmployeeUhApprovalList(EmployeeMasterBo employeeMasterBo)
        {
            List<EmployeeMasterBo> employeeMasterBoList = new List<EmployeeMasterBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEEUHAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeMasterBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.TempEmployeeCode, DbType.String, employeeMasterBo.TempEmployeeCode);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeMasterBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, employeeMasterBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, employeeMasterBo.IsApproved);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, employeeMasterBo.ApprovedBy);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeMasterBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeMasterBoList.Add(
                            new EmployeeMasterBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Code = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                TempEmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.TempEmployeeCode]),
                                FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                DesignationName = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]),
                                ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                                BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]),
                                IsActive = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsApproved]),
                                IsApproved = employeeMasterBo.IsApproved
                            });
                    }

                    return employeeMasterBoList;
                }
            }
        }

        public DataTable SearchEmployeeUhApprovalListForExport(EmployeeMasterBo employeeMasterBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORT_EMPLOYEEUHAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeMasterBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.TempEmployeeCode, DbType.String, employeeMasterBo.TempEmployeeCode);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, employeeMasterBo.FirstName);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, employeeMasterBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, employeeMasterBo.IsApproved);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, employeeMasterBo.ApprovedBy);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeMasterBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dtEmployeeMasterList = new DataTable();
                ds.Tables.Add(dtEmployeeMasterList);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtEmployeeMasterList);
                }

                return dtEmployeeMasterList;
            }
        }

        #endregion Unit Head Approval

        #region Offer Letter

        public EmployeeMasterBo GetOfferLetterDetails(int employeeId)
        {
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GetEmployeeOfferLetterDetail);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeMasterBo.UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.ReferenceNo]);
                        employeeMasterBo.Id = Utility.CheckDbNull<int>(reader["Eligiable"]);
                        employeeMasterBo.BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]);
                        employeeMasterBo.Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross]);
                        employeeMasterBo.FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyName]);
                        //employeeMasterBO.ReportingToName = Utility.CheckDBNull<string>(reader[DBParam.Output.ReportingToName]);
                        //employeeMasterBO.ReportingToID = Utility.CheckDBNull<int>(reader["ReportingToID"]);
                    }
                }
            }

            return employeeMasterBo;
        }
        //Jeeva comment for New payStructure
        //public EmployeeMasterBo EmployeeOfferLetter(int employeeId, int reportingToId)
        //{
        //    using (FSMEntities context = new FSMEntities())
        //    {
        //        EmployeeMasterBo result = new EmployeeMasterBo();
        //        int domainId = Utility.DomainId();

        //        if (employeeId != 0)
        //        {
        //            result = (from employee in context.tbl_EmployeeMaster
        //                          //from pay in context.tbl_EmployeePayStructure
        //                          //      .Where(EPM => EPM.EmployeeId == E.ID && !(EPM.IsDeleted ?? false)).OrderBy(EPM => EPM.EffectiveFrom).DefaultIfEmpty()
        //                      let pay = context.tbl_Pay_EmployeePayStructure
        //                                       .Where(epm => epm.EmployeeID == employeeId && !(epm.IsDeleted))
        //                                       .OrderBy(epm => epm.EffectiveFrom).FirstOrDefault()
        //                      from baseLocation in context.tbl_BusinessUnit
        //                                        .Where(bum => bum.ID == employee.BaseLocationID).DefaultIfEmpty()
        //                      from prefix in context.tbl_CodeMaster.Where(c => c.ID == employee.PrefixID).DefaultIfEmpty()
        //                      from dep in context.tbl_Designation.Where(d => d.ID == employee.DesignationID).DefaultIfEmpty()
        //                      from band in context.tbl_Band.Where(b => b.ID == employee.BandID).DefaultIfEmpty()
        //                      from level in context.tbl_EmployeeGrade.Where(l => l.ID == employee.GradeID).DefaultIfEmpty()
        //                      from personalDetails in context.tbl_EmployeePersonalDetails
        //                                           .Where(ea => ea.EmployeeID == employee.ID && !ea.IsDeleted).DefaultIfEmpty()
        //                      where employee.ID == employeeId && !employee.IsDeleted && employee.DomainID == domainId
        //                      select new EmployeeMasterBo
        //                      {
        //                          FirstName = employee.FirstName,
        //                          LastName = prefix.Code,
        //                          DateOFJoin = employee.DOJ,
        //                          Designation = dep.Name ?? "-",
        //                          FileName = personalDetails.PermanentAddress == "" || personalDetails.PermanentAddress == null ? personalDetails.PresentAddress == "" || personalDetails.PresentAddress == null ? "-" : personalDetails.PresentAddress : personalDetails.PermanentAddress, //for Employee Permanent OR Present Address
        //                          BandName = band.Name ?? "-",
        //                          GradeName = level.Name ?? "-",
        //                          ReportingToName = context.tbl_EmployeeMaster.FirstOrDefault(s => s.ID == reportingToId).FullName,
        //                          ApproverName = context.tbl_EmployeeMaster.FirstOrDefault(s => s.ID == reportingToId).LastName ?? "", //For Last name of the Head
        //                          DepartmentName = context.tbl_Designation.FirstOrDefault(s => s.ID == context.tbl_EmployeeMaster.FirstOrDefault(f => f.ID == reportingToId).DesignationID).Name ?? "-", //for the Designation of the Head
        //                          PayStructureBo = new PayStructureBO
        //                          {
        //                              Basic = Math.Round(pay.Basic),
        //                              HRA = Math.Round(pay.HRA),
        //                              MedicalAllowance = Math.Round(pay.MedicalAllowance),
        //                              Conveyance = Math.Round(pay.Conveyance),
        //                              SplAllow = Math.Round(pay.SplAllow),
        //                              ERPF = Math.Round(pay.ERPF),
        //                              ERESI = Math.Round(pay.ERESI),
        //                              Gratuity = Math.Round(pay.Gratuity),
        //                              Bonus = Math.Round(pay.Bonus),
        //                              Medicalclaim = Math.Round(pay.Mediclaim),
        //                              PLI = Math.Round(pay.PLI),
        //                              MobileDataCard = Math.Round(pay.MobileDataCard),
        //                              OtherPerks = Math.Round(pay.OtherPerks ?? 0),
        //                              EducationAllowance = Math.Round(pay.EducationAllow ?? 0),
        //                              PaperMagazine = Math.Round(pay.MagazineAllow ?? 0)
        //                          }
        //                      }).FirstOrDefault();
        //        }

        //        return result;
        //    }
        //}

        #endregion Offer Letter

        #region Employment Details

        public List<EmploymentDetailsBo> SearchEmploymentDetails(EmploymentDetailsBo employmentDetailsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<EmploymentDetailsBo> list = (from employeeDetails in context.tbl_EmploymentDetails
                                                  join employeeMaster in context.tbl_EmployeeMaster on employeeDetails.ModifiedBy equals employeeMaster.ID
                                                  join a in context.tbl_Year on employeeDetails.YearID equals a.ID into year
                                                  from fromYear in year.DefaultIfEmpty()
                                                  join b in context.tbl_Month on employeeDetails.MonthID equals b.ID into month
                                                  from fromMonth in month.DefaultIfEmpty()
                                                  join c in context.tbl_Year on employeeDetails.ToYearID equals c.ID into tyear
                                                  from toYear in tyear.DefaultIfEmpty()
                                                  join b in context.tbl_Month on employeeDetails.ToMonthID equals b.ID into tmonth
                                                  from toMonth in tmonth.DefaultIfEmpty()
                                                  where !employeeDetails.IsDeleted && employeeDetails.DomainID == domainId && employeeDetails.EmployeeID == employmentDetailsBo.EmployeeId
                                                  orderby employeeDetails.ModifiedOn descending
                                                  select new EmploymentDetailsBo
                                                  {
                                                      EmploymentDetailsId = employeeDetails.ID,
                                                      CompanyName = employeeDetails.CompanyName,
                                                      // FinancialYear = FY.Name,
                                                      MonthId = employeeDetails.MonthID ?? 0,
                                                      Month = fromMonth.Code,
                                                      YearId = employeeDetails.YearID,
                                                      Year = fromYear.Name,
                                                      ToMonthId = employeeDetails.ToMonthID ?? 0,
                                                      ToMonth = toMonth.Code,
                                                      ToYearId = employeeDetails.ToYearID ?? 0,
                                                      ToYear = toYear.Name,
                                                      LastDesignation = employeeDetails.LastDesignation,
                                                      WorkLocation = employeeDetails.WorkLocation,
                                                      EmployeeId = employeeDetails.EmployeeID,
                                                      EmployeeCode = employeeDetails.Code,
                                                      ReportingTo = employeeDetails.ReportingTo,
                                                      Remarks = employeeDetails.Remarks,
                                                      ModifiedOn = employeeDetails.ModifiedOn,
                                                      ModifiedByName = employeeMaster.FullName
                                                  }).ToList();

                foreach (EmploymentDetailsBo l in list)
                {
                    l.Duration = CalculateDuration(l.Month, l.Year, l.ToMonth, l.ToYear);
                }

                return list;
            }
        }

        private string CalculateDuration(string fromMonth, string fromYear, string toMonth, string toYear)
        {
            int diffMonth;
            DateTime fromDate = Convert.ToDateTime(fromYear + "-" + fromMonth + "-01");
            DateTime toDate = Convert.ToDateTime(toYear + "-" + toMonth + "-01");
            int diffYears = toDate.Year - fromDate.Year;
            if (toDate.Month > fromDate.Month)
            {
                diffMonth = toDate.Month - fromDate.Month;
            }
            else
            {
                diffMonth = fromDate.Month - toDate.Month;
            }

            var result = diffYears + " Year(s) " + diffMonth + " Month(s)";
            return result;
        }

        public string CreateEmploymentDetails(EmploymentDetailsBo employmentDetailsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                _check = context.tbl_EmploymentDetails.Count(s => s.CompanyName == employmentDetailsBo.CompanyName && s.EmployeeID == employmentDetailsBo.EmployeeId && !s.IsDeleted && s.DomainID == domainId);

                if (_check == 0)
                {
                    tbl_EmploymentDetails employmentDetails = new tbl_EmploymentDetails
                    {
                        EmployeeID = employmentDetailsBo.EmployeeId,
                        CompanyName = employmentDetailsBo.CompanyName,
                        MonthID = employmentDetailsBo.MonthId,
                        YearID = employmentDetailsBo.YearId,
                        ToMonthID = employmentDetailsBo.ToMonthId,
                        ToYearID = employmentDetailsBo.ToYearId,
                        LastDesignation = employmentDetailsBo.LastDesignation,
                        Code = employmentDetailsBo.EmployeeCode,
                        ReportingTo = employmentDetailsBo.ReportingTo,
                        WorkLocation = employmentDetailsBo.WorkLocation,
                        Remarks = employmentDetailsBo.Remarks,
                        IsDeleted = employmentDetailsBo.IsDeleted,
                        CreatedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = Utility.UserId(),
                        DomainID = Utility.DomainId(),
                        HistoryID = Guid.NewGuid()
                    };

                    context.tbl_EmploymentDetails.Add(employmentDetails);
                    context.SaveChanges();

                    return Notification.Inserted;
                }
                else
                {
                    return Notification.AlreadyExists;
                }
            }
        }

        public string UpdateEmploymentDetails(EmploymentDetailsBo employmentDetailsBo)
        {
            FSMEntities context = new FSMEntities();

            tbl_EmploymentDetails employmentDetails = context.tbl_EmploymentDetails.FirstOrDefault(b => b.ID == employmentDetailsBo.EmploymentDetailsId && b.EmployeeID == employmentDetailsBo.EmployeeId);

            if (employmentDetails != null)
            {
                employmentDetails.CompanyName = employmentDetailsBo.CompanyName;
                employmentDetails.MonthID = employmentDetailsBo.MonthId;
                employmentDetails.YearID = employmentDetailsBo.YearId;
                employmentDetails.ToMonthID = employmentDetailsBo.ToMonthId;
                employmentDetails.ToYearID = employmentDetailsBo.ToYearId;
                employmentDetails.LastDesignation = employmentDetailsBo.LastDesignation;
                employmentDetails.Code = employmentDetailsBo.EmployeeCode;
                employmentDetails.ReportingTo = employmentDetailsBo.ReportingTo;
                employmentDetails.WorkLocation = employmentDetailsBo.WorkLocation;
                employmentDetails.Remarks = employmentDetailsBo.Remarks;
                employmentDetails.ModifiedOn = DateTime.Now;
                employmentDetails.ModifiedBy = Utility.UserId();
                employmentDetails.HistoryID = Guid.NewGuid();
            }
            context.Entry(employmentDetails).State = EntityState.Modified;
            context.SaveChanges();

            return Notification.Updated;
        }

        public string DeleteEmploymentDetails(EmploymentDetailsBo employmentDetailsBo)
        {
            FSMEntities context = new FSMEntities();

            tbl_EmploymentDetails employmentDetails = context.tbl_EmploymentDetails.FirstOrDefault(b => b.ID == employmentDetailsBo.EmploymentDetailsId);

            if (employmentDetails != null)
            {
                employmentDetails.IsDeleted = employmentDetailsBo.IsDeleted;
                employmentDetails.ModifiedOn = DateTime.Now;
                employmentDetails.ModifiedBy = Utility.UserId();
            }
            context.Entry(employmentDetails).State = EntityState.Modified;
            context.SaveChanges();

            return Notification.Deleted;
        }

        #endregion Employment Details

        #region Skill Details

        public List<SkillDetailsBo> GetEmployeeSkillsList(SkillDetailsBo skillDetailsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<SkillDetailsBo> list = (from s in context.tbl_EmployeeSkills
                                             join em in context.tbl_EmployeeMaster on s.ModifiedBy equals em.ID into emp
                                             from employee in emp.DefaultIfEmpty()
                                             join a in context.tbl_Skills on s.SkillsID equals a.ID into skill
                                             from skills in skill.DefaultIfEmpty()
                                             join b in context.tbl_CodeMaster on s.LevelID equals b.ID into level
                                             from levels in level.DefaultIfEmpty()
                                             join c in context.tbl_Year on s.LastUsedYearID equals c.ID into year
                                             from lastYear in year.DefaultIfEmpty()
                                             where !s.IsDeleted && s.DomainID == domainId && s.EmployeeID == skillDetailsBo.EmployeeId
                                             orderby s.ModifiedOn descending
                                             select new SkillDetailsBo
                                             {
                                                 Id = s.ID,
                                                 SkillsId = s.SkillsID,
                                                 Skills = skills.Name,
                                                 LevelId = s.LevelID,
                                                 Level = levels.Code,
                                                 YearId = s.Year,
                                                 MonthId = s.Month,
                                                 LastUsedVersion = s.LastUsedVersion,
                                                 LastUsedyearId = s.LastUsedYearID ?? 0,
                                                 LastUsedYear = lastYear.Name,
                                                 Description = s.Description ?? "",
                                                 EmployeeId = s.EmployeeID,
                                                 ModifiedOn = s.ModifiedOn,
                                                 ModifiedByName = employee.FullName,
                                                 IsLocked = s.IsLocked,
                                                 MenuCode = skillDetailsBo.MenuCode ?? "",
                                                 FileUploadId = s.FileID,
                                                 DateofExpiry = s.DateofExpiry
                                             }).ToList();
                return list;
            }
        }

        public string CreateSkillDetails(SkillDetailsBo skillDetailsBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                //check = context.tbl_EmployeeSkills.Count(s => s.ID == skillDetailsBO.SkillDetailID && !s.IsDeleted && s.DomainID == domainID);
                //if (check == 0)
                //{
                tbl_EmployeeSkills employeeSkills = new tbl_EmployeeSkills
                {
                    EmployeeID = skillDetailsBo.EmployeeId,
                    SkillsID = skillDetailsBo.SkillsId,
                    LevelID = skillDetailsBo.LevelId,
                    Year = skillDetailsBo.YearId,
                    Month = skillDetailsBo.MonthId,
                    LastUsedVersion = skillDetailsBo.LastUsedVersion,
                    LastUsedYearID = skillDetailsBo.LastUsedyearId,
                    Description = skillDetailsBo.Description,
                    IsLocked = skillDetailsBo.IsLocked,
                    FileID = skillDetailsBo.FileUploadId,
                    DateofExpiry = skillDetailsBo.DateofExpiry,
                    IsDeleted = skillDetailsBo.IsDeleted,
                    CreatedBy = Utility.UserId(),
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now,
                    ModifiedBy = Utility.UserId(),
                    DomainID = Utility.DomainId(),
                    HistoryID = Guid.NewGuid()
                };

                context.tbl_EmployeeSkills.Add(employeeSkills);
                context.SaveChanges();

                skillDetailsBo.SkillDetailId = Convert.ToInt32(context.tbl_EmployeeSkills.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                return Notification.Inserted+"/" + skillDetailsBo.SkillDetailId;
                //}
                //else
                //{
                //    return "Already Exists";
                //}
            }
        }

        public string UpdateSkillDetails(SkillDetailsBo skillDetailsBo)
        {
            FSMEntities context = new FSMEntities();
            _check = context.tbl_EmployeeSkills.Count(s => s.ID == skillDetailsBo.SkillDetailId && s.EmployeeID == skillDetailsBo.EmployeeId && !s.IsDeleted);

            if (_check != 0)
            {
                tbl_EmployeeSkills employeeSkills = context.tbl_EmployeeSkills.FirstOrDefault(b => b.ID == skillDetailsBo.SkillDetailId && b.EmployeeID == skillDetailsBo.EmployeeId);

                if (employeeSkills != null)
                {
                    employeeSkills.SkillsID = skillDetailsBo.SkillsId;
                    employeeSkills.LevelID = skillDetailsBo.LevelId;
                    employeeSkills.Year = skillDetailsBo.YearId;
                    employeeSkills.Month = skillDetailsBo.MonthId;
                    employeeSkills.LastUsedVersion = skillDetailsBo.LastUsedVersion;
                    employeeSkills.LastUsedYearID = skillDetailsBo.LastUsedyearId;
                    employeeSkills.Description = skillDetailsBo.Description;
                    employeeSkills.IsLocked = skillDetailsBo.IsLocked;
                    employeeSkills.FileID = skillDetailsBo.FileUploadId;
                    employeeSkills.DateofExpiry = skillDetailsBo.DateofExpiry;
                    employeeSkills.IsDeleted = skillDetailsBo.IsDeleted;
                    employeeSkills.ModifiedOn = DateTime.Now;
                    employeeSkills.ModifiedBy = Utility.UserId();

                    context.Entry(employeeSkills).State = EntityState.Modified;
                }

                context.SaveChanges();

                return Notification.Updated+"/" + skillDetailsBo.SkillDetailId;
            }
            else
            {
                return Notification.AlreadyExists;
            }
        }

        public string DeleteSkillDetails(SkillDetailsBo skillDetailsBo)
        {
            FSMEntities context = new FSMEntities();
            _check = context.tbl_EmployeeSkills.Count(s => s.ID == skillDetailsBo.SkillDetailId); // Anbu

            if (_check != 0)
            {
                tbl_EmployeeSkills employeeSkills = context.tbl_EmployeeSkills.FirstOrDefault(b => b.ID == skillDetailsBo.SkillDetailId);
                if (employeeSkills != null)
                {
                    employeeSkills.IsDeleted = skillDetailsBo.IsDeleted;
                    employeeSkills.ModifiedOn = DateTime.Now;
                    employeeSkills.ModifiedBy = Utility.UserId();
                    context.Entry(employeeSkills).State = EntityState.Modified;
                }

                context.SaveChanges();

                return Notification.Deleted;
            }

            return string.Empty;
        }

        public SkillDetailsBo GetSkillDetails(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                SkillDetailsBo skillDetailsBo = (from skills in context.tbl_EmployeeSkills
                                                 join employeeMaster in context.tbl_EmployeeMaster on skills.ModifiedBy equals employeeMaster.ID into emp
                                                 from employee in emp.DefaultIfEmpty()
                                                 join files in context.tbl_FileUpload on skills.FileID equals files.Id into fileName
                                                 from filelist in fileName.DefaultIfEmpty()
                                                 where !skills.IsDeleted && skills.ID == id
                                                 select new SkillDetailsBo
                                                 {
                                                     SkillDetailId = skills.ID,
                                                     SkillsId = skills.SkillsID,
                                                     LevelId = skills.LevelID,
                                                     YearId = skills.Year,
                                                     MonthId = skills.Month,
                                                     LastUsedVersion = skills.LastUsedVersion,
                                                     LastUsedyearId = skills.LastUsedYearID ?? 0,
                                                     Description = skills.Description,
                                                     EmployeeId = skills.EmployeeID,
                                                     ModifiedOn = skills.ModifiedOn,
                                                     ModifiedByName = employee.FullName,
                                                     IsLocked = skills.IsLocked,
                                                     DateofExpiry = skills.DateofExpiry,
                                                     FileUploadId = skills.FileID,
                                                     FileName = filelist.OriginalFileName
                                                 }).FirstOrDefault();
                return skillDetailsBo;
            }
        }

        public string LockUnlockEmployeeSkills(int employeeId, bool status)
        {
            string message;
            using (FSMEntities context = new FSMEntities())
            {
                List<tbl_EmployeeSkills> records = context.tbl_EmployeeSkills.Where(x => x.EmployeeID == employeeId).ToList();
                records.ForEach(a => a.IsLocked = status);
                context.SaveChanges();
                message = status ? "Locked Successfully." : "Unlocked Successfully.";
            }
            return message;
        }

        #endregion Skill Details

        #region Employee Skill Matrix Report

        public List<ReportInputParamBo> SearchEmployeeSkillMatrix(ReportInputParamBo reportInputParamBo)
        {
            List<ReportInputParamBo> reportInputParamBoList = new List<ReportInputParamBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EMPLOYEESKILLMATRIX);

                Database.AddInParameter(dbCommand, DBParam.Input.SkillsID, DbType.Int32, reportInputParamBo.SkillsId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, reportInputParamBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, reportInputParamBo.IsActiveRecords);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        reportInputParamBoList.Add(
                            new ReportInputParamBo
                            {
                                BeginnerCount = Utility.CheckDbNull<int>(reader["Beginner"]),
                                InterMediateCount = Utility.CheckDbNull<int>(reader["Intermediate"]),
                                ExpertCount = Utility.CheckDbNull<int>(reader["Experience"]),
                                Skills = Utility.CheckDbNull<string>(reader["Skills"]),
                                SkillsId = Utility.CheckDbNull<int>(reader["SkillsID"]),
                                BeginnerLevelId = Utility.CheckDbNull<int>(reader["BeginnerLevelID"]),
                                InterMediateLevelId = Utility.CheckDbNull<int>(reader["IntermediateLevelID"]),
                                ExpertLevelId = Utility.CheckDbNull<int>(reader["ExpertLevelID"])
                            });
                    }

                    return reportInputParamBoList;
                }
            }
        }

        public List<SkillDetailsBo> GetEmployeeSkillsDetailsList(int skillsId, int levelId, bool isActive)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<SkillDetailsBo> list = (from skills in context.tbl_EmployeeSkills
                                             join employeeMaster in context.tbl_EmployeeMaster on skills.EmployeeID equals employeeMaster.ID into emp
                                             from employee in emp.DefaultIfEmpty()
                                             join c in context.tbl_Year on skills.LastUsedYearID equals c.ID into year
                                             from lastYear in year.DefaultIfEmpty()
                                             where !skills.IsDeleted
                                                && skills.DomainID == domainId
                                                && employee.IsActive == isActive
                                                && skills.SkillsID == skillsId
                                                && skills.LevelID == levelId
                                             orderby skills.ModifiedOn descending
                                             select new SkillDetailsBo
                                             {
                                                 Id = skills.ID,
                                                 SkillsId = skills.SkillsID,
                                                 LevelId = skills.LevelID,
                                                 YearId = skills.Year,
                                                 MonthId = skills.Month,
                                                 LastUsedVersion = skills.LastUsedVersion,
                                                 LastUsedyearId = skills.LastUsedYearID ?? 0,
                                                 LastUsedYear = lastYear.Name,
                                                 EmployeeId = skills.EmployeeID,
                                                 FirstName = employee.Code + "  -  " + employee.FirstName
                                             }).ToList();
                return list;
            }
        }

        #endregion Employee Skill Matrix Report

        // create class for employee payroll, Employee Skills,
    }
}