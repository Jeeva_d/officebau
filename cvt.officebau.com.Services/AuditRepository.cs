﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace cvt.officebau.com.Services
{
    public class AuditRepository : BaseRepository
    {
        private readonly string _conString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        public DataTable SearchAudit(string tableName)
        {
            string query = "select * from " + tableName;
            using (SqlConnection con = new SqlConnection(_conString))
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            sda.Fill(ds);
                            return ds.Tables[0];
                        }
                    }
                }
            }
        }

        #region Audit Trial

        public DataTable AuditTrialList(AuditTrailBo auditTrailBo)
        {
            if (auditTrailBo.ToDate != null)
            {
                auditTrailBo.ToDate = Convert.ToDateTime(auditTrailBo.ToDate).AddDays(1);
            }

            using (SqlConnection con = new SqlConnection(_conString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.GET_AUDITTABLEDETAILS, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.DBName, string.Empty);
                    cmd.Parameters.AddWithValue(DBParam.Input.TableName, auditTrailBo.TableName);
                    cmd.Parameters.AddWithValue(DBParam.Input.FromDate, auditTrailBo.FromDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.ToDate, auditTrailBo.ToDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.UserID, Utility.UserId());
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            sda.Fill(ds);
                            return ds.Tables[0];
                        }
                    }
                }
            }
        }

        public List<cvt.officebau.com.ViewModels.Entity> SearchAutoCompleteAuditTableName(string tableName)
        {
            List<cvt.officebau.com.ViewModels.Entity> list = new List<cvt.officebau.com.ViewModels.Entity>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_AUTOCOMPLETEAUDITTABLENAME);

                Database.AddInParameter(dbCommand, DBParam.Input.DBName, DbType.String, string.Empty);
                Database.AddInParameter(dbCommand, DBParam.Input.TableName, DbType.String, tableName);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        list.Add(new  cvt.officebau.com.ViewModels.Entity
                        {
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name])
                        });
                    }
                }
            }

            return list;
        }

        public string UpdateAuditTables()
        {
            string message = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.UPDATE_AUDITTABLE);

                Database.AddInParameter(dbCommand, DBParam.Input.DBName, DbType.String, string.Empty);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        message = Utility.CheckDbNull<string>(reader[DBParam.Output.Result]);
                    }
                }
            }

            return message;
        }

        #endregion Audit Trial
    }
}