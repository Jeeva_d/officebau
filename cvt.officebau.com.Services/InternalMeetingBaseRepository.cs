﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Data;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class InternalMeetingBaseRepository : BaseRepository
    {
        public FSMEntities _datacontext = new FSMEntities();
        private int _domainId = Utility.DomainId();

        public IQueryable<tbl_EmployeeMaster> EmployeeList()
        {
            return (from employee in _datacontext.tbl_EmployeeMaster
                    where employee.DomainID == _domainId
                    select employee);
        }

        public IQueryable<tbl_InternalMeeting> InternalMeetingList()
        {
            return (from internalmeeting in _datacontext.tbl_InternalMeeting
                    where internalmeeting.DomainId == _domainId
                    select internalmeeting);
        }
        public IQueryable<tbl_InternalMeeting_Type> InternalMeetingTypeList()
        {
            return (from InternalMeetingTypeList in _datacontext.tbl_InternalMeeting_Type
                    where InternalMeetingTypeList.DomainId == _domainId
                    select InternalMeetingTypeList);
        }


        public tbl_InternalMeeting GetInternalMeeting_ById(int id)
        {
            return (from a in _datacontext.tbl_InternalMeeting
                    where a.Id == id && a.DomainId == _domainId
                    select a).FirstOrDefault();
        }

        public IQueryable<tbl_InternalMeeting_Task> TaskList(int ImId)
        {
            return (from task in _datacontext.tbl_InternalMeeting_Task
                    where task.IMId == ImId && task.DomainId == _domainId
                    select task);
        }

        public tbl_InternalMeeting_Task GetInternalMeeting_Task_ById(int id)
        {
            return (from a in _datacontext.tbl_InternalMeeting_Task
                    where a.Id == id && a.DomainId == _domainId
                    select a).FirstOrDefault();
        }

        public IQueryable<tbl_InternalMeeting_Task> TaskListByUserId(int userId)
        {
            return (from task in _datacontext.tbl_InternalMeeting_Task
                    where task.AssignedToId == userId
                    select task);
        }

        public IQueryable<tbl_EmployeeTask> EmployeeTaskList(int taskId)
        {
            return (from employeeTask in _datacontext.tbl_EmployeeTask
                    where employeeTask.TaskId == taskId
                    select employeeTask);
        }

        public tbl_EmployeeTask GetEmployeeTask(int id)
        {
            return (from a in _datacontext.tbl_EmployeeTask
                    where a.Id == id
                    select a).FirstOrDefault();
        }

        public string SaveInternalMeeting(InternalMeetingBO internalMeetingBO)
        {
            DateTime dateNow = DateTime.Now;

            tbl_InternalMeeting internalMeeting = new tbl_InternalMeeting
            {
                Title = internalMeetingBO.Title,
                MeetingDate = internalMeetingBO.MeetingDate.Value,
                MeetingTime = internalMeetingBO.MeetingTime,
                Location = internalMeetingBO.Location,
                OrganizedById = internalMeetingBO.OrganizerId,
                NextMeetingDate = internalMeetingBO.NextMeetingDate,
                TypeId = internalMeetingBO.MeetingTypeId,
                PresenterId = internalMeetingBO.PresenterId,
                Attendees = internalMeetingBO.Attendees,
                Description=internalMeetingBO.Description,
                DomainId = internalMeetingBO.DomainId,
                CreatedBy = internalMeetingBO.CreatedBy,
                CreatedOn = dateNow,
                ModifiedBy = internalMeetingBO.ModifiedBy,
                ModifiedOn = dateNow,
                HistoryId = Guid.NewGuid()
            };

            _datacontext.tbl_InternalMeeting.Add(internalMeeting);
            _datacontext.SaveChanges();
            return Notification.Inserted;
        }

        public string UpdateInternalMeeting(InternalMeetingBO internalMeetingBO)
        {
            string message;
            var internalMeeting = GetInternalMeeting_ById(internalMeetingBO.Id);

            if (internalMeeting != null)
            {
                internalMeeting.Title = internalMeetingBO.Title;
                internalMeeting.MeetingDate = internalMeetingBO.MeetingDate.Value;
                internalMeeting.MeetingTime = internalMeetingBO.MeetingTime;
                internalMeeting.Location = internalMeetingBO.Location;
                internalMeeting.OrganizedById = internalMeetingBO.OrganizerId;
                internalMeeting.NextMeetingDate = internalMeetingBO.NextMeetingDate;
                internalMeeting.TypeId = internalMeetingBO.MeetingTypeId;
                internalMeeting.PresenterId = internalMeetingBO.PresenterId;
                internalMeeting.Attendees = internalMeetingBO.Attendees;
                internalMeeting.Description = internalMeetingBO.Description;
                internalMeeting.ModifiedBy = internalMeetingBO.ModifiedBy;
                internalMeetingBO.ModifiedOn = DateTime.Now;
                _datacontext.Entry(internalMeeting).State = EntityState.Modified;
                _datacontext.SaveChanges();
                message = Notification.Updated;
            }
            else
            {
                message = Constants.Notify_Message_RecordReferred;
            }
            return message;
        }
    }
}