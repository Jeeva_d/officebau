﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Services
{
    public class ApplicationConfigurationRepository : BaseRepository
    {
        #region Report

        public DataTable GetBusinessAccessReport(int employeeId)
        {
            DataSet ds = new DataSet();
            DataTable dtReport = new DataTable("ApplicationConfigurationreport");
            ds.Tables.Add(dtReport);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_BUSINESSACCESSREPORT);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtReport);
                }

                return dtReport;
            }
        }

        #endregion Report

        #region Search CostCenter

        public int SearchCostCenter()
        {
            int costCenter = 0;
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_COSTCENTER);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        costCenter = Convert.ToInt32(reader[DBParam.Output.Value]);
                    }
                }

                return costCenter;
            }
        }

        #endregion Search CostCenter

        public string ManageEmailConfiguration(ApplicationConfigBo applicationConfigBo)
        {
            string result;
            using (DbConnection dbCon = Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("ManageEmailConfiguration");
                dbCon.Open();
                Database.AddInParameter(dbCommand, "@To", DbType.String, applicationConfigBo.To);
                Database.AddInParameter(dbCommand, "@CC", DbType.String, applicationConfigBo.Cc);
                Database.AddInParameter(dbCommand, "@BCC", DbType.String, applicationConfigBo.Description);
                Database.AddInParameter(dbCommand, "@Key", DbType.String, applicationConfigBo.Code);
                Database.AddInParameter(dbCommand, "@IsAttachment", DbType.Boolean, applicationConfigBo.IsAttachment);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                result = Database.ExecuteScalar(dbCommand).ToString();
                dbCon.Close();
            }

            return result;
        }

        public ApplicationConfigBo GetEmailConfig(string key)
        {
            ApplicationConfigBo applicationConfigBo = new ApplicationConfigBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetEmailConfig");
                Database.AddInParameter(dbCommand, "@Key", DbType.String, key);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        applicationConfigBo = new ApplicationConfigBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            To = Utility.CheckDbNull<string>(reader["Toid"]),
                            Cc = Utility.CheckDbNull<string>(reader["CC"]),
                            Code = Utility.CheckDbNull<string>(reader["EmailKey"]),
                            IsAttachment = Utility.CheckDbNull<bool>(reader["IsAttachment"]),
                            Description = Utility.CheckDbNull<string>(reader["BCC"]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy])
                        };
                    }
                }
            }

            return applicationConfigBo;
        }

        public List<ApplicationConfigBo> SearchEmailConfig()
        {
            List<ApplicationConfigBo> applicationConfigBoList = new List<ApplicationConfigBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("[SearchEmailConfig]");
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        applicationConfigBoList.Add(new ApplicationConfigBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            To = Utility.CheckDbNull<string>(reader["Toid"]),
                            Cc = Utility.CheckDbNull<string>(reader["CC"]),
                            Code = Utility.CheckDbNull<string>(reader["EmailKey"]),
                            IsAttachment = Utility.CheckDbNull<bool>(reader["IsAttachment"]),
                            Description = Utility.CheckDbNull<string>(reader["BCC"]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy])
                        });
                    }
                }
            }

            return applicationConfigBoList;
        }

        public ApplicationConfigBo GetMailInformation(ApplicationConfigBo applicationConfigBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetEmailConfig");
                Database.AddInParameter(dbCommand, "@Key", DbType.String, applicationConfigBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, applicationConfigBo.Type);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        applicationConfigBo = new ApplicationConfigBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID])
                        };
                    }
                }
            }

            return applicationConfigBo;
        }

        #region Application Configuration

        public List<ApplicationConfigurationBo> GetApplicationConfiguration(ApplicationConfigurationBo applicationConfigurationBo)
        {
            List<ApplicationConfigurationBo> businessBoList = new List<ApplicationConfigurationBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_APPLICATIONCONFIGURATION);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, applicationConfigurationBo.BusinessUnitId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        applicationConfigurationBo = new ApplicationConfigurationBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ConfigCode = Utility.CheckDbNull<string>(reader[DBParam.Output.ConfigCode]),
                            ConfigDescription = Utility.CheckDbNull<string>(reader[DBParam.Output.ConfigDescription]),
                            ControlType = Utility.CheckDbNull<string>(reader[DBParam.Output.ConfigControlType])
                        };

                        if (applicationConfigurationBo.ControlType.ToUpper() == "CHECKBOX")
                        {
                            applicationConfigurationBo.Checkbox = Convert.ToBoolean(Convert.ToInt32(reader[DBParam.Output.ConfigValue]));
                        }

                        if (applicationConfigurationBo.ControlType.ToUpper() == "TEXTBOX" || applicationConfigurationBo.ControlType.ToUpper() == "DROPDOWN")
                        {
                            applicationConfigurationBo.Textbox = Utility.CheckDbNull<string>(reader[DBParam.Output.ConfigValue].ToString());
                        }

                        applicationConfigurationBo.BusinessUnitId = Utility.CheckDbNull<int>(reader[DBParam.Output.BusinessUnitID]);
                        applicationConfigurationBo.ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]);
                        applicationConfigurationBo.Operation = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]);
                        applicationConfigurationBo.ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]);

                        businessBoList.Add(applicationConfigurationBo);
                    }
                }

                return businessBoList;
            }
        }

        public string ManageApplicationConfiguration(List<ApplicationConfigurationBo> applicationConfigurationBoList)
        {
            string userMessage = string.Empty;

            using (DbConnection dbCon = Database.CreateConnection())
            {
                foreach (ApplicationConfigurationBo applicationConfigurationBo in applicationConfigurationBoList)
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_APPLICATIONCONFIGURATION);
                    dbCon.Open();
                    Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, applicationConfigurationBo.Id);
                    Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, applicationConfigurationBo.BusinessUnitId);
                    Database.AddInParameter(dbCommand, DBParam.Input.ConfigCode, DbType.String, applicationConfigurationBo.ConfigCode);
                    if (applicationConfigurationBo.ControlType.ToUpper() == "CHECKBOX")
                    {
                        Database.AddInParameter(dbCommand, DBParam.Input.ConfigValue, DbType.String, applicationConfigurationBo.Checkbox ? 1 : 0);
                    }

                    if (applicationConfigurationBo.ControlType.ToUpper() == "TEXTBOX" || applicationConfigurationBo.ControlType.ToUpper() == "THEMESELECTION" || applicationConfigurationBo.ControlType.ToUpper() == "DROPDOWN")
                    {
                        Database.AddInParameter(dbCommand, DBParam.Input.ConfigValue, DbType.String, applicationConfigurationBo.Textbox);
                    }

                    Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    userMessage = Database.ExecuteScalar(dbCommand).ToString();
                    dbCon.Close();
                }
            }

            return userMessage;
        }

        #endregion Application Configuration

        #region Payroll Component Configuration

        public List<ApplicationConfigurationBo> GetPayrollComponentConfiguration(ApplicationConfigurationBo applicationConfigurationBo)
        {
            List<ApplicationConfigurationBo> businessBoList = new List<ApplicationConfigurationBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_PAYROLLCOMPONENTCONFIGURATION);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, applicationConfigurationBo.BusinessUnitId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        applicationConfigurationBo = new ApplicationConfigurationBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ComponentId = Utility.CheckDbNull<int>(reader[DBParam.Output.ComponentID]),
                            PayrollCode = Utility.CheckDbNull<string>(reader[DBParam.Output.PayrollCode]),
                            PayrollDescription = Utility.CheckDbNull<string>(reader[DBParam.Output.PayrollDescription]),
                            PayrollControlType = Utility.CheckDbNull<string>(reader[DBParam.Output.PayrollControlType]),
                            Checkbox = Convert.ToBoolean(Convert.ToInt32(reader[DBParam.Output.PayrollConfigValue])),
                            BusinessUnitId = Utility.CheckDbNull<int>(reader[DBParam.Output.BusinessUnitID]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            Operation = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn])
                        };

                        businessBoList.Add(applicationConfigurationBo);
                    }
                }

                return businessBoList;
            }
        }

        public string ManagePayrollComponentConfiguration(List<ApplicationConfigurationBo> applicationConfigurationBoList)
        {
            string userMessage = string.Empty;

            using (DbConnection dbCon = Database.CreateConnection())
            {
                foreach (ApplicationConfigurationBo applicationConfigurationBo in applicationConfigurationBoList)
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_PAYROLLCOMPONENTCONFIGURATION);
                    dbCon.Open();
                    Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, applicationConfigurationBo.Id);
                    Database.AddInParameter(dbCommand, DBParam.Input.ComponentID, DbType.Int32, applicationConfigurationBo.ComponentId);
                    Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, applicationConfigurationBo.BusinessUnitId);
                    Database.AddInParameter(dbCommand, DBParam.Input.PayrollConfigCode, DbType.String, applicationConfigurationBo.PayrollConfigCode);
                    Database.AddInParameter(dbCommand, DBParam.Input.PayrollConfigValue, DbType.Boolean, applicationConfigurationBo.Checkbox);
                    Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    userMessage = Database.ExecuteScalar(dbCommand).ToString();
                    dbCon.Close();
                }
            }

            return userMessage;
        }

        #endregion Payroll Component Configuration

        #region Account Configuration

        #region Get ApplicationConfiguration

        public ApplicationConfigBo GetApplicationAccountConfiguration()
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_APPLICATIONACCOUNTCONFIGURATION);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                ApplicationConfigBo applicationConfigBo = new ApplicationConfigBo();

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        if (reader[DBParam.Output.Code].ToString() == "CURNY")
                        {
                            applicationConfigBo.CurrencyName = Utility.CheckDbNull<string>(reader[DBParam.Output.Value]);
                        }

                        if (reader[DBParam.Output.Code].ToString() == "STARTMTH")
                        {
                            applicationConfigBo.StartMonthName = Utility.CheckDbNull<string>(reader[DBParam.Output.Value]);
                        }

                        if (reader[DBParam.Output.Code].ToString() == "NRECORD")
                        {
                            applicationConfigBo.NoOfRecords = Utility.CheckDbNull<string>(reader[DBParam.Output.Value]);
                        }

                        if (reader[DBParam.Output.Code].ToString() == "COSTCT")
                        {
                            applicationConfigBo.IsCostCenter = reader[DBParam.Output.Value].ToString() == "1";
                        }

                        applicationConfigBo.Id = Utility.CheckDbNull<int>(reader[DBParam.Output.Count]);
                        applicationConfigBo.CostcenterCount = Utility.CheckDbNull<int>(reader[DBParam.Output.CostcenterCount]);
                        applicationConfigBo.ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]);
                        applicationConfigBo.ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]);
                    }
                }

                return applicationConfigBo;
            }
        }

        #endregion Get ApplicationConfiguration

        #region Manage ApplicationConfiguration

        public ApplicationConfigBo ManageApplicationAccountConfiguration(List<ApplicationConfigBo> list)
        {
            ApplicationConfigBo appBo = new ApplicationConfigBo();
            using (DbConnection dbCon = Database.CreateConnection())
            {
                foreach (ApplicationConfigBo item in list)
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_APPLICATIONACCOUNTCONFIGURATION);
                    dbCon.Open();
                    Database.AddInParameter(dbCommand, DBParam.Input.Code, DbType.String, item.Code);
                    Database.AddInParameter(dbCommand, DBParam.Input.Value, DbType.String, item.Value);
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                    string result = Database.ExecuteScalar(dbCommand).ToString();
                    appBo.UserMessage = result;
                    dbCon.Close();
                }
            }

            return appBo;
        }

        #endregion Manage ApplicationConfiguration

        #endregion Account Configuration

        #region Screen for Others User Access

        public List<MasterDataBo> GetOtherUserAcessList()
        {
            using (FSMEntities context = new FSMEntities())
            {
                var list = (from a in context.tbl_OtherUserAccess
                            orderby a.ScreenName ascending
                            select new MasterDataBo
                            {
                                Id = a.Id,
                                Code = a.ScreenName,
                                Parameters = a.DesignationIds
                            }).ToList();
                return list;
            }
        }

        public string SaveOtherUserAccess(int id, string deslist)
        {
            DateTime date = DateTime.Now;
            int userid = Utility.UserId();
            using (FSMEntities context = new FSMEntities())
            {
                var otherUserAccess = context.tbl_OtherUserAccess.FirstOrDefault(a => a.Id == id);

                if (otherUserAccess != null)
                {
                    otherUserAccess.DesignationIds = deslist;
                    otherUserAccess.ModifiedBy = userid;
                    otherUserAccess.ModifiedOn = date;
                    context.Entry(otherUserAccess).State = EntityState.Modified;
                    context.SaveChanges();
                    return Notification.Updated;
                }
            }

            return BOConstants.Error;
        }

        #endregion Screen for Others User Access

        #region Category Mapping 

        public List<DocumentManagerBO> GetCategoryMappingList()
        {
            using (FSMEntities context = new FSMEntities())
            {
                var list = (from b in context.tbl_EmployeeMaster
                            where !(b.IsDeleted) && (b.IsActive == false)
                            orderby b.FirstName ascending
                            select new DocumentManagerBO
                            {
                                EmployeeId = b.ID,
                                FirstName = b.FirstName,
                            }).ToList();
                return list;
            }
        }

        public DocumentManagerBO GetDocumentCategoryMapping(int employeeId)
        {
            DocumentManagerBO documentManagerBo = new DocumentManagerBO();
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();
                if (employeeId != 0)
                {
                    documentManagerBo = (from c in context.tbl_DocumentCategoryMapping
                             where c.DomainId == domainId && c.EmployeeId == employeeId
                             select new DocumentManagerBO
                             {
                                 EmployeeId = c.EmployeeId,
                                 Id = c.Id,
                                 CategoryId = c.CategoryIds
                             }).FirstOrDefault();
                }
                return documentManagerBo;
            }
        }

        public string SaveDocumentCategoryMapping(DocumentManagerBO documentManagerBO)
        {
            DateTime date = DateTime.Now;
            int userid = Utility.UserId();

            using (FSMEntities context = new FSMEntities())
            {
                int check = 0;
                int domainId = Utility.DomainId();
                if (documentManagerBO.Id == 0)
                {
                    check = context.tbl_DocumentCategoryMapping.Count(s => s.EmployeeId == documentManagerBO.EmployeeId && !s.IsDeleted && s.DomainId == domainId);

                    if (check == 0)
                    {
                        tbl_DocumentCategoryMapping documentCategoryMapping = new tbl_DocumentCategoryMapping
                        {
                            EmployeeId = documentManagerBO.EmployeeId,
                            CategoryIds = documentManagerBO.CategoryId.ToString(),
                            IsDeleted = documentManagerBO.IsDeleted,
                            CreatedBy = Utility.UserId(),
                            CreatedOn = date,
                            ModifiedOn = date,
                            ModifiedBy = Utility.UserId(),
                            DomainId = domainId
                        };
                        context.tbl_DocumentCategoryMapping.Add(documentCategoryMapping);
                        context.SaveChanges();

                        return Notification.Updated;
                    }
                    return Notification.AlreadyExists;
                }
                else
                {
                        var categoryMapping = context.tbl_DocumentCategoryMapping.FirstOrDefault(b => b.Id == documentManagerBO.Id);
                        if (categoryMapping != null)
                        {
                            categoryMapping.EmployeeId = documentManagerBO.EmployeeId;
                            categoryMapping.CategoryIds = documentManagerBO.CategoryId.ToString();
                            categoryMapping.ModifiedBy = userid;
                            categoryMapping.ModifiedOn = date;
                            context.Entry(categoryMapping).State = EntityState.Modified;
                            context.SaveChanges();
                            return Notification.Updated;
                        }
                }
                return BOConstants.Error;
            }

            #endregion  Category Mapping 
        }
    }
}
