﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class UserLoginRepository : BaseRepository
    {
        #region Validatate UserLogin

        public UserLoginBo ValidateUserLogin(UserLoginBo userLoginBo)
        {
            UserLoginBo userBo = new UserLoginBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.VALIDATEUSER);

                Database.AddInParameter(dbCommand, DBParam.Input.UserName, DbType.String, userLoginBo.UserName);
                Database.AddInParameter(dbCommand, DBParam.Input.Password, DbType.String, userLoginBo.Password);
                Database.AddInParameter(dbCommand, DBParam.Input.CompanyID, DbType.String, userLoginBo.CompanyId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userBo = new UserLoginBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage]),
                            UserId = Utility.CheckDbNull<int>(reader[DBParam.Output.UserID]),
                            DomainId = Utility.CheckDbNull<int>(reader[DBParam.Output.DomainID]),
                            UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.UserName]),
                            FirstName = Utility.CheckDbNull<string>(reader[DBParam.Output.FirstName]),
                            CompanyName = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyName]),
                            LastLogin = Utility.CheckDbNull<string>(reader[DBParam.Output.LastLogin]),
                            CurrencySymbol = Utility.CheckDbNull<string>(reader[DBParam.Output.CurrencySymbol]),
                            Designation = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]),
                            FilePath = Utility.CheckDbNull<string>(reader[DBParam.Output.FileName]),
                            FileId = Utility.CheckDbNull<Guid>(reader[DBParam.Output.FileUploadID]),
                            MenuUrl = Utility.CheckDbNull<string>(reader[DBParam.Output.MenuURL]),
                            IsPasswordChanged = Convert.ToBoolean(reader[DBParam.Output.IsPasswordChanged]),
                            Gender = Utility.CheckDbNull<string>(reader[DBParam.Output.Gender])
                        };
                        Utility.SetSession("StartMonth", Utility.CheckDbNull<int>(reader[DBParam.Output.StartMonth]));
                        Utility.SetSession("CommunicationEmailId", Utility.CheckDbNull<string>(reader[DBParam.Output.CommunicationEmailId]));
                        Utility.SetSession("ColorPalette", Utility.CheckDbNull<string>(reader["ColorPalette"]));
                        Utility.SetSession("IsSuperAdmin", Convert.ToBoolean(reader["IsSuperUser"]).ToString());

                    }
                }
            }

            return userBo;
        }

        #endregion Validatate UserLogin

        #region Validatate OldPassword

        public UserLoginBo ValidateOldPassword(UserLoginBo userLoginBo)
        {
            UserLoginBo userBo = new UserLoginBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.VALIDATEOLDPASSWORD);

                Database.AddInParameter(dbCommand, DBParam.Input.UserName, DbType.String, userLoginBo.UserName);
                Database.AddInParameter(dbCommand, DBParam.Input.Password, DbType.String, userLoginBo.Password);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userBo = new UserLoginBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage]),
                            UserId = Utility.CheckDbNull<int>(reader[DBParam.Output.UserID])
                        };
                    }
                }
            }

            return userBo;
        }

        #endregion Validatate OldPassword

        #region Manage Password Audit

        public UserLoginBo ManagePasswordAudit(UserLoginBo userLoginBo)
        {
            UserLoginBo userBo = new UserLoginBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGEPASSWORDAUDIT);

                Database.AddInParameter(dbCommand, DBParam.Input.UserName, DbType.String, userLoginBo.UserName);
                Database.AddInParameter(dbCommand, DBParam.Input.GUID, DbType.String, userLoginBo.Guid);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userBo = new UserLoginBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage]),
                            UserId = Utility.CheckDbNull<int>(reader[DBParam.Output.UserID])
                        };
                    }
                }
            }

            return userBo;
        }

        #endregion Manage Password Audit

        #region ChangePassword

        public string ChangePassword(UserLoginBo userLoginBo)
        {
            UserLoginBo userBo = new UserLoginBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CHANGEPASSWORD);

                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, userLoginBo.UserId);
                Database.AddInParameter(dbCommand, DBParam.Input.Password, DbType.String, userLoginBo.ConfirmPassword);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userBo = new UserLoginBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage])
                        };
                    }
                }
            }

            return userBo.UserMessage;
        }

        #endregion ChangePassword

        #region SearchMenu

        public List<RbsMenuBo> SearchMenu(int userId, string rootConfig)
        {
            List<RbsMenuBo> menuList = new List<RbsMenuBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_RBSMENU);
                Database.AddInParameter(dbCommand, "@RootConfig", DbType.String, rootConfig);

                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, userId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        RbsMenuBo menuBo = new RbsMenuBo
                        {
                            Module = reader[DBParam.Output.Module].ToString(),
                            SubModule = reader[DBParam.Output.SubModule].ToString(),
                            SubMenuIcons = reader[DBParam.Output.SubMenuIcon].ToString(),
                            Menu = reader[DBParam.Output.Menu].ToString(),
                            MenuUrl = reader[DBParam.Output.MenuURL].ToString(),
                            MenuCode = reader[DBParam.Output.MenuCode].ToString(),
                            MorderId = reader[DBParam.Output.MOrderID].ToString(),
                            SubModuleUrl = reader[DBParam.Output.SMOrderID].ToString(),
                            IsMobility = Convert.ToInt32(reader[DBParam.Output.IsMobility])
                        };
                        menuList.Add(menuBo);
                    }
                }
            }

            return menuList;
        }

        #endregion SearchMenu

        #region Get CompanyList For User

        public List<SelectListItem> GetCompanyListForUser(string employeeCode)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_COMPANYLISTFORUSER);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCode, DbType.String, employeeCode);

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        #endregion Get CompanyList For User

        #region LandingPage

        public string GetStartScreen()
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();
                int employeeId = Utility.UserId();
                string moduleName = Convert.ToString(Utility.GetSession("ModuleName")).ToLower();

                string result = (from o in context.tbl_EmployeeOtherDetails
                                 from m in context.tbl_RBSMenu
                                                  .Where(mm => mm.MenuCode == o.StartMenuURL)
                                 where o.DomainID == domainId
                                    && o.EmployeeID == employeeId
                                    && ((m.RootConfig ?? "") == "" ? moduleName : m.RootConfig).ToLower().Contains(moduleName)
                                 select "/" + m.MenuURL + "?menuCode=" + m.MenuCode).FirstOrDefault();

                return result;
            }
        }

        #endregion LandingPage

        #region ResetPassword

        public string ResetPassword(UserLoginBo userLoginBo)
        {
            UserLoginBo userBo = new UserLoginBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RESETPASSWORD);

                Database.AddInParameter(dbCommand, DBParam.Input.UserName, DbType.String, userLoginBo.UserName);
                Database.AddInParameter(dbCommand, DBParam.Input.GUID, DbType.String, userLoginBo.Guid);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userBo = new UserLoginBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage])
                        };
                    }
                }
            }

            return userBo.UserMessage;
        }

        public string PopulateMailBody(string firstName, string username, string guid, string domainId)
        {
            string body;

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/ResetPassword.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{UserName}", username);
            body = body.Replace("{FirstName}", firstName);
            body = body.Replace("{GUID}", guid);
            body = body.Replace("{URL}", AppConstants.ApplicationRootUrl() + "/UserLogin/ResetPasswordUsingOtp?domainId=" + domainId);
            return body;
        }

        #endregion ResetPassword

        #region Manage Login History

        public int ManageLoginHistory(UserLoginBo userLoginBo)
        {
            CultureInfo customCulture = new CultureInfo("en-US", true) { DateTimeFormat = { ShortDatePattern = "yyyy-MM-dd" } };
            Thread.CurrentThread.CurrentCulture = customCulture;
            Thread.CurrentThread.CurrentUICulture = customCulture;

            string ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

            string device = HttpContext.Current.Request.Browser.IsMobileDevice ? "Mobile" : "desktop";

            HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
            string browserType = browser.Type;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGELOGINHISTORY);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, userLoginBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.IPAddress, DbType.String, ip);
                Database.AddInParameter(dbCommand, DBParam.Input.LoginTime, DbType.DateTime, DateTime.Now);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, userLoginBo.IsActive);
                Database.AddInParameter(dbCommand, DBParam.Input.PhysicalAddress, DbType.String, ip);
                Database.AddInParameter(dbCommand, DBParam.Input.BrowserType, DbType.String, browserType);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, userLoginBo.Location);
                Database.AddInParameter(dbCommand, DBParam.Input.Latitudelongitude, DbType.String, userLoginBo.Latitude);
                Database.AddInParameter(dbCommand, DBParam.Input.Device, DbType.String, device);
                Database.AddInParameter(dbCommand, "@ClientHostName", DbType.String, string.Empty);
                Database.AddInParameter(dbCommand, DBParam.Input.LogOutTimeFlag, DbType.Boolean, userLoginBo.LogOutTimeFlag);
                Database.AddInParameter(dbCommand, DBParam.Input.Region, DbType.String, string.Empty);
                Database.AddInParameter(dbCommand, DBParam.Input.LogOutType, DbType.String, userLoginBo.CompanyName);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                userLoginBo.LoginId = Convert.ToInt32(Database.ExecuteScalar(dbCommand));
                return userLoginBo.LoginId;
            }
        }

        public DataTable SearchBoardCast(string message)
        {
            string constring = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constring))
            {
                using (SqlCommand cmd = new SqlCommand(message, con))
                {
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        using (DataSet ds = new DataSet())
                        {
                            sda.Fill(ds);
                            return ds.Tables[0];
                        }
                    }
                }
            }
        }

        #endregion Manage Login History

        #region Login History

        public List<UserLoginBo> SearchLoginHistory(DateTime? fromDate, DateTime? toDate)
        {
            DateTime toDateTime = DateTime.Now;
            List<UserLoginBo> historyList = new List<UserLoginBo>();

            if (toDate != null)
            {
                string dateOnly = Convert.ToDateTime(toDate).ToShortDateString();
                toDateTime = Convert.ToDateTime(dateOnly + " 11:59:00 PM");
            }

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_LOGINHISTORY);

                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.DateTime, fromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.DateTime, toDate != null ? toDateTime : (object) null);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        UserLoginBo userLoginBo = new UserLoginBo
                        {
                            Id = Convert.ToInt32(reader[DBParam.Output.ID]),
                            FormHitCount = Convert.ToInt32(reader[DBParam.Output.Count]),
                            UserName = reader[DBParam.Output.EmployeeName].ToString(),
                            BrowserName = reader[DBParam.Output.BrowserName].ToString(),
                            LoginTime = Convert.ToDateTime(reader[DBParam.Output.LoginTime])
                                .ToString("dd-MMM-yyyy HH:mm:ss tt"),
                            LogoutTime = string.IsNullOrWhiteSpace(reader[DBParam.Output.LogOutTime].ToString())
                                ? string.Empty
                                : Convert.ToDateTime(reader[DBParam.Output.LogOutTime])
                                    .ToString("dd-MMM-yyyy HH:mm:ss tt"),
                            IpAddress = reader[DBParam.Output.IPAddress].ToString(),
                            Location = reader[DBParam.Output.Location].ToString(),
                            Latitude = reader[DBParam.Output.LatitudeLongitude].ToString()
                        };
                        historyList.Add(userLoginBo);
                    }
                }
            }

            return historyList;
        }

        public List<UserLoginBo> SearchLoginFormHistory(int loginHistoryId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<UserLoginBo> list = (from e in context.tbl_LoginForms
                                          where e.DomainID == domainId && e.LoginHistoryID == loginHistoryId
                                          orderby e.CreatedOn descending
                                          select new UserLoginBo
                                          {
                                              FormName = e.FormName,
                                              LoginHistoryTime = e.CreatedOn
                                          }).ToList();
                return list;
            }
        }

        #endregion Login History

        #region Search CurrentEvents

        public List<BaseBo> SearchCurrentEvents()
        {
            List<BaseBo> historyList = new List<BaseBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_CURRENTEVENTS);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BaseBo userLoginBo = new BaseBo
                        {
                            Id = Convert.ToInt32(reader[DBParam.Output.ID]),
                            UserName = reader[DBParam.Output.Name].ToString(),
                            UserMessage = reader[DBParam.Output.EvenType].ToString(),
                            Operation = reader[DBParam.Output.Type].ToString()
                        };
                        historyList.Add(userLoginBo);
                    }
                }
            }

            return historyList;
        }

        public string SendWishes(BaseBo baseBo)
        {
            if (baseBo.Id == 0)
            {
                baseBo.UserName = baseBo.UserMessage;
                baseBo.UserMessage = null;
            }

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SENDWISHESH);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, baseBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.FromID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.TOID, DbType.Int32, baseBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.Oldmessage, DbType.String, baseBo.UserMessage);
                Database.AddInParameter(dbCommand, DBParam.Input.Message, DbType.String, baseBo.UserName);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, baseBo.Operation);
                var ret = Database.ExecuteScalar(dbCommand).ToString();
                return ret;
            }
        }

        public string MarkMessageRead(int messageId, int isViewed)
        {
            string result;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.NOTIFICATION_MESSAGEREAD);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, messageId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                result = Database.ExecuteScalar(dbCommand).ToString();
            }

            return result;
        }

        #endregion Search CurrentEvents

        #region Landing Screen Modules

        public string GetCompanyModules()
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();

                string result = (from o in context.tbl_Company
                                 where o.ID == domainId && o.IsDeleted == false
                                 select o.Modules).FirstOrDefault();

                return result;
            }
        }

        #endregion Landing Screen Modules

        public string SetPinForMobileUser(UserLoginBo userLoginBo)
        {
            UserLoginBo userBo = new UserLoginBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SetPinForMobileUser");

                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, userLoginBo.UserId);
                Database.AddInParameter(dbCommand, "@DeviceInfo", DbType.String, Utility.GetSession("deviceId"));
                Database.AddInParameter(dbCommand, "@Pin", DbType.String, userLoginBo.Pin);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, "@OverwriteDevice", DbType.String, userLoginBo.UserMessage);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userBo = new UserLoginBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage])
                        };
                    }
                }
            }

            return userBo.UserMessage;
        }

        public UserLoginBo GetValidUserForPin(UserLoginBo userLoginBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetValidUserForPin");

                Database.AddInParameter(dbCommand, "@DeviceInfo", DbType.String, Utility.GetSession("deviceId"));
                Database.AddInParameter(dbCommand, "@Pin", DbType.String, userLoginBo.Pin);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userLoginBo = new UserLoginBo()
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage]),
                            UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.UserName]),
                            Password = Utility.CheckDbNull<string>(reader["Password"]),
                            CompanyId = Utility.CheckDbNull<int>(reader["companyid"])
                        };
                    }
                }
            }

            return userLoginBo;
        }
    }
}