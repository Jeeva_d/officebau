﻿using cvt.officebau.com.MessageConstants;
using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace cvt.officebau.com.Services
{
    public class AdminRepository : BaseRepository
    {
        public List<AdminBo> GetCompanyAdminList(AdminBo adminBo)
        {
            List<AdminBo> adminBoList = new List<AdminBo>();

            using (Database.CreateConnection())
            {
                System.Data.Common.DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_NEWADMIN);

                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, adminBo.CompanyName);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        adminBoList.Add(
                            new AdminBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                CompanyName = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyName]),
                                ActiveCount = Utility.CheckDbNull<int>(reader[DBParam.Output.ActiveCount]),
                                BusinessUnitCount = Utility.CheckDbNull<int>(reader[DBParam.Output.BusinessUnitCount]),
                                NoOfEmployees = Utility.CheckDbNull<int>(reader[DBParam.Output.NoOfEmployees]),
                                IsActive = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsActive]),
                                EmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]),
                                ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                                CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.CreatedOn])
                            });
                    }
                }
            }

            return adminBoList;
        }

        public string InActiveEmployees(AdminBo adminBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                tbl_Company active = context.tbl_Company.FirstOrDefault(b => b.ID == adminBo.Id);
                if (active != null)
                {
                    active.IsActive = adminBo.IsActive;
                    active.ModifiedOn = DateTime.Now;
                    active.ModifiedBy = adminBo.ModifiedBy;

                    context.Entry(active).State = EntityState.Modified;
                }

                context.SaveChanges();
                return adminBo.IsActive ? "The Customer is Inactivated Successfully." : "The Customer is Activated Successfully.";
            }
        }

        public string GetEmployeeName(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                if (context.tbl_EmployeeMaster.Count(a => a.ID == id) > 0)
                {
                    return context.tbl_EmployeeMaster.FirstOrDefault(a => a.ID == id)?.FullName;
                }

                return "";
            }
        }

        #region Search AutoComplete CompanyName

        public List<AdminBo> SearchAutoCompleteCompanyName(string companyName)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<AdminBo> list = (from t in context.tbl_Company
                                      where !(t.IsDeleted ?? false) && t.FullName.Contains(companyName)
                                      orderby t.FullName
                                      select new AdminBo
                                      {
                                          CompanyName = t.FullName ?? string.Empty,
                                          Id = t.ID
                                      }).ToList();
                return list;
            }
        }

        #endregion Search AutoComplete CompanyName

        public AdminBo GetAdminDetail(int id)
        {
            AdminBo adminBo = new AdminBo();

            using (Database.CreateConnection())
            {
                System.Data.Common.DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_ADMINFORCOMPANY);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        adminBo = new AdminBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            CompanyName = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyName]),
                            ModuleNames = Utility.CheckDbNull<string>(reader[DBParam.Output.Modules]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            EmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]),
                            ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            LogoUrl = Utility.CheckDbNull<string>(reader[DBParam.Output.Logo])
                        };
                    }
                }
            }

            return adminBo;
        }

        public AdminBo GetCompanyEmailDetails(int companyId, string defaultPassword)
        {
            using (FSMEntities context = new FSMEntities())
            {
                AdminBo result = (from l in context.tbl_Company
                                  join e in context.tbl_EmployeeMaster on l.ID equals e.DomainID
                                  join q in context.tbl_Login on l.ID equals q.DomainID
                                  join bu in context.tbl_BusinessUnit on l.ID equals bu.DomainID
                                  where l.ID == companyId && !(l.IsDeleted ?? false)
                                                          && !q.IsChanged && e.FirstName.Contains("Admin")
                                                          && bu.ParentID != 0 && !(bu.IsDeleted ?? false)
                                  select new AdminBo
                                  {
                                      Id = l.ID,
                                      EmailId = e.EmailID,
                                      ContactNo = l.ContactNo,
                                      CompanyName = l.Name,
                                      Password = defaultPassword,
                                      BusinessUnit = bu.Name,
                                      Operation = e.LoginCode
                                  }).FirstOrDefault();

                return result;
            }
        }

        public string ManageAdminDetails(AdminBo adminBo)
        {
            string defaultPassword = ConfigurationManager.AppSettings["DefaultPassword"];

            using (Database.CreateConnection())
            {
                System.Data.Common.DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CREATE_BUSINESSACCESS);
                Database.AddInParameter(dbCommand, DBParam.Input.CompanyName, DbType.String, adminBo.CompanyName);
                Database.AddInParameter(dbCommand, DBParam.Input.EmailID, DbType.String, adminBo.EmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.Modules, DbType.String, adminBo.ModuleNames);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, adminBo.BusinessUnit);
                Database.AddInParameter(dbCommand, DBParam.Input.DefaultPassword, DbType.String, EncryptionUtility.GetHashedValue(defaultPassword));
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, adminBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.Logo, DbType.String, adminBo.LogoUrl);

                adminBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return adminBo.UserMessage;
        }

        public string PopulateCompanyMailBody(string companyName, string mailId, string contactNo, string defaultPassword, string loginCode)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/CompanyDetails.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Name}", companyName);
            body = body.Replace("{Content}", "");
            body = body.Replace("{EmailID}", mailId);
            body = body.Replace("{ContactNo}", contactNo);
            body = body.Replace("{UserID}", loginCode);
            body = body.Replace("{DefaultPassword}", defaultPassword);
            return body;
        }

        public string UpdateAdminForCompany(AdminBo adminBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                tbl_Company company = context.tbl_Company.FirstOrDefault(b => b.ID == adminBo.Id);
                if (company != null)
                {
                    company.Name = adminBo.CompanyName;
                    company.FullName = adminBo.CompanyName;
                    company.ContactNo = adminBo.ContactNo;
                    company.EmailID = adminBo.EmailId;
                    company.Modules = adminBo.ModuleNames;
                    company.ModifiedOn = DateTime.Now;
                    company.ModifiedBy = Utility.UserId();
                    context.Entry(company).State = EntityState.Modified;
                }

                context.SaveChanges();

                context.tbl_BusinessUnit.Where(b => b.DomainID == adminBo.Id).ToList().ForEach(b =>
                {
                    b.Name = adminBo.BusinessUnit;
                    b.Remarks = adminBo.BusinessUnit;
                    b.Address = adminBo.BusinessUnit;
                    b.ModifiedOn = DateTime.Now;
                    b.ModifiedBy = adminBo.ModifiedBy;
                    context.Entry(b).State = EntityState.Modified;
                    context.SaveChanges();
                });

                return Notification.Updated;
            }
        }

        public string DeleteAdminForCompany(AdminBo adminBo)
        {
            using (Database.CreateConnection())
            {
                System.Data.Common.DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DELETE_COMPANYANDDEPENDENTRECORDS);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, adminBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                adminBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return adminBo.UserMessage;
        }

        #region Deployment

        public List<AdminBo> GetDeploymentList()
        {
            List<AdminBo> adminBoList = new List<AdminBo>();

            using (Database.CreateConnection())
            {
                System.Data.Common.DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_DEPLOYMENTACTIVITIESLIST);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        adminBoList.Add(
                            new AdminBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                CompanyName = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyName]),
                                FromDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.FromDate]),
                                ToDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ToDate]),
                                IsActive = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsActive]),
                                BroadcastMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.BroadcastMessage]),
                            });
                    }
                }
            }

            return adminBoList;
        }
        public AdminBo GetDeployment(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                AdminBo result = new AdminBo();

                if (id != null)
                {
                    result = (from c in context.DeploymentActivities
                              where c.ID == id && !(c.IsDeleted ?? false)
                              select new AdminBo
                              {
                                  Id = c.ID,
                                  BroadcastMessage = c.BrodcastMessage,
                                  IsActive = c.IsActive,
                                  FromDate = c.FromDate,
                                  ToDate = c.ToDate,
                                  CompanyIDs = c.DomainID
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public string ManageDeployment(AdminBo adminBo)
        {
            using (Database.CreateConnection())
            {
                System.Data.Common.DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_DEPLOYMENT);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, adminBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.BroadcastMessage, DbType.String, adminBo.BroadcastMessage);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, adminBo.IsActive);
                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.DateTime, adminBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.DateTime, adminBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.String, adminBo.CompanyIDs);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, adminBo.IsDeleted);
                adminBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return adminBo.UserMessage;
        }

        public string UpdateDeploymentActivityStatus(int id, bool value)
        {
            string message;
            using (FSMEntities context = new FSMEntities())
            {
                DeploymentActivity records = context.DeploymentActivities.FirstOrDefault(x => x.ID == id);
                if (records != null)
                {
                    records.IsActive = value;
                }

                context.SaveChanges();

                message = value ? "InActivated Successfully." : "Activated Successfully.";
            }
            return message;
        }

        #endregion Deployment
    }
}