﻿using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class EmployeeAppraisalSystemRepository : BaseRepository
    {
        #region variable

        private int _check;

        #endregion variable

        #region Appraisal System Configuration

        public List<AppraisalConfigBo> GetAppraisalSystemConfigurationList(AppraisalConfigBo appraisalConfigBo)
        {
            List<AppraisalConfigBo> appraisalConfigBoList = new List<AppraisalConfigBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_APPRAISALSYSTEMCONFIGURATIONLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, appraisalConfigBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        appraisalConfigBoList.Add(new AppraisalConfigBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            AppraisalName = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            BusinessUnitId = Utility.CheckDbNull<int>(reader[DBParam.Output.BusinessUnitID]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            AppraiseeStartDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.AppraiseeStartDate]),
                            AppraiseeEndDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.AppraiseeEndDate]),
                            AppraiserStartDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.AppraiserStartDate]),
                            AppraiserEndDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.AppraiserEndDate]),
                            EligibilityDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EligibilityDate])
                        });
                    }
                }

                return appraisalConfigBoList;
            }
        }

        public AppraisalConfigBo GetAppraisalSystemConfiguration(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                AppraisalConfigBo result = (from a in context.tbl_AppraisalConfiguration
                                            from e in context.tbl_EmployeeMaster
                                                  .Where(em => em.ID == a.ModifiedBy)
                                            where a.ID == id
                                            select new AppraisalConfigBo
                                            {
                                                Id = a.ID,
                                                AppraisalName = a.AppraisalName,
                                                BusinessUnitId = a.BUID,
                                                AppraiserStartDate = a.AppraiserStartDate,
                                                AppraiserEndDate = a.AppraiserEndDate,
                                                AppraiseeStartDate = a.AppraiseeStartDate,
                                                AppraiseeEndDate = a.AppraiseeEndDate,
                                                EligibilityDate = a.EligibilityDate,
                                                ModifiedOn = a.ModifiedOn,
                                                ModifiedByName = e.FullName
                                            }).FirstOrDefault();
                return result;
            }
        }

        public string CreateAppraisalSystemConfiguration(AppraisalConfigBo appraisalConfigBo)
        {
            int domainId = Utility.DomainId();
            using (FSMEntities context = new FSMEntities())
            {
                int dupCheck = context.tbl_AppraisalConfiguration.Count(a => !a.IsDeleted && a.DomainID == domainId && a.ID != appraisalConfigBo.Id && a.AppraisalName.Trim() == appraisalConfigBo.AppraisalName.Trim() && a.BUID == appraisalConfigBo.BusinessUnitId);

                string message;
                if (dupCheck == 0)
                {
                    tbl_AppraisalConfiguration appraisalConfig = new tbl_AppraisalConfiguration
                    {
                        AppraisalName = appraisalConfigBo.AppraisalName,
                        BUID = appraisalConfigBo.BusinessUnitId,
                        AppraiseeStartDate = appraisalConfigBo.AppraiseeStartDate,
                        AppraiseeEndDate = appraisalConfigBo.AppraiseeEndDate,
                        AppraiserStartDate = appraisalConfigBo.AppraiserStartDate,
                        AppraiserEndDate = appraisalConfigBo.AppraiserEndDate,
                        EligibilityDate = appraisalConfigBo.EligibilityDate,
                        IsDeleted = appraisalConfigBo.IsDeleted,
                        CreatedBy = appraisalConfigBo.CreatedBy,
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = appraisalConfigBo.ModifiedBy,
                        DomainID = appraisalConfigBo.DomainId,
                        HistoryID = Guid.NewGuid()
                    };

                    context.tbl_AppraisalConfiguration.Add(appraisalConfig);
                    context.SaveChanges();
                    message = Notification.Inserted;
                }
                else
                {
                    message = Notification.AlreadyExists;
                }
                return message;
            }
        }

        public string UpdateAppraisalSystemConfiguration(AppraisalConfigBo appraisalConfigBo)
        {
            int domainId = Utility.DomainId();
            using (FSMEntities context = new FSMEntities())
            {
                int dupCheck = context.tbl_AppraisalConfiguration.Count(a => !a.IsDeleted && a.DomainID == domainId && a.ID != appraisalConfigBo.Id && a.AppraisalName.Trim() == appraisalConfigBo.AppraisalName.Trim() && a.BUID == appraisalConfigBo.BusinessUnitId);

                string message;
                if (dupCheck == 0)
                {
                    tbl_AppraisalConfiguration appraisalConfig = context.tbl_AppraisalConfiguration.FirstOrDefault(a => a.ID == appraisalConfigBo.Id);
                    if (appraisalConfig != null)
                    {
                        appraisalConfig.AppraisalName = appraisalConfigBo.AppraisalName;
                        appraisalConfig.BUID = appraisalConfigBo.BusinessUnitId;
                        appraisalConfig.AppraiseeStartDate = appraisalConfigBo.AppraiseeStartDate;
                        appraisalConfig.AppraiseeEndDate = appraisalConfigBo.AppraiseeEndDate;
                        appraisalConfig.AppraiserStartDate = appraisalConfigBo.AppraiserStartDate;
                        appraisalConfig.AppraiserEndDate = appraisalConfigBo.AppraiserEndDate;
                        appraisalConfig.EligibilityDate = appraisalConfigBo.EligibilityDate;

                        appraisalConfig.IsDeleted = appraisalConfigBo.IsDeleted;
                        appraisalConfig.ModifiedOn = DateTime.Now;
                        appraisalConfig.ModifiedBy = appraisalConfigBo.ModifiedBy;
                        appraisalConfig.DomainID = appraisalConfigBo.DomainId;
                        appraisalConfig.HistoryID = Guid.NewGuid();

                        context.Entry(appraisalConfig).State = EntityState.Modified;
                    }

                    context.SaveChanges();
                    message = Notification.Updated;
                }
                else
                {
                    message = Notification.AlreadyExists;
                }

                return message;
            }
        }

        public string DeleteAppraisalSystemConfiguration(AppraisalConfigBo appraisalConfigBo)
        {
            int domainId = Utility.DomainId();
            using (FSMEntities context = new FSMEntities())
            {
                int checkReference = context.tbl_EmployeeAppraisal.Count(a => !a.IsDeleted && a.DomainID == domainId && a.AppraisalConfigurationID == appraisalConfigBo.Id);

                string message;
                if (checkReference == 0)
                {
                    tbl_AppraisalConfiguration appraisalConfig = context.tbl_AppraisalConfiguration.FirstOrDefault(a => a.ID == appraisalConfigBo.Id);

                    if (appraisalConfig != null)
                    {
                        appraisalConfig.IsDeleted = appraisalConfigBo.IsDeleted;
                        appraisalConfig.ModifiedOn = DateTime.Now;
                        appraisalConfig.ModifiedBy = appraisalConfigBo.ModifiedBy;
                        appraisalConfig.DomainID = appraisalConfigBo.DomainId;
                        appraisalConfig.HistoryID = Guid.NewGuid();

                        context.Entry(appraisalConfig).State = EntityState.Modified;
                    }

                    context.SaveChanges();
                    message = Notification.Deleted;
                }
                else
                {
                    message = Constants.Notify_Message_RecordReferred;
                }
                return message;
            }
        }

        #endregion Appraisal System Configuration

        #region Questionnaire

        public List<QuestionnaireBo> GetQuestionList(QuestionnaireBo questionnaireBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<QuestionnaireBo> list = (from q in context.tbl_Questionnaire
                                              join em in context.tbl_EmployeeMaster on q.ModifiedBy equals em.ID into emp
                                              from employee in emp.DefaultIfEmpty()
                                              where !q.IsDeleted && q.DomainID == domainId
                                              orderby q.ModifiedOn descending
                                              select new QuestionnaireBo
                                              {
                                                  Id = q.ID,
                                                  Question = q.Question,
                                                  Remarks = q.Remarks,
                                                  ModifiedOn = q.ModifiedOn,
                                                  ModifiedByName = employee.FullName
                                              }).ToList();
                return list;
            }
        }

        public string CreateNewQuestions(QuestionnaireBo questionnaireBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                _check = context.tbl_Questionnaire.Count(s => !s.IsDeleted && s.DomainID == domainId && s.Question == questionnaireBo.Question);
                if (_check == 0)
                {
                    tbl_Questionnaire questionnaire = new tbl_Questionnaire
                    {
                        Question = questionnaireBo.Question,
                        Remarks = questionnaireBo.Remarks,
                        IsDeleted = questionnaireBo.IsDeleted,
                        CreatedBy = questionnaireBo.CreatedBy,
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = questionnaireBo.ModifiedBy,
                        DomainID = questionnaireBo.DomainId,
                        HistoryID = Guid.NewGuid()
                    };

                    context.tbl_Questionnaire.Add(questionnaire);
                    context.SaveChanges();
                    questionnaireBo.QuestionId = Convert.ToInt32(context.tbl_Questionnaire.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
                    return Notification.Inserted + "/" + questionnaireBo.QuestionId;
                }

                return Notification.AlreadyExists;
            }
        }

        public string UpdateQuestion(QuestionnaireBo questionnaireBo)
        {
            FSMEntities context = new FSMEntities();
            _check = context.tbl_Questionnaire.Count(s => s.ID != questionnaireBo.Id && s.Question == questionnaireBo.Question && !s.IsDeleted);

            if (_check == 0)
            {
                tbl_Questionnaire questionnaire = context.tbl_Questionnaire.FirstOrDefault(b => b.ID == questionnaireBo.Id);
                if (questionnaire != null)
                {
                    questionnaire.Question = questionnaireBo.Question;
                    questionnaire.Remarks = questionnaireBo.Remarks;
                    questionnaire.IsDeleted = questionnaireBo.IsDeleted;
                    questionnaire.CreatedBy = questionnaireBo.CreatedBy;
                    questionnaire.CreatedOn = DateTime.Now;
                    questionnaire.ModifiedOn = DateTime.Now;
                    questionnaire.ModifiedBy = questionnaireBo.ModifiedBy;

                    context.Entry(questionnaire).State = EntityState.Modified;
                }

                context.SaveChanges();

                return Notification.Updated + "/" + questionnaireBo.QuestionId;
            }

            return Notification.AlreadyExists;
        }

        public string DeleteQuestion(QuestionnaireBo questionnaireBo)
        {
            FSMEntities context = new FSMEntities();
            int checkappQuestions = context.tbl_AppraisalQuestion.Count(aq => !aq.IsDeleted && aq.QuestionID == questionnaireBo.Id);
            int checkempQuestions = context.tbl_EmployeeAppraisalQuestionnaireMapping.Count(am => !am.IsDeleted && am.QuestionID == questionnaireBo.Id);
            if ((checkappQuestions == 0) && (checkempQuestions == 0))
            {
                tbl_Questionnaire questionnaire = context.tbl_Questionnaire.FirstOrDefault(b => !b.IsDeleted && b.ID == questionnaireBo.Id);
                if (questionnaire != null)
                {
                    questionnaire.IsDeleted = questionnaireBo.IsDeleted;
                    questionnaire.ModifiedOn = DateTime.Now;
                    questionnaire.ModifiedBy = questionnaireBo.ModifiedBy;

                    context.Entry(questionnaire).State = EntityState.Modified;
                }

                context.SaveChanges();

                return Notification.Deleted;
            }
            else
            {
                return Notification.Record_Referred;
            }
        }

        public QuestionnaireBo GetQuestions(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                QuestionnaireBo questionnaireBo = (from q in context.tbl_Questionnaire
                                                   join em in context.tbl_EmployeeMaster on q.ModifiedBy equals em.ID into emp
                                                   from employee in emp.DefaultIfEmpty()
                                                   where !q.IsDeleted && q.ID == id
                                                   select new QuestionnaireBo
                                                   {
                                                       Id = q.ID,
                                                       Question = q.Question,
                                                       Remarks = q.Remarks,
                                                       ModifiedOn = q.ModifiedOn,
                                                       ModifiedByName = employee.FullName
                                                   }).FirstOrDefault();
                return questionnaireBo;
            }
        }

        #endregion Questionnaire
    }
}