﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class EmployeeProfileRepository : BaseRepository
    {
        public EmployeeMasterBo GetEmployeeProfile(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();
                // int domainId = Utility.DomainId();

                if (id != 0)
                {
                    int dependentCount = 0;
                    string type = "PayrollV1";
                    decimal gross = decimal.MinValue; //  = Convert.ToDecimal(context.tbl_EmployeePayStructure.Count(pt => pt.IsDeleted == false && pt.EmployeeId == id) == 0 ? 0 : context.tbl_EmployeePayStructure.Where(pt => pt.IsDeleted == false && pt.EmployeeId == id).OrderByDescending(s => s.EffectiveFrom).Select(s => s.Gross).FirstOrDefault());
                    var payStructureId = 0;//Convert.ToInt32(context.tbl_EmployeePayStructure.Count(pt => pt.IsDeleted == false && pt.EmployeeId == id) == 0 ? 0 : context.tbl_EmployeePayStructure.Where(pt => pt.IsDeleted == false && pt.EmployeeId == id).OrderByDescending(s => s.EffectiveFrom).Select(s => s.Id).FirstOrDefault());

                    if (payStructureId == 0)
                    {
                        type = "PayrollV2";
                        //string effectiveFrom = string.Empty;
                        //int componentId = Convert.ToInt32(context.tbl_Pay_PayrollCompontents.Where(pt => pt.IsDeleted == false && pt.DomainID == domainId && pt.Code == "GRO").Select(pt => pt.ID).FirstOrDefault());
                        var payStructure = (from ps in context.tbl_Pay_EmployeePayStructure
                                            from psd in context.tbl_Pay_EmployeePayStructureDetails
                                                .Where(psd => psd.Isdeleted == false && psd.PayStructureId == ps.ID).DefaultIfEmpty()
                                            from c in context.tbl_Pay_PayrollCompontents
                                                .Where(c => c.IsDeleted == false && c.DomainID == psd.DomainId && c.ID == psd.ComponentId && c.Code == "GRO").DefaultIfEmpty()
                                            where ps.IsDeleted == false && ps.EmployeeID == id //&& psd.ComponentId == componentId
                                            select new
                                            {
                                                Gross = psd.Amount,
                                                PaystructureId = ps.ID,
                                                ps.EffectiveFrom
                                            }).OrderByDescending(ps => ps.EffectiveFrom).FirstOrDefault();
                        if (payStructure != null)
                        {
                            Debug.Assert(payStructure.Gross != null, "payStructure.Gross != null");
                            gross = payStructure.Gross.Value;
                            payStructureId = payStructure.PaystructureId;
                        }
                    }

                    if (context.tbl_EmployeeDependentDetails != null)
                    {
                        dependentCount = context.tbl_EmployeeDependentDetails.Count(d => d.IsDeleted == false && d.EmployeeID == id);
                    }

                    employeeMasterBo = (from e in context.tbl_EmployeeMaster
                                        join bu in context.tbl_BusinessUnit on e.BaseLocationID equals bu.ID into ba
                                        from basel in ba.DefaultIfEmpty()
                                        join cm in context.tbl_CodeMaster on e.PrefixID equals cm.ID
                                        join design in context.tbl_Designation on e.DesignationID equals design.ID into de
                                        from des in de.DefaultIfEmpty()
                                        join depart in context.tbl_Department on e.DepartmentID equals depart.ID into dep
                                        from dt in dep.DefaultIfEmpty()
                                        join fu in context.tbl_FileUpload on e.FileID equals fu.Id into fileUpload
                                        from files in fileUpload.DefaultIfEmpty()
                                        join ep in context.tbl_EmployeePersonalDetails on e.ID equals ep.EmployeeID into emp
                                        from personalDetails in emp.DefaultIfEmpty()
                                        join design in context.tbl_Functional on e.FunctionalID equals design.ID into fun
                                        from func in fun.DefaultIfEmpty()
                                        join ge in context.tbl_CodeMaster on e.GenderID equals ge.ID into gender
                                        from gen in gender.DefaultIfEmpty()
                                        join co in context.tbl_Company on e.DomainID equals co.ID into company
                                        from comp in company.DefaultIfEmpty()
                                        where e.ID == id
                                        //  && (!(E.IsActive ?? false) || ((E.IsActive ?? false) && currentDate.Date <= inActiveDate.Date))
                                        select new EmployeeMasterBo
                                        {
                                            FullName = e.FullName,
                                            FirstName = e.FirstName,
                                            PrefixType = cm.Code,
                                            BaseLocation = basel.Name,
                                            DesignationName = des.Name,
                                            DepartmentName = dt.Name,
                                            FunctionalName = func.Name,
                                            DateOFJoin = e.DOJ,
                                            BandId = e.BandID,
                                            GradeId = e.GradeID,
                                            ContactNo = e.ContactNo,
                                            EmailId = e.EmailID,
                                            FileName = files.FileName ?? "",
                                            FileUploadId = files.Id,
                                            Dob = personalDetails.DateOfBirth,
                                            NoOfDependent = dependentCount,
                                            Gross = gross,
                                            PaystructureId = payStructureId,
                                            BaseLocationId = basel.ID,
                                            Gender = gen.Code,
                                            CompanyName = comp.FullName ?? "",
                                            LogoId = comp.Logo,
                                            UserMessage = type
                                        }).FirstOrDefault();
                    if (employeeMasterBo != null)
                    {
                        employeeMasterBo.StatutoryDetailsBo = GetStatutoryDetails(id);
                        employeeMasterBo.PersonalDetailsBo = GetPersonalDetails(id);
                        employeeMasterBo.PayStubMonthList = GetMonthNameForPayStub(id);
                        employeeMasterBo.CompanyAssetList = GetAssetsTypeList(id).ToList();
                        employeeMasterBo.NewsandEventsList = SearchNewsAndEventsViewersList("PROFILE").ToList();
                        employeeMasterBo.LeaveManagementList = GetMyApprovalCount();
                        employeeMasterBo.MyPerformanceList = MyPerformance();
                        employeeMasterBo.ExpiryAlert = GetExpiryAlert();
                    }
                }

                return employeeMasterBo;
            }
        }

        public EmployeeMasterBo GetOfficialDetails(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();

                if (id != 0)
                {
                    employeeMasterBo = (from e in context.tbl_EmployeeMaster
                                        join d in context.tbl_Department on e.DepartmentID equals d.ID into depart
                                        from department in depart.DefaultIfEmpty()
                                        join design in context.tbl_Designation on e.DesignationID equals design.ID into designation
                                        from designations in designation.DefaultIfEmpty()
                                        join design in context.tbl_Functional on e.FunctionalID equals design.ID into fun
                                        from func in fun.DefaultIfEmpty()
                                        join emp in context.tbl_EmployeeMaster on e.ReportingToID equals emp.ID into repo
                                        from report in repo.DefaultIfEmpty()
                                        join emp1 in context.tbl_Band on e.BandID equals emp1.ID into ban
                                        from band in ban.DefaultIfEmpty()
                                        join emp11 in context.tbl_EmployeeGrade on e.GradeID equals emp11.ID into grade
                                        from grade1 in grade.DefaultIfEmpty()
                                        join bu in context.tbl_BusinessUnit on e.BaseLocationID equals bu.ID into bus
                                        from baseLocation in bus.DefaultIfEmpty()
                                        join p in context.tbl_EmployeePersonalDetails on e.ID equals p.EmployeeID into personal
                                        from pd in personal.DefaultIfEmpty()
                                        where e.ID == id //&& !e.IsDeleted && !(e.IsActive ?? false)
                                        select new EmployeeMasterBo
                                        {
                                            Id = e.ID,
                                            Code = e.Code,
                                            PrefixId = e.PrefixID,
                                            FirstName = e.FirstName,
                                            LastName = e.LastName,
                                            DateOFJoin = e.DOJ,
                                            EmailId = e.EmailID,
                                            DepartmentName = department.Name,
                                            DesignationName = designations.Name,
                                            FunctionalName = func.Name,
                                            ReportingToName = report.FullName,
                                            BaseLocation = baseLocation.Name,
                                            BusinessUnitIDs = e.BusinessUnitID,
                                            BandName = band.Name,
                                            GradeName = grade1.Name,
                                            Doc = e.DOC,
                                            ContactNo = e.ContactNo,
                                            Qualification = e.Qualification,
                                            DepartmentId = e.DepartmentID,
                                            DesignationId = e.DesignationID,
                                            GenderId = e.GenderID,
                                            FunctionalId = e.FunctionalID,
                                            GradeId = e.GradeID,
                                            BandId = e.BandID,
                                            ReportingToId = e.ReportingToID,
                                            BaseLocationId = e.BaseLocationID,
                                            EmploymentTypeId = e.EmploymentTypeID,
                                            IsDeleted = e.IsDeleted,
                                            Dob = pd.DateOfBirth,
                                            EmergencyContactNo = e.EmergencyContactNo
                                        }).FirstOrDefault();
                }

                return employeeMasterBo;
            }
        }

        public PersonalDetailsBo GetPersonalDetails(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                PersonalDetailsBo personalDetailsBo = new PersonalDetailsBo();

                if (id != 0)
                {
                    var check = context.tbl_EmployeePersonalDetails.Count(s => s.EmployeeID == id && !s.IsDeleted);
                    if (check != 0)
                    {
                        personalDetailsBo = (from e in context.tbl_EmployeeMaster
                                             join per in context.tbl_EmployeePersonalDetails on e.ID equals per.EmployeeID into person
                                             from personal in person.DefaultIfEmpty()
                                             join cd in context.tbl_BloodGroup on personal.BloodGroupID equals cd.ID into blood
                                             from bloodGroup in blood.DefaultIfEmpty()
                                             join cdd in context.tbl_CodeMaster on personal.MaritalStatusID equals cdd.ID into fun
                                             from func in fun.DefaultIfEmpty()
                                             join c in context.tbl_City on personal.CityID equals c.ID into cit
                                             from city in cit.DefaultIfEmpty()
                                             where e.ID == id && !personal.IsDeleted
                                             select new PersonalDetailsBo
                                             {
                                                 Id = personal.EmployeeID,
                                                 PersonalDetailId = personal.ID,
                                                 EmployeeId = e.ID,
                                                 FatherName = personal.FatherName,
                                                 Dob = personal.DateOfBirth,
                                                 BloodGroup = bloodGroup.Name,
                                                 MaritalStatus = func.Code,
                                                 Anniversary = personal.Anniversary,
                                                 SpouseName = personal.SpouseName,
                                                 PresentAddress = personal.PresentAddress,
                                                 PermanentAddress = personal.PermanentAddress,
                                                 CityName = city.Name,
                                                 CityId = personal.CityID,
                                                 BloodGroupId = personal.BloodGroupID,
                                                 MaritalStatusId = personal.MaritalStatusID,
                                                 IsDeleted = personal.IsDeleted,
                                                 MotherName = personal.MotherName
                                             }).FirstOrDefault();
                    }
                    else
                    {
                        personalDetailsBo.EmployeeId = id;
                    }
                }

                return personalDetailsBo;
            }
        }

        public StatutoryDetailsBo GetStatutoryDetails(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                StatutoryDetailsBo statutoryDetailsBo = new StatutoryDetailsBo();

                if (id != 0)
                {
                    int check = context.tbl_EmployeeStatutoryDetails.Count(s => s.EmployeeID == id && !s.IsDeleted);
                    if (check != 0)
                    {
                        statutoryDetailsBo = (from e in context.tbl_EmployeeMaster
                                              join b in context.tbl_EmployeeStatutoryDetails on e.ID equals b.EmployeeID into bank
                                              from statutoryDetails in bank.DefaultIfEmpty()
                                              where e.ID == id && !statutoryDetails.IsDeleted
                                              select new StatutoryDetailsBo
                                              {
                                                  StatutoryDetailId = statutoryDetails.ID,
                                                  EmployeeId = e.ID,
                                                  AadharId = statutoryDetails.AadharID,
                                                  AccountNo = statutoryDetails.AccountNo,
                                                  BankName = statutoryDetails.BankName,
                                                  BranchName = statutoryDetails.BranchName,
                                                  IfscCode = statutoryDetails.IFSCCode,
                                                  PanNo = statutoryDetails.PAN_No,
                                                  PfNo = statutoryDetails.PFNo,
                                                  EsiNo = statutoryDetails.ESINo,
                                                  Uan = statutoryDetails.UANNo,
                                                  PolicyName = statutoryDetails.PolicyName,
                                                  PolicyNo = statutoryDetails.PolicyNo,
                                                  IsDeleted = statutoryDetails.IsDeleted,
                                                  CompanyPolicyNo = statutoryDetails.CompanyPolicyNo,
                                                  TpInsurance = statutoryDetails.TPI
                                              }).FirstOrDefault();

                        statutoryDetailsBo.PolicyName = "N/A";
                        statutoryDetailsBo.PolicyNo = "N/A";
                        statutoryDetailsBo.CompanyPolicyNo = "N/A";
                    }
                    else
                    {
                        statutoryDetailsBo.EmployeeId = id;
                    }
                }

                return statutoryDetailsBo;
            }
        }

        public PayStructureBO GetPayroll(int? employeeId, int monthId, int yearId, bool? isProfile)
        {
            PayStructureBO payStructureBo = new PayStructureBO();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETEMPLOYEEPAYSTUB);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsProfile, DbType.Boolean, isProfile);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        payStructureBo.EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]);
                        payStructureBo.EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]);
                        payStructureBo.Designation = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]);
                        payStructureBo.PFNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PFNo]);
                        payStructureBo.ESINo = Utility.CheckDbNull<string>(reader[DBParam.Output.ESINo]);
                        payStructureBo.UANNo = Utility.CheckDbNull<string>(reader[DBParam.Output.UAN]);
                        payStructureBo.PANNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PANNO]);
                        payStructureBo.AadharNo = Utility.CheckDbNull<string>(reader[DBParam.Output.AadharID]);
                        payStructureBo.BankAccountNo = Utility.CheckDbNull<string>(reader[DBParam.Output.AccountNo]);
                        payStructureBo.Gross = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross]), 0);
                        payStructureBo.Basic = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]), 0);
                        payStructureBo.HRA = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]), 0);
                        payStructureBo.MedicalAllowance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]), 0);
                        payStructureBo.SplAllow = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.SplAllow]), 0);
                        payStructureBo.Conveyance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]), 0);
                        payStructureBo.EEPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEPF]));
                        payStructureBo.EEESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEESI]));
                        payStructureBo.PT = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PT]), 0);
                        payStructureBo.TDS = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS]), 0);
                        payStructureBo.ERPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERPF]));
                        payStructureBo.ERESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERESI]));
                        payStructureBo.Bonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Bonus]), 0);
                        payStructureBo.Gratuity = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gratuity]));
                        payStructureBo.PLI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PLI]), 0);
                        payStructureBo.Medicalclaim = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Medicalclaim]), 0);
                        payStructureBo.MobileDataCard = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MobileDataCard]), 0);
                        payStructureBo.OtherEarnings = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherEarning]), 0);
                        payStructureBo.OtherPerks = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherPerks]), 0);
                        payStructureBo.Deduction = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Deduction]), 0);
                        payStructureBo.Loans = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Loans]), 0);
                        payStructureBo.WorkingDays = Utility.CheckDbNull<int>(reader[DBParam.Output.WorkingDays]);
                        payStructureBo.PresentDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PresentDays]);
                        payStructureBo.CompanyName = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyName]);
                        payStructureBo.BusinessUnitAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnitAddress]);
                        payStructureBo.DOJ = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]);
                        payStructureBo.FixedDA = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.FixedDA]), 0);
                        payStructureBo.EducationAllowance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]), 0);
                        payStructureBo.IncomeTax = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.IncomeTax]), 0);
                        payStructureBo.InstalmentOnLoan = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.InstalmentOnLoan]), 0);
                        payStructureBo.Logo = Utility.CheckDbNull<string>(reader[DBParam.Output.Logo]);
                        payStructureBo.Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region]);
                        payStructureBo.OverTime = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OverTime]), 0);
                        payStructureBo.EducationAllow = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllow]), 0);
                        payStructureBo.MonsoonAllow = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]), 0);
                        payStructureBo.PaperMagazine = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]), 0);
                        //=======================
                        payStructureBo.IsEducationAllow = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEducationAllow]);
                        payStructureBo.IsOverTime = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOverTime]);
                        payStructureBo.IsEEPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEPF]);
                        payStructureBo.IsEEESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEESI]);
                        payStructureBo.IsPT = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPT]);
                        payStructureBo.IsTDS = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsTDS]);
                        payStructureBo.IsERPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERPF]);
                        payStructureBo.IsERESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERESI]);
                        payStructureBo.IsGratuity = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsGratuity]);
                        payStructureBo.IsPLI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPLI]);
                        payStructureBo.IsMediclaim = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMediclaim]);
                        payStructureBo.IsMobileDatacard = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMobileDatacard]);
                        payStructureBo.IsOtherPerks = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOtherPerks]);
                        //=======================
                    }
                }
            }

            return payStructureBo;
        }

        public List<SelectListItem> GetMonthNameForPayStub(int employeeId)
        {
            List<SelectListItem> dependentDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETMONTHNAMEFORPAYSTUB);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependentDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Value = Utility.CheckDbNull<string>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependentDropDownList;
        }

        public List<CompanyAssetsBo> GetCompanyAssets(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();
                List<CompanyAssetsBo> companyAssetsBoList = new List<CompanyAssetsBo>();

                if (id != null)
                {
                    companyAssetsBoList = (from a in context.tbl_AssetType
                                           join c in context.tbl_CompanyAssets on a.ID equals c.AssetTypeID into assets
                                           from e in assets.DefaultIfEmpty()
                                           where e.EmployeeID == id && a.IsDeleted == false && !e.IsDeleted && e.DomainID == domainId
                                           orderby a.Name
                                           select new CompanyAssetsBo
                                           {
                                               Name = a.Name,
                                               UserMessage = a.Remarks,
                                               Qty = e.QTY,
                                               Make = e.Make,
                                               SerialNumber = e.SerialNo,
                                               HandoverOn = e.HandoverOn,
                                               ReturnedDate = e.ReturnedOn,
                                               ReturnedRemarks = e.ReturnedRemarks
                                           }).ToList();
                }

                return companyAssetsBoList;
            }
        }

        public List<NewsAndEventsBo> SearchNewsAndEventsViewersList(string type)
        {
            List<NewsAndEventsBo> newsAndEventsBoList = new List<NewsAndEventsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHNEWSANDEVENTSBU);

                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        newsAndEventsBoList.Add(new NewsAndEventsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PublishedOn]),
                            EndDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EndDate]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.PublishedBy]),
                            UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.CreatedBy]),
                            StartDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.StartDate])
                        });
                    }
                }
            }

            return newsAndEventsBoList;
        }


        public List<PayStructureBO> GetEmployeePayStructure_V2(int id, string type)
        {
            List<PayStructureBO> payStructureBoList = new List<PayStructureBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetPayStructureDetails_V2");

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeePayStructureID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        payStructureBoList.Add(new PayStructureBO()
                        {
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Component]),
                            Basic = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Month]),
                            Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Year]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.Type])
                        });
                    }
                }
            }
            return payStructureBoList;
        }

        public List<LeaveManagementBo> GetMyApprovalCount()
        {
            List<LeaveManagementBo> leaveManagementBoList = new List<LeaveManagementBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_MYAPPROVALSCOUNT);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        leaveManagementBoList.Add(new LeaveManagementBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.Count]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description])
                        });
                    }
                }
            }

            return leaveManagementBoList;
        }

        public List<SelectListItem> MyPerformance()
        {
            List<SelectListItem> dependentDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PROFILE_MYPERFORMANCE);

                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependentDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            Value = Utility.CheckDbNull<string>(reader[DBParam.Output.Duration])
                        });
                    }
                }
            }

            return dependentDropDownList;
        }

        public List<CompanyAssetsBo> GetAssetsTypeList(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();
                List<CompanyAssetsBo> companyAssetsBoList = new List<CompanyAssetsBo>();

                if (id != null)
                {
                    companyAssetsBoList = (from a in context.tbl_AssetType
                                           join c in context.tbl_CompanyAssets
                                                            .Where(ca => ca.EmployeeID == id && !ca.IsDeleted)
                                               on a.ID equals c.AssetTypeID into g
                                           from e in g.DefaultIfEmpty()
                                           where (a.IsDeleted ?? false) == false && a.DomainID == domainId
                                           orderby e.QTY descending
                                           select new CompanyAssetsBo
                                           {
                                               Name = a.Name,
                                               UserMessage = a.Remarks,
                                               Qty = e.QTY,
                                               Make = e.Make,
                                               SerialNumber = e.SerialNo,
                                               HandoverOn = e.HandoverOn,
                                               ReturnedDate = e.ReturnedOn,
                                               ReturnedRemarks = e.ReturnedRemarks
                                           }).ToList();
                }

                return companyAssetsBoList;
            }
        }

        public DataSet GetPayroll_V2(int? employeeId, int monthId, int yearId)
        {
            DataSet dataSet = new DataSet();
            DataTable dataTable = new DataTable();
            dataSet.Tables.Add(dataTable);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_PayStubConfiguration");

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    dataSet.Load(reader, LoadOption.OverwriteChanges, dataTable);
                }
            }
            return dataSet;
        }
        public List<DocumentDetailsBo> GetExpiryAlert()
        {
            List<DocumentDetailsBo> ExpiryAlertlist = new List<DocumentDetailsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_EXPIRYALERT);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ExpiryAlertlist.Add(new DocumentDetailsBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader["Name"]),
                            Description = Utility.CheckDbNull<string>(reader["BindValue"]),
                            Title = Utility.CheckDbNull<string>(reader["Icon"])
                        });
                    }
                }
            }

            return ExpiryAlertlist.OrderBy(a => a.UserMessage).ToList();
        }

        public List<DocumentDetailsBo> GetExpiryAlertDetails(string key)
        {
            List<DocumentDetailsBo> ExpiryAlertlist = new List<DocumentDetailsBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_EXPIRYALERTDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, "@Key", DbType.String, key);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ExpiryAlertlist.Add(new DocumentDetailsBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader["FullName"]),
                            Description = Utility.CheckDbNull<string>(reader["DocType"]),
                            DateOfExpiry = Utility.CheckDbNull<DateTime>(reader["DateOfExpiry"])
                        });
                    }
                }
            }

            return ExpiryAlertlist;
        }
    }
}