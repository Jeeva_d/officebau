﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Services
{
    public class EmployeeClaimsRepository : BaseRepository
    {
        #region Report

        public List<EmployeeClaimsReportBo> GetCompleteClaimsList(EmployeeClaimsReportBo employeeClaimsReportBo)
        {
            List<EmployeeClaimsReportBo> employeeClaimsReportBoList = new List<EmployeeClaimsReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_COMPLETECLAIMSLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeClaimsReportBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.DateTime, employeeClaimsReportBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.DateTime, employeeClaimsReportBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, employeeClaimsReportBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeClaimsReportBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.CategoryID, DbType.Int32, employeeClaimsReportBo.CategoryId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsReportBoList.Add(
                            new EmployeeClaimsReportBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                FullName = Utility.CheckDbNull<string>(reader[DBParam.Output.FullName]),
                                EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                RequestedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.RequestedDate]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                ApprovalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ApprovalAmount]),
                                SettlerAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SettlerAmount]),
                                BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                DepartmentName = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                                SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo]),
                                Category = Utility.CheckDbNull<string>(reader[DBParam.Output.CategoryName])
                            });
                    }
                }
            }

            return employeeClaimsReportBoList;
        }

        #endregion Report

        #region Claims

        public List<EmployeeClaimsBo> GetClaimsRequestList(EmployeeClaimsBo employeeClaimsBo)
        {
            List<EmployeeClaimsBo> employeeClaimsBoList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHCLAIMREQUEST);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeClaimsBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeClaimsBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.OperationType, DbType.String, employeeClaimsBo.Operation);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBoList.Add(
                            new EmployeeClaimsBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                CategoryName = Utility.CheckDbNull<string>(reader[DBParam.Output.CategoryName]),
                                ApprovalId = Utility.CheckDbNull<int>(reader[DBParam.Output.ApprovalID]),
                                ApprovarName = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovarName]),
                                ClaimedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ClaimedDate]),
                                CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                StatusId = Utility.CheckDbNull<int>(reader[DBParam.Output.StatusID]),
                                SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo]),
                                DestinationCity = Utility.CheckDbNull<string>(reader[DBParam.Output.DestinationCity]),
                                ApprovalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ApprovalAmount])
                            });
                    }
                }
            }

            return employeeClaimsBoList;
        }

        public EmployeeClaimsBo GetClaimsRequest(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeClaimsBo result = new EmployeeClaimsBo();
                if (id != null)
                {
                    result = (from c in context.tbl_ClaimsRequest
                              join ca in context.tbl_ClaimsApprove on c.ID equals ca.ClaimItemID into approve
                              from aaprover in approve.DefaultIfEmpty()
                              join cs in context.tbl_ClaimsFinancialApprove on c.ID equals cs.ClaimItemID into settle
                              from settler in settle.DefaultIfEmpty()
                              join cus in context.tbl_ClaimCategory on c.CategoryID equals cus.ID
                              join st in context.tbl_Status on c.StatusID equals st.ID
                              join emp in context.tbl_EmployeeMaster on c.CreatedBy equals emp.ID
                              join empca in context.tbl_EmployeeMaster on aaprover.CreatedBy equals empca.ID into empca
                              from empmca in empca.DefaultIfEmpty()
                              join empcs in context.tbl_EmployeeMaster on settler.CreatedBy equals empcs.ID into empcs
                              from empmcs in empcs.DefaultIfEmpty()
                              join fu in context.tbl_FileUpload on c.FileUpload equals fu.Id into fileUpload
                              from files in fileUpload.DefaultIfEmpty()
                              join cas in context.tbl_Status on aaprover.StatusID equals cas.ID into app
                              from appStatus in app.DefaultIfEmpty()
                              join caf in context.tbl_Status on settler.StatusID equals caf.ID into appfin
                              from finappStatus in appfin.DefaultIfEmpty()
                              join bu in context.tbl_BusinessUnit on emp.BaseLocationID equals bu.ID into business
                              from bUnit in business.DefaultIfEmpty()
                              join tr in context.tbl_EmployeeTravelRequest on c.TravelReqNo equals tr.TravelReqNo into trd
                              from t in trd.DefaultIfEmpty()
                              where c.ID == id
                              select new EmployeeClaimsBo
                              {
                                  CategoryId = c.CategoryID,
                                  ApprovalId = c.ApproverID,
                                  Id = c.ID,
                                  ClaimedDate = c.ClaimDate,
                                  Amount = c.Amount,
                                  BillNo = c.BillNo,
                                  Remarks = c.Remarks,
                                  SubmittedDate = c.SubmittedDate,
                                  FileUpload = files.OriginalFileName,
                                  FileUploadId = c.FileUpload,
                                  StatusId = c.StatusID,
                                  StatusName = st.Code,
                                  IsDeleted = c.IsDeleted,
                                  ApprovarName = (empmca.EmpCodePattern ?? "") + (empmca.Code ?? "") + " - " + empmca.FullName,
                                  ApprovalAmount = aaprover.ApprovedAmount,
                                  ApproverRemarks = aaprover.Comments,
                                  ApproverDate = aaprover.CreatedOn,
                                  SettlerName = (empmcs.EmpCodePattern ?? "") + (empmcs.Code ?? "") + " - " + empmcs.FullName,
                                  SettlerRemarks = settler.Remarks,
                                  CreatedOn = settler.CreatedOn,
                                  ModifiedByName = emp.FullName,
                                  ModifiedOn = c.ModifiedOn,
                                  ApproverStatus = appStatus.Code,
                                  FinApproverStatus = finappStatus.Code,
                                  BaseLocation = bUnit.Name,
                                  DestinationId = c.DestinationID ?? 0,
                                  FinApprovedAmount = settler.FinApprovedAmount,
                                  IsTravelClaim = c.IsTravelClaim ?? false,
                                  TravelReqNo = c.TravelReqNo,
                                  TravelReqId = t.ID
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public int GetClaimStatusId(string type, string statusName)
        {
            int statusId;

            using (FSMEntities context = new FSMEntities())
            {
                statusId = Convert.ToInt32(context.tbl_Status.Where(b => !b.IsDeleted && b.Type == type && b.Code == statusName).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());
            }

            return statusId;
        }

        public string CreateClaimsRequest(EmployeeClaimsBo employeeClaimsBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();

                int check = context.tbl_ClaimsRequest.Count(s => s.ID == employeeClaimsBo.Id && !s.IsDeleted && s.DomainID == domainId);

                if (check == 0)
                {
                    tbl_ClaimsRequest claimsRequest = new tbl_ClaimsRequest
                    {
                        CategoryID = employeeClaimsBo.CategoryId,
                        ApproverID = employeeClaimsBo.ApprovalId,
                        ClaimDate = employeeClaimsBo.ClaimedDate,
                        Amount = employeeClaimsBo.Amount,
                        DestinationID = employeeClaimsBo.DestinationId,
                        SubmittedDate = employeeClaimsBo.SubmittedDate,
                        BillNo = employeeClaimsBo.BillNo,
                        StatusID = GetClaimStatusId("Claims", "Open"),
                        Remarks = employeeClaimsBo.Remarks,
                        FileUpload = employeeClaimsBo.FileUploadId,
                        IsTravelClaim = employeeClaimsBo.IsTravelClaim,
                        TravelReqNo = employeeClaimsBo.TravelReqNo,
                        IsDeleted = employeeClaimsBo.IsDeleted,
                        CreatedBy = employeeClaimsBo.CreatedBy,
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now,
                        ModifiedBy = employeeClaimsBo.ModifiedBy,
                        DomainID = employeeClaimsBo.DomainId,
                        HistoryID = Guid.NewGuid()
                    };

                    context.tbl_ClaimsRequest.Add(claimsRequest);
                    context.SaveChanges();

                    employeeClaimsBo.Id = Convert.ToInt32(context.tbl_ClaimsRequest.Where(b => !b.IsDeleted && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());

                    // File Upload
                    if (employeeClaimsBo.FileUploadId != null)
                    {
                        tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == employeeClaimsBo.FileUploadId);
                        if (fileUpload != null)
                        {
                            fileUpload.ReferenceTable = "tbl_ClaimsRequest";
                            context.Entry(fileUpload).State = EntityState.Modified;
                        }

                        context.SaveChanges();
                    }

                    return Notification.Inserted;
                }

                return Notification.AlreadyExists;
            }
        }

        public string PopulateMailBody(string userName)
        {
            string body;

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/Claims.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{UserName}", userName);
            body = body.Replace("{Content}", "");
            return body;
        }

        public string UpdateClaimsRequest(EmployeeClaimsBo employeeClaimsBo)
        {
            FSMEntities context = new FSMEntities();

            if (employeeClaimsBo.Id != 0)
            {
                tbl_ClaimsRequest claimsRequest = context.tbl_ClaimsRequest.FirstOrDefault(b => b.ID == employeeClaimsBo.Id);

                if (claimsRequest != null)
                {
                    claimsRequest.CategoryID = employeeClaimsBo.CategoryId;
                    claimsRequest.ApproverID = employeeClaimsBo.ApprovalId;
                    claimsRequest.ClaimDate = employeeClaimsBo.ClaimedDate;
                    claimsRequest.Amount = employeeClaimsBo.Amount;
                    claimsRequest.DestinationID = employeeClaimsBo.DestinationId;
                    claimsRequest.BillNo = employeeClaimsBo.BillNo;
                    claimsRequest.StatusID = employeeClaimsBo.StatusId;
                    claimsRequest.Remarks = employeeClaimsBo.Remarks;
                    claimsRequest.FileUpload = employeeClaimsBo.FileUploadId;
                    claimsRequest.IsTravelClaim = employeeClaimsBo.IsTravelClaim;
                    claimsRequest.TravelReqNo = employeeClaimsBo.TravelReqNo;
                    claimsRequest.IsDeleted = employeeClaimsBo.IsDeleted;
                    claimsRequest.ModifiedOn = DateTime.Now;
                    claimsRequest.ModifiedBy = employeeClaimsBo.ModifiedBy;

                    context.Entry(claimsRequest).State = EntityState.Modified;
                }

                context.SaveChanges();

                employeeClaimsBo.Id = employeeClaimsBo.Id;

                // File Upload
                if (employeeClaimsBo.FileUploadId != null)
                {
                    tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == employeeClaimsBo.FileUploadId);
                    if (fileUpload != null)
                    {
                        fileUpload.ReferenceTable = "tbl_ClaimsRequest";
                        context.Entry(fileUpload).State = EntityState.Modified;
                    }

                    context.SaveChanges();
                }

                return Notification.Updated;
            }

            return Notification.AlreadyExists;
        }

        public string DeleteClaimsRequest(EmployeeClaimsBo employeeClaimsBo)
        {
            FSMEntities context = new FSMEntities();
            tbl_ClaimsRequest employee = context.tbl_ClaimsRequest.FirstOrDefault(b => b.ID == employeeClaimsBo.Id);

            if (employee != null)
            {
                employee.IsDeleted = employeeClaimsBo.IsDeleted;
                employee.ModifiedOn = DateTime.Now;
                employee.ModifiedBy = employeeClaimsBo.ModifiedBy;
            }

            context.SaveChanges();

            return Notification.Deleted;
        }

        public List<EmployeeClaimsBo> GetCategoryList()
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<EmployeeClaimsBo> list = (from t in context.tbl_ClaimCategory
                                               where !t.IsDeleted && t.DomainID == domainId
                                               orderby t.Name
                                               select new EmployeeClaimsBo
                                               {
                                                   Name = t.Name,
                                                   Id = t.ID
                                               }).ToList();
                return list;
            }
        }

        public string SaveClaimsApproval(string idList, int approverId)
        {
            string outputMessage = BOConstants.Submitted;

            using (FSMEntities context = new FSMEntities())
            {
                foreach (string claimId in idList.Split(','))
                {
                    if (!string.IsNullOrWhiteSpace(claimId))
                    {
                        int id = Convert.ToInt32(claimId);
                        tbl_ClaimsRequest claimsRequest = context.tbl_ClaimsRequest.FirstOrDefault(b => b.ID == id);

                        if (claimsRequest != null)
                        {
                            claimsRequest.StatusID = GetClaimStatusId("Claims", "Submitted");
                            claimsRequest.ApproverID = approverId;
                            claimsRequest.SubmittedDate = DateTime.Now.Date;
                            claimsRequest.ModifiedOn = DateTime.Now;
                            claimsRequest.ModifiedBy = Utility.UserId();
                            claimsRequest.HistoryID = Guid.NewGuid();

                            context.Entry(claimsRequest).State = EntityState.Modified;
                        }

                        context.SaveChanges();
                    }
                }

                return outputMessage;
            }
        }

        public string SearchApproverDetail(int employeeId, string type)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeClaimsBo result;
                int domainId = Utility.DomainId();

                int reportingId = Convert.ToInt32(context.tbl_EmployeeMaster.Where(b => b.ID == employeeId && !b.IsDeleted && !(b.IsActive ?? false) && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ReportingToID).FirstOrDefault());
                int approvalId = Convert.ToInt32(context.tbl_ClaimsApproverConfiguration.Where(b => b.EmployeeID == employeeId && b.DomainID == domainId).OrderByDescending(b => b.ID).Select(b => b.ApproverID).FirstOrDefault());
                int check = context.tbl_ClaimsApproverConfiguration.Count(s => s.EmployeeID == employeeId && s.DomainID == domainId);

                if (check != 0)
                {
                    if (approvalId != 0)
                    {
                        result = (from e in context.tbl_ClaimsApproverConfiguration
                                  join rt in context.tbl_EmployeeMaster on e.ApproverID equals rt.ID into report
                                  from r in report.DefaultIfEmpty()
                                  where e.ApproverID == approvalId && e.DomainID == domainId
                                  select new EmployeeClaimsBo
                                  {
                                      ApprovarName = (r.EmpCodePattern ?? "") + (r.Code ?? "") + " - " + r.FullName,
                                      ApprovalId = e.ApproverID
                                  }).FirstOrDefault();
                        if (type == "Name")
                        {
                            return result == null ? "" : result.ApprovarName;
                        }

                        return result == null ? "0" : result.ApprovalId.ToString();
                    }

                    return null;
                }

                if (reportingId != 0)
                {
                    result = (from e in context.tbl_EmployeeMaster
                              where e.ID == reportingId && !e.IsDeleted && !(e.IsActive ?? false) && e.DomainID == domainId
                              select new EmployeeClaimsBo
                              {
                                  ApprovarName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                  ApprovalId = e.ID
                              }).FirstOrDefault();
                    if (type == "Name")
                    {
                        return result == null ? "" : result.ApprovarName;
                    }

                    return result == null ? "0" : result.ApprovalId.ToString();
                }

                return null;
            }
        }

        public int SearchIsClaimChecked(int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int isClaimChecked = Convert.ToInt32(context.tbl_EmployeeOtherDetails.Where(b => b.EmployeeID == employeeId && !b.IsDeleted).Select(b => b.IsClaimRequest).FirstOrDefault());
                return isClaimChecked != 0 ? 1 : 0;
            }
        }

        public string ReSubmitClaimsRequest(EmployeeClaimsBo employeeClaimsBo)
        {
            FSMEntities context = new FSMEntities();
            tbl_ClaimsRequest claimsRequest = context.tbl_ClaimsRequest.FirstOrDefault(b => b.ID == employeeClaimsBo.Id);

            if (claimsRequest != null)
            {
                claimsRequest.StatusID = GetClaimStatusId("Claims", "Resubmitted");
                claimsRequest.ModifiedOn = DateTime.Now;
                claimsRequest.ModifiedBy = employeeClaimsBo.ModifiedBy;
                claimsRequest.CategoryID = employeeClaimsBo.CategoryId;
                claimsRequest.Amount = employeeClaimsBo.Amount;
                claimsRequest.DestinationID = employeeClaimsBo.DestinationId;
                claimsRequest.ApproverID = employeeClaimsBo.ApprovalId;

                context.Entry(claimsRequest).State = EntityState.Modified;
                context.SaveChanges();

                employeeClaimsBo.Id = employeeClaimsBo.Id;

                tbl_ClaimsApprove claimsApprove = context.tbl_ClaimsApprove.FirstOrDefault(b => b.ClaimItemID == employeeClaimsBo.Id);

                if (claimsApprove != null)
                {
                    claimsApprove.StatusID = GetClaimStatusId("Claims", "Resubmitted");
                    claimsApprove.FinApproverStatusID = 0;
                    claimsApprove.FinancialApproverID = 0;
                    claimsApprove.ModifiedOn = DateTime.Now;
                    claimsApprove.ModifiedBy = employeeClaimsBo.ModifiedBy;
                    context.Entry(claimsApprove).State = EntityState.Modified;
                    claimsApprove.ApprovedAmount = null;
                    claimsApprove.Comments = string.Empty;
                    claimsRequest.Amount = employeeClaimsBo.Amount;

                    context.SaveChanges();
                }
            }

            tbl_ClaimsFinancialApprove claimsFinApprove = context.tbl_ClaimsFinancialApprove.FirstOrDefault(b => b.ClaimItemID == employeeClaimsBo.Id);

            if (claimsFinApprove != null)
            {
                context.Entry(claimsFinApprove).State = EntityState.Deleted;
                context.SaveChanges();
            }

            // File Upload
            if (employeeClaimsBo.FileUploadId != null)
            {
                tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == employeeClaimsBo.FileUploadId);
                if (fileUpload != null)
                {
                    fileUpload.ReferenceTable = "tbl_ClaimsRequest";
                    context.Entry(fileUpload).State = EntityState.Modified;
                }

                context.SaveChanges();
            }

            return BOConstants.Resubmitted;
        }

        public string CheckClaimEligibleAmount(EmployeeClaimsBo employeeClaimsBo)
        {
            string userMessage = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CHECK_CLAIMELIGIBLEAMOUNT);

                Database.AddInParameter(dbCommand, DBParam.Input.ClaimTypeID, DbType.Int32, employeeClaimsBo.CategoryId);
                Database.AddInParameter(dbCommand, DBParam.Input.DestinationID, DbType.Int32, employeeClaimsBo.DestinationId);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, employeeClaimsBo.Amount);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.RequesterID, DbType.Int32, employeeClaimsBo.RequesterId == 0 ? employeeClaimsBo.EmployeeId : employeeClaimsBo.RequesterId);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userMessage = Utility.CheckDbNull<string>(reader["Output"]);
                    }
                }

                return userMessage;
            }
        }

        public List< cvt.officebau.com.ViewModels.Entity> AutoCompleteTravelReqNo(string searchTravelReqNo, int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();
                int statusId = (context.tbl_Status.Where(s => s.Type == "Travel" && s.Code == "Approved").Select(s => s.ID).FirstOrDefault());
                List< cvt.officebau.com.ViewModels.Entity> list = (from t in context.tbl_EmployeeTravelRequest
                                        join c in context.tbl_ClaimsRequest on t.TravelReqNo equals c.TravelReqNo into claims
                                        from cr in claims.DefaultIfEmpty()
                                        where !(t.IsDeleted)
                                            && t.DomainID == domainId
                                            && t.StatusID == statusId
                                            && t.EmployeeID == employeeId
                                            && t.TravelReqNo.Contains(searchTravelReqNo)
                                            && cr.TravelReqNo == null
                                        select new  cvt.officebau.com.ViewModels.Entity()
                                        {
                                            Name = t.TravelReqNo,
                                            Id = t.ID
                                        }).ToList();

                return list;
            }
        }

        public TravelBO GetTravelDetails(int travelReqId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();
                TravelBO list = (from t in context.tbl_EmployeeTravelRequest
                                 where t.DomainID == domainId
                                     && t.ID == travelReqId
                                 select new TravelBO()
                                 {
                                     TravelFrom = t.TravelFrom,
                                     TravelTo = t.TravelTo,
                                     TravelDate = t.TravelDate,
                                     ReturnDate = t.ReturnDate,
                                     TotalAmount = t.TotalAmount,
                                     TravelReqNo = t.TravelReqNo
                                 }).FirstOrDefault();

                return list;
            }
        }

        #endregion Claims

        #region Approval

        public List<EmployeeClaimsBo> GetEmployeeClaimsApproveList(EmployeeClaimsBo employeeClaimsBo)
        {
            List<EmployeeClaimsBo> employeeClaimsBoList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHCLAIMAPPROVE);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeClaimsBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.OperationType, DbType.String, employeeClaimsBo.Operation);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBoList.Add(
                            new EmployeeClaimsBo
                            {
                                ClaimedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ClaimedDate]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                                RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                                Counts = Utility.CheckDbNull<int>(reader[DBParam.Output.Counts]),
                                StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                StatusId = Utility.CheckDbNull<int>(reader[DBParam.Output.StatusID]),
                                SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo])
                            });
                    }
                }
            }

            return employeeClaimsBoList;
        }

        public List<EmployeeClaimsBo> GetApprovalEmployeeClaimsList(EmployeeClaimsBo employeeClaimsBo, string claimsEligible)
        {
            List<EmployeeClaimsBo> employeeClaimsBoList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETCLAIMREQUESTLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.RequesterID, DbType.Int32, employeeClaimsBo.RequesterId);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeClaimsBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.SubmittedDate, DbType.DateTime, employeeClaimsBo.SubmittedDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBoList.Add(new EmployeeClaimsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            RequesterItemId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterItemID]),
                            ClaimedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ClaimedDate]),
                            CategoryName = Utility.CheckDbNull<string>(reader[DBParam.Output.CategoryName]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            ApprovalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ApprovalAmount]),
                            RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                            FileUpload = Utility.CheckDbNull<string>(reader[DBParam.Output.FileName]),
                            StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]),
                            DestinationId = Utility.CheckDbNull<int>(reader[DBParam.Output.DestinationID]),
                            CategoryId = Utility.CheckDbNull<int>(reader[DBParam.Output.CategoryID]),
                            DestinationCity = Utility.CheckDbNull<string>(reader[DBParam.Output.DestinationCity]),
                            MetroAmount = (claimsEligible == "1" ? Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]) : Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount])),
                            FileUploadId = Utility.CheckDbNull<Guid>(reader[DBParam.Output.FileUploadID]) == null ? Guid.Empty : Utility.CheckDbNull<Guid>(reader[DBParam.Output.FileUploadID]),
                            TravelReqId = Utility.CheckDbNull<int>(reader[DBParam.Output.TravelReqID]),
                            IsClaimsEligibilityCheckRequired = (claimsEligible == "1" ? true : false)
                        });
                    }
                }
            }

            return employeeClaimsBoList;
        }

        public EmployeeClaimsBo ManageClaimsApprovel(int? financialApproverId, string remarks, string status, IList<EmployeeClaimsBo> list)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();

            using (DbConnection dbCon = Database.CreateConnection())
            {
                dbCon.Open();
                foreach (EmployeeClaimsBo item in list)
                {
                    if (item.IsChecked)
                    {
                        DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGECLAIMAPPROVE);
                        if (item.Id == 0)
                        {
                            Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, 0);
                        }
                        else
                        {
                            Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, item.Id);
                        }

                        Database.AddInParameter(dbCommand, DBParam.Input.RequesterItemID, DbType.Int32, item.RequesterItemId);
                        Database.AddInParameter(dbCommand, DBParam.Input.RequesterID, DbType.Int32, item.RequesterId);
                        Database.AddInParameter(dbCommand, DBParam.Input.FinancialApproverID, DbType.Int32, financialApproverId);
                        Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, status);
                        Database.AddInParameter(dbCommand, DBParam.Input.ApprovalAmount, DbType.Decimal, item.ApprovalAmount);
                        Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, remarks);
                        Database.AddInParameter(dbCommand, DBParam.Input.CreatedBy, DbType.Int32, Utility.UserId());
                        Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, item.IsDeleted);
                        Database.AddInParameter(dbCommand, DBParam.Input.ModifiedBy, DbType.Int32, Utility.UserId());
                        Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                        employeeClaimsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
                    }
                }
            }

            return employeeClaimsBo;
        }

        #endregion Approval

        #region Financial Approval

        public List<EmployeeClaimsBo> GetEmployeeClaimsFinancialApprovalList(EmployeeClaimsBo employeeClaimsBo)
        {
            List<EmployeeClaimsBo> employeeClaimsBoList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHCLAIMFINANCIALAPPROVE);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeClaimsBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.OperationType, DbType.String, employeeClaimsBo.Operation);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBoList.Add(
                            new EmployeeClaimsBo
                            {
                                SubmittedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedOn]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                                ApprovarName = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovarName]),
                                ApprovalId = Utility.CheckDbNull<int>(reader[DBParam.Output.ApprovalID]),
                                RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                                Counts = Utility.CheckDbNull<int>(reader[DBParam.Output.Counts]),
                                StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                StatusId = Utility.CheckDbNull<int>(reader[DBParam.Output.StatusID]),
                                SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo])
                            });
                    }
                }
            }

            return employeeClaimsBoList;
        }

        public List<EmployeeClaimsBo> GetFinancialApprovalEmployeeClaimsList(EmployeeClaimsBo employeeClaimsBo, string claimsEligible)
        {
            List<EmployeeClaimsBo> employeeClaimsBoList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETCLAIMFINANCIALAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.RequesterID, DbType.Int32, employeeClaimsBo.RequesterId);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, employeeClaimsBo.ApprovalId);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeClaimsBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.SubmittedDate, DbType.DateTime, employeeClaimsBo.SubmittedDate);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialApproverID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBoList.Add(new EmployeeClaimsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            RequesterItemId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterItemID]),
                            ClaimedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ClaimedDate]),
                            ApproverDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                            CategoryName = Utility.CheckDbNull<string>(reader[DBParam.Output.CategoryName]),
                            ApprovalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ApprovalAmount]),
                            RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                            ApprovalId = Utility.CheckDbNull<int>(reader[DBParam.Output.ApprovalID]),
                            StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            FileUpload = Utility.CheckDbNull<string>(reader[DBParam.Output.FileName]),
                            FinApproverStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.FinApproverStatus]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]),
                            DestinationId = Utility.CheckDbNull<int>(reader[DBParam.Output.DestinationID]),
                            CategoryId = Utility.CheckDbNull<int>(reader[DBParam.Output.CategoryID]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            DestinationCity = Utility.CheckDbNull<string>(reader[DBParam.Output.DestinationCity]),
                            MetroAmount = (claimsEligible == "1" ? Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]) : Utility.CheckDbNull<decimal>(reader[DBParam.Output.ApprovalAmount])),
                            FileUploadId = Utility.CheckDbNull<Guid>(reader[DBParam.Output.FileUploadID]) == null ? Guid.Empty : Utility.CheckDbNull<Guid>(reader[DBParam.Output.FileUploadID]),
                            TravelReqId = Utility.CheckDbNull<int>(reader[DBParam.Output.TravelReqID]),
                            IsClaimsEligibilityCheckRequired = (claimsEligible == "1" ? true : false)
                        });
                    }
                }
            }

            return employeeClaimsBoList;
        }

        public EmployeeClaimsBo ManageClaimFinancialApproval(int? finApproverId, string remarks, string status, IList<EmployeeClaimsBo> list)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();

            using (DbConnection dbCon = Database.CreateConnection())
            {
                dbCon.Open();
                foreach (EmployeeClaimsBo item in list)
                {
                    if (item.IsChecked)
                    {
                        DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CREATECLAIMFINANCIALAPPROVAL);
                        if (item.Id == 0)
                        {
                            Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, 0);
                        }
                        else
                        {
                            Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, item.Id);
                        }

                        Database.AddInParameter(dbCommand, DBParam.Input.RequesterItemID, DbType.Int32, item.RequesterItemId);
                        Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, item.ApprovalId);
                        Database.AddInParameter(dbCommand, DBParam.Input.SettlerID, DbType.Int32, finApproverId);
                        Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, status);
                        Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, remarks);
                        Database.AddInParameter(dbCommand, DBParam.Input.CreatedBy, DbType.Int32, Utility.UserId());
                        Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, item.IsDeleted);
                        Database.AddInParameter(dbCommand, DBParam.Input.ModifiedBy, DbType.Int32, Utility.UserId());
                        Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                        Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, item.Amount);
                        employeeClaimsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
                    }
                }
            }

            return employeeClaimsBo;
        }

        #endregion Financial Approval

        #region Settler

        public List<EmployeeClaimsBo> GetEmployeeClaimsSettleList(EmployeeClaimsBo employeeClaimsBo)
        {
            List<EmployeeClaimsBo> employeeClaimsBoList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHCLAIMSETTLE);

                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeClaimsBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.OperationType, DbType.String, employeeClaimsBo.Operation);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBoList.Add(
                            new EmployeeClaimsBo
                            {
                                SubmittedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedOn]),
                                ClaimedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.SubmittedDate]),
                                FinApprovedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.FinApprovedDate]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Requester = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                                ApprovarName = Utility.CheckDbNull<string>(reader[DBParam.Output.ApprovarName]),
                                ApprovalId = Utility.CheckDbNull<int>(reader[DBParam.Output.ApprovalID]),
                                RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                                Counts = Utility.CheckDbNull<int>(reader[DBParam.Output.Counts]),
                                StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                StatusId = Utility.CheckDbNull<int>(reader[DBParam.Output.StatusID]),
                                SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo])
                            });
                    }
                }
            }

            return employeeClaimsBoList;
        }

        public List<EmployeeClaimsBo> GetSettlerEmployeeClaimsList(EmployeeClaimsBo employeeClaimsBo)
        {
            List<EmployeeClaimsBo> employeeClaimsBoList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETCLAIMAPPROVALLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.RequesterID, DbType.Int32, employeeClaimsBo.RequesterId);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, employeeClaimsBo.ApprovalId);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, employeeClaimsBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.SubmittedDate, DbType.DateTime, employeeClaimsBo.SubmittedDate);
                Database.AddInParameter(dbCommand, DBParam.Input.SettlerID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBoList.Add(new EmployeeClaimsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            RequesterItemId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterItemID]),
                            ClaimedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ClaimedDate]),
                            ApproverDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ApprovedDate]),
                            CategoryName = Utility.CheckDbNull<string>(reader[DBParam.Output.CategoryName]),
                            ApprovalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ApprovalAmount]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SettlerAmount]),
                            RequesterId = Utility.CheckDbNull<int>(reader[DBParam.Output.RequesterID]),
                            ApprovalId = Utility.CheckDbNull<int>(reader[DBParam.Output.ApprovalID]),
                            PaymentMode = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTypeID]),
                            SubmittedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PaymentDate]),
                            StatusName = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            FileUpload = Utility.CheckDbNull<string>(reader[DBParam.Output.FileName]),
                            BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName]),
                            AccountNo = Utility.CheckDbNull<string>(reader[DBParam.Output.AccountNo]),
                            ApproverStatus = Utility.CheckDbNull<string>(reader[DBParam.Output.ApproverStatus]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]),
                            DestinationCity = Utility.CheckDbNull<string>(reader[DBParam.Output.DestinationCity]),
                            MetroAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                            FileUploadId = Utility.CheckDbNull<Guid>(reader[DBParam.Output.FileUploadID]) == null ? Guid.Empty : Utility.CheckDbNull<Guid>(reader[DBParam.Output.FileUploadID]),
                        });
                    }
                }
            }

            return employeeClaimsBoList;
        }

        public EmployeeClaimsBo ManageClaimSettle(int paymentTypeId, string remarks, string status, DateTime submittedDate, IList<EmployeeClaimsBo> list)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();

            using (DbConnection dbCon = Database.CreateConnection())
            {
                dbCon.Open();
                foreach (EmployeeClaimsBo item in list)
                {
                    if (item.IsChecked)
                    {
                        DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CREATE_CLAIMSETTLE);
                        Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, item.Id == 0 ? 0 : item.Id);
                        Database.AddInParameter(dbCommand, DBParam.Input.RequesterItemID, DbType.Int32, item.RequesterItemId);
                        Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, item.ApprovalId);
                        Database.AddInParameter(dbCommand, DBParam.Input.PaymentTypeID, DbType.Int32, paymentTypeId);
                        Database.AddInParameter(dbCommand, DBParam.Input.SettlerAmount, DbType.Decimal, item.ApprovalAmount);
                        Database.AddInParameter(dbCommand, DBParam.Input.PaymentDate, DbType.DateTime, submittedDate);
                        Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, status);
                        Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, remarks);
                        Database.AddInParameter(dbCommand, DBParam.Input.CreatedBy, DbType.Int32, Utility.UserId());
                        Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, item.IsDeleted);
                        Database.AddInParameter(dbCommand, DBParam.Input.ModifiedBy, DbType.Int32, Utility.UserId());
                        Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                        employeeClaimsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
                    }
                }
            }

            return employeeClaimsBo;
        }

        #endregion Settler

        #region Email

        public EmployeeClaimsBo GetClaimMailerDetails(int fromId, int toId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                EmployeeClaimsBo mailerDetails = (from e in context.tbl_EmployeeMaster
                                                  join d in context.tbl_Designation on e.DesignationID equals d.ID into desi
                                                  from designation in desi.DefaultIfEmpty()
                                                  where e.ID == fromId
                                                  select new EmployeeClaimsBo
                                                  {
                                                      Regards = e.FullName + (" [ " + designation.Name + " ] "),
                                                      CcEmail = e.EmailID,
                                                      LoginCode = e.LoginCode,
                                                      ReceiverName = context.tbl_EmployeeMaster.FirstOrDefault(a => a.ID == toId).FullName,
                                                      ToEmail = context.tbl_EmployeeMaster.FirstOrDefault(a => a.ID == toId).EmailID,
                                                      PrefixId = context.tbl_EmployeeMaster.FirstOrDefault(a => a.ID == toId).PrefixID,
                                                      PrefixCode = context.tbl_CodeMaster.FirstOrDefault(c => c.ID == context.tbl_EmployeeMaster.FirstOrDefault(a => a.ID == toId).PrefixID).Code
                                                  }).FirstOrDefault();
                return mailerDetails;
            }
        }

        public string GetClaimEmailDetails(string idList)
        {
            string htmlContent = string.Empty;
            decimal total = 0;
            using (FSMEntities context = new FSMEntities())
            {
                foreach (string claimId in idList.Split(','))
                {
                    if (!string.IsNullOrWhiteSpace(claimId))
                    {
                        int id = Convert.ToInt32(claimId);
                        tbl_ClaimsRequest claimsRequest = context.tbl_ClaimsRequest.FirstOrDefault(b => b.ID == id);
                        if (claimsRequest != null)
                        {
                            total = total + claimsRequest.Amount;
                            htmlContent = htmlContent + @"<tr>
                                             <td>
                                                 <p class=MsoNormal>
                                                  <span style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black;'>
                                                      " + context.tbl_ClaimCategory.FirstOrDefault(c => c.ID == claimsRequest.CategoryID)?.Name + @"
                                                     </span>
                                                 </p>
                                             </ td >
                                             <td style=text-align:center;>
                                                 <p class=MsoNormal>
                                                  <span style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black'>
                                                       " + claimsRequest.ClaimDate.ToString("dd-MMM-yyyy") + @"
                                                  </span>
                                              </p>
                                             </td>
                                             <td style=text-align:right;>
                                                    <p class=MsoNormal>
                                                     <span style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black'>
                                                       " + claimsRequest.Amount.ToString("##,#.00") + @"
                                                     </span>
                                                 </p>
                                             </td>
                                         </tr>";
                        }
                    }
                }

                htmlContent = htmlContent + @"<tr style = 'text-align:right;'>
                                                 <td colspan = 2>
                                                      <p class=MsoNormal>
                                                        <b style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black;'>
                                                            Total <o:p>&nbsp;</o:p>
                                                        </b>
                                                    </p>
                                                 </td>
                                                 <td>
                                                     <p class=MsoNormal>
                                                         <b style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black'>
                                                            " + total.ToString("##,#.00") + @"
                                                             <o:p> &nbsp;</o:p>
                                                             </b>
                                                         </p>
                                                     </td> ";
                return htmlContent;
            }
        }

        public string GetClaimApproverEmailDetails(IList<EmployeeClaimsBo> list, string type)
        {
            string htmlContent = string.Empty;
            decimal total = 0;
            decimal? approvedTotal = 0;
            using (FSMEntities context = new FSMEntities())
            {
                if (type.Contains("Approved"))
                {
                    htmlContent = htmlContent + @" <td>
                                                      <p class=MsoNormal>
                                                          <b style='font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black'>
                                                              Approved Amount
                                                              <o:p>&nbsp;</o:p>
                                                          </b>
                                                      </p>
                                                   </td>";
                }

                foreach (EmployeeClaimsBo item in list)
                {
                    EmployeeClaimsBo claimsRequest = (from a in context.tbl_ClaimsApprove
                                                      join r in context.tbl_ClaimsRequest on a.ClaimItemID equals r.ID
                                                      join c in context.tbl_ClaimCategory on r.CategoryID equals c.ID
                                                      join f in context.tbl_ClaimsFinancialApprove on a.ClaimItemID equals f.ClaimItemID into finance
                                                      from fn in finance.DefaultIfEmpty()
                                                      join s in context.tbl_ClaimsSettle on a.ClaimItemID equals s.ClaimItemID into settle
                                                      from se in settle.DefaultIfEmpty()
                                                      where type.Contains("Manager") && a.ClaimItemID == item.RequesterItemId
                                                         || type.Contains("Finance") && fn.ClaimItemID == item.RequesterItemId
                                                         || type.Contains("Settlement") && se.ClaimItemID == item.RequesterItemId
                                                      select new EmployeeClaimsBo
                                                      {
                                                          ClaimedDate = r.ClaimDate,
                                                          Amount = r.Amount,
                                                          ApprovalAmount = a.ApprovedAmount,
                                                          CategoryName = c.Name
                                                      }).FirstOrDefault();
                    if (claimsRequest != null)
                    {
                        total = total + claimsRequest.Amount;
                        approvedTotal = approvedTotal + claimsRequest.ApprovalAmount;
                        htmlContent = htmlContent + @"</tr><tr>
                                                         <td>
                                                             <p class=MsoNormal>
                                                              <span style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black;'>
                                                                  " + claimsRequest.CategoryName + @"
                                                                 </span>
                                                             </p>
                                                         </ td >
                                                         <td style=text-align:center;>
                                                             <p class=MsoNormal>
                                                              <span style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black'>
                                                                   " + claimsRequest.ClaimedDate.ToString("dd-MMM-yyyy") + @"
                                                              </span>
                                                          </p>
                                                         </td>
                                                         <td style=text-align:right;>
                                                                <p class=MsoNormal>
                                                                 <span style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black'>
                                                                   " + claimsRequest.Amount.ToString("##,#.00") + @"
                                                                 </span>
                                                             </p>
                                                         </td>";
                        if (type.Contains("Approved"))
                        {
                            htmlContent = htmlContent + @" <td style=text-align:right;>
                                                          <p class=MsoNormal>
                                                                <span style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black'>
                                                                  " + Convert.ToDecimal(claimsRequest.ApprovalAmount).ToString("##,#.00") + @"
                                                                </span>
                                                           </p>
                                                       </td>";
                        }
                    }
                }

                htmlContent = htmlContent + @"</tr><tr style = 'text-align:right;'>
                                                 <td colspan = 2>
                                                      <p class=MsoNormal>
                                                        <b style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black;'>
                                                            Total <o:p>&nbsp;</o:p>
                                                        </b>
                                                    </p>
                                                 </td>
                                                 <td>
                                                     <p class=MsoNormal>
                                                         <b style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black'>
                                                            " + total.ToString("##,#.00") + @"
                                                             <o:p> &nbsp;</o:p>
                                                             </b>
                                                         </p>
                                                     </td> ";
                if (type.Contains("Approved"))
                {
                    htmlContent = htmlContent + @" <td>
                                                      <p class=MsoNormal>
                                                         <b style = 'font-size:10.5pt;font-family:Segoe UI,sans-serif;color:black'>
                                                            " + Convert.ToDecimal(approvedTotal).ToString("##,#.00") + @"
                                                             <o:p> &nbsp;</o:p>
                                                         </b>
                                                       </p>
                                                     </td>";
                }

                return htmlContent + "</tr>";
            }
        }

        public string PopulateClaimMailBody(string mailToName, string idList, string regards, string prefix)
        {
            string content = @"I would like to request a claim for the below mentioned expenses.
                               Kindly consider my request.";

            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/Claims.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Name}", mailToName);
            body = body.Replace("{Content}", content);
            body = body.Replace("{ClaimDetails}", GetClaimEmailDetails(idList));
            body = body.Replace("{Regards}", regards);
            body = body.Replace("{Prefix}", prefix);
            return body;
        }

        public string PopulateClaimApproverMailBody(string mailToName, IList<EmployeeClaimsBo> list, string regards, string type, string prefix)
        {
            string content = string.Empty;

            if (type.Contains("Approved"))
            {
                content = "Your claim request has been approved.";
            }
            else if (type.Contains("Rejected"))
            {
                content = "Your claim request has been rejected.";
            }
            else if (type.Contains("Settlement"))
            {
                content = "Your claim request has been settled.";
            }

            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/Claims.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Name}", mailToName);
            body = body.Replace("{Content}", content);
            body = body.Replace("{ClaimDetails}", GetClaimApproverEmailDetails(list, type));
            body = body.Replace("{Regards}", regards);
            body = body.Replace("{Prefix}", prefix);
            return body;
        }

        #endregion Email
    }
}