﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Services
{
    public class NewsandEventsRepository : BaseRepository
    {
        public List<NewsAndEventsBo> Searchnewsandeventslist()
        {
            List<NewsAndEventsBo> newsandeventslist = new List<NewsAndEventsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHNEWSANDEVENTS);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        newsandeventslist.Add(new NewsAndEventsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PublishedOn]),
                            EndDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EndDate]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.PublishedBy]),
                            UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.CreatedBy]),
                            Guid = reader[DBParam.Output.HistoryID].ToString(),
                            FileInfarmationList = FileInformation(reader[DBParam.Output.HistoryID].ToString(), "MULTI"),
                            StartDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.StartDate])
                        });
                    }
                }
            }

            return newsandeventslist;
        }

        public string ManageNewsAndEvents(NewsAndEventsBo newsAndEventsBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGENEWSANDEVENTS);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, newsAndEventsBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, newsAndEventsBo.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.Description, DbType.String, newsAndEventsBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, newsAndEventsBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.EventDate, DbType.DateTime, newsAndEventsBo.EventDate);

                newsAndEventsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return newsAndEventsBo.UserMessage;
        }

        public List<NewsAndEventsBo> Searchfilepreview(string guidName, string description)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<NewsAndEventsBo> list = (from t in context.tbl_MultipleFileUpload
                                              where t.ID == new Guid(guidName)
                                              select new NewsAndEventsBo
                                              {
                                                  Id = t.RowID,
                                                  Name = t.FileName,
                                                  Description = description
                                                  //  ID = t.ID,
                                              }).ToList();
                return list;
            }
        }

        public List<FileInfarmation> FileInformation(string guidName, string listno)
        {
            string filePath = "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("EVENTS");
            if (listno.ToUpper() == "SINGLE")
            {
                using (FSMEntities context = new FSMEntities())
                {
                    List<FileInfarmation> list = (from t in context.tbl_MultipleFileUpload
                                                  where t.ID == new Guid(guidName)
                                                  select new FileInfarmation
                                                  {
                                                      FileName = t.FileName,
                                                      Path = filePath.Replace("\\", "//")
                                                  }).Take(1).ToList();
                    return list;
                }
            }

            using (FSMEntities context = new FSMEntities())
            {
                List<FileInfarmation> list = (from t in context.tbl_MultipleFileUpload
                                              where t.ID == new Guid(guidName)
                                              select new FileInfarmation
                                              {
                                                  FileName = t.FileName,
                                                  Path = filePath.Replace("\\", "//")
                                              }).ToList();
                return list;
            }
        }

        public NewsAndEventsBo GetNewsAndEvents(int id)
        {
            string filePath = "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("EVENTS");
            using (FSMEntities context = new FSMEntities())
            {
                NewsAndEventsBo result = new NewsAndEventsBo();
                if (id != 0)
                {
                    result = (from c in context.tbl_NewsAndEvents
                              where c.ID == id
                              select new NewsAndEventsBo
                              {
                                  Name = c.Name,
                                  Description = c.Description,
                                  Id = c.ID,
                                  StartDate = c.StartDate,
                                  EndDate = c.EndDate,
                                  EventDate=c.EventDate,
                                  BusinessUnitId = c.BusinessUnit
                              }).FirstOrDefault();
                }

                if (result != null)
                {
                    result.Guid = Convert.ToString(context.tbl_NewsAndEvents.Where(b => b.ID == result.Id).Select(b => b.HistoryID).FirstOrDefault());
                    result.Path = filePath.Replace("\\", "//");
                    result.FileInfarmationList = FileInformation(result.Guid, "MULTI");
                }
                return result;
            }
        }

        public string GetDescription(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                string result = string.Empty;
                if (id != 0)
                {
                    result = Convert.ToString(context.tbl_NewsAndEvents.Where(b => b.ID == id).Select(b => b.Description).FirstOrDefault());
                }

                return result;
            }
        }

        public string PublishNewsAndEvents(NewsAndEventsBo newsAndEventsBo)
        {
            FSMEntities context = new FSMEntities();
            tbl_NewsAndEvents newsAndEvents = context.tbl_NewsAndEvents.FirstOrDefault(b => b.ID == newsAndEventsBo.Id);

            if (newsAndEvents != null)
            {
                newsAndEvents.EndDate = newsAndEventsBo.EndDate;
                newsAndEvents.BusinessUnit = newsAndEventsBo.BusinessUnitId;
                newsAndEvents.PublishedOn = DateTime.Now;
                newsAndEvents.ModifiedOn = DateTime.Now;
                newsAndEvents.ModifiedBy = Utility.UserId();
                newsAndEvents.PublishedBy = Utility.UserId();
                newsAndEvents.StartDate = newsAndEventsBo.StartDate;

                context.Entry(newsAndEvents).State = EntityState.Modified;
            }

            context.SaveChanges();

            newsAndEventsBo.Id = newsAndEventsBo.Id;
            return BOConstants.Published;
        }

        public List<NewsAndEventsBo> SearchnewsandeventsViewrslist(string type)
        {
            List<NewsAndEventsBo> newsandeventslist = new List<NewsAndEventsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHNEWSANDEVENTSBU);

                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        newsandeventslist.Add(new NewsAndEventsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            CreatedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PublishedOn]),
                            EndDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EndDate]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.PublishedBy]),
                            UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.CreatedBy]),
                            Guid = reader[DBParam.Output.HistoryID].ToString(),
                            FileInfarmationList = FileInformation(reader[DBParam.Output.HistoryID].ToString(), "MULTI"),
                            StartDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.StartDate]),
                            EventDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.EventDate])
                        });
                    }
                }
            }

            return newsandeventslist;
        }
    }
}