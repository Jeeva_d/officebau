﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class ReportDashboardRepository : BaseRepository
    {
        public DataTable SearchReportDashboard(ReportDashboardBo reportDashboardBo)
        {
            DataSet ds = new DataSet();
            DataTable dtreportDashboard = new DataTable("GetReportDashboard");
            ds.Tables.Add(dtreportDashboard);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_REPORTDASHBOARD);
                Database.AddInParameter(dbCommand, DBParam.Input.ReportType, DbType.String, reportDashboardBo.ReportType);
                Database.AddInParameter(dbCommand, DBParam.Input.GroupBy, DbType.String, reportDashboardBo.GroupBy);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, reportDashboardBo.IsActive);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtreportDashboard);
                }

                return dtreportDashboard;
            }
        }

        public string SearchReportDashboardBarchart(ReportDashboardBo reportDashboardBo)
        {
            DataSet ds = new DataSet();
            DataTable dtreportDashboard = new DataTable("GetReportDashboard");
            ds.Tables.Add(dtreportDashboard);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_REPORTDASHBOARD);
                Database.AddInParameter(dbCommand, DBParam.Input.ReportType, DbType.String, reportDashboardBo.ReportType);
                Database.AddInParameter(dbCommand, DBParam.Input.GroupBy, DbType.String, reportDashboardBo.GroupBy);
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, reportDashboardBo.IsActive);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtreportDashboard);
                }

                dtreportDashboard.Columns.RemoveAt(dtreportDashboard.Columns.Count - 1);
                string result = string.Empty;
                foreach (DataColumn col in dtreportDashboard.Columns)
                {
                    result += Utility.CheckDbNull<string>(col.ToString()) + "/";
                }

                result = result.TrimEnd('/') + "#";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count - 1; i++)
                    {
                        for (int j = 0; j < ds.Tables[0].Rows[i].ItemArray.Length; j++)
                        {
                            result += Utility.CheckDbNull<string>(ds.Tables[0].Rows[i][j].ToString()) + "/";
                        }

                        result = result.TrimEnd('/') + "#";
                    }
                }

                return result.TrimEnd('#');
            }
        }

        public ReportDashboardBo GetDashboardTiles()
        {
            ReportDashboardBo reportDashboardBo = new ReportDashboardBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.REPORTDASHOARDTILES);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        reportDashboardBo = new ReportDashboardBo
                        {
                            HeadcountDesignation = Utility.CheckDbNull<int>(reader["HeadcountforDesignation"]),
                            HeadcountDepartment = Utility.CheckDbNull<int>(reader["HeadcountforDepartment"]),
                            CompensationDesignation = Utility.CheckDbNull<decimal>(reader["CompensationforDesignation"]),
                            CompensationDepartment = Utility.CheckDbNull<decimal>(reader["CompensationforDepartment"])
                        };
                    }
                }

                return reportDashboardBo;
            }
        }
    }
}