﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace cvt.officebau.com.Services
{
    public class IncomeRepository : BaseRepository
    {
        #region Search Income

        public List<IncomeBo> SearchIncome(SearchParamsBo searchParamsBo)
        {
            List<IncomeBo> incomeBoList = new List<IncomeBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_INCOME);

                Database.AddInParameter(dbCommand, DBParam.Input.CustomerName, DbType.String, searchParamsBo.CustomerName);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, searchParamsBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount);
                Database.AddInParameter(dbCommand, DBParam.Input.RevenueCenterID, DbType.Int32, searchParamsBo.CostCenterId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBoList.Add(
                            new IncomeBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                InvoiceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.InvoiceDate]),
                                DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                                InvoiceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo]),
                                CustomerId = Utility.CheckDbNull<int>(reader[DBParam.Output.CustomerID]),
                                CustomerName = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName]),
                                Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                                ReceivedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ReceivedAmount]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                ScreenType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                                 FileID= Utility.CheckDbNull<string>(reader[DBParam.Output.HistoryID])
                                
                            });
                    }
                }
            }

            return incomeBoList;
        }

        #endregion Search Income

        #region Supporting function

        private object ValueOrDBNullIfZero(string val)
        {
            if (string.IsNullOrEmpty(val))
            {
                return DBNull.Value;
            }

            return val;
        }

        #endregion Supporting function

        public List<InvoiceHoldingsBO> SearchCustomerSoDetails(int id)
        {
            List<InvoiceHoldingsBO> invoiceReceivableBoList = new List<InvoiceHoldingsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchCustomerSODetails");
                Database.AddInParameter(dbCommand, DBParam.Input.CustomerID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        invoiceReceivableBoList.Add(new InvoiceHoldingsBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            InvoiceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount])
                        });
                    }
                }
            }

            return invoiceReceivableBoList;
        }

        #region Get Income

        public IncomeBo GetIncome(int id)
        {
            IncomeBo incomeBo = new IncomeBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_INCOME);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBo = new IncomeBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            InvoiceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.InvoiceDate]),
                            DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                            CustomerId = Utility.CheckDbNull<int>(reader[DBParam.Output.CustomerID]),
                            CustomerName = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName].ToString()),
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.BillingAddress].ToString()),
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms].ToString()),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description].ToString()),
                            InvoiceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo].ToString()),
                            ScreenType = Utility.CheckDbNull<string>(reader[DBParam.Output.ScreenType].ToString()),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                            ReceivedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ReceivedAmount]),
                            DiscountPercentage = Utility.CheckDbNull<decimal>(reader[DBParam.Output.DiscountPercentage]),
                            DiscountValue = Utility.CheckDbNull<decimal>(reader[DBParam.Output.DiscountValue]),
                            CurrencyRate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CurrencyRate]),
                            Currency = Utility.CheckDbNull<string>(reader[DBParam.Output.Currency]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            CompanyName = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyName]),
                            CompanyLogo = Utility.CheckDbNull<string>(reader[DBParam.Output.Logo]),
                            CompanyAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyAddress]),
                            StateName = Utility.CheckDbNull<string>(reader[DBParam.Output.STATENAME]),
                            CompanyEmaild = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyEmaild]),
                            CompanyGstno = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyGSTNO]),
                            CompanyContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyContactNo]),
                            CustomerGstNo = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerGSTNo]),
                            CustomerState = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerState]),
                            CostCenter = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.RevenueCenterID])
                            },
                            Discount = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.DiscountID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.DiscountType])
                            },
                            ToEmail = Utility.CheckDbNull<string>(reader["ToEmail"]),
                            CcEmail = Utility.CheckDbNull<string>(reader["CCEmail"]),
                            BccEmail = Utility.CheckDbNull<string>(reader["BCCEmail"]),
                            PaymentFavour = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentFavour]),
                            UserMessage = Utility.CheckDbNull<string>(reader["SoNos"]),

                            fileUpload = Utility.CheckDbNull<string>(reader[DBParam.Output.FileName]),
                         

                            FileID = Utility.CheckDbNull<string>(reader[DBParam.Output.HistoryID])
                        };
                    }
                }
            }

            incomeBo.ItemDetail = SearchItemDetailList(id);
            incomeBo.InvoiceHoldingDetails = SearchInvoiceHoldingList(id);
            if (!string.IsNullOrEmpty(incomeBo.CompanyLogo))
            {
                incomeBo.CompanyLogo = DownloadFile(incomeBo.CompanyLogo).FileName;
            }

            return incomeBo;
        }
        //Temp 

        public static FileUpload DownloadFile(string fileId)
        {
            var consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            var con = new SqlConnection(consString);

            var fileUpload = new FileUpload();
            var cmd = new SqlCommand(Constants.GETUPLOADEDFILE)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.FileID, fileId);
            con.Open();
            using (IDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    fileUpload.FileName = reader[DBParam.Output.FileName].ToString();
                    fileUpload.FileType = reader[DBParam.Output.FileType].ToString();
                    fileUpload.OriginalFileName = reader[DBParam.Output.OriginalFileName].ToString();
                }
            }

            con.Close();

            return fileUpload;
        }
        public ProductMasterBo GetGstHsn(int id, string type)
        {
            ProductMasterBo productMasterBo = new ProductMasterBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETHSNCODE);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        productMasterBo = new ProductMasterBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Sgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            Cgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            SaleRate = Utility.CheckDbNull<decimal>(reader["SalesPrice"]),
                            PurchaseRate = Utility.CheckDbNull<decimal>(reader["PurchasePrice"]),
                            HsnCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Code])
                        };
                    }
                }

                return productMasterBo;
            }
        }

        public List<ItemDetailsBO> SearchItemDetailList(int id)
        {
            List<ItemDetailsBO> itemDetailsBoList = new List<ItemDetailsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_INVOICEITEMLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.InvoiceID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        itemDetailsBoList.Add(new ItemDetailsBO
                        {
                            ProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.ProductID]),
                            ItemRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            QTY = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            Rate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]),
                            SGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            CGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            SGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTAmount]),
                            CGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGSTAmount]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTAmount]) + Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGSTAmount]) +
                                     Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) * Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]),
                            ID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            IsProduct = Utility.CheckDbNull<int>(reader[DBParam.Output.IsProduct]),
                            ProductList = masterDataRepository.GetPreRequestProduct("Sales"),
                            ProductName = Utility.CheckDbNull<string>(reader[DBParam.Output.ProductName]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.HsnCode]),
                           // FileID = Utility.CheckDbNull<string>(reader[DBParam.Output.HistoryID])
                        });
                        itemDetailsBoList[itemDetailsBoList.Count - 1].ProductList.Find(a => a.Value == itemDetailsBoList[itemDetailsBoList.Count - 1].ProductID.ToString()).Selected = true;
                    }
                }

                if (itemDetailsBoList.Count == 0)
                {
                    itemDetailsBoList.Add(new ItemDetailsBO
                    {
                        ProductList = masterDataRepository.GetPreRequestProduct("Sales")
                    });
                }
            }

            return itemDetailsBoList;
        }

        #endregion Get Income

        #region Manange Invoice

        public string ManageInvoice(IncomeBo incomeBo)
        {
            string userMessage = string.Empty;
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);

            switch (incomeBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    userMessage = CreateInvoice(incomeBo, con);
                    break;

                case Operations.Update:
                    userMessage = UpdateInvoice(incomeBo, con);
                    break;

                case Operations.Delete:
                    userMessage = DeleteInvoice(incomeBo, con);
                    break;
            }

            return userMessage;
        }

        private string CreateInvoice(IncomeBo incomeBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.CREATE_INVOICE)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            FillInvoiceParameters(incomeBo, cmd);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string UpdateInvoice(IncomeBo incomeBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.UPDATE_INVOICE)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            FillInvoiceParameters(incomeBo, cmd);
            cmd.Parameters.AddWithValue(DBParam.Input.ID, incomeBo.Id);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string DeleteInvoice(IncomeBo incomeBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.DELETE_INVOICE)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, incomeBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private void FillInvoiceParameters(IncomeBo incomeBo, SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue(DBParam.Input.InvoiceNo, ValueOrDBNullIfZero(incomeBo.InvoiceNo));
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(incomeBo.Description));
            cmd.Parameters.AddWithValue(DBParam.Input.Date, incomeBo.InvoiceDate);
            cmd.Parameters.AddWithValue(DBParam.Input.CustomerID, incomeBo.CustomerId);
            cmd.Parameters.AddWithValue(DBParam.Input.DueDate, Utility.CheckDbNull<DateTime>(incomeBo.DueDate));
            cmd.Parameters.AddWithValue(DBParam.Input.Type, incomeBo.ScreenType);
            cmd.Parameters.AddWithValue(DBParam.Input.RevenueCenterID, incomeBo.CostCenter == null ? 0 : incomeBo.CostCenter.Id ?? 0);
            cmd.Parameters.AddWithValue(DBParam.Input.DiscountID, incomeBo.Discount == null ? 0 : incomeBo.Discount.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.DiscountValue, incomeBo.DiscountValue);
            cmd.Parameters.AddWithValue(DBParam.Input.DiscountPercentage, incomeBo.DiscountPercentage);
            cmd.Parameters.AddWithValue(DBParam.Input.BillAddress, ValueOrDBNullIfZero(incomeBo.BillingAddress));
            cmd.Parameters.AddWithValue(DBParam.Input.CurrencyRate, incomeBo.CurrencyRate);
            cmd.Parameters.AddWithValue(DBParam.Input.UploadFile, ValueOrDBNullIfZero (incomeBo.fileUpload));
            cmd.Parameters.AddWithValue(DBParam.Input.INVOICEITEM, Utility.ToDataTable(incomeBo.ItemDetail, "InvoiceItem"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.FileID, ValueOrDBNullIfZero(incomeBo.FileID));
            

        }

        #endregion Manange Invoice

        #region Invoice ReceivableList

        public List<InvoiceReceivableBO> SearchInvoiceReceivableList(int? id, int customerId)
        {
            List<InvoiceReceivableBO> invoiceReceivableBoList = new List<InvoiceReceivableBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_RECEIVABLELIST);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.CustomerID, DbType.Int32, customerId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        invoiceReceivableBoList.Add(new InvoiceReceivableBO
                        {
                            ID = Utility.CheckDbNull<int>(reader[DBParam.Output.InvoicePaymentID]),
                            InvoiceID = Utility.CheckDbNull<int>(reader[DBParam.Output.InvoiceID]),
                            InvoiceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            InvoicedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.InvoicedDate]),
                            DueDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DueDate]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            ReceivedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ReceivedAmount]),
                            OutstandingAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OutstandingAmount]),
                            ReceivableAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.updateAmount]),
                            HoldingAmount = Utility.CheckDbNull<decimal>(reader["HoldingAmount"]),
                            HoldingsCount = Utility.CheckDbNull<int>(reader["HoldingsCount"]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.ProductName])
                        });
                    }
                }
            }

            return invoiceReceivableBoList;
        }

        public List<InvoiceHoldingsBO> SearchInvoiceHoldingList(int id)
        {
            List<InvoiceHoldingsBO> invoiceReceivableBoList = new List<InvoiceHoldingsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchinvoiceHoldingList");

                Database.AddInParameter(dbCommand, DBParam.Input.InvoiceID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        invoiceReceivableBoList.Add(new InvoiceHoldingsBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            InvoiceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.LedgerName]),
                            InvoiceID = Utility.CheckDbNull<int>(reader[DBParam.Output.InvoiceID]),
                            Date = Utility.CheckDbNull<DateTime?>(reader[DBParam.Output.Date]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            OutstandingAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OutstandingAmount]),
                            InvoicedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.InvoicedAmount]),
                            LedgerID = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID]),
                            LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger")
                        });
                        if (invoiceReceivableBoList[invoiceReceivableBoList.Count - 1].LedgerID != 0)
                        {
                            invoiceReceivableBoList[invoiceReceivableBoList.Count - 1].LedgerList.Find(a => a.Value == invoiceReceivableBoList[invoiceReceivableBoList.Count - 1].LedgerID.ToString()).Selected = true;
                        }
                    }
                }
            }

            return invoiceReceivableBoList;
        }

        public string ManageInvoiceHoldings(IList<InvoiceHoldingsBO> invoiceHoldingsBOs)
        {
            string usermessage = string.Empty;

            foreach (InvoiceHoldingsBO b in invoiceHoldingsBOs)
            {
                if (b.Date.HasValue)
                {
                    if (b.LedgerID != 0)
                    {
                        if (b.Amount != 0)
                        {
                            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                            SqlConnection con = new SqlConnection(consString);
                            SqlCommand cmd = new SqlCommand("ManageInvoiceHolding", con)
                            {
                                CommandType = CommandType.StoredProcedure,
                                Connection = con
                            };
                            cmd.Parameters.AddWithValue(DBParam.Input.ID, b.Id);
                            cmd.Parameters.AddWithValue(DBParam.Input.InvoiceID, invoiceHoldingsBOs[0].InvoiceID);
                            cmd.Parameters.AddWithValue(DBParam.Input.Date, b.Date);
                            cmd.Parameters.AddWithValue(DBParam.Input.Amount, b.Amount);
                            cmd.Parameters.AddWithValue(DBParam.Input.IsDeleted, b.IsDeleted);
                            cmd.Parameters.AddWithValue(DBParam.Input.LedgerID, b.LedgerID);
                            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
                            con.Open();
                            usermessage = cmd.ExecuteScalar().ToString();
                        }
                    }
                }
            }

            return usermessage;
        }

        #endregion Invoice ReceivableList

        #region Receivables

        public string ManageReceivable(IncomeBo incomeBo)
        {
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);
            string usermessage;
            SqlCommand cmd = new SqlCommand(Constants.MANAGE_RECEIVABLE, con)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, incomeBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(incomeBo.Description));
            cmd.Parameters.AddWithValue(DBParam.Input.Date, incomeBo.ReceivedDate);
            cmd.Parameters.AddWithValue(DBParam.Input.CustomerID, incomeBo.CustomerId);
            cmd.Parameters.AddWithValue(DBParam.Input.TotalAmount, incomeBo.TotalAmount);
            cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, incomeBo.PaymentMode == null ? 0 : incomeBo.PaymentMode.Id);
            if (incomeBo.Bank.Id != null)
            {
                cmd.Parameters.AddWithValue(DBParam.Input.BankID, incomeBo.Bank.Id);
            }
            else
            {
                cmd.Parameters.AddWithValue(DBParam.Input.BankID, 0);
            }

            cmd.Parameters.AddWithValue(DBParam.Input.LedgerID, incomeBo.Ledger == null ? 0 : incomeBo.Ledger.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.Reference, ValueOrDBNullIfZero(incomeBo.Reference));
            cmd.Parameters.AddWithValue(DBParam.Input.IsDeleted, incomeBo.IsDeleted);
            cmd.Parameters.AddWithValue(DBParam.Input.InvoiceReceivable, Utility.ToDataTable(incomeBo.ReceivableDetail, "InvoiceReceivable"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            con.Open();
            usermessage = cmd.ExecuteScalar().ToString();
            if (usermessage == Notification.Inserted)
            {
                string bank = "Cash";
                if (incomeBo.Bank.Id != null)
                {
                    MasterDataRepository masterDataRepository = new MasterDataRepository();
                    incomeBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
                    bank = incomeBo.BankList.Find(a => a.Value == incomeBo.Bank.Id.ToString()).Text;
                }

                CommonRepository commonRepository = new CommonRepository();
                incomeBo.PaymentModeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
                PopulateMailBodyInvoice(incomeBo.CustomerName, incomeBo.TotalAmount, bank, incomeBo.ReceivedDate, incomeBo.Description,
                    incomeBo.PaymentModeList.Find(a => a.Value == incomeBo.PaymentMode.Id.ToString()).Text,
                    incomeBo.ReceivableDetail.Find(a => Utility.CheckDbNull<string>(a.UserMessage) != "").UserMessage
                );
            }

            return usermessage;
        }

        public IncomeBo GetInvoiceReceivable(int id)
        {
            IncomeBo incomeBo = new IncomeBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_INVOICERECEIVABLE);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBo = new IncomeBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ReceivedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ReceivedDate]),
                            CustomerId = Utility.CheckDbNull<int>(reader[DBParam.Output.CustomerID]),
                            CustomerName = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName]),
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms]),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                            ReceivedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ReceivedAmount]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            Ledger = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID])
                            },
                            PaymentMode = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.PaymentModeID])
                            },
                            Bank = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.BankID])
                            },
                            CurrencyRate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CurrencyRate]),
                            Currency = Utility.CheckDbNull<string>(reader[DBParam.Output.Currency]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Reference = Utility.CheckDbNull<string>(reader[DBParam.Output.Reference])
                        };
                    }
                }
            }

            if (incomeBo.CustomerId != 0)
            {
                incomeBo.ReceivableDetail = SearchInvoiceReceivableList(incomeBo.Id, incomeBo.CustomerId);
            }

            return incomeBo;
        }

        public IncomeBo GetInvoiceCustomerDetails(int id)
        {
            IncomeBo incomeBo = new IncomeBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_INVOICECUSTOMERDETAIL);

                Database.AddInParameter(dbCommand, DBParam.Input.CustomerID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBo = new IncomeBo
                        {
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms]),
                            Currency = Utility.CheckDbNull<string>(reader[DBParam.Output.CurrencyName])
                        };
                    }
                }
            }

            return incomeBo;
        }

        #endregion Receivables

        #region AR

        #region Search Receipts

        public List<IncomeBo> SearchReceipts(SearchParamsBo searchParamsBo)
        {
            List<IncomeBo> incomeBoList = new List<IncomeBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Search_Receipts");

                Database.AddInParameter(dbCommand, DBParam.Input.PartyName, DbType.String, searchParamsBo.PartyName);
                Database.AddInParameter(dbCommand, DBParam.Input.LedgerName, DbType.String, searchParamsBo.LedgerName);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBoList.Add(
                            new IncomeBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                InvoiceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                PartyName = Utility.CheckDbNull<string>(reader[DBParam.Output.PartyName]),
                                LedgerName = Utility.CheckDbNull<string>(reader[DBParam.Output.LedgerName]),
                                Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                ReceivedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Bank = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName])
                                },
                                PaymentMode = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentMode])
                                }
                            });
                    }
                }
            }

            return incomeBoList;
        }

        #endregion Search Receipts

        public IncomeBo GetReceiptsDetails(int? ledgerId, int? id)
        {
            List<ItemDetailsBO> incomeBoList = new List<ItemDetailsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchJournalReceipts");
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.LedgerID, DbType.Int32, ledgerId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBoList.Add(
                            new ItemDetailsBO
                            {
                                ID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                InvoiceID = Utility.CheckDbNull<int>(reader["RefID"]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaidAmount]),
                                ProductName = Utility.CheckDbNull<string>(reader[DBParam.Output.JournalNo]),
                                Rate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                SGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.updateAmount])
                            });
                    }
                }
            }

            IncomeBo incomeBo = new IncomeBo
            {
                ItemDetail = incomeBoList
            };
            return incomeBo;
        }

        public string ManageReceipts(IncomeBo incomeBo)
        {
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);
            SqlCommand cmd = new SqlCommand("Manage_receipts", con)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, incomeBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.Date, incomeBo.ReceivedDate);
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(incomeBo.Description));
            cmd.Parameters.AddWithValue(DBParam.Input.PartyName, incomeBo.PartyName);
            cmd.Parameters.AddWithValue(DBParam.Input.TotalAmount, incomeBo.TotalAmount);
            cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, incomeBo.PaymentMode == null ? 0 : incomeBo.PaymentMode.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.BankID, incomeBo.Bank.Id ?? 0);

            cmd.Parameters.AddWithValue(DBParam.Input.LedgerID, incomeBo.Ledger == null ? 0 : incomeBo.Ledger.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.IsDeleted, incomeBo.IsDeleted);
            if (incomeBo.ItemDetail == null)
            {
                incomeBo.ItemDetail = new List<ItemDetailsBO>();
            }

            cmd.Parameters.AddWithValue("@Payment", Utility.ToDataTable(incomeBo.ItemDetail, "Payment"));
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            con.Open();
            string usermessage = cmd.ExecuteScalar().ToString();

            if (usermessage == Notification.Inserted)
            {
                string bank = "Cash";
                MasterDataRepository masterDataRepository = new MasterDataRepository();

                if (incomeBo.Bank.Id != null)
                {
                    incomeBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
                    bank = incomeBo.BankList.Find(a => a.Value == incomeBo.Bank.Id.ToString()).Text;
                }

                incomeBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger");

                CommonRepository commonRepository = new CommonRepository();
                incomeBo.PaymentModeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
                incomeBo.PaymentModeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
                PopulateMailBodyPaymentRecipt(incomeBo.PartyName, incomeBo.TotalAmount, bank, incomeBo.ReceivedDate, incomeBo.Description,
                    incomeBo.PaymentModeList.Find(a => a.Value == incomeBo.PaymentMode.Id.ToString()).Text,
                    incomeBo.LedgerList.Find(a => a.Value == incomeBo.Ledger.Id.ToString()).Text, "OfficeBAU Receipt -",
                    "A new Receipt has been booked in OfficeBAU today. The details are", "Receipt"
                );
            }

            return usermessage;
        }

        #region Manage Payment

        public string ManagePayment(IncomeBo incomeBo)
        {
            string userMessage = string.Empty;

            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);

            switch (incomeBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    userMessage = CreatePayment(incomeBo, con);
                    break;

                case Operations.Update:
                    userMessage = UpdatePayment(incomeBo, con);
                    break;

                case Operations.Delete:
                    userMessage = DeletePayment(incomeBo, con);
                    break;
            }

            return userMessage;
        }

        private string CreatePayment(IncomeBo incomeBo, SqlConnection con)
        {
            SqlCommand cmd;
            cmd = new SqlCommand("Createpayment_New") { CommandType = CommandType.StoredProcedure, Connection = con };
            FillPaymentParameters(incomeBo, cmd);

            con.Open();
            string usermessage = cmd.ExecuteScalar().ToString();
            if (usermessage == Notification.Inserted)
            {
                string bank = "Cash";
                MasterDataRepository masterDataRepository = new MasterDataRepository();

                if (incomeBo.Bank.Id != null)
                {
                    incomeBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
                    bank = incomeBo.BankList.Find(a => a.Value == incomeBo.Bank.Id.ToString()).Text;
                }

                incomeBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger");

                CommonRepository commonRepository = new CommonRepository();
                incomeBo.PaymentModeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
                PopulateMailBodyPaymentRecipt(incomeBo.PartyName, incomeBo.TotalAmount, bank, incomeBo.ReceivedDate, incomeBo.Description,
                    incomeBo.PaymentModeList.Find(a => a.Value == incomeBo.PaymentMode.Id.ToString()).Text,
                    incomeBo.LedgerList.Find(a => a.Value == incomeBo.Ledger.Id.ToString()).Text, "OfficeBAU Payment - ",
                    "A new Payment has been booked in OfficeBAU today. The details are", "Payment"
                );
            }

            return usermessage;
        }

        private string UpdatePayment(IncomeBo incomeBo, SqlConnection con)
        {
            SqlCommand cmd;
            cmd = new SqlCommand("Update_payment") { CommandType = CommandType.StoredProcedure, Connection = con };
            FillPaymentParameters(incomeBo, cmd);
            cmd.Parameters.AddWithValue(DBParam.Input.ID, incomeBo.Id);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string DeletePayment(IncomeBo incomeBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand("Delete_payment")
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, incomeBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.TotalAmount, incomeBo.TotalAmount);
            cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, incomeBo.PaymentMode == null ? 0 : incomeBo.PaymentMode.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private void FillPaymentParameters(IncomeBo incomeBo, SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue(DBParam.Input.Date, incomeBo.ReceivedDate);
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(incomeBo.Description));
            cmd.Parameters.AddWithValue(DBParam.Input.PartyName, incomeBo.PartyName);
            cmd.Parameters.AddWithValue(DBParam.Input.TotalAmount, incomeBo.TotalAmount);
            cmd.Parameters.AddWithValue(DBParam.Input.PaymentModeID, incomeBo.PaymentMode == null ? 0 : incomeBo.PaymentMode.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.BankID, incomeBo.Bank.Id ?? 0);
            cmd.Parameters.AddWithValue(DBParam.Input.LedgerID, incomeBo.Ledger == null ? 0 : incomeBo.Ledger.Id);
            if (incomeBo.ItemDetail == null)
            {
                incomeBo.ItemDetail = new List<ItemDetailsBO>();
            }

            cmd.Parameters.AddWithValue("@Payment", Utility.ToDataTable(incomeBo.ItemDetail, "Payment"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
        }

        #endregion Manage Payment

        public IncomeBo GetAccountPayments(int id)
        {
            IncomeBo incomeBo = new IncomeBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetPayments");

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBo = new IncomeBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ReceivedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                            PartyName = Utility.CheckDbNull<string>(reader[DBParam.Output.PartyName]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Reference = Utility.CheckDbNull<string>(reader[DBParam.Output.Reference]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            Ledger = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID])
                            },
                            PaymentMode = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.PaymentMode])
                            },
                            Bank = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.BankID])
                            },
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount])
                        };
                    }
                }
            }

            if (incomeBo.Id != 0)
            {
                incomeBo.ItemDetail = GetPaymentDetails(null, incomeBo.Id).ItemDetail;
            }

            if (incomeBo.ItemDetail != null)
            {
                if (!incomeBo.ItemDetail.Any())
                {
                    incomeBo.ItemDetail = null;
                }
            }

            return incomeBo;
        }

        #endregion AR

        #region Search ReceiptPayments

        public IncomeBo GetAccountReceipts(int id)
        {
            IncomeBo incomeBo = new IncomeBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetReceipts");

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBo = new IncomeBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ReceivedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                            PartyName = Utility.CheckDbNull<string>(reader[DBParam.Output.PartyName]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Reference = Utility.CheckDbNull<string>(reader[DBParam.Output.Reference]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            Ledger = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID])
                            },
                            PaymentMode = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.PaymentMode])
                            },
                            Bank = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.BankID])
                            },
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount])
                        };
                    }
                }
            }

            if (incomeBo.Id != 0)
            {
                incomeBo.ItemDetail = GetReceiptsDetails(null, incomeBo.Id).ItemDetail;
            }

            if (incomeBo.ItemDetail != null)
            {
                if (!incomeBo.ItemDetail.Any())
                {
                    incomeBo.ItemDetail = null;
                }
            }

            return incomeBo;
        }

        public List<IncomeBo> SearchReceiptPayments(SearchParamsBo searchParamsBo)
        {
            List<IncomeBo> incomeBoList = new List<IncomeBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Searchpayments");

                Database.AddInParameter(dbCommand, DBParam.Input.PartyName, DbType.String, searchParamsBo.PartyName);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount);
                Database.AddInParameter(dbCommand, DBParam.Input.LedgerName, DbType.String, searchParamsBo.LedgerName);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBoList.Add(
                            new IncomeBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                InvoiceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                                PartyName = Utility.CheckDbNull<string>(reader[DBParam.Output.PartyName]),
                                LedgerName = Utility.CheckDbNull<string>(reader[DBParam.Output.LedgerName]),
                                Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                ReceivedAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Bank = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName])
                                },
                                PaymentMode = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentMode])
                                }
                            });
                    }
                }
            }

            return incomeBoList;
        }

        public IncomeBo GetPaymentDetails(int? ledgerId, int? id)
        {
            List<ItemDetailsBO> incomeBoList = new List<ItemDetailsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchJournalPayment");
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.LedgerID, DbType.Int32, ledgerId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBoList.Add(
                            new ItemDetailsBO
                            {
                                ID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                InvoiceID = Utility.CheckDbNull<int>(reader["RefID"]),
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaidAmount]),
                                ProductName = Utility.CheckDbNull<string>(reader[DBParam.Output.JournalNo]),
                                Rate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                SGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.updateAmount])
                            });
                    }
                }
            }

            IncomeBo incomeBo = new IncomeBo { ItemDetail = incomeBoList };
            return incomeBo;
        }

        #endregion Search ReceiptPayments

        #region Email

        public string PopulateMailBodyPaymentRecipt(string vendor, decimal amount, string account, DateTime? date, string desc, string payMode, string ledger, string sub, string message, string key)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/PaymentandsRecipts.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{content}", message);
            body = body.Replace("{PartyName}", vendor);
            body = body.Replace("{Ledger}", ledger);
            body = body.Replace("{Amount}", "INR" + " " + Utility.PrecisionForDecimal(amount, 2));
            body = body.Replace("{desc}", desc);
            body = body.Replace("{Date}", Convert.ToDateTime(date).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{account}", account);
            body = body.Replace("{Mode}", payMode);
            body = body.Replace("{Createdby}", Utility.GetSession("FirstName") + " - On " + DateTime.Now.ToString("dd-MMMM-yyyy h:mm tt") + " IST");
            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();
            ApplicationConfigBo applicationConfigBo = applicationConfigurationRepository.GetEmailConfig(key);
            CommonRepository commonRepository = new CommonRepository();
            commonRepository.ManageEmail_InsertintoProcessorTable(applicationConfigBo.To, applicationConfigBo.Cc, applicationConfigBo.Description, body,
                sub + " " + ledger + " - INR " + Utility.PrecisionForDecimal(amount, 2), key, null);
            return body;
        }

        public string PopulateMailBodyInvoice(string vendor, decimal amount, string account, DateTime? date, string desc, string payMode, string product)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/Invoice.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{PartyName}", vendor);
            body = body.Replace("{Product}", product);
            body = body.Replace("{Amount}", "INR" + " " + Utility.PrecisionForDecimal(amount, 2));
            body = body.Replace("{desc}", desc);
            body = body.Replace("{Date}", Convert.ToDateTime(date).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{account}", account);
            body = body.Replace("{Mode}", payMode);
            body = body.Replace("{Createdby}", Utility.GetSession("FirstName") + " - On" + DateTime.Now.ToString("dd-MMMM-yyyy h:mm tt") + " IST");

            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();
            ApplicationConfigBo applicationConfigBo = applicationConfigurationRepository.GetEmailConfig("Income");
            CommonRepository commonRepository = new CommonRepository();
            commonRepository.ManageEmail_InsertintoProcessorTable(applicationConfigBo.To, applicationConfigBo.Cc, applicationConfigBo.Description, body,
                "OfficeBAU Income - " + product + " - INR " + Utility.PrecisionForDecimal(amount, 2), "Income", null);
            return body;
        }

        public string PopulateMailBody(string customer)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/Invoice.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{customer}", customer);
            return body;
        }

        #endregion Email
    }
}