﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Services
{
    public class TravelRepository : BaseRepository
    {
        #region Request

        public List<TravelBO> SearchTravelRequest(TravelBO travelBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<TravelBO> requestedList = (from l in context.tbl_EmployeeTravelRequest
                                                join t in context.tbl_CodeMaster on l.TravelTypeID equals t.ID
                                                join s in context.tbl_Status on l.StatusID equals s.ID
                                                where l.EmployeeID == travelBo.EmployeeId && !l.IsDeleted
                                                                                          && (travelBo.StatusId == 0 || l.StatusID == travelBo.StatusId)
                                                                                          && (travelBo.TravelTypeId == 0 || l.TravelTypeID == travelBo.TravelTypeId)
                                                                                          && l.DomainID == domainId
                                                orderby l.TravelDate descending
                                                select new TravelBO
                                                {
                                                    Id = l.ID,
                                                    TravelReqNo = l.TravelReqNo,
                                                    TravelDate = l.TravelDate,
                                                    ReturnDate = l.ReturnDate,
                                                    TravelFrom = l.TravelFrom,
                                                    TravelTo = l.TravelTo,
                                                    TravelType = t.Code,
                                                    Status = s.Code,
                                                    NoOfSubordinates = context.tbl_SubordinatesDetails.Count(x => x.TravelRequestID == l.ID)
                                                }).ToList();
                return requestedList;
            }
        }

        public TravelBO GetTravelRequest(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                TravelBO request = (from l in context.tbl_EmployeeTravelRequest
                                    join a in context.tbl_EmployeeTravelApproval on l.ID equals a.TravelRequestID
                                    join s in context.tbl_Status on l.StatusID equals s.ID
                                    join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                    join em in context.tbl_EmployeeMaster on l.ModifiedBy equals em.ID
                                    join an in context.tbl_EmployeeMaster on a.ApproverID equals an.ID
                                    join f in context.tbl_FileUpload on l.AttachmentFileID equals f.Id into ff
                                    from file in ff.DefaultIfEmpty()
                                    where l.ID == id && !l.IsDeleted
                                    select new TravelBO
                                    {
                                        Id = l.ID,
                                        TravelReqNo = l.TravelReqNo,
                                        EmployeeId = l.EmployeeID,
                                        EmployeeName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                        TravelDate = l.TravelDate,
                                        TravelFrom = l.TravelFrom,
                                        TravelTo = l.TravelTo,
                                        TravelTypeId = l.TravelTypeID,
                                        ModeOfTransportId = l.ModeOfTransportID,
                                        PurposeOfTravel = l.PurposeOfTravel,
                                        TotalAmount = l.TotalAmount,
                                        Notes = l.Notes,
                                        ReturnDate = l.ReturnDate,
                                        CurrencyId = l.CurrencyID,
                                        ApproverId = a.ApproverID,
                                        Approver = (an.EmpCodePattern ?? "") + (an.Code ?? "") + " - " + an.FullName,
                                        StatusId = l.StatusID,
                                        Status = s.Code,
                                        AttachmentFileId = file.Id,
                                        AttachmentFileName = file.OriginalFileName,
                                        ModifiedByName = em.FullName,
                                        ModifiedOn = l.ModifiedOn,
                                        DomainId = l.DomainID,
                                        NoOfSubordinates = context.tbl_SubordinatesDetails.Count(x => x.TravelRequestID == l.ID),
                                        IsBillableToCustomer = l.IsBillableToCustomer ?? false,
                                        ApproverRemarks = a.ApproverRemarks,
                                        ApprovedDate = a.ModifiedOn
                                    }).FirstOrDefault();

                if (request != null)
                {
                    request.SubordinatesList = GetSubordinatesList(request.Id, request.DomainId);
                }

                return request;
            }
        }

        public string GetTravelRequestNo()
        {
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();

                int request = Convert.ToInt32(context.tbl_EmployeeTravelRequest.Count(r => r.DomainID == domainId)) + 1;

                return "TRN" + request.ToString(); //Utility.GetSession("CompanyName")
            }
        }

        public string ValidateTravelRequestNo(int id, string travelReqNo)
        {
            string message;
            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();

                int request = Convert.ToInt32(context.tbl_EmployeeTravelRequest.Count(r => r.DomainID == domainId && r.ID != id && r.TravelReqNo.Trim().ToUpper() == travelReqNo.Trim().ToUpper()));
                message = request == 0 ? "valid" : "invalid";
            }
            return message;
        }

        public List<Subordinates> GetSubordinatesList(int travelRequestId, int domainId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<Subordinates> result = (from s in context.tbl_SubordinatesDetails
                                             where s.TravelRequestID == travelRequestId && s.DomainID == domainId
                                             orderby s.Name
                                             select new Subordinates
                                             {
                                                 Name = s.Name
                                             }).ToList();

                return result;
            }
        }

        public string CreateTravelRequest(TravelBO travelBo)
        {
            string outputMessage;
            using (FSMEntities context = new FSMEntities())
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    tbl_EmployeeTravelRequest travelDetails = new tbl_EmployeeTravelRequest
                    {
                        EmployeeID = travelBo.EmployeeId,
                        TravelTypeID = travelBo.TravelTypeId,
                        ModeOfTransportID = travelBo.ModeOfTransportId,
                        PurposeOfTravel = travelBo.PurposeOfTravel,
                        TravelFrom = travelBo.TravelFrom,
                        TravelTo = travelBo.TravelTo,
                        TravelDate = travelBo.TravelDate,
                        ReturnDate = travelBo.ReturnDate,
                        CurrencyID = travelBo.CurrencyId,
                        TotalAmount = travelBo.TotalAmount,
                        Notes = travelBo.Notes,
                        AttachmentFileID = travelBo.AttachmentFileId,
                        StatusID = travelBo.StatusId,
                        IsDeleted = false
                    };
                    travelDetails.AttachmentFileID = travelBo.AttachmentFileId;
                    travelDetails.IsBillableToCustomer = travelBo.IsBillableToCustomer;
                    travelDetails.TravelReqNo = travelBo.TravelReqNo;

                    travelDetails.CreatedBy = travelBo.CreatedBy;
                    travelDetails.CreatedOn = DateTime.Now;
                    travelDetails.ModifiedBy = travelBo.ModifiedBy;
                    travelDetails.ModifiedOn = DateTime.Now;
                    travelDetails.DomainID = travelBo.DomainId;
                    travelDetails.HistoryID = Guid.NewGuid();

                    context.tbl_EmployeeTravelRequest.Add(travelDetails);
                    context.SaveChanges();

                    travelBo.Id = Convert.ToInt32(context.tbl_EmployeeTravelRequest.OrderByDescending(b => b.ID).Select(b => b.ID).FirstOrDefault());

                    tbl_EmployeeTravelApproval travelApproval = new tbl_EmployeeTravelApproval
                    {
                        TravelRequestID = travelBo.Id,
                        ApproverID = travelBo.ApproverId,
                        ApproverStatusID = travelBo.StatusId,
                        CreatedBy = travelBo.CreatedBy,
                        CreatedOn = DateTime.Now,
                        ModifiedBy = travelBo.ModifiedBy,
                        ModifiedOn = DateTime.Now,
                        DomainID = travelBo.DomainId,
                        HistoryID = Guid.NewGuid()
                    };
                    context.tbl_EmployeeTravelApproval.Add(travelApproval);
                    context.SaveChanges();

                    transaction.Complete();

                    outputMessage = Notification.Inserted;
                }
            }

            // Insert Subordinates
            ManageSubordinates(travelBo.Id, travelBo.SubordinatesList);

            return outputMessage;
        }

        private void ManageSubordinates(int travelRequestId, List<Subordinates> subordinatesList)
        {
            if (subordinatesList != null && subordinatesList.Count > 0)
            {
                string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                SqlConnection con = new SqlConnection(consString);

                SqlCommand cmd = new SqlCommand(Constants.MANAGE_SUBORDINATES)
                {
                    CommandType = CommandType.StoredProcedure,
                    Connection = con
                };
                cmd.Parameters.AddWithValue(DBParam.Input.ID, travelRequestId);
                cmd.Parameters.AddWithValue(DBParam.Input.SubordinatesList, Utility.ToDataTable(subordinatesList, "SubordinatesList"));
                cmd.Parameters.AddWithValue(DBParam.Input.UserID, Utility.UserId());
                cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public string UpdateTravelRequest(TravelBO travelBo)
        {
            string outputMessage;

            using (FSMEntities context = new FSMEntities())
            {
                using (TransactionScope transaction = new TransactionScope())
                {

                    tbl_EmployeeTravelRequest travelDetails = context.tbl_EmployeeTravelRequest.FirstOrDefault(b => b.ID == travelBo.Id);
                    if (travelDetails != null)
                    {
                        travelDetails.EmployeeID = travelBo.EmployeeId;
                        travelDetails.TravelTypeID = travelBo.TravelTypeId;
                        travelDetails.ModeOfTransportID = travelBo.ModeOfTransportId;
                        travelDetails.PurposeOfTravel = travelBo.PurposeOfTravel;
                        travelDetails.TravelFrom = travelBo.TravelFrom;
                        travelDetails.TravelTo = travelBo.TravelTo;
                        travelDetails.TravelDate = travelBo.TravelDate;
                        travelDetails.ReturnDate = travelBo.ReturnDate;
                        travelDetails.CurrencyID = travelBo.CurrencyId;
                        travelDetails.TotalAmount = travelBo.TotalAmount;
                        travelDetails.Notes = travelBo.Notes;
                        travelDetails.AttachmentFileID = travelBo.AttachmentFileId;
                        travelDetails.StatusID = travelBo.StatusId;
                        travelDetails.IsDeleted = false;
                        travelDetails.IsBillableToCustomer = travelBo.IsBillableToCustomer;
                        travelDetails.TravelReqNo = travelBo.TravelReqNo;
                        travelDetails.ModifiedBy = travelBo.ModifiedBy;
                        travelDetails.ModifiedOn = DateTime.Now;
                        context.Entry(travelDetails).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    tbl_EmployeeTravelApproval travelApproval = context.tbl_EmployeeTravelApproval.FirstOrDefault(b => b.TravelRequestID == travelBo.Id);
                    if (travelApproval != null)
                    {
                        travelApproval.ApproverID = travelBo.ApproverId;
                        travelApproval.ApproverStatusID = travelBo.StatusId;
                        travelApproval.ModifiedBy = travelBo.ModifiedBy;
                        travelApproval.ModifiedOn = DateTime.Now;
                        context.Entry(travelApproval).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    transaction.Complete();

                    outputMessage = Notification.Updated;
                }
            }

            // Insert Subordinates
            ManageSubordinates(travelBo.Id, travelBo.SubordinatesList);

            return outputMessage;
        }

        public string DeleteTravelRequest(TravelBO travelBo)
        {
            string outputMessage;

            using (FSMEntities context = new FSMEntities())
            {
                int check = context.tbl_EmployeeTravelRequest.Count(s => s.ID == travelBo.Id && s.StatusID == travelBo.StatusId);

                if (check == 1)
                {
                    tbl_EmployeeTravelRequest travelDetails = context.tbl_EmployeeTravelRequest.FirstOrDefault(b => b.ID == travelBo.Id);
                    if (travelDetails != null)
                    {
                        travelDetails.IsDeleted = true;
                        travelDetails.ModifiedBy = travelBo.ModifiedBy;
                        travelDetails.ModifiedOn = DateTime.Now;
                        context.Entry(travelDetails).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    outputMessage = Notification.Deleted;
                }
                else
                {
                    outputMessage =Notification.Record_Referred;
                }
            }

            return outputMessage;
        }

        #endregion Request

        #region Approval

        public TravelBO GetTravelRequestApproval(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                TravelBO request = (from l in context.tbl_EmployeeTravelRequest
                                    join a in context.tbl_EmployeeTravelApproval on l.ID equals a.TravelRequestID
                                    join s in context.tbl_Status on l.StatusID equals s.ID
                                    join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                    join em in context.tbl_EmployeeMaster on l.ModifiedBy equals em.ID
                                    join an in context.tbl_EmployeeMaster on a.ApproverID equals an.ID
                                    join f in context.tbl_FileUpload on l.AttachmentFileID equals f.Id into ff
                                    from file in ff.DefaultIfEmpty()
                                    where l.ID == id && !l.IsDeleted
                                    select new TravelBO
                                    {
                                        Id = l.ID,
                                        TravelReqNo = l.TravelReqNo,
                                        EmployeeId = l.EmployeeID,
                                        EmployeeName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                        TravelDate = l.TravelDate,
                                        TravelFrom = l.TravelFrom,
                                        TravelTo = l.TravelTo,
                                        TravelTypeId = l.TravelTypeID,
                                        ModeOfTransportId = l.ModeOfTransportID,
                                        PurposeOfTravel = l.PurposeOfTravel,
                                        TotalAmount = l.TotalAmount,
                                        Notes = l.Notes,
                                        ReturnDate = l.ReturnDate,
                                        CurrencyId = l.CurrencyID,
                                        ApproverId = a.ApproverID,
                                        Approver = (an.EmpCodePattern ?? "") + (an.Code ?? "") + " - " + an.FullName,
                                        StatusId = l.StatusID,
                                        Status = s.Code,
                                        AttachmentFileId = file.Id,
                                        AttachmentFileName = file.OriginalFileName,
                                        ModifiedByName = an.FullName,
                                        ModifiedOn = a.ModifiedOn,
                                        DomainId = l.DomainID,
                                        NoOfSubordinates = context.tbl_SubordinatesDetails.Count(x => x.TravelRequestID == l.ID),
                                        IsBillableToCustomer = l.IsBillableToCustomer ?? false,
                                        ApproverRemarks = a.ApproverRemarks,
                                        ApprovedDate = a.ModifiedOn
                                    }).FirstOrDefault();

                if (request != null)
                {
                    request.SubordinatesList = GetSubordinatesList(request.Id, request.DomainId);
                }

                return request;
            }
        }

        public List<TravelBO> SearchTravelRequestApproval(TravelBO travelBo)
        {
            int domainId = Utility.DomainId();
            int approverId = Utility.UserId();

            using (FSMEntities context = new FSMEntities())
            {
                List<TravelBO> requestedList = (from l in context.tbl_EmployeeTravelRequest
                                                join a in context.tbl_EmployeeTravelApproval on l.ID equals a.TravelRequestID
                                                join t in context.tbl_CodeMaster on l.TravelTypeID equals t.ID
                                                join s in context.tbl_Status on l.StatusID equals s.ID
                                                join em in context.tbl_EmployeeMaster on l.EmployeeID equals em.ID
                                                where !l.IsDeleted && a.ApproverID == approverId
                                                                   && (travelBo.StatusId == 0 || a.ApproverStatusID == travelBo.StatusId)
                                                                   && (travelBo.TravelTypeId == 0 || l.TravelTypeID == travelBo.TravelTypeId)
                                                                   && l.DomainID == domainId
                                                orderby l.TravelDate descending
                                                select new TravelBO
                                                {
                                                    Id = l.ID,
                                                    TravelReqNo = l.TravelReqNo,
                                                    TravelDate = l.TravelDate,
                                                    ReturnDate = l.ReturnDate,
                                                    TravelFrom = l.TravelFrom,
                                                    TravelTo = l.TravelTo,
                                                    TravelType = t.Code,
                                                    Status = s.Code,
                                                    EmployeeName = (em.EmpCodePattern ?? "") + (em.Code ?? "") + " - " + em.FullName,
                                                    NoOfSubordinates = context.tbl_SubordinatesDetails.Count(x => x.TravelRequestID == l.ID)
                                                }).ToList();
                foreach (TravelBO item in requestedList)
                {
                    item.NoOfDays = Convert.ToInt32(((item.ReturnDate ?? item.TravelDate).AddDays(1) - item.TravelDate).TotalDays);
                }

                return requestedList;
            }
        }

        public string ApproveTravelRequest(TravelBO travelBo)
        {
            string outputMessage;

            using (FSMEntities context = new FSMEntities())
            {
                using (TransactionScope transaction = new TransactionScope())
                {

                    tbl_EmployeeTravelRequest travelDetails = context.tbl_EmployeeTravelRequest.FirstOrDefault(b => b.ID == travelBo.Id);
                    if (travelDetails != null)
                    {
                        travelDetails.StatusID = travelBo.StatusId;
                        travelDetails.ModifiedBy = travelBo.ModifiedBy;
                        travelDetails.ModifiedOn = DateTime.Now;

                        context.Entry(travelDetails).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    tbl_EmployeeTravelApproval travelApproval = context.tbl_EmployeeTravelApproval.FirstOrDefault(b => b.TravelRequestID == travelBo.Id);
                    if (travelApproval != null)
                    {
                        travelApproval.ApproverStatusID = travelBo.StatusId;
                        travelApproval.ApproverRemarks = travelBo.ApproverRemarks;
                        travelApproval.ModifiedBy = travelBo.ModifiedBy;
                        travelApproval.ModifiedOn = DateTime.Now;

                        context.Entry(travelApproval).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    transaction.Complete();

                    outputMessage = BOConstants.Approved;
                }
            }

            return outputMessage;
        }

        public string RejectTravelRequest(TravelBO travelBo)
        {
            string outputMessage;

            using (FSMEntities context = new FSMEntities())
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    tbl_EmployeeTravelRequest travelDetails = context.tbl_EmployeeTravelRequest.FirstOrDefault(b => b.ID == travelBo.Id);
                    if (travelDetails != null)
                    {
                        travelDetails.StatusID = travelBo.StatusId;
                        travelDetails.ModifiedBy = travelBo.ModifiedBy;
                        travelDetails.ModifiedOn = DateTime.Now;

                        context.Entry(travelDetails).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    tbl_EmployeeTravelApproval travelApproval = context.tbl_EmployeeTravelApproval.FirstOrDefault(b => b.TravelRequestID == travelBo.Id);
                    if (travelApproval != null)
                    {
                        travelApproval.ApproverStatusID = travelBo.StatusId;
                        travelApproval.ApproverRemarks = travelBo.ApproverRemarks;
                        travelApproval.ModifiedBy = travelBo.ModifiedBy;
                        travelApproval.ModifiedOn = DateTime.Now;

                        context.Entry(travelApproval).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    transaction.Complete();

                    outputMessage = BOConstants.Rejected;
                }
            }

            return outputMessage;
        }

        #endregion Approval
    }
}