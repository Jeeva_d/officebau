﻿using cvt.officebau.com.MessageConstants;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class SuperAdminRepository : BaseRepository
    {
        #region Company Mapping

        public List<UserLoginBo> SearchEmployeeList()
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<UserLoginBo> result = (from e in context.tbl_EmployeeMaster
                                            join c in context.tbl_EmployeeCompanyMapping on e.ID equals c.EmployeeID
                                            where !e.IsDeleted
                                            select new UserLoginBo
                                            {
                                                EmployeeId = e.ID,
                                                UserName = e.Code + " - " + e.FullName
                                            }).ToList();
                return result;
            }
        }

        public List<SelectListItem> SearchCompanyList()
        {
            using (FSMEntities context = new FSMEntities())
            {
                return context.tbl_Company.Where(c => c.IsDeleted == false)
                              .OrderBy(c => c.FullName).ToList()
                              .Select(c => new SelectListItem
                              {
                                  Value = c.ID.ToString(),
                                  Text = c.FullName
                              }).ToList();
            }
        }

        public UserLoginBo GetEmployeeCompanyMapping(int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                UserLoginBo userLoginBo = new UserLoginBo();

                if (context.tbl_EmployeeCompanyMapping.Count(c => !c.IsDeleted && c.EmployeeID == employeeId) != 0)
                {
                    userLoginBo = (from c in context.tbl_EmployeeCompanyMapping
                                   join e in context.tbl_EmployeeMaster on c.EmployeeID equals e.ID
                                   where c.EmployeeID == employeeId
                                   select new UserLoginBo
                                   {
                                       Id = c.ID,
                                       EmployeeId = c.EmployeeID,
                                       UserName = e.FullName,
                                       CompanyIdList = c.CompanyIDs
                                   }).FirstOrDefault();
                }
                else
                {
                    userLoginBo.EmployeeId = employeeId;
                    userLoginBo.UserName = context.tbl_EmployeeMaster.Where(e => e.ID == employeeId).Select(e => e.FullName).FirstOrDefault();
                }

                if (employeeId != 0)
                {
                    if (userLoginBo != null)
                    {
                        userLoginBo.CompanyList = SearchCompanyList();
                        userLoginBo.CompanyId = context.tbl_EmployeeMaster.FirstOrDefault(e => e.ID == employeeId).DomainID;
                    }
                }
                else
                {
                    if (userLoginBo != null)
                    {
                        userLoginBo.CompanyList = null;
                    }
                }

                return userLoginBo;
            }
        }

        public string ManageEmployeeCompanyMapping(UserLoginBo manageBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                if (context.tbl_EmployeeCompanyMapping.Count(c => !c.IsDeleted && c.EmployeeID == manageBo.EmployeeId) != 0)
                {
                    tbl_EmployeeCompanyMapping updateMapping = context.tbl_EmployeeCompanyMapping.FirstOrDefault(c => !c.IsDeleted && c.EmployeeID == manageBo.EmployeeId);
                    if (updateMapping != null)
                    {
                        updateMapping.CompanyIDs = manageBo.CompanyIdList;
                        updateMapping.ModifiedBy = manageBo.UserId;
                        updateMapping.ModifiedOn = DateTime.Now;
                        context.Entry(updateMapping).State = EntityState.Modified;
                    }

                    context.SaveChanges();
                }
                else
                {
                    tbl_EmployeeCompanyMapping insertMapping = new tbl_EmployeeCompanyMapping
                    {
                        EmployeeID = manageBo.EmployeeId,
                        CompanyIDs = manageBo.CompanyIdList,
                        CreatedBy = manageBo.UserId,
                        CreatedOn = DateTime.Now,
                        ModifiedBy = manageBo.UserId,
                        ModifiedOn = DateTime.Now,
                        HistoryID = Guid.NewGuid()
                    };
                    context.tbl_EmployeeCompanyMapping.Add(insertMapping);
                    context.SaveChanges();
                }

                return BOConstants.Company_mapped;
            }
        }

        public List<UserLoginBo> SearchAutoCompleteEmployee(string employeeName)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<UserLoginBo> list = (from t in context.tbl_EmployeeMaster
                                          where !t.IsDeleted && t.FullName.Contains(employeeName) && t.HasAccess
                                          orderby t.FullName
                                          select new UserLoginBo
                                          {
                                              Name = (t.Code ?? "") + " - " + (t.FullName ?? ""),
                                              Id = t.ID
                                          }).ToList();
                return list;
            }
        }

        #endregion Company Mapping
    }
}