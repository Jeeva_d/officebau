﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Services
{
    public class LeaveManagementRepository : BaseRepository
    {
        #region Leave Policy

        public List<LeaveManagementBo> SearchLeavePolicy(int domainId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<LeaveManagementBo> policyList = (from l in context.tbl_LeavePolicy
                                                      join t in context.tbl_LeaveTypes on l.LeaveTypeID equals t.ID
                                                      join b in context.tbl_BusinessUnit on l.RegionID equals b.ID
                                                      where !l.IsDeleted
                                                            && !t.IsDeleted
                                                            && l.DomainID == domainId
                                                      orderby l.ModifiedOn descending
                                                      select new LeaveManagementBo
                                                      {
                                                          Id = l.ID,
                                                          LeaveType = t.Name,
                                                          Region = b.Name,
                                                          AvailableLeave = l.NoOfLeaves
                                                      }).ToList();
                return policyList;
            }
        }

        public LeaveManagementBo GetLeavePolicy(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                LeaveManagementBo policyDetails = (from l in context.tbl_LeavePolicy
                                                   where !l.IsDeleted && l.ID == id
                                                   select new LeaveManagementBo
                                                   {
                                                       Id = l.ID,
                                                       LeaveTypeId = l.LeaveTypeID,
                                                       RegionId = l.RegionID,
                                                       AvailableLeave = l.NoOfLeaves,
                                                       Description = l.Description
                                                   }).FirstOrDefault();
                return policyDetails;
            }
        }

        public string ManageLeavePolicy(LeaveManagementBo leaveManagementBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int check = context.tbl_LeavePolicy.Where(p => !p.IsDeleted
                                                               && p.DomainID == leaveManagementBo.DomainId && p.ID != leaveManagementBo.Id
                                                               && p.LeaveTypeID == leaveManagementBo.LeaveTypeId && p.RegionID == leaveManagementBo.RegionId).Select(p => p.ID).Count();

                if (leaveManagementBo.IsDeleted)
                {
                    var checkPolicy = (from a in context.tbl_EmployeeAvailedLeave
                                       from e in context.tbl_EmployeeMaster
                                         .Where(em => em.ID == a.EmployeeID && !(em.IsDeleted))
                                       where a.LeaveTypeID == leaveManagementBo.LeaveTypeId && e.RegionID == leaveManagementBo.RegionId
                                       select new { a.EmployeeID }).ToList();
                    if (checkPolicy.Count == 0)
                    {
                        tbl_LeavePolicy deletePolicy = context.tbl_LeavePolicy.FirstOrDefault(p => !p.IsDeleted && p.ID == leaveManagementBo.Id);
                        if (deletePolicy != null)
                        {
                            deletePolicy.IsDeleted = leaveManagementBo.IsDeleted;
                            deletePolicy.ModifiedBy = Utility.UserId();
                            deletePolicy.ModifiedOn = DateTime.Now;

                            context.Entry(deletePolicy).State = EntityState.Modified;
                        }

                        context.SaveChanges();

                        return Notification.Deleted;
                    }

                    return Notification.Record_Referred;
                }

                if (check == 0)
                {
                    if (leaveManagementBo.Id != 0)
                    {
                        tbl_LeavePolicy updatePolicy = context.tbl_LeavePolicy.FirstOrDefault(p => !p.IsDeleted && p.ID == leaveManagementBo.Id);
                        if (updatePolicy != null)
                        {
                            updatePolicy.RegionID = leaveManagementBo.RegionId;
                            updatePolicy.LeaveTypeID = leaveManagementBo.LeaveTypeId;
                            if (leaveManagementBo.AvailableLeave != null)
                            {
                                updatePolicy.NoOfLeaves = (byte)leaveManagementBo.AvailableLeave;
                            }

                            updatePolicy.Description = leaveManagementBo.Description;
                            updatePolicy.ModifiedBy = Utility.UserId();
                            updatePolicy.ModifiedOn = DateTime.Now;

                            context.Entry(updatePolicy).State = EntityState.Modified;
                        }

                        context.SaveChanges();

                        return Notification.Updated;
                    }

                    byte noOfLeaves = 0;
                    if (leaveManagementBo.AvailableLeave != null)
                    {
                        noOfLeaves = (byte)leaveManagementBo.AvailableLeave;
                    }

                    tbl_LeavePolicy insertPolicy = new tbl_LeavePolicy()
                    {
                        LeaveTypeID = leaveManagementBo.LeaveTypeId,
                        RegionID = leaveManagementBo.RegionId,
                        NoOfLeaves = noOfLeaves,
                        Description = leaveManagementBo.Description,
                        DomainID = leaveManagementBo.DomainId,
                        HistoryID = Guid.NewGuid(),
                        CreatedBy = Utility.UserId(),
                        ModifiedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedOn = DateTime.Now
                    };

                    context.tbl_LeavePolicy.Add(insertPolicy);
                    context.SaveChanges();

                    return Notification.Inserted;
                }

                return Notification.AlreadyExists;
            }
        }

        #endregion Leave Policy

        #region Leave Type

        public List<LeaveManagementBo> SearchLeaveType(int domainId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<LeaveManagementBo> leaveTypeList = (from t in context.tbl_LeaveTypes
                                                         where !t.IsDeleted && t.DomainID == domainId
                                                         orderby t.ModifiedOn descending
                                                         select new LeaveManagementBo
                                                         {
                                                             LeaveTypeId = t.ID,
                                                             LeaveTypeCode = t.Code,
                                                             LeaveTypeName = t.Name,
                                                             Description = t.Remarks
                                                         }).ToList();
                return leaveTypeList;
            }
        }

        public LeaveManagementBo GetLeaveType(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                LeaveManagementBo policyDetails = (from t in context.tbl_LeaveTypes
                                                   where !t.IsDeleted && t.ID == id
                                                   select new LeaveManagementBo
                                                   {
                                                       LeaveTypeId = t.ID,
                                                       LeaveTypeCode = t.Code,
                                                       LeaveTypeName = t.Name,
                                                       Description = t.Remarks
                                                   }).FirstOrDefault();
                return policyDetails;
            }
        }

        public string ManageLeaveType(LeaveManagementBo leaveManagementBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int check = context.tbl_LeaveTypes.Count(p => !p.IsDeleted
                                                              && p.DomainID == leaveManagementBo.DomainId && p.ID != leaveManagementBo.LeaveTypeId
                                                              && p.Name == leaveManagementBo.LeaveTypeName);

                int checkCode = context.tbl_LeaveTypes.Count(p => !p.IsDeleted
                                                                  && p.DomainID == leaveManagementBo.DomainId && p.ID != leaveManagementBo.LeaveTypeId
                                                                  && p.Code == leaveManagementBo.LeaveTypeCode);

                if (leaveManagementBo.IsDeleted)
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.DELETE_LEAVETYPE);

                    Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, leaveManagementBo.LeaveTypeId);
                    Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                    return Database.ExecuteScalar(dbCommand).ToString();
                }

                if (check == 0 && checkCode == 0 && leaveManagementBo.LeaveTypeId != 0)
                {
                    tbl_LeaveTypes updatePolicy = context.tbl_LeaveTypes.FirstOrDefault(p => !p.IsDeleted && p.ID == leaveManagementBo.LeaveTypeId);

                    if (updatePolicy != null)
                    {
                        updatePolicy.Code = leaveManagementBo.LeaveTypeCode;
                        updatePolicy.Name = leaveManagementBo.LeaveTypeName;
                        updatePolicy.Remarks = leaveManagementBo.Description;
                        updatePolicy.ModifiedBy = Utility.UserId();
                        updatePolicy.ModifiedOn = DateTime.Now;

                        context.Entry(updatePolicy).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    return Notification.Updated;
                }

                if (check == 0 && checkCode == 0 && leaveManagementBo.LeaveTypeId == 0)
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CREATE_LEAVETYPE);

                    Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, leaveManagementBo.LeaveTypeId);
                    Database.AddInParameter(dbCommand, DBParam.Input.Code, DbType.String, leaveManagementBo.LeaveTypeCode);
                    Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, leaveManagementBo.LeaveTypeName);
                    Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, leaveManagementBo.Description);
                    Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                    return Database.ExecuteScalar(dbCommand).ToString();
                }

                if (checkCode != 0)
                {
                    return BOConstants.Code_Already_Exists;
                }

                return Notification.AlreadyExists;
            }
        }

        #endregion Leave Type

        #region Leave Request

        public List<LeaveManagementBo> SearchLeaveRequest(LeaveManagementBo leaveManagementBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<LeaveManagementBo> requestedList = (from l in context.tbl_EmployeeLeave
                                                         join e in context.tbl_EmployeeMaster on l.ApproverID equals e.ID
                                                         join t in context.tbl_LeaveTypes on l.LeaveTypeID equals t.ID
                                                         join s in context.tbl_Status on l.StatusID equals s.ID
                                                         let duration = context.tbl_EmployeeLeaveDateMapping.Count(d => !d.IsDeleted && d.LeaveRequestID == l.ID)
                                                         where l.EmployeeID == leaveManagementBo.EmployeeId && !l.IsDeleted
                                                                 && (leaveManagementBo.StatusId == 0 || (l.StatusID == leaveManagementBo.StatusId))
                                                                 && (leaveManagementBo.LeaveTypeId == 0 || l.LeaveTypeID == leaveManagementBo.LeaveTypeId)
                                                                 && l.DomainID == domainId
                                                         orderby l.RequestedDate descending
                                                         select new LeaveManagementBo
                                                         {
                                                             Id = l.ID,
                                                             FromDate = (DateTime?)l.FromDate,
                                                             ToDate = (DateTime?)l.ToDate,
                                                             Reason = l.Reason,
                                                             RequesterRemarks = l.RequesterRemarks,
                                                             ApproverRemarks = l.ApproverRemarks,
                                                             Approver = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                                             LeaveType = t.Name,
                                                             Status = new  cvt.officebau.com.ViewModels.Entity()
                                                             {
                                                                 Name = s.Code
                                                             },
                                                             RequestedDate = l.RequestedDate,
                                                             Duration = l.Duration,
                                                             NoOfDays = duration,
                                                             HrStatus = context.tbl_Status.FirstOrDefault(hr => hr.ID == l.HRStatusID && hr.Type.ToUpper().Contains("LEAVE")).Code,
                                                             ApproverStatus = context.tbl_Status.FirstOrDefault(app => app.ID == l.ApproverStatusID && app.Type.ToUpper().Contains("LEAVE")).Code,
                                                             IsHalfDay = l.IsHalfDay ?? false
                                                         }).ToList();
                return requestedList;
            }
        }

        public LeaveManagementBo GetLeaveRequest(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                LeaveManagementBo leaveManagementBo = (from l in context.tbl_EmployeeLeave
                                                       join s in context.tbl_Status on l.StatusID equals s.ID
                                                       join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                                       join a in context.tbl_EmployeeMaster on l.ApproverID equals a.ID
                                                       join em in context.tbl_EmployeeMaster on l.ModifiedBy equals em.ID
                                                       join bu in context.tbl_BusinessUnit on em.BaseLocationID equals bu.ID into business
                                                       from bUnit in business.DefaultIfEmpty()
                                                       join h in context.tbl_EmployeeMaster on l.HRApproverID equals h.ID into hra
                                                       from hr in hra.DefaultIfEmpty()
                                                       let duration = context.tbl_EmployeeLeaveDateMapping.Count(d => !d.IsDeleted && d.LeaveRequestID == l.ID)
                                                       where l.ID == id && !l.IsDeleted
                                                       select new LeaveManagementBo
                                                       {
                                                           Id = l.ID,
                                                           EmployeeId = l.EmployeeID,
                                                           EmployeeName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                                           FromDate = l.FromDate,
                                                           ToDate = l.ToDate,
                                                           PurposeId = l.PurposeID,
                                                           RequestedDate = l.RequestedDate,
                                                           RequesterRemarks = l.RequesterRemarks,
                                                           LeaveTypeId = l.LeaveTypeID,
                                                           Reason = l.Reason,
                                                           ApproverId = l.ApproverID,
                                                           Approver = (a.EmpCodePattern ?? "") + (a.Code ?? "") + " - " + a.FullName,
                                                           ApproverStatus = context.tbl_Status.FirstOrDefault(app => app.ID == l.ApproverStatusID && app.Type.ToUpper().Contains("LEAVE")).Code,
                                                           //LeaveTypeID = l.LeaveTypeID,
                                                           StatusCode = s.Code,
                                                           Duration = l.Duration ?? (byte?)duration,
                                                           ApproverRemarks = l.ApproverRemarks,
                                                           ApprovedDate = l.ApprovedDate,
                                                           AlternativeContactNo = l.AlternativeContactNo,
                                                           HrRemarks = l.HRRemarks,
                                                           HrName = (hr.EmpCodePattern ?? "") + (hr.Code ?? "") + " - " + hr.FullName,
                                                           HrStatus = context.tbl_Status.FirstOrDefault(hrs => hrs.ID == l.HRStatusID && hrs.Type.ToUpper().Contains("LEAVE")).Code,
                                                           IsHalfDay = l.IsHalfDay ?? false,
                                                           HrApprovedDate = l.HRApprovedDate,
                                                           Doj = e.DOJ,
                                                           ModifiedByName = em.FullName,
                                                           ModifiedOn = l.ModifiedOn,
                                                           BaseLocation = bUnit.Name,
                                                           ReplacementEmployeeID = l.ReplacementEmployeeID,
                                                           IsAssetClearanceRequired = l.IsAssetClearanceRequired ?? false
                                                       }).FirstOrDefault();
                return leaveManagementBo;
            }
        }

        public string ManageLeaveRequest(LeaveManagementBo leaveManagementBo)
        {
            string outputMessage;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_LEAVEREQUEST);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, leaveManagementBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, leaveManagementBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.LeaveTypeID, DbType.Int32, leaveManagementBo.LeaveTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.PurposeID, DbType.Int32, leaveManagementBo.PurposeId);
                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.Date, leaveManagementBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.Date, leaveManagementBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Duration, DbType.Int32, leaveManagementBo.Duration);
                Database.AddInParameter(dbCommand, DBParam.Input.Reason, DbType.String, leaveManagementBo.Reason);
                Database.AddInParameter(dbCommand, DBParam.Input.RequesterRemarks, DbType.String, leaveManagementBo.RequesterRemarks);
                Database.AddInParameter(dbCommand, DBParam.Input.AlternativeContactNo, DbType.String, leaveManagementBo.AlternativeContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, leaveManagementBo.ApproverId);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, leaveManagementBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, leaveManagementBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.IsHalfDay, DbType.Boolean, leaveManagementBo.IsHalfDay);
                Database.AddInParameter(dbCommand, DBParam.Input.ReplacementEmployeeID, DbType.Int32, leaveManagementBo.ReplacementEmployeeID);
                outputMessage = Database.ExecuteScalar(dbCommand).ToString();
            }
            return outputMessage;
        }

        public string UpdateLeaveRequest(LeaveManagementBo leaveManagementBo)
        {
            string outputMessage;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.UPDATE_LEAVEREQUEST);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, leaveManagementBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, leaveManagementBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.LeaveTypeID, DbType.Int32, leaveManagementBo.LeaveTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.PurposeID, DbType.Int32, leaveManagementBo.PurposeId);
                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.Date, leaveManagementBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.Date, leaveManagementBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Duration, DbType.Int32, leaveManagementBo.Duration);
                Database.AddInParameter(dbCommand, DBParam.Input.Reason, DbType.String, leaveManagementBo.Reason);
                Database.AddInParameter(dbCommand, DBParam.Input.RequesterRemarks, DbType.String, leaveManagementBo.RequesterRemarks);
                Database.AddInParameter(dbCommand, DBParam.Input.AlternativeContactNo, DbType.String, leaveManagementBo.AlternativeContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, leaveManagementBo.ApproverId);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, leaveManagementBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, leaveManagementBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.IsHalfDay, DbType.Boolean, leaveManagementBo.IsHalfDay);
                Database.AddInParameter(dbCommand, DBParam.Input.ReplacementEmployeeID, DbType.Int32, leaveManagementBo.ReplacementEmployeeID);
                //Database.AddInParameter(dbCommand, "@Resubmit", DbType.String, leaveManagementBo.UserMessage);
                outputMessage = Database.ExecuteScalar(dbCommand).ToString();
            }
            return outputMessage;
        }

        public List<LeaveManagementBo> SearchRequestedLeaves(DateTime fromDate, DateTime toDate, int employeeId, string type = "")
        {
            List<LeaveManagementBo> requestedLeaveList = new List<LeaveManagementBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_REQUESTEDLEAVES);

                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.Date, fromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.Date, toDate);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        var leaveManagementBo = new LeaveManagementBo()
                        {
                            RequestedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.RequestedDate]),
                            RequestedDays = Utility.CheckDbNull<string>(reader[DBParam.Output.RequestedDays]),
                            NoOfDays = Utility.CheckDbNull<int>(reader[DBParam.Output.NoOfDays]),
                            LeaveType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                        };
                        requestedLeaveList.Add(leaveManagementBo);
                    }
                }
            }
            return requestedLeaveList;
        }

        public string CheckHolidays(DateTime date, string operation)
        {
            int check = 0;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CHECK_HOLIDAYS);

                Database.AddInParameter(dbCommand, DBParam.Input.Date, DbType.Date, date);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        check = Utility.CheckDbNull<int>(reader[DBParam.Output.Count]);
                    }
                }
            }
            return operation + '/' + check;
        }

        public string CheckAvailableLeave(int leaveTypeId, int requesterId, int leaveRequestId)
        {
            decimal result = 0, appliedleave = 0;
            int noOfDays = 0;

            using (FSMEntities context = new FSMEntities())
            {
                int domainId = Utility.DomainId();
                int check = context.tbl_EmployeeAvailedLeave.Count(s => s.EmployeeID == requesterId && s.Year == (DateTime.Now.Year) && s.LeaveTypeID == leaveTypeId && !s.IsDeleted && s.DomainID == domainId);

                if (check != 0)
                {
                    result = Convert.ToDecimal(context.tbl_EmployeeAvailedLeave.Where(s => s.EmployeeID == requesterId && s.Year == DateTime.Now.Year && s.LeaveTypeID == leaveTypeId && !s.IsDeleted && s.DomainID == domainId).Select(b => (b.TotalLeave - b.AvailedLeave)).FirstOrDefault());

                    if (leaveRequestId != 0)
                    {
                        List< cvt.officebau.com.ViewModels.Entity> leaveDays = (from l in context.tbl_EmployeeLeaveDateMapping
                                                     join e in context.tbl_EmployeeLeave on l.LeaveRequestID equals e.ID
                                                     where l.LeaveRequestID == leaveRequestId && e.LeaveTypeID == leaveTypeId
                                                            && !e.IsDeleted && e.DomainID == domainId
                                                     select new  cvt.officebau.com.ViewModels.Entity
                                                     {
                                                         Id = e.ID
                                                     }).ToList();
                        appliedleave = Convert.ToDecimal(leaveDays.Count());
                    }
                    if (leaveTypeId == Convert.ToInt32(context.tbl_LeaveTypes.Where(s => !s.IsDeleted && s.DomainID == domainId && s.Name.ToUpper().Contains("PRIVILEGE")).Select(s => s.ID).FirstOrDefault()))
                    {
                        DateTime dateOfjoin = context.tbl_EmployeeMaster.Where(em => em.ID == requesterId && em.DomainID == domainId).Select(em => em.DOJ).FirstOrDefault();
                        noOfDays = (DateTime.Now - dateOfjoin).Days;
                    }
                }
            }
            return (result + appliedleave).ToString(CultureInfo.InvariantCulture) + '/' + noOfDays;
        }

        public List<LeaveManagementBo> GetAvailableLeave(int employeeId)
        {
            List<LeaveManagementBo> requestedLeaveList = new List<LeaveManagementBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_LEAVELIST);

                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, DateTime.Now.Year);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        var leaveManagementBo = new LeaveManagementBo()
                        {
                            LeaveTypeName = Utility.CheckDbNull<string>(reader[DBParam.Output.LeaveType]),
                            TotalLeave = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalLeave]),
                            AvailedLeave = Utility.CheckDbNull<decimal>(reader[DBParam.Output.AvailedLeave]),
                            CarryForwardLeave = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CarryForwardLeave]),
                        };
                        requestedLeaveList.Add(leaveManagementBo);
                    }
                }
            }
            return requestedLeaveList;
        }

        public string GetApprover(int employeeId, string type)
        {
            using (FSMEntities context = new FSMEntities())
            {
                int reportingId = Convert.ToInt32(context.tbl_EmployeeMaster.Where(b => b.ID == employeeId && !b.IsDeleted && !(b.IsActive ?? false)).OrderByDescending(b => b.ID).Select(b => b.ReportingToID).FirstOrDefault());

                if (reportingId != 0)
                {
                    var result = (from e in context.tbl_EmployeeMaster
                                  where e.ID == reportingId && !e.IsDeleted && !(e.IsActive ?? false)
                                  select new
                                  {
                                      Approver = e.FullName,
                                      ApproverID = e.ID,
                                      e.Code,
                                      e.EmpCodePattern
                                  }).FirstOrDefault();
                    if (type.ToUpper() == "NAME")
                    {
                        return ((result == null) ? "" : ((result.EmpCodePattern ?? "") + (result.Code ?? "") + " - " + result.Approver));
                    }
                    else
                    {
                        return (result?.ApproverID.ToString());
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public List<SelectListItem> GetLeaveTypeDropDownBasedOnLeaveTypeId(int leaveTypeId)
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List< cvt.officebau.com.ViewModels.Entity> list = (from t in context.tbl_LeaveTypes
                                        where t.DomainID == domainId && t.ID == leaveTypeId
                                        orderby t.Name
                                        select new  cvt.officebau.com.ViewModels.Entity
                                        {
                                            Name = t.Name,
                                            Id = t.ID,
                                        }).ToList();
                foreach ( cvt.officebau.com.ViewModels.Entity item in list)
                {
                    dependantDropDownList.Add(new SelectListItem
                    {
                        Text = item.Name,
                        Value = item.Id.ToString()
                    });
                }
                return dependantDropDownList;
            }
        }

        #endregion Leave Request

        #region Leave Approve

        public List<LeaveManagementBo> SearchEmployeeLeaveRequest(int employeeId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<LeaveManagementBo> requestedList = (from l in context.tbl_EmployeeLeave
                                                         join e in context.tbl_EmployeeMaster on l.ApproverID equals e.ID
                                                         join t in context.tbl_LeaveTypes on l.LeaveTypeID equals t.ID
                                                         join s in context.tbl_Status on l.StatusID equals s.ID
                                                         let duration = context.tbl_EmployeeLeaveDateMapping.Count(d => !d.IsDeleted && d.LeaveRequestID == l.ID)
                                                         where l.EmployeeID == employeeId && !l.IsDeleted && l.DomainID == domainId
                                                            && l.StatusID == (context.tbl_Status.FirstOrDefault(ts => !ts.IsDeleted && ts.Type == "Leave" && ts.Code == "Approved").ID)
                                                         orderby l.RequestedDate descending
                                                         select new LeaveManagementBo
                                                         {
                                                             Id = l.ID,
                                                             FromDate = (DateTime?)l.FromDate,
                                                             ToDate = (DateTime?)l.ToDate,
                                                             Reason = l.Reason,
                                                             RequesterRemarks = l.RequesterRemarks,
                                                             ApproverRemarks = l.ApproverRemarks,
                                                             Approver = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                                             LeaveType = t.Name,
                                                             Status = new  cvt.officebau.com.ViewModels.Entity()
                                                             {
                                                                 Name = s.Code
                                                             },
                                                             HrStatus = context.tbl_Status.FirstOrDefault(hr => hr.ID == l.HRStatusID && hr.Type.ToUpper().Contains("LEAVE")).Code,
                                                             RequestedDate = l.RequestedDate,
                                                             Duration = l.Duration,
                                                             NoOfDays = duration,
                                                             IsHalfDay = l.IsHalfDay ?? false
                                                         }).ToList();
                return requestedList.Where(l => !(l.LeaveType.ToUpper().Replace(" ", "").Contains("PERMISSION")) && !(l.LeaveType.ToUpper().Replace(" ", "").Contains("ONDUTY"))).ToList();
            }
        }

        public List<LeaveManagementBo> SearchLeaveApproval(LeaveManagementBo leaveManagementBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<LeaveManagementBo> requestedList = (from l in context.tbl_EmployeeLeave
                                                         join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                                         join t in context.tbl_LeaveTypes on l.LeaveTypeID equals t.ID
                                                         join s in context.tbl_Status on l.StatusID equals s.ID
                                                         let duration = context.tbl_EmployeeLeaveDateMapping.Where(d => !d.IsDeleted && d.LeaveRequestID == l.ID).Count()
                                                         where l.ApproverID == leaveManagementBo.UserId && !l.IsDeleted
                                                          && (leaveManagementBo.EmployeeId == 0 || l.EmployeeID == leaveManagementBo.EmployeeId)
                                                         && (leaveManagementBo.StatusId == 0 || l.ApproverStatusID == leaveManagementBo.StatusId)
                                                         && (leaveManagementBo.LeaveTypeId == 0 || l.LeaveTypeID == leaveManagementBo.LeaveTypeId)
                                                         && l.DomainID == leaveManagementBo.DomainId
                                                         orderby l.RequestedDate descending
                                                         select new LeaveManagementBo
                                                         {
                                                             Id = l.ID,
                                                             RequesterRemarks = l.RequesterRemarks,
                                                             Duration = l.Duration,
                                                             RequestedDate = l.RequestedDate,
                                                             NoOfDays = duration,
                                                             Employee = new  cvt.officebau.com.ViewModels.Entity()
                                                             {
                                                                 Name = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName
                                                             },
                                                             LeaveType = t.Name,
                                                             Status = new  cvt.officebau.com.ViewModels.Entity()
                                                             {
                                                                 Name = s.Code
                                                             },
                                                             ApproverStatus = context.tbl_Status.FirstOrDefault(app => app.ID == l.ApproverStatusID && app.Type.ToUpper().Contains("LEAVE")).Code,
                                                             HrStatus = context.tbl_Status.FirstOrDefault(hr => hr.ID == l.HRStatusID && hr.Type.ToUpper().Contains("LEAVE")).Code,
                                                             IsHalfDay = l.IsHalfDay ?? false,
                                                             ApprovedDate = l.ApprovedDate,
                                                             EmployeeId = e.ID,
                                                             FromDate = l.FromDate,
                                                             ToDate = l.ToDate
                                                         }).ToList();
                return requestedList;
            }
        }

        public string ApproveLeaveRequest(LeaveManagementBo leaveManagementBo)
        {
            string outputMessage;

            using (FSMEntities context = new FSMEntities())
            {
                if (context.tbl_EmployeeLeave.Where(l => !l.IsDeleted && l.ID == leaveManagementBo.Id
                                                                      && l.StatusID == context.tbl_Status.FirstOrDefault(s => !s.IsDeleted && s.Type == "Leave" && s.Code == "Pending").ID).Count() != 0)
                {
                    tbl_EmployeeLeave approverAction = context.tbl_EmployeeLeave.FirstOrDefault(l => l.ID == leaveManagementBo.Id && !l.IsDeleted);
                    if (approverAction != null)
                    {
                        approverAction.ApproverRemarks = leaveManagementBo.ApproverRemarks;
                        approverAction.ApprovedDate = DateTime.Now;
                        approverAction.ApproverStatusID = GetStatusId("Leave", leaveManagementBo.StatusCode);
                        approverAction.ReplacementEmployeeID = leaveManagementBo.ReplacementEmployeeID;
                        if (leaveManagementBo.StatusCode.ToUpper().Contains("APPROVE"))
                        {
                            approverAction.HRApproverID = leaveManagementBo.HrApproverId;
                            approverAction.HRStatusID = (leaveManagementBo.StatusCode.ToUpper().Contains("APPROVE")) ? (int?)(GetStatusId("Leave", "Pending")) : null;
                        }
                        else
                        {
                            approverAction.StatusID = GetStatusId("Leave", leaveManagementBo.StatusCode);
                        }

                        approverAction.ModifiedBy = leaveManagementBo.ApproverId;
                        approverAction.ModifiedOn = DateTime.Now;
                        approverAction.DomainID = leaveManagementBo.DomainId;

                        context.Entry(approverAction).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    outputMessage = leaveManagementBo.StatusCode + " Successfully";

                    if (!outputMessage.ToLower().Contains("approved"))
                    {
                        UpdateAvailedLeave(leaveManagementBo);
                    }
                }
                else
                {
                    outputMessage = BOConstants.cannot_Approve_Reject;
                }
            }
            return outputMessage;
        }

        public void UpdateAvailedLeave(LeaveManagementBo leaveManagementBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                var check = context.tbl_EmployeeAvailedLeave.Count(l => l.EmployeeID == leaveManagementBo.EmployeeId && l.LeaveTypeID == leaveManagementBo.LeaveTypeId && l.Year == leaveManagementBo.RequestedDate.Year && l.DomainID == leaveManagementBo.DomainId && !l.IsDeleted);

                if (check != 0)
                {
                    decimal availedLeave = Convert.ToDecimal(context.tbl_EmployeeAvailedLeave.Where(s => s.EmployeeID == leaveManagementBo.EmployeeId && s.LeaveTypeID == leaveManagementBo.LeaveTypeId && s.Year == (DateTime.Now.Year) && s.DomainID == leaveManagementBo.DomainId && !s.IsDeleted).Select(s => s.AvailedLeave).FirstOrDefault());

                    if (leaveManagementBo.IsHalfDay == false)
                    {
                        availedLeave = (availedLeave - Convert.ToDecimal(leaveManagementBo.Duration ?? 0));
                    }
                    else
                    {
                        availedLeave = (availedLeave - Convert.ToDecimal(0.5));
                    }

                    tbl_EmployeeAvailedLeave leave = context.tbl_EmployeeAvailedLeave.FirstOrDefault(l => l.EmployeeID == leaveManagementBo.EmployeeId && l.LeaveTypeID == leaveManagementBo.LeaveTypeId && l.Year == leaveManagementBo.RequestedDate.Year && l.DomainID == leaveManagementBo.DomainId && !l.IsDeleted);

                    if (leave != null)
                    {
                        leave.AvailedLeave = Convert.ToDecimal(availedLeave);
                        leave.ModifiedBy = Utility.UserId();
                        leave.ModifiedOn = DateTime.Now;

                        context.Entry(leave).State = EntityState.Modified;
                    }

                    context.SaveChanges();
                }
            }
        }

        public string UpdateAssetClearance(LeaveManagementBo leaveManagementBo)
        {
            string outputMessage;

            using (FSMEntities context = new FSMEntities())
            {
                tbl_EmployeeLeave approverAction = context.tbl_EmployeeLeave.FirstOrDefault(l => l.ID == leaveManagementBo.Id && !l.IsDeleted);
                if (approverAction != null)
                {
                    approverAction.IsAssetClearanceRequired = leaveManagementBo.IsAssetClearanceRequired;
                    approverAction.ModifiedBy = Utility.UserId();
                    approverAction.ModifiedOn = DateTime.Now;

                    context.Entry(approverAction).State = EntityState.Modified;
                }

                context.SaveChanges();

                outputMessage = Notification.Updated;
            }
            return outputMessage;
        }

        #endregion Leave Approve

        #region HR Approval

        public int GetStatusId(string type, string codeName)
        {
            using (FSMEntities context = new FSMEntities())
            {
                return context.tbl_Status.Where(s => !s.IsDeleted && s.Type == type && s.Code == codeName).Select(s => s.ID).FirstOrDefault();
            }
        }

        public List<LeaveManagementBo> SearchApprovedLeave(LeaveManagementBo leaveManagementBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<LeaveManagementBo> requestedList = (from l in context.tbl_EmployeeLeave
                                                         join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                                         join t in context.tbl_LeaveTypes on l.LeaveTypeID equals t.ID
                                                         join s in context.tbl_Status on l.HRStatusID equals s.ID
                                                         join a in context.tbl_EmployeeMaster on l.ApproverID equals a.ID
                                                         let duration = context.tbl_EmployeeLeaveDateMapping.Count(d => !d.IsDeleted && d.LeaveRequestID == l.ID)
                                                         where !l.IsDeleted
                                                            && (leaveManagementBo.StatusId == 0 || l.HRStatusID == leaveManagementBo.StatusId)
                                                            && (leaveManagementBo.LeaveTypeId == 0 || l.LeaveTypeID == leaveManagementBo.LeaveTypeId)
                                                            && (l.DomainID == leaveManagementBo.DomainId)
                                                             && (l.HRApproverID == leaveManagementBo.UserId)
                                                         orderby l.RequestedDate descending
                                                         select new LeaveManagementBo
                                                         {
                                                             Id = l.ID,
                                                             RequesterRemarks = l.RequesterRemarks,
                                                             Duration = l.Duration,
                                                             RequestedDate = l.RequestedDate,
                                                             NoOfDays = duration,
                                                             EmployeeId = l.EmployeeID,
                                                             Employee = new  cvt.officebau.com.ViewModels.Entity()
                                                             {
                                                                 Name = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName
                                                             },
                                                             LeaveType = t.Name,
                                                             Status = new  cvt.officebau.com.ViewModels.Entity()
                                                             {
                                                                 Name = s.Code
                                                             },
                                                             ApprovedDate = l.ApprovedDate,
                                                             ApproverName = (a.EmpCodePattern ?? "") + (a.Code ?? "") + " - " + a.FullName,
                                                             IsHalfDay = l.IsHalfDay ?? false,
                                                             FromDate = l.FromDate,
                                                             ToDate = l.ToDate
                                                         }).ToList();
                return requestedList;
            }
        }

        public string UpdateHrStatus(LeaveManagementBo leaveManagementBo)
        {
            string outputMessage;

            using (FSMEntities context = new FSMEntities())
            {
                if (context.tbl_EmployeeLeave.Where(l => !l.IsDeleted && l.ID == leaveManagementBo.Id
                                                                      && l.HRStatusID == context.tbl_Status.FirstOrDefault(s => !s.IsDeleted && s.Type == "Leave" && s.Code == "Pending").ID).Count() != 0)
                {
                    tbl_EmployeeLeave approverAction = context.tbl_EmployeeLeave.FirstOrDefault(l => l.ID == leaveManagementBo.Id && !l.IsDeleted);

                    //HR Approval Workflow
                    if (leaveManagementBo.IsHrApprovalRequired == "0")
                    {
                        if (approverAction != null) approverAction.HRApproverID = leaveManagementBo.HrApproverId;
                    }

                    if (approverAction != null)
                    {
                        approverAction.ReplacementEmployeeID = leaveManagementBo.ReplacementEmployeeID;
                        approverAction.HRRemarks = leaveManagementBo.HrRemarks;
                        approverAction.HRStatusID = leaveManagementBo.StatusId;
                        approverAction.StatusID = leaveManagementBo.StatusId;
                        approverAction.HRApprovedDate = DateTime.Now;
                        approverAction.ModifiedBy = leaveManagementBo.UserId;
                        approverAction.ModifiedOn = DateTime.Now;
                        approverAction.DomainID = leaveManagementBo.DomainId;

                        context.Entry(approverAction).State = EntityState.Modified;
                    }

                    context.SaveChanges();

                    outputMessage = leaveManagementBo.StatusCode + " Successfully";

                    if (leaveManagementBo.StatusCode.ToLower().Contains("approved"))
                    {
                        //TimeSpan punchTime = TimeSpan.ParseExact(leaveManagementBo.Duration.ToString(), formats, CultureInfo.InvariantCulture);
                        if (leaveManagementBo.Duration != null)
                        {
                            decimal duration = (decimal)leaveManagementBo.Duration;
                            int leaveTypeId = GetLeaveTypeId("PERMISSION");
                            context.UpdateBiometricPunches(leaveManagementBo.EmployeeId, leaveManagementBo.FromDate, (leaveTypeId == leaveManagementBo.LeaveTypeId ? PunchType.OfficeBAU_Permission : (leaveManagementBo.IsHalfDay ? PunchType.OfficeBAU_HalfDay : PunchType.OfficeBAU_FullDay))
                                , Operations.Insert, duration, null, Utility.UserId(), Utility.DomainId());
                        }

                        // UpdateBiometricPunches(leaveManagementBo.EmployeeId, leaveManagementBo.FromDate, (LeaveTypeID == leaveManagementBo.LeaveTypeId ? PunchType.OfficeBAU_Permission : (leaveManagementBo.IsHalfDay == true ? PunchType.OfficeBAU_HalfDay : PunchType.OfficeBAU_FullDay)), Operations.Insert, duration, null);
                    }

                    if (!leaveManagementBo.StatusCode.ToLower().Contains("approved"))
                    {
                        UpdateAvailedLeave(leaveManagementBo);
                    }
                }
                else
                {
                    outputMessage = BOConstants.cannot_Approve_Reject;
                }
            }
            return outputMessage;
        }

        public int GetLeaveTypeId(string code)
        {
            int domainId = Utility.DomainId();
            using (FSMEntities context = new FSMEntities())
            {
                return (context.tbl_LeaveTypes.Where(lt => !lt.IsDeleted && lt.DomainID == domainId && lt.Name.ToUpper().Contains(code)).FirstOrDefault()) == null ? 0 :
                    (context.tbl_LeaveTypes.Where(lt => !lt.IsDeleted && lt.DomainID == domainId && lt.Name.ToUpper().Contains(code)).Select(it => it.ID).FirstOrDefault());
            }
        }

        #endregion HR Approval

        #region Process LOP

        public string CreateLopDetails(LeaveManagementBo leaveManagementBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PROCESS_LOPDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, leaveManagementBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, leaveManagementBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, leaveManagementBo.BusinessUnitIdList);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                return leaveManagementBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }
        }

        public DataTable GetLopDetails(LeaveManagementBo leaveManagementBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETLOPDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, leaveManagementBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, leaveManagementBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, leaveManagementBo.BusinessUnitIdList);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("LOPDetails");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }
                return dtSalesTargetReport;
            }
        }

        public bool ProcessLopButtonVisibility(LeaveManagementBo leaveManagementBo)
        {
            bool result = true;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PROCESSLOP_VALIDATION);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, leaveManagementBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, leaveManagementBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, leaveManagementBo.BusinessUnitIdList);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                var message = Database.ExecuteScalar(dbCommand).ToString();

                if (message.ToLower() == "success")
                {
                    result = false;
                }
            }
            return result;
        }

        #endregion Process LOP

        #region LOP Details

        public List<LopDetailsBo> SearchLopDetails(LopDetailsBo lopDetailsBo)
        {
            List<LopDetailsBo> list = new List<LopDetailsBo>();

            using (Database.CreateConnection())
            {
                string menuCode = lopDetailsBo.MenuCode;

                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_LOPDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, lopDetailsBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, lopDetailsBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, lopDetailsBo.BusinessUnitIdList);
                Database.AddInParameter(dbCommand, DBParam.Input.HasLopDays, DbType.Boolean, lopDetailsBo.HasLopDays);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        lopDetailsBo = new LopDetailsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeNo]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                            MonthId = Utility.CheckDbNull<byte>(reader[DBParam.Output.MonthID]),
                            YearId = Utility.CheckDbNull<short>(reader[DBParam.Output.YearId]),
                            LopDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.LOPDays]),
                            StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            Location = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            LopType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            ActualWorkDays = Utility.CheckDbNull<int>(reader[DBParam.Output.NoOfDays]),
                            Doj = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]),
                            MenuCode = menuCode
                        };
                        list.Add(lopDetailsBo);
                    }
                }
                return list;
            }
        }

        public string ManageLopDetails(List<LopDetailsBo> lopDetailsBoList)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);

            SqlCommand cmd = new SqlCommand(Constants.MANAGE_LOPDETAILS)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.LOPDetails, ConvertToDataTable(lopDetailsBoList, "LOPDetails"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        public static DataTable ConvertToDataTable<T>(List<T> items, string name)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            foreach (T item in items)
            {
                object[] values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            dataTable.Columns.Remove("entity");
            dataTable.Columns.Remove("UserMessage");
            dataTable.Columns.Remove("CreatedOn");
            dataTable.Columns.Remove("CreatedBy");
            dataTable.Columns.Remove("ModifiedOn");
            dataTable.Columns.Remove("ModifiedBy");
            dataTable.Columns.Remove("UserID");
            dataTable.Columns.Remove("UserName");
            dataTable.Columns.Remove("MenuCode");
            dataTable.Columns.Remove("ModifiedByName");
            dataTable.Columns.Remove("Operation");
            dataTable.Columns.Remove("DomainID");
            dataTable.Columns.Remove("GUID");
            dataTable.Columns.Remove("Designation");
            dataTable.Columns.Remove("Latitude");
            dataTable.Columns.Remove("longitude");
            dataTable.Columns.Remove("Location");
            dataTable.Columns.Remove("LoginID");
            dataTable.Columns.Remove("IsActive");
            dataTable.Columns.Remove("Name");
            dataTable.Columns.Remove("DOJ");
            dataTable.Columns.Remove("SNo");
            dataTable.Columns.Remove("Parameter");
            dataTable.Columns.Remove("Parameter1");
            dataTable.Columns.Remove("Parameter2");
            dataTable.Columns.Remove("Parameter3");
            dataTable.Columns.Remove("Emailhtml");

            if (name == "LOPDetails")
            {
                dataTable.Columns.Remove("EmployeeName");
                dataTable.Columns.Remove("BusinessUnitIDList");
                dataTable.Columns.Remove("MonthList");
                dataTable.Columns.Remove("YearList");
                dataTable.Columns.Remove("IsDeleted");
                dataTable.Columns.Remove("LOPType");
                dataTable.Columns.Remove("HasLopDays");
                dataTable.Columns.Remove("BusinessUnitList");
                dataTable.Columns.Remove("ActualWorkDays");
                dataTable.Columns["ID"].SetOrdinal(0);
                dataTable.Columns["MonthID"].SetOrdinal(1);
                dataTable.Columns["YearID"].SetOrdinal(2);
                dataTable.Columns["EmployeeID"].SetOrdinal(3);
                dataTable.Columns["LOPDays"].SetOrdinal(4);
                dataTable.Columns["StatusCode"].SetOrdinal(5);
            }
            return dataTable;
        }

        #endregion LOP Details

        #region Missed Punches Request

        public List<MissedPunchesBo> SearchMissedPunchesRequest(MissedPunchesBo missedPunchesBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<MissedPunchesBo> requestedList = (from l in context.tbl_EmployeeMissedPunches
                                                       join h in context.tbl_EmployeeMaster on l.EmployeeID equals h.ID
                                                       join e in context.tbl_EmployeeMaster on l.HRApproverID equals e.ID into hr
                                                       from hrApprover in hr.DefaultIfEmpty()
                                                       join status in context.tbl_Status on l.StatusID equals status.ID
                                                       join c in context.tbl_CodeMaster on l.PunchTypeID equals c.ID into code
                                                       from punchType in code.DefaultIfEmpty()
                                                       where l.EmployeeID == missedPunchesBo.EmployeeId && !l.IsDeleted
                                                               && (missedPunchesBo.StatusId == 0 || (l.StatusID == missedPunchesBo.StatusId))
                                                               && l.DomainID == domainId
                                                       orderby l.CreatedOn descending
                                                       select new MissedPunchesBo
                                                       {
                                                           Id = l.ID,
                                                           Remarks = l.Remarks ?? "",
                                                           HrApproverRemarks = l.HRRemarks ?? "",
                                                           PunchTypeId = l.PunchTypeID,
                                                           PunchDate = l.PunchDate,
                                                           PunchTime = l.PunchTime,
                                                           Status = new  cvt.officebau.com.ViewModels.Entity()
                                                           {
                                                               Name = status.Code
                                                           },
                                                           EmployeePunchType = new  cvt.officebau.com.ViewModels.Entity()
                                                           {
                                                               Name = punchType.Code
                                                           },
                                                           HrApproverName = (hrApprover.EmpCodePattern ?? "") + (hrApprover.Code ?? "") + " - " + hrApprover.FullName,
                                                           //HRApproverID = l.HRApproverID,
                                                           // HRApproverName = hr.FullName,
                                                           StatusId = l.StatusID,
                                                           RequestedDate = l.CreatedOn,
                                                           StatusCode = context.tbl_Status.FirstOrDefault(hrs => hrs.ID == l.StatusID && hrs.Type.ToUpper().Contains("LEAVE")).Code,
                                                       }).ToList();
                return requestedList;
            }
        }

        public MissedPunchesBo GetMissedPunchesRequest(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                MissedPunchesBo missedPunchesBo = (from l in context.tbl_EmployeeMissedPunches
                                                   join s in context.tbl_Status on l.StatusID equals s.ID
                                                   join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                                   join h in context.tbl_EmployeeMaster on l.HRApproverID equals h.ID into hra
                                                   from hr in hra.DefaultIfEmpty()
                                                   join em in context.tbl_EmployeeMaster on l.ModifiedBy equals em.ID
                                                   join bu in context.tbl_BusinessUnit on em.BaseLocationID equals bu.ID into business
                                                   from bUnit in business.DefaultIfEmpty()
                                                   where l.ID == id
                                                   select new MissedPunchesBo
                                                   {
                                                       Id = l.ID,
                                                       EmployeeId = l.EmployeeID,
                                                       EmployeeName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                                       Remarks = l.Remarks ?? "",
                                                       HrApproverRemarks = l.HRRemarks ?? "",
                                                       PunchTypeId = l.PunchTypeID,
                                                       PunchDate = l.PunchDate,
                                                       PunchTime = l.PunchTime,
                                                       StatusCode = s.Code,
                                                       Status = new  cvt.officebau.com.ViewModels.Entity()
                                                       {
                                                           Name = s.Code
                                                       },
                                                       StatusId = l.StatusID,
                                                       HrApproverName = (hr.EmpCodePattern ?? "") + (hr.Code ?? "") + " - " + hr.FullName,
                                                       HrApprovedDate = l.HRApprovedOn,
                                                       ModifiedByName = em.FullName,
                                                       ModifiedOn = l.ModifiedOn,
                                                       BaseLocation = bUnit.Name
                                                   }).FirstOrDefault();
                return missedPunchesBo;
            }
        }

        public string ManageMissedPunchesRequest(MissedPunchesBo missedPunchesBo)
        {
            string outputMessage;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_MISSEDPUNCHES);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, missedPunchesBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, missedPunchesBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.PunchTypeID, DbType.Int32, missedPunchesBo.PunchTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.PunchDate, DbType.Date, missedPunchesBo.PunchDate);
                Database.AddInParameter(dbCommand, DBParam.Input.HRApproverID, DbType.Int32, missedPunchesBo.HrApproverId);
                Database.AddInParameter(dbCommand, DBParam.Input.PunchTime, DbType.DateTime, (Convert.ToDateTime(missedPunchesBo.PunchDate).Date + missedPunchesBo.PunchTime));
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, missedPunchesBo.Remarks);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, missedPunchesBo.IsDeleted);
                outputMessage = Database.ExecuteScalar(dbCommand).ToString();
            }
            return outputMessage;
        }

        #endregion Missed Punches Request

        #region Missed Punches Approval

        public List<MissedPunchesBo> SearchPunchesApproval(MissedPunchesBo missedPunchesBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<MissedPunchesBo> requestedList = (from l in context.tbl_EmployeeMissedPunches
                                                       join e in context.tbl_EmployeeMaster on l.HRApproverID equals e.ID into hr
                                                       from hrApprover in hr.DefaultIfEmpty()
                                                       join r in context.tbl_EmployeeMaster on l.EmployeeID equals r.ID
                                                       join s in context.tbl_Status on l.StatusID equals s.ID
                                                       join c in context.tbl_CodeMaster on l.PunchTypeID equals c.ID into code
                                                       from cm in code.DefaultIfEmpty()
                                                       where !l.IsDeleted && l.DomainID == missedPunchesBo.DomainId
                                                          && (missedPunchesBo.StatusId == 0 || (l.StatusID == missedPunchesBo.StatusId)) &&
                                                          (missedPunchesBo.HrApproverId == null || (l.HRApproverID == missedPunchesBo.HrApproverId))
                                                       orderby l.CreatedOn descending
                                                       select new MissedPunchesBo
                                                       {
                                                           Id = l.ID,
                                                           EmployeeId = l.EmployeeID,
                                                           Remarks = l.Remarks,
                                                           HrApproverRemarks = l.HRRemarks,
                                                           PunchTypeId = l.PunchTypeID,
                                                           PunchDate = l.PunchDate,
                                                           PunchTime = l.PunchTime,
                                                           Status = new  cvt.officebau.com.ViewModels.Entity()
                                                           {
                                                               Name = s.Code
                                                           },
                                                           StatusCode = s.Code ?? "",
                                                           HrApproverId = l.HRApproverID,
                                                           HrApproverName = hrApprover.FullName,
                                                           RequestedDate = l.CreatedOn,
                                                           StatusId = l.StatusID,
                                                           HrApprovedDate = l.HRApprovedOn,
                                                           Employee = new  cvt.officebau.com.ViewModels.Entity()
                                                           {
                                                               Name = (r.EmpCodePattern ?? "") + (r.Code ?? "") + " - " + r.FullName
                                                           },
                                                           EmployeePunchType = new  cvt.officebau.com.ViewModels.Entity()
                                                           {
                                                               Name = cm.Code
                                                           },
                                                       }).ToList();
                return requestedList;
            }
        }

        public string MissedPunchesApproval(int? id, string status, string remarks, int domainId, int punchTypeId, DateTime punchDate, bool isDeleted, DateTime hrApprovedOn, int employeeId)
        {
            string outputMessage;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_MISSEDPUNCHAPPROVE);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.PunchTypeID, DbType.Int32, punchTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.PunchDate, DbType.Date, punchDate);
                Database.AddInParameter(dbCommand, DBParam.Input.HRRemarks, DbType.String, remarks);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, status);
                //this.Database.AddInParameter(dbCommand, DBParam.Input.HRApproverID, DbType.Int32, hrApproverID);
                Database.AddInParameter(dbCommand, DBParam.Input.HRApprovedOn, DbType.Date, hrApprovedOn);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, isDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.ModifiedBy, DbType.Int32, Utility.UserId());
                outputMessage = Database.ExecuteScalar(dbCommand).ToString();
            }
            return outputMessage;
        }

        #endregion Missed Punches Approval

        #region Missed Check-In/Check-Out Punches Report

        public List<MissedPunchesBo> MissedCheckInCheckOutReportList(MissedPunchesBo missedPunchesBo)
        {
            List<MissedPunchesBo> missedPunchesBoList = new List<MissedPunchesBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MISSEDCHECKINCHECKOUTREPORTLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, missedPunchesBo.Year);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, missedPunchesBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, missedPunchesBo.BusinessUnitIDs);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, missedPunchesBo.StatusId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        missedPunchesBoList.Add(new MissedPunchesBo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            TotalCheckIn = Utility.CheckDbNull<int>(reader[DBParam.Output.CheckIn]),
                            TotalCheckOut = Utility.CheckDbNull<int>(reader[DBParam.Output.CheckOut]),
                            PunchDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.PunchTime]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region]),
                            StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            MonthId = missedPunchesBo.MonthId,
                            YearName = missedPunchesBo.YearName
                        });
                    }
                }
            }
            return missedPunchesBoList;
        }

        public List<MissedPunchesBo> MissedCheckInCheckOutPopup(MissedPunchesBo missedPunchesBo)
        {
            int punchTypeId = 0;
            //int year = Convert.ToDateTime(missedPunchesBo.PunchDate).Year;
            //int month = Convert.ToDateTime(missedPunchesBo.PunchDate).Month;
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                var statusId = context.tbl_Status.Where(hr => hr.Type.ToUpper().Contains("LEAVE") && hr.Code.ToUpper() == missedPunchesBo.StatusCode).Select(hr => hr.ID).FirstOrDefault();
                if (missedPunchesBo.PunchType.ToUpper() == "CHECKIN")
                {
                    punchTypeId = context.tbl_CodeMaster.Where(hr => hr.Type.ToUpper().Contains("PUNCHTYPE") && hr.Code.ToUpper() == "CHECK IN").Select(hr => hr.ID).FirstOrDefault();
                }
                else if (missedPunchesBo.PunchType.ToUpper() == "CHECKOUT")
                {
                    punchTypeId = context.tbl_CodeMaster.Where(hr => hr.Type.ToUpper().Contains("PUNCHTYPE") && hr.Code.ToUpper() == "CHECK OUT").Select(hr => hr.ID).FirstOrDefault();
                }

                List<MissedPunchesBo> requestedList = (from l in context.tbl_EmployeeMissedPunches
                                                       where l.EmployeeID == missedPunchesBo.EmployeeId && !l.IsDeleted
                                                              && l.PunchTypeID == punchTypeId && l.StatusID == statusId
                                                              && (missedPunchesBo.YearName == 0 || l.PunchDate.Year == missedPunchesBo.YearName)
                                                              && (missedPunchesBo.MonthId == 0 || l.PunchDate.Month == missedPunchesBo.MonthId)
                                                              && l.DomainID == domainId
                                                       orderby l.PunchDate descending
                                                       select new MissedPunchesBo
                                                       {
                                                           PunchDate = l.PunchDate,
                                                           PunchType = missedPunchesBo.PunchType,
                                                           PunchTime = l.PunchTime,
                                                           Remarks = l.Remarks ?? ""
                                                       }).ToList();
                return requestedList;
            }
        }

        #endregion Missed Check-In/Check-Out Punches Report

        #region Email

        public LeaveManagementBo GetEmailContent(int leaveId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                LeaveManagementBo result = (from l in context.tbl_EmployeeLeave
                                            join e in context.tbl_EmployeeMaster on l.ApproverID equals e.ID
                                            join emp in context.tbl_EmployeeMaster on l.EmployeeID equals emp.ID
                                            join t in context.tbl_LeaveTypes on l.LeaveTypeID equals t.ID
                                            join s in context.tbl_Status on l.StatusID equals s.ID
                                            join h in context.tbl_EmployeeMaster on l.HRApproverID equals h.ID into hr
                                            from ap in hr.DefaultIfEmpty()
                                            from approverPrefix in context.tbl_CodeMaster.Where(c => c.ID == e.PrefixID).DefaultIfEmpty()
                                            from prefix in context.tbl_CodeMaster.Where(c => c.ID == emp.PrefixID).DefaultIfEmpty()
                                            from des in context.tbl_Designation.Where(d => d.ID == emp.DesignationID).DefaultIfEmpty()
                                            from designation in context.tbl_Designation.Where(d => d.ID == e.DesignationID).DefaultIfEmpty()
                                            from hrDesignation in context.tbl_Designation.Where(d => d.ID == ap.DesignationID).DefaultIfEmpty()
                                            let duration = context.tbl_EmployeeLeaveDateMapping.Count(d => !d.IsDeleted && d.LeaveRequestID == l.ID)
                                            where l.ID == leaveId && !l.IsDeleted
                                            select new LeaveManagementBo
                                            {
                                                LeaveTypeName = t.Name,
                                                EmployeeCode = emp.LoginCode,
                                                EmployeeName = emp.FullName,
                                                NoOfDays = duration,
                                                FromDate = (DateTime?)l.FromDate,
                                                ToDate = (DateTime?)l.ToDate,
                                                Reason = l.Reason,
                                                Duration = l.Duration,
                                                EmployeeEmail = emp.EmailID,
                                                ApproverEmail = e.EmailID,
                                                RequesterRemarks = l.RequesterRemarks,
                                                ApproverRemarks = l.ApproverRemarks,
                                                Approver = e.FullName,
                                                StatusCode = s.Code,
                                                ApproverName = e.LoginCode,
                                                HrEmail = ap.EmailID,
                                                HrName = ap.FullName,
                                                HrRemarks = l.HRRemarks,
                                                HrLoginCode = ap.LoginCode,
                                                Prefix = prefix.Code,
                                                ApproverPrefix = approverPrefix.Code,
                                                Designation = des.Name,
                                                ApproverDesignation = designation.Name,
                                                HrDesignation = hrDesignation.Name,
                                                IsHalfDay = l.IsHalfDay ?? false,
                                            }).FirstOrDefault();

                return result;
            }
        }

        public string PopulateLeaveMailBody(LeaveManagementBo leaveManagementBo, string type, string remarks)
        {
            string content = string.Empty;
            string regards;
            string designation;

            if (type == "Request")
            {
                content = @"I would like to request a leave for the below mentioned dates.
                            I would be glad to help with a plan to cover my workload in my absence.
                            Thank you for your consideration of my request.";
            }
            else if (type.Contains("Approved"))
            {
                content = "Your leave request has been approved. <br/>";
            }
            else if (type.Contains("Rejected"))
            {
                content = "Your leave request has been rejected. <br/>";
            }

            if (type.Contains("Manager"))
            {
                regards = leaveManagementBo.Approver;
                designation = leaveManagementBo.ApproverDesignation;
            }
            else if (type.Contains("HR"))
            {
                regards = leaveManagementBo.HrName;
                designation = leaveManagementBo.HrDesignation;
            }
            else
            {
                regards = leaveManagementBo.EmployeeName;
                designation = leaveManagementBo.Designation;
            }
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/Leaves.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Name}", type == "Request" ? leaveManagementBo.Approver : leaveManagementBo.EmployeeName);
            body = body.Replace("{Content}", content);
            body = body.Replace("{FromDate}", Convert.ToDateTime(leaveManagementBo.FromDate).ToString("dd-MMM-yyyy"));
            body = body.Replace("{ToDate}", Convert.ToDateTime(leaveManagementBo.ToDate).ToString("dd-MMM-yyyy"));
            body = body.Replace("{Days}", !leaveManagementBo.LeaveTypeName.ToUpper().Contains("PERMISSION")
                                        ? (leaveManagementBo.IsHalfDay ? "½" : leaveManagementBo.NoOfDays.ToString()) + " day(s)"
                                        : leaveManagementBo.Duration.ToString() + " hour(s)");
            body = body.Replace("{LeaveType}", leaveManagementBo.LeaveTypeName);
            body = body.Replace("{Reason}", leaveManagementBo.Reason);
            body = body.Replace("{Remarks}", type == "Request" ? string.Empty : "<b>Remarks : </b>" + remarks);
            body = body.Replace("{Regards}", regards);
            body = body.Replace("{Prefix}", type == "Request" ? leaveManagementBo.ApproverPrefix : leaveManagementBo.Prefix);
            body = body.Replace("{Designation}", designation);
            return body;
        }

        #endregion Email

        #region Total Hours Clocked Report

        public List<MissedPunchesBo> TotalHoursClockedReportList(MissedPunchesBo missedPunchesBo)
        {
            List<MissedPunchesBo> missedPunchesBoList = new List<MissedPunchesBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.TOTALHOURSCLOCKEDREPORTLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, missedPunchesBo.Year);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, missedPunchesBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, missedPunchesBo.BusinessUnitIDs);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        missedPunchesBoList.Add(new MissedPunchesBo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            TotalMinutes = Utility.CheckDbNull<int>(reader[DBParam.Output.Minutes]),
                            TotalDuration = Utility.CheckDbNull<string>(reader[DBParam.Output.Duration]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region])
                        });
                    }
                }
            }
            return missedPunchesBoList;
        }

        #endregion Total Hours Clocked Report

        #region Export Employee LeaveDays

        public DataTable ExportEmployeeLeaveDays(int year, int businessUnitId)
        {
            DataSet ds = new DataSet();
            DataTable dtLeaveType = new DataTable("ExportEmployeeLeaveDays");
            ds.Tables.Add(dtLeaveType);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORT_LEAVETYPE_DETAILS);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, year);
                Database.AddInParameter(dbCommand, DBParam.Input.BaseLocationID, DbType.Int32, businessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtLeaveType);
                }
                return dtLeaveType;
            }
        }

        #endregion Export Employee LeaveDays

        #region UploadLeaveDays

        public DataTable GetAvailableLeaveList(LeaveManagementBo leaveManagementBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_AVAILABLELEAVELIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCodes, DbType.String, leaveManagementBo.EmployeeCodes);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, leaveManagementBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, leaveManagementBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dtAvailableLeaveList = new DataTable("AvailableLeaveList");
                ds.Tables.Add(dtAvailableLeaveList);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtAvailableLeaveList);
                }
                return dtAvailableLeaveList;
            }
        }

        public List<LeaveManagementBo> SearchLeaveDays(LeaveManagementBo leaveManagementBo)
        {
            List<LeaveManagementBo> list = new List<LeaveManagementBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_LEAVEDAYS);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeCodes, DbType.String, leaveManagementBo.EmployeeCodes);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, leaveManagementBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.String, leaveManagementBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.LeaveTypeID, DbType.String, leaveManagementBo.LeaveTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        leaveManagementBo = new LeaveManagementBo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeNo]),
                            Year = Utility.CheckDbNull<string>(reader[DBParam.Output.FinancialYear]),
                            LopDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.LOPDays])
                        };
                        list.Add(leaveManagementBo);
                    }
                }
                return list;
            }
        }

        public string ManageLeaveDays(List<LeaveManagementBo> leaveBoList) // Anbu
        {
            string outputMessage = string.Empty;
            using (DbConnection dbConnection = Database.CreateConnection())
            {
                foreach (LeaveManagementBo leaveManagementBoList in leaveBoList)
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_LEAVEDAYS);
                    dbConnection.Open();
                    Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.String, leaveManagementBoList.EmployeeId);
                    Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, leaveManagementBoList.YearId);
                    Database.AddInParameter(dbCommand, DBParam.Input.LeaveTypeID, DbType.Int32, leaveManagementBoList.LeaveTypeId);
                    Database.AddInParameter(dbCommand, DBParam.Input.LopDays, DbType.Decimal, leaveManagementBoList.LopDays);
                    Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    outputMessage = Database.ExecuteScalar(dbCommand).ToString();
                    dbConnection.Close();
                }
            }
            return outputMessage;
        }

        public List< cvt.officebau.com.ViewModels.Entity> SearchAutoCompleteEmployeeOnBuChange(string searchName, int BUID)
        {
            List< cvt.officebau.com.ViewModels.Entity> list = new List< cvt.officebau.com.ViewModels.Entity>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_AUTOCOMPLETE_EMPLOYEE_ON_BU_CHANGE);

                Database.AddInParameter(dbCommand, DBParam.Input.SearchParameter, DbType.String, searchName);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.String, BUID);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        list.Add(new  cvt.officebau.com.ViewModels.Entity
                        {
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return list;
        }

        #endregion UploadLeaveDays

        #region Complete Leave Report

        public List<LeaveReportBo> CompleteLeaveReportList(LeaveReportBo leaveReportBo)
        {
            List<LeaveReportBo> leaveReportBoList = new List<LeaveReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_COMPLETELEAVEREPORTLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.FromDate, DbType.DateTime, leaveReportBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.ToDate, DbType.DateTime, leaveReportBo.ToDate);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, leaveReportBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, leaveReportBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, leaveReportBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        leaveReportBoList.Add(new LeaveReportBo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            LeaveType = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            TotalLeave = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Total]),
                            NoOfDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.NoOfDays]),
                            FromDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.FromDate]),
                            ToDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ToDate]),
                            IsHalfDay = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsHalfDay]),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region]),
                        });
                    }
                }
            }
            return leaveReportBoList;
        }

        #endregion Complete Leave Report

        #region Late Arrival Report

        public List<LeaveReportBo> LateArrivalReportList(LeaveReportBo leaveReportBo)
        {
            List<LeaveReportBo> leaveReportBoList = new List<LeaveReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_LATEARRIVALREPORTLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, leaveReportBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, leaveReportBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, leaveReportBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, leaveReportBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, leaveReportBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.Minutes, DbType.Int32, leaveReportBo.Minutes);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        leaveReportBoList.Add(new LeaveReportBo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region]),
                            Minutes = Utility.CheckDbNull<int>(reader[DBParam.Output.Minutes]),
                            Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.Punches]),
                            Hours = Utility.CheckDbNull<string>(reader[DBParam.Output.PunchTime]),
                        });
                    }
                }
            }
            return leaveReportBoList;
        }

        #endregion Late Arrival Report

        #region Late Arrival Report

        public List<LeaveReportBo> EarlyDepartureReportList(LeaveReportBo leaveReportBo)
        {
            List<LeaveReportBo> leaveReportBoList = new List<LeaveReportBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_EARLYDEPARTUREREPORTLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, leaveReportBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, leaveReportBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, leaveReportBo.YearId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, leaveReportBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, leaveReportBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.Minutes, DbType.Int32, leaveReportBo.Minutes);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        leaveReportBoList.Add(new LeaveReportBo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Region = Utility.CheckDbNull<string>(reader[DBParam.Output.Region]),
                            Minutes = Utility.CheckDbNull<int>(reader[DBParam.Output.Minutes]),
                            Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.Punches]),
                            Hours = Utility.CheckDbNull<string>(reader[DBParam.Output.PunchTime]),
                        });
                    }
                }
            }
            return leaveReportBoList;
        }

        #endregion Late Arrival Report

        #region Revert Leave

        public string RevertLeave(LeaveManagementBo leaveManagementBo)
        {
            string outputMessage;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGEREVERTLEAVE);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, leaveManagementBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.LeaveDate, DbType.Date, leaveManagementBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                outputMessage = Database.ExecuteScalar(dbCommand).ToString();
            }
            return outputMessage;
        }

        public List<LeaveManagementBo> ShowLeaveDetails(LeaveManagementBo leaveManagementBo)
        {
            List<LeaveManagementBo> leaveManagementBoList = new List<LeaveManagementBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETLEAVEDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, leaveManagementBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.LeaveDate, DbType.DateTime, leaveManagementBo.FromDate);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        leaveManagementBoList.Add(
                            new LeaveManagementBo()
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                FromDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.FromDate]),
                                ToDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ToDate]),
                                NoOfDays = Utility.CheckDbNull<int>(reader[DBParam.Output.Duration]),
                                StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.Requester]),
                                LeaveType = Utility.CheckDbNull<string>(reader[DBParam.Output.LeaveType]),
                                IsHalfDay = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsHalfDay])
                            });
                    }
                }
                return leaveManagementBoList;
            }
        }

        #endregion Revert Leave

        #region Employee Comp off Report

        public DataTable SearchEmployeeCompOffReport(MissedPunchesBo missedPunchesBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EMPLOYEECOMPOFF);

                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, missedPunchesBo.Year);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, missedPunchesBo.MonthId);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, missedPunchesBo.StatusId);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, missedPunchesBo.BusinessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                DataSet ds = new DataSet();
                DataTable dtEmployeeCompOff = new DataTable("EmployeeCompOff");
                ds.Tables.Add(dtEmployeeCompOff);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtEmployeeCompOff);
                }
                return dtEmployeeCompOff;
            }
        }

        #endregion Employee Comp off Report
    }
}