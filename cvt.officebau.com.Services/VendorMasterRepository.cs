﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class VendorMasterRepository : BaseRepository
    {
        #region Get Vendor

        public VendorMasterBo GetVendor(int vendorId)
        {
            VendorMasterBo vendorMasterBo = new VendorMasterBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_VENDOR);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, vendorId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        vendorMasterBo = new VendorMasterBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Address = Utility.CheckDbNull<string>(reader[DBParam.Output.Address]),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms]),
                            ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                            ContactPerson = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactPerson]),
                            ContactPersonNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactPersonNo]),
                            EmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]),
                            VendorEmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.VendorEmailID]),
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.BillingAddress]),
                            City = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CityID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.CityName])
                            },
                            Currency = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.CurrencyID])
                            },
                            PanNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PanNO]),
                            TanNo = Utility.CheckDbNull<string>(reader[DBParam.Output.TanNo]),
                            TinNo = Utility.CheckDbNull<string>(reader[DBParam.Output.TinNo]),
                            CstNo = Utility.CheckDbNull<string>(reader[DBParam.Output.CSTNo]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            GstNo = Utility.CheckDbNull<string>(reader[DBParam.Output.GSTNo])
                        };
                    }
                }
            }

            return vendorMasterBo;
        }

        #endregion Get Vendor

        #region Search Vendor

        public List<VendorMasterBo> SearchVendor(VendorMasterBo vendorMasterBo)
        {
            List<VendorMasterBo> vendorMasterBoList = new List<VendorMasterBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_VENDOR);

                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, vendorMasterBo.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        vendorMasterBoList.Add(
                            new VendorMasterBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                                ContactPersonNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactPersonNo]),
                                ContactPerson = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactPerson]),
                                ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo]),
                                City = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.CityName])
                                },
                                Currency = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.CurrencyName])
                                },
                                PanNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PANNo]),
                                GstNo = Utility.CheckDbNull<string>(reader[DBParam.Output.GSTNo]),
                                PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms])
                            });
                    }
                }
            }

            return vendorMasterBoList;
        }

        #endregion Search Vendor

        #region Manage Vendor

        public string ManageVendor(VendorMasterBo vendorMasterBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_VENDOR);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, vendorMasterBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.CityID, DbType.String, vendorMasterBo.City == null ? null : vendorMasterBo.City.Id == 0 ? null : vendorMasterBo.City.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.CurrencyID, DbType.String, vendorMasterBo.Currency == null ? null : vendorMasterBo.Currency.Id == 0 ? null : vendorMasterBo.Currency.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactPersonNo, DbType.String, vendorMasterBo.ContactPersonNo);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, vendorMasterBo.Name);
                Database.AddInParameter(dbCommand, DBParam.Input.Address, DbType.String, vendorMasterBo.Address);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, vendorMasterBo.Remarks);
                Database.AddInParameter(dbCommand, DBParam.Input.PaymentTerms, DbType.String, vendorMasterBo.PaymentTerms);
                Database.AddInParameter(dbCommand, DBParam.Input.ContactNo, DbType.String, vendorMasterBo.ContactNo);
                Database.AddInParameter(dbCommand, DBParam.Input.contactPerson, DbType.String, vendorMasterBo.ContactPerson);
                Database.AddInParameter(dbCommand, DBParam.Input.EmailID, DbType.String, vendorMasterBo.EmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.VendorEmailID, DbType.String, vendorMasterBo.VendorEmailId);
                Database.AddInParameter(dbCommand, DBParam.Input.BillingAddress, DbType.String, vendorMasterBo.BillingAddress);
                Database.AddInParameter(dbCommand, DBParam.Input.PanNo, DbType.String, vendorMasterBo.PanNo);
                Database.AddInParameter(dbCommand, DBParam.Input.TanNo, DbType.String, vendorMasterBo.TanNo);
                Database.AddInParameter(dbCommand, DBParam.Input.TinNo, DbType.String, vendorMasterBo.TinNo);
                Database.AddInParameter(dbCommand, DBParam.Input.CSTNo, DbType.String, vendorMasterBo.CstNo);
                Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, vendorMasterBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.GSTNo, DbType.String, vendorMasterBo.GstNo);

                vendorMasterBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return vendorMasterBo.UserMessage;
        }

        #endregion Manage Vendor
    }
}
