﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Web;

namespace cvt.officebau.com.Services
{
    public class TransferRepository : BaseRepository
    {
        #region Search Transfer

        public List<TransferBo> SearchTransfer(SearchParamsBo searchParamsBo)
        {
            List<TransferBo> transferBoList = new List<TransferBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.SEARCH_TRANSFER);
                Database.AddInParameter(dbCmd, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCmd, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCmd, DBParam.Input.BankID, DbType.Int32, searchParamsBo.Id);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCmd, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCmd, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount);

                using (IDataReader reader = Database.ExecuteReader(dbCmd))
                {
                    while (reader.Read())
                    {
                        transferBoList.Add(
                            new TransferBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                TransferFrom = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Id = Utility.CheckDbNull<int>(reader[DBParam.Output.FromID]),
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.TransferFromName])
                                },
                                TransferTo = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ToID]),
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.TransferToName])
                                },
                                Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                                Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.VoucherDate]),
                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.VoucherDescription])
                            });
                    }
                }
            }

            return transferBoList;
        }

        #endregion

        #region Get Transfer

        public TransferBo GetTransfer(int transferId)
        {
            TransferBo transferBo = new TransferBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.GET_TRANSFER);

                Database.AddInParameter(dbCmd, DBParam.Input.ID, DbType.Int32, transferId);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCmd))
                {
                    while (reader.Read())
                    {
                        transferBo = new TransferBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            TransferFrom = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.FromID])
                            },
                            TransferTo = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ToID])
                            },
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            Date = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.VoucherDate]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.VoucherDescription]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy])
                        };
                    }
                }
            }

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            transferBo.TransferToList = masterDataRepository.SearchMasterDataDropDown("Bank");
            string from = "Cash";
            string to = "Cash";
            if ((transferBo.TransferFrom == null ? 0 : transferBo.TransferFrom.Id) != 0)
            {
                from = transferBo.TransferToList.Find(a => a.Value == transferBo.TransferFrom.Id.ToString()).Text;
            }

            if ((transferBo.TransferTo == null ? 0 : transferBo.TransferTo.Id) != 0)
            {
                to = transferBo.TransferToList.Find(a => a.Value == transferBo.TransferTo.Id.ToString()).Text;
            }

            transferBo.Emailhtml = PopulateMailBodyTransferUpdated(from, Convert.ToDecimal(transferBo.Amount), to, transferBo.Date, transferBo.Description, transferBo.ModifiedByName, Convert.ToDateTime(transferBo.ModifiedOn));
            return transferBo;
        }

        #endregion

        #region Manage Transfer

        public string ManageTransfer(TransferBo transferBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.MANAGE_TRANSFER);

                Database.AddInParameter(dbCmd, DBParam.Input.ID, DbType.Int32, transferBo.Id);
                Database.AddInParameter(dbCmd, DBParam.Input.FromID, DbType.Int32, transferBo.TransferFrom?.Id);
                Database.AddInParameter(dbCmd, DBParam.Input.VoucherDescription, DbType.String, transferBo.Description);
                Database.AddInParameter(dbCmd, DBParam.Input.ToID, DbType.Int32, transferBo.TransferTo?.Id);
                Database.AddInParameter(dbCmd, DBParam.Input.Amount, DbType.Decimal, transferBo.Amount);
                Database.AddInParameter(dbCmd, DBParam.Input.VoucherDate, DbType.DateTime, transferBo.Date);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCmd, DBParam.Input.HasDeleted, DbType.Boolean, transferBo.IsDeleted);
                Database.AddInParameter(dbCmd, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                transferBo.UserMessage = Database.ExecuteScalar(dbCmd).ToString();
                MasterDataRepository masterDataRepository = new MasterDataRepository();
                transferBo.TransferToList = masterDataRepository.SearchMasterDataDropDown("Bank");
                string from = "Cash";
                string to = "Cash";
                if ((transferBo.TransferFrom == null ? 0 : transferBo.TransferFrom.Id) != 0)
                {
                    @from = transferBo.TransferToList.Find(a => a.Value == transferBo.TransferFrom.Id.ToString()).Text;
                }

                if ((transferBo.TransferTo == null ? 0 : transferBo.TransferTo.Id) != 0)
                {
                    to = transferBo.TransferToList.Find(a => a.Value == transferBo.TransferTo.Id.ToString()).Text;
                }

                if (transferBo.UserMessage == Notification.Inserted)
                {
                    PopulateMailBodyTransfer(@from, Convert.ToDecimal(transferBo.Amount), to, transferBo.Date, transferBo.Description);
                }

                if (transferBo.UserMessage == Notification.Updated)
                {
                    PopulateMailBodyTransferUpdatedEmail(@from, Convert.ToDecimal(transferBo.Amount), to, transferBo.Date, transferBo.Description, transferBo.Emailhtml);
                }
            }

            return transferBo.UserMessage;
        }

        #endregion

        #region Get Available Balance

        public TransferBo GetAvailableBalance(int contraVoucherId)
        {
            TransferBo transferBo = new TransferBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.GET_AVAILABLEBALANCE);

                Database.AddInParameter(dbCmd, DBParam.Input.ID, DbType.Int32, contraVoucherId);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCmd))
                {
                    while (reader.Read())
                    {
                        transferBo = new TransferBo
                        {
                            OpeningBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OpeningBalance])
                        };
                    }
                }
            }

            return transferBo;
        }

        #endregion


        public string PopulateMailBodyTransfer(string from, decimal amount, string to, DateTime? date, string desc)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/Transfer.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{From}", from);
            body = body.Replace("{To}", to);
            body = body.Replace("{Amount}", "INR" + " " + Utility.PrecisionForDecimal(amount, 2));
            body = body.Replace("{desc}", desc);
            body = body.Replace("{Date}", Convert.ToDateTime(date).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{Createdby}", Utility.GetSession("FirstName") + " - On " + DateTime.Now.ToString("dd-MMMM-yyyy h:mm tt") + " IST");
            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();
            ApplicationConfigBo applicationConfigBo = applicationConfigurationRepository.GetEmailConfig("Transfer");
            CommonRepository commonRepository = new CommonRepository();
            commonRepository.ManageEmail_InsertintoProcessorTable(applicationConfigBo.To, applicationConfigBo.Cc, applicationConfigBo.Description, body,
                "OfficeBAU Transfer" + " - INR " + Utility.PrecisionForDecimal(amount, 2), "Transfer", null);
            return body;
        }

        public string PopulateMailBodyTransferUpdated(string from, decimal amount, string to, DateTime? date, string desc, string mod, DateTime datecreated)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/UpdatedTransfer.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{From}", from);
            body = body.Replace("{To}", to);
            body = body.Replace("{Amount}", "INR" + " " + Utility.PrecisionForDecimal(amount, 2));
            body = body.Replace("{desc}", desc);
            body = body.Replace("{Date}", Convert.ToDateTime(date).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{Createdby}", mod + " - On " + datecreated.ToString("dd-MMMM-yyyy h:mm tt") + " IST");
            return body;
        }

        public string PopulateMailBodyTransferUpdatedEmail(string from, decimal amount, string to, DateTime? date, string desc, string body)
        {
            body = body.Replace("{FromUpdated}", from);
            body = body.Replace("{ToUpdated}", to);
            body = body.Replace("{AmountUpdated}", "INR" + " " + Utility.PrecisionForDecimal(amount, 2));
            body = body.Replace("{descUpdated}", desc);
            body = body.Replace("{DateUpdated}", Convert.ToDateTime(date).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{CreatedbyUpdated}", Utility.GetSession("FirstName") + " - On " + DateTime.Now.ToString("dd-MMMM-yyyy h:mm tt") + " IST");
            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();
            ApplicationConfigBo applicationConfigBo = applicationConfigurationRepository.GetEmailConfig("Transfer");
            CommonRepository commonRepository = new CommonRepository();
            commonRepository.ManageEmail_InsertintoProcessorTable(applicationConfigBo.To, applicationConfigBo.Cc, applicationConfigBo.Description, body,
                "OfficeBAU Transfer" + " - INR " + Utility.PrecisionForDecimal(amount, 2), "Transfer", null);
            return body;
        }
    }
}
