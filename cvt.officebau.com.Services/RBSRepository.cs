﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;

namespace cvt.officebau.com.Services
{
    public class RbsRepository : BaseRepository
    {
        public bool IsAuthorized(int userId, string menuId, string operationType)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SearchUserMenuAccess);

                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, userId);
                Database.AddInParameter(dbCommand, DBParam.Input.MenuID, DbType.String, menuId);
                Database.AddInParameter(dbCommand, DBParam.Input.OperationType, DbType.String, operationType);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    if (reader.Read())
                    {
                        int resultInt = Convert.ToInt32(reader[DBParam.Output.Result].ToString());
                        return resultInt == 1;
                    }
                }
            }

            return false;
        }

        #region Manage RBSMenu

        public string ManageRbsMenu(IList<RbsUserMenuBo> rbsUserMenuBo)
        {
            string outputMessage = string.Empty;

            using (Database.CreateConnection())
            {
                foreach (RbsUserMenuBo rbs in rbsUserMenuBo)
                {
                    DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_RBSMENU);
                    Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, rbs.UserId);
                    Database.AddInParameter(dbCommand, DBParam.Input.MenuID, DbType.Int32, rbs.MenuId);
                    Database.AddInParameter(dbCommand, DBParam.Input.MRead, DbType.Boolean, rbs.MenuRead);
                    Database.AddInParameter(dbCommand, DBParam.Input.MWrite, DbType.Boolean, rbs.MenuWrite);
                    Database.AddInParameter(dbCommand, DBParam.Input.MEdit, DbType.Boolean, rbs.MenuEdit);
                    Database.AddInParameter(dbCommand, DBParam.Input.MDelete, DbType.Boolean, rbs.MenuDelete);
                    Database.AddInParameter(dbCommand, DBParam.Input.ModifiedBy, DbType.Int32, Utility.UserId());
                    Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                    outputMessage = Database.ExecuteScalar(dbCommand).ToString();
                }

                return outputMessage;
            }
        }

        #endregion Manage RBSMenu

        #region Search User

        public List<RbsUserMenuBo> SearchUser(RbsUserMenuBo rbsMenuBo)
        {
            List<RbsUserMenuBo> userlist = new List<RbsUserMenuBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHUSER);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userlist.Add(
                            new RbsUserMenuBo
                            {
                                UserId = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                UserName = reader[DBParam.Output.Name].ToString(),
                                EmployeeName = reader[DBParam.Output.EmployeeName].ToString()
                            });
                    }
                }
            }

            return userlist;
        }

        #endregion Search User

        #region Get RBSUserMenu

        public List<RbsUserMenuBo> GetRbsUserMenu(int userId, int? moduleId, string type)
        {
            List<RbsUserMenuBo> listOfMenu = new List<RbsUserMenuBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand;
                if (string.IsNullOrEmpty(type))
                {
                    dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_RBSMENUFORUSER);
                }
                else
                {
                    dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_RBSSUBMODULE_MENUFORUSER);
                }

                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, userId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                if (!string.IsNullOrEmpty(type))
                {
                    Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, type);
                }

                if (moduleId == 0)
                {
                    Database.AddInParameter(dbCommand, DBParam.Input.ModuleID, DbType.Int32, null);
                }
                else
                {
                    Database.AddInParameter(dbCommand, DBParam.Input.ModuleID, DbType.Int32, moduleId);
                }

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        listOfMenu.Add(new RbsUserMenuBo
                        {
                            Module = reader[DBParam.Output.Module].ToString(),
                            Menu = reader[DBParam.Output.Menu].ToString(),
                            MenuRead = Convert.ToBoolean(reader[DBParam.Output.MenuRead].ToString()),
                            MenuWrite = Convert.ToBoolean(reader[DBParam.Output.MenuWrite].ToString()),
                            MenuEdit = Convert.ToBoolean(reader[DBParam.Output.MenuEdit].ToString()),
                            MenuDelete = Convert.ToBoolean(reader[DBParam.Output.MenuDelete].ToString()),
                            MenuId = Convert.ToInt32(reader[DBParam.Output.MenuID].ToString()),
                            RbsModule = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Name = reader[DBParam.Output.ModuleID].ToString()
                            },
                            UserId = userId
                        });
                    }
                }
            }

            return listOfMenu;
        }

        #endregion Get RBSUserMenu

        #region GetDependantDropdownList

        public List<RbsMenuBo> GetDependantDropdownList(string type)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                if (string.IsNullOrEmpty(type))
                {
                    List<RbsMenuBo> list = (from t in context.tbl_RBSSubModule
                                            where t.DomainID == domainId
                                            orderby t.SubModule
                                            select new RbsMenuBo
                                            {
                                                Module = t.SubModule,
                                                Id = t.ID
                                            }).ToList();
                    return list;
                }

                if (type.Equals("REPORT"))
                {
                    List<RbsMenuBo> list = (from rbs in context.tbl_RBSMenu
                                            join subrbs in context.tbl_RBSMenu on rbs.SubModuleID equals subrbs.ID
                                            join sub in context.tbl_RBSSubModule on subrbs.SubModuleID equals sub.ID into su
                                            from s in su.DefaultIfEmpty()
                                            where rbs.DomainID == domainId && (rbs.IsSubmenu == true || rbs.IsSubmenu == null) && rbs.RootConfig.ToUpper().Equals("REPORT")
                                            select new RbsMenuBo
                                            {
                                                Module = s.SubModule + " - " + subrbs.RootConfig,
                                                Id = rbs.SubModuleID
                                            }).Distinct().ToList();
                    return list;
                }
                else
                {
                    List<RbsMenuBo> list = (from rbs in context.tbl_RBSMenu
                                            join subrbs in context.tbl_RBSMenu on rbs.SubModuleID equals subrbs.ID
                                            join sub in context.tbl_RBSSubModule on subrbs.SubModuleID equals sub.ID into su
                                            from s in su.DefaultIfEmpty()
                                            where rbs.DomainID == domainId && (rbs.IsSubmenu == true || rbs.IsSubmenu == null) && rbs.RootConfig.ToUpper().Equals("CONFIG")
                                            select new RbsMenuBo
                                            {
                                                Module = s.SubModule,
                                                Id = rbs.SubModuleID
                                            }).Distinct().ToList();
                    return list;
                }
            }
        }

        #endregion GetDependantDropdownList

        #region Search Menu for Reportal

        public List<RbsMenuBo> SearchMenuforReportal()
        {
            List<RbsMenuBo> menuList = new List<RbsMenuBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_RBSMENUFOR_REPORTAL);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.MenuCode, DbType.String, Utility.GetSession("MenuID"));
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    RbsMenuBo menuBO;
                    while (reader.Read())
                    {
                        menuBO = new RbsMenuBo
                        {
                            Module = reader[DBParam.Output.Module].ToString(),
                            Menu = reader[DBParam.Output.Menu].ToString(),
                            MenuUrl = reader[DBParam.Output.MenuURL].ToString(),
                            MenuCode = reader[DBParam.Output.MenuCode].ToString(),
                            MorderId = reader[DBParam.Output.MOrderID].ToString(),
                            ModuleId = Utility.CheckDbNull<int>(reader[DBParam.Output.ModuleID]),
                            ModuleGroup = reader[DBParam.Output.ModuleGroup].ToString(),
                            SubModule = reader[DBParam.Output.SubModule].ToString()
                        };
                        menuList.Add(menuBO);
                    }
                }
            }

            return menuList;
        }

        #endregion Search Menu for Reportal

        #region Search Menu for Configuration

        public List<RbsMenuBo> SearchMenuForConfiguration()
        {
            List<RbsMenuBo> menuList = new List<RbsMenuBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_RBSMENUFORCONFIGURATION);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.MenuCode, DbType.String, Utility.GetSession("MenuID"));
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    RbsMenuBo menuBO;
                    while (reader.Read())
                    {
                        menuBO = new RbsMenuBo
                        {
                            Module = reader[DBParam.Output.Module].ToString(),
                            Menu = reader[DBParam.Output.Menu].ToString(),
                            MenuUrl = reader[DBParam.Output.MenuURL].ToString(),
                            MenuCode = reader[DBParam.Output.MenuCode].ToString(),
                            MorderId = reader[DBParam.Output.MOrderID].ToString(),
                            ModuleId = Utility.CheckDbNull<int>(reader[DBParam.Output.ModuleID]),
                            ModuleGroup = reader[DBParam.Output.ModuleGroup].ToString(),
                            SubModule = reader[DBParam.Output.SubModule].ToString()
                        };
                        menuList.Add(menuBO);
                    }
                }
            }

            return menuList;
        }

        #endregion Search Menu for Configuration

        #region HasAuthorized

        public List<bool> HasAuthorized(int userId, string menuCode)
        {
            List<bool> table = new List<bool>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_RBSUSERMENUACCESS);

                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, userId);
                Database.AddInParameter(dbCommand, DBParam.Input.MenuCode, DbType.String, menuCode);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    RbsUserMenuBo RBSUser;
                    while (reader.Read())
                    {
                        RBSUser = new RbsUserMenuBo
                        {
                            MenuRead = Convert.ToBoolean(reader[DBParam.Output.MenuRead]),
                            MenuWrite = Convert.ToBoolean(reader[DBParam.Output.MenuWrite]),
                            MenuEdit = Convert.ToBoolean(reader[DBParam.Output.MenuEdit]),
                            MenuDelete = Convert.ToBoolean(reader[DBParam.Output.MenuDelete])
                        };
                        table.Add(RBSUser.MenuRead);
                        table.Add(RBSUser.MenuWrite);
                        table.Add(RBSUser.MenuEdit);
                        table.Add(RBSUser.MenuDelete);
                    }
                }
            }

            return table.ToList();
        }

        public string SearchrMenu(string menuCode)
        {
            string menu = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHRMENU);
                Database.AddInParameter(dbCommand, DBParam.Input.MenuCode, DbType.String, menuCode);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        menu = reader[DBParam.Output.Menu].ToString();
                    }
                }
            }

            return menu;
        }

        #endregion HasAuthorized

        #region RPT RBS

        #region RBS Report Based On Single Module

        public List<RbsUserMenuBo> RPT_EmployeeMenulist(int moduleId)
        {
            List<RbsUserMenuBo> listEmployee = new List<RbsUserMenuBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_EMPLOYEEMENULIST);

                Database.AddInParameter(dbCommand, DBParam.Input.ModuleID, DbType.Int32, moduleId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        listEmployee.Add(new RbsUserMenuBo
                        {
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            Module = Utility.CheckDbNull<string>(reader[DBParam.Output.Module]),
                            Menu = Utility.CheckDbNull<string>(reader[DBParam.Output.Menu]),
                            MenuRead = Convert.ToBoolean(reader[DBParam.Output.MenuRead].ToString()),
                            MenuWrite = Convert.ToBoolean(reader[DBParam.Output.MenuWrite].ToString()),
                            MenuEdit = Convert.ToBoolean(reader[DBParam.Output.MenuEdit].ToString()),
                            MenuDelete = Convert.ToBoolean(reader[DBParam.Output.MenuDelete].ToString())
                        });
                    }
                }
            }

            return listEmployee;
        }

        public List<RbsMenuBo> GetMenuDropDownList(int? moduleId)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<RbsMenuBo> list = (from t in context.tbl_RBSMenu
                                        where t.SubModuleID == moduleId && t.IsSubmenu != true && t.DomainID == domainId
                                        orderby t.Menu
                                        select new RbsMenuBo
                                        {
                                            Menu = t.Menu,
                                            Id = t.ID
                                        }).ToList();
                return list;
            }
        }

        #endregion RBS Report Based On Single Module

        #region RBS Report Based On Multiple Module

        public List<RbsUserMenuBo> GetAllvalues(string module, string subModule, string subModuleMenu)
        {
            List<RbsUserMenuBo> rbsUserMenuBOlist = new List<RbsUserMenuBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_RBSUSERMENUACCESSLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.Module, DbType.String, module);
                Database.AddInParameter(dbCommand, DBParam.Input.SubModule, DbType.String, subModule);
                Database.AddInParameter(dbCommand, DBParam.Input.SubModuleMenu, DbType.String, subModuleMenu);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        rbsUserMenuBOlist.Add(new RbsUserMenuBo
                        {
                            Module = Utility.CheckDbNull<string>(reader[DBParam.Output.Module]),
                            SubModule = Utility.CheckDbNull<string>(reader[DBParam.Output.SubModule]),
                            Menu = Utility.CheckDbNull<string>(reader[DBParam.Output.Menu]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            MenuRead = Convert.ToBoolean(reader[DBParam.Output.MenuRead].ToString()),
                            MenuWrite = Convert.ToBoolean(reader[DBParam.Output.MenuWrite].ToString()),
                            MenuEdit = Convert.ToBoolean(reader[DBParam.Output.MenuEdit].ToString()),
                            MenuDelete = Convert.ToBoolean(reader[DBParam.Output.MenuDelete].ToString())
                        });
                    }
                }
            }

            return rbsUserMenuBOlist;
        }

        #endregion RBS Report Based On Multiple Module

        #endregion RPT RBS



        #region Password Reset BY Admin

        public List<RbsUserMenuBo> EmployeeListforPasswordReset(RbsUserMenuBo rbsUserMenuBo)
        {
            List<RbsUserMenuBo> userlist = new List<RbsUserMenuBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EMPLOYEE_LIST_FOR_PASSWORDRESET);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, rbsUserMenuBo.BusinessUnitIDs);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, rbsUserMenuBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userlist.Add(
                            new RbsUserMenuBo
                            {
                                EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                                EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                                UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                                BusinessUnitIDs = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                                EmailId = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID]),
                                LoginCode = Utility.CheckDbNull<string>(reader[DBParam.Output.LoginCode]),
                            });
                    }
                }
            }

            return userlist;
        }

        public string GetResetPassword(RbsUserMenuBo rbsUserMenuBo)
        {
            UserLoginBo userBo = new UserLoginBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_RESET_PASSWORD_ADMIN);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, rbsUserMenuBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.Password, DbType.String, rbsUserMenuBo.Guid);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userBo = new UserLoginBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage])
                        };
                    }
                }
            }

            return userBo.UserMessage;
        }

        public string PopulateMailBody(string username, string guid)
        {
            string body;

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/ResetPasswordByAdmin.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{UserCode}", username.Split('-')[0]);
            body = body.Replace("{UserName}", username.Split('-')[1]);
            body = body.Replace("{GUID}", guid);
            body = body.Replace("{URL}", AppConstants.ApplicationRootUrl());
            return body;
        }

        public List< cvt.officebau.com.ViewModels.Entity> SearchAutoCompleteEmployee(string searchName)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List< cvt.officebau.com.ViewModels.Entity> list = (from t in context.tbl_EmployeeMaster
                                        where !t.IsDeleted && !(t.IsActive ?? false) && t.DomainID == domainId && t.FullName.Contains(searchName)
                                        orderby t.FullName
                                        select new  cvt.officebau.com.ViewModels.Entity
                                        {
                                            Name = (t.EmpCodePattern ?? "") + (t.Code ?? "") + " - " + (t.FullName ?? ""),
                                            Id = t.ID
                                        }).ToList();
                return list;
            }
        }

        #endregion Password Reset BY Admin
    }
}