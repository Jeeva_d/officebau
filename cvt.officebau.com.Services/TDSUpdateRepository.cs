﻿using cvt.officebau.com.MessageConstants;
using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace cvt.officebau.com.Services
{
    public class TdsUpdateRepository : BaseRepository
    {
        private class TdsLastMonthYear
        {
            public int MonthId { get; set; }
            public int Year { get; set; }
            public string YearName { get; set; }
            public DateTime TdsDate { get; set; }
        }

        public string GetLastMonthYear(int baseLocationId)
        {
            int monthId, yearId, domainId = Utility.DomainId();
            string result = BOConstants.BusinessUnit_TDS_Record;

            using (FSMEntities context = new FSMEntities())
            {
                List<TdsLastMonthYear> list = (from tds in context.tbl_TDS
                                               join fin in context.tbl_FinancialYear on tds.YearID equals fin.ID
                                               where tds.BaseLocationID == baseLocationId && !tds.IsDeleted && tds.DomainId == domainId //&& !Convert.ToBoolean(fin.IsDeleted)
                                               select new TdsLastMonthYear
                                               {
                                                   MonthId = tds.MonthId,
                                                   Year = tds.YearID,
                                                   YearName = fin.Name
                                               }).ToList();

                foreach (TdsLastMonthYear li in list)
                {
                    li.TdsDate = new DateTime(Convert.ToInt32(li.YearName), li.MonthId, 1);
                }

                yearId = list.OrderByDescending(a => a.TdsDate).Select(a => a.Year).FirstOrDefault();
                monthId = list.OrderByDescending(a => a.TdsDate).Select(a => a.MonthId).FirstOrDefault();
            }

            if (monthId != 0 && yearId != 0)
            {
                result = monthId + "/" + yearId;
            }

            return result;
        }

        public string CopyTds(TDSUpdateBo tdsUpdateBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand("CopyTDS");
                Database.AddInParameter(dbCmd, DBParam.Input.MonthID, DbType.Int32, tdsUpdateBo.ToMonthId);
                Database.AddInParameter(dbCmd, DBParam.Input.YearID, DbType.Int32, tdsUpdateBo.ToYearID);
                Database.AddInParameter(dbCmd, DBParam.Input.BusinessUnit, DbType.Int32, tdsUpdateBo.BaseLocationID);
                Database.AddInParameter(dbCmd, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                tdsUpdateBo.UserMessage = Database.ExecuteScalar(dbCmd).ToString();
            }

            return tdsUpdateBo.UserMessage;
        }

        public List<TDSUpdateBo> SearchTdsDetails(TDSUpdateBo tdsUpdateBo)
        {
            List<TDSUpdateBo> list = new List<TDSUpdateBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand("Pay_SearchTDSDetails");

                Database.AddInParameter(dbCmd, DBParam.Input.MonthID, DbType.Int32, tdsUpdateBo.MonthId);
                Database.AddInParameter(dbCmd, DBParam.Input.YearID, DbType.Int32, tdsUpdateBo.Name);
                Database.AddInParameter(dbCmd, DBParam.Input.BusinessUnit, DbType.String, tdsUpdateBo.BusinessUnitIdList);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCmd))
                {
                    while (reader.Read())
                    {
                        tdsUpdateBo = new TDSUpdateBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeNo]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                            MonthId = Utility.CheckDbNull<int>(reader[DBParam.Output.MonthID]),
                            YearID = Utility.CheckDbNull<int>(reader[DBParam.Output.YearID]),
                            TDSAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDSAmount]),
                            StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                            Location = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            BaseLocationID = Utility.CheckDbNull<int>(reader[DBParam.Output.BaseLocationID]),
                            Doj = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ])
                        };
                        list.Add(tdsUpdateBo);
                    }
                }
                return list;
            }
        }

        public string ManageTdsDetails(List<TDSUpdateBo> tdsUpdateBoList)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString);

            SqlCommand cmd = new SqlCommand("Pay_ManageTDSDetails")
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue("@TDSDetails", ConvertToDataTable(tdsUpdateBoList, "TDSDetails"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        public static DataTable ConvertToDataTable<T>(List<T> items, string name)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            foreach (T item in items)
            {
                object[] values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            dataTable.Columns.Remove("entity");
            dataTable.Columns.Remove("UserMessage");
            dataTable.Columns.Remove("CreatedOn");
            dataTable.Columns.Remove("CreatedBy");
            dataTable.Columns.Remove("ModifiedOn");
            dataTable.Columns.Remove("ModifiedBy");
            dataTable.Columns.Remove("UserID");
            dataTable.Columns.Remove("UserName");
            dataTable.Columns.Remove("MenuCode");
            dataTable.Columns.Remove("ModifiedByName");
            dataTable.Columns.Remove("Operation");
            dataTable.Columns.Remove("DomainID");
            dataTable.Columns.Remove("GUID");
            dataTable.Columns.Remove("Designation");
            dataTable.Columns.Remove("Latitude");
            dataTable.Columns.Remove("longitude");
            dataTable.Columns.Remove("Location");
            dataTable.Columns.Remove("LoginID");
            dataTable.Columns.Remove("IsActive");
            dataTable.Columns.Remove("Name");
            dataTable.Columns.Remove("DOJ");
            dataTable.Columns.Remove("SNo");
            dataTable.Columns.Remove("Parameter");
            dataTable.Columns.Remove("Parameter1");
            dataTable.Columns.Remove("Parameter2");
            dataTable.Columns.Remove("Parameter3");
            dataTable.Columns.Remove("Emailhtml");
            dataTable.Columns.Remove("ToMonthId");
            dataTable.Columns.Remove("ToYearID");

            if (name == "TDSDetails")
            {
                dataTable.Columns.Remove("EmployeeName");
                dataTable.Columns.Remove("BusinessUnitIDList");
                dataTable.Columns.Remove("MonthList");
                dataTable.Columns.Remove("YearList");
                dataTable.Columns.Remove("IsDeleted");
                dataTable.Columns.Remove("BusinessUnitList");
                dataTable.Columns.Remove("StatusCode");
                dataTable.Columns.Remove("YearName");
                dataTable.Columns["ID"].SetOrdinal(0);
                dataTable.Columns["MonthID"].SetOrdinal(1);
                dataTable.Columns["YearID"].SetOrdinal(2);
                dataTable.Columns["EmployeeID"].SetOrdinal(3);
                dataTable.Columns["TDSAmount"].SetOrdinal(4);
            }
            return dataTable;
        }

        //public string ManageTDSDetails(List<TDSUpdateBo> list)
        //{
        //    var tdsUpdateBo = new TDSUpdateBo();

        //    var consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
        //    var con = new SqlConnection(consString);
        //    var cmd = new SqlCommand("Pay_ManageTDSDetails")
        //    {
        //        CommandType = CommandType.StoredProcedure,
        //        Connection = con
        //    }; con.Open();
        //    cmd.Parameters.AddWithValue("@TDSDetails", Utility.ToDataTable(list, "TDSDetails"));

        //    using (var reader = cmd.ExecuteReader())
        //    {
        //        while (reader.Read())
        //        {
        //            tdsUpdateBo.Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]);
        //            tdsUpdateBo.EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]);
        //            tdsUpdateBo.EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]);
        //            tdsUpdateBo.Name = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeNo]);
        //            tdsUpdateBo.UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]);
        //            tdsUpdateBo.MonthId = Utility.CheckDbNull<Int32>(reader[DBParam.Output.MonthID]);
        //            tdsUpdateBo.YearID = Utility.CheckDbNull<Int32>(reader[DBParam.Output.YearID]);
        //            tdsUpdateBo.TDSAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDSAmount]);
        //            tdsUpdateBo.StatusCode = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]);
        //            tdsUpdateBo.Location = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]);
        //            tdsUpdateBo.Doj = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]);
        //        }
        //    }
        //    return tdsUpdateBo;
        //}
    }
}