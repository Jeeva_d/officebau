﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;

namespace cvt.officebau.com.Services
{
    public class PayPayrollReportsRepository : BaseRepository
    {
        public List<Pay_PayrollReportsBO> Pay_Rpt_PayrollMonthly(string businessUnitList, int? departmentId, int? componentId, int financialYearId)
        {
            List<Pay_PayrollReportsBO> boList = new List<Pay_PayrollReportsBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PAY_RPT_PAYROLLMONTHLY);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, departmentId ?? 0);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.ComponentID, DbType.Int32, componentId ?? 0);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, financialYearId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO
                        {
                            Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ComponentValues]),
                            Month = Utility.CheckDbNull<string>(reader[DBParam.Output.Month])
                        };
                        boList.Add(payrollReportBo);
                    }
                }
            }

            return boList;
        }

        public DataTable Pay_Report_SearchEmployeePayroll(string businessUnitList, int? departmentId, int? componentId, int financialYearId)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_Report_SearchEmployeePayroll");

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, departmentId ?? 0);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.ComponentID, DbType.Int32, componentId ?? 0);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, financialYearId);

                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }

                return dtSalesTargetReport;
            }
        }

        public DataTable Pay_RPT_SearchPayrollByMonthly(int monthId, int yearId, string businessUnitList, int? departmentId)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_Rpt_searchpayrollbymonthly");

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, departmentId);

                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }

                return dtSalesTargetReport;
            }
        }

        public DataTable Pay_RPT_SearchPayrollDetailReport(int monthId, int yearId)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_Rpt_searchpayrollbymonthandyear");

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }

                return dtSalesTargetReport;
            }
        }
        public DataTable Pay_RPT_SearchPayrollBankDetailReport(int monthId, int yearId, int year)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_Rpt_searchpayrollBankStatement");

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }
                dtSalesTargetReport.Columns.Add("Employee Resident ID");
                dtSalesTargetReport.Columns.Add("Benificiary Narration");
                dtSalesTargetReport.Columns["Employee Resident ID"].SetOrdinal(1);
                for (int i = 0; i < dtSalesTargetReport.Rows.Count; i++)
                {
                    dtSalesTargetReport.Rows[i]["Benificiary Narration"] = "SALARY FOR " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(monthId) + " " + year;
                }

                return dtSalesTargetReport;
            }
        }

        public DataTable Pay_GetPayrollHistory(int employeeId)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_Rpt_searchindividualpayroll");

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);

                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }

                return dtSalesTargetReport;
            }
        }

        public DataTable Pay_GetEmployeePaystructureHistory(int companyPayStructureID)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_Rpt_EmployeePaystructure");

                Database.AddInParameter(dbCommand, DBParam.Input.CompanyPayStructureId, DbType.Int32, companyPayStructureID);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable employeePaystructureReport = new DataTable("EmployeePaystructureReport");
                ds.Tables.Add(employeePaystructureReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, employeePaystructureReport);
                }

                return employeePaystructureReport;
            }
        }

        public DataTable Pay_RPT_SearchComponentsByMonthly(int monthId, int yearId, string businessUnitList, int? departmentId)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_Rpt_searchpayrollcomponents");

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, departmentId);

                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }

                return dtSalesTargetReport;
            }
        }
        public DataTable Pay_UnmappedPaystructure(int? employeeId, bool isActive)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_Rpt_UnmappedEmployeePaystructure");

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, isActive);

                DataSet ds = new DataSet();
                DataTable dtpaystructReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(dtpaystructReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtpaystructReport);
                }

                return dtpaystructReport;
            }
        }

        #region Complete Payroll Report

        public DataTable Pay_EmployeeCompletePayrollRPTList(int fromMonthId, int fromYearId, int toMonthId, int toYearId)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Pay_Rpt_searchPayrollcompleteReport");

                Database.AddInParameter(dbCommand, "@FromMonthID", DbType.Int32, fromMonthId);
                Database.AddInParameter(dbCommand, "@FromYearID", DbType.Int32, fromYearId);
                Database.AddInParameter(dbCommand, "@ToMonthID", DbType.Int32, toMonthId);
                Database.AddInParameter(dbCommand, "@ToYearID", DbType.Int32, toYearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }

                return dtSalesTargetReport;
            }
        }

        #endregion Complete Payroll Report
    }
}