﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace cvt.officebau.com.Services
{
    public class SalesOrderRepository : BaseRepository
    {
        #region Search SalesOrder

        public List<IncomeBo> SearchSalesOrder(SearchParamsBo searchParamsBo)
        {
            List<IncomeBo> incomeBoList = new List<IncomeBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchSalesOrder");

                Database.AddInParameter(dbCommand, DBParam.Input.CustomerName, DbType.String, searchParamsBo.CustomerName);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, searchParamsBo.Status);
                Database.AddInParameter(dbCommand, DBParam.Input.StartDate, DbType.String, searchParamsBo.StartDate == null ? null : Convert.ToDateTime(searchParamsBo.StartDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.EndDate, DbType.String, searchParamsBo.EndDate == null ? null : Convert.ToDateTime(searchParamsBo.EndDate).ToString("MM/dd/yyyy"));
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.Notations, DbType.String, searchParamsBo.Notations);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, searchParamsBo.Amount);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBoList.Add(
                            new IncomeBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                InvoiceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.InvoiceDate]),
                                InvoiceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo]),
                                CustomerId = Utility.CheckDbNull<int>(reader[DBParam.Output.CustomerID]),
                                CustomerName = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName]),
                                Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                                Status = Utility.CheckDbNull<string>(reader[DBParam.Output.Status]),
                                CreatedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.CreatedBy])
                            });
                    }
                }
            }

            return incomeBoList;
        }

        #endregion Search SalesOrder

        public string ManageSalesOrder(IncomeBo incomeBo)
        {
            string userMessage = string.Empty;
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);

            switch (incomeBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    userMessage = CreateSalesOrder(incomeBo, con);
                    break;

                case Operations.Update:
                    userMessage = UpdateSalesOrder(incomeBo, con);
                    break;

                case Operations.Delete:
                    userMessage = DeleteSalesOrder(incomeBo, con);
                    break;
            }

            return userMessage;
        }

        private string DeleteSalesOrder(IncomeBo incomeBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand("DeleteSO") { CommandType = CommandType.StoredProcedure, Connection = con };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, incomeBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string CreateSalesOrder(IncomeBo incomeBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand("CreateSO") { CommandType = CommandType.StoredProcedure, Connection = con };
            FillInvoiceParameters(incomeBo, cmd);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string UpdateSalesOrder(IncomeBo incomeBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand("UpdateSo") { CommandType = CommandType.StoredProcedure, Connection = con };
            FillInvoiceParameters(incomeBo, cmd);
            cmd.Parameters.AddWithValue(DBParam.Input.ID, incomeBo.Id);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private void FillInvoiceParameters(IncomeBo incomeBo, SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue(DBParam.Input.InvoiceNo, ValueOrDBNullIfZero(incomeBo.InvoiceNo));
            cmd.Parameters.AddWithValue(DBParam.Input.Description, ValueOrDBNullIfZero(incomeBo.Description));
            cmd.Parameters.AddWithValue(DBParam.Input.Date, incomeBo.InvoiceDate);
            cmd.Parameters.AddWithValue(DBParam.Input.CustomerID, incomeBo.CustomerId);
            cmd.Parameters.AddWithValue(DBParam.Input.BillAddress, ValueOrDBNullIfZero(incomeBo.BillingAddress));
            cmd.Parameters.AddWithValue(DBParam.Input.CurrencyRate, incomeBo.CurrencyRate);
            cmd.Parameters.AddWithValue(DBParam.Input.INVOICEITEM, Utility.ToDataTable(incomeBo.ItemDetail, "InvoiceItem"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
        }

        public List<ItemDetailsBO> SearchSoinvoiceDetailList(string id)
        {
            List<ItemDetailsBO> itemDetailsBoList = new List<ItemDetailsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchSOINVOICEDetailList");

                Database.AddInParameter(dbCommand, "@SoID", DbType.String, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        itemDetailsBoList.Add(new ItemDetailsBO
                        {
                            ProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.ProductID]),
                            ItemRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            QTY = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            Rate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]),
                            SGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            CGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            SGSTAmount = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                         Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]) / 100 *
                                         Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            CGSTAmount = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                         Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]) / 100 *
                                         Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            Amount = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                     Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]) + Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                     Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]) / 100 *
                                     Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]) +
                                     Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) *
                                     Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]) / 100 *
                                     Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            SOItemID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            IsProduct = Utility.CheckDbNull<int>(reader[DBParam.Output.IsProduct]),
                            ProductList = masterDataRepository.GetPreRequestProduct("Sales")
                        });
                        itemDetailsBoList[itemDetailsBoList.Count - 1].ProductList.Find(a => a.Value == itemDetailsBoList[itemDetailsBoList.Count - 1].ProductID.ToString()).Selected = true;
                    }
                }

                if (itemDetailsBoList.Count == 0)
                {
                    itemDetailsBoList.Add(new ItemDetailsBO
                    {
                        ProductList = masterDataRepository.GetPreRequestProduct("Sales")
                    });
                }
            }

            return itemDetailsBoList;
        }

        #region Supporting function

        private object ValueOrDBNullIfZero(string val)
        {
            if (string.IsNullOrEmpty(val))
            {
                return DBNull.Value;
            }

            return val;
        }

        #endregion Supporting function

        #region Get Income

        public IncomeBo GetSalesOrder(int id)
        {
            IncomeBo incomeBo = new IncomeBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("GetSalesOrder");

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        incomeBo = new IncomeBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            InvoiceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.InvoiceDate]),
                            CustomerId = Utility.CheckDbNull<int>(reader[DBParam.Output.CustomerID]),
                            CustomerName = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerName].ToString()),
                            BillingAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.BillingAddress].ToString()),
                            PaymentTerms = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentTerms].ToString()),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description].ToString()),
                            InvoiceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.InvoiceNo].ToString()),
                            TotalAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TotalAmount]),
                            CurrencyRate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CurrencyRate]),
                            Currency = Utility.CheckDbNull<string>(reader[DBParam.Output.Currency]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            CompanyName = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyName]),
                            CompanyLogo = Utility.CheckDbNull<string>(reader[DBParam.Output.Logo]),
                            CompanyAddress = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyAddress]),
                            StateName = Utility.CheckDbNull<string>(reader[DBParam.Output.STATENAME]),
                            CompanyEmaild = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyEmaild]),
                            CompanyGstno = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyGSTNO]),
                            CompanyContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.CompanyContactNo]),
                            CustomerGstNo = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerGSTNo]),
                            CustomerState = Utility.CheckDbNull<string>(reader[DBParam.Output.CustomerState]),
                            ToEmail = Utility.CheckDbNull<string>(reader["ToEmail"]),
                            CcEmail = Utility.CheckDbNull<string>(reader["CCEmail"]),
                            BccEmail = Utility.CheckDbNull<string>(reader["BCCEmail"]),
                            PaymentFavour = Utility.CheckDbNull<string>(reader[DBParam.Output.PaymentFavour])
                        };
                    }
                }
            }

            incomeBo.ItemDetail = SearchSOitemlist(id);
            return incomeBo;
        }

        public List<ItemDetailsBO> SearchSOitemlist(int id)
        {
            List<ItemDetailsBO> itemDetailsBoList = new List<ItemDetailsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchSOitemlist");

                Database.AddInParameter(dbCommand, DBParam.Input.InvoiceID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        itemDetailsBoList.Add(new ItemDetailsBO
                        {
                            ProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.ProductID]),
                            ItemRemarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            QTY = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            Rate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]),
                            SGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            CGST = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            SGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTAmount]),
                            CGSTAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGSTAmount]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGSTAmount]) + Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGSTAmount]) +
                                     Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]) * Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rate]),
                            ID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            IsProduct = Utility.CheckDbNull<int>(reader[DBParam.Output.IsProduct]),
                            ProductList = masterDataRepository.GetPreRequestProduct("Sales"),
                            ProductName = Utility.CheckDbNull<string>(reader[DBParam.Output.ProductName]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.HsnCode])
                        });
                        itemDetailsBoList[itemDetailsBoList.Count - 1].ProductList.Find(a => a.Value == itemDetailsBoList[itemDetailsBoList.Count - 1].ProductID.ToString()).Selected = true;
                    }
                }

                if (itemDetailsBoList.Count == 0)
                {
                    itemDetailsBoList.Add(new ItemDetailsBO
                    {
                        ProductList = masterDataRepository.GetPreRequestProduct("Sales")
                    });
                }
            }

            return itemDetailsBoList;
        }

        #endregion Get Income
    }
}