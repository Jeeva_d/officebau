﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class InternalMeetingRepository : InternalMeetingBaseRepository
    {
        #region InternalMeeting
        public string ManageInternalMeeting(InternalMeetingBO internalMeetingBO)
        {
            string message = "";
            internalMeetingBO.DomainId = Utility.DomainId();
            internalMeetingBO.CreatedBy = Utility.UserId();
            internalMeetingBO.ModifiedBy = Utility.UserId();
            if (internalMeetingBO.Id == 0)
            {
                message = SaveInternalMeeting(internalMeetingBO);
            }
            else
            {
                message = UpdateInternalMeeting(internalMeetingBO);
            }

            return message;
        }

        public InternalMeetingBO GetInternalMeeting(int id)
        {
            var i = GetInternalMeeting_ById(id);

            InternalMeetingBO internalMeetingBO = new InternalMeetingBO()
            {
                Id = i.Id,
                Title = i.Title,
                MeetingDate = i.MeetingDate,
                MeetingTime = i.MeetingTime,
                Location = i.Location,
                OrganizerId = i.OrganizedById,
                NextMeetingDate = i.NextMeetingDate,
                MeetingTypeId = i.TypeId,
                Attendees = i.Attendees,
                Description=i.Description,
               // ModifiedByName = i.Modified_By.FullName,
                ModifiedOn = i.ModifiedOn
            };

            return internalMeetingBO;
        }

        public List<InternalMeetingBO> SearchInternalMeeting(string title, int? organizedBy, int? meetingType)
        {
            var i = InternalMeetingList().Where(a => a.Title.Contains(title) &&
                                a.TypeId == (meetingType !=null ? meetingType : a.TypeId )&&
                                a.OrganizedById == (organizedBy != null ? organizedBy : a.OrganizedById)).ToList();
            var internalMeeting = i.Select(a =>
                        new InternalMeetingBO
                        {
                            Id = a.Id,
                            Title = a.Title,
                            MeetingDate = a.MeetingDate,
                            MeetingTime = a.MeetingTime,
                            Location = a.Location,
                            OrganizerId = a.OrganizedById,
                            NextMeetingDate = a.NextMeetingDate,
                            MeetingTypeId = a.TypeId,
                            Attendees = a.Attendees,
                            Description=a.Description
                        }).ToList();

            return internalMeeting;
        }

        public List<SelectListItem> GetEmployeeList()
        {
            var employee = EmployeeList().Where(a => !a.IsDeleted && a.IsActive == false).OrderBy(a => a.FullName).ToList();
            var DropDown = new List<SelectListItem>
            {
                new SelectListItem { Value = "", Text = "--Select--"}
            };

            DropDown.AddRange(employee.Select(a => new SelectListItem
            {
                Value = a.ID.ToString(),
                Text = a.FullName

            }).ToList());

            return DropDown;
        }

        public List<SelectListItem> GetMeetingTypeList()
        {
            var MeetingType = InternalMeetingTypeList().OrderBy(a => a.Name).ToList();
            var DropDown = new List<SelectListItem>
            {
                new SelectListItem { Value = "", Text = "--Select--" }
            };

            DropDown.AddRange(MeetingType.Select(a => new SelectListItem
            {
                Value = a.Id.ToString(),
                Text = a.Name

            }).ToList());

            return DropDown;
        }
        #endregion

    }
}