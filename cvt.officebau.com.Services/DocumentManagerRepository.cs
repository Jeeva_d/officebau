﻿using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class DocumentManagerRepository : BaseRepository
    {
        public DocumentManagerBO GetDocumentManager(int id)
        {
            string filePath = "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("DOCUMENTS");
            using (FSMEntities context = new FSMEntities())
            {
                DocumentManagerBO result = new DocumentManagerBO();
                if (id != 0)
                {
                    result = (from c in context.tbl_DocumentManager
                              join e in context.tbl_EmployeeMaster on c.ModifiedBy equals e.ID
                              join fu in context.tbl_FileUpload on c.HistoryId equals fu.Id into fileUpload
                              from files in fileUpload.DefaultIfEmpty()
                              where c.Id == id
                              select new DocumentManagerBO
                              {
                                  DocumentName = c.DocumentName,
                                  DocumentCategoryId = c.DocumentCategoryId,
                                  Description = c.Description,
                                  Id = c.Id,
                                  ExpiryDate = c.ExpiryDate,
                                  CreatedBy = c.CreatedBy,
                                  CreatedOn = c.CreatedOn,
                                  Active = c.Active,
                                  ModifiedByName = e.FullName,
                                  ModifiedOn = c.ModifiedOn,
                                  HistoryID = c.HistoryId,
                                  FileName = files.OriginalFileName
                              }).FirstOrDefault();
                }
                if (result != null)
                {
                    result.Guid = Convert.ToString(context.tbl_DocumentManager.Where(b => b.Id == result.Id).Select(b => b.HistoryId).FirstOrDefault());
                    result.Path = filePath.Replace("\\", "//");
                    result.FileInformationList = FileInformation(result.Guid, "MULTI");
                }

                return result;
            }
        }

        public List<DocumentManagerBO> GetDocumentManagersList(DocumentManagerBO documentMangerBO)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<DocumentManagerBO> list = (from h in context.tbl_DocumentManager
                                                join c in context.tbl_DocumentCategory on h.DocumentCategoryId equals c.Id into doc
                                                join em in context.tbl_EmployeeMaster on h.CreatedBy equals em.ID
                                                from dc in doc.DefaultIfEmpty()
                                                where (!h.IsDeleted && (documentMangerBO.DocumentCategoryId == 0 || h.DocumentCategoryId == documentMangerBO.DocumentCategoryId)
                                                && (documentMangerBO.DocumentName == null || h.DocumentName.Contains(documentMangerBO.DocumentName)) && (h.Active.Equals(documentMangerBO.Active)))
                                                orderby h.ModifiedOn descending
                                                select new DocumentManagerBO
                                                {
                                                    Id = h.Id,
                                                    DocumentName = h.DocumentName,
                                                    DocumentCategoryId = h.DocumentCategoryId,
                                                    ExpiryDate = h.ExpiryDate,
                                                    ModifiedByName = em.FirstName + " " + em.LastName,
                                                    CreatedOn = h.CreatedOn,
                                                    Active = h.Active,
                                                    Description = h.Description,
                                                    CategoryName = dc.Name,
                                                    HistoryID = h.HistoryId

                                                }).ToList();
                return list;

            }
        }

        public List<FileInformation> FileInformation(string guidName, string listno)
        {
            string filePath = "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("DOCUMENTS");
            if (listno.ToUpper() == "SINGLE")
            {
                using (FSMEntities context = new FSMEntities())
                {
                    List<FileInformation> list = (from t in context.tbl_MultipleFileUpload
                                                  where t.ID == new Guid(guidName)
                                                  select new FileInformation
                                                  {
                                                      FileName = t.FileName,
                                                      Path = filePath.Replace("\\", "//")
                                                  }).Take(1).ToList();
                    return list;
                }
            }

            using (FSMEntities context = new FSMEntities())
            {
                List<FileInformation> list = (from t in context.tbl_MultipleFileUpload
                                              where t.ID == new Guid(guidName)
                                              select new FileInformation
                                              {
                                                  FileName = t.FileName,
                                                  Path = filePath.Replace("\\", "//")
                                              }).ToList();
                return list;
            }
        }

        public string ManageDocument(DocumentManagerBO documentMangerBO)
        {
            string message = string.Empty;
            using (FSMEntities context = new FSMEntities())
            {
                int check = 0;
                int domainId = Utility.DomainId();

                if (documentMangerBO.Id == 0)
                {
                    if (documentMangerBO.DocumentName != null)
                    {
                        check = context.tbl_DocumentManager.AsEnumerable().Count(s => s.DocumentName == documentMangerBO.DocumentName && s.DocumentCategoryId == documentMangerBO.DocumentCategoryId && !s.IsDeleted && s.DomainId == domainId);
                    }
                    if (check == 0)
                    {
                        tbl_DocumentManager documentManager = new tbl_DocumentManager
                        {
                            DocumentName = documentMangerBO.DocumentName,
                            DocumentCategoryId = documentMangerBO.DocumentCategoryId,
                            ExpiryDate = documentMangerBO.ExpiryDate,
                            Active = documentMangerBO.Active,
                            Description = documentMangerBO.Description,
                            CreatedBy = Utility.UserId(),
                            CreatedOn = DateTime.Now,
                            ModifiedOn = DateTime.Now,
                            ModifiedBy = Utility.UserId(),
                            DomainId = Utility.DomainId(),
                            HistoryId = documentMangerBO.HistoryID
                        };
                        context.tbl_DocumentManager.Add(documentManager);
                        context.SaveChanges();

                        if (documentMangerBO.HistoryID != null)
                        {
                            tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == documentMangerBO.HistoryID);
                            if (fileUpload != null)
                            {
                                fileUpload.ReferenceTable = "tbl_DocumentManager";
                                context.Entry(fileUpload).State = EntityState.Modified;
                            }
                            context.SaveChanges();
                        }

                        return message = Notification.Inserted + "/" + documentMangerBO.HistoryID;
                    }
                    else
                    {
                        return message = Notification.AlreadyExists;
                    }
                }
                else
                {
                    if (documentMangerBO.IsDeleted)
                    {
                        tbl_DocumentManager documentManager = context.tbl_DocumentManager.FirstOrDefault(b => b.Id == documentMangerBO.Id);
                        if (documentManager != null)
                        {
                            documentManager.IsDeleted = documentMangerBO.IsDeleted;
                            documentManager.ModifiedOn = DateTime.Now;
                            documentManager.ModifiedBy = Utility.UserId();
                            context.Entry(documentManager).State = EntityState.Modified;
                        }

                        context.SaveChanges();
                        documentMangerBO.Id = documentMangerBO.Id;
                        return message = Notification.Deleted;
                    }
                    else
                    {
                        if (documentMangerBO.DocumentName != null)
                        {
                            check = context.tbl_DocumentManager.AsEnumerable().Count(s => s.DocumentName == documentMangerBO.DocumentName && s.DocumentCategoryId == documentMangerBO.DocumentCategoryId && s.Id != documentMangerBO.Id && !s.IsDeleted && s.DomainId == domainId);
                        }

                        if (check == 0)
                        {

                            tbl_DocumentManager documentManager = context.tbl_DocumentManager.FirstOrDefault(b => b.Id == documentMangerBO.Id);
                            if (documentManager != null)
                            {
                                documentManager.DocumentName = documentMangerBO.DocumentName;
                                documentManager.Id = documentMangerBO.Id;
                                documentManager.DocumentCategoryId = documentMangerBO.DocumentCategoryId;
                                documentManager.ExpiryDate = documentMangerBO.ExpiryDate;
                                documentManager.Active = documentMangerBO.Active;
                                documentManager.Description = documentMangerBO.Description;
                                documentManager.CreatedBy = Utility.UserId();
                                documentManager.CreatedOn = DateTime.Now;
                                documentManager.IsDeleted = documentMangerBO.IsDeleted;
                                documentManager.ModifiedOn = DateTime.Now;
                                documentManager.ModifiedBy = Utility.UserId();
                                documentManager.HistoryId = documentMangerBO.HistoryID;
                                context.Entry(documentManager).State = EntityState.Modified;
                            };

                            context.SaveChanges();
                            documentMangerBO.Id = documentMangerBO.Id;

                            if (documentMangerBO.HistoryID != null)
                            {
                                tbl_FileUpload fileUpload = context.tbl_FileUpload.FirstOrDefault(b => b.Id == documentMangerBO.HistoryID);
                                if (fileUpload != null)
                                {
                                    fileUpload.ReferenceTable = "tbl_DocumentManager";
                                    context.Entry(fileUpload).State = EntityState.Modified;
                                }

                                context.SaveChanges();
                            }
                            return message = Notification.Updated + "/";
                        }
                        else
                        {
                            return message = Notification.AlreadyExists;
                        }
                    }
                }
            }
        }

        public List<DocumentManagerBO> Searchfilepreview(string guidName, string description)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<DocumentManagerBO> list = (from t in context.tbl_MultipleFileUpload
                                                where t.ID == new Guid(guidName)
                                                select new DocumentManagerBO
                                                {
                                                    Id = t.RowID,
                                                    FileName = t.OriginalFileName,
                                                    Description = description
                                                }).ToList();
                return list;
            }
        }

        public List<DocumentManagerBO> GetDocumentViewer()
        {
            List<DocumentManagerBO> list = new List<DocumentManagerBO>();
            using (FSMEntities context = new FSMEntities())
            {
                list = (from e in context.tbl_DocumentCategory.AsEnumerable()
                        where !e.IsDeleted
                        orderby e.Name
                        select new DocumentManagerBO
                        {
                            CategoryId = Convert.ToString(e.Id) + "|" + e.Name,
                            Parent = "#",
                            Document = e.Name
                        }).ToList();
            }
            return list;
        }

        public List<DocumentManagerBO> DocumentViewerList(int id)
        {
            List<DocumentManagerBO> list = new List<DocumentManagerBO>();
            using (FSMEntities context = new FSMEntities())
            {
                list = (from e in context.tbl_DocumentManager
                        join a in context.tbl_EmployeeMaster on e.CreatedBy equals a.ID
                        join f in context.tbl_FileUpload on e.HistoryId equals f.Id
                        where e.DocumentCategoryId == id && (e.Active == false) && !e.IsDeleted
                        orderby e.CreatedOn descending
                        select new DocumentManagerBO
                        {
                            Document = e.DocumentName,
                            Extension = f.FileName,
                            UserName = a.FullName,
                            CreatedOn = e.CreatedOn,
                            ExpiryDate = e.ExpiryDate,
                            HistoryID = e.HistoryId
                        }).ToList();
            }
            return list;
        }
    }
}
