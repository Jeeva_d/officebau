﻿using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class AccountsDashboardRepository
    {
        private readonly FSMEntities datacontext;
        public AccountsDashboardRepository()
        {
            datacontext = new FSMEntities();
        }

        public List<AccountsDashboardBO> SearchShareDetailsChart()
        {
            var vendorResult = GetVendorResult();
            var customerResult = GetCustomerResult();

            var customer_vw = GetViewCustomer().Select(z => new
            {
                z.Customer,
                z.Amount,
                z.Received,
                z.CustomerID
            });

            var vendor_vw = GetViewVendor().Select(z => new
            {
                z.NAME,
                z.Amount,
                z.PaidAmount,
                z.VendorID,
                z.Ledger

            });

            var vendor = (from z in vendor_vw
                          join n in vendorResult on z.VendorID equals n.VendorId
                          select new
                          {
                              z.NAME,
                              z.Amount,
                              z.PaidAmount,
                              z.VendorID,
                              n.tbl_EmployeeMaster.FirstName,
                              n.EmployeeId,
                              n.Percentage,
                              Type = "Expense",
                              z.Ledger
                          }).ToList();

            var customer = (from z in customer_vw
                            join n in customerResult on z.CustomerID equals n.CustomerId
                            select new
                            {
                                z.Customer,
                                z.Amount,
                                z.Received,
                                z.CustomerID,
                                n.tbl_EmployeeMaster.FirstName,
                                n.EmployeeId,
                                n.Percentage,
                                Type = "Income"
                            }).ToList();

            List<AccountsDashboardBO> list = new List<AccountsDashboardBO>();

            foreach (var n in customer.GroupBy(a => a.EmployeeId))
            {
                AccountsDashboardBO accountsDashboardBO = new AccountsDashboardBO
                {
                    EmployeeId = n.First().EmployeeId,
                    EmployeeName = n.First().FirstName,
                    RevenueBooked = 0,
                    RevenueProcessed = 0,
                    Type = n.First().Type,

                };
                foreach (var r in n)
                {
                    accountsDashboardBO.RevenueBooked += ((r.Amount) * (r.Percentage ?? 0)) / 100;
                    accountsDashboardBO.RevenueProcessed += ((r.Received ?? 0) * (r.Percentage ?? 0)) / 100;
                }
                list.Add(accountsDashboardBO);
            }

            foreach (var n in vendor.GroupBy(a => a.EmployeeId))
            {
                AccountsDashboardBO accountsDashboardBO = new AccountsDashboardBO
                {
                    EmployeeId = n.First().EmployeeId,
                    EmployeeName = n.First().FirstName,
                    ExpenseBooked = 0,
                    ExpenseProcessed = 0,
                    ExpenseIncentiveBooked = 0,
                    ExpenseIncentiveProcessed = 0,
                    Type = n.First().Type,
                };
                foreach (var r in n.ToList().GroupBy(a => a.Ledger))
                {
                    foreach (var s in r)
                    {

                        if (r.Key.ToUpper().Contains("SHARE"))
                        {
                            accountsDashboardBO.ExpenseBooked += ((s.Amount ?? 0) * (s.Percentage ?? 0)) / 100;
                            accountsDashboardBO.ExpenseProcessed += ((s.PaidAmount ?? 0) * (s.Percentage ?? 0)) / 100;
                        }
                        else
                        {
                            accountsDashboardBO.ExpenseIncentiveBooked += ((s.Amount ?? 0) * (s.Percentage ?? 0)) / 100;
                            accountsDashboardBO.ExpenseIncentiveProcessed += ((s.PaidAmount ?? 0) * (s.Percentage ?? 0)) / 100;
                        }
                    }
                }
                list.Add(accountsDashboardBO);
            }


            var result = list.GroupBy(a => a.EmployeeId).Select(n => new AccountsDashboardBO
            {
                EmployeeId = n.First().EmployeeId,
                EmployeeName = n.First().EmployeeName,
                ExpenseBooked = n.Sum(a => a.ExpenseBooked),
                ExpenseProcessed = n.Sum(a => a.ExpenseProcessed),
                RevenueBooked = Convert.ToDecimal(Utility.PrecisionForDecimal(n.Sum(a => a.RevenueBooked), 0)),
                RevenueProcessed = Convert.ToDecimal(Utility.PrecisionForDecimal(n.Sum(a =>a.RevenueProcessed), 0)),
                ExpenseIncentiveBooked = Convert.ToDecimal(Utility.PrecisionForDecimal(n.Sum(a => a.ExpenseIncentiveBooked), 0)),
                ExpenseIncentiveProcessed = Convert.ToDecimal(Utility.PrecisionForDecimal(n.Sum(a => a.ExpenseIncentiveProcessed), 0))
            }).ToList();

            return result;
        }

        public IQueryable<VW_Expense> GetViewVendor()
        {
            var vendorResult = GetVendorResult();

            return datacontext.VW_Expense.Where(a => vendorResult.Select(v => v.VendorId).Contains(a.VendorID) && a.IsDeleted == false);
        }

        public IQueryable<tbl_ShareMapping> GetVendorResult()
        {
            return datacontext.tbl_ShareMapping.Where(a => !a.IsDeleted && !a.IsCustomer);
        }

        public IQueryable<VW_Income> GetViewCustomer()
        {
            var customerResult = GetCustomerResult();

            return datacontext.VW_Income.Where(a => customerResult.Select(v => v.CustomerId).Contains(a.CustomerID) && a.IsDeleted == false);
        }

        public IQueryable<tbl_ShareMapping> GetCustomerResult()
        {
            return datacontext.tbl_ShareMapping.Where(a => !a.IsDeleted && a.IsCustomer);
        }
    }
}
