﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class OrganisationChartRepository : BaseRepository
    {
        #region Organisation Chart

        public List<BaseBo> OrganisationChartDesignationWise()
        {
            List<BaseBo> boList = new List<BaseBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.ORGANISATIONCHART_DESIGNATION_WISE);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BaseBo baseBo = new BaseBo
                        {
                            UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName].ToString()),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName].ToString())
                        };
                        boList.Add(baseBo);
                    }
                }
            }

            return boList;
        }

        public List<OrganisationChartBo> OrganisationChartReportingTo(bool? isActive)
        {
            List<OrganisationChartBo> organisationChartBoList = new List<OrganisationChartBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.ORGANISATIONCHART_REPORTINGTO);

                Database.AddInParameter(dbCommand, DBParam.Input.IsActive, DbType.Boolean, isActive);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        organisationChartBoList.Add(new OrganisationChartBo
                        {
                            NodeID = Utility.CheckDbNull<int>(reader[DBParam.Output.NodeID]),
                            ParentID = Utility.CheckDbNull<int>(reader[DBParam.Output.ParentID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName].ToString()),
                            Designation = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName].ToString()),
                            EmailID = Utility.CheckDbNull<string>(reader[DBParam.Output.EmailID].ToString()),
                            ContactNo = Utility.CheckDbNull<string>(reader[DBParam.Output.ContactNo].ToString()),
                            ProfileImages = Utility.CheckDbNull<string>(reader[DBParam.Output.ProfileImages].ToString()),
                            Gender = Utility.CheckDbNull<string>(reader[DBParam.Output.Gender].ToString())
                        });

                        if (!string.IsNullOrWhiteSpace(organisationChartBoList[organisationChartBoList.Count - 1].ProfileImages))
                        {
                            organisationChartBoList[organisationChartBoList.Count - 1].ProfileImages = "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("PROFILE") + organisationChartBoList[organisationChartBoList.Count - 1].ProfileImages;
                        }
                        else
                        {
                            if ((organisationChartBoList[organisationChartBoList.Count - 1].Gender ?? "").ToUpper() == "FEMALE")
                            {
                                organisationChartBoList[organisationChartBoList.Count - 1].ProfileImages = "..\\Images\\DefaultProfile_female.png";
                            }
                            else
                            {
                                organisationChartBoList[organisationChartBoList.Count - 1].ProfileImages = "..\\Images\\DefaultProfile.png";
                            }
                        }
                    }
                }
            }

            return organisationChartBoList;
        }

        #endregion Organisation Chart
    }
}