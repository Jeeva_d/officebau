﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Services
{
    public class FandFSettlementRepository : BaseRepository
    {
        #region Request

        public List<FandFBo> SearchFandFSettlementRequest(FandFBo fandFBo)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List<FandFBo> requestedList = (from l in context.tbl_FandF
                                               join emp in context.tbl_EmployeeMaster on l.EmployeeID equals emp.ID
                                               join s in context.tbl_Status on l.StatusID equals s.ID
                                               join desi in context.tbl_Designation on emp.DesignationID equals desi.ID
                                               join h in context.tbl_EmployeeMaster on l.HRApproverId equals h.ID into hra
                                               from hr in hra.DefaultIfEmpty()
                                               where (fandFBo.EmployeeId == 0 || l.EmployeeID == fandFBo.EmployeeId) && !l.IsDeleted
                                                        && (fandFBo.StatusId == 0 || (l.StatusID == fandFBo.StatusId))
                                                       && l.DomainID == domainId
                                               orderby l.ModifiedOn descending
                                               select new FandFBo
                                               {
                                                   Id = l.ID,
                                                   EmployeeId = l.EmployeeID,
                                                   EmployeeName = (hr.EmpCodePattern ?? "") + (emp.Code ?? "") + " - " + emp.FullName,
                                                   LastWorkingDate = l.LastWorkingDate,
                                                   YearsOfService = l.YearsOfService,
                                                   NetAmountPayable = l.NetAmountPayable,
                                                   HrApproverRemarks = l.HRRemarks,
                                                   Designation = desi.Name,
                                                   HRApprover = (hr.EmpCodePattern ?? "") + (hr.Code ?? "") + " - " + hr.FullName,
                                                   Status = s.Code
                                               }).ToList();
                return requestedList;
            }
        }

        public FandFBo GetFandFSettlementRequest(int id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                FandFBo fandFBo = (from l in context.tbl_FandF
                                   join s in context.tbl_Status on l.StatusID equals s.ID
                                   join sa in context.tbl_Status on l.HRStatusID equals sa.ID
                                   join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                   join em in context.tbl_EmployeeMaster on l.ModifiedBy equals em.ID
                                   join h in context.tbl_EmployeeMaster on l.HRApproverId equals h.ID into hra
                                   from hr in hra.DefaultIfEmpty()
                                   join c in context.tbl_EmployeeMaster on e.ReportingToID equals c.ID into report
                                   from rep in report.DefaultIfEmpty()
                                   join c in context.tbl_EmployeeStatutoryDetails on l.EmployeeID equals c.EmployeeID into cont
                                   from sta in cont.DefaultIfEmpty()
                                   join des in context.tbl_Designation on e.DesignationID equals des.ID
                                   where l.ID == id && !(l.IsDeleted)
                                   select new FandFBo
                                   {
                                       Id = l.ID,
                                       EmployeeId = l.EmployeeID,
                                       EmployeeName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                       DateOfJoining = e.DOJ,
                                       LastWorkingDate = l.LastWorkingDate,
                                       TotalDaysInMonth = l.TotalDaysInMonth,
                                       DaysPaid = l.DaysPaid,
                                       Remarks = l.Remarks,
                                       ReasonForLeaving = l.ReasonForLeaving,
                                       NoticeDays = l.NoticeDays,
                                       LeaveEncashedDays = l.LeaveEncashedDays,
                                       LOPDays = l.LOPDays,
                                       StatusId = l.StatusID,
                                       Status = s.Code,
                                       PFNumber = sta.PFNo,
                                       UANumber = sta.UANNo,
                                       Designation = des.Name,
                                       YearsOfService = l.YearsOfService,
                                       HRApprover = (hr.EmpCodePattern ?? "") + (hr.Code ?? "") + " - " + hr.FirstName,
                                       HRStatus = sa.Code,
                                       HrApproverRemarks = l.HRRemarks,
                                       HRApproverId = l.HRApproverId,
                                       HrApprovedDate = l.HRApprovedDate,
                                       ModifiedByName = em.FullName,
                                       ModifiedOn = l.ModifiedOn,
                                   }).FirstOrDefault();
                return fandFBo;
            }
        }

        public string ManageFandFSettlementRequest(FandFBo fandFBo)
        {
            string outputMessage;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_FANDFSETTLEMENT);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, fandFBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, fandFBo.EmployeeId);
                Database.AddInParameter(dbCommand, "@LastWorkingDate", DbType.Date, fandFBo.LastWorkingDate);
                Database.AddInParameter(dbCommand, "@YearsOfService", DbType.String, fandFBo.YearsOfService);
                Database.AddInParameter(dbCommand, "@TotalDaysInMonth", DbType.Int32, fandFBo.TotalDaysInMonth);
                Database.AddInParameter(dbCommand, "@DaysPaid", DbType.Int32, fandFBo.DaysPaid);
                Database.AddInParameter(dbCommand, "@ReasonForLeaving", DbType.String, fandFBo.ReasonForLeaving);
                Database.AddInParameter(dbCommand, "@NoticeDays", DbType.Int32, fandFBo.NoticeDays ?? 0);
                Database.AddInParameter(dbCommand, "@LeaveEncashedDays", DbType.Int32, fandFBo.LeaveEncashedDays ?? 0);
                Database.AddInParameter(dbCommand, DBParam.Input.LopDays, DbType.Int32, fandFBo.LOPDays ?? 0);
                Database.AddInParameter(dbCommand, "@NetAmountPayable", DbType.Decimal, fandFBo.NetAmountPayable);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, fandFBo.Remarks);
                Database.AddInParameter(dbCommand, DBParam.Input.HRApproverID, DbType.Int32, fandFBo.HRApproverId);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, fandFBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                string output = Database.ExecuteScalar(dbCommand).ToString();
                outputMessage = output.Split('/')[0];
                if (Convert.ToInt32(output.Split('/')[1]) > 0)
                {
                    ManageFandFComponentDetails(Convert.ToInt32(output.Split('/')[1]), fandFBo);
                }
            }
            return outputMessage;
        }

        private void ManageFandFComponentDetails(int fandFId, FandFBo fandFBo)
        {
            if (fandFBo.FandFComponentDetailsBoList != null && fandFBo.FandFComponentDetailsBoList.Count > 0)
            {
                string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                SqlConnection con = new SqlConnection(consString);

                SqlCommand cmd = new SqlCommand(Constants.MANAGE_FANDFCOMPONENTDETAILS)
                {
                    CommandType = CommandType.StoredProcedure,
                    Connection = con
                };
                cmd.Parameters.AddWithValue(DBParam.Input.ID, fandFId);
                cmd.Parameters.AddWithValue("@TotalDaysInMonth", fandFBo.TotalDaysInMonth);
                cmd.Parameters.AddWithValue("@LeaveEncashedDays", fandFBo.LeaveEncashedDays);
                cmd.Parameters.AddWithValue("@FandFComponentDetailsList", Utility.ToDataTable(fandFBo.FandFComponentDetailsBoList, "FandFComponentDetailsBoList"));
                cmd.Parameters.AddWithValue(DBParam.Input.UserID, Utility.UserId());
                cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public FandFBo GetEmployeeDetails(int employeeId)
        {
            using (FSMEntities context = new FSMEntities())
            {
                if (employeeId != 0)
                {
                    var result = (from e in context.tbl_EmployeeMaster
                                  from d in context.tbl_Designation
                                   .Where(d => d.ID == (int)e.DesignationID).DefaultIfEmpty()
                                  from s in context.tbl_EmployeeStatutoryDetails
                                         .Where(s => !s.IsDeleted && !(s.IsActive) && s.EmployeeID == e.ID).DefaultIfEmpty()
                                  where !(e.IsActive ?? false)
                                        && e.ID == employeeId
                                        && !e.IsDeleted
                                  select new FandFBo
                                  {
                                      DateOfJoining = e.DOJ,
                                      //UserMessage = e.DOJ.ToString("dd MMM yyyy"),
                                      PFNumber = s.PFNo,
                                      UANumber = s.UANNo,
                                      Designation = d.Name
                                  }).FirstOrDefault();

                    if (result != null)
                    {
                        result.UserMessage = Convert.ToDateTime(result.DateOfJoining).ToString("dd MMM yyyy");
                        //DaysPaid = 
                    }
                    return result;
                }
                else
                {
                    return null;
                }
            }
        }

        public FandFBo GetFandFDaysPaid(int employeeId, DateTime dateOfJoining, DateTime lastWorkingDate)
        {
            FandFBo fandfBo = new FandFBo();
            using (FSMEntities context = new FSMEntities())
            {
                var result = context.GetFandFDaysPaid(employeeId, dateOfJoining, lastWorkingDate, Utility.UserId(), Utility.DomainId()).FirstOrDefault();

                if (result != null)
                {
                    fandfBo.YearsOfService = result.YearsOfService;
                    fandfBo.DaysPaid = result.DaysPaid;
                    fandfBo.TotalDaysInMonth = result.TotalDaysInMonth;
                    fandfBo.LOPDays = (result.TotalDaysInMonth - result.DaysPaid);
                }

                return fandfBo;
            }
        }

        public List<FandFComponentDetailsBo> GetFandFComponentDetailsList(int id, int employeeId, int TotalDaysInMonth, int DaysPaid, int LeaveEncashedDays, List<FandFComponentDetailsBo> FandFComponentDetailsBoList)
        {
            List<FandFComponentDetailsBo> fandfComponentDetailsBoList = new List<FandFComponentDetailsBo>();

            //using (Database.CreateConnection())
            {
                string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                SqlConnection con = new SqlConnection(consString);

                SqlCommand cmd = new SqlCommand(Constants.GET_FANDFCOMPONENTDETAILSLIST)
                {
                    CommandType = CommandType.StoredProcedure,
                    Connection = con
                };
                cmd.Parameters.AddWithValue(DBParam.Input.ID, id);
                cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, employeeId);
                cmd.Parameters.AddWithValue("@TotalDaysInMonth", TotalDaysInMonth);
                cmd.Parameters.AddWithValue("@DaysPaid", DaysPaid);
                cmd.Parameters.AddWithValue("@LeaveEncashedDays", LeaveEncashedDays);
                cmd.Parameters.AddWithValue("@FandFComponentDetailsList", Utility.ToDataTable(FandFComponentDetailsBoList, "FandFComponentDetailsBoList"));
                cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
                cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                con.Open();

                using (IDataReader reader = Database.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        fandfComponentDetailsBoList.Add(new FandFComponentDetailsBo()
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            FandFId = Utility.CheckDbNull<int>(reader["FandFId"]),
                            Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            IsEditableAmount = Utility.CheckDbNull<bool>(reader["IsEditableAmount"]),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks]),
                            IsEditableRemarks = Utility.CheckDbNull<bool>(reader["IsEditableRemarks"]),
                            IsDeleted = Utility.CheckDbNull<bool>(reader["IsDeleted"]),
                            Ordinal = Utility.CheckDbNull<int>(reader[DBParam.Output.Ordinal]),
                        });
                    }
                }
                con.Close();
            }
            return fandfComponentDetailsBoList;
        }

        #endregion Request

        #region HR Approve

        public List<FandFBo> SearchFandFHRApprove(FandFBo fandFBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<FandFBo> requestedList = (from l in context.tbl_FandF
                                               join e in context.tbl_EmployeeMaster on l.EmployeeID equals e.ID
                                               join r in context.tbl_EmployeeMaster on l.ModifiedBy equals r.ID
                                               join h in context.tbl_EmployeeMaster on l.HRApproverId equals h.ID into hra
                                               from hr in hra.DefaultIfEmpty()
                                               join s in context.tbl_Status on l.HRStatusID equals s.ID
                                               where !(l.IsDeleted)
                                                          && (fandFBo.EmployeeId == 0 || l.EmployeeID == fandFBo.EmployeeId)
                                                            && (fandFBo.StatusId == 0 || l.HRStatusID == fandFBo.StatusId)
                                                            && (l.DomainID == fandFBo.DomainId)
                                                             && (l.HRApproverId == fandFBo.UserId)
                                               orderby l.ModifiedOn descending
                                               select new FandFBo
                                               {
                                                   Id = l.ID,
                                                   EmployeeId = l.EmployeeID,
                                                   EmployeeName = (e.EmpCodePattern ?? "") + (e.Code ?? "") + " - " + e.FullName,
                                                   NetAmountPayable = l.NetAmountPayable,
                                                   LastWorkingDate = l.LastWorkingDate,
                                                   HrApproverRemarks = l.HRRemarks,
                                                   HRApproverId = l.HRApproverId,
                                                   StatusId = l.StatusID,
                                                   HRStatus = s.Code,
                                                   HRApprover = (hr.EmpCodePattern ?? "") + (hr.Code ?? "") + " - " + hr.FullName,
                                                   ModifiedOn = l.ModifiedOn,
                                                   HRName = (r.EmpCodePattern ?? "") + (r.Code ?? "") + " - " + r.FullName
                                               }).ToList();
                return requestedList;
            }
        }

        public string FandFHRApprove(FandFBo fandFBo)
        {
            string outputMessage;
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            using (FSMEntities context = new FSMEntities())
            {
                if (context.tbl_FandF.Where(l => !(l.IsDeleted) && l.ID == fandFBo.Id
                                                                      && l.HRStatusID == context.tbl_Status.FirstOrDefault(s => !s.IsDeleted && s.Type == "EOS" && s.Code == "Pending").ID).Count() != 0)
                {
                    tbl_FandF approverAction = context.tbl_FandF.FirstOrDefault(l => l.ID == fandFBo.Id && !(l.IsDeleted));
                    if (approverAction != null)
                    {
                        approverAction.HRRemarks = fandFBo.HrApproverRemarks;
                        approverAction.HRApprovedDate = DateTime.Now;
                        approverAction.StatusID = masterDataRepository.GetStatusId("EOS", fandFBo.HRStatus);
                        approverAction.HRStatusID = masterDataRepository.GetStatusId("EOS", fandFBo.HRStatus);
                        approverAction.ModifiedBy = fandFBo.HRApproverId;
                        approverAction.ModifiedOn = DateTime.Now;
                        approverAction.DomainID = fandFBo.DomainId;

                        context.Entry(approverAction).State = EntityState.Modified;
                    }
                    context.SaveChanges();

                    outputMessage = fandFBo.HRStatus + " Successfully.";
                }
                else
                {
                    outputMessage = BOConstants.cannot_Approve_Reject;
                }
            }
            return outputMessage;
        }

        #endregion HR Approve
    }
}