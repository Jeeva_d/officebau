﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class NotificationRepository
    {
        public static List<CustomBo> GetAnnouncements()
        {
            List<CustomBo> lstAnnouncements;
            string dbConnectionSettings = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

            using (SqlConnection dbConnection = new SqlConnection(dbConnectionSettings))
            {
                dbConnection.Open();
                string sqlCommandText =
                    @"Update tbl_Notifications set IsViewed=1 where (ReceiverIsViewed = 1 and Datediff(DAY, CreatedOn, Getdate()) >= 1) and (SenderIsViewed = 1 and Datediff(DAY,  CreatedOn,Getdate()) >= 1);SELECT FromID,TOID,n.ID,Message ,FromName,eto.firstName + '' + Isnull(eto.lastName, '')+ '-' + eto.Code  AS TOName,OldMessage,ImagePath, ffrom.FileName AS FromPhoto,  fto.FileName AS ToPhoto FROM [dbo].[tbl_Notifications] n LEFT JOIN tbl_EmployeeMaster eto ON eto.ID = n.ToID LEFT JOIN tbl_EmployeeMaster efrom ON efrom.ID = n.FromID LEFT JOIN tbl_FileUpload ffrom ON ffrom.Id = efrom.FileID LEFT JOIN tbl_FileUpload fto ON fto.Id = eto.FileID WHERE ISViewed<>1 and ( (FromID =" +
                    Utility.UserId() + " AND ISNULL(OldMessage,'') <> '') OR TOID=" + Utility.UserId() + ") order by n.CreatedOn DESC";

                using (SqlCommand sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                {
                    AddSqlDependency(sqlCommand);

                    if (dbConnection.State == ConnectionState.Closed)
                    {
                        dbConnection.Open();
                    }

                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    lstAnnouncements = GetAnnouncements(reader);
                }
            }

            return lstAnnouncements;
        }

        public static CustomBo GetBuildNotification()
        {
            CustomBo lstAnnouncements;
            string dbConnectionSettings = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

            using (SqlConnection dbConnection = new SqlConnection(dbConnectionSettings))
            {
                dbConnection.Open();
                string sqlCommandText = @"SELECT ID,BrodcastMessage FROM [dbo].[DeploymentActivities]  WHERE ((" + Utility.DomainId() + ") in (SELECT Item  FROM dbo.Splitstring(DomainID, ',') WHERE Isnull(Item, '') <> ''))  AND IsActive = 0 AND IsDeleted = 0 AND (Cast(FromDate AS DATETIME) <= Cast (Getdate() AS DATETIME) AND Cast (ToDate AS DATETIME) >= Cast (Getdate() AS DATETIME)) AND (Cast (ToDate AS DATETIME) >= Cast (FromDate AS DATETIME))";

                using (SqlCommand sqlCommand = new SqlCommand(sqlCommandText, dbConnection))
                {
                    sqlCommand.Notification = null;
                    SqlDependency dependency = new SqlDependency(sqlCommand);

                    dependency.OnChange += (sender, sqlNotificationEvents) =>
                    {
                        if (sqlNotificationEvents.Type == SqlNotificationType.Change)
                        {
                            StudentHub.BuildActivity(sqlNotificationEvents.Info.ToString());
                        }
                    };
                    if (dbConnection.State == ConnectionState.Closed)
                    {
                        dbConnection.Open();
                    }

                    SqlDataReader reader = sqlCommand.ExecuteReader();
                    lstAnnouncements = new CustomBo();

                    while (reader.Read())
                    {
                        lstAnnouncements.Id = Utility.CheckDbNull<int>(reader["ID"]);
                        lstAnnouncements.UserMessage = Utility.CheckDbNull<string>(reader["BrodcastMessage"]);
                    }

                    //lstAnnouncements = GetAnnouncements(reader);
                }
            }

            return lstAnnouncements;
        }

        /// <summary>
        ///     Adds SQLDependency for change notification and passes the information to Student Hub for broadcasting
        /// </summary>
        /// <param name="sqlCommand"></param>
        private static void AddSqlDependency(SqlCommand sqlCommand)
        {
            sqlCommand.Notification = null;
            SqlDependency dependency = new SqlDependency(sqlCommand);

            dependency.OnChange += (sender, sqlNotificationEvents) =>
            {
                if (sqlNotificationEvents.Type == SqlNotificationType.Change)
                {
                    StudentHub.SendUptodateInformation(sqlNotificationEvents.Info.ToString());
                }
            };
        }

        /// <summary>
        ///     Fills the Student Records
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static List<CustomBo> GetAnnouncements(SqlDataReader reader)
        {
            List<CustomBo> lstAnnouncements = new List<CustomBo>();
            DataTable dt = new DataTable();

            dt.Load(reader);
            dt.AsEnumerable().ToList().ForEach
            (
                i => lstAnnouncements.Add(new CustomBo
                {
                    Id = (int)i[DBParam.Output.ID],
                    UserMessage = (string)i[DBParam.Output.Message], //to get the Message from DB
                    UserName = (string)i[DBParam.Output.FromName], //to get the Sender Name from DB
                    EmployeeId = (int)i[DBParam.Output.FromID], //to get the FromID from DB
                    Location = i[DBParam.Output.OldMessage].ToString(),
                    ModifiedByName = string.IsNullOrWhiteSpace(Utility.CheckDbNull<string>(i["FromPhoto"])) ? "https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" : "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("PROFILE") + Utility.CheckDbNull<string>(i["FromPhoto"]), //to get the Sender Photo from DB
                    UserId = Utility.UserId(),
                    LoginId = (int)i[DBParam.Output.ToID], //to get the ToID from DB
                    Name = (string)i[DBParam.Output.ToName], //to get the Receiver Name from DB
                    Operation = string.IsNullOrWhiteSpace(Utility.CheckDbNull<string>(i["ToPhoto"])) ? "https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" : "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("PROFILE") + Utility.CheckDbNull<string>(i["ToPhoto"]) //to get the Receiver Photo from DB
                })
            );
            return lstAnnouncements;
        }
    }
}