﻿using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;

namespace cvt.officebau.com.Services
{

    public class EmployeeInvoiceRepository
    {
        private readonly FSMEntities datacontext;


        public EmployeeInvoiceRepository()
        {
            datacontext = new FSMEntities();
        }
        public List<EmployeeInvoice> SearchEmployeeBillingInvoice(int? monthId, int? year, string customerName)
        {
            var result = (from c in datacontext.tbl_MonthlyCustomerBilling
                          join a in datacontext.tbl_Month on c.MonthId equals a.ID
                          join b in datacontext.tblCustomers on c.CustomerId equals b.ID
                          join e in datacontext.tblCurrencies on b.CurrencyID equals e.ID
                          where c.MonthId == (monthId == null || monthId == 0 ? c.MonthId : monthId) &&
                          c.Year == (year == null || year == 0 ? c.Year : year)
                          && b.Name.ToUpper().Contains(string.IsNullOrEmpty(customerName) ? b.Name.ToUpper() : customerName.ToUpper())
                          && !c.Isdeleted
                          orderby c.MonthId descending, c.Year descending
                          select new EmployeeInvoice
                          {
                              Id = c.Id,
                              MonthName = a.Code,
                              CurrencySymbol = e.CurrencyCode,
                              CurrencyCode = e.Code,
                              Year = c.Year,
                              CustomerName = b.Name,
                              CurrencyRate = c.CurrencyRate,
                              Total = c.tbl_MonthlyCustomerBillingDetails.Where(a => a.MonthlyID == c.Id && !a.Isdeleted).Sum(a => a.Total),
                              Hours = datacontext.tbl_MonthlyCustomerBillingDetails.Where(l => l.MonthlyID == c.Id).Sum(h => h.HoursLogged),
                              Resources = datacontext.tbl_MonthlyCustomerBillingDetails.Where(n => !n.Isdeleted && n.MonthlyID == c.Id).Select(c => new EmployeeInvoiceDetails { Selected = true }).Count(),
                              EmployeeIds = datacontext.tbl_MonthlyCustomerBillingDetails.Where(n => !n.Isdeleted && n.MonthlyID == c.Id).Select(c => c.EmployeeID).AsEnumerable(),
                          }).ToList();

            foreach (var n in result)
            {
                n.Remuneration = 0;
                foreach (var e in n.EmployeeIds)
                {
                    n.Remuneration += (GetCTC(e).Item1 ?? 0);
                }
            }
            return result;
        }

        public EmployeeInvoice GetBillingEmployeeList(int monthId, int year, int customerId)
        {
            var employeeId = UnmappedEmployeeIds(monthId, year, customerId) ?? new List<int>();
            var result = (from a in datacontext.tbl_EmployeeBilling
                          where a.IsActive && !a.Isdeleted && a.CustomerId == customerId && (!employeeId.Contains(a.EmployeeId))
                          orderby a.EmployeeId
                          select a).ToList();
            var n = new List<EmployeeInvoiceDetails>();
            foreach (var a in result)
            {
                var c = new EmployeeInvoiceDetails()
                {
                    EmployeeCode = a.tbl_EmployeeMaster.EmpCodePattern + a.tbl_EmployeeMaster.Code,
                    EmployeeId = a.EmployeeId,
                    EmployeeName = a.tbl_EmployeeMaster.FullName,
                    HourlyRate = a.HourlyRate,
                    FixedRate = a.FixedRate,
                    Fixed = a.Fixed,
                    RemunerationPercentage = a.RemunerationPercentage,
                    BillingPercentage = a.BillingPercentage,
                    EmployeeBillingId = a.Id
                };
                var payroll = GetCTC(a.EmployeeId);
                c.Remuneration = payroll.Item1;
                c.NetPay = payroll.Item2;
                n.Add(c);
            }
            return new EmployeeInvoice() { DetailList = n };
        }

        public (decimal?, decimal?) GetCTC(int employeeId)
        {
            int domainId = Utility.DomainId();
            //to avoid the DB loob
            if (EmployeeInvoiceStatic.employeepayroll == null)
                EmployeeInvoiceStatic.employeepayroll = datacontext.tbl_Pay_EmployeePayroll.OrderByDescending(p => p.YearId).ThenByDescending(l => l.MonthId).ToList();
            if (EmployeeInvoiceStatic.Tile1ID == null)
                EmployeeInvoiceStatic.Tile1ID = Convert.ToInt32(datacontext.tbl_DashBoardConfiguration.
                                     FirstOrDefault(n => n.Key == "Tile1ID" && n.DomainID == domainId).Value);
            if (EmployeeInvoiceStatic.Tile2ID == null)
                EmployeeInvoiceStatic.Tile2ID = Convert.ToInt32(datacontext.tbl_DashBoardConfiguration.
                                     FirstOrDefault(n => n.Key == "Tile2ID" && n.DomainID == domainId).Value);

            return (EmployeeInvoiceStatic.employeepayroll.FirstOrDefault(z => z.EmployeeId == employeeId && !z.IsDeleted)?.tbl_Pay_EmployeePayrollDetails.
                             FirstOrDefault(pa => pa.ComponentId == EmployeeInvoiceStatic.Tile1ID)?.Amount,
                    EmployeeInvoiceStatic.employeepayroll.FirstOrDefault(z => z.EmployeeId == employeeId && !z.IsDeleted)?.tbl_Pay_EmployeePayrollDetails.
                             FirstOrDefault(pa => pa.ComponentId == EmployeeInvoiceStatic.Tile2ID)?.Amount);
        }

        public (decimal?, decimal?) GetCTC(int employeeId, int monthId, int yearId)
        {
            int domainId = Utility.DomainId();
            //to avoid the DB loob
            if (EmployeeInvoiceStatic.employeepayroll == null)
                EmployeeInvoiceStatic.employeepayroll = datacontext.tbl_Pay_EmployeePayroll.OrderByDescending(p => p.YearId).ThenByDescending(l => l.MonthId).ToList();
            if (EmployeeInvoiceStatic.Tile1ID == null)
                EmployeeInvoiceStatic.Tile1ID = Convert.ToInt32(datacontext.tbl_DashBoardConfiguration.
                                     FirstOrDefault(n => n.Key == "Tile1ID" && n.DomainID == domainId).Value);
            if (EmployeeInvoiceStatic.Tile2ID == null)
                EmployeeInvoiceStatic.Tile2ID = Convert.ToInt32(datacontext.tbl_DashBoardConfiguration.
                                     FirstOrDefault(n => n.Key == "Tile2ID" && n.DomainID == domainId).Value);

            return (EmployeeInvoiceStatic.employeepayroll.FirstOrDefault(z => z.EmployeeId == employeeId && !z.IsDeleted && z.MonthId == monthId && z.YearId == yearId)?.tbl_Pay_EmployeePayrollDetails.
                             FirstOrDefault(pa => pa.ComponentId == EmployeeInvoiceStatic.Tile1ID)?.Amount,
                    EmployeeInvoiceStatic.employeepayroll.FirstOrDefault(z => z.EmployeeId == employeeId && !z.IsDeleted && z.MonthId == monthId && z.YearId == yearId)?.tbl_Pay_EmployeePayrollDetails.
                             FirstOrDefault(pa => pa.ComponentId == EmployeeInvoiceStatic.Tile2ID)?.Amount);
        }

        public List<int> UnmappedEmployeeIds(int monthId, int year, int customerId)
        {
            List<int> EmployeeIds = datacontext.tbl_MonthlyCustomerBilling.Where(a => !a.Isdeleted && a.MonthId == monthId &&
                                    a.Year == year && a.CustomerId == customerId).SelectMany(n => n.tbl_MonthlyCustomerBillingDetails.Where(a => !a.Isdeleted).Select(c => c.EmployeeID)).ToList();

            return EmployeeIds;
        }

        public EmployeeInvoice GetBilling(int id)
        {
            var a = datacontext.tbl_MonthlyCustomerBilling.FirstOrDefault(b => !b.Isdeleted && b.Id == id);
            var result = new EmployeeInvoice
            {
                Id = a.Id,
                MonthId = a.MonthId,
                Year = a.Year,
                CustomerId = a.CustomerId,
                CurrencyCode = datacontext.tblCurrencies.FirstOrDefault(c => c.ID == a.tblCustomer.CurrencyID).Code,
                CurrencySymbol = datacontext.tblCurrencies.FirstOrDefault(c => c.ID == a.tblCustomer.CurrencyID).CurrencyCode,
                CustomerName = a.tblCustomer.Name,
                PaymentTerms = a.tblCustomer.PaymentTerms,
                ModifiedOn = a.ModifiedOn,
                CreatedOn = a.CreatedOn,
                CurrencyRate = a.CurrencyRate,
                ModifiedByName = datacontext.tbl_EmployeeMaster.FirstOrDefault(e => e.ID == a.ModifiedBy)?.FirstName,
                DetailList = a.tbl_MonthlyCustomerBillingDetails.Where(n => !n.Isdeleted).Select(c => new EmployeeInvoiceDetails
                {
                    Id = c.Id,
                    EmployeeId = c.EmployeeID,
                    EmployeeCode = c.tbl_EmployeeMaster.EmpCodePattern + c.tbl_EmployeeMaster.Code,
                    EmployeeName = c.tbl_EmployeeMaster.FullName,
                    HourlyRate = c.HourlyRate,
                    FixedRate = c.FixedRate,
                    Fixed = c.FixedRate.HasValue,
                    HoursLogged = c.HoursLogged,
                    Total = c.Total,
                    Selected = true,
                    ModifiedOn = c.ModifiedOn,
                    ModifiedByName = datacontext.tbl_EmployeeMaster.FirstOrDefault(e => e.ID == c.ModifiedBy)?.FirstName,
                    BillingPercentage = c.BillingPercentage,
                    RemunerationPercentage = c.RemunerationPercentage,
                    EmployeeBillingId = c.EmployeeBillingId,
                    Remuneration = GetCTC(c.EmployeeID).Item1,
                    NetPay = GetCTC(c.EmployeeID).Item2
                }).OrderBy(c => c.EmployeeId).ToList()
            };

            return result;
        }

        public string ManageEmployeeInvoice(EmployeeInvoice empInvoice)
        {
            int userId = Utility.UserId();
            DateTime dateNow = DateTime.Now;
            int domainID = Utility.DomainId();
            var inv = datacontext.tbl_MonthlyCustomerBilling.FirstOrDefault(a => !a.Isdeleted && a.Id == empInvoice.Id);
            var invdetails = MonthlyCustomerBillingDetails(empInvoice, inv?.tbl_MonthlyCustomerBillingDetails.ToList());

            if (inv == null)
            {
                inv = new tbl_MonthlyCustomerBilling()
                {
                    MonthId = empInvoice.MonthId,
                    Year = empInvoice.Year,
                    CustomerId = empInvoice.CustomerId,
                    CurrencyRate = empInvoice.CurrencyRate ?? 0,
                    CreatedBy = userId,
                    ModifiedBy = userId,
                    CreatedOn = dateNow,
                    ModifiedOn = dateNow,
                    DomainId = domainID,
                    tbl_MonthlyCustomerBillingDetails = invdetails
                };
                datacontext.tbl_MonthlyCustomerBilling.Add(inv);
                datacontext.SaveChanges();
                return Notification.Inserted;
            }
            else
            {
                inv.ModifiedBy = userId;
                inv.ModifiedOn = dateNow;
                inv.CurrencyRate = empInvoice.CurrencyRate ?? 0;
                inv.tbl_MonthlyCustomerBillingDetails = invdetails;
                datacontext.Entry(inv).State = EntityState.Modified;
                datacontext.SaveChanges();

                return Notification.Updated;
            }

        }

        public List<tbl_MonthlyCustomerBillingDetails> MonthlyCustomerBillingDetails(EmployeeInvoice employeeInvoice, List<tbl_MonthlyCustomerBillingDetails> t = null)
        {
            int userId = Utility.UserId();
            DateTime dateNow = DateTime.Now;
            int domainID = Utility.DomainId();

            var list = new List<tbl_MonthlyCustomerBillingDetails>();
            if (t != null)
            {
                list.AddRange(t?.Where(a => a.Isdeleted));
            }
            foreach (var i in employeeInvoice.DetailList)
            {
                var invs = t?.FirstOrDefault(a => !a.Isdeleted && a.Id == i.Id);
                if (i.Selected || !i.Id.Equals(0))
                {
                    if (invs == null)
                        list.Add(new tbl_MonthlyCustomerBillingDetails()
                        {
                            Id = i.Id,
                            EmployeeID = i.EmployeeId,
                            MonthlyID = employeeInvoice.Id,
                            FixedRate = i.FixedRate,
                            HourlyRate = i.HourlyRate,
                            HoursLogged = i.HoursLogged,
                            Total = i.Total,
                            BillingPercentage = i.BillingPercentage,
                            RemunerationPercentage = i.RemunerationPercentage,
                            EmployeeBillingId = i.EmployeeBillingId,
                            Isdeleted = i.IsDeleted,
                            CreatedBy = userId,
                            ModifiedBy = userId,
                            CreatedOn = dateNow,
                            ModifiedOn = dateNow,
                            DomainId = domainID,
                        });
                    else
                    {
                        invs.FixedRate = i.FixedRate;
                        invs.HourlyRate = i.HourlyRate;
                        invs.HoursLogged = i.HoursLogged;
                        invs.Total = i.Total;
                        invs.Isdeleted = invs.Isdeleted || i.IsDeleted;
                        invs.ModifiedBy = userId;
                        invs.ModifiedOn = dateNow;
                        list.Add(invs);
                    }
                }
            }

            return list;
        }

        public List<EmployeeInvoice> SearchEmployeeBillingInvoiceReport(DateTime? fromDate, DateTime? todate, string customerName, string employeeName, bool? Active)
        {
            var result = (from cbi in datacontext.tbl_MonthlyCustomerBillingDetails
                          join c in datacontext.tbl_MonthlyCustomerBilling on cbi.MonthlyID equals c.Id
                          join a in datacontext.tbl_Month on c.MonthId equals a.ID
                          join b in datacontext.tblCustomers on c.CustomerId equals b.ID
                          join e in datacontext.tblCurrencies on b.CurrencyID equals e.ID
                          join emp in datacontext.tbl_EmployeeMaster on cbi.EmployeeID equals emp.ID
                          join t in datacontext.tbl_EmployeeBilling on cbi.EmployeeBillingId equals t.Id
                          where (EntityFunctions.CreateDateTime(c.Year, c.MonthId, 1, 0, 0, 0) >= (fromDate ?? EntityFunctions.CreateDateTime(c.Year, c.MonthId, 1, 0, 0, 0))
                          && EntityFunctions.CreateDateTime(c.Year, c.MonthId, 1, 0, 0, 0) <= (todate ?? EntityFunctions.CreateDateTime(c.Year, c.MonthId, 1, 0, 0, 0)))
                          && b.Name.ToUpper().Contains(string.IsNullOrEmpty(customerName) ? b.Name.ToUpper() : customerName.ToUpper())
                          && emp.FullName.ToUpper().Contains(string.IsNullOrEmpty(employeeName) ? emp.FullName.ToUpper() : employeeName.ToUpper())
                          && t.IsActive == (Active ?? t.IsActive)
                           && !c.Isdeleted && !cbi.Isdeleted
                          orderby c.ModifiedOn descending
                          select new EmployeeInvoice
                          {
                              Id = c.Id,
                              MonthName = a.Code,
                              CurrencySymbol = e.CurrencyCode,
                              CurrencyCode = e.Code,
                              Year = c.Year,
                              CustomerName = b.Name,
                              CurrencyRate = c.CurrencyRate,
                              EmployeeId = cbi.EmployeeID,
                              EmployeeName = emp.FullName,
                              Total = (cbi.Total),
                              MonthId = c.MonthId,
                              UserName = emp.FirstName,
                              RemunerationPercentage = cbi.RemunerationPercentage,
                              TotalBillingOnCalc = (cbi.Total * (cbi.BillingPercentage ?? 100)) / 100,
                          }).Distinct().ToList();

            foreach (var n in result)
            {
                n.Remuneration = 0;
                n.Expense = 0;
                n.Remuneration += ((GetCTC(n.EmployeeId).Item1 ?? 0) * (n.RemunerationPercentage ?? 100)) / 100;
                n.Expense += ((GetCTC(n.EmployeeId, n.MonthId, n.Year).Item1 ?? 0) * (n.RemunerationPercentage ?? 100)) / 100;
                n.TotalRemunerationCalc += ((GetCTC(n.EmployeeId, n.MonthId, n.Year).Item1 ?? 0));

            }
            return result.OrderByDescending(y => y.Year).ThenByDescending(m => m.MonthId).GroupBy(a => a.CustomerName).SelectMany(a => a).ToList();
        }

        public List<EmployeeInvoice> SearchEmployeeBillingInvoiceDashBoard(int? yearId, int orderby)
        {
            var list = SearchEmployeeBillingInvoiceReport(null, null, string.Empty, string.Empty, null);
            if (yearId != null)
            {
                list = list.Where(a => a.Year == yearId).ToList();
            }

            var result = list.GroupBy(a => a.EmployeeId).Select(b => new EmployeeInvoice
            {
                EmployeeName = b.First().UserName,
                Total = b.Sum(n => n.Total * (n.CurrencyRate ?? 1)),
                Remuneration = b.Sum(x => x.Expense),
                CurrencyRate = ((b.Sum(x => x.Total * (x.CurrencyRate ?? 1)) - b.Sum(n => n.Expense)) / b.Sum(n => n.Total)) * 100
            });

            if (orderby == 1)
                return result.OrderByDescending(a => a.CurrencyRate).ToList();
            else if (orderby == 2)
                return result.OrderByDescending(a => a.Total).ToList();
            else
                return result.OrderByDescending(a => a.Remuneration).ToList();
        }
    }
}
