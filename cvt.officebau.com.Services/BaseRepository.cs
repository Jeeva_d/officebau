﻿using Microsoft.Practices.EnterpriseLibrary.Data;

namespace cvt.officebau.com.Services
{
    public class BaseRepository
    {
        public BaseRepository(string name)
            : this(DatabaseFactory.CreateDatabase(name))
        {
        }

        public BaseRepository()
        {
            if (Database == null)
            {
                Database = DatabaseFactory.CreateDatabase("Conn");
            }
        }

        public BaseRepository(Database database)
        {
            Database = database;
        }

        protected Database Database { get; }
    }
}