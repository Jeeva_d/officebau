﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class FinancialYearRepository : BaseRepository
    {
        #region Search FinancialYear

        public List<FinancialYearBo> SearchFinancialYear(FinancialYearBo financialYearBo)

        {
            List<FinancialYearBo> financialYearBoList = new List<FinancialYearBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_FINANCIALYEAR);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        financialYearBoList.Add(
                            new FinancialYearBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                FinancialYear = Utility.CheckDbNull<string>(reader[DBParam.Output.FinancialYear]),
                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description])
                            });
                    }
                }
            }

            return financialYearBoList;
        }

        #endregion Search FinancialYear

        #region Get FinancialYear

        public FinancialYearBo GetFinancialYear(int yearId)
        {
            FinancialYearBo financialYearBo = new FinancialYearBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_FINANCIALYEAR);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        financialYearBo = new FinancialYearBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            FinancialYear = Utility.CheckDbNull<string>(reader[DBParam.Output.FinancialYear]),
                            StartMonth = Utility.CheckDbNull<string>(reader[DBParam.Output.StartMonth]),
                            EndMonth = Utility.CheckDbNull<string>(reader[DBParam.Output.EndMonth]),
                            Value = Utility.CheckDbNull<string>(reader[DBParam.Output.Value])
                        };
                    }
                }
            }

            return financialYearBo;
        }

        #endregion Get FinancialYear

        #region Manage FinancialYear

        public string ManageFinancialYear(FinancialYearBo financialYearBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_FINANCIALYEAR);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, financialYearBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYear, DbType.String, financialYearBo.FinancialYear);
                Database.AddInParameter(dbCommand, DBParam.Input.Description, DbType.String, financialYearBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.HasDeleted, DbType.Boolean, financialYearBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                financialYearBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return financialYearBo.UserMessage;
        }

        #endregion Manage FinancialYear

        public FinancialYearBo GetFnMonthAppConfig()
        {
            FinancialYearBo financialYearBo = new FinancialYearBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GetFNMonthAppConfig);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        financialYearBo = new FinancialYearBo
                        {
                            Value = Utility.CheckDbNull<string>(reader[DBParam.Output.Value]),
                            StartMonth = Utility.CheckDbNull<string>(reader[DBParam.Output.StartMonth]),
                            EndMonth = Utility.CheckDbNull<string>(reader[DBParam.Output.EndMonth])
                        };
                    }
                }
            }

            return financialYearBo;
        }
    }
}