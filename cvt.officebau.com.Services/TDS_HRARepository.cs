﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class TdsHraRepository : BaseRepository
    {
        #region Manage

        /// <summary>
        ///     This function is used to Manage the HRA for TDS
        /// </summary>
        /// <param name="tdsHrabo"></param>
        /// <returns>Success or Alert message to the view</returns>
        public string ManageHraDeclare(TdsHrabo tdsHrabo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_HRA);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, tdsHrabo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, tdsHrabo.EmployeeId != 0 ? tdsHrabo.EmployeeId : Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, tdsHrabo.Month.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, tdsHrabo.Year.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.StartYearID, DbType.Int32, tdsHrabo.StartYear.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.Declaration, DbType.Decimal, tdsHrabo.Declaration);
                Database.AddInParameter(dbCommand, DBParam.Input.Submitted, DbType.Decimal, tdsHrabo.Submitted);
                Database.AddInParameter(dbCommand, DBParam.Input.Cleared, DbType.Decimal, tdsHrabo.Cleared);
                Database.AddInParameter(dbCommand, DBParam.Input.Rejected, DbType.Decimal, tdsHrabo.Rejected);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, tdsHrabo.Remarks);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionEmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, tdsHrabo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                tdsHrabo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return tdsHrabo.UserMessage;
        }

        #endregion Manage

        #region Search

        #region Search TDS_HRA Declaration

        /// <summary>
        ///     This function is used to retrive the Details of the HRA Declaration from the DB
        /// </summary>
        /// <param name="tdsHraDeclare"></param>
        /// <returns>TDSdeclaration to the view</returns>
        public List<TdsHrabo> Search_TDSHRADeclaration(TdsHrabo tdsHraDeclare)
        {
            List<TdsHrabo> tdSdeclaration = new List<TdsHrabo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_HRADECLARATION);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, tdsHraDeclare.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, tdsHraDeclare.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.StartYearID, DbType.Int32, tdsHraDeclare.FinancialYearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        TdsHrabo declare = new TdsHrabo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            Month = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.MonthYear].ToString()),
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.MonthID])
                            },
                            Year = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.YearID])
                            },
                            StartYear = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.StartYearID])
                            },
                            Declaration = decimal.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Declaration]), 2),
                            Submitted = decimal.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Submitted]), 2),
                            Cleared = decimal.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Cleared]), 2),
                            Rejected = decimal.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rejected]), 2),
                            IsOpen = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOpen]),
                            IsProofOpen = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsProofOpen]),
                            IsFinancialYear = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsFinancialYear]),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks].ToString())
                        };
                        tdSdeclaration.Add(declare);
                    }
                }

                return tdSdeclaration;
            }
        }

        #endregion Search TDS_HRA Declaration

        #region Search TDS_HRA Employee

        /// <summary>
        ///     This function is used to retrive the List the Employee from the DB
        /// </summary>
        /// <param name="tdsHrabo"></param>
        /// <returns>employeeList to the view</returns>
        public List<TdsHrabo> SearchEmployee(TdsHrabo tdsHrabo)
        {
            List<TdsHrabo> employeeList = new List<TdsHrabo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEEHRA);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, tdsHrabo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    TdsHrabo clearanceBO;
                    while (reader.Read())
                    {
                        clearanceBO = new TdsHrabo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName].ToString()),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode].ToString())
                        };
                        employeeList.Add(clearanceBO);
                    }
                }
            }

            return employeeList;
        }

        #endregion Search TDS_HRA Employee

        #region Search SearchTDS Clearance for Employee

        /// <summary>
        ///     This function is used to retrive the List the Clearance details for the specific Employee from the DB
        /// </summary>
        /// <param name="tdsHrabo"></param>
        /// <returns>clearanceList to the view</returns>
        public List<TdsHrabo> SearchTdsClearance(TdsHrabo tdsHrabo)
        {
            List<TdsHrabo> clearanceList = new List<TdsHrabo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_HRACLEARANCE);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, tdsHrabo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, tdsHrabo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.StartYearID, DbType.Int32, tdsHrabo.FinancialYearId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    TdsHrabo clearanceBO;
                    while (reader.Read())
                    {
                        clearanceBO = new TdsHrabo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            Month = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.MonthYear].ToString()),
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.MonthID])
                            },
                            Year = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.YearID])
                            },
                            StartYear = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.StartYearID])
                            },
                            Declaration = decimal.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Declaration]), 2),
                            Submitted = decimal.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Submitted]), 2),
                            Cleared = decimal.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Cleared]), 2),
                            Rejected = decimal.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Rejected]), 2),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks].ToString()),
                            IsOpen = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOpen]),
                            IsProofOpen = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsProofOpen]),
                            IsFinancialYear = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsFinancialYear])
                        };
                        clearanceList.Add(clearanceBO);
                    }
                }
            }

            return clearanceList;
        }

        #endregion Search SearchTDS Clearance for Employee

        #endregion Search

        #region TDS IT Declaration Report

        public DataTable TDSITDeclarationRPTList(int financialyearId, string formType)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Rpt_TDSITDeclarationReport");

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, 0);
                Database.AddInParameter(dbCommand, "@FYID", DbType.Int32, financialyearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Type, DbType.String, formType);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeeTDSReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }

                return dtSalesTargetReport;
            }
        }

        #endregion

        #region TDS HRA Clearance Report

        public DataTable TDSHRAClearanceRPTList(int financialyearId, string status)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("Rpt_TDSHRAClearanceReport");

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, 0);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, financialyearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Status, DbType.String, status);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeeTDSReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }

                return dtSalesTargetReport;
            }
        }

        #endregion
    }
}