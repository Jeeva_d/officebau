﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class InventoryDashboardRepository : BaseRepository
    {
        #region Tiles Details

        public InventoryBO GetDashboardTilesDetails()
        {
            InventoryBO inventoryBo = new InventoryBO();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_INVENTORYDASHBOARDTILESDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        inventoryBo = new InventoryBO
                        {
                            LowStockThreshold = Utility.CheckDbNull<int>(reader[DBParam.Output.LowStockThreshold]),
                            OutofStock = Utility.CheckDbNull<int>(reader[DBParam.Output.OutofStock]),
                            POQuantity = Utility.CheckDbNull<decimal>(reader[DBParam.Output.POQuantity]),
                            SOQuantity = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SOQuantity]),
                        };
                    }
                }
                return inventoryBo;
            }
        }

        #endregion Tiles Details

        #region Dashboard List

        public DataTable SearchInventoryDashboardList(string screenType)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_INVENTORYDASHBOARDLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.ScreenType, DbType.String, screenType);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                DataSet ds = new DataSet();
                DataTable inventoryDashboard = new DataTable("InventoryDashboardList");
                ds.Tables.Add(inventoryDashboard);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, inventoryDashboard);
                }

                return inventoryDashboard;
            }
        }

        #endregion Dashboard List
    }
}