﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;

namespace cvt.officebau.com.Services
{
    public class TdsRepository : BaseRepository
    {
        public List<Tdsbo> SearchFinancialYear(Tdsbo tds)
        {
            List<Tdsbo> tdsboList = new List<Tdsbo>();

            using (Database.CreateConnection())
            {
                DbCommand dbcmd = Database.GetStoredProcCommand(Constants.SEARCH_TDSFINANCIALYEAR);

                Database.AddInParameter(dbcmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbcmd, DBParam.Input.CALID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbcmd, DBParam.Input.EmployeeName, DbType.String, tds.ModifiedByName);

                using (IDataReader reader = Database.ExecuteReader(dbcmd))
                {
                    while (reader.Read())
                    {
                        tds = new Tdsbo
                        {
                            YearId = Convert.ToInt32(reader[DBParam.Output.ID].ToString()),
                            FinancialYear = reader[DBParam.Output.Code].ToString()
                        };
                        tdsboList.Add(tds);
                    }
                }
            }

            return tdsboList;
        }

        public List<Tdsbo> SearchCurrentFinancialMonth(int yearId)
        {
            List<Tdsbo> tdsboList = new List<Tdsbo>();

            using (Database.CreateConnection())
            {
                DbCommand dbcmd = Database.GetStoredProcCommand(Constants.SEARCH_FINANCIALMONTH);

                Database.AddInParameter(dbcmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbcmd, DBParam.Input.CALID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbcmd, DBParam.Input.FinancialYearID, DbType.Int32, yearId);

                using (IDataReader reader = Database.ExecuteReader(dbcmd))
                {
                    while (reader.Read())
                    {
                        Tdsbo tdsBo = new Tdsbo
                        {
                            CurrentFinancialYearId = Convert.ToInt32(reader[DBParam.Output.ID].ToString()),
                            CurrentFinancialYear = reader[DBParam.Output.CurrentFinancialYear].ToString()
                        };
                        tdsboList.Add(tdsBo);
                    }
                }
            }

            return tdsboList;
        }

        public string ManageTds(int empid, int monthid, int yearid, decimal? tax)
        {
            string message;

            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.MANAGE_TDS);

                Database.AddInParameter(dbCmd, DBParam.Input.EmployeeID, DbType.Int32, empid);
                Database.AddInParameter(dbCmd, DBParam.Input.MonthID, DbType.Int32, monthid);
                Database.AddInParameter(dbCmd, DBParam.Input.YearID, DbType.Int32, yearid);
                Database.AddInParameter(dbCmd, DBParam.Input.TDSAmount, DbType.Decimal, tax);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCmd, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                message = Database.ExecuteScalar(dbCmd).ToString();
            }

            return message;
        }

        public List<Tdsbo> SearchEmployeeTds(int monthId, int yearId, string businessUnitIDs, string report)
        {
            List<Tdsbo> tdsboList = new List<Tdsbo>();
            TdsCalculator tdsCalculator = new TdsCalculator();

            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEETDS);

                Database.AddInParameter(dbCmd, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCmd, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCmd, DBParam.Input.BusinessUnit, DbType.String, businessUnitIDs);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCmd))
                {
                    while (reader.Read())
                    {
                        Tdsbo tdsBo = new Tdsbo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            MonthId = Utility.CheckDbNull<int>(reader[DBParam.Output.Counts]),
                            TdsRate = Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS]),
                            YearId = Utility.CheckDbNull<int>(reader[DBParam.Output.RunTDS])
                        };
                        if (tdsBo.MonthId == 0 && tdsBo.YearId == 1)
                        {
                            tdsBo.TdsRate = Utility.CheckDbNull<decimal>(tdsCalculator.Calculate(Convert.ToInt32(reader[DBParam.Output.EmployeeID]), yearId, monthId, 1, report, Utility.DomainId(), string.Empty, Convert.ToInt32(reader[DBParam.Output.YearName])));
                        }

                        tdsBo.BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.PANNO]);
                        tdsBo.ChallanNumber = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]);
                        tdsBo.Year = Utility.CheckDbNull<int>(reader[DBParam.Output.YearName]);
                        tdsboList.Add(tdsBo);
                    }
                }
            }

            return tdsboList;
        }

        public List<TdsHrabo> SearchemployeeTdsList(TdsHrabo tdsHrabo, string type)
        {
            List<TdsHrabo> tdshraboList = new List<TdsHrabo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.SEARCH_EMPLOYEETDSLIST);

                Database.AddInParameter(dbCmd, DBParam.Input.ID, DbType.Int32, tdsHrabo.Id);
                Database.AddInParameter(dbCmd, DBParam.Input.EmployeeID, DbType.Int32, tdsHrabo.EmployeeId);
                Database.AddInParameter(dbCmd, DBParam.Input.StartYearID, DbType.Int32, tdsHrabo.FinancialYearId);
                Database.AddInParameter(dbCmd, DBParam.Input.Type, DbType.String, type);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCmd))
                {
                    while (reader.Read())
                    {
                        TdsHrabo employeeBo = new TdsHrabo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            Month = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Name = Utility.CheckDbNull<string>(reader[DBParam.Output.MonthYear].ToString()),
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.MonthID])
                            },
                            TdsRate =
                                decimal.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDSAmount]), 2),
                            Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross]),
                            Pt = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PT]),
                            NetPay = Utility.CheckDbNull<decimal>(reader[DBParam.Output.NetPay]),
                            FileCount = Utility.CheckDbNull<int>(reader[DBParam.Output.Count]),
                            Remarks = Utility.CheckDbNull<string>(reader["Header"].ToString()),

                        };
                        tdshraboList.Add(employeeBo);
                    }
                }
            }

            return tdshraboList;
        }

        #region Search TDS Employee

        public List<TdsHrabo> SearchEmployeeTds(TdsHrabo tdsHrabo)
        {
            List<TdsHrabo> tdshraboList = new List<TdsHrabo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.SEARCH_TDSEMPLOYEE);

                Database.AddInParameter(dbCmd, DBParam.Input.EmployeeID, DbType.Int32, tdsHrabo.EmployeeId);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCmd))
                {
                    while (reader.Read())
                    {
                        TdsHrabo employeeBo = new TdsHrabo
                        {
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName =
                                Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName].ToString()),
                            EmployeeCode =
                                Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode].ToString())
                        };
                        tdshraboList.Add(employeeBo);
                    }
                }
            }

            return tdshraboList;
        }

        #endregion Search TDS Employee

        #region TDS Other Component

        public List<TdsComponentBo> SearchTdsOtherComponent(TdsComponentBo tdsComponentBo)
        {
            List<TdsComponentBo> tdsComponentBoList = new List<TdsComponentBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.SEARCH_TDSOTHERCOMPONENT);

                Database.AddInParameter(dbCmd, DBParam.Input.BusinessUnitID, DbType.Int32, tdsComponentBo.BusinessUnitId);
                Database.AddInParameter(dbCmd, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCmd, DBParam.Input.StartYearID, DbType.Int32, tdsComponentBo.FinancialYearId);

                using (IDataReader reader = Database.ExecuteReader(dbCmd))
                {
                    TdsComponentBo employeeBO;
                    while (reader.Read())
                    {
                        employeeBO = new TdsComponentBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName].ToString()),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode].ToString()),
                            FinancialYearId = Utility.CheckDbNull<int>(reader[DBParam.Output.FinancialYear]),
                            Bonus = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Bonus]),
                            Pli = Utility.CheckDbNull<decimal>(reader[DBParam.Output.PLI]),
                            OtherIncome = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherIncome]),
                            BusinessUnit = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit].ToString())
                        };
                        tdsComponentBoList.Add(employeeBO);
                    }
                }
            }

            return tdsComponentBoList;
        }

        public DataTable ExportTdsComponent(TdsComponentBo tdsComponentBo)
        {
            DataSet ds = new DataSet();
            DataTable dtTdsComponent = new DataTable("ExportTDSComponent");
            ds.Tables.Add(dtTdsComponent);

            using (Database.CreateConnection())
            {
                DbCommand dbCmd = Database.GetStoredProcCommand(Constants.EXPORT_TDSCOMPONENT);
                Database.AddInParameter(dbCmd, DBParam.Input.YearID, DbType.Int32, tdsComponentBo.FinancialYearId);
                Database.AddInParameter(dbCmd, DBParam.Input.BaseLocationID, DbType.Int32, tdsComponentBo.BusinessUnitId);
                Database.AddInParameter(dbCmd, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCmd, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCmd))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtTdsComponent);
                }

                return dtTdsComponent;
            }
        }

        public TdsComponentBo GetTdsComponent(int? id)
        {
            using (FSMEntities context = new FSMEntities())
            {
                TdsComponentBo result = new TdsComponentBo();

                if (id != null)
                {
                    result = (from I in context.tbl_TDSEmployeeOtherIncome
                              join em in context.tbl_EmployeeMaster on I.EmployeeID equals em.ID
                              where I.ID == id
                              select new TdsComponentBo
                              {
                                  Id = I.ID,
                                  EmployeeId = em.ID,
                                  EmployeeCode = (em.EmpCodePattern ?? "") + em.Code,
                                  EmployeeName = em.FullName,
                                  Bonus = I.Bonus ?? 0,
                                  Pli = I.PLI ?? 0,
                                  OtherIncome = I.OtherIncome ?? 0
                              }).FirstOrDefault();
                }

                return result;
            }
        }

        public string DeleteTdsComponent(int id)
        {
            FSMEntities context = new FSMEntities();
            tbl_TDSEmployeeOtherIncome tdsComponent = context.tbl_TDSEmployeeOtherIncome.FirstOrDefault(b => b.ID == id);

            if (tdsComponent != null)
            {
                tdsComponent.IsDeleted = true;
                tdsComponent.ModifiedOn = DateTime.Now;
                tdsComponent.ModifiedBy = Utility.UserId();

                context.Entry(tdsComponent).State = EntityState.Modified;
            }

            context.SaveChanges();

            return Notification.Deleted;
        }

        public string UpdateTdsComponent(TdsComponentBo tdsComponentBo)
        {
            FSMEntities context = new FSMEntities();
            tbl_TDSEmployeeOtherIncome tdsComponent = context.tbl_TDSEmployeeOtherIncome.FirstOrDefault(b => b.ID == tdsComponentBo.Id);

            if (tdsComponent != null)
            {
                tdsComponent.Bonus = tdsComponentBo.Bonus;
                tdsComponent.PLI = tdsComponentBo.Pli;
                tdsComponent.OtherIncome = tdsComponentBo.OtherIncome;
                tdsComponent.ModifiedOn = DateTime.Now;
                tdsComponent.ModifiedBy = Utility.UserId();

                context.Entry(tdsComponent).State = EntityState.Modified;
            }

            context.SaveChanges();

            return Notification.Updated;
        }

        #endregion TDS Other Component


    }
}
