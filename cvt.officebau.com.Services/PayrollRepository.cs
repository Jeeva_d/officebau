﻿using cvt.officebau.com.Repository;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace cvt.officebau.com.Services
{
    public class PayrollRepository : BaseRepository
    {
        #region ManagePayroll

        public string ManagePayroll(PayrollBO payrollBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGEEMPLOYEEPAYROLL);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, payrollBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, payrollBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, payrollBo.MonthID);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, payrollBo.YearID);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeePayStructureID, DbType.Int32, payrollBo.EmployeePayStructureID);
                Database.AddInParameter(dbCommand, DBParam.Input.Basic, DbType.Decimal, payrollBo.Basic);
                Database.AddInParameter(dbCommand, DBParam.Input.HRA, DbType.Decimal, payrollBo.HRA);
                Database.AddInParameter(dbCommand, DBParam.Input.Conveyance, DbType.Decimal, payrollBo.Conveyance);
                Database.AddInParameter(dbCommand, DBParam.Input.MedicalAllowance, DbType.Decimal, payrollBo.MedicalAllowance);
                Database.AddInParameter(dbCommand, DBParam.Input.EducationAllowance, DbType.Decimal, payrollBo.EducationAllowance);
                Database.AddInParameter(dbCommand, DBParam.Input.OverTime, DbType.Decimal, payrollBo.OverTime);
                Database.AddInParameter(dbCommand, DBParam.Input.PaperMagazine, DbType.Decimal, payrollBo.PaperMagazine);
                Database.AddInParameter(dbCommand, DBParam.Input.MonsoonAllow, DbType.Decimal, payrollBo.MonsoonAllowance);
                Database.AddInParameter(dbCommand, DBParam.Input.SplAllow, DbType.Decimal, payrollBo.SplAllow);
                Database.AddInParameter(dbCommand, DBParam.Input.EEPF, DbType.Decimal, payrollBo.EEPF);
                Database.AddInParameter(dbCommand, DBParam.Input.EEESI, DbType.Decimal, payrollBo.EEESI);
                Database.AddInParameter(dbCommand, DBParam.Input.PT, DbType.Decimal, payrollBo.PT);
                Database.AddInParameter(dbCommand, DBParam.Input.TDS, DbType.Decimal, payrollBo.TDS);
                Database.AddInParameter(dbCommand, DBParam.Input.ERPF, DbType.Decimal, payrollBo.ERPF);
                Database.AddInParameter(dbCommand, DBParam.Input.ERESI, DbType.Decimal, payrollBo.ERESI);
                Database.AddInParameter(dbCommand, DBParam.Input.Bonus, DbType.Decimal, payrollBo.Bonus);
                Database.AddInParameter(dbCommand, DBParam.Input.Gratuity, DbType.Decimal, payrollBo.Gratuity);
                Database.AddInParameter(dbCommand, DBParam.Input.PLI, DbType.Decimal, payrollBo.PLI);
                Database.AddInParameter(dbCommand, DBParam.Input.Mediclaim, DbType.Decimal, payrollBo.Medicalclaim);
                Database.AddInParameter(dbCommand, DBParam.Input.MobileDataCard, DbType.Decimal, payrollBo.MobileDataCard);
                Database.AddInParameter(dbCommand, DBParam.Input.OtherEarning, DbType.Decimal, payrollBo.OtherEarnings);
                Database.AddInParameter(dbCommand, DBParam.Input.OtherPerks, DbType.Decimal, payrollBo.OtherPerks);
                Database.AddInParameter(dbCommand, DBParam.Input.OtherDeduction, DbType.Decimal, payrollBo.Deduction);
                Database.AddInParameter(dbCommand, DBParam.Input.Loans, DbType.Decimal, payrollBo.Loans);
                Database.AddInParameter(dbCommand, DBParam.Input.NetSalary, DbType.Decimal, payrollBo.NetSalary);
                Database.AddInParameter(dbCommand, DBParam.Input.CTC, DbType.Decimal, payrollBo.CTC);
                Database.AddInParameter(dbCommand, DBParam.Input.GrossEarning, DbType.Decimal, payrollBo.Gross);
                Database.AddInParameter(dbCommand, DBParam.Input.IsApproved, DbType.Boolean, payrollBo.IsApproved);
                Database.AddInParameter(dbCommand, DBParam.Input.IsProcessed, DbType.Boolean, payrollBo.IsProcessed);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.LoginID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.BaseLocationID, DbType.Int32, payrollBo.BaseLocationID);
                Database.AddInParameter(dbCommand, DBParam.Input.NoofDaysPayable, DbType.Decimal, payrollBo.WorkingDays);
                payrollBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return payrollBo.UserMessage;
        }

        #endregion ManagePayroll

        #region Calculate ESI Individual

        public List<PayrollBO> CalculateEsiIndividual(PayrollBO payrollBo)
        {
            List<PayrollBO> result = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CALCULATEESIINDIVIDUAL);

                Database.AddInParameter(dbCommand, DBParam.Input.OverTime, DbType.Decimal, payrollBo.OverTime);
                Database.AddInParameter(dbCommand, DBParam.Input.IsESIRequired, DbType.Int32, payrollBo.IsESIRequired);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, payrollBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.WorkingDays, DbType.Decimal, payrollBo.WorkingDays);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, payrollBo.MonthID);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, payrollBo.YearID);
                Database.AddInParameter(dbCommand, DBParam.Input.BaseLocationID, DbType.Int32, payrollBo.BaseLocationID);
                Database.AddInParameter(dbCommand, DBParam.Input.Basic, DbType.Decimal, payrollBo.Basic);
                Database.AddInParameter(dbCommand, DBParam.Input.HRA, DbType.Decimal, payrollBo.HRA);
                Database.AddInParameter(dbCommand, DBParam.Input.MedicalAllowance, DbType.Decimal, payrollBo.MedicalAllowance);
                Database.AddInParameter(dbCommand, DBParam.Input.Conveyance, DbType.Decimal, payrollBo.Conveyance);
                Database.AddInParameter(dbCommand, DBParam.Input.SplAllow, DbType.Decimal, payrollBo.SplAllow);
                Database.AddInParameter(dbCommand, DBParam.Input.EducationAllowance, DbType.Decimal, payrollBo.EducationAllowance);
                Database.AddInParameter(dbCommand, DBParam.Input.PaperMagazine, DbType.Decimal, payrollBo.PaperMagazine);
                Database.AddInParameter(dbCommand, DBParam.Input.MonsoonAllow, DbType.Decimal, payrollBo.MonsoonAllowance);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        result.Add(new PayrollBO
                        {
                            EEESI = Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEESI]),
                            ERESI = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERESI])
                        });
                    }
                }

                return result;
            }
        }

        #endregion Calculate ESI Individual

        #region SearchPayrollHistory

        public List<PayrollBO> SearchPayrollHistory(int employeeId)
        {
            List<PayrollBO> payrollBoList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHEMPLOYEEPAYROLLHISTORY);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            DOJ = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]),
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            MonthID = Utility.CheckDbNull<int>(reader[DBParam.Output.MonthID]),
                            Month = Utility.CheckDbNull<string>(reader[DBParam.Output.Month]),
                            YearID = Utility.CheckDbNull<int>(reader[DBParam.Output.YearId]),
                            FixedGross = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.FixedGross]), 0),
                            WorkingDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.No_of_days_payable]),
                            Basic = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]), 0),
                            HRA = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]), 0),
                            MedicalAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]), 0),
                            SplAllow = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.SplAllow]), 0),
                            OverTime = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OverTime]), 0),
                            EducationAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]), 0),
                            PaperMagazine =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]), 0),
                            MonsoonAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]), 0),
                            Conveyance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]), 0),
                            OtherEarnings =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherEarning]), 0),
                            PaidGross = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaidGross]), 0),
                            EEPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEPF])),
                            EEESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEESI])),
                            TDS = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS]), 0),
                            PT = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PT]), 0),
                            Loans = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Loans]), 0),
                            Deduction = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherDeductions]),
                                0),
                            NetSalary = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.NetPayable]), 0),
                            ERPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERPF])),
                            ERESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERESI])),
                            Bonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Bonus]), 0),
                            Gratuity = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gratuity])),
                            PLI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PLI]), 0),
                            Medicalclaim =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Mediclaim]), 0),
                            MobileDataCard =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MobileDataCard]), 0),
                            OtherPerks = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherPerks]), 0),
                            CTC = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.CTC]), 0),
                            Operation = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Department = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                            SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo]),
                            IsEducationAllow = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEducationAllow]),
                            IsOverTime = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOverTime]),
                            IsEEPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEPF]),
                            IsEEESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEESI]),
                            IsPT = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPT]),
                            IsTDS = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsTDS]),
                            IsERPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERPF]),
                            IsERESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERESI]),
                            IsGratuity = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsGratuity]),
                            IsPLI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPLI]),
                            IsMediclaim = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMediclaim]),
                            IsMobileDatacard = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMobileDatacard]),
                            IsOtherPerks = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOtherPerks])
                        };
                        //=======================
                        //=======================
                        payrollBoList.Add(payrollBo);
                    }
                }
            }

            return payrollBoList;
        }

        #endregion SearchPayrollHistory

        #region PayrollHistory

        public List<PayrollBO> GetPayrollHistory(int employeeId)
        {
            List<PayrollBO> boList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETEMPLOYEEPAYROLLHISTORY);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            Month = Utility.CheckDbNull<string>(reader[DBParam.Output.Month]),
                            YearID = Utility.CheckDbNull<int>(reader[DBParam.Output.YearId]),
                            Gross = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross]), 0),
                            Basic = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]), 0),
                            HRA = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]), 0),
                            MedicalAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]), 0),
                            SplAllow = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.SplAllow]), 0),
                            EducationAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]), 0),
                            PaperMagazine =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]), 0),
                            MonsoonAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]), 0),
                            OverTime = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OverTime]), 0),
                            Conveyance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]), 0),
                            EEPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEPF])),
                            EEESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEESI])),
                            ERPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERPF])),
                            ERESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERESI])),
                            Bonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Bonus]), 0),
                            Gratuity = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gratuity])),
                            PLI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PLI]), 0),
                            PT = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PT]), 0),
                            TDS = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS]), 0),
                            Medicalclaim =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Mediclaim]), 0),
                            MobileDataCard =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MobileDataCard]), 0),
                            OtherEarnings =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherEarning]), 0),
                            OtherPerks = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherPerks]), 0),
                            Loans = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Loans]), 0),
                            Deduction = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherDeductions]),
                                0),
                            NetSalary = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.NetSalary]), 0),
                            CTC = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.CTC]), 0),
                            IsEducationAllow = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEducationAllow]),
                            IsOverTime = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOverTime]),
                            IsEEPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEPF]),
                            IsEEESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEESI]),
                            IsPT = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPT]),
                            IsTDS = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsTDS]),
                            IsERPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERPF]),
                            IsERESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERESI]),
                            IsGratuity = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsGratuity]),
                            IsPLI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPLI]),
                            IsMediclaim = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMediclaim]),
                            IsMobileDatacard = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMobileDatacard]),
                            IsOtherPerks = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOtherPerks])
                        };
                        //-----------
                        boList.Add(payrollBo);
                    }
                }
            }

            return boList;
        }

        #endregion PayrollHistory

        #region ManageRevertPayroll

        public string ManageRevertPayroll(PayrollBO payrollBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.REVERTPAYROLL);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeName, DbType.String, payrollBo.EmployeeName);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, payrollBo.BusinessUnitIDs);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, payrollBo.MonthID);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, payrollBo.YearID);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                payrollBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return payrollBo.UserMessage;
        }

        #endregion ManageRevertPayroll

        #region Coveringletter

        public List<PayrollBO> Coveringletter(int monthId, int yearId, string businessUnitList)
        {
            List<PayrollBO> boList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.COVERINGLETTER);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName]),
                            UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.AccountNo]),
                            Operation = Utility.CheckDbNull<string>(reader[DBParam.Output.IFSCCode]),
                            Gross = Math.Round(Convert.ToDecimal(reader[DBParam.Output.NetPayable]), 0)
                        };
                        boList.Add(payrollBo);
                    }
                }
            }

            return boList;
        }

        #endregion Coveringletter

        #region check Payroll Processed

        public string CheckPayrollProcessed(int monthId, int yearId, string businessUnitList, int? department)
        {
            string userMessage = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.CHECKPAYROLLBYMONTHLY);

                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, department);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        userMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage]);
                    }
                }
            }

            if (userMessage.Split('!')[0] != "0")
            {
                return userMessage.Replace(userMessage.Split('!')[0], "");
            }

            return string.Empty;
        }

        #endregion check Payroll Processed

        #region Populate MailBody for Payroll

        public string PopulateMailBody(string monthName, string yearName, int monthId, int yearId, string businessUnitList, int? department)
        {
            string body;
            StringBuilder strContent = new StringBuilder();

            List<PayrollBO> payrollBoList = RPT_SearchPayrollByMonthly(monthId, yearId, businessUnitList, department);

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/Mail-Template/PayrollDetails.html")))
            {
                body = reader.ReadToEnd();
            }

            decimal totalnetpay = 0, totalCtc = 0, totalTds = 0, totalLoans = 0, totalDeduction = 0, totalOtherEarnings = 0, totalLop = 0;
            foreach (PayrollBO item in payrollBoList)
            {
                strContent.AppendLine(@"<tr>");
                strContent.AppendLine(@"<td width='20%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt'>
                                         <p class=MsoNormal><span>" + item.EmployeeName +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span> " + item.NetSalary.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span>  " + item.CTC.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span> " + item.TDS.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span>  " + item.Loans.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='13%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span> " + item.Deduction.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='13%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span> " + item.OtherEarnings.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span>  " + item.LOPDays.ToString("#,###,##0.00") +
                                      "</span></p></td>");
                strContent.AppendLine(@"</tr>");

                totalnetpay = totalnetpay + item.NetSalary;
                totalCtc = totalCtc + item.CTC;
                totalTds = totalTds + item.TDS;
                totalLoans = totalLoans + item.Loans;
                totalDeduction = totalDeduction + item.Deduction;
                totalOtherEarnings = totalOtherEarnings + item.OtherEarnings;
                totalLop = totalLop + item.LOPDays;
            }

            strContent.AppendLine(@"<tr>");
            strContent.AppendLine(@"<td width='20%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt'>
                                           <p class=MsoNormal><span><b>Total</b>
                                        </span></p></td>");
            strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span><b> " + totalnetpay.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span> <b>" + totalCtc.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span><b> " + totalTds.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span><b>" + totalLoans.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='13%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span><b>" + totalDeduction.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='13%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                           <p class=MsoNormal><span> <b>" + totalOtherEarnings.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"<td width='10%' style='padding:6.0pt 6.0pt 6.0pt 6.0pt;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;text-align:right;'>
                                            <p class=MsoNormal><span><b>" + totalLop.ToString("#,###,##0.00") +
                                  "</b></span></p></td>");
            strContent.AppendLine(@"</tr>");

            body = body.Replace("{List}", strContent.ToString());
            body = body.Replace("{Month}", monthName);
            body = body.Replace("{Year}", yearName);
            body = body.Replace("{Date}", DateTime.Now.ToString("dd-MMM-yyyy"));

            return body;
        }

        #endregion Populate MailBody for Payroll

        #region Search Payroll

        public string SearchPayrollForTds(int monthId, int yearId, string businessUnitList, string isLopUpdate)
        {
            PayrollBO payrollBoList = new PayrollBO();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHPAYROLLFORTDS);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.IsLOPUpdate, DbType.String, isLopUpdate);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        payrollBoList.UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.UserMessage]);
                    }
                }
            }

            return payrollBoList.UserMessage;
        }

        public List<PayrollBO> SearchPayroll(int monthId, int yearId, string businessUnitList, int? department, int? statusId, int? proceed)
        {
            List<PayrollBO> payrollBoList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHEMPLOYEEPAYROLL);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, department);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, statusId);
                Database.AddInParameter(dbCommand, DBParam.Input.Proceed, DbType.Int32, proceed);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            DOJ = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]),
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeePayStructureID =
                                Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeePaystructureID]),
                            MonthID = Utility.CheckDbNull<int>(reader[DBParam.Output.MonthID]),
                            YearID = Utility.CheckDbNull<int>(reader[DBParam.Output.YearId]),
                            FixedGross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.FixedGross]),
                            WorkingDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.No_of_days_payable]),
                            Basic = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]), 0),
                            HRA = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]), 0),
                            SplAllow = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.SplAllow]), 0),
                            EducationAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]), 0),
                            PaperMagazine =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]), 0),
                            OverTime = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OverTime]), 0),
                            MonsoonAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]), 0),
                            MedicalAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]), 0),
                            Conveyance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]), 0),
                            OtherEarnings = Math.Round(Convert.ToDecimal(reader[DBParam.Output.OtherEarning]), 0),
                            PaidGross = Math.Round(Convert.ToDecimal(reader[DBParam.Output.PaidGross]), 0),
                            EEPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEPF])),
                            EEESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEESI])),
                            TDS = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS]), 0),
                            PT = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PT]), 0),
                            Loans = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Loans]), 0),
                            Deduction = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherDeductions]),
                                0),
                            Gross = Math.Round(Convert.ToDecimal(reader[DBParam.Output.Gross]), 0),
                            ERPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERPF])),
                            ERESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERESI])),
                            Bonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Bonus]), 0),
                            Gratuity = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gratuity])),
                            PLI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PLI]), 0),
                            MobileDataCard =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Mobile_Datacard]), 0),
                            Medicalclaim =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Mediclaim]), 0),
                            OtherPerks = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherPerks]), 0),
                            IsProcessed = Convert.ToBoolean(reader[DBParam.Output.IsProcessed]),
                            IsApproved = Convert.ToBoolean(reader[DBParam.Output.IsApproved]),
                            PSBonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PSBonus]), 0)
                        };

                        payrollBo.NetSalary = Math.Round(payrollBo.PaidGross + payrollBo.PLI + payrollBo.MobileDataCard + payrollBo.OtherEarnings + payrollBo.OtherPerks + payrollBo.MonsoonAllowance
                                                         - (payrollBo.EEESI + payrollBo.EEPF + payrollBo.TDS + payrollBo.PT + payrollBo.Deduction));

                        if (payrollBo.Loans > payrollBo.NetSalary)
                        {
                            payrollBo.Loans = payrollBo.NetSalary;
                            payrollBo.NetSalary = payrollBo.NetSalary - payrollBo.Loans;
                        }
                        else
                        {
                            payrollBo.NetSalary = payrollBo.NetSalary - payrollBo.Loans;
                        }

                        payrollBo.CTC = Math.Round(payrollBo.PaidGross + payrollBo.OtherPerks + payrollBo.OtherEarnings
                                                   + Math.Round(payrollBo.Bonus, 0) + payrollBo.OverTime + payrollBo.MonsoonAllowance
                                                   + payrollBo.Medicalclaim + payrollBo.PLI + payrollBo.ERESI
                                                   + payrollBo.ERPF + payrollBo.Gratuity + payrollBo.MobileDataCard);
                        payrollBo.BaseLocationID = Utility.CheckDbNull<int>(reader[DBParam.Output.BaseLocationID]);
                        payrollBo.Operation = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]);
                        payrollBo.Department = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]);
                        payrollBo.DOB = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOB]);
                        payrollBo.AccountNo = Utility.CheckDbNull<string>(reader[DBParam.Output.AccountNo]);
                        payrollBo.IFSCCode = Utility.CheckDbNull<string>(reader[DBParam.Output.IFSCCode]);
                        payrollBo.PANNo = Utility.CheckDbNull<string>(reader[DBParam.Output.PANNO]);
                        payrollBo.BeneID = Utility.CheckDbNull<string>(reader[DBParam.Output.BeneID]);

                        //=======================
                        payrollBo.IsEducationAllow = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEducationAllow]);
                        payrollBo.IsOverTime = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOverTime]);
                        payrollBo.IsEEPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEPF]);
                        payrollBo.IsEEESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEESI]);
                        payrollBo.IsPT = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPT]);
                        payrollBo.IsTDS = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsTDS]);
                        payrollBo.IsERPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERPF]);
                        payrollBo.IsERESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERESI]);
                        payrollBo.IsGratuity = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsGratuity]);
                        payrollBo.IsPLI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPLI]);
                        payrollBo.IsMediclaim = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMediclaim]);
                        payrollBo.IsMobileDatacard = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMobileDatacard]);
                        payrollBo.IsOtherPerks = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOtherPerks]);
                        //=======================
                        payrollBoList.Add(payrollBo);
                    }
                }
            }

            return payrollBoList;
        }

        public DataTable ExportEmployeeComponent(int monthId, int yearId, string businessUnitList, int? department, int? statusId)
        {
            DataSet ds = new DataSet();
            DataTable dtEmployeeComponent = new DataTable("EmployeeComponent");
            ds.Tables.Add(dtEmployeeComponent);

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.EXPORT_EMPLOYEEPAYROLLCOMPONENT);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, department);
                Database.AddInParameter(dbCommand, DBParam.Input.StatusID, DbType.Int32, statusId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtEmployeeComponent);
                }

                return dtEmployeeComponent;
            }
        }

        #endregion Search Payroll

        #region GetPayroll

        public PayrollBO GetPayroll(int employeeId, int monthId, int yearId)
        {
            PayrollBO payrollBo = new PayrollBO();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETEMPLOYEEPAYROLL);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, employeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        payrollBo.EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]);
                        payrollBo.EmployeePayStructureID = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeePaystructureID]);
                        payrollBo.Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]);
                        payrollBo.EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]);
                        payrollBo.MonthID = Utility.CheckDbNull<int>(reader[DBParam.Output.MonthID]);
                        payrollBo.Month = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]);
                        payrollBo.YearID = Utility.CheckDbNull<int>(reader[DBParam.Output.YearId]);
                        payrollBo.Gross = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gross]), 0);
                        payrollBo.Basic = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]), 0);
                        payrollBo.HRA = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]), 0);
                        payrollBo.MedicalAllowance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]), 0);
                        payrollBo.SplAllow = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.SplAllow]), 0);
                        payrollBo.EducationAllowance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]), 0);
                        payrollBo.PaperMagazine = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]), 0);
                        payrollBo.MonsoonAllowance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]), 0);
                        payrollBo.OverTime = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OverTime]), 0);
                        payrollBo.Conveyance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]), 0);
                        payrollBo.EEPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEPF]));
                        payrollBo.EEESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEESI]));
                        payrollBo.PT = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PT]), 0);
                        payrollBo.TDS = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS]), 0);
                        payrollBo.ERPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERPF]));
                        payrollBo.ERESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERESI]));
                        payrollBo.Bonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Bonus]), 0);
                        payrollBo.NetSalary = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.NetSalary]), 0);
                        payrollBo.PSBonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PSBonus]), 0);
                        payrollBo.Gratuity = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gratuity]));
                        payrollBo.PLI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PLI]), 0);
                        payrollBo.Medicalclaim = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Medicalclaim]), 0);
                        payrollBo.MobileDataCard = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MobileDataCard]), 0);
                        payrollBo.OtherEarnings = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherEarning]), 0);
                        payrollBo.OtherPerks = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherPerks]), 0);
                        payrollBo.Deduction = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Deduction]), 0);
                        payrollBo.Loans = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Loans]), 0);
                        payrollBo.IsProcessed = Convert.ToBoolean(reader[DBParam.Output.IsProcessed]);
                        payrollBo.IsApproved = Convert.ToBoolean(reader[DBParam.Output.IsApproved]);
                        payrollBo.NetSalary = Math.Round(payrollBo.Gross + payrollBo.PLI + payrollBo.MobileDataCard + payrollBo.OtherEarnings + payrollBo.OtherPerks + payrollBo.OverTime + payrollBo.MonsoonAllowance
                                                         - (payrollBo.EEESI + payrollBo.EEPF + payrollBo.TDS + payrollBo.PT + payrollBo.Deduction));

                        if (payrollBo.Loans > payrollBo.NetSalary)
                        {
                            payrollBo.Loans = payrollBo.NetSalary;
                            payrollBo.NetSalary = payrollBo.NetSalary - payrollBo.Loans;
                        }
                        else
                        {
                            payrollBo.NetSalary = payrollBo.NetSalary - payrollBo.Loans;
                        }

                        payrollBo.CTC = Math.Round(payrollBo.Gross + payrollBo.OtherPerks + payrollBo.OtherEarnings
                                                   + Math.Round(payrollBo.Bonus, 0) + payrollBo.OverTime + payrollBo.MonsoonAllowance
                                                   + payrollBo.Medicalclaim + payrollBo.PLI + payrollBo.ERESI
                                                   + payrollBo.ERPF + payrollBo.Gratuity + payrollBo.MobileDataCard);
                        payrollBo.BaseLocationID = Utility.CheckDbNull<int>(reader[DBParam.Output.BaseLocationID]);
                        payrollBo.WorkingDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.No_of_days_payable]);
                        payrollBo.IsESIRequired = Utility.CheckDbNull<int>(reader[DBParam.Output.IsESIRequired]);
                        //=======================
                        payrollBo.IsEducationAllow = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEducationAllow]);
                        payrollBo.IsOverTime = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOverTime]);
                        payrollBo.IsEEPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEPF]);
                        payrollBo.IsEEESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEESI]);
                        payrollBo.IsPT = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPT]);
                        payrollBo.IsTDS = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsTDS]);
                        payrollBo.IsERPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERPF]);
                        payrollBo.IsERESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERESI]);
                        payrollBo.IsGratuity = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsGratuity]);
                        payrollBo.IsPLI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPLI]);
                        payrollBo.IsMediclaim = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMediclaim]);
                        payrollBo.IsMobileDatacard = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMobileDatacard]);
                        payrollBo.IsOtherPerks = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOtherPerks]);
                        //=======================
                    }
                }
            }

            return payrollBo;
        }

        public string GetPayrollComponentConfigValue(string businessUnitIDs)
        {
            string result = string.Empty;

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_PAYROLLCOMPONENTCONFIGVALUE);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, businessUnitIDs);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        result = Utility.CheckDbNull<string>(reader[DBParam.Output.Result]);
                    }
                }
            }

            return result;
        }

        #endregion GetPayroll

        #region Payroll Configuration

        public PayrollBO GetPayrollConfiguration(PayrollBO payrollBo)
        {
            using (FSMEntities context = new FSMEntities())
            {
                List<PayrollBO> configList = (from t in context.tbl_PayrollConfiguration
                                              join emp in context.tbl_EmployeeMaster on t.ModifiedBy equals emp.ID
                                              join bu in context.tbl_BusinessUnit on emp.BaseLocationID equals bu.ID into business
                                              from bUnit in business.DefaultIfEmpty()
                                              where t.DomainID == payrollBo.DomainId && payrollBo.BusinessUnitID == t.BusinessUnitID
                                              select new PayrollBO
                                              {
                                                  Component = t.Components,
                                                  Limit = t.Limit,
                                                  Percentage = t.Percentage,
                                                  Value = t.Value,
                                                  ModifiedByName = emp.FullName,
                                                  ModifiedOn = t.ModifiedOn,
                                                  BaseLocation = bUnit.Name
                                              }).ToList();
                if (configList.Count() != 0)
                {
                    payrollBo.ModifiedByName = configList[0].ModifiedByName;
                    payrollBo.ModifiedOn = configList[0].ModifiedOn;
                    payrollBo.BaseLocation = configList[0].BaseLocation;
                }

                if (configList.Count().Equals(0))
                {
                    payrollBo.InsertFlag = true;
                    payrollBo.EmployeePFComponent = "EmployeePF";
                    payrollBo.EmployerPFComponent = "EmployerPF";
                    payrollBo.EmployeeESIComponent = "EmployeeESI";
                    payrollBo.EmployerESIComponent = "EmployerESI";
                }

                foreach (PayrollBO item in configList)
                {
                    if (item.Component.ToUpper().Equals("EMPLOYEEPF"))
                    {
                        payrollBo.EmployeePFComponent = item.Component;
                        payrollBo.EmployeePFPercentage = decimal.Round(Convert.ToDecimal(item.Percentage), 2);
                        payrollBo.EmployeePFLimit = decimal.Round(Convert.ToDecimal(item.Limit), 2);
                        payrollBo.EmployeePFValue = decimal.Round(Convert.ToDecimal(item.Value), 2);
                    }
                    else if (item.Component.ToUpper().Equals("EMPLOYERPF"))
                    {
                        payrollBo.EmployerPFComponent = item.Component;
                        payrollBo.EmployerPFPercentage = decimal.Round(Convert.ToDecimal(item.Percentage), 2);
                        payrollBo.EmployerPFLimit = decimal.Round(Convert.ToDecimal(item.Limit), 2);
                        payrollBo.EmployerPFValue = decimal.Round(Convert.ToDecimal(item.Value), 2);
                    }
                    else if (item.Component.ToUpper().Equals("EMPLOYEEESI"))
                    {
                        payrollBo.EmployeeESIComponent = item.Component;
                        payrollBo.EmployeeESIPercentage = decimal.Round(Convert.ToDecimal(item.Percentage), 2);
                        payrollBo.EmployeeESILimit = decimal.Round(Convert.ToDecimal(item.Limit), 2);
                        payrollBo.EmployeeESIValue = decimal.Round(Convert.ToDecimal(item.Value), 2);
                    }
                    else
                    {
                        payrollBo.EmployerESIComponent = item.Component;
                        payrollBo.EmployerESIPercentage = decimal.Round(Convert.ToDecimal(item.Percentage), 2);
                        payrollBo.EmployerESILimit = decimal.Round(Convert.ToDecimal(item.Limit), 2);
                        payrollBo.EmployerESIValue = decimal.Round(Convert.ToDecimal(item.Value), 2);
                    }
                }

                return payrollBo;
            }
        }

        public string ManagePayrollConfiguration(PayrollBO managePayrollConfig)
        {
            string successMessage = string.Empty;

            using (FSMEntities context = new FSMEntities())
            {
                string[] configList = { "EMPLOYEEPF", "EMPLOYERPF", "EMPLOYEEESI", "EMPLOYERESI" };
                if (!context.tbl_PayrollConfiguration.Any(a => a.DomainID == managePayrollConfig.DomainId && a.BusinessUnitID == managePayrollConfig.BusinessUnitID))
                {
                    foreach (string item in configList)
                    {
                        tbl_PayrollConfiguration tblPayrollConfiguration = new tbl_PayrollConfiguration();
                        FillPayrollConfigurtionClass(tblPayrollConfiguration, managePayrollConfig, true, item);
                        context.tbl_PayrollConfiguration.Add(tblPayrollConfiguration);
                        context.SaveChanges();
                        successMessage = Notification.Inserted;
                    }
                }
                else
                {
                    foreach (string item in configList)
                    {
                        tbl_PayrollConfiguration tblPayrollConfiguration = context.tbl_PayrollConfiguration.FirstOrDefault(a => a.DomainID == managePayrollConfig.DomainId && a.Components == item && a.BusinessUnitID == managePayrollConfig.BusinessUnitID);
                        FillPayrollConfigurtionClass(tblPayrollConfiguration, managePayrollConfig, false, item);
                        context.Entry(tblPayrollConfiguration).State = EntityState.Modified;
                        context.SaveChanges();
                        successMessage = Notification.Updated;
                    }
                }

                return successMessage;
            }
        }

        private void FillPayrollConfigurtionClass(tbl_PayrollConfiguration tblPayrollConfiguration, PayrollBO managePayrollConfig, bool insertFlag, string configName)
        {
            if (configName.Equals("EMPLOYEEPF"))
            {
                tblPayrollConfiguration.Components = managePayrollConfig.EmployeePFComponent;
                tblPayrollConfiguration.Percentage = managePayrollConfig.EmployeePFPercentage;
                tblPayrollConfiguration.Value = managePayrollConfig.EmployeePFValue;
                tblPayrollConfiguration.Limit = managePayrollConfig.EmployeePFLimit;
            }
            else if (configName.Equals("EMPLOYERPF"))
            {
                tblPayrollConfiguration.Components = managePayrollConfig.EmployerPFComponent;
                tblPayrollConfiguration.Percentage = managePayrollConfig.EmployerPFPercentage;
                tblPayrollConfiguration.Value = managePayrollConfig.EmployerPFValue;
                tblPayrollConfiguration.Limit = managePayrollConfig.EmployerPFLimit;
            }
            else if (configName.Equals("EMPLOYEEESI"))
            {
                tblPayrollConfiguration.Components = managePayrollConfig.EmployeeESIComponent;
                tblPayrollConfiguration.Percentage = managePayrollConfig.EmployeeESIPercentage;
                tblPayrollConfiguration.Value = managePayrollConfig.EmployeeESIValue;
                tblPayrollConfiguration.Limit = managePayrollConfig.EmployeeESILimit;
            }
            else
            {
                tblPayrollConfiguration.Components = managePayrollConfig.EmployerESIComponent;
                tblPayrollConfiguration.Percentage = managePayrollConfig.EmployerESIPercentage;
                tblPayrollConfiguration.Value = managePayrollConfig.EmployerESIValue;
                tblPayrollConfiguration.Limit = managePayrollConfig.EmployerESILimit;
            }

            tblPayrollConfiguration.ModifiedBy = managePayrollConfig.ModifiedBy;
            tblPayrollConfiguration.ModifiedOn = DateTime.Now;
            tblPayrollConfiguration.DomainID = managePayrollConfig.DomainId;

            if (insertFlag)
            {
                tblPayrollConfiguration.BusinessUnitID = managePayrollConfig.BusinessUnitID;
                tblPayrollConfiguration.CreatedBy = managePayrollConfig.CreatedBy;
                tblPayrollConfiguration.CreatedOn = DateTime.Now;
                tblPayrollConfiguration.ModifiedBy = managePayrollConfig.ModifiedBy;
                tblPayrollConfiguration.ModifiedOn = DateTime.Now;
                tblPayrollConfiguration.HistoryID = Guid.NewGuid();
            }
        }

        #endregion Payroll Configuration

        #region Report

        public List<PayrollBO> Rpt_PayrollMonthly(string businessUnitList, int? deparmentId, string component, int financialYearId)
        {
            List<PayrollBO> boList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_PAYROLLMONTHLY);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, deparmentId == null ? 0 : deparmentId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.Component, DbType.String, component);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, financialYearId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Component]),
                            Month = Utility.CheckDbNull<string>(reader[DBParam.Output.Month])
                        };
                        boList.Add(payrollBo);
                    }
                }
            }

            return boList;
        }

        public DataTable Rpt_searchemployeepayroll(string businessUnitList, int? departmentId, string component, int financialYearId)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.Rpt_searchemployeepayroll);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, departmentId ?? 0);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.Component, DbType.String, component);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, financialYearId);

                DataSet ds = new DataSet();
                DataTable dtSalesTargetReport = new DataTable("EmployeepayrollReport");
                ds.Tables.Add(dtSalesTargetReport);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    ds.Load(reader, LoadOption.OverwriteChanges, dtSalesTargetReport);
                }

                return dtSalesTargetReport;
            }
        }

        public List<PayrollBO> RPT_SearchPayrollByMonthly(int monthId, int yearId, string businessUnitList, int? department)
        {
            List<PayrollBO> payrollBoList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_SEARCHPAYROLLBYMONTHLY);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, department);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            EmployeeCode = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeCode]),
                            EmployeeNo = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeNo]),
                            BusinessUnitIDs = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Month = Utility.CheckDbNull<string>(reader[DBParam.Output.MonthYear]),
                            DOJ = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOJ]),
                            WorkingDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.No_of_days_payable]),
                            FixedGross = Math.Round(Convert.ToDecimal(reader[DBParam.Output.FixedGross]), 0),
                            Gross = Math.Round(Convert.ToDecimal(reader[DBParam.Output.Gross]), 0),
                            Basic = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]), 0),
                            HRA = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]), 0),
                            MedicalAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]), 0),
                            Conveyance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]), 0),
                            SplAllow = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.SplAllow]), 0),
                            OverTime = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OverTime]), 0),
                            EducationAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]), 0),
                            PaperMagazine =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]), 0),
                            MonsoonAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]), 0),
                            OtherEarnings = Math.Round(Convert.ToDecimal(reader[DBParam.Output.OtherEarning]), 0),
                            EEPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEPF])),
                            EEESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEESI])),
                            TDS = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS]), 0),
                            PT = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PT]), 0),
                            Loans = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Loans]), 0),
                            Deduction = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherDeductions]),
                                0),
                            ERPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERPF])),
                            ERESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERESI])),
                            Bonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Bonus]), 0),
                            Gratuity = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gratuity])),
                            PLI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PLI]), 0),
                            MobileDataCard =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Mobile_Datacard]), 0),
                            Medicalclaim =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Mediclaim]), 0),
                            OtherPerks = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherPerks]), 0),
                            NetSalary = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.NetSalary]), 0),
                            CTC = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.CTC]), 0),
                            LOPDays = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.LOPDays]), 0),
                            IsEducationAllow = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEducationAllow]),
                            IsOverTime = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOverTime]),
                            IsEEPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEPF]),
                            IsEEESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEESI]),
                            IsPT = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPT]),
                            IsTDS = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsTDS]),
                            IsERPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERPF]),
                            IsERESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERESI]),
                            IsGratuity = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsGratuity]),
                            IsPLI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPLI]),
                            IsMediclaim = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMediclaim]),
                            IsMobileDatacard = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMobileDatacard]),
                            IsOtherPerks = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOtherPerks])
                        };
                        //=======================
                        //=======================
                        payrollBoList.Add(payrollBo);
                    }
                }
            }

            return payrollBoList;
        }

        public List<PayrollBO> RPT_SearchComponentsByMonthly(int monthId, int yearId, string businessUnitList, int? department)
        {
            List<PayrollBO> payrollBoList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.RPT_SEARCHPAYROLLCOMPONENTS);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.MonthID, DbType.Int32, monthId);
                Database.AddInParameter(dbCommand, DBParam.Input.YearID, DbType.Int32, yearId);
                Database.AddInParameter(dbCommand, DBParam.Input.Location, DbType.String, businessUnitList);
                Database.AddInParameter(dbCommand, DBParam.Input.DepartmentID, DbType.Int32, department);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payrollBo = new PayrollBO
                        {
                            BusinessUnitIDs = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Operation = Utility.CheckDbNull<string>(reader[DBParam.Output.DepartmentName]),
                            WorkingDays = Utility.CheckDbNull<decimal>(reader[DBParam.Output.No_of_days_payable]),
                            FixedGross = Math.Round(Convert.ToDecimal(reader[DBParam.Output.FixedGross]), 0),
                            Gross = Math.Round(Convert.ToDecimal(reader[DBParam.Output.Gross]), 0),
                            Basic = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Basic]), 0),
                            HRA = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.HRA]), 0),
                            MedicalAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MedicalAllowance]), 0),
                            Conveyance = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Conveyance]), 0),
                            SplAllow = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.SplAllow]), 0),
                            OverTime = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OverTime]), 0),
                            EducationAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EducationAllowance]), 0),
                            PaperMagazine =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PaperMagazine]), 0),
                            MonsoonAllowance =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.MonsoonAllow]), 0),
                            OtherEarnings = Math.Round(Convert.ToDecimal(reader[DBParam.Output.OtherEarning]), 0),
                            EEPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEPF])),
                            EEESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.EEESI])),
                            TDS = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.TDS]), 0),
                            PT = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PT]), 0),
                            Loans = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Loans]), 0),
                            Deduction = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherDeductions]),
                                0),
                            ERPF = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERPF])),
                            ERESI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.ERESI])),
                            Bonus = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Bonus]), 0),
                            Gratuity = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Gratuity])),
                            PLI = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.PLI]), 0),
                            MobileDataCard =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Mobile_Datacard]), 0),
                            Medicalclaim =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Mediclaim]), 0),
                            OtherPerks = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.OtherPerks]), 0),
                            NetSalary = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.NetSalary]), 0),
                            CTC = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.CTC]), 0),
                            IsEducationAllow = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEducationAllow]),
                            IsOverTime = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOverTime]),
                            IsEEPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEPF]),
                            IsEEESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsEEESI]),
                            IsPT = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPT]),
                            IsTDS = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsTDS]),
                            IsERPF = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERPF]),
                            IsERESI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsERESI]),
                            IsGratuity = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsGratuity]),
                            IsPLI = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsPLI]),
                            IsMediclaim = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMediclaim]),
                            IsMobileDatacard = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsMobileDatacard]),
                            IsOtherPerks = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsOtherPerks])
                        };
                        //=======================
                        //=======================
                        payrollBoList.Add(payrollBo);
                    }
                }
            }

            return payrollBoList;
        }

        #endregion Report

        #region PT Configuration

        public List<PayrollBO> SearchPtConfiguration(PayrollBO payrollBo)
        {
            List<PayrollBO> payrollBoList = new List<PayrollBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_PTCONFIGURATION);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, payrollBo.BusinessUnitID);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, payrollBo.FinancialYearID);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PayrollBO payBo = new PayrollBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            MinGross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MinGross]),
                            MaxGross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MaxGross]),
                            Gross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]),
                            BusinessUnitIDs = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            FinancialYear = Utility.CheckDbNull<string>(reader[DBParam.Output.FinancialYear])
                        };
                        payrollBoList.Add(payBo);
                    }
                }
            }

            return payrollBoList;
        }

        public PayrollBO GetPtConfiguration(int id)
        {
            PayrollBO payrollBo = new PayrollBO();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_PTCONFIGURATION);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        payrollBo.Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]);
                        payrollBo.MinGross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MinGross]);
                        payrollBo.MaxGross = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MaxGross]);
                        payrollBo.Amount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount]);
                        payrollBo.BusinessUnitID = Utility.CheckDbNull<int>(reader[DBParam.Output.BaseLocationID]);
                        payrollBo.FinancialYearID = Utility.CheckDbNull<int>(reader[DBParam.Output.FinancialYear]);
                        payrollBo.ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]);
                        payrollBo.ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]);
                        payrollBo.BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation]);
                    }
                }
            }

            return payrollBo;
        }

        public string ManagePtConfiguration(PayrollBO payrollBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_PTCONFIGURATION);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, payrollBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.MinGross, DbType.Decimal, payrollBo.MinGross);
                Database.AddInParameter(dbCommand, DBParam.Input.MaxGross, DbType.Decimal, payrollBo.MaxGross);
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Decimal, payrollBo.Amount);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, payrollBo.BusinessUnitID);
                Database.AddInParameter(dbCommand, DBParam.Input.FinancialYearID, DbType.Int32, payrollBo.FinancialYearID);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, payrollBo.IsDeleted);
                payrollBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return payrollBo.UserMessage;
        }

        #endregion PT Configuration
    }
}