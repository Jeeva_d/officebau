﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Services
{
    public class HrmsDashboardRepository : BaseRepository
    {
        #region Attrition

        public List<HrmsDashboardBo> SearchAttrition()
        {
            List<HrmsDashboardBo> hrmsDashboardBoList = new List<HrmsDashboardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_ATTRITION);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    HrmsDashboardBo hrmsDashboardBO;
                    while (reader.Read())
                    {
                        hrmsDashboardBO = new HrmsDashboardBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Attrition = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Attrition])
                        };
                        hrmsDashboardBoList.Add(hrmsDashboardBO);
                    }
                }
            }

            return hrmsDashboardBoList;
        }

        #endregion Attrition

        #region Top Paid Monthly

        public List<HrmsDashboardBo> SearchTopPaidMonthly(int amount, int businessUnitId)
        {
            List<HrmsDashboardBo> hrmsDashboardBoList = new List<HrmsDashboardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_TOPPAIDMONTHLY);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.Amount, DbType.Int32, amount);
                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnitID, DbType.Int32, businessUnitId);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    HrmsDashboardBo hrmsDashboardBO;
                    while (reader.Read())
                    {
                        hrmsDashboardBO = new HrmsDashboardBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            Compensation = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Amount])
                        };
                        hrmsDashboardBoList.Add(hrmsDashboardBO);
                    }
                }
            }

            return hrmsDashboardBoList;
        }

        #endregion Top Paid Monthly

        #region Headcount

        public HrmsDashboardBo GetDashboardTiles()
        {
            HrmsDashboardBo hrmsDashboardBo = new HrmsDashboardBo();
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.HRMS_DASHBOARDTILES);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        hrmsDashboardBo = new HrmsDashboardBo
                        {
                            Headcount = Utility.CheckDbNull<int>(reader[DBParam.Output.Headcount]),
                            CurrentlyHired = Utility.CheckDbNull<int>(reader[DBParam.Output.CurrentlyHired]),
                            Compensation =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Compensation])),
                            Attrition = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Attrition])
                        };
                    }
                }
            }
            return hrmsDashboardBo;
        }

        public List<HrmsDashboardBo> SearchHeadcount()
        {
            List<HrmsDashboardBo> hrmsDashboardBoList = new List<HrmsDashboardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_HEADCOUNT);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    HrmsDashboardBo hrmsDashboardBO;
                    while (reader.Read())
                    {
                        hrmsDashboardBO = new HrmsDashboardBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Headcount = Utility.CheckDbNull<int>(reader[DBParam.Output.Headcount])
                        };
                        hrmsDashboardBoList.Add(hrmsDashboardBO);
                    }
                }
            }

            return hrmsDashboardBoList;
        }

        public List<HrmsDashboardBo> GetHeadcountByBu(string businessUnit, string reportType)
        {
            List<HrmsDashboardBo> hrmsDashboardBoList = new List<HrmsDashboardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand;
                if (reportType.ToLower() == "department")
                {
                    dbCommand = Database.GetStoredProcCommand(Constants.GET_HEADCOUNTBYBU);
                }
                else
                {
                    dbCommand = Database.GetStoredProcCommand(Constants.GET_HEADCOUNTOFDESIGNATIONBYBU);
                }

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, businessUnit ?? "");
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    HrmsDashboardBo hrmsDashboardBO;
                    while (reader.Read())
                    {
                        hrmsDashboardBO = new HrmsDashboardBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Headcount = Utility.CheckDbNull<int>(reader[DBParam.Output.Headcount]),
                            ReportType = reportType.ToLower()
                        };
                        hrmsDashboardBoList.Add(hrmsDashboardBO);
                    }
                }
            }

            return hrmsDashboardBoList;
        }

        public int GetTopRangeValue()
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                HrmsDashboardBo list = (from t in context.tbl_Pay_EmployeePayroll
                                        join a in context.tbl_Pay_EmployeePayrollDetails on t.Id equals a.PayrollId
                                        where !(t.IsDeleted) && t.DomainId == domainId
                                        orderby a.Amount descending
                                        select new HrmsDashboardBo
                                        {
                                            Compensation = a.Amount ?? 0,
                                            Id = t.Id
                                        }).FirstOrDefault();

                int roundTo = 1000;

                int topRangeValue = list == null ? 0 : (int)(((double)list.Compensation + 0.5 * roundTo) / roundTo) * roundTo;
                return topRangeValue;
            }
        }

        #endregion Headcount

        #region Compensation

        public List<HrmsDashboardBo> SearchCompensation()
        {
            List<HrmsDashboardBo> hrmsDashboardBoList = new List<HrmsDashboardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_COMPENSATION);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    HrmsDashboardBo hrmsDashboardBO;
                    while (reader.Read())
                    {
                        hrmsDashboardBO = new HrmsDashboardBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Compensation = Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Compensation]))
                        };
                        hrmsDashboardBoList.Add(hrmsDashboardBO);
                    }
                }
            }

            return hrmsDashboardBoList;
        }

        public List<HrmsDashboardBo> GetCompensationByBu(string businessUnit, string reportType)
        {
            List<HrmsDashboardBo> hrmsDashboardBoList = new List<HrmsDashboardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand;
                if (reportType.ToLower() == "department")
                {
                    dbCommand = Database.GetStoredProcCommand(Constants.GET_COMPENSATIONBYBU);
                }
                else
                {
                    dbCommand = Database.GetStoredProcCommand(Constants.GET_COMPENSATIONOFDESIGNATIONBYBU);
                }

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, businessUnit ?? "");
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    HrmsDashboardBo hrmsDashboardBO;
                    while (reader.Read())
                    {
                        hrmsDashboardBO = new HrmsDashboardBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader[DBParam.Output.BusinessUnit]),
                            Compensation =
                                Math.Round(Utility.CheckDbNull<decimal>(reader[DBParam.Output.Compensation])),
                            ReportType = reportType.ToLower()
                        };
                        hrmsDashboardBoList.Add(hrmsDashboardBO);
                    }
                }
            }

            return hrmsDashboardBoList;
        }

        #endregion Compensation

        public List<SelectListItem> GetPayrollComponentDropDown()
        {
            List<SelectListItem> dependantDropDownList = new List<SelectListItem>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PAY_SEARCHPAYROLLCOMPONENENTS);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                dependantDropDownList.Add(new SelectListItem { Text = Utility.CheckDbNull<string>(Constants.SELECT), Value = "" });
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        dependantDropDownList.Add(new SelectListItem
                        {
                            Text = Utility.CheckDbNull<string>(reader[DBParam.Output.Code]),
                            Value = "" + Utility.CheckDbNull<int>(reader[DBParam.Output.ID])
                        });
                    }
                }
            }

            return dependantDropDownList;
        }

        public string ManageDashBoarrdConfig(string Key, string value)
        {
            string msg = string.Empty;
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("ManageDashBoardConfig");

                Database.AddInParameter(dbCommand, "key", DbType.String, Key);
                Database.AddInParameter(dbCommand, "Value", DbType.String, value);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                msg = Database.ExecuteScalar(dbCommand).ToString();
            }

            return msg;
        }

        public List<HrmsDashboardBo> SearchDashboardConfig()
        {
            List<HrmsDashboardBo> hrmsDashboardBoList = new List<HrmsDashboardBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand("SearchDashboardConfig");

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    HrmsDashboardBo hrmsDashboardBO;
                    while (reader.Read())
                    {
                        hrmsDashboardBO = new HrmsDashboardBo
                        {
                            UserMessage = Utility.CheckDbNull<string>(reader["Key"]),
                            ReportType = Utility.CheckDbNull<string>(reader["Value"]),
                            UserName = Utility.CheckDbNull<string>(reader[DBParam.Output.Description])
                        };

                        hrmsDashboardBoList.Add(hrmsDashboardBO);
                    }
                }
            }

            return hrmsDashboardBoList;
        }
    }
}