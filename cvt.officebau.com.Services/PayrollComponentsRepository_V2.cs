﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class PayrollComponentsRepositoryV2 : BaseRepository
    {
        public List<PayrollComponentsBo_V2> SearchPayrollComponentList(PayrollComponentsBo_V2 payrollComponentsBo)
        {
            List<PayrollComponentsBo_V2> componentList = new List<PayrollComponentsBo_V2>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PAY_SEARCHPAYROLLCOMPONENENTS);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        componentList.Add(new PayrollComponentsBo_V2
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Code = Utility.CheckDbNull<string>(reader[DBParam.Output.Code]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Ordinal = Utility.CheckDbNull<int>(reader[DBParam.Output.Ordinal]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn])
                        });
                    }
                }
            }

            return componentList;
        }

        public PayrollComponentsBo_V2 GetPayrollComponent(int id)
        {
            PayrollComponentsBo_V2 payrollComponentsBo = new PayrollComponentsBo_V2();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PAY_GETPAYROLLCOMPONENENT);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        payrollComponentsBo = new PayrollComponentsBo_V2
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Code = Utility.CheckDbNull<string>(reader[DBParam.Output.Code]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            Ordinal = Utility.CheckDbNull<int>(reader[DBParam.Output.Ordinal]),
                        };
                    }
                }
            }

            return payrollComponentsBo;
        }

        public string Pay_ManagePayrollComponents(PayrollComponentsBo_V2 payrollComponentsBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.PAY_MANAGEPAYROLLCOMPONENENTS);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, payrollComponentsBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.Name, DbType.String, payrollComponentsBo.Code);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, payrollComponentsBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.Ordinal, DbType.Int32, payrollComponentsBo.Ordinal);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, payrollComponentsBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                return Database.ExecuteScalar(dbCommand).ToString();
            }
        }
    }
}