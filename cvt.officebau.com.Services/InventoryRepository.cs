﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace cvt.officebau.com.Services
{
    public class InventoryRepository : BaseRepository
    {
        #region Stock Distribution

        public List<InventoryBO> SearchStockDistribution(InventoryBO inventoryBo)
        {
            List<InventoryBO> listOfMenu = new List<InventoryBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_STOCKDISTRIBUTION);

                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, inventoryBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        listOfMenu.Add(new InventoryBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            CostCenterID = Utility.CheckDbNull<int>(reader[DBParam.Output.CostCenterID]),
                            CostCenter = Utility.CheckDbNull<string>(reader[DBParam.Output.CostCenter]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            ProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.ProductID]),
                            ProductName = Utility.CheckDbNull<string>(reader[DBParam.Output.ProductName]),
                            DistributedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DistributedDate]),
                            Qty = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                        });
                    }
                }
            }

            return listOfMenu;
        }

        public InventoryBO GetStockDistributionDetails(int id)
        {
            InventoryBO inventoryBo = new InventoryBO();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_STOCKDISTRIBUTIONDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        inventoryBo = new InventoryBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            CostCenterID = Utility.CheckDbNull<int>(reader[DBParam.Output.CostCenterID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID]),
                            EmployeeName = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            Party = Utility.CheckDbNull<string>(reader[DBParam.Output.Party]),
                            IsInternal = Utility.CheckDbNull<bool>(reader[DBParam.Output.IsInternal]),
                            ProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.ProductID]),
                            ProductName = Utility.CheckDbNull<string>(reader[DBParam.Output.ProductName]),
                            DistributedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DistributedDate]),
                            Qty = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                        };
                    }
                }
            }
            return inventoryBo;
        }

        public string ManageStockDistribution(InventoryBO inventoryBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_STOCKDISTRIBUTION);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, inventoryBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, inventoryBo.EmployeeId);
                Database.AddInParameter(dbCommand, DBParam.Input.ProductID, DbType.Int32, inventoryBo.ProductID);
                Database.AddInParameter(dbCommand, DBParam.Input.Qty, DbType.Int32, inventoryBo.Qty);
                Database.AddInParameter(dbCommand, DBParam.Input.CostCenterID, DbType.Int32, inventoryBo.CostCenterID);
                Database.AddInParameter(dbCommand, DBParam.Input.Party, DbType.String, inventoryBo.Party);
                Database.AddInParameter(dbCommand, DBParam.Input.IsInternal, DbType.Boolean, inventoryBo.IsInternal);
                Database.AddInParameter(dbCommand, DBParam.Input.DistributedDate, DbType.DateTime, inventoryBo.DistributedDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Remarks, DbType.String, inventoryBo.Remarks);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, inventoryBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                inventoryBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return inventoryBo.UserMessage;
        }

        public List< cvt.officebau.com.ViewModels.Entity> SearchAutoCompleteEmployee(string employeeName)
        {
            int domainId = Utility.DomainId();

            using (FSMEntities context = new FSMEntities())
            {
                List< cvt.officebau.com.ViewModels.Entity> list = (from t in context.tbl_EmployeeMaster
                                        where !t.IsDeleted && t.DomainID == domainId && !(t.IsActive ?? false) && t.FullName.Contains(employeeName)
                                        orderby t.FullName
                                        select new  cvt.officebau.com.ViewModels.Entity
                                        {
                                            Name = (t.EmpCodePattern ?? "") + (t.Code ?? "") + " - " + (t.FullName ?? ""),
                                            Id = t.ID
                                        }).ToList();
                return list;
            }
        }

        #endregion Stock Distribution

        #region Stock Adjustment

        public List<InventoryBO> SearchStockAdjustment(InventoryBO inventoryBo)
        {
            List<InventoryBO> listOfMenu = new List<InventoryBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_STOCKADJUSTMENT);

                Database.AddInParameter(dbCommand, DBParam.Input.ProductID, DbType.Int32, inventoryBo.ProductID);
                Database.AddInParameter(dbCommand, DBParam.Input.LedgerID, DbType.Int32, inventoryBo.LedgerID);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.OperationType, DbType.String, inventoryBo.Operation);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        listOfMenu.Add(new InventoryBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            AdjustedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.AdjustedDate]),
                            LedgerID = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID]),
                            LedgerName = Utility.CheckDbNull<string>(reader[DBParam.Output.LedgerName]),
                            AdjustedStock = Utility.CheckDbNull<int>(reader[DBParam.Output.AdjustedQty]),
                            Reason = Utility.CheckDbNull<string>(reader[DBParam.Output.ReasonName]),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            AdjustedBy = Utility.CheckDbNull<string>(reader[DBParam.Output.AdjustedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                        });
                    }
                }
            }

            return listOfMenu;
        }

        public InventoryBO GetStockOnHand(int id)
        {
            InventoryBO inventoryBo = new InventoryBO();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_STOCKONHANDVALUE);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        inventoryBo = new InventoryBO
                        {
                            StockonHand = Utility.CheckDbNull<int>(reader[DBParam.Output.StockonHand])
                        };
                    }
                }

                return inventoryBo;
            }
        }

        public InventoryBO GetStockAdjustmentDetails(int id)
        {
            InventoryBO inventoryBo = new InventoryBO();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_STOCKADJUSTMENTDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        inventoryBo = new InventoryBO
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            AdjustedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.AdjustedDate]),
                            LedgerID = Utility.CheckDbNull<int>(reader[DBParam.Output.LedgerID]),
                            LedgerName = Utility.CheckDbNull<string>(reader[DBParam.Output.LedgerName].ToString()),
                            ReasonID = Utility.CheckDbNull<int>(reader[DBParam.Output.ReasonID]),
                            Reason = Utility.CheckDbNull<string>(reader[DBParam.Output.ReasonName].ToString()),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Remarks].ToString()),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy].ToString()),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                        };
                    }
                }
            }
            inventoryBo.StockAdjustmentDetails = SearchStockAdjustmentDetailsList(id);

            return inventoryBo;
        }

        public List<StockAdjustmentDetailsBO> SearchStockAdjustmentDetailsList(int id)
        {
            List<StockAdjustmentDetailsBO> stockAdjustmentDetailsBoList = new List<StockAdjustmentDetailsBO>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_STOCKADJUSTMENTDETAILSLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.AdjustmentID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                MasterDataRepository masterDataRepository = new MasterDataRepository();
                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        stockAdjustmentDetailsBoList.Add(new StockAdjustmentDetailsBO
                        {
                            ID = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ItemProductList = masterDataRepository.SearchMasterDataDropDown("Product_V2"),
                            ItemProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.ProductID]),
                            ItemStockonHand = Utility.CheckDbNull<int>(reader[DBParam.Output.StockonHand]),
                            AdjustmentID = Utility.CheckDbNull<int>(reader[DBParam.Output.AdjustmentID]),
                            AdjustedQty = Utility.CheckDbNull<int>(reader[DBParam.Output.AdjustedQty]),
                            RemainingQty = Utility.CheckDbNull<int>(reader[DBParam.Output.RemainingQty])
                        });
                        stockAdjustmentDetailsBoList[stockAdjustmentDetailsBoList.Count - 1].ItemProductList.Find(a => a.Value == stockAdjustmentDetailsBoList[stockAdjustmentDetailsBoList.Count - 1].ItemProductID.ToString()).Selected = true;
                    }
                }

                if (stockAdjustmentDetailsBoList.Count == 0)
                {
                    stockAdjustmentDetailsBoList.Add(new StockAdjustmentDetailsBO
                    {
                        ItemProductList = masterDataRepository.SearchMasterDataDropDown("Product_V2")
                    });
                }
            }

            return stockAdjustmentDetailsBoList;
        }

        public string ManageStockAdjustmentDetails(InventoryBO inventoryBo)
        {
            string userMessage = string.Empty;
            string consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            SqlConnection con = new SqlConnection(consString);

            switch (inventoryBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    userMessage = CreateStockAdjustment(inventoryBo, con);
                    break;

                case Operations.Update:
                    userMessage = UpdateStockAdjustment(inventoryBo, con);
                    break;

                case Operations.Delete:
                    userMessage = DeleteStockAdjustment(inventoryBo, con);
                    break;
            }

            return userMessage;
        }

        private string CreateStockAdjustment(InventoryBO inventoryBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.CREATE_STOCKADJUSTMENT)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            FillInvoiceParameters(inventoryBo, cmd);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string UpdateStockAdjustment(InventoryBO inventoryBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.UPDATE_STOCKADJUSTMENT)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            FillInvoiceParameters(inventoryBo, cmd);
            cmd.Parameters.AddWithValue(DBParam.Input.ID, inventoryBo.Id);

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private string DeleteStockAdjustment(InventoryBO inventoryBo, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(Constants.DELETE_STOCKADJUSTMENT)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.ID, inventoryBo.Id);
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());

            con.Open();
            return cmd.ExecuteScalar().ToString();
        }

        private void FillInvoiceParameters(InventoryBO inventoryBo, SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue(DBParam.Input.LedgerID, inventoryBo.LedgerID);
            cmd.Parameters.AddWithValue(DBParam.Input.AdjustedDate, Utility.CheckDbNull<DateTime>(inventoryBo.AdjustedDate));
            cmd.Parameters.AddWithValue(DBParam.Input.ReasonID, inventoryBo.ReasonID);
            cmd.Parameters.AddWithValue(DBParam.Input.Remarks, inventoryBo.Remarks ?? "");
            cmd.Parameters.AddWithValue(DBParam.Input.StockAdjustmentItem, Utility.ToDataTable(inventoryBo.StockAdjustmentDetails, "StockAdjustmentItem"));
            cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
            cmd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
        }

        #endregion Stock Adjustment

        #region Stock Maintenance

        public List<InventoryBO> SearchStockMaintenance(InventoryBO inventoryBo)
        {
            List<InventoryBO> inventoryBoList = new List<InventoryBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_STOCKMAINTENANCE);

                Database.AddInParameter(dbCommand, DBParam.Input.ProductID, DbType.Int32, inventoryBo.ProductID);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        inventoryBoList.Add(new InventoryBO
                        {
                            ProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.ProductID]),
                            ProductName = Utility.CheckDbNull<string>(reader[DBParam.Output.ProductName]),
                            StockonHand = Utility.CheckDbNull<int>(reader[DBParam.Output.StockonHand]),
                            CommitedStock = Utility.CheckDbNull<int>(reader[DBParam.Output.CommitedStock]),
                            AvailableStock = Utility.CheckDbNull<int>(reader[DBParam.Output.AvailableStock]),
                            UOM = Utility.CheckDbNull<string>(reader[DBParam.Output.UOM]),
                            Manufacturer = Utility.CheckDbNull<string>(reader[DBParam.Output.Manufacturer]),
                        });
                    }
                }
            }

            return inventoryBoList;
        }

        public ProductMasterBo GetProductDetails(int productId)
        {
            ProductMasterBo productMasterBo = new ProductMasterBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_STOCKPRODUCTDETAILS);

                Database.AddInParameter(dbCommand, DBParam.Input.ProductID, DbType.Int32, productId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        productMasterBo = new ProductMasterBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Name = Utility.CheckDbNull<string>(reader[DBParam.Output.Name]),
                            SaleRate = Utility.CheckDbNull<decimal>(reader["SalesPrice"]),
                            PurchaseRate = Utility.CheckDbNull<decimal>(reader["PurchasePrice"]),
                            PurchaseDescription = Utility.CheckDbNull<string>(reader["PurchaseDescription"]),
                            SalesDescription = Utility.CheckDbNull<string>(reader["SalesDescription"]),
                            SalesLedgerId = Utility.CheckDbNull<int>(reader["SalesLedgerId"]),
                            PurchaseLedgerId = Utility.CheckDbNull<int>(reader["PurchaseLedgerId"]),
                            IsInventory = Utility.CheckDbNull<bool>(reader["IsInventroy"]),
                            IsPurchase = Utility.CheckDbNull<bool>(reader["IsPurchase"]),
                            IsSales = Utility.CheckDbNull<bool>(reader["IsSales"]),
                            PurchaseLedgerName = Utility.CheckDbNull<string>(reader["PurchaseLedgerName"]),
                            SalesLedgerName = Utility.CheckDbNull<string>(reader["SalesLedgerName"]),
                            Uom = Utility.CheckDbNull<string>(reader[DBParam.Output.UOM]),
                            Type = Utility.CheckDbNull<string>(reader[DBParam.Output.Type]),
                            HsnCode = Utility.CheckDbNull<string>(reader[DBParam.Output.HsnCode]),
                            Manufacturer = Utility.CheckDbNull<string>(reader[DBParam.Output.Manufacturer]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            LowStockThresold = Utility.CheckDbNull<decimal>(reader["LowStockThresold"]),
                            OpeningStock = Utility.CheckDbNull<int>(reader[DBParam.Output.OpeningStock]),
                            Cgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.CGST]),
                            Sgst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.SGST]),
                            Igst = Utility.CheckDbNull<decimal>(reader[DBParam.Output.IGST]),
                        };
                    }
                }
            }
            return productMasterBo;
        }

        public List<InventoryBO> SearchStockTransactionDetails(int productId)
        {
            List<InventoryBO> inventoryBoList = new List<InventoryBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_INVENTORYTRANSACTION);

                Database.AddInParameter(dbCommand, DBParam.Input.ProductID, DbType.Int32, productId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        inventoryBoList.Add(new InventoryBO
                        {
                            ProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.ProductID]),
                            SourceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.Date]),
                            SourceNo = Utility.CheckDbNull<string>(reader[DBParam.Output.SourceNo]),
                            StockType = Utility.CheckDbNull<string>(reader[DBParam.Output.StockType]),
                            Party = Utility.CheckDbNull<string>(reader[DBParam.Output.Party]),
                            Qty = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                        });
                    }
                }
            }

            return inventoryBoList;
        }

        #endregion Stock Maintenance

        #region My Stock Distribution

        public List<InventoryBO> SearchMyStockDistribution(int productId)
        {
            List<InventoryBO> inventoryBoList = new List<InventoryBO>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_MYSTOCKDISTRIBUTION);

                Database.AddInParameter(dbCommand, DBParam.Input.ProductID, DbType.Int32, productId);
                Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        inventoryBoList.Add(new InventoryBO
                        {
                            ProductID = Utility.CheckDbNull<int>(reader[DBParam.Output.ProductID]),
                            ProductName = Utility.CheckDbNull<string>(reader[DBParam.Output.ProductName]),
                            CostCenter = Utility.CheckDbNull<string>(reader[DBParam.Output.CostCenter]),
                            DistributedDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DistributedDate]),
                            Qty = Utility.CheckDbNull<int>(reader[DBParam.Output.Qty]),
                            Remarks = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                            UOM = Utility.CheckDbNull<string>(reader[DBParam.Output.UOM]),
                        });
                    }
                }
            }

            return inventoryBoList;
        }

        #endregion My Stock Distribution
    }
}