﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class BankRepository : BaseRepository
    {
        #region Search Bank

        public List<BankBo> SearchBankList(BankBo bankBo)
        {
            List<BankBo> bankBoList = new List<BankBo>();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_BANK);

                Database.AddInParameter(dbCommand, DBParam.Input.BankName, DbType.String, bankBo.BankName);
                Database.AddInParameter(dbCommand, DBParam.Input.AccountNo, DbType.String, bankBo.AccountNo);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        bankBoList.Add(
                            new BankBo
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                                BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName]),
                                AccountNo = Utility.CheckDbNull<string>(reader[DBParam.Output.AccountNo]),
                                AccountType = new  cvt.officebau.com.ViewModels.Entity
                                {
                                    Name = Utility.CheckDbNull<string>(reader[DBParam.Output.AccountTypeName])
                                },
                                OpeningBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OpeningBalance]),
                                ClosingBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.ClosingBalance]),
                                OpeningBalanceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.OpeningBalDate]),
                                Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description]),
                                DisplayName = Utility.CheckDbNull<string>(reader[DBParam.Output.DisplayName]),
                                BranchName = Utility.CheckDbNull<string>(reader[DBParam.Output.BranchName]),
                                IfscCode = Utility.CheckDbNull<string>(reader[DBParam.Output.IFSCCode]),
                                SwiftCode = Utility.CheckDbNull<string>(reader[DBParam.Output.SWIFTCode]),
                                MicrCode = Utility.CheckDbNull<string>(reader[DBParam.Output.MICRCode])
                            });
                    }
                }
            }

            return bankBoList;
        }

        #endregion Search Bank

        #region Get Bank

        public BankBo GetBankDetail(int bankId)
        {
            BankBo bankBo = new BankBo();
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_BANK);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, bankId);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        bankBo = new BankBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            BankName = Utility.CheckDbNull<string>(reader[DBParam.Output.BankName].ToString()),
                            DisplayName = Utility.CheckDbNull<string>(reader[DBParam.Output.DisplayName].ToString()),
                            AccountNo = Utility.CheckDbNull<string>(reader[DBParam.Output.AccountNo].ToString()),
                            AccountType = new  cvt.officebau.com.ViewModels.Entity
                            {
                                Id = Utility.CheckDbNull<int>(reader[DBParam.Output.AccountTypeID])
                            },
                            OpeningBalance = Utility.CheckDbNull<decimal>(reader[DBParam.Output.OpeningBalance]),
                            OpeningBalanceDate = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.OpeningBalDate]),
                            Limit = Utility.CheckDbNull<decimal>(reader[DBParam.Output.Limit]),
                            Description = Utility.CheckDbNull<string>(reader[DBParam.Output.Description].ToString()),
                            BranchName = Utility.CheckDbNull<string>(reader[DBParam.Output.BranchName]),
                            IfscCode = Utility.CheckDbNull<string>(reader[DBParam.Output.IFSCCode]),
                            SwiftCode = Utility.CheckDbNull<string>(reader[DBParam.Output.SWIFTCode]),
                            MicrCode = Utility.CheckDbNull<string>(reader[DBParam.Output.MICRCode]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy])
                        };
                    }
                }
            }

            return bankBo;
        }

        #endregion Get Bank

        #region Manage Bank

        public string ManageBankDetails(BankBo bankBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_BANK);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, bankBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.BankName, DbType.String, bankBo.BankName);
                Database.AddInParameter(dbCommand, DBParam.Input.DisplayName, DbType.String, bankBo.DisplayName);
                Database.AddInParameter(dbCommand, DBParam.Input.AccountNo, DbType.String, bankBo.AccountNo);
                Database.AddInParameter(dbCommand, DBParam.Input.BranchName, DbType.String, bankBo.BranchName);
                Database.AddInParameter(dbCommand, DBParam.Input.IFSCCode, DbType.String, bankBo.IfscCode);
                Database.AddInParameter(dbCommand, DBParam.Input.SWIFTCode, DbType.String, bankBo.SwiftCode);
                Database.AddInParameter(dbCommand, DBParam.Input.MICRCode, DbType.String, bankBo.MicrCode);
                Database.AddInParameter(dbCommand, DBParam.Input.Description, DbType.String, bankBo.Description);
                Database.AddInParameter(dbCommand, DBParam.Input.AccountTypeID, DbType.Int32, bankBo.AccountType?.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.OpeningBalance, DbType.Decimal, bankBo.OpeningBalance);
                Database.AddInParameter(dbCommand, DBParam.Input.OpeningBalDate, DbType.DateTime, bankBo.OpeningBalanceDate);
                Database.AddInParameter(dbCommand, DBParam.Input.Limit, DbType.Decimal, bankBo.Limit);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Int32, bankBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());

                bankBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return bankBo.UserMessage;
        }

        #endregion Manage Bank
    }
}