﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace cvt.officebau.com.Services
{
    public class EmployeeClaimsConfigurationRepository : BaseRepository
    {
        public List<EmployeeClaimsBo> SearchClaimsPolicyList()
        {
            List<EmployeeClaimsBo> claimsPolicyList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCHCLAIMSPOLICY);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        claimsPolicyList.Add(new EmployeeClaimsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ExpenseType = Utility.CheckDbNull<string>(reader[DBParam.Output.ExpenseType]),
                            Band = Utility.CheckDbNull<string>(reader[DBParam.Output.Band]),
                            Level = Utility.CheckDbNull<string>(reader[DBParam.Output.Level]),
                            Designation = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName]),
                            MetroAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MetroAmount]),
                            NonMetroAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.NonMetroAmount]),
                            SNo = Utility.CheckDbNull<int>(reader[DBParam.Output.RowNo])
                        });
                    }
                }
            }

            return claimsPolicyList;
        }

        public EmployeeClaimsBo GetClaimPolicy(int id)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GETCLAIMPOLICY);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBo = new EmployeeClaimsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            ExpenseTypeId = Utility.CheckDbNull<int>(reader[DBParam.Output.ExpenseTypeID]),
                            BandId = Utility.CheckDbNull<int>(reader[DBParam.Output.BandID]),
                            LevelId = Utility.CheckDbNull<int>(reader[DBParam.Output.LevelID]),
                            DesignationId = Utility.CheckDbNull<int>(reader[DBParam.Output.DesignationID]),
                            MetroAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.MetroAmount]),
                            NonMetroAmount = Utility.CheckDbNull<decimal>(reader[DBParam.Output.NonMetroAmount]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation])
                        };
                    }
                }
            }

            return employeeClaimsBo;
        }

        public string ManageClaimPolicy(EmployeeClaimsBo employeeClaimsBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_CLAIMPOLICY);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeClaimsBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.ExpenseTypeID, DbType.Int32, employeeClaimsBo.ExpenseTypeId);
                Database.AddInParameter(dbCommand, DBParam.Input.BandID, DbType.Int32, employeeClaimsBo.BandId);
                Database.AddInParameter(dbCommand, DBParam.Input.LevelID, DbType.Int32, employeeClaimsBo.LevelId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, employeeClaimsBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.MetroAmount, DbType.Decimal, employeeClaimsBo.MetroAmount);
                Database.AddInParameter(dbCommand, DBParam.Input.NonMetroAmount, DbType.Decimal, employeeClaimsBo.NonMetroAmount);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, employeeClaimsBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                employeeClaimsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return employeeClaimsBo.UserMessage;
        }

        #region Designation Mapping

        public List<EmployeeClaimsBo> SearchDesignationMapping()
        {
            List<EmployeeClaimsBo> employeeClaimsBoList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.SEARCH_DESIGNATIONMAPPING);

                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBoList.Add(new EmployeeClaimsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Band = Utility.CheckDbNull<string>(reader[DBParam.Output.Band]),
                            Level = Utility.CheckDbNull<string>(reader[DBParam.Output.Level]),
                            Designation = Utility.CheckDbNull<string>(reader[DBParam.Output.DesignationName])
                        });
                    }
                }
            }

            return employeeClaimsBoList;
        }

        public string ManageDesignationMapping(EmployeeClaimsBo employeeClaimsBo)
        {
            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_DESIGNATIONMAPPING);
                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, employeeClaimsBo.Id);
                Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.BandID, DbType.Int32, employeeClaimsBo.BandId);
                Database.AddInParameter(dbCommand, DBParam.Input.GradeID, DbType.Int32, employeeClaimsBo.LevelId);
                Database.AddInParameter(dbCommand, DBParam.Input.DesignationID, DbType.Int32, employeeClaimsBo.DesignationId);
                Database.AddInParameter(dbCommand, DBParam.Input.IsDeleted, DbType.Boolean, employeeClaimsBo.IsDeleted);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                employeeClaimsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
            }

            return employeeClaimsBo.UserMessage;
        }

        public EmployeeClaimsBo GetDesignationMapping(int id)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_DESIGNATIONMAPPING);

                Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, id);
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBo = new EmployeeClaimsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            BandId = Utility.CheckDbNull<int>(reader[DBParam.Output.BandID]),
                            LevelId = Utility.CheckDbNull<int>(reader[DBParam.Output.LevelID]),
                            DesignationId = Utility.CheckDbNull<int>(reader[DBParam.Output.DesignationID]),
                            ModifiedByName = Utility.CheckDbNull<string>(reader[DBParam.Output.ModifiedBy]),
                            ModifiedOn = Utility.CheckDbNull<DateTime>(reader[DBParam.Output.ModifiedOn]),
                            BaseLocation = Utility.CheckDbNull<string>(reader[DBParam.Output.BaseLocation])
                        };
                    }
                }
            }

            return employeeClaimsBo;
        }

        #endregion Designation Mapping

        #region Claims Approver Configuration

        public List<EmployeeClaimsBo> SearchClaimsApproverConfiguration(int? businessUnitId)
        {
            List<EmployeeClaimsBo> employeeClaimsBoList = new List<EmployeeClaimsBo>();

            using (Database.CreateConnection())
            {
                DbCommand dbCommand = Database.GetStoredProcCommand(Constants.GET_CLAIMSAPPROVERCONFIGLIST);

                Database.AddInParameter(dbCommand, DBParam.Input.BusinessUnit, DbType.String, businessUnitId);
                Database.AddInParameter(dbCommand, DBParam.Input.UserID, DbType.Int32, Utility.UserId());
                Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());

                using (IDataReader reader = Database.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        employeeClaimsBoList.Add(new EmployeeClaimsBo
                        {
                            Id = Utility.CheckDbNull<int>(reader[DBParam.Output.ID]),
                            Employee = Utility.CheckDbNull<string>(reader[DBParam.Output.EmployeeName]),
                            ReportingToName = Utility.CheckDbNull<string>(reader[DBParam.Output.ReportingToName]),
                            ReportingToId = Utility.CheckDbNull<int>(reader[DBParam.Output.ReportingToID]),
                            EmployeeId = Utility.CheckDbNull<int>(reader[DBParam.Output.EmployeeID])
                        });
                    }
                }
            }

            return employeeClaimsBoList;
        }

        public EmployeeClaimsBo ManageClaimsApproverConfiguration(List<EmployeeClaimsBo> list)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();

            using (DbConnection dbCon = Database.CreateConnection())
            {
                dbCon.Open();
                foreach (EmployeeClaimsBo item in list)
                {
                    if (item.IsDeleted)
                    {
                        DbCommand dbCommand = Database.GetStoredProcCommand(Constants.MANAGE_CLAIMSAPPROVERCONFIGURATION);
                        Database.AddInParameter(dbCommand, DBParam.Input.ID, DbType.Int32, item.Id == 0 ? 0 : item.Id);
                        Database.AddInParameter(dbCommand, DBParam.Input.EmployeeID, DbType.Int32, item.EmployeeId);
                        Database.AddInParameter(dbCommand, DBParam.Input.ApproverID, DbType.Int32, item.ReportingToId ?? 0);
                        Database.AddInParameter(dbCommand, DBParam.Input.SessionID, DbType.Int32, Utility.UserId());
                        Database.AddInParameter(dbCommand, DBParam.Input.DomainID, DbType.Int32, Utility.DomainId());
                        employeeClaimsBo.UserMessage = Database.ExecuteScalar(dbCommand).ToString();
                    }
                }
            }

            return employeeClaimsBo;
        }

        #endregion Claims Approver Configuration
    }
}