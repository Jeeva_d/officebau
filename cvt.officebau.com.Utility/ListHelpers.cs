﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace cvt.officebau.com.Utilities
{
    public static class ListHelpers
    {
        public static IOrderedEnumerable<T> OrderByDirection<T, TKey>(this IEnumerable<T> list, Func<T, TKey> keySelector, bool ascending)
        {
            return ascending ? list.OrderBy(keySelector) : list.OrderByDescending(keySelector);
        }

        public static bool ContainsAll<T>(this List<T> a, IEnumerable<T> b)
        {
            return a != null && b != null && !b.Except(a).Any();
        }

        public static bool IsEmpty<T>(this List<T> list)
        {
            return list == null || list.Count == 0;
        }

        public static bool HasValue<T>(this List<T> list)
        {
            return list != null && list.Count > 0;
        }

        public static List<T> NullIfEmpty<T>(this List<T> list)
        {
            return list?.Any() == true ? list : null;
        }

        public static string ToCommaDelimitedString(this List<string> list, bool spaces = false)
        {
            // If the list is null or empty, return an empty string
            if (list?.Any() != true)
                return string.Empty;

            // Join the list with commas, adding spaces if directed
            var r = string.Join((spaces ? ", " : ","), list);

            // Return the result
            return r;
        }

        /// <summary>
        /// Returns a value if the key exists, default(TValue) otherwise.  Saves the caller a check for ContainsKey.
        /// </summary>
        /// <returns></returns>
        public static TValue GetValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
        {
            return dictionary.ContainsKey(key) ? dictionary[key] : default(TValue);
        }

        /// <summary>
        /// Determines if this dictionary has exactly the same keys and values as another dictionary.
        /// Assumes that keyComparer will only return true for at most one value in either dictionary, otherwise may return an unexpected result.
        /// </summary>
        /// <param name="other">Another dictionary to compare.</param>
        /// <param name="keyComparer">A comparer for the keys.  Defaults to TKey.Equals.</param>
        /// <param name="valueComparer">A comparer for the values.  Defaults to TValue.Equals.</param>
        /// <returns></returns>
        /// Not implemented as an override as .Equals since GetHashCode() would be a pain to implement.
        public static bool EqualTo<TKey, TValue>(this Dictionary<TKey, TValue> me, Dictionary<TKey, TValue> other, Func<TKey, TKey, bool> keyComparer = null, Func<TValue, TValue, bool> valueComparer = null)
        {
            if (me == null && other == null)
                return true;
            if (me == null || other == null)
                return false;
            if (me.Keys.Count != other.Keys.Count)
                return false;
            if (me.Keys.Count == 0 && other.Keys.Count == 0)
                return true;

            keyComparer = keyComparer ?? new Func<TKey, TKey, bool>((k1, k2) => (k1 == null && k2 == null) || k1?.Equals(k2) == true);
            valueComparer = valueComparer ?? new Func<TValue, TValue, bool>((v1, v2) => (v1 == null && v2 == null) || v1?.Equals(v2) == true);

            foreach (var key in me.Keys)
            {
                bool found = false;
                foreach (var otherKey in other.Keys)
                {
                    if (keyComparer(key, otherKey))
                    {
                        if (valueComparer(me[key], other[otherKey]))
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Determines if each entry in the list has at least one matching entry in the other list and vice-versa.
        /// </summary>
        /// <param name="other">Another list to compare..</param>
        /// <param name="comparer">A comparer for the list entries.  Defaults to T.Equals.</param>
        /// <returns></returns>
        /// Not implemented as an override as .Equals since GetHashCode() would be a pain to implement.
        public static bool EqualTo<U, T>(this List<T> me, List<U> other, Func<U, T, bool> comparer = null)
        {
            if (me == null && other == null)
                return true;
            if (me == null || other == null)
                return false;
            if (me.Count != other.Count)
                return false;
            comparer = comparer ?? new Func<U, T, bool>((k1, k2) => (k1 == null && k2 == null) || k1?.Equals(k2) == true);

            foreach (var meVal in me)
            {
                bool found = false;
                foreach (var otherVal in other)
                {
                    if (comparer(otherVal, meVal))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return false;
            }

            foreach (var otherVal in other)
            {
                bool found = false;
                foreach (var meVal in me)
                {
                    if (comparer(otherVal, meVal))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Break a list into chunks, batch them into smaller lists within a list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="countPerBatch"></param>
        /// <returns></returns>
        public static List<List<T>> Batch<T>(this List<T> list, int countPerBatch)
        {
            if (list == null)
                return null;

            if (countPerBatch == 0)
                throw new ArgumentOutOfRangeException("countPerBatch must be greater than 0");

            var batchedList = new List<List<T>> {
                new List<T>() // add the inital list
            };

            int currentListIndex = 0;
            int currentListItemIndex = 0;
            while (currentListItemIndex < list.Count)
            {
                // When the current list fills up, create a new one and repeat the process.
                if (batchedList[currentListIndex].Count == countPerBatch)
                {
                    currentListIndex++;
                    batchedList.Add(new List<T>());
                }

                batchedList[currentListIndex].Add(list[currentListItemIndex]);

                currentListItemIndex++;
            }

            return batchedList;
        }
    }
}
