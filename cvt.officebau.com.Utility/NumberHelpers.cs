﻿using System;

namespace cvt.officebau.com.Utilities
{
    public static class NumberHelpers
    {
        public static decimal? MatchNullAndZero(this decimal? val, decimal? otherVal)
        {
            if (otherVal == null && val == 0)
                return null;
            if (otherVal == 0 && val == null)
                return 0;
            return val;
        }

        public static decimal MatchNullAndZero(this decimal? val, decimal otherVal)
        {
            if (otherVal == 0 || val == null)
                return 0;
            return val.Value;
        }

        public static bool CloseTo(this decimal val, decimal otherVal, decimal precision = .001m)
        {
            return Math.Abs(val - otherVal) <= precision;
        }

        public static decimal? Round(this decimal? val, int precision)
        {
            if (val.HasValue)
                return Math.Round(val.Value, precision);
            else
                return null;
        }
    }
}
