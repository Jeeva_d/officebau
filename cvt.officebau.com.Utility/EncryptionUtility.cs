﻿using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace cvt.officebau.com.Utilities
{
    public static class EncryptionUtility
    {
        private static readonly string privateKey = ConfigurationManager.AppSettings["Salt"];

        public static string GetHashedValue(string password)
        {
            SHA256 hash = new SHA256CryptoServiceProvider();
            return Convert.ToBase64String(hash.ComputeHash(Encoding.UTF8.GetBytes(string.Concat(password, privateKey))));
        }

        public static string Encrypt(string inputString)
        {
            byte[] byKey = { };
            byte[] IV = {18, 52, 86, 120, 144, 171, 205, 239};
            byKey = Encoding.UTF8.GetBytes(privateKey.Substring(0, 8));
            var des = new DESCryptoServiceProvider();
            var inputByteArray = Encoding.UTF8.GetBytes(inputString);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return Convert.ToBase64String(ms.ToArray());
        }

        public static string Decrypt(string inputString)
        {
            inputString = inputString.Replace(" ", "+");
            byte[] byKey = { };
            byte[] IV = {18, 52, 86, 120, 144, 171, 205, 239};
            var inputByteArray = new byte[inputString.Length];

            byKey = Encoding.UTF8.GetBytes(privateKey.Substring(0, 8));
            var des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(inputString.Replace(" ", "+"));
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            var encoding = Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }
    }
}
