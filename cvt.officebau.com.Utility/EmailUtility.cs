﻿using System;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;

namespace cvt.officebau.com.Utilities
{
    public static class EmailUtility
    {

        public static void SendInstantEmail(string toEmailID, string content, string subject, string ccEmailID, string attachment)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                string[] toID = toEmailID.Replace(';', ',').Split(',');
                string[] ccID = ccEmailID.Replace(';', ',').Split(',');

                foreach (string str in toID)
                {
                    if (IsValidEmailAddress(str))
                    {
                        mailMessage.To.Add(str);
                    }
                }

                foreach (string str in ccID)
                {
                    if (IsValidEmailAddress(str))
                    {
                        mailMessage.CC.Add(str);
                    }
                }

                // Send Email only when ToAddress is valid
                if (mailMessage.To != null || mailMessage.CC != null)
                {
                    string emailAddress = WebConfigurationManager.AppSettings["EmailAddress"];
                    string emailPassword = WebConfigurationManager.AppSettings["EmailPassword"];
                    string smtpHost = WebConfigurationManager.AppSettings["SMTPHost"];
                    string smtpPort = WebConfigurationManager.AppSettings["SMTPPort"];

                    string fromAdress = string.IsNullOrEmpty(Utility.GetSession("CommunicationEmailId")) ? WebConfigurationManager.AppSettings["EmailAddress"] : Utility.GetSession("CommunicationEmailId");

                    MailAddress address = new MailAddress(fromAdress);
                    mailMessage.From = address;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Subject = subject;
                    mailMessage.Body = content;
                    if (attachment == "Invoice")
                    {
                        Attachment attach = new Attachment("c:/textfile.txt");
                        mailMessage.Attachments.Add(attach);
                    }

                    SmtpClient client = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        EnableSsl = true,
                        Host = smtpHost,
                        Port = int.Parse(smtpPort)
                    };

                    NetworkCredential credentials = new NetworkCredential(emailAddress, emailPassword);
                    client.Credentials = credentials;
                    mailMessage.ReplyToList.Add(new MailAddress(fromAdress));
                    client.Send(mailMessage);
                    //Need to uncommand the file
                    //UtilityRepository _rep = new UtilityRepository();
                    //_rep.SaveMailToBackupTable(fromAdress, toEmailID, ccEmailID, subject, content);
                }
            }
            catch (Exception)
            {
                // throw ex;
            }
        }

        private static bool IsValidEmailAddress(string emailAddress)
        {
            if (!string.IsNullOrEmpty(emailAddress))
            {
                try
                {
                    MailAddress m = new MailAddress(emailAddress);
                    return true;
                }
                catch (FormatException)
                {
                    return false;
                }
            }

            return false;
        }


    }

    
}
