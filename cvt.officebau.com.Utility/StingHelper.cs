﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace cvt.officebau.com.Utilities
{
    public static class StringHelpers
    {
        public static string GetDateStringFormatted(this DateTime? date)
        {
            if(date == null || date == DateTime.MinValue)
                return "";
            return Convert.ToDateTime(date).ToString("dd-MMM-yyyy");
        }

        /// <summary>
        /// Replace Double Quotes with Single Double Quotes
        /// </summary>
        /// <param name="self"></param>
        /// <param name="rsh"></param>
        /// <returns></returns>
        private static readonly Regex regexDoubleQuote = new Regex("\"+");
        public static string RemoveDoubleQuotes(this string self)
        {
            return regexDoubleQuote.Replace(self.EmptyIfNull(), "\"");
        }


        /// <summary>
        /// Performs a case in-sensitive string comparision
        /// </summary>
        /// <param name="self"></param>
        /// <param name="rsh"></param>
        /// <returns></returns>
        public static bool ICmp(this string self, string rsh)
        {
            if (self == null && rsh == null)
                return true;
            return self != null && self.Equals(rsh, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Case-insensitive version of StartsWith
        /// </summary>
        /// <returns></returns>
        public static bool StartsWithICmp(this string self, string rsh)
        {
            if (self == null && rsh == null)
                return true;
            return self != null && self.StartsWith(rsh, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Case-insensitive version of EndsWith
        /// </summary>
        /// <returns></returns>
        public static bool EndsWithICmp(this string self, string rsh)
        {
            if (self == null && rsh == null)
                return true;
            return self != null && self.EndsWith(rsh, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Case-insensitive version of Contains
        /// </summary>
        /// <returns></returns>
        public static bool ContainsICmp(this string self, string rsh)
        {
            if (self == null && rsh == null)
                return true;
            return self != null && System.Threading.Thread.CurrentThread.CurrentCulture.CompareInfo.IndexOf(self, rsh, System.Globalization.CompareOptions.IgnoreCase) >= 0;
        }

        /// <summary>
        /// Efficiently compares two strings and returns true if they are the same, ignoring case and whitespace differences.
        /// </summary>
        public static bool ICmpNoWhitespace(this string self, string other)
        {
            if (self == null && other == null)
                return true;

            if (self == null || other == null)
                return false;

            int otherIndex = 0;
            for (int selfIndex = 0; selfIndex < self.Length; selfIndex++)
            {
                if (Char.IsWhiteSpace(self[selfIndex]))
                    continue;
                while (otherIndex < other.Length && Char.IsWhiteSpace(other[otherIndex])) otherIndex++; // fast-forward to next non-whitespace character or end of the string;
                if (otherIndex >= other.Length) return false; // there's a non-whitespace character in self that doesn't have a match in other, return false.

                if (other[otherIndex] != self[selfIndex] && Char.ToUpper(other[otherIndex]) != Char.ToUpper(self[selfIndex])) return false;
                otherIndex++;
            }
            while (otherIndex < other.Length && Char.IsWhiteSpace(other[otherIndex])) otherIndex++; // fast-forward through any remaining whitespace characters.

            // If we've gone through all of both strings and haven't found an unequal character, then the strings are equal (not-including whitespace or case).
            return otherIndex >= other.Length;
        }


        public static string SafeString(this string self)
        {
            return !String.IsNullOrWhiteSpace(self) ? self.Trim() : "";
        }

        /// <summary>
        /// Converts a string to a PascalCase string - replaces every non-space character to a space, capitalizes the first letter of each word, 
        /// then removes the spaces.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToPascalCase(string s)
        {
            s = Regex.Replace(s, @"[^a-zA-Z0-9_]", " ");
            System.Globalization.TextInfo info = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
            s = info.ToTitleCase(s).Replace(" ", string.Empty);
            return s;
        }

        /// <summary>
        /// Converts a string to a PascalCase string - replaces every non-space character to a space, capitalizes the first letter of each word, 
        /// then removes the spaces.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ToSeparateWords(string s)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                if (i == 0)
                {
                    sb.Append(Char.ToUpper(s[i]));
                    continue;
                }

                char current = s[i];
                char previous = sb[sb.Length - 1];

                if (!char.IsLetterOrDigit(current))
                    current = ' ';

                bool changeDetected = (char.IsUpper(current) && !char.IsUpper(previous)) ||
                    (char.IsDigit(current) && !char.IsDigit(previous));

                if (changeDetected && !char.IsWhiteSpace(previous))
                    sb.Append(" ");

                sb.Append(current);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Used when a string hash code needs to be stable across .NET versions and stored somewhere.  The built-in .NET one is not suitable for 
        /// using a hashcode outside the application domain in which the string was created.
        /// </summary>
        // https://stackoverflow.com/questions/36845430/persistent-hashcode-for-strings
        public static int GetStableHashCode(this string str)
        {
            unchecked
            {
                int hash1 = 5381;
                int hash2 = hash1;

                for (int i = 0; i < str.Length && str[i] != '\0'; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ str[i];
                    if (i == str.Length - 1 || str[i + 1] == '\0')
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
                }

                return hash1 + (hash2 * 1566083941);
            }
        }


        /// <summary>
        /// Returns true if the string equals "true", "yes" or "1"
        /// These are common in data import files.
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool MeansTrue(this string self)
        {
            if (string.IsNullOrEmpty(self))
                return false;
            else
                return (self.ICmp("true") || self.ICmp("yes") || self == "1");
        }

        /// <summary>
        /// Returns null if the string is null or empty, for convenient use in null coalescing operator.
        /// </summary>
        public static string NullIfEmpty(this string self)
        {
            return string.IsNullOrEmpty(self) ? null : self;
        }

        public static string EmptyIfNull(this string self)
        {
            return string.IsNullOrEmpty(self) ? string.Empty : self;
        }

        /// <summary>
        /// Shortcut for !String.IsNullOrEmpty
        /// </summary>
        public static bool HasValue(this string self)
        {
            return !string.IsNullOrEmpty(self);
        }

        /// <summary>
        /// Shortcut for !String.IsNullOrEmpty
        /// </summary>
        public static bool IsNullOrEmpty(this string self)
        {
            return string.IsNullOrEmpty(self);
        }

        /// <summary>
        /// Truncates a string to a given length
        /// </summary>
        /// <param name="self"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Truncate(this string self, int length)
        {
            return self == null ? null : (self.Length <= length ? self : self.Substring(0, length));
        }

        /// <summary>
        /// Upper Case First Letter of the String
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string CaptializeFirstLetter(this string self)
        {
            string output = string.Empty;
            foreach (var _self in self.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries))
            {
                output += " " + _self.Substring(0, 1).ToUpper() + _self.Substring(1);
            }
            return output.Trim();
        }

        /// <summary>
        /// Detects Html tags in text
        /// Does not validate Html, just that a string contains a pattern that looks like an html tag:
        ///   <sometag></sometag> or
        ///   <sometag>
        /// </summary>
        /// <param name="testString">String to test</param>
        /// <returns>True when tags are detected</returns>
        private static readonly Lazy<Regex> tagRegex = new Lazy<Regex>(() => new Regex(@"<[^>]+>"), true);
        public static bool HasHtmlTags(string testString)
        {
            return !string.IsNullOrEmpty(testString) && tagRegex.Value.IsMatch(testString);
        }

        /// <summary>
        /// Helper method to converts a string with path separator of -> into a list of strings.
        /// </summary>
        /// <returns></returns>
        public static List<string> ParseCategoryPath(string categoryPath)
        {
            if (categoryPath.IsNullOrEmpty())
                return new List<string>();

            if (!categoryPath.Contains(" -> "))
                return new List<string>() { categoryPath };

            List<string> categories = new List<string>();
            StringBuilder sb = new StringBuilder(categoryPath);
            int start = 0;
            do
            {
                int end = categoryPath.IndexOf(" -> ", start);
                if (end < 0)
                    end = categoryPath.Length;

                categories.Add(categoryPath.Substring(start, end - start).Trim());
                start = end + 4;
            } while (start < categoryPath.Length);
            return categories;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="currenySymbol"></param>
        /// <returns></returns>
        public static string ToCurrencyText(this decimal value, string currenySymbol = null)
        {
            return currenySymbol + value.ToString("#,###,##0.00");
        }

        public static string ReplaceAll(this string s, char[] charsToReplace, char replacement)
        {
            if (s.IsNullOrEmpty())
                return s;

            if (charsToReplace.Contains(replacement))
                throw new ArgumentException("Replacement must not be in the values to replace");

            StringBuilder sb = new StringBuilder(s);
            foreach (var c in charsToReplace)
                sb.Replace(c, replacement);

            return sb.ToString();

        }

        public static bool LooksLikeJson(string s)
        {
            if (s.IsNullOrEmpty())
                return false;

            // skip any whitespace characters
            int starts = 0;
            while (starts < s.Length && Char.IsWhiteSpace(s[starts]))
                starts++;

            if (starts >= s.Length)
                return false;

            return s[starts] == '{' || s[starts] == '[';

        }

        public static string SanitizeFileName(string filename)
        {
            char[] charsToReplace = System.IO.Path.GetInvalidFileNameChars().Concat(" +#&".ToCharArray()).ToArray();
            return filename.ReplaceAll(charsToReplace, '_');
        }

        public static Stream GenerateStreamFromString(string s, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;
            return new MemoryStream(encoding.GetBytes(s ?? ""));
        }

        private static readonly Lazy<Assembly> ParentAssembly = new Lazy<Assembly>(() => Assembly.GetEntryAssembly() ?? Assembly.GetCallingAssembly(), true);

        //
        // turns a single stack frame object into an informative string
        //
        private static string StackFrameToString(StackFrame sf)
        {
            StringBuilder sb = new StringBuilder();
            MemberInfo mi = sf.GetMethod();


            // build method name
            sb.Append("   ");
            sb.Append(mi.DeclaringType?.Namespace);
            sb.Append(".");
            sb.Append(mi.DeclaringType?.Name);
            sb.Append(".");
            sb.Append(mi.Name);

            // build method params            
            sb.Append("(");
            int intParam = 0;
            foreach (ParameterInfo objParameter in sf.GetMethod().GetParameters())
            {
                intParam++;
                if (intParam > 1) sb.Append(", ");
                sb.Append(objParameter.Name);
                sb.Append(" As ");
                sb.Append(objParameter.ParameterType.Name);
            }
            sb.Append(")");
            sb.Append(Environment.NewLine);

            ////if source code is available, append location info
            sb.Append("       ");
            string filename = sf.GetFileName();
            if (string.IsNullOrEmpty(filename))
            {
                sb.Append(Path.GetFileName(ParentAssembly.Value.CodeBase));
                ////native code offset is always available
                sb.Append(": N ");
                sb.Append($"{sf.GetNativeOffset():#00000}");
            }
            else
            {
                sb.Append(Path.GetFileName(filename));
                sb.Append(": line ");
                sb.Append($"{sf.GetFileLineNumber():#0000}");
                sb.Append(", col ");
                sb.Append($"{sf.GetFileColumnNumber():#00}");
                ////if IL is available, append IL location info
                if (sf.GetILOffset() != StackFrame.OFFSET_UNKNOWN)
                {
                    sb.Append(", IL ");
                    sb.Append($"{sf.GetILOffset():#0000}");
                }
            }
            sb.Append(Environment.NewLine);

            return sb.ToString();
        }

        //
        // enhanced stack trace generator
        //
        private static string EnhancedStackTrace(StackTrace objStackTrace, string strSkipClassName)
        {
            int intFrame;

            StringBuilder sb = new StringBuilder();

            sb.Append(Environment.NewLine);
            sb.Append("---- Stack Trace ----");
            sb.Append(Environment.NewLine);

            for (intFrame = 0; intFrame < (objStackTrace.FrameCount - 1); intFrame++)
            {
                StackFrame sf = objStackTrace.GetFrame(intFrame);
                MemberInfo mi = sf.GetMethod();

                if ((strSkipClassName != "") && (mi.DeclaringType?.Name.IndexOf(strSkipClassName) > -1))
                {
                } ////don't include frames with this name
                else
                    sb.Append(StackFrameToString(sf));

            }
            sb.Append(Environment.NewLine);

            return sb.ToString();
        }

        private static string EnhancedStackTrace(Exception objException)
        {
            StackTrace objStackTrace = new StackTrace(objException, true);
            return EnhancedStackTrace(objStackTrace, "");
        }

        public static string FlattenException(Exception exObject)
        {
            if (exObject == null)
                return "";

            try
            {
                string strEnhancedException = EnhancedStackTrace(exObject);

                if (exObject.InnerException != null)
                {
                    strEnhancedException += "\r\n############# INNER EXCEPTION #############\r\n" +
                                            EnhancedStackTrace(exObject.InnerException);
                }

                return "############# ENHANCED STACK TRACE #############\r\n\r\n\r\n" +
                       ExceptionToString(exObject) +
                       "\r\n\r\n############# STANDARD STACK TRACE #############\r\n\r\n" +
                       strEnhancedException;
            }
            catch (Exception ex)
            {
                return "Error '" + ex.Message + "' while generating exception string";
            }
        }

        //
        //translate exception object to string, with additional system info
        //
        internal static String ExceptionToString(Exception objException)
        {
            StringBuilder sb = new StringBuilder();

            if (objException.InnerException != null)
            {
                //sometimes the original exception is wrapped in a more relevant outer exception
                //the detail exception is the "inner" exception
                //see http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnbda/html/exceptdotnet.asp            
                sb.Append("(Inner Exception)");
                sb.Append(Environment.NewLine);
                sb.Append(ExceptionToString(objException.InnerException));
                sb.Append(Environment.NewLine);
                sb.Append("(Outer Exception)");
                sb.Append(Environment.NewLine);
            }

            //get exception-specific information
            sb.Append("Exception Source:      ");
            try
            {
                sb.Append(objException.Source);
            }
            catch (Exception ex)
            {
                sb.Append(ex.Message);
            }
            sb.Append(Environment.NewLine);
            sb.Append("Exception Type:        ");
            try
            {
                sb.Append(objException.GetType().FullName);
            }
            catch (Exception ex)
            {
                sb.Append(ex.Message);
            }

            sb.Append(Environment.NewLine);

            sb.Append("Exception Message:     ");
            try
            {
                sb.Append(objException.Message);
            }
            catch (Exception ex)
            {
                sb.Append(ex.Message);
            }

            sb.Append(Environment.NewLine);

            sb.Append("Exception Target Site: ");
            try
            {
                sb.Append(objException.TargetSite.Name);
            }
            catch (Exception ex)
            {
                sb.Append(ex.Message);
            }

            sb.Append(Environment.NewLine);

            try
            {
                string x = EnhancedStackTrace(objException);
                sb.Append(x);
            }
            catch (Exception ex)
            {
                sb.Append(ex.Message);
            }

            sb.Append(Environment.NewLine);


            return sb.ToString();
        }

        /// <summary>
        /// Concatenates source strings, limited to maxLength.
        /// If needed, trims the end of source strings in proportion to their length to reach maxLength
        /// </summary>
        public static string ConcatToMaxLength(int maxLength, string separator, params string[] source)
        {
            StringBuilder rc = new StringBuilder(maxLength);

            // Put together to test against maxLength.  If it doesn't exceed maxLength, it's finished.
            for (int i = 0; i < source.Length; i++)
            {
                rc.Append(source[i]);
                if (i < source.Length - 1 && !string.IsNullOrEmpty(separator))
                    rc.Append(separator);
            }

            if (rc.ToString().Length > maxLength)
            {
                int totalLength = rc.Length;
                // Rounding may cause this to under trim by one character, correct for this by adding one to the total to Trim
                int totalTrim = (rc.Length - maxLength) + 1;
                rc.Clear();
                int trim;
                // Each contributor to the total length is trimmed in proportion to its length. 
                // This is floating point and we can only trim by integer units, there can be rounding errors in either direction
                trim = (int)((separator.Length / (double)totalLength) * totalTrim * (source.Length - 1) + 0.5);
                trim = Math.Min(separator.Length, trim);
                separator = separator.Substring(0, separator.Length - trim);
                for (int i = 0; i < source.Length; i++)
                {
                    trim = (int)((source[i].Length / (double)totalLength) * totalTrim + 0.5);
                    trim = Math.Min(source[i].Length, trim);
                    rc.Append(source[i].Substring(0, source[i].Length - trim));
                    if (i < source.Length - 1 && !string.IsNullOrEmpty(separator))
                        rc.Append(separator);
                }
            }
            return rc.ToString();
        }

        public static string ReplaceLastOccurrence(this string Source, string Find, string Replace)
        {
            int place = Source.LastIndexOf(Find);

            if (place == -1)
                return Source;

            string result = Source.Remove(place, Find.Length).Insert(place, Replace);
            return result;
        }

        /// <summary>
        /// Replace placeholders in a string with values from a dictionary
        /// </summary>
        /// <param name="source">The source string</param>
        /// <param name="replacements">A string,string replacement dictionary conaining keys and the values with which those keys should be replaced</param>
        /// <param name="exemptKeys">Keys which should not be replaced, and should be passed through as-is (usually for replacement elsewhere)</param>
        /// <param name="removeUnusedKeys">If true, empty placeholders will be removed after replacements have been performed for all keys in the replacements dictionary</param>
        /// <param name="leftIndicator">The left portion of the key indicator - this tells the method that the string that follows is a key to be replaced</param>
        /// <param name="rightIndicator">The right portion of the key indicator - this tells the method where the key ends</param>
        /// <returns></returns>
        public static string InsertReplacements(this string s, Dictionary<string, string> replacements, List<string> exemptKeys = null,
                                                bool removeUnusedKeys = true, string leftDelimiter = "{{", string rightDelimiter = "}}")
        {
            // Validate input
            if (replacements?.Any() != true)
                return s;
            if (exemptKeys == null)
                exemptKeys = new List<string>();

            var sb = new StringBuilder(s);

            // Clone the dictionary so we don't affect the source
            var tReplacements = new Dictionary<string, string>();

            foreach (var key in replacements.Keys)
                tReplacements[key] = replacements[key];

            var keysToRemove = tReplacements.Keys.Where(_ => exemptKeys.Contains(_)).ToList();

            // Remove exempt keys from replacement dictionary
            foreach (var key in keysToRemove)
                tReplacements.Remove(key);

            // Loop two times in case one of the replacement values contains a placeholder
            for (int i = 0; i < 2; i++)
            {
                foreach (var key in tReplacements.Keys)
                    sb.Replace(leftDelimiter + key + rightDelimiter, tReplacements[key]);
            }

            string r = sb.ToString();

            if (removeUnusedKeys)
                r = r.RemoveUnusedKeys(exemptKeys, leftDelimiter, rightDelimiter);

            // Return the modified string
            return r;
        }

        private static string RemoveUnusedKeys(this string s, List<string> exemptKeys = null, string leftDelimiter = "{{", string rightDelimiter = "}}")
        {
            // Validate Input
            if (leftDelimiter == rightDelimiter || leftDelimiter.Length == 1 || rightDelimiter.Length == 1)
                throw new Exception("Cannot remove unused keys.  To remove unused keys, the left delimiter must be different from the right delimiter, and each delimiter must consist of two or more characters");

            if (exemptKeys == null)
                exemptKeys = new List<string>();

            var sb = new StringBuilder(s);

            var unusedKeys = s.GetUnusedKeys(leftDelimiter, rightDelimiter);

            // Remove exempt keys from unusedKeys
            unusedKeys = unusedKeys.Where(_ => !exemptKeys.Contains(_)).ToList();

            // Remove unused keys
            foreach (var key in unusedKeys)
                sb.Replace(key, "");

            // Return the result
            return sb.ToString();
        }

        private static List<string> GetUnusedKeys(this string s, string leftDelimiter, string rightDelimiter)
        {
            // Validate Input
            if (leftDelimiter == rightDelimiter || leftDelimiter.Length == 1 || rightDelimiter.Length == 1)
                throw new Exception("Cannot get a list of unused keys. The left delimiter must be different from the right delimiter, and each delimiter must consist of two or more characters");

            int leftIndex = -1;
            int rightIndex = -1;
            int limit = 100; // If we go past 100, give up
            int count = 0;
            var unusedKeys = new List<string>();

            // Identify unused keys
            leftIndex = s.IndexOf(leftDelimiter);

            while (leftIndex > 0)
            {
                rightIndex = s.IndexOf(rightDelimiter, leftIndex + 3);

                if (rightIndex > 0)
                {
                    var tKey = s.Substring(leftIndex, (rightIndex - leftIndex) + rightDelimiter.Length);

                    // If the key isn't listed in the exempt keys list,
                    // add it to the unused keys list if it doesn't already exist there.
                    if (!unusedKeys.Contains(tKey))
                        unusedKeys.Add(tKey);

                }

                leftIndex = s.IndexOf(leftDelimiter, rightIndex + rightDelimiter.Length);
                count++;

                if (count > limit)
                    throw new Exception("Unable to get unused keys.  Loop detected.");

            }// end while

            // Return the result
            return unusedKeys;
        }

    }
}
