﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Configuration;

namespace cvt.officebau.com.Utilities
{
    public static class Utility
    {
        public static T CheckDbNull<T>(object obj)
        {
            return obj == DBNull.Value ? default(T) : (T)obj;
        }

        public static string GetSession(string key)
        {
            return HttpContext.Current.Session[key.ToLower()]?.ToString();
        }

        public static void SetSession(string key, object value)
        {
            HttpContext.Current.Session[key.ToLower()] = value;
        }

        public static string GetSubstring(string value, int limit)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            return value.Length > limit ? value.Substring(0, limit) : value;
        }

        public static int UserId()
        {
            return Convert.ToInt32(GetSession("UserID"));
        }
        public static int TmpUserId()
        {
            return Convert.ToInt32(GetSession("TmpUserID"));
        }
        public static int DomainId()
        {
            return Convert.ToInt32(GetSession("DomainID"));
        }

        public static string GetUploadFileName(string fileName)
        {
            return Path.GetFileName(DomainId() + "_" + DateTime.Now.Ticks + "_" + fileName);
        }

        public static DateTime GetCurrentDate()
        {
            return DateTime.Now;
        }

        public static int LoginId()
        {
            return Convert.ToInt32(GetSession("LoginID"));
        }

        public static bool CheckMedia()
        {
            var myBrowserCaps = HttpContext.Current.Request.Browser;
            return myBrowserCaps.IsMobileDevice;
        }

        public static string UploadUrl(string key)
        {
            var path = string.Empty;

            switch (key.ToUpper())
            {
                case "CLAIMS":
                    path = "HRMS\\CLAIMS\\REQUEST\\";
                    break;
                case "EMPLOYEMENTTERMS":
                    path = "HRMS\\CONTRACTEMPLOYEEDETAILS\\EMPLOYMENTTERMS\\";
                    break;
                case "PROFILE":
                    path = "HRMS\\EMPPROFILE\\";
                    break;

                case "EXPENSE":
                    path = "ACCOUNTS\\BILLS\\";
                    break;

                case "RECEIPTS":
                    path = "ACCOUNTS\\RECEIPTS\\";
                    break;

                case "INVOICE":
                    path = "ACCOUNTS\\INVOICES\\";
                    break;

                case "SALESORDER":
                    path = "ACCOUNTS\\SALESORDER\\";
                    break;

                case "EVENTS":
                    path = "HRMS\\EVENTS\\";
                    break;

                case "EMPDOCS":
                    path = "HRMS\\EMPDOCS\\";
                    break;

                case "EMPSKILLS":
                    path = "HRMS\\EMPSKILLS\\";
                    break;

                case "COMPANYLOGO":
                    path = "COMPANYLOGOS\\";
                    break;

                case "COMMON":
                    path = "COMMON\\";
                    break;

                case "TRAVEL":
                    path = "HRMS\\TRAVEL\\";
                    break;

                case "FORM16":
                    path = "HRMS\\FORM16\\";
                    break;

                case "PAYSTUB":
                    path = "PAYSTUB\\";
                    break;

                case "DOCUMENTS":
                    path = "HRMS\\DOCUMENT MANAGER\\";
                    break;

            }

            return path;
        }

        internal static T CheckDbNull<T>(object v, object p)
        {
            throw new NotImplementedException();
        }



        public static string GetSpQuery(object data)
        {
            DbCommand dbCommand = (DbCommand)data;
            var query = "EXEC " + dbCommand.CommandText + " ";

            for (var i = 0; i < dbCommand.Parameters.Count; i++)
            {
                string value;

                switch (dbCommand.Parameters[i].DbType)
                {
                    case DbType.Int16:
                    case DbType.Int32:
                    case DbType.Int64:
                        value = (dbCommand.Parameters[i].Value == DBNull.Value) ? "0" : Convert.ToString(dbCommand.Parameters[i].Value);
                        break;

                    case DbType.Decimal:
                        value = (dbCommand.Parameters[i].Value == DBNull.Value) ? "0" : Convert.ToString(dbCommand.Parameters[i].Value);
                        break;

                    case DbType.Boolean:
                        value = (dbCommand.Parameters[i].Value == DBNull.Value) ? "''" : "'" + Convert.ToString(dbCommand.Parameters[i].Value) + "'";
                        break;

                    default:
                        value = (dbCommand.Parameters[i].Value == DBNull.Value) ? "''" : "'" + Convert.ToString(dbCommand.Parameters[i].Value ?? "") + "'";
                        break;
                }

                query = query + dbCommand.Parameters[i].ParameterName + " = " + value + ",";
            }

            query = query.TrimEnd(',');
            return query;
        }

        public static string PrecisionForDecimal(decimal obj, int? precisionCount)
        {
            var str = "#,##0";
            if (precisionCount == null)
            {
                precisionCount = Convert.ToInt32(WebConfigurationManager.AppSettings["DefaultPrecision"]);
            }

            for (var i = 0; i < precisionCount; i++)
            {
                if (i == 0)
                {
                    str = str + ".0";
                }
                else
                {
                    str = str + "0";
                }
            }

            return obj.ToString(str);
        }
        public static DataTable SupressEmptyColumns(DataTable dtSource)
        {
            System.Collections.ArrayList columnsToRemove = new System.Collections.ArrayList();
            foreach (DataColumn dc in dtSource.Columns)
            {
                bool colEmpty = true;
                foreach (DataRow dr in dtSource.Rows)
                {
                    if (dr[dc.ColumnName] is DBNull || dr[dc.ColumnName].ToString() == "0.000000")
                    {

                    }
                    else
                    {
                        colEmpty = false;
                    }
                }
                if (colEmpty == true)
                {
                    columnsToRemove.Add(dc.ColumnName);
                }
            }
            foreach (string columnName in columnsToRemove)
            {
                dtSource.Columns.Remove(columnName);
            }
            return dtSource;
        }
        public static DataTable ToDataTable<T>(List<T> items, string name)
        {
            var dataTable = new DataTable(typeof(T).Name);
            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (var item in items)
            {
                var values = new object[props.Length];
                for (var i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }

                dataTable.Rows.Add(values);
            }

            switch (name)
            {
                case "ExpenseDetail":
                    dataTable.Columns.Remove("UserMessage");
                    dataTable.Columns.Remove("LedgerList");
                    dataTable.Columns.Remove("TotalAmount");
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["LedgerID"].SetOrdinal(1);
                    dataTable.Columns["ExpenseDetailRemarks"].SetOrdinal(2);
                    dataTable.Columns["SGST"].SetOrdinal(3);
                    dataTable.Columns["SGSTAmount"].SetOrdinal(4);
                    dataTable.Columns["CGST"].SetOrdinal(5);
                    dataTable.Columns["CGSTAmount"].SetOrdinal(6);
                    dataTable.Columns["Qty"].SetOrdinal(7);
                    dataTable.Columns["Amount"].SetOrdinal(8);
                    dataTable.Columns["IsDeleted"].SetOrdinal(9);
                    dataTable.Columns["ExpenseID"].SetOrdinal(10);
                    dataTable.Columns["IsProduct"].SetOrdinal(11);
                    dataTable.Columns["POIDItemID"].SetOrdinal(12);

                    break;

                case "ExpenseItems":
                    dataTable.Columns.Remove("UserMessage");
                    dataTable.Columns.Remove("ProductList");
                    dataTable.Columns.Remove("Amount");
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["ProductID"].SetOrdinal(1);
                    dataTable.Columns["ExpenseItemRemarks"].SetOrdinal(2);
                    dataTable.Columns["QTY"].SetOrdinal(3);
                    dataTable.Columns["Rate"].SetOrdinal(4);
                    dataTable.Columns["IsDeleted"].SetOrdinal(5);
                    dataTable.Columns["ExpenseID"].SetOrdinal(6);
                    dataTable.Columns["Typeid"].SetOrdinal(7);
                    break;

                case "InvoiceItem":
                    dataTable.Columns.Remove("UserMessage");
                    dataTable.Columns.Remove("ProductList");
                    dataTable.Columns.Remove("Amount");
                    dataTable.Columns.Remove("ProductName");
                    dataTable.Columns.Remove("IsChecked");
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["ProductID"].SetOrdinal(1);
                    dataTable.Columns["ItemRemarks"].SetOrdinal(2);
                    dataTable.Columns["QTY"].SetOrdinal(3);
                    dataTable.Columns["Rate"].SetOrdinal(4);
                    dataTable.Columns["SGST"].SetOrdinal(5);
                    dataTable.Columns["SGSTAmount"].SetOrdinal(6);
                    dataTable.Columns["CGST"].SetOrdinal(7);
                    dataTable.Columns["CGSTAmount"].SetOrdinal(8);
                    dataTable.Columns["IsDeleted"].SetOrdinal(9);
                    dataTable.Columns["InvoiceID"].SetOrdinal(10);
                    dataTable.Columns["IsProduct"].SetOrdinal(11);
                    dataTable.Columns["SOItemID"].SetOrdinal(12);
                    break;
                case "Payment":
                    dataTable.Columns.Remove("UserMessage");
                    dataTable.Columns.Remove("ProductList");
                    dataTable.Columns.Remove("Amount");
                    dataTable.Columns.Remove("ProductName");
                    dataTable.Columns.Remove("ProductID");
                    dataTable.Columns.Remove("ItemRemarks");
                    dataTable.Columns.Remove("QTY");
                    dataTable.Columns.Remove("SGST");
                    dataTable.Columns.Remove("Rate");
                    dataTable.Columns.Remove("CGST");
                    dataTable.Columns.Remove("CGSTAmount");
                    dataTable.Columns.Remove("IsProduct");
                    dataTable.Columns.Remove("IsChecked");
                    dataTable.Columns.Remove("SOItemID");
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["SGSTAmount"].SetOrdinal(1);
                    dataTable.Columns["SGSTAmount"].ColumnName = "Amount";
                    dataTable.Columns["IsDeleted"].SetOrdinal(2);
                    dataTable.Columns["InvoiceID"].SetOrdinal(3);
                    dataTable.Columns["InvoiceID"].ColumnName = "RefId";
                    break;

                case "InvoiceReceivable":
                    dataTable.Columns.Remove("UserMessage");
                    dataTable.Columns.Remove("InvoiceNo");
                    dataTable.Columns.Remove("Description");
                    dataTable.Columns.Remove("ReceivedAmount");
                    dataTable.Columns.Remove("InvoicedDate");
                    dataTable.Columns.Remove("DueDate");
                    dataTable.Columns.Remove("TotalAmount");
                    dataTable.Columns.Remove("OutstandingAmount");
                    dataTable.Columns.Remove("IsChecked");
                    dataTable.Columns.Remove("HoldingAmount");
                    dataTable.Columns.Remove("HoldingsCount");
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["InvoiceID"].SetOrdinal(1);
                    dataTable.Columns["ReceivableAmount"].SetOrdinal(2);
                    dataTable.Columns["IsDeleted"].SetOrdinal(3);
                    break;

                case "Journal":
                    dataTable.Columns.Remove("UserMessage");
                    dataTable.Columns.Remove("LedgerProductList");
                    dataTable.Columns.Remove("NameList");
                    dataTable.Columns.Remove("NameID");
                    dataTable.Columns.Remove("Description");
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["Credit"].SetOrdinal(1);
                    dataTable.Columns["Debit"].SetOrdinal(2);
                    dataTable.Columns["LedgerProductID"].SetOrdinal(3);
                    dataTable.Columns["IsDeleted"].SetOrdinal(4);
                    dataTable.Columns["Type"].SetOrdinal(5);

                    break;
                case "Payroll":
                    dataTable.Columns.Remove("Description");
                    dataTable.Columns.Remove("IsEditable");
                    dataTable.Columns.Remove("PayType");
                    dataTable.Columns.Remove("ComponentCode");
                    dataTable.Columns.Remove("Note");
                    dataTable.Columns.Remove("ConfigValue");
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["ComponentId"].SetOrdinal(1);
                    dataTable.Columns["EmployeePayStructureId"].SetOrdinal(2);
                    dataTable.Columns["Amount"].SetOrdinal(3);
                    dataTable.Columns["EmployeeId"].SetOrdinal(4);
                    break;

                case "SubordinatesList":
                    dataTable.Columns["Name"].SetOrdinal(0);
                    dataTable.Columns["IsDeleted"].SetOrdinal(1);
                    break;

                case "StockAdjustmentItem":
                    dataTable.Columns.Remove("ItemProductList");
                    dataTable.Columns.Remove("ItemProductName");
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["AdjustmentID"].SetOrdinal(1);
                    dataTable.Columns["ItemProductID"].SetOrdinal(2);
                    dataTable.Columns["AdjustedQty"].SetOrdinal(3);
                    dataTable.Columns["ItemStockonHand"].SetOrdinal(4);
                    dataTable.Columns["RemainingQty"].SetOrdinal(5);
                    dataTable.Columns["IsDeleted"].SetOrdinal(6);
                    break;

                case "FandFComponentDetailsBoList":
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["FandFId"].SetOrdinal(1);
                    dataTable.Columns["Type"].SetOrdinal(2);
                    dataTable.Columns["Description"].SetOrdinal(3);
                    dataTable.Columns["Amount"].SetOrdinal(4);
                    dataTable.Columns["IsEditableAmount"].SetOrdinal(5);
                    dataTable.Columns["Remarks"].SetOrdinal(6);
                    dataTable.Columns["IsEditableRemarks"].SetOrdinal(7);
                    dataTable.Columns["IsDeleted"].SetOrdinal(8);
                    dataTable.Columns["Ordinal"].SetOrdinal(9);
                    break;

                default:
                    dataTable.Columns.Remove("UserMessage");
                    dataTable.Columns.Remove("BillNo");
                    dataTable.Columns.Remove("Description");
                    dataTable.Columns.Remove("ExpenseAmount");
                    dataTable.Columns.Remove("PaidAmount");
                    dataTable.Columns.Remove("DueDate");
                    dataTable.Columns.Remove("OutstandingAmount");
                    dataTable.Columns.Remove("BillDate");
                    dataTable.Columns.Remove("IsChecked");
                    dataTable.Columns.Remove("HoldingAmount");
                    dataTable.Columns.Remove("HoldingsCount");
                    dataTable.Columns["ID"].SetOrdinal(0);
                    dataTable.Columns["ExpenseID"].SetOrdinal(1);
                    dataTable.Columns["PayAmount"].SetOrdinal(2);
                    dataTable.Columns["IsDeleted"].SetOrdinal(3);
                    break;
            }

            return dataTable;
        }
    }

    public static class AppConstants
    {
        public static string ApplicationRootUrl()
        {
            return HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath
                                                                     + HttpContext.Current.Request.Url.Query, string.Empty);
        }
    }
}
