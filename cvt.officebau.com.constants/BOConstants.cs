﻿namespace cvt.officebau.com.MessageConstants
{
    public class BOConstants
    {
        public const string EnterCustomer = "Please enter the Customer.";
        public const string EnterBusinessUnit = "Please enter the Business Unit.";
        public const string EnterEmailID = "Please enter the Email ID.";
        public const string InvalidEmailID = "Email ID is not valid.";
        public const string EnterContactNo = "Please enter the Contact No.";
        public const string EnterVersion = "Please enter the Version.";
        public const string EnterBroadcastMessage = "Please enter the Broadcast Message.";
        public const string EnterFrom = "Please select the From.";
        public const string EnterUntil = "Please select the Until.";
        public const string SelectCustomer = "Please select the Customer.";
        public const string Error = "An error occured while processing the data.";
        public const string ValidPunches = "Please enter the valid Punches.";
        public const string No_Records_Found = "No records found for the selected month and year.";
        public const string Successfully_Cleared = "successfully cleared the records!";
        public const string Resubmitted = "Resubmitted Successfully.";
        public const string Submitted = "Submitted Successfully.";
        public const string Configure_Loan = "Please Configure the Loan.";
        public const string Map_Selected_Desigination = "Please Map the Band and Grade for the selected Designation.";
        public const string Biometric_Code_already_mapped = "Biometric Code already mapped for another employee.";
        public const string Withdraw = " Withdraw Successfully.";
        public const string Cannot_Withdraw = "You Cannot Withdraw.";
        public const string Return_Company_Assets = "Please Return the Company Assets.";
        public const string cannot_Approve_Reject = "You cannot Approve/Reject.";
        public const string Code_Already_Exists = "Code Already Exists.";
        public const string Published = "Published Successfully";
        public const string Company_mapped = "Company was mapped Successfully.";
        public const string BusinessUnit_TDS_Record = "The selected BusinessUnit doesn't have any TDS records!";
        public const string Approved = "Approved Successfully.";
        public const string Rejected = "Rejected Successfully.";
        public const string Payroll_Not_Processed = "Payroll is not processed for the selected Month !";
        public const string Payslip_Sent = "Payslip has been Sent to the Employees!";
        public const string Excel_InValid = "Not a valid Excel.";
        public const string Claim_Amount_validation = "Claim amount validation is Successfully.";
        public const string Check_Excel = "Please check the excel file.";
        public const string Email_Not_Configured = "Email address is not configured for the selected Customer";
        public const string Email_Sent = "Email sent successfully";
        public const string Email_Invalid = "Email ID not provided or Invaild!";
        public const string User_Invalid = "Please enter valid User.";
        public const string Old_Password_Match = "Old Password does not match.";
        public const string InVaild_Old_Password = "Please enter the Valid Old Password.";
        public const string Session_Retained = "Your session retained successfully !";
        public const string Uploaded = "Uploaded Successfully.";

        public const string Select_Currency = "Please select the Currency.";
        public const string Select_Start_Month = "Please select the Start Month.";
        public const string Enter_NoOfRecords = "Please enter the No of Records.";
        public const string NoOfRecords_Range = "Please enter No of Records between 5 to 500.";
        public const string Enter_To_Address = "Please enter the To Address.";

        public const string Textbox_Range = "Please enter Loan % between 0 to 100.";
        public const string Select_BusinessUnit = "Please select the Business Unit.";

        public const string Enter_AppraisalName = "Please enter the Appraisal Name.";
        public const string Select_Appraisee_Start_Date = "Please select the Appraisee Start Date";
        public const string Select_Appraisee_End_Date = "Please select the Appraisee End Date";
        public const string Select_Appraiser_Start_Date = "Please select the Appraiser Start Date";
        public const string Select_Appraiser_End_Date = "Please select the Appraiser End Date";
        public const string Select_Eligibility_Date = "Please select the Eligibility Date";

        public const string Select_Date = "Please select the Date.";
        public const string Select_Month = "Please Select the Month.";
        public const string Select_Year = "Please Select the Year.";
        public const string Enter_Password = "Please enter the Password.";
        public const string Enter_Server = "Please enter the Server.";
        public const string Enter_UserCode = "Please enter the User Name.";

        public const string Enter_TableName = "Please enter the Table Name.";

        public const string Enter_BankName = "Please enter the Bank Name.";
        public const string BankName_Range = "Bank Name should not exceed 100 characters.";
        public const string Enter_DisplayName = "Please enter the Display Name.";
        public const string AccountNumber_Length = "Please enter the valid Account Number.";
        public const string Enter_AccountNumber = "Please enter the Account Number.";
        public const string Enter_Opening_Balance = "Please enter the Opening Balance.";
        public const string Opening_Balance_Validation = "The field Opening Balance must be a number.";
        public const string Enter_Limit = "Please enter the Limit.";
        public const string Amount_Validation = "Please enter the valid Amount.";
        public const string Enter_Opening_Bal_Date = "Please enter the Opening Balance Date.";
        public const string Enter_Account_Type = "Please enter the Account Type.";
        public const string Enter_Branch_Name = "Please enter the Branch Name.";
        public const string Enter_IFSC_Code = "Please enter the IFSC Code.";

        public const string select_Employee = "Please select the Employee.";
        public const string select_DropDown = "Please select a value from the Dropdown.";

        public const string Enter_Tag_Name = "Please enter the Tag Name.";
        public const string Enter_Amount = "Please enter the Amount.";

        public const string Enter_Description = "Please enter the Description.";
        public const string Enter_Region = "Please select the Region.";
        public const string Enter_Address = "Please enter the Address.";

        public const string Enter_HandoverOn = "Please enter the Given On.";
        public const string Enter_ReturnedOn = "Please enter the Returned On.";
        public const string Enter_Quantity = "Please enter the Quantity.";
        public const string Quantity_Range = "Please enter the Valid Quantity.";

        public const string Enter_CompanyName = "Please enter the Company Name.";
        public const string Enter_Company_FullName = "Please enter the Company FullName.";
        public const string PAN = "Not a valid PAN.";
        public const string Enter_Payment_Favour = "Please enter the Payment Favour.";
        public const string Enter_Employee_Code_Pattern = "Please enter the Employee Code Pattern.";
        public const string InValid_Communication_EmailId = "Communication Email ID is not valid.";

        public const string Enter_Name = "Please enter the  Name.";
        public const string Name_Range = "Name should not exceed 100 characters.";
        public const string select_Effective_From = "Please select the Effective From.";

        public const string Enter_Code = "Please enter the Employee Code.";

        public const string Enter_Payment_Terms = "Please enter the Payment Terms.";
        public const string Enter_CustomerName = "Please enter the Customer Name.";
        public const string CustomerName_Range = "Customer Name should not exceed 100 characters.";
        public const string select_Sales_Executive = "Please select the Sales Executive.";
        public const string select_status = "Please select the status.";
        public const string Business_Volume = "Please enter valid Monthly Business Volume.";
        public const string Enter_Customer_Enquiry_Name = "Please enter the Customer Enquiry Name.";

        public const string Enter_First_Name = "Please enter the First Name.";
        public const string Enter_Desigination = "Please enter the Designation.";
        public const string Enter_Department = "Please enter the Department.";

        public const string select_Document_Category = "Please select the Document Category.";
        public const string Enter_Document_Name = "Please enter the Document Name.";


        public const string Enter_Hourly_Rate = "Please enter the Hourly Rate.";
        public const string Enter_Fixed_Rate = "Please enter the Fixed Rate.";
        public const string Enter_Billing_Utilization = "Please enter the Billing Utilization.";
        public const string Enter_Remuneration_Utilization = "Please enter the Remuneration Utilization.";


        public const string select_Category = "Please select the Category.";
        public const string Enter_Claim_Amount = "Please enter the Claim Amount.";
        public const string select_Band = "Please select the Band.";
        public const string select_Grade = "Please select the Grade.";
        public const string select_Designation = "Please select the Designation.";
        public const string Enter_Metro_Amount = "Please enter the Metro Amount.";
        public const string Enter_NonMetro_Amount = "Please enter the Non-Metro Amount.";
        public const string Enter_Destination_City = "Please enter the Destination City.";
        public const string Enter_Travel_RequestNo = "Please enter the Travel Request No.";

        public const string Enter_Remarks = "Please enter the Remarks.";
        public const string select_HR_Approver = "Please select the HR Approver.";

        public const string select_Loan_Type = "Please select the Loan Type.";
        public const string Enter_Tenure = "Please enter the Tenure.";
        public const string Enter_Date_of_Realization = "Please enter the Date of Realization.";
        public const string Enter_ReferenceNo = "Please enter the Reference No.";
        public const string Enter_Financial_charges_ROI = "Please enter the Financial charges and ROI.";
        public const string Financial_charges_ROI_Check = "The field Financial charges and ROI must be a number.";
        public const string Enter_Payment_date = "Please enter the Payment Date.";
        public const string Financial_charges_ROI_Forclosure = "The field Financial charges and ROI for Forclosure must be a number.";

        public const string Enter_Login_Code = "Please enter the Login Code.";
        public const string Enter_EmployeeNo = "Please enter the Employee No.";
        public const string Enter_Last_Name = "Please enter the Last Name.";
        public const string Select_Department = "Please select the Department.";
        public const string Enter_Emergency_MobileNo = "Please enter the Emergency Mobile No.";
        public const string select_Gender = "Please select the Gender.";
        public const string Enter_DOJ = "Please enter the Date of Joining.";
        public const string Enter_DOB = "Please enter the Date of Birth.";
        public const string Enter_ReportingTo = "Please enter the Reporting To.";
        public const string select_Employment_Type = "Please select the Employment Type.";
        public const string select_Prefix = "Please select the Prefix.";
        public const string Enter_Aadhar_Card = "Please enter the Aadhar Card.";
        public const string select_Reason = "Please select the Reason.";
        public const string Enter_Inactive_From = "Please enter the Inactive From.";
        public const string Enter_Contractor = "Please enter the Contractor.";
        public const string Enter_Bank_AccountNo = "Please enter the Bank Account No.";
        public const string Enter_PAN = "Please enter the PAN.";
        public const string Enter_AadharNo = "Please enter the Aadhar No.";
        public const string Enter_Title = "Please select the Title.";
        public const string upload_file = "Please upload the file.";
        public const string select_Document_Type = "Please select the Document Type.";
        public const string select_Relationship = "Please select the Relationship.";
        public const string Enter_Last_Designation = "Please enter the Last Designation.";
        public const string Enter_Skills = "Please select the Skills.";
        public const string select_Level = "Please select the Level.";
        public const string Enter_Reason = "Please enter the Reason.";
        public const string Enter_Relieving_Date = "Please enter the Exp. Relieving Date.";
        public const string Enter_Tentative_Relieving_Date = "Please enter the Tentative Relieving Date.";
        public const string Enter_HR_Approver = "Please enter the HR Approver.";
        public const string Enter_Exit_Feedback = "Please enter the Exit Feedback.";
        public const string Enter_RelievedOn = "Please enter the Relieved On.";
        public const string Enter_Gross = "Please enter the Gross.";
        public const string select_Company_Paystructure = "Please select the Company Paystructure.";
        public const string Enter_Vendor_Name = "Please enter the Vendor Name.";
        public const string Vendor_Name_Range = "Vendor Name should not exceed 100 characters.";
        public const string Enter_Reference = "Please enter the Reference.";
        public const string Enter_Due_Date = "Please enter the Due Date.";
        public const string Enter_Bill_Date = "Please enter the Bill Date.";
        public const string Enter_Cost_Center = "Please select the Cost Center.";

        public const string Enter_Employee_Name = "Please enter the Employee Name.";
        public const string Enter_Last_Working_Date = "Please enter the Last Working Date.";


        public const string Enter_Reason_for_Leaving = "Please enter the Reason for Leaving.";
        public const string Enter_Notice_Days = "Please enter the Notice Days.";
        public const string Enter_Leave_Encashed_Days = "Please enter the Leave Encashed Days.";
        public const string Enter_LOP_Days = "Please enter the LOP Days.";

        public const string Year_Validation = "Please enter the Valid Year.";
        public const string Start_Year = "Please enter the Start Year.";

        public const string select_Type = "Please select the Type.";
        public const string Enter_Group = "Please enter the Group.";

        public const string EnterDate = "Please enter the Date.";
        public const string Enter_Full_Day = "Please enter Full Day.";
        public const string Enter_valid_Full_Day = "Please enter valid Full Day.";
        public const string Enter_Half_Day = "Please enter Half Day.";
        public const string Enter_valid_Half_Day = "Please enter valid Half Day.";

        public const string Enter_Party_Name = "Please enter the Party Name.";
        public const string Party_Name_Range = "Party Name should not exceed 100 characters.";
        public const string Enter_Invoice_No = "Please enter the Invoice No.";
        public const string Invoice_No_Range = "Invoice No should not exceed 100 characters.";
        public const string Enter_Billing_Address = "Please enter the Billing Address.";
        public const string Billing_Address_Range = "Billing Address should not exceed 800 characters.";
        public const string Valid_Currency = "Please enter the valid Currency.";
        public const string Enter_Discount_Value = "Please enter the Discount Value.";
        public const string Enter_Discount_Percentage = "Please enter the Discount Percentage.";
        public const string Percentage_value_Range = "Percentage value must be range from 1 to 100.";
        public const string Enter_Received_Date = "Please enter the Received Date.";
        public const string Enter_Invoice_Date = "Please enter the Invoice Date.";

        public const string EnterTitle = "Please enter the Title.";
        public const string Select_Meeting_Date = "Please select the Meeting Date.";
        public const string Enter_Meeting_Time = "Please enter the Meeting Time.";
        public const string Enter_Location = "Please enter the Location.";
        public const string select_Organized_By = "Please select the Organized By.";
        public const string select_Meeting_Type = "Please select the Meeting Type.";

        public const string select_Product = "Please select the Product.";
        public const string select_Ledger = "Please select the Ledger";
        public const string select_Adjusted_Date = "Please select the Adjusted Date.";
        public const string select_Party = "Please select the Party.";

        public const string Enter_Journal_No = "Please enter the Journal No.";
        public const string Journal_No_Range = "Journal No should not exceed 20 characters.";

        public const string Select_Leave_Type = "Please Select the Leave Type.";
        public const string Select_Approver = "Please Select the Approver.";
        public const string Enter_Approver = "Please enter the Approver.";
        public const string Enter_Available_Leaves = "Please enter the Available Leaves.";
        public const string Enter_Leave_Code = "Please enter the Leave Code.";
        public const string Select_Region = "Please Select the Region.";
        public const string Enter_Leave_Type = "Please enter the Leave Type.";
        public const string Enter_From_Date = "Please enter From Date.";
        public const string Enter_TO_Date = "Please enter To Date.";
        public const string Select_Punch_Type = "Please Select the Punch Type.";
        public const string Enter_Time = "Please enter the Time.";
        public const string Select_Replacement_Employee = "Please Select the Replacement Employee.";
        public const string Enter_Vacation_Name = "Please enter the Vacation Name.";
        public const string Select_Purpose = "Please Select the Purpose.";

        public const string Enter_Ledger = "Please enter the Ledger.";
        public const string Account_Range = "Account should not exceed 100 characters.";

        public const string Enter_Event = "Please select the Event.";
        public const string Enter_CC_Recipients = "Please enter the CC Recipients.";

        public const string EnterCode = "Please enter the Code.";

        public const string Enter_Event_Name = "Please enter Event Name.";
        public const string Enter_Event_Description = "Please enter Event Description.";

        public const string Select_Financial_Year = "Please select the Financial Year.";
        public const string Select_From_Month = "Please select the From Month.";
        public const string Select_From_Year = "Please select the From Year.";
        public const string Select_To_Month = "Please select the To Month.";
        public const string Select_To_Year = "Please select the To Year.";

        public const string Enter_Percentage = "Please enter Percentage between 1 to 100.";
        public const string Enter_Employee_PF_Percentage = "Please enter Employee PF Percentage.";
        public const string Enter_Employee_PF_Limit = "Please enter Employee PF Limit.";
        public const string Enter_Employee_PF_Value = "Please enter Employee PF Value.";
        public const string Enter_Employer_PF_Percentage = "Please enter Employer PF Percentage.";
        public const string Enter_Employer_PF_Limit = "Please enter Employer PF Limit.";
        public const string Enter_Employer_PF_Value = "Please enter Employer PF Value.";
        public const string Enter_Employee_ESI_Percentage = "Please enter Employee ESI Percentage.";
        public const string Enter_Employee_ESI_Limit = "Please enter Employee ESI Limit.";
        public const string Enter_Employee_ESI_Value = "Please enter Employee ESI Value.";
        public const string Enter_Employer_ESI_Percentage = "Please enter Employer ESI Percentage.";
        public const string Enter_Employer_ESI_Limit = "Please enter Employer ESI Limit.";
        public const string Enter_Employer_ESI_Value = "Please enter Employer ESI Value.";
        public const string Enter_Min_Gross = "Please enter Min Gross.";
        public const string Enter_Max_Gross = "Please enter Max Gross.";
        public const string Enter_Amount_Deducted = "Please enter Amount to be Deducted.";

        public const string Enter_Ordinal = "Please enter the Ordinal.";

        public const string Enter_SGST = "Please enter the SGST.";
        public const string Value_Range = "Value for {0} must be between {1} and {2}.";
        public const string Enter_Valid_SGST = "Please enter the valid SGST.";
        public const string Enter_Valid_CGST = "Please enter the valid CGST.";
        public const string Enter_CGST = "Please enter the CGST.";
        public const string Enter_HSN_Code = "Please enter the HSN Code.";

        public const string Enter_valid_Opening_Stock = "Please enter the valid Opening Stock.";
        public const string Select_Transaction_Type = "Please select the Transaction Type.";
        public const string Enter_IGST = "Please enter the IGST.";
        public const string Enter_Valid_IGST = "Please enter the valid IGST.";

        public const string Enter_Question = "Please enter the Question.";

        public const string Enter_TEU = "Please enter the TEU.";
        public const string Enter_TUE = "Please enter the TUE.";
        public const string Enter_Rake_No = "Please enter the Rake No.";
        public const string Enter_Import_Date = "Please enter the Import Date.";
        public const string Enter_Export_Date = "Please enter the Export Date.";

        public const string Enter_Planned_TEU = "Please enter the Planned TEU.";
        public const string Valid_Planned_TEU = "Please enter valid Planned TEU.";

        public const string valid_Declaration = "Please enter the valid Declaration.";
        public const string select_Form_Type = "Please select the Form Type.";
        public const string valid_Submitted = "Please enter the valid Submitted.";
        public const string valid_Cleared = "Please enter the valid Cleared.";
        public const string valid_Rejected = "Please enter the valid Rejected.";

        public const string Enter_Ceiling_Amount = "Please enter the Ceiling Amount.";
        public const string Ceiling_Amount_Validation = "The field Ceiling Amount must be a number.";
        public const string select_Section = "Please select the Section.";
        public const string select_CTC = "Please select the CTC.";
        public const string select_Basic = "Please select the Basic.";
        public const string select_Medical = "Please select the Medical.";
        public const string select_Conveyance = "Please select the Conveyance.";
        public const string select_HRA = "Please select the HRA.";

        public const string Enter_Window_Opens = "Please enter the Window Opens on.";
        public const string Valid_Month_Open_Date = "Please enter the valid Month Open Date.";
        public const string Enter_Window_Closes = "Please enter the Window Closes on.";
        public const string Valid_Month_Close_Date = "Please enter the valid Month Close Date.";
        public const string Enter_Engine_Process_Date = "Please enter the Engine Process Date.";

        public const string Valid_Percentage = "Please enter the valid Percentage.";
        public const string Enter_Window_Close_Date = "Please enter the Window Close Date.";
        public const string Enter_Window_Open_Date = "Please enter the Window Open Date.";
        public const string Enter_Engine_Process_Time = "Please enter the Engine Process Time.";

        public const string Enter_Voucher_Date = "Please enter the  Voucher Date.";
        public const string Enter_TransferFrom = "Please enter the TransferFrom.";
        public const string Enter_TransferTo = "Please enter the TransferTo.";

        public const string Enter_Total_Amount = "Please enter the Total Amount.";
        public const string Total_Amount_Validation = "Total Amount Must be greater than 10.";
        public const string Enter_Purpose_of_Travel = "Please enter the Purpose of Travel.";
        public const string Enter_Travel_From = "Please enter the Travel From.";
        public const string Enter_Travel_To = "Please enter the Travel To.";
        public const string Enter_Travel_Date = "Please enter Travel Date.";
        public const string Select_Travel_Type = "Please select the Travel Type.";
        public const string Select_Mode_of_Transport = "Please select the Mode of Transport.";

        public const string Space_Validation = "No spaces allowed.";
        public const string Enter_New_Password = "Please enter the New Password.";
        public const string Enter_Confirm_Password = "Please enter the Confirm Password.";
    }
}
