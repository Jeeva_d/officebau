﻿using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace cvt.officebau.com.Content.Utility
{
    public class TdsEngineBo : BaseBo
    {
        public decimal? TaxableAmount { get; set; }
        public decimal? HRADeclarationAmount { get; set; }
        public decimal? HraExemptionAmount { get; set; }
        public string Component { get; set; }
        public string Sections { get; set; }
        public decimal? BasicAmount { get; set; }
        public decimal? PreBasicAmount { get; set; }
        public decimal? HRAAMount { get; set; }
        public int NoMonth { get; set; }
        public decimal? Conveyance { get; set; }
        public decimal? Medical { get; set; }
        public decimal? PreCTC { get; set; }
        public decimal? PrePT { get; set; }
        public decimal? PreMedical { get; set; }
        public decimal? PreConveyance { get; set; }
        public decimal? PreHRAAMount { get; set; }
        public decimal? CTC { get; set; }
        public decimal? PT { get; set; }
        public decimal? EEPF { get; set; }
        public decimal? Bonus { get; set; }
        public decimal? PLI { get; set; }
        public decimal? OtherIncome { get; set; }
        public decimal? SpecialAllowance { get; set; }
        public decimal? DeclarationAmount { get; set; }
        public DateTime? DOB { get; set; }
        public decimal ITPaid { get; set; }
        public int IsProof { get; set; }
        public decimal? StandardDeduction { get; set; }
        public decimal? PreStandardDeduction { get; set; }
        public int TDSExemptionMonths { get; set; }
    }

    public class TdsCalculator
    {
        public TdsCalculationBo Calculatedetails(int employeeid, int yearId, int monthid, int domainID, string payrollProcessed)
        {
            _initialDomainId = domainID;
            TdsCalculationBo ttd = new TdsCalculationBo();
            _employeeId = employeeid;
            using (SqlConnection con = new SqlConnection(_connection))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.TDS_HRADETAILSENGINE, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, employeeid);
                    cmd.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.Month, monthid);
                    con.Open();
                    _tdsEngineBo = new TdsEngineBo();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            _tdsEngineBo.NoMonth = Convert.ToInt32(reader[DBParam.Output.NOOFMonth]);
                            _tdsEngineBo.BasicAmount = Convert.ToDecimal(reader[DBParam.Output.Basic]);
                            _tdsEngineBo.HRAAMount = Convert.ToDecimal(reader[DBParam.Output.HRA]);
                            _tdsEngineBo.HRADeclarationAmount = Convert.ToDecimal(reader[DBParam.Output.Declartion]);
                            _tdsEngineBo.Conveyance = Convert.ToDecimal(reader[DBParam.Output.Conveyance]);
                            _tdsEngineBo.Medical = Convert.ToDecimal(reader[DBParam.Output.MedicalAllowance]);
                            _tdsEngineBo.StandardDeduction = Convert.ToDecimal(reader[DBParam.Output.StandardDeduction]);
                            _tdsEngineBo.CTC = Convert.ToDecimal(reader[DBParam.Output.CTC]);
                            _tdsEngineBo.PT = Convert.ToDecimal(reader[DBParam.Output.PT]);
                            _tdsEngineBo.Bonus = Convert.ToDecimal(reader[DBParam.Output.Bonus]);
                            _tdsEngineBo.PLI = Convert.ToDecimal(reader[DBParam.Output.PLI]);
                            _tdsEngineBo.OtherIncome = Convert.ToDecimal(reader[DBParam.Output.OtherIncome]);
                            _financialYear = Convert.ToInt32(reader[DBParam.Output.FY]);
                            _tdsEngineBo.PreCTC = Convert.ToDecimal(reader[DBParam.Output.SumofCTC]);
                            _tdsEngineBo.PrePT = Convert.ToDecimal(reader[DBParam.Output.SumofPT]);
                            _tdsEngineBo.PreHRAAMount = Convert.ToDecimal(reader[DBParam.Output.SumofHRA]);
                            _tdsEngineBo.PreMedical = Convert.ToDecimal(reader[DBParam.Output.SumofMedicalAllowance]);
                            _tdsEngineBo.PreConveyance = Convert.ToDecimal(reader[DBParam.Output.SumofConveyance]);
                            _tdsEngineBo.PreStandardDeduction = Convert.ToDecimal(reader[DBParam.Output.SumofStandardDeduction]);
                            _tdsEngineBo.PreBasicAmount = Convert.ToDecimal(reader[DBParam.Output.SumofBasic]);
                            _tdsEngineBo.DOB = Utilities.Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOB]);
                            _tdsEngineBo.ITPaid = Convert.ToDecimal(reader[DBParam.Output.TDSPaid]);
                            ttd.DOJ = Convert.ToDateTime(reader[DBParam.Output.DateOfJoin]);
                            ttd.FY = Convert.ToInt32(reader[DBParam.Output.YearName]);
                            ttd.Month = reader[DBParam.Output.MonthNames].ToString();
                            ttd.Name = reader[DBParam.Output.Name].ToString();
                            ttd.PanNo = reader[DBParam.Output.PanNo].ToString();
                            ttd.IsProof = Convert.ToInt32(reader[DBParam.Output.IsProof].ToString());
                            ttd.CessPrecentage = Convert.ToInt32(reader[DBParam.Output.CessPrecentage].ToString());
                            ttd.TDSExemptionMonths = Convert.ToInt32(reader[DBParam.Output.TDSExemptionMonths].ToString());
                        }
                    }
                }
            }

            ttd.Ctc = _tdsEngineBo.CTC * _tdsEngineBo.NoMonth + _tdsEngineBo.PreCTC;
            ttd.Pt = _tdsEngineBo.PrePT;
            ttd.Bonus = _tdsEngineBo.Bonus;
            ttd.PLI = _tdsEngineBo.PLI;
            ttd.OtherIncome = _tdsEngineBo.OtherIncome;
            ttd.ProCTC = _tdsEngineBo.CTC * _tdsEngineBo.NoMonth;
            ttd.ProPT = _tdsEngineBo.PT * _tdsEngineBo.NoMonth;
            ttd.ProConveyanceAnnaum = _tdsEngineBo.Conveyance * _tdsEngineBo.NoMonth;
            ttd.ProMedicalperannum = _tdsEngineBo.Medical * _tdsEngineBo.NoMonth;
            _tdsEngineBo.PreStandardDeduction = _tdsEngineBo.StandardDeduction * (totalMonths - ttd.TDSExemptionMonths - _tdsEngineBo.NoMonth);
            ttd.ProStandardDeductionAnnaum = _tdsEngineBo.StandardDeduction * _tdsEngineBo.NoMonth;
            ttd.ActCTC = _tdsEngineBo.PreCTC;
            ttd.ActPT = _tdsEngineBo.PrePT;
            ttd.ActConveyanceAnnaum = _tdsEngineBo.PreConveyance;
            ttd.ActMedicalperannum = _tdsEngineBo.PreMedical;
            ttd.ActStandardDeductionAnnaum = _tdsEngineBo.PreStandardDeduction;
            ttd.HRA = TaxableAmountHra();
            ttd.ConveyanceAnnaum = TaxableAmountConveyance();
            ttd.Medicalperannum = TaxableAmountMedical(ttd.IsProof);
            ttd.StandardDeductionAnnaum = TaxableAmountStandardDeduction();
            _age = DateTime.Now.Year - (_tdsEngineBo.DOB?.Year ?? DateTime.Now.Year);
            if (payrollProcessed == "1")
            {
                _tdsEngineBo.NoMonth = 0;
                ttd.ActCTC = ttd.Ctc;
                ttd.ProCTC = 0;
                ttd.ActConveyanceAnnaum = _tdsEngineBo.PreConveyance + ttd.ProConveyanceAnnaum;
                ttd.ProConveyanceAnnaum = 0;
                ttd.ActMedicalperannum = _tdsEngineBo.PreMedical + ttd.ProMedicalperannum;
                ttd.ProMedicalperannum = 0;
                ttd.ActPT = _tdsEngineBo.PrePT + ttd.ProPT;
                ttd.ProPT = 0;
            }

            ttd.tax80CC = Tax80Cc();
            ttd.tax80D = Tax80D(ttd.IsProof, ttd.FY);
            ttd.tax80U = Tax80U();
            ttd.tax80TTA = Tax80Tta(ttd.FY);
            ttd.tax80TTB = Tax80Ttb();
            ttd.tax20 = Tax20();
            ttd.tax80G = Tax80G();
            ttd.tax80E = Tax80E();
            List<TdsEngineBo> list80U = ComponentDeclarationNoLimit();
            ttd.OtherTaxDetails = list80U.Where(n => n.Sections != "80E").Sum(a => a.DeclarationAmount);
            //ttd.tax80E = list80U.Where(n => n.Sections == "80E").Sum(a => a.DeclarationAmount);
            ttd.ITPaid = _tdsEngineBo.ITPaid;
            Calculate(employeeid, yearId, monthid, 0, string.Empty, _initialDomainId, payrollProcessed, ttd.FY);
            ttd.Surcharge = _surchargeVal;
            ttd.TotalTax = _totalTax;
            if (payrollProcessed == "1")
            {
                ttd.NoMonth = 0;
            }
            else
            {
                ttd.NoMonth = _tdsEngineBo.NoMonth;
            }

            return ttd;
        }

        // GET: TDSEngine
        public decimal? Calculate(int employeeid, int yearId, int monthid, int savevalue, string report, int domainID, string payrollProcessed, int? FinancialYear)
        {
            _employeeId = employeeid;
            _initialDomainId = domainID;
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.TDS_HRADETAILSENGINE, connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, employeeid);
                    cmd.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.Month, monthid);
                    connection.Open();
                    _tdsEngineBo = new TdsEngineBo();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            _tdsEngineBo.NoMonth = Convert.ToInt32(reader[DBParam.Output.NOOFMonth]);
                            _tdsEngineBo.BasicAmount = Convert.ToDecimal(reader[DBParam.Output.Basic]);
                            _tdsEngineBo.HRAAMount = Convert.ToDecimal(reader[DBParam.Output.HRA]);
                            _tdsEngineBo.HRADeclarationAmount = Convert.ToDecimal(reader[DBParam.Output.Declartion]);
                            _tdsEngineBo.Conveyance = Convert.ToDecimal(reader[DBParam.Output.Conveyance]);
                            _tdsEngineBo.Medical = Convert.ToDecimal(reader[DBParam.Output.MedicalAllowance]);
                            _tdsEngineBo.CTC = Convert.ToDecimal(reader[DBParam.Output.CTC]);
                            _tdsEngineBo.PT = Convert.ToDecimal(reader[DBParam.Output.PT]);
                            _tdsEngineBo.Bonus = Convert.ToDecimal(reader[DBParam.Output.Bonus]);
                            _tdsEngineBo.PLI = Convert.ToDecimal(reader[DBParam.Output.PLI]);
                            _tdsEngineBo.OtherIncome = Convert.ToDecimal(reader[DBParam.Output.OtherIncome]);
                            _financialYear = Convert.ToInt32(reader[DBParam.Output.FY]);
                            _tdsEngineBo.PreCTC = Convert.ToDecimal(reader[DBParam.Output.SumofCTC]);
                            _tdsEngineBo.PrePT = Convert.ToDecimal(reader[DBParam.Output.SumofPT]);
                            _tdsEngineBo.PreHRAAMount = Convert.ToDecimal(reader[DBParam.Output.SumofHRA]);
                            _tdsEngineBo.PreMedical = Convert.ToDecimal(reader[DBParam.Output.SumofMedicalAllowance]);
                            _tdsEngineBo.PreConveyance = Convert.ToDecimal(reader[DBParam.Output.SumofConveyance]);
                            _tdsEngineBo.PreBasicAmount = Convert.ToDecimal(reader[DBParam.Output.SumofBasic]);
                            _tdsEngineBo.DOB = Utilities.Utility.CheckDbNull<DateTime>(reader[DBParam.Output.DOB]);
                            _tdsEngineBo.ITPaid = Convert.ToDecimal(reader[DBParam.Output.TDSPaid]);
                            _tdsEngineBo.IsProof = Convert.ToInt32(reader[DBParam.Output.IsProof].ToString());
                            _tdsEngineBo.StandardDeduction = Convert.ToDecimal(reader[DBParam.Output.StandardDeduction]);
                            _tdsEngineBo.PreStandardDeduction = Convert.ToDecimal(reader[DBParam.Output.SumofStandardDeduction]);
                            _cessPercentage = Convert.ToInt32(reader[DBParam.Output.CessPrecentage].ToString());
                            _tdsEngineBo.TDSExemptionMonths = Convert.ToInt32(reader[DBParam.Output.TDSExemptionMonths].ToString());
                        }
                    }
                }
            }

            if (_tdsEngineBo.NoMonth == 0)
            {
                TdsRepository tdsRepository = new TdsRepository();
                tdsRepository.ManageTds(_employeeId, monthid, yearId, 0);
                return 0;
            }

            _tdsEngineBo.PreStandardDeduction = _tdsEngineBo.StandardDeduction * (totalMonths - _tdsEngineBo.TDSExemptionMonths - _tdsEngineBo.NoMonth);

            _age = DateTime.Now.Year - _tdsEngineBo.DOB.Value.Year;
            decimal? taxHraPerAnnual = TaxableAmountHra();
            decimal? taxConveyanceAnnual = TaxableAmountConveyance();
            decimal? taxMedicalPerAnnual = TaxableAmountMedical(_tdsEngineBo.IsProof);
            decimal? taxStandardDeduction = TaxableAmountStandardDeduction();
            //sum off Tax Exemption
            decimal? taxableIncome = taxHraPerAnnual + taxMedicalPerAnnual + taxConveyanceAnnual + taxStandardDeduction;

            taxableIncome = _tdsEngineBo.CTC * _tdsEngineBo.NoMonth + _tdsEngineBo.PreCTC - _tdsEngineBo.PrePT - taxableIncome + _tdsEngineBo.Bonus + _tdsEngineBo.PLI + _tdsEngineBo.OtherIncome;

            // Calculate the Deduction based on the declaration or Clearance.
            decimal? tax80Cc = Tax80Cc();
            decimal? tax80D = Tax80D(_tdsEngineBo.IsProof, FinancialYear);
            decimal? tax80U = Tax80U();
            decimal? tax80Tta = Tax80Tta(FinancialYear);
            decimal? tax80Ttb = Tax80Ttb();
            decimal? tax20 = Tax20();
            decimal? tax80G = Tax80G();
            decimal? tax80E = Tax80E();
            List<TdsEngineBo> list80U = ComponentDeclarationNoLimit();
            decimal? taxNoLimitSum = list80U.Sum(a => a.DeclarationAmount);
            taxableIncome = taxableIncome - (tax80Cc + tax80D + tax80U + tax80Tta + tax80Ttb + taxNoLimitSum + tax20 + tax80E + tax80G);
            //taxable amount
            decimal? taxable = 0;
            if (_age < 60)
            {
                if (payrollProcessed == "1")
                {
                    _tdsEngineBo.NoMonth = 1;
                }

                taxable = (PayableTax(taxableIncome, false, _cessPercentage) - _tdsEngineBo.ITPaid) / _tdsEngineBo.NoMonth;
            }
            else
            {
                if (payrollProcessed == "1")
                {
                    _tdsEngineBo.NoMonth = 1;
                }

                taxable = (PayableTax(taxableIncome, true, _cessPercentage) - _tdsEngineBo.ITPaid) / _tdsEngineBo.NoMonth;
            }

            TdsRepository tds = new TdsRepository();
            if (savevalue == 1 && report != "REPORT")
            {
                tds.ManageTds(_employeeId, monthid, yearId, Math.Ceiling(Convert.ToDecimal(taxable)));
            }

            return Math.Ceiling(Convert.ToDecimal(taxable));
        }

        #region Supporting Functions

        public decimal? GetTdsLimitForCalculation(string section, int financialYear)
        {
            _resultTdsSectionValue = 0;
            using (SqlConnection con = new SqlConnection(_connection))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.GET_TDSLIMIT_FORCALCULATION, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, _employeeId);
                    cmd.Parameters.AddWithValue(DBParam.Input.Section, section);
                    cmd.Parameters.AddWithValue(DBParam.Input.FYID, financialYear);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, _initialDomainId);
                    con.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            _resultTdsSectionValue = Convert.ToDecimal(reader[DBParam.Output.Limit]);
                        }
                    }
                }
            }

            return _resultTdsSectionValue;
        }

        #endregion Supporting Functions

        #region Variables

        private TdsEngineBo _tdsEngineBo = new TdsEngineBo();

        private readonly string _connection = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
        private readonly decimal? _exemptionConveyance = 1600;
        private readonly decimal? _exemptionMedicalPerAnnual = 15000;
        private readonly decimal? _exemptionStandardDeductionAnnual = 50000;

        //decimal? eightyCC = 150000;
        //decimal? eightyU = 100000;
        //decimal? eightyTTA = 10000;
        private decimal? _eightyDFamily;

        private int _financialYear;
        private int _initialDomainId;
        private decimal? _resultTdsSectionValue = 0;
        private int _age;
        private int _employeeId;
        private decimal? _totalTax = 0;
        private decimal _surchargeVal;
        private readonly int totalMonths = 12;
        private int _cessPercentage;

        #endregion Variables

        #region FindTaxableIncome from Paystructure

        #region HRA

        private decimal? TaxableAmountHra()
        {
            //Calculation Base ON scenarios least must consider

            //Scenario 1
            //Amount fully received from HRA
            decimal? totalHraPerAnnual = _tdsEngineBo.HRAAMount * _tdsEngineBo.NoMonth + _tdsEngineBo.PreHRAAMount;

            //Scenario 2
            //Actual rent paid less 10% of basic
            decimal? declarationAmount = _tdsEngineBo.HRADeclarationAmount - (_tdsEngineBo.BasicAmount * _tdsEngineBo.NoMonth + _tdsEngineBo.PreBasicAmount) * 10 / 100;

            //scenario 3
            //if he/she lives in metro city 50% off Basic
            //if he/she lives non metro then 40% off Basic

            decimal? basicTaxableHra = _tdsEngineBo.BasicAmount * _tdsEngineBo.NoMonth + _tdsEngineBo.PreBasicAmount * 50 / 100;

            //get min value of above three scenario
            decimal?[] hraPerAnnual = { totalHraPerAnnual, declarationAmount, basicTaxableHra };

            if (hraPerAnnual.Min() < 0)
            {
                return 0;
            }

            return hraPerAnnual.Min();
        }

        #endregion HRA

        #region Conveyance

        private decimal? TaxableAmountConveyance()
        {
            decimal? taxableConveyancePerAnnual = 0;
            // maximum limit of conveyance per month check
            taxableConveyancePerAnnual = _tdsEngineBo.Conveyance * _tdsEngineBo.NoMonth + _tdsEngineBo.PreConveyance;

            if (taxableConveyancePerAnnual > _exemptionConveyance * 12)
            {
                taxableConveyancePerAnnual = _exemptionConveyance * 12;
            }

            return taxableConveyancePerAnnual;
        }

        #endregion Conveyance

        #region Medical

        private decimal? TaxableAmountMedical(int isProof)
        {
            decimal? taxableMedicalPerAnnual = 0;
            int noOfMonth = _tdsEngineBo.NoMonth;
            decimal? preMedical = _tdsEngineBo.PreMedical;
            if (isProof == 1)
            {
                noOfMonth = 1;
                preMedical = 0;
            }

            // maximum limit of medical per Anum check
            if (_tdsEngineBo.Medical * noOfMonth + preMedical < _exemptionMedicalPerAnnual)
            {
                taxableMedicalPerAnnual = _tdsEngineBo.Medical * noOfMonth + preMedical;
            }
            else
            {
                taxableMedicalPerAnnual = _exemptionMedicalPerAnnual;
            }

            return taxableMedicalPerAnnual;
        }

        #endregion Medical

        #region Standard Deduction

        private decimal? TaxableAmountStandardDeduction()
        {
            decimal? taxableStandardDeductionPerAnnual = 0;
            // maximum limit of conveyance per month check
            taxableStandardDeductionPerAnnual = _tdsEngineBo.StandardDeduction * _tdsEngineBo.NoMonth + _tdsEngineBo.PreStandardDeduction;

            if (taxableStandardDeductionPerAnnual > _exemptionStandardDeductionAnnual)
            {
                taxableStandardDeductionPerAnnual = _exemptionStandardDeductionAnnual;
            }

            return taxableStandardDeductionPerAnnual;
        }

        #endregion Standard Deduction

        #endregion FindTaxableIncome from Paystructure

        #region FindTaxableIncome from Tax Sections

        #region 80CC Calculation

        //for 80CC the sum of declaration should not exceed the max limit 80CC
        private decimal? Tax80Cc()
        {
            List<TdsEngineBo> list80Cc = ComponentDeclaration("80C");

            //sum off all components

            decimal? tax80CcSum = list80Cc.Where(n => n.Sections == "80C" || n.Sections == "80CCC").Sum(a => a.DeclarationAmount);
            decimal employerPf = 0;
            employerPf = Convert.ToDecimal(list80Cc.Where(n => n.Sections == "80C").Select(a => a.EEPF).FirstOrDefault());
            decimal? tax80CcdSum = list80Cc.Where(n => n.Sections == "80CCD").Sum(a => a.DeclarationAmount);
            if (tax80CcdSum > 50000)
            {
                tax80CcdSum = 50000;
            }

            decimal? eightyCc = GetTdsLimitForCalculation("80C", _financialYear);
            //check whether the sum exceed  the limit if it exceed then Exemption will taken the max limit
            if (tax80CcSum + employerPf > eightyCc)
            {
                tax80CcSum = eightyCc;
                return tax80CcSum + tax80CcdSum;
            }

            return tax80CcSum + employerPf + tax80CcdSum;
        }

        #endregion 80CC Calculation

        #region 80G Calculation

        //for 80CC the sum of declaration should not exceed the max limit 80CC
        private decimal? Tax80G()
        {
            List<TdsEngineBo> list80G = ComponentDeclaration("80G");
            decimal? tax80GSum = list80G.Where(n => n.Sections == "80G").Sum(a => a.DeclarationAmount);
            return tax80GSum;
        }

        #endregion 80G Calculation

        #region 80D Calculation

        //for 80D the sum of declaration should not exceed the max limit 80D
        private decimal? Tax80D(int isProof, int? financialYear)
        {
            int isSeniorCitizenValue = 0;

            isSeniorCitizenValue = financialYear <= 2017 ? 30000 : 50000;

            _eightyDFamily = _age < 60 ? 25000 : 30000;

            List<TdsEngineBo> list80D = ComponentDeclaration("Mediclaim");

            decimal? taxFamily = list80D.Where(n => n.Sections == "Mediclaim").Sum(a => a.DeclarationAmount);
            int isSeniorCitizenIncluded = Convert.ToInt32(list80D.Where(n => n.Component == "Is Senior citizens included.").Sum(a => a.DeclarationAmount));
            if (isSeniorCitizenIncluded > 0)
            {
                taxFamily = taxFamily - 1;
                if (taxFamily > _eightyDFamily + isSeniorCitizenValue)
                {
                    taxFamily = _eightyDFamily + isSeniorCitizenValue;
                }
            }
            else
            {
                if (taxFamily > _eightyDFamily)
                {
                    taxFamily = _eightyDFamily;
                }
            }

            list80D = new List<TdsEngineBo>();
            list80D = ComponentDeclaration("80DDB");

            decimal? tax80Ddd = list80D.Where(n => n.Sections == "80DDB").Sum(a => a.DeclarationAmount);

            if (_age > 80)
            {
                if (tax80Ddd > 80000)
                {
                    tax80Ddd = 80000;
                }
            }
            else if (_age > 60)
            {
                if (tax80Ddd > 60000)
                {
                    tax80Ddd = 60000;
                }
            }
            else
            {
                if (tax80Ddd > 40000)
                {
                    tax80Ddd = 40000;
                }
            }

            return taxFamily + tax80Ddd;
        }

        #endregion 80D Calculation

        #region 80TTA

        //for 80TTA the sum of declaration should not exceed the max limit 80TTA

        private decimal? Tax80Tta(int? financialYear)
        {
            decimal? eightyTta = 0;

            List<TdsEngineBo> list80Tta = ComponentDeclaration("80TTA");
            decimal? tax80TtaSum = list80Tta.Sum(a => a.DeclarationAmount);
            eightyTta = GetTdsLimitForCalculation("80TTA", _financialYear);

            if (financialYear >= 2018)
            {
                if (_age > 60)
                {
                    eightyTta = 0;
                }
            }

            if (tax80TtaSum > eightyTta)
            {
                tax80TtaSum = eightyTta;
            }

            return tax80TtaSum;
        }

        #endregion 80TTA

        #region Tax80TTB

        //for 80TTB the sum of declaration should not exceed the max limit 80TTB
        private decimal? Tax80Ttb()
        {
            decimal? tax80TtbSum = 0;

            if (_age > 60)
            {
                List<TdsEngineBo> list80Ttb = ComponentDeclaration("80TTB");
                tax80TtbSum = list80Ttb.Sum(a => a.DeclarationAmount);
                decimal? eightyTtb = GetTdsLimitForCalculation("80TTB", _financialYear);
                if (tax80TtbSum > eightyTtb)
                {
                    tax80TtbSum = eightyTtb;
                }
            }

            return tax80TtbSum;
        }

        #endregion Tax80TTB

        #region Tax20

        private decimal? Tax20()
        {
            List<TdsEngineBo> list20 = ComponentDeclaration("24");
            decimal? tax20Sum = list20.Sum(a => a.DeclarationAmount);
            if (tax20Sum > 200000)
            {
                tax20Sum = 200000;
            }

            return tax20Sum;
        }

        #endregion Tax20

        #region Tax80E

        private decimal? Tax80E()
        {
            List<TdsEngineBo> list80E = ComponentDeclaration("80E");
            decimal? tax80ESum = list80E.Where(n => n.Sections == "80E").Sum(a => a.DeclarationAmount);
            return tax80ESum;
        }

        #endregion Tax80E

        #region 80U calculation

        //for 80U the sum of declaration should not exceed the max limit 80U

        private decimal? Tax80U()
        {
            List<TdsEngineBo> list80U = ComponentDeclaration("80U");
            decimal? tax80USum = list80U.Sum(a => a.DeclarationAmount);
            decimal? eightyU = GetTdsLimitForCalculation("80U", _financialYear);
            if (tax80USum > eightyU)
            {
                tax80USum = eightyU;
            }

            return tax80USum;
        }

        #endregion 80U calculation

        #region TDSHouseTax ** To Be included later

        //
        //private decimal? HouseTax()
        //{
        //    decimal? Annualincome = 0;
        //    decimal? PropertyTax = 0;
        //    decimal? LoanInterest = 0;
        //    decimal? Deductions30 = 0;
        //    decimal? PropertyTaxself = 0;
        //    decimal? LoanInterestself = 0;

        //    // get all required component for house tax calculation
        //    List<TDSEngineBO> listHouseTax = new List<TDSEngineBO>();
        //    listHouseTax = TDSHouseTax();
        //    foreach (TDSEngineBO i in listHouseTax)
        //    {
        //        if (i.ModifiedByName.ToUpper().Contains("LETOUT"))
        //        {
        //            if (i.Component.ToUpper().Contains("INCOME"))
        //            {
        //                Annualincome = i.DeclarationAmount;
        //            }

        //            if (i.Component.ToUpper().Contains("TAX"))
        //            {
        //                PropertyTax = i.DeclarationAmount;
        //            }

        //            if (i.Component.ToUpper().Contains("INTEREST"))
        //            {
        //                LoanInterest = i.DeclarationAmount;
        //            }
        //        }
        //        else
        //        {
        //            if (i.Component.ToUpper().Contains("TAX"))
        //            {
        //                PropertyTaxself = i.DeclarationAmount;
        //            }

        //            if (i.Component.ToUpper().Contains("INTEREST"))
        //            {
        //                LoanInterestself = i.DeclarationAmount;
        //            }
        //        }
        //    }

        //    //Deductions of property tax from annual income
        //    decimal? houseTax = Annualincome - PropertyTax;
        //    //get the maintaince charge as per the %
        //    Deductions30 = houseTax * 30 / 100;
        //    //return the house tax and also avoid negative value
        //    return houseTax - Deductions30 - LoanInterest + PropertyTaxself + LoanInterestself;
        //}

        #endregion TDSHouseTax ** To Be included later

        #endregion FindTaxableIncome from Tax Sections

        #region Final payable tax amount

        private decimal? PayableTax(decimal? taxableIncome, bool isSeniorCitizen, int cessPercentage)
        {
            decimal? tax = taxableIncome;
            decimal? slab2Tax = 0;
            slab2Tax = CalculateTax(taxableIncome, isSeniorCitizen);
            _totalTax = slab2Tax;
            if (tax <= 500000)
            {
                if (slab2Tax <= 12500)
                {
                    _totalTax = 0;
                    slab2Tax = _totalTax;
                }
                else
                {
                    slab2Tax = _totalTax - 12500;
                }
            }

            if (tax > 10000000)
            {
                decimal? surcharge = 0;
                surcharge = _totalTax * 15 / 100;
                _surchargeVal = Convert.ToDecimal(surcharge);
                slab2Tax = _surchargeVal + _totalTax;
            }
            else if (tax > 5000000)
            {
                decimal? surcharge = 0;
                surcharge = _totalTax * 10 / 100;
                _surchargeVal = Convert.ToDecimal(surcharge);
                slab2Tax = _surchargeVal + _totalTax;
            }

            return slab2Tax + slab2Tax * cessPercentage / 100;
        }

        private static decimal CalculateTax(decimal? taxableAmount, bool isSeniorCitizen)
        {
            decimal slab1 = isSeniorCitizen ? 300000 : 250000;
            const decimal slab2 = 500000;
            const decimal slab3 = 1000000;
            decimal? tax = 0;

            if (taxableAmount > slab1)
            {
                var temp = taxableAmount - slab1;

                if (temp > slab1)
                {
                    temp = temp - slab1;
                    tax = 250000 * 5 / 100;

                    if (temp > slab2)
                    {
                        if (temp > 500000)
                        {
                            temp = temp - slab2;
                            tax += 500000 * 20 / 100;
                        }
                        else
                        {
                            tax += temp * 20 / 100;
                        }

                        if (temp > slab3)
                        {
                            tax += temp * 30 / 100;
                        }
                        else
                        {
                            temp = temp * 30 / 100;
                            tax += temp;
                        }
                    }
                    else
                    {
                        tax += temp * 20 / 100;
                    }
                }
                else
                {
                    temp = temp * 5 / 100;
                    tax = temp;
                }
            }
            else
            {
                tax = 0;
            }

            return Convert.ToDecimal(tax);
        }

        #endregion Final payable tax amount

        #region DB Connections

        public List<TdsEngineBo> ComponentDeclaration(string section)
        {
            List<TdsEngineBo> listTds = new List<TdsEngineBo>();
            using (SqlConnection con = new SqlConnection(_connection))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.TDS_SECTIONDETAILSENGINE, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, _employeeId);
                    cmd.Parameters.AddWithValue(DBParam.Input.Section, section);
                    cmd.Parameters.AddWithValue(DBParam.Input.FYID, _financialYear);
                    con.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var tdsEngineBo = new TdsEngineBo
                            {
                                DeclarationAmount = Convert.ToDecimal(reader[DBParam.Output.Declaration]),
                                Component = reader[DBParam.Output.Component].ToString(),
                                Sections = reader[DBParam.Output.Section].ToString(),
                                EEPF = Convert.ToDecimal(reader[DBParam.Output.EEPF].ToString()) //For Employee PF
                            };
                            listTds.Add(tdsEngineBo);
                        }
                    }
                }
            }

            return listTds;
        }

        public List<TdsEngineBo> ComponentDeclarationNoLimit()
        {
            List<TdsEngineBo> listTds = new List<TdsEngineBo>();
            using (SqlConnection con = new SqlConnection(_connection))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.TDS_SECTIONDETAILSENGINE_NOLIMIT, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, _employeeId);
                    cmd.Parameters.AddWithValue(DBParam.Input.FYID, _financialYear);

                    con.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var tdsEngineBo = new TdsEngineBo
                            {
                                DeclarationAmount = Convert.ToDecimal(reader[DBParam.Output.Declaration]),
                                Component = reader[DBParam.Output.Component].ToString(),
                                Sections = reader[DBParam.Output.Section].ToString()
                            };
                            listTds.Add(tdsEngineBo);
                        }
                    }
                }
            }

            return listTds;
        }

        public List<TdsEngineBo> TdsHouseTax()
        {
            List<TdsEngineBo> listTds = new List<TdsEngineBo>();
            using (SqlConnection con = new SqlConnection(_connection))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.TDS_HouseTaxDETAILSENGINE, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, _employeeId);
                    cmd.Parameters.AddWithValue(DBParam.Input.FYID, _financialYear);

                    con.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var tdsEngineBo = new TdsEngineBo
                            {
                                DeclarationAmount = Convert.ToDecimal(reader[DBParam.Output.Declaration]),
                                Component = reader[DBParam.Output.Component].ToString(),
                                ModifiedByName = reader[DBParam.Output.Type].ToString()
                            };
                            listTds.Add(tdsEngineBo);
                        }
                    }
                }
            }

            return listTds;
        }

        #endregion DB Connections
    }
}