﻿

function spinner(ID) {
    var className = "." + ID;
    $(className).button("loading");
    setTimeout(function () {
        $(className).button("reset");
    },
        500);
}

function getAjaxCall(u, d, i, c) {
    var tabindex = 1;
    $("input,select,button").each(function () {
        if (this.type != "hidden") {
            var $input = $(this);
            $input.removeAttr("tabindex", tabindex);
            tabindex++;
        }
    });
    $("#" + i).empty();
    $("#" + i).addClass("loaders");

    $.ajax({
        url: u,
        type: "GET",
        cache: false,
        data: d,
        success: function (data) {
            $("#" + i).removeClass("loaders");
            $("#" + i).empty().html(data);
            if (c != null) {
                c();
            }
            setTimeout(function () {
                $(".modal").find(".panel-body").css("min-height", 200);
            },
                1000);
            $(".modal-body").find(".panel-heading").css("cursor", "move");

        },
        error: function (data) {
            $("#ErrorPopup").modal("show");
            setTimeout(function () {

            },
                1000);
        }
    });
}


function getAjaxCallreturnValue(u, d) {

    $.ajax({
        url: u,
        type: "GET",
        cache: false,
        data: d,
        success: function (data) {
            $.notify({
                message: data
            },
                {
                    type: 'success',
                    timer: 4000
                });

        },
        error: function (data) {
            return data;
        }
    });
}

function postAjaxCall(u, d, i, c, busyControl, busystart) {
    $("#" + i).empty();
    $("#" + i).addClass("loaders");
    $.ajax({
        url: u,
        type: "POST",
        cache: false,
        data: d,
        success: function (data) {
            $("#" + i).html(data);
            $("#" + i).removeClass("loaders");
            if (c != null) {
                c();
            }
        },
        error: function (data) {
            $("#ErrorPopup").modal("show");

        }
    });
}

$(document).ready(function () {
    $("body").on("contextmenu",
        function (e) {
            return false;
        });
});

$(document).keydown(function (event) {
    if (event.keyCode === 123) {
        return false;
    } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
        return false; //Prevent from ctrl+shift+i
    }
});

//Notification
function sendWish() {
    var textareawish = $("#textareawish").val().replace("  ", " ");
    if ($("#textareawish").val() == "") {
        $("#errormessageforwish").text("Please enter the text");
        return false;
    }
    $.ajax({
        url: "/UserLogin/SendWish",
        type: "GET",
        cache: false,
        dataType: "json",
        data: {
            EmployeeId: $("#toid").val(),
            UserName: $("#currentmessagea").val(),
            UserMessage: textareawish,
            Operation: $("#wishtype").val(),
            Id: $("#messageID").val()
        },
        success: function (data) {
            $.notify({
                message: data
            },
                {
                    type: "success",
                    timer: 4000
                });
            $("#errormessageforwish").text("");
            $("#birthmessage").modal("hide");
        },
        error: function (data) {
        }
    });
}

$(function () {
    var notifications = $.connection.studentHub;
    notifications.client.updateStudentInformation = function (serverResponse) {
        debugger;
        getStudentInformation();
    };
    $.connection.hub.start().done(function () {
        debugger;

        getStudentInformation();
    }).fail(function (error) {
         alert(error);
    });
    notifications.client.BuildActivityInformation = function (serverResponse) {
        GetBuildActivity();
    };
    $.connection.hub.start().done(function () {
        GetBuildActivity();
    }).fail(function (error) {

        //alert(error);
    });
});

function GetBuildActivity() {
    $.ajax({
        url: "/UserLogin/BuildActivityCount",
        type: "GET",
        cache: false,
        dataType: "json",
        data: {},
        success: function (response) {
            if (response.Id !== 0) {
                $("#clickeventforbrodcastmessage").click();
                $("#broadcastmessagetodisplay")
                    .html(
                        "<img class='img-responsive pad' style='height: 60px; width: 60px;float:left;margin-right:15px;' src='/Images/Broadcast.png'/> <span>" +
                        response.UserMessage +
                        "</span>");
            }
        },
        failure: function (response) {
        }
    });
}

function getStudentInformation() {
    $("#bindMessage").empty();
    $.ajax({
        url: "/UserLogin/GetMessageCount",
        type: "GET",
        cache: false,
        dataType: "json",
        data: {},
        success: function (response) {
            $("#bindMessage").empty();
            $(".Notification").html(response.length);
            $.each(response,
                function (index, val) {
                    var cur = "'" + val.UserMessage + "'";
                    var old = "'" + val.Location + "'";
                    var fromimg = val.ModifiedByName;
                    var toimg = val.Operation;
                    var msgid = val.Id;
                    var userid = val.UserId;
                    var toid = val.LoginId;
                    var sender = "'" + val.UserName + "'";
                    var receiver = "'" + val.Name + "'";
                    if (userid == toid) {
                        $("#bindMessage").append(
                            '<li data-toggle="modal" data-target="#birthmessage" onclick="usermesagepoup(' +
                            val.EmployeeId +
                            "," +
                            encodeURIComponent(cur) +
                            "," +
                            encodeURIComponent(old) +
                            "," +
                            msgid +
                            "," +
                            encodeURIComponent(sender) +
                            "," +
                            encodeURIComponent(receiver) +
                            "," +
                            ')"><a href="#"><div class="pull-left"><img src="' +
                            fromimg +
                            '" style="height:60px;width:60px;padding:10px;" class="img-circle" alt="User Image"></div><h5> ' +
                            val.UserName +
                            "</h5><p>" +
                            val.UserMessage.substring(0, 20) +
                            "</p></a></li>");
                    } else {
                        $("#bindMessage").append(
                            '<li data-toggle="modal" data-target="#birthmessage" onclick="usermesagepoup(' +
                            val.EmployeeID +
                            "," +
                            encodeURIComponent(cur) +
                            "," +
                            encodeURIComponent(old) +
                            "," +
                            msgid +
                            "," +
                            encodeURIComponent(sender) +
                            "," +
                            encodeURIComponent(receiver) +
                            "," +
                            ')"><a href="#"><div class="pull-left"><img src="' +
                            toimg +
                            '" style="height:60px;width:60px;padding:10px;" class="img-circle" alt="User Image"></div><h5> ' +
                            val.Name +
                            "</h5><p>" +
                            val.Location.substring(0, 20) +
                            "</p></a></li>");
                    }
                });
        },
        failure: function (response) {
            //alert(response.d);
        }
    });
}
$(document).ready(function () {
    $("#bindEvent").empty();
    $.ajax({
        url: "/UserLogin/GetCurrentEvents",
        type: "GET",
        cache: false,
        dataType: "json",
        data: {
        },
        success: function (response) {

            $("#bindEvent").empty();
            $(".Notificationevent").html(response.length);
            $.each(response,
                function (index, val) {
                    var cur = "";
                    var old = "";
                    var str = "";
                    if (val.Operation != 0)
                        str = '<span class="fa fa-check pull-right" style="color:#3c8dbc"></span>';
                    if (val.UserMessage == "Birthday") {
                        cur = "' Happy Birthday !'";
                        $("#bindEvent")
                            .append('<li data-toggle="modal" data-target="#birthmessage" onclick="usermesagepoups(' +
                                val.Id +
                                "," +
                                cur +
                                ')"><a href="#"><i class="fa fa-birthday-cake text-aqua"></i>' +
                                val.UserName +
                                str +
                                " </a> </li>");
                    } else if (val.UserMessage == "Anniversary") {
                        cur = "' Happy Anniversary !'";
                        $("#bindEvent")
                            .append('<li data-toggle="modal" data-target="#birthmessage" onclick="usermesagepoups(' +
                                val.Id +
                                "," +
                                cur +
                                ')"><a href="#"><i class="fa fa-diamond text-aqua"></i>' +
                                val.UserName +
                                str +
                                " </a> </li>");
                    } else {
                        cur = "' Welcome Onboard !'";
                        $("#bindEvent")
                            .append('<li data-toggle="modal" data-target="#birthmessage" onclick="usermesagepoups(' +
                                val.Id +
                                "," +
                                cur +
                                ')"><a href="#"><i class="fa fa-user text-aqua"></i>' +
                                val.UserName +
                                str +
                                " </a> </li>");

                    }
                });
        },
        failure: function (response) {
            //alert(response.d);
        }
    });
});
function MarkMessageRead(msgid, isViewed) {
    $.ajax({
        url: "/UserLogin/MarkMessageRead",
        type: "POST",
        cache: false,
        dataType: "json",
        data: { messageId: msgid, isViewed: isViewed },
        success: function (response) {
        },
        failure: function (response) {
        }
    });
}

function usermesagepoup(id, c, o, msgid, sender, receiver) {
    $.ajax({
        url: "/UserLogin/NotificationPopup",
        type: "POST",
        cache: false,
        data: {},
        success: function (data) {
            $("#GetNotificationPopup").html(data);
            if (o != "") {
                $("#textareawish").hide();
                $("#savemessage").hide();
                $("#oldmessage").text(decodeURIComponent(sender + " - " + o));
                MarkMessageRead(msgid, 1);
                $("#setheader").text("Messages");
            } else {
                MarkMessageRead(msgid, 0);
                $("#setheader").text("Enter your reply");
            }
            $("#textareawish").val("");
            $("#currentmessage").text("");
            $("#currentmessagea").val("");
            $("#currentmessagea").val(decodeURIComponent(receiver + " - " + c));
            $("#currentmessage").text(decodeURIComponent(receiver + " - " + c));
            $("#toid").val(id);
            $("#messageID").val(msgid);
        },
        error: function (data) {
        }
    });

}

function usermesagepoups(id, c) {
    $.ajax({
        url: "/UserLogin/NotificationPopup",
        type: "POST",
        cache: false,
        data: {},
        success: function (data) {
            $("#GetNotificationPopup").html(data);
            $("#oldmessage").text("");
            if (c == " Happy Birthday !")
                $("#wishtype").val("Birthday");
            else if (c == " Happy Anniversary !")
                $("#wishtype").val("Anniversar");
            else
                $("#wishtype").val("others");

            $("#textareawish").show();
            $("#setheader").text("Enter your Wishes");
            $("#textareawish").val(c);
            $("#currentmessage").text("");
            $("#currentmessagea").val("");
            $("#toid").val(id);
            $("#messageID").val(0);
        },
        error: function (data) {
        }
    });
}

$("#bindbutton").click(function () {
    $.ajax({
        url: "/UserLogin/BroadCast",
        type: "Get",
        cache: false,
        data: { msg: $("#boardcastmsg").val() },
        success: function (data) {
            $("#bindtext").html(data);
            $("#oldmessage").text("");
            if (c == " Happy Birthday !")
                $("#wishtype").val("Birthday");
            else if (c == " Happy Anniversary !")
                $("#wishtype").val("Anniversar");
            else
                $("#wishtype").val("others");

            $("#textareawish").show();
            $("#setheader").text("Enter your Wishes");
            $("#textareawish").val(c);
            $("#currentmessage").text("");
            $("#currentmessagea").val("");
            $("#toid").val(id);
            $("#messageID").val(0);
        },
        error: function (data) {
        }
    });
});

function OpenSidebar() {
    $(".sidemenu").stop(true, true).toggle("slide",
        {
            direction: "right"
        });
}

function CloseSidebar() {
    $(".sidemenu").stop(true, true).toggle("slide", { direction: "right" });
}

$(".slide-submenu").on("click",
    function () {
        $(".sidemenu").stop(true, true).toggle("slide", { direction: "right" });
    });

$.widget.bridge("uibutton", $.ui.button);

//loader
function ajaxindicatorstart() {
    if (!cntrlIsPressed) {
        if ($("body").find("#resultLoading").attr("id") != "resultLoading") {
            $("body").append(
                '<div id="resultLoading" class="bg" style="display:none"><div><img  style="height:80%" src="../Content/Landing-Page/images/loading.gif"><div></div></div><div class="bg"></div></div>');
        }
        $("#resultLoading").css({
            'width': "100%",
            'height': "100%",
            'position': "fixed",
            'z-index': "10000000",
            'top': "0px",
            'left': "260px",
            'right': "0",
            'bottom': "0",
            'margin': "auto"
        });
        $("#resultLoading .bg").css({
            'color': "#ffffff",
            'opacity': "1",
            'width': "100%",
            'height': "100%",
            'position': "absolute",
            'top': "0"
        });
        $("#resultLoading>div:first").css({
            'width': "250px",
            'height': "75px",
            'text-align': "center",
            'position': "fixed",
            'top': "0",
            'left': "0",
            'right': "0",
            'bottom': "0",
            'margin': "auto",
            'font-size': "16px",
            'z-index': "10",
            'color': "#ffffff"

        });
        $("#resultLoading .bg").height("100%");
        $("#resultLoading").fadeIn();
        $("body").css("cursor", "wait");
    }
}

function ajaxindicatorstop() {
    $("#resultLoading .bg").height("100%");
    $("#resultLoading").fadeOut();
    $("body").css("cursor", "default");
}

function CheckForMobileDevice() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
        return true;
    else
        return false;
}

function JQueryDatatable(tableId, viewType, scroll) {
    if (!CheckForMobileDevice()) {
        setTimeout(function () {
            gridResize();
        },
            400);
        $('#' + tableId).DataTable({
            "paging": true,
            "lengthChange": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "aaSorting": [],
            //"pagingType": "full_numbers",
            responsive: true,
            pageResize: true
        });

    }
    else if (scroll === true) {
        $('#' + tableId).DataTable({
            "paging": true,
            "lengthChange": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "aaSorting": [],
            "pagingType": "simple",
            responsive: true,
            scrollX: true,
        });
    }
    else {
        if (viewType === 'FormView') {
            $("#" + tableId + " td").each(function () {
                var cellText = $(this).text();
                if ($.trim(cellText) === '') {
                    $(this).text('-');
                }
            });
            $('#' + tableId + ' tbody tr').removeAttr('class');
            $('#' + tableId + ' tbody tr td').removeAttr('class');
            $('#' + tableId + ' tbody tr td:before').addClass('text-left');
            $('#' + tableId + ' tbody tr td').addClass('text-left');
        }

        $('#' + tableId).DataTable({
            "paging": true,
            "lengthChange": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "aaSorting": [],
            "pagingType": "simple",
            responsive: true
        });


    }
}



//Grid Resize
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $(document).ready(function () {
        $(".hideonmobile").hide();
    });
    function gettablestyle(idofthetable) {
        $("#" + idofthetable).DataTable({
            'paging': ($(window).width() > 967) ? true : false,
            "info": ($(window).width() > 967) ? true : false,
            "bFilter": true,
            "bPaginate": true,
            "aaSorting": [],
            "lengthChange": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "searching": true,
            "autoWidth": false,
            responsive: true,
            pageResize: ($(window).width() > 967) ? false : true,
        });
    }
    $(document).ajaxStop(function () {
        $(document).ready(function () {
            setTimeout(function () {
                $(".hidemodified").hide();
            },
                1000
            );
        });
    });
    gridResize();
} else {
    function gettablestyle(idofthetable) {
        $("#" + idofthetable).DataTable({
            'paging': ($(window).width() > 967) ? true : false,
            "info": ($(window).width() > 967) ? true : false,
            "bFilter": true,
            "bPaginate": true,
            "aaSorting": [],
            "lengthChange": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "searching": true,
            "autoWidth": false,
            responsive: true,
            pageResize: ($(window).width() > 967) ? false : true,
        });
    }
}
function gridResize() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('#resize_wrapper').removeAttr("style");
        $('#resize_wrapper').removeAttr("id");
    } else {
        var fotter = $('footer').height();
        var windowHeight = $(window).height();
        var div = windowHeight - fotter - 165 - $('#accordion').height();
        if (div > 200) {
            $('#resize_wrapper').height(div);
            $('#resize_wrapper').css("height", div);
        }
    }
}

//Dropdown sorting
function sortDropdownValuesByText(ID) {
    var setValue = $("#" + ID).val();
    $("#" + ID).html($("#" + ID + " option").sort(function (x, y) {
        return $(x).text() < $(y).text() ? -1 : 1;
    }));
    $("#" + ID).get(0).selectedIndex = 0;
    $("#" + ID).val(setValue).change();
}

// Check Input Datas
function CheckSQLSusceptibleQuery(InputValues) {
    var myArray = ["INSERT", "UPDATE", "ALTER", "DROP", "DELETE "];
    var returnFlag = false;

    InputValues = InputValues.toUpperCase();
    for (i = 0; i < myArray.length; i++) {
        if (InputValues.indexOf(myArray[i]) != -1) {
            returnFlag = true;
        }
    }

    return returnFlag;
}

//var dt = new Date();
//var day = dt.getDate();
//var month = dt.getMonth() + 1;
//var year = dt.getFullYear();
//var hour = dt.getHours();
//var mins = dt.getMinutes();
//var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;

var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
var d = new Date();
var curr_date = d.getDate();
var curr_month = d.getMonth();
var curr_year = d.getFullYear();
var hour = d.getHours();
var min = d.getMinutes();
var sec = d.getSeconds();

var Common = {
    ExcelExport: function (tableID, companyname, reportName, formName, pagination, createdby) {

        try {
            $("#" + tableID).DataTable({
                "paging": pagination,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aaSorting": [],
                "pageLength": -1,
                "destroy": true,
            });

        } catch (e) {
        }

        var currentdate = (curr_date + "-" + m_names[curr_month] + "-" + curr_year);

        var a = document.createElement("a");

        var data_type = "data:application/vnd.ms-excel";
        var table_div = document.getElementById(tableID);
        var table_html = table_div.outerHTML.replace(/<table/g, '<table border="2px";')
            .replace(/<th /g, '<th style="background-color:#b0d895;"');

        var newRow = '<table border="2px" style:"font-family:Cambria;"><tr><td><b> Company </b></td><td colspan=3> ' +
            companyname +
            " </td></tr><tr><td><b> Report Name</b></td><td colspan=3> " +
            reportName.replace(/_/g, " ") +
            " </td></tr><tr><td><b> Created By</b></td><td colspan=3> " +
            createdby +
            ' </td></tr><tr><td><b> Created On</b></td><td align="left" colspan=3> ' +
            currentdate +
            " </td></tr><tr><td><b> Report Source</b></td><td colspan=3> OfficeBAU </td></tr><tr></tr></table>";
        newRow = newRow.replace(/ /g, "%20");
        table_html = table_html.replace(/ /g, "%20");
        a.href = data_type + ", " + newRow + table_html;
        //document.body.appendChild(a);
        try {
            $("#" + tableID).DataTable({
                "paging": pagination,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aaSorting": [],
                "destroy": true,
            });
        } catch (e) {
        }

        var dformat = [curr_date, m_names[curr_month], curr_year].join("-") + "_" + [hour, min, sec].join(":");
        var date = (curr_date + "-" + m_names[curr_month] + "-" + curr_year + " " + hour + ":" + min + ":" + sec);
        a.download = companyname.replace(/ /g, "") + "_" + reportName + "_" + dformat + ".xls";
        a.click();
        //tableID.preventDefault();
    },
    searchDropdownByYear: function (fnmth) {
        var currentMonth = curr_month;
        var currentYear = curr_year;

        if (fnmth > (currentMonth + 1))
            currentYear = currentYear - 1;

        var date = new Date(currentYear + " " + fnmth + " " + curr_date);
        var firstdate, lastdate;
        var d1, d2, m1, m2, y1, y2;

        $("#FirstDate").prop("disabled", true);
        $("#SecondDate").prop("disabled", true);

        switch ($("#searchdropdown option:selected").val()) {
            case "Select":
                $(".Getdate").val("");
                break;

            case "All":
                $("#FirstDate").prop("disabled", true);
                $("#SecondDate").prop("disabled", true);
                $("#FirstDate").val("");
                $("#SecondDate").val("");
                return;
                break;

            case "Custom":

                $("#FirstDate").val("");
                $("#SecondDate").val("");

                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);
                return;
                break;

            case "Today":
                $(".firstdate").show();
                $(".seconddate").show();

                d1 = date.getDate();
                m1 = date.getMonth() + 1;
                y1 = date.getFullYear();

                d2 = date.getDate();
                m2 = date.getMonth() + 1;
                y2 = date.getFullYear();

                break;

            case "Last365":
                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);

                var firstDay = new Date(curr_year - 1, curr_month, date.getDate());
                var lastDay = new Date(curr_year, curr_month, date.getDate() - 1);

                d1 = (firstDay.getDate());
                m1 = (firstDay.getMonth()) + 1;
                y1 = firstDay.getFullYear();

                d2 = (lastDay.getDate());
                m2 = (lastDay.getMonth()) + 1;
                y2 = lastDay.getFullYear();

                break;

            case "Week":
                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);
                var firstDay = new Date(date.getFullYear(), date.getMonth(), (date.getDate() - (date.getDay() - 1)));
                var lastDay = new Date(date.getFullYear(), date.getMonth(), (date.getDate() + 6 - (date.getDay())));

                d1 = (firstDay.getDate());
                m1 = (firstDay.getMonth() + 1);
                y1 = firstDay.getFullYear();

                d2 = (lastDay.getDate());
                m2 = (lastDay.getMonth() + 1);
                y2 = lastDay.getFullYear();

                break;

            case "Weekdate":
                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);

                var firstDay = new Date(date.getFullYear(), date.getMonth(), (date.getDate() - (date.getDay() - 1)));

                d1 = (firstDay.getDate());
                m1 = (firstDay.getMonth() + 1);
                y1 = firstDay.getFullYear();

                d2 = (date.getDate());
                m2 = (date.getMonth() + 1);
                y2 = lastDay.getFullYear();

                break;

            case "Month":
                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);
                var firstDay = new Date(curr_year, curr_month, 1);
                var lastDay = new Date(curr_year, curr_month + 1, 0);

                d1 = (firstDay.getDate());
                m1 = (firstDay.getMonth() + 1);
                y1 = firstDay.getFullYear();

                d2 = (lastDay.getDate());
                m2 = (lastDay.getMonth() + 1);
                y2 = lastDay.getFullYear();

                break;

            case "Monthdate":
                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);

                var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

                d1 = (firstDay.getDate());
                m1 = (firstDay.getMonth() + 1);
                y1 = firstDay.getFullYear();

                d2 = (date.getDate());
                m2 = (date.getMonth() + 1);
                y2 = lastDay.getFullYear();

                break;

            case "Quarter":
                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);

                //var firstDay = new Date(date.getFullYear(), fnmth - 1, 1);
                //var lastDay = new Date(date.getFullYear(), fnmth + 3, 0);
                var quarter = Math.floor((d.getMonth() / 3));

                var firstDay = new Date(curr_year, quarter * 3, 1);
                var lastDay = new Date(curr_year, firstDay.getMonth() + 3, 0);

                d1 = (firstDay.getDate());
                m1 = (firstDay.getMonth() + 1);
                y1 = firstDay.getFullYear();

                d2 = (lastDay.getDate());
                m2 = (lastDay.getMonth() + 1);
                y2 = lastDay.getFullYear();

                break;

            case "Quarterdate":
                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);

                var firstDay = new Date(date.getFullYear(), date.getMonth() * 3 - 3, 1);
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

                d1 = (firstDay.getDate());
                m1 = (firstDay.getMonth() + 1);
                y1 = firstDay.getFullYear();

                d2 = (date.getDate());
                m2 = (date.getMonth() + 1);
                y2 = lastDay.getFullYear();

                break;

            case "Year":
                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);

                var firstDay = new Date(date.getFullYear(), fnmth, 1);
                var lastDay = new Date(date.getFullYear() + 1, fnmth - 1, 0);

                d1 = (firstDay.getDate());
                m1 = (firstDay.getMonth());
                y1 = firstDay.getFullYear();

                // Set Year & Month
                if (fnmth == 12) {
                    m1 = 12;
                    y1 = y1 - 1;
                }

                d2 = (lastDay.getDate());
                m2 = (lastDay.getMonth() + 1);
                y2 = lastDay.getFullYear();

                break;

            case "Yeardate":
                $("#FirstDate").prop("disabled", false);
                $("#SecondDate").prop("disabled", false);

                var firstDay = new Date(date.getFullYear(), 1, 1);
                var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

                d1 = (firstDay.getDate());
                m1 = (firstDay.getMonth());
                y1 = firstDay.getFullYear();

                d2 = (date.getDate());
                m2 = (date.getMonth() + 1);
                y2 = lastDay.getFullYear();

                break;

        }

        // Start Year date a& Month
        if (d1 < 10) {
            d1 = "0" + d1;
        }
        if (m1 < 10) {
            m1 = "0" + m1;
        }

        // end Year date a& Month
        if (d2 < 10) {
            d2 = "0" + d2;
        }
        if (m2 < 10) {
            m2 = "0" + m2;
        }

        firstdate = d1 + "-" + m1 + "-" + y1;
        lastdate = d2 + "-" + m2 + "-" + y2;
        $("#FirstDate").datepicker("setDate", firstdate);
        $("#SecondDate").datepicker("setDate", lastdate);

    },
    searchByFinancialYear: function (fnmth) {
        var m_names = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        var curr_date = d.getDate();
        var currentMonth = curr_month;
        var currentYear = curr_year;
        //if current month is lesser than four
        if (curr_month < 3) {
            currentYear = currentYear - 1;
        }

        //if (fnmth > (currentMonth + 1))
        //    currentYear = currentYear - 1;   

        var date = new Date(currentYear + " " + (fnmth) + " " + curr_date);
        var firstdate, lastdate;
        var d1, d2, m1, m2, y1, y2;

        var firstDay = new Date(date.getFullYear(), fnmth, 1);
        var lastDay = new Date(date.getFullYear() + 1, fnmth - 1, 0);


        d1 = (firstDay.getDate());
        m1 = (firstDay.getMonth());
        y1 = firstDay.getFullYear();

        // Set Year & Month
        if (fnmth == 12) {
            m1 = 12;
            y1 = y1 - 1;
        }

        d2 = (lastDay.getDate());
        m2 = (lastDay.getMonth() + 1);
        y2 = lastDay.getFullYear();

        // Start Year date a& Month
        if (d1 < 10) {
            d1 = "0" + d1;
        }
        if (m1 < 10) {
            m1 = "0" + m1;
        }

        // end Year date a& Month
        if (d2 < 10) {
            d2 = "0" + d2;
        }
        if (m2 < 10) {
            m2 = "0" + m2;
        }
        debugger;
        firstdate = d1 + "-" + m1 + "-" + y1;
        lastdate = d2 + "-" + m2 + "-" + y2;
        $("#FirstDate").datepicker("setDate", firstdate);
        $("#SecondDate").datepicker("setDate", lastdate);
    },
    PrintPdf: function (DivID) {
        var mode = "iframe";
        var close = mode == "popup";
        var options = {
            mode: mode,
            popClose: close
        };
        $("#" + DivID).printArea(options);
    },

    fnAllowNumericOnly: function (event) {
        if ((event.charCode < 48 || event.charCode > 57) && event.keyCode != 8 && event.keyCode != 9) {
            event.charCode = 0;
            return false;
        }
    },

    displayerror: function (message) {
        $.notify({
            message: message
        },
            {
                type: "warning",
                timer: 4000
            });
    },

    fnCheckSpecialCharacter: function (event) {
        if (!((event.charCode > 43 && event.charCode < 47) ||
            (event.charCode > 64 && event.charCode <= 90) ||
            (event.charCode >= 95 && event.charCode < 123) ||
            event.charCode == 32 ||
            event.keyCode == 8 ||
            event.keyCode == 9)) {
            return false;
        }
    },

    AllowPhoneNoCharacters: function (event) {
        if ((event.charCode != 0 && event.charCode < 48 || event.charCode > 57) &&
            event.keyCode != 8 &&
            event.keyCode != 9 &&
            event.charCode != 40 &&
            event.charCode != 41 &&
            event.charCode != 43 &&
            event.charCode != 32) {
            event.charCode = 0;
            return false;
        }
    },
    WhiteSpaceDisable: function (event) {
        if (event.charCode === 32)
            return false;
    },
    AllowCharactersNOsWithSpace: function (event) {
        if ((event.charCode != 0 && event.charCode < 48 || event.charCode > 57) &&
            event.keyCode != 8 &&
            event.keyCode != 9 &&
            event.charCode != 32) {
            event.charCode = 0;
            return false;
        }
    },

    fnAllowCharactersNOsAndAlphabets: function (event) {
        if (!((event.charCode > 47 && event.charCode < 58) ||
            (event.charCode > 64 && event.charCode <= 90) ||
            (event.charCode >= 95 && event.charCode < 123) ||
            event.charCode == 32 ||
            event.keyCode == 8 ||
            event.keyCode == 9)) {
            return false;
        }
    },

    AllowNoWithSpecialCharacters: function (event) {
        if ((event.charCode != 0 && event.charCode < 47 || event.charCode > 57) &&
            event.keyCode != 8 &&
            event.keyCode != 9 &&
            event.charCode != 40 &&
            event.charCode != 41 &&
            event.charCode != 43 &&
            event.charCode != 44 &&
            event.charCode != 38 &&
            event.charCode != 32 &&
            event.charCode != 92) {
            event.charCode = 0;
            return false;
        }
    },
    fnAllowDecimalNos: function (event, id) {
        var value = $("#" + id).val();
        if ((event.charCode < 48 || event.charCode > 57) &&
            event.keyCode != 8 &&
            event.keyCode != 9 &&
            event.charCode != 46 &&
            event.charCode != 0) {
            event.charCode = 0;
            return false;
        }
        if (value.indexOf(".") != -1 && event.charCode == 46) {
            event.charCode = 0;
            return false;
        }
    },
    fnAlphaOnly: function (event) {
        if (event.charCode > 31
            && (event.charCode < 65 || event.charCode > 90)
            && (event.charCode < 97 || event.charCode > 122)) {
            return false;
        }
    },
    fnNumberOnly: function (event) {
        if ((event.charCode != 0 && event.charCode < 48 || event.charCode > 57) &&
            event.keyCode != 8 &&
            event.keyCode != 9) {
            event.charCode = 0;
            return false;
        }
    },
    getLocalDateTimeFromUTC: function () {
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
        ];

        var UTCDate = $("#hdnModifiedOn").val() + " UTC";
        UTCDate = UTCDate.replace(/-/g, "/");
        var LocalDate = new Date(UTCDate);
        var day = (LocalDate.getDate() < 10 ? "0" + LocalDate.getDate() : LocalDate.getDate());
        var monthIndex = LocalDate.getMonth();
        var year = LocalDate.getFullYear().toString().substr(2);
        var hour = LocalDate.getHours();
        var minutes = (LocalDate.getMinutes() < 10 ? "0" + LocalDate.getMinutes() : LocalDate.getMinutes());
        //$("#spnModifiedOn").text(day + '-' + monthNames[monthIndex] + '-' + year + ' ' + hour + ":" + minutes);
    },
};