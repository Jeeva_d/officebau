﻿//using Microsoft.Office.Interop.Excel;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Drawing;
//using System.IO;
//using System.Linq;
//using DataTable = System.Data.DataTable;

//namespace ExcelSample.Models
//{
//    public class ExcelUtlity
//    {
//        //public System.Data.DataTable ConvertToDataTable<T>(IList<T> data)
//        //{
//        //    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
//        //    System.Data.DataTable table = new System.Data.DataTable();
//        //    foreach (PropertyDescriptor prop in properties)
//        //    {
//        //        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
//        //    }
//        //    foreach (T item in data)
//        //    {
//        //        System.Data.DataRow row = table.NewRow();
//        //        foreach (PropertyDescriptor prop in properties)
//        //        {
//        //            if (prop.GetValue(item).ToString().Contains("Entity"))
//        //                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
//        //            else
//        //                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
//        //        }
//        //        table.Rows.Add(row);
//        //    }
//        //    return table;

//        //}
//        public DataTable ConvertToDataTable(IDataReader dr)
//        {
//            //PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
//            DataTable table = new DataTable();
//            table.Load(dr);
//            //foreach (PropertyDescriptor prop in properties)
//            //{
//            //    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
//            //}
//            //foreach (T item in data)
//            //{
//            //    System.Data.DataRow row = table.NewRow();
//            //    foreach (PropertyDescriptor prop in properties)
//            //    {
//            //        if (prop.GetValue(item).ToString().Contains("Entity"))
//            //            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
//            //        else
//            //            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
//            //    }
//            //    table.Rows.Add(row);
//            //}
//            return table;
//        }

//        public bool WriteObjectToExcel(IDataReader data, string worksheetName, string saveAsLocation, string fileName,
//                                       string ReporType, string userID, string exportType)
//        {
//            DataTable dt = new DataTable();
//            bool status = WriteDataTableToExcel(ConvertToDataTable(data), worksheetName, saveAsLocation, fileName, ReporType, userID, exportType);
//            return status;
//        }

//        /// <summary>
//        ///     FUNCTION FOR EXPORT TO EXCEL
//        /// </summary>
//        /// <param name="dataTable"></param>
//        /// <param name="worksheetName"></param>
//        /// <param name="saveAsLocation"></param>
//        /// <returns></returns>
//        public bool WriteDataTableToExcel(DataTable dataTable, string worksheetName,
//                                          string saveAsLocation, string fileName, string ReporType, string userID, string exportType)
//        {
//            try
//            {
//                if (exportType == "ReturnSalesReport")
//                {
//                    dataTable.Columns.RemoveAt(3);
//                    dataTable.Columns.RemoveAt(3);
//                    dataTable.Columns.RemoveAt(3);
//                    dataTable.Columns.RemoveAt(3);
//                    dataTable.Columns.RemoveAt(3);
//                    dataTable.Columns.RemoveAt(3);
//                    dataTable.Columns.RemoveAt(3);
//                    dataTable.Columns.RemoveAt(3);
//                }
//                else if (exportType == "payroll")
//                {
//                    dataTable.Columns.RemoveAt(0);
//                    dataTable.Columns.RemoveAt(1);
//                    dataTable.Columns.RemoveAt(1);
//                    dataTable.Columns.RemoveAt(2);
//                    dataTable.Columns.RemoveAt(23);
//                    dataTable.Columns.RemoveAt(23);
//                    dataTable.Columns.RemoveAt(23);
//                    dataTable.Columns.RemoveAt(23);
//                }

//                List<string> lines = new List<string>();
//                string[] columnNames = dataTable.Columns.Cast<DataColumn>().Select(column => column.ColumnName).ToArray();

//                string header = string.Join(",", columnNames);
//                lines.Add(header);
//                EnumerableRowCollection<string> valueLines = dataTable.AsEnumerable().Select(row => string.Join(",", row.ItemArray));
//                lines.AddRange(valueLines);

//                if (!Directory.Exists(saveAsLocation))
//                {
//                    Directory.CreateDirectory(saveAsLocation);
//                }

//                fileName = userID + "_" + fileName;
//                saveAsLocation = saveAsLocation + fileName;

//                if (File.Exists(saveAsLocation))
//                {
//                    File.Delete(saveAsLocation);
//                }

//                File.WriteAllLines(saveAsLocation, lines);
//                return true;
//            }
//            catch (Exception)
//            {
//                return false;
//            }
//        }

//        /// <summary>
//        ///     FUNCTION FOR FORMATTING EXCEL CELLS
//        /// </summary>
//        /// <param name="range"></param>
//        /// < param name="HTMLcolorCode"></param>
//        /// <param name="fontColor"></param>
//        /// < param name="IsFontbool"></param>
//        public void FormattingExcelCells(Range range, string HTMLcolorCode, Color fontColor, bool IsFontbool)
//        {
//            range.Interior.Color = ColorTranslator.FromHtml(HTMLcolorCode);
//            range.Font.Color = ColorTranslator.ToOle(fontColor);
//            if (IsFontbool)
//            {
//                range.Font.Bold = IsFontbool;
//            }
//        }
//    }
//}
