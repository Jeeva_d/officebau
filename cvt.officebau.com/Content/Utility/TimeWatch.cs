﻿using cvt.officebau.com.Services;
using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace cvt.officebau.com.Utilities
{
    public class TimeWatch
    {
        public string GetPunchDetailsList(HttpPostedFileBase file, string fileLocation, string deviceType)
        {
            DataSet ds = new DataSet();
            string message = string.Empty;

            if (file.ContentLength > 0)
            {
                string fileExtension = Path.GetExtension(file.FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    string fileName = fileLocation + "\\" + file.FileName;

                    if (!Directory.Exists(fileLocation))
                    {
                        Directory.CreateDirectory(fileLocation);
                    }

                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }

                    file.SaveAs(fileLocation + "\\" + file.FileName);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();
                    }

                    file.SaveAs(fileName);

                    string excelConnectionString = string.Empty;
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1\"";
                    }
                    else if (fileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";
                    }

                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();
                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    string[] excelSheets = new string[dt.Rows.Count];
                    int t = 0;
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }

                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }

                    DataTable dt1 = ds.Tables[0];
                    DataTable filteredRows = dt1.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is DBNull)).CopyToDataTable();
                    if (deviceType.ToUpper() == "TIMEWATCH")
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            filteredRows.Rows.RemoveAt(0);
                        }

                        filteredRows.Columns.RemoveAt(0);
                        filteredRows.Columns.RemoveAt(1);
                        filteredRows.Columns.RemoveAt(1);
                        filteredRows.Columns.RemoveAt(3);
                        filteredRows.Columns.RemoveAt(4);
                        filteredRows.Columns.RemoveAt(5);
                        filteredRows.Columns.RemoveAt(6);

                        for (int i = 0; i < filteredRows.Rows.Count; i++)
                        {
                            for (int j = 0; j < 1; j++)
                            {
                                if (string.IsNullOrWhiteSpace(filteredRows.Rows[i][j].ToString()))
                                {
                                    filteredRows.Rows[i][j] = filteredRows.Rows[i - 1][j];
                                }
                            }
                        }
                    }
                    else if (deviceType.ToUpper() == "HIFOCUS")
                    {
                        filteredRows.Rows.RemoveAt(0);
                        filteredRows.Columns[2].SetOrdinal(0);
                        filteredRows.Columns.RemoveAt(3);
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("Punch2", typeof(string))
                        {
                            DefaultValue = ","
                        };
                        filteredRows.Columns.Add(newColumn);

                        System.Data.DataColumn newColumn2 = new System.Data.DataColumn("Punch3", typeof(string))
                        {
                            DefaultValue = ","
                        };
                        filteredRows.Columns.Add(newColumn2);

                        System.Data.DataColumn newColumn3 = new System.Data.DataColumn("Punch4", typeof(string))
                        {
                            DefaultValue = ","
                        };
                        filteredRows.Columns.Add(newColumn3);

                        filteredRows.Rows.Cast<DataRow>().ToList().ForEach(r => r.SetField(filteredRows.Columns[2].ColumnName, r[filteredRows.Columns[2].ColumnName].ToString().Replace(".", ":")));

                    }
                    excelConnection.Close();

                    string conn = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                    SqlConnection con = new SqlConnection(conn);

                    using (SqlConnection connection = new SqlConnection(conn))
                    {
                        using (SqlCommand sqlCommand = new SqlCommand(Constants.TIMEWATCHUPLOAD, connection))
                        {
                            sqlCommand.CommandType = CommandType.StoredProcedure;
                            sqlCommand.Parameters.AddWithValue(DBParam.Input.TIMEWATCH, filteredRows);
                            sqlCommand.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                            sqlCommand.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
                            connection.Open();
                            using (SqlDataReader reader = sqlCommand.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    message = reader["Output"].ToString();
                                }
                            }
                        }
                    }
                }
            }

            return message;
        }
    }
}
