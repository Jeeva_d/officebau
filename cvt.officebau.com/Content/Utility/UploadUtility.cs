﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using cvt.officebau.com.Repository;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;

namespace cvt.officebau.com.Utilities
{
    public class UploadUtility
    {
        #region DownloadFile

        public static FileUpload DownloadFile(string fileId)
        {
            var consString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            var con = new SqlConnection(consString);

            var fileUpload = new FileUpload();
            var cmd = new SqlCommand(Constants.GETUPLOADEDFILE)
            {
                CommandType = CommandType.StoredProcedure,
                Connection = con
            };
            cmd.Parameters.AddWithValue(DBParam.Input.FileID, fileId);
            con.Open();
            using (IDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    fileUpload.FileName = reader[DBParam.Output.FileName].ToString();
                    fileUpload.FileType = reader[DBParam.Output.FileType].ToString();
                    fileUpload.OriginalFileName = reader[DBParam.Output.OriginalFileName].ToString();
                }
            }

            con.Close();

            return fileUpload;
        }

        #endregion DownloadFile

        public static void CheckForFolder(string folderPath)
        {
            if (!Directory.Exists(folderPath))
            {
                try
                {
                    Directory.CreateDirectory(folderPath);
                }
                catch
                {
                    // ignored
                }
            }
        }

        #region Upload 

        public static List<string> UploadFile(HttpPostedFileBase file, string folder, string storageType = null)
        {
            var domainId = Utility.DomainId();
            var companyName = Utility.GetSession("CompanyName");
            var employeeId = Utility.UserId();
            var fileName = Utility.GetUploadFileName(file.FileName);

            var list = new List<string>();
            string uploadFolder;

            if (!string.IsNullOrWhiteSpace(storageType))
            {
                uploadFolder = AppDomain.CurrentDomain.BaseDirectory + "Images\\" + companyName + "\\" + folder;
            }
            else
            {
                uploadFolder = ConfigurationManager.AppSettings["UploadPath"] + companyName + "\\" + folder;
            }

            CheckForFolder(uploadFolder);
            file.SaveAs(uploadFolder + "\\" + fileName);
            return UpdateFileUpload(file, folder, fileName, employeeId, domainId, list, uploadFolder);
        }

        private static List<string> UpdateFileUpload(HttpPostedFileBase file, string folder, string fileName, int userID, int domainID, List<string> list, string uploadFolder)
        {
            list.Add(file.FileName);
            using (var context = new FSMEntities())
            {
                var fileUpload = new tbl_FileUpload
                {
                    Id = Guid.NewGuid(),
                    Path = uploadFolder,
                    FileName = fileName,
                    OriginalFileName = file.FileName,
                    FileType = file.ContentType,
                    DomainID = domainID,
                    CreatedBy  = userID,
                    CreatedOn = DateTime.Now,
                    ModifiedBy = userID,
                    ModifiedOn = DateTime.Now
                };
                context.tbl_FileUpload.Add(fileUpload);
                context.SaveChanges();
                list.Add(context.tbl_FileUpload.OrderByDescending(b => b.CreatedOn).Select(b => b.Id).FirstOrDefault().ToString());
            }

            list.Add(folder);
            list.Add(fileName);
            return list;
        }

        #endregion Upload 
    }
}
