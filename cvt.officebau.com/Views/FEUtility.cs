﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cvt.officebau.com.ViewModels
{
    public static class FEUtility
    {
        public static string Masker(string value, int precision)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            var value1 = value.Substring(value.Length - precision, precision);
            var requiredMask = new String('x', value.Length - value1.Length);
            var maskedString = string.Concat(requiredMask, value1);
            var mask = string.Format(maskedString, ".{4}", "$0 ");
            return (mask);
        }
    }
}