﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace cvt.officebau.com
{
    public class MvcApplication : System.Web.HttpApplication
    {
        readonly string connString = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            SqlDependency.Start(connString);

        }

        void Application_Error(object sender, EventArgs e)
        {
            Exception objErr = Server.GetLastError().GetBaseException();
            string err = "Error Caught in OfficeBau Application error event\n" +
                    "Error in: " + Request.Url.ToString() +
                    "\nError Message:" + objErr.Message.ToString() +
                    "\nStack Trace:" + objErr.StackTrace.ToString();

            if (objErr.Message == "File does not exist.")
                return;
            Server.ClearError();
            if (!Response.IsRequestBeingRedirected)
                Response.Redirect("~/UserLogin/ErrorView?errorMsg=" + objErr.Message);

        }
    }
}
