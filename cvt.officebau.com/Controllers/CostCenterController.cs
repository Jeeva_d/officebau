﻿using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class CostCenterController : Controller
    {
        #region Variables

        private readonly CostCenterRepository _costCenterRepository = new CostCenterRepository();

        #endregion Variables

        public ActionResult CostCenter()
        {
            CostCenterBo costCenterBo = new CostCenterBo();
            return PartialView(_costCenterRepository.SearchCostCenterList(costCenterBo));
        }

        #region Create CostCenter

        public ActionResult CreateCostCenter(int id)
        {
            CostCenterBo costCenterBo = _costCenterRepository.GetCostCenter(id);
            return PartialView(costCenterBo);
        }

        [HttpPost]
        public ActionResult CreateCostCenter(CostCenterBo costCenterBo)
        {
            return Json(_costCenterRepository.ManageCostCenter(costCenterBo));
        }

        #endregion Create CostCenter
    }
}