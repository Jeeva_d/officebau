﻿/*Code Review Done Neeed Proper Commands for all the methods and removed some unwanted local variables*/

using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class IncomeController : Controller
    {
        #region Customer Autocomplete

        public ActionResult AutoCompleteCustomer(string customerName)
        {
            return Json(_commonRepository.SearchAutoCompleteParameter("Customer", "Name", customerName), JsonRequestBehavior.AllowGet);
        }

        #endregion Customer Autocomplete

        public ActionResult InvoiceItem()
        {
            return PartialView();
        }

        public ActionResult SalesOrderDetails(int customerId)
        {
            return PartialView(_incomeRepository.SearchCustomerSoDetails(customerId));
        }

        #region Variables

        private readonly IncomeRepository _incomeRepository = new IncomeRepository();
        private readonly CommonRepository _commonRepository = new CommonRepository();

        #endregion Variables

        #region Search IncomeDetails

        public ActionResult SearchIncomeDetails(string menuCode, int? id, int? customerId, string screenType)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "SALES");
            Utility.SetSession("SubMenuRetention", "INVOICE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Income");

            SearchParamsBo searchParamsBo = new SearchParamsBo();
            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();

            searchParamsBo.IsCostCenter = applicationConfigurationRepository.SearchCostCenter();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            searchParamsBo.CostCenterList = masterDataRepository.SearchMasterDataDropDown("CostCenter");
            searchParamsBo.Id = id ?? 0;
            searchParamsBo.CustomerId = customerId ?? 0;
            searchParamsBo.ScreenType = screenType;
            //commonRepository.GetSearchDataHistory(searchParamsBO, "pageload", FormName, Convert.ToInt32(Session["StartMonth"]));
            return View(searchParamsBo);
        }

        public ActionResult IncomeDetails(SearchParamsBo searchParamsBo)
        {
            //if (searchParamsBO.Event == "search")
            //    commonRepository.GetSearchDataHistory(searchParamsBO, "search", FormName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(_incomeRepository.SearchIncome(searchParamsBo));
        }

        #endregion Search IncomeDetails

        #region Invoice

        public ActionResult Invoice(int id, string localScreenType, string menuCode, string soIds, string customerName, int? customerId)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "VOUCHER");
            Utility.SetSession("SubMenuRetention", "INCOME");

            ItemDetailsBO itemDetailsBo = new ItemDetailsBO();

            IncomeBo incomeBo = _incomeRepository.GetIncome(id);
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            incomeBo.DiscountList = _commonRepository.SearchDependantDropDown(0, "CodeMaster", "Discount", "Type");
            ApplicationConfigurationRepository applicationConfigrepository = new ApplicationConfigurationRepository();
            incomeBo.IsCostCenter = applicationConfigrepository.SearchCostCenter();
            incomeBo.CostCenterList = masterDataRepository.SearchMasterDataDropDown("CostCenter");

            if (id == 0)
            {
                incomeBo.ItemDetail = new List<ItemDetailsBO> { itemDetailsBo };

                itemDetailsBo.ProductList = masterDataRepository.GetPreRequestProduct("Sales");
            }

            if (localScreenType != null)
            {
                incomeBo.ScreenType = localScreenType;
            }

            SalesOrderRepository salesOrderRepository = new SalesOrderRepository();
            if (!string.IsNullOrWhiteSpace(soIds))
            {
                incomeBo.CustomerName = customerName;
                incomeBo.CustomerId = Convert.ToInt32(customerId);
                incomeBo.ItemDetail.AddRange(salesOrderRepository.SearchSoinvoiceDetailList(soIds));
                incomeBo.ItemDetail = incomeBo.ItemDetail.FindAll(a => a.ProductID != 0);
            }

            incomeBo.MenuCode = menuCode;
            incomeBo.Emailhtml = PopulateMailBody(incomeBo);
            return PartialView(incomeBo);
        }

        [HttpPost]
        public ActionResult Invoice(IncomeBo incomeBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (incomeBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), incomeBo.MenuCode, incomeBo.Id == 0 ? "Save" : "Edit");
            }

            if (incomeBo != null && incomeBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), incomeBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            if (incomeBo.DiscountPercentage != 0)
            {
                incomeBo.DiscountPercentage = incomeBo.DiscountPercentage / 100;
            }

            incomeBo.ScreenType = "Invoice";
            return Json(_incomeRepository.ManageInvoice(incomeBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGstAndSgstValue(int id, string type)
        {
            return Json(_incomeRepository.GetGstHsn(id, type), JsonRequestBehavior.AllowGet);
        }

        public string GetInvoicePdfByteArray(IncomeBo inv)
        {
            string uploadFolder = ConfigurationManager.AppSettings["UploadPath"];

            string html = RenderRazorViewToString("InvoicePrint", inv);
            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                    pdf_orientation, true);

            int webPageWidth = 1024;

            int webPageHeight = 0;

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(html, "");
            //doc.Security.UserPassword = "Test1";
            //doc.Security.OwnerPassword = "Test2";
            //doc.Security.CanAssembleDocument = false;
            //doc.Security.CanCopyContent = true;
            //doc.Security.CanEditAnnotations = true;
            //doc.Security.CanEditContent = true;
            //doc.Security.CanFillFormFields = true;
            //doc.Security.CanPrint = true;
            // save pdf document
            doc.Save(uploadFolder + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("INVOICE") + inv.CompanyName + "_" + inv.InvoiceNo + ".pdf");
            // close pdf document
            doc.Close();

            return inv.CompanyName + "_" + inv.InvoiceNo + ".pdf";
        }

        public ActionResult SendInvoiceEmail(int id, string toId, string ccId, string bccId, string subject)
        {
            IncomeBo invoice = _incomeRepository.GetIncome(id);

            string msg = BOConstants.Email_Not_Configured;
            string filePath = ConfigurationManager.AppSettings["UploadPath"] + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("INVOICE");

            string uploadedFile = GetInvoicePdfByteArray(invoice);

            if (!string.IsNullOrWhiteSpace(invoice.ToEmail))
            {
                UtilityRepository email = new UtilityRepository();
                email.SendEmail(toId, ccId, PopulateMailBody(invoice), subject, filePath, bccId, uploadedFile);
                msg = BOConstants.Email_Sent;
            }

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public string PopulateMailBody(IncomeBo income)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Server.MapPath("~/Content/Mail-Template/sendInvoice.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{InvNo}", income.InvoiceNo);
            body = body.Replace("{invDate}", Convert.ToDateTime(income.InvoiceDate).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{invDueDate}", Convert.ToDateTime(income.DueDate).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{Amount}", Utility.PrecisionForDecimal(income.TotalAmount, 2));
            body = body.Replace("{Companyemail}", income.CompanyEmaild);
            body = body.Replace("{companyName}", income.CompanyName);

            return body;
        }

        public ActionResult InvoicePrint(int inid)
        {
            IncomeRepository rep = new IncomeRepository();
            return View(rep.GetIncome(inid));
        }

        #region Invoice Print

        public ActionResult InvoiceDownload(int inId)
        {
            IncomeBo inv = _incomeRepository.GetIncome(inId);

            int afterDecimal = Convert.ToInt32(inv.TotalAmount.ToString("0.00").Split('.')[1]);
            inv.Totalinwords = NumberToText((int)inv.TotalAmount, true, false) + " Rupees And " + NumberToText(afterDecimal, true, false) + " Paisa Only.";
            return File(ConfigurationManager.AppSettings["UploadPath"]
                      + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("INVOICE") +
                        GetInvoicePdfByteArray(inv), MediaTypeNames.Application.Pdf,
                inv.CompanyName + "_" + inv.InvoiceNo + ".pdf");
        }

        public static string NumberToText(int number, bool useAnd, bool useArab)
        {
            if (number == 0)
            {
                return "Zero";
            }

            string and = useAnd ? "and " : ""; // deals with using 'and' separator

            if (number == -2147483648)
            {
                return "Minus Two Hundred " + and + "Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred " + and + "Forty Eight";
            }

            int[] num = new int[4];
            int first = 0;
            int u, h, t;
            StringBuilder sb = new StringBuilder();

            if (number < 0)
            {
                sb.Append("Minus ");
                number = -number;
            }

            string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
            string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };
            num[0] = number % 1000; // units
            num[1] = number / 1000;
            num[2] = number / 100000;
            num[1] = num[1] - 100 * num[2]; // thousands
            num[3] = number / 10000000; // crores
            num[2] = num[2] - 100 * num[3]; // lakhs
            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }

            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0)
                {
                    continue;
                }

                u = num[i] % 10; // ones
                t = num[i] / 10;
                h = num[i] / 100; // hundreds
                t = t - 10 * h; // tens

                if (h > 0)
                {
                    sb.Append(words0[h] + "Hundred ");
                }

                if (u > 0 || t > 0)
                {
                    if (h > 0 || i < first)
                    {
                        sb.Append(and);
                    }

                    if (t == 0)
                    {
                        sb.Append(words0[u]);
                    }
                    else if (t == 1)
                    {
                        sb.Append(words1[u]);
                    }
                    else
                    {
                        sb.Append(words2[t - 2] + words0[u]);
                    }
                }

                if (i != 0)
                {
                    sb.Append(words3[i - 1]);
                }
            }

            string temp = sb.ToString().TrimEnd();

            if (useArab && Math.Abs(number) >= 1000000000)
            {
                int index = temp.IndexOf("Hundred Crore", StringComparison.Ordinal);
                if (index > -1)
                {
                    return temp.Substring(0, index) + "Arab" + temp.Substring(index + 13);
                }

                index = temp.IndexOf("Hundred", StringComparison.Ordinal);
                return temp.Substring(0, index) + "Arab" + temp.Substring(index + 7);
            }

            return temp;
        }

        #endregion Invoice Print

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        #endregion Invoice

        #region Upload Download

        public JsonResult Upload(HttpPostedFileBase file)
        {
            List<string> list = new List<string>();
            if (file != null)
            {
                list = UploadUtility.UploadFile(file, Utility.UploadUrl("INVOICE"));
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteFile(string deleteFile)
        {
            return Json(Notification.Deleted, JsonRequestBehavior.AllowGet);
        }

        #endregion Upload Download

        #region Receivable

        public ActionResult InvoicePayment(int? id, string menuCode, int customerId, string customerName, string localScreenType, string methodName)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "SALES");
            Utility.SetSession("SubMenuRetention", "INVOICE");

            IncomeBo incomeBo = new IncomeBo();
            ItemDetailsBO itemDetailsBo = new ItemDetailsBO();
            InvoiceReceivableBO invoiceReceivableBo = new InvoiceReceivableBO();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            if (customerId == 0)
            {
                incomeBo.ItemDetail = new List<ItemDetailsBO> { itemDetailsBo };
                incomeBo.ReceivableDetail = new List<InvoiceReceivableBO> { invoiceReceivableBo };
            }

            incomeBo = _incomeRepository.GetInvoiceCustomerDetails(customerId);
            incomeBo.ReceivableDetail = _incomeRepository.SearchInvoiceReceivableList(id, customerId);

            if (methodName == "get")
            {
                incomeBo = _incomeRepository.GetInvoiceReceivable(customerId);
            }

            incomeBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
            incomeBo.PaymentModeList = _commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");

            if (localScreenType != null)
            {
                incomeBo.ScreenType = localScreenType;
            }

            incomeBo.MenuCode = menuCode;
            return PartialView(incomeBo);
        }

        [HttpPost]
        public ActionResult InvoicePayment(IncomeBo incomeBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (incomeBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), incomeBo.Id == 0 ? "Save" : "Edit");
            }

            if (incomeBo != null && incomeBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            return Json(_incomeRepository.ManageReceivable(incomeBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult InvoiceHoldingDetails(int id)
        {
            return PartialView(_incomeRepository.SearchInvoiceHoldingList(id));
        }

        [HttpPost]
        public ActionResult InvoiceHoldingDetails(IList<InvoiceHoldingsBO> invoiceHoldingsBo)
        {
            return Json(_incomeRepository.ManageInvoiceHoldings(invoiceHoldingsBo));
        }

        #endregion Receivable

        #region Receipts

        public ActionResult SearchReceipts(string menuCode, int? id, string screenType, int? statusId)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "VOUCHER");
            Utility.SetSession("SubMenuRetention", "RECEIPTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Receipts");

            //commonRepository.GetSearchDataHistory(searchParamsBO, "pageload", FormName, Convert.ToInt32(Session["StartMonth"]));
            SearchParamsBo searchParamsBo = new SearchParamsBo
            {
                Id = id ?? 0,
                StatusId = statusId ?? 0,
                ScreenType = screenType
            };
            return View(searchParamsBo);
        }

        public ActionResult GetReceiptsDetails(int? ledgerId)
        {
            return PartialView(_incomeRepository.GetReceiptsDetails(ledgerId, null));
        }

        public ActionResult Receipts(SearchParamsBo searchParamsBo)
        {
            return PartialView(_incomeRepository.SearchReceipts(searchParamsBo));
        }

        public ActionResult AccountReceipts(int id, string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            IncomeBo incomeBo = _incomeRepository.GetAccountReceipts(id);

            incomeBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
            incomeBo.PaymentModeList = _commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
            incomeBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger");
            incomeBo.CurrencyList = masterDataRepository.SearchMasterDataDropDown("Currency");
            incomeBo.MenuCode = menuCode;
            return PartialView(incomeBo);
        }

        [HttpPost]
        public ActionResult AccountReceipts(IncomeBo incomeBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (incomeBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), incomeBo.MenuCode, incomeBo.Id == 0 ? "Save" : "Edit");
            }

            if (incomeBo != null && incomeBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), incomeBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            return Json(_incomeRepository.ManageReceipts(incomeBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Receipts

        #region Customer

        public ActionResult Customer()
        {
            CustomerDetailsBo customerDetailsBo = new CustomerDetailsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            customerDetailsBo.CurrencyList = masterDataRepository.SearchMasterDataDropDown("Currency");
            return PartialView(customerDetailsBo);
        }

        [HttpPost]
        public ActionResult Customer(CustomerDetailsBo customerDetailsBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            IncomeBo incomeBo = new IncomeBo();
            bool hasAccess = false;
            if (customerDetailsBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), customerDetailsBo.MenuCode, incomeBo.Id == 0 ? "Save" : "Edit");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            CustomerRepository customerRepository = new CustomerRepository();

            string userMessage = customerRepository.ManageCustomer(customerDetailsBo);
            return Json(userMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion Customer

        #region Product / Service

        public ActionResult ProductService(string source)
        {
            ProductMasterBo productMasterBo = new ProductMasterBo();
            CommonRepository commonRepository = new CommonRepository();

            productMasterBo.TransactionTypeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "ProductType", "Type");
            productMasterBo.TypeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "ProductServiceType", "Type");
            productMasterBo.UserMessage = source;
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            productMasterBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger");
            return PartialView(productMasterBo);
        }

        [HttpPost]
        public ActionResult ProductService(ProductMasterBo productMasterBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            IncomeBo incomeBo = new IncomeBo();
            bool hasAccess = false;
            if (productMasterBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), productMasterBo.MenuCode, incomeBo.Id == 0 ? "Save" : "Edit");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            ProductMasterRepository productMasterRepository = new ProductMasterRepository();
            return Json(productMasterRepository.ManageProduct(productMasterBo));
        }

        #endregion Product / Service

        #region Payment

        public ActionResult SearchPaymentDetails(string menuCode, int? id, string screenType, int? statusId)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "VOUCHER");
            Utility.SetSession("SubMenuRetention", "PAYMENTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Payments");

            //commonRepository.GetSearchDataHistory(searchParamsBO, "pageload", FormName, Convert.ToInt32(Session["StartMonth"]));
            SearchParamsBo searchParamsBo = new SearchParamsBo
            {
                Id = id ?? 0,
                StatusId = statusId ?? 0,
                ScreenType = screenType
            };
            return View(searchParamsBo);
        }

        public ActionResult GetPaymentDetails(int? ledgerId)
        {
            return PartialView(_incomeRepository.GetPaymentDetails(ledgerId, null));
        }

        public ActionResult PaymentDetails(SearchParamsBo searchParamsBo)
        {
            //if (searchParamsBO.Event == "search")
            //    commonRepository.GetSearchDataHistory(searchParamsBO, "search", FormName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(_incomeRepository.SearchReceiptPayments(searchParamsBo));
        }

        public ActionResult Payment(int id, string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            IncomeBo incomeBo = _incomeRepository.GetAccountPayments(id);

            incomeBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
            incomeBo.PaymentModeList = _commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
            incomeBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger");
            incomeBo.MenuCode = menuCode;
            return PartialView(incomeBo);
        }

        [HttpPost]
        public ActionResult Payment(IncomeBo incomeBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (incomeBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), incomeBo.MenuCode, incomeBo.Id == 0 ? "Save" : "Edit");
            }

            if (incomeBo != null && incomeBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), incomeBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            return Json(_incomeRepository.ManagePayment(incomeBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Payment
    }
}