﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class AuditTrailController : Controller
    {
        #region Variables

        private readonly AuditRepository _auditRepository = new AuditRepository();

        #endregion Variables

        #region Audit Trial

        public ActionResult SearchAuditTrail()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "AUDITTRAIL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Audit Trial");

            AuditTrailBo auditTrailBo = new AuditTrailBo();
            return View(auditTrailBo);
        }

        public ActionResult AuditTrialList(AuditTrailBo auditTrailBo)
        {
            if (auditTrailBo.ToDate == null)
            {
                auditTrailBo.ToDate = DateTime.Now;
            }

            if (auditTrailBo.FromDate == null)
            {
                auditTrailBo.FromDate = auditTrailBo.ToDate.Value.AddYears(-1);
            }

            return PartialView(_auditRepository.AuditTrialList(auditTrailBo));
        }

        public ActionResult AutoCompleteAuditTableName(string tableName)
        {
            return Json(_auditRepository.SearchAutoCompleteAuditTableName(tableName), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAuditTables()
        {
            return Json(_auditRepository.UpdateAuditTables(), JsonRequestBehavior.AllowGet);
        }

        #endregion Audit Trial
    }
}