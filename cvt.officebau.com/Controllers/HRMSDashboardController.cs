﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class HRMSDashboardController : Controller
    {
        #region Variable

        private readonly HrmsDashboardRepository _hrmsDashboardRepository = new HrmsDashboardRepository();

        #endregion Variable

        #region Attrition

        public ActionResult SearchAttrition()
        {
            return Json(_hrmsDashboardRepository.SearchAttrition(), JsonRequestBehavior.AllowGet);
        }

        #endregion Attrition

        #region Top Paid Monthly

        public ActionResult SearchTopPaidMonthly(int amount, int businessUnitId)
        {
            return Json(_hrmsDashboardRepository.SearchTopPaidMonthly(amount, businessUnitId), JsonRequestBehavior.AllowGet);
        }

        #endregion Top Paid Monthly

        #region Headcount

        public ActionResult HrmsDashboard(string menuCode)
        {
            Utility.SetSession("MenuRetention", "DASHBOARDANDREPORTS");
            Utility.SetSession("SubMenuRetention", "HRMS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("HRMS Dashboard");

            MasterDataRepository masterRepository = new MasterDataRepository();

            HrmsDashboardBo hrmsDashboardBo = _hrmsDashboardRepository.GetDashboardTiles();
            hrmsDashboardBo.ReportType = "Department";
            hrmsDashboardBo.BusinessUnitList = masterRepository.GetBusinessUnitDropDown();
            hrmsDashboardBo.EndRangeValue = _hrmsDashboardRepository.GetTopRangeValue();
            return View(hrmsDashboardBo);
        }

        public ActionResult SearchHeadcount(string reportType)
        {
            return Json(_hrmsDashboardRepository.SearchHeadcount(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetHeadcountByBusinessUnit(string businessUnit, string reportType)
        {
            return Json(_hrmsDashboardRepository.GetHeadcountByBu(businessUnit, reportType), JsonRequestBehavior.AllowGet);
        }

        public ActionResult HeadcountGridView(string businessUnit, string reportType)
        {
            return PartialView(_hrmsDashboardRepository.GetHeadcountByBu(businessUnit, reportType));
        }

        #endregion Headcount

        #region Compensation

        public ActionResult SearchCompensation(string reportType)
        {
            return Json(_hrmsDashboardRepository.SearchCompensation(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCompensationByBusinessUnit(string businessUnit, string reportType)
        {
            return Json(_hrmsDashboardRepository.GetCompensationByBu(businessUnit, reportType), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CompensationGridView(string businessUnit, string reportType)
        {
            HrmsDashboardBo hrmsDashboardBo = new HrmsDashboardBo { ReportType = reportType.ToLower() };

            System.Collections.Generic.List<HrmsDashboardBo> hrmsDashboardBoList = _hrmsDashboardRepository.GetCompensationByBu(businessUnit, reportType);
            if (hrmsDashboardBoList.Count == 0)
            {
                hrmsDashboardBoList.Add(hrmsDashboardBo);
            }

            return PartialView(hrmsDashboardBoList);
        }

        #endregion Compensation

        #region DashBoard Configuration

        public ActionResult DashBoardConfig()
        {
            ConfigBo configBo = new ConfigBo
            {
                PayStructureComponentList = _hrmsDashboardRepository.GetPayrollComponentDropDown()
            };
            configBo.PayStructureComponentList.RemoveAt(0);
            return PartialView(configBo);
        }

        public ActionResult SaveDashBoardConfig(string key, string value)
        {
            return Json(_hrmsDashboardRepository.ManageDashBoarrdConfig(key, value), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PayrollDashboardConfiguration()
        {
            ConfigBo configBo = new ConfigBo
            {
                PayStructureComponentList = _hrmsDashboardRepository.GetPayrollComponentDropDown()
            };
            configBo.PayStructureComponentList.RemoveAt(0);
            return PartialView(configBo);
        }

        public ActionResult SearchDashboardConfig()
        {
            return Json(_hrmsDashboardRepository.SearchDashboardConfig(), JsonRequestBehavior.AllowGet);
        }

        #endregion DashBoard Configuration
    }
}