﻿//using cvt.officebau.com.Utilities;
//using cvt.officebau.com.Services;
//using cvt.officebau.com.ViewModels;
//using System;
//using System.Collections.Generic;
//using System.Web.Mvc;

//namespace cvt.officebau.com.Controllers
//{
//    [SessionExpire]
//    public class PayStructureController : Controller
//    {
//        #region Duplicate Name Check

//        public ActionResult DuplicateCheck(string name, string table, int bu)
//        {
//            PayStructureBO payStructureBo = new PayStructureBO();
//            if (name.Trim() == string.Empty)
//            {
//                payStructureBo.UserMessage = "1";
//            }
//            else
//            {
//                payStructureBo = _payStructureRepository.DuplicateCheck(name, table, bu);
//            }

//            return Json(payStructureBo.UserMessage, JsonRequestBehavior.AllowGet);
//        }

//        #endregion

//        #region Check for Addition Components Paystructure

//        public ActionResult CheckforAdditionComponentsPaystructure(string businessUnitId, string type)
//        {
//            return Json(_payStructureRepository.CheckforAdditionComponentsPaystructure(businessUnitId, type), JsonRequestBehavior.AllowGet);
//        }

//        #endregion

//        #region Variables

//        private readonly PayStructureRepository _payStructureRepository = new PayStructureRepository();

//        private string _message = string.Empty;
//        private bool _hasAccess;

//        #endregion

//        #region Company Paystructure

//        public ActionResult CompanyPayStructure()
//        {
//            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
//            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
//             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Company Paystructure");

//            PayStructureBO payStructureBo = new PayStructureBO();

//            List<PayStructureBO> payStructureBoList = _payStructureRepository.SearchCompanyPaystructure(payStructureBo);
//            return PartialView(payStructureBoList);
//        }

//        public ActionResult CreateCompanyPayStructure(int id)
//        {
//            PayStructureBO payStructureBo = new PayStructureBO
//            {
//                MenuCode = MenuCode.COMPPAYSTRUCT
//            };

//            if (id != 0)
//            {
//                payStructureBo = _payStructureRepository.GetCompanyPayStructure(id);
//            }

//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            payStructureBo.BusinessUnitList = masterDataRepository.GetRegionBasedOnEmployeeAccess(Utility.UserId(), 0, string.Empty);
//            return PartialView(payStructureBo);
//        }

//        [HttpPost]
//        public ActionResult CreateCompanyPayStructures(PayStructureBO payStructureBo)
//        {
//            payStructureBo.MenuCode = MenuCode.CompanyPaystructure;
//            RbsRepository rbsRepository = new RbsRepository();

//            _hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), payStructureBo.MenuCode, payStructureBo.Id == 0 ? "Save" : "Edit");

//            if (!_hasAccess)
//            {
//                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
//            }

//            _message = _payStructureRepository.ManageCompanyPayStructure(payStructureBo);
//            return Json(_message, JsonRequestBehavior.AllowGet);
//        }

//        public ActionResult DeleteCompanyPayStructure(int id, bool isDeleted)
//        {
//            PayStructureBO payStructureBo = new PayStructureBO
//            {
//                Id = id,
//                IsDeleted = true
//            };
//            RbsRepository rbsRepository = new RbsRepository();

//            bool hasAccess = false;
//            if (payStructureBo.IsDeleted)
//            {
//                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.CompanyPaystructure, "Delete");
//            }

//            if (!hasAccess)
//            {
//                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
//            }

//            _message = _payStructureRepository.DeleteCompanyPayStructure(payStructureBo);
//            return Json(_message, JsonRequestBehavior.AllowGet);
//        }

//        #endregion

//        #region Employee Pay Structure

//        public ActionResult EmployeePayStructure()
//        {
//            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
//            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
//             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Paystructure");

//            MasterDataRepository masterDataRepository = new MasterDataRepository();
//            PayStructureBO payStructureBo = new PayStructureBO
//            {
//                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
//            };
//            payStructureBo.BusinessUnitList.RemoveAt(0);
//            return PartialView(payStructureBo);
//        }

//        public ActionResult EmployeeList()
//        {
//            PayStructureBO payStructureBo = new PayStructureBO();
//            return PartialView(_payStructureRepository.SearchUser(payStructureBo));
//        }

//        public ActionResult EmployeePayStructureDetail(PayStructureBO payStructureBo)
//        {
//            List<PayStructureBO> payStructureBoList = _payStructureRepository.SearchEmployee(payStructureBo);
//            return PartialView(payStructureBoList);
//        }

//        public ActionResult SearchEmployeePayStructure(int id)
//        {
//            MasterDataRepository masterDataRepository = new MasterDataRepository();
//            PayStructureBO payStructureBo = _payStructureRepository.GetPayStructureList(id);
//            payStructureBo.CompanyPayStuctureList = masterDataRepository.GetRegionBasedOnEmployeeAccess(Utility.UserId(), 0, "CompanyPayStructure");
//            return PartialView(payStructureBo);
//        }

//        public ActionResult EmployeePayStructureList(PayStructureBO payStructureBo)
//        {
//            payStructureBo = _payStructureRepository.SearchEmployeePayStructure(payStructureBo);
//            if (payStructureBo.Counts != 0 && payStructureBo.CheckCompanyPS != 0)
//            {
//                return PartialView(payStructureBo);
//            }

//            return Json(payStructureBo.CheckCompanyPS == 0 ? "Please Configure the Company Pay Structure." :
//                "Please Configure the Statutory Details.", JsonRequestBehavior.AllowGet);
//        }

//        [HttpPost]
//        public ActionResult SearchEmployeePayStructure(PayStructureBO payStructureBo, decimal? medicalClaim,
//        decimal? pli, decimal? mobileDataCard, decimal? bonus, decimal? perks, string ctc, decimal? medicalAllowance, decimal? conveyance)
//        {
//            payStructureBo.MenuCode = MenuCode.EmployeePaystructure;
//            RbsRepository rbsRepository = new RbsRepository();

//            _hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), payStructureBo.MenuCode, payStructureBo.Id == 0 ? "Save" : "Edit");

//            if (!_hasAccess)
//            {
//                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
//            }

//            payStructureBo.Medicalclaim = medicalClaim;
//            payStructureBo.PLI = pli;
//            payStructureBo.Bonus = bonus;
//            payStructureBo.MobileDataCard = mobileDataCard;
//            payStructureBo.OtherPerks = perks;
//            payStructureBo.MedicalAllowance = medicalAllowance;
//            payStructureBo.Conveyance = conveyance;
//            payStructureBo.CTC = Convert.ToDecimal(ctc);

//            _message = _payStructureRepository.ManageEmployeePayStructure(payStructureBo);
//            return Json(_message, JsonRequestBehavior.AllowGet);
//        }

//        public ActionResult GetPayHistoryList(PayStructureBO payStructureBo)
//        {
//            return PartialView(_payStructureRepository.GetPayHistoryList(payStructureBo));
//        }

//        public ActionResult DeleteEmployeePayStructure(int id, bool isDeleted)
//        {
//            PayStructureBO payStructureBo = new PayStructureBO
//            {
//                Id = id,
//                IsDeleted = true
//            };
//            RbsRepository rbsRepository = new RbsRepository();

//            bool hasAccess = false;
//            if (payStructureBo.IsDeleted)
//            {
//                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.EmployeePaystructure, "Delete");
//            }

//            if (!hasAccess)
//            {
//                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
//            }

//            _message = _payStructureRepository.DeleteEmployeePayStructure(payStructureBo);
//            return Json(_message, JsonRequestBehavior.AllowGet);
//        }

//        public ActionResult AutoCompleteEmployee(string employeeName, string bu)
//        {
//            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
//            List<CustomBo> resultList = employeeMasterRepository.SearchAutoCompleteEmployee(employeeName, bu);
//            return Json(resultList, JsonRequestBehavior.AllowGet);
//        }

//        #endregion

//        #region PayStructure Configuration

//        public ActionResult PayStructureConfiguration()
//        {
//             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("PayStructure Configuration");

//            MasterDataRepository masterDataRepository = new MasterDataRepository();
//            PayStructureBO payStructureBo = new PayStructureBO
//            {
//                BusinessUnitList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", string.Empty)
//            };
//            return PartialView(payStructureBo);
//        }

//        public ActionResult ManagePayStructureConfiguration(string code)
//        {
//            PayStructureBO payStructureBo = new PayStructureBO { Code = code };

//            MasterDataRepository masterDataRepository = new MasterDataRepository();
//            payStructureBo.BusinessUnitList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", string.Empty);
//            string datas = _payStructureRepository.GetPaystructureConfiguration(payStructureBo);
//            if (datas != string.Empty)
//            {
//                payStructureBo.BusinessUnitIDs = datas.Split('/')[0];
//                payStructureBo.ModifiedByName = datas.Split('/')[1];
//                payStructureBo.ModifiedOn = Convert.ToDateTime(datas.Split('/')[2]);
//                payStructureBo.BaseLocation = datas.Split('/')[3];
//            }

//            return PartialView(payStructureBo);
//        }

//        [HttpPost]
//        public ActionResult ManagePayStructureConfiguration(PayStructureBO payStructureBo)
//        {
//            payStructureBo.DomainId = Utility.DomainId();
//            payStructureBo.CreatedBy = Utility.UserId();
//            payStructureBo.ModifiedBy = Utility.UserId();
//            return Json(_payStructureRepository.ManagePayStructureConfiguration(payStructureBo), JsonRequestBehavior.AllowGet);
//        }

//        #endregion
//    }
//}
