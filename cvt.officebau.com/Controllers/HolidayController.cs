﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class HolidayController : Controller
    {
        #region Variable

        private readonly HolidayRepository _holidayRepository = new HolidayRepository();

        #endregion Variable

        #region Holiday

        public ActionResult Holidays()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Holidays");

            return PartialView();
        }

        public ActionResult CreateHoliday(int id, string holidayType)
        {
            HolidayBo holidayBo = new HolidayBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            if (id != 0)
            {
                holidayBo = _holidayRepository.GetHoliday(id);
                if (holidayType.ToLower().Contains("yearly"))
                {
                    string regionId = holidayBo.BusinessUnit.Replace(",", string.Empty);
                    holidayBo.RegionId = Convert.ToInt32(regionId);
                }

                holidayBo.YearId = masterDataRepository.GetCurrentYearId(Convert.ToString(holidayBo.Year));
                holidayBo.HolidayType = holidayType;
            }
            else
            {
                holidayBo.HolidayType = holidayType;
            }

            holidayBo.RegionListAll = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", string.Empty);
            holidayBo.RegionList = masterDataRepository.GetRegionDropDownDependOnEmployee(Utility.UserId());
            holidayBo.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            return PartialView(holidayBo);
        }

        [HttpPost]
        public ActionResult CreateHoliday(HolidayBo holidayBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.Holidays, holidayBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_holidayRepository.ManageHoliday(holidayBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchHoliday(HolidayBo holidayBo)
        {
            return PartialView(_holidayRepository.SearchHoliday(holidayBo));
        }

        public int GetFinancialYearId(DateTime date)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return masterDataRepository.GetCurrentYearId(date.Year.ToString());
        }

        public JsonResult GetFinancialYearDropdown()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            System.Collections.Generic.List<SelectListItem> yearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            yearList.RemoveAt(0);
            return Json(yearList, JsonRequestBehavior.AllowGet);
        }

        #endregion Holiday

        #region Business Hours

        public ActionResult BusinessHours()
        {
            return PartialView();
        }

        public ActionResult SearchBusinessHours()
        {
            return PartialView(_holidayRepository.SearchBusinessHours(Utility.DomainId()));
        }

        public ActionResult CreateBusinessHours(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            HolidayBo holidayBo = new HolidayBo();

            if (id != 0)
            {
                holidayBo = _holidayRepository.GetBusinessHours(id);
            }

            holidayBo.RegionList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", string.Empty);
            return PartialView(holidayBo);
        }

        [HttpPost]
        public ActionResult CreateBusinessHours(HolidayBo holidayBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.BusinessHours, holidayBo.Id == 0 ? "Save" : "Edit");

            if (holidayBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.BusinessHours, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_holidayRepository.ManageBusinessHours(holidayBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Business Hours
    }
}