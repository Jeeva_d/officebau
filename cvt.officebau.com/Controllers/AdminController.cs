﻿using ClosedXML.Excel;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace cvt.officebau.com.Controllers
{
    public class AdminController : Controller
    {
        private AdminBo _adminBo = new AdminBo();
        private readonly AdminRepository _adminRepository = new AdminRepository();
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;

        #region Admin - Manage Customer

        public ActionResult AdminUser()
        {
            return View();
        }

        public ActionResult SearchNewAdmin()
        {
            if (Utility.UserId() > 0)
            {
                Utility.SetSession("MenuRetention", "ADMIN");
                Utility.SetSession("SubMenuRetention", "ADMINFORCOMPANY");
                return View(_adminBo);
            }
            else
            {
                RouteValueDictionary routeValues = new RouteValueDictionary { { "sessionExpiry", "" }, { "deviceId", "" } };
                return RedirectToAction("UserLogin", "UserLogin", routeValues);
            }
        }

        public ActionResult NewAdminList(AdminBo adminBo)
        {
            return PartialView(_adminRepository.GetCompanyAdminList(adminBo));
        }

        public ActionResult InactiveEmployees(AdminBo adminBo)
        {
            return Json(_adminRepository.InActiveEmployees(adminBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteCompany(string companyName)
        {
            System.Collections.Generic.List<AdminBo> resultList = _adminRepository.SearchAutoCompleteCompanyName(companyName);
            return Json(resultList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateNewAdmin(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            if (id != 0)
            {
                _adminBo = _adminRepository.GetAdminDetail(id);
            }
            _adminBo.ModulesList = masterDataRepository.SearchMasterDataDropDown("RBSModule", string.Empty);
            _adminBo.ModulesList.RemoveAt(0);

            return PartialView(_adminBo);
        }

        [HttpPost]
        public ActionResult CreateNewAdmin(AdminBo adminBo)
        {
            string message = string.Empty;

            switch (adminBo.Operation.ToUpper())
            {
                case Operations.Insert:

                    message = _adminRepository.ManageAdminDetails(adminBo);

                    if (message.ToUpper().Contains("SUCCESSFULLY"))
                    {
                        int companyId = Convert.ToInt32(message.Split('/')[1]);
                        string defaultPassword = ConfigurationManager.AppSettings["DefaultPassword"];

                        adminBo = _adminRepository.GetCompanyEmailDetails(companyId, defaultPassword);
                        EmailUtility.SendInstantEmail(adminBo.EmailId, _adminRepository.PopulateCompanyMailBody(adminBo.CompanyName, adminBo.EmailId,
                                adminBo.ContactNo, defaultPassword, adminBo.Operation), "Welcome to OfficeBAU !", adminBo.EmailId, string.Empty);
                    }

                    break;

                case Operations.Update:
                    message = _adminRepository.UpdateAdminForCompany(adminBo);
                    break;

                case Operations.Delete:
                    message = _adminRepository.DeleteAdminForCompany(adminBo);
                    break;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportDatabaseTablesForDomain(int domainId, string domainName)
        {
            DataSet ds = GetExcelData(domainId);
            DataTable dt = ds.Tables[0];
            ds.Tables.Remove(dt);
            int c = 0;

            using (XLWorkbook wb = new XLWorkbook())
            {
                IXLWorksheet ws = wb.Worksheets.Add("Index");
                ws.Style.Fill.PatternBackgroundColor = XLColor.White;
                ws.Style.Fill.BackgroundColor = XLColor.White;
                foreach (DataTable table in ds.Tables)
                {
                    string value = dt.Rows[c].ItemArray[0].ToString();
                    ds.Tables[c].TableName = value.Length <= 30 ? value : value.Substring(0, 30);

                    c = c + 1;
                }

                c = 0;
                foreach (DataTable table in ds.Tables)
                {
                    DataColumnCollection columns = ds.Tables[c].Columns;

                    if (columns.Contains("EmployeeID"))
                    {
                        ds.Tables[c].Columns.Add("Employee Name").SetOrdinal(ds.Tables[c].Columns["EmployeeID"].Ordinal + 1);
                        DataView view = new DataView(ds.Tables[c]);
                        DataTable distinctValues = view.ToTable(true, "EmployeeID");
                        foreach (DataRow a in distinctValues.Rows)
                        {
                            DataRow[] dr = ds.Tables["tbl_EmployeeMaster"].Select("ID = " + Convert.ToInt32(a["EmployeeID"]).ToString());
                            if (dr.Length > 0)
                            {
                                ds.Tables[c].Select(string.Format("[EmployeeID] = '{0}'", Convert.ToInt32(a["EmployeeID"]))).ToList<DataRow>().ForEach(r => r["Employee Name"] = dr[0]["FullName"]);
                            }
                        }
                        ds.Tables[c].Columns.Remove("EmployeeID");
                    }
                    c = c + 1;
                }

                dt.Columns.Add("S.No");
                dt.Columns["S.No"].SetOrdinal(0);
                string[] header = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };
                string[] cols = new string[dt.Columns.Count];
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    cols[i] = dt.Columns[i].ToString();
                }

                int StartIndexCols = 1;

                for (int i = 1; i <= cols.Length; i++)
                {
                  
                        string dataCell = header[i - 1] + Convert.ToString(StartIndexCols);
                        ws.Cell(dataCell).Value = cols[i - 1];
                        ws.Cell(dataCell).Style.Font.Bold = true;
                        ws.Cell(dataCell).Style.Fill.BackgroundColor = XLColor.AirForceBlue;
                        ws.Cell(dataCell).Style.Font.FontColor = XLColor.White;
                        ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                }

                int startIndexData = 2;
                string lastChar = header[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["S.No"] = i + 1;
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        string dataCell = header[j] + startIndexData;
                        string a = dt.Rows[i][j].ToString();
                        string value = dt.Rows[i][j].ToString();
                        a = a.Replace("&nbsp;", " ");
                        a = a.Replace("&amp;", "&");
                        ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        ws.Cell(dataCell).Style.Alignment.Indent = 1;
                        ws.Cell(dataCell).Hyperlink = new XLHyperlink("'" + (value.Length <= 30 ? value : value.Substring(0, 30)) + "'!A1");

                        ws.Cell(dataCell).SetValue(a.Replace("*", ""));
                        lastChar = header[j];
                    }
                    startIndexData++;
                }
                string range = "A1:" + lastChar + dt.Rows.Count + 1;
                ws.Range(range).Style.Font.FontSize = 10;
                ws.Range(range).Style.Font.FontName = "Cambria";

                ws.Range(range).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                ws.Range(range).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                ws.Range(range).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                ws.Range(range).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                foreach (DataTable table in ds.Tables)
                {
                    wb.Worksheets.Add(table);
                }

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", domainName
                                                                                                                       + "_" + DateTime.Now.ToShortDateString() + ".xlsx");
                }
            }
        }

        public DataSet GetExcelData(int domainId)
        {
            DataSet dataset = new DataSet();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DBA_GetAllTableForDomain", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, domainId);
                    con.Open();
                    using (SqlDataAdapter dr = new SqlDataAdapter(cmd))
                    {
                        dr.Fill(dataset);
                    }
                }
            }

            return dataset;
        }

        #endregion Admin - Manage Customer

        #region Deployment Acitvity

        public ActionResult DeploymentList()
        {
            if (Utility.UserId() > 0)
            {
                Utility.SetSession("MenuRetention", "ADMIN");
                Utility.SetSession("SubMenuRetention", "ADMINFORDEPLOYMENT");

                return View(_adminRepository.GetDeploymentList());
            }
            else
            {
                RouteValueDictionary routeValues = new RouteValueDictionary { { "sessionExpiry", "" }, { "deviceId", "" } };
                return RedirectToAction("UserLogin", "UserLogin", routeValues);
            }
        }

        public ActionResult CreateDeployment(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            if (id != 0)
            {
                _adminBo = _adminRepository.GetDeployment(id);
            }
            _adminBo.CompanyList = masterDataRepository.SearchMasterDataDropDown("Company");
            return PartialView(_adminBo);
        }

        [HttpPost]
        public ActionResult ManageDeployment(AdminBo adminBo)
        {
            if (!string.IsNullOrWhiteSpace(adminBo.CompanyIDs))
            {
                var companyId = adminBo.CompanyIDs.TrimStart(',');
                adminBo.CompanyIDs = "," + companyId + ",";
            }

            string message = _adminRepository.ManageDeployment(adminBo);

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateDeploymentActivityStatus(int id, bool value)
        {
            return Json(_adminRepository.UpdateDeploymentActivityStatus(id, value), JsonRequestBehavior.AllowGet);
        }

        #endregion Deployment Acitvity
    }
}