﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class MasterDataController : Controller
    {
        #region Variables

        private readonly MasterDataRepository _masterDataRepository = new MasterDataRepository();

        #endregion Variables

        #region Menu List

        public ActionResult MasterMenu(string menuCode)
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            switch (menuCode.ToUpper())
            {
                case "MASDATA":
                    Utility.SetSession("SubMenuRetention", "MASTERDATA-ACCOUNT");
                    break;

                case "MASDATA_HRMS":
                    Utility.SetSession("SubMenuRetention", "MASTERDATA-HRMS");
                    break;

                case "MASDATA_CONF":
                    Utility.SetSession("SubMenuRetention", "MASTERDATA-CONFIGURATION");
                    break;
            }

             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Master Data");

            return View(_masterDataRepository.SearchMasterDataMenuList(menuCode));
        }

        #endregion Menu List

        #region Search Master Data

        public ActionResult SearchMasterData(string mainTable, string depandantTable)
        {
            return PartialView(_masterDataRepository.SearchMasterData(mainTable, depandantTable));
        }

        #endregion Search Master Data

        #region Delete MasterData

        [HttpPost]
        public ActionResult DeleteMasterData(MasterDataBo masterDataBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            masterDataBo.MenuCode = MenuCode.MasterData;
            bool hasAccess = false;
            masterDataBo.IsDeleted = true;

            if (masterDataBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), masterDataBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_masterDataRepository.DeleteMasterData(masterDataBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Delete MasterData

        #region  Add Master Data

        public ActionResult AddMasterData(string mainTable, string depandantTable, int? id)
        {
            MasterDataBo masterDataBo = new MasterDataBo { MainTable = mainTable, DependantTable = depandantTable, TypeId = null };

            if (id != null)
            {
                masterDataBo = _masterDataRepository.GetMasterData(mainTable, depandantTable, id);
            }

            if (depandantTable.ToUpper() != "NULL" || string.IsNullOrWhiteSpace(depandantTable))
            {
                masterDataBo.DependantDropDownList = _masterDataRepository.SearchMasterDataDropDown(depandantTable, "");
            }

            return PartialView(masterDataBo);
        }

        [HttpPost]
        public ActionResult AddMasterData(MasterDataBo masterDataBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            masterDataBo.MenuCode = MenuCode.MasterData;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), masterDataBo.MenuCode, masterDataBo.Id == 0 ? "Save" : "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_masterDataRepository.ManageMasterData(masterDataBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Add Master Data
    }
}
