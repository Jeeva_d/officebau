﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeClaimsConfigurationController : Controller
    {
        #region Claim Configuration
        UtilityRepository _rep = new UtilityRepository();

        public ActionResult EmployeeClaimsConfiguration()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
            _rep.TrackFormNavigation("Claims Configuration");

            RbsRepository rBsRepository = new RbsRepository();
            return View(rBsRepository.SearchMenuForConfiguration());
        }

        #endregion Claim Configuration

        #region Variable

        private EmployeeClaimsBo _employeeClaimsBo = new EmployeeClaimsBo();
        private readonly EmployeeClaimsConfigurationRepository _employeeClaimsConfigurationRepository = new EmployeeClaimsConfigurationRepository();

        #endregion Variable

        #region Claim Policy

        public ActionResult ClaimsPolicy()
        {
            return PartialView();
        }

        public ActionResult SearchClaimsPolicyList()
        {
            return PartialView(_employeeClaimsConfigurationRepository.SearchClaimsPolicyList());
        }

        public ActionResult CreateClaimPolicy(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            _employeeClaimsBo.Id = id;

            if (id != 0)
            {
                _employeeClaimsBo = _employeeClaimsConfigurationRepository.GetClaimPolicy(id);
            }

            _employeeClaimsBo.ExpenseTypeList = masterDataRepository.SearchMasterDataDropDown("ClaimCategory", string.Empty);
            _employeeClaimsBo.BandList = masterDataRepository.SearchMasterDataDropDown("Band", string.Empty);
            _employeeClaimsBo.LevelList = masterDataRepository.SearchMasterDataDropDown("EmployeeGrade", string.Empty);
            _employeeClaimsBo.DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty);

            return PartialView(_employeeClaimsBo);
        }

        [HttpPost]
        public ActionResult CreateClaimPolicy(EmployeeClaimsBo employeeClaimsBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            employeeClaimsBo.MenuCode = MenuCode.ClaimPolicy;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeClaimsBo.MenuCode, employeeClaimsBo.Id == 0 ? "Save" : "Edit");

            if (employeeClaimsBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeClaimsBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            string message = _employeeClaimsConfigurationRepository.ManageClaimPolicy(employeeClaimsBo);
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Claim Policy

        #region Designation Mapping

        public ActionResult EmployeeConfiguration()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
            _rep.TrackFormNavigation("Configuration");

            RbsRepository rBsRepository = new RbsRepository();
            return View(rBsRepository.SearchMenuForConfiguration());
        }

        public ActionResult SearchDesignationMapping()
        {
            _rep.TrackFormNavigation("Designation  Mapping");
            return PartialView(_employeeClaimsConfigurationRepository.SearchDesignationMapping());
        }

        public ActionResult CreateDesignationMapping(int id)
        {
            _employeeClaimsBo.MenuCode = MenuCode.EMPCONFIG;

            if (id != 0)
            {
                _employeeClaimsBo = _employeeClaimsConfigurationRepository.GetDesignationMapping(id);
            }

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            _employeeClaimsBo.BandList = masterDataRepository.SearchMasterDataDropDown("Band", string.Empty);
            _employeeClaimsBo.LevelList = masterDataRepository.SearchMasterDataDropDown("EmployeeGrade", string.Empty);
            _employeeClaimsBo.DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty);
            return PartialView(_employeeClaimsBo);
        }

        [HttpPost]
        public ActionResult CreateDesignationMapping(EmployeeClaimsBo employeeClaimsBo)
        {
            employeeClaimsBo.ModifiedBy = Utility.UserId();
            employeeClaimsBo.CreatedOn = Utility.GetCurrentDate();
            employeeClaimsBo.ModifiedOn = Utility.GetCurrentDate();
            employeeClaimsBo.DomainId = Utility.DomainId();
            employeeClaimsBo.MenuCode = MenuCode.DesignationMapping;

            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeClaimsBo.MenuCode, employeeClaimsBo.Id == 0 ? "Save" : "Edit");

            if (employeeClaimsBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeClaimsBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_employeeClaimsConfigurationRepository.ManageDesignationMapping(employeeClaimsBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Designation Mapping

        #region Claims Approver Configuration

        public ActionResult ClaimsApprovalConfiguration()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _employeeClaimsBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();

            return PartialView(_employeeClaimsBo);
        }

        public ActionResult SearchApprovalConfiguration(int? businessUnitId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            _employeeClaimsBo.DesignationList = masterDataRepository.SearchMasterDataDropDown("EmployeeMaster", string.Empty);
            return PartialView(_employeeClaimsConfigurationRepository.SearchClaimsApproverConfiguration(businessUnitId));
        }

        [HttpPost]
        public ActionResult SubmitClaimApprovalConfiguration(List<EmployeeClaimsBo> list)
        {
            RbsRepository rbsRepository = new RbsRepository();
            _employeeClaimsBo.MenuCode = MenuCode.CLAMSAPPROVALCONFIG;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), _employeeClaimsBo.MenuCode, _employeeClaimsBo.Id == 0 ? "Save" : "Edit");
            if (_employeeClaimsBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), _employeeClaimsBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            _employeeClaimsBo = _employeeClaimsConfigurationRepository.ManageClaimsApproverConfiguration(list);
            return Json(_employeeClaimsBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteEmployee(string searchName, int? employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName, employeeId == 0 ? null : employeeId), JsonRequestBehavior.AllowGet);
        }

        #endregion Claims Approver Configuration
    }
}