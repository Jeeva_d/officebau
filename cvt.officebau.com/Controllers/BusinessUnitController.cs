﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class BusinessUnitController : Controller
    {
        #region Variables

        private BusinessUnitRepository _businessUnitRepository;

        #endregion Variables

        #region Business Unit

        public ActionResult BusinessUnit()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "MASTERDATA");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Business Unit");

            BusinessUnitBo businessUnitBo = new BusinessUnitBo();
            _businessUnitRepository = new BusinessUnitRepository();

            return PartialView(_businessUnitRepository.SearchBusinessUnit(businessUnitBo));
        }

        public ActionResult CreateBusinessUnit(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            _businessUnitRepository = new BusinessUnitRepository();
            BusinessUnitBo businessUnitBo = _businessUnitRepository.GetBusinessUnit(id);
            businessUnitBo.BusinessUnitList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", string.Empty);
            return PartialView(businessUnitBo);
        }

        [HttpPost]
        public ActionResult CreateBusinessUnit(BusinessUnitBo businessUnitBo)
        {
            businessUnitBo.ModifiedBy = Utility.UserId();
            businessUnitBo.CreatedOn = Utility.GetCurrentDate();
            businessUnitBo.ModifiedOn = Utility.GetCurrentDate();
            businessUnitBo.DomainId = Utility.DomainId();

            RbsRepository rbsRepository = new RbsRepository();
            _businessUnitRepository = new BusinessUnitRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), businessUnitBo.Id == 0 ? "Save" : "Edit");

            if (businessUnitBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_businessUnitRepository.ManageBusinessUnit(businessUnitBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Business Unit
    }
}