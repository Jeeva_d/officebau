﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class LeaveManagementController : Controller
    {
        #region Variable

        private readonly LeaveManagementRepository _leaveManagementRepository = new LeaveManagementRepository();

        #endregion Variable

        #region Leave Policy

        public ActionResult LeavePolicy()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "LEAVEPOLICY");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Leave Policy");

            return PartialView();
        }

        public ActionResult SearchLeavePolicy()
        {
            return PartialView(_leaveManagementRepository.SearchLeavePolicy(Utility.DomainId()));
        }

        public ActionResult CreateLeavePolicy(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo();

            if (id != 0)
            {
                leaveManagementBo = _leaveManagementRepository.GetLeavePolicy(id);
            }

            leaveManagementBo.RegionList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", string.Empty);
            leaveManagementBo.LeaveTypeList = masterDataRepository.SearchMasterDataDropDown("LeaveTypes", string.Empty);
            return PartialView(leaveManagementBo);
        }

        [HttpPost]
        public ActionResult CreateLeavePolicy(LeaveManagementBo leaveManagementBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            leaveManagementBo.DomainId = Utility.DomainId();
            leaveManagementBo.UserId = Utility.UserId();
            leaveManagementBo.ModifiedOn = Utility.GetCurrentDate();
            leaveManagementBo.MenuCode = MenuCode.LeavePolicy;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), leaveManagementBo.MenuCode, leaveManagementBo.Id == 0 ? "Save" : "Edit");

            if (leaveManagementBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), leaveManagementBo.MenuCode, "Delete");
            }

            return Json(!hasAccess ? Constants.UnAuthorizedAccess : _leaveManagementRepository.ManageLeavePolicy(leaveManagementBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Leave Policy

        #region Leave Configuration

        public ActionResult LeaveConfiguration()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Leave Configuration");

            RbsRepository rBsRepository = new RbsRepository();
            return View(rBsRepository.SearchMenuForConfiguration());
        }

        #region LeaveType

        public ActionResult LeaveType()
        {
            return PartialView();
        }

        public ActionResult SearchLeaveType()
        {
            return PartialView(_leaveManagementRepository.SearchLeaveType(Utility.DomainId()));
        }

        public ActionResult CreateLeaveType(int id)
        {
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo();

            if (id != 0)
            {
                leaveManagementBo = _leaveManagementRepository.GetLeaveType(id);
            }

            return PartialView(leaveManagementBo);
        }

        [HttpPost]
        public ActionResult CreateLeaveType(LeaveManagementBo leaveManagementBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            leaveManagementBo.DomainId = Utility.DomainId();
            leaveManagementBo.UserId = Utility.UserId();
            leaveManagementBo.ModifiedOn = Utility.GetCurrentDate();
            leaveManagementBo.MenuCode = MenuCode.LeaveType;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), leaveManagementBo.MenuCode, leaveManagementBo.LeaveTypeId == 0 ? "Save" : "Edit");

            if (leaveManagementBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), leaveManagementBo.MenuCode, "Delete");
            }

            return Json(!hasAccess ? Constants.UnAuthorizedAccess : _leaveManagementRepository.ManageLeaveType(leaveManagementBo), JsonRequestBehavior.AllowGet);
        }

        #endregion LeaveType

        //Not Used
        //public ActionResult BusinessHours()//Anbu
        //{
        //    return View();
        //}

        #endregion Leave Configuration

        #region Leave Request

        public ActionResult LeaveRequest()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "LEAVEREQUEST");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Leave Request");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo
            {
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave"),
                LeaveTypeList = masterDataRepository.SearchMasterDataDropDown("LeaveTypes", string.Empty),
                EmployeeList = masterDataRepository.GetEmployeeDropDown("EmployeeMaster", Utility.UserId(), true)
            };
            if (Utility.TmpUserId() > 0)
            {
                leaveManagementBo.EmployeeId = Utility.TmpUserId();
                leaveManagementBo.LeaveRequestBy = "Other";
            }
            else
            {
                Utility.SetSession("TmpUserID", null);
                leaveManagementBo.EmployeeId = Utility.UserId();
                leaveManagementBo.LeaveRequestBy = "Self";
            }
            if (masterDataRepository.HasOtherUserAccess(Utility.UserId(), "Leave request"))
            {
                leaveManagementBo.HasHRApplicationRole = true;
            }
            return View(leaveManagementBo);
        }

        public ActionResult SearchRequesterLeave(LeaveManagementBo leaveManagementBo)
        {
            Utility.SetSession("TmpUserID", leaveManagementBo.EmployeeId);
            if (leaveManagementBo.EmployeeId == 0)
            {
                leaveManagementBo.EmployeeId = Utility.UserId();
            }

            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            return PartialView(leaveManagementRepository.SearchLeaveRequest(leaveManagementBo));
        }

        public ActionResult CreateRequesterLeave(int id, int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo();
            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            if (id != 0)
            {
                leaveManagementBo = _leaveManagementRepository.GetLeaveRequest(id);
            }

            else
            {
                leaveManagementBo.Approver = _leaveManagementRepository.GetApprover(employeeId, "Name");
                leaveManagementBo.ApproverId = Convert.ToInt32(_leaveManagementRepository.GetApprover(employeeId, "ID"));
            }

            if (leaveManagementBo.Doj != null)
            {
                leaveManagementBo.NoOfDays = (DateTime.Now - (leaveManagementBo.Doj ?? DateTime.Now)).Days;
            }

            ViewData["AvailableLeaveList"] = _leaveManagementRepository.GetAvailableLeave(employeeId);

            leaveManagementBo.EmployeeId = employeeId;
            leaveManagementBo.LeaveTypeList = leaveManagementBo.LeaveTypeId > 0 ?
                _leaveManagementRepository.GetLeaveTypeDropDownBasedOnLeaveTypeId(leaveManagementBo.LeaveTypeId) :
                masterDataRepository.SearchMasterDataDropDown("LeaveTypes", string.Empty);

            leaveManagementBo.ApproverList = masterDataRepository.GetEmployeeDropDown("BusinessUnit", employeeId, false);
            leaveManagementBo.ReplacementEmployeeList = masterDataRepository.GetEmployeeDropDown("EmployeeMaster", employeeId, true);
            if (Utility.TmpUserId() > 0)
            {
                leaveManagementBo.EmployeeName = masterDataRepository.GetEmployeeDetails(employeeId).FullName;
            }

            leaveManagementBo.MenuCode = MenuCode.LeaveRequest;
            return PartialView(leaveManagementBo);
        }

        [HttpPost]
        public ActionResult CreateRequesterLeave(LeaveManagementBo leaveManagementBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            UtilityRepository email = new UtilityRepository();

            leaveManagementBo.MenuCode = MenuCode.LeaveRequest;
            string message = string.Empty;

            if (leaveManagementBo.ToDate == null)
            {
                leaveManagementBo.ToDate = leaveManagementBo.FromDate;
            }

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), leaveManagementBo.MenuCode, leaveManagementBo.Id == 0 ? "Save" : "Edit");

            if (leaveManagementBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), leaveManagementBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (leaveManagementBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    message = _leaveManagementRepository.ManageLeaveRequest(leaveManagementBo);
                    if (message.ToLower().Contains("successfully"))
                    {
                        int leaveId = Convert.ToInt32(message.Split(',')[1]);
                        leaveManagementBo = _leaveManagementRepository.GetEmailContent(leaveId);
                        email.SendEmail(leaveManagementBo.ApproverEmail, leaveManagementBo.EmployeeEmail,
                            _leaveManagementRepository.PopulateLeaveMailBody(leaveManagementBo, "Request", string.Empty),
                            "Leave Request : " + leaveManagementBo.EmployeeCode + " ( " + leaveManagementBo.EmployeeName + " )");
                        message = message.Split(',')[0];
                    }

                    break;

                case Operations.Update:
                    message = _leaveManagementRepository.UpdateLeaveRequest(leaveManagementBo);
                    break;

                case Operations.Delete:
                    message = _leaveManagementRepository.ManageLeaveRequest(leaveManagementBo);
                    break;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchRequestedLeaves(DateTime fromDate, DateTime toDate, int employeeId)
        {
            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            return PartialView(_leaveManagementRepository.SearchRequestedLeaves(fromDate, toDate, employeeId, "view"));
        }

        public string CalculateDuration(DateTime fromDate, DateTime toDate, int employeeId)
        {
            string duration = string.Empty;
            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            List<LeaveManagementBo> leaveManagementBoList = _leaveManagementRepository.SearchRequestedLeaves(fromDate, toDate, employeeId);
            if (leaveManagementBoList.Count > 0)
            {
                duration = leaveManagementBoList[0].NoOfDays.ToString();
            }

            return duration;
        }

        public string CheckHolidays(DateTime date, string operation)
        {
            return _leaveManagementRepository.CheckHolidays(date, operation);
        }

        public string CheckAvailableLeave(int leaveTypeId, int? requesterId, int? leaveRequestId)
        {
            if ((requesterId ?? 0) == 0)
            {
                requesterId = Utility.UserId();
            }
            return _leaveManagementRepository.CheckAvailableLeave(leaveTypeId, (int)requesterId, (int)leaveRequestId);
        }

        public ActionResult GetAvailableLeave(int employeeId)
        {
            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }

            return PartialView(_leaveManagementRepository.GetAvailableLeave(employeeId));
        }

        public ActionResult AutoCompleteEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName), JsonRequestBehavior.AllowGet);
        }

        #endregion Leave Request

        #region Leave Approve

        public ActionResult LeaveApprove()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "LEAVEAPPROVAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Leave Approval");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo
            {
                ApproverId = Utility.UserId(),
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave"),
                StatusId = _leaveManagementRepository.GetStatusId("Leave", "Pending"),
                LeaveTypeList = masterDataRepository.SearchMasterDataDropDown("LeaveTypes", string.Empty),
                EmployeeList = masterDataRepository.SearchEmpDropDownForCode(string.Empty, 0)
            };

            return View(leaveManagementBo);
        }

        public ActionResult SearchApproverLeave(LeaveManagementBo leaveManagementBo)
        {
            leaveManagementBo.UserId = Utility.UserId();
            leaveManagementBo.DomainId = Utility.DomainId();

            return PartialView(_leaveManagementRepository.SearchLeaveApproval(leaveManagementBo));
        }

        public ActionResult ApproveLeaveRequest(int id, int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            GetApplicationConfiguration getApplicationConfiguration = new GetApplicationConfiguration();

            LeaveManagementBo leaveManagementBo = _leaveManagementRepository.GetLeaveRequest(id);
            leaveManagementBo.LeaveTypeList = masterDataRepository.SearchMasterDataDropDown("LeaveTypes", string.Empty);
            leaveManagementBo.ApproverList = masterDataRepository.GetEmployeeNameBasedonBu(employeeId);
            leaveManagementBo.ReplacementEmployeeList = masterDataRepository.GetEmployeeDropDown("EmployeeMaster", employeeId, true);
            leaveManagementBo.MenuCode = MenuCode.LeaveApproval;

            //HR Approval Workflow
            leaveManagementBo.IsHrApprovalRequired = getApplicationConfiguration.GetApplicationConfigValue(Config.HRAPPREQ, "", Utility.UserId());
            return PartialView(leaveManagementBo);
        }

        [HttpPost]
        public ActionResult ApproveLeaveRequest(LeaveManagementBo leaveManagementBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            using (TransactionScope transaction = new TransactionScope())
            {
                try
                {
                    leaveManagementBo = ManagerApproval(leaveManagementBo);
                    if (leaveManagementBo.IsHrApprovalRequired == "0" && leaveManagementBo.StatusCode.ToLower() == "approved")
                    {
                        HrApproval(leaveManagementBo);
                    }

                    transaction.Complete();
                }
                catch (Exception)
                {
                    leaveManagementBo.UserMessage = Constants.Default_Error_Message;
                }
            }

            return Json(leaveManagementBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateAssetClearance(LeaveManagementBo leaveManagementBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_leaveManagementRepository.UpdateAssetClearance(leaveManagementBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CompanyAsset(CompanyAssetsBo companyAssetsBo)
        {
            EmployeeMasterRepository _employeeMasterRepository = new EmployeeMasterRepository();
            return PartialView(_employeeMasterRepository.SearchCompanyAssets(companyAssetsBo));
        }

        private LeaveManagementBo ManagerApproval(LeaveManagementBo leaveManagementBo)
        {
            LeaveManagementBo leaveManagementBoOut = new LeaveManagementBo();
            UtilityRepository email = new UtilityRepository();

            leaveManagementBo.ApproverId = Utility.UserId();
            leaveManagementBo.DomainId = Utility.DomainId();

            leaveManagementBoOut.Id = leaveManagementBo.Id;
            leaveManagementBoOut.ApproverId = leaveManagementBo.ApproverId;
            leaveManagementBoOut.DomainId = leaveManagementBo.DomainId;
            leaveManagementBoOut.StatusCode = leaveManagementBo.StatusCode;
            leaveManagementBoOut.IsHrApprovalRequired = leaveManagementBo.IsHrApprovalRequired;
            leaveManagementBoOut.HrRemarks = leaveManagementBo.ApproverRemarks;
            leaveManagementBoOut.Duration = leaveManagementBo.Duration;
            leaveManagementBoOut.EmployeeId = leaveManagementBo.EmployeeId;
            leaveManagementBoOut.LeaveTypeId = leaveManagementBo.LeaveTypeId;
            leaveManagementBoOut.FromDate = leaveManagementBo.FromDate;
            leaveManagementBoOut.IsHalfDay = leaveManagementBo.IsHalfDay;
            leaveManagementBoOut.RequestedDate = leaveManagementBo.RequestedDate;
            leaveManagementBoOut.ReplacementEmployeeID = leaveManagementBo.ReplacementEmployeeID;

            leaveManagementBoOut.UserMessage = _leaveManagementRepository.ApproveLeaveRequest(leaveManagementBo);

            if (leaveManagementBoOut.UserMessage.ToLower().Contains("successfully"))
            {
                string subject = leaveManagementBo.StatusCode.ToLower() == "approved" ? "Leave Approved (Manager):" : "Leave Rejected (Manager):";
                string statusType = "Manager" + leaveManagementBo.StatusCode;
                leaveManagementBo = _leaveManagementRepository.GetEmailContent(leaveManagementBo.Id);
                email.SendEmail(leaveManagementBo.EmployeeEmail, leaveManagementBo.ApproverEmail + ";" + leaveManagementBo.HrEmail,
                    _leaveManagementRepository.PopulateLeaveMailBody(leaveManagementBo, statusType, leaveManagementBo.ApproverRemarks),
                    subject + " " + leaveManagementBo.ApproverName + " ( " + leaveManagementBo.Approver + " )");
            }

            return leaveManagementBoOut;
        }

        public ActionResult SearchEmployeeLeaveList(int employeeId)
        {
            return PartialView(_leaveManagementRepository.SearchEmployeeLeaveRequest(employeeId));
        }

        #endregion Leave Approve

        #region HR Approval

        public ActionResult HrApproval(string menuCode)
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "HRAPPROVAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Leave Approval - HR");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo
            {
                ApproverId = Utility.UserId(),
                StatusId = _leaveManagementRepository.GetStatusId("Leave", "Pending"),
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave"),
                LeaveTypeList = masterDataRepository.SearchMasterDataDropDown("LeaveTypes", string.Empty)
            };

            return View(leaveManagementBo);
        }

        public ActionResult SearchApprovedLeave(LeaveManagementBo leaveManagementBo)
        {
            leaveManagementBo.DomainId = Utility.DomainId();
            leaveManagementBo.UserId = Utility.UserId();

            return PartialView(_leaveManagementRepository.SearchApprovedLeave(leaveManagementBo));
        }

        public ActionResult ApprovedLeaveDetails(int id, int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            GetApplicationConfiguration getApplicationConfiguration = new GetApplicationConfiguration();

            LeaveManagementBo leaveManagementBo = _leaveManagementRepository.GetLeaveRequest(id);

            leaveManagementBo.LeaveTypeList = masterDataRepository.SearchMasterDataDropDown("LeaveTypes", string.Empty);
            leaveManagementBo.ApproverList = masterDataRepository.SearchEmpDropDownForCode(string.Empty, 0);
            leaveManagementBo.ReplacementEmployeeList = masterDataRepository.GetEmployeeDropDown("EmployeeMaster", employeeId, true);
            leaveManagementBo.MenuCode = MenuCode.HRApproval;

            //HR Approval Workflow
            leaveManagementBo.IsHrApprovalRequired = getApplicationConfiguration.GetApplicationConfigValue(Config.HRAPPREQ, "", Utility.UserId());
            return PartialView(leaveManagementBo);
        }

        [HttpPost]
        public ActionResult ApprovedLeaveDetails(LeaveManagementBo leaveManagementBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            leaveManagementBo.MenuCode = MenuCode.HRApproval;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), leaveManagementBo.MenuCode, "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            string message;
            using (TransactionScope transaction = new TransactionScope())
            {
                try
                {
                    message = HrApproval(leaveManagementBo);
                    transaction.Complete();
                }
                catch (Exception)
                {
                    message = Constants.Default_Error_Message;
                }
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        private string HrApproval(LeaveManagementBo leaveManagementBo)
        {
            UtilityRepository email = new UtilityRepository();
            leaveManagementBo.UserId = Utility.UserId();
            leaveManagementBo.DomainId = Utility.DomainId();
            leaveManagementBo.StatusId = _leaveManagementRepository.GetStatusId("Leave", leaveManagementBo.StatusCode);
            leaveManagementBo.HrApproverId = Utility.UserId();

            string message = _leaveManagementRepository.UpdateHrStatus(leaveManagementBo);
            if (message.ToLower().Contains("successfully"))
            {
                string subject = leaveManagementBo.StatusCode.ToLower() == "approved" ? "Leave Approved (HR):" : "Leave Rejected (HR):";
                string statusType = "HR" + leaveManagementBo.StatusCode;
                leaveManagementBo = _leaveManagementRepository.GetEmailContent(leaveManagementBo.Id);
                email.SendEmail(leaveManagementBo.EmployeeEmail, leaveManagementBo.HrEmail,
                    _leaveManagementRepository.PopulateLeaveMailBody(leaveManagementBo, statusType, leaveManagementBo.HrRemarks),
                    subject + " " + leaveManagementBo.HrLoginCode + " ( " + leaveManagementBo.HrName + " )");
            }

            return message;
        }

        #endregion HR Approval

        #region Leave Summary

        public ActionResult SearchProcessLop()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "LEAVESUMMARY");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Leave Summary");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo
            {
                MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty),
                YearList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty),
                RegionList = masterDataRepository.GetBusinessUnitDropDown(),
                MonthId = DateTime.Now.Month,
                YearId = masterDataRepository.GetCurrentYearId(DateTime.Now.Year.ToString())
            };

            return View(leaveManagementBo);
        }

        public ActionResult ProcessLopDetails(LeaveManagementBo leaveManagementBo)
        {
            leaveManagementBo.DomainId = Utility.DomainId();
            leaveManagementBo.UserId = Utility.UserId();
            return PartialView(_leaveManagementRepository.GetLopDetails(leaveManagementBo));
        }

        [HttpPost]
        public ActionResult CreateLopDetails(LeaveManagementBo leaveManagementBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }
            return Json(_leaveManagementRepository.CreateLopDetails(leaveManagementBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProcessLopButtonVisibility(LeaveManagementBo leaveManagementBo)
        {
            return Json(_leaveManagementRepository.ProcessLopButtonVisibility(leaveManagementBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Leave Summary

        #region LOP Details

        public ActionResult LopDetails(string menuCode)
        {
            if (menuCode.ToUpper() == "LOPDETAILS")
            {
                Utility.SetSession("MenuRetention", "PAYROLL");
                Utility.SetSession("SubMenuRetention", "LOPUPDATE");
            }
            else
            {
                Utility.SetSession("MenuRetention", "PAYROLL");
                Utility.SetSession("SubMenuRetention", "LOPUPDATE");
            }
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("LOP Update");

            LopDetailsBo lopDetailsBo = new LopDetailsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            lopDetailsBo.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            lopDetailsBo.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            lopDetailsBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            lopDetailsBo.MonthList.RemoveAt(0);
            lopDetailsBo.YearList.RemoveAt(0);
            lopDetailsBo.BusinessUnitList.RemoveAt(0);
            lopDetailsBo.MenuCode = menuCode;
            return View(lopDetailsBo);
        }

        public ActionResult SearchLopDetails(LopDetailsBo lopDetailsBo)
        {
            return PartialView(_leaveManagementRepository.SearchLopDetails(lopDetailsBo));
        }

        [HttpPost]
        public ActionResult ManageLopDetails(List<LopDetailsBo> lopDetailsBoList)
        {
            string menuCode = (lopDetailsBoList != null) ? lopDetailsBoList[0].MenuCode : "";
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), menuCode, "Edit");
            return Json(!hasAccess ? Constants.UnAuthorizedAccess : _leaveManagementRepository.ManageLopDetails(lopDetailsBoList), JsonRequestBehavior.AllowGet);
        }

        #endregion LOP Details

        #region Missed Punches

        #region Request

        public ActionResult SearchMissedPunches()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "MISSEDPUNCHESREQUEST");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Missed Punches - Request");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                EmployeeList = masterDataRepository.GetEmployeeDropDown("EmployeeMaster", Utility.UserId(), true),
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave")
            };
            if (Utility.TmpUserId() > 0)
            {
                missedPunchesBo.EmployeeId = Utility.TmpUserId();
                missedPunchesBo.RequestBy = "Other";
            }
            else
            {
                Utility.SetSession("TmpUserID", null);
                missedPunchesBo.EmployeeId = Utility.UserId();
                missedPunchesBo.RequestBy = "Self";
            }
            if (masterDataRepository.HasOtherUserAccess(Utility.UserId(), "Missed Punches"))
            {
                missedPunchesBo.HasHRApplicationRole = true;
            }

            return View(missedPunchesBo);
        }

        public ActionResult MissedPunchesList(MissedPunchesBo missedPunchesBo)
        {
            Utility.SetSession("TmpUserID", missedPunchesBo.EmployeeId);
            if (missedPunchesBo.EmployeeId == 0)
            {
                missedPunchesBo.EmployeeId = Utility.UserId();
            }
            return PartialView(_leaveManagementRepository.SearchMissedPunchesRequest(missedPunchesBo));
        }

        public ActionResult CreateMissedPunches(int id, int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                PunchTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "PunchType")
            };
            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            if (id != 0)
            {
                missedPunchesBo = _leaveManagementRepository.GetMissedPunchesRequest(id);
                missedPunchesBo.PunchTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "PunchType");
            }
            else
            {
                missedPunchesBo.HrApproverName = _leaveManagementRepository.GetApprover(employeeId, "Name");
                missedPunchesBo.HrApproverId = Convert.ToInt32(_leaveManagementRepository.GetApprover(employeeId, "ID"));
            }

            missedPunchesBo.EmployeeId = employeeId;
            if (Utility.TmpUserId() > 0)
            {
                missedPunchesBo.EmployeeName = masterDataRepository.GetEmployeeDetails(employeeId).FullName;
            }
            return PartialView(missedPunchesBo);
        }

        [HttpPost]
        public ActionResult CreateMissedPunches(MissedPunchesBo missedPunchesBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.MissedPunchesRequest, "Edit");
            return Json(!hasAccess ? Constants.UnAuthorizedAccess :
                _leaveManagementRepository.ManageMissedPunchesRequest(missedPunchesBo),
                JsonRequestBehavior.AllowGet);
        }

        #endregion Request

        #region Approval

        public ActionResult SearchApprovePunches()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "MISSEDPUNCHESAPPROVAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Missed Punches - Approval");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave"),
                StatusId = _leaveManagementRepository.GetStatusId("Leave", "Pending"),
                EmployeeList = masterDataRepository.SearchEmpDropDownForCode(string.Empty, 0)
            };

            return View(missedPunchesBo);
        }

        public ActionResult ApprovePunchesList(MissedPunchesBo missedPunchesBo)
        {
            missedPunchesBo.DomainId = Utility.DomainId();
            missedPunchesBo.HrApproverId = Utility.UserId();
            return PartialView(_leaveManagementRepository.SearchPunchesApproval(missedPunchesBo));
        }

        //Not Used
        //public ActionResult ApproveMissedPunches(int id) //Anbu
        //{
        //    MissedPunchesBo missedPunchesBo = _leaveManagementRepository.GetMissedPunchesRequest(id);
        //    return PartialView(missedPunchesBo);
        //}

        [HttpPost]
        public ActionResult ApproveMissedPunches(int? id, string status, string remarks, int punchTypeId, string punchDate, int employeeId)
        {
            RbsRepository rbsRepository = new RbsRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo { MenuCode = MenuCode.MissedPunchesApprove };

            DateTime hrApprovedOn = DateTime.Now;

            int domainId = Utility.DomainId();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), missedPunchesBo.MenuCode, "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_leaveManagementRepository.MissedPunchesApproval(id, status, remarks, domainId, punchTypeId, Convert.ToDateTime(punchDate), true, hrApprovedOn, employeeId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemarksDetails(int id)
        {
            return Json(_leaveManagementRepository.GetMissedPunchesRequest(id).Remarks, JsonRequestBehavior.AllowGet);
        }

        #endregion Approval

        #endregion Missed Punches

        #region Reports

        public ActionResult Reports()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "REPORTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Leave Report");

            RbsRepository rBsRepository = new RbsRepository();
            return View(rBsRepository.SearchMenuforReportal());
        }

        #region Missed Check-In/Check-Out Punches Report

        public ActionResult SearchMissedCheckInCheckOutReport()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "REPORT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Missed Check-In/Check-Out Report");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty)
            };

            //missedPunchesBo.MonthList.RemoveAt(0);
            missedPunchesBo.YearList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            //missedPunchesBo.YearList.RemoveAt(0);
            missedPunchesBo.StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave");
            missedPunchesBo.StatusId = masterDataRepository.GetStatusId("Leave", "Approved");
            missedPunchesBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            return PartialView(missedPunchesBo);
        }

        public ActionResult MissedCheckInCheckOutReportList(MissedPunchesBo missedPunchesBo)
        {
            return PartialView(_leaveManagementRepository.MissedCheckInCheckOutReportList(missedPunchesBo));
        }

        public ActionResult MissedCheckInCheckOutPopup(MissedPunchesBo missedPunchesBo)
        {
            return PartialView(_leaveManagementRepository.MissedCheckInCheckOutPopup(missedPunchesBo));
        }

        #endregion Missed Check-In/Check-Out Punches Report

        #region Total Hours Clocked Report

        public ActionResult SearchTotalHoursClockedReport()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "REPORT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Total Hours Clocked Report");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty)
            };

            //missedPunchesBo.MonthList.RemoveAt(0);
            missedPunchesBo.YearList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            //missedPunchesBo.YearList.RemoveAt(0);
            missedPunchesBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            return PartialView(missedPunchesBo);
        }

        public ActionResult TotalHoursReportList(MissedPunchesBo missedPunchesBo)
        {
            return PartialView(_leaveManagementRepository.TotalHoursClockedReportList(missedPunchesBo));
        }

        #endregion Total Hours Clocked Report

        #region Complete Leave Report

        public ActionResult SearchCompleteLeaveReport()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "REPORT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Complete Leave Report");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveReportBo leaveReportBo = new LeaveReportBo
            {
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown(),
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave"),
                StatusId = masterDataRepository.GetStatusId("Leave", "Approved")
            };

            return PartialView(leaveReportBo);
        }

        public ActionResult CompleteLeaveReportList(LeaveReportBo leaveReportBo)
        {
            return PartialView(_leaveManagementRepository.CompleteLeaveReportList(leaveReportBo));
        }

        #endregion Complete Leave Report

        #region Late Arrival Report

        public ActionResult SearchLateArrivalReport()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "REPORT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Late Arrival Report");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveReportBo leaveReportBo = new LeaveReportBo
            {
                MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty)
            };

            //leaveReportBo.MonthList.RemoveAt(0);
            leaveReportBo.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            //leaveReportBo.YearList.RemoveAt(0);
            leaveReportBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            leaveReportBo.Status = "Late";
            return PartialView(leaveReportBo);
        }

        public ActionResult LateArrivalReportList(LeaveReportBo leaveReportBo)
        {
            return PartialView(_leaveManagementRepository.LateArrivalReportList(leaveReportBo));
        }

        #endregion Late Arrival Report

        #region Early Departure Report

        public ActionResult SearchEarlyDepartureReport()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "REPORT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Early Departure Report");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveReportBo leaveReportBo = new LeaveReportBo
            {
                MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty)
            };

            //leaveReportBo.MonthList.RemoveAt(0);
            leaveReportBo.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            //leaveReportBo.YearList.RemoveAt(0);
            leaveReportBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            leaveReportBo.Status = "Early";
            return PartialView(leaveReportBo);
        }

        public ActionResult EarlyDepartureReportList(LeaveReportBo leaveReportBo)
        {
            return PartialView(_leaveManagementRepository.EarlyDepartureReportList(leaveReportBo));
        }

        #endregion Early Departure Report

        #region Employee Comp off Report

        public ActionResult EmployeeCompoffReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty)
            };

            //missedPunchesBo.MonthList.RemoveAt(0);
            missedPunchesBo.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            //missedPunchesBo.YearList.RemoveAt(0);
            missedPunchesBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            missedPunchesBo.StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave");
            return PartialView(missedPunchesBo);
        }

        public ActionResult EmployeeCompoffList(MissedPunchesBo missedPunchesBo)
        {
            DataTable dt = _leaveManagementRepository.SearchEmployeeCompOffReport(missedPunchesBo);
            return PartialView(dt);
        }

        #endregion Employee Comp off Report

        #endregion Reports

        #region UploadLeaveDays

        public ActionResult UploadLeaveDays(LeaveManagementBo leaveManagementBo)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            leaveManagementBo.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            leaveManagementBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            leaveManagementBo.LeaveTypeList = masterDataRepository.SearchMasterDataDropDown("LeaveTypes", string.Empty);
            return PartialView(leaveManagementBo);
        }

        [HttpPost]
        public ActionResult ExcelUpload(HttpPostedFileBase file, int businessUnitId, int yearId)
        {
            DataSet ds = new DataSet();
            string message = string.Empty;

            // ReSharper disable once PossibleNullReferenceException
            if (Request.Files != null && Request.Files["file"].ContentLength > 0)
            {
                string fileExtension = Path.GetExtension(Request.Files["file"].FileName);

                try
                {
                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        string fileLocation = Server.MapPath("~/Content/UploadLeaveDays/") + Session["CompanyName"] + "\\LeaveDays";
                        string fileName = Server.MapPath("~/Content/UploadLeaveDays/") + Session["CompanyName"] + "\\LeaveDays\\" + file.FileName;

                        if (!Directory.Exists(fileLocation))
                        {
                            Directory.CreateDirectory(fileLocation);
                        }

                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }

                        file.SaveAs(fileLocation + "\\" + file.FileName);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            file.InputStream.CopyTo(ms);
                            ms.GetBuffer();
                        }

                        Request.Files["file"].SaveAs(fileName);

                        string excelConnectionString = string.Empty;
                        if (fileExtension == ".xls")
                        {
                            excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        }
                        else if (fileExtension == ".xlsx")
                        {
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        }

                        OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                        excelConnection.Open();
                        DataTable dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        if (dt == null)
                        {
                            return null;
                        }

                        string[] excelSheets = new string[dt.Rows.Count];
                        int t = 0;
                        foreach (DataRow row in dt.Rows)
                        {
                            excelSheets[t] = row["TABLE_NAME"].ToString();
                            t++;
                        }

                        OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                        string query = $"Select * from [{excelSheets[0]}]";
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                        {
                            dataAdapter.Fill(ds);
                        }

                        excelConnection.Close();
                    }

                    string conn = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i][0].ToString() != string.Empty)
                        {
                            using (SqlConnection connection = new SqlConnection(conn))
                            {
                                using (SqlCommand sqlCommand = new SqlCommand(Constants.UPLOADLEAVEDAYS, connection))
                                {
                                    sqlCommand.CommandType = CommandType.StoredProcedure;
                                    sqlCommand.Parameters.AddWithValue(DBParam.Input.EmployeeCode, ds.Tables[0].Rows[i][0].ToString());
                                    sqlCommand.Parameters.AddWithValue(DBParam.Input.EmployeeName, ds.Tables[0].Rows[i][1].ToString().Replace(" ", ""));
                                    sqlCommand.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
                                    sqlCommand.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                                    sqlCommand.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
                                    sqlCommand.Parameters.AddWithValue(DBParam.Input.CL, ds.Tables[0].Rows[i][2].ToString());
                                    sqlCommand.Parameters.AddWithValue(DBParam.Input.SL, ds.Tables[0].Rows[i][3].ToString());
                                    sqlCommand.Parameters.AddWithValue(DBParam.Input.PL, ds.Tables[0].Rows[i][4].ToString());
                                    connection.Open();
                                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                                    {
                                        while (reader.Read())
                                        {
                                            message = reader["Output"].ToString();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return Json(BOConstants.Check_Excel, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAvailableLeaveList(LeaveManagementBo leaveManagementBo)
        {
            leaveManagementBo.DomainId = Utility.DomainId();
            leaveManagementBo.UserId = Utility.UserId();
            return PartialView(_leaveManagementRepository.GetAvailableLeaveList(leaveManagementBo));
        }

        public ActionResult UpdateLeave(LeaveManagementBo leaveManagementBo)
        {
            return PartialView(_leaveManagementRepository.SearchLeaveDays(leaveManagementBo));
        }

        [HttpPost]
        public ActionResult ManageLeave(List<LeaveManagementBo> leaveBoList)
        {
            //RBSRepository rbsRepository = new RBSRepository();
            //bool hasAccess = false;
            //hasAccess = rbsRepository.IsAuthorized(Utility.UserID(), MenuCode.LeaveConfiguration, ((leaveBOList.ID == 0) ? "Save" : "Edit"));
            //if (!hasAccess)
            //{
            //    return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            return Json(_leaveManagementRepository.ManageLeaveDays(leaveBoList), JsonRequestBehavior.AllowGet);
            //}
        }

        public ActionResult AutoCompleteEmployeeOnBUChange(string searchName, int buId)
        {
            return Json(_leaveManagementRepository.SearchAutoCompleteEmployeeOnBuChange(searchName, buId), JsonRequestBehavior.AllowGet);
        }


        #endregion UploadLeaveDays

        #region Revert Leave

        public ActionResult RevertLeave()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Revert Leave");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo
            {
                EmployeeList = masterDataRepository.SearchEmpDropDownForCode(string.Empty, 0)
            };

            return PartialView(leaveManagementBo);
        }

        public ActionResult ShowLeaveDetails(LeaveManagementBo leaveManagementBo)
        {
            return PartialView(_leaveManagementRepository.ShowLeaveDetails(leaveManagementBo));
        }

        [HttpPost]
        public ActionResult RevertLeave(LeaveManagementBo leaveManagementBo)
        {
            return Json(_leaveManagementRepository.RevertLeave(leaveManagementBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Revert Leave
    }
}