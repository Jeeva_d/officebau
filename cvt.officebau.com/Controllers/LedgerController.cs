﻿/*Code Review Done Neeed Proper Commands for all the methods and removed some unwanted local variables*/

using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class LedgerController : Controller
    {
        #region Ledger

        public ActionResult Ledger(string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "MASTER");
            Utility.SetSession("SubMenuRetention", "CHARTOFACCOUNTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Ledger");

            LedgerBo ledgerBo = new LedgerBo();
            _ledgerRepository = new LedgerRepository();

            return View(_ledgerRepository.SearchLedger(ledgerBo));
        }

        #endregion Ledger

        #region Auto Complete

        public ActionResult AutoCompleteHsnCode(string hsnCode)
        {
            ProductMasterRepository productMasterRepository = new ProductMasterRepository();
            return Json(productMasterRepository.AutoCompleteHsnCode(hsnCode), JsonRequestBehavior.AllowGet);
        }

        #endregion Auto Complete

        public ActionResult GetHsnCode(int hsnId)
        {
            ProductMasterRepository productMasterRepository = new ProductMasterRepository();
            string result = productMasterRepository.GetGstHsnCode(hsnId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region Variables

        private LedgerRepository _ledgerRepository = new LedgerRepository();

        #endregion Variables

        #region Create Ledger

        public ActionResult CreateLedger(int ledgerId, string menuCode)
        {
            LedgerBo ledgerBo = _ledgerRepository.GetLedger(ledgerId);
            ledgerBo.MenuCode = menuCode;
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            ledgerBo.GroupIdList = masterDataRepository.SearchMasterDataDropDown("GroupLedger");
            return PartialView(ledgerBo);
        }

        [HttpPost]
        public ActionResult CreateLedger(LedgerBo ledgerBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (ledgerBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), ledgerBo.MenuCode, ledgerBo.Id == 0 ? "Save" : "Edit");
            }

            if (ledgerBo != null && ledgerBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), ledgerBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            return Json(_ledgerRepository.ManageLedger(ledgerBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Create Ledger

        #region Add Group

        public ActionResult AddGroup()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult AddGroup(GroupLedgerBo groupLedgerBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (groupLedgerBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), "GROUP", groupLedgerBo.Id == 0 ? "Save" : "Edit");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            GroupLedgerRepository groupLedgerRepository = new GroupLedgerRepository();
            return Json(groupLedgerRepository.ManageGroupLedger(groupLedgerBo));
        }

        #endregion Add Group
    }
}