﻿//using cvt.officebau.com.Utilities;
//using cvt.officebau.com.Services;
//using cvt.officebau.com.ViewModels;
//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Data;
//using System.Data.OleDb;
//using System.Data.SqlClient;
//using System.IO;
//using System.Web;
//using System.Web.Mvc;

//namespace cvt.officebau.com.Controllers
//{
//    [SessionExpire]
//    public class PayrollController : Controller
//    {
//        #region GetPayrollHistory

//        public ActionResult GetPayrollHistory(int employeeId)
//        {
//            return PartialView(_payrollRepository.GetPayrollHistory(employeeId));
//        }

//        #endregion GetPayrollHistory

//        #region Employee Autocomplete with InActive

//        public ActionResult AutoCompleteEmployeeWithInActive(string searchName)
//        {
//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            return Json(masterDataRepository.AutoCompleteEmployeeWithInActive(searchName), JsonRequestBehavior.AllowGet);
//        }

//        #endregion Employee Autocomplete with InActive

//        [HttpPost]
//        public ActionResult SendMailPayroll(string monthName, string yearName, int monthId, int yearId, string businessUnitList, int? department, int? proceed)
//        {
//            string checkPayrollProcessed = _payrollRepository.CheckPayrollProcessed(monthId, yearId, businessUnitList, department);
//            if (!string.IsNullOrEmpty(checkPayrollProcessed))
//            {
//                if (!string.IsNullOrEmpty(checkPayrollProcessed.Remove(0, 1).Split('!')[0]))
//                {
//                    EmailUtility.SendInstantEmail(checkPayrollProcessed.Remove(0, 1).Split('!')[0], _payrollRepository.PopulateMailBody(monthName, yearName, monthId, yearId, businessUnitList, department),
//                        "OfficeBAU Payroll Details for Month of " + monthName + " " + yearName, checkPayrollProcessed.Remove(0, 1).Split('!')[1], string.Empty);

//                    return Json("YES", JsonRequestBehavior.AllowGet);
//                }

//                return Json("NO", JsonRequestBehavior.AllowGet);
//            }

//            return Json("NO", JsonRequestBehavior.AllowGet);
//        }

//        #region Variables

//        private readonly PayrollRepository _payrollRepository = new PayrollRepository();
//        private PayrollBO _payrollBo = new PayrollBO();

//        #endregion Variables

//        #region Payroll

//        public ActionResult EmployeePayroll()
//        {
//            Utility.SetSession("MenuRetention", "PAYROLL");
//            Utility.SetSession("SubMenuRetention", "RUNPAYROLL");
//             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Run Payroll");

//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            _payrollBo.MonthIDList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
//            _payrollBo.YearIDList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
//            _payrollBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
//            _payrollBo.BusinessUnitList.RemoveAt(0);
//            _payrollBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);
//            return View(_payrollBo);
//        }

//        public ActionResult SearchPayroll(int monthId, int yearId, string businessUnitList, int? department, int? statusId, int? proceed)
//        {
//            List<PayrollBO> payrollList = new List<PayrollBO>();
//            string isLopUpdate = ConfigurationManager.AppSettings["IsLOPUpdate"];
//            //GetApplicationConfiguration getApplicationConfiguration = new GetApplicationConfiguration();
//            //string IsLOPUpdate = getApplicationConfiguration.GetApplicationConfigValue("LOPUPDAT", businessUnitList);

//            string message = _payrollRepository.SearchPayrollForTds(monthId, yearId, businessUnitList, isLopUpdate);
//            Utility.SetSession("IsLOPUpdate", isLopUpdate);

//            if (isLopUpdate != "1")
//            {
//                if (proceed == 1)
//                {
//                    payrollList = _payrollRepository.SearchPayroll(monthId, yearId, businessUnitList, department, statusId, proceed);
//                    return PartialView(payrollList);
//                }

//                if (proceed == 1 || message.ToUpper() == "YES")
//                {
//                    payrollList = _payrollRepository.SearchPayroll(monthId, yearId, businessUnitList, department, statusId, proceed);
//                    return PartialView(payrollList);
//                }

//                return Json("TDS", JsonRequestBehavior.AllowGet);
//            }

//            if (proceed == 0 && message.ToUpper() != "YES")
//            {
//                return Json(message.ToUpper(), JsonRequestBehavior.AllowGet);
//            }

//            if (proceed == 1 || message.ToUpper() == "YES")
//            {
//                payrollList = _payrollRepository.SearchPayroll(monthId, yearId, businessUnitList, department, statusId, proceed);
//            }

//            return PartialView(payrollList);
//        }

//        [HttpPost]
//        public ActionResult SavePayrollList(IList<PayrollBO> payrollBoList)
//        {
//            string message = string.Empty;
//            RbsRepository rbsRepository = new RbsRepository();
//            _payrollBo.MenuCode = MenuCode.RunPayroll;

//            foreach (PayrollBO n in payrollBoList)
//            {
//                bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), _payrollBo.MenuCode, payrollBoList[0].Id == 0 ? "Save" : "Edit");
//                if (!hasAccess)
//                {
//                    return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
//                }

//                message = _payrollRepository.ManagePayroll(n);
//            }

//            return Json(message, JsonRequestBehavior.AllowGet);
//        }

//        public ActionResult SaveIndivdualPayroll(int employeeId, int monthId, int yearId)
//        {
//            return PartialView(_payrollRepository.GetPayroll(employeeId, monthId, yearId));
//        }

//        public ActionResult CalculateEsi(PayrollBO payrollBo)
//        {
//            return Json(_payrollRepository.CalculateEsiIndividual(payrollBo), JsonRequestBehavior.AllowGet);
//        }

//        [HttpPost]
//        public ActionResult SaveIndivdualPayroll(PayrollBO payrollBo)
//        {
//            RbsRepository rbsRepository = new RbsRepository();
//            payrollBo.MenuCode = MenuCode.RunPayroll;
//            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), payrollBo.MenuCode, payrollBo.Id == 0 ? "Save" : "Edit");
//            if (!hasAccess)
//            {
//                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
//            }

//            return Json(_payrollRepository.ManagePayroll(payrollBo), JsonRequestBehavior.AllowGet);
//        }

//        public ActionResult Configuration()
//        {
//            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
//            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
//             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Payroll Configuration");

//            RbsRepository rBsRepository = new RbsRepository();
//            return View(rBsRepository.SearchMenuForConfiguration());
//        }

//        public ActionResult SearchPayrollHistory(int employeeId)
//        {
//            List<PayrollBO> payrollBoList = _payrollRepository.SearchPayrollHistory(employeeId);
//            return PartialView(payrollBoList);
//        }

//        public ActionResult RevertPayroll()
//        {
//            Utility.SetSession("MenuRetention", "PAYROLL");
//            Utility.SetSession("SubMenuRetention", "REVERTPAYROLL");
//             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Revert Payroll");

//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            _payrollBo.MonthIDList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
//            _payrollBo.YearIDList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
//            _payrollBo.EmployeeIDList = masterDataRepository.SearchEmpDropDownForCode(string.Empty, 0);
//            _payrollBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
//            _payrollBo.BusinessUnitList.RemoveAt(0);
//            return View(_payrollBo);
//        }

//        [HttpPost]
//        public ActionResult RevertPayroll(PayrollBO payrollBo)
//        {
//            return Json(_payrollRepository.ManageRevertPayroll(payrollBo), JsonRequestBehavior.AllowGet);
//        }

//        public ActionResult GetPayrollComponentConfigValue(string businessUnitIDs)
//        {
//            return Json(_payrollRepository.GetPayrollComponentConfigValue(businessUnitIDs), JsonRequestBehavior.AllowGet);
//        }

//        #endregion Payroll

//        #region Payroll Configuration

//        public ActionResult PayrollConfiguration()
//        {
//             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Payroll Configuration");

//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            _payrollBo.BusinessUnitList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", string.Empty);
//            return PartialView(_payrollBo);
//        }

//        public ActionResult ManagePayrollConfiguration(int bu)
//        {
//            _payrollBo.DomainId = Utility.DomainId();
//            _payrollBo.BusinessUnitID = bu;
//            _payrollBo = _payrollRepository.GetPayrollConfiguration(_payrollBo);
//            return PartialView(_payrollBo);
//        }

//        [HttpPost]
//        public ActionResult ManagePayrollConfiguration(PayrollBO payrollBo)
//        {
//            payrollBo.DomainId = Utility.DomainId();
//            payrollBo.CreatedBy = Utility.UserId();
//            payrollBo.ModifiedBy = Utility.UserId();
//            return Json(_payrollRepository.ManagePayrollConfiguration(payrollBo), JsonRequestBehavior.AllowGet);
//        }

//        #endregion Payroll Configuration

//        #region Reports

//        public ActionResult PayrollReport()
//        {
//            Utility.SetSession("MenuRetention", "PAYROLL");
//            Utility.SetSession("SubMenuRetention", "REPORTS");
//             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Payroll Report");

//            RbsRepository rBsRepository = new RbsRepository();
//            return View(rBsRepository.SearchMenuforReportal());
//        }

//        public ActionResult MonthlyPayrollRPT()
//        {
//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            _payrollBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
//            _payrollBo.BusinessUnitList.RemoveAt(0);
//            _payrollBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);
//            _payrollBo.FinancialYearList = Utility.GetFinancialYearText();
//            return PartialView(_payrollBo);
//        }

//        public ActionResult SearchMonthlyRpt(string businessUnitList, int? deparmentId, string component, int financialYearId)
//        {
//            return Json(_payrollRepository.Rpt_PayrollMonthly(businessUnitList, deparmentId, component, financialYearId), JsonRequestBehavior.AllowGet);
//        }

//        public ActionResult SearchMonthlyPayrollRPT(string businessUnitList, int? deparmentId, string component, int financialYearId)
//        {
//            DataTable dataTable = _payrollRepository.Rpt_searchemployeepayroll(businessUnitList, deparmentId, component, financialYearId);
//            return PartialView(dataTable);
//        }

//        public ActionResult EmployeeIndividualPayrollRPT()
//        {
//            return PartialView(_payrollBo);
//        }

//        public ActionResult ExcelExport(string monthId, string yearId, string month, string year)
//        {
//            return File(ConfigurationManager.AppSettings["UploadPath"] + Utility.UserId() + "_" + monthId + "_" + yearId + "_payroll.csv", "text/csv", "Payroll_" + month + "-" + year + ".csv");
//        }

//        public ActionResult RPT_SearchPayrollByMonthly()
//        {
//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            _payrollBo.MonthIDList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
//            _payrollBo.YearIDList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
//            _payrollBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
//            _payrollBo.BusinessUnitList.RemoveAt(0);
//            _payrollBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);

//            return PartialView(_payrollBo);
//        }

//        public ActionResult RPT_SearchPayrollByMonthlyView(int monthId, int yearId, string businessUnitList, int? department)
//        {
//            List<PayrollBO> payrollBoList = _payrollRepository.RPT_SearchPayrollByMonthly(monthId, yearId, businessUnitList, department);
//            return PartialView(payrollBoList);
//        }

//        public ActionResult RPT_SearchComponentsByMonthly()
//        {
//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            _payrollBo.MonthIDList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
//            _payrollBo.YearIDList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
//            _payrollBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
//            _payrollBo.BusinessUnitList.RemoveAt(0);
//            _payrollBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);

//            return PartialView(_payrollBo);
//        }

//        public ActionResult RPT_SearchComponentsByMonthlyView(int monthId, int yearId, string businessUnitList, int? department)
//        {
//            List<PayrollBO> payrollBoList = _payrollRepository.RPT_SearchComponentsByMonthly(monthId, yearId, businessUnitList, department);
//            return PartialView(payrollBoList);
//        }

//        #endregion Reports

//        #region Excelupload

//        public ActionResult Excelupload()
//        {
//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            _payrollBo.MonthIDList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
//            _payrollBo.YearIDList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
//            return PartialView(_payrollBo);
//        }

//        [HttpPost]
//        public ActionResult Excelupload(HttpPostedFileBase file, int monthId, int yearId, string type)
//        {
//            DataSet ds = new DataSet();
//            string message = string.Empty;

//            // ReSharper disable once PossibleNullReferenceException
//            if (Request.Files != null && Request.Files["file"].ContentLength > 0)
//            {
//                string fileExtension = Path.GetExtension(Request.Files["file"].FileName);

//                if (fileExtension == ".xls" || fileExtension == ".xlsx")
//                {
//                    string fileLocation;
//                    string fileName;
//                    switch (type.ToUpper())
//                    {
//                        case "PAY":
//                            fileLocation = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\RK";
//                            fileName = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\RK\\" + file.FileName;
//                            break;

//                        default: //LOP & LOPV2
//                            fileLocation = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\LOP";
//                            fileName = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\LOP\\" + file.FileName;
//                            break;
//                    }

//                    if (!Directory.Exists(fileLocation))
//                    {
//                        Directory.CreateDirectory(fileLocation);
//                    }

//                    if (System.IO.File.Exists(fileName))
//                    {
//                        System.IO.File.Delete(fileName);
//                    }

//                    file.SaveAs(fileLocation + "\\" + file.FileName);
//                    using (MemoryStream ms = new MemoryStream())
//                    {
//                        file.InputStream.CopyTo(ms);
//                        ms.GetBuffer();
//                    }

//                    Request.Files["file"].SaveAs(fileName);

//                    string excelConnectionString = string.Empty;
//                    switch (fileExtension)
//                    {
//                        case ".xls":
//                            excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
//                            break;

//                        case ".xlsx":
//                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
//                            break;
//                    }

//                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
//                    excelConnection.Open();
//                    DataTable dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
//                    if (dt == null)
//                    {
//                        return null;
//                    }

//                    string[] excelSheets = new string[dt.Rows.Count];
//                    int t = 0;
//                    foreach (DataRow row in dt.Rows)
//                    {
//                        excelSheets[t] = row["TABLE_NAME"].ToString();
//                        t++;
//                    }

//                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
//                    string query = $"Select * from [{excelSheets[0]}]";
//                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
//                    {
//                        dataAdapter.Fill(ds);
//                    }

//                    excelConnection.Close();
//                }

//                string conn = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
//                //var con = new SqlConnection(conn);
//                //var check = "select 1 from tbl_EmployeePayroll where MonthID=" + monthId + " and YearID=" + yearId + " and IsProcessed=1 and IsDeleted=0";
//                //con.Open();
//                //var cmd1 = new SqlCommand(check, con);
//                //var result = Convert.ToInt32(cmd1.ExecuteScalar());
//                //con.Close();

//                switch (type.ToUpper())
//                {
//                    case "PAY":
//                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
//                        {
//                            if (ds.Tables[0].Rows[i][0].ToString() != string.Empty)
//                            {
//                                using (SqlConnection connection = new SqlConnection(conn))
//                                {
//                                    using (SqlCommand commd = new SqlCommand(Constants.PAYROLLEXCELUPLOAD, connection))
//                                    {
//                                        commd.CommandType = CommandType.StoredProcedure;
//                                        commd.Parameters.AddWithValue(DBParam.Input.EmployeeCode,
//                                            ds.Tables[0].Rows[i][0].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.EmployeeName,
//                                            ds.Tables[0].Rows[i][1].ToString().Replace(" ", ""));
//                                        commd.Parameters.AddWithValue(DBParam.Input.MonthID, monthId);
//                                        commd.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
//                                        commd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
//                                        commd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
//                                        commd.Parameters.AddWithValue(DBParam.Input.TDS,
//                                            ds.Tables[0].Rows[i][3].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.PT,
//                                            ds.Tables[0].Rows[i][4].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.EEESI,
//                                            ds.Tables[0].Rows[i][5].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.OtherDeduction,
//                                            ds.Tables[0].Rows[i][6].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.ERESI,
//                                            ds.Tables[0].Rows[i][7].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.PLI,
//                                            ds.Tables[0].Rows[i][8].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.Mediclaim,
//                                            ds.Tables[0].Rows[i][9].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.MobileDataCard,
//                                            ds.Tables[0].Rows[i][10].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.OtherEarning,
//                                            ds.Tables[0].Rows[i][11].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.OtherPerks,
//                                            ds.Tables[0].Rows[i][12].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.Loans,
//                                            ds.Tables[0].Rows[i][13].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.Bonus,
//                                            ds.Tables[0].Rows[i][14].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.OverTime,
//                                            ds.Tables[0].Rows[i][15].ToString());
//                                        commd.Parameters.AddWithValue(DBParam.Input.MonsoonAllow,
//                                            ds.Tables[0].Rows[i][16].ToString());
//                                        connection.Open();
//                                        using (SqlDataReader reader = commd.ExecuteReader())
//                                        {
//                                            while (reader.Read())
//                                            {
//                                                message = reader["Output"].ToString();
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                        break;

//                    default: // LOP & LOPV2
//                        int monthDayCount = DateTime.DaysInMonth(yearId, monthId);
//                        string spName;
//                        if (type.ToUpper() == "LOPV2")
//                        {
//                            spName = Constants.LOPEXCELUPLOAD_V2;
//                        }
//                        else
//                        {
//                            spName = Constants.LOPEXCELUPLOAD;
//                        }
//                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
//                        {
//                            if (ds.Tables[0].Columns[0].ToString() == "EmpCode")
//                            {
//                                if (Convert.ToDecimal(Convert.ToString(ds.Tables[0].Rows[i][1])) > 0)
//                                {
//                                    if (Convert.ToDecimal(Convert.ToString(ds.Tables[0].Rows[i][1])) <= monthDayCount)
//                                    {
//                                        using (SqlConnection connection = new SqlConnection(conn))
//                                        {
//                                            using (SqlCommand commd = new SqlCommand(spName, connection))
//                                            {
//                                                commd.CommandType = CommandType.StoredProcedure;
//                                                commd.Parameters.AddWithValue(DBParam.Input.EmployeeCode, ds.Tables[0].Rows[i][0].ToString());
//                                                commd.Parameters.AddWithValue(DBParam.Input.EmployeeName, ds.Tables[0].Rows[i][3].ToString().Replace(" ", ""));
//                                                commd.Parameters.AddWithValue(DBParam.Input.MonthID, monthId);
//                                                commd.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
//                                                commd.Parameters.AddWithValue(DBParam.Input.LopDays, ds.Tables[0].Rows[i][1].ToString());
//                                                commd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
//                                                commd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
//                                                connection.Open();
//                                                using (SqlDataReader reader = commd.ExecuteReader())
//                                                {
//                                                    while (reader.Read())
//                                                    {
//                                                        message = reader["Output"].ToString();
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                message = "Not a valid Excel.";
//                            }
//                        }
//                        break;
//                }
//            }

//            return Json(message, JsonRequestBehavior.AllowGet);
//        }

//        #endregion Excelupload

//        #region Employee Autocomplete based on BU

//        public ActionResult AutoCompleteEmployee(string searchName)
//        {
//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName), JsonRequestBehavior.AllowGet);
//        }

//        public ActionResult AutoCompleteAllEmployee(string searchName)
//        {
//            MasterDataRepository masterDataRepository = new MasterDataRepository();

//            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName, 0, "All"), JsonRequestBehavior.AllowGet);
//        }

//        #endregion Employee Autocomplete based on BU
//    }
//}