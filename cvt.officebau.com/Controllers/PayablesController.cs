﻿/*Code Review Done Neeed Proper Commands for all the methods and removed some unwanted local variables need Regions*/

using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class PayablesController : Controller
    {
        #region Search ExpenseDetails

        public ActionResult SearchExpenseDetail(string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "PURCHASE");
            Utility.SetSession("SubMenuRetention", "PAYMENTSMADE");

            SearchParamsBo searchParamsBo = new SearchParamsBo();

            //commonRepository.GetSearchDataHistory(searchParamsBO, "pageload", FormName, Convert.ToInt32(Session["StartMonth"]));

            return View(searchParamsBo);
        }

        #endregion Search ExpenseDetails

        #region Expense Detail

        public ActionResult ExpenseDetail(SearchParamsBo searchParamsBo)
        {
            //if (searchParamsBO.Event == "search")
            //    commonRepository.GetSearchDataHistory(searchParamsBO, "search", FormName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(_payableRepository.SearchExpensePayable(searchParamsBo));
        }

        #endregion Expense Detail

        #region Search IncomeDetail

        public ActionResult SearchIncomeDetail(string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "SALES");
            Utility.SetSession("SubMenuRetention", "PAYMENTSRECEIVED");
            // commonRepository.GetSearchDataHistory(searchParamsBO, "pageload", FormName, Convert.ToInt32(Session["StartMonth"]));
            SearchParamsBo searchParamsBo = new SearchParamsBo();

            return View(searchParamsBo);
        }

        #endregion Search IncomeDetail

        #region Income Detail

        public ActionResult IncomeDetail(SearchParamsBo searchParamsBo)
        {
            //if (searchParamsBO.Event == "search")
            //    commonRepository.GetSearchDataHistory(searchParamsBO, "search", FormName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(_payableRepository.SearchIncomePayable(searchParamsBo));
        }

        #endregion Income Detail

        #region Paid Details

        public ActionResult PaidDetails(ExpenseBo expenseBo)
        {
            return PartialView(_payableRepository.SearchExpensePaidbyVendor(expenseBo));
        }

        #endregion Paid Details

        #region Receivable Details

        public ActionResult ReceivableDetails(IncomeBo incomeBo)
        {
            return PartialView(_payableRepository.SearchReceivablebyCustomer(incomeBo));
        }

        #endregion Receivable Details

        #region Variables

        private PayableRepository _payableRepository = new PayableRepository();

        #endregion Variables

        #region Party Report

        public ActionResult RptParty()
        {
            Session["MenuRetention"] = "REPORTS";
            Session["SubMenuRetention"] = "PARTYREPORT";

            _payableRepository = new PayableRepository();
            return View(_payableRepository.SearchRptParty());
        }

        public ActionResult RptPartyDetails(int vendorId, string screenType)
        {
            _payableRepository = new PayableRepository();
            return PartialView(_payableRepository.Searchrptpartydetails(vendorId, screenType));
        }

        #endregion Party Report
    }
}