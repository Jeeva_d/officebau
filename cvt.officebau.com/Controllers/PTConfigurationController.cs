﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    public class PTConfigurationController : Controller
    {
        #region Variables

        private readonly PayrollRepository _payrollRepository = new PayrollRepository();

        #endregion Variables

        #region PTConfiguration

        public ActionResult ProfessionalTaxConfiguration()
        {
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Professional Tax Configuration");
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            PayrollBO payrollBo = new PayrollBO
            {
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown(),
                FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty)
            };
            UtilityRepository utilityRepository = new UtilityRepository();
            payrollBo.FinancialYearList = utilityRepository.GetFinancialYearText();
            return PartialView(payrollBo);
        }

        public ActionResult ProfessionalTaxConfigurationList(int bu, int fy)
        {
            PayrollBO payrollBo = new PayrollBO
            {
                BusinessUnitID = bu,
                FinancialYearID = fy
            };
            return PartialView(_payrollRepository.SearchPtConfiguration(payrollBo));
        }

        public ActionResult ManageProfessionalTaxConfiguration(int id)
        {
            PayrollBO payrollBo = _payrollRepository.GetPtConfiguration(id);
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            payrollBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            payrollBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            UtilityRepository utilityRepository = new UtilityRepository();
            payrollBo.FinancialYearList = utilityRepository.GetFinancialYearText();
            return PartialView(payrollBo);
        }

        [HttpPost]
        public ActionResult ManageProfessionalTaxConfiguration(PayrollBO payrollBo)
        {
            return Json(_payrollRepository.ManagePtConfiguration(payrollBo), JsonRequestBehavior.AllowGet);
        }

        #endregion PTConfiguration
    }
}