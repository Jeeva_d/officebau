﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeClaimsController : Controller
    {
        #region Variables

        private readonly EmployeeClaimsRepository _employeeClaimsRepository = new EmployeeClaimsRepository();
        UtilityRepository email = new UtilityRepository();
        #endregion Variables

        #region Request

        public ActionResult SearchClaimRequest()
        {
            Utility.SetSession("MenuRetention", "TRAVELANDCLAIMS");
            Utility.SetSession("SubMenuRetention", "CLAIMREQUEST");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Claims Request");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo
            {
                ClaimStatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Claims"),
                EmployeeList = masterDataRepository.GetEmployeeDropDown("EmployeeMaster", Utility.UserId(), true)
            };
            if (Utility.TmpUserId() > 0)
            {
                employeeClaimsBo.EmployeeId = Utility.TmpUserId();
                employeeClaimsBo.RequestBy = "Other";
            }
            else
            {
                Utility.SetSession("TmpUserID", null);
                employeeClaimsBo.EmployeeId = Utility.UserId();
                employeeClaimsBo.RequestBy = "Self";
            }
            if (masterDataRepository.HasOtherUserAccess(Utility.UserId(), "Claim request"))
            {
                employeeClaimsBo.HasHRApplicationRole = true;
            }
            employeeClaimsBo.ApprovarName = _employeeClaimsRepository.SearchApproverDetail(employeeClaimsBo.EmployeeId, "Name");
            employeeClaimsBo.ApprovalId = Convert.ToInt32(_employeeClaimsRepository.SearchApproverDetail(employeeClaimsBo.EmployeeId, "ID"));
            employeeClaimsBo.IsChecked = Convert.ToBoolean(_employeeClaimsRepository.SearchIsClaimChecked(employeeClaimsBo.EmployeeId));
            return View(employeeClaimsBo);
        }

        public ActionResult EmployeeClaimsList(EmployeeClaimsBo employeeClaimsBo)
        {
            Utility.SetSession("TmpUserID", employeeClaimsBo.EmployeeId);
            if (employeeClaimsBo.EmployeeId == 0)
            {
                employeeClaimsBo.EmployeeId = Utility.UserId();
            }
            return PartialView(_employeeClaimsRepository.GetClaimsRequestList(employeeClaimsBo));
        }

        public ActionResult ClaimsRequest(int id, int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();
            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            if (id != 0)
            {
                employeeClaimsBo = _employeeClaimsRepository.GetClaimsRequest(id);
                if (!string.IsNullOrWhiteSpace(employeeClaimsBo.FileUpload))
                {
                    employeeClaimsBo.FileUpload = employeeClaimsBo.FileUpload;
                }
            }
            employeeClaimsBo.EmployeeId = employeeId;
            if (Utility.TmpUserId() > 0)
            {
                employeeClaimsBo.Employee = masterDataRepository.GetEmployeeDetails(employeeId).FullName;
            }
            employeeClaimsBo.DestinationList = masterDataRepository.SearchMasterDataDropDown("DestinationCities", string.Empty);
            employeeClaimsBo.CategoryList = masterDataRepository.SearchMasterDataDropDown("ClaimCategory", string.Empty);

            return PartialView(employeeClaimsBo);
        }

        [HttpPost]
        public ActionResult ClaimsRequest(EmployeeClaimsBo employeeClaimsBo)
        {
            string message = string.Empty;

            employeeClaimsBo.CreatedBy = employeeClaimsBo.EmployeeId;
            employeeClaimsBo.ModifiedBy = employeeClaimsBo.EmployeeId;
            employeeClaimsBo.DomainId = Utility.DomainId();
            employeeClaimsBo.MenuCode = MenuCode.EmployeeClaimRequest;
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeClaimsBo.MenuCode, employeeClaimsBo.Id == 0 ? "Save" : "Edit");

            if (employeeClaimsBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeClaimsBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employeeClaimsBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    message = _employeeClaimsRepository.CreateClaimsRequest(employeeClaimsBo);
                    break;

                case Operations.Update:
                    message = _employeeClaimsRepository.UpdateClaimsRequest(employeeClaimsBo);
                    break;

                case Operations.Delete:
                    employeeClaimsBo.IsDeleted = true;
                    message = _employeeClaimsRepository.DeleteClaimsRequest(employeeClaimsBo);
                    break;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCategoryList()
        {
            return Json(_employeeClaimsRepository.GetCategoryList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubmitClaimRequest(string idList, int approverId, int employeeId)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo
            {
                DomainId = Utility.DomainId()
            };
            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            employeeClaimsBo.EmployeeId = employeeId;
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            

            employeeClaimsBo.ApprovalIdList = masterDataRepository.SearchEmpDropDownForCode(string.Empty, 0);

            string message = _employeeClaimsRepository.SaveClaimsApproval(idList, approverId);
            if (message == "Submitted Successfully")
            {
                employeeClaimsBo = _employeeClaimsRepository.GetClaimMailerDetails(employeeClaimsBo.EmployeeId, approverId);
                email.SendEmail(employeeClaimsBo.ToEmail, employeeClaimsBo.CcEmail,
                    _employeeClaimsRepository.PopulateClaimMailBody(employeeClaimsBo.ReceiverName, idList, employeeClaimsBo.Regards, employeeClaimsBo.PrefixCode),
                    "Claim Requested : " + employeeClaimsBo.LoginCode + " ( " + employeeClaimsBo.Regards + " )");
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Upload(HttpPostedFileBase file)
        {
            List<string> list = new List<string>();
            if (file != null)
            {
                list = UploadUtility.UploadFile(file, Utility.UploadUrl("CLAIMS"));
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteFile(string deleteFile)
        {
            return Json("Deleted successfully", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReSubmitClaims(int id, string amount, int categoryId, int destinationId, int employeeId)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo
            {
                Id = id,
                Amount = Convert.ToDecimal(amount),
                CategoryId = categoryId,
                DestinationId = destinationId,
            };
            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            employeeClaimsBo.EmployeeId = employeeId;
            employeeClaimsBo.ModifiedBy = employeeId;
            employeeClaimsBo.ApprovalId = Convert.ToInt32(_employeeClaimsRepository.SearchApproverDetail(employeeClaimsBo.EmployeeId, "ID"));
            _employeeClaimsRepository.ReSubmitClaimsRequest(employeeClaimsBo);
            return Json(BOConstants.Resubmitted, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckClaimEligibleAmount(EmployeeClaimsBo employeeClaimsBo)
        {
            if(employeeClaimsBo.EmployeeId == 0)
            {
                employeeClaimsBo.EmployeeId = employeeClaimsBo.RequesterId;
            }
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            string configValue = masterDataRepository.GetApplicationConfigValue(Config.CLAIMS_ELIGIBLECHK, "", employeeClaimsBo.EmployeeId);

            if (configValue == "1")
            {
                return Json(_employeeClaimsRepository.CheckClaimEligibleAmount(employeeClaimsBo), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(BOConstants.Claim_Amount_validation, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AutoCompleteTravelReqNo(string searchTravelReqNo, int employeeId)
        {
            return Json(_employeeClaimsRepository.AutoCompleteTravelReqNo(searchTravelReqNo, employeeId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTravelDetails(int travelReqId)
        {
            return PartialView(_employeeClaimsRepository.GetTravelDetails(travelReqId));
        }

        #endregion Request

        #region Approval

        public ActionResult SearchClaimsApproval()
        {
            Utility.SetSession("MenuRetention", "TRAVELANDCLAIMS");
            Utility.SetSession("SubMenuRetention", "CLAIMAPPROVAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Claims Approval");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo
            {
                ClaimStatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Claims"),
                SettlerList = masterDataRepository.SearchEmpDropDownForCode("Finance", 0)
            };

            return View(employeeClaimsBo);
        }

        public ActionResult EmployeeClaimsApproveList(EmployeeClaimsBo employeeClaimsBo)
        {
            return PartialView(_employeeClaimsRepository.GetEmployeeClaimsApproveList(employeeClaimsBo));
        }

        public ActionResult ApprovalEmployeeClaimsList(EmployeeClaimsBo employeeClaimsBo)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            string configValue = masterDataRepository.GetApplicationConfigValue(Config.CLAIMS_ELIGIBLECHK, "", employeeClaimsBo.RequesterId);

            return PartialView(_employeeClaimsRepository.GetApprovalEmployeeClaimsList(employeeClaimsBo, configValue));
        }

        [HttpPost]
        public ActionResult SubmitClaimApproval(int? settlerId, string remarks, string status, IList<EmployeeClaimsBo> list)
        {
            
            EmployeeClaimsBo employeeClaimsBo = _employeeClaimsRepository.ManageClaimsApprovel(settlerId, remarks, status, list);

            int employeeId = list[0].RequesterId;
            string message = employeeClaimsBo.UserMessage;
            if (message.Contains("Successfully"))
            {
                employeeClaimsBo = _employeeClaimsRepository.GetClaimMailerDetails(Utility.UserId(), employeeId);
                email.SendEmail(employeeClaimsBo.ToEmail, employeeClaimsBo.CcEmail,
                    _employeeClaimsRepository.PopulateClaimApproverMailBody(employeeClaimsBo.ReceiverName, list, employeeClaimsBo.Regards, "Manager" + status, employeeClaimsBo.PrefixCode),
                    "Claim " + status + " (Manger): " + employeeClaimsBo.LoginCode + " ( " + employeeClaimsBo.Regards + " )");
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Approval

        #region Financial Approval

        public ActionResult SearchClaimsFinancialApproval()
        {
            Utility.SetSession("MenuRetention", "TRAVELANDCLAIMS");
            Utility.SetSession("SubMenuRetention", "APPROVAL-FINANCE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Claims Approval - Finance");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo
            {
                ClaimStatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Claims"),
                StatusId = masterDataRepository.GetStatusId("Claims", "Submitted")
            };

            return View(employeeClaimsBo);
        }

        public ActionResult EmployeeClaimsFinancialList(EmployeeClaimsBo searchBo)
        {
            return PartialView(_employeeClaimsRepository.GetEmployeeClaimsFinancialApprovalList(searchBo));
        }

        public ActionResult FinancialApprovalEmployeeClaimsList(EmployeeClaimsBo employeeClaimsBo)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            string configValue = masterDataRepository.GetApplicationConfigValue(Config.CLAIMS_ELIGIBLECHK, "", employeeClaimsBo.RequesterId);

            return PartialView(_employeeClaimsRepository.GetFinancialApprovalEmployeeClaimsList(employeeClaimsBo, configValue));
        }

        [HttpPost]
        public ActionResult SubmitClaimFinancialApproval(int? finApproverId, string remarks, string status, IList<EmployeeClaimsBo> list)
        {            
            EmployeeClaimsBo employeeClaimsBo = _employeeClaimsRepository.ManageClaimFinancialApproval(finApproverId, remarks, status, list);

            int employeeId = list[0].RequesterId;
            string message = employeeClaimsBo.UserMessage;
            if (message.Contains("Successfully"))
            {
                

                employeeClaimsBo = _employeeClaimsRepository.GetClaimMailerDetails(Utility.UserId(), employeeId);
                email.SendEmail(employeeClaimsBo.ToEmail, employeeClaimsBo.CcEmail,
                    _employeeClaimsRepository.PopulateClaimApproverMailBody(employeeClaimsBo.ReceiverName, list, employeeClaimsBo.Regards, "Finance" + status, employeeClaimsBo.PrefixCode),
                    "Claim " + status + " (Finance): " + employeeClaimsBo.LoginCode + " ( " + employeeClaimsBo.Regards + " )");
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Financial Approval

        #region Settler

        public ActionResult SearchClaimsSettlement()
        {
            Utility.SetSession("MenuRetention", "TRAVELANDCLAIMS");
            Utility.SetSession("SubMenuRetention", "SETTLEMENT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Claims Settlement");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo
            {
                ClaimStatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Claims"),
                PaymentTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "PaymentType"),
                StatusId = masterDataRepository.GetStatusId("Claims", "Approved")
            };

            return View(employeeClaimsBo);
        }

        public ActionResult EmployeeClaimsSettlerList(EmployeeClaimsBo searchBo)
        {
            return PartialView(_employeeClaimsRepository.GetEmployeeClaimsSettleList(searchBo));
        }

        public ActionResult SettlerEmployeeClaimsList(EmployeeClaimsBo searchBo)
        {
            return PartialView(_employeeClaimsRepository.GetSettlerEmployeeClaimsList(searchBo));
        }

        [HttpPost]
        public ActionResult SubmitClaimSettlement(int paymentTypeId, string remarks, string status, DateTime submittedDate, IList<EmployeeClaimsBo> list)
        {
            

            EmployeeClaimsBo employeeClaimsBo = _employeeClaimsRepository.ManageClaimSettle(paymentTypeId, remarks, status, submittedDate, list);

            int employeeId = list[0].RequesterId;
            string message = employeeClaimsBo.UserMessage;
            if (message.Contains("Successfully"))
            {
                employeeClaimsBo = _employeeClaimsRepository.GetClaimMailerDetails(Utility.UserId(), employeeId);
                email.SendEmail(employeeClaimsBo.ToEmail, employeeClaimsBo.CcEmail,
                    _employeeClaimsRepository.PopulateClaimApproverMailBody(employeeClaimsBo.ReceiverName, list, employeeClaimsBo.Regards, "Settlement" + status, employeeClaimsBo.PrefixCode),
                    "Claim " + status + " (Settlement): " + employeeClaimsBo.LoginCode + " ( " + employeeClaimsBo.Regards + " )");
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Settler

        #region Report

        public ActionResult ClaimsReport()
        {
            Utility.SetSession("MenuRetention", "TRAVELANDCLAIMS");
            Utility.SetSession("SubMenuRetention", "REPORTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Claims Report");

            RbsRepository rBsRepository = new RbsRepository();
            return View(rBsRepository.SearchMenuforReportal());
        }

        public ActionResult CompleteClaimsReport()
        {
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Complete Claims Report");
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeClaimsReportBo employeeClaimsReportBo = new EmployeeClaimsReportBo
            {
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown(),
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Claims"),
                CategoryList = masterDataRepository.SearchMasterDataDropDown("ClaimCategory", string.Empty)
            };

            return PartialView(employeeClaimsReportBo);
        }

        public ActionResult SearchCompleteClaimsReport(EmployeeClaimsReportBo employeeClaimsReportBo)
        {
            if (employeeClaimsReportBo.FromDate == null)
            {
                employeeClaimsReportBo.FromDate = DateTime.Now.AddDays(-180);
            }

            if (employeeClaimsReportBo.ToDate == null)
            {
                employeeClaimsReportBo.ToDate = DateTime.Now;
            }

            return PartialView(_employeeClaimsRepository.GetCompleteClaimsList(employeeClaimsReportBo));
        }

        public ActionResult AutoCompleteEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName), JsonRequestBehavior.AllowGet);
        }
        #endregion Report
    }
}