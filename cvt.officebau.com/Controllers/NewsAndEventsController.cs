﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class NewsAndEventsController : Controller
    {
        #region Varaibles

        private readonly NewsandEventsRepository _rep = new NewsandEventsRepository();

        #endregion

        #region Event Log

        public ActionResult EventLogList()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "NEWSANDEVENTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("News and Events");

            return View(_rep.Searchnewsandeventslist());
        }

        #endregion Event Log

        #region File Upload

        public ActionResult Fileupload(HttpPostedFileBase file, string guid, int totalfile, int currentfile)
        {
            string ServerSavePath;
            string newGuid = Guid.NewGuid().ToString().Substring(0, 3).Trim();
            string fileName = Utility.GetUploadFileName(file.FileName.Replace(" ", ""));
            if (file.ContentLength > 0)
            {
                ServerSavePath = AppDomain.CurrentDomain.BaseDirectory + "Images\\"
                                                                       + Utility.GetSession("CompanyName")
                                                                       + "\\" + Utility.UploadUrl("EVENTS");

                UploadUtility.CheckForFolder(ServerSavePath);
                file.SaveAs(ServerSavePath + fileName);

                using (FSMEntities context = new FSMEntities())
                {
                    tbl_MultipleFileUpload fileupload = new tbl_MultipleFileUpload
                    {
                        ID = new Guid(guid),
                        Path = ServerSavePath,
                        FileName = fileName,
                        OriginalFileName = file.FileName,
                        CreatedBy = Utility.UserId(),
                        CreatedOn = DateTime.Now,
                        ModifiedBy = Utility.UserId(),
                        ModifiedOn = DateTime.Now
                    };
                    context.tbl_MultipleFileUpload.Add(fileupload);
                    context.SaveChanges();
                }
            }

            string returnPath = "..\\Images\\"
                           + Utility.GetSession("CompanyName")
                           + "\\" + Utility.UploadUrl("EVENTS");

            return Json(fileName + '/' + totalfile + '/' + currentfile + '/' + newGuid + '#' + returnPath);
        }

        #endregion File Upload

        #region Get File Names

        public ActionResult GetFileNames(string description, string guidName)
        {
            return Json(_rep.Searchfilepreview(guidName, description), JsonRequestBehavior.AllowGet);
        }

        #endregion Get File Names

        #region Delete Image 

        public ActionResult DeleteImageFile(int id, string fileName, string guid)
        {
            FSMEntities context = new FSMEntities();
            tbl_MultipleFileUpload newsAndEvents = context.tbl_MultipleFileUpload.FirstOrDefault(b => b.RowID == id);
            context.Entry(newsAndEvents).State = EntityState.Deleted;
            context.SaveChanges();

            string serverSavePath = AppDomain.CurrentDomain.BaseDirectory + "Images\\"
                                                                       + Utility.GetSession("CompanyName")
                                                                       + "\\" + Utility.UploadUrl("EVENTS");

            if (System.IO.File.Exists(serverSavePath + '/' + fileName))
            {
                System.IO.File.Delete(serverSavePath + '/' + fileName);
            }

            return Json("");
        }

        #endregion Delete Image 

        #region Create New and Events

        public ActionResult CreateNewsAndEvents(int id)
        {
            return PartialView(_rep.GetNewsAndEvents(id));
        }

        [HttpPost]
        public ActionResult CreateNewsAndEvents(NewsAndEventsBo newsAndEventsBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            newsAndEventsBo.MenuCode = MenuCode.NewsandEvents;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), newsAndEventsBo.MenuCode, newsAndEventsBo.Id == 0 ? "Save" : "Edit");
            if (newsAndEventsBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), newsAndEventsBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            string message = _rep.ManageNewsAndEvents(newsAndEventsBo);
            return Json(message);
        }

        #endregion Create New and Events

        #region Publish News and Events

        public ActionResult NewsandEventPreview(int id)
        {
            return PartialView(_rep.GetNewsAndEvents(id));
        }

        public ActionResult PublishNewEvents(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            NewsAndEventsBo newsAndEventsBo = _rep.GetNewsAndEvents(id);
            newsAndEventsBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            newsAndEventsBo.BusinessUnitList.RemoveAt(0);
            return PartialView(newsAndEventsBo);
        }

        [HttpPost]
        public ActionResult PublishNewEvents(NewsAndEventsBo newsAndEventsBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            newsAndEventsBo.MenuCode = MenuCode.NewsandEvents;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), newsAndEventsBo.MenuCode, "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_rep.PublishNewsAndEvents(newsAndEventsBo));
        }

        #endregion Publish News and Events

        #region Event Viewer

        public ActionResult EventViewer()
        {
            return View(_rep.SearchnewsandeventsViewrslist("NewsandEvents"));
        }

        public ActionResult DetailNewsAndEvents(int id)
        {
            NewsAndEventsBo newsAndEventsBo = _rep.GetNewsAndEvents(id);
            newsAndEventsBo.FileInfarmationList = _rep.FileInformation(newsAndEventsBo.Guid, "MULTI");
            return View(newsAndEventsBo);
        }

        public JsonResult GetDescription(int id)
        {
            string description = _rep.GetDescription(id);
            return Json(description, JsonRequestBehavior.AllowGet);
        }

        #endregion Event Viewer
    }
}
