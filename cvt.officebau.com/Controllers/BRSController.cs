﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class BRSController : Controller
    {
        #region Variables

        private readonly BrsRepository _brsRepository = new BrsRepository();
        private readonly CommonRepository _commonRepository = new CommonRepository();

        private readonly string FormName = "BRS";

        #endregion Variables

        #region BRS Search

        public ActionResult BrsSearch(string menuCode, string operation)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "VOUCHER");
            Utility.SetSession("SubMenuRetention", "BRS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("BRS");

            SearchParamsBo searchParamsBo = new SearchParamsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            searchParamsBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
            searchParamsBo.BankList.RemoveAt(0);
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", FormName, Convert.ToInt32(Session["StartMonth"]));
            searchParamsBo.Operation = operation;

            return PartialView(searchParamsBo);
        }

        public ActionResult BrsSearchDetail(SearchParamsBo searchParamsBo)
        {
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", FormName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(_brsRepository.SearchBrsList(searchParamsBo));
        }

        public ActionResult GetBankAccountNumber(int? id)
        {
            return Json(_brsRepository.GetBankAccountNumber(id), JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteBrs(Brsbo brsBo)
        {
            return Json(_brsRepository.DeleteBRS(brsBo), JsonRequestBehavior.AllowGet);
        }
        #endregion BRS Search


        #region Process BRS

        #region BRS Individual

        [HttpPost]
        public ActionResult ManageBrs(Brsbo brsBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;

            if (brsBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), brsBo.MenuCode, brsBo.Id == 0 ? "Save" : "Edit");
            }

            if (brsBo != null && !brsBo.IsReconciled)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), brsBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            return Json(_brsRepository.ManageBrs(brsBo));
        }

        #endregion BRS Individual

        #region BRS All

        public ActionResult ProcessBrs(Brsbo brsBo)
        {
            return Json(_brsRepository.ManageReconciliation(brsBo), JsonRequestBehavior.AllowGet);
        }

        #endregion BRS All

        #region Search SourceScreen

        public ActionResult SearchSourceScreen(int id, string sourceType)
        {
            Brsbo brsBo = _brsRepository.GetSourceScreen(id, sourceType);
            return Json(brsBo.Id, JsonRequestBehavior.AllowGet);
        }

        #endregion Search SourceScreen

        #endregion Process BRS
    }
}