﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class BankController : Controller
    {
        #region Bank Master

        public ActionResult BankMaster(string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "MASTER");
            Utility.SetSession("SubMenuRetention", "BANKMASTER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Bank Master");

            return View(_bankRepository.SearchBankList(_bankBo));
        }

        #endregion Bank Master

        #region Variables

        private readonly BankRepository _bankRepository = new BankRepository();
        private readonly BankBo _bankBo = new BankBo();

        #endregion Variables

        #region Create Bank

        public ActionResult CreateBank(int bankId, string menuCode)
        {
            BankBo bankBo = _bankRepository.GetBankDetail(bankId);
            bankBo.MenuCode = menuCode;
            CommonRepository commonRepository = new CommonRepository();
            bankBo.AccountTypeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "Account", "Type");
            return PartialView(bankBo);
        }

        [HttpPost]
        public ActionResult CreateBank(BankBo bankBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;

            if (bankBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), bankBo.MenuCode, bankBo.Id == 0 ? "Save" : "Edit");
            }

            if (bankBo != null && bankBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), bankBo.MenuCode, "Delete");
            }

            return Json(!hasAccess ? Constants.UnAuthorizedAccess : _bankRepository.ManageBankDetails(bankBo));
        }

        #endregion Create Bank
    }
}