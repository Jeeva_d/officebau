﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Configuration;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeRecruitmentController : Controller
    {
        #region variable

        private readonly EmployeeMasterRepository _employeeMasterRepository = new EmployeeMasterRepository();
        private string _message = string.Empty;

        #endregion variable

        #region Recruitment

        public ActionResult SearchEmployeeRecruitment()
        {
            Utility.SetSession("MenuRetention", "EMPLOYEEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "ONBOARDREQUEST");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Onboard Request");

            return View();
        }

        public ActionResult EmployeeRecruitment(EmployeeMasterBo employeeMasterBo)
        {
            employeeMasterBo.EmployeeId = Utility.UserId();
            employeeMasterBo.IsActive = true;
            employeeMasterBo.Code = "TMP_";

            return PartialView(_employeeMasterRepository.SearchEmployeeRequestApprovalList(employeeMasterBo));
        }

        public ActionResult CreateNewEmployee(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();
            if (id != 0)
            {
                employeeMasterBo = _employeeMasterRepository.GetEmployeeMaster(id);
                Utility.SetSession("EmployeeName", employeeMasterBo.FirstName);
            }

            // Based on Type
            employeeMasterBo.PrefixList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Prefix");
            employeeMasterBo.GenderList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Gender");
            employeeMasterBo.JobTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Job");

            // Based on table
            employeeMasterBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);
            employeeMasterBo.DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty);
            employeeMasterBo.RegionList = masterDataRepository.GetRegionBasedOnEmployeeAccess(Utility.UserId(), 0, string.Empty);
            employeeMasterBo.GradeList = masterDataRepository.SearchMasterDataDropDown("EmployeeGrade", string.Empty);
            employeeMasterBo.BandList = masterDataRepository.SearchMasterDataDropDown("Band", string.Empty);
            employeeMasterBo.FunctionalList = masterDataRepository.SearchMasterDataDropDown("Functional", string.Empty);
            employeeMasterBo.EmploymentTypeList = masterDataRepository.SearchMasterDataDropDown("EmployementType", string.Empty);
            employeeMasterBo.ContractorList = masterDataRepository.SearchMasterDataDropDown("Contract", string.Empty);
            return PartialView(employeeMasterBo);
        }

        [HttpPost]
        public ActionResult CreateNewEmployee(EmployeeMasterBo employeeMasterBo)
        {
            switch (employeeMasterBo.MenuCode)
            {
                case "APP":
                    employeeMasterBo.MenuCode = MenuCode.Onboard_Approval_HR;
                    break;

                case "UH":
                    employeeMasterBo.MenuCode = MenuCode.Onboard_Approval_UH;
                    break;

                case "OB":
                    employeeMasterBo.MenuCode = MenuCode.Onboard;
                    break;
            }

            employeeMasterBo.HasAccess = true;
            employeeMasterBo.IsActive = true;
            RbsRepository rbsRepository = new RbsRepository();

            employeeMasterBo.BusinessUnitIDs = "," + employeeMasterBo.BaseLocationId + ",";

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeMasterBo.MenuCode, employeeMasterBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employeeMasterBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    _message = _employeeMasterRepository.CreateEmployee(employeeMasterBo);
                    Utility.SetSession("EmployeeName", employeeMasterBo.FirstName);
                    break;

                case Operations.Update:
                    _message = _employeeMasterRepository.UpdateEmployee(employeeMasterBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        #endregion Recruitment

        #region HR Approval

        public ActionResult SearchEmployeeApproval()
        {
            Utility.SetSession("MenuRetention", "EMPLOYEEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "ONBOARDAPPROVAL-HR");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Onboard Approval - HR");

            return View();
        }

        public ActionResult EmployeeApproval(EmployeeMasterBo employeeMasterBo)
        {
            employeeMasterBo.EmployeeId = Utility.UserId();
            if (employeeMasterBo.IsApproved == false)
            {
                employeeMasterBo.ApprovedBy = Utility.UserId();
                employeeMasterBo.Code = null;
                employeeMasterBo.IsApproved = null;
            }
            else
            {
                employeeMasterBo.Code = "TMP_";
            }

            return PartialView(_employeeMasterRepository.SearchEmployeeRequestApprovalList(employeeMasterBo));
        }

        public ActionResult ApproveNewEmployee(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            GetApplicationConfiguration getApplicationConfiguration = new GetApplicationConfiguration();
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();
            if (id != 0)
            {
                employeeMasterBo = _employeeMasterRepository.GetEmployeePayStructure(id);
                Utility.SetSession("EmployeeName", employeeMasterBo.FirstName);
            }
            else
            {
                employeeMasterBo.Code = masterDataRepository.AutoGenerateEmployeeCode();
            }

            // Based on Type
            employeeMasterBo.PrefixList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Prefix");
            employeeMasterBo.GenderList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Gender");
            employeeMasterBo.JobTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Job");

            // Based on table
            employeeMasterBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);
            employeeMasterBo.DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty);
            employeeMasterBo.RegionList = masterDataRepository.GetRegionBasedOnEmployeeAccess(Utility.UserId(), 0, string.Empty);
            employeeMasterBo.BusinessUnitList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", "Region");
            employeeMasterBo.GradeList = masterDataRepository.SearchMasterDataDropDown("EmployeeGrade", string.Empty);
            employeeMasterBo.BandList = masterDataRepository.SearchMasterDataDropDown("Band", string.Empty);
            employeeMasterBo.FunctionalList = masterDataRepository.SearchMasterDataDropDown("Functional", string.Empty);
            employeeMasterBo.EmploymentTypeList = masterDataRepository.SearchMasterDataDropDown("EmployementType", string.Empty);
            employeeMasterBo.ContractorList = masterDataRepository.SearchMasterDataDropDown("Contract", string.Empty);

            //UH Approval Workflow
            employeeMasterBo.IsUhApprovalRequired = getApplicationConfiguration.GetApplicationConfigValue(Config.UHAPPREQ, employeeMasterBo.BaseLocationId == 0 ? "" : Convert.ToString(employeeMasterBo.BaseLocationId), Utility.UserId());
            return PartialView(employeeMasterBo);
        }

        [HttpPost]
        public ActionResult ApproveNewEmployee(EmployeeMasterBo employeeMasterBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.Onboard_Approval_HR, employeeMasterBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employeeMasterBo.Operation.ToUpper())
            {
                case Operations.Update:
                    if (employeeMasterBo.IsUhApprovalRequired == "0")
                    {
                        employeeMasterBo.ApprovedBy = Utility.UserId();
                        employeeMasterBo.ApprovedOn = DateTime.Now;
                        employeeMasterBo.SecondApproverRemarks = employeeMasterBo.ApproverRemarks;
                        employeeMasterBo = UnitHeadApproval(employeeMasterBo, masterDataRepository);
                    }
                    else
                    {
                        employeeMasterBo = HrApproval(employeeMasterBo);
                    }

                    break;
            }

            return Json(employeeMasterBo.UserMessage + "/" + employeeMasterBo.Code, JsonRequestBehavior.AllowGet);
        }

        private EmployeeMasterBo HrApproval(EmployeeMasterBo employeeMasterBo)
        {
            employeeMasterBo.IsActive = true;
            employeeMasterBo.HasAccess = true;
            employeeMasterBo.ApprovedBy = Utility.UserId();
            employeeMasterBo.ApprovedOn = DateTime.Now;
            employeeMasterBo.Doc = employeeMasterBo.DateOFJoin.AddDays(180);

            employeeMasterBo.BusinessUnitIDs = "," + employeeMasterBo.BaseLocationId + ",";
            employeeMasterBo.UserMessage = _employeeMasterRepository.UpdateEmployee(employeeMasterBo);
            return employeeMasterBo;
        }

        public JsonResult GetBusinessUnitDependOnRegion(int? regionId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            System.Collections.Generic.List<SelectListItem> regionList = masterDataRepository.GetRegionBasedOnEmployeeAccess(Utility.UserId(), regionId, string.Empty);
            regionList.RemoveAt(0);
            return Json(regionList, JsonRequestBehavior.AllowGet);
        }

        #endregion HR Approval

        #region Unit Head Approval

        public ActionResult SearchEmployeeApprovalByUnitHead(string menuCode)
        {
            Utility.SetSession("MenuRetention", "EMPLOYEEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "ONBOARDAPPROVAL-UH");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Onboard Approval - UH");

            return View();
        }

        public ActionResult EmployeeApprovalByUnitHead(EmployeeMasterBo employeeMasterBo)
        {
            employeeMasterBo.EmployeeId = Utility.UserId();
            if (employeeMasterBo.IsApproved == false)
            {
                employeeMasterBo.ApprovedBy = Utility.UserId();
                employeeMasterBo.Code = null;
                employeeMasterBo.IsApproved = null;
            }
            else
            {
                employeeMasterBo.Code = "TMP_";
            }

            return PartialView(_employeeMasterRepository.SearchEmployeeUhApprovalList(employeeMasterBo));
        }

        public ActionResult ApproveNewEmployeeByUnitHead(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();
            if (id != 0)
            {
                employeeMasterBo = _employeeMasterRepository.GetEmployeePayStructure(id);
                Utility.SetSession("EmployeeName", employeeMasterBo.FirstName);
            }
            else
            {
                employeeMasterBo.Code = masterDataRepository.AutoGenerateEmployeeCode();
            }

            // Based on Type
            employeeMasterBo.PrefixList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Prefix");
            employeeMasterBo.GenderList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Gender");
            employeeMasterBo.JobTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Job");

            // Based on table
            employeeMasterBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);
            employeeMasterBo.DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty);
            employeeMasterBo.RegionList = masterDataRepository.GetRegionBasedOnEmployeeAccess(Utility.UserId(), 0, string.Empty);
            employeeMasterBo.BusinessUnitList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", "Region");
            employeeMasterBo.GradeList = masterDataRepository.SearchMasterDataDropDown("EmployeeGrade", string.Empty);
            employeeMasterBo.BandList = masterDataRepository.SearchMasterDataDropDown("Band", string.Empty);
            employeeMasterBo.FunctionalList = masterDataRepository.SearchMasterDataDropDown("Functional", string.Empty);
            employeeMasterBo.EmploymentTypeList = masterDataRepository.SearchMasterDataDropDown("EmployementType", string.Empty);
            employeeMasterBo.ContractorList = masterDataRepository.SearchMasterDataDropDown("Contract", string.Empty);
            return PartialView(employeeMasterBo);
        }

        [HttpPost]
        public ActionResult ApproveNewEmployeeByUnitHead(EmployeeMasterBo employeeMasterBo)
        {
            employeeMasterBo.ModifiedBy = Utility.UserId();
            employeeMasterBo.DomainId = Utility.DomainId();
            employeeMasterBo.MenuCode = MenuCode.Onboard_Approval_UnitHead;
            RbsRepository rbsRepository = new RbsRepository();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeMasterBo.MenuCode, employeeMasterBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employeeMasterBo.Operation.ToUpper())
            {
                case Operations.Update:
                    employeeMasterBo = UnitHeadApproval(employeeMasterBo, masterDataRepository);
                    break;
            }

            return Json(employeeMasterBo.UserMessage + "/" + employeeMasterBo.Code, JsonRequestBehavior.AllowGet);
        }

        private EmployeeMasterBo UnitHeadApproval(EmployeeMasterBo employeeMasterBo, MasterDataRepository masterDataRepository)
        {
            employeeMasterBo.IsActive = false;
            employeeMasterBo.HasAccess = true;
            employeeMasterBo.SecondApprovedBy = Utility.UserId();
            employeeMasterBo.SecondApprovedOn = DateTime.Now;
            employeeMasterBo.Doc = employeeMasterBo.DateOFJoin.AddDays(180);

            if (string.IsNullOrWhiteSpace(employeeMasterBo.LoginCode))
            {
                employeeMasterBo.Code = masterDataRepository.GenerateEmployeeCode(employeeMasterBo.BaseLocationId);
                bool chkDupLoginCode = "1" == _employeeMasterRepository.EmployeeLoginCodeCheck(employeeMasterBo.EmailId, "EmployeeMaster", employeeMasterBo.Id);

                if (chkDupLoginCode)
                {
                    employeeMasterBo.UserMessage = "Email ID Already Exists.";
                    return employeeMasterBo;
                }

                employeeMasterBo.LoginCode = employeeMasterBo.EmailId;
            }

            employeeMasterBo.BusinessUnitIDs = "," + employeeMasterBo.BaseLocationId + ",";
            employeeMasterBo.UserMessage = _employeeMasterRepository.UpdateEmployee(employeeMasterBo);
            if (employeeMasterBo.UserMessage.ToLower().Contains("successfully"))
            {
                if (employeeMasterBo.SecondApprovedBy != null)
                {
                    _employeeMasterRepository.ManageMenu(employeeMasterBo);
                }

                UtilityRepository email = new UtilityRepository();

                EmployeeMasterBo emailContent = _employeeMasterRepository.GetEmailContent(Utility.UserId());
                string defaultPassword = ConfigurationManager.AppSettings["DefaultPassword"];
                string url = ConfigurationManager.AppSettings["URL"];
                string mailSubject = "Welcome Aboard !";

                email.SendEmail(employeeMasterBo.EmailId, emailContent.EmailId, _employeeMasterRepository.PopulateOnBoardApprovalMailBody(employeeMasterBo.FirstName + " " + (employeeMasterBo.LastName ?? ""), emailContent.Name, employeeMasterBo.LoginCode, defaultPassword, url), mailSubject);
            }

            return employeeMasterBo;
        }

        #endregion Unit Head Approval
    }
}