﻿using cvt.officebau.com.Services;
using cvt.officebau.com.Utilities;
using System.Web.Mvc;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeBillingInvoiceController : Controller
    {
        private readonly MasterDataRepository masterDataRepository;
        private readonly UtilityRepository track;
        private readonly EmployeeInvoiceRepository employeeInvoiceRepository;


        public EmployeeBillingInvoiceController()
        {
            this.masterDataRepository = new MasterDataRepository();
            this.track = new UtilityRepository();
            employeeInvoiceRepository = new EmployeeInvoiceRepository();
        }

        /// <summary>
        /// Landing page of the invoice of the customer
        /// </summary>
        /// <returns>A view result with EmployeeInvoice BO</returns>
        public ActionResult SearchEmployeeBillingInvoice()
        {
            Utility.SetSession("MenuRetention", "PAYROLL");
            Utility.SetSession("SubMenuRetention", "GENERATEINVOICE");
            track.TrackFormNavigation("Generate Invoice");
            EmployeeInvoice employeeInvoice = new EmployeeInvoice()
            {
                MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty),
                YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty)
            };
            employeeInvoice.YearList = RearrangeList(employeeInvoice.YearList);
            return View(employeeInvoice);
        }

        /// <summary>
        /// To get the searched invoice list
        /// </summary>
        /// <param name="monthId">Month of the invoice </param>
        /// <param name="year">Year of the invoice</param>
        /// <param name="CustomerName">Customer of the invoice</param>
        /// <returns></returns>
        public ActionResult EmployeeInvoiceList(int? monthId, int? year, string CustomerName)
        {
            return PartialView(employeeInvoiceRepository.SearchEmployeeBillingInvoice(monthId, year, CustomerName));
        }

        /// <summary>
        /// Create/Edit an invoice 
        /// </summary>
        /// <param name="id">id of the invoice to be edited (0 on create operation)</param>
        /// <returns></returns>
        public ActionResult CreateNewBillingInvoice(int id)
        {
            var employeeInvoice = new EmployeeInvoice();
            if (!id.Equals(0))
            {
                employeeInvoice = employeeInvoiceRepository.GetBilling(id);
                employeeInvoice.DetailList.AddRange(employeeInvoiceRepository.GetBillingEmployeeList(employeeInvoice.MonthId, employeeInvoice.Year, employeeInvoice.CustomerId).DetailList);
            }
            employeeInvoice.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            employeeInvoice.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            employeeInvoice.YearList = RearrangeList(employeeInvoice.YearList);
            return PartialView(employeeInvoice);
        }

        /// <summary>
        /// Create/Edit an invoice
        /// </summary>
        /// <param name="employeeInvoice">employeeInvoice object to create/edit</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateNewBillingInvoice(EmployeeInvoice employeeInvoice)
        {
            return Json(employeeInvoiceRepository.ManageEmployeeInvoice(employeeInvoice));
        }
        public List<SelectListItem> RearrangeList(List<SelectListItem> list)
        {
            foreach (var n in list)
            {
                if (!string.IsNullOrEmpty(n.Value))
                    n.Value = n.Text;
            }
            return list;
        }
        /// <summary>
        /// To get the employee list for the given details
        /// </summary>
        /// <param name="monthid">month of the invoice</param>
        /// <param name="year">year of the invoice</param>
        /// <param name="customerId">customer of the invoice</param>
        /// <returns></returns>
        public ActionResult GetBillingEmployeeList(int monthid, int year, int customerId)
        {
            return PartialView(employeeInvoiceRepository.GetBillingEmployeeList(monthid, year, customerId));
        }

        public ActionResult PrintInvoice()
        {
            return View();
        }
    }
}