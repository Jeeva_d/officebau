﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class PayrollDashboardController : Controller
    {
        #region Main View

        public ActionResult PayrollDashboard()
        {
            Utility.SetSession("MenuRetention", "DASHBOARDANDREPORTS");
            Utility.SetSession("SubMenuRetention", "PAYROLL");
            UtilityRepository track = new UtilityRepository(); track.TrackFormNavigation("Payroll Dashboard");

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            PayrollBO payrollBo = _payrollDashboarRepository.GetLastMonthYear();
            payrollBo.MonthIDList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            payrollBo.YearIDList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            return View(payrollBo);
        }

        #endregion Main View

        #region Payroll Dashboard BU

        public ActionResult PayrollDashboardBusinessUnit(string type, int? monthId, int yearId)
        {
            return Json(_payrollDashboarRepository.GetbuWise(type, monthId, yearId), JsonRequestBehavior.AllowGet);
        }

        #endregion Payroll Dashboard BU

        #region Get Dashboard Tiles

        public JsonResult GetDashboardTiles(int? monthId, int yearId)
        {
            return Json(_payrollDashboarRepository.GetDashboardTiles(monthId, yearId), JsonRequestBehavior.AllowGet);
        }

        #endregion Get Dashboard Tiles

        #region GET Projected NetPay

        public ActionResult GetProjectedNetPay(string bu, string type, int? monthId, int yearId)
        {
            return Json(_payrollDashboarRepository.GetProjectedNetPay(bu, type, monthId, yearId), JsonRequestBehavior.AllowGet);
        }

        #endregion GET Projected NetPay

        #region GET Employee List

        public ActionResult GetEmployeeList(string bu, string type, int? monthId, int yearId)
        {
            return PartialView(_payrollDashboarRepository.GetEmployeeList(bu, type, monthId, yearId));
        }
        public ActionResult GetEmployeeCompare(int orderBy)
        {
            EmployeeInvoiceRepository employeeInvoiceRepository = new EmployeeInvoiceRepository();
            return Json(employeeInvoiceRepository.SearchEmployeeBillingInvoiceDashBoard(null, orderBy), JsonRequestBehavior.AllowGet);
        }
        #endregion GET Employee List

        #region Variables

        private readonly PayrollDashboadRepository _payrollDashboarRepository = new PayrollDashboadRepository();

        #endregion Variables
    }
}