﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class InventoryDashboardController : Controller
    {
        #region Variables

        private readonly InventoryDashboardRepository _inventoryDashboardRepository = new InventoryDashboardRepository();

        #endregion Variables

        #region Inventory Dashboard

        // GET: InventoryDashboard
        public ActionResult InventoryDashboard()
        {
            Utility.SetSession("MenuRetention", "DASHBOARDANDREPORTS");
            Utility.SetSession("SubMenuRetention", "INVENTORY");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Inventory");
            InventoryBO inventoryBo = _inventoryDashboardRepository.GetDashboardTilesDetails();
            return View(inventoryBo);
        }

        public ActionResult InventoryDashboardList(string screenType)
        {
            System.Data.DataTable dt = _inventoryDashboardRepository.SearchInventoryDashboardList(screenType);
            return PartialView(dt);
        }

        #endregion Inventory Dashboard
    }
}