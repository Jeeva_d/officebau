﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Controllers
{
    public class UserLoginController : Controller
    {
        #region SignOut

        public ActionResult SignOut()
        {
            UserLoginBo userLoginBo = new UserLoginBo
            {
                Id = Utility.LoginId()
            };
            Session.Abandon();
            userLoginBo.CompanyName = "SignOut";
            if (Utility.GetSession("ADMIN") != "ADMIN")
            {
                _userLoginRepository.ManageLoginHistory(userLoginBo);
            }

            return null;
        }

        #endregion SignOut

        #region Reset Password

        public ActionResult ResetPassword(UserLoginBo userLoginBo)
        {
            userLoginBo.Guid = Guid.NewGuid().ToString().Substring(0, 6);
            string result = _userLoginRepository.ResetPassword(userLoginBo);

            if (!result.Split('/')[0].Contains("credential"))
            {
                bool isValidEmail = IsValid(result.Split('/')[3]);

                if (isValidEmail)
                {
                    if (result.Split('/')[0] != "Valid")
                    {
                        userLoginBo.UserMessage = result.Split('/')[0];
                        return Json(userLoginBo.UserMessage, JsonRequestBehavior.AllowGet);
                    }

                    userLoginBo.Guid = result.Split('/')[1];
                    userLoginBo.UserMessage = result.Split('/')[0];

                    string email = userLoginBo.UserName;
                    string domainId = result.Split('/')[2];
                    string firstname = result.Split('/')[4];
                    Utility.SetSession("DomainID", domainId);

                    EmailUtility.SendInstantEmail(result.Split('/')[3], _userLoginRepository.PopulateMailBody(firstname, email, userLoginBo.Guid,
                        EncryptionUtility.Encrypt(domainId)), "OfficeBAU User login - Forgot Password", string.Empty, string.Empty);

                    return Json(userLoginBo.UserMessage, JsonRequestBehavior.AllowGet);
                }

                return Json(BOConstants.Email_Invalid, JsonRequestBehavior.AllowGet);
            }

            return Json(BOConstants.User_Invalid, JsonRequestBehavior.AllowGet);
        }

        #endregion Reset Password

        #region Get Menu List

        public ActionResult Menu()
        {
            RbsMenuBo menuBo = new RbsMenuBo
            {
                EmployeeId = Utility.UserId()
            };
            List<RbsMenuBo> rbsMenuBoList = _userLoginRepository.SearchMenu(menuBo.EmployeeId, Utility.GetSession("ModuleName"));
            return PartialView(rbsMenuBoList);
        }

        #endregion Get Menu List

        #region Private methods

        private bool IsValid(string emailAddress)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(emailAddress))
                {
                    MailAddress mailAddress = new MailAddress(emailAddress);
                    return mailAddress.Address == emailAddress;
                }

                return false;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        #endregion Private methods

        #region Variables and Default Methods

        private readonly IAnnouncements _iAnnouncement = new AnnouncementSer();
        private readonly UserLoginRepository _userLoginRepository = new UserLoginRepository();

        public static List<UserLoginBo> LoginHistoryList = new List<UserLoginBo>();

        public ActionResult ErrorView(string errorMsg)
        {
            ViewBag.errorMessage = errorMsg;
            return PartialView();
        }

        #endregion Variables and Default Methods

        #region UserLogin

        public ActionResult UserLogin(string sessionExpiry = "", string deviceId = "")
        {
            Utility.SetSession("deviceId", deviceId);
            UserLoginBo userLoginBo = new UserLoginBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            string adminCredential = ConfigurationManager.AppSettings["AdminCredential"];

            userLoginBo.UserMessage = adminCredential.Split(',')[0];
            userLoginBo.CompanyList = masterDataRepository.SearchMasterDataDropDown("Company", string.Empty);

            ViewBag.SessionExpiry = sessionExpiry.ToUpper().Equals("TRUE") ? "TRUE" : "";
            return View(userLoginBo);
        }

        public JsonResult GetCompanyListForUser(string employeeCode)
        {
            UserLoginBo userLoginBo = new UserLoginBo
            {
                CompanyList = _userLoginRepository.GetCompanyListForUser(employeeCode)
            };
            return Json(userLoginBo.CompanyList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ValidateUserLogin(UserLoginBo userLoginBo)
        {
            string adminCredential = ConfigurationManager.AppSettings["AdminCredential"];

            if (!string.IsNullOrWhiteSpace(userLoginBo.UserName))
            {
                if (userLoginBo.UserName.ToLower() == adminCredential.Split(',')[0].ToLower() && userLoginBo.Password == adminCredential.Split(',')[1])
                {
                    
                    string startScreen = Url.Action("AdminUser", "Admin");

                    Utility.SetSession("UserID", "1000001");
                    Utility.SetSession("FirstName", adminCredential.Split(',')[0].ToUpper());
                    Utility.SetSession("UserName", adminCredential.Split(',')[0].ToUpper());
                    Utility.SetSession("ADMIN", "ADMIN");
                    Utility.SetSession("CompanyName", "FSM");
                    Utility.SetSession("LastLogin", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    Utility.SetSession("Currency", "");
                    Utility.SetSession("ModuleName", "");
                    Utility.SetSession("IsProfile", "FALSE");
                    Utility.SetSession("ColorPalette", "skin-blue");

                    if ((userLoginBo.Gender ?? "").ToUpper() == "FEMALE")
                    {
                        Utility.SetSession("FileName", "..\\Images\\DefaultProfile_female.png");
                    }
                    else
                    {
                        Utility.SetSession("FileName", "..\\Images\\DefaultProfile.png");
                    }

                    return Json("Valid." + startScreen + ".SuperAdmin", JsonRequestBehavior.AllowGet);
                }
            }

            Utility.SetSession("CompanyName", "FSM");
            Utility.SetSession("Currency", "");
            Utility.SetSession("ModuleName", "");
            Utility.SetSession("IsProfile", "FALSE");

            string location = userLoginBo.Location;
            string latitude = string.Empty;

            if (userLoginBo.Latitude != null)
            {
                latitude = userLoginBo.Latitude + ',' + userLoginBo.Longitude;
            }
            Utility.SetSession("Password", userLoginBo.Password);
            if (!string.IsNullOrWhiteSpace(userLoginBo.Password))
            {
                userLoginBo.Password = userLoginBo.Password.Trim();
                userLoginBo.Password = EncryptionUtility.GetHashedValue(userLoginBo.Password);
            }
            else if (!string.IsNullOrWhiteSpace(Utility.GetSession("deviceId")))
            {
                if (userLoginBo.Pin != 0)
                {
                    userLoginBo = _userLoginRepository.GetValidUserForPin(userLoginBo);
                    if (userLoginBo.UserMessage.ToUpper() != "VALID")
                    {
                        return Json(userLoginBo.UserMessage);
                    }
                }
            }
            userLoginBo = _userLoginRepository.ValidateUserLogin(userLoginBo);

            string message = userLoginBo.UserMessage;
            if (message.ToUpper() == "VALID" || message.ToUpper() == "SUPERADMIN")
            {
                EmployeeInvoiceStatic.employeepayroll = null;
                EmployeeInvoiceStatic.Tile1ID = null;
                EmployeeInvoiceStatic.Tile2ID = null;

                string startScreen;
                Utility.SetSession("UserID", userLoginBo.UserId);
                Utility.SetSession("DomainID", userLoginBo.DomainId);
                Utility.SetSession("UserName", userLoginBo.UserName);
                Utility.SetSession("FirstName", userLoginBo.FirstName);
                Utility.SetSession("CompanyName", userLoginBo.CompanyName);
                Utility.SetSession("LastLogin", userLoginBo.LastLogin);
                Utility.SetSession("Currency", userLoginBo.CurrencySymbol);

                string isExcelDownload = ConfigurationManager.AppSettings["IsExcelDownload"];

                Utility.SetSession("IsExcelDownload", isExcelDownload);

                if (!string.IsNullOrWhiteSpace(userLoginBo.Designation))
                {
                    Utility.SetSession("Designation", userLoginBo.Designation);
                }
                else
                {
                    Utility.SetSession("Designation", "Sales");
                }

                if (message.ToUpper() == "SUPERADMIN")
                {                  
                    startScreen = Url.Action("SuperAdmin", "SuperAdmin")+"?menuCode=SUPERADMIN";
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(userLoginBo.MenuUrl))
                    {
                        startScreen = "/" + userLoginBo.MenuUrl;
                    }
                    else
                    {
                        if (Utility.GetSession("FirstName").ToUpper().Trim() == "ADMIN")
                        {
                            startScreen = Url.Action("SearchEmployeeMaster", "EmployeeMaster") +"?menuCode=EMPMAS"; 
                        }
                        else
                        {
                            startScreen = Url.Action("EmployeeProfile", "EmployeeProfile") +"?menuCode=PROFILE"; 
                        }
                    }
                }

                if (userLoginBo.FileId != null && userLoginBo.FileId != new Guid())
                {
                    userLoginBo.FilePath = "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("PROFILE") + userLoginBo.FilePath;
                }
                else
                {
                    if ((userLoginBo.Gender ?? "").ToUpper() == "FEMALE")
                    {
                        userLoginBo.FilePath = "..\\Images\\DefaultProfile_female.png";
                    }
                    else
                    {
                        userLoginBo.FilePath = "..\\Images\\DefaultProfile.png";
                    }
                }

                Utility.SetSession("FileName", userLoginBo.FilePath);
                Utility.SetSession("ADMIN", "");
                userLoginBo.CompanyName = string.Empty;
                userLoginBo.Location = location;
                userLoginBo.Latitude = latitude;
                Utility.SetSession("LoginID", _userLoginRepository.ManageLoginHistory(userLoginBo));

                _iAnnouncement.GetBuildNotification();
                _iAnnouncement.GetStudentDetails();
                message = "Valid";
                return Json(message + "." + startScreen + "." + userLoginBo.IsPasswordChanged, JsonRequestBehavior.AllowGet);
            }

            if (message.ToUpper() == "SUPERADMIN")
            {
              
                string startScreen = Url.Action("SUPERADMIN","UserLogin");
                Utility.SetSession("UserID", userLoginBo.UserId);
                Utility.SetSession("FirstName", userLoginBo.FirstName);
                Utility.SetSession("ADMIN", "SUPERADMIN");
                return Json(message + "." + startScreen, JsonRequestBehavior.AllowGet);
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion UserLogin

        #region Reset Password UsingOTP

        public ActionResult ResetPasswordUsingOtp(string domainId)
        {
            UserLoginBo userLoginBo = new UserLoginBo
            {
                DomainId = Convert.ToInt32(EncryptionUtility.Decrypt(domainId))
            };
            Utility.SetSession("DomainID", userLoginBo.DomainId);
            return View(userLoginBo);
        }

        [HttpPost]
        public ActionResult ResetPasswordUsingOtp(UserLoginBo userLoginBo)
        {
            Utility.SetSession("DomainID", Utility.DomainId());
            userLoginBo = _userLoginRepository.ManagePasswordAudit(userLoginBo);

            if (userLoginBo.UserMessage == "Correct Code")
            {
                Utility.SetSession("UserID", userLoginBo.UserId);
                return Json(userLoginBo.UserMessage, JsonRequestBehavior.AllowGet);
            }

            return Json(userLoginBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion Reset Password UsingOTP

        #region First Login

        public ActionResult FirstLogin(string url)
        {
            ViewBag.url = url;
            return View();
        }

        [HttpPost]
        public ActionResult FirstLogin(UserLoginBo userLoginBo)
        {
            if (Utility.GetSession("Password") != userLoginBo.CompanyName)
            {
                return Json(BOConstants.Old_Password_Match);
            }

            userLoginBo.UserId = Convert.ToInt32(Utility.GetSession("UserID"));
            userLoginBo.ConfirmPassword = EncryptionUtility.GetHashedValue(userLoginBo.ConfirmPassword);
            userLoginBo.UserMessage = _userLoginRepository.ChangePassword(userLoginBo);

            if (userLoginBo.UserMessage.ToUpper() == "VALID")
            {
                _userLoginRepository.ManageLoginHistory(userLoginBo);
                return Json(userLoginBo.UserMessage, JsonRequestBehavior.AllowGet);
            }

            return Json(userLoginBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion First Login

        #region Change Password UsingOTP

        public ActionResult ChangePasswordUsingOtp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePasswordUsingOtp(UserLoginBo userLoginBo)
        {
            userLoginBo.UserId = Convert.ToInt32(Utility.GetSession("UserID"));
            userLoginBo.ConfirmPassword = EncryptionUtility.GetHashedValue(userLoginBo.ConfirmPassword);
            userLoginBo.UserMessage = _userLoginRepository.ChangePassword(userLoginBo);

            if (userLoginBo.UserMessage.ToUpper() == "VALID")
            {
                string startScreen = Url.Action("EmployeeProfile", "EmployeeProfile") +"?menuCode=PROFILE";
                _userLoginRepository.ManageLoginHistory(userLoginBo);
                return Json(userLoginBo.UserMessage + "." + startScreen, JsonRequestBehavior.AllowGet);
            }

            return Json(userLoginBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion Change Password UsingOTP

        #region MPIN

        public ActionResult SetPin()
        {
            UserLoginBo userLoginBo = new UserLoginBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            string adminCredential = ConfigurationManager.AppSettings["AdminCredential"];

            userLoginBo.UserMessage = adminCredential.Split(',')[0];
            userLoginBo.CompanyList = masterDataRepository.SearchMasterDataDropDown("Company", string.Empty);
            return PartialView(userLoginBo);
        }

        [HttpPost]
        public ActionResult SetPin(string userName, string password, int companyId, int pin, string overwriteDevice)
        {
            UserLoginBo userLoginBo = new UserLoginBo
            {
                CompanyId = companyId,
                UserName = userName,
                Password = password.Trim()
            };
            Utility.SetSession("Password", userLoginBo.Password);
            userLoginBo.Password = EncryptionUtility.GetHashedValue(userLoginBo.Password);
            userLoginBo = _userLoginRepository.ValidateUserLogin(userLoginBo);
            if (userLoginBo.UserMessage.ToUpper() == "VALID")
            {
                userLoginBo.Pin = pin;
                userLoginBo.UserMessage = overwriteDevice;
                userLoginBo.UserMessage = _userLoginRepository.SetPinForMobileUser(userLoginBo);
            }
            return Json(userLoginBo.UserMessage);
        }

        #endregion MPIN

        #region ChangePassword

        public ActionResult ChangePassword()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult ChangePassword(string oldPassword, string newPassword)
        {
            UserLoginBo userLoginBo = new UserLoginBo
            {
                UserName = Utility.GetSession("UserName"),
                Password = oldPassword,
                NewPassword = newPassword
            };
            userLoginBo.Password = EncryptionUtility.GetHashedValue(userLoginBo.Password);
            userLoginBo = _userLoginRepository.ValidateOldPassword(userLoginBo);

            if (!string.IsNullOrWhiteSpace(userLoginBo.UserMessage) && userLoginBo.UserMessage.ToUpper() == "VALID")
            {
                userLoginBo.ConfirmPassword = EncryptionUtility.GetHashedValue(newPassword);
                userLoginBo.UserMessage = _userLoginRepository.ChangePassword(userLoginBo);
                return Json("Valid", JsonRequestBehavior.AllowGet);
            }

            return Json(BOConstants.InVaild_Old_Password, JsonRequestBehavior.AllowGet);
        }

        #endregion ChangePassword

        #region LandingPage

        [SessionExpire]
        public ActionResult LandingPage()
        {
            ViewBag.ModuleList = _userLoginRepository.GetCompanyModules();
            return View();
        }

        [HttpPost]
        public ActionResult LandingPage(string module)
        {
            Utility.SetSession("ModuleName", module);
            Utility.SetSession("IsProfile", "FALSE");

            string startUrl = _userLoginRepository.GetStartScreen();
            RbsMenuBo rbsMenuBo = _userLoginRepository.SearchMenu(Utility.UserId(), Utility.GetSession("ModuleName")).FirstOrDefault();

            if (rbsMenuBo != null)
            {
                switch (module.ToUpper())
                {
                    case "CONFIGURATION":
                        if (!rbsMenuBo.MenuUrl.IsNullOrEmpty())
                            startUrl = Url.Action(rbsMenuBo.MenuUrl.Split('/')[1], rbsMenuBo.MenuUrl.Split('/')[0]) + "?menuCode=" + rbsMenuBo.MenuCode;
                        break;

                    case "HRMS":
                        Utility.SetSession("IsProfile", "TRUE");
                        if (string.IsNullOrWhiteSpace(startUrl))
                        {
                            startUrl =Url.Action("EmployeeProfile", "EmployeeProfile") +"?menuCode=PROFILE"; 
                        }
                        break;

                    case "ACCOUNT":
                    case "ACCOUNTS":
                        Utility.SetSession("IsProfile", "TRUE");
                        if (string.IsNullOrWhiteSpace(startUrl))
                        {
                            startUrl = Url.Action("dashboard", "DashBoard") +"?menuCode=ACCDBOARD"; 
                        }
                        break;

                    case "INVENTORY":
                        startUrl = "/" + (rbsMenuBo.MenuUrl ?? "") + "?menuCode=" + (rbsMenuBo.MenuCode ?? "");
                        break;
                }
            }
            else
            {
                startUrl = Url.Action("NotAuthorized", "RBS")+"?menuCode=NOTAUTH";
            }

            return Json(startUrl, JsonRequestBehavior.AllowGet);
        }

        #endregion LandingPage

        #region Login History

        [SessionExpire]
        public ActionResult LoginHistory()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "LOGINHISTORY");
            UtilityRepository track = new UtilityRepository(); track.TrackFormNavigation("Login History");

            UserLoginBo userLoginBo = new UserLoginBo();
            return PartialView(userLoginBo);
        }

        [SessionExpire]
        public ActionResult SearchLoginHistory(DateTime? fromDate, DateTime? toDate)
        {
            LoginHistoryList = _userLoginRepository.SearchLoginHistory(fromDate, toDate);
            return PartialView(LoginHistoryList);
        }

        [SessionExpire]
        public ActionResult SearchLoginFormHistory(int loginHistoryId)
        {
            return PartialView(_userLoginRepository.SearchLoginFormHistory(loginHistoryId));
        }

        #endregion Login History

        #region Announcements

        public ActionResult GetMessageCount()
        {
            return Json(_iAnnouncement.GetStudentDetails(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult BuildActivityCount()
        {
            return Json(_iAnnouncement.GetBuildNotification(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCurrentEvents()
        {
            return Json(_userLoginRepository.SearchCurrentEvents(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendWish(BaseBo baseBo)
        {
            return Json(_userLoginRepository.SendWishes(baseBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult MarkMessageRead(int messageId, int isViewed)
        {
            return Json(_userLoginRepository.MarkMessageRead(messageId, isViewed), JsonRequestBehavior.AllowGet);
        }

        public ActionResult NotificationPopup()
        {
            return PartialView();
        }

        public ActionResult BroadCast(string msg)
        {
            return PartialView(_userLoginRepository.SearchBoardCast(msg));
        }

        #endregion Announcements

        #region RestoreUserSession

        public ActionResult RestoreUserSession()
        {
            return Json(BOConstants.Session_Retained, JsonRequestBehavior.AllowGet);
        }

        #endregion RestoreUserSession
    }
}