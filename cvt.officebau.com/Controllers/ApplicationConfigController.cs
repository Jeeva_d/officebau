﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class ApplicationConfigController : Controller
    {
        #region Variables

        private readonly ApplicationConfigurationRepository _applicationConfigurationRepository = new ApplicationConfigurationRepository();

        #endregion Variables

        #region Application Configuration

        public ActionResult ApplicationConfiguration()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ApplicationConfigBo applicationConfigBo = _applicationConfigurationRepository.GetApplicationAccountConfiguration();
            applicationConfigBo.CurrencyList = masterDataRepository.SearchMasterDataDropDown("Currency");
            applicationConfigBo.StartMonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            return PartialView(applicationConfigBo);
        }

        [HttpPost]
        public JsonResult SaveApplicationConfiguration(ApplicationConfigBo applicationconfigBo)
        {
            List<ApplicationConfigBo> list = new List<ApplicationConfigBo>();
            if (applicationconfigBo != null)
            {
                ApplicationConfigBo applicationConfigBo = new ApplicationConfigBo();
                list.Add(applicationConfigBo);
                if (applicationconfigBo.IsCostCenter)
                {
                    list[0].Value = "1";
                    list[0].Code = "COSTCT";
                }
                else
                {
                    list[0].Value = "0";
                    list[0].Code = "COSTCT";
                }

                applicationConfigBo = new ApplicationConfigBo();
                if (applicationconfigBo.StartMonthName != null)
                {
                    list.Add(applicationConfigBo);
                    list[1].Value = applicationconfigBo.StartMonthName;
                    list[1].Code = "STARTMTH";
                }

                applicationConfigBo = new ApplicationConfigBo();
                if (applicationconfigBo.CurrencyName != null)
                {
                    list.Add(applicationConfigBo);
                    list[2].Value = applicationconfigBo.CurrencyName;
                    list[2].Code = "CURNY";
                }

                applicationConfigBo = new ApplicationConfigBo();
                if (applicationconfigBo.NoOfRecords != null)
                {
                    list.Add(applicationConfigBo);
                    list[3].Value = applicationconfigBo.NoOfRecords;
                    list[3].Code = "NRECORD";
                }
            }

            applicationconfigBo = _applicationConfigurationRepository.ManageApplicationAccountConfiguration(list);
            if (applicationconfigBo.UserMessage.Contains("Successfully"))
            {
                FinancialYearRepository financialYearRepository = new FinancialYearRepository();
                FinancialYearBo financialYearBo = financialYearRepository.GetFnMonthAppConfig();
                Utility.SetSession("StartMonth", financialYearBo.Value);
            }

            return Json(applicationconfigBo.UserMessage);
        }

        public ActionResult SearchCostCenter()
        {
            return Json(_applicationConfigurationRepository.SearchCostCenter(), JsonRequestBehavior.AllowGet);
        }

        #endregion Application Configuration

        #region Email Config

        public ActionResult EmailConfiguration()
        {
            List<ApplicationConfigBo> applicationConfigurationList = _applicationConfigurationRepository.SearchEmailConfig();
            return PartialView(applicationConfigurationList);
        }

        public ActionResult GetEmailConfig(string key)
        {
            return PartialView(_applicationConfigurationRepository.GetEmailConfig(key));
        }

        [HttpPost]
        public ActionResult SaveEmailConfiguration(ApplicationConfigBo applicationConfig)
        {
            return Json(_applicationConfigurationRepository.ManageEmailConfiguration(applicationConfig));
        }

        public ActionResult DescriptionPopup(string key, string type)
        {
            ApplicationConfigBo applicationConfigBo = new ApplicationConfigBo
            {
                Code = key,
                Type = type
            };
            return PartialView(_applicationConfigurationRepository.GetMailInformation(applicationConfigBo));
        }

        #endregion Email Config
    }
}