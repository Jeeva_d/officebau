﻿using cvt.officebau.com.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.Utilities;

namespace cvt.officebau.com.Controllers
{
    public class EventCalendarController : Controller
    {
        // GET: EventCalendar
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GeteventDataForTheGivenMonthAndYear(int yearId, int monthid)
        {

            List<Eventclass> list = new List<Eventclass>();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conn"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SearchcurrenteventsforCalander", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.Month, monthid);
                    cmd.Parameters.AddWithValue(DBParam.Input.EmployeeID, Utility.UserId());
                    con.Open();
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new Eventclass
                            {
                                title = Utility.CheckDbNull<string>(reader["title"]),
                                start = Utility.CheckDbNull<string>(reader["start"]),
                                icon = Utility.CheckDbNull<string>(reader["icon"]),
                                color = Utility.CheckDbNull<string>(reader["color"]),
                                End = Utility.CheckDbNull<string>(reader["End"]),

                            });
                        }
                    }
                }
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

    }

    public class Eventclass
    {
        public string title { get; set; }
        public string start { get; set; }
        public string icon { get; set; }
        public string color { get; set; }
        public string End { get; set; }

    }

}