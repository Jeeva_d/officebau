﻿using System;
using System.Linq;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;
using System.IO;

namespace cvt.officebau.com.Controllers
{
    public class ImportAndExportController : Controller
    {
        #region DownloadFiles

        public FileResult DownloadFile(string fileUploadId)
        {
            FileUpload fileUpload = UploadUtility.DownloadFile(fileUploadId);
            return File(fileUpload.FileName, fileUpload.FileType, fileUpload.OriginalFileName);
        }

        public FileResult DownloadMultipleFiletable(int id)
        {
            FileUpload fileUpload = new FileUpload();
            using (FSMEntities context = new FSMEntities())
            {


                fileUpload = (from e in context.tbl_MultipleFileUpload.Where(e => e.RowID == id)
                              select new FileUpload
                              {
                                  FileName = e.Path + e.FileName,
                                  FileType = e.OriginalFileName
                              }).FirstOrDefault();
            }
            return File(fileUpload.FileName, Path.GetExtension(fileUpload.FileType), fileUpload.FileType);
        }
        #endregion DownloadFiles
    }
}