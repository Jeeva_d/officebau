﻿using cvt.officebau.com.Services;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;

using System;
using System.Data;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    public class Pay_PayrollReportsController : Controller
    {
        private readonly PayPayrollReportsRepository _payrollReportsRepository = new PayPayrollReportsRepository();

        public ActionResult Index()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "PAYROLL");
            UtilityRepository track = new UtilityRepository(); track.TrackFormNavigation("Salary Component");

            Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            payrollReportBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            payrollReportBo.BusinessUnitList.RemoveAt(0);
            payrollReportBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);
            payrollReportBo.ComponentList = masterDataRepository.SearchMasterDataDropDown("Pay_PayrollCompontents", string.Empty);
            payrollReportBo.ComponentList.RemoveAt(0);
            UtilityRepository utilityRepository = new UtilityRepository();
            payrollReportBo.FinancialYearList = utilityRepository.GetFinancialYearText();
            return PartialView(payrollReportBo);
        }

        public ActionResult Pay_SearchMonthlyRPT(string businessUnitList, int? deparmentId, int? componentId, int financialYearId)
        {
            return Json(_payrollReportsRepository.Pay_Rpt_PayrollMonthly(businessUnitList, deparmentId, componentId, financialYearId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Pay_SearchMonthlyPayrollRPT(string businessUnitList, int? deparmentId, int? componentId, int financialYearId)
        {
            DataTable dataTable = _payrollReportsRepository.Pay_Report_SearchEmployeePayroll(businessUnitList, deparmentId, componentId, financialYearId);
            return PartialView(dataTable);
        }

        public ActionResult Pay_RPT_SearchPayrollByMonthly()
        {
            Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            payrollReportBo.MonthIdList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            payrollReportBo.YearIdList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            payrollReportBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            payrollReportBo.BusinessUnitList.RemoveAt(0);
            payrollReportBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);
            return PartialView(payrollReportBo);
        }

        public ActionResult Pay_RPT_SearchPayrollByMonthlyView(int monthId, int yearId, string businessUnitList, int? departmentId)
        {
            DataTable dataTable = _payrollReportsRepository.Pay_RPT_SearchPayrollByMonthly(monthId, yearId, businessUnitList, departmentId);
            return PartialView(dataTable);
        }

        public ActionResult Pay_DetailedReport()
        {
            Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            payrollReportBo.MonthIdList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            payrollReportBo.YearIdList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            return PartialView(payrollReportBo);
        }

        public ActionResult Pay_DetailedReportView(int monthId, int yearId)
        {
            DataTable dataTable = _payrollReportsRepository.Pay_RPT_SearchPayrollDetailReport(monthId, yearId);
            return PartialView(dataTable);
        }

        public ActionResult Pay_BankdReport()
        {
            Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            payrollReportBo.MonthIdList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            payrollReportBo.YearIdList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            return PartialView(payrollReportBo);
        }

        public ActionResult Pay_BankdReportView(int monthId, int yearId, int year)
        {
            DataTable dataTable = _payrollReportsRepository.Pay_RPT_SearchPayrollBankDetailReport(monthId, yearId, year);
            return PartialView(dataTable);
        }

        public ActionResult Pay_EmployeeIndividualPayrollRPT()
        {
            Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO();
            return PartialView(payrollReportBo);
        }

        public ActionResult Pay_GetPayrollHistory(int employeeId)
        {
            DataTable dataTable = _payrollReportsRepository.Pay_GetPayrollHistory(employeeId);
            return PartialView(dataTable);
        }

        public ActionResult AutoCompleteAllEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName, 0, "All"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Pay_RPT_SearchComponentsByMonthly()
        {
            Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            payrollReportBo.MonthIdList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            payrollReportBo.YearIdList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            payrollReportBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            payrollReportBo.BusinessUnitList.RemoveAt(0);
            payrollReportBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);
            return PartialView(payrollReportBo);
        }

        public ActionResult Pay_RPT_SearchComponentsByMonthlyView(int monthId, int yearId, string businessUnitList, int? departmentId)
        {
            DataTable dataTable = _payrollReportsRepository.Pay_RPT_SearchComponentsByMonthly(monthId, yearId, businessUnitList, departmentId);
            return PartialView(dataTable);
        }

        public ActionResult Pay_UnmappedEmployeePaystructureRPT()
        {
            Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO();
            return PartialView(payrollReportBo);
        }

        public ActionResult Pay_GetUnmappedPaystructure(int? employeeId, bool isActive)
        {
            DataTable dataTable = _payrollReportsRepository.Pay_UnmappedPaystructure(employeeId, isActive);
            return PartialView(dataTable);
        }

        public ActionResult SearchEmployeeBillingReport()
        {
            EmployeeInvoice employeeInvoiceBo = new EmployeeInvoice();
            return PartialView(employeeInvoiceBo);
        }

        public ActionResult EmployeeBillingReportList(DateTime? fromDate, DateTime? todate, string customerName, string employeeName, bool? Active)
        {
            EmployeeInvoiceRepository employeeInvoiceRepository = new EmployeeInvoiceRepository();
            return PartialView(employeeInvoiceRepository.SearchEmployeeBillingInvoiceReport(fromDate, todate, customerName, employeeName, Active));
        }

        #region Complete Payroll Report

        public ActionResult Pay_EmployeeCompletePayrollRPT()
        {
            Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            payrollReportBo.MonthIdList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            payrollReportBo.YearIdList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            return PartialView(payrollReportBo);
        }

        public ActionResult Pay_EmployeeCompletePayrollRPTList(int fromMonthId, int fromYearId, int toMonthId, int toYearId)
        {
            DataTable dataTable = _payrollReportsRepository.Pay_EmployeeCompletePayrollRPTList(fromMonthId, fromYearId, toMonthId, toYearId);
            return PartialView(dataTable);
        }

        #endregion Complete Payroll Report


        public ActionResult Pay_EmployeePaystructureRPT()
        {
            Pay_PayrollReportsBO payrollReportBo = new Pay_PayrollReportsBO();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            payrollReportBo.BusinessUnitList = masterDataRepository.SearchMasterDataDropDown("Pay_CompanyPayStructure", "");

            return PartialView(payrollReportBo);
        }

        public ActionResult Pay_EmployeePaystructureRPTList(int companyPayStructureID)
        {
            DataTable dataTable = _payrollReportsRepository.Pay_GetEmployeePaystructureHistory(companyPayStructureID);
            return PartialView(dataTable);
        }
    }
}