﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeePayroll_V2Controller : Controller
    {
        #region Declaretion

        private readonly EmployeePayrollRepository _employeePayrollRepository = new EmployeePayrollRepository();

        #endregion Declaretion

        #region Employee Payroll

        public ActionResult Index()
        {
            Utility.SetSession("MenuRetention", "PAYROLL");
            Utility.SetSession("SubMenuRetention", "EMPLOYEEPAYROLL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Payroll");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            PayrollBo_V2 payrollBo = new PayrollBo_V2
            {
                MonthIdList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty),
                YearIdList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };

            payrollBo.BusinessUnitList.RemoveAt(0);
            return View(payrollBo);
        }

        public ActionResult SearchPayroll(int monthId, int yearId, string businessUnitList, int? statusId, int? proceed)
        {
            DataSet ds = new DataSet();

            string message = _employeePayrollRepository.SearchPayrollForTdsLop(monthId, yearId, businessUnitList);

            if (message.ToUpper() != "YES")
            {
                return Json(message.ToUpper(), JsonRequestBehavior.AllowGet);
            }

            if (message.ToUpper() == "YES")
            {
                ds = _employeePayrollRepository.SearchEmployeePayroll(monthId, yearId, businessUnitList, statusId, proceed);
            }

            return PartialView(ds);
        }

        public ActionResult SavePayroll(int monthId, int yearId, string businessUnitList)
        {
            return Json(_employeePayrollRepository.SavePayroll(monthId, yearId, businessUnitList, 0), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PayrollActivity(int monthId, int yearId, string businessUnitList, int statusId)
        {
            return Json(_employeePayrollRepository.SavePayroll(monthId, yearId, businessUnitList, statusId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeePayroll(int id, int employeePayStructureId, int monthId, int yearId, string isApproved)
        {
            ViewBag.isApproved = isApproved;
            return PartialView(_employeePayrollRepository.GetPayrollDetails(id, employeePayStructureId, monthId, yearId).OrderByDescending(a => a.IsEditable).ToList());
        }

        [HttpPost]
        public ActionResult SaveSinglePayroll(List<EmployeePayrollBo_v2> list, int monthId, int yearId, int employeeId)
        {
            return Json(_employeePayrollRepository.ManageSinglePayroll(list, monthId, yearId, employeeId));
        }

        #endregion Employee Payroll

        #region Revert Payroll

        public ActionResult RevertEmployeePayroll()
        {
            Utility.SetSession("MenuRetention", "PAYROLL");
            Utility.SetSession("SubMenuRetention", "REVERTPAYROLL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Revert Payroll");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            PayrollBo_V2 payrollBo = new PayrollBo_V2
            {
                MonthIdList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty),
                YearIdList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty),
                EmployeeIDList = masterDataRepository.SearchEmpDropDownForCode(string.Empty, 0),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            payrollBo.BusinessUnitList.RemoveAt(0);
            return View(payrollBo);
        }

        public ActionResult AutoCompleteEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RevertEmployeePayroll(PayrollBo_V2 payrollBo)
        {
            return Json(_employeePayrollRepository.ManageRevertPayroll(payrollBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Revert Payroll

        #region Send Email Payslip

        public ActionResult SendEmail(int monthId, int yearId, string businessUnitList)
        {
            string result;
            List<SelectListItem> employeeList = _employeePayrollRepository.GetEmployeeList(monthId, yearId, businessUnitList);

            if (!employeeList.Any())
            {
                result = BOConstants.Payroll_Not_Processed;
            }
            else
            {
                result = BOConstants.Payslip_Sent;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmployeePayslip_V2()
        {
            return PartialView();
        }

        #endregion Send Email Payslip

        #region GetPayrollHistory

        public ActionResult GetPayrollHistory(int employeeId)
        {
            return PartialView(_employeePayrollRepository.GetPayrollHistory(employeeId));
        }

        public ActionResult GetPayrollSummaryList(int monthId, int yearId, string businessUnitList)
        {
            return PartialView(_employeePayrollRepository.GetPayrollSummaryList(monthId, yearId, businessUnitList));
        }

        #endregion GetPayrollHistory

        #region LOP Details

        public ActionResult LopDetails(string menuCode)
        {
            Utility.SetSession("MenuRetention", "PAYROLL");
            Utility.SetSession("SubMenuRetention", "LOPUPDATE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("LOP Update");

            LopDetailsBo lopDetailsBo = new LopDetailsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            lopDetailsBo.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            lopDetailsBo.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            lopDetailsBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            lopDetailsBo.MonthList.RemoveAt(0);
            lopDetailsBo.YearList.RemoveAt(0);
            lopDetailsBo.BusinessUnitList.RemoveAt(0);
            lopDetailsBo.MenuCode = menuCode;
            return View(lopDetailsBo);
        }

        public ActionResult SearchLopDetails(LopDetailsBo lopDetailsBo)
        {
            return PartialView(_employeePayrollRepository.SearchLopDetails(lopDetailsBo));
        }

        [HttpPost]
        public ActionResult ManageLopDetails(List<LopDetailsBo> lopDetailsBoList)
        {
            string menuCode = (lopDetailsBoList != null) ? lopDetailsBoList[0].MenuCode : "";
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), menuCode, "Edit");
            return Json(!hasAccess ? Constants.UnAuthorizedAccess : _employeePayrollRepository.ManageLopDetails(lopDetailsBoList), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLopDetails(LopDetailsBo lopDetailsBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), lopDetailsBo.MenuCode, "Delete");
            return Json(!hasAccess ? Constants.UnAuthorizedAccess : _employeePayrollRepository.DeleteLopDetails(lopDetailsBo), JsonRequestBehavior.AllowGet);
        }

        #endregion LOP Details

        [HttpPost]
        public ActionResult SendMailPayroll(string monthName, string yearName, int monthId, int yearId, string businessUnitList, int? department, int? proceed)
        {
            string checkPayrollProcessed = _employeePayrollRepository.CheckPayrollProcessed(monthId, yearId, businessUnitList, department);
            if (!string.IsNullOrEmpty(checkPayrollProcessed))
            {
                if (!string.IsNullOrEmpty(checkPayrollProcessed.Remove(0, 1).Split('!')[0]))
                {
                    EmailUtility.SendInstantEmail(checkPayrollProcessed.Remove(0, 1).Split('!')[0], _employeePayrollRepository.PopulateMailBody(monthName, yearName, monthId, yearId, businessUnitList, department),
                        "OfficeBAU Payroll Details for Month of " + monthName + " " + yearName, checkPayrollProcessed.Remove(0, 1).Split('!')[1], string.Empty);

                    return Json("YES", JsonRequestBehavior.AllowGet);
                }

                return Json("NO", JsonRequestBehavior.AllowGet);
            }

            return Json("NO", JsonRequestBehavior.AllowGet);
        }



        #region Excelupload
        private string PayrollUploadProcess(DataTable dataTable, int monthId, int yearId)
        {
            foreach (DataRow row in dataTable.Rows)
            {
                //Get EmployeeCode from excel by removing the text
                string employeecode = Regex.Match(row["Emp_Code"].ToString(), @"\d+").Value;
                int employeeId = 0, year = 0, paystId = 0, payrollCheck = 0, domainId = Utility.DomainId();
                string yearname = yearId.ToString();
                if (!string.IsNullOrEmpty(employeecode))
                {
                    using (FSMEntities context = new FSMEntities())
                    {
                        //Get EmployeeId using given EmployeeCode
                        employeeId = context.tbl_EmployeeMaster.Where(a => a.Code == employeecode && a.DomainID == domainId).FirstOrDefault().ID;
                        if (employeeId != 0)
                        {
                            //check whether payroll is saved for the particular employee for the given year and month
                            payrollCheck = context.tbl_Pay_EmployeePayroll.Count(a => a.EmployeeId == employeeId && a.MonthId == monthId && a.YearId == yearId && a.IsDeleted == false);
                            if (payrollCheck == 0)
                            {
                                //Get EmployeePaystructureID
                                paystId = context.Pay_GetEmployeePayStructureId(monthId, yearId, domainId, employeeId).FirstOrDefault().Value;
                                //Get Year Id 
                                year = context.tbl_FinancialYear.Where(a => a.IsDeleted != true && a.Name == yearname && a.DomainID == domainId).FirstOrDefault().ID;
                            }
                        }
                    }
                    if (paystId != 0)
                    {
                        //Get employeepayroll for the given employee
                        List<EmployeePayrollBo_v2> list = _employeePayrollRepository.GetPayrollDetails(0, paystId, monthId, year);
                        for (var i = 0; i < list.Count; i++)
                        {
                            //Check for editable component
                            if (list[i].IsEditable)
                            {
                                //Update the editable component value
                                list[i].Amount = Convert.ToDecimal(row[list[i].Description.ToString()]);
                            }
                        }
                        //Insert payroll foreach rows in the excel.
                        _employeePayrollRepository.ManageSinglePayroll(list, monthId, year, employeeId);
                    }
                }
            }
            return Notification.Inserted;
        }

        public ActionResult ExcelUploadPayroll()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            PayrollBO payrollBo = new PayrollBO
            {
                MonthIDList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty),
                YearIDList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty)
            };
            return PartialView(payrollBo);
        }

        public ActionResult ExcelUpload()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            PayrollBO payrollBo = new PayrollBO
            {
                MonthIDList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty),
                YearIDList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty)
            };
            return PartialView(payrollBo);
        }

        [HttpPost]
        public ActionResult ExcelUpload(HttpPostedFileBase file, int monthId, int yearId, string type)
        {
            try
            {
                DataSet ds = new DataSet();
                string message = string.Empty;

                // ReSharper disable once PossibleNullReferenceException
                if (Request.Files != null && Request.Files["file"].ContentLength > 0)
                {
                    string fileExtension = Path.GetExtension(Request.Files["file"].FileName);

                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        string fileLocation;
                        string fileName;
                        switch (type.ToUpper())
                        {
                            case "PAY":
                                fileLocation = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\RK";
                                fileName = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\RK\\" + file.FileName;
                                break;

                            default: //LOP & LOPV2
                                fileLocation = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\LOP";
                                fileName = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\LOP\\" + file.FileName;
                                break;
                        }

                        if (!Directory.Exists(fileLocation))
                        {
                            Directory.CreateDirectory(fileLocation);
                        }

                        if (System.IO.File.Exists(fileName))
                        {
                            System.IO.File.Delete(fileName);
                        }

                        file.SaveAs(fileLocation + "\\" + file.FileName);
                        using (MemoryStream ms = new MemoryStream())
                        {
                            file.InputStream.CopyTo(ms);
                            ms.GetBuffer();
                        }

                        Request.Files["file"].SaveAs(fileName);

                        string excelConnectionString = string.Empty;
                        switch (fileExtension)
                        {
                            case ".xls":
                                excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                                break;

                            case ".xlsx":
                                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                                break;
                        }

                        OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                        excelConnection.Open();
                        DataTable dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        if (dt == null)
                        {
                            return null;
                        }

                        string[] excelSheets = new string[dt.Rows.Count];
                        int t = 0;
                        foreach (DataRow row in dt.Rows)
                        {
                            excelSheets[t] = row["TABLE_NAME"].ToString();
                            t++;
                        }

                        OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                        string query = $"Select * from [{excelSheets[0]}]";
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                        {
                            dataAdapter.Fill(ds);
                        }

                        excelConnection.Close();
                    }

                    string conn = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
                    switch (type.ToUpper())
                    {
                        case "PAY":
                            message = PayrollUploadProcess(ds.Tables[0], monthId, yearId);
                            break;
                        default: // LOP & LOPV2
                            int monthDayCount = DateTime.DaysInMonth(yearId, monthId);
                            string spName;
                            if (type.ToUpper() == "LOPV2")
                            {
                                spName = Constants.LOPEXCELUPLOAD_V2;
                            }
                            else
                            {
                                spName = Constants.LOPEXCELUPLOAD;
                            }
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                if (ds.Tables[0].Columns[0].ToString() == "EmpCode")
                                {
                                    //if (Convert.ToDecimal(Convert.ToString(ds.Tables[0].Rows[i][1])) > 0)
                                    //{
                                    if (Convert.ToDecimal(Convert.ToString(ds.Tables[0].Rows[i][1])) <= monthDayCount)
                                    {
                                        using (SqlConnection connection = new SqlConnection(conn))
                                        {
                                            using (SqlCommand commd = new SqlCommand(spName, connection))
                                            {
                                                commd.CommandType = CommandType.StoredProcedure;
                                                commd.Parameters.AddWithValue(DBParam.Input.EmployeeCode, ds.Tables[0].Rows[i][0].ToString());
                                                commd.Parameters.AddWithValue(DBParam.Input.EmployeeName, ds.Tables[0].Rows[i][3].ToString().Replace(" ", ""));
                                                commd.Parameters.AddWithValue(DBParam.Input.MonthID, monthId);
                                                commd.Parameters.AddWithValue(DBParam.Input.YearID, yearId);
                                                commd.Parameters.AddWithValue(DBParam.Input.LopDays, ds.Tables[0].Rows[i][1].ToString());
                                                commd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                                                commd.Parameters.AddWithValue(DBParam.Input.SessionID, Utility.UserId());
                                                connection.Open();
                                                using (SqlDataReader reader = commd.ExecuteReader())
                                                {
                                                    while (reader.Read())
                                                    {
                                                        message = reader["Output"].ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //}
                                }
                                else
                                {
                                    message = BOConstants.Excel_InValid;
                                }
                            }
                            break;
                    }
                }

                return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        #endregion Excelupload
    }
}