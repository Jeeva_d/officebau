﻿using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class CarryForwardLeaveController : Controller
    {
        #region Variables

        private CarryForwardLeaveBO _carryForwardLeaveBo = new CarryForwardLeaveBO();
        private readonly CarryForwardLeaveRepository _carryForwardLeaveRepository = new CarryForwardLeaveRepository();

        #endregion Variables

        #region Carry Forward Leave

        public ActionResult CarryForwardLeaveSummary()
        {
            DataSet ds = new DataSet();
            ds = _carryForwardLeaveRepository.GetCarryforwardSummary(_carryForwardLeaveBo);
            return PartialView(ds);
        }

        public ActionResult GetCarryForwardLeaveDetails(int employeeID, string leaveType)
        {
            return PartialView(_carryForwardLeaveRepository.GetCarryForwardDetails(employeeID, leaveType));
        }

        public ActionResult CreateNewCarryForwardLeave()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            CarryForwardLeaveBO carryForwardLeaveBO = new CarryForwardLeaveBO
            {
                YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty),
                LeaveTypeList = masterDataRepository.SearchMasterDataDropDown("LeaveTypes")
            };
            return PartialView(carryForwardLeaveBO);
        }

        public ActionResult CarryForwardLeaveDetails(CarryForwardLeaveBO carryForwardLeaveBo)
        {
            return PartialView(_carryForwardLeaveRepository.SearchCarryForwardDetails(carryForwardLeaveBo));
        }

        [HttpPost]
        public ActionResult SaveCarryForwardLeave(List<CarryForwardLeaveBO> list)
        {
            _carryForwardLeaveBo = _carryForwardLeaveRepository.ManageCarryForwardLeave(list);
            return Json(_carryForwardLeaveBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion Carry Forward Leave
    }
}