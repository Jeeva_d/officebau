﻿using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class FinancialYearController : Controller
    {
        #region Variables

        private readonly FinancialYearRepository _financialYearRepository = new FinancialYearRepository();

        #endregion Variables

        public ActionResult FinancialYear()
        {
            FinancialYearBo financialYearBo = new FinancialYearBo();

            return PartialView(_financialYearRepository.SearchFinancialYear(financialYearBo));
        }

        public ActionResult AddFinancialYear(int yearId)
        {
            FinancialYearBo financialYearBo = yearId != 0 ? _financialYearRepository.GetFinancialYear(yearId) : _financialYearRepository.GetFnMonthAppConfig();

            return PartialView(financialYearBo);
        }

        [HttpPost]
        public ActionResult AddFinancialYear(FinancialYearBo financialYearBo)
        {
            return Json(_financialYearRepository.ManageFinancialYear(financialYearBo));
        }
    }
}