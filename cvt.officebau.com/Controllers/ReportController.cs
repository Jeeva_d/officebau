﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class ReportController : Controller
    {
        #region SalesReports

        public ActionResult SalesReports()
        {
            Utility.SetSession("MenuRetention", "SALES");
            Utility.SetSession("SubMenuRetention", "REPORTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Sales Report");

            return View();
        }

        #endregion SalesReports

        #region Variable

        private readonly ReportBo reportBO = new ReportBo();
        private readonly ReportRepository reportRepository = new ReportRepository();

        #endregion Variable

        #region Sales Report - Chart

        public ActionResult MonthlyTargetReport(string menuCode)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            reportBO.BusinessUnitId = masterDataRepository.GetBaseLocation(Utility.UserId());
            reportBO.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            reportBO.YearList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            reportBO.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            reportBO.MonthList.RemoveAt(0);
            reportBO.YearList.RemoveAt(0);
            reportBO.BusinessUnitList.RemoveAt(0);
            return PartialView(reportBO);
        }

        public ActionResult BusinessUnitSalesTarget(ReportBo reportBo)
        {
            return Json(reportRepository.GetBusinessUnitSalesTarget(reportBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult TargetReportData(int monthId, int yearId)
        {
            return Json(reportRepository.GetTargetReportData(monthId, yearId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalesExecutiveTargetReport(ReportBo reportBo)
        {
            return Json(reportRepository.GetSalesExecutiveReportData(reportBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchActualSalesReport(ReportBo reportBo)
        {
            return PartialView(reportRepository.SearchActualSalesReport(reportBo));
        }

        public ActionResult SearchCommittedSalesReport(ReportBo reportBo)
        {
            return PartialView(reportRepository.SearchCommittedSalesReport(reportBo));
        }

        //public ActionResult SearchSalesActivity(int employeeID)
        //{
        //    return PartialView(reportRepository.SearchSalesActivity(employeeID, Utility.DomainId()));
        //}

        #endregion Sales Report - Chart

        #region Rail-Rake Report

        public ActionResult RailRakeReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            reportBO.BusinessUnitList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", "Region");
            return PartialView(reportBO);
        }

        #endregion Rail-Rake Report

        #region Activity Count Report

        public ActionResult ActivityCountReport(string menuCode)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            reportBO.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            return PartialView(reportBO);
        }

        public ActionResult SearchActivityCountReport(ReportBo reportBo)
        {
            return PartialView(reportRepository.GetActivityCountReport(reportBo));
        }

        //public ActionResult SearchActivityCount(DateTime visitDate, int employeeID)
        //{
        //    return PartialView(reportRepository.GetSalesCountList(visitDate, employeeID));
        //}

        #endregion Activity Count Report

        #region Monthly Grid BUWise

        public ActionResult MonthlyGridBusinessUnitWise(string menuCode)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            reportBO.BusinessUnitId = masterDataRepository.GetBaseLocation(Utility.UserId());
            reportBO.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            reportBO.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            reportBO.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            reportBO.MonthList.RemoveAt(0);
            reportBO.YearList.RemoveAt(0);
            reportBO.BusinessUnitList.RemoveAt(0);
            return PartialView(reportBO);
        }

        public ActionResult BusinessUnitSalesTargetGrid(ReportBo reportBo)
        {
            return PartialView(reportRepository.GetBusinessUnitSalesGrid(reportBo));
        }

        #endregion Monthly Grid BUWise

        #region Sales Compliance Report

        public ActionResult SalesComplianceReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            reportBO.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            reportBO.YearList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            reportBO.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            reportBO.MonthList.RemoveAt(0);
            reportBO.YearList.RemoveAt(0);
            reportBO.BusinessUnitList.RemoveAt(0);
            return PartialView(reportBO);
        }

        public ActionResult SalesComplianceReportList(ReportBo reportBo)
        {
            return PartialView(reportRepository.SearchSalesComplianceReport(reportBo));
        }

        public ActionResult SalesComplianceReportDetails(int? monthId, int? yearId, int employeeId, int dateValue)
        {
            return PartialView(reportRepository.GetSalesComplianceReportDetails(monthId, yearId, employeeId, dateValue));
        }

        #endregion Sales Compliance Report
    }
}