﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeOffBoardProcessController : Controller
    {
        #region Variables

        private EmployeeOffBoardBo _employeeOffBoardBo = new EmployeeOffBoardBo();
        private readonly EmployeeOffBoardProcessRepository _employeeOffBoardProcessRepository = new EmployeeOffBoardProcessRepository();

        #endregion Variables

        #region OffBoard Request

        public ActionResult OffBoardRequest(string menuCode)
        {
            Utility.SetSession("MenuRetention", "EXITPROCESS");
            Utility.SetSession("SubMenuRetention", "OFFBOARDREQUEST");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Offboard Request");

            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            _employeeOffBoardBo.EmployeeId = Utility.UserId();
            string id = _employeeOffBoardProcessRepository.GetOffboardRequestId(Utility.UserId());

            if (!string.IsNullOrWhiteSpace(id))
            {
                _employeeOffBoardBo = _employeeOffBoardProcessRepository.GetOffBoardRequest(Convert.ToInt32(id));
            }
            else
            {
                _employeeOffBoardBo.Approver = leaveManagementRepository.GetApprover(Utility.UserId(), "Name");
                _employeeOffBoardBo.ApproverId = Convert.ToInt32(leaveManagementRepository.GetApprover(Utility.UserId(), "ID"));
            }

            return View(_employeeOffBoardBo);
        }

        [HttpPost]
        public ActionResult OffBoardRequest(EmployeeOffBoardBo employeeOffBoardBo)
        {
            employeeOffBoardBo.MenuCode = MenuCode.EmployeeOffBoardRequest;
            string message = string.Empty;
            RbsRepository rbsRepository = new RbsRepository();
            employeeOffBoardBo.ModifiedBy = Utility.UserId();
            employeeOffBoardBo.EmployeeId = Utility.UserId();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeOffBoardBo.MenuCode, "Save");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employeeOffBoardBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    message = _employeeOffBoardProcessRepository.ManageOffBoardRequest(employeeOffBoardBo);
                    break;

                case Operations.Update:
                    message = _employeeOffBoardProcessRepository.ManageOffBoardRequest(employeeOffBoardBo);
                    break;

                case Operations.Withdraw:
                    employeeOffBoardBo.IsDeleted = true;
                    employeeOffBoardBo.EmployeeId = Utility.UserId();
                    message = _employeeOffBoardProcessRepository.WithdrawOffBoardRequest(employeeOffBoardBo);
                    break;

                case Operations.Delete:
                    message = _employeeOffBoardProcessRepository.DeleteOffBoardRequest(employeeOffBoardBo);
                    break;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OffBoardWithdrawHistory(int employeeId)
        {
            _employeeOffBoardBo.EmployeeId = employeeId;
            _employeeOffBoardBo.DomainId = Utility.DomainId();

            return PartialView(_employeeOffBoardProcessRepository.GetOffBoardHistory(employeeId));
        }

        #endregion OffBoard Request

        #region OffBoard Approve

        public ActionResult SearchOffBoardApprove(string menuCode)
        {
            Utility.SetSession("MenuRetention", "EXITPROCESS");
            Utility.SetSession("SubMenuRetention", "OFFBOARDAPPROVAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Offboard Approval");

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _employeeOffBoardBo.ApproverId = Utility.UserId();
            _employeeOffBoardBo.StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "OffBoard");
            _employeeOffBoardBo.StatusId = _employeeOffBoardProcessRepository.GetStatusId("OffBoard", "Initiated");
            return View(_employeeOffBoardBo);
        }

        public ActionResult OffBoardApproveList(EmployeeOffBoardBo employeeOffBoardBo)
        {
            employeeOffBoardBo.UserId = Utility.UserId();
            employeeOffBoardBo.DomainId = Utility.DomainId();
            return PartialView(_employeeOffBoardProcessRepository.SearchOffBoardApproval(employeeOffBoardBo));
        }

        public ActionResult OffBoardApprove(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _employeeOffBoardBo = _employeeOffBoardProcessRepository.GetOffBoardRequest(id);
            _employeeOffBoardBo.ApproverList = masterDataRepository.SearchEmpDropDownForCode("HR Manager", 0);
            _employeeOffBoardBo.MenuCode = MenuCode.EmployeeOffBoardApprove;
            return PartialView(_employeeOffBoardBo);
        }

        [HttpPost]
        public ActionResult OffBoardApprove(int id, string status, string remarks, int? hrApproverId, DateTime? tentativeRelievingDate, int employeeId, DateTime? initiatedDate, DateTime? relievingDate)
        {
            RbsRepository rbsRepository = new RbsRepository();
            int approverId = Utility.UserId();
            int domainId = Utility.DomainId();
            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_employeeOffBoardProcessRepository.ApproveOffBoardRequest(id, status, remarks, approverId, domainId, hrApproverId, tentativeRelievingDate, employeeId, initiatedDate, relievingDate), JsonRequestBehavior.AllowGet);
        }

        #endregion OffBoard Approve

        #region HR Approval

        public ActionResult SearchOffBoardHrApprove(string menuCode)
        {
            Utility.SetSession("MenuRetention", "EXITPROCESS");
            Utility.SetSession("SubMenuRetention", "EXITPROCESS");

            MasterDataRepository masterDataRepository = new MasterDataRepository();

             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Offboard Approval - HR");
            _employeeOffBoardBo.HrApproverId = Utility.UserId();
            _employeeOffBoardBo.StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "OffBoard");
            _employeeOffBoardBo.StatusId = _employeeOffBoardProcessRepository.GetStatusId("OffBoard", "Pending");
            return View(_employeeOffBoardBo);
        }

        public ActionResult OffBoardHrApproveList(EmployeeOffBoardBo employeeOffBoardBo)
        {
            employeeOffBoardBo.UserId = Utility.UserId();
            employeeOffBoardBo.DomainId = Utility.DomainId();
            return PartialView(_employeeOffBoardProcessRepository.SearchOffBoardHrApproval(employeeOffBoardBo));
        }

        public ActionResult OffBoardHrApprove(int id, int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            _employeeOffBoardBo.MenuCode = MenuCode.EmployeeOffBoardHRApproval;

            _employeeOffBoardBo = _employeeOffBoardProcessRepository.GetOffBoardHrApprove(id);
            ViewData["CompanyAsset"] = _employeeOffBoardProcessRepository.SearchCompanyAssetDetail(_employeeOffBoardBo);

            EmployeeOffBoardBo employeeExitBo = _employeeOffBoardProcessRepository.GetExitInterviewDetail(id);
            employeeExitBo.RelivingReasonList = masterDataRepository.SearchMasterDataDropDown("ExitReason", string.Empty);
            ViewData["ExitDetails"] = employeeExitBo;
            return PartialView(_employeeOffBoardBo);
        }

        public ActionResult CompanyAssetsDetails(int employeeId, int offboardProcessID)
        {
            _employeeOffBoardBo.EmployeeId = employeeId;
            _employeeOffBoardBo.Id = offboardProcessID;
            return PartialView(_employeeOffBoardProcessRepository.SearchCompanyAssetDetail(_employeeOffBoardBo));
        }

        public ActionResult AssetsReturnedDetails(EmployeeOffBoardBo employeeOffBoardBo)
        {
            return PartialView(_employeeOffBoardProcessRepository.SearchCompanyAssetDetail(employeeOffBoardBo));
        }

        public ActionResult ReturnedDetails(int id)
        {
            CompanyAssetsBo companyAssetsBo = new CompanyAssetsBo();
            if (id != 0)
            {
                companyAssetsBo = _employeeOffBoardProcessRepository.GetReturnDetail(id);
            }

            return PartialView(companyAssetsBo);
        }

        public ActionResult RemarksDetails(int id)
        {
            CompanyAssetsBo companyAssetsBo = new CompanyAssetsBo();
            if (id != 0)
            {
                companyAssetsBo = _employeeOffBoardProcessRepository.GetReturnDetail(id);
            }

            return PartialView(companyAssetsBo);
        }

        [HttpPost]
        public ActionResult ReturnedDetails(CompanyAssetsBo companyAssetsBo)
        {
            _employeeOffBoardBo.UserId = Utility.UserId();
            _employeeOffBoardBo.DomainId = Utility.DomainId();
            return Json(_employeeOffBoardProcessRepository.UpdateReturnedDetails(companyAssetsBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExitInterviewDetails(int id, int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            _employeeOffBoardBo = _employeeOffBoardProcessRepository.GetExitInterviewDetail(id);
            _employeeOffBoardBo.RelivingReasonList = masterDataRepository.SearchMasterDataDropDown("ExitReason", string.Empty);
            return PartialView(_employeeOffBoardBo);
        }

        [HttpPost]
        public ActionResult ExitInterviewDetails(EmployeeOffBoardBo employeeOffBoardBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_employeeOffBoardProcessRepository.UpdateExitInterviewDetails(employeeOffBoardBo), JsonRequestBehavior.AllowGet);
        }

        #endregion HR Approval

        #region Assets Clearance

        public ActionResult SearchAssetsClearance()
        {
            Utility.SetSession("MenuRetention", "EXITPROCESS");
            Utility.SetSession("SubMenuRetention", "ASSETCLEARANCE");

            MasterDataRepository masterDataRepository = new MasterDataRepository();

             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Assets Clearance");
            _employeeOffBoardBo.HrApproverId = Utility.UserId();
            _employeeOffBoardBo.StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "OffBoard");
            _employeeOffBoardBo.StatusId = _employeeOffBoardProcessRepository.GetStatusId("OffBoard", "Pending");
            return View(_employeeOffBoardBo);
        }

        public ActionResult AssetsClearanceList(EmployeeOffBoardBo employeeOffBoardBo)
        {
            employeeOffBoardBo.UserId = Utility.UserId();
            employeeOffBoardBo.DomainId = Utility.DomainId();
            return PartialView(_employeeOffBoardProcessRepository.SearchEmployeeAssetsList(employeeOffBoardBo));
        }

        #endregion Assets Clearance

        #region Asset Report

        public ActionResult SearchAssetReport()
        {
            Utility.SetSession("MenuRetention", "EMPLOYEEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "REPORTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Assset Report");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            CompanyAssetsBo companyAssetsBo = new CompanyAssetsBo
            {
                EmployeeId = Utility.UserId(),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown(),
                AssetTypeList = masterDataRepository.SearchMasterDataDropDown("AssetType", string.Empty)
            };
            return PartialView(companyAssetsBo);
        }

        public ActionResult AssetReportList(CompanyAssetsBo companyAssetsBo)
        {
            return PartialView(_employeeOffBoardProcessRepository.SearchAssetReturnDetails(companyAssetsBo));
        }

        public ActionResult AutoCompleteEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName), JsonRequestBehavior.AllowGet);
        }

        #endregion Asset Report

        #region Offboard Request HR

        public ActionResult SearchOffBoardRequestHR()
        {
            Utility.SetSession("MenuRetention", "EXITPROCESS");
            Utility.SetSession("SubMenuRetention", "OFFBOARDREQUEST-HR");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Request - HR");

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _employeeOffBoardBo.StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "OffBoard");
            return View(_employeeOffBoardBo);
        }

        public ActionResult OffboardRequestHRList(EmployeeOffBoardBo employeeOffBoardBO)
        {
            return PartialView(_employeeOffBoardProcessRepository.GetOffboardRequestHRList(employeeOffBoardBO));
        }

        public ActionResult OffBoardRequestHR(int Id)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            if (Id != 0)
            {
                _employeeOffBoardBo = _employeeOffBoardProcessRepository.GetOffBoardRequest(Id);
            }
            else
            {
                _employeeOffBoardBo.Approver = leaveManagementRepository.GetApprover(Utility.UserId(), "Name");
                _employeeOffBoardBo.ApproverId = Convert.ToInt32(leaveManagementRepository.GetApprover(Utility.UserId(), "ID"));
            }

            return PartialView(_employeeOffBoardBo);
        }

        [HttpPost]
        public ActionResult OffBoardRequestHR(EmployeeOffBoardBo employeeOffBoardBO)
        {
            employeeOffBoardBO.MenuCode = MenuCode.EmployeeOffBoardRequestHR;
            string Message = string.Empty;
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            employeeOffBoardBO.ModifiedBy = Utility.UserId();

            if (employeeOffBoardBO != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeOffBoardBO.MenuCode, "Save");
            }
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employeeOffBoardBO.Operation.ToUpper())
            {
                case Operations.Insert:
                    Message = _employeeOffBoardProcessRepository.ManageOffBoardRequest(employeeOffBoardBO);
                    break;

                case Operations.Update:
                    Message = _employeeOffBoardProcessRepository.ManageOffBoardRequest(employeeOffBoardBO);
                    break;

                case Operations.Withdraw:
                    employeeOffBoardBO.IsDeleted = true;
                    employeeOffBoardBO.CreatedBy = Utility.UserId();
                    employeeOffBoardBO.ModifiedBy = Utility.UserId();
                    employeeOffBoardBO.DomainId = Utility.DomainId();
                    Message = _employeeOffBoardProcessRepository.WithdrawOffBoardRequest(employeeOffBoardBO);
                    break;

                case Operations.Delete:
                    Message = _employeeOffBoardProcessRepository.DeleteOffBoardRequest(employeeOffBoardBO);
                    break;
            }

            return Json(Message, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AutoCompleteEmployeeByBU(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName, Utility.UserId()), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetApproverDetails(int employeeID)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            string Approver = leaveManagementRepository.GetApprover(employeeID, "Name");
            int ApproverId = Convert.ToInt32(leaveManagementRepository.GetApprover(employeeID, "ID"));

            return Json(ApproverId + "/" + Approver, JsonRequestBehavior.AllowGet);
        }

        #endregion Offboard Request HR
    }
}