﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class VendorMasterController : Controller
    {
        #region Vendor Master

        public ActionResult VendorMaster(string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "MASTER");
            Utility.SetSession("SubMenuRetention", "VENDORMASTER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Vendor Master");
            VendorMasterBo vendorMasterBo = new VendorMasterBo();
            return View(_vendorMasterRepository.SearchVendor(vendorMasterBo));
        }

        #endregion Vendor Master

        #region Auto Complete

        public ActionResult AutoCompleteCity(string name)
        {
            CommonRepository commonRepository = new CommonRepository();
            return Json(commonRepository.SearchAutoComplete("City", "Name", name), JsonRequestBehavior.AllowGet);
        }

        #endregion Auto Complete

        #region Variables

        private readonly VendorMasterRepository _vendorMasterRepository = new VendorMasterRepository();

        #endregion Variables

        #region Add Vendor

        public ActionResult AddVendorEdit(int id, string menuCode)
        {
            VendorMasterBo vendorMasterBo = _vendorMasterRepository.GetVendor(id);
            vendorMasterBo.MenuCode = menuCode;

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            vendorMasterBo.CountryList = masterDataRepository.SearchMasterDataDropDown("Country", string.Empty);
            vendorMasterBo.CurrencyList = masterDataRepository.SearchMasterDataDropDown("Currency");
            vendorMasterBo.Statelist = new List<SelectListItem>();
            vendorMasterBo.Citylist = new List<SelectListItem>();

            return PartialView(vendorMasterBo);
        }

        [HttpPost]
        public ActionResult AddVendorEdit(VendorMasterBo vendorMasterBo)
        {
            if (!string.IsNullOrWhiteSpace(vendorMasterBo.PanNo))
            {
                vendorMasterBo.PanNo = vendorMasterBo.PanNo.ToUpper();
            }

            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), vendorMasterBo.MenuCode, vendorMasterBo.Id == 0 ? "Save" : "Edit");

            if (vendorMasterBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), vendorMasterBo.MenuCode, "Delete");
            }

            return !hasAccess ? Json(Constants.UnAuthorizedAccess) : Json(_vendorMasterRepository.ManageVendor(vendorMasterBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Add Vendor
    }
}