﻿using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class Profit_Share_Analysis_ConfigurationController : Controller
    {
        // GET: Accounts_Configuration
        #region Variables

        private Profit_Share_Analysis_ConfigurationBo profit_Share_Analysis_ConfigurationBo = new Profit_Share_Analysis_ConfigurationBo();
        private Profit_Share_Analysis_Configuration_Repository profit_Share_Analysis_Configuration_Repository = new Profit_Share_Analysis_Configuration_Repository();

        #endregion Variables

        #region Profit Share Analysis Configuration

        public ActionResult Profit_Share_Analysis_Configuration_Search()
        {
            return PartialView(profit_Share_Analysis_ConfigurationBo);
        }

        public ActionResult Profit_Share_Analysis_Configuration_SearchList(string employeeName)
        {
            return PartialView(profit_Share_Analysis_Configuration_Repository.ProfitShareAnalysisConfigurationSearch(employeeName));
        }

        public ActionResult Create_profit_Share_Analysis_Configuration(int id)
        {
            if (id != 0)
            {
                profit_Share_Analysis_ConfigurationBo = profit_Share_Analysis_Configuration_Repository.GetProfitShareAnalysisConfiguration(id);
            }
            return PartialView(profit_Share_Analysis_ConfigurationBo);
        }

        [HttpPost]
        public ActionResult Create_profit_Share_Analysis_Configuration(Profit_Share_Analysis_ConfigurationBo profit_Share_Analysis_ConfigurationBo)
        {
            return Json(profit_Share_Analysis_Configuration_Repository.ManageProfitShareAnalysisConfiguration(profit_Share_Analysis_ConfigurationBo));
        }

        #endregion Profit Share Analysis Configuration
    }
}