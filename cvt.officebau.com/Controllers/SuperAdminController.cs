﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class SuperAdminController : Controller
    {
        #region Super Admin

        public ActionResult SuperAdmin()
        {
            return PartialView();
        }

        #endregion Super Admin

        #region Variable

        private readonly SuperAdminRepository _superAdminRepository = new SuperAdminRepository();

        #endregion

        #region Company Mapping

        public ActionResult CompanyMapping()
        {
            Utility.SetSession("MenuRetention", "SUPERADMIN");
            Utility.SetSession("SubMenuRetention", "COMPANYMAPPING");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Company Mapping");

            ViewData["employeeList"] = _superAdminRepository.SearchEmployeeList();
            return View();
        }

        public ActionResult EmployeeList()
        {
            return PartialView();
        }

        public ActionResult ManageCompanyMapping(int employeeId)
        {
            return PartialView(_superAdminRepository.GetEmployeeCompanyMapping(employeeId));
        }

        [HttpPost]
        public ActionResult ManageCompanyMapping(UserLoginBo userLoginBo)
        {
            userLoginBo.UserId = Utility.UserId();
            return Json(_superAdminRepository.ManageEmployeeCompanyMapping(userLoginBo));
        }

        public ActionResult SearchAutoCompleteEmployee(string employeeName)
        {
            return Json(_superAdminRepository.SearchAutoCompleteEmployee(employeeName), JsonRequestBehavior.AllowGet);
        }

        #endregion Company Mapping
    }
}
