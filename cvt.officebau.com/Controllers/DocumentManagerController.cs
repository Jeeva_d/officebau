﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    public class DocumentManagerController : Controller
    {
        #region Variables

        private readonly DocumentManagerRepository _documentManagerRepository = new DocumentManagerRepository();
        private DocumentManagerBO _documentManagerBO = new DocumentManagerBO();

        #endregion Variables

        public ActionResult SearchDocumentManager()
        {
            Utility.SetSession("MenuRetention", "DOCUMENTMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "DOCUMENTMANAGER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Document Manager");

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _documentManagerBO.DocumentCategoryList = masterDataRepository.SearchMasterDataDropDown("DocumentCategory", string.Empty);

            return View(_documentManagerBO);
        }

        public ActionResult DocumentManagerList(DocumentManagerBO documentManagerBO)
        {
            return PartialView(_documentManagerRepository.GetDocumentManagersList(documentManagerBO));
        }

        public ActionResult CreateDocumentManager(int id)
        {
            if (id != 0)
            {
                _documentManagerBO = _documentManagerRepository.GetDocumentManager(id);
                if (!string.IsNullOrWhiteSpace(_documentManagerBO.FileName))
                {
                    _documentManagerBO.FileName = _documentManagerBO.FileName;
                }
            }
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _documentManagerBO.DocumentCategoryList = masterDataRepository.SearchMasterDataDropDown("DocumentCategory", string.Empty);

            return PartialView(_documentManagerBO);
        }

        [HttpPost]
        public ActionResult CreateDocumentManager(DocumentManagerBO documentManagerBO)
        {
            documentManagerBO.MenuCode = MenuCode.DOCUMENTMANAGER;
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (documentManagerBO != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), documentManagerBO.MenuCode, documentManagerBO.Id == 0 ? "Save" : "Edit");
            }

            if (documentManagerBO != null && documentManagerBO.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), documentManagerBO.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            string message = _documentManagerRepository.ManageDocument(documentManagerBO);
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #region File Upload

        public JsonResult Upload(HttpPostedFileBase file)
        {
            List<string> list = new List<string>();
            if (file != null)
            {
                list = UploadUtility.UploadFile(file, Utility.UploadUrl("DOCUMENTS"));
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion File Upload

        #region Get File Names

        public ActionResult GetFileNames(string description, string guidName)
        {
            return Json(_documentManagerRepository.Searchfilepreview(guidName, description), JsonRequestBehavior.AllowGet);
        }

        #endregion Get File Names

        #region Delete Image 

        public ActionResult DeleteImageFile(int id, string fileName, string guid)
        {
            FSMEntities context = new FSMEntities();
            tbl_MultipleFileUpload newsAndEvents = context.tbl_MultipleFileUpload.FirstOrDefault(b => b.RowID == id);
            context.Entry(newsAndEvents).State = EntityState.Deleted;
            context.SaveChanges();

            string serverSavePath = AppDomain.CurrentDomain.BaseDirectory + "Images\\"
                                                                       + Utility.GetSession("CompanyName")
                                                                       + "\\" + Utility.UploadUrl("DOCUMENTS");

            if (System.IO.File.Exists(serverSavePath + '/' + fileName))
            {
                System.IO.File.Delete(serverSavePath + '/' + fileName);
            }

            return Json("");
        }
       
        #endregion Delete Image 
        public FileResult DownloadFile(string fileUploadId)
        {
            FileUpload fileUpload = UploadUtility.DownloadFile(fileUploadId);
            return File(fileUpload.FileName, fileUpload.FileType, fileUpload.OriginalFileName);
        }

        public FileResult DownloadMultipleFiletable(int id)
        {
            FileUpload fileUpload = new FileUpload();
            using (FSMEntities context = new FSMEntities())
            {
                fileUpload = (from e in context.tbl_MultipleFileUpload.Where(e => e.RowID == id)
                              select new FileUpload
                              {
                                  FileName = e.Path + e.FileName,
                                  FileType = e.OriginalFileName
                              }).FirstOrDefault();
            }
            return File(fileUpload.FileName, System.IO.Path.GetExtension(fileUpload.FileType), fileUpload.FileType);
        }

        #region DocumentViewer/--

  
        /// <summary>
        /// Document Viewer Page
        /// </summary>
        /// <returns></returns>
        public ActionResult DocumentViewer()
        {
            Utility.SetSession("MenuRetention", "DOCUMENTMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "DOCUMENTVIEWER");
            return View();
        }

        /// <summary>
        /// Get Document Viewer
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDocumentViewer()
        {
            return Json(_documentManagerRepository.GetDocumentViewer(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Document Viewer List
        /// </summary>
        /// <param name="id">Document ID</param>
        /// <returns></returns>
        public ActionResult DocumentViewerList(int id)
        {
            return PartialView(_documentManagerRepository.DocumentViewerList(id));
        }

        /// <summary>
        /// Download Document
        /// </summary>
        /// <param name="fileUploadId">Document ID</param>
        /// <returns></returns>
        public ActionResult DocumentView(string fileUploadId)
        {
            // using common method for getting file information.
            return PartialView(UploadUtility.DownloadFile(fileUploadId));
        }

        #endregion DocumentViewer
    }
}
