﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class MailerController : Controller
    {
        #region Variables

        private readonly MailerRepository _mailerRepository = new MailerRepository();

        #endregion Variables

        #region Mailer

        public ActionResult SearchMailerByBusinessUnit(string menuCode)
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Mail Configuration");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            MailerBo mailerBo = new MailerBo { BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown() };
            return PartialView(mailerBo);
        }

        public ActionResult MailerList(int? buId)
        {
            MailerBo mailerBo = new MailerBo
            {
                BusinessUnitId = buId == null ? 0 : Convert.ToInt32(buId)
            };

            return PartialView(_mailerRepository.SearchCcReceipts(mailerBo));
        }

        public ActionResult ManageCcReceipts(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            MailerBo mailerBo = _mailerRepository.GetCcReceipts(id);

            mailerBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            mailerBo.EventList = masterDataRepository.SearchMasterDataDropDown("MailerEventKeys", string.Empty);
            return PartialView(mailerBo);
        }

        [HttpPost]
        public ActionResult ManageCcReceipts(MailerBo mailerBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            mailerBo.MenuCode = MenuCode.MailConfiguration;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), mailerBo.MenuCode, "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_mailerRepository.ManageCcReceipts(mailerBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Mailer
    }
}