﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class TransferController : Controller
    {
        #region Get Available Bank Balance

        public ActionResult GetAvailableBalance(int id)
        {
            TransferBo transferBo = _transferRepository.GetAvailableBalance(id);
            return Json(transferBo.OpeningBalance, JsonRequestBehavior.AllowGet);
        }

        #endregion Get Available Bank Balance

        #region Variables

        private readonly TransferRepository _transferRepository = new TransferRepository();
        private readonly CommonRepository _commonRepository = new CommonRepository();

        #endregion Variables

        #region Transfer

        public ActionResult SearchTransfer(string menuCode, int? id, string screenType, int? statusId)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "VOUCHER");
            Utility.SetSession("SubMenuRetention", "CONTRA");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Transfer");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            SearchParamsBo searchParamsBo = new SearchParamsBo
            {
                BankList = masterDataRepository.SearchMasterDataDropDown("Bank"),
                Id = id ?? 0,
                StatusId = statusId ?? 0,
                ScreenType = screenType
            };
            return View(searchParamsBo);
        }

        public ActionResult Transfer(SearchParamsBo searchParamsBo)
        {
            return PartialView(_transferRepository.SearchTransfer(searchParamsBo));
        }

        public ActionResult ClearTransferDetail(SearchParamsBo searchParamsBo)
        {
            DateTime financialYear = _commonRepository.GetFinancialYear(Convert.ToInt32(Session["StartMonth"]));

            searchParamsBo.Id = 0;
            searchParamsBo.Bank = new Entity { Id = searchParamsBo.Id };
            searchParamsBo.StartDate = financialYear;
            searchParamsBo.EndDate = financialYear.AddDays(365 - 1);
            searchParamsBo.Notations = string.Empty;
            searchParamsBo.Amount = string.Empty;

            return Json(searchParamsBo, JsonRequestBehavior.AllowGet);
        }

        #endregion Transfer

        #region Create Transfer

        public ActionResult CreateTransfer(int id, string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            TransferBo transferBo = _transferRepository.GetTransfer(id);
            transferBo.MenuCode = menuCode;
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            transferBo.TransferToList = masterDataRepository.SearchMasterDataDropDown("Bank");
            transferBo.TransferFromList = masterDataRepository.SearchMasterDataDropDown("Bank");

            return PartialView(transferBo);
        }

        [HttpPost]
        public ActionResult CreateTransfer(TransferBo transferBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (transferBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), transferBo.MenuCode, transferBo.Id == 0 ? "Save" : "Edit");
            }

            if (transferBo != null && transferBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), transferBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            return Json(_transferRepository.ManageTransfer(transferBo));
        }

        #endregion Create Transfer
    }
}