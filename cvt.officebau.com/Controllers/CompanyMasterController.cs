﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class CompanyMasterController : Controller
    {
        #region Variables

        private readonly CompanyMasterRepository _companyMasterRepository = new CompanyMasterRepository();

        #endregion Variables

        #region File Upload

        public JsonResult Upload(HttpPostedFileBase file)
        {
            List<string> list = new List<string>();

            if (file != null)
            {
                list = UploadUtility.UploadFile(file, Utility.UploadUrl("COMPANYLOGO"), "INTERNAL");
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion File Upload

        #region Company Master

        public ActionResult SearchCompanyMaster()
        {
            Utility.SetSession("MenuRetention", "SUPERADMIN");
            Utility.SetSession("SubMenuRetention", "COMPANYMASTER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Company Master");
            return View(new CompanyMasterBo());
        }

        public ActionResult CompanyMaster(CompanyMasterBo companyMasterBo)
        {
            return PartialView(_companyMasterRepository.GetCompanyMasterList(companyMasterBo));
        }

        public ActionResult CreateCompanyMaster(int id)
        {
            CompanyMasterBo companyMasterBo = new CompanyMasterBo { MenuCode = MenuCode.CompanyMaster };

            if (id != 0)
            {
                companyMasterBo = _companyMasterRepository.GetCompanyMasterDetail(id);
            }

            return PartialView(companyMasterBo);
        }

        [HttpPost]
        public ActionResult CreateCompanyMaster(CompanyMasterBo companyMasterBo)
        {
            if (!string.IsNullOrWhiteSpace(companyMasterBo.Panno))
            {
                companyMasterBo.Panno = companyMasterBo.Panno.ToUpper();
            }

            companyMasterBo.MenuCode = MenuCode.CompanyMaster;
            if (!string.IsNullOrWhiteSpace(companyMasterBo.FileUpload))
            {
                companyMasterBo.FileUploadId = companyMasterBo.FileUploadId;
            }

            string message = _companyMasterRepository.ManageCompanyMaster(companyMasterBo);
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MyCompanyDetails(string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "COMPANYMASTER");

            CompanyMasterBo companyMasterBo = new CompanyMasterBo
            {
                Id = Utility.DomainId()
            };
            companyMasterBo = _companyMasterRepository.GetCompanyMasterDetail(companyMasterBo.Id);
            companyMasterBo.MenuCode = Utility.GetSession("MenuID");

            if (!string.IsNullOrWhiteSpace(companyMasterBo.Logo))
            {
                string imagePath = UploadUtility.DownloadFile(companyMasterBo.FileUploadId.ToString()).FileName;
                companyMasterBo.Logo = "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("COMPANYLOGO") + imagePath.Split('\\')[imagePath.Split('\\').Length - 1];
            }

            return View(companyMasterBo);
        }

        #endregion Company Master
    }
}