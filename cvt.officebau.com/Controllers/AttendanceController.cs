﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class AttendanceController : Controller
    {
        #region Variables

        private AttendanceRepository _attendanceRepository;

        #endregion Variables

        #region Add New Log for Employee

        public ActionResult AddNewEmployeeLogPunches(AttendanceBo attendanceBo)
        {
            if (attendanceBo.LogDate == DateTime.MinValue)
            {
                attendanceBo.LogDate = Convert.ToDateTime("01-01-1900 12:00:00 AM");
            }

            return PartialView(attendanceBo);
        }

        #endregion Add New Log for Employee

        #region Employee Attendance

        public ActionResult Attendance()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "EMPLOYEEATTENDANCE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Attendance");

            AttendanceBo attendanceBo = new AttendanceBo();
            MasterDataRepository masterRepository = new MasterDataRepository();

            attendanceBo.MonthList = masterRepository.SearchMasterDataDropDown("Month", string.Empty);
            attendanceBo.YearList = masterRepository.SearchMasterDataDropDown("Financialyear", string.Empty);
            attendanceBo.BusinessUnitList = masterRepository.GetBusinessUnitDropDown();
            attendanceBo.MonthId = DateTime.Now.Month;
            attendanceBo.YearId = masterRepository.GetCurrentYearId(DateTime.Now.Year.ToString());
            return View(attendanceBo);
        }

        public ActionResult SearchMonthlyAttendance(AttendanceBo attendanceBo)
        {
            _attendanceRepository = new AttendanceRepository();
            string isLopUpdate = ConfigurationManager.AppSettings["IsLOPUpdate"];

            Utility.SetSession("IsLOPUpdate", isLopUpdate);

            if (!string.IsNullOrWhiteSpace(attendanceBo.BusinessUnitId))
            {
                attendanceBo.BusinessUnitId = attendanceBo.BusinessUnitId.Trim(',');
            }

            return PartialView(_attendanceRepository.SearchMonthlyAttendance(attendanceBo));
        }

        public ActionResult CheckBusinessBusinessUnit(int? businessUnitId, string formName)
        {
            _attendanceRepository = new AttendanceRepository();

            if (formName.ToUpper() == "EMPLOYEEATTENDANCE")
            {
                return Json(_attendanceRepository.CheckBusinessHoursBasedOnBusinessUnit(businessUnitId), JsonRequestBehavior.AllowGet);
            }

            return Json(_attendanceRepository.CheckBusinessHoursBasedOnEmployeeId(Utility.UserId()), JsonRequestBehavior.AllowGet);
        }

        #endregion Employee Attendance

        #region Log Attendance

        public ActionResult LogAttendance()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "LOGATTENDANCE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Log Attendance");

            AttendanceBo attendanceBo = new AttendanceBo { AttendanceDate = DateTime.Now.Date };
            return View(attendanceBo);
        }

        public ActionResult SearchLogAttendance(AttendanceBo attendanceBo)
        {
            _attendanceRepository = new AttendanceRepository();
            return PartialView(_attendanceRepository.SearchDailyAttendance(attendanceBo));
        }

        [HttpPost]
        public ActionResult ManageLogAttendance(AttendanceBo attendanceBo)
        {
            _attendanceRepository = new AttendanceRepository();
            RbsRepository rbsRepository = new RbsRepository();
            attendanceBo.MenuCode = MenuCode.LogAttendance;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), attendanceBo.MenuCode, attendanceBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_attendanceRepository.ManageDailyAttendance(attendanceBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Log Attendance

        #region My Attendance

        public ActionResult SearchEmployeeAttendance()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "MYATTENDANCE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("My Attendance");

            MasterDataRepository masterRepository = new MasterDataRepository();

            AttendanceBo attendanceBo = new AttendanceBo
            {
                EmployeeId = Utility.UserId(),
                MonthList = masterRepository.SearchMasterDataDropDown("Month", string.Empty),
                YearList = masterRepository.SearchMasterDataDropDown("Financialyear", string.Empty),
                MonthId = DateTime.Now.Month,
                YearId = masterRepository.GetCurrentYearId(DateTime.Now.Year.ToString())
            };
            return View(attendanceBo);
        }

        public ActionResult EmployeeAttendance(AttendanceBo attendanceBo)
        {
            _attendanceRepository = new AttendanceRepository();
            attendanceBo.EmployeeId = Utility.UserId();
            return PartialView(_attendanceRepository.SearchEmployeeAttendance(attendanceBo));
        }

        public ActionResult GetMyAttendance(int employeeId, DateTime logDate)
        {
            _attendanceRepository = new AttendanceRepository();
            return PartialView(_attendanceRepository.GetMyAttendance(employeeId, logDate));
        }

        #endregion My Attendance

        #region Biometric Sync

        public ActionResult BiometricSynchronize()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "BIOMETRICSYNC");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Biometric Sync");

            return View();
        }

        public ActionResult BiometricSynchronizePartialView()
        {
            MasterDataRepository masterRepository = new MasterDataRepository();
            AttendanceBo attendanceBo = new AttendanceBo
            {
                EmployeeId = Utility.UserId(),
                MonthList = masterRepository.SearchMasterDataDropDown("Month", string.Empty),
                YearList = masterRepository.SearchMasterDataDropDown("Financialyear", string.Empty)
            };
            return PartialView(attendanceBo);
        }

        [HttpPost]
        public ActionResult BiometricSynchronize(AttendanceBo attendanceBo)
        {
            _attendanceRepository = new AttendanceRepository();
            return Json(_attendanceRepository.Synchronization(attendanceBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Biometric Sync

        #region Upload Log Punches

        public ActionResult UploadLogPunches()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult AttendanceLogPunchesUpload(HttpPostedFileBase file, string deviceName)
        {
            string message = Notification.InvalidFile;
            TimeWatch timeWatch = new TimeWatch();
            string fileLocation = Server.MapPath("~/Content/TimeWatch/") + Session["CompanyName"] + "\\TimeWatch";
            try
            {
                message = timeWatch.GetPunchDetailsList(file, fileLocation, deviceName);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
            }
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Upload Log Punches

        public ActionResult AutoCompleteEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonBu(searchName), JsonRequestBehavior.AllowGet);
        }
    }
}