﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class InventoryController : Controller
    {
        #region Variables

        private readonly InventoryRepository _inventoryRepository = new InventoryRepository();

        #endregion Variables

        #region Stock Distribution

        // GET: Inventory
        public ActionResult StockDistribution()
        {
            Utility.SetSession("MenuRetention", "INVENTORY");
            Utility.SetSession("SubMenuRetention", "STOCKDISTRIBUTION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Stock Distribution");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            InventoryBO inventoryBo = new InventoryBO
            {
                ProductList = masterDataRepository.SearchMasterDataDropDown("Product_V2")
            };

            return View(inventoryBo);
        }

        public ActionResult SearchStockDistribution(InventoryBO inventoryBo)
        {
            List<InventoryBO> searchStockDistributionList = _inventoryRepository.SearchStockDistribution(inventoryBo);
            return PartialView(searchStockDistributionList);
        }

        public ActionResult CreateNewStockDistribution(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            InventoryBO inventoryBo = new InventoryBO();
            if (id != 0)
            {
                inventoryBo = _inventoryRepository.GetStockDistributionDetails(id);
            }
            inventoryBo.CostCenterList = masterDataRepository.SearchMasterDataDropDown("CostCenter");
            inventoryBo.ProductList = masterDataRepository.SearchMasterDataDropDown("Product_V2");

            return PartialView(inventoryBo);
        }

        [HttpPost]
        public ActionResult CreateNewStockDistribution(InventoryBO inventoryBo)
        {
            inventoryBo.MenuCode = MenuCode.StockDistribution;
            inventoryBo.ModifiedBy = Utility.UserId();
            inventoryBo.CreatedOn = Utility.GetCurrentDate();
            inventoryBo.ModifiedOn = Utility.GetCurrentDate();
            inventoryBo.DomainId = Utility.DomainId();

            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), inventoryBo.MenuCode, inventoryBo.Id == 0 ? "Save" : "Edit");

            if (inventoryBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), inventoryBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            string message = _inventoryRepository.ManageStockDistribution(inventoryBo);

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteEmployee(string employeeName)
        {
            return Json(_inventoryRepository.SearchAutoCompleteEmployee(employeeName), JsonRequestBehavior.AllowGet);
        }

        #endregion Stock Distribution

        #region StockAdjustment

        public ActionResult StockAdjustment()
        {
            Utility.SetSession("MenuRetention", "INVENTORY");
            Utility.SetSession("SubMenuRetention", "STOCKADJUSTMENT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Stock Adjustment");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            InventoryBO inventoryBo = new InventoryBO
            {
                ProductList = masterDataRepository.SearchMasterDataDropDown("Product_V2"),
                LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger")
            };
            return PartialView(inventoryBo);
        }

        public ActionResult SearchStockAdjustment(InventoryBO inventoryBo)
        {
            List<InventoryBO> searchStockAdjustmentList = _inventoryRepository.SearchStockAdjustment(inventoryBo);
            return PartialView(searchStockAdjustmentList);
        }

        public ActionResult GetStockValue(int id)
        {
            return Json(_inventoryRepository.GetStockOnHand(id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateNewStockAdjustment(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            InventoryBO inventoryBo = new InventoryBO();

            StockAdjustmentDetailsBO stockAdjustmentDetailsBo = new StockAdjustmentDetailsBO();
            if (id == 0)
            {
                inventoryBo.StockAdjustmentDetails = new List<StockAdjustmentDetailsBO> { stockAdjustmentDetailsBo };
                stockAdjustmentDetailsBo.ItemProductList = masterDataRepository.SearchMasterDataDropDown("Product_V2");
            }
            else
            {
                inventoryBo = _inventoryRepository.GetStockAdjustmentDetails(id);
            }

            inventoryBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger");
            inventoryBo.ReasonList = masterDataRepository.SearchMasterDataDropDown("InventoryReason", string.Empty);

            return PartialView(inventoryBo);
        }

        public ActionResult ManageStockAdjustment(InventoryBO inventoryBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            inventoryBo.MenuCode = MenuCode.StockAdjustment;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), inventoryBo.MenuCode, inventoryBo.Id == 0 ? "Save" : "Edit");
            if (inventoryBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), inventoryBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_inventoryRepository.ManageStockAdjustmentDetails(inventoryBo));
        }

        #endregion StockAdjustment

        #region Stock Maintenance

        public ActionResult SearchStockMaintenance()
        {
            Utility.SetSession("MenuRetention", "INVENTORY");
            Utility.SetSession("SubMenuRetention", "STOCKMAINTENANCE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Stock Maintenance");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            InventoryBO inventoryBo = new InventoryBO
            {
                ProductList = masterDataRepository.SearchMasterDataDropDown("Product_V2")
            };
            return View(inventoryBo);
        }

        public ActionResult StockMaintenanceList(InventoryBO inventoryBo)
        {
            return PartialView(_inventoryRepository.SearchStockMaintenance(inventoryBo));
        }

        public ActionResult StockMaintenance(int productId)
        {
            InventoryBO inventoryBo = new InventoryBO {ProductID = productId};

            return PartialView(inventoryBo);
        }

        public ActionResult StockProductDetails(int productId)
        {
            return PartialView(_inventoryRepository.GetProductDetails(productId));
        }

        public ActionResult StockTransactionDetails(int productId)
        {
            return PartialView(_inventoryRepository.SearchStockTransactionDetails(productId));
        }

        public ActionResult StockAdjustmentDetails(InventoryBO inventoryBo)
        {
            inventoryBo.Operation = "StockMaintenance";
            return PartialView(_inventoryRepository.SearchStockAdjustment(inventoryBo));
        }

        #endregion Stock Maintenance

        #region My Stock

        public ActionResult SearchMyStock(string menuCode)
        {
            Utility.SetSession("MenuRetention", "INVENTORY");
            Utility.SetSession("SubMenuRetention", "MYSTOCK");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("My Stock");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            InventoryBO inventoryBo = new InventoryBO
            {
                ProductList = masterDataRepository.SearchMasterDataDropDown("Product_V2")
            };
            return View(inventoryBo);
        }

        public ActionResult MyStockList(int productId)
        {
            return PartialView(_inventoryRepository.SearchMyStockDistribution(productId));
        }

        #endregion My Stock
    }
}