﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Linq;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class OtherReportsController : Controller
    {
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;

        public ActionResult Reports(string menuCode, string screenType, DateTime? startDate, DateTime? endDate, string name)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "DASHBOARDANDREPORTS");
            Utility.SetSession("SubMenuRetention", "OTHERREPORTS");
            UtilityRepository track = new UtilityRepository(); track.TrackFormNavigation("Account - Other Reports");

            //OtherReportBO otherReportBO = new OtherReportBO();
            //ApplicationConfigurationRepository applicationConfigrepository = new ApplicationConfigurationRepository();
            //otherReportBO.IsCostCenter = applicationConfigrepository.SearchCostCenter();
            //otherReportBO.Operation = ScreenType;
            //otherReportBO.FromDate = StartDate;
            //otherReportBO.ToDate = EndDate;
            //otherReportBO.Name = Name;
            //return View(otherReportBO);

            RbsRepository rbsRepository = new RbsRepository();
            List<RbsMenuBo> rbsMenuBoList = rbsRepository.SearchMenuforReportal();

            switch (screenType ?? "")
            {
                case "GSTSummary":
                    screenType = MenuCode.GSTMONRPT; // Menucode for GSTSummary
                    if (rbsMenuBoList.Count > 0)
                    {
                        if (rbsMenuBoList.FindAll(x => x.MenuCode == screenType).Count > 0)
                        {
                            screenType = "GSTSummary";
                        }
                        else
                        {
                            screenType = "NotAuthorized";
                        }

                        foreach (RbsMenuBo rbs in rbsMenuBoList)
                        {
                            rbs.Operation = screenType;
                        }
                    }

                    break;
            }

            return View(rbsMenuBoList);
        }

        public ActionResult ExpenseTransactiondetail(string parameter, string partyName, string type)
        {
            return PartialView(_reportRepository.SearchExpenseTransactiondetail(parameter, partyName, type));
        }

        public ActionResult IncomeTransactiondetail(string parameter, string partyName, string type)
        {
            return PartialView(_reportRepository.SearchIncomeTransactiondetail(parameter, partyName, type));
        }

        [HttpPost]
        public ActionResult IncomeTransactiondetail(string partyName, string startDate, string endDate, string type)
        {
            return PartialView(_reportRepository.SearchCostcenterTransactiondetail(partyName, startDate, endDate, type));
        }

        public ActionResult SearchLoanDetails()
        {
            return PartialView(_reportRepository.SearchLoanDetails());
        }

        public ActionResult GetLoanDetails(int id)
        {
            return PartialView(_reportRepository.GetLoanDetails(id));
        }

        #region

        private readonly ReportRepository _reportRepository = new ReportRepository();
        private readonly CommonRepository _commonRepository = new CommonRepository();

        private string _formName = string.Empty;

        #endregion

        //Expense Supplier Report

        #region ExpenseBySupplier Report

        public ActionResult SearchExpenseSupplierRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "EXPENSEBYSUPPLIER";
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));
            return PartialView(searchParamsBo);
        }

        public ActionResult ExpenseSupplierRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "EXPENSEBYSUPPLIER";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetSupplierExpenseList(searchParamsBo));
        }

        public DataSet GetSupplierExpenseList(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtExpenseSupplier = new DataTable("ExpenseSupplier");
            ds.Tables.Add(dtExpenseSupplier);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_EXPENSESUPPLIER, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtExpenseSupplier);
                    }
                }
            }

            return ds;
        }

        #endregion

        //Expense Ledger Report

        #region ExpenseByLedger Report

        public ActionResult SearchExpenseLedgerRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "EXPENSEBYLEDGER";
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));
            return PartialView(searchParamsBo);
        }

        public ActionResult ExpenseLedgerRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "EXPENSEBYLEDGER";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetExpenseLedgerList(searchParamsBo));
        }

        public DataSet GetExpenseLedgerList(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtExpenseLedger = new DataTable("ExpenseLedger");
            ds.Tables.Add(dtExpenseLedger);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_EXPENSELEDGER, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtExpenseLedger);
                    }
                }
            }

            return ds;
        }

        #endregion

        //Income Customer Report

        #region IncomeByCustomer Report

        public ActionResult SearchIncomeCustomerRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "INCOMEBYCUSTOMER";
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(searchParamsBo);
        }

        public DataSet GetCustomerList(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtIncomeCustomer = new DataTable("IncomeCustomer");
            ds.Tables.Add(dtIncomeCustomer);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_INCOMECUSTOMER, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtIncomeCustomer);
                    }
                }
            }

            return ds;
        }

        public ActionResult IncomeCustomerRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "INCOMEBYCUSTOMER";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetCustomerList(searchParamsBo));
        }

        #endregion

        //Income Product/Service Report

        #region IncomeByProduct/Service Report

        public ActionResult SearchIncomeProductServiceRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "INCOMEBYPRODUCTSERVICE";
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(searchParamsBo);
        }

        public ActionResult IncomeProductServiceRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "INCOMEBYPRODUCTSERVICE";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetProductServiceList(searchParamsBo));
        }

        public DataSet GetProductServiceList(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtIncomeProductService = new DataTable("IncomeProductService");
            ds.Tables.Add(dtIncomeProductService);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_INCOMEPRODUCTSERVICE, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtIncomeProductService);
                    }
                }
            }

            return ds;
        }

        #endregion

        //DayBookByCash Report

        #region DayBookByCash Report

        public ActionResult SearchDayBookByCashRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "DAYBOOKBYCASH";
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(searchParamsBo);
        }

        public ActionResult DayBookByCashRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "DAYBOOKBYCASH";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetDayBookByCashList(searchParamsBo));
        }

        public DataSet GetDayBookByCashList(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtDayBookByCash = new DataTable("DayBookByCash");
            ds.Tables.Add(dtDayBookByCash);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_DAYBOOKBYCASH, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtDayBookByCash);
                    }
                }
            }

            return ds;
        }

        #endregion

        #region DayBookByBank Report

        public ActionResult DayBookByBankRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "DAYBOOKBYBANK";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetDayBookByBank(searchParamsBo));
        }

        public ActionResult SearchBankDropDown(SearchParamsBo searchParamsBo)
        {
            _formName = "DAYBOOKBYBANK";
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            searchParamsBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(searchParamsBo);
        }

        public DataSet GetDayBookByBank(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtDayBookByBank = new DataTable("DayBookByBank");
            ds.Tables.Add(dtDayBookByBank);

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_DAYBOOKBYBANK, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.BankID, searchParamsBo.Id);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtDayBookByBank);
                    }
                }
            }

            return ds;
        }

        #endregion

        //DayBookByLedger Report

        #region DayBookByLedger

        public ActionResult SearchLedgerDropDown(SearchParamsBo searchParamsBo)
        {
            _formName = "LEDGERDETAILS";
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            searchParamsBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger");
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(searchParamsBo);
        }

        public ActionResult DayBookByLedgerRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "LEDGERDETAILS";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetDayBookByLedger(searchParamsBo));
        }

        public DataSet GetDayBookByLedger(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            //DataTable dtDayBookByLedger = new DataTable("DayBookByLedger");
            //ds.Tables.Add(dtDayBookByLedger);

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_DAYBOOKBYLEDGER, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.LedgerName, searchParamsBo.LedgerName);

                    con.Open();

                    using (SqlDataAdapter dr = new SqlDataAdapter(cmd))
                    {
                        dr.Fill(ds);
                        ds.Tables[0].TableName = "LedgerDetails";
                        ds.Tables[1].TableName = "DayBookByLedger";
                    }
                }
            }

            return ds;
        }

        public ActionResult AutoCompleteLedger(string ledgerName)
        {
            CommonRepository commonRepository = new CommonRepository();
            return Json(commonRepository.SearchAutoCompleteParameter("Ledger", "Name", ledgerName), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region VendorStatement

        public ActionResult VendorStatement(SearchParamsBo searchParamsBo)
        {
            _formName = "VENDORSTATEMENT";
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            searchParamsBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Vendor");
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(searchParamsBo);
        }

        public ActionResult VendorStatementRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "VENDORSTATEMENT";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetDayVendorStatement(searchParamsBo));
        }

        public DataSet GetDayVendorStatement(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_VENDORSTATEMENT, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.LedgerName, searchParamsBo.VendorName);

                    con.Open();

                    using (SqlDataAdapter dr = new SqlDataAdapter(cmd))
                    {
                        dr.Fill(ds);
                        ds.Tables[0].TableName = "LedgerDetails";
                        ds.Tables[1].TableName = "DayBookByLedger";
                    }
                }
            }

            return ds;
        }

        public ActionResult AutoCompleteVendor(string vendorName)
        {
            CommonRepository commonRepository = new CommonRepository();
            return Json(commonRepository.SearchAutoCompleteParameter("Vendor", "Name", vendorName), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CustomerStatement

        public ActionResult CustomerStatement(SearchParamsBo searchParamsBo)
        {
            _formName = "CUSTOMERSTATEMENT";
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            searchParamsBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Customer");
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(searchParamsBo);
        }

        public ActionResult CustomerStatementRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "CUSTOMERSTATEMENT";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetDayCustomerStatement(searchParamsBo));
        }

        public DataSet GetDayCustomerStatement(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_CUSTOMERSTATEMENT, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.LedgerName, searchParamsBo.VendorName);

                    con.Open();

                    using (SqlDataAdapter dr = new SqlDataAdapter(cmd))
                    {
                        dr.Fill(ds);
                        ds.Tables[0].TableName = "LedgerDetails";
                        ds.Tables[1].TableName = "DayBookByLedger";
                    }
                }
            }

            return ds;
        }

        public ActionResult AutoCompleteCustomer(string vendorName)
        {
            CommonRepository commonRepository = new CommonRepository();
            return Json(commonRepository.SearchAutoCompleteParameter("Customer", "Name", vendorName), JsonRequestBehavior.AllowGet);
        }

        #endregion

        //AgingAP Report

        #region AgingAP Report

        public ActionResult SearchAgingApReport(ReportsBo reportsBoParam)
        {
            ReportsBo reportsBo = new ReportsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            reportsBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");

            ViewData["AgingforAp"] = GetAgingApReport(reportsBoParam.Id);
            return PartialView(reportsBo);
        }

        public ActionResult AgingApReport(int yearId)
        {
            return PartialView(GetAgingApReport(yearId));
        }

        public DataSet GetAgingApReport(int yearId)
        {
            DataSet ds = new DataSet();
            DataTable dtAgingForAp = new DataTable("AgingforAp");
            ds.Tables.Add(dtAgingForAp);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_AGINGAP, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.FinancialYearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtAgingForAp);
                    }
                }
            }

            return ds;
        }

        #endregion

        //AgingAR Report

        #region AgingAR Report

        public ActionResult SearchAgingArReport(ReportsBo reportsBoParam)
        {
            ReportsBo reportsBo = new ReportsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            reportsBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");

            ViewData["AgingforAR"] = GetAgingArRpt(reportsBoParam.Id);
            return PartialView(reportsBo);
        }

        public ActionResult AgingArReport(int yearId)
        {
            return PartialView(GetAgingArRpt(yearId));
        }

        public DataSet GetAgingArRpt(int yearId)
        {
            DataSet ds = new DataSet();
            DataTable dtAgingForAr = new DataTable("AgingforAR");
            ds.Tables.Add(dtAgingForAr);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_AGINGAR, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.FinancialYearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtAgingForAr);
                    }
                }
            }

            return ds;
        }

        #endregion

        //CostCenter Report

        #region CostCenter Report

        public ActionResult SearchCostCenterRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "COSTCENTER";
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));
            return PartialView(searchParamsBo);
        }

        public ActionResult CostCenterRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "COSTCENTER";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetCostCenterRpt(searchParamsBo));
        }

        public DataSet GetCostCenterRpt(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtCostCenter = new DataTable("CostCenter");
            ds.Tables.Add(dtCostCenter);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_COSTCENTER, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtCostCenter);
                    }
                }
            }

            return ds;
        }

        #endregion

        //Revenue Center Report

        #region RevenueCenter Report

        public ActionResult SearchRevenueCenterRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "REVENUECENTER";
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));
            return PartialView(searchParamsBo);
        }

        public ActionResult RevenueCenterRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "REVENUECENTER";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetRevenueCenterRpt(searchParamsBo));
        }

        public DataSet GetRevenueCenterRpt(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtRevenueCenter = new DataTable("RevenueCenter");
            ds.Tables.Add(dtRevenueCenter);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_REVENUECENTER, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtRevenueCenter);
                    }
                }
            }

            return ds;
        }

        #endregion

        #region CompareCostCenter Report

        public ActionResult SearchCompareCostCenterRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "COSTVSREVENUECENTER";
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(searchParamsBo);
        }

        public ActionResult CompareCostCenter(SearchParamsBo searchParamsBo)
        {
            _formName = "COSTVSREVENUECENTER";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            CostCenterRepository costCenterRepository = new CostCenterRepository();
            return Json(costCenterRepository.GetCompareCostRevenue(searchParamsBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CompareCostCenterGridRpt(SearchParamsBo searchParamsBo)
        {
            return PartialView(GetCompareCostCenterRpt(searchParamsBo));
        }

        public DataSet GetCompareCostCenterRpt(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtCompareCostCenter = new DataTable("CompareCostCenter");
            ds.Tables.Add(dtCompareCostCenter);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.GETCOMPARECOSTREVENUECENTER, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.Name, searchParamsBo.LedgerName);
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtCompareCostCenter);
                    }
                }
            }

            return ds;
        }

        #endregion

        #region Expense Budgeting

        public ActionResult RptExpenseBudgeting()
        {
            ReportsBo reportsBo = new ReportsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            reportsBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");
            reportsBo.FinancialYearList.RemoveAll(i => i.Text == "-- Select --");
            return PartialView(reportsBo);
        }

        public ActionResult GetExpenseBudgetReport(int yearId, string type)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(GetExpenseBudgeting(yearId, type));
            ds.Tables.Add(GetIncomeBudgeting(yearId, type));
            return PartialView(ds);
        }

        public DataTable GetExpenseBudgeting(int yearId, string type)
        {
            DataTable expenseBudgeting = new DataTable("ExpenseBudgeting");
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Rpt_ExpenseBudgeting", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.FinancialYearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.Type, type);
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        expenseBudgeting.Load(dr, LoadOption.OverwriteChanges);
                    }
                }
            }

            return expenseBudgeting;
        }

        #endregion  Expense Budgeting

        #region Income Budgeting

        public ActionResult RptEIncomeBudgeting()
        {
            ReportsBo reportsBo = new ReportsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            reportsBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");
            reportsBo.FinancialYearList.RemoveAll(i => i.Text == "-- Select --");
            return PartialView(reportsBo);
        }

        public ActionResult GetIncomeBudgetReport(int yearId, string type)
        {
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(GetIncomeBudgeting(yearId, type));
            return PartialView(dataSet);
        }

        public DataTable GetIncomeBudgeting(int yearId, string type)
        {
            DataTable expenseBudgeting = new DataTable("IncomeBudgeting");
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Rpt_Incomebudgeting", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.FinancialYearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.Type, type);

                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        expenseBudgeting.Load(dr, LoadOption.OverwriteChanges);
                    }
                }
            }

            return expenseBudgeting;
        }

        #endregion  Income Budgeting

        #region  Financial Report

        public ActionResult ReportFinancialYear()
        {
            ReportsBo reportsBo = new ReportsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            reportsBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");
            reportsBo.FinancialYearList.RemoveAll(i => i.Text == "-- Select --");
            return PartialView(reportsBo);
        }

        public ActionResult GetFyReport(int fyid, string rptType)
        {
            return PartialView(GetFyReports(fyid, rptType));
        }

        public DataSet GetFyReports(int fyId, string rptType)
        {
            DataSet ds = new DataSet();
            DataTable expenseBudgeting = new DataTable("Temp_RPT_FinancialReport");
            ds.Tables.Add(expenseBudgeting);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("RPT_FinancialReport", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@FYID", fyId);
                    cmd.Parameters.AddWithValue("@RptType", rptType);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());

                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, expenseBudgeting);
                    }
                }
            }

            return ds;
        }

        #endregion  Financial Report

        #region GST Input tax Report

        public ActionResult SearchGstInputTaxReport()
        {
            ReportsBo reportsBo = new ReportsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _formName = "GSTINPUTTAXREPORT";

            reportsBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");
            reportsBo.FinancialYearId = masterDataRepository.GetCurrentYearId(DateTime.Now.Year.ToString());
            return PartialView(reportsBo);
        }

        public ActionResult GstInputTaxReport(int yearId, string reportType, bool isIncludeZero)
        {
            _formName = "GSTINPUTTAXREPORT";
            return PartialView(GstInputTaxReportList(yearId, reportType, isIncludeZero));
        }

        public DataSet GstInputTaxReportList(int yearId, string reportType, bool isIncludeZero)
        {
            DataSet ds = new DataSet();
            DataTable dtInputTax = new DataTable("InputTax");
            ds.Tables.Add(dtInputTax);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_GSTINPUTTAX, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.FinancialYearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.ReportType, reportType);
                    cmd.Parameters.AddWithValue(DBParam.Input.IsIncludeZero, isIncludeZero);
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtInputTax);
                    }
                }
            }

            return ds;
        }

        #endregion GST Input tax Report

        #region GST Output tax Report

        public ActionResult SearchGstOutputTaxReport()
        {
            ReportsBo reportsBo = new ReportsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _formName = "GSTOUTPUTTAXREPORT";
            reportsBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");
            reportsBo.FinancialYearId = masterDataRepository.GetCurrentYearId(DateTime.Now.Year.ToString());
            return PartialView(reportsBo);
        }

        public ActionResult GstOutputTaxReport(int yearId, string reportType, bool isIncludeZero)
        {
            _formName = "GSTOUTPUTTAXREPORT";
            return PartialView(GstOutputTaxReportList(yearId, reportType, isIncludeZero));
        }

        public DataSet GstOutputTaxReportList(int yearId, string reportType, bool isIncludeZero)
        {
            DataSet ds = new DataSet();
            DataTable dtOutputTax = new DataTable("OutputTax");
            ds.Tables.Add(dtOutputTax);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_GSTOUTPUTTAX, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.FinancialYearID, yearId);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.ReportType, reportType);
                    cmd.Parameters.AddWithValue(DBParam.Input.IsIncludeZero, isIncludeZero);
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtOutputTax);
                    }
                }
            }

            return ds;
        }

        #endregion GST Output tax Report

        #region GST Summary Report

        public ActionResult SearchGstSummaryReport()
        {
            ReportsBo reportsBo = new ReportsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _formName = "GSTSUMMARYREPORT";
            reportsBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");
            reportsBo.FinancialYearId = masterDataRepository.GetCurrentYearId(DateTime.Now.Year.ToString());
            return PartialView(reportsBo);
        }

        public ActionResult GstSummaryReport(int yearId, string reportType)
        {
            _formName = "GSTSUMMARYREPORT";
            return PartialView(_reportRepository.GstSummaryReportList(yearId, reportType));
        }

        #endregion GST Summary Report

        #region GST Summary Monthly Report

        public ActionResult SearchGstSummaryMonthlyReport()
        {
            ReportsBo reportsBo = new ReportsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _formName = "GSTSUMMARYMONTHLYREPORT";
            reportsBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");
            reportsBo.FinancialYearId = masterDataRepository.GetCurrentYearId(DateTime.Now.Year.ToString());
            return PartialView(reportsBo);
        }

        public ActionResult GstSummaryMonthlyReport(int yearId, string reportType)
        {
            _formName = "GSTSUMMARYMONTHLYREPORT";
            return PartialView(_reportRepository.GstSummaryMonthlyReportList(yearId, reportType));
        }

        #endregion GST Summary Monthly Report

        #region Open Invoice Report

        public ActionResult SearchOpenInvoiceRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "openinvoice";
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return PartialView(searchParamsBo);
        }

        public DataSet GetopenInvoiceList(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtopenInvoices = new DataTable("OpenInvoice");
            ds.Tables.Add(dtopenInvoices);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.RPT_OPENINVOICES, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue(DBParam.Input.CustomerName, searchParamsBo.CustomerName);
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtopenInvoices);
                    }
                }
            }

            return ds;
        }

        public ActionResult OpenInvoiceRpt(SearchParamsBo searchParamsBo)
        {
            _formName = "openinvoice";
            if (searchParamsBo.Event == "search")
            {
                _commonRepository.GetSearchDataHistory(searchParamsBo, "search", _formName, Convert.ToInt32(Session["StartMonth"]));
            }

            return PartialView(GetopenInvoiceList(searchParamsBo));
        }

        #endregion

        #region Complete Report

        #region Expense Report

        public ActionResult TDSRpt(SearchParamsBo searchParamsBo)
        {
            return PartialView(searchParamsBo);
        }

        public DataSet GetTDSList(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtTDS = new DataTable("tds");
            ds.Tables.Add(dtTDS);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Rpt_TDSReport", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.FromDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.ToDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    cmd.Parameters.AddWithValue("@SearchTextbox", searchParamsBo.CustomerName);
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtTDS);
                    }
                }
            }

            return ds;
        }

        public ActionResult SearchtdsRpt(SearchParamsBo searchParamsBo)
        {
            return PartialView(GetTDSList(searchParamsBo));
        }

        #endregion Expense Report

        #region Sales Report

        public ActionResult SearchCompleteSalesRpt(SearchParamsBo searchParamsBo)
        {
            return PartialView(searchParamsBo);
        }

        public DataSet GetCompleteSalesRpt(SearchParamsBo searchParamsBo)
        {
            DataSet ds = new DataSet();
            DataTable dtTDS = new DataTable("SalesRpt");
            ds.Tables.Add(dtTDS);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Rpt_CompleteSales", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue(DBParam.Input.FromDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.ToDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtTDS);
                    }
                }
            }

            return ds;
        }

        public ActionResult CompleteSalesRptList(SearchParamsBo searchParamsBo)
        {
            return PartialView(GetCompleteSalesRpt(searchParamsBo));
        }

        #endregion Sales Report

        #endregion Complete Report

        #region  Profit Share Analysis

        public ActionResult ProfitShareAnalysis()
        {
            return PartialView();
        }

        public class ChartBo
        {
            public string Consultant { get; set; }
            public string Type { get; set; }
            public string Party { get; set; }
            public decimal? Sales { get; set; }
            public decimal? Incentive { get; set; }
            public decimal? Share { get; set; }
        }

        public ActionResult GetProfitShareAnalysis()
        {
            var chartBo = new ChartBo();
            var listGraphModel = new List<ChartBo>();

            AccountsDashboardRepository accountsDashboardRepository = new AccountsDashboardRepository();
            var das = accountsDashboardRepository.SearchShareDetailsChart();
            foreach (var i in das)
            {
                listGraphModel.Add(new ChartBo { Consultant = i.EmployeeName, Sales = i.RevenueBooked, Incentive = i.ExpenseIncentiveBooked, Share = i.ExpenseBooked });
            }
            return Json(listGraphModel, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ChartDrilldown(string type, string employeeName)
        {
            AccountsDashboardRepository accountsDashboardRepository = new AccountsDashboardRepository();

            if (type.ToLower() == "sales")
            {
                DataTable dt = new DataTable
                {
                    TableName = "SalesRpt"
                };
                dt.Columns.Add("Vendor", typeof(string));
                dt.Columns.Add("Ledger", typeof(string));
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Amount", typeof(string));
                dt.Columns.Add("Status", typeof(string));


                var cus = accountsDashboardRepository.GetCustomerResult().Where(c => c.tbl_EmployeeMaster.FirstName == employeeName && c.CustomerId != null && !c.IsDeleted).Select(a => a.tblCustomer.Name).ToList();
                var list = accountsDashboardRepository.GetViewCustomer().ToList();
                var result = list.Where(a => cus.Contains(a.Customer)).OrderByDescending(a => a.Date).Select(b =>
                                     dt.Rows.Add(b.Customer, b.Product, Convert.ToDateTime(b.Date).ToString("dd MMM yyyy"), Utility.PrecisionForDecimal(b.Amount, 0), b.StatusName)

                      ).ToList();
                DataSet ds = new DataSet();

                ds.Tables.Add(dt);
                return PartialView(ds);
            }
            else
            {
                var vendor = accountsDashboardRepository.GetVendorResult().Where(c => c.tbl_EmployeeMaster.FirstName == employeeName && c.VendorId != null && !c.IsDeleted).Select(a => a.tblVendor.Name).ToList();

                DataTable dt = new DataTable
                {
                    TableName = "SalesRpt"
                };
                dt.Columns.Add("Vendor", typeof(string));
                dt.Columns.Add("Ledger", typeof(string));
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Amount", typeof(string));
                dt.Columns.Add("Status", typeof(string));
                if (type.ToLower() == "share")
                {
                    var venderList = accountsDashboardRepository.GetViewVendor().ToList();
                    venderList.Where(a => vendor.Contains(a.NAME) && a.Ledger.ToLower().Contains("share")).OrderByDescending(a => a.Date).Select(b =>
                                       dt.Rows.Add(b.NAME, b.Ledger, b.Date.ToString("dd MMM yyyy"), Utility.PrecisionForDecimal(Convert.ToDecimal(b.Amount), 0), b.StatusName)

                      ).ToList();
                }
                else
                {
                    var venderList = accountsDashboardRepository.GetViewVendor().ToList();
                    venderList.Where(a => vendor.Contains(a.NAME) && !a.Ledger.ToLower().Contains("share")).OrderByDescending(a => a.Date).Select(b =>
                    dt.Rows.Add(b.NAME, b.Ledger, b.Date.ToString("dd MMM yyyy"), Utility.PrecisionForDecimal(Convert.ToDecimal(b.Amount), 0), b.StatusName)
                      ).ToList();
                }
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                return PartialView(ds);
            }
        }

        #endregion
    }
}