﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeProfileController : Controller
    {
        #region Employee Profile

        public ActionResult EmployeeProfile()
        {
            _employeeMasterBo = _employeeProfileRepository.GetEmployeeProfile(Utility.UserId());
            Utility.SetSession("EmployeeName", _employeeMasterBo.FirstName);
            if (!string.IsNullOrWhiteSpace(_employeeMasterBo.FileName))
            {
                _employeeMasterBo.FileName = Path.Combine("..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("PROFILE"), _employeeMasterBo.FileName);
            }
            else
            {
                _employeeMasterBo.FileName = (_employeeMasterBo.Gender ?? "").ToUpper() == "FEMALE" ?
                    "..\\Images\\DefaultProfile_female.png" : "..\\Images\\DefaultProfile.png";
            }

            if (_employeeMasterBo.LogoId != null && _employeeMasterBo.LogoId != new Guid())
            {
                string imagePath = UploadUtility.DownloadFile(Convert.ToString(_employeeMasterBo.LogoId)).FileName;
                if (!string.IsNullOrEmpty(imagePath))
                    _employeeMasterBo.Logo = "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("COMPANYLOGO") + imagePath.Split('\\')[imagePath.Split('\\').Length - 1];
            }
            else
            {
                _employeeMasterBo.Logo = string.Empty;
            }

            GetApplicationConfiguration getApplicationConfiguration = new GetApplicationConfiguration();
            _employeeMasterBo.IsStatutoryRequired = getApplicationConfiguration.GetApplicationConfigValue(Config.EMPSTATU, _employeeMasterBo.BaseLocationId == 0 ? "" : Convert.ToString(_employeeMasterBo.BaseLocationId), Utility.UserId());

            return View(_employeeMasterBo);
        }

        #endregion Employee Profile

        #region File Upload

        [HttpPost]
        public JsonResult FileUpload(HttpPostedFileBase file)
        {
            List<string> list = new List<string>();

            if (file != null && file.ContentLength > 0)
            {
                list = UploadUtility.UploadFile(file, Utility.UploadUrl("PROFILE"), "INTERNAL");
                EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();

                if (list != null)
                {
                    employeeMasterRepository.UploadEmployeeProfileImage(list[1], Utility.UserId(), Utility.UserId());
                    string filePath = Path.Combine("..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("PROFILE"), list[3]);
                    Utility.SetSession("FileName", filePath);
                }
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion File Upload

        #region Official Details

        public ActionResult OfficialDetails(EmployeeMasterBo employeeMasterBo)
        {
            employeeMasterBo.EmployeeId = Utility.UserId();
            employeeMasterBo = _employeeProfileRepository.GetOfficialDetails(Utility.UserId());

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            employeeMasterBo.GenderList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Gender");
            return PartialView(employeeMasterBo);
        }

        #endregion Official Details

        #region Personal Details

        public ActionResult PersonalDetails(PersonalDetailsBo personalDetailsBo)
        {
            personalDetailsBo.EmployeeId = Utility.UserId();
            personalDetailsBo = _employeeProfileRepository.GetPersonalDetails(Utility.UserId());
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            // Based on Type
            personalDetailsBo.BloodGroupList = masterDataRepository.SearchMasterDataDropDown("BloodGroup", string.Empty);
            personalDetailsBo.MaritalStatusList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "MaritalStatus");

            // Based on table
            personalDetailsBo.CityList = masterDataRepository.SearchMasterDataDropDown("City", string.Empty);

            //Dependent Details List
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            DependentDetailsBo dependentDetailsBo = new DependentDetailsBo { EmployeeId = Utility.UserId() };

            ViewData["DependentDetails"] = employeeMasterRepository.SearchDependentDetails(dependentDetailsBo);

            return PartialView(personalDetailsBo);
        }

        #endregion Personal Details

        #region Statutory Details

        public ActionResult StatutoryDetails(StatutoryDetailsBo statutoryDetailsBo)
        {
            statutoryDetailsBo.EmployeeId = Utility.UserId();
            statutoryDetailsBo = _employeeProfileRepository.GetStatutoryDetails(Utility.UserId());
            return PartialView(statutoryDetailsBo);
        }

        #endregion Statutory Details

        #region Pay Stub

        public ActionResult Paystub(PayrollBO payrollBo)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            payrollBo.MonthIDList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            payrollBo.YearIDList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);

            payrollBo.MonthID = DateTime.Now.Month;
            payrollBo.YearID = masterDataRepository.GetCurrentYearId(DateTime.Now.Year.ToString());
            return PartialView(payrollBo);
        }

        #endregion Pay Stub

        #region Pay Slip

        public ActionResult EmployeePayslip(int? employeeId, int monthId, int yearId, bool? isProfile)
        {
            if (employeeId == 0 || employeeId == null)
            {
                employeeId = Utility.UserId();
            }

            PayStructureBO payStructureBo = _employeeProfileRepository.GetPayroll(employeeId, monthId, yearId, isProfile);
            if (payStructureBo.Basic != null)
            {
                if (!string.IsNullOrWhiteSpace(payStructureBo.Logo))
                {
                    string imagePath = UploadUtility.DownloadFile(payStructureBo.Logo).FileName;
                    payStructureBo.Logo = "..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("COMPANYLOGO") + imagePath.Split('\\')[imagePath.Split('\\').Length - 1];
                }

                return PartialView(payStructureBo);
            }

            return Json(BOConstants.Payroll_Not_Processed, JsonRequestBehavior.AllowGet);
        }

        #endregion Pay Slip

        #region Dependent Details

        public ActionResult DependentDetails()
        {
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            DependentDetailsBo dependentDetailsBo = new DependentDetailsBo { EmployeeId = Utility.UserId() };

            return PartialView(employeeMasterRepository.SearchDependentDetails(dependentDetailsBo));
        }

        #endregion Dependent Details

        #region Company Assets

        public ActionResult CompanyAssetDetails()
        {
            return PartialView(_employeeProfileRepository.GetCompanyAssets(Utility.UserId()));
        }

        #endregion Company Assets

        #region Get Paystructure

        public ActionResult GetPayStructure_V2(int id, string type)
        {
            return PartialView(_employeeProfileRepository.GetEmployeePayStructure_V2(id, type));
        }

        #endregion Get Paystructure

        #region Crop Image

        public ActionResult CropPhotoImage()
        {
            return PartialView();
        }

        #endregion Crop Image

        #region Declarations

        private EmployeeMasterBo _employeeMasterBo = new EmployeeMasterBo();
        private readonly EmployeeProfileRepository _employeeProfileRepository = new EmployeeProfileRepository();

        #endregion Declarations

        #region Pay Slip_V2

        public ActionResult EmployeePayslip_V2(int? employeeId, int monthId, int yearId)
        {
            if (employeeId == 0 || employeeId == null)
            {
                employeeId = Utility.UserId();
            }

            DataSet dataSet = _employeeProfileRepository.GetPayroll_V2(employeeId, monthId, yearId);
            ViewData.Model = dataSet.Tables[0].AsEnumerable();

            if (dataSet.Tables[0].Rows.Count > 0)
            {
                return PartialView("EmployeePayslip_V2");
            }

            return Json(BOConstants.Payroll_Not_Processed, JsonRequestBehavior.AllowGet);
        }

        #endregion Pay Slip_V2

        #region ExpiryAlert

        public ActionResult GetExpiryAlertDetails(string key)
        {
            return PartialView(_employeeProfileRepository.GetExpiryAlertDetails(key));
        }

        #endregion ExpiryAlert
    }
}