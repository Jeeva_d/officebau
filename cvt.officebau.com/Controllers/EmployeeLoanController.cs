﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeLoanController : Controller
    {
        #region variables

        private readonly EmployeeLoanRepository _employeeLoanRepository = new EmployeeLoanRepository();

        #endregion variables

        #region Loan Request

        public ActionResult LoanRequest()
        {
            Utility.SetSession("MenuRetention", "LOANMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "LOANREQUEST");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Loan Request");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo
            {
                EmployeeId = Utility.UserId(),
                LoanStatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Loan")
            };
            return View(employeeLoanBo);
        }

        public ActionResult LoanList(EmployeeLoanBo employeeLoanBo)
        {
            employeeLoanBo.EmployeeId = Utility.UserId();
            return PartialView(_employeeLoanRepository.GetLoanRequestList(employeeLoanBo));
        }

        public ActionResult CreateLoanRequest(int id)
        {
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo
            {
                EmployeeId = Utility.UserId()
            };
            if (id != 0)
            {
                employeeLoanBo = _employeeLoanRepository.GetLoanRequest(id);
            }
            else
            {
                EmployeeClaimsRepository employeeClaimsRepository = new EmployeeClaimsRepository();
                employeeLoanBo.ApproverName = employeeClaimsRepository.SearchApproverDetail(Utility.UserId(), "Name");
                employeeLoanBo.ApproverId = Convert.ToInt32(employeeClaimsRepository.SearchApproverDetail(Utility.UserId(), "ID"));
                employeeLoanBo.BusinessUnitId = Convert.ToInt32(_employeeLoanRepository.GetBaseLocationId(Utility.UserId()));
                employeeLoanBo.StatusName = string.Empty;
            }

            employeeLoanBo.Gross = _employeeLoanRepository.GetFixedGrossForLoan();
            return PartialView(employeeLoanBo);
        }

        [HttpPost]
        public ActionResult CreateLoanRequest(EmployeeLoanBo employeeLoanBo)
        {
            string message = string.Empty;
 
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.EmployeeLoanRequest, employeeLoanBo.Id == 0 ? "Save" : "Edit");

            if (employeeLoanBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.EmployeeLoanRequest, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employeeLoanBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    message = _employeeLoanRepository.CreateLoanRequest(employeeLoanBo);
                    break;

                case Operations.Update:
                    message = _employeeLoanRepository.UpdateLoanRequest(employeeLoanBo);
                    break;

                case Operations.Delete:
                    employeeLoanBo.IsDeleted = true;
                    message = _employeeLoanRepository.DeleteLoanRequest(employeeLoanBo);
                    break;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeeLoanConfigruation(int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            string configValue = masterDataRepository.GetApplicationConfigValue(Config.LOAN_ELIGIBLECHK, "", employeeId);

            if (configValue == "1")
            {
                return Json(_employeeLoanRepository.GetEmployeeConfiguration(employeeId), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("open/0/" + Convert.ToString(int.MaxValue), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetLoanAmortizationList(int id)
        {
            return PartialView(_employeeLoanRepository.GetLoanAmortizationList(id));
        }

        #endregion Loan Request

        #region Loan Approval

        public ActionResult LoanApproval()
        {
            Utility.SetSession("MenuRetention", "LOANMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "LOANAPPROVAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Loan Approval");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo
            {
                LoanStatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Loan")
            };

            return View(employeeLoanBo);
        }

        public ActionResult SearchLoanApproval(EmployeeLoanBo employeeLoanBo)
        {
            return PartialView(_employeeLoanRepository.GetEmployeeLoanApproveList(employeeLoanBo));
        }

        public ActionResult ApprovalEmployeeLoanDetails(EmployeeLoanBo employeeLoanBo)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            employeeLoanBo = _employeeLoanRepository.GetLoanApproveDetails(employeeLoanBo);
            employeeLoanBo.SettlerList = masterDataRepository.SearchEmpDropDownForCode("Finance", employeeLoanBo.RequesterId);

            return PartialView(employeeLoanBo);
        }

        [HttpPost]
        public ActionResult ApprovalEmployeeLoanDetails(int? settlerId, string remarks, string status, int id)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.LoanApproval, id == 0 ? "Save" : "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_employeeLoanRepository.ManageLoanApprovel(settlerId, remarks, status, id, Utility.UserId()), JsonRequestBehavior.AllowGet);
        }

        #endregion Loan Approval

        #region Loan FinanceApproval

        public ActionResult LoanFinance()
        {
            Utility.SetSession("MenuRetention", "LOANMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "APPROVAL-FINANCE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Loan Approval FINANCE");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo
            {
                LoanStatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Loan")
            };

            return View(employeeLoanBo);
        }

        public ActionResult SearchLoanFinance(EmployeeLoanBo employeeLoanBo)
        {
            return PartialView(_employeeLoanRepository.SearchFinanceApproval(employeeLoanBo));
        }

        public ActionResult ApproveLoanFinance(int loanRequestId)
        {
            EmployeeLoanBo employeeLoanBo = _employeeLoanRepository.ApproveLoanFianance(loanRequestId);
            return PartialView(employeeLoanBo);
        }

        [HttpPost]
        public ActionResult ApproveLoanFinance(int loanRequestId, string remarks, string status)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.LoanFinanceApproval, loanRequestId == 0 ? "Save" : "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_employeeLoanRepository.ManageLoanFinanceApproval(remarks, status, loanRequestId, Utility.UserId()), JsonRequestBehavior.AllowGet);
        }

        #endregion Loan FinanceApproval

        #region Loan Settlement

        public ActionResult LoanSettlement()
        {
            Utility.SetSession("MenuRetention", "LOANMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "SETTLEMENT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Loan Settlement");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo
            {
                LoanStatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Loan")
            };

            int statusId = _employeeLoanRepository.GetLoanStatusId("Loan", "Approved");
            ViewData["SettlementList"] = _employeeLoanRepository.SearchLoanSettlement(statusId);
            return View(employeeLoanBo);
        }

        public ActionResult SettlementList(int? statusId)
        {
            return PartialView(_employeeLoanRepository.SearchLoanSettlement(statusId));
        }

        public ActionResult ManageSettlement(int id, int loanId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            EmployeeLoanBo employeeLoanBo = _employeeLoanRepository.GetLoanSettlement(id, loanId);
            employeeLoanBo.PaymentModeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "PaymentType");
            return PartialView(employeeLoanBo);
        }

        [HttpPost]
        public ActionResult ManageSettlement(EmployeeLoanBo employeeLoanBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.LoanSettlement, employeeLoanBo.Id == 0 ? "Save" : "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_employeeLoanRepository.ManageLoanSettlement(employeeLoanBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Loan Settlement

        #region Loan Foreclosure

        public ActionResult LoanForeclosure()
        {
            Utility.SetSession("MenuRetention", "LOANMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "FORECLOSURE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Loan Foreclosure");

            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo
            {
                LoanStatusList = _employeeLoanRepository.GetStatusCode()
            };

            ViewData["ForeclosureList"] = _employeeLoanRepository.SearchLoanForeclosure(null);
            return View(employeeLoanBo);
        }

        public ActionResult ForeclosureList(int? statusId)
        {
            return PartialView(_employeeLoanRepository.SearchLoanForeclosure(statusId));
        }

        public ActionResult ManageForeclosure(int id, int loanId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeLoanBo employeeLoanBo = _employeeLoanRepository.GetLoanForeclosure(id, loanId);
            employeeLoanBo.PaymentModeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "PaymentType");
            return PartialView(employeeLoanBo);
        }

        [HttpPost]
        public ActionResult ManageForeclosure(EmployeeLoanBo employeeLoanBo)
        {
            return Json(_employeeLoanRepository.ManageLoanForeclosure(employeeLoanBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Loan Foreclosure

        #region Loan Configuration

        /// <summary>
        ///     Main View to load Loan Configuration
        /// </summary>
        /// <returns>View</returns>
        public ActionResult LoanConfiguration()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Loan Configuration");

            return PartialView();
        }

        /// <summary>
        ///     Method to get the Loan Configuration List.
        /// </summary>
        /// <returns>Loan Configuration List</returns>
        public ActionResult SearchLoanConfiguration()
        {
            return PartialView(_employeeLoanRepository.SearchEmployeeLoanConfiguration());
        }

        /// <summary>
        ///     Method to get data for Add / Edit Loan Configuration.
        /// </summary>
        /// <param name="configurationId">ConfigurationID</param>
        /// <returns>Configuration Data</returns>
        public ActionResult CreateLoanConfiguration(int configurationId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo();
            if (!configurationId.Equals(0))
            {
                employeeLoanBo = _employeeLoanRepository.GetEmployeeLoanConfiguration(configurationId);
            }

            employeeLoanBo.BandList = masterDataRepository.SearchMasterDataDropDown("Band", string.Empty);
            employeeLoanBo.GradeList = masterDataRepository.SearchMasterDataDropDown("EmployeeGrade", string.Empty);
            employeeLoanBo.DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty);
            return PartialView(employeeLoanBo);
        }

        /// <summary>
        ///     Method to Save / Update Loan Configuration.
        /// </summary>
        /// <param name="employeeLoanBo">EmployeeLoanBO</param>
        /// <returns>Success or Error Message.</returns>
        [HttpPost]
        public ActionResult CreateLoanConfiguration(EmployeeLoanBo employeeLoanBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.LoanConfiguration, employeeLoanBo.Id == 0 ? "Save" : "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_employeeLoanRepository.ManageEmployeeLoanConfiguration(employeeLoanBo));
        }

        #endregion Loan Configuration

        #region Report

        public ActionResult LoanReport()
        {
            Utility.SetSession("MenuRetention", "LOANMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "REPORTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Loan Report");

            RbsRepository rBsRepository = new RbsRepository();
            return View(rBsRepository.SearchMenuforReportal());
        }

        public ActionResult CompleteLoanReport()
        {
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Complete Loan Report");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeLoanReportBo employeeLoanReportBo = new EmployeeLoanReportBo
            {
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown(),
                //employeeLoanReportBO.StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Loan");
                StatusList = _employeeLoanRepository.GetStatusCode()
            };

            return PartialView(employeeLoanReportBo);
        }

        public ActionResult SearchCompleteLoanReport(EmployeeLoanReportBo employeeLoanReportBo)
        {
            if (employeeLoanReportBo.FromDate == null)
            {
                employeeLoanReportBo.FromDate = DateTime.Now.AddDays(-180);
            }

            if (employeeLoanReportBo.ToDate == null)
            {
                employeeLoanReportBo.ToDate = DateTime.Now;
            }

            return PartialView(_employeeLoanRepository.GetCompleteLoanList(employeeLoanReportBo));
        }

        public ActionResult AutoCompleteLoanApproverList(string approverName)
        {
            return Json(_employeeLoanRepository.GetLoanApproverList(approverName), JsonRequestBehavior.AllowGet);
        }

        public ActionResult RepaymentDetailsList(int id)
        {
            return PartialView(_employeeLoanRepository.GetRepaymentDetailsList(id));
        }

        #endregion Report
    }
}