﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    public class PayrollComponents_V2Controller : Controller
    {
        // GET: Payroll_V2

        #region Varaibles

        private readonly PayrollComponentsRepositoryV2 _payrollComponentsRepositoryV2 = new PayrollComponentsRepositoryV2();

        #endregion Varaibles

        [SessionExpire]
        public ActionResult PayrollComponentList_V2(PayrollComponentsBo_V2 payrollComponentsBo)
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Payroll Components");

            return PartialView(_payrollComponentsRepositoryV2.SearchPayrollComponentList(payrollComponentsBo));
        }

        public ActionResult PayrollComponent_V2(int id)
        {
            return PartialView(_payrollComponentsRepositoryV2.GetPayrollComponent(id));
        }

        [HttpPost]
        public ActionResult ManagePayrollComponent(PayrollComponentsBo_V2 payrollComponentsBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            payrollComponentsBo.MenuCode = MenuCode.PayrollComponent;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), payrollComponentsBo.MenuCode, payrollComponentsBo.Id == 0 ? "Save" : "Edit");
            if (payrollComponentsBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), payrollComponentsBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_payrollComponentsRepositoryV2.Pay_ManagePayrollComponents(payrollComponentsBo));
        }
    }
}