﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class FandFSettlementController : Controller
    {
        #region Variable

        private readonly FandFSettlementRepository _fandFSettlementRepository = new FandFSettlementRepository();

        #endregion Variable

        #region Request

        public ActionResult SearchFandFRequest()
        {
            Utility.SetSession("MenuRetention", "EXITPROCESS");
            Utility.SetSession("SubMenuRetention", "FANDFSETTLEMENTREQUEST");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("F and F Settlement - Request");

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            FandFBo fandFBo = new FandFBo
            {
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "EOS"),
            };

            return View(fandFBo);
        }

        public ActionResult FandFRequestList(FandFBo fandFBo)
        {
            return PartialView(_fandFSettlementRepository.SearchFandFSettlementRequest(fandFBo));
        }

        public ActionResult CreateFandFRequest(FandFBo fandFBoIP)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            FandFBo fandFBo = new FandFBo();

            if (fandFBoIP.FandFComponentDetailsBoList == null)
            {
                fandFBoIP.FandFComponentDetailsBoList = new List<FandFComponentDetailsBo>();
            }

            if (fandFBoIP.Id != 0)
            {
                fandFBo = _fandFSettlementRepository.GetFandFSettlementRequest(fandFBoIP.Id);

                if (fandFBoIP.Operation != "select")
                {
                    if (fandFBoIP != null && fandFBoIP.LastWorkingDate != DateTime.MinValue)
                    {
                        fandFBo.FandFComponentDetailsBoList = _fandFSettlementRepository.GetFandFComponentDetailsList(fandFBo.Id, fandFBo.EmployeeId, fandFBo.TotalDaysInMonth ?? 0, fandFBo.DaysPaid ?? 0, fandFBo.LeaveEncashedDays ?? 0, fandFBoIP.FandFComponentDetailsBoList);
                    }
                    else
                    {
                        fandFBoIP.FandFComponentDetailsBoList = new List<FandFComponentDetailsBo>();
                    }
                }
                else
                {
                    fandFBo.Operation = "";
                    fandFBo.FandFComponentDetailsBoList = _fandFSettlementRepository.GetFandFComponentDetailsList(fandFBo.Id, fandFBo.EmployeeId, fandFBo.TotalDaysInMonth ?? 0, fandFBo.DaysPaid ?? 0, fandFBo.LeaveEncashedDays ?? 0, fandFBoIP.FandFComponentDetailsBoList);
                }
            }
            else
            {
                fandFBo = fandFBoIP;
                if (fandFBoIP.EmployeeId > 0 && (fandFBoIP.TotalDaysInMonth ?? 0) > 0)
                {
                    fandFBo.FandFComponentDetailsBoList = _fandFSettlementRepository.GetFandFComponentDetailsList(fandFBoIP.Id, fandFBoIP.EmployeeId, fandFBoIP.TotalDaysInMonth ?? 0, fandFBoIP.DaysPaid ?? 0, fandFBoIP.LeaveEncashedDays ?? 0, fandFBoIP.FandFComponentDetailsBoList);
                }
                else
                {
                    fandFBo.FandFComponentDetailsBoList = new List<FandFComponentDetailsBo>();
                }
            }

            fandFBo.HRApproverList = masterDataRepository.GetEmployeeNameBasedonBu(Utility.UserId());

            return PartialView(fandFBo);
        }

        [HttpPost]
        public ActionResult ManageFandFRequest(FandFBo fandFBo)
        {
            string message = string.Empty;
            message = _fandFSettlementRepository.ManageFandFSettlementRequest(fandFBo);

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployee(searchName), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmployeeDetails(int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(_fandFSettlementRepository.GetEmployeeDetails(employeeId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetYearofServices(int employeeId, DateTime dateOfJoining, DateTime lastWorkingDate)
        {
            return Json(_fandFSettlementRepository.GetFandFDaysPaid(employeeId, dateOfJoining, lastWorkingDate), JsonRequestBehavior.AllowGet);
        }

        public ActionResult FandFComponentDetailsList(int id, int employeeId, int TotalDaysInMonth, int DaysPaid, int LeaveEncashedDays)
        {
            List<FandFComponentDetailsBo> FandFComponentDetailsBoList = new List<FandFComponentDetailsBo>();
            return PartialView(_fandFSettlementRepository.GetFandFComponentDetailsList(id, employeeId, TotalDaysInMonth, DaysPaid, LeaveEncashedDays, FandFComponentDetailsBoList));
        }

        #endregion Request

        #region HR Approve

        public ActionResult SearchFandFHRApprove()
        {
            Utility.SetSession("MenuRetention", "EXITPROCESS");
            Utility.SetSession("SubMenuRetention", "FANDFSETTLEMENT-HR");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("F and F Settlement - HR");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            FandFBo fandFBo = new FandFBo
            {
                HRApproverId = Utility.UserId(),
                StatusId = masterDataRepository.GetStatusId("EOS", "Pending"),
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "EOS"),
            };

            return View(fandFBo);
        }

        public ActionResult FandFHRApprovalList(FandFBo fandFBo)
        {
            fandFBo.DomainId = Utility.DomainId();
            fandFBo.UserId = Utility.UserId();

            return PartialView(_fandFSettlementRepository.SearchFandFHRApprove(fandFBo));
        }

        public ActionResult HRApproveFandF(FandFBo fandFBo)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            fandFBo = _fandFSettlementRepository.GetFandFSettlementRequest(fandFBo.Id);

            if (fandFBo != null)
            {
                if (fandFBo.FandFComponentDetailsBoList == null)
                {
                    fandFBo.FandFComponentDetailsBoList = new List<FandFComponentDetailsBo>();
                }
                fandFBo.FandFComponentDetailsBoList = _fandFSettlementRepository.GetFandFComponentDetailsList(fandFBo.Id, fandFBo.EmployeeId, fandFBo.TotalDaysInMonth ?? 0, fandFBo.DaysPaid ?? 0, fandFBo.LeaveEncashedDays ?? 0, fandFBo.FandFComponentDetailsBoList);
            }

            return PartialView(fandFBo);
        }

        [HttpPost]
        public ActionResult ApproveFandFSettlement(FandFBo fandFBo)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            fandFBo.UserId = Utility.UserId();
            fandFBo.DomainId = Utility.DomainId();
            fandFBo.HRApproverId = Utility.UserId();

            string message = _fandFSettlementRepository.FandFHRApprove(fandFBo);
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion  HR Approve
    }
}