﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    public class CompanyPayStructure_V2Controller : Controller
    {
        #region variables

        private readonly CompanyPayStructureRepositoryV2 _repository = new CompanyPayStructureRepositoryV2();

        #endregion variables

        [SessionExpire]
        public ActionResult CompanyPayStructureList()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Company PayStructure");

            return PartialView(_repository.SearchCompanyPayStructure());
        }

        public ActionResult CreateCompanyPayStructure(int id)
        {
            return PartialView(_repository.GetCompanyPayStructure(id));
        }

        [HttpPost]
        public ActionResult CreateCompanyPayStructure(CompanyPayStructureBo_V2 companyPayStructureBoV2)
        {
            companyPayStructureBoV2.MenuCode = MenuCode.CompanyPay;
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), companyPayStructureBoV2.MenuCode, companyPayStructureBoV2.Id == 0 ? "Save" : "Edit");

            if (companyPayStructureBoV2.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), companyPayStructureBoV2.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_repository.ManageCompanyPayStructure(companyPayStructureBoV2));
        }

        public ActionResult AddComponent(int id)
        {
            return PartialView(_repository.SearchCompanyPayStructureComponents(id));
        }

        [HttpPost]
        public ActionResult AddComponent(IList<CompanyPayStructureBo_V2> companyPayStructureBos)
        {
            RbsRepository rbsRepository = new RbsRepository();
            return !rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.CompanyPay, "Save") ? Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet)
                                                                                              : Json(_repository.ManageCompanyPayStructureComponents(companyPayStructureBos));
        }
    }
}