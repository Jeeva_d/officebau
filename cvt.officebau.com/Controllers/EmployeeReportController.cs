﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Data;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeReportController : Controller
    {
        #region Employee Report

        public ActionResult EmployeeReport()
        {
            Utility.SetSession("MenuRetention", "DASHBOARDANDREPORTS");
            Utility.SetSession("SubMenuRetention", "REPORTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Report");

            RbsRepository rBsRepository = new RbsRepository();
            return View(rBsRepository.SearchMenuforReportal());
        }

        #endregion Employee Report

        #region variable

        private readonly EmployeeMasterRepository _employeeMasterRepository = new EmployeeMasterRepository();

        #endregion variable

        #region Joining Report

        public ActionResult EmployeeJoiningReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeReportBo employeeReportBo = new EmployeeReportBo
            {
                RegionList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", ""),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(employeeReportBo);
        }

        public ActionResult EmployeeJoiningList(EmployeeReportBo employeeReportBo)
        {
            employeeReportBo.DomainId = Utility.DomainId();
            return PartialView(_employeeMasterRepository.SearchEmployeeJoinReport(employeeReportBo));
        }

        #endregion Joining Report

        #region Relieving Report

        public ActionResult EmployeeRelievingReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeReportBo employeeReportBo = new EmployeeReportBo
            {
                RegionList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", ""),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(employeeReportBo);
        }

        public ActionResult EmployeeRelievingList(EmployeeReportBo employeeReportBo)
        {
            employeeReportBo.DomainId = Utility.DomainId();
            return PartialView(_employeeMasterRepository.SearchEmployeeRelieveReport(employeeReportBo));
        }

        #endregion Relieving Report

        #region Employee Complete Report

        public ActionResult EmployeeCompleteReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty),
                DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(reportInputParamBo);
        }

        public ActionResult EmployeeCompleteReportList(ReportInputParamBo reportInputParamBo)
        {
            DataTable dt = _employeeMasterRepository.SearchEmployeeCompleteReport(reportInputParamBo);
            return PartialView(dt);
        }

        #endregion Employee Complete Report

        #region Employee Dependent Report

        public ActionResult EmployeeDependentReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty),
                DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(reportInputParamBo);
        }

        public ActionResult EmployeeDependentList(ReportInputParamBo reportInputParamBo)
        {
            DataTable dt = _employeeMasterRepository.SearchEmployeeDependentListReport(reportInputParamBo);
            return PartialView(dt);
        }

        #endregion Employee Dependent Report

        #region Employee Asset Report

        public ActionResult EmployeeAssetReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty),
                DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(reportInputParamBo);
        }

        public ActionResult EmployeeAssetList(ReportInputParamBo reportInputParamBo)
        {
            DataTable dt = _employeeMasterRepository.SearchEmployeeAssetReport(reportInputParamBo);
            return PartialView(dt);
        }

        #endregion Employee Asset Report

        #region Employee New Hires Report

        public ActionResult EmployeeNewHiresReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty),
                DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(reportInputParamBo);
        }

        public ActionResult EmployeeNewHiresList(ReportInputParamBo reportInputParamBo)
        {
            DataTable dt = _employeeMasterRepository.SearchEmployeeNewHiresReport(reportInputParamBo);
            return PartialView(dt);
        }

        #endregion Employee New Hires Report

        #region Employee Asset Defaulters Report

        public ActionResult EmployeeAssetDefaulters()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty),
                DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(reportInputParamBo);
        }

        public ActionResult EmployeeAssetDefaultersList(ReportInputParamBo reportInputParamBo)
        {
            DataTable dataTable = _employeeMasterRepository.SearchEmployeeDefaultersReport(reportInputParamBo);
            return PartialView(dataTable);
        }

        #endregion Employee Asset Defaulters Report

        #region Employee Skill Set Report

        public ActionResult EmployeeSkillSetReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                SkillsList = masterDataRepository.SearchMasterDataDropDown("Skills", string.Empty),
                DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(reportInputParamBo);
        }

        public ActionResult EmployeeSkillSetList(ReportInputParamBo reportInputParamBo)
        {
            DataTable dt = _employeeMasterRepository.SearchEmployeeSkillSetReport(reportInputParamBo);
            return PartialView(dt);
        }

        #endregion Employee Skill Set Report

        #region Employee Skill Matrix Report

        public ActionResult EmployeeSkillMatrixReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                SkillsList = masterDataRepository.SearchMasterDataDropDown("Skills", string.Empty),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(reportInputParamBo);
        }

        public ActionResult EmployeeSkillMatrixList(ReportInputParamBo reportInputParamBo)
        {
            return PartialView(_employeeMasterRepository.SearchEmployeeSkillMatrix(reportInputParamBo));
        }

        public ActionResult GetEmployeeSkillDetails(int skillsId, int levelId, bool isActive)
        {
            return PartialView(_employeeMasterRepository.GetEmployeeSkillsDetailsList(skillsId, levelId, isActive));
        }

        #endregion Employee Skill Matrix Report

        #region Travel Details Report

        public ActionResult TravelDetailsReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty),
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Travel"),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return PartialView(reportInputParamBo);
        }

        public ActionResult TravelDetailsReportList(ReportInputParamBo reportInputParamBo)
        {
            return PartialView(_employeeMasterRepository.TravelDetailsReportList(reportInputParamBo));
        }

        public ActionResult AutoCompleteEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployee(searchName), JsonRequestBehavior.AllowGet);
        }

        #endregion Travel Details Report
    }
}