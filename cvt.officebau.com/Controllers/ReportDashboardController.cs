﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class ReportDashboardController : Controller
    {
        #region Variables

        private ReportDashboardBo _reportDashboardBo = new ReportDashboardBo();
        private readonly ReportDashboardRepository _reportDashboardRepository = new ReportDashboardRepository();

        #endregion Variables

        // GET: ReportDashboard

        #region Report Dashboard

        public ActionResult SearchReportDashboard()
        {
            Utility.SetSession("MenuRetention", "DASHBOARD");
            Utility.SetSession("SubMenuRetention", "REPORTDASHBOARD");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Report Dashboard");

            _reportDashboardBo = _reportDashboardRepository.GetDashboardTiles();
            return View(_reportDashboardBo);
        }

        public ActionResult ReportDashboard(ReportDashboardBo reportDashboardBo)
        {
            return PartialView(_reportDashboardRepository.SearchReportDashboard(reportDashboardBo));
        }

        public ActionResult ReportDashboardBarchart(ReportDashboardBo reportDashboardBo)
        {
            return Json(_reportDashboardRepository.SearchReportDashboardBarchart(reportDashboardBo), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
