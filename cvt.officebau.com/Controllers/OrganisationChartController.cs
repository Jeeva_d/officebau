﻿using cvt.officebau.com.Services;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class OrganisationChartController : Controller
    {
        private readonly OrganisationChartRepository organisationChartRepository = new OrganisationChartRepository();

        public ActionResult OrganisationChartDesignation()
        {
            return PartialView();
        }

        public ActionResult OrganisationChartReportingWise()
        {
            //return PartialView(organisationChartRepository.OrganisationChartReportingTo());
            return PartialView();
        }

        public ActionResult DrawOrganisationChartReportingWise(bool? isActive)
        {
            return PartialView(organisationChartRepository.OrganisationChartReportingTo(isActive));
        }

        public ActionResult OrganisationChartDesignationWise(string param)
        {
            return Json(organisationChartRepository.OrganisationChartDesignationWise(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult OrganisationChartReportingTo()
        {
            return Json(organisationChartRepository.OrganisationChartReportingTo(false), JsonRequestBehavior.AllowGet);
        }
    }
}
