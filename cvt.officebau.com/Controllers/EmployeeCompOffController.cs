﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeCompOffController : Controller
    {
        #region Variables

        private readonly EmployeeCompOffRepository _employeeCompOffRepository = new EmployeeCompOffRepository();

        #endregion Variables

        #region Comp Off Request

        public ActionResult CompOffRequest()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "COMPOFFREQUEST");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Comp Off Request");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeCompOffBo employeeCompOffBo = new EmployeeCompOffBo
            {
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave"),
                EmployeeList = masterDataRepository.GetEmployeeDropDown("EmployeeMaster", Utility.UserId(), true)
            };
            if (Utility.TmpUserId() > 0)
            {
                employeeCompOffBo.EmployeeId = Utility.TmpUserId();
                employeeCompOffBo.RequestBy = "Other";
            }
            else
            {
                Utility.SetSession("TmpUserID", null);
                employeeCompOffBo.EmployeeId = Utility.UserId();
                employeeCompOffBo.RequestBy = "Self";
            }
            if (masterDataRepository.HasOtherUserAccess(Utility.UserId(), "Comp-Off request"))
            {
                employeeCompOffBo.HasHRApplicationRole = true;
            }

            return View(employeeCompOffBo);
        }

        public ActionResult SearchCompOffRequest(EmployeeCompOffBo employeeCompOffBo)
        {
            Utility.SetSession("TmpUserID", employeeCompOffBo.EmployeeId);
            if (employeeCompOffBo.EmployeeId == 0)
            {
                employeeCompOffBo.EmployeeId = Utility.UserId();
            }
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();

            return PartialView(employeeCompOffRepository.SearchCompOffRequest(employeeCompOffBo));
        }

        public ActionResult CreateCompOffRequest(int id, int employeeId)
        {
            EmployeeCompOffBo employeeCompOffBo = new EmployeeCompOffBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            if (id != 0)
            {
                employeeCompOffBo = _employeeCompOffRepository.GetCompOffRequest(id);
            }
            else
            {
                employeeCompOffBo.ApproverName = _employeeCompOffRepository.GetApprover(employeeId, "Name");
                employeeCompOffBo.ApproverId = Convert.ToInt32(_employeeCompOffRepository.GetApprover(employeeId, "ID"));
            }
            employeeCompOffBo.EmployeeId = employeeId;
            if (Utility.TmpUserId() > 0)
            {
                employeeCompOffBo.EmployeeName = masterDataRepository.GetEmployeeDetails(employeeId).FullName;
            }
            return PartialView(employeeCompOffBo);
        }

        [HttpPost]
        public ActionResult CreateCompOffRequest(EmployeeCompOffBo employeeCompOffBo)
        {
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();
            RbsRepository rBsRepository = new RbsRepository();
            employeeCompOffBo.MenuCode = MenuCode.CompOffRequest;

            bool hasAccess = rBsRepository.IsAuthorized(Utility.UserId(), employeeCompOffBo.MenuCode, employeeCompOffBo.Id == 0 ? "Save" : "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(employeeCompOffRepository.ManageCompOffRequest(employeeCompOffBo), JsonRequestBehavior.AllowGet);
        }

        public string GetDuration(DateTime date, int employeeId)
        {
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();
            return Convert.ToString(employeeCompOffRepository.CalculateDuration(employeeId, date));
        }

        #endregion Comp Off Request

        #region Comp Off Approval

        public ActionResult CompOffApprove()
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "COMPOFFAPPROVAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Comp Off Approval");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();
            EmployeeCompOffBo employeeCompOffBo = new EmployeeCompOffBo
            {
                ApproverId = Utility.UserId(),
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave"),
                StatusId = employeeCompOffRepository.GetStatusId("Leave", "Pending")
            };

            return View(employeeCompOffBo);
        }

        public ActionResult SearchApproverCompOff(EmployeeCompOffBo employeeCompOffBo)
        {
            return PartialView(_employeeCompOffRepository.SearchCompOffApproval(employeeCompOffBo));
        }

        public ActionResult ApproveCompOffRequest(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeCompOffBo employeeCompOffBo = _employeeCompOffRepository.GetCompOffRequest(id);
            employeeCompOffBo.ApproverList = masterDataRepository.GetEmployeeNameBasedonBu(Utility.UserId());
            return PartialView(employeeCompOffBo);
        }

        [HttpPost]
        public ActionResult ApproveCompOffRequest(EmployeeCompOffBo employeeCompOffBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.CompOffApproval, employeeCompOffBo.Id == 0 ? "Save" : "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(employeeCompOffRepository.ManageCompOffApproval(employeeCompOffBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Comp Off Approval

        #region Comp Off HR Approval

        public ActionResult SearchCompOffHrApproval(string menuCode)
        {
            Utility.SetSession("MenuRetention", "LEAVEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "COMPOFFHRAPPROVAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Comp Off HR Approval");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();
            EmployeeCompOffBo employeeCompOffBo = new EmployeeCompOffBo
            {
                ApproverId = Utility.UserId(),
                StatusList = masterDataRepository.SearchMasterDataDropDown("Status", "Leave"),
                StatusId = employeeCompOffRepository.GetStatusId("Leave", "Pending")
            };

            return View(employeeCompOffBo);
        }

        public ActionResult CompOffHrApprovalList(EmployeeCompOffBo employeeCompOffBo)
        {
            return PartialView(_employeeCompOffRepository.CompOffHrApprovalList(employeeCompOffBo));
        }

        public ActionResult CompOffHrApproval(int id)
        {
            return PartialView(_employeeCompOffRepository.GetCompOffRequest(id));
        }

        [HttpPost]
        public ActionResult CompOffHrApproval(EmployeeCompOffBo employeeCompOffBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.CompOffApproval, employeeCompOffBo.Id == 0 ? "Save" : "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            string message = employeeCompOffRepository.ManageCompOffHrApproval(employeeCompOffBo);
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Comp Off HR Approval
    }
}