﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class JournelController : Controller
    {
        #region Variables

        private readonly JournelRepository _journelRepository = new JournelRepository();

        #endregion Variables

        #region Journel

        public ActionResult JournelDetail(string menuCode, int? id, string screenType, bool? isNewRecord)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "VOUCHER");
            Utility.SetSession("SubMenuRetention", "JOURNAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Journel");

            JournelBO journalBo = new JournelBO();
            List<JournelBO> journalList = new List<JournelBO>();
            if (isNewRecord ?? false)
            {
                journalList.Insert(0, new JournelBO { Id = id ?? 0, IsNewRecord = isNewRecord, JournelNo = string.Empty, Amount = null, Date = null, CreatedOn = null });
            }
            else
            {
                journalList = _journelRepository.SearchJournal(journalBo);
            }

            return View(journalList);
        }

        public ActionResult Journel(int id, string menuCode)
        {
            JournelDetailBO journelDetailBo = new JournelDetailBO();
            JournelBO journelBo = _journelRepository.GetJournal(id);
            journelBo.MenuCode = menuCode;
            if (id == 0)
            {
                journelBo.JournelDetail = new List<JournelDetailBO> { journelDetailBo };
                journelDetailBo.LedgerProductList = _journelRepository.Getdropdown_ledgerproduct();
                journelDetailBo.LedgerProductList.AddRange(_journelRepository.GetPartyName());
                journelBo.MenuCode = menuCode;
            }

            return PartialView(journelBo);
        }

        [HttpPost]
        public ActionResult Journel(JournelBO journalBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (journalBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), journalBo.MenuCode, journalBo.Id == 0 ? "Save" : "Edit");
            }

            if (journalBo != null && journalBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), journalBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            return Json(_journelRepository.ManageJournel(journalBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Journel
    }
}