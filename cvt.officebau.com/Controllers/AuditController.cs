﻿using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class AuditController : Controller
    {
        public ActionResult Audit()
        {
            MasterDataRepository masterRepository = new MasterDataRepository();

            _auditBo.AuditList = masterRepository.SearchMasterDataDropDown("TriggerAudit", string.Empty);
            return PartialView(_auditBo);
        }

        public ActionResult SearchAuditDetails(string tablename)
        {
            return PartialView(_technicalAuditRepository.SearchAudit(tablename));
        }

        #region

        private readonly AuditBo _auditBo = new AuditBo();
        private readonly AuditRepository _technicalAuditRepository = new AuditRepository();

        #endregion
    }
}