﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeMasterController : Controller
    {
        #region Duplicate Name Check

        public ActionResult EmployeeLoginCodeCheck(string loginCode, int employeeId)
        {
            string userMessage = loginCode.Trim() == string.Empty ? "1" : _employeeMasterRepository.EmployeeLoginCodeCheck(loginCode, "EmployeeMaster", employeeId);

            return Json(userMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion Duplicate Name Check

        #region OfferLetter

        public ActionResult OfferLetter(int employeeId)
        {
            EmployeeMasterBo employeeMasterBo = _employeeMasterRepository.GetOfferLetterDetails(employeeId);
            if (employeeMasterBo.Id != 0)
            {
                return PartialView(employeeMasterBo);
            }

            return Json("0", JsonRequestBehavior.AllowGet);
        }

        #endregion OfferLetter

        #region variable

        private readonly EmployeeMasterRepository _employeeMasterRepository = new EmployeeMasterRepository();
        private string _message = string.Empty;

        #endregion variable

        #region Employee Master Details

        public ActionResult SearchEmployeeMaster()
        {
            Utility.SetSession("MenuRetention", "EMPLOYEEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "EMPLOYEEMASTER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee Master");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            GetApplicationConfiguration getApplicationConfiguration = new GetApplicationConfiguration();
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo
            {
                IsActive = false,
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown(),
                EmploymentTypeList = masterDataRepository.SearchMasterDataDropDown("EmployementType", string.Empty),
                DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty),
                DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty),
                FunctionalList = masterDataRepository.SearchMasterDataDropDown("Functional", string.Empty)
            };

            employeeMasterBo.IsStatutoryRequired = getApplicationConfiguration.GetApplicationConfigValue(Config.EMPSTATU, employeeMasterBo.BaseLocationId == 0 ? "" : Convert.ToString(employeeMasterBo.BaseLocationId), Utility.UserId());

            return View(employeeMasterBo);
        }

        public ActionResult EmployeeMaster(EmployeeMasterBo employeeMasterBo)
        {
            employeeMasterBo.EmployeeId = Utility.UserId();
            employeeMasterBo.IsActive = employeeMasterBo.IsActive;
            return PartialView(_employeeMasterRepository.SearchEmployeeMasterList(employeeMasterBo));
        }

        public ActionResult CreateNewEmployee(int id)
        {
            GetApplicationConfiguration getApplicationConfiguration = new GetApplicationConfiguration();
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo
            {
                Id = id
            };

            employeeMasterBo.IsStatutoryRequired = getApplicationConfiguration.GetApplicationConfigValue(Config.EMPSTATU, employeeMasterBo.BaseLocationId == 0 ? "" : Convert.ToString(employeeMasterBo.BaseLocationId), Utility.UserId());

            return PartialView(employeeMasterBo);
        }

        public ActionResult EmployeeMasterDetails(int employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            GetApplicationConfiguration getApplicationConfiguration = new GetApplicationConfiguration();
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();
            if (employeeId != 0)
            {
                employeeMasterBo = _employeeMasterRepository.GetEmployeeMaster(employeeId);
                Utility.SetSession("EmployeeName", employeeMasterBo.FirstName);
                employeeMasterBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            }
            else
            {
                employeeMasterBo.BusinessUnitList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", "Region");
            }

            if (!string.IsNullOrWhiteSpace(employeeMasterBo.FileName))
            {
                employeeMasterBo.FileName = Path.Combine("..\\Images\\" + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("PROFILE"), employeeMasterBo.FileName);
            }
            else
            {
                if ((employeeMasterBo.Gender ?? "").ToUpper() == "FEMALE")
                {
                    employeeMasterBo.FileName = "..\\Images\\profile-avatar_female.png";
                }
                else
                {
                    employeeMasterBo.FileName = "..\\Images\\profile-avatar.png";
                }
            }

            // Based on Type
            employeeMasterBo.PrefixList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Prefix");
            employeeMasterBo.GenderList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Gender");
            employeeMasterBo.JobTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Job");
            employeeMasterBo.EmploymentTypeList = masterDataRepository.SearchMasterDataDropDown("EmployementType", string.Empty);
            employeeMasterBo.JobTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Job");
            employeeMasterBo.JobTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Job");
            employeeMasterBo.JobTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Job");

            // Based on table
            employeeMasterBo.DepartmentList = masterDataRepository.SearchMasterDataDropDown("Department", string.Empty);
            employeeMasterBo.DesignationList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty);
            employeeMasterBo.RegionList = masterDataRepository.SearchMasterDataDropDown("BusinessUnit", string.Empty);
            employeeMasterBo.GradeList = masterDataRepository.SearchMasterDataDropDown("EmployeeGrade", string.Empty);
            employeeMasterBo.BandList = masterDataRepository.SearchMasterDataDropDown("Band", string.Empty);
            employeeMasterBo.FunctionalList = masterDataRepository.SearchMasterDataDropDown("Functional", string.Empty);
            employeeMasterBo.ReasonList = masterDataRepository.SearchMasterDataDropDown("ExitReason", string.Empty);
            employeeMasterBo.ContractorList = masterDataRepository.SearchMasterDataDropDown("Contract", string.Empty);
            employeeMasterBo.EmploymentTypeList = masterDataRepository.SearchMasterDataDropDown("EmployementType", string.Empty);

            //UH Approval Workflow
            employeeMasterBo.IsUhApprovalRequired = getApplicationConfiguration.GetApplicationConfigValue(Config.UHAPPREQ, employeeMasterBo.BaseLocationId == 0 ? "" : Convert.ToString(employeeMasterBo.BaseLocationId), Utility.UserId());
            return PartialView(employeeMasterBo);
        }

        [HttpPost]
        public ActionResult CreateNewEmployee(EmployeeMasterBo employeeMasterBo)
        {
            employeeMasterBo.ModifiedBy = Utility.UserId();
            employeeMasterBo.DomainId = Utility.DomainId();
            employeeMasterBo.MenuCode = MenuCode.EmployeeMaster;
            RbsRepository rbsRepository = new RbsRepository();

            if (!string.IsNullOrWhiteSpace(employeeMasterBo.BusinessUnitIDs))
            {
                employeeMasterBo.BusinessUnitIDs = "," + employeeMasterBo.BusinessUnitIDs + ",";
            }
            else
            {
                employeeMasterBo.BusinessUnitIDs = "," + employeeMasterBo.BaseLocationId + ",";
            }

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeMasterBo.MenuCode, employeeMasterBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employeeMasterBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    employeeMasterBo.CreatedBy = Utility.UserId();
                    employeeMasterBo.IsActive = false;
                    employeeMasterBo.HasAccess = true;
                    _message = _employeeMasterRepository.CreateEmployee(employeeMasterBo);
                    Utility.SetSession("EmployeeName", employeeMasterBo.FirstName);
                    break;

                case Operations.Update:
                    employeeMasterBo.HasAccess = true;
                    _message = _employeeMasterRepository.UpdateEmployee(employeeMasterBo);
                    break;

                case Operations.Delete:
                    employeeMasterBo.IsDeleted = true;
                    employeeMasterBo.HasAccess = false;
                    _message = _employeeMasterRepository.DeleteEmployee(employeeMasterBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteEmployee(string searchName, int? employeeId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployeebasedonAccess(searchName, employeeId == 0 ? null : employeeId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBusinessUnitDependOnRegion(int? regionId)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.GetBusinessUnitDependOnRegion(regionId), JsonRequestBehavior.AllowGet);
        }

        #endregion Employee Master Details

        #region Employee Personal Details

        public ActionResult EmployeePersonalDetails(int employeeId)
        {
            PersonalDetailsBo personalDetailsBo = new PersonalDetailsBo();

            if (employeeId != 0)
            {
                personalDetailsBo = _employeeMasterRepository.GetEmployeeDetails(employeeId);
            }

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            // Based on Type
            personalDetailsBo.BloodGroupList = masterDataRepository.SearchMasterDataDropDown("BloodGroup", string.Empty);
            personalDetailsBo.MaritalStatusList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "MaritalStatus");

            // Based on table
            personalDetailsBo.CountryList = masterDataRepository.SearchMasterDataDropDown("Country", string.Empty);
            personalDetailsBo.StateList = masterDataRepository.SearchMasterDataDropDown("state", string.Empty);
            personalDetailsBo.CityList = masterDataRepository.SearchMasterDataDropDown("City", string.Empty);
            personalDetailsBo.GenderList = masterDataRepository.SearchMasterDataDropDown("EmployeeGrade", string.Empty);
            //personalDetailsBO.EthnicityList = masterDataRepository.SearchMasterDataDropDown("EthniCity", string.Empty);

            return PartialView(personalDetailsBo);
        }

        [HttpPost]
        public ActionResult EmployeePersonalDetails(PersonalDetailsBo personalDetailsBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            personalDetailsBo.Operation = personalDetailsBo.PersonalDetailId == 0 ? Operations.Insert : Operations.Update;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.EmployeeMaster, personalDetailsBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (personalDetailsBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    _message = _employeeMasterRepository.CreateEmployeePersonalDetails(personalDetailsBo);
                    break;

                case Operations.Update:
                    _message = _employeeMasterRepository.UpdateEmployeePersonalDetails(personalDetailsBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Duplicatecheck(string employeeCode, int bu, int employeeId)
        {
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo
            {
                UserMessage = employeeCode.Trim() == string.Empty
                    ? "1"
                    : _employeeMasterRepository.DuplicateCheck(employeeCode, bu, employeeId)
            };

            return Json(employeeMasterBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FileUpload(HttpPostedFileBase file, int employeeId)
        {
            List<string> list = new List<string>();

            if (file != null && file.ContentLength > 0)
            {
                list = UploadUtility.UploadFile(file, Utility.UploadUrl("PROFILE"), "INTERNAL");
            }

            return Json(list[1], JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDesignationBandGrade(int designationId)
        {
            List<string> list = new List<string>();
            EmployeeMasterBo employeeMasterBo = _employeeMasterRepository.GetDesignationBandGrade(designationId);
            list.Add(employeeMasterBo == null ? string.Empty : employeeMasterBo.GradeId.ToString());
            list.Add(employeeMasterBo == null ? string.Empty : employeeMasterBo.GradeName);
            list.Add(employeeMasterBo == null ? string.Empty : employeeMasterBo.BandId.ToString());
            list.Add(employeeMasterBo == null ? string.Empty : employeeMasterBo.BandName);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckDateOfBirth(string date)
        {
            string result = "true";

            if (date != "")
            {
                DateTime input = Convert.ToDateTime(date);

                if (input <= DateTime.Now.AddYears(-18))
                {
                    result = "false";
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion Employee Personal Details

        #region Statutory Details

        public ActionResult EmployeeBankDetails(int employeeId)
        {
            StatutoryDetailsBo statutoryDetailsBo = new StatutoryDetailsBo();
            if (employeeId != 0)
            {
                statutoryDetailsBo = _employeeMasterRepository.GetEmployeeBankDetails(employeeId);
            }

            return PartialView(statutoryDetailsBo);
        }

        [HttpPost]
        public ActionResult EmployeeBankDetails(StatutoryDetailsBo statutoryDetailsBo)
        {
            statutoryDetailsBo.ModifiedBy = Utility.UserId();
            statutoryDetailsBo.DomainId = Utility.DomainId();
            statutoryDetailsBo.MenuCode = MenuCode.EmployeeMaster;
            if (!string.IsNullOrWhiteSpace(statutoryDetailsBo.PanNo))
            {
                statutoryDetailsBo.PanNo = statutoryDetailsBo.PanNo.ToUpper();
            }

            RbsRepository rbsRepository = new RbsRepository();

            statutoryDetailsBo.Operation = statutoryDetailsBo.StatutoryDetailId == 0 ? Operations.Insert : Operations.Update;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), statutoryDetailsBo.MenuCode, statutoryDetailsBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (statutoryDetailsBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    statutoryDetailsBo.CreatedBy = Utility.UserId();
                    _message = _employeeMasterRepository.CreateEmployeeBankDetails(statutoryDetailsBo);
                    break;

                case Operations.Update:
                    _message = _employeeMasterRepository.UpdateEmployeeBankDetails(statutoryDetailsBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        #endregion Statutory Details

        #region Documents

        public ActionResult EmployeeDocuments(int employeeId)
        {
            DocumentDetailsBo documentDetailsBo = new DocumentDetailsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            documentDetailsBo.EmployeeId = employeeId;
            ViewData["DocumentList"] = _employeeMasterRepository.GetEmployeeDocumentsList(documentDetailsBo);
            documentDetailsBo.DocumentTypeList = masterDataRepository.SearchMasterDataDropDown("DocumentType", string.Empty);
            return PartialView(documentDetailsBo);
        }

        [HttpPost]
        public JsonResult SaveEmployeeDocument(DocumentDetailsBo documentDetailsBo)
        {
            documentDetailsBo.ModifiedBy = Utility.UserId();
            documentDetailsBo.DomainId = Utility.DomainId();
            documentDetailsBo.MenuCode = MenuCode.EmployeeMaster;
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), documentDetailsBo.MenuCode, documentDetailsBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (documentDetailsBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    documentDetailsBo.CreatedBy = Utility.UserId();
                    documentDetailsBo.IsDeleted = false;
                    _message = _employeeMasterRepository.CreateEmployeeDocumentDetails(documentDetailsBo);
                    break;

                case Operations.Delete:
                    _message = _employeeMasterRepository.DeleteEmployeeDocumentDetails(documentDetailsBo);
                    string uploadFolder = ConfigurationManager.AppSettings["UploadPath"] + Utility.GetSession("CompanyName") + @"\HRMS\EMP_DOCUMENTS\" + Utility.GetSession("EmployeeName").Replace("  ", string.Empty) + "\\" + documentDetailsBo.FileName;

                    if (System.IO.File.Exists(uploadFolder))
                    {
                        System.IO.File.Delete(uploadFolder);
                    }

                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DocumentList(DocumentDetailsBo documentDetailsBo)
        {
            return PartialView(_employeeMasterRepository.GetEmployeeDocumentsList(documentDetailsBo));
        }

        //File Upload
        public JsonResult Upload(HttpPostedFileBase file, string screenType = "")
        {
            List<string> list = new List<string>();
            if (file != null)
            {
                if (screenType.ToUpper() == "EMPSKILLS")
                    list = UploadUtility.UploadFile(file, Utility.UploadUrl("EMPSKILLS"));
                else
                    list = UploadUtility.UploadFile(file, Utility.UploadUrl("EMPDOCS"));
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion Documents

        #region Other Details Tab

        public ActionResult EmployeeOtherDetails(int employeeId)
        {
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();
            if (employeeId != 0)
            {
                employeeMasterBo = _employeeMasterRepository.GetEmployeeOtherDetails(employeeId);
            }

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            employeeMasterBo.ApplicationRoleList = masterDataRepository.SearchMasterDataDropDown("ApplicationRole", string.Empty);
            employeeMasterBo.StartScreenList = masterDataRepository.SearchMasterDataDropDownByCode("RBSMenu", string.Empty);
            return PartialView(employeeMasterBo);
        }

        [HttpPost]
        public JsonResult SaveEmployeeOtherDetails(EmployeeMasterBo employeeMasterBo)
        {
            employeeMasterBo.ModifiedBy = Utility.UserId();
            employeeMasterBo.DomainId = Utility.DomainId();
            employeeMasterBo.MenuCode = MenuCode.EmployeeMaster;
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employeeMasterBo.MenuCode, employeeMasterBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employeeMasterBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    employeeMasterBo.CreatedBy = Utility.UserId();
                    _message = _employeeMasterRepository.CreateEmployeeOtherDetails(employeeMasterBo);
                    break;

                case Operations.Update:
                    _message = _employeeMasterRepository.UpdateEmployeeOtherDetails(employeeMasterBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        #endregion Other Details Tab

        #region Company Assets

        public ActionResult CompanyAssetsList(CompanyAssetsBo companyAssetsBo)
        {
            ViewData["CompanyAsset"] = _employeeMasterRepository.SearchCompanyAssets(companyAssetsBo);
            return PartialView(companyAssetsBo);
        }

        public ActionResult CompanyAsset()
        {
            return PartialView();
        }

        public ActionResult EmployeeCompanyAssets(int employeeId, int assetId)
        {
            CompanyAssetsBo companyAssetsBo = new CompanyAssetsBo { EmployeeId = employeeId };
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            if (assetId != 0)
            {
                companyAssetsBo = _employeeMasterRepository.GetCompanyAsset(assetId);
            }

            companyAssetsBo.AssetTypeList = masterDataRepository.SearchMasterDataDropDown("AssetType", string.Empty);
            return PartialView(companyAssetsBo);
        }

        [HttpPost]
        public ActionResult EmployeeCompanyAssets(CompanyAssetsBo companyAssetsBo)
        {
            companyAssetsBo.ModifiedBy = Utility.UserId();
            companyAssetsBo.DomainId = Utility.DomainId();
            companyAssetsBo.MenuCode = MenuCode.EmployeeMaster;
            companyAssetsBo.ReturnStatus = false;
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), companyAssetsBo.MenuCode, companyAssetsBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (companyAssetsBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    companyAssetsBo.CreatedBy = Utility.UserId();
                    _message = _employeeMasterRepository.CreateCompanyAssets(companyAssetsBo);
                    break;

                case Operations.Update:
                    _message = _employeeMasterRepository.UpdateCompanyAssets(companyAssetsBo);
                    break;

                case Operations.Delete:
                    _message = _employeeMasterRepository.DeleteCompanyAssets(companyAssetsBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        ////No view
        //public ActionResult GetEmployeeCompanyAssets(int assetId) //Anbu
        //{
        //    MasterDataRepository masterDataRepository = new MasterDataRepository();
        //    CompanyAssetsBo companyAssetsBo = new CompanyAssetsBo();

        //    if (assetId != 0)
        //    {
        //        companyAssetsBo = _employeeMasterRepository.GetCompanyAsset(assetId);
        //    }

        //    companyAssetsBo.AssetTypeList = masterDataRepository.SearchMasterDataDropDown("AssetType", string.Empty);
        //    return View(companyAssetsBo);
        //}

        #endregion Company Assets

        #region Dependent Details

        public ActionResult EmployeeDependentDetails(int employeeId)
        {
            DependentDetailsBo dependentDetailsBo = new DependentDetailsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            dependentDetailsBo.EmployeeId = employeeId;
            ViewData["DependentDetails"] = _employeeMasterRepository.SearchDependentDetails(dependentDetailsBo);
            dependentDetailsBo.GenderList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Gender");
            return PartialView(dependentDetailsBo);
        }

        [HttpPost]
        public ActionResult EmployeeDependentDetails(DependentDetailsBo dependentDetailsBo)
        {
            dependentDetailsBo.ModifiedBy = Utility.UserId();
            dependentDetailsBo.DomainId = Utility.DomainId();
            dependentDetailsBo.MenuCode = MenuCode.EmployeeMaster;
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), dependentDetailsBo.MenuCode, dependentDetailsBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (dependentDetailsBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    dependentDetailsBo.CreatedBy = Utility.UserId();
                    _message = _employeeMasterRepository.CreateDependentDetails(dependentDetailsBo);
                    break;

                case Operations.Update:
                    _message = _employeeMasterRepository.UpdateDependentDetails(dependentDetailsBo);
                    break;

                case Operations.Delete:
                    _message = _employeeMasterRepository.DeleteDependentDetails(dependentDetailsBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmployeeDependentDetailsList(DependentDetailsBo dependentDetailsBo)
        {
            return PartialView(_employeeMasterRepository.SearchDependentDetails(dependentDetailsBo));
        }

        #endregion Dependent Details

        #region Employment Details

        public ActionResult EmploymentDetails(int employeeId)
        {
            EmploymentDetailsBo employmentDetailsBo = new EmploymentDetailsBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            employmentDetailsBo.EmployeeId = employeeId;
            ViewData["EmploymentDetails"] = _employeeMasterRepository.SearchEmploymentDetails(employmentDetailsBo);
            employmentDetailsBo.YearList = masterDataRepository.SearchMasterDataDropDown("Year", string.Empty);
            employmentDetailsBo.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            employmentDetailsBo.ToYearList = masterDataRepository.SearchMasterDataDropDown("Year", string.Empty);
            employmentDetailsBo.ToMonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            return PartialView(employmentDetailsBo);
        }

        [HttpPost]
        public ActionResult EmploymentDetails(EmploymentDetailsBo employmentDetailsBo)
        {
            employmentDetailsBo.ModifiedBy = Utility.UserId();
            employmentDetailsBo.DomainId = Utility.DomainId();
            employmentDetailsBo.MenuCode = MenuCode.EmployeeMaster;
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), employmentDetailsBo.MenuCode, employmentDetailsBo.Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (employmentDetailsBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    employmentDetailsBo.CreatedBy = Utility.UserId();
                    _message = _employeeMasterRepository.CreateEmploymentDetails(employmentDetailsBo);
                    break;

                case Operations.Update:
                    _message = _employeeMasterRepository.UpdateEmploymentDetails(employmentDetailsBo);
                    break;

                case Operations.Delete:
                    _message = _employeeMasterRepository.DeleteEmploymentDetails(employmentDetailsBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EmploymentDetailsList(EmploymentDetailsBo employmentDetailsBo)
        {
            return PartialView(_employeeMasterRepository.SearchEmploymentDetails(employmentDetailsBo));
        }

        //  public ActionResult EmploymentDetailsList(EmploymentDetailsBO employmentDetailsBO)
        //{
        //    return PartialView(employeeMasterRepository.SearchEmploymentDetails(employmentDetailsBO));

        //}

        #endregion Employment Details

        #region Skill Details

        public ActionResult SkillDetails(int employeeId)
        {
            SkillDetailsBo skillDetailsBo = new SkillDetailsBo
            {
                EmployeeId = employeeId,
                MenuCode = MenuCode.EmployeeMaster
            };
            ViewData["SkillDetails"] = _employeeMasterRepository.GetEmployeeSkillsList(skillDetailsBo);
            return PartialView(skillDetailsBo);
        }

        public ActionResult SkillDetailsList(SkillDetailsBo skillDetailsBo)
        {
            return PartialView(_employeeMasterRepository.GetEmployeeSkillsList(skillDetailsBo));
        }

        public ActionResult EmployeeSkillDetails(int employeeId, int skillDetailId, string menuCode)
        {
            SkillDetailsBo skillDetailsBo = new SkillDetailsBo { EmployeeId = employeeId };
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            if (skillDetailId != 0)
            {
                skillDetailsBo = _employeeMasterRepository.GetSkillDetails(skillDetailId);
            }
            skillDetailsBo.MenuCode = menuCode;
            skillDetailsBo.SkillList = masterDataRepository.SearchMasterDataDropDown("Skills", string.Empty);
            skillDetailsBo.LevelList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "ExperienceLevel");
            skillDetailsBo.YearList = masterDataRepository.SearchMasterDataDropDown("Year", "ID");
            skillDetailsBo.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", "ID");
            skillDetailsBo.LastUsedyearList = masterDataRepository.SearchMasterDataDropDown("Year", string.Empty);
            return PartialView(skillDetailsBo);
        }

        [HttpPost]
        public ActionResult ManageEmployeeSkillDetail(SkillDetailsBo skillDetailsBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), skillDetailsBo.MenuCode, skillDetailsBo.SkillDetailId == 0 ? "Save" : (skillDetailsBo.IsDeleted ? "Delete" : "Edit"));

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (skillDetailsBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    _message = _employeeMasterRepository.CreateSkillDetails(skillDetailsBo);
                    break;

                case Operations.Update:
                    _message = _employeeMasterRepository.UpdateSkillDetails(skillDetailsBo);
                    break;

                case Operations.Delete:
                    _message = _employeeMasterRepository.DeleteSkillDetails(skillDetailsBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LockUnlockEmployeeSkills(int employeeId, bool status)
        {
            return Json(_employeeMasterRepository.LockUnlockEmployeeSkills(employeeId, status), JsonRequestBehavior.AllowGet);
        }

        #endregion Skill Details
    }
}