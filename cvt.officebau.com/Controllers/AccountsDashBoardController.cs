﻿using cvt.officebau.com.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    public class AccountsDashBoardController : Controller
    {

        private readonly AccountsDashboardRepository rep;


        public AccountsDashBoardController()
        {
            this.rep = new AccountsDashboardRepository();
        }
        // GET: AccountsDashBoard
        public ActionResult AccountsDashBoard()
        {
            return View();
        }

        public ActionResult GetCharResult()
        {
            return Json(rep.SearchShareDetailsChart(), JsonRequestBehavior.AllowGet);
        }
    }
}