﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Repository;
using cvt.officebau.com.Services;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace cvt.officebau.com.Controllers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class SessionExpire : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            string actionvalue = ctx.Request.RequestContext.RouteData.Values["action"].ToString();
            filterContext.HttpContext.Response.Cookies["SessionTimeOut"].Value = (180 * 60).ToString();
            if (actionvalue.ToUpper() != "INVOICEDOWNLOAD" && actionvalue.ToUpper() != "SALESORDERDOWNLOAD" && actionvalue.ToUpper() != "PRINTINVOICE")
            {
                //Session timeout. If invalid session, redirect to login page.
                if (ctx.Session["UserID"] == null)
                {
                    Returnlogin(filterContext);
                }

                UpdateNavigation();

                //If the request is for full page refresh, we have to validate menu URL with menucode
                if (!filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    string menuCode = HttpContext.Current.Request.QueryString["Menucode"];
                    //Non ajax call without menu code
                    if (string.IsNullOrEmpty(menuCode))
                    {
                        Returnlogin(filterContext);
                    }
                    else
                    {
                        if (menuCode != Utility.GetSession("MenuID"))
                        {
                            Utility.SetSession("TmpUserID", null);
                        }
                    }

                    //To set menuID for the application user to check the RBS
                    Utility.SetSession("MenuID", menuCode);

                    RbsRepository rep = new RbsRepository();
                    string menu = rep.SearchrMenu(menuCode);

                    //Non ajax call without menu code not match with db url
                    if (ctx.Request.RequestContext.RouteData.Values["action"].ToString().ToUpper() != menu.Split('/')[1].ToUpper() ||
                        ctx.Request.RequestContext.RouteData.Values["Controller"].ToString().ToUpper() != menu.Split('/')[0].ToUpper())
                    {
                        Returnlogin(filterContext);
                    }
                }
            }

            base.OnActionExecuting(filterContext);
        }

        private void Returnlogin(ActionExecutingContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                {
                    {"Controller", "UserLogin"},
                    {"Action", "UserLogin"},
                    {"sessionExpiry", "TRUE"}
                });
        }

        // Update the last update time in Database.
        private void UpdateNavigation()
        {
            int loginId = Utility.LoginId();
            if (loginId != 0)
            {
                FSMEntities context = new FSMEntities();
                tbl_LoginHistory loginHistory = context.tbl_LoginHistory.FirstOrDefault(b => b.ID == loginId);
                if (loginHistory != null) loginHistory.LogOutTime = DateTime.Now;
                context.SaveChanges();
            }
        }
    }
}
