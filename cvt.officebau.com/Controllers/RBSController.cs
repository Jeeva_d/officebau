﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class RBSController : Controller
    {
        #region RBS

        public ActionResult SearchRbs()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "ROLEBASEDSECURITY");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Role Based Security");

            RbsUserMenuBo rbsUserMenuBo = new RbsUserMenuBo();
            ViewData["UserList"] = _rbsRepository.SearchUser(rbsUserMenuBo);
            return View(rbsUserMenuBo);
        }

        #endregion RBS

        #region UserGrid

        public ActionResult UserGrid()
        {
            RbsUserMenuBo rbsUserMenuBo = new RbsUserMenuBo();
            return PartialView(_rbsRepository.SearchUser(rbsUserMenuBo));
        }

        #endregion UserGrid

        #region Menulist

        public ActionResult Menulist(int userId, int? moduleId, string type)
        {
            return PartialView(_rbsRepository.GetRbsUserMenu(userId, moduleId, type));
        }

        #endregion Menulist

        #region NotAuthorized

        public ActionResult NotAuthorized()
        {
            return View();
        }

        #endregion NotAuthorized

        #region Private methods

        private bool IsValid(string emailAddress)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(emailAddress))
                {
                    MailAddress mailAddress = new MailAddress(emailAddress);
                    return mailAddress.Address == emailAddress;
                }

                return false;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        #endregion Private methods

        #region Variables

        private readonly RbsRepository _rbsRepository = new RbsRepository();

        #endregion Variables

        #region SaveRBSUserMenu

        [HttpPost]
        public JsonResult SaveRbsUserMenu(IList<RbsUserMenuBo> rbsUserMenuBoList)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.RBS, rbsUserMenuBoList[0].Id == 0 ? "Save" : "Edit");
            return !hasAccess ? Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet) : Json(rbsRepository.ManageRbsMenu(rbsUserMenuBoList));
        }

        public JsonResult GetDependantDropdownList(string isReportalMenuModule)
        {
            if (isReportalMenuModule.Equals("NO"))
            {
                isReportalMenuModule = string.Empty;
            }
            else if (isReportalMenuModule.Equals("YES"))
            {
                isReportalMenuModule = "REPORT";
            }
            else
            {
                isReportalMenuModule = "CONFIG";
            }

            return Json(_rbsRepository.GetDependantDropdownList(isReportalMenuModule), JsonRequestBehavior.AllowGet);
        }

        #endregion SaveRBSUserMenu

        #region RBS Report

        #region RBS Report Based On Single Module

        public ActionResult RbsReport()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            RbsUserMenuBo rbsUserMenuBo = new RbsUserMenuBo
            {
                RbsModuleList = masterDataRepository.SearchMasterDataDropDown("rbssubmodule", string.Empty)
            };
            return PartialView(rbsUserMenuBo);
        }

        public ActionResult RPT_EmployeeMenulist(int moduleId)
        {
            return PartialView(_rbsRepository.RPT_EmployeeMenulist(moduleId));
        }

        public ActionResult RPT_Menulist(int userId, int moduleId, string type)
        {
            return PartialView(_rbsRepository.GetRbsUserMenu(userId, moduleId, type));
        }

        public JsonResult GetMenuDropDownList(int moduleId)
        {
            return Json(_rbsRepository.GetMenuDropDownList(moduleId), JsonRequestBehavior.AllowGet);
        }

        #endregion RBS Report Based On Single Module

        #region RBS Report Based On Multiple Module

        public ActionResult ModuleList()
        {
            return PartialView(_rbsRepository.GetAllvalues(string.Empty, string.Empty, string.Empty));
        }

        public ActionResult SubModuleList(string module, string subModule, string subModuleMenu)
        {
            return PartialView(_rbsRepository.GetAllvalues(module, subModule, subModuleMenu));
        }

        public ActionResult SubModuleMenuList(string module, string subModule, string subModuleMenu)
        {
            return PartialView(_rbsRepository.GetAllvalues(module, subModule, subModuleMenu));
        }

        public ActionResult SubModuleMenuEmployeeList(string module, string subModule, string subModuleMenu)
        {
            return PartialView(_rbsRepository.GetAllvalues(module, subModule, subModuleMenu));
        }

        #endregion RBS Report Based On Multiple Module

        #endregion RBS Report

        #region Password Reset BY Admin

        public ActionResult PasswordResetByAdmin()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "RESETPASSWORD-ADMIN");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Password Reset By Admin");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            RbsUserMenuBo rbsUserMenuBo = new RbsUserMenuBo
            {
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            rbsUserMenuBo.BusinessUnitList.RemoveAt(0);
            return View(rbsUserMenuBo);
        }

        public ActionResult EmployeeListforPasswordReset(RbsUserMenuBo rbsUserMenuBo)
        {
            return PartialView(_rbsRepository.EmployeeListforPasswordReset(rbsUserMenuBo));
        }

        public ActionResult GetResetPassword(RbsUserMenuBo rbsUserMenuBo)
        {
            string newResetPassword = Guid.NewGuid().ToString().Substring(0, 6);
            rbsUserMenuBo.Guid = EncryptionUtility.GetHashedValue(newResetPassword);
            string result = _rbsRepository.GetResetPassword(rbsUserMenuBo);

            if (result.Split('/')[1] != "" || result.Split('/')[1] != null)
            {
                bool isemail = IsValid(result.Split('/')[1]);
                if (isemail)
                {
                    EmailUtility.SendInstantEmail(result.Split('/')[1], _rbsRepository.PopulateMailBody(result.Split('/')[2], newResetPassword), "OfficeBAU User login - Reset Password by Admin", string.Empty, string.Empty);
                    return Json(newResetPassword, JsonRequestBehavior.AllowGet);
                }

                return Json(newResetPassword, JsonRequestBehavior.AllowGet);
            }

            return Json(newResetPassword, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteEmployee(string searchName)
        {
            return Json(_rbsRepository.SearchAutoCompleteEmployee(searchName), JsonRequestBehavior.AllowGet);
        }

        #endregion Password Reset BY Admin
    }
}