﻿using cvt.officebau.com.Services;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeBillingController : Controller
    {
        #region variables

        private EmployeeBillingBO employeeBillingBO = new EmployeeBillingBO();
        private EmployeeBillingRepository employeeBillingRepository = new EmployeeBillingRepository();
        private UtilityRepository track = new UtilityRepository();

        #endregion

        #region Employee Billing

        /// <summary>
        /// Landing page of the Employee Billin screen of the Employee
        /// </summary>
        /// <returns>A view result with EmployeeBilling BO</returns>
        public ActionResult SearchEmployeeBilling()
        {
            Utility.SetSession("MenuRetention", "EMPLOYEEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "EMPLOYEEBILLING");
            track.TrackFormNavigation("Employee Billing");
            return View(employeeBillingBO);
        }

        /// <summary>
        /// To get the searched Employee Billing list
        /// </summary>
        /// <param name="employeeName">Name of the employee</param>
        /// <param name="customerName">Name of the Customer</param>
        /// <param name="IsActive">Status of the employee</param>
        /// <returns>List Of billing employee's</returns>
        public ActionResult SearchEmployeeBillingList(string employeeName, string customerName, bool IsActive)
        {
            return PartialView(employeeBillingRepository.SearchEmployee(employeeName, customerName, IsActive));
        }

        /// <summary>
        /// Create/Edit an Employee Billing 
        /// </summary>
        /// <param name="id">id of the Biling Employee to be edited (0 on create operation)</param>
        /// <returns>A parial view result with Employee Billing Bo</returns>
        public ActionResult CreateEmployeeBilling(int id)
        {
            if (id != 0)
            {
                employeeBillingBO = employeeBillingRepository.GetemployeeBilling(id);
            }
            return PartialView(employeeBillingBO);
        }

        /// <summary>
        /// Create/Edit an Employee Billing
        /// </summary>
        /// <param name="employeeBillingBO">Employee Billing  object to create/edit</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateEmployeeBilling(EmployeeBillingBO employeeBillingBO)
        {
            return Json(employeeBillingRepository.ManageEmployeeBilling(employeeBillingBO));
        }

        /// <summary>
        /// Create/Edit an Employee Billing
        /// </summary>
        /// <param name="id">EmployeeBillingId of the deleted billing employee</param>
        /// <param name="employeeId">Employeeid  of the selected employee</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteEmployeeBilling(int id, int employeeId)
        {
            return Json(employeeBillingRepository.DeleteEmployeeBilling(id, employeeId));
        }

        #endregion
    }
}