﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class PurchaseOrderController : Controller
    {
        // GET: PurchaseOrder

        public ActionResult Index(string menuCode, int? id, string screenType)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "PURCHASE");
            Utility.SetSession("SubMenuRetention", "PURCHASEORDER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("PURCHASEORDER");
            SearchParamsBo searchParamsBo = new SearchParamsBo
            {
                Id = id ?? 0,
                ScreenType = screenType
            };
            return View(searchParamsBo);
        }

        public ActionResult PurchaseOrderDetails(SearchParamsBo searchParamsBo)
        {
            return PartialView(_purchaseOrderRepository.SearchPurchaseOrder(searchParamsBo));
        }

        public ActionResult CreateNewPurchaseOrder(int id, string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            ExpenseItemsBO expenseItemBo = new ExpenseItemsBO();
            ExpenseDetailsBO expenseDetailsBo = new ExpenseDetailsBO();
            ExpenseBo expenseBo = _purchaseOrderRepository.GetPoDetails(id);
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();
            if (id == 0)
            {
                expenseBo.ExpenseDetail = new List<ExpenseDetailsBO>();
                expenseBo.ExpenseItems = new List<ExpenseItemsBO>();
                expenseBo.ExpenseDetail.Add(expenseDetailsBo);
                expenseBo.ExpenseItems.Add(expenseItemBo);
                expenseDetailsBo.LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger");
                expenseItemBo.ProductList = masterDataRepository.GetPreRequestProduct("Purchase");
            }

            expenseDetailsBo.LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger");
            expenseBo.CostCenterList = masterDataRepository.SearchMasterDataDropDown("CostCenter");
            expenseItemBo.ProductList = masterDataRepository.GetPreRequestProduct("Purchase");
            expenseBo.IsCostCenter = applicationConfigurationRepository.SearchCostCenter();
            expenseBo.MenuCode = menuCode;
            return PartialView(expenseBo);
        }

        [HttpPost]
        public ActionResult CreateNewPurchaseOrder(ExpenseBo expenseBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (expenseBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), expenseBo.MenuCode, expenseBo.Id == 0 ? "Save" : "Edit");
            }

            if (expenseBo != null && expenseBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), expenseBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            return Json(_purchaseOrderRepository.ManagePo(expenseBo), JsonRequestBehavior.AllowGet);
        }

        #region Variables


        private readonly PurchaseOrderRepository _purchaseOrderRepository = new PurchaseOrderRepository();

        #endregion Variables
    }
}