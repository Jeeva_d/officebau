﻿using ClosedXML.Excel;

using cvt.officebau.com.Services;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.ViewModels;

using Microsoft.Reporting.WebForms;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    public class ExcelExportController : Controller
    {
        #region variables

        private readonly string operation = Operations.ExcelExport;

        #endregion variables

        #region Customer Contact

        public ActionResult CustomerContactExcelExport(string formName, string customerName, string fullName, string emailId, string contactNo)
        {
            CustomerContactRepository customerContactService = new CustomerContactRepository();
            CustomerContactBo customerContactBo = new CustomerContactBo
            {
                Operation = operation,
                CustomerName = customerName,
                FullName = fullName,
                EmailId = emailId,
                ContactNo = contactNo
            };

            List<CustomerContactBo> data = customerContactService.GetCustomerContactList(customerContactBo);

            DataTable dt = new DataTable
            {
                TableName = "Customer_Contact"// Excel sheet name.
            };
            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("Contact Name", typeof(string));
            dt.Columns.Add("Department", typeof(string));
            dt.Columns.Add("Designation", typeof(string));
            dt.Columns.Add("Email ID", typeof(string));
            dt.Columns.Add("Contact No", typeof(long));

            foreach (CustomerContactBo i in data)
            {
                dt.Rows.Add(
                        i.CustomerName,
                        i.FullName,
                        i.DepartmentName,
                        i.DesignationName,
                        i.EmailId,
                        i.ContactNo);
            }
            return ExportDataTableExcel("Customer_Contact", dt);
        }

        #endregion Customer Contact

        #region Company Master

        public ActionResult CompanyMasterExcelExport(string formName, string name, string contactNo, string emailId, string gstno)
        {
            CompanyMasterRepository companyMasterRepository = new CompanyMasterRepository();
            CompanyMasterBo companyMasterBo = new CompanyMasterBo
            {
                Name = name,
                ContactNo = contactNo,
                EmailId = emailId,
                Gstno = gstno
            };

            List<CompanyMasterBo> data = companyMasterRepository.GetCompanyMasterList(companyMasterBo);

            DataTable dt = new DataTable
            {
                TableName = "Company_Master"// Excel sheet name.
            };
            dt.Columns.Add("Company Name", typeof(string));
            dt.Columns.Add("Company Full Name", typeof(string));
            dt.Columns.Add("ContactNo", typeof(string));
            dt.Columns.Add("Address", typeof(string));
            dt.Columns.Add("Email ID", typeof(string));
            dt.Columns.Add("PAN No", typeof(string));
            dt.Columns.Add("TAN No", typeof(string));
            dt.Columns.Add("GST No", typeof(string));

            foreach (CompanyMasterBo i in data)
            {
                dt.Rows.Add(
                        i.Name,
                        i.FullName,
                        i.ContactNo,
                        i.Address,
                        i.EmailId,
                        i.Panno,
                        i.Tanno,
                        i.Gstno);
            }
            return ExportDataTableExcel("Company_Master", dt);
        }

        #endregion Company Master

        #region Customer Master

        public ActionResult CustomerExcelExport(string formName, string cityName, string orderName, int statusId, int salesPersonId)
        {
            CustomerBo customerBo = new CustomerBo();
            CustomerMasterRepository customerMasterService = new CustomerMasterRepository();

            customerBo.Operation = operation;
            customerBo.CityName = cityName;
            customerBo.OrderName = orderName;
            customerBo.StatusId = statusId;
            customerBo.SalesPersonId = salesPersonId;

            List<CustomerBo> data = customerMasterService.GetCustomerList(customerBo);

            DataTable dt = new DataTable
            {
                TableName = "Customer_Master"// Excel sheet name.
            };
            dt.Columns.Add("Customer Enquiry Name", typeof(string));
            dt.Columns.Add("Customer Order Name", typeof(string));
            dt.Columns.Add("Sales Executive", typeof(string));
            dt.Columns.Add("City", typeof(string));
            dt.Columns.Add("State", typeof(string));
            dt.Columns.Add("Monthly Volume", typeof(string));
            dt.Columns.Add("Type of Business", typeof(string));
            dt.Columns.Add("Contacts", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Email ID", typeof(string));
            dt.Columns.Add("Contact No", typeof(string));
            dt.Columns.Add("Address 1", typeof(string));
            dt.Columns.Add("Address 2", typeof(string));
            dt.Columns.Add("Created BY", typeof(string));
            dt.Columns.Add("Created On", typeof(string));

            foreach (CustomerBo i in data)
            {
                dt.Rows.Add(
                        i.EnquiryName,
                        i.OrderName,
                        i.SalesPersonName,
                        i.CityName,
                        i.StateName,
                        i.MonthlyBusinessVolume,
                        i.TypeofBusiness,
                        i.CustomerCount,
                        i.StatusName,
                        i.EmailId,
                        i.ContactNo,
                        i.Address1,
                        i.Address2,
                        i.CreatedByName,
                        Convert.ToDateTime(i.CreatedOn).ToString("dd-MMM-yyyy"));
            }
            return ExportDataTableExcel("Customer_Master", dt);
        }

        #endregion Customer Master

        #region Employee Excel Export

        public ActionResult EmployeeExcelExport(EmployeeMasterBo employeeMasterBo)
        {
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();

            employeeMasterBo.Operation = operation;
            employeeMasterBo.EmployeeId = Utility.UserId();

            DataTable data = employeeMasterRepository.SearchEmployeeMasterListForExport(employeeMasterBo);
            DataTable dt = new DataTable();

            foreach (DataColumn col in data.Columns)
            {
                dt.Columns.Add(col.ColumnName, typeof(string));
            }

            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    List<string> newRowList = new List<string>();
                    foreach (DataColumn col in data.Columns)
                    {
                        newRowList.Add(row[col.ColumnName].ToString());
                    }
                    dt.Rows.Add(newRowList.ToArray());
                }
            }
            return ExportDataTableExcel("Employee_Master", dt);
        }

        public ActionResult EmployeeRequestExcelExport(string formName, string code, string firstName, string isApproved, string contactNo)
        {
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            employeeMasterBo.Operation = operation;
            employeeMasterBo.TempEmployeeCode = code;
            employeeMasterBo.FirstName = firstName;
            employeeMasterBo.IsApproved = Convert.ToBoolean(isApproved);
            employeeMasterBo.EmployeeId = Utility.UserId();
            if (employeeMasterBo.IsApproved == false)
            {
                employeeMasterBo.ApprovedBy = Utility.UserId();
                employeeMasterBo.Code = null;
                //employeeMasterBO.IsApproved = null;
                employeeMasterBo.ContactNo = contactNo;
            }
            else
            {
                employeeMasterBo.Code = "TMP_";
            }

            DataTable data = employeeMasterRepository.SearchEmployeeRequestApprovalListForExport(employeeMasterBo);
            if (employeeMasterBo.IsApproved == true)
            {
                data.Columns.RemoveAt(1);
            }

            return ExportDataTableExcel(formName, data);
        }

        public ActionResult EmployeeUnitHeadApprovalExcelExport(string formName, string code, string firstName, string isApproved, string contactNo)
        {
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            employeeMasterBo.Operation = operation;
            employeeMasterBo.TempEmployeeCode = code;
            employeeMasterBo.FirstName = firstName;
            employeeMasterBo.IsApproved = (isApproved != "false");
            employeeMasterBo.EmployeeId = Utility.UserId();
            if (employeeMasterBo.IsApproved == false)
            {
                employeeMasterBo.ApprovedBy = Utility.UserId();
                employeeMasterBo.Code = null;
                //employeeMasterBO.IsApproved = null;
                employeeMasterBo.ContactNo = contactNo;
            }
            else
            {
                employeeMasterBo.Code = "TMP_";
            }

            DataTable data = employeeMasterRepository.SearchEmployeeUhApprovalListForExport(employeeMasterBo);
            if (employeeMasterBo.IsApproved == true)
            {
                data.Columns.RemoveAt(1);
            }

            return ExportDataTableExcel(formName, data);
        }

        #endregion Employee Excel Export

        #region Download excel for Payroll

        public FileResult Download(string uploadFileName, string fileName)
        {
            string filepath = Server.MapPath("~/Content/") + uploadFileName;
            return File(filepath, "text/csv", "" + fileName + ".xlsx");
        }

        #endregion Download excel for Payroll

        #region Sales Report Excel

        public ActionResult SalesReportExcel()
        {
            return File(ConfigurationManager.AppSettings["UploadPath"] + Utility.LoginId()
                + "_SalesActivityReport.xlsx", "text/xlsx", +Utility.LoginId() + "_SalesActivityReport.xlsx");
        }

        #endregion Sales Report Excel

        #region Activity Count Excel

        public ActionResult ActivityCountExcel(string formName, string businessUnit, string date)
        {
            string dateFormat = DateTime.Now.ToString("dd-MMM-yyyy");
            string reportName = "Activity_Count_report";

            StringBuilder sb = new StringBuilder();
            string fileName = reportName + "_" + dateFormat + ".xls";

            ReportBo reportBo = new ReportBo();
            ReportRepository reportRepository = new ReportRepository();

            reportBo.Operation = operation;
            reportBo.BusinessUnit = businessUnit;

            if (!string.IsNullOrWhiteSpace(date))
            {
                reportBo.VisitDate = Convert.ToDateTime(date);
            }

            List<ReportBo> data = reportRepository.GetActivityCountReport(reportBo);

            sb.Append("<table style='1px solid black; font-size:14px; font-family:Cambria;' border='1' >");
            sb.Append(GetExcelHeader(reportName));
            sb.Append("<tr>");
            sb.Append("<td style='width:200px;background-color:#b0d895;'><b>Sales Executive</b></td>");
            sb.Append("<td style='width:120px;background-color:#b0d895;'><b>Business Unit</b></td>");
            sb.Append("<td style='width:120px;background-color:#b0d895;'><b>Activity Count</b></td>");
            sb.Append("<td style='width:120px;background-color:#b0d895;'><b>Last Visit</b></td>");
            sb.Append("</tr>");

            if (data != null && data.Any())
            {
                foreach (ReportBo result in data)
                {
                    sb.Append("<tr style='height:20px;'>");
                    sb.Append("<td>" + result.SalesExecutive + "</td>");
                    sb.Append("<td>" + result.BusinessUnit + "</td>");
                    sb.Append("<td>" + result.Count + "</td>");
                    sb.Append("<td>" + result.LastVisit.ToString("dd-MMM-yyyy") + "</td>");
                }
            }

            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "data:application/vnd.ms-excel";
            byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
            return File(buffer, "data:application/vnd.ms-excel");
        }

        #endregion Activity Count Excel

        #region Claims Excel Export

        #region Request

        public ActionResult ClaimRequestExcel(string formName, int statusId, string operationType, int employeeId)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();
            EmployeeClaimsRepository employeeClaimsRepository = new EmployeeClaimsRepository();

            employeeClaimsBo.StatusId = statusId;
            employeeClaimsBo.Operation = operationType;
            if (employeeId == 0)
            {
                employeeClaimsBo.EmployeeId = Utility.UserId();
            }
            else
            {
                employeeClaimsBo.EmployeeId = employeeId;
            }
            List<EmployeeClaimsBo> data = employeeClaimsRepository.GetClaimsRequestList(employeeClaimsBo);

            DataTable dt = new DataTable
            {
                TableName = "Claims_Request"// Excel sheet name.
            };
            dt.Columns.Add("Expense Date", typeof(string));
            dt.Columns.Add("Category", typeof(string));
            dt.Columns.Add("Destination", typeof(string));
            dt.Columns.Add("Claim Amount", typeof(decimal));
            dt.Columns.Add("Approved Amount", typeof(decimal));
            dt.Columns.Add("Approver Name", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (EmployeeClaimsBo i in data)
            {
                decimal? approvalAmount = i.ApprovalAmount > 0 ? i.ApprovalAmount : null;
                dt.Rows.Add(
                         Convert.ToDateTime(i.ClaimedDate).ToString("dd-MMM-yyyy"),
                        i.CategoryName,
                        i.DestinationCity,
                        i.Amount,
                        approvalAmount,
                        i.ApprovarName,
                        i.StatusName);
            }
            return ExportDataTableExcel("Claims_Request", dt);
        }

        #endregion Request

        public ActionResult DocumentManagerExcel(string formName, int documentCategoryId, string documentName, bool active)
        {
            DocumentManagerBO documentManagerBO = new DocumentManagerBO();
            DocumentManagerRepository documentManagerRepository = new DocumentManagerRepository();
            documentManagerBO.DocumentName = documentName;
            documentManagerBO.DocumentCategoryId = documentCategoryId;
            documentManagerBO.Active = active;


            List<DocumentManagerBO> data = documentManagerRepository.GetDocumentManagersList(documentManagerBO);


            DataTable dt = new DataTable
            {
                TableName = "Document_Manager"// Excel sheet name.
            };
            dt.Columns.Add("Document Name", typeof(string));
            dt.Columns.Add("Document Category", typeof(string));
            dt.Columns.Add("Expiry Date", typeof(string));
            dt.Columns.Add("Uploaded By", typeof(string));
            dt.Columns.Add("Uploaded On", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (DocumentManagerBO i in data)
            {
                var expiryDate = Convert.ToDateTime(i.ExpiryDate).ToString("dd-MMM-yyyy");
                dt.Rows.Add(
                         i.DocumentName,
                         i.CategoryName,
                         Convert.ToDateTime(i.ExpiryDate) == DateTime.MinValue ? string.Empty : expiryDate,
                         i.ModifiedByName,
                         Convert.ToDateTime(i.CreatedOn).ToString("dd-MMM-yyyy"),
                         i.Description,
                         i.Active ? "Inactive" : "Active");
            }
            return ExportDataTableExcel("Document_Manager", dt);
        }

        #endregion Request

        #region Approval

        public ActionResult ClaimApproveExcel(string formName, int statusId, string operationType)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();
            EmployeeClaimsRepository employeeClaimsRepository = new EmployeeClaimsRepository();

            employeeClaimsBo.StatusId = statusId;
            employeeClaimsBo.Operation = operationType;

            List<EmployeeClaimsBo> data = employeeClaimsRepository.GetEmployeeClaimsApproveList(employeeClaimsBo);

            DataTable dt = new DataTable
            {
                TableName = "Claims_Approval"// Excel sheet name.
            };
            dt.Columns.Add("Requested On", typeof(string));
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("No. of Bills", typeof(string));

            foreach (EmployeeClaimsBo i in data)
            {
                dt.Rows.Add(
                         Convert.ToDateTime(i.ClaimedDate).ToString("dd-MMM-yyyy"),
                        i.Requester,
                        i.Amount,
                        i.StatusName,
                        i.Counts);
            }
            return ExportDataTableExcel("Claims_Approval", dt);
        }

        #endregion Approval

        #region Financial Approve

        public ActionResult ClaimFinancialExcel(string formName, int statusId, string operationType)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();
            EmployeeClaimsRepository employeeClaimsRepository = new EmployeeClaimsRepository();

            employeeClaimsBo.StatusId = statusId;
            employeeClaimsBo.Operation = operationType;

            List<EmployeeClaimsBo> data = employeeClaimsRepository.GetEmployeeClaimsFinancialApprovalList(employeeClaimsBo);

            DataTable dt = new DataTable
            {
                TableName = "Claims_Financial_Approval"// Excel sheet name.
            };
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Approver", typeof(string));
            dt.Columns.Add("Approved On", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("No. of Bills", typeof(string));

            foreach (EmployeeClaimsBo i in data)
            {
                dt.Rows.Add(
                        i.Amount,
                        i.Requester,
                        i.ApprovarName,
                        Convert.ToDateTime(i.SubmittedDate).ToString("dd-MMM-yyyy"),
                        i.StatusName,
                        i.Counts);
            }
            return ExportDataTableExcel("Claims_Financial_Approval", dt);
        }

        #endregion Financial Approve

        #region Settlement

        public ActionResult ClaimSettleExcel(string formName, int statusId, string operationType)
        {
            EmployeeClaimsBo employeeClaimsBo = new EmployeeClaimsBo();
            EmployeeClaimsRepository employeeClaimsRepository = new EmployeeClaimsRepository();

            employeeClaimsBo.StatusId = statusId;
            employeeClaimsBo.Operation = operationType;

            List<EmployeeClaimsBo> data = employeeClaimsRepository.GetEmployeeClaimsSettleList(employeeClaimsBo);

            DataTable dt = new DataTable
            {
                TableName = "Claims_Settlement"// Excel sheet name.
            };
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Requested On", typeof(string));
            dt.Columns.Add("Approver", typeof(string));
            dt.Columns.Add("Approved On", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("No. of Bills", typeof(string));

            foreach (EmployeeClaimsBo i in data)
            {
                dt.Rows.Add(
                        i.Amount,
                        i.Requester,
                        i.ClaimedDate.ToString("dd-MMM-yyyy"),
                        i.ApprovarName,
                        Convert.ToDateTime(i.SubmittedDate).ToString("dd-MMM-yyyy"),
                        i.StatusName,
                        i.Counts);
            }
            return ExportDataTableExcel("Claims_Settlement", dt);
        }

        #endregion Settlement

        #region Complete Claims Report

        public ActionResult CompleteClaimsReportExcel(string fromDate, string toDate, string businessUnitId, int statusId, int employeeId, int categoryId)
        {
            EmployeeClaimsReportBo employeeClaimsReportBo = new EmployeeClaimsReportBo();
            EmployeeClaimsRepository employeeClaimsRepository = new EmployeeClaimsRepository();

            if (string.IsNullOrEmpty(fromDate))
            {
                employeeClaimsReportBo.FromDate = DateTime.Now.AddDays(-180);
            }
            else
            {
                employeeClaimsReportBo.FromDate = Convert.ToDateTime(fromDate);
            }
            if (string.IsNullOrEmpty(toDate))
            {
                employeeClaimsReportBo.ToDate = DateTime.Now;
            }
            else
            {
                employeeClaimsReportBo.ToDate = Convert.ToDateTime(toDate);
            }
            employeeClaimsReportBo.BusinessUnitId = string.IsNullOrEmpty(businessUnitId) ? 0 : Convert.ToInt32(businessUnitId);
            employeeClaimsReportBo.StatusId = statusId;
            employeeClaimsReportBo.EmployeeId = employeeId;
            employeeClaimsReportBo.CategoryId = categoryId;

            List<EmployeeClaimsReportBo> data = employeeClaimsRepository.GetCompleteClaimsList(employeeClaimsReportBo);

            DataTable dt = new DataTable
            {
                TableName = "Claim_analysis_report"// Excel sheet name.
            };
            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Expense Date", typeof(string));
            dt.Columns.Add("Category", typeof(string));
            dt.Columns.Add("Requested Amount", typeof(decimal));
            dt.Columns.Add("Approved Amount", typeof(decimal));
            dt.Columns.Add("Settled Amount", typeof(decimal));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));

            foreach (EmployeeClaimsReportBo i in data)
            {
                string approvalAmount = (i.ApprovalAmount != null && i.ApprovalAmount > 0) ? Convert.ToDecimal(i.ApprovalAmount).ToString(CultureInfo.InvariantCulture) : null;
                string settlerAmount = (i.SettlerAmount != null && i.SettlerAmount > 0) ? Convert.ToDecimal(i.SettlerAmount).ToString(CultureInfo.InvariantCulture) : null;
                dt.Rows.Add(
                        i.EmployeeCode,
                        i.FullName,
                        i.RequestedDate.ToString("dd-MMM-yyyy"),
                        i.Category,
                        i.Amount,
                        approvalAmount,
                        settlerAmount,
                        i.Status,
                        i.BusinessUnit);
            }
            return ExportDataTableExcel("Claim_analysis_report", dt);
        }

        #endregion Complete Claims Report

        #region Claim Policy

        public ActionResult ClaimPolicyExcel(string formName)
        {
            EmployeeClaimsConfigurationRepository employeeClaimsConfigurationRepository = new EmployeeClaimsConfigurationRepository();

            List<EmployeeClaimsBo> data = employeeClaimsConfigurationRepository.SearchClaimsPolicyList();

            DataTable dt = new DataTable
            {
                TableName = formName// Excel sheet name.
            };
            dt.Columns.Add("Category", typeof(string));
            dt.Columns.Add("Designation", typeof(string));
            dt.Columns.Add("Band", typeof(string));
            dt.Columns.Add("Grade", typeof(string));
            dt.Columns.Add("Metro", typeof(decimal));
            dt.Columns.Add("Non-Metro", typeof(decimal));

            foreach (EmployeeClaimsBo i in data)
            {
                dt.Rows.Add(
                        i.ExpenseType,
                        i.Designation,
                        i.Band,
                        i.Level,
                        i.MetroAmount,
                        i.NonMetroAmount);
            }
            return ExportDataTableExcel(formName, dt);
        }

        #endregion Claim Policy

        #region Leave Excel Export

        #region Request

        public ActionResult LeaveRequestExcel(string formName, int statusId, int leaveTypeId, int employeeId)
        {
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo();
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            leaveManagementBo.StatusId = statusId;
            leaveManagementBo.LeaveTypeId = leaveTypeId;
            if (employeeId == 0)
            {
                leaveManagementBo.EmployeeId = Utility.UserId();
            }
            else
            {
                leaveManagementBo.EmployeeId = employeeId;
            }

            List<LeaveManagementBo> data = leaveManagementRepository.SearchLeaveRequest(leaveManagementBo);

            DataTable dt = new DataTable
            {
                TableName = "Leave_Request"// Excel sheet name.
            };
            dt.Columns.Add("Requested On", typeof(string));
            dt.Columns.Add("Leave Type", typeof(string));
            dt.Columns.Add("Duration", typeof(string));
            dt.Columns.Add("From Date", typeof(string));
            dt.Columns.Add("To Date", typeof(string));
            dt.Columns.Add("Approver Status", typeof(string));
            dt.Columns.Add("HR Status", typeof(string));

            foreach (LeaveManagementBo i in data)
            {
                string leaveDuration;
                if (i.Duration != null)
                {
                    if (i.Duration <= 1)
                    {
                        leaveDuration = Convert.ToString(i.Duration) + " hour";
                    }
                    else
                    {
                        leaveDuration = Convert.ToString(i.Duration) + " hours";
                    }
                }
                else
                {
                    if (i.IsHalfDay)
                    {
                        leaveDuration = "0.5";
                    }
                    else
                    {
                        leaveDuration = i.NoOfDays.ToString();
                    }
                    if (i.NoOfDays == 1)
                    {
                        leaveDuration += " day";
                    }
                    else
                    {
                        leaveDuration += " days";
                    }
                }
                dt.Rows.Add(
                        i.RequestedDate.ToString("dd-MMM-yyyy"),
                        i.LeaveType,
                        leaveDuration,
                        Convert.ToDateTime(i.FromDate).ToString("dd-MMM-yyyy"),
                        Convert.ToDateTime(i.ToDate).ToString("dd-MMM-yyyy"),
                        i.ApproverStatus,
                        i.HrStatus);
            }
            return ExportDataTableExcel("Leave_Request", dt);
        }

        #endregion Request

        #region Approval

        public ActionResult LeaveApproveExcel(string formName, int statusId, int leaveTypeId)
        {
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo();
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            leaveManagementBo.UserId = Utility.UserId();
            leaveManagementBo.DomainId = Utility.DomainId();
            leaveManagementBo.StatusId = statusId;
            leaveManagementBo.LeaveTypeId = leaveTypeId;

            List<LeaveManagementBo> data = leaveManagementRepository.SearchLeaveApproval(leaveManagementBo);

            DataTable dt = new DataTable
            {
                TableName = "Leave_Approval"// Excel sheet name.
            };
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Leave Type", typeof(string));
            dt.Columns.Add("From Date", typeof(string));
            dt.Columns.Add("To Date", typeof(string));
            dt.Columns.Add("Duration", typeof(string));
            dt.Columns.Add("Approver Status", typeof(string));
            dt.Columns.Add("Processed On", typeof(string));

            foreach (LeaveManagementBo i in data)
            {
                string leaveDuration;
                if (i.Duration != null)
                {
                    if (i.Duration <= 1)
                    {
                        leaveDuration = Convert.ToString(i.Duration) + " hour";
                    }
                    else
                    {
                        leaveDuration = Convert.ToString(i.Duration) + " hours";
                    }
                }
                else
                {
                    if (i.IsHalfDay)
                    {
                        leaveDuration = "0.5";
                    }
                    else
                    {
                        leaveDuration = i.NoOfDays.ToString();
                    }
                    if (i.NoOfDays == 1)
                    {
                        leaveDuration += " day";
                    }
                    else
                    {
                        leaveDuration += " days";
                    }
                }
                string approvedDate = i.ApprovedDate != null ? Convert.ToDateTime(i.ApprovedDate).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                        i.Employee.Name,
                        i.LeaveType,
                        Convert.ToDateTime(i.FromDate).ToString("dd-MMM-yyyy"),
                        Convert.ToDateTime(i.ToDate).ToString("dd-MMM-yyyy"),
                        leaveDuration,
                        i.ApproverStatus,
                        approvedDate);
            }
            return ExportDataTableExcel("Leave_Approval", dt);
        }

        #endregion Approval

        #region HR Approve

        public ActionResult LeaveHrApprovalExcel(string formName, int statusId, int leaveTypeId)
        {
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo();
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            leaveManagementBo.DomainId = Utility.DomainId();
            leaveManagementBo.UserId = Utility.UserId();
            leaveManagementBo.StatusId = statusId;
            leaveManagementBo.LeaveTypeId = leaveTypeId;

            List<LeaveManagementBo> data = leaveManagementRepository.SearchApprovedLeave(leaveManagementBo);

            DataTable dt = new DataTable
            {
                TableName = "Leave_Approval-HR"// Excel sheet name.
            };
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Leave Type", typeof(string));
            dt.Columns.Add("From Date", typeof(string));
            dt.Columns.Add("To Date", typeof(string));
            dt.Columns.Add("Duration", typeof(string));
            dt.Columns.Add("Approver", typeof(string));
            dt.Columns.Add("Approved On", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (LeaveManagementBo i in data)
            {
                string leaveDuration;
                if (i.Duration != null)
                {
                    if (i.Duration <= 1)
                    {
                        leaveDuration = Convert.ToString(i.Duration) + " hour";
                    }
                    else
                    {
                        leaveDuration = Convert.ToString(i.Duration) + " hours";
                    }
                }
                else
                {
                    if (i.IsHalfDay)
                    {
                        leaveDuration = "0.5";
                    }
                    else
                    {
                        leaveDuration = i.NoOfDays.ToString();
                    }
                    if (i.NoOfDays == 1)
                    {
                        leaveDuration += " day";
                    }
                    else
                    {
                        leaveDuration += " days";
                    }
                }

                dt.Rows.Add(
                        i.Employee.Name,
                        i.LeaveType,
                        Convert.ToDateTime(i.FromDate).ToString("dd-MMM-yyyy"),
                        Convert.ToDateTime(i.ToDate).ToString("dd-MMM-yyyy"),
                        leaveDuration,
                        i.ApproverName,
                        Convert.ToDateTime(i.ApprovedDate).ToString("dd-MMM-yyyy"),
                        i.Status.Name);
            }
            return ExportDataTableExcel("Leave_Approval-HR", dt);
        }

        #endregion HR Approve

        #region Leave Type

        public ActionResult LeaveTypeExcel(string formName)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            List<LeaveManagementBo> data = leaveManagementRepository.SearchLeaveType(Utility.DomainId());

            DataTable dt = new DataTable
            {
                TableName = "Leave_Type"// Excel sheet name.
            };
            dt.Columns.Add("Leave", typeof(string));
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (LeaveManagementBo i in data)
            {
                dt.Rows.Add(
                        i.LeaveTypeName,
                        i.LeaveTypeCode,
                        i.Description);
            }
            return ExportDataTableExcel("Leave_Type", dt);
        }

        #endregion Leave Type

        #region Leave Policy

        public ActionResult LeavePolicyExcel(string formName)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            List<LeaveManagementBo> data = leaveManagementRepository.SearchLeavePolicy(Utility.DomainId());

            DataTable dt = new DataTable
            {
                TableName = "Leave_Policy"// Excel sheet name.
            };
            dt.Columns.Add("Region", typeof(string));
            dt.Columns.Add("Type", typeof(string));
            dt.Columns.Add("Available Leave", typeof(string));

            foreach (LeaveManagementBo i in data)
            {
                dt.Rows.Add(
                        i.Region,
                        i.LeaveType,
                        i.AvailableLeave);
            }
            return ExportDataTableExcel("Leave_Policy", dt);
        }

        #endregion Leave Policy

        #region Holiday

        public ActionResult HolidayExcel(string formName, string holidayType, int yearId, string holidayMenu)
        {
            HolidayRepository holidayRepository = new HolidayRepository();
            HolidayBo holidayBo = new HolidayBo();

            if (holidayBo.HolidayType == null)
            {
                holidayBo.HolidayType = holidayType;
            }
            holidayBo.YearId = yearId;

            List<HolidayBo> data = holidayRepository.SearchHoliday(holidayBo);

            DataTable dt = new DataTable
            {
                TableName = holidayType + "_" + "Holiday"// Excel sheet name.
            };
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Day", typeof(string));
            if (holidayMenu == "False")
            {
                dt.Columns.Add("Region", typeof(string));
            }
            dt.Columns.Add("Description", typeof(string));

            foreach (HolidayBo i in data)
            {
                DataRow row = dt.NewRow();
                row["Date"] = Convert.ToDateTime(i.HolidayDate).ToString("dd-MMM-yyyy");
                row["Day"] = i.Holiday;
                if (holidayMenu == "False")
                {
                    row["Region"] = i.Region;
                }
                row["Description"] = i.Description;
                dt.Rows.Add(row);
            }
            return ExportDataTableExcel(holidayType + "_" + "Holiday", dt);
        }

        #endregion Holiday

        #region Business Hour

        public ActionResult BusinessExcel(string formName)
        {
            HolidayRepository holidayRepository = new HolidayRepository();

            List<HolidayBo> data = holidayRepository.SearchBusinessHours(Utility.DomainId());

            DataTable dt = new DataTable
            {
                TableName = "Business_Hours"// Excel sheet name.
            };
            dt.Columns.Add("Region", typeof(string));
            dt.Columns.Add("Full Day", typeof(string));
            dt.Columns.Add("Half Day", typeof(string));
            dt.Columns.Add("Business Start Hour", typeof(string));
            dt.Columns.Add("Business End Hour", typeof(string));
            dt.Columns.Add("Is Based On Last Check out", typeof(string));
            dt.Columns.Add("Is Include Leave Details In Paystub", typeof(string));

            foreach (HolidayBo i in data)
            {
                string businessStartHour = Convert.ToDateTime((DateTime.Today).Date + i.BusinessStartHour).ToString("hh:mm tt");
                string businessEndHour = Convert.ToDateTime((DateTime.Today).Date + i.BusinessEndHour).ToString("hh:mm tt");
                string isBasedOnLastCheckout = i.IsBasedOnLastCheckout ? "Yes" : "No";
                string isIncludeLeaveDetailsInPaystub = i.IsIncludeLeaveDetailsInPaystub ? "Yes" : "No";
                dt.Rows.Add(
                        i.Region,
                        i.FullDay,
                        i.HalfDay,
                        businessStartHour,
                        businessEndHour,
                        isBasedOnLastCheckout,
                        isIncludeLeaveDetailsInPaystub);
            }
            return ExportDataTableExcel("Business_Hours", dt);
            //sb.Append("<td style='mso-number-format:\\@;'>" + result.FullDay + "</td>");
            //sb.Append("<td style='mso-number-format:\\@;'>" + result.HalfDay + "</td>");
        }

        #endregion Business Hour

        #region Lop Details

        public ActionResult LopDetailsExcel(string formName, int monthId, string yearId, string businessUnitIdList, bool hasLopDays)
        {
            LopDetailsBo lopDetailsBo = new LopDetailsBo();
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            lopDetailsBo.MonthId = monthId;
            lopDetailsBo.YearId = Convert.ToInt32(yearId);
            lopDetailsBo.BusinessUnitIdList = businessUnitIdList;
            lopDetailsBo.HasLopDays = hasLopDays;

            List<LopDetailsBo> data = leaveManagementRepository.SearchLopDetails(lopDetailsBo);

            DataTable dt = new DataTable
            {
                TableName = "Lop_Details"// Excel sheet name.
            };
            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Department", typeof(string));
            dt.Columns.Add("LOP Days", typeof(string));
            dt.Columns.Add("Joining Date", typeof(string));
            dt.Columns.Add("Payroll Status", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));

            foreach (LopDetailsBo i in data)
            {
                dt.Rows.Add(
                        i.Name,
                        i.EmployeeName,
                        i.UserMessage,
                        i.LopDays,
                        i.Doj.ToString("dd-MMM-yyyy"),
                        i.StatusCode,
                        i.Location);
            }
            return ExportDataTableExcel("Lop_Details", dt);
        }

        public ActionResult LopDetailsExcel_V2(string formName, int monthId, string yearId, string businessUnitIdList, bool hasLopDays)
        {
            LopDetailsBo lopDetailsBo = new LopDetailsBo();
            EmployeePayrollRepository employeePayrollRepository = new EmployeePayrollRepository();

            lopDetailsBo.MonthId = monthId;
            lopDetailsBo.YearId = Convert.ToInt32(yearId);
            lopDetailsBo.BusinessUnitIdList = businessUnitIdList;
            lopDetailsBo.HasLopDays = hasLopDays;

            List<LopDetailsBo> data = employeePayrollRepository.SearchLopDetails(lopDetailsBo);

            DataTable dt = new DataTable
            {
                TableName = "Lop_Details"// Excel sheet name.
            };
            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Department", typeof(string));
            dt.Columns.Add("LOP Days", typeof(string));
            dt.Columns.Add("Joining Date", typeof(string));
            dt.Columns.Add("Payroll Status", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));

            foreach (LopDetailsBo i in data)
            {
                dt.Rows.Add(
                        i.Name,
                        i.EmployeeName,
                        i.UserMessage,
                        i.LopDays,
                        i.Doj.ToString("dd-MMM-yyyy"),
                        i.StatusCode,
                        i.Location);
            }
            return ExportDataTableExcel("Lop_Details", dt);
        }

        public void ExportLopDetails(string formName, string downloadFileName, int month, int year, string location)
        {
            var fileName = downloadFileName + ".xlsx";

            AttendanceRepository attendanceRepository = new AttendanceRepository();

            DataTable data = attendanceRepository.ExportLopDetails(month, year, location);

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(data, formName);

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                using (MemoryStream myMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(myMemoryStream);
                    myMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        public void ExportLOPDetails_V2(string formName, string downloadFileName, int month, int year, string location)
        {
            var fileName = downloadFileName + ".xlsx";

            AttendanceRepository attendanceRepository = new AttendanceRepository();

            DataTable data = attendanceRepository.ExportLopDetails_V2(month, year, location);

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(data, formName);

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                using (MemoryStream myMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(myMemoryStream);
                    myMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        public ActionResult ProcessLopDetailsExcelExport(string reportName, int monthId, int yearId, string businessUnitIdList)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo
            {
                MonthId = monthId,
                YearId = yearId,
                BusinessUnitIdList = businessUnitIdList
            };

            DataTable data = leaveManagementRepository.GetLopDetails(leaveManagementBo);

            return ExportDataTableExcel(reportName.Replace(" ", ""), data);
        }

        #endregion Lop Details

        #endregion Leave Excel Export

        #region Payroll Excel

        //#region Company Paystructure

        //public ActionResult CompanyExcel(string formName)
        //{
        //    PayStructureRepository payStructureRepository = new PayStructureRepository();
        //    PayStructureBO payStructureBO = new PayStructureBO();

        //    List<PayStructureBO> data = payStructureRepository.SearchCompanyPaystructure(payStructureBO);

        //    DataTable dt = new DataTable
        //    {
        //        TableName = "Company_Paystructure"// Excel sheet name.
        //    };
        //    dt.Columns.Add("Name", typeof(string));
        //    dt.Columns.Add("Basic %", typeof(decimal));
        //    dt.Columns.Add("HRA %", typeof(decimal));
        //    dt.Columns.Add("Medical Allowance", typeof(decimal));
        //    dt.Columns.Add("Conveyance", typeof(decimal));
        //    dt.Columns.Add("Effective From", typeof(string));
        //    dt.Columns.Add("Region", typeof(string));

        //    foreach (PayStructureBO i in data)
        //    {
        //        dt.Rows.Add(
        //                i.Name,
        //                i.Basic,
        //                i.HRA,
        //                i.MedicalAllowance,
        //                i.Conveyance,
        //                 Convert.ToDateTime(i.EffictiveFrom).ToString("dd-MMM-yyyy"),
        //                i.BusinessUnitIDs);
        //    }
        //    return ExportDataTableExcel("Company_Pay_Structure", dt);
        //}

        //#endregion Company Paystructure

        //#region Employee Paystructure

        //public ActionResult EmployeePayExcel(string formName, string businessUnitIDs, int? employeeID, bool IsActive)
        //{
        //    PayStructureRepository payStructureRepository = new PayStructureRepository();
        //    PayStructureBO payStructureBO = new PayStructureBO
        //    {
        //        BusinessUnitIDs = businessUnitIDs,
        //        EmployeeId = Convert.ToInt32(employeeID),
        //        IsActive = IsActive
        //    };
        //    List<PayStructureBO> data = payStructureRepository.SearchEmployee(payStructureBO);

        //    DataTable dt = new DataTable
        //    {
        //        TableName = "Employee_Paystructure"// Excel sheet name.
        //    };
        //    dt.Columns.Add("Employee Name", typeof(string));
        //    dt.Columns.Add("Effective From", typeof(string));
        //    dt.Columns.Add("Gross", typeof(decimal));
        //    dt.Columns.Add("Company Pay Structure", typeof(string));

        //    foreach (PayStructureBO i in data)
        //    {
        //        dt.Rows.Add(
        //                i.EmployeeName,
        //                Convert.ToDateTime(i.EffictiveFrom).ToString("dd-MMM-yyyy"),
        //                i.Gross,
        //                i.CompanyPayStucture.Name);
        //    }
        //    return ExportDataTableExcel("Employee_Pay_Structure", dt);
        //}

        //#endregion Employee Paystructure

        #region Process Payroll

        public ActionResult PayrollExcel(string formName, int monthId, int yearId, string businessUnitList, int? department, int? statusId, int? proceed)
        {
            PayrollRepository payrollRepository = new PayrollRepository();

            List<PayrollBO> data = payrollRepository.SearchPayroll(monthId, yearId, businessUnitList, department, statusId, proceed);
            DataTable dt = new DataTable();

            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Employee_Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Date_of_Birth", typeof(DateTime));
            dt.Columns.Add("PAN_No", typeof(string));
            dt.Columns.Add("Date_of_Joining", typeof(DateTime));
            dt.Columns.Add("Fixed_Gross", typeof(decimal));
            dt.Columns.Add("Payable_days", typeof(string));
            dt.Columns.Add("Basic", typeof(decimal));
            dt.Columns.Add("HRA", typeof(decimal));
            dt.Columns.Add("Medical", typeof(decimal));
            dt.Columns.Add("Conv", typeof(decimal));
            dt.Columns.Add("Spl_Allow", typeof(decimal));

            if (data.FirstOrDefault() != null && data.Select(s => s.IsEducationAllow).FirstOrDefault())
            {
                dt.Columns.Add("Education_Allow", typeof(decimal));
            }
            dt.Columns.Add("Paper&Magazine_Allow", typeof(decimal));
            dt.Columns.Add("Monsoon_Allow", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsOverTime).FirstOrDefault())
            {
                dt.Columns.Add("Over_Time", typeof(decimal));
            }
            dt.Columns.Add("Other_Earnings", typeof(decimal));
            dt.Columns.Add("Paid_Gross", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsEEPF).FirstOrDefault())
            {
                dt.Columns.Add("EPF", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsEEESI).FirstOrDefault())
            {
                dt.Columns.Add("EESI", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsTDS).FirstOrDefault())
            {
                dt.Columns.Add("TDS", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsPT).FirstOrDefault())
            {
                dt.Columns.Add("PT", typeof(decimal));
            }
            dt.Columns.Add("Loan", typeof(decimal));
            dt.Columns.Add("Other_Deduction", typeof(decimal));
            dt.Columns.Add("Total Deductions", typeof(decimal));
            dt.Columns.Add("Net_Payable", typeof(decimal));

            if (data.FirstOrDefault() != null && data.Select(s => s.IsERPF).FirstOrDefault())
            {
                dt.Columns.Add("ERPF", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsERESI).FirstOrDefault())
            {
                dt.Columns.Add("ERESI", typeof(decimal));
            }
            dt.Columns.Add("Bonus", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsGratuity).FirstOrDefault())
            {
                dt.Columns.Add("Gratuity", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsPLI).FirstOrDefault())
            {
                dt.Columns.Add("PLI", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsMobileDatacard).FirstOrDefault())
            {
                dt.Columns.Add("Mobile/Datacard", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsMediclaim).FirstOrDefault())
            {
                dt.Columns.Add("MediClaim", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsOtherPerks).FirstOrDefault())
            {
                dt.Columns.Add("Other_Perks", typeof(decimal));
            }
            dt.Columns.Add("CTC", typeof(decimal));
            dt.Columns.Add("Business Unit", typeof(string));
            dt.Columns.Add("IFSC_Code", typeof(string));
            dt.Columns.Add("Account_No", typeof(string));
            dt.Columns.Add("Beneficiary_ID", typeof(string));

            if (data.Any())
            {
                int count = 0;
                foreach (PayrollBO result in data)
                {
                    DataRow newRow = dt.NewRow();
                    if (result.IsProcessed)
                    {
                        newRow["Status"] = "Processed";
                    }
                    else if (result.IsApproved)
                    {
                        newRow["Status"] = "Approved";
                    }
                    else if (result.Id != 0)
                    {
                        newRow["Status"] = "Saved";
                    }
                    else
                    {
                        newRow["Status"] = "Open";
                    }
                    newRow["Employee_Code"] = result.EmployeeCode;
                    newRow["Employee Name"] = result.EmployeeName;
                    newRow["Date_of_Birth"] = result.DOB.ToString("dd-MMM-yyyy");
                    newRow["PAN_No"] = result.PANNo;
                    newRow["Date_of_Joining"] = result.DOJ.ToString("dd-MMM-yyyy");
                    newRow["Fixed_Gross"] = result.FixedGross.ToString("##,#.00");
                    newRow["Payable_days"] = result.WorkingDays;
                    newRow["Basic"] = result.Basic.ToString("##,#.00");
                    newRow["HRA"] = result.HRA.ToString("##,#.00");
                    newRow["Medical"] = result.MedicalAllowance.ToString("##,#.00");
                    newRow["Conv"] = result.Conveyance.ToString("##,#.00");
                    newRow["Spl_Allow"] = result.SplAllow.ToString("##,#.00");
                    if (result.IsEducationAllow)
                    {
                        newRow["Education_Allow"] = result.EducationAllowance.ToString("##,#.00");
                    }
                    newRow["Paper&Magazine_Allow"] = result.PaperMagazine.ToString("##,#.00");
                    newRow["Monsoon_Allow"] = result.MonsoonAllowance.ToString("##,#.00");
                    if (result.IsOverTime)
                    {
                        newRow["Over_Time"] = result.OverTime.ToString("##,#.00");
                    }
                    newRow["Other_Earnings"] = result.OtherEarnings.ToString("##,#.00");
                    newRow["Paid_Gross"] = result.PaidGross.ToString("##,#.00");
                    if (result.IsEEPF)
                    {
                        newRow["EPF"] = result.EEPF.ToString("##,#.00");
                    }
                    if (result.IsEEESI)
                    {
                        newRow["EESI"] = result.EEESI.ToString("##,#.00");
                    }
                    if (result.IsTDS)
                    {
                        newRow["TDS"] = result.TDS.ToString("##,#.00");
                    }
                    if (result.IsPT)
                    {
                        newRow["PT"] = result.PT.ToString("##,#.00");
                    }
                    newRow["Loan"] = result.Loans.ToString("##,#.00");
                    newRow["Other_Deduction"] = result.Deduction.ToString("##,#.00");
                    newRow["Total Deductions"] = (result.Deduction + result.Loans + result.PT + result.TDS + result.EEESI + result.EEPF).ToString("##,#.00");
                    newRow["Net_Payable"] = result.NetSalary.ToString("##,#.00");
                    if (result.IsERPF)
                    {
                        newRow["ERPF"] = result.ERPF.ToString("##,#.00");
                    }
                    if (result.IsERESI)
                    {
                        newRow["ERESI"] = result.ERESI.ToString("##,#.00");
                    }
                    newRow["Bonus"] = result.Bonus.ToString("##,#.00");
                    if (result.IsGratuity)
                    {
                        newRow["Gratuity"] = result.Gratuity.ToString("##,#.00");
                    }
                    if (result.IsPLI)
                    {
                        newRow["PLI"] = result.PLI.ToString("##,#.00");
                    }
                    if (result.IsMobileDatacard)
                    {
                        newRow["Mobile/Datacard"] = result.MobileDataCard.ToString("##,#.00");
                    }
                    if (result.IsMediclaim)
                    {
                        newRow["MediClaim"] = result.Medicalclaim.ToString("##,#.00");
                    }
                    if (result.IsOtherPerks)
                    {
                        newRow["Other_Perks"] = result.OtherPerks.ToString("##,#.00");
                    }
                    newRow["CTC"] = result.CTC.ToString("##,#.00");
                    newRow["Business Unit"] = Convert.ToString(result.Operation);
                    newRow["IFSC_Code"] = Convert.ToString(result.IFSCCode);
                    newRow["Account_No"] = Convert.ToString(result.AccountNo);
                    newRow["Beneficiary_ID"] = Convert.ToString(result.BeneID);
                    count = count + 1;
                    dt.Rows.Add(newRow);
                }
            }

            return ExportDataTableExcel("Run_Payroll", dt);
        }

        #endregion Process Payroll

        #region Monthly Payroll Analysis Excel

        public ActionResult MonthlyPayrollAnalysisExcel(string formName, int monthId, int yearId, string businessUnitList, int? department)
        {
            PayrollRepository payrollRepository = new PayrollRepository();

            List<PayrollBO> data = payrollRepository.RPT_SearchPayrollByMonthly(monthId, yearId, businessUnitList, department);

            DataTable dt = new DataTable();

            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Date of Joining", typeof(DateTime));
            dt.Columns.Add("Business Unit", typeof(string));
            dt.Columns.Add("Month & Year", typeof(string));
            dt.Columns.Add("Payable days", typeof(string));
            dt.Columns.Add("Fixed Gross", typeof(decimal));
            dt.Columns.Add("Basic", typeof(decimal));
            dt.Columns.Add("HRA", typeof(decimal));
            dt.Columns.Add("Medical", typeof(decimal));
            dt.Columns.Add("Conveyance", typeof(decimal));
            dt.Columns.Add("Special Allowance", typeof(decimal));

            if (data.FirstOrDefault() != null && data.Select(s => s.IsEducationAllow).FirstOrDefault())
            {
                dt.Columns.Add("Education_Allowance", typeof(decimal));
            }
            dt.Columns.Add("Paper&Magazine_Allowance", typeof(decimal));
            dt.Columns.Add("Monsoon_Allowance", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsOverTime).FirstOrDefault())
            {
                dt.Columns.Add("Over_Time", typeof(decimal));
            }
            dt.Columns.Add("Other_Earnings", typeof(decimal));
            dt.Columns.Add("Paid Gross", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsEEPF).FirstOrDefault())
            {
                dt.Columns.Add("Employee PF", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsEEESI).FirstOrDefault())
            {
                dt.Columns.Add("Employee ESI", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsTDS).FirstOrDefault())
            {
                dt.Columns.Add("TDS", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsPT).FirstOrDefault())
            {
                dt.Columns.Add("PT", typeof(decimal));
            }
            dt.Columns.Add("Loan", typeof(decimal));
            dt.Columns.Add("Other Deduction", typeof(decimal));
            dt.Columns.Add("Total Deductions", typeof(decimal));
            dt.Columns.Add("Net Payable", typeof(decimal));

            if (data.FirstOrDefault() != null && data.Select(s => s.IsERPF).FirstOrDefault())
            {
                dt.Columns.Add("Employer PF", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsERESI).FirstOrDefault())
            {
                dt.Columns.Add("Employer ESI", typeof(decimal));
            }
            dt.Columns.Add("Bonus", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsGratuity).FirstOrDefault())
            {
                dt.Columns.Add("Gratuity", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsPLI).FirstOrDefault())
            {
                dt.Columns.Add("PLI", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsMobileDatacard).FirstOrDefault())
            {
                dt.Columns.Add("Mobile or Datacard", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsMediclaim).FirstOrDefault())
            {
                dt.Columns.Add("MediClaim", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsOtherPerks).FirstOrDefault())
            {
                dt.Columns.Add("Other Perks", typeof(decimal));
            }
            dt.Columns.Add("CTC", typeof(decimal));

            if (data.Any())
            {
                foreach (PayrollBO result in data)
                {
                    DataRow newRow = dt.NewRow();
                    newRow["Employee Code"] = result.EmployeeNo;
                    newRow["Employee Name"] = result.EmployeeName;
                    newRow["Date of Joining"] = result.DOJ.ToString("dd-MMM-yyyy");
                    newRow["Business Unit"] = Convert.ToString(result.BusinessUnitIDs);
                    newRow["Month & Year"] = result.Month;
                    newRow["Payable days"] = result.WorkingDays;
                    newRow["Fixed Gross"] = result.FixedGross.ToString("##,#.00");
                    newRow["Basic"] = result.Basic.ToString("##,#.00");
                    newRow["HRA"] = result.HRA.ToString("##,#.00");
                    newRow["Medical"] = result.MedicalAllowance.ToString("##,#.00");
                    newRow["Conveyance"] = result.Conveyance.ToString("##,#.00");
                    newRow["Special Allowance"] = result.SplAllow.ToString("##,#.00");
                    if (result.IsEducationAllow)
                    {
                        newRow["Education_Allowance"] = result.EducationAllowance.ToString("##,#.00");
                    }
                    newRow["Paper&Magazine_Allowance"] = result.PaperMagazine.ToString("##,#.00");
                    newRow["Monsoon_Allowance"] = result.MonsoonAllowance.ToString("##,#.00");
                    if (result.IsOverTime)
                    {
                        newRow["Over_Time"] = result.OverTime.ToString("##,#.00");
                    }
                    newRow["Other_Earnings"] = result.OtherEarnings.ToString("##,#.00");
                    newRow["Paid Gross"] = result.PaidGross.ToString("##,#.00");
                    if (result.IsEEPF)
                    {
                        newRow["Employee PF"] = result.EEPF.ToString("##,#.00");
                    }
                    if (result.IsEEESI)
                    {
                        newRow["Employee ESI"] = result.EEESI.ToString("##,#.00");
                    }
                    if (result.IsTDS)
                    {
                        newRow["TDS"] = result.TDS.ToString("##,#.00");
                    }
                    if (result.IsPT)
                    {
                        newRow["PT"] = result.PT.ToString("##,#.00");
                    }
                    newRow["Loan"] = result.Loans.ToString("##,#.00");
                    newRow["Other Deduction"] = result.Deduction.ToString("##,#.00");
                    newRow["Total Deductions"] = (result.Deduction + result.Loans + result.PT + result.TDS + result.EEESI + result.EEPF).ToString("##,#.00");
                    newRow["Net Payable"] = result.NetSalary.ToString("##,#.00");
                    if (result.IsERPF)
                    {
                        newRow["Employer PF"] = result.ERPF.ToString("##,#.00");
                    }
                    if (result.IsERESI)
                    {
                        newRow["Employer ESI"] = result.ERESI.ToString("##,#.00");
                    }
                    newRow["Bonus"] = result.Bonus.ToString("##,#.00");
                    if (result.IsGratuity)
                    {
                        newRow["Gratuity"] = result.Gratuity.ToString("##,#.00");
                    }
                    if (result.IsPLI)
                    {
                        newRow["PLI"] = result.PLI.ToString("##,#.00");
                    }
                    if (result.IsMobileDatacard)
                    {
                        newRow["Mobile or Datacard"] = result.MobileDataCard.ToString("##,#.00");
                    }
                    if (result.IsMediclaim)
                    {
                        newRow["MediClaim"] = result.Medicalclaim.ToString("##,#.00");
                    }
                    if (result.IsOtherPerks)
                    {
                        newRow["Other Perks"] = result.OtherPerks.ToString("##,#.00");
                    }
                    newRow["CTC"] = result.CTC.ToString("##,#.00");
                    dt.Rows.Add(newRow);
                }
            }

            return ExportDataTableExcel("Employee_Salary_statement", dt);
        }

        #endregion Monthly Payroll Analysis Excel

        #region Monthly Payroll Component Excel

        public ActionResult MonthlyPayrollComponentExcel(string formName, int monthId, int yearId, string businessUnitList, int? department)
        {
            PayrollRepository payrollRepository = new PayrollRepository();

            List<PayrollBO> data = payrollRepository.RPT_SearchComponentsByMonthly(monthId, yearId, businessUnitList, department);
            DataTable dt = new DataTable();

            dt.Columns.Add("Department", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));
            dt.Columns.Add("Fixed Gross", typeof(decimal));
            dt.Columns.Add("Basic", typeof(decimal));
            dt.Columns.Add("HRA", typeof(decimal));
            dt.Columns.Add("Medical", typeof(decimal));
            dt.Columns.Add("Conveyance", typeof(decimal));
            dt.Columns.Add("Special Allowance", typeof(decimal));

            if (data.FirstOrDefault() != null && data.Select(s => s.IsEducationAllow).FirstOrDefault())
            {
                dt.Columns.Add("Education_Allowance", typeof(decimal));
            }
            dt.Columns.Add("Paper&Magazine_Allowance", typeof(decimal));
            dt.Columns.Add("Monsoon_Allowance", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsOverTime).FirstOrDefault())
            {
                dt.Columns.Add("Over_Time", typeof(decimal));
            }
            dt.Columns.Add("Other_Earnings", typeof(decimal));
            dt.Columns.Add("Paid Gross", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsEEPF).FirstOrDefault())
            {
                dt.Columns.Add("Employee PF", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsEEESI).FirstOrDefault())
            {
                dt.Columns.Add("Employee ESI", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsTDS).FirstOrDefault())
            {
                dt.Columns.Add("TDS", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsPT).FirstOrDefault())
            {
                dt.Columns.Add("PT", typeof(decimal));
            }
            dt.Columns.Add("Loan", typeof(decimal));
            dt.Columns.Add("Other Deduction", typeof(decimal));
            dt.Columns.Add("Total Deductions", typeof(decimal));
            dt.Columns.Add("Net Payable", typeof(decimal));

            if (data.FirstOrDefault() != null && data.Select(s => s.IsERPF).FirstOrDefault())
            {
                dt.Columns.Add("Employer PF", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsERESI).FirstOrDefault())
            {
                dt.Columns.Add("Employer ESI", typeof(decimal));
            }
            dt.Columns.Add("Bonus", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsGratuity).FirstOrDefault())
            {
                dt.Columns.Add("Gratuity", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsPLI).FirstOrDefault())
            {
                dt.Columns.Add("PLI", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsMobileDatacard).FirstOrDefault())
            {
                dt.Columns.Add("Mobile or Datacard", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsMediclaim).FirstOrDefault())
            {
                dt.Columns.Add("MediClaim", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsOtherPerks).FirstOrDefault())
            {
                dt.Columns.Add("Other Perks", typeof(decimal));
            }
            dt.Columns.Add("CTC", typeof(decimal));

            if (data.Any())
            {
                foreach (PayrollBO result in data)
                {
                    DataRow newRow = dt.NewRow();
                    newRow["Department"] = Convert.ToString(result.Operation);
                    newRow["Business Unit"] = Convert.ToString(result.BusinessUnitIDs);
                    newRow["Fixed Gross"] = result.FixedGross.ToString("##,#.00");
                    newRow["Basic"] = result.Basic.ToString("##,#.00");
                    newRow["HRA"] = result.HRA.ToString("##,#.00");
                    newRow["Medical"] = result.MedicalAllowance.ToString("##,#.00");
                    newRow["Conveyance"] = result.Conveyance.ToString("##,#.00");
                    newRow["Special Allowance"] = result.SplAllow.ToString("##,#.00");
                    if (result.IsEducationAllow)
                    {
                        newRow["Education_Allowance"] = result.EducationAllowance.ToString("##,#.00");
                    }
                    newRow["Paper&Magazine_Allowance"] = result.PaperMagazine.ToString("##,#.00");
                    newRow["Monsoon_Allowance"] = result.MonsoonAllowance.ToString("##,#.00");
                    if (result.IsOverTime)
                    {
                        newRow["Over_Time"] = result.OverTime.ToString("##,#.00");
                    }
                    newRow["Other_Earnings"] = result.OtherEarnings.ToString("##,#.00");
                    newRow["Paid Gross"] = result.PaidGross.ToString("##,#.00");
                    if (result.IsEEPF)
                    {
                        newRow["Employee PF"] = result.EEPF.ToString("##,#.00");
                    }
                    if (result.IsEEESI)
                    {
                        newRow["Employee ESI"] = result.EEESI.ToString("##,#.00");
                    }
                    if (result.IsTDS)
                    {
                        newRow["TDS"] = result.TDS.ToString("##,#.00");
                    }
                    if (result.IsPT)
                    {
                        newRow["PT"] = result.PT.ToString("##,#.00");
                    }
                    newRow["Loan"] = result.Loans.ToString("##,#.00");
                    newRow["Other Deduction"] = result.Deduction.ToString("##,#.00");
                    newRow["Total Deductions"] = (result.Deduction + result.Loans + result.PT + result.TDS + result.EEESI + result.EEPF).ToString("##,#.00");
                    newRow["Net Payable"] = result.NetSalary.ToString("##,#.00");
                    if (result.IsERPF)
                    {
                        newRow["Employer PF"] = result.ERPF.ToString("##,#.00");
                    }
                    if (result.IsERESI)
                    {
                        newRow["Employer ESI"] = result.ERESI.ToString("##,#.00");
                    }
                    newRow["Bonus"] = result.Bonus.ToString("##,#.00");
                    if (result.IsGratuity)
                    {
                        newRow["Gratuity"] = result.Gratuity.ToString("##,#.00");
                    }
                    if (result.IsPLI)
                    {
                        newRow["PLI"] = result.PLI.ToString("##,#.00");
                    }
                    if (result.IsMobileDatacard)
                    {
                        newRow["Mobile or Datacard"] = result.MobileDataCard.ToString("##,#.00");
                    }
                    if (result.IsMediclaim)
                    {
                        newRow["MediClaim"] = result.Medicalclaim.ToString("##,#.00");
                    }
                    if (result.IsOtherPerks)
                    {
                        newRow["Other Perks"] = result.OtherPerks.ToString("##,#.00");
                    }
                    newRow["CTC"] = result.CTC.ToString("##,#.00");
                    dt.Rows.Add(newRow);
                }
            }

            return ExportDataTableExcel("Location_Wise_Consolidated_rpt", dt);
        }

        #endregion Monthly Payroll Component Excel

        #region Monthly Payroll Report

        public ActionResult MonthlyPayrollExcel(string formName, string businessUnitList, int? departmentId, string component, int financialYearId)
        {
            //FileName = Utility.GetSession("CompanyName") + "_" + (DateTime.Now.Year.ToString() + "-" + (Convert.ToInt32(DateTime.Now.Year.ToString().Remove(0, 2)) + 1)) + "_Salary_Component_report" + ".xls";
            PayrollRepository payrollRepository = new PayrollRepository();

            DataTable data = payrollRepository.Rpt_searchemployeepayroll(businessUnitList, departmentId, component, financialYearId);

            //dt.Columns.Add("Employee ID");
            data.Columns["EmpCode"].ColumnName = "Emp Code";
            data.Columns["EmpName"].ColumnName = "Emp Name";
            data.Columns["JoiningDate"].ColumnName = "Joining Date";
            data.Columns["Business_Unit"].ColumnName = "Business_Unit";
            data.Columns["Department_Name"].ColumnName = "Department_Name";
            //foreach (DataColumn col in data.Columns)
            //{
            //    switch (col.ColumnName.ToString())
            //    {
            //        case "EmpCode":
            //            dt.Columns.Add("Emp Code", typeof(string));
            //            break;

            //        case "EmpName":
            //            dt.Columns.Add("Emp Name", typeof(string));
            //            break;

            //        case "JoiningDate":
            //            dt.Columns.Add("Joining Date", typeof(DateTime));
            //            break;

            //        case "Business_Unit":
            //            dt.Columns.Add("Business_Unit", typeof(string));
            //            break;

            //        case "Department_Name":
            //            dt.Columns.Add("Department_Name", typeof(string));
            //            break;

            //        default:
            //            dt.Columns.Add(col.ColumnName, typeof(decimal));
            //            break;
            //    }
            //}

            //if (data.Rows.Count > 0)
            //{
            //    foreach (var result in data.Rows)
            //    {
            //        List<string> newRowList = new List<string>();
            //        for (int i = 0; i < ((DataRow)(result)).ItemArray.Length; i++)
            //        {
            //            if (i >= 5)
            //            {
            //                if (!string.IsNullOrWhiteSpace(((DataRow)(result)).ItemArray[i].ToString()))
            //                {
            //                    newRowList.Add(Convert.ToDecimal(((DataRow)(result)).ItemArray[i]).ToString("#,##.00"));
            //                }
            //                else
            //                {
            //                    newRowList.Add((((DataRow)(result)).ItemArray[i] ?? "").ToString());
            //                }
            //            }
            //            else if (i == 0)
            //            {
            //                newRowList.Add(((DataRow)(result)).ItemArray[0].ToString());
            //            }
            //            else
            //            {
            //                newRowList.Add(((DataRow)(result)).ItemArray[i].ToString());
            //            }
            //            //if (i == 0)
            //            //{
            //            //    string BU = string.Empty, Department = string.Empty;
            //            //    if (((DataRow)(result)).ItemArray[3] != null && ((DataRow)(result)).ItemArray[3].ToString() != "")
            //            //    {
            //            //        BU = ((DataRow)(result)).ItemArray[3].ToString().ToUpper().Substring(0, 3);
            //            //    }
            //            //    if (((DataRow)(result)).ItemArray[4] != null && ((DataRow)(result)).ItemArray[4].ToString() != "")
            //            //    {
            //            //        Department = ((DataRow)(result)).ItemArray[4].ToString();
            //            //    }
            //            //    newRowList.Add(BU + "_" + ((Department.Length > 2) ? Department.Substring(0, 3) : Department) + "_" + ((DataRow)(result)).ItemArray[i].ToString());
            //            //}
            //        }
            //        dt.Rows.Add(newRowList.ToArray());
            //    }
            //}
            return ExportDataTableExcel("Salary_Component_report", data, false, "none");
        }

        #endregion Monthly Payroll Report

        #region Payroll History

        public ActionResult IndividualPayrollExcel(string formName, int employeeId)
        {
            PayrollRepository payrollRepository = new PayrollRepository();

            List<PayrollBO> data = payrollRepository.SearchPayrollHistory(employeeId);

            DataTable dt = new DataTable();

            dt.Columns.Add("Employee_Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Date of Joining", typeof(DateTime));
            dt.Columns.Add("Business Unit", typeof(string));
            dt.Columns.Add("Month", typeof(string));
            dt.Columns.Add("Year", typeof(string));
            dt.Columns.Add("Fixed Gross", typeof(decimal));
            dt.Columns.Add("Payable days", typeof(string));
            dt.Columns.Add("Basic", typeof(decimal));
            dt.Columns.Add("HRA", typeof(decimal));
            dt.Columns.Add("Medical", typeof(decimal));
            dt.Columns.Add("Conveyance", typeof(decimal));
            dt.Columns.Add("Special Allowance", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsEducationAllow).FirstOrDefault())
            {
                dt.Columns.Add("Education_Allowance", typeof(decimal));
            }
            dt.Columns.Add("Paper&Magazine_Allowance", typeof(decimal));
            dt.Columns.Add("Monsoon_Allowance", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsOverTime).FirstOrDefault())
            {
                dt.Columns.Add("Over_Time", typeof(decimal));
            }
            dt.Columns.Add("Other_Earnings", typeof(decimal));
            dt.Columns.Add("Paid Gross", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsEEPF).FirstOrDefault())
            {
                dt.Columns.Add("Employee PF", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsEEESI).FirstOrDefault())
            {
                dt.Columns.Add("Employee ESI", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsTDS).FirstOrDefault())
            {
                dt.Columns.Add("TDS", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsPT).FirstOrDefault())
            {
                dt.Columns.Add("PT", typeof(decimal));
            }
            dt.Columns.Add("Loan", typeof(decimal));
            dt.Columns.Add("Other Deduction", typeof(decimal));
            dt.Columns.Add("Total Deductions", typeof(decimal));
            dt.Columns.Add("Net Payable", typeof(decimal));

            if (data.FirstOrDefault() != null && data.Select(s => s.IsERPF).FirstOrDefault())
            {
                dt.Columns.Add("Employer PF", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsERESI).FirstOrDefault())
            {
                dt.Columns.Add("Employer ESI", typeof(decimal));
            }
            dt.Columns.Add("Bonus", typeof(decimal));
            if (data.FirstOrDefault() != null && data.Select(s => s.IsGratuity).FirstOrDefault())
            {
                dt.Columns.Add("Gratuity", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsPLI).FirstOrDefault())
            {
                dt.Columns.Add("PLI", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsMediclaim).FirstOrDefault())
            {
                dt.Columns.Add("MediClaim", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsMobileDatacard).FirstOrDefault())
            {
                dt.Columns.Add("Mobile or Datacard", typeof(decimal));
            }
            if (data.FirstOrDefault() != null && data.Select(s => s.IsOtherPerks).FirstOrDefault())
            {
                dt.Columns.Add("Other Perks", typeof(decimal));
            }
            dt.Columns.Add("CTC", typeof(decimal));

            if (data.Any())
            {
                foreach (PayrollBO result in data)
                {
                    DataRow newRow = dt.NewRow();
                    newRow["Employee_Code"] = result.EmployeeCode;
                    newRow["Employee Name"] = result.EmployeeName;
                    newRow["Date of Joining"] = result.DOJ.ToString("dd-MMM-yyyy");
                    newRow["Business Unit"] = Convert.ToString(result.Operation);
                    newRow["Month"] = result.Month;
                    newRow["Year"] = result.YearID;
                    newRow["Fixed Gross"] = result.FixedGross.ToString("##,#.00");
                    newRow["Payable days"] = result.WorkingDays;
                    newRow["Basic"] = result.Basic.ToString("##,#.00");
                    newRow["HRA"] = result.HRA.ToString("##,#.00");
                    newRow["Medical"] = result.MedicalAllowance.ToString("##,#.00");
                    newRow["Conveyance"] = result.Conveyance.ToString("##,#.00");
                    newRow["Special Allowance"] = result.SplAllow.ToString("##,#.00");
                    if (result.IsEducationAllow)
                    {
                        newRow["Education_Allowance"] = result.EducationAllowance.ToString("##,#.00");
                    }
                    newRow["Paper&Magazine_Allowance"] = result.PaperMagazine.ToString("##,#.00");
                    newRow["Monsoon_Allowance"] = result.MonsoonAllowance.ToString("##,#.00");
                    if (result.IsOverTime)
                    {
                        newRow["Over_Time"] = result.OverTime.ToString("##,#.00");
                    }
                    newRow["Other_Earnings"] = result.OtherEarnings.ToString("##,#.00");
                    newRow["Paid Gross"] = result.PaidGross.ToString("##,#.00");
                    if (result.IsEEPF)
                    {
                        newRow["Employee PF"] = result.EEPF.ToString("##,#.00");
                    }
                    if (result.IsEEESI)
                    {
                        newRow["Employee ESI"] = result.EEESI.ToString("##,#.00");
                    }
                    if (result.IsTDS)
                    {
                        newRow["TDS"] = result.TDS.ToString("##,#.00");
                    }
                    if (result.IsPT)
                    {
                        newRow["PT"] = result.PT.ToString("##,#.00");
                    }
                    newRow["Loan"] = result.Loans.ToString("##,#.00");
                    newRow["Other Deduction"] = result.Deduction.ToString("##,#.00");
                    newRow["Total Deductions"] = (result.Deduction + result.Loans + result.PT + result.TDS + result.EEESI + result.EEPF).ToString("##,#.00");
                    newRow["Net Payable"] = result.NetSalary.ToString("##,#.00");
                    if (result.IsERPF)
                    {
                        newRow["Employer PF"] = result.ERPF.ToString("##,#.00");
                    }
                    if (result.IsERESI)
                    {
                        newRow["Employer ESI"] = result.ERESI.ToString("##,#.00");
                    }
                    newRow["Bonus"] = result.Bonus.ToString("##,#.00");
                    if (result.IsGratuity)
                    {
                        newRow["Gratuity"] = result.Gratuity.ToString("##,#.00");
                    }
                    if (result.IsPLI)
                    {
                        newRow["PLI"] = result.PLI.ToString("##,#.00");
                    }
                    if (result.IsMediclaim)
                    {
                        newRow["MediClaim"] = result.Medicalclaim.ToString("##,#.00");
                    }
                    if (result.IsMobileDatacard)
                    {
                        newRow["Mobile or Datacard"] = result.MobileDataCard.ToString("##,#.00");
                    }
                    if (result.IsOtherPerks)
                    {
                        newRow["Other Perks"] = result.OtherPerks.ToString("##,#.00");
                    }
                    newRow["CTC"] = result.CTC.ToString("##,#.00");
                    dt.Rows.Add(newRow);
                }
            }
            return ExportDataTableExcel("Individual_Salary_report", dt);
        }

        #endregion Payroll History

        #region Payroll Covering Letter

        public ActionResult CoveringLetter(string formName, int monthId, int yearId, string businessUnitList, string month, string year)
        {
            PayrollRepository payrollRepository = new PayrollRepository();

            List<PayrollBO> data = payrollRepository.Coveringletter(monthId, yearId, businessUnitList);

            DataTable dt = new DataTable
            {
                TableName = "Payroll_Covering_Letter"// Excel sheet name.
            };
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Bank Name", typeof(string));
            dt.Columns.Add("Account No", typeof(string));
            dt.Columns.Add("IFSC Code", typeof(string));
            dt.Columns.Add("Net Payable", typeof(decimal));

            foreach (PayrollBO i in data)
            {
                dt.Rows.Add(
                        i.EmployeeName,
                        i.UserMessage,
                       i.UserName,
                        i.Operation,
                        i.Gross);
            }
            return ExportDataTableExcel("Payroll_Covering_Letter", dt);
        }

        #endregion Payroll Covering Letter

        #region Payroll Employee Component

        public void ExportEmployeeComponent(string formName, string downloadFileName, int month, int year, string location, int? department, int? statusId)
        {
            string fileName = downloadFileName + ".xlsx";

            PayrollRepository payrollRepository = new PayrollRepository();

            DataTable data = payrollRepository.ExportEmployeeComponent(month, year, location, department, statusId);

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(data, formName);

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                using (MemoryStream myMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(myMemoryStream);
                    myMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        #endregion Payroll Employee Component

        #region Employee Payroll Summary

        public ActionResult SummaryPayrollExcel(string formName, int monthId, int yearId, string businessUnitList, int? department, int? statusId, int? proceed)
        {
            decimal netSalary = 0, eepf = 0, eeesi = 0, erpf = 0, eresi = 0, tds = 0, gratuity = 0, pt = 0, deduction = 0, mobileDataCard = 0,
                bonus = 0, pli = 0, otherPerks = 0, medicalclaim = 0, loans = 0;

            PayrollRepository payrollRepository = new PayrollRepository();

            List<PayrollBO> data = payrollRepository.SearchPayroll(monthId, yearId, businessUnitList, department, statusId, proceed);

            DataTable dt = new DataTable();

            dt.Columns.Add("Net Salary", typeof(decimal));
            dt.Columns.Add("Employee PF", typeof(decimal));
            dt.Columns.Add("Employee ESI", typeof(decimal));
            dt.Columns.Add("Employer PF", typeof(decimal));
            dt.Columns.Add("Employer ESI", typeof(decimal));
            dt.Columns.Add("TDS", typeof(decimal));
            dt.Columns.Add("Gratuity", typeof(decimal));
            dt.Columns.Add("Professional Tax", typeof(decimal));
            dt.Columns.Add("Other Deductions", typeof(decimal));
            dt.Columns.Add("Mobile/Datacard", typeof(decimal));
            dt.Columns.Add("Bonus", typeof(decimal));
            dt.Columns.Add("Performance Linked Incentive", typeof(decimal));
            dt.Columns.Add("Other Perks", typeof(decimal));
            dt.Columns.Add("Mediclaim", typeof(decimal));
            dt.Columns.Add("Loan", typeof(decimal));

            if (data != null && data.Any())
            {
                foreach (PayrollBO result in data)
                {
                    netSalary += result.NetSalary;
                    eepf += result.EEPF;
                    eeesi += result.EEESI;
                    erpf += result.ERPF;
                    eresi += result.ERESI;
                    tds += result.TDS;
                    gratuity += result.Gratuity;
                    pt += result.PT;
                    deduction += result.Deduction;
                    mobileDataCard += result.MobileDataCard;
                    bonus += result.Bonus;
                    pli += result.PLI;
                    otherPerks += result.OtherPerks;
                    medicalclaim += result.Medicalclaim;
                    loans += result.Loans;
                }
                dt.Rows.Add(
                       netSalary,
                      eepf,
                      eeesi,
                      erpf,
                       eresi, tds, gratuity, pt, deduction, mobileDataCard, bonus, pli, otherPerks,
                       medicalclaim, loans);
            }
            return ExportDataTableExcel("Employee_Payroll_Summary", dt);
        }

        public ActionResult SummaryPayroll_V2Excel(string formName, int monthId, int yearId, string businessUnitList)
        {
            EmployeePayrollRepository employeePayrollRepository = new EmployeePayrollRepository();

            DataSet data = employeePayrollRepository.GetPayrollSummaryList(monthId, yearId, businessUnitList);

            DataTable dt = new DataTable
            {
                TableName = "Employee_Payroll_Summary"// Excel sheet name.
            };

            foreach (DataColumn col in data.Tables[0].Columns)
            {
                dt.Columns.Add(col.ColumnName, Type.GetType(Convert.ToString(col.DataType.FullName)));
            }

            if (data.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in data.Tables[0].Rows)
                {
                    List<string> newRowList = new List<string>();
                    foreach (DataColumn col in data.Tables[0].Columns)
                    {
                        newRowList.Add(row[col.ColumnName].ToString());
                    }
                    dt.Rows.Add(newRowList.ToArray());
                }
            }

            return ExportDataTableExcel("Employee_Payroll_Summary", dt);
        }

        #endregion Employee Payroll Summary

        #endregion Payroll Excel

        #region Login History Excel

        public ActionResult LoginExcel(string formName, string fromDate, string toDate)
        {
            UserLoginRepository userLoginRepository = new UserLoginRepository();

            DateTime? fDate = null;
            DateTime? tDate = null;

            if (fromDate != "")
            {
                fDate = Convert.ToDateTime(fromDate);
            }
            if (toDate != "")
            {
                tDate = Convert.ToDateTime(toDate);
            }

            List<UserLoginBo> data = userLoginRepository.SearchLoginHistory(fDate, tDate);

            DataTable dt = new DataTable
            {
                TableName = "Login_History"// Excel sheet name.
            };
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Login Time", typeof(DateTime));
            dt.Columns.Add("Last Accessed Time", typeof(DateTime));
            dt.Columns.Add("Navigation", typeof(string));
            dt.Columns.Add("Browser Name", typeof(string));
            dt.Columns.Add("IP Address", typeof(string));
            dt.Columns.Add("Location", typeof(string));
            dt.Columns.Add("Latitude/Longitude", typeof(string));

            foreach (UserLoginBo i in data)
            {
                dt.Rows.Add(
                        i.UserName,
                        i.LoginTime != "" ? Convert.ToDateTime(i.LoginTime).ToString("dd-MMM-yyyy HH:mm:ss tt") : "",
                        i.LogoutTime != "" ? Convert.ToDateTime(i.LogoutTime).ToString("dd-MMM-yyyy HH:mm:ss tt") : "",
                        i.FormHitCount != 0 ? i.FormHitCount.ToString() : "",
                        i.BrowserName,
                        i.IpAddress,
                        i.Location,
                        i.Latitude);
            }
            return ExportDataTableExcel("Login_History", dt, true);
        }

        #endregion Login History Excel

        #region Master Data Excel

        public ActionResult MasterDataExcel(string formName, string mainTable, string dependentTable)
        {
            string reportName;
            switch (mainTable)
            {
                case "BusinessUnit":
                    if (dependentTable.ToLower() != "null")
                    {
                        reportName = "Sub_Business_Unit";
                    }
                    else
                    {
                        reportName = "Region";
                    }
                    break;

                case "AssetType":
                    reportName = "Asset_Type";
                    break;

                case "CustomerDepartment":
                    reportName = "Customer_Department";
                    break;

                case "CustomerDesignation":
                    reportName = "Customer_Designation";
                    break;

                case "LeaveTypes":
                    reportName = "Leave_Types";
                    break;

                case "DocumentType":
                    reportName = "Document_Type";
                    break;

                case "ExitReason":
                    reportName = "Exit_Reason";
                    break;

                case "EmployeeGrade":
                    reportName = "Employee_Grade";
                    break;

                case "BloodGroup":
                    reportName = "Blood_Group";
                    break;

                case "DestinationCities":
                    reportName = "Destination_Cities";
                    break;

                case "UOM":
                    reportName = "Unit_of_Measurement";
                    break;

                default:
                    reportName = mainTable;
                    break;
            }

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            List<MasterDataBo> data = masterDataRepository.SearchMasterData(mainTable, dependentTable);

            DataTable dt = new DataTable
            {
                TableName = reportName// Excel sheet name.
            };
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            if (dependentTable.ToLower() != "null")
            {
                switch (dependentTable)
                {
                    case "BusinessUnit":
                        dependentTable = "Business Unit";
                        break;
                }
                dt.Columns.Add(dependentTable, typeof(string));
            }
            if (mainTable == "DestinationCities")
            {
                dt.Columns.Add("Type", typeof(string));
            }

            foreach (MasterDataBo i in data)
            {
                DataRow row = dt.NewRow();
                row["Code"] = i.Code;
                row["Description"] = i.Description;
                if (dependentTable.ToLower() != "null")
                {
                    row[dependentTable] = i.DependantDropDown.Name;
                }
                if (i.MainTable == "DestinationCities")
                {
                    row["Type"] = i.TypeName;
                }
                dt.Rows.Add(row);
            }
            return ExportDataTableExcel(reportName, dt);
        }

        #region Claim Category Excel

        public ActionResult ClaimsExcel(string formName)
        {
            MasterDataBo masterDataBo = new MasterDataBo();
            ClaimCategoryRepository claimCategoryRepository = new ClaimCategoryRepository();
            masterDataBo.DomainId = Utility.DomainId();

            List<MasterDataBo> data = claimCategoryRepository.GetClaimsCatergoryList(masterDataBo);

            DataTable dt = new DataTable
            {
                TableName = "Claim_Category"// Excel sheet name.
            };
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (MasterDataBo i in data)
            {
                dt.Rows.Add(
                        i.Code,
                        i.Description);
            }
            return ExportDataTableExcel("Claim_Category", dt);
        }

        #endregion Claim Category Excel

        #region Business Unit Excel

        public ActionResult BusinessUnitExcel(string formName)
        {
            BusinessUnitBo businessUnitBo = new BusinessUnitBo();
            BusinessUnitRepository businessUnitRepository = new BusinessUnitRepository();

            List<BusinessUnitBo> data = businessUnitRepository.SearchBusinessUnit(businessUnitBo);

            DataTable dt = new DataTable
            {
                TableName = "Business_Unit"// Excel sheet name.
            };
            dt.Columns.Add("Region", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Address", typeof(string));

            foreach (BusinessUnitBo i in data)
            {
                dt.Rows.Add(
                    i.BusinessUnitName,
                        i.Code,
                        i.Description,
                        i.Address);
            }
            return ExportDataTableExcel("Business_Unit", dt);
        }

        #endregion Business Unit Excel

        #endregion Master Data Excel

        #region Monthly Grid BU Wise

        public ActionResult GridBusinessUnitWiseExcel(int monthId, int yearId, int businessUnitId)
        {
            string dateFormat = DateTime.Now.ToString("dd-MMM-yyyy");
            string reportName = "BU_wise_Sales_report-Grid";

            StringBuilder sb = new StringBuilder();
            string fileName = reportName + "_" + dateFormat + ".xls";

            ReportRepository reportRepository = new ReportRepository();
            ReportBo reportBo = new ReportBo
            {
                MonthId = monthId,
                YearId = yearId,
                BusinessUnitId = businessUnitId
            };
            List<ReportBo> data = reportRepository.GetBusinessUnitSalesGrid(reportBo);

            sb.Append("<table style='1px solid black; font-size:14px; font-family:Cambria;' border='1' >");
            sb.Append(GetExcelHeader(reportName));
            sb.Append("<tr>");
            sb.Append("<td style='width:180px;background-color:#b0d895;'><b>Business Unit</b></td>");
            sb.Append("<td style='width:180px;background-color:#b0d895;'><b>Sales Executive</b></td>");
            sb.Append("<td style='width:180px;background-color:#b0d895;'><b>Customer</b></td>");
            sb.Append("<td style='width:180px;background-color:#b0d895;'><b>Achieved</b></td>");
            sb.Append("<td style='width:180px;background-color:#b0d895;'><b>Planned</b></td>");

            sb.Append("</tr>");

            if (data != null && data.Any())
            {
                foreach (ReportBo result in data)
                {
                    sb.Append("<tr style='height:20px;'>");
                    sb.Append("<td>" + result.BusinessUnit + "</td>");
                    sb.Append("<td>" + result.SalesExecutive + "</td>");
                    sb.Append("<td>" + result.Customer + "</td>");
                    sb.Append("<td style='text-align:center'>" + result.Achieved + "</td>");
                    sb.Append("<td style='text-align:center'>" + result.Expected + "</td>");
                }
            }

            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "data:application/vnd.ms-excel";
            byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
            return File(buffer, "data:application/vnd.ms-excel");
        }

        #endregion Monthly Grid BU Wise

        #region Loan Details

        #region Complete Loan Report

        public ActionResult CompleteLoanReportExcel(string fromDate, string toDate, string businessUnitId, int statusId, string approverId, int employeeId, int? typeId)
        {
            EmployeeLoanReportBo employeeLoanReportBo = new EmployeeLoanReportBo();
            EmployeeLoanRepository employeeLoanRepository = new EmployeeLoanRepository();

            if (employeeLoanReportBo.FromDate == null)
            {
                employeeLoanReportBo.FromDate = DateTime.Now.AddDays(-180);
            }

            if (employeeLoanReportBo.ToDate == null)
            {
                employeeLoanReportBo.ToDate = DateTime.Now;
            }
            employeeLoanReportBo.BusinessUnitId = string.IsNullOrEmpty(businessUnitId) ? 0 : Convert.ToInt32(businessUnitId);
            employeeLoanReportBo.StatusId = statusId;
            employeeLoanReportBo.ApproverId = string.IsNullOrEmpty(approverId) ? 0 : Convert.ToInt32(approverId);
            employeeLoanReportBo.EmployeeId = employeeId;
            employeeLoanReportBo.TypeId = typeId;

            List<EmployeeLoanReportBo> data = employeeLoanRepository.GetCompleteLoanList(employeeLoanReportBo);

            DataTable dt = new DataTable
            {
                TableName = "Loan_analyses_report"// Excel sheet name.
            };
            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Requested Date", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Loan Type", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Approver Status", typeof(string));
            dt.Columns.Add("Fin Approver Status", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));
            dt.Columns.Add("Outstanding Amount", typeof(decimal));
            dt.Columns.Add("Repayment Status", typeof(string));

            foreach (EmployeeLoanReportBo i in data)
            {
                string outstandingAmount = i.OutstandingAmount != 0 ? i.OutstandingAmount.ToString("##,#.00") : null;
                dt.Rows.Add(
                        i.EmployeeCode,
                        i.FullName,
                        i.RequestedDate.ToString("dd-MMM-yyyy"),
                        i.Amount.ToString("##,#.00"),
                        i.LoanType,
                        i.Status,
                        i.ApproverStatus,
                        i.FinApproverStatus,
                        i.BusinessUnit,
                        outstandingAmount,
                        (i.Status == "Settled" || i.Status == "Foreclosed" || i.Status == "Closed") ? i.RepaymentStatus : "");
            }
            return ExportDataTableExcel("Loan_analyses_report", dt);
        }

        #endregion Complete Loan Report

        #region Request

        public ActionResult LoanRequestExcel(string formName, int statusId)
        {
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo();
            EmployeeLoanRepository employeeLoanRepository = new EmployeeLoanRepository();

            employeeLoanBo.StatusId = statusId;

            List<EmployeeLoanBo> data = employeeLoanRepository.GetLoanRequestList(employeeLoanBo);

            DataTable dt = new DataTable
            {
                TableName = "Loan_Request"// Excel sheet name.
            };
            dt.Columns.Add("Loan Type", typeof(string));
            dt.Columns.Add("Requested Date", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Total Amount", typeof(decimal));
            dt.Columns.Add("Approver Name", typeof(string));
            dt.Columns.Add("Approved On", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (EmployeeLoanBo i in data)
            {
                string type = (i.TypeId == 0) ? "Salary Advance" : "Soft Loan";
                string approvalDate = i.ApprovedDate != DateTime.MinValue ? Convert.ToDateTime(i.ApprovedDate).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                        type,
                        i.RequestedDate.ToString("dd-MMM-yyyy"),
                        i.Amount.ToString("##,#.00"),
                        Convert.ToDecimal(i.TotalAmount).ToString("##,#.00"),
                        i.ApproverName,
                        approvalDate,
                        i.LoanStatusId.Name);
            }
            return ExportDataTableExcel("Loan_Request", dt);
        }

        #endregion Request

        #region Approval

        public ActionResult LoanApproveExcel(string formName, int statusId)
        {
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo();
            EmployeeLoanRepository employeeLoanRepository = new EmployeeLoanRepository();

            employeeLoanBo.StatusId = statusId;

            List<EmployeeLoanBo> data = employeeLoanRepository.GetEmployeeLoanApproveList(employeeLoanBo);

            DataTable dt = new DataTable
            {
                TableName = "Loan_Approval"// Excel sheet name.
            };
            dt.Columns.Add("Loan Type", typeof(string));
            dt.Columns.Add("Requested Date", typeof(string));
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Total Amount", typeof(decimal));
            dt.Columns.Add("Status", typeof(string));

            foreach (EmployeeLoanBo i in data)
            {
                string type = (i.TypeId == 0) ? "Salary Advance" : "Soft Loan";
                dt.Rows.Add(
                        type,
                        i.RequestedDate.ToString("dd-MMM-yyyy"),
                        i.Requester,
                        i.Amount,
                        i.TotalAmount,
                        i.StatusName);
            }
            return ExportDataTableExcel("Loan_Approval", dt);
        }

        #endregion Approval

        #region Financial Approve

        public ActionResult LoanFinancialExcel(string formName, int statusId)
        {
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo();
            EmployeeLoanRepository employeeLoanRepository = new EmployeeLoanRepository();

            employeeLoanBo.StatusId = statusId;

            List<EmployeeLoanBo> data = employeeLoanRepository.SearchFinanceApproval(employeeLoanBo);

            DataTable dt = new DataTable
            {
                TableName = "Loan_Approval-Finance"// Excel sheet name.
            };
            dt.Columns.Add("Loan Type", typeof(string));
            dt.Columns.Add("Requested Date", typeof(string));
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Total Amount", typeof(decimal));
            dt.Columns.Add("Status", typeof(string));

            foreach (EmployeeLoanBo i in data)
            {
                string type = (i.TypeId == 0) ? "Salary Advance" : "Soft Loan";
                dt.Rows.Add(
                        type,
                        i.RequestedDate.ToString("dd-MMM-yyyy"),
                        i.Requester,
                        i.Amount,
                        i.TotalAmount,
                        i.StatusName);
            }
            return ExportDataTableExcel("Loan_Approval-Finance", dt);
        }

        #endregion Financial Approve

        #region Settlement

        public ActionResult LoanSettleExcel(string formName, int statusId)
        {
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo();
            EmployeeLoanRepository employeeLoanRepository = new EmployeeLoanRepository();

            employeeLoanBo.StatusId = statusId;

            List<EmployeeLoanBo> data = employeeLoanRepository.SearchLoanSettlement(statusId);

            DataTable dt = new DataTable
            {
                TableName = "Loan_Settlement"// Excel sheet name.
            };
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Total Amount", typeof(decimal));
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Requested On", typeof(string));
            dt.Columns.Add("Approver", typeof(string));
            dt.Columns.Add("Approved On", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (EmployeeLoanBo i in data)
            {
                string approvalDate = i.ApprovedDate != DateTime.MinValue ? Convert.ToDateTime(i.ApprovedDate).ToString("dd-MMM-yyyy") : null;
                string requestedDate = i.CreatedOn != DateTime.MinValue ? Convert.ToDateTime(i.CreatedOn).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                        i.Amount.ToString("##,##0.00"),
                        Convert.ToDecimal(i.TotalAmount).ToString("##,##0.00"),
                        i.Requester,
                        requestedDate,
                        i.ApproverName,
                        approvalDate,
                        i.StatusName);
            }
            return ExportDataTableExcel("Loan_Settlement", dt);
        }

        #endregion Settlement

        #region Configuration

        public ActionResult LoanConfigurationExcel(string formName)
        {
            EmployeeLoanRepository employeeLoanRepository = new EmployeeLoanRepository();

            List<EmployeeLoanBo> data = employeeLoanRepository.SearchEmployeeLoanConfiguration();

            DataTable dt = new DataTable
            {
                TableName = "Loan_Configuration"// Excel sheet name.
            };
            dt.Columns.Add("Designation", typeof(string));
            dt.Columns.Add("Band", typeof(string));
            dt.Columns.Add("Grade", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Tenure", typeof(string));
            dt.Columns.Add("Financial charges and ROI", typeof(string));

            foreach (EmployeeLoanBo i in data)
            {
                dt.Rows.Add(
                        i.DesignationName,
                        i.BandName,
                        i.GradeName,
                        i.Amount,
                        i.Tenure,
                        i.InterestRate);
            }
            return ExportDataTableExcel("Loan_Configuration", dt);
        }

        #endregion Configuration

        #region Foreclosure

        public ActionResult LoanForeclosureExcel(string formName, int statusId)
        {
            EmployeeLoanBo employeeLoanBo = new EmployeeLoanBo();
            EmployeeLoanRepository employeeLoanRepository = new EmployeeLoanRepository();

            employeeLoanBo.StatusId = statusId;

            List<EmployeeLoanBo> data = employeeLoanRepository.SearchLoanForeclosure(statusId);

            DataTable dt = new DataTable
            {
                TableName = "Loan_Foreclosure"// Excel sheet name.
            };
            dt.Columns.Add("Loan Type", typeof(string));
            dt.Columns.Add("Outstanding Amount", typeof(decimal));
            if (data != null && data.Any())
            {
                if (data.Where(S => S.StatusName == "Foreclosed").Count() != 0)
                {
                    dt.Columns.Add("Total Amount", typeof(decimal));
                }
            }
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Settled On", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            if (data != null)
            {
                foreach (EmployeeLoanBo i in data)
                {
                    string type = (i.TypeId == 0) ? "Salary Advance" : "Soft Loan";
                    string settledOn = i.CreatedOn != DateTime.MinValue
                        ? Convert.ToDateTime(i.CreatedOn).ToString("dd-MMM-yyyy")
                        : null;
                    DataRow row = dt.NewRow();
                    row["Loan Type"] = type;
                    row["Outstanding Amount"] = i.Amount.ToString("##,#.00");
                    if (i.StatusName == "Foreclosed")
                    {
                        row["Total Amount"] = Convert.ToDecimal(i.TotalAmount).ToString("##,#.00");
                    }

                    row["Requester"] = i.Requester;
                    row["Settled On"] = settledOn;
                    row["Status"] = i.StatusName;
                    dt.Rows.Add(row);
                }
            }

            return ExportDataTableExcel("Loan_Foreclosure", dt);
        }

        #endregion Foreclosure

        #endregion Loan Details

        #region Employee Report

        #region Joining Report

        public ActionResult EmployeeJoiningReportExcel(DateTime? fromDate, DateTime? toDate, int? regionId, int? businessUnitId, string isActiveRecords)
        {
            EmployeeReportBo employeeReportBo = new EmployeeReportBo();
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            employeeReportBo.FromDate = fromDate;
            employeeReportBo.ToDate = toDate;
            employeeReportBo.RegionId = regionId;
            employeeReportBo.BusinessUnitId = businessUnitId;
            employeeReportBo.IsActiveRecords = (isActiveRecords == "" ? null : isActiveRecords);

            List<EmployeeReportBo> data = employeeMasterRepository.SearchEmployeeJoinReport(employeeReportBo);

            DataTable dt = new DataTable
            {
                TableName = "Employee_Joining_report"// Excel sheet name.
            };
            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Father Name", typeof(string));
            dt.Columns.Add("Region", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));
            dt.Columns.Add("Department", typeof(string));
            dt.Columns.Add("Designation", typeof(string));
            dt.Columns.Add("Date of Joining", typeof(string));
            dt.Columns.Add("Gender", typeof(string));
            dt.Columns.Add("Date of Birth", typeof(string));
            dt.Columns.Add("Marital Status", typeof(string));
            dt.Columns.Add("Present Address", typeof(string));
            dt.Columns.Add("Contact Number", typeof(string));
            dt.Columns.Add("Email ID", typeof(string));
            dt.Columns.Add("PF No", typeof(string));
            dt.Columns.Add("ESI No", typeof(string));
            dt.Columns.Add("Gross Salary", typeof(decimal));
            dt.Columns.Add("UAN", typeof(string));
            dt.Columns.Add("Bank Name", typeof(string));
            dt.Columns.Add("Bank Account No", typeof(string));
            dt.Columns.Add("IFSC Code", typeof(string));
            dt.Columns.Add("Aadhar Card", typeof(string));
            dt.Columns.Add("PAN", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (EmployeeReportBo i in data)
            {
                string dob = (i.Dob != null && i.Dob != DateTime.MinValue) ? Convert.ToDateTime(i.Dob).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                        i.Code,
                        i.FullName,
                        i.FatherName,
                        i.Region,
                        i.BusinessUnit,
                        i.Department,
                        i.Designation,
                        i.Doj.ToString("dd-MMM-yyyy"),
                        i.Gender,
                        dob,
                        i.MaritalStatus,
                        i.Address,
                        i.ContactNo,
                        i.EmailId,
                        i.PfNo,
                        i.EsiNo,
                        i.GrossSalary,
                        i.Uan,
                        i.BankName,
                        i.AccountNo,
                        i.IfscCode,
                        i.AadharId,
                        i.PanNo,
                        i.IsActiveRecords);
            }
            return ExportDataTableExcel("Employee_Joining_report", dt);
        }

        #endregion Joining Report

        #region Relieving Report

        public ActionResult EmployeeRelievingReportExcel(DateTime? fromDate, DateTime? toDate, int? regionId, int? businessUnitId)
        {
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            EmployeeReportBo employeeReportBo = new EmployeeReportBo
            {
                FromDate = fromDate,
                ToDate = toDate,
                RegionId = regionId,
                BusinessUnitId = businessUnitId
            };

            List<EmployeeReportBo> data = employeeMasterRepository.SearchEmployeeRelieveReport(employeeReportBo);

            DataTable dt = new DataTable
            {
                TableName = "Employee_Relieving_report"// Excel sheet name.
            };
            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Father Name", typeof(string));
            dt.Columns.Add("Region", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));
            dt.Columns.Add("Department", typeof(string));
            dt.Columns.Add("Designation", typeof(string));
            dt.Columns.Add("Date of Joining", typeof(string));
            dt.Columns.Add("Relieving Date", typeof(string));
            dt.Columns.Add("Gender", typeof(string));
            dt.Columns.Add("Date of Birth", typeof(string));
            dt.Columns.Add("Marital Status", typeof(string));
            dt.Columns.Add("Present Address", typeof(string));
            dt.Columns.Add("Contact Number", typeof(string));
            dt.Columns.Add("Email ID", typeof(string));
            dt.Columns.Add("PF No", typeof(string));
            dt.Columns.Add("ESI No", typeof(string));
            dt.Columns.Add("Gross Salary", typeof(decimal));
            dt.Columns.Add("UAN", typeof(string));
            dt.Columns.Add("Bank Name", typeof(string));
            dt.Columns.Add("Bank Account No", typeof(string));
            dt.Columns.Add("IFSC Code", typeof(string));
            dt.Columns.Add("Aadhar Card", typeof(string));
            dt.Columns.Add("PAN", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (EmployeeReportBo i in data)
            {
                string dob = (i.Dob != null && i.Dob != DateTime.MinValue) ? Convert.ToDateTime(i.Dob).ToString("dd-MMM-yyyy") : null;
                string relievingDate = i.RelievingDate != null ? Convert.ToDateTime(i.RelievingDate).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                        i.Code,
                        i.FullName,
                        i.FatherName,
                        i.Region,
                        i.BusinessUnit,
                        i.Department,
                        i.Designation,
                        i.Doj.ToString("dd-MMM-yyyy"),
                        relievingDate,
                        i.Gender,
                        dob,
                        i.MaritalStatus,
                        i.Address,
                        i.ContactNo,
                        i.EmailId,
                        i.PfNo,
                        i.EsiNo,
                        i.GrossSalary,
                        i.Uan,
                        i.BankName,
                        i.AccountNo,
                        i.IfscCode,
                        i.AadharId,
                        i.PanNo,
                        i.IsActiveRecords);
            }
            return ExportDataTableExcel("Employee_Relieving_report", dt);
        }

        #endregion Relieving Report

        #endregion Employee Report

        #region Header

        private string GetExcelHeader(string reportName)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<tr style:'font-family:Cambria;'><td><b> Company </b></td>");
            sb.Append("<td colspan=3>" + Utility.GetSession("CompanyName") + "</td></tr>");

            sb.Append("<tr style:'font-family:Cambria;><td><b> Report Name </b></td>");
            sb.Append("<td colspan=3>" + reportName.Replace('_', ' ') + "</td></tr>");

            sb.Append("<tr style:'font-family:Cambria;><td> <b>Created By </b></td>");
            sb.Append("<td colspan=3>" + Utility.GetSession("FirstName") + "</td></tr>");

            sb.Append("<tr style:'font-family:Cambria;><td><b> Created On </b></td>");
            sb.Append("<td colspan=3 style='text-align: left;'>" + DateTime.Now.ToString("dd-MMM-yyyy") + "</td></tr>");

            sb.Append("<tr style:'font-family:Cambria;><td><b> Report Source </b></td>");
            sb.Append("<td colspan=3>" + "OfficeBAU" + "</td></tr><tr></tr>");

            return sb.ToString();
        }

        #endregion Header

        #region OffBoard Process

        #region Approve

        public ActionResult OffBoardApproveExcel(string formName, int statusId, string code, string firstName)
        {
            EmployeeOffBoardProcessRepository employeeOffBoardProcessRepository = new EmployeeOffBoardProcessRepository();
            EmployeeOffBoardBo employeeOffBoardBo = new EmployeeOffBoardBo
            {
                StatusId = statusId,
                Code = code,
                FirstName = firstName,
                UserId = Utility.UserId(),
                DomainId = Utility.DomainId()
            };

            DataTable data = employeeOffBoardProcessRepository.SearchEmployeeOffBoardApprovalListForExport(employeeOffBoardBo);

            return ExportDataTableExcel("Offboard_Approval", data);
        }

        #endregion Approve

        #region HR Approve

        public ActionResult OffBoardHrApproveExcel(string formName, int statusId, string code, string firstName)
        {
            EmployeeOffBoardProcessRepository employeeOffBoardProcessRepository = new EmployeeOffBoardProcessRepository();
            EmployeeOffBoardBo employeeOffBoardBo = new EmployeeOffBoardBo
            {
                StatusId = statusId,
                Code = code,
                FirstName = firstName,
                UserId = Utility.UserId(),
                DomainId = Utility.DomainId()
            };

            DataTable data = employeeOffBoardProcessRepository.SearchEmployeeOffBoardHrApprovalListForExport(employeeOffBoardBo);

            return ExportDataTableExcel("Exit Process", data);
        }

        #endregion HR Approve

        #region Asset Clearance

        public ActionResult OffBoardAssetClearance(string formName, int statusId, string code, string firstName)
        {
            EmployeeOffBoardProcessRepository employeeOffBoardProcessRepository = new EmployeeOffBoardProcessRepository();
            EmployeeOffBoardBo employeeOffBoardBo = new EmployeeOffBoardBo
            {
                StatusId = statusId,
                Code = code,
                FirstName = firstName,
                UserId = Utility.UserId(),
                DomainId = Utility.DomainId()
            };

            DataTable data = employeeOffBoardProcessRepository.SearchAssetListExcelExport(employeeOffBoardBo);

            return ExportDataTableExcel("Asset_Clearance", data);
        }

        #endregion Asset Clearance

        #region Asset Report

        public ActionResult AssetReturnDetailList(string formName, int assetTypeId, bool? employeeStatus, bool? returnAssetStatus, string baseLocationId, int employeeId)
        {
            CompanyAssetsBo companyAssetsBo = new CompanyAssetsBo();
            EmployeeOffBoardProcessRepository employeeOffBoardProcessRepository = new EmployeeOffBoardProcessRepository();

            companyAssetsBo.EmployeeId = Utility.UserId();
            companyAssetsBo.AssetTypeId = assetTypeId;
            companyAssetsBo.EmployeeStatus = employeeStatus;
            companyAssetsBo.ReturnAssetStatus = returnAssetStatus;
            companyAssetsBo.BaseLocationId = string.IsNullOrEmpty(baseLocationId) ? 0 : Convert.ToInt32(baseLocationId);
            companyAssetsBo.DomainId = Utility.DomainId();
            companyAssetsBo.EmployeeId = employeeId;

            List<CompanyAssetsBo> data = employeeOffBoardProcessRepository.SearchAssetReturnDetails(companyAssetsBo);

            DataTable dt = new DataTable
            {
                TableName = "Employee_Asset_report"// Excel sheet name.
            };
            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Asset Type", typeof(string));
            dt.Columns.Add("Make", typeof(string));
            dt.Columns.Add("Serial Number", typeof(string));
            dt.Columns.Add("QTY", typeof(string));
            dt.Columns.Add("Given On", typeof(string));
            dt.Columns.Add("Returned On", typeof(string));
            dt.Columns.Add("Returned Remarks", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));
            dt.Columns.Add("Return Status", typeof(string));
            dt.Columns.Add("Employee Status", typeof(string));

            foreach (CompanyAssetsBo i in data)
            {
                var returnStatus = i.ReturnStatus ? "Returned" : "To be Returned";
                string status = i.IsActive ? "Inactive" : "Active";
                string returnedDate = i.ReturnedDate != DateTime.MinValue ? Convert.ToDateTime(i.ReturnedDate).ToString("dd-MMM-yyyy") : "";
                dt.Rows.Add(
                        i.Code,
                        i.EmployeeName,
                         i.Name,
                        i.Make,
                        i.SerialNumber,
                        i.Qty,
                        Convert.ToDateTime(i.HandoverOn).ToString("dd-MMM-yyyy"),
                        returnedDate,
                        i.ReturnedRemarks,
                        i.BusinessUnitName,
                        returnStatus,
                        status);
            }
            return ExportDataTableExcel("Employee_Asset_report", dt);
        }

        #endregion Asset Report

        #endregion OffBoard Process

        #region TDS

        public ActionResult ExcelExportTdsReport(int monthId, int yearId, string businessUnitIDs)
        {
            TdsRepository repository = new TdsRepository();
            List<Tdsbo> employeeTdsList = repository.SearchEmployeeTds(monthId, yearId, businessUnitIDs, "REPORT");

            string dateFormat = DateTime.Now.ToShortDateString();
            string reportName = "TDS_Report";

            StringBuilder sb = new StringBuilder();
            string fileName = reportName + "_" + dateFormat + ".xls";

            sb.Append("<table style='1px solid black; font-size:14px; font-family:Cambria;'border='1' >");
            sb.Append(GetExcelHeader(reportName));
            foreach (IGrouping<string, Tdsbo> n in employeeTdsList.GroupBy(a => a.BusinessUnit))
            {
                sb.Append("<tr><td style=background-color:#eee> " + n.Key);
                sb.Append("</td></tr>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Employee Code</b></td>");
                sb.Append("<td style='width:130px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Employee Name</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>PanNo</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Actual Gross</ b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Actual Months</ b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Projected Gross</ b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Projected Months</ b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#ffb74d;vertical-align: middle;'><b>Total Income from Salary</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Bonus</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>PLI</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>OtherIncome</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#90caf9;vertical-align: middle;'><b>Total Incomes</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>HRA</b></td>");
                if (n.Select(a => a.Year).FirstOrDefault() <= 2017)
                {
                    sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Conveyance</b></td>");
                    sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Medical</b></td>");
                }
                else
                {
                    sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Standard Deducation</b></td>");
                }
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>PT</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#ff9800;vertical-align: middle;'><b>Income from Salary Head</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Section 24 - Housing Loan Interest</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#00c0ef;vertical-align: middle;'><b>Gross Total Income</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>80 C / 80 CCC and 80CCD </b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>80D - Mediclaim Insurance</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>80U - Deduction for Disability</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>80E - Education Loan Interest</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>80G - Donation</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>80TTA - Interest on Savings bank account</b></td>");
                if (n.Select(a => a.Year).FirstOrDefault() >= 2018)
                {
                    sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>80TTB - Interest on Savings bank account</b></td>");
                }
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Others</b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>Total Exemption</ b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#90caf9;vertical-align: middle;'><b>Taxable Income</ b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#ffb74d;vertical-align: middle;'><b>Income Tax</ b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>87A</ b></td>");
                sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>SurCharge</b></td>");
                if (n.Select(a => a.Year).FirstOrDefault() <= 2017)
                {
                    sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>cess 3%</b></td>");
                }
                else
                {
                    sb.Append("<td style='width:90px;text-align: center;background-color:#b0d895;vertical-align: middle;'><b>cess 4%</b></td>");
                }
                sb.Append("<td style='width:90px;text-align: center;background-color:#00c0ef;vertical-align: middle;'><b>Total Tax</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Apr</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>May</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Jun</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Jul</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Aug</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Sep</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Oct</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Nov</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Dec</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Jan</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Feb</b></td>");
                sb.Append("<td style='width:70px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Mar</b></td>");
                sb.Append("<td style='width:100px;text-align: center;background-color:#78bbe2;vertical-align: middle;'><b>Remaining Amount</b></td>");
                sb.Append("</tr>");
                foreach (Tdsbo result in employeeTdsList.Where(ss => ss.BusinessUnit == n.Key))
                {
                    TdsHrabo tdsHrabo = new TdsHrabo
                    {
                        EmployeeId = result.EmployeeId,
                        FinancialYearId = yearId
                    };

                    List<TdsHrabo> employeeTdSfortheyear = repository.SearchemployeeTdsList(tdsHrabo, "REPORT");
                    StringBuilder sb1 = new StringBuilder();
                    decimal tdsAmount;
                    int? monthid = 4;
                    decimal sumofTdsAmount = 0;
                    int noOfMonth = 0;
                    for (int i = 0; i < employeeTdSfortheyear.Count; i++)
                    {
                        if (Convert.ToDecimal(employeeTdSfortheyear[i].Month.Id) > 0)
                        {
                            tdsAmount = Convert.ToDecimal(employeeTdSfortheyear[i].TdsRate);
                            monthid = employeeTdSfortheyear[i].Month.Id;
                            noOfMonth = noOfMonth + 1;
                        }
                        else
                        {
                            tdsAmount = 0;
                        }
                        sumofTdsAmount = sumofTdsAmount + tdsAmount;
                        if (tdsAmount == 0)
                        {
                            sb1.Append("<td style='text-align:center;'> - </td>");
                        }
                        else
                        {
                            sb1.Append("<td>" + tdsAmount.ToString("#,###,##0.00") + "</td>");
                        }
                    }
                    decimal? sum;
                    TdsCalculator tds = new TdsCalculator();
                    TdsCalculationBo model = tds.Calculatedetails(result.EmployeeId, yearId, Convert.ToInt32((monthid == 4 && noOfMonth == 0) ? 4 : monthid), Utility.DomainId(), string.Empty);
                    sb.Append("</tr>");
                    sb.Append("<td>" + result.EmployeeName.Split('-')[0] + "</td>");
                    sb.Append("<td>" + result.EmployeeName.Split('-')[1] + "</td>");
                    sb.Append("<td>" + model.PanNo + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.ActCTC)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + (noOfMonth) + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.ProCTC)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + (model.TDSExemptionMonths > 0 ? ((12 - model.TDSExemptionMonths) - noOfMonth) : (12 - noOfMonth)) + "</td>");
                    sb.Append("<td style='background-color:#ffb74d;'>" + @Math.Ceiling(@Convert.ToDecimal(model.Ctc)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.Bonus)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.PLI)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.OtherIncome)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td style='background-color:#90caf9;'>" + (@Math.Ceiling(Convert.ToDecimal(model.Ctc)) + @Math.Ceiling(Convert.ToDecimal(model.Bonus))
                                         + @Math.Ceiling(Convert.ToDecimal(model.PLI)) + @Math.Ceiling(Convert.ToDecimal(model.OtherIncome))).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.HRA)).ToString("#,###,##0.00") + "</td>");
                    if (model.FY <= 2017)
                    {
                        sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.ConveyanceAnnaum)).ToString("#,###,##0.00") + "</td>");
                        sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.Medicalperannum)).ToString("#,###,##0.00") + "</td>");
                    }
                    else
                    {
                        sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.StandardDeductionAnnaum)).ToString("#,###,##0.00") + "</td>");
                    }

                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.Pt)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td style='background-color:#ff9800;'>" + ((@Math.Ceiling(Convert.ToDecimal(model.Ctc)) + @Math.Ceiling(Convert.ToDecimal(model.Bonus))
                                         + @Math.Ceiling(Convert.ToDecimal(model.PLI)) + @Math.Ceiling(Convert.ToDecimal(model.OtherIncome)))
                                         - (@Math.Ceiling(@Convert.ToDecimal(model.HRA)) + @Math.Ceiling(@Convert.ToDecimal(model.ConveyanceAnnaum)) + @Math.Ceiling(@Convert.ToDecimal(model.StandardDeductionAnnaum))
                                         + @Math.Ceiling(@Convert.ToDecimal(model.Medicalperannum)) + @Math.Ceiling(@Convert.ToDecimal(model.Pt)))).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.tax20)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td style='background-color:#00c0ef;'>" + ((@Math.Ceiling(Convert.ToDecimal(model.Ctc)) + @Math.Ceiling(Convert.ToDecimal(model.Bonus))
                                        + @Math.Ceiling(Convert.ToDecimal(model.PLI)) + @Math.Ceiling(Convert.ToDecimal(model.OtherIncome)))
                                        - (@Math.Ceiling(@Convert.ToDecimal(model.HRA)) + @Math.Ceiling(@Convert.ToDecimal(model.ConveyanceAnnaum)) + @Math.Ceiling(@Convert.ToDecimal(model.Medicalperannum))
                                        + @Math.Ceiling(@Convert.ToDecimal(model.StandardDeductionAnnaum))
                                        + @Math.Ceiling(@Convert.ToDecimal(model.Pt))) - @Math.Ceiling(@Convert.ToDecimal(model.tax20))).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.tax80CC)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.tax80D)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.tax80U)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.tax80E)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.tax80G)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.tax80TTA)).ToString("#,###,##0.00") + "</td>");
                    if (model.FY >= 2018)
                    {
                        sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.tax80TTB)).ToString("#,###,##0.00") + "</td>");
                    }

                    sb.Append("<td>" + @Math.Ceiling(@Convert.ToDecimal(model.OtherTaxDetails)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("<td>" + Convert.ToDecimal((model.tax80CC + model.tax80D + model.tax80TTA + model.tax80TTB + model.tax80U + model.OtherTaxDetails + model.tax80E + model.tax20 + model.tax80G)).ToString("#,###,##0.00") + "</td>");
                    sum = model.Ctc - (model.Medicalperannum + model.HRA + model.ConveyanceAnnaum + model.StandardDeductionAnnaum) - model.Pt + (model.Bonus + model.PLI + model.OtherIncome);
                    sum = sum + 0;
                    sum = sum - (model.tax80CC + model.tax80D + model.tax80TTA + model.tax80TTB + model.tax80U + model.OtherTaxDetails + model.tax80E + model.tax20 + model.tax80G);
                    sb.Append("<td style='background-color:#90caf9;'>" + sum + "</td>");
                    sb.Append("<td style='background-color:#ffb74d;'> " + @Math.Ceiling(Convert.ToDecimal(model.TotalTax)).ToString("#,###,##0.00") + "</td>");
                    decimal? tax = sum;
                    decimal? rebate = 0;
                    if (sum <= 500020)
                    {
                        if (model.TotalTax <= 12500)
                        {
                            rebate = Math.Round(Convert.ToDecimal(model.TotalTax), 0);
                            sb.Append("<td> " + @Math.Ceiling(Convert.ToDecimal(rebate)).ToString("#,###,##0.00") + "</td>");
                        }
                        else
                        {
                            tax = model.TotalTax - 12500;
                            rebate = 12500;
                            sb.Append("<td> " + 12500.00 + "</td>");
                        }
                    }
                    else
                    {
                        sb.Append("<td>  0 </td>");
                    }
                    sb.Append("<td> " + @Math.Ceiling(Convert.ToDecimal(@Convert.ToDecimal(model.Surcharge))).ToString("#,###,##0.00") + "</td>");

                    if (tax != 0)
                    {
                        tax = model.TotalTax + model.Surcharge;
                        sum = tax * model.CessPrecentage / 100;
                        tax = tax + sum;
                    }
                    else
                    {
                        sum = 0;
                    }
                    sb.Append("<td>" + @Math.Ceiling(Convert.ToDecimal(@sum)).ToString("#,###,##0.00") + "</td>");
                    sum = tax - rebate;
                    sb.Append("<td style='background-color:#00c0ef'>" + @Math.Ceiling(Convert.ToDecimal(@sum)).ToString("#,###,##0.00") + "</td>");
                    sb.Append(sb1);
                    sb.Append("<td>" + @Math.Ceiling(Convert.ToDecimal(sum - sumofTdsAmount)).ToString("#,###,##0.00") + "</td>");
                    sb.Append("</tr>");
                }
                sb.Append("<tr></tr>");
            }

            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "data:application/vnd.ms-excel";
            byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
            return File(buffer, "data:application/vnd.ms-excel");
        }

        #endregion TDS

        #region Offer Letter RDLC

        public static string NumberToText(int number, bool useAnd, bool useArab)
        {
            if (number == 0)
            {
                return "Zero";
            }

            string and = useAnd ? "and " : ""; // deals with using 'and' separator

            if (number == -2147483648)
            {
                return "Minus Two Hundred " + and + "Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred " + and + "Forty Eight";
            }

            int[] num = new int[4];
            int first = 0;
            int ones, hundreds, tens;
            StringBuilder sb = new StringBuilder();

            if (number < 0)
            {
                sb.Append("Minus ");
                number = -number;
            }
            string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
            string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };
            num[0] = number % 1000; // units
            num[1] = number / 1000;
            num[2] = number / 100000;
            num[1] = num[1] - 100 * num[2]; // thousands
            num[3] = number / 10000000; // crores
            num[2] = num[2] - 100 * num[3]; // lakhs
            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }

            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0)
                {
                    continue;
                }

                ones = num[i] % 10; // ones
                tens = num[i] / 10;
                hundreds = num[i] / 100; // hundreds
                tens = tens - 10 * hundreds; // tens

                if (hundreds > 0)
                {
                    sb.Append(words0[hundreds] + "Hundred ");
                }

                if (ones > 0 || tens > 0)
                {
                    if (hundreds > 0 || i < first)
                    {
                        sb.Append(and);
                    }

                    if (tens == 0)
                    {
                        sb.Append(words0[ones]);
                    }
                    else if (tens == 1)
                    {
                        sb.Append(words1[ones]);
                    }
                    else
                    {
                        sb.Append(words2[tens - 2] + words0[ones]);
                    }
                }
                if (i != 0)
                {
                    sb.Append(words3[i - 1]);
                }
            }

            string temp = sb.ToString().TrimEnd();

            if (useArab && Math.Abs(number) >= 1000000000)
            {
                int index = temp.IndexOf("Hundred Crore", StringComparison.Ordinal);
                if (index > -1)
                {
                    return temp.Substring(0, index) + "Arab" + temp.Substring(index + 13);
                }

                index = temp.IndexOf("Hundred", StringComparison.Ordinal);
                return temp.Substring(0, index) + "Arab" + temp.Substring(index + 7);
            }
            return temp;
        }

        public FileContentResult OfferLetter(string referenceNo, int employeeId, string baseLocation, int reportingToId, decimal gross, string companyName)
        {
            LocalReport localReport = new LocalReport
            {
                ReportPath = Server.MapPath("~/Content/OfficeBAUOfferLetter.rdlc")
            };
            EmployeeMasterBo employeeMasterBo = new EmployeeMasterBo();
            //Jeeva comment for New payStructure
            //employeeMasterBO = employeeMasterRepository.EmployeeOfferLetter(empid, reportingToID);

            decimal? totalEarningsAmount = employeeMasterBo.PayStructureBo.Basic + employeeMasterBo.PayStructureBo.HRA + employeeMasterBo.PayStructureBo.MedicalAllowance + employeeMasterBo.PayStructureBo.Conveyance + employeeMasterBo.PayStructureBo.SplAllow + employeeMasterBo.PayStructureBo.EducationAllowance + employeeMasterBo.PayStructureBo.PaperMagazine;
            decimal? totalBenefitsAmount = employeeMasterBo.PayStructureBo.ERPF + employeeMasterBo.PayStructureBo.ERESI + employeeMasterBo.PayStructureBo.Gratuity + employeeMasterBo.PayStructureBo.Bonus + employeeMasterBo.PayStructureBo.Medicalclaim + employeeMasterBo.PayStructureBo.PLI + employeeMasterBo.PayStructureBo.MobileDataCard + employeeMasterBo.PayStructureBo.OtherPerks;
            decimal? totalCtc = totalEarningsAmount + totalBenefitsAmount;

            ReportParameter doj = new ReportParameter("DOJ", employeeMasterBo.DateOFJoin.ToString("dd MMM yyyy"));
            ReportParameter refno = new ReportParameter("RefNo", referenceNo);
            ReportParameter empname = new ReportParameter("EmployeeName", employeeMasterBo.FirstName);
            ReportParameter companyAddress = new ReportParameter("CompanyAddress", Convert.ToString(employeeMasterBo.FileName));
            ReportParameter companyFullName = new ReportParameter("CompanyFullName", Convert.ToString(companyName));
            ReportParameter prefix = new ReportParameter("Prefix", Convert.ToString(employeeMasterBo.LastName));
            ReportParameter designation = new ReportParameter("Designation", Convert.ToString(employeeMasterBo.Designation));
            ReportParameter band = new ReportParameter("Band", Convert.ToString(employeeMasterBo.BandName));
            ReportParameter grade = new ReportParameter("Level", Convert.ToString(employeeMasterBo.GradeName));
#pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            ReportParameter grossinno = new ReportParameter("Gross", gross == Convert.ToDecimal("0.00") ? "0.00" : gross.ToString("#,###.00"));
#pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            int afterdecimal = Convert.ToInt32(gross.ToString("0.00").Split('.')[1]);
            ReportParameter grossinword = new ReportParameter("GrossInWords", NumberToText((int)gross, true, false) + " Rupees And " + NumberToText(afterdecimal, true, false) + " Paisa Only.");
            ReportParameter worklocation = new ReportParameter("WorkLocation", Convert.ToString(baseLocation));
            ReportParameter head = new ReportParameter("HeadName", Convert.ToString(employeeMasterBo.ReportingToName) + ' ' + Convert.ToString(employeeMasterBo.ApproverName));
            ReportParameter headDesignation = new ReportParameter("HeadDesignation", Convert.ToString(employeeMasterBo.DepartmentName));
            ReportParameter basic = new ReportParameter("Basic", (employeeMasterBo.PayStructureBo.Basic == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.Basic == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.Basic).ToString("#,###.00"));
            ReportParameter hra = new ReportParameter("HRA", (employeeMasterBo.PayStructureBo.HRA == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.HRA == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.HRA).ToString("#,###.00"));
            ReportParameter medicalAllowance = new ReportParameter("MedicalAllowance", (employeeMasterBo.PayStructureBo.MedicalAllowance == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.MedicalAllowance == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.MedicalAllowance).ToString("#,###.00"));
            ReportParameter conveyance = new ReportParameter("Conveyance", (employeeMasterBo.PayStructureBo.Conveyance == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.Conveyance == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.Conveyance).ToString("#,###.00"));
            ReportParameter splAllow = new ReportParameter("SplAllow", (employeeMasterBo.PayStructureBo.SplAllow == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.SplAllow == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.SplAllow).ToString("#,###.00"));
            ReportParameter educationAllow = new ReportParameter("EducationAllow", (employeeMasterBo.PayStructureBo.EducationAllowance == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.EducationAllowance == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.EducationAllowance).ToString("#,###.00"));
            ReportParameter magazineAllow = new ReportParameter("MagazineAllow", (employeeMasterBo.PayStructureBo.PaperMagazine == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.PaperMagazine == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.PaperMagazine).ToString("#,###.00"));
            ReportParameter erpf = new ReportParameter("ERPF", (employeeMasterBo.PayStructureBo.ERPF == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.ERPF == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.ERPF).ToString("#,###.00"));
            ReportParameter eresi = new ReportParameter("ERESI", (employeeMasterBo.PayStructureBo.ERESI == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.ERESI == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.ERESI).ToString("#,###.00"));
            ReportParameter gratuity = new ReportParameter("Gratuity", (employeeMasterBo.PayStructureBo.Gratuity == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.Gratuity == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.Gratuity).ToString("#,###.00"));
            ReportParameter bonus = new ReportParameter("Bonus", (employeeMasterBo.PayStructureBo.Bonus == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.Bonus == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.Bonus).ToString("#,###.00"));
            ReportParameter medicalclaim = new ReportParameter("Medicalclaim", (employeeMasterBo.PayStructureBo.Medicalclaim == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.Medicalclaim == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.Medicalclaim).ToString("#,###.00"));
            ReportParameter pli = new ReportParameter("PLI", (employeeMasterBo.PayStructureBo.PLI == Convert.ToDecimal("0.00") || employeeMasterBo.PayStructureBo.PLI == null) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.PLI).ToString("#,###.00"));
            ReportParameter mobileDataCard = new ReportParameter("MobileDataCard", (Convert.ToDecimal(employeeMasterBo.PayStructureBo.MobileDataCard) == Convert.ToDecimal("0.00")) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.MobileDataCard).ToString("#,###.00"));
            ReportParameter otherPerks = new ReportParameter("OtherPerks", (Convert.ToDecimal(employeeMasterBo.PayStructureBo.OtherPerks) == Convert.ToDecimal("0.00")) ? "0.00" : Convert.ToDecimal(employeeMasterBo.PayStructureBo.OtherPerks).ToString("#,###.00"));
            ReportParameter totalEarnings = new ReportParameter("TotalEarnings", (Convert.ToDecimal(totalEarningsAmount) == Convert.ToDecimal("0.00")) ? "0.00" : Convert.ToDecimal(totalEarningsAmount).ToString("#,###.00"));
            ReportParameter totalBenefits = new ReportParameter("TotalBenefits", (Convert.ToDecimal(totalBenefitsAmount) == Convert.ToDecimal("0.00")) ? "0.00" : Convert.ToDecimal(totalBenefitsAmount).ToString("#,###.00"));
            ReportParameter ctc = new ReportParameter("CTC", (Convert.ToDecimal(totalCtc) == Convert.ToDecimal("0.00")) ? "0.00" : Convert.ToDecimal(totalCtc).ToString("#,###.00"));

            //localReport.SetParameters(new ReportParameter[] { DOJ, refno, empname, companyAddress, companyFullName, prefix, designation, band, grade, grossinno, grossinword, worklocation, Head, HeadDesignation,
            //    Basic, HRA});

            localReport.SetParameters(new ReportParameter[] { doj, refno, empname, companyAddress, companyFullName, prefix, designation, band, grade, grossinno, grossinword, worklocation, head, headDesignation,
                        basic, hra, medicalAllowance, conveyance, splAllow,educationAllow,magazineAllow, erpf, eresi, gratuity, bonus, medicalclaim, pli, mobileDataCard, otherPerks, totalEarnings, totalBenefits, ctc });
            string reportType = "pdf";
            string deviceInfo = "<DeviceInfo>" +
                           "  <OutputFormat>jpeg</OutputFormat>" +
                           "  <PageWidth>8.5in</PageWidth>" +
                           "  <PageHeight>11in</PageHeight>" +
                           "  <MarginTop>0.5in</MarginTop>" +
                           "  <MarginLeft>1in</MarginLeft>" +
                           "  <MarginRight>1in</MarginRight>" +
                           "  <MarginBottom>0.5in</MarginBottom>" +
                           "</DeviceInfo>";
            byte[] renderedBytes;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string[] streams;
            Warning[] warnings;
            renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension,
                out streams, out warnings);
            return File(renderedBytes, mimeType);
        }

        #endregion Offer Letter RDLC

        #region Attendance

        public ActionResult EmployeeAttendanceExcelExport(string formName, int month, int year)
        {
            StringBuilder sb = new StringBuilder();

            string dateFormat = DateTime.Now.ToString("dd-MMM-yyyy");
            string reportName = formName;

            string fileName = reportName + "_" + dateFormat + ".xls";

            AttendanceBo attendanceBo = new AttendanceBo();
            AttendanceRepository attendanceRepository = new AttendanceRepository();
            attendanceBo.MonthId = month;
            attendanceBo.YearId = year;
            attendanceBo.EmployeeId = Utility.UserId();

            DataTable data = attendanceRepository.SearchEmployeeAttendance(attendanceBo);

            sb.Append("<table style='1px solid black; font-size:14px; font-family:Cambria;' border='1' >");
            sb.Append(GetExcelHeader(reportName));
            sb.Append("<tr>");

            foreach (DataColumn col in data.Columns)
            {
                if (Convert.ToString(col.ColumnName).ToLower() == "days" || Convert.ToString(col.ColumnName).ToLower() == "row1")
                {
                    if (Convert.ToString(col.ColumnName).ToLower() == "days")
                    {
                        sb.Append("<td style='width:140px;background-color:#b0d895;' border='1'><b>" + col.ColumnName + "</b></td>");
                    }
                }
                else
                {
                    sb.Append("<td style='width:40px;background-color:#b0d895;text-align:center;' border='1'><b>" + col.ColumnName.Split('/')[0] + "</b></td>");
                }
            }
            sb.Append("</tr>");

            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    sb.Append("<tr style='height:20px;'>");
                    foreach (DataColumn col in data.Columns)
                    {
                        if (Convert.ToString(col.ColumnName).ToLower() == "days" || Convert.ToString(col.ColumnName).ToLower() == "row1")
                        {
                            if (Convert.ToString(col.ColumnName).ToLower() == "days")
                            {
                                sb.Append("<td>" + Convert.ToString(row[col.ColumnName]) + "</td>");
                            }
                        }
                        else
                        {
                            sb.Append("<td style='text-align:center'>" + Convert.ToString(row[col.ColumnName]) + "</td>");
                        }
                    }
                    sb.Append("</tr>");
                }
            }

            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "data:application/vnd.ms-excel";
            byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
            return File(buffer, "data:application/vnd.ms-excel");
        }

        public ActionResult SearchMonthlyAttendanceExcelExport(string formName, int monthId, int yearId, string businessUnitId)
        {
            AttendanceBo attendanceBo = new AttendanceBo();
            AttendanceRepository attendanceRepository = new AttendanceRepository();
            attendanceBo.MonthId = monthId;
            attendanceBo.YearId = yearId;
            if (!string.IsNullOrWhiteSpace(businessUnitId))
            {
                attendanceBo.BusinessUnitId = businessUnitId.Trim(',');
            }

            attendanceBo.EmployeeId = Utility.UserId();

            DataTable data = attendanceRepository.SearchMonthlyAttendance(attendanceBo);
            DataTable dt = new DataTable();
            foreach (DataColumn col in data.Columns)
            {
                string columnName = Convert.ToString(col.ColumnName.ToLower());
                switch (columnName.Split('/')[0])
                {
                    case "employee_name":
                        dt.Columns.Add(col.ColumnName, typeof(string));
                        break;

                    case "total_hours":
                    case "approved leave":
                        dt.Columns.Add(col.ColumnName, typeof(string));
                        break;

                    case "wo sat":
                    case "wo sun":
                        dt.Columns.Add(col.ColumnName, typeof(string));
                        break;

                    case "id":
                    case "a":
                    case "p":
                    case "hd":
                        if (columnName != "id")
                        {
                            dt.Columns.Add(col.ColumnName, typeof(string));
                        }
                        break;

                    default:
                        if (!string.IsNullOrWhiteSpace(col.ColumnName.Split('/')[1]))
                            dt.Columns.Add(col.ColumnName, typeof(string));
                        else
                            dt.Columns.Add(col.ColumnName.Split('/')[0], typeof(string));
                        break;
                }
            }

            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    List<string> newRowList = new List<string>();
                    foreach (DataColumn col in data.Columns)
                    {
                        switch (Convert.ToString(col.ColumnName).ToLower().Split('/')[0])
                        {
                            case "employee_name":
                            case "contact_no":
                                newRowList.Add(row[col.ColumnName].ToString());
                                break;

                            case "wo sat":
                            case "wo sun":
                                newRowList.Add(row[col.ColumnName].ToString().Split('/')[0]);
                                break;

                            case "id":
                            case "a":
                            case "p":
                            case "hd":
                                if (Convert.ToString(col.ColumnName).ToLower() != "id")
                                {
                                    newRowList.Add(row[col.ColumnName].ToString());
                                }
                                break;

                            default:
                                newRowList.Add(row[col.ColumnName].ToString().Split('/')[0]);
                                break;
                        }
                    }
                    dt.Rows.Add(newRowList.ToArray());
                }
            }

            return ExportDataTableExcel(formName, dt);
        }

        public ActionResult LogAttendanceExcelExport(string formName, string date)
        {
            AttendanceBo attendanceBo = new AttendanceBo();
            AttendanceRepository attendanceRepository = new AttendanceRepository();

            attendanceBo.AttendanceDate = Convert.ToDateTime(date);

            List<AttendanceBo> data = attendanceRepository.SearchDailyAttendance(attendanceBo);

            DataTable dt = new DataTable
            {
                TableName = "Log_Attendance"// Excel sheet name.
            };
            dt.Columns.Add("Employee_Code", typeof(string));
            dt.Columns.Add("Employee_Name", typeof(string));
            dt.Columns.Add("Punches", typeof(string));
            dt.Columns.Add("Duration (Hrs)", typeof(string));
            dt.Columns.Add("Date", typeof(string));

            foreach (AttendanceBo i in data)
            {
                dt.Rows.Add(
                        i.EmployeeCode,
                        i.EmployeeName,
                        i.Punches,
                        i.PunchTime,
                        Convert.ToDateTime(i.AttendanceDate).ToString("dd-MMM-yyyy"));
            }
            return ExportDataTableExcel("Log_Attendance", dt);
        }

        #endregion Attendance

        #region Missed Punches

        #region Request

        public ActionResult MissedPunchesRequestExcel(string formName, int statusId, int employeeId)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                StatusId = statusId
            };
            if (employeeId == 0)
            {
                missedPunchesBo.EmployeeId = Utility.UserId();
            }
            else
            {
                missedPunchesBo.EmployeeId = employeeId;
            }

            List<MissedPunchesBo> data = leaveManagementRepository.SearchMissedPunchesRequest(missedPunchesBo);

            DataTable dt = new DataTable
            {
                TableName = "Missed_Punches-Request"// Excel sheet name.
            };
            dt.Columns.Add("Requested On", typeof(string));
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Punch Type", typeof(string));
            dt.Columns.Add("Time", typeof(string));
            dt.Columns.Add("Approved By", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (MissedPunchesBo i in data)
            {
                string requestedOn = i.CreatedOn != DateTime.MinValue ? Convert.ToDateTime(i.RequestedDate).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                        requestedOn,
                        Convert.ToDateTime(i.PunchDate).ToString("dd-MMM-yyyy"),
                        i.EmployeePunchType.Name,
                        (Convert.ToDateTime(i.PunchDate).Date + i.PunchTime).ToString("hh:mm tt"),
                        i.HrApproverName,
                        i.Status.Name);
            }
            return ExportDataTableExcel("Missed_Punches-Request", dt);
        }

        #endregion Request

        #region Approval

        public ActionResult MissedPunchesApprovalExcel(string formName, int statusId)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                EmployeeId = Utility.UserId(),
                StatusId = statusId,
                DomainId = Utility.DomainId(),
                HrApproverId = Utility.UserId()
            };

            List<MissedPunchesBo> data = leaveManagementRepository.SearchPunchesApproval(missedPunchesBo);

            DataTable dt = new DataTable
            {
                TableName = "Missed_Punches-Approval"// Excel sheet name.
            };
            dt.Columns.Add("Requester", typeof(string));
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Punch Type", typeof(string));
            dt.Columns.Add("Time", typeof(string));
            dt.Columns.Add("Requested On", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (MissedPunchesBo i in data)
            {
                string requestedOn = i.CreatedOn != DateTime.MinValue ? Convert.ToDateTime(i.RequestedDate).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                        i.Employee.Name,
                        Convert.ToDateTime(i.PunchDate).ToString("dd-MMM-yyyy"),
                        i.EmployeePunchType.Name,
                        (Convert.ToDateTime(i.PunchDate).Date + i.PunchTime).ToString("hh:mm tt"),
                        requestedOn,
                        i.Status.Name);
            }
            return ExportDataTableExcel("Missed_Punches-Approval", dt);
        }

        #endregion Approval

        #region Missed Check-In/Check-Out

        public ActionResult MissedCheckInCheckOutReportExcel(int monthId, int yearId, string businessUnitIDs, int statusId, int yearName)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                Year = yearId,
                YearName = yearName,
                MonthId = monthId,
                BusinessUnitIDs = businessUnitIDs,
                StatusId = statusId
            };

            List<MissedPunchesBo> data = leaveManagementRepository.MissedCheckInCheckOutReportList(missedPunchesBo);

            DataTable dt = new DataTable
            {
                TableName = "Missed_Check-In/Check-Out_report"// Excel sheet name.
            };
            dt.Columns.Add("Emp_Code", typeof(string));
            dt.Columns.Add("Employee_Name", typeof(string));
            dt.Columns.Add("Check-In", typeof(string));
            dt.Columns.Add("Check-Out", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (MissedPunchesBo i in data)
            {
                string totalCheckIn = i.TotalCheckIn != 0 ? i.TotalCheckIn.ToString() : null;
                string totalCheckOut = i.TotalCheckOut != 0 ? i.TotalCheckOut.ToString() : null;
                dt.Rows.Add(
                        i.EmployeeCode,
                        i.EmployeeName,
                        totalCheckIn,
                        totalCheckOut,
                        i.StatusCode);
            }
            return ExportDataTableExcel("Missed_Check-In_or_Out_rpt", dt);
        }

        #endregion Missed Check-In/Check-Out

        #endregion Missed Punches

        #region RDLC Pay Stub

        public FileContentResult PayStub(string reportName, int? employeeId, int monthId, int yearId, bool isProfile)
        {
            decimal DefaultValue = 0;

            LocalReport localReport = new LocalReport
            {
                DisplayName = reportName,
                EnableExternalImages = true
            };

            reportName = "Salary Slip for the Month of " + reportName;

            EmployeeProfileRepository employeeProfileRepository = new EmployeeProfileRepository();

            if (employeeId == null || employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            PayStructureBO payStructureBo = employeeProfileRepository.GetPayroll(employeeId, monthId, yearId, isProfile);

            switch (payStructureBo.Region.ToLower())
            {
                case "chennai":
                    localReport.ReportPath = Server.MapPath("~/Content/OfficeBAUPaySlip_Chennai.rdlc");
                    break;

                case "tuticorin":
                    localReport.ReportPath = Server.MapPath("~/Content/OfficeBAUPaySlip_Tuticorin.rdlc");

                    payStructureBo.Designation = payStructureBo.Designation.Split('-')[0];
                    break;

                case "panipat":
                    localReport.ReportPath = Server.MapPath("~/Content/OfficeBAUPaySlip_Panipat.rdlc");
                    break;

                case "mumbai":
                case "ahmedabad":
                    localReport.ReportPath = Server.MapPath("~/Content/OfficeBAUPaySlip_Mumbai.rdlc");
                    break;

                default:
                    localReport.ReportPath = Server.MapPath("~/Content/OfficeBAUPaySlip_Chennai.rdlc");
                    break;
            }
            string type = string.Empty;
            if (!string.IsNullOrWhiteSpace(payStructureBo.Logo) && new Guid(payStructureBo.Logo) != new Guid())
            {
                FileUpload fileUpload = UploadUtility.DownloadFile(payStructureBo.Logo);
                payStructureBo.Logo = new Uri(fileUpload.FileName).AbsoluteUri;
                type = fileUpload.FileType;
            }

            // Earnings
            decimal eepfTotal = (payStructureBo.EEPF == DefaultValue || payStructureBo.EEPF == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.EEPF);
            decimal eeesiTotal = (payStructureBo.EEESI == DefaultValue || payStructureBo.EEESI == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.EEESI);
            decimal tdsTotal = (payStructureBo.TDS == DefaultValue || payStructureBo.TDS == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.TDS);
            decimal ptTotal = (payStructureBo.PT == DefaultValue || payStructureBo.PT == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.PT);
            decimal loansTotal = (payStructureBo.Loans == DefaultValue || payStructureBo.Loans == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.Loans);
#pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            decimal deductionTotal = (payStructureBo.Deduction == DefaultValue) ? DefaultValue : Convert.ToDecimal(payStructureBo.Deduction);
#pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            decimal grossTotal = (payStructureBo.Gross == DefaultValue || payStructureBo.Gross == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.Gross);
            decimal monsoonAllow = (payStructureBo.MonsoonAllow == DefaultValue || payStructureBo.MonsoonAllow == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.MonsoonAllow);

            decimal mobileDataCardTotal = (payStructureBo.MobileDataCard == DefaultValue || payStructureBo.MobileDataCard == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.MobileDataCard);
#pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            decimal otherEarningsTotal = payStructureBo.OtherEarnings == DefaultValue ? DefaultValue : Convert.ToDecimal(payStructureBo.OtherEarnings);
#pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            decimal otherPerksTotal = (payStructureBo.OtherPerks == DefaultValue || payStructureBo.OtherPerks == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.OtherPerks);
            decimal pliTotal = (payStructureBo.PLI == DefaultValue || payStructureBo.PLI == null) ? DefaultValue : Convert.ToDecimal(payStructureBo.PLI);

            decimal totalOtherEarnings = (mobileDataCardTotal + otherEarningsTotal + otherPerksTotal + pliTotal);

            grossTotal = (grossTotal + totalOtherEarnings + monsoonAllow);

            ReportParameter employeeName = new ReportParameter("EmployeeName", payStructureBo.EmployeeName.ToString());
            ReportParameter employeeCode = new ReportParameter("EmployeeCode", payStructureBo.EmployeeCode.ToString());
            ReportParameter designation = new ReportParameter("Designation", payStructureBo.Designation.ToString());
            ReportParameter pfNo = new ReportParameter("PFNo", payStructureBo.PFNo.ToString());
            ReportParameter esiNo = new ReportParameter("ESINo", payStructureBo.ESINo.ToString());
            ReportParameter uanNo = new ReportParameter("UANNo", payStructureBo.UANNo.ToString());
            ReportParameter aadharNo = new ReportParameter("AadharNo", payStructureBo.AadharNo.ToString());
            ReportParameter panNo = new ReportParameter("PANNo", payStructureBo.PANNo.ToString());
            ReportParameter bankAccountNo = new ReportParameter("BankAccountNo", payStructureBo.BankAccountNo.ToString());

            ReportParameter gross = new ReportParameter("Gross", grossTotal.ToString("#,###.00"));
            ReportParameter basic = new ReportParameter("Basic", (payStructureBo.Basic == Convert.ToDecimal("0.00") || payStructureBo.Basic == null) ? "0.00" : Convert.ToDecimal(payStructureBo.Basic).ToString("#,###.00"));
            ReportParameter hra = new ReportParameter("HRA", (payStructureBo.HRA == Convert.ToDecimal("0.00") || payStructureBo.HRA == null) ? "0.00" : Convert.ToDecimal(payStructureBo.HRA).ToString("#,###.00"));
            ReportParameter MedicalAllowance = new ReportParameter("MedicalAllowance", (payStructureBo.MedicalAllowance == Convert.ToDecimal("0.00") || payStructureBo.MedicalAllowance == null) ? "0.00" : Convert.ToDecimal(payStructureBo.MedicalAllowance).ToString("#,###.00"));
            ReportParameter SplAllow = new ReportParameter("SplAllow", (payStructureBo.SplAllow == Convert.ToDecimal("0.00") || payStructureBo.SplAllow == null) ? "0.00" : Convert.ToDecimal(payStructureBo.SplAllow).ToString("#,###.00"));
            ReportParameter Conveyance = new ReportParameter("Conveyance", (payStructureBo.Conveyance == Convert.ToDecimal("0.00") || payStructureBo.Conveyance == null) ? "0.00" : Convert.ToDecimal(payStructureBo.Conveyance).ToString("#,###.00"));
            ReportParameter EEPF = new ReportParameter("EEPF", eepfTotal.ToString("#,###.00"));
            ReportParameter EEESI = new ReportParameter("EEESI", eeesiTotal.ToString("#,###.00"));
            ReportParameter PT = new ReportParameter("PT", ptTotal.ToString("#,###.00"));
            ReportParameter TDS = new ReportParameter("TDS", tdsTotal.ToString("#,###.00"));
            ReportParameter ERPF = new ReportParameter("ERPF", (payStructureBo.ERPF == Convert.ToDecimal("0.00") || payStructureBo.ERPF == null) ? "0.00" : Convert.ToDecimal(payStructureBo.ERPF).ToString("#,###.00"));
            ReportParameter ERESI = new ReportParameter("ERESI", (payStructureBo.ERESI == Convert.ToDecimal("0.00") || payStructureBo.ERESI == null) ? "0.00" : Convert.ToDecimal(payStructureBo.ERESI).ToString("#,###.00"));
            ReportParameter Bonus = new ReportParameter("Bonus", (payStructureBo.Bonus == Convert.ToDecimal("0.00") || payStructureBo.Bonus == null) ? "0.00" : Convert.ToDecimal(payStructureBo.Bonus).ToString("#,###.00"));
            ReportParameter Gratuity = new ReportParameter("Gratuity", (payStructureBo.Gratuity == Convert.ToDecimal("0.00") || payStructureBo.Gratuity == null) ? "0.00" : Convert.ToDecimal(payStructureBo.Gratuity).ToString("#,###.00"));
            ReportParameter PLI = new ReportParameter("PLI", pliTotal.ToString("#,###.00"));
            ReportParameter Medicalclaim = new ReportParameter("Medicalclaim", (payStructureBo.Medicalclaim == Convert.ToDecimal("0.00") || payStructureBo.Medicalclaim == null) ? "0.00" : Convert.ToDecimal(payStructureBo.Medicalclaim).ToString("#,###.00"));
            ReportParameter MobileDataCard = new ReportParameter("MobileDataCard", mobileDataCardTotal.ToString("#,###.00"));
            ReportParameter OtherEarnings = new ReportParameter("OtherEarnings", otherEarningsTotal.ToString("#,###.00"));
            ReportParameter OtherPerks = new ReportParameter("OtherPerks", otherPerksTotal.ToString("#,###.00"));
            ReportParameter Deduction = new ReportParameter("Deduction", deductionTotal.ToString("#,###.00"));
            ReportParameter Loans = new ReportParameter("Loans", loansTotal.ToString("#,###.00"));

            ReportParameter WorkingDays = new ReportParameter("WorkingDays", payStructureBo.WorkingDays.ToString());
            ReportParameter PresentDays = new ReportParameter("PresentDays", payStructureBo.PresentDays.ToString());
            ReportParameter CompanyName = new ReportParameter("CompanyName", payStructureBo.CompanyName.ToString());
            ReportParameter BusinessUnitAddress = new ReportParameter("BusinessUnitAddress", payStructureBo.BusinessUnitAddress.ToString());
            ReportParameter DOJ = new ReportParameter("DOJ", payStructureBo.DOJ.ToString("dd MMM yyyy"));

            ReportParameter FixedDA = new ReportParameter("FixedDA", (payStructureBo.FixedDA == Convert.ToDecimal("0.00") || payStructureBo.FixedDA == null) ? "0.00" : Convert.ToDecimal(payStructureBo.FixedDA).ToString("#,###.00"));
            ReportParameter EducationAllowance = new ReportParameter("EducationAllowance", (payStructureBo.EducationAllowance == Convert.ToDecimal("0.00") || payStructureBo.EducationAllowance == null) ? "0.00" : Convert.ToDecimal(payStructureBo.EducationAllowance).ToString("#,###.00"));
            ReportParameter MagazineAllowance = new ReportParameter("MagazineAllowance", (payStructureBo.PaperMagazine == Convert.ToDecimal("0.00") || payStructureBo.PaperMagazine == null) ? "0.00" : Convert.ToDecimal(payStructureBo.PaperMagazine).ToString("#,###.00"));
            ReportParameter MonsoonAllowance = new ReportParameter("MonsoonAllowance", (payStructureBo.MonsoonAllow == Convert.ToDecimal("0.00") || payStructureBo.MonsoonAllow == null) ? "0.00" : Convert.ToDecimal(payStructureBo.MonsoonAllow).ToString("#,###.00"));
            ReportParameter IncomeTax = new ReportParameter("IncomeTax", (payStructureBo.IncomeTax == Convert.ToDecimal("0.00") || payStructureBo.IncomeTax == null) ? "0.00" : Convert.ToDecimal(payStructureBo.IncomeTax).ToString("#,###.00"));
            ReportParameter InstalmentOnLoan = new ReportParameter("InstalmentOnLoan", (payStructureBo.InstalmentOnLoan == Convert.ToDecimal("0.00") || payStructureBo.InstalmentOnLoan == null) ? "0.00" : Convert.ToDecimal(payStructureBo.InstalmentOnLoan).ToString("#,###.00"));

            ReportParameter Logo = new ReportParameter("Logo", (payStructureBo.Logo == null) ? "" : payStructureBo.Logo.ToString());
            ReportParameter ImageType = new ReportParameter("ImageType", type);
            ReportParameter modeofPay = new ReportParameter("ModeofPay", ((payStructureBo.BankAccountNo.ToString() == "-") ? "Cash / Cheque" : "Bank"));
            ReportParameter payStubMonthYear = new ReportParameter("payStubMonthYear", reportName.ToString());

            decimal netTotalAmount = (grossTotal) - (eepfTotal + eeesiTotal + tdsTotal + ptTotal + loansTotal + deductionTotal);
            decimal totalDeductionsAmount = eepfTotal + eeesiTotal + tdsTotal + ptTotal + loansTotal + deductionTotal;
#pragma warning disable CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            totalDeductionsAmount = (totalDeductionsAmount == null) ? 0 : totalDeductionsAmount;
#pragma warning restore CS0472 // The result of the expression is always the same since a value of this type is never equal to 'null'
            ReportParameter netTotal = new ReportParameter("NetTotal", netTotalAmount.ToString("#,###.00"));
            int afterDecimal = Convert.ToInt32(netTotalAmount.ToString("0.00").Split('.')[1]);
            ReportParameter netTotalInWords = new ReportParameter("NetTotalInWords", NumberToText((int)netTotalAmount, true, false) + " Rupees And " + NumberToText(afterDecimal, true, false) + " Paisa Only");
            ReportParameter totalDeductions = new ReportParameter("TotalDeductions", (Convert.ToDecimal(totalDeductionsAmount) == Convert.ToDecimal("0.00")) ? "0.00" : Convert.ToDecimal(totalDeductionsAmount).ToString("#,###.00"));

            ReportParameter OverTime = new ReportParameter("OverTime", (payStructureBo.OverTime == Convert.ToDecimal("0.00") || payStructureBo.OverTime == null) ? "0.00" : Convert.ToDecimal(payStructureBo.OverTime).ToString("#,###.00"));
            ReportParameter educationAllow = new ReportParameter("EducationAllow", (payStructureBo.EducationAllow == Convert.ToDecimal("0.00") || payStructureBo.EducationAllow == null) ? "0.00" : Convert.ToDecimal(payStructureBo.EducationAllow).ToString("#,###.00"));

            switch (payStructureBo.Region.ToLower())
            {
                case "chennai":
                    localReport.SetParameters(new ReportParameter[] { employeeName,employeeCode,designation, pfNo, esiNo, uanNo, panNo, bankAccountNo, gross, basic, hra, MedicalAllowance, SplAllow, Conveyance,
                                            EEPF, EEESI, PT, TDS, ERPF, ERESI, Bonus, Gratuity, PLI, Medicalclaim, MobileDataCard, OtherEarnings, OtherPerks, Deduction, Loans,
                                            WorkingDays, PresentDays, CompanyName, BusinessUnitAddress, DOJ, FixedDA, EducationAllowance, IncomeTax, InstalmentOnLoan, Logo, netTotal, netTotalInWords, modeofPay, totalDeductions, payStubMonthYear,aadharNo,ImageType});
                    break;

                case "tuticorin":

                    localReport.SetParameters(new ReportParameter[] { employeeName,employeeCode,designation, pfNo, esiNo, uanNo, panNo, bankAccountNo, gross, basic, hra, MedicalAllowance, SplAllow, Conveyance,
                                            EEPF, EEESI, PT, TDS, ERPF, ERESI, Bonus, Gratuity, PLI, Medicalclaim, MobileDataCard, OtherEarnings, OtherPerks, Deduction, Loans,
                                            WorkingDays, PresentDays, CompanyName, BusinessUnitAddress, DOJ, FixedDA, EducationAllowance, IncomeTax, InstalmentOnLoan, Logo, netTotal, netTotalInWords, modeofPay, totalDeductions, payStubMonthYear,aadharNo,
                                            OverTime, educationAllow});
                    break;

                case "panipat":
                    localReport.SetParameters(new ReportParameter[] { employeeName,employeeCode,designation, pfNo, esiNo, uanNo, panNo, bankAccountNo, gross, basic, hra, MedicalAllowance, SplAllow, Conveyance,
                                            EEPF, EEESI, PT, TDS, ERPF, ERESI, Bonus, Gratuity, PLI, Medicalclaim, MobileDataCard, OtherEarnings, OtherPerks, Deduction, Loans,
                                            WorkingDays, PresentDays, CompanyName, BusinessUnitAddress, DOJ, FixedDA, EducationAllowance, IncomeTax, InstalmentOnLoan, Logo, netTotal, netTotalInWords, modeofPay, totalDeductions, payStubMonthYear,aadharNo,
                                            educationAllow});
                    break;

                case "mumbai":
                case "ahmedabad":
                    localReport.SetParameters(new ReportParameter[] { employeeName,employeeCode,designation, pfNo, esiNo, uanNo, panNo, bankAccountNo, gross, basic, hra, MedicalAllowance, SplAllow, Conveyance,
                                            EEPF, EEESI, PT, TDS, ERPF, ERESI, Bonus, Gratuity, PLI, Medicalclaim, MobileDataCard, OtherEarnings, OtherPerks, Deduction, Loans,
                                            WorkingDays, PresentDays, CompanyName, BusinessUnitAddress, DOJ, FixedDA, EducationAllowance, IncomeTax, InstalmentOnLoan, Logo, netTotal, netTotalInWords, modeofPay, totalDeductions, payStubMonthYear,aadharNo,
                                            educationAllow,MagazineAllowance,MonsoonAllowance});
                    break;

                default:
                    localReport.SetParameters(new ReportParameter[] { employeeName,employeeCode,designation, pfNo, esiNo, uanNo, panNo, bankAccountNo, gross, basic, hra, MedicalAllowance, SplAllow, Conveyance,
                                            EEPF, EEESI, PT, TDS, ERPF, ERESI, Bonus, Gratuity, PLI, Medicalclaim, MobileDataCard, OtherEarnings, OtherPerks, Deduction, Loans,
                                            WorkingDays, PresentDays, CompanyName, BusinessUnitAddress, DOJ, FixedDA, EducationAllowance, IncomeTax, InstalmentOnLoan, Logo, netTotal, netTotalInWords, modeofPay, totalDeductions, payStubMonthYear,aadharNo,ImageType});
                    break;
            }

            localReport.Refresh();

            //localReport.SetParameters(new ReportParameter[] { EmployeeName });

            string reportType = "pdf";
            string deviceInfo = "<DeviceInfo>" +
                           "  <OutputFormat>jpeg</OutputFormat>" +
                           "  <PageWidth>8.5in</PageWidth>" +
                           "  <PageHeight>11in</PageHeight>" +
                           "  <MarginTop>0.5in</MarginTop>" +
                           "  <MarginLeft>1in</MarginLeft>" +
                           "  <MarginRight>1in</MarginRight>" +
                           "  <MarginBottom>0.5in</MarginBottom>" +
                           "</DeviceInfo>";
            byte[] renderedBytes;
            string mimeType;
            string encoding;
            string fileNameExtension;
            string[] streams;
            Warning[] warnings;
            renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            return File(renderedBytes, mimeType);
            //}
            //else
            //{
            //    return ("Payroll is not processed for the selected Month !", JsonRequestBehavior.AllowGet);
            //}
        }

        #endregion RDLC Pay Stub

        #region Mail Configuration Excel

        public ActionResult MailConfigurationExcel(string formName, int businessId)
        {
            string dateFormat = DateTime.Now.ToString("dd-MMM-yyyy");
            string reportName = "Mail_Configuration";

            StringBuilder sb = new StringBuilder();
            string fileName = reportName + "_" + dateFormat + ".xls";

            MailerBo mailerBo = new MailerBo();
            MailerRepository mailerRepository = new MailerRepository();
            mailerBo.BusinessUnitId = businessId;

            List<MailerBo> data = mailerRepository.SearchCcReceipts(mailerBo);

            sb.Append("<table style='1px solid black; font-size:14px; font-family:Cambria;' border='1' >");
            sb.Append(GetExcelHeader(reportName));
            sb.Append("<tr>");
            sb.Append("<td style='width:180px;background-color:#b0d895;'><b>Events</b></td>");
            sb.Append("<td style='width:180px;background-color:#b0d895;'><b>Business Unit</b></td>");
            sb.Append("<td style='width:180px;background-color:#b0d895;'><b>CC Recipients</b></td>");
            sb.Append("</tr>");

            if (data != null && data.Any())
            {
                foreach (MailerBo result in data)
                {
                    sb.Append("<tr style='height:20px;'>");
                    sb.Append("<td>" + result.Event + "</td>");
                    sb.Append("<td>" + result.BusinessUnit + "</td>");
                    sb.Append("<td>" + result.CCrecpts + "</td>");
                }
            }

            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "data:application/vnd.ms-excel";
            byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
            return File(buffer, "data:application/vnd.ms-excel");
        }

        #endregion Mail Configuration Excel

        #region TotalClockedHours

        public ActionResult TotalClockedHoursExcel(int monthId, int yearId, string businessUnitIDs)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                Year = yearId,
                MonthId = monthId,
                BusinessUnitIDs = businessUnitIDs
            };

            List<MissedPunchesBo> data = leaveManagementRepository.TotalHoursClockedReportList(missedPunchesBo);

            DataTable dt = new DataTable
            {
                TableName = "Total_Hours_Clocked_report"// Excel sheet name.
            };
            dt.Columns.Add("Emp_Code", typeof(string));
            dt.Columns.Add("Employee_Name", typeof(string));
            dt.Columns.Add("Total Minutes", typeof(string));
            dt.Columns.Add("Total Duration", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));

            int slNo = 1;
            foreach (MissedPunchesBo i in data)
            {
                // ReSharper disable once UseStringInterpolation
                string totalDuration = string.Format("{0:[h]:mm}", i.TotalDuration);
                dt.Rows.Add(
                        i.EmployeeCode,
                        i.EmployeeName,
                        (i.TotalMinutes == 0 ? "" : i.TotalMinutes.ToString()),
                        totalDuration == "0:00" ? null : totalDuration,
                        i.BaseLocation);
                slNo = slNo + 1;
            }
            return ExportDataTableExcel("Total_Hours_Clocked_report", dt);
            //sb.Append("<td style='mso-number-format:\\@;text-align:right'>" + TotalDuration + "</td>");
        }

        #endregion TotalClockedHours

        #region TDS Component

        public void ExportTdsComponent(string formName, string downloadFileName, int year, int baseLocationId)
        {
            string fileName = downloadFileName + ".xlsx";

            TdsRepository tdsRepository = new TdsRepository();
            TdsComponentBo tdsComponentBo = new TdsComponentBo
            {
                FinancialYearId = year,
                BusinessUnitId = baseLocationId
            };

            DataTable data = tdsRepository.ExportTdsComponent(tdsComponentBo);

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(data, formName);

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                using (MemoryStream myMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(myMemoryStream);
                    myMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        public ActionResult ExportTdsComponentDetails(string formName, int businessUnitId, int financialYearId)
        {
            TdsRepository tdsRepository = new TdsRepository();
            TdsComponentBo tdsComponentBo = new TdsComponentBo
            {
                BusinessUnitId = businessUnitId,
                FinancialYearId = financialYearId
            };

            List<TdsComponentBo> data = tdsRepository.SearchTdsOtherComponent(tdsComponentBo);

            DataTable dt = new DataTable
            {
                TableName = "TDS_Other_Component"// Excel sheet name.
            };
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Financial Year", typeof(string));
            dt.Columns.Add("Bonus", typeof(decimal));
            dt.Columns.Add("PLI", typeof(decimal));
            dt.Columns.Add("Other Income", typeof(decimal));
            dt.Columns.Add("Business Unit", typeof(string));

            foreach (TdsComponentBo i in data)
            {
                dt.Rows.Add(
                        i.EmployeeName,
                        i.EmployeeCode,
                        i.FinancialYearId,
                        i.Bonus,
                        i.Pli,
                        i.OtherIncome,
                        i.BusinessUnit);
            }
            return ExportDataTableExcel("TDS_Other_Component", dt);
        }

        #endregion TDS Component

        #region Sales Compliance Excel Export

        public ActionResult SalesComplianceReportExcelExport(ReportBo reportBo)
        {
            StringBuilder sb = new StringBuilder();
            string dateFormat = DateTime.Now.ToString("dd-MMM-yyyy");
            string reportName = "Sales_Compliance_report";

            string fileName = reportName + "_" + dateFormat + ".xls";

            ReportRepository reportRepository = new ReportRepository();
            DataTable data = reportRepository.SearchSalesComplianceReport(reportBo);

            sb.Append("<table style='1px solid black; font-size:14px; font-family:Cambria;' border='1' >");
            sb.Append(GetExcelHeader(reportName));
            sb.Append("<tr>");

            foreach (DataColumn col in data.Columns)
            {
                sb.Append("<td style='width:140px;background-color:#b0d895;' border='1'><b>" + col.ColumnName + "</b></td>");
            }
            sb.Append("</tr>");

            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    sb.Append("<tr style='height:20px;'>");
                    foreach (DataColumn col in data.Columns)
                    {
                        if (col.ColumnName.ToUpper().Contains("DAY") && !string.IsNullOrWhiteSpace(row[col.ColumnName].ToString()))
                        {
                            sb.Append("<td>" + row[col.ColumnName].ToString().Split(',')[2] + "</td>");
                        }
                        else
                        {
                            sb.Append("<td>" + Convert.ToString(row[col.ColumnName]) + "</td>");
                        }
                    }
                    sb.Append("</tr>");
                }
            }

            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "data:application/vnd.ms-excel";
            byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
            return File(buffer, "data:application/vnd.ms-excel");
        }

        #endregion Sales Compliance Excel Export

        #region Export Employee Leave Days

        public void ExportEmployeeLeaveDays(string formName, string downloadFileName, int year, int businessUnitId)
        {
            string fileName = downloadFileName + ".xlsx";

            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();

            DataTable data = leaveManagementRepository.ExportEmployeeLeaveDays(year, businessUnitId);

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(data, formName);

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                using (MemoryStream myMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(myMemoryStream);
                    myMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        #endregion Export Employee Leave Days

        #region Designation Mapping

        public ActionResult DesignationMapping(string formName)
        {
            EmployeeClaimsConfigurationRepository employeeClaimsConfigurationRepository = new EmployeeClaimsConfigurationRepository();

            List<EmployeeClaimsBo> data = employeeClaimsConfigurationRepository.SearchDesignationMapping();

            DataTable dt = new DataTable
            {
                TableName = "Designation_Mapping"// Excel sheet name.
            };
            dt.Columns.Add("Designation", typeof(string));
            dt.Columns.Add("Band", typeof(string));
            dt.Columns.Add("Grade", typeof(string));

            foreach (EmployeeClaimsBo i in data)
            {
                dt.Rows.Add(
                        i.Designation,
                        i.Band,
                        i.Level);
            }
            return ExportDataTableExcel("Designation_Mapping", dt);
        }

        #endregion Designation Mapping

        #region Available Leave List

        public ActionResult AvailableLeaveListExcelExport(string reportName, int yearId, int businessUnitId)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            LeaveManagementBo leaveManagementBo = new LeaveManagementBo
            {
                YearId = yearId,
                BusinessUnitId = businessUnitId
            };

            DataTable data = leaveManagementRepository.GetAvailableLeaveList(leaveManagementBo);

            DataTable dt = new DataTable();

            foreach (DataColumn col in data.Columns)
            {
                if (Convert.ToString(col.ColumnName).ToLower() == "name")
                {
                    dt.Columns.Add("Employee_Name");
                }
                else
                {
                    dt.Columns.Add(col.ColumnName);
                }
            }

            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    List<string> newRowList = new List<string>();
                    foreach (DataColumn col in data.Columns)
                    {
                        newRowList.Add(row[col.ColumnName].ToString());
                    }
                    dt.Rows.Add(newRowList.ToArray());
                }
            }
            return ExportDataTableExcel(reportName.Replace(" ", ""), dt);
        }

        #endregion Available Leave List

        #region Complete Leave Report

        public ActionResult CompleteLeaveReportExcel(string formName, int statusId, DateTime fromDate, DateTime toDate, int businessUnitId, int employeeId)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            LeaveReportBo leaveReportBo = new LeaveReportBo
            {
                StatusId = statusId,
                FromDate = fromDate,
                ToDate = toDate,
                BusinessUnitId = businessUnitId,
                EmployeeId = employeeId
            };

            List<LeaveReportBo> data = leaveManagementRepository.CompleteLeaveReportList(leaveReportBo);

            DataTable dt = new DataTable();
            dt.Columns.Add("Emp_Code", typeof(string));
            dt.Columns.Add("Employee_Name", typeof(string));
            dt.Columns.Add("Total Leave", typeof(string));
            dt.Columns.Add("From Date", typeof(DateTime));
            dt.Columns.Add("To Date", typeof(DateTime));
            dt.Columns.Add("Day(s)", typeof(string));
            dt.Columns.Add("Leave Type", typeof(string));
            dt.Columns.Add("Half Day", typeof(string));
            dt.Columns.Add("Reason", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));

            if (data != null && data.Any())
            {
                string oldEmployeeName = "";
                foreach (LeaveReportBo result in data)
                {
                    string fDate = Convert.ToDateTime(result.FromDate).ToString("dd-MMM-yyyy");
                    string tDate = Convert.ToDateTime(result.ToDate).ToString("dd-MMM-yyyy");
                    string employeeName = result.EmployeeName;
                    string totalLeave = result.TotalLeave.ToString("#0.0");
                    string noOfDays = result.NoOfDays.ToString("#0.0");

                    DataRow newRow = dt.NewRow();

                    if (employeeName != oldEmployeeName)
                    {
                        newRow["Emp_Code"] = result.EmployeeCode;
                        newRow["Employee_Name"] = employeeName;
                        newRow["Total Leave"] = totalLeave;
                    }
                    else
                    {
                        newRow["Emp_Code"] = "";
                        newRow["Employee_Name"] = "";
                        newRow["Total Leave"] = "";
                    }

                    newRow["From Date"] = fDate;
                    newRow["To Date"] = tDate;
                    newRow["Day(s)"] = noOfDays;
                    newRow["Leave Type"] = result.LeaveType;
                    newRow["Half Day"] = (result.IsHalfDay ? "Yes" : "No");
                    newRow["Reason"] = result.Remarks;
                    newRow["Status"] = result.Status;
                    if (employeeName != oldEmployeeName)
                    {
                        newRow["Business Unit"] = result.BusinessUnit;
                    }
                    else
                    {
                        newRow["Business Unit"] = "";
                    }
                    if (employeeName != oldEmployeeName)
                    {
                        oldEmployeeName = employeeName;
                    }
                    dt.Rows.Add(newRow);
                }
            }
            return ExportDataTableExcel("Complete_Leave_Report", dt);
        }

        #endregion Complete Leave Report

        #region Comp Off

        #region CompOff Request

        public ActionResult CompOffRequestExcel(string formName, int statusId, int employeeId)
        {
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();
            EmployeeCompOffBo employeeCompOffBo = new EmployeeCompOffBo
            {
                StatusId = statusId
            };
            if (employeeId == 0)
            {
                employeeCompOffBo.EmployeeId = Utility.UserId();
            }
            else
            {
                employeeCompOffBo.EmployeeId = employeeId;
            }
            List<EmployeeCompOffBo> data = employeeCompOffRepository.SearchCompOffRequest(employeeCompOffBo);

            DataTable dt = new DataTable
            {
                TableName = "Comp_Off_Request"// Excel sheet name.
            };
            dt.Columns.Add("Comp Off Date", typeof(string));
            dt.Columns.Add("Duration", typeof(string));
            dt.Columns.Add("Approver Name", typeof(string));
            dt.Columns.Add("Approved On", typeof(string));
            dt.Columns.Add("HR Name", typeof(string));
            dt.Columns.Add("HR Approved On", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (EmployeeCompOffBo i in data)
            {
                string duration = Convert.ToString(i.Duration).Substring(0, 5);
                string approvedDate = i.ApprovedDate != DateTime.MinValue ? (Convert.ToDateTime(i.ApprovedDate)).ToString("dd-MMM-yyyy") : null;
                string hrApprovedDate = i.HRApprovedDate != DateTime.MinValue ? (Convert.ToDateTime(i.HRApprovedDate)).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                       (Convert.ToDateTime(i.Date)).ToString("dd-MMM-yyyy"),
                        duration,
                        i.ApproverName,
                        approvedDate,
                        i.HrName,
                        hrApprovedDate,
                        i.StatusCode);
            }
            return ExportDataTableExcel("Comp_Off_Request", dt);
        }

        #endregion CompOff Request

        #region CompOff Approval

        public ActionResult CompOffApprovalExcel(string formName, int statusId)
        {
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();
            EmployeeCompOffBo employeeCompOffBo = new EmployeeCompOffBo
            {
                EmployeeId = Utility.UserId(),
                StatusId = statusId
            };

            List<EmployeeCompOffBo> data = employeeCompOffRepository.SearchCompOffApproval(employeeCompOffBo);

            DataTable dt = new DataTable
            {
                TableName = "Comp_Off_Approval"// Excel sheet name.
            };
            dt.Columns.Add("Comp Off Date", typeof(string));
            dt.Columns.Add("Requester Name", typeof(string));
            dt.Columns.Add("Requested On", typeof(string));
            dt.Columns.Add("Duration", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Approved On", typeof(string));

            foreach (EmployeeCompOffBo i in data)
            {
                string duration = Convert.ToString(i.Duration).Substring(0, 5);
                string approvedDate = i.ApprovedDate != DateTime.MinValue ? (Convert.ToDateTime(i.ApprovedDate)).ToString("dd-MMM-yyyy") : null;
                string requestedOn = i.CreatedOn != DateTime.MinValue ? Convert.ToDateTime(i.CreatedOn).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                       (Convert.ToDateTime(i.Date)).ToString("dd-MMM-yyyy"),
                       i.EmployeeName,
                       requestedOn,
                        duration,
                        i.StatusCode,
                        approvedDate);
            }
            return ExportDataTableExcel("Comp_Off_Approval", dt);
        }

        #endregion CompOff Approval

        #region CompOff HR Approval

        public ActionResult CompOffHrApprovalListExcelExport(string formName, int statusId)
        {
            EmployeeCompOffRepository employeeCompOffRepository = new EmployeeCompOffRepository();
            EmployeeCompOffBo employeeCompOffBo = new EmployeeCompOffBo
            {
                EmployeeId = Utility.UserId(),
                StatusId = statusId
            };

            List<EmployeeCompOffBo> data = employeeCompOffRepository.CompOffHrApprovalList(employeeCompOffBo);

            DataTable dt = new DataTable
            {
                TableName = "Comp_Off_HR_Approval"// Excel sheet name.
            };
            dt.Columns.Add("Comp Off Date", typeof(string));
            dt.Columns.Add("Requester Name", typeof(string));
            dt.Columns.Add("Requested On", typeof(string));
            dt.Columns.Add("Duration", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Approved On", typeof(string));

            foreach (EmployeeCompOffBo i in data)
            {
                string duration = Convert.ToString(i.Duration).Substring(0, 5);
                string approvedDate = i.ApprovedDate != DateTime.MinValue ? (Convert.ToDateTime(i.ApprovedDate)).ToString("dd-MMM-yyyy") : null;
                string requestedOn = i.CreatedOn != DateTime.MinValue ? Convert.ToDateTime(i.CreatedOn).ToString("dd-MMM-yyyy") : null;
                dt.Rows.Add(
                       (Convert.ToDateTime(i.Date)).ToString("dd-MMM-yyyy"),
                       i.EmployeeName,
                       requestedOn,
                        duration,
                        i.StatusCode,
                        approvedDate);
            }
            return ExportDataTableExcel("Comp_Off_HR_Approval", dt);
        }

        #endregion CompOff HR Approval

        #endregion Comp Off

        #region Admin

        public ActionResult AdminForCompanyExcelExport(string formName, string name)
        {
            AdminBo adminBo = new AdminBo();
            AdminRepository adminRepository = new AdminRepository();

            adminBo.CompanyName = name;

            List<AdminBo> data = adminRepository.GetCompanyAdminList(adminBo);

            DataTable dt = new DataTable
            {
                TableName = "Manage_Customer_Details"// Excel sheet name.
            };
            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("No of BU's", typeof(string));
            dt.Columns.Add("Total No of Users", typeof(string));
            dt.Columns.Add("Active Users", typeof(string));
            dt.Columns.Add("Email ID", typeof(string));
            dt.Columns.Add("Contact No", typeof(string));
            dt.Columns.Add("Action", typeof(string));

            foreach (AdminBo i in data)
            {
                dt.Rows.Add(
                        i.CompanyName,
                        i.BusinessUnitCount,
                        i.NoOfEmployees,
                        i.ActiveCount,
                        i.EmailId,
                        i.ContactNo,
                        i.IsActive ? "Inactive" : "Active");
            }
            return ExportDataTableExcel("Manage_Customer_Details", dt);
        }

        #endregion Admin

        #region TDS Limit Configuration

        public ActionResult TdsLimitConfiguration(string formName, int year, int regionId)
        {
            TdsConfigurationRepository tdsConfigurationRepository = new TdsConfigurationRepository();
            TdsConfigurationBo tdsConfigurationBo = new TdsConfigurationBo
            {
                FinancialYearId = year,
                RegionId = regionId
            };

            List<TdsConfigurationBo> data = tdsConfigurationRepository.SearchTdsLimitConfiguration(tdsConfigurationBo);

            DataTable dt = new DataTable
            {
                TableName = "TDS_Limit_Configuration"// Excel sheet name.
            };
            dt.Columns.Add("Financial Year", typeof(string));
            dt.Columns.Add("Section", typeof(string));
            dt.Columns.Add("Ceiling Amount", typeof(decimal));
            dt.Columns.Add("Region", typeof(string));

            foreach (TdsConfigurationBo i in data)
            {
                dt.Rows.Add(
                        i.FinancialYear,
                        i.Section,
                        i.Limit.ToString("##,#.00"),
                        i.Region);
            }
            return ExportDataTableExcel("TDS_Limit_Configuration", dt);
        }

        #endregion TDS Limit Configuration

        #region ApplicationConfiguration

        public ActionResult ApplicationConfigurationExcelExport(string formName, int employeeId)
        {
            string dateFormat = DateTime.Now.ToString("dd-MMM-yyyy");
            string reportName = "Application_Configuration_Report";

            StringBuilder sb = new StringBuilder();
            string fileName = reportName + "_" + dateFormat + ".xls";

            ApplicationConfigurationReportBo applicationConfigurationReportBo = new ApplicationConfigurationReportBo();
            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();

            applicationConfigurationReportBo.EmployeeId = employeeId;

            DataTable dt = applicationConfigurationRepository.GetBusinessAccessReport(employeeId);

            sb.Append("<table style='1px solid black; font-size:14px; font-family:Cambria;' border='1' >");
            sb.Append(GetExcelHeader(reportName));
            sb.Append("<tr>");

            foreach (DataColumn col in dt.Columns)
            {
                if (Convert.ToString(col.ColumnName).ToLower() == "employeecode")
                {
                    sb.Append("<td style='width:200px;background-color:#b0d895;' border='1'><b>" + "Employee_Code" + "</b></td>");
                }
                else if (Convert.ToString(col.ColumnName).ToLower() == "employeename")
                {
                    sb.Append("<td style='width:200px;background-color:#b0d895;' border='1'><b>" + "Employee_Name" + "</b></td>");
                }
                else
                {
                    sb.Append("<td style='width:140px;background-color:#b0d895;' border='1'><b>" + col.ColumnName + "</b></td>");
                }
            }
            sb.Append("</tr>");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    sb.Append("<tr style='height:20px;'>");
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (Convert.ToString(col.ColumnName).ToLower() == "employeecode")
                        {
                            sb.Append("<td style='text-align:right'>" + row[col.ColumnName].ToString().Split(',')[0] + "</td>");
                        }
                        else if (Convert.ToString(col.ColumnName).ToLower() == "employeename")
                        {
                            sb.Append("<td style='text-align:left'>" + row[col.ColumnName].ToString().Split(',')[0] + "</td>");
                        }
                        else
                        {
                            if (row[col.ColumnName].ToString().Split(',')[0] == "1")
                            {
                                sb.Append("<td style='text-align:center'>" + "Has Access" + "</td>");
                            }
                            else
                            {
                                sb.Append("<td style='text-align:center'>" + "No Access" + "</td>");
                            }
                        }
                    }
                    sb.Append("</tr>");
                }
            }
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "data:application/vnd.ms-excel";
            byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
            return File(buffer, "data:application/vnd.ms-excel");
        }

        #endregion ApplicationConfiguration

        #region AuditTrail

        public ActionResult AuditTrailExcelExport(string formName, string tableName, DateTime? fromDate, DateTime? toDate)
        {
            string dateFormat = DateTime.Now.ToString("dd-MMM-yyyy");
            string reportName = "Audit_Trail";

            StringBuilder sb = new StringBuilder();
            string fileName = reportName + "_" + dateFormat + ".xls";

            AuditTrailBo auditTrailBo = new AuditTrailBo();
            AuditRepository auditRepository = new AuditRepository();

            auditTrailBo.TableName = tableName;

            auditTrailBo.ToDate = toDate ?? DateTime.Now;
            auditTrailBo.FromDate = fromDate ?? (auditTrailBo.ToDate.Value.AddYears(-1));

            DataTable dt = auditRepository.AuditTrialList(auditTrailBo);

            sb.Append("<table style='1px solid black; font-size:14px; font-family:Cambria;' border='1' >");
            sb.Append(GetExcelHeader(reportName));
            sb.Append("<tr>");

            if (dt != null)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (col.ColumnName.ToUpper() != "ROWID")
                    {
                        switch (col.DataType.FullName)
                        {
                            case "System.Int32":
                                sb.Append("<td style='width:140px;background-color:#b0d895;' border='1'><b>" + col.ColumnName + "</b></td>");
                                break;

                            case "System.DateTime":
                                sb.Append("<td style='width:200px;background-color:#b0d895;' border='1'><b>" + col.ColumnName + "</b></td>");
                                break;

                            default:
                                sb.Append("<td style='width:300px;background-color:#b0d895;' border='1'><b>" + col.ColumnName + "</b></td>");
                                break;
                        }
                    }
                }
                sb.Append("</tr>");

                foreach (DataRow row in dt.Rows)
                {
                    sb.Append("<tr style='height:20px;'>");
                    string bgcolor = string.Empty;
                    foreach (DataColumn col in dt.Columns)
                    {
                        if (col.ColumnName.ToUpper() == "ROWID")
                        {
                            bgcolor = row[col.ColumnName].ToString() == "1" ? "" : "orange !important";
                        }
                        if (col.ColumnName.ToUpper() != "ROWID")
                        {
                            switch (col.DataType.FullName)
                            {
                                case "System.Int32":
                                    sb.Append("<td style='mso-number-format:\\@;text-align:right;background-color:" + bgcolor + ";'>" + Convert.ToString(row[col.ColumnName]) + "</td>");
                                    break;

                                case "System.DateTime":
                                    sb.Append("<td style='mso-number-format:\\@;text-align:right;background-color:" + bgcolor + ";'>" + Convert.ToDateTime(row[col.ColumnName]).ToString("dd MMM yyyy HH:mm tt") + "</td>");
                                    break;

                                default:
                                    sb.Append("<td style='mso-number-format:\\@;text-align:left;background-color:" + bgcolor + ";'>" + Convert.ToString(row[col.ColumnName]) + "</td>");
                                    break;
                            }
                        }
                    }
                    sb.Append("</tr>");
                }
            }
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "data:application/vnd.ms-excel";
            byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
            return File(buffer, "data:application/vnd.ms-excel");
        }

        #endregion AuditTrail

        #region Master

        #region Vender Master

        public ActionResult VendorMasterExcel(string formName)
        {
            VendorMasterBo vendorMasterBo = new VendorMasterBo();
            VendorMasterRepository vendorMasterRepository = new VendorMasterRepository();

            List<VendorMasterBo> data = vendorMasterRepository.SearchVendor(vendorMasterBo);

            DataTable dt = new DataTable
            {
                TableName = "Vendor_Master"// Excel sheet name.
            };
            dt.Columns.Add("Vendor Name", typeof(string));
            dt.Columns.Add("Contact No", typeof(string));
            dt.Columns.Add("Contact Person", typeof(string));
            dt.Columns.Add("Contact Person No", typeof(string));
            dt.Columns.Add("City", typeof(string));
            dt.Columns.Add("PAN No", typeof(string));
            dt.Columns.Add("Currency", typeof(string));
            dt.Columns.Add("Payment Terms", typeof(string));

            foreach (VendorMasterBo i in data)
            {
                dt.Rows.Add(
                        i.Name,
                        i.ContactNo,
                        i.ContactPerson,
                        i.ContactPersonNo,
                        i.City.Name,
                        i.PanNo,
                        i.Currency.Name,
                        i.PaymentTerms);
            }
            return ExportDataTableExcel("Vendor_Master", dt);
        }

        #endregion Vender Master

        #region Customer Master

        public ActionResult CustomerMasterExcel(string formName)
        {
            CustomerRepository customerRepository = new CustomerRepository();
            CustomerDetailsBo customerDetailsBo = new CustomerDetailsBo();

            List<CustomerDetailsBo> data = customerRepository.SearchCustomer(customerDetailsBo);

            DataTable dt = new DataTable
            {
                TableName = "Customer_Master"// Excel sheet name.
            };
            dt.Columns.Add("Customer Name", typeof(string));
            dt.Columns.Add("Contact No", typeof(string));
            dt.Columns.Add("Contact Person", typeof(string));
            dt.Columns.Add("Contact Person No", typeof(string));
            dt.Columns.Add("City", typeof(string));
            dt.Columns.Add("PAN No", typeof(string));
            dt.Columns.Add("Currency", typeof(string));

            foreach (CustomerDetailsBo i in data)
            {
                dt.Rows.Add(
                        i.Name,
                        i.ContactNo,
                        i.ContactPerson,
                        i.ContactPersonNo,
                        i.City.Name,
                        i.PanNo,
                        i.Currency.Name);
            }
            return ExportDataTableExcel("Customer_Master", dt);
        }

        #endregion Customer Master

        #region Product Master

        public ActionResult ProductMasterExcel(string formName, string productName, int typeId)
        {
            ProductMasterRepository productMasterRepository = new ProductMasterRepository();
            ProductMasterBo productMasterBo = new ProductMasterBo
            {
                Name = productName,
                TypeId = typeId
            };

            List<ProductMasterBo> data = productMasterRepository.SearchProduct(productMasterBo);

            DataTable dt = new DataTable
            {
                TableName = "Product_Master"// Excel sheet name.
            };
            dt.Columns.Add("Name", typeof(string));
            //dt.Columns.Add("Transaction Type", typeof(string));
            //dt.Columns.Add("Is Sales", typeof(string));
            dt.Columns.Add("Sales Ledger", typeof(string));
            // dt.Columns.Add("Sales Rate", typeof(string));
            //dt.Columns.Add("Is Purchase", typeof(string));
            dt.Columns.Add("Purchase Ledger", typeof(string));
            dt.Columns.Add("Manufacturer", typeof(string));
            // dt.Columns.Add("Purchase Rate", typeof(string));
            //dt.Columns.Add("Opening Stock", typeof(string));
            // dt.Columns.Add("Low Stock Threshold", typeof(string));
            //dt.Columns.Add("Is Inventory", typeof(string));
            dt.Columns.Add("HSN Code", typeof(string));
            dt.Columns.Add("CGST", typeof(string));
            dt.Columns.Add("SGST", typeof(string));
            dt.Columns.Add("IGST", typeof(string));

            foreach (ProductMasterBo i in data)
            {
                dt.Rows.Add(
                        i.Name,
                        // i.Type,
                        //i.IsSales == true ? "Yes" : "No",
                        i.SalesLedgerName,
                        // i.SaleRate == null ? "" : Convert.ToDecimal(i.SaleRate).ToString("#,##0.00"),
                        //i.IsPurchase == true ? "Yes" : "No",
                        i.PurchaseLedgerName,
                        i.Manufacturer,
                        // i.PurchaseRate == null ? "" : Convert.ToDecimal(i.PurchaseRate).ToString("#,##0.00"),
                        // i.OpeningStock == null ? "" : Convert.ToDecimal(i.OpeningStock).ToString("#,##0.00"),
                        //i.LowStockThresold == null ? "" : Convert.ToDecimal(i.OpeningStock).ToString("#,##0.00"),
                        //i.IsInventory == true ? "Yes" : "No",
                        i.HsnCode,
                        i.Cgst,
                        i.Sgst,
                        i.Igst
                        );
            }
            return ExportDataTableExcel("Product_Master", dt);
        }

        #endregion Product Master

        #region Ledger

        public ActionResult LedgerExcel(string formName)
        {
            LedgerRepository ledgerRepository = new LedgerRepository();
            LedgerBo ledgerBo = new LedgerBo();

            List<LedgerBo> data = ledgerRepository.SearchLedger(ledgerBo);

            DataTable dt = new DataTable
            {
                TableName = "Chart_of_Accounts"// Excel sheet name.
            };
            dt.Columns.Add("Ledger", typeof(string));
            dt.Columns.Add("Opening Balance", typeof(decimal));
            dt.Columns.Add("Group", typeof(string));
            dt.Columns.Add("Ledger Type", typeof(string));
            dt.Columns.Add("CGST", typeof(string));
            dt.Columns.Add("SGST", typeof(string));
            dt.Columns.Add("IGST", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (LedgerBo i in data)
            {
                dt.Rows.Add(
                        i.Name,
                        i.OpeningBalance,
                        i.Group.Name,
                        i.LedgerType,
                        i.Cgst,
                        i.Sgst,
                        i.Igst,
                        i.Description);
            }
            return ExportDataTableExcel("Chart_of_Accounts", dt);
        }

        #endregion Ledger

        #region Group

        public ActionResult GroupLedgerExcel(string formName)
        {
            GroupLedgerRepository groupLedgerRepository = new GroupLedgerRepository();
            GroupLedgerBo groupLedgerBo = new GroupLedgerBo();

            List<GroupLedgerBo> data = groupLedgerRepository.SearchGroupLedger(groupLedgerBo);

            DataTable dt = new DataTable
            {
                TableName = "Group"// Excel sheet name.
            };
            dt.Columns.Add("Group", typeof(string));
            dt.Columns.Add("Type", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (GroupLedgerBo i in data)
            {
                string reportType = string.Empty;
                if (i.ReportType.Id == 1)
                {
                    reportType = "Assets";
                }
                else if (i.ReportType.Id == 2)
                {
                    reportType = "Liabilities";
                }
                else if (i.ReportType.Id == 3)
                {
                    reportType = "Income (P & L)";
                }
                else if (i.ReportType.Id == 4)
                {
                    reportType = "Expense (P & L)";
                }
                dt.Rows.Add(
                        i.Group,
                        reportType,
                        i.Description);
            }
            return ExportDataTableExcel("Group", dt);
        }

        #endregion Group

        #region Bank Master

        public ActionResult BankMasterExcel(string formName)
        {
            BankRepository bankRepository = new BankRepository();
            BankBo bankBo = new BankBo();

            List<BankBo> data = bankRepository.SearchBankList(bankBo);

            DataTable dt = new DataTable
            {
                TableName = "Bank_Master"// Excel sheet name.
            };
            dt.Columns.Add("Bank Name", typeof(string));
            dt.Columns.Add("Branch Name", typeof(string));
            dt.Columns.Add("Account No", typeof(string));
            dt.Columns.Add("Account Type", typeof(string));
            dt.Columns.Add("Opening Balance Date", typeof(string));
            dt.Columns.Add("Opening Balance", typeof(decimal));
            dt.Columns.Add("Closing Balance", typeof(decimal));
            dt.Columns.Add("IFSC Code", typeof(string));
            dt.Columns.Add("SWIFT Code", typeof(string));
            dt.Columns.Add("MICR Code", typeof(string));
            dt.Columns.Add("Display Name", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (BankBo i in data)
            {
                dt.Rows.Add(
                        i.BankName,
                        i.BranchName,
                        i.AccountNo,
                        i.AccountType.Name,
                        Convert.ToDateTime(i.OpeningBalanceDate).ToString("dd-MMM-yyyy"),
                        i.OpeningBalance,
                        i.ClosingBalance,
                        i.IfscCode,
                        i.SwiftCode,
                        i.MicrCode,
                        i.DisplayName,
                        i.Description);
            }
            return ExportDataTableExcel("Bank_Master", dt);
        }

        #endregion Bank Master

        #endregion Master

        #region Income

        public ActionResult IncomeExcelExport(DateTime? startDate, DateTime? endDate, string customerName, string status, string notations, string amount, int costCenterId)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            IncomeRepository incomeRepository = new IncomeRepository();
            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;
            searchParamsBo.CustomerName = customerName;
            searchParamsBo.Status = status;
            searchParamsBo.Notations = notations;
            searchParamsBo.Amount = amount == "" ? null : amount;
            searchParamsBo.CostCenterId = costCenterId;
            List<IncomeBo> data = incomeRepository.SearchIncome(searchParamsBo);

            DataTable dt = new DataTable
            {
                TableName = "Invoice"// Excel sheet name.
            };
            dt.Columns.Add("Invoice Date", typeof(string));
            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("Invoice No", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Due Date", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (IncomeBo i in data)
            {
                dt.Rows.Add(
                        Convert.ToDateTime(i.InvoiceDate).ToString("dd-MMM-yyyy"),
                        i.CustomerName,
                        i.InvoiceNo,
                        i.TotalAmount,
                        i.Remarks,
                        Convert.ToDateTime(i.DueDate).ToString("dd-MMM-yyyy"),
                        i.Status);
            }
            return ExportDataTableExcel("Invoice", dt);
        }

        #endregion Income

        #region Expense

        public ActionResult ExpenseExcelExport(DateTime? startDate, DateTime? endDate, string vendorName, string status, string screenType, string notations, string amount, int costCenterId, string ledgerName, string Event, string columnName)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            ExpenseRepository expenseRepository = new ExpenseRepository();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;
            searchParamsBo.VendorName = vendorName;
            searchParamsBo.Status = status;
            searchParamsBo.ScreenType = screenType;
            searchParamsBo.Notations = notations;
            searchParamsBo.Amount = amount == "" ? null : amount;
            searchParamsBo.CostCenterId = costCenterId;
            searchParamsBo.LedgerName = ledgerName;
            searchParamsBo.Event = Event;
            searchParamsBo.ColumnName = columnName;
            List<ExpenseBo> data;

            if (searchParamsBo.Event == "report")
            {
                data = expenseRepository.SearchExpenseDuplicatedRecords(searchParamsBo);
            }
            else
            {
                data = expenseRepository.SearchExpenseTransactionDetails(searchParamsBo);
            }

            DataTable dt = new DataTable
            {
                TableName = "Expense"// Excel sheet name.
            };
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Vendor", typeof(string));
            dt.Columns.Add("Ledger/Product", typeof(string));
            dt.Columns.Add("Bill No", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Due Date", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            foreach (ExpenseBo i in data)
            {
                string dueDate = i.ScreenType == "Bill" ? Convert.ToDateTime(i.DueDate).ToString("dd-MMM-yyyy") : "";
                dt.Rows.Add(
                        Convert.ToDateTime(i.BillDate).ToString("dd-MMM-yyyy"),
                        i.TotalAmount,
                        i.VendorName,
                        i.UserMessage,
                        i.BillNo,
                        i.Remarks.TrimEnd(','),
                        dueDate,
                        i.Status);
            }
            return ExportDataTableExcel("Expense", dt);
        }

        #endregion Expense

        #region Master Data

        #region Cost Center

        public ActionResult CostCenterExcel(string formName)
        {
            CostCenterBo costCenterBo = new CostCenterBo();
            CostCenterRepository costCenterRepository = new CostCenterRepository();

            List<CostCenterBo> data = costCenterRepository.SearchCostCenterList(costCenterBo);

            DataTable dt = new DataTable
            {
                TableName = "Cost_Center"// Excel sheet name.
            };
            dt.Columns.Add("Code", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (CostCenterBo i in data)
            {
                dt.Rows.Add(
                        i.Code,
                        i.Description);
            }
            return ExportDataTableExcel("Cost_Center", dt);
        }

        #endregion Cost Center

        #region Financial Year

        public ActionResult FinancialYearExcel(string formName)
        {
            FinancialYearBo financialyearBo = new FinancialYearBo();
            FinancialYearRepository financialYearRepository = new FinancialYearRepository();

            List<FinancialYearBo> data = financialYearRepository.SearchFinancialYear(financialyearBo);

            DataTable dt = new DataTable
            {
                TableName = "Financial_Year"// Excel sheet name.
            };
            dt.Columns.Add("Start Year", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (FinancialYearBo i in data)
            {
                dt.Rows.Add(
                        i.FinancialYear,
                        i.Description);
            }
            return ExportDataTableExcel("Financial_Year", dt);
        }

        #endregion Financial Year

        #region GST Configuration

        public ActionResult GstConfigurationExcelExport(string formName)
        {
            ProductMasterRepository productMasterRepository = new ProductMasterRepository();
            ProductMasterBo productMasterBo = new ProductMasterBo();

            List<ProductMasterBo> data = productMasterRepository.GetGstHsnList(productMasterBo);

            DataTable dt = new DataTable
            {
                TableName = "GST_Configuration"// Excel sheet name.
            };
            dt.Columns.Add("HSN Code", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("CGST", typeof(decimal));
            dt.Columns.Add("SGST", typeof(decimal));
            dt.Columns.Add("IGST", typeof(decimal));

            foreach (ProductMasterBo i in data)
            {
                dt.Rows.Add(
                        i.GsthsnCode,
                        i.GstDescription,
                        i.Cgst,
                        i.Sgst,
                        i.Igst);
            }
            return ExportDataTableExcel("GST_Configuration", dt, false, "ExcludeTotal");
        }

        #endregion GST Configuration

        #region Accounts Configuration

        public ActionResult ProfitShareAnalysisConfigurationExcel(string formName, string employeeName)
        {
            Profit_Share_Analysis_ConfigurationBo profit_Share_Analysis_ConfigurationBo = new Profit_Share_Analysis_ConfigurationBo();
            Profit_Share_Analysis_Configuration_Repository profit_Share_Analysis_Configuration_Repository = new Profit_Share_Analysis_Configuration_Repository();

            List<Profit_Share_Analysis_ConfigurationBo> data = profit_Share_Analysis_Configuration_Repository.ProfitShareAnalysisConfigurationSearch(employeeName);

            DataTable dt = new DataTable
            {
                TableName = "Profit_Share_Analysis"// Excel sheet name.
            };
            dt.Columns.Add("Employee", typeof(string));
            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("Vendor", typeof(string));
            dt.Columns.Add("Percentage", typeof(decimal));

            foreach (Profit_Share_Analysis_ConfigurationBo i in data)
            {
                dt.Rows.Add(
                        i.EmployeeName,
                        i.CustomerName,
                        i.VendorName,
                        i.Percentage);
            }
            return ExportDataTableExcel("Profit_Share_Analysis", dt);
        }

        #endregion Accounts Configuration

        #endregion Master Data

        #region Journal

        public ActionResult JournalExcel(string formName)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            DataSet ds = new DataSet();
            DataTable dtExpenseSupplier = new DataTable("Journal");
            ds.Tables.Add(dtExpenseSupplier);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(Constants.SEARCH_JOURNAL, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.ScreenType, "R");
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        ds.Load(dr, LoadOption.OverwriteChanges, dtExpenseSupplier);
                    }
                }
            }
            return ExportDataTableExcel("Journal", dtExpenseSupplier, true);
        }

        #endregion Journal

        #region Transfer

        public ActionResult TransferExcel(string formName, DateTime? startDate, DateTime? endDate, int bankId, string notations, string amount)
        {
            TransferRepository transferRepository = new TransferRepository();
            SearchParamsBo searchParamsBo = new SearchParamsBo
            {
                StartDate = startDate,
                EndDate = endDate,
                Id = bankId,
                Notations = notations,
                Amount = amount == "" ? null : amount
            };
            List<TransferBo> data = transferRepository.SearchTransfer(searchParamsBo);

            DataTable dt = new DataTable
            {
                TableName = "Transfer"// Excel sheet name.
            };
            dt.Columns.Add("Voucher Date", typeof(string));
            dt.Columns.Add("Transfer From", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Transfer To", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (TransferBo i in data)
            {
                string transferFrom = i.TransferFrom.Id == 0 ? "Cash" : i.TransferFrom.Name;
                string transferTo = i.TransferTo.Id == 0 ? "Cash" : i.TransferTo.Name;
                dt.Rows.Add(
                        Convert.ToDateTime(i.Date).ToString("dd-MMM-yyyy"),
                        transferFrom,
                        i.Amount,
                        transferTo,
                        i.Description);
            }
            return ExportDataTableExcel("Contra", dt);
        }

        #endregion Transfer

        #region Payments

        public ActionResult PaymentsExcel(string formName, DateTime? startDate, DateTime? endDate, string partyName, string notations, string amount, string ledgerName)
        {
            IncomeRepository incomeRepository = new IncomeRepository();
            SearchParamsBo searchParamsBo = new SearchParamsBo
            {
                StartDate = startDate,
                EndDate = endDate,
                PartyName = partyName,
                Notations = notations,
                Amount = amount == "" ? null : amount,
                LedgerName = ledgerName
            };

            List<IncomeBo> data = incomeRepository.SearchReceiptPayments(searchParamsBo);

            DataTable dt = new DataTable
            {
                TableName = "Payments"// Excel sheet name.
            };
            dt.Columns.Add("Pay Date", typeof(string));
            dt.Columns.Add("Party", typeof(string));
            dt.Columns.Add("Ledger", typeof(string));
            dt.Columns.Add("Pay Mode", typeof(string));
            dt.Columns.Add("Bank", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Description", typeof(string));

            foreach (IncomeBo i in data)
            {
                dt.Rows.Add(
                        Convert.ToDateTime(i.InvoiceDate).ToString("dd-MMM-yyyy"),
                        i.PartyName,
                        i.LedgerName,
                        i.PaymentMode.Name,
                        i.Bank.Name,
                        i.ReceivedAmount,
                        i.Remarks);
            }
            return ExportDataTableExcel("Payments", dt);
        }

        #endregion Payments

        #region Receipts

        public ActionResult ReceiptsExcel(string formName, DateTime? startDate, DateTime? endDate, string partyName, string notations, string amount, string ledgerName)
        {
            IncomeRepository incomeRepository = new IncomeRepository();
            SearchParamsBo searchParamsBo = new SearchParamsBo
            {
                StartDate = startDate,
                EndDate = endDate,
                PartyName = partyName,
                Notations = notations,
                Amount = amount == "" ? null : amount,
                LedgerName = ledgerName
            };
            List<IncomeBo> data = incomeRepository.SearchReceipts(searchParamsBo);

            DataTable dt = new DataTable
            {
                TableName = "Receipts"// Excel sheet name.
            };
            dt.Columns.Add("Received Date", typeof(string));
            dt.Columns.Add("Party", typeof(string));
            dt.Columns.Add("Ledger", typeof(string));
            dt.Columns.Add("Pay Mode", typeof(string));
            dt.Columns.Add("Bank", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Description", typeof(string));

            foreach (IncomeBo i in data)
            {
                dt.Rows.Add(
                        Convert.ToDateTime(i.InvoiceDate).ToString("dd-MMM-yyyy"),
                        i.PartyName,
                        i.LedgerName,
                        i.PaymentMode.Name,
                        i.Bank.Name,
                        i.ReceivedAmount,
                        i.Remarks);
            }
            return ExportDataTableExcel("Receipts", dt);
        }

        #endregion Receipts

        #region Other Reports

        #region Expense by Supplier

        public ActionResult ExpenseBySupplierExcelExport(string formName, DateTime? startDate, DateTime? endDate)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;

            DataSet ds = otherReportsController.GetSupplierExpenseList(searchParamsBo);
            DataTable dt = ds.Tables[0];

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Expense_by_Supplier", dt);
        }

        #endregion Expense by Supplier

        #region Expense by Ledger

        public ActionResult ExpenseByLedgerExcelExport(string formName, DateTime? startDate, DateTime? endDate)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;

            DataSet ds = otherReportsController.GetExpenseLedgerList(searchParamsBo);
            DataTable dt = ds.Tables[0];

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Expense_by_Ledger_or_Product", dt);
        }

        #endregion Expense by Ledger

        #region Income by Customer

        public ActionResult IncomeByCustomerExcelExport(string formName, DateTime? startDate, DateTime? endDate)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;

            DataSet ds = otherReportsController.GetCustomerList(searchParamsBo);
            DataTable dt = ds.Tables[0];

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Sales_by_Customer", dt);
        }

        #endregion Income by Customer

        #region Income by Product/Services

        public ActionResult IncomeByProductServicesExcelExport(string formName, DateTime? startDate, DateTime? endDate)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;

            DataSet ds = otherReportsController.GetProductServiceList(searchParamsBo);
            DataTable dt = ds.Tables[0];

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Sales_by_Product_or_Service", dt);
        }

        #endregion Income by Product/Services

        #region Aging for Ap

        public ActionResult AgingForApExcelExport(string formName, int yearId)
        {
            OtherReportsController otherReportsController = new OtherReportsController();

            DataSet ds = otherReportsController.GetAgingApReport(yearId);
            DataTable dt = ds.Tables[0];

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Aging_for_Account_Payable", dt);
        }

        #endregion Aging for Ap

        #region Aging for AR

        public ActionResult AgingForArExcelExport(string formName, int yearId)
        {
            OtherReportsController otherReportsController = new OtherReportsController();

            DataSet ds = otherReportsController.GetAgingArRpt(yearId);
            DataTable dt = ds.Tables[0];

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Aging_for_Account_Receivable", dt);
        }

        #endregion Aging for AR

        #region Daybook by Cash

        public ActionResult DayBookByCashExcelExport(string formName, DateTime? startDate, DateTime? endDate)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;

            DataSet ds = otherReportsController.GetDayBookByCashList(searchParamsBo);
            DataTable dt = ds.Tables[0];
            dt.Columns.RemoveAt(0);

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Daybook_by_Cash", dt, false, "RECEIPTS|PAYMENTS");
        }

        #endregion Daybook by Cash

        #region Daybook by Bank

        public ActionResult DayBookByBankExcelExport(string formName, DateTime? startDate, DateTime? endDate, int bankId)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;
            searchParamsBo.Id = bankId;

            DataSet ds = otherReportsController.GetDayBookByBank(searchParamsBo);
            DataTable dt = ds.Tables[0];
            dt.Columns.RemoveAt(0);

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Daybook_by_Bank", dt, false, "RECEIPTS|PAYMENTS");
        }

        #endregion Daybook by Bank

        #region Expense by Cost Center

        public ActionResult ExpenseByCostCenterExcelExport(string formName, DateTime? startDate, DateTime? endDate)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;

            DataSet ds = otherReportsController.GetCostCenterRpt(searchParamsBo);
            DataTable dt = ds.Tables[0];

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Expense_by_Cost_Center", dt);
        }

        #endregion Expense by Cost Center

        #region Income by Revenue Center

        public ActionResult IncomeByRevenueCenterExcelExport(string formName, DateTime? startDate, DateTime? endDate)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;

            DataSet ds = otherReportsController.GetRevenueCenterRpt(searchParamsBo);
            DataTable dt = ds.Tables[0];

            dt.Rows.RemoveAt(dt.Rows.Count - 1);

            return ExportDataTableExcel("Income_by_Revenue_Center", dt);
        }

        #endregion Income by Revenue Center

        #region Cost Vs Revenue Center Analyses

        public ActionResult CostVsRevenueCenterExcelExport(string formName, DateTime? startDate, DateTime? endDate)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;

            DataSet ds = otherReportsController.GetCompareCostCenterRpt(searchParamsBo);
            DataTable data = ds.Tables[0];

            DataTable dt = new DataTable();

            foreach (DataColumn col in data.Columns)
            {
                if (Convert.ToString(col.ColumnName).ToLower() == "totalamount")
                {
                    dt.Columns.Add("Total Amount", typeof(decimal));
                }
                else if (Convert.ToString(col.ColumnName).ToLower() == "receivables")
                {
                    dt.Columns.Add(col.ColumnName, typeof(decimal));
                }
                else if (Convert.ToString(col.ColumnName).ToLower() == "payable")
                {
                    dt.Columns.Add(col.ColumnName, typeof(decimal));
                }
                else
                {
                    dt.Columns.Add(col.ColumnName, typeof(string));
                }
            }
            foreach (DataRow row in data.Rows)
            {
                List<string> newRowList = new List<string>();
                foreach (DataColumn col in data.Columns)
                {
                    if (Convert.ToString(col.ColumnName).ToLower() != "cost center")
                    {
                        if (!string.IsNullOrWhiteSpace(row[col.ColumnName].ToString()))
                        {
                            newRowList.Add(Convert.ToDecimal(row[col.ColumnName]).ToString("#,##0.00"));
                        }
                        else
                        {
                            newRowList.Add(row[col.ColumnName].ToString() == "" ? null : row[col.ColumnName].ToString());
                        }
                    }
                    else
                    {
                        newRowList.Add(row[col.ColumnName].ToString());
                    }
                }
                dt.Rows.Add(newRowList.ToArray());
            }

            return ExportDataTableExcel("Cost_Vs_Revenue_Center_Analyses", dt);
        }

        #endregion Cost Vs Revenue Center Analyses

        #region Loan Summary

        public ActionResult LoanSummaryExcel(string formName)
        {
            ReportRepository reportRepository = new ReportRepository();

            List<ReportsBo> data = reportRepository.SearchLoanDetails();

            DataTable dt = new DataTable();
            dt.Columns.Add("Ledger", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));

            if (data != null && data.Any())
            {
                foreach (ReportsBo result in data)
                {
                    dt.Rows.Add(result.Name,
                        result.Amount.ToString("##,#.00"));
                }
            }

            return ExportDataTableExcel("Loan_Summary", dt);
        }

        #endregion Loan Summary

        #region Loan Details

        public ActionResult LoanDetailsExcel(string formName, int id)
        {
            ReportRepository reportRepository = new ReportRepository();

            List<ReportsBo> data = reportRepository.GetLoanDetails(id);

            DataTable dt = new DataTable();
            dt.Columns.Add("Date", typeof(DateTime));
            dt.Columns.Add("Payment", typeof(decimal));
            dt.Columns.Add("Received", typeof(decimal));
            dt.Columns.Add("Description", typeof(string));

            if (data != null && data.Any())
            {
                foreach (ReportsBo result in data)
                {
                    string payment = result.Type == "Make Payment" ? Convert.ToString(result.Amount, CultureInfo.InvariantCulture) : null;
                    string received = result.Type != "Make Payment" ? Convert.ToString(result.Amount, CultureInfo.InvariantCulture) : null;
                    dt.Rows.Add(Convert.ToDateTime(result.FromDate).ToString("dd-MMM-yyyy"),
                        payment,
                        received,
                        result.BillNo);
                }
                //sb.Append("<tr>");
                //sb.Append("<td style='text-align:right;'><b>Total</b></td>");
                //sb.Append("<td><b>" + Data.Where(a => a.Type.ToUpper() == "MAKE PAYMENT").Sum(a => a.Amount).ToString("##,#.00") + "</b></td>");
                //sb.Append("<td><b>" + Data.Where(a => a.Type.ToUpper() != "MAKE PAYMENT").Sum(a => a.Amount).ToString("##,#.00") + "</b></td>");
                //decimal amount = (Data.Where(a => a.Type.ToUpper() == "MAKE PAYMENT").Sum(a => a.Amount)) - (Data.Where(a => a.Type.ToUpper() != "MAKE PAYMENT").Sum(a => a.Amount));
                //sb.Append("<td><b>" + amount.ToString("##,#.00") + "</b></td>");
            }
            return ExportDataTableExcel("Loan_Details", dt);
        }

        #endregion Loan Details

        #region Ledger Statement Report

        public ActionResult LedgerStatementExcelExport(string formName, DateTime? startDate, DateTime? endDate, string ledgerName)
        {
            string reportName;

            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;
            searchParamsBo.LedgerName = ledgerName;
            searchParamsBo.VendorName = ledgerName;

            DataSet ds;
            if (formName == "VendorStatement")
            {
                ds = otherReportsController.GetDayVendorStatement(searchParamsBo);
                reportName = "Vendor_Statement_Report";
            }
            else if (formName == "CusromerStatement")
            {
                ds = otherReportsController.GetDayCustomerStatement(searchParamsBo);
                reportName = "Customer_Statement_Report";
            }
            else
            {
                reportName = "Ledger_Statement_Report";
                ds = otherReportsController.GetDayBookByLedger(searchParamsBo);
            }
            DataTable dt = ds.Tables[1];
            dt.Columns.RemoveAt(0);

            return ExportDataTableExcel(reportName, dt);
        }

        #endregion Ledger Statement Report

        #region BankDetails

        public ActionResult BankDetails(SearchParamsBo searchParamsBo)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("[Rpt_BankDetails]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue(DBParam.Input.StartDate, searchParamsBo.StartDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.EndDate, searchParamsBo.EndDate);
                    cmd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                    con.Open();

                    using (SqlDataAdapter dr = new SqlDataAdapter(cmd))
                    {
                        dr.Fill(ds);
                    }
                }
            }
            DataTable dt = ds.Tables[0];
            ds.Tables.Remove(dt);
            int c = 0;
            foreach (DataTable table in ds.Tables)
            {
                ds.Tables[c].TableName = dt.Rows[c].ItemArray[1].ToString();
                c = c + 1;
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(ds);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Utility.GetSession("CompanyName")
                        + "_" + "Export_Data" + ".xlsx");
                }
            }
        }

        #endregion BankDetails

        #region Open Invoice 

        public ActionResult OpenInvoiceExcelExport(string formName, DateTime? startDate, DateTime? endDate, string customerName)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;
            searchParamsBo.CustomerName = customerName;

            DataSet ds = otherReportsController.GetopenInvoiceList(searchParamsBo);
            DataTable dt = ds.Tables[0];

            return ExportDataTableExcel("Open_Invoice", dt);
        }

        public ActionResult tdsExcelExport(string formName, DateTime? startDate, DateTime? endDate, string customerName)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;
            searchParamsBo.CustomerName = customerName;

            DataSet ds = otherReportsController.GetTDSList(searchParamsBo);
            DataTable dt = ds.Tables[0];

            return ExportDataTableExcel("Expense_Report", dt);
        }

        public ActionResult CompleteSalesReportExcelExport(string formName, DateTime? startDate, DateTime? endDate)
        {
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            OtherReportsController otherReportsController = new OtherReportsController();

            searchParamsBo.StartDate = startDate;
            searchParamsBo.EndDate = endDate;

            DataSet ds = otherReportsController.GetCompleteSalesRpt(searchParamsBo);
            DataTable dt = ds.Tables[0];

            return ExportDataTableExcel("Sales_Report", dt);
        }

        #endregion Open Invoice 

        #endregion Other Reports

        #region Late Arrival

        public ActionResult LateArrivalExcel(string formName, int businessUnitId, int employeeId, int monthId, int yearId, string status, int minutes)
        {
            LeaveReportBo leaveReportBo = new LeaveReportBo();
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            leaveReportBo.BusinessUnitId = businessUnitId;
            leaveReportBo.EmployeeId = employeeId;
            leaveReportBo.MonthId = monthId;
            leaveReportBo.YearId = yearId;
            leaveReportBo.Status = status;
            leaveReportBo.Minutes = minutes;

            List<LeaveReportBo> data = leaveManagementRepository.LateArrivalReportList(leaveReportBo);

            DataTable dt = new DataTable();
            dt.Columns.Add("Emp Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Date", typeof(DateTime));
            dt.Columns.Add("Minutes", typeof(string));
            dt.Columns.Add("Hours", typeof(string));
            dt.Columns.Add("Punches", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));

            if (data != null && data.Any())
            {
                string oldEmployeeName = "";
                foreach (LeaveReportBo result in data)
                {
                    string employeeName = result.EmployeeName;

                    DataRow newRow = dt.NewRow();
                    if (employeeName != oldEmployeeName)
                    {
                        newRow["Emp Code"] = result.EmployeeCode;
                        newRow["Employee Name"] = employeeName;
                    }
                    else
                    {
                        newRow["Emp Code"] = "";
                        newRow["Employee Name"] = "";
                    }
                    newRow["Date"] = Convert.ToDateTime(result.Date).ToString("dd MMM yyy");
                    newRow["Minutes"] = result.Minutes;
                    newRow["Hours"] = result.Hours;
                    newRow["Punches"] = result.UserMessage;
                    newRow["Status"] = result.Status;
                    newRow["Business Unit"] = result.BusinessUnit;
                    if (employeeName != oldEmployeeName)
                    {
                        oldEmployeeName = employeeName;
                    }
                    dt.Rows.Add(newRow);
                }
            }
            return ExportDataTableExcel("Late_Arrival_report", dt);
        }

        #endregion Late Arrival

        #region Early Departure

        public ActionResult EarlyDepartureExcel(string formName, int businessUnitId, int employeeId, int monthId, int yearId, string status, int minutes)
        {
            LeaveReportBo leaveReportBo = new LeaveReportBo();
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            leaveReportBo.BusinessUnitId = businessUnitId;
            leaveReportBo.EmployeeId = employeeId;
            leaveReportBo.MonthId = monthId;
            leaveReportBo.YearId = yearId;
            leaveReportBo.Status = status;
            leaveReportBo.Minutes = minutes;

            List<LeaveReportBo> data = leaveManagementRepository.EarlyDepartureReportList(leaveReportBo);

            DataTable dt = new DataTable();
            dt.Columns.Add("Emp Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Date", typeof(DateTime));
            dt.Columns.Add("Minutes", typeof(string));
            dt.Columns.Add("Hours", typeof(string));
            dt.Columns.Add("Punches", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));

            if (data != null && data.Any())
            {
                string oldEmployeeName = "";
                foreach (LeaveReportBo result in data)
                {
                    string employeeName = result.EmployeeName;

                    DataRow newRow = dt.NewRow();
                    if (employeeName != oldEmployeeName)
                    {
                        newRow["Emp Code"] = result.EmployeeCode;
                        newRow["Employee Name"] = employeeName;
                    }
                    else
                    {
                        newRow["Emp Code"] = "";
                        newRow["Employee Name"] = "";
                    }
                    newRow["Date"] = Convert.ToDateTime(result.Date).ToString("dd MMM yyy");
                    newRow["Minutes"] = result.Minutes;
                    newRow["Hours"] = result.Hours;
                    newRow["Punches"] = result.UserMessage;
                    newRow["Status"] = result.Status;
                    newRow["Business Unit"] = result.BusinessUnit;
                    if (employeeName != oldEmployeeName)
                    {
                        oldEmployeeName = employeeName;
                    }
                    dt.Rows.Add(newRow);
                }
            }
            return ExportDataTableExcel("Early_Departure_report", dt);
        }

        #endregion Early Departure

        #region GST Input Tax Report

        public ActionResult GstInputTaxExcelExport(int yearId, string reportType, bool isIncludeZero)
        {
            OtherReportsController otherReportsController = new OtherReportsController();

            DataSet ds = otherReportsController.GstInputTaxReportList(yearId, reportType, isIncludeZero);
            DataTable data = ds.Tables[0];

            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    if (col.ColumnName.ToLower() != "id")
                    {
                        switch (Convert.ToString(col.ColumnName))
                        {
                            case "Vendor":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            case "Type":
                            case "GST No":
                            case "Bill No":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            case "CGST %":
                            case "SGST %":
                            case "IGST %":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            case "HSN Code":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            default:
                                dt.Columns.Add(col.ColumnName, typeof(decimal));
                                break;
                        }
                    }
                }
                string vendor = string.Empty;
                if (data.Rows.Count > 0)
                {
                    string oldVendor;
                    data.Rows.RemoveAt(data.Rows.Count - 1);
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            if (col.ColumnName.ToLower() != "id")
                            {
                                switch (Convert.ToString(col.ColumnName))
                                {
                                    case "Vendor":
                                        oldVendor = row[col.ColumnName].ToString();
                                        if (vendor != oldVendor)
                                        {
                                            vendor = row[col.ColumnName].ToString();
                                            newRowList.Add(vendor);
                                        }
                                        else
                                        {
                                            oldVendor = "";
                                            newRowList.Add(oldVendor);
                                        }
                                        if (vendor != oldVendor)
                                        {
                                            oldVendor = vendor;
                                        }
                                        break;

                                    case "Type":
                                    case "GST No":
                                    case "Bill No":
                                        newRowList.Add(row[col.ColumnName].ToString());
                                        break;

                                    case "CGST %":
                                    case "SGST %":
                                    case "IGST %":
                                        newRowList.Add(row[col.ColumnName].ToString());
                                        break;

                                    case "HSN Code":
                                        newRowList.Add(row[col.ColumnName].ToString());
                                        break;

                                    default:
                                        newRowList.Add(Convert.ToDecimal(row[col.ColumnName]).ToString("#,##0.00"));
                                        break;
                                }
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }

            return ExportDataTableExcel("GST_Input_Tax_Report", dt);
        }

        #endregion GST Input Tax Report

        #region GST Output Tax Report

        public ActionResult GstOutputTaxExcelExport(int yearId, string reportType, bool isIncludeZero)
        {
            OtherReportsController otherReportsController = new OtherReportsController();

            DataSet ds = otherReportsController.GstOutputTaxReportList(yearId, reportType, isIncludeZero);
            DataTable data = ds.Tables[0];

            DataTable dt = new DataTable();

            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    if (col.ColumnName.ToLower() != "id")
                    {
                        switch (Convert.ToString(col.ColumnName))
                        {
                            case "Customer":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            case "Type":
                            case "GST No":
                            case "Invoice No":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            case "CGST %":
                            case "SGST %":
                            case "IGST %":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            case "HSN Code":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            default:
                                dt.Columns.Add(col.ColumnName, typeof(decimal));
                                break;
                        }
                    }
                }
                string customer = string.Empty;
                if (data.Rows.Count > 0)
                {
                    string oldVendor;
                    data.Rows.RemoveAt(data.Rows.Count - 1);
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            if (Convert.ToString(col.ColumnName).ToLower() != "id")
                            {
                                switch (Convert.ToString(col.ColumnName))
                                {
                                    case "Customer":
                                        oldVendor = row[col.ColumnName].ToString();
                                        if (customer != oldVendor)
                                        {
                                            customer = row[col.ColumnName].ToString();
                                            newRowList.Add(customer);
                                        }
                                        else
                                        {
                                            oldVendor = "";
                                            newRowList.Add(oldVendor);
                                        }
                                        if (customer != oldVendor)
                                        {
                                            oldVendor = customer;
                                        }
                                        break;

                                    case "Type":
                                    case "GST No":
                                    case "Invoice No":
                                        newRowList.Add(row[col.ColumnName].ToString());
                                        break;

                                    case "CGST %":
                                    case "SGST %":
                                    case "IGST %":
                                        newRowList.Add(row[col.ColumnName].ToString());
                                        break;

                                    case "HSN Code":
                                        newRowList.Add(row[col.ColumnName].ToString());
                                        break;

                                    default:
                                        newRowList.Add(Convert.ToDecimal(row[col.ColumnName]).ToString("#,##0.00"));
                                        break;
                                }
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }

            return ExportDataTableExcel("GST_Output_Tax_Report", dt);
        }

        #endregion GST Output Tax Report

        #region GST Summary

        public ActionResult GstSummaryExcelExport(string formName, int yearId, string reportType)
        {
            ReportsBo reportsBo = new ReportsBo();
            ReportRepository reportRepository = new ReportRepository();

            reportsBo.FinancialYearId = yearId;
            reportsBo.Type = reportType;

            List<GstReportBo> data = reportRepository.GstSummaryReportList(yearId, reportType);

            DataTable dt = new DataTable();

            dt.Columns.Add("GST", typeof(string));
            dt.Columns.Add("GST Output Tax", typeof(decimal));
            dt.Columns.Add("GST Input Tax", typeof(decimal));

            if (data != null && data.Any())
            {
                foreach (IGrouping<string, GstReportBo> result in data.GroupBy(n => n.Name))
                {
                    DataRow newRow = dt.NewRow();
                    newRow["GST"] = result.Key;
                    foreach (GstReportBo group in result)
                    {
                        if (group.Type.ToUpper() == "OUTPUT")
                        {
                            newRow["GST Output Tax"] = group.Total.ToString("#,##0.00");
                        }
                        if (group.Type.ToUpper() == "INPUT")
                        {
                            newRow["GST Input Tax"] = group.Total.ToString("#,##0.00");
                        }
                    }
                    dt.Rows.Add(newRow);
                }
            }
            dt.Rows.RemoveAt(dt.Rows.Count - 1);
            return ExportDataTableExcel("GST_Summary_Report", dt);
        }

        #endregion GST Summary

        #region Financial Report

        public ActionResult FinancialReportExcelExport(int yearId, string reportType)
        {
            OtherReportsController otherReportsController = new OtherReportsController();

            DataSet ds = otherReportsController.GetFyReports(yearId, reportType);
            DataTable data = ds.Tables[0];

            DataTable dt = new DataTable();

            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    if (col.ColumnName.ToLower() != "sortid")
                    {
                        switch (Convert.ToString(col.ColumnName))
                        {
                            case "Name":
                                dt.Columns.Add("Particulars", typeof(string));
                                break;

                            case "NET Profit":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            case "GroupType":
                                dt.Columns.Add("Group Type", typeof(string));
                                break;

                            case "Total":
                                dt.Columns.Add(col.ColumnName, typeof(decimal));
                                break;

                            default:
                                dt.Columns.Add(col.ColumnName.Substring(0, 3), typeof(decimal));
                                break;
                        }
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        switch (row[1].ToString())
                        {
                            case "Total Amount":
                            case "Gross Profit":
                            case "NET Profit":
                                break;

                            default:
                                List<string> newRowList = new List<string>();
                                foreach (DataColumn col in data.Columns)
                                {
                                    if (Convert.ToString(col.ColumnName).ToLower() != "sortid")
                                    {
                                        switch (Convert.ToString(col.ColumnName))
                                        {
                                            case "Name":
                                            case "GroupType":
                                                newRowList.Add(row[col.ColumnName].ToString());
                                                break;

                                            default:
                                                newRowList.Add(Convert.ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName])).ToString("#,##0.00"));
                                                break;
                                        }
                                    }
                                }
                                dt.Rows.Add(newRowList.ToArray());
                                break;
                        }
                    }
                }
            }

            return ExportDataTableExcel("Financial_Report", dt);
        }

        #endregion Financial Report

        #region Budgeting Report

        public ActionResult BudgetingReportExcelExport(int yearId, string type, string reportType)
        {
            OtherReportsController otherReportsController = new OtherReportsController();

            DataTable data = otherReportsController.GetIncomeBudgeting(yearId, reportType);
            DataTable expenseBudgeting = otherReportsController.GetExpenseBudgeting(yearId, type);

            data.Merge(expenseBudgeting);

            DataTable dt = new DataTable();

            if (data.Rows.Count > 0)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName))
                    {
                        case "Particulars":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        case "Percent":
                            dt.Columns.Add("Ratio", typeof(string));
                            break;

                        case "Planned":
                        case "Actual":
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName.Substring(0, 3), typeof(decimal));
                            break;
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName))
                            {
                                case "Particulars":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                case "Percent":
                                    newRowList.Add(Utility.CheckDbNull<decimal>(row[col.ColumnName]).ToString("#,##0.00") + " %");
                                    break;

                                default:
                                    newRowList.Add(Convert.ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName])).ToString("#,##0.00"));
                                    break;
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }

            return ExportDataTableExcel("Budgeting_Report", dt);
        }

        #endregion Budgeting Report

        #region Employee Complete Report

        public ActionResult EmployeeCompleteRptExcelExport(string formName, int departmentId, int designationId, int businessUnitId, bool isActive, string ColName)
        {
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                BusinessUnitId = businessUnitId,
                DepartmentId = departmentId,
                DesignationId = designationId,
                IsActiveRecords = isActive,
                ColName = ColName

            };
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();


            DataTable data = employeeMasterRepository.SearchEmployeeCompleteReport(reportInputParamBo);

            return ExportDataTableExcel("Employee_Complete_report", data);
        }

        #endregion Employee Complete Report

        #region EmployeeBilling && Generate Invoice

        public ActionResult EmployeeBillingExcelExport(string formName, string employeeName, string customerName, bool isActive)
        {
            EmployeeBillingBO employeeBillingBO = new EmployeeBillingBO
            {
                EmployeeName = employeeName,
                CustomerName = customerName,
                Active = isActive
            };
            EmployeeBillingRepository employeeBillingRepository = new EmployeeBillingRepository();
            List<EmployeeBillingBO> data = employeeBillingRepository.SearchEmployee(employeeName, customerName, isActive);

            DataTable dt = new DataTable();
            dt.Columns.Add("Employee", typeof(string));
            dt.Columns.Add("Emp Code", typeof(string));
            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("Currency", typeof(string));
            dt.Columns.Add("Hourly Rate", typeof(decimal));
            dt.Columns.Add("Fixed Rate", typeof(decimal));
            dt.Columns.Add("Billing Utilization", typeof(decimal));
            dt.Columns.Add("Remuneration Utilization", typeof(decimal));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Status", typeof(string));

            if (data != null && data.Any())
            {
                foreach (EmployeeBillingBO result in data)
                {
                    dt.Rows.Add(
                        result.EmployeeName,
                        result.EmployeeCode,
                        result.CustomerName,
                        result.CurrencyCode,
                        result.HourlyRate,
                        result.FixedRate,
                        result.BillingPercentage,
                        result.RemunerationPercentage,
                        result.Description,
                        result.Active ? "Active" : "InActive");
                }
            }

            return ExportDataTableExcel("Employee_Billing", dt);
        }

        public ActionResult EmployeeInvoiceBilling(string formName, int? monthId, int? year, string CustomerName)
        {
            EmployeeInvoiceRepository employeeInvoiceRepository = new EmployeeInvoiceRepository();
            List<EmployeeInvoice> data = employeeInvoiceRepository.SearchEmployeeBillingInvoice(monthId, year, CustomerName);

            DataTable dt = new DataTable();

            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("Month", typeof(string));
            dt.Columns.Add("year", typeof(int));
            dt.Columns.Add("Amount Currency", typeof(string));
            dt.Columns.Add("Amount", typeof(decimal));
            dt.Columns.Add("Hours", typeof(int));
            dt.Columns.Add("Resources", typeof(int));
            dt.Columns.Add("Total (INR)", typeof(decimal));
            dt.Columns.Add("CTC (INR)", typeof(decimal));

            if (data != null && data.Any())
            {
                foreach (EmployeeInvoice result in data)
                {
                    dt.Rows.Add(result.CustomerName,
                        result.MonthName,
                        result.Year,
                        result.CurrencyCode,
                        result.Total,
                        result.Hours,
                        result.Resources,
                        result.Total * result.CurrencyRate,
                        result.Remuneration);
                }
            }

            return ExportDataTableExcel("Employee_Generate_Invoice", dt);
        }

        public ActionResult EmployeeBillingReport(string formName, DateTime? fromDate, DateTime? todate, string customerName, string employeeName, bool? Active)
        {
            EmployeeInvoiceRepository employeeInvoiceRepository = new EmployeeInvoiceRepository();
            List<EmployeeInvoice> data = employeeInvoiceRepository.SearchEmployeeBillingInvoiceReport(fromDate, todate, customerName, employeeName, Active);

            DataTable dt = new DataTable();

            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("Employee", typeof(string));
            dt.Columns.Add("Month", typeof(string));
            dt.Columns.Add("Year", typeof(int));
            dt.Columns.Add("Billed Currency", typeof(string));
            dt.Columns.Add("Billed Amount", typeof(decimal));
            dt.Columns.Add("Billed (INR)", typeof(decimal));
            dt.Columns.Add("Billed Utilization (INR)", typeof(decimal));
            dt.Columns.Add("CTC (INR)", typeof(decimal));
            dt.Columns.Add("CTC Utilization (INR)", typeof(decimal));

            if (data != null && data.Any())
            {
                foreach (EmployeeInvoice result in data)
                {
                    dt.Rows.Add(result.CustomerName,
                        result.EmployeeName,
                        result.MonthName,
                        result.Year,
                        result.CurrencyCode,
                        result.Total,
                        result.Total * result.CurrencyRate,
                         result.TotalBillingOnCalc * result.CurrencyRate,
                        result.TotalRemunerationCalc,
                        result.Expense);
                }
            }

            return ExportDataTableExcel("Employee_Billing_Report", dt);
        }
        #endregion

        #region Employee Dependent Report

        public ActionResult EmployeeDependentRptExcelExport(string formName, int departmentId, int designationId, int businessUnitId, bool isActive)
        {
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo();
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            reportInputParamBo.BusinessUnitId = businessUnitId;
            reportInputParamBo.DepartmentId = departmentId;
            reportInputParamBo.DesignationId = designationId;
            reportInputParamBo.IsActiveRecords = isActive;

            DataTable data = employeeMasterRepository.SearchEmployeeDependentListReport(reportInputParamBo);
            DataTable dt = new DataTable();

            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    dt.Columns.Add(col.ColumnName, typeof(string));
                }
                string employeeName = string.Empty;
                string employeeCode = string.Empty;
                string status = string.Empty;
                string businessUnit = string.Empty;

                if (data.Rows.Count > 0)
                {
                    string oldEmployee, oldEmployeeCode, oldStatus, oldBusinessunit;
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        if (employeeCode != row.ItemArray[0].ToString())
                        {
                            employeeName = "";
                            employeeCode = "";
                            status = "";
                            businessUnit = "";
                        }
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName))
                            {
                                case "Employee Name":
                                    oldEmployee = row[col.ColumnName].ToString();
                                    if (employeeName != oldEmployee)
                                    {
                                        employeeName = row[col.ColumnName].ToString();
                                        newRowList.Add(employeeName);
                                    }
                                    else
                                    {
                                        oldEmployee = "";
                                        newRowList.Add(oldEmployee);
                                    }
                                    break;

                                case "Employee Code":
                                    oldEmployeeCode = row[col.ColumnName].ToString();
                                    if (employeeCode != oldEmployeeCode)
                                    {
                                        employeeCode = row[col.ColumnName].ToString();
                                        newRowList.Add(employeeCode);
                                    }
                                    else
                                    {
                                        oldEmployeeCode = "";
                                        newRowList.Add(oldEmployeeCode);
                                    }
                                    break;

                                case "Status":
                                    oldStatus = row[col.ColumnName].ToString();
                                    if (status != oldStatus)
                                    {
                                        status = row[col.ColumnName].ToString();
                                        newRowList.Add(status);
                                    }
                                    else
                                    {
                                        oldStatus = "";
                                        newRowList.Add(oldStatus);
                                    }
                                    break;

                                case "Business Unit":
                                    oldBusinessunit = row[col.ColumnName].ToString();
                                    if (businessUnit != oldBusinessunit)
                                    {
                                        businessUnit = row[col.ColumnName].ToString();
                                        newRowList.Add(businessUnit);
                                    }
                                    else
                                    {
                                        oldBusinessunit = "";
                                        newRowList.Add(oldBusinessunit);
                                    }
                                    break;

                                case "Date of Birth":
                                    if (!string.IsNullOrWhiteSpace(row[col.ColumnName].ToString()))
                                    {
                                        newRowList.Add(Convert.ToDateTime(row[col.ColumnName]).ToString("dd-MMM-yyyy"));
                                    }
                                    else
                                    {
                                        newRowList.Add(row[col.ColumnName].ToString());
                                    }
                                    break;

                                default:
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Employee_Dependent_report", dt);
        }

        #endregion Employee Dependent Report

        #region Employee Asset Report

        public ActionResult EmployeeAssetRptExcelExport(string formName, int departmentId, int designationId, int businessUnitId, bool isActive)
        {
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo();
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            reportInputParamBo.BusinessUnitId = businessUnitId;
            reportInputParamBo.DepartmentId = departmentId;
            reportInputParamBo.DesignationId = designationId;
            reportInputParamBo.IsActiveRecords = isActive;

            DataTable data = employeeMasterRepository.SearchEmployeeAssetReport(reportInputParamBo);
            DataTable dt = new DataTable();

            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    dt.Columns.Add(col.ColumnName, typeof(string));
                }
                string employeeName = string.Empty;
                string employeeCode = string.Empty;
                if (data.Rows.Count > 0)
                {
                    string oldEmployeeCode, oldEmployee;
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName))
                            {
                                case "Employee Name":
                                    oldEmployee = row[col.ColumnName].ToString();
                                    if (employeeName != oldEmployee)
                                    {
                                        employeeName = row[col.ColumnName].ToString();
                                        newRowList.Add(employeeName);
                                    }
                                    else
                                    {
                                        oldEmployee = "";
                                        newRowList.Add(oldEmployee);
                                    }
                                    if (employeeName != oldEmployee)
                                    {
                                        oldEmployee = employeeName;
                                    }
                                    break;

                                case "Employee Code":
                                    oldEmployeeCode = row[col.ColumnName].ToString();
                                    if (employeeCode != oldEmployeeCode)
                                    {
                                        employeeCode = row[col.ColumnName].ToString();
                                        newRowList.Add(employeeCode);
                                    }
                                    else
                                    {
                                        oldEmployeeCode = "";
                                        newRowList.Add(oldEmployeeCode);
                                    }
                                    if (employeeCode != oldEmployeeCode)
                                    {
                                        oldEmployeeCode = employeeCode;
                                    }
                                    break;

                                case "Given On":
                                case "Returned On":
                                    if (!string.IsNullOrWhiteSpace(row[col.ColumnName].ToString()))
                                    {
                                        newRowList.Add(Convert.ToDateTime(row[col.ColumnName]).ToString("dd-MMM-yyyy"));
                                    }
                                    else
                                    {
                                        newRowList.Add(row[col.ColumnName].ToString());
                                    }
                                    break;

                                default:
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }

            return ExportDataTableExcel("Employee_Asset_report", dt);
        }

        #endregion Employee Asset Report

        #region Employee New Hires Report

        public ActionResult NewHiresRptExcelExport(string formName, int departmentId, int designationId, int businessUnitId)
        {
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo();
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            reportInputParamBo.BusinessUnitId = businessUnitId;
            reportInputParamBo.DepartmentId = departmentId;
            reportInputParamBo.DesignationId = designationId;

            DataTable data = employeeMasterRepository.SearchEmployeeNewHiresReport(reportInputParamBo);

            return ExportDataTableExcel("New_Hires_report", data);
        }

        #endregion Employee New Hires Report

        #region Employee Asset Defaulters Report

        public ActionResult EmployeeAssetDefaultersExcelExport(string formName, int departmentId, int designationId, int businessUnitId)
        {
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo();
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            reportInputParamBo.BusinessUnitId = businessUnitId;
            reportInputParamBo.DepartmentId = departmentId;
            reportInputParamBo.DesignationId = designationId;

            DataTable data = employeeMasterRepository.SearchEmployeeDefaultersReport(reportInputParamBo);
            DataTable dt = new DataTable();

            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    dt.Columns.Add(col.ColumnName, typeof(string));
                }
                string employeeName = string.Empty;
                string employeeCode = string.Empty;
                if (data.Rows.Count > 0)
                {
                    string oldEmployee, oldEmployeeCode;
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName))
                            {
                                case "Employee Name":
                                    oldEmployee = row[col.ColumnName].ToString();
                                    if (employeeName != oldEmployee)
                                    {
                                        employeeName = row[col.ColumnName].ToString();
                                        newRowList.Add(employeeName);
                                    }
                                    else
                                    {
                                        oldEmployee = "";
                                        newRowList.Add(oldEmployee);
                                    }
                                    if (employeeName != oldEmployee)
                                    {
                                        oldEmployee = employeeName;
                                    }
                                    break;

                                case "Employee Code":
                                    oldEmployeeCode = row[col.ColumnName].ToString();
                                    if (employeeCode != oldEmployeeCode)
                                    {
                                        employeeCode = row[col.ColumnName].ToString();
                                        newRowList.Add(employeeCode);
                                    }
                                    else
                                    {
                                        oldEmployeeCode = "";
                                        newRowList.Add(oldEmployeeCode);
                                    }
                                    if (employeeCode != oldEmployeeCode)
                                    {
                                        oldEmployeeCode = employeeCode;
                                    }
                                    break;

                                case "Given On":
                                case "Returned On":
                                    if (!string.IsNullOrWhiteSpace(row[col.ColumnName].ToString()))
                                    {
                                        newRowList.Add(Convert.ToDateTime(row[col.ColumnName]).ToString("dd-MMM-yyyy"));
                                    }
                                    else
                                    {
                                        newRowList.Add(row[col.ColumnName].ToString());
                                    }
                                    break;

                                default:
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Employee_Asset_defaulters", dt);
        }

        #endregion Employee Asset Defaulters Report

        #region Employee Skill Set Report

        public ActionResult EmployeeSkillSetExport(string formName, int skillsId, int designationId, int businessUnitId, bool isActive)
        {
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo();
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            reportInputParamBo.BusinessUnitId = businessUnitId;
            reportInputParamBo.SkillsId = skillsId;
            reportInputParamBo.DesignationId = designationId;
            reportInputParamBo.IsActiveRecords = isActive;

            DataTable data = employeeMasterRepository.SearchEmployeeSkillSetReport(reportInputParamBo);
            DataTable dt = new DataTable();

            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    dt.Columns.Add(col.ColumnName, typeof(string));
                }
                string employeeName = string.Empty;
                string employeeCode = string.Empty;
                //string status = string.Empty;
                if (data.Rows.Count > 0)
                {
                    string oldEmployee, oldEmployeeCode;
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            // string oldstatus = string.Empty;
                            switch (Convert.ToString(col.ColumnName))
                            {
                                case "Employee Name":
                                    oldEmployee = row[col.ColumnName].ToString();
                                    if (employeeName != oldEmployee)
                                    {
                                        employeeName = row[col.ColumnName].ToString();
                                        newRowList.Add(employeeName);
                                    }
                                    else
                                    {
                                        oldEmployee = "";
                                        newRowList.Add(oldEmployee);
                                    }
                                    if (employeeName != oldEmployee)
                                    {
                                        oldEmployee = employeeName;
                                    }
                                    break;

                                case "Employee Code":
                                    oldEmployeeCode = row[col.ColumnName].ToString();
                                    if (employeeCode != oldEmployeeCode)
                                    {
                                        employeeCode = row[col.ColumnName].ToString();
                                        newRowList.Add(employeeCode);
                                    }
                                    else
                                    {
                                        oldEmployeeCode = "";
                                        newRowList.Add(oldEmployeeCode);
                                    }
                                    if (employeeCode != oldEmployeeCode)
                                    {
                                        oldEmployeeCode = employeeCode;
                                    }
                                    break;

                                //case "Status":
                                //    oldstatus = row[col.ColumnName].ToString();
                                //    if (status != oldstatus)
                                //    {
                                //        status = row[col.ColumnName].ToString();
                                //        newRowList.Add(status);
                                //    }
                                //    else
                                //    {
                                //        oldstatus = "";
                                //        newRowList.Add(oldstatus);
                                //    }
                                //    if (status != oldstatus)
                                //    {
                                //        oldstatus = status;
                                //    }
                                //    break;

                                default:
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Employee_Skill_set_report", dt);
        }

        #endregion Employee Skill Set Report

        #region Employee Skill Matrix Report

        public ActionResult EmployeeSkillMatrixExcel(string formName, int skillsId, int businessUnitId, bool isActive)
        {
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo();
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            reportInputParamBo.BusinessUnitId = businessUnitId;
            reportInputParamBo.SkillsId = skillsId;
            reportInputParamBo.IsActiveRecords = isActive;

            List<ReportInputParamBo> data = employeeMasterRepository.SearchEmployeeSkillMatrix(reportInputParamBo);

            DataTable dt = new DataTable();

            dt.Columns.Add("Skills", typeof(string));
            dt.Columns.Add("Beginner", typeof(int));
            dt.Columns.Add("Intermediate", typeof(int));
            dt.Columns.Add("Expert", typeof(int));

            if (data != null && data.Any())
            {
                foreach (ReportInputParamBo result in data)
                {
                    dt.Rows.Add(result.Skills,
                        result.BeginnerCount,
                        result.InterMediateCount,
                        result.ExpertCount);
                }
            }

            return ExportDataTableExcel("Employee_Skill_Matrix", dt);
        }

        #endregion Employee Skill Matrix Report

        #region Travel

        public ActionResult TravelExcelExport(string formName, int statusId, int travelTypeId, int employeeId)
        {
            TravelBO travelBo = new TravelBO();
            TravelRepository travelRepository = new TravelRepository();
            travelBo.StatusId = statusId;
            travelBo.TravelTypeId = travelTypeId;
            travelBo.ApproverId = Utility.UserId();
            if (employeeId == 0)
            {
                travelBo.EmployeeId = Utility.UserId();
            }
            else
            {
                travelBo.EmployeeId = employeeId;
            }
            DataTable dt = new DataTable();

            if (formName == "TravelRequest")
            {
                List<TravelBO> data = travelRepository.SearchTravelRequest(travelBo);

                dt.Columns.Add("Travel Request No", typeof(string));
                dt.Columns.Add("Travel Date", typeof(string));
                dt.Columns.Add("Return Date", typeof(string));
                dt.Columns.Add("Travel From", typeof(string));
                dt.Columns.Add("Travel To", typeof(string));
                dt.Columns.Add("Subordinates", typeof(string));
                dt.Columns.Add("Status", typeof(string));

                if (data != null && data.Any())
                {
                    foreach (TravelBO result in data)
                    {
                        dt.Rows.Add(result.TravelReqNo,
                                result.TravelDate.ToString("dd-MMM-yy"),
                                result.ReturnDate == null ? "" : Convert.ToDateTime(result.ReturnDate).ToString("dd-MMM-yy"),
                                result.TravelFrom,
                                result.TravelTo,
                                result.NoOfSubordinates == 0 ? "" : result.NoOfSubordinates.ToString(),
                                result.Status);
                    }
                }

                return ExportDataTableExcel("Travel_Request", dt);
            }
            else
            {
                List<TravelBO> data = travelRepository.SearchTravelRequestApproval(travelBo);

                dt.Columns.Add("Requester", typeof(string));
                dt.Columns.Add("Request No", typeof(string));
                dt.Columns.Add("Travel Date", typeof(string));
                dt.Columns.Add("Return Date", typeof(string));
                dt.Columns.Add("Travel From", typeof(string));
                dt.Columns.Add("Travel To", typeof(string));
                dt.Columns.Add("Status", typeof(string));
                dt.Columns.Add("No of Days", typeof(int));
                dt.Columns.Add("Subordinates", typeof(string));

                if (data != null && data.Any())
                {
                    foreach (TravelBO result in data)
                    {
                        dt.Rows.Add(result.EmployeeName,
                                result.TravelReqNo,
                                result.TravelDate.ToString("dd-MMM-yy"),
                                result.ReturnDate == null ? "" : Convert.ToDateTime(result.ReturnDate).ToString("dd-MMM-yy"),
                                result.TravelFrom,
                                result.TravelTo,
                                result.Status,
                                result.NoOfDays,
                                result.NoOfSubordinates == 0 ? "" : result.NoOfSubordinates.ToString());
                    }
                }

                return ExportDataTableExcel("Travel_Approval", dt);
            }
        }

        #endregion Travel

        #region Comp off Report

        public ActionResult CompOffReportExcelExport(string formName, int monthId, int yearId, int businessUnitId, int statusId)
        {
            LeaveManagementRepository leaveManagementRepository = new LeaveManagementRepository();
            MissedPunchesBo missedPunchesBo = new MissedPunchesBo
            {
                MonthId = monthId,
                Year = yearId,
                BusinessUnitId = businessUnitId,
                StatusId = statusId
            };

            DataTable data = leaveManagementRepository.SearchEmployeeCompOffReport(missedPunchesBo);
            DataTable dt = new DataTable();

            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    dt.Columns.Add(col.ColumnName, typeof(string));
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName).ToLower())
                            {
                                case "comp off date":
                                    if (!string.IsNullOrWhiteSpace(row[col.ColumnName].ToString()))
                                    {
                                        newRowList.Add(Convert.ToDateTime(row[col.ColumnName]).ToString("dd-MMM-yyyy"));
                                    }
                                    else
                                    {
                                        newRowList.Add(row[col.ColumnName].ToString());
                                    }
                                    break;

                                case "duration":
                                    if (!string.IsNullOrWhiteSpace(row[col.ColumnName].ToString()))
                                    {
                                        newRowList.Add(Convert.ToString(row[col.ColumnName]).Substring(0, 5));
                                    }
                                    else
                                    {
                                        newRowList.Add(row[col.ColumnName].ToString() == "" ? "0:00" : row[col.ColumnName].ToString());
                                    }
                                    break;

                                default:
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Comp_off_report", dt);
        }

        #endregion Comp off Report

        #region Travel Details Report

        public ActionResult EmployeeTravelDetailsExcelExport(string formName, int departmentId, int businessUnitId, int employeeId, int statusId)
        {
            EmployeeMasterRepository employeeMasterRepository = new EmployeeMasterRepository();
            ReportInputParamBo reportInputParamBo = new ReportInputParamBo
            {
                EmployeeId = employeeId,
                BusinessUnitId = businessUnitId,
                DepartmentId = departmentId,
                StatusId = statusId
            };

            DataTable data = employeeMasterRepository.TravelDetailsReportList(reportInputParamBo);
            DataTable dt = new DataTable();

            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName))
                    {
                        case "Total Amount":
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;
                    }
                }
                string employeeName = string.Empty;
                if (data.Rows.Count > 0)
                {
                    string oldEmployee;
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName))
                            {
                                case "Emp Name":
                                    oldEmployee = row[col.ColumnName].ToString();
                                    if (employeeName != oldEmployee)
                                    {
                                        employeeName = row[col.ColumnName].ToString();
                                        newRowList.Add(employeeName);
                                    }
                                    else
                                    {
                                        oldEmployee = "";
                                        newRowList.Add(oldEmployee);
                                    }
                                    if (employeeName != oldEmployee)
                                    {
                                        oldEmployee = employeeName;
                                    }
                                    break;

                                case "Travel Date":
                                case "Return Date":
                                    if (!string.IsNullOrWhiteSpace(row[col.ColumnName].ToString()))
                                    {
                                        newRowList.Add(Convert.ToDateTime(row[col.ColumnName]).ToString("dd-MMM-yyyy"));
                                    }
                                    else
                                    {
                                        newRowList.Add(row[col.ColumnName].ToString());
                                    }
                                    break;

                                case "Total Amount":
                                    newRowList.Add(Convert.ToDecimal(row[col.ColumnName]).ToString("00,###.##"));
                                    break;

                                case "Billable to customer":
                                    newRowList.Add(Convert.ToBoolean(row[col.ColumnName]) == false ? "No" : "Yes");
                                    break;

                                default:
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            if (dt.Rows.Count > 0)
            {
                dt.Columns.Remove("ReqID");
                dt.Columns.Remove("Attachment");
            }
            return ExportDataTableExcel("Travel_Details_Report", dt, false, "Total Amount");
        }

        #endregion Travel Details Report

        #region EmployeePayroll_v2

        public ActionResult SearchPayrollExcel(int monthId, int yearId, string businessUnitList, int? statusId, int? proceed, string screentype)
        {
            string month = monthId.ToString();
            string year = yearId.ToString();
            EmployeePayrollRepository employeePayrollRepository = new EmployeePayrollRepository();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            PayrollBo_V2 payrollBo = new PayrollBo_V2
            {
                MonthIdList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty),
                YearIdList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty)
            };

            DataTable data =
                employeePayrollRepository.SearchEmployeePayroll(monthId, yearId, businessUnitList, statusId, proceed, screentype).Tables["PayrollDatatable"];
            data.Columns["remarks"].SetOrdinal(data.Columns.Count - 1);

            string monthName = Convert.ToString(payrollBo.MonthIdList.Where(m => m.Value.Equals(month)).Select(s => s.Text).FirstOrDefault());
            string yearName = Convert.ToString(payrollBo.YearIdList.Where(m => m.Value.Equals(year)).Select(s => s.Text).FirstOrDefault());
            string monthAndYear = (monthName + '_' + yearName);

            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    if (col.ColumnName.ToUpper() != "ORDERNUMBER" && col.ColumnName.ToUpper() != "ID" &&
                        col.ColumnName.ToUpper() != "EMPLOYEEPAYSTRUCTURE" &&
                        col.ColumnName.ToUpper() != "EMPLOYEEID" &&
                        col.ColumnName.ToUpper() != "EFFECTIVEFROM" &&
                        col.ColumnName.ToUpper() != "ISPROCESSED" && col.ColumnName.ToUpper() != "ISAPPROVED")
                    {
                        switch (Convert.ToString(col.ColumnName))
                        {
                            case "EmployeeName":
                            case "Workdays":
                            case "IFSC Code":
                            case "Bank Name":
                            case "DOJ":
                            case "Date Of Joining":
                            case "Department":
                            case "Designation":
                            case "Account No":
                            case "PAN No":
                            case "Beneficiary ID":
                            case "Remarks":
                                dt.Columns.Add(col.ColumnName, typeof(string));
                                break;

                            case "Code":
                                dt.Columns.Add("Emp_Code", typeof(string));
                                break;

                            default:
                                dt.Columns.Add(col.ColumnName, typeof(decimal));
                                break;
                        }
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            if (col.ColumnName.ToUpper() != "ORDERNUMBER" && col.ColumnName.ToUpper() != "ID" &&
                                col.ColumnName.ToUpper() != "EMPLOYEEPAYSTRUCTURE" &&
                                col.ColumnName.ToUpper() != "EMPLOYEEID" &&
                                col.ColumnName.ToUpper() != "EFFECTIVEFROM" &&
                                col.ColumnName.ToUpper() != "ISPROCESSED" && col.ColumnName.ToUpper() != "ISAPPROVED")
                            {
                                switch (Convert.ToString(col.ColumnName))
                                {
                                    case "EmployeeName":
                                    case "Code":
                                    case "Workdays":
                                    case "IFSC Code":
                                    case "Bank Name":
                                    case "DOJ":
                                    case "Date Of Joining":
                                    case "Department":
                                    case "Designation":
                                    case "Account No":
                                    case "PAN No":
                                    case "Beneficiary ID":
                                    case "Remarks":
                                        newRowList.Add(row[col.ColumnName].ToString());
                                        break;

                                    default:
                                        newRowList.Add(Convert.ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName])).ToString("#,##0.00"));
                                        break;
                                }
                            }
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            if (screentype == "savepayroll")
                return ExportDataTableExcelPayroll("EmployeePayroll", dt, false, "", monthAndYear);
            else
                return ExportDataTableExcel("EmployeePayroll", dt, false, "", monthAndYear);
        }

        #region TDS Details

        public ActionResult TdsDetailsExcel(string formName, int monthId, string yearId, string values)
        {
            TdsUpdateRepository tdsUpdateRepository = new TdsUpdateRepository();
            TDSUpdateBo tdsUpdateBo = new TDSUpdateBo
            {
                MonthId = monthId,
                Name = yearId,
                BusinessUnitIdList = values
            };

            List<TDSUpdateBo> data = tdsUpdateRepository.SearchTdsDetails(tdsUpdateBo);

            DataTable dt = new DataTable
            {
                TableName = "TDS_Details"// Excel sheet name.
            };
            dt.Columns.Add("Employee Code", typeof(string));
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Department", typeof(string));
            dt.Columns.Add("TDS Amount", typeof(decimal));
            dt.Columns.Add("Joining Date", typeof(string));
            dt.Columns.Add("Payroll Status", typeof(string));
            dt.Columns.Add("Business Unit", typeof(string));

            foreach (TDSUpdateBo i in data)
            {
                dt.Rows.Add(
                        i.Name,
                        i.EmployeeName,
                        i.UserMessage,
                        i.TDSAmount,
                        i.Doj.ToString("dd-MMM-yyyy"),
                        i.StatusCode,
                        i.Location);
            }
            return ExportDataTableExcel("TDS_Details", dt);
        }

        #endregion TDS Details

        #region Employee Paystructure_V2

        public ActionResult EmployeePayStructureExcel(string formName, string businessUnitIDs, int? employeeId, bool isActive)
        {
            EmployeePayStructureRepositoryV2 employeePayStructureRepository = new EmployeePayStructureRepositoryV2();
            EmployeePayStructureBo_V2 employeePayStructureBo = new EmployeePayStructureBo_V2
            {
                BusinessUnitIDs = businessUnitIDs,
                EmployeeId = Convert.ToInt32(employeeId),
                IsActive = isActive
            };
            List<EmployeePayStructureBo_V2> data = employeePayStructureRepository.SearchEmployee(employeePayStructureBo);

            DataTable dt = new DataTable
            {
                TableName = "Employee_Paystructure"// Excel sheet name.
            };
            dt.Columns.Add("Employee Name", typeof(string));
            dt.Columns.Add("Effective From", typeof(string));
            dt.Columns.Add("Basic", typeof(decimal));
            dt.Columns.Add("Company Pay Structure", typeof(string));

            foreach (EmployeePayStructureBo_V2 i in data)
            {
                dt.Rows.Add(
                        i.EmployeeName,
                        Convert.ToDateTime(i.EffectiveFrom).ToString("dd-MMM-yyyy"),
                        i.Basic,
                        i.CompanyPayStucture.Name);
            }
            return ExportDataTableExcel("Employee_Pay_Structure", dt);
        }

        #endregion Employee Paystructure_V2

        #region Company Paystructure

        public ActionResult CompanyPayStructureExcel(string formName)
        {
            CompanyPayStructureRepositoryV2 companyPayStructureRepository = new CompanyPayStructureRepositoryV2();

            List<CompanyPayStructureBo_V2> data = companyPayStructureRepository.SearchCompanyPayStructure();

            DataTable dt = new DataTable
            {
                TableName = "Company_Paystructure"// Excel sheet name.
            };
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Effective From", typeof(string));
            dt.Columns.Add("Modified On", typeof(DateTime));
            dt.Columns.Add("Modified By", typeof(string));

            foreach (CompanyPayStructureBo_V2 i in data)
            {
                dt.Rows.Add(
                        i.Name,
                        Convert.ToDateTime(i.EffectiveFrom).ToString("dd-MMM-yyyy"),
                        Convert.ToDateTime(i.ModifiedOn).ToString("dd-MMM-yyyy"),
                        i.ModifiedByName
                        );
            }
            return ExportDataTableExcel("Company_Pay_Structure", dt);
        }

        #endregion Company Paystructure

        #region Payroll Components

        public ActionResult PayrollComponentsExcel(string formName)
        {
            PayrollComponentsRepositoryV2 payrollComponentsRepository = new PayrollComponentsRepositoryV2();
            PayrollComponentsBo_V2 payrollComponentsBo = new PayrollComponentsBo_V2();

            List<PayrollComponentsBo_V2> data = payrollComponentsRepository.SearchPayrollComponentList(payrollComponentsBo);

            DataTable dt = new DataTable
            {
                TableName = "Payroll_Component"// Excel sheet name.
            };
            dt.Columns.Add("Component Name", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Ordinal", typeof(string));
            dt.Columns.Add("Modified By", typeof(string));
            dt.Columns.Add("Modified On", typeof(DateTime));

            foreach (PayrollComponentsBo_V2 i in data)
            {
                dt.Rows.Add(
                        i.Code,
                        i.Description,
                        i.Ordinal,
                        i.ModifiedByName,
                        Convert.ToDateTime(i.ModifiedOn).ToString("dd-MMM-yyyy")
                        );
            }
            return ExportDataTableExcel("Payroll_Components", dt);
        }

        #endregion Payroll Components

        #region Payroll report_V2

        public ActionResult SalaryMonthlyExcel(string businessUnitList, int? departmentId, int? componentId, int financialYearId)
        {
            PayPayrollReportsRepository payrollReportsRepository = new PayPayrollReportsRepository();

            DataTable data =
                payrollReportsRepository.Pay_Report_SearchEmployeePayroll(businessUnitList, departmentId, componentId, financialYearId);
            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName.ToLower()))
                    {
                        case "emp name":
                        case "department_name":
                        case "emp_code":
                        case "joining_date":
                        case "business_unit":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName.ToLower()))
                            {
                                case "emp name":
                                case "department_name":
                                case "emp_code":
                                case "joining_date":
                                case "business_unit":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                default:
                                    newRowList.Add(Convert
                                        .ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName]))
                                        .ToString("#,##0.00"));
                                    break;
                            }
                        }

                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Salary_Component_Report", dt);
        }

        public ActionResult EmployeeMonthlyPayrollAnalysisExcel(int monthId, int yearId, string businessUnitList, int? departmentId)
        {
            PayPayrollReportsRepository payrollReportsRepository = new PayPayrollReportsRepository();

            DataTable data =
                payrollReportsRepository.Pay_RPT_SearchPayrollByMonthly(monthId, yearId, businessUnitList, departmentId);
            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName).ToLower())
                    {
                        case "emp_code":
                        case "employee_name":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        case "joining_date":
                        case "business_unit":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName).ToLower())
                            {
                                case "emp_code":
                                case "employee_name":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                case "joining_date":
                                case "business_unit":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                default:
                                    newRowList.Add(Convert
                                        .ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName]))
                                        .ToString("#,##0.00"));
                                    break;
                            }
                        }

                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Employee_Salary_statement", dt);
        }

        public ActionResult EmployeeIndividualExcelExport(int employeeId)
        {
            PayPayrollReportsRepository payrollReportsRepository = new PayPayrollReportsRepository();

            DataTable data =
                payrollReportsRepository.Pay_GetPayrollHistory(employeeId);
            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName).ToLower())
                    {
                        case "monthid":
                            break;

                        case "emp_code":
                        case "employee_name":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        case "joining_date":
                        case "month":
                        case "year":
                        case "business_unit":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName).ToLower())
                            {
                                case "monthid":
                                    break;

                                case "emp_code":
                                case "employee_name":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                case "joining_date":
                                case "month":

                                case "year":
                                case "business_unit":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                default:
                                    newRowList.Add(Convert
                                        .ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName]))
                                        .ToString("#,##0.00"));
                                    break;
                            }
                        }

                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Individual_Salary_report", dt);
        }

        public ActionResult EmployeeMonthlyPayrollComponentExcel(int monthId, int yearId, string businessUnitList, int? departmentId)
        {
            PayPayrollReportsRepository payrollReportsRepository = new PayPayrollReportsRepository();

            DataTable data =
                payrollReportsRepository.Pay_RPT_SearchComponentsByMonthly(monthId, yearId, businessUnitList, departmentId);
            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName).ToLower())
                    {
                        case "business_unit":
                        case "department":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName).ToLower())
                            {
                                case "business_unit":
                                case "department":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                default:
                                    newRowList.Add(Convert
                                        .ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName]))
                                        .ToString("#,##0.00"));
                                    break;
                            }
                        }

                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Location_Wise_Consolidated_rpt", dt);
        }

        public ActionResult EmployeeCompletePayrollExcelExport(string formName, int fromMonthId, int fromYearId, int toMonthId, int toYearId)
        {
            PayPayrollReportsRepository payrollReportsRepository = new PayPayrollReportsRepository();

            DataTable data =
                payrollReportsRepository.Pay_EmployeeCompletePayrollRPTList(fromMonthId, fromYearId, toMonthId, toYearId);
            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName).ToLower())
                    {
                        case "monthid":
                            break;

                        case "emp_code":
                        case "employee_name":
                        case "designation":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        case "joining_date":
                        case "month":
                        case "year":
                        case "business_unit":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName).ToLower())
                            {
                                case "monthid":
                                    break;

                                case "emp_code":
                                case "employee_name":
                                case "designation":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                case "joining_date":
                                case "month":

                                case "year":
                                case "business_unit":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                default:
                                    newRowList.Add(Convert
                                        .ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName]))
                                        .ToString("#,##0.00"));
                                    break;
                            }
                        }

                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Complete_Payroll_report", dt);
        }


        public ActionResult EmployeePayStructureExcelExport(string formName, int CompanyPayStructureId)
        {
            PayPayrollReportsRepository payrollReportsRepository = new PayPayrollReportsRepository();

            DataTable data =
                payrollReportsRepository.Pay_GetEmployeePaystructureHistory(CompanyPayStructureId);
            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName).ToLower())
                    {
                        case "employeeid":
                            break;
                        case "emp_code":
                        case "employee_name":
                        case "department":
                        case "designation":
                        case "effectivefrom":
                        case "date_of_joining":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName).ToLower())
                            {
                                case "employeeid":
                                    break;
                                case "emp_code":
                                case "employee_name":
                                case "department":
                                case "designation":
                                case "effectivefrom":
                                case "date_of_joining":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                default:
                                    newRowList.Add(Convert
                                        .ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName]))
                                        .ToString("#,##0.00"));
                                    break;
                            }
                        }

                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("EmployeePaystructure_report", dt);
        }

        #endregion Payroll report_V2

        #region Unmapped Paystructure

        public ActionResult UnmappedPaystructureExcelExport(int? employeeID, bool isActive)
        {
            PayPayrollReportsRepository payrollReportsRepository = new PayPayrollReportsRepository();

            DataTable data =
                payrollReportsRepository.Pay_UnmappedPaystructure(employeeID, isActive);
            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    dt.Columns.Add(col.ColumnName, typeof(string));
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            newRowList.Add(row[col.ColumnName].ToString());
                        }
                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("Unmapped_Paystructure", dt);
        }

        #endregion Unmapped Paystructure

        #endregion EmployeePayroll_v2

        #region Inventory

        #region Stock Maintenance

        public ActionResult StockMaintenanceExcelExport(string formName, int productId)
        {
            InventoryBO inventoryBo = new InventoryBO();
            InventoryRepository inventoryRepository = new InventoryRepository();

            inventoryBo.ProductID = productId;
            List<InventoryBO> data = inventoryRepository.SearchStockMaintenance(inventoryBo);

            DataTable dt = new DataTable();
            dt.Columns.Add("Product Name", typeof(string));
            dt.Columns.Add("Stock on Hand", typeof(decimal));
            dt.Columns.Add("Commited Stock", typeof(string));
            dt.Columns.Add("Available Stock", typeof(decimal));
            dt.Columns.Add("UOM", typeof(string));
            dt.Columns.Add("Manufacturer", typeof(string));

            foreach (InventoryBO i in data)
            {
                dt.Rows.Add(
                        i.ProductName,
                        i.StockonHand,
                        i.CommitedStock,
                        i.AvailableStock,
                        i.UOM,
                        i.Manufacturer);
            }
            return ExportDataTableExcel("Stock_Maintenance", dt);
        }

        #endregion Stock Maintenance

        #region My Stock

        public ActionResult MyStockExcelExport(string formName, int productId)
        {
            InventoryRepository inventoryRepository = new InventoryRepository();

            List<InventoryBO> data = inventoryRepository.SearchMyStockDistribution(productId);

            DataTable dt = new DataTable();
            dt.Columns.Add("Cost Center", typeof(string));
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Product", typeof(string));
            dt.Columns.Add("Quantity", typeof(string));
            dt.Columns.Add("UOM", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (InventoryBO i in data)
            {
                string distributedDate = Convert.ToDateTime(i.DistributedDate).ToString("dd-MMM-yyyy");
                dt.Rows.Add(
                        i.CostCenter,
                        distributedDate,
                        i.ProductName,
                        i.Qty,
                        i.UOM,
                        i.Remarks);
            }
            return ExportDataTableExcel("My_Stock", dt);
        }

        #endregion My Stock

        #region Stock Adjustment

        public ActionResult StockAdjustmentExcelExport(string formName, int productId, int ledgerId)
        {
            InventoryRepository inventoryRepository = new InventoryRepository();
            InventoryBO inventoryBo = new InventoryBO
            {
                ProductID = productId,
                LedgerID = ledgerId
            };
            List<InventoryBO> data = inventoryRepository.SearchStockAdjustment(inventoryBo);

            DataTable dt = new DataTable();
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Ledger", typeof(string));
            dt.Columns.Add("Reason", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Adjusted By", typeof(string));

            foreach (InventoryBO i in data)
            {
                dt.Rows.Add(
                        Convert.ToDateTime(i.AdjustedDate).ToString("dd-MMM-yyyy"),
                        i.LedgerName,
                        i.Reason,
                        i.Remarks,
                        i.AdjustedBy);
            }
            return ExportDataTableExcel("Stock_Adjustment", dt);
        }

        #endregion Stock Adjustment

        #region Stock Distribution

        public ActionResult StockDistributionExcelExport(string formName, int employeeId)
        {
            InventoryRepository inventoryRepository = new InventoryRepository();
            InventoryBO inventoryBo = new InventoryBO
            {
                EmployeeId = employeeId
            };

            List<InventoryBO> data = inventoryRepository.SearchStockDistribution(inventoryBo);

            DataTable dt = new DataTable();
            dt.Columns.Add("Cost Center", typeof(string));
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Product", typeof(string));
            dt.Columns.Add("Party", typeof(string));
            dt.Columns.Add("Quantity", typeof(string));
            dt.Columns.Add("Description", typeof(string));

            foreach (InventoryBO i in data)
            {
                string distributedDate = Convert.ToDateTime(i.DistributedDate).ToString("dd-MMM-yyyy");
                dt.Rows.Add(
                        i.CostCenter,
                        distributedDate,
                        i.ProductName,
                        i.EmployeeName,
                        i.Qty,
                        i.Remarks);
            }
            return ExportDataTableExcel("Stock_Distribution", dt);
        }

        #endregion Stock Distribution

        #endregion Inventory

        #region TDS Reports

        #region IT Declaration Report

        public ActionResult TDSITDeclarationExcel(int financialYearId, string formType)
        {
            TdsHraRepository _tdsHraRepository = new TdsHraRepository();

            DataTable data =
                _tdsHraRepository.TDSITDeclarationRPTList(financialYearId, formType);
            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName.ToLower()))
                    {
                        case "financial year":
                        case "section":
                        case "component":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName.ToLower()))
                            {
                                case "financial year":
                                case "section":
                                case "component":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                default:
                                    newRowList.Add(Convert
                                        .ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName]))
                                        .ToString("#,##0.00"));
                                    break;
                            }
                        }

                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("IT_Complete_report", dt);
        }

        #endregion IT Declaration Report

        #region HRA Clearance Report

        public ActionResult TDSHRAClearanceExcel(int financialYearId, string status)
        {
            TdsHraRepository _tdsHraRepository = new TdsHraRepository();

            DataTable data =
                _tdsHraRepository.TDSHRAClearanceRPTList(financialYearId, status);
            DataTable dt = new DataTable();
            if (data != null)
            {
                foreach (DataColumn col in data.Columns)
                {
                    switch (Convert.ToString(col.ColumnName.ToLower()))
                    {
                        case "employee name":
                        case "employee code":
                            dt.Columns.Add(col.ColumnName, typeof(string));
                            break;

                        default:
                            dt.Columns.Add(col.ColumnName, typeof(decimal));
                            break;
                    }
                }

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        List<string> newRowList = new List<string>();
                        foreach (DataColumn col in data.Columns)
                        {
                            switch (Convert.ToString(col.ColumnName.ToLower()))
                            {
                                case "employee name":
                                case "employee code":
                                    newRowList.Add(row[col.ColumnName].ToString());
                                    break;

                                default:
                                    newRowList.Add(Convert.ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName])).ToString("#,##0.00") == "0.00" ? null : Convert.ToDecimal(Utility.CheckDbNull<decimal>(row[col.ColumnName])).ToString("#,##0.00"));
                                    break;
                            }
                        }

                        dt.Rows.Add(newRowList.ToArray());
                    }
                }
            }
            return ExportDataTableExcel("HRA_Complete_report", dt);
        }

        #endregion HRA Clearance Report

        #endregion TDS Reports

        #region ExportExcel

        private ActionResult ExportDataTableExcel(string sheetName, DataTable dataTable, bool isDateAndTime = false, string totalColumn = "", string monthAndYear = "")
        {
            string[] tokens = totalColumn.ToUpper().Split('|');
            string[] header = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };
            dataTable.Columns.Add("S.No", typeof(string));
            dataTable.Columns["S.No"].SetOrdinal(0);
            //creating a new Workbook
            XLWorkbook wb = new XLWorkbook();
            // adding a new sheet in workbook
            IXLWorksheet ws = wb.Worksheets.Add(sheetName);
            // adding a new sheet in workbook
            //adding content
            ws.Style.Fill.PatternBackgroundColor = XLColor.White;
            ws.Style.Fill.BackgroundColor = XLColor.White;

            //Title
            ws.Cell("A1").Value = "Company";
            ws.Cell("B1").Value = Utility.GetSession("CompanyName");
            ws.Cell("A2").Value = "Report Name";
            ws.Cell("B2").Value = sheetName.Replace('_', ' ');
            ws.Cell("A3").Value = "Created On";
            ws.Cell("B3").Value = DateTime.Now.ToString("dd-MMM-yyyy");
            ws.Range("B1:C1").Merge();
            ws.Range("B2:C2").Merge();
            ws.Range("B3:C3").Merge();
            ws.Range("B3:C3").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            ws.Range("A1:C1").Style.Font.Bold = true;
            if (monthAndYear != "")
            {
                ws.Cell("A4").Value = "Month";
                ws.Cell("B4").Value = monthAndYear;
                ws.Range("B4:C4").Merge();
                ws.Range("B4:C4").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                ws.Range("A1:C4").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C4").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C4").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C4").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C4").Style.Font.FontName = "Cambria";
                ws.Range("A1:C4").Style.Font.FontSize = 12;
            }
            else
            {
                ws.Range("A1:C3").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C3").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C3").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C3").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C3").Style.Font.FontName = "Cambria";
                ws.Range("A1:C3").Style.Font.FontSize = 12;
            }
            //add columns
            string[] cols = new string[dataTable.Columns.Count];
            for (int c = 0; c < dataTable.Columns.Count; c++)
            {
                cols[c] = dataTable.Columns[c].ToString();
            }

            //char StartCharCols = 'A';
            int StartIndexCols = 6;

            #region CreatingColumnHeaders

            for (int i = 1; i <= cols.Length; i++)
            {
                if (i == cols.Length)
                {
                    string dataCell = Convert.ToString(header[i - 1]) + Convert.ToString(StartIndexCols);
                    ws.Cell(dataCell).Value = cols[i - 1];
                    ws.Cell(dataCell).Style.Font.Bold = true;
                    ws.Cell(dataCell).Style.Fill.BackgroundColor = XLColor.AirForceBlue;
                    ws.Cell(dataCell).Style.Font.FontColor = XLColor.White;
                    ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                }
                else
                {
                    string dataCell = Convert.ToString(header[i - 1]) + Convert.ToString(StartIndexCols);
                    ws.Cell(dataCell).Value = cols[i - 1];
                    ws.Cell(dataCell).Style.Font.Bold = true;
                    ws.Cell(dataCell).Style.Fill.BackgroundColor = XLColor.AirForceBlue;
                    ws.Cell(dataCell).Style.Font.FontColor = XLColor.White;
                    ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    //StartCharCols++;
                }
            }

            #endregion CreatingColumnHeaders

            //Merging Header

            string range = "A7:" + Convert.ToString(header[0]) + "3";
            ws.Range(range).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.RightBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.TopBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            //char StartCharData = 'A';
            int startIndexData = 7;
            string lastChar = Convert.ToString(header[0]);
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                dataTable.Rows[i]["S.No"] = i + 1;
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    string dataType = dataTable.Columns[j].DataType.Name.ToLower();
                    string dataCell = Convert.ToString(header[j]) + startIndexData;
                    string a = dataTable.Rows[i][j].ToString();
                    a = a.Replace("&nbsp;", " ");
                    a = a.Replace("&amp;", "&");

                    switch (dataType)
                    {
                        case "int":
                        case "int32":
                            int val;
                            if (int.TryParse(a, out val))
                            {
                                ws.Cell(dataCell).Value = val;
                                ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                                ws.Cell(dataCell).Style.Alignment.Indent = 1;
                            }
                            break;

                        case "date":
                        case "datetime":
                            DateTime dt;
                            if (isDateAndTime == false)
                            {
                                //check if datetime type
                                if (DateTime.TryParse(a, out dt))
                                {
                                    ws.Cell(dataCell).Value = dt.ToShortDateString();
                                    ws.Range(range).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                    ws.Range(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                    ws.Cell(dataCell).SetValue(dt.ToString("dd-MMM-yyyy"));
                                }
                            }
                            else
                            {
                                if (DateTime.TryParse(a, out dt))
                                {
                                    ws.Cell(dataCell).Value = dt.ToShortDateString();
                                    ws.Range(range).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                    ws.Range(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                    ws.Cell(dataCell).SetValue(dt.ToString("dd-MMM-yyyy HH:mm:ss tt"));
                                }
                            }
                            break;

                        case "double":
                        case "decimal":
                            decimal dec = decimal.MinValue;
                            if (decimal.TryParse(a, out dec))
                            {
                                ws.Cell(dataCell).Value = Utility.PrecisionForDecimal(dec, 2);
                                ws.Range(range).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                                ws.Range(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                ws.Cell(dataCell).Style.Alignment.Indent = 1;
                                ws.Cell(dataCell).SetValue(dec);
                                ws.Range(dataCell).Style.NumberFormat.Format = "#,##0.00";
                                if (string.IsNullOrWhiteSpace(totalColumn))
                                {
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).FormulaA1 = "=Sum(" + Convert.ToString(header[j]) + "7:" + Convert.ToString(header[j]) + Convert.ToString((dataTable.Rows.Count + 6)) + ")";
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).Style.Alignment.Indent = 1;
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).Style.Font.Bold = true;
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).Style.NumberFormat.Format = "#,##0.00";
                                }
                                else
                                {
                                    int pos = Array.IndexOf(tokens, dataTable.Columns[j].ColumnName.ToUpper());
                                    if (pos > -1)
                                    {
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).FormulaA1 = "=Sum(" + Convert.ToString(header[j]) + "7:" + Convert.ToString(header[j]) + Convert.ToString((dataTable.Rows.Count + 6)) + ")";
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).Style.Alignment.Indent = 1;
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).Style.Font.Bold = true;
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 7)).Style.NumberFormat.Format = "#,##0.00";
                                    }
                                }
                            }
                            break;

                        default:
                            ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                            ws.Cell(dataCell).Style.Alignment.Indent = 1;
                            ws.Cell(dataCell).SetValue(a.Replace("*", ""));
                            break;
                    }
                    ws.Cell(dataCell).WorksheetColumn().AdjustToContents();

                    // StartCharData++;
                    lastChar = Convert.ToString(header[j]);
                }
                //StartCharData = 'A';
                startIndexData++;
            }

            int totalRows = dataTable.Rows.Count + 7;
            range = "A6:" + lastChar + totalRows;
            ws.Range(range).Style.Font.FontSize = 10;
            ws.Range(range).Style.Font.FontName = "Cambria";

            ws.Range(range).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.RightBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.TopBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    Utility.GetSession("CompanyName") + "_" + sheetName + ".xlsx");
            }
        }

        #endregion ExportExcel

        private ActionResult ExportDataTableExcelPayroll(string sheetName, DataTable dataTable, bool isDateAndTime = false, string totalColumn = "", string monthAndYear = "")
        {
            string[] tokens = totalColumn.ToUpper().Split('|');
            string[] header = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };
            dataTable.Columns.Add("S.No", typeof(string));
            dataTable.Columns["S.No"].SetOrdinal(0);
            //creating a new Workbook
            XLWorkbook wb = new XLWorkbook();
            // adding a new sheet in workbook
            IXLWorksheet ws = wb.Worksheets.Add(sheetName);
            // adding a new sheet in workbook
            //adding content
            ws.Style.Fill.PatternBackgroundColor = XLColor.White;
            ws.Style.Fill.BackgroundColor = XLColor.White;

            //Title

            //add columns
            string[] cols = new string[dataTable.Columns.Count];
            for (int c = 0; c < dataTable.Columns.Count; c++)
            {
                cols[c] = dataTable.Columns[c].ToString();
            }

            //char StartCharCols = 'A';
            int StartIndexCols = 1;

            #region CreatingColumnHeaders

            for (int i = 1; i <= cols.Length; i++)
            {
                if (i == cols.Length)
                {
                    string dataCell = Convert.ToString(header[i - 1]) + Convert.ToString(StartIndexCols);
                    ws.Cell(dataCell).Value = cols[i - 1];
                    ws.Cell(dataCell).Style.Font.Bold = true;
                    ws.Cell(dataCell).Style.Fill.BackgroundColor = XLColor.AirForceBlue;
                    ws.Cell(dataCell).Style.Font.FontColor = XLColor.White;
                    ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                }
                else
                {
                    string dataCell = Convert.ToString(header[i - 1]) + Convert.ToString(StartIndexCols);
                    ws.Cell(dataCell).Value = cols[i - 1];
                    ws.Cell(dataCell).Style.Font.Bold = true;
                    ws.Cell(dataCell).Style.Fill.BackgroundColor = XLColor.AirForceBlue;
                    ws.Cell(dataCell).Style.Font.FontColor = XLColor.White;
                    ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    //StartCharCols++;
                }
            }

            #endregion CreatingColumnHeaders

            //Merging Header

            string range = "A1:" + Convert.ToString(header[0]) + "1";
            //ws.Range(range).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            //ws.Range(range).Style.Border.RightBorder = XLBorderStyleValues.Thin;
            //ws.Range(range).Style.Border.TopBorder = XLBorderStyleValues.Thin;
            //ws.Range(range).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            //char StartCharData = 'A';
            int startIndexData = 2;
            string lastChar = Convert.ToString(header[0]);
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                dataTable.Rows[i]["S.No"] = i + 1;
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    string dataType = dataTable.Columns[j].DataType.Name.ToLower();
                    string dataCell = Convert.ToString(header[j]) + startIndexData;
                    string a = dataTable.Rows[i][j].ToString();
                    a = a.Replace("&nbsp;", " ");
                    a = a.Replace("&amp;", "&");

                    switch (dataType)
                    {
                        case "int":
                        case "int32":
                            int val;
                            if (int.TryParse(a, out val))
                            {
                                ws.Cell(dataCell).Value = val;
                                ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                                ws.Cell(dataCell).Style.Alignment.Indent = 1;
                            }
                            break;

                        case "date":
                        case "datetime":
                            DateTime dt;
                            if (isDateAndTime == false)
                            {
                                //check if datetime type
                                if (DateTime.TryParse(a, out dt))
                                {
                                    ws.Cell(dataCell).Value = dt.ToShortDateString();
                                    ws.Range(range).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                    ws.Range(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                    ws.Cell(dataCell).SetValue(dt.ToString("dd-MMM-yyyy"));
                                }
                            }
                            else
                            {
                                if (DateTime.TryParse(a, out dt))
                                {
                                    ws.Cell(dataCell).Value = dt.ToShortDateString();
                                    ws.Range(range).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                    ws.Range(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                    ws.Cell(dataCell).SetValue(dt.ToString("dd-MMM-yyyy HH:mm:ss tt"));
                                }
                            }
                            break;

                        case "double":
                        case "decimal":
                            decimal dec = decimal.MinValue;
                            if (decimal.TryParse(a, out dec))
                            {
                                ws.Cell(dataCell).Value = Utility.PrecisionForDecimal(dec, 2);
                                ws.Range(range).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                                ws.Range(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                ws.Cell(dataCell).Style.Alignment.Indent = 1;
                                ws.Cell(dataCell).SetValue(dec);
                                ws.Range(dataCell).Style.NumberFormat.Format = "#,##0.00";
                                if (string.IsNullOrWhiteSpace(totalColumn))
                                {
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).FormulaA1 = "=Sum(" + Convert.ToString(header[j]) + "2:" + Convert.ToString(header[j]) + Convert.ToString((dataTable.Rows.Count + 1)) + ")";
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).Style.Alignment.Indent = 1;
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).Style.Font.Bold = true;
                                    ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).Style.NumberFormat.Format = "#,##0.00";
                                }
                                else
                                {
                                    int pos = Array.IndexOf(tokens, dataTable.Columns[j].ColumnName.ToUpper());
                                    if (pos > -1)
                                    {
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).FormulaA1 = "=Sum(" + Convert.ToString(header[j]) + "2:" + Convert.ToString(header[j]) + Convert.ToString((dataTable.Rows.Count + 1)) + ")";
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).Style.Alignment.Indent = 1;
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).Style.Font.Bold = true;
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + 2)).Style.NumberFormat.Format = "#,##0.00";
                                    }
                                }
                            }
                            break;

                        default:
                            ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                            ws.Cell(dataCell).Style.Alignment.Indent = 1;
                            ws.Cell(dataCell).SetValue(a.Replace("*", ""));
                            break;
                    }
                    ws.Cell(dataCell).WorksheetColumn().AdjustToContents();

                    // StartCharData++;
                    lastChar = Convert.ToString(header[j]);
                }
                //StartCharData = 'A';
                startIndexData++;
            }

            int totalRows = dataTable.Rows.Count + 2;
            range = "A1:" + lastChar + totalRows;
            ws.Range(range).Style.Font.FontSize = 10;
            ws.Range(range).Style.Font.FontName = "Cambria";

            ws.Range(range).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.RightBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.TopBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    Utility.GetSession("CompanyName") + "_" + sheetName + ".xlsx");
            }
        }


        private ActionResult ExportDataTableExcelPayrollMonthlyDetails(string sheetName, DataTable dataTable, bool isDateAndTime = false, string totalColumn = "", string monthAndYear = "")
        {
            string[] tokens = totalColumn.ToUpper().Split('|');
            string[] header = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ" };
            //dataTable.Columns.Add("Group", typeof(string));
            //dataTable.Columns["Group"].SetOrdinal(0);
            //creating a new Workbook
            XLWorkbook wb = new XLWorkbook();
            // adding a new sheet in workbook
            IXLWorksheet ws = wb.Worksheets.Add(sheetName);
            // adding a new sheet in workbook
            //adding content
            ws.Style.Fill.PatternBackgroundColor = XLColor.White;
            ws.Style.Fill.BackgroundColor = XLColor.White;

            //Title
            ws.Cell("A1").Value = "Company";
            ws.Cell("B1").Value = Utility.GetSession("CompanyName");
            ws.Cell("A2").Value = "Report Name";
            ws.Cell("B2").Value = sheetName.Replace('_', ' ');
            ws.Cell("A3").Value = "Created On";
            ws.Cell("B3").Value = DateTime.Now.ToString("dd-MMM-yyyy");
            ws.Range("B1:C1").Merge();
            ws.Range("B2:C2").Merge();
            ws.Range("B3:C3").Merge();
            ws.Range("B3:C3").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            ws.Range("A1:C1").Style.Font.Bold = true;
            if (monthAndYear != "")
            {
                ws.Cell("A4").Value = "Month";
                ws.Cell("B4").Value = monthAndYear;
                ws.Range("B4:C4").Merge();
                ws.Range("B4:C4").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                ws.Range("A1:C4").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C4").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C4").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C4").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C4").Style.Font.FontName = "Cambria";
                ws.Range("A1:C4").Style.Font.FontSize = 12;
            }
            else
            {
                ws.Range("A1:C3").Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C3").Style.Border.RightBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C3").Style.Border.TopBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C3").Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                ws.Range("A1:C3").Style.Font.FontName = "Cambria";
                ws.Range("A1:C3").Style.Font.FontSize = 12;
            }
            //add columns
            int earningsCount = 0;
            int deductionsCount = 0;
            string[] cols = new string[dataTable.Columns.Count];
            for (int c = 0; c < dataTable.Columns.Count; c++)
            {

                cols[c] = dataTable.Columns[c].ToString();

                if (dataTable.Columns[c].ToString().Contains("/Earnings"))
                    if (dataTable.Columns[c].ToString().Split('/')[1] == "Earnings")
                        earningsCount++;
                if (dataTable.Columns[c].ToString().Contains("/Deductions"))
                    if (dataTable.Columns[c].ToString().Split('/')[1] == "Deductions")
                        deductionsCount++;
            }
            cols = cols.Where(a => a != null).ToArray();
            //char StartCharCols = 'A';
            int StartIndexCols = 6;

            #region CreatingColumnHeaders

            string earnings = string.Empty;
            string deductions = string.Empty;

            for (int i = 1; i <= cols.Length; i++)
            {
                if (cols[i - 1].ToString().Contains("/Earnings"))
                    if (cols[i - 1].Split('/')[1] == "Earnings")
                        if (string.IsNullOrEmpty(earnings))
                        {
                            earnings = Convert.ToString(header[i - 1]) + Convert.ToString(StartIndexCols - 1) + ":" + Convert.ToString(header[i - 1 + earningsCount - 1]) + Convert.ToString(StartIndexCols - 1);
                            ws.Range(earnings).Merge();
                            ws.Range(earnings).Style.Fill.BackgroundColor = XLColor.Green;
                            ws.Range(earnings).Value = "Earnings";
                            ws.Range(earnings).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            ws.Range(earnings).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                            ws.Range(earnings).Style.Font.FontColor = XLColor.White;


                        }
                if (cols[i - 1].ToString().Contains("/Deductions"))
                    if (cols[i - 1].Split('/')[1] == "Deductions")
                        if (string.IsNullOrEmpty(deductions))
                        {
                            deductions = Convert.ToString(header[i - 1]) + Convert.ToString(StartIndexCols - 1) + ":" + Convert.ToString(header[i - 1 + deductionsCount - 1]) + Convert.ToString(StartIndexCols - 1);
                            ws.Range(deductions).Merge();
                            ws.Range(deductions).Style.Fill.BackgroundColor = XLColor.Red;
                            ws.Range(deductions).Value = "Deductions";
                            ws.Range(deductions).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            ws.Range(deductions).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                            ws.Range(deductions).Style.Font.FontColor = XLColor.White;

                        }
                // if (cols[i - 1] != "Department")
                if (i == cols.Length)
                {
                    string dataCell = Convert.ToString(header[i - 1]) + Convert.ToString(StartIndexCols);
                    ws.Cell(dataCell).Value = cols[i - 1].Split('/')[0];
                    ws.Cell(dataCell).Style.Font.Bold = true;
                    ws.Cell(dataCell).Style.Fill.BackgroundColor = XLColor.AirForceBlue;
                    ws.Cell(dataCell).Style.Font.FontColor = XLColor.White;
                    ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                }
                else
                {
                    string dataCell = Convert.ToString(header[i - 1]) + Convert.ToString(StartIndexCols);
                    ws.Cell(dataCell).Value = cols[i - 1].Split('/')[0];
                    ws.Cell(dataCell).Style.Font.Bold = true;
                    ws.Cell(dataCell).Style.Fill.BackgroundColor = XLColor.AirForceBlue;
                    ws.Cell(dataCell).Style.Font.FontColor = XLColor.White;
                    ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    //StartCharCols++;
                }
            }

            #endregion CreatingColumnHeaders

            //Merging Header

            string range = "A7:" + Convert.ToString(header[0]) + "3";
            ws.Range(range).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.RightBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.TopBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            //char StartCharData = 'A';
            int startIndexData = 7;
            string lastChar = Convert.ToString(header[0]);

            var groupedBydep = dataTable.AsEnumerable().GroupBy(row => row.Field<string>("Department")).ToList();
            foreach (var group in groupedBydep)
            {
                string dataCell = Convert.ToString(header[0]) + startIndexData;
                ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                ws.Cell(dataCell).Style.Alignment.Indent = 1;
                ws.Cell(dataCell).SetValue(group.Key.Replace("*", ""));
                startIndexData++;

                for (int i = 0; i < group.Count(); i++)
                {
                    //   dataTable.Rows[i]["S.No"] = i + 1;
                    for (int j = 0; j < cols.Count(); j++)
                    {

                        string dataType = dataTable.Columns[j].DataType.Name.ToLower();
                        dataCell = Convert.ToString(header[j]) + startIndexData;
                        string a = group.ElementAt(i)[j].ToString();
                        if (dataTable.Columns[j].ToString() == "Department")
                        {
                            a = "";
                        }
                        a = a.Replace("&nbsp;", " ");
                        a = a.Replace("&amp;", "&");

                        switch (dataType)
                        {
                            case "int":
                            case "int32":
                                int val;
                                if (int.TryParse(a, out val))
                                {
                                    ws.Cell(dataCell).Value = val;
                                    ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                                    ws.Cell(dataCell).Style.Alignment.Indent = 1;
                                }
                                break;

                            case "date":
                            case "datetime":
                                DateTime dt;
                                if (isDateAndTime == false)
                                {
                                    //check if datetime type
                                    if (DateTime.TryParse(a, out dt))
                                    {
                                        ws.Cell(dataCell).Value = dt.ToShortDateString();
                                        ws.Range(range).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                        ws.Range(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                        ws.Cell(dataCell).SetValue(dt.ToString("dd-MMM-yyyy"));
                                    }
                                }
                                else
                                {
                                    if (DateTime.TryParse(a, out dt))
                                    {
                                        ws.Cell(dataCell).Value = dt.ToShortDateString();
                                        ws.Range(range).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                        ws.Range(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                        ws.Cell(dataCell).SetValue(dt.ToString("dd-MMM-yyyy HH:mm:ss tt"));
                                    }
                                }
                                break;

                            case "double":
                            case "decimal":
                                decimal dec = decimal.MinValue;
                                if (decimal.TryParse(a, out dec))
                                {
                                    ws.Cell(dataCell).Value = Utility.PrecisionForDecimal(dec, 2);
                                    ws.Range(range).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                                    ws.Range(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                    ws.Cell(dataCell).Style.Alignment.Indent = 1;
                                    ws.Cell(dataCell).SetValue(dec);
                                    ws.Range(dataCell).Style.NumberFormat.Format = "#,##0.00";
                                    if (string.IsNullOrWhiteSpace(totalColumn))
                                    {
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).FormulaA1 = "=Sum(" + Convert.ToString(header[j]) + "7:" + Convert.ToString(header[j]) + Convert.ToString((dataTable.Rows.Count + groupedBydep.Count() + 6)) + ")";
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).Style.Alignment.Indent = 1;
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).Style.Font.Bold = true;
                                        ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).Style.NumberFormat.Format = "#,##0.00";
                                    }
                                    else
                                    {
                                        int pos = Array.IndexOf(tokens, dataTable.Columns[j].ColumnName.ToUpper());
                                        if (pos > -1)
                                        {
                                            ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).FormulaA1 = "=Sum(" + Convert.ToString(header[j]) + "7:" + Convert.ToString(header[j]) + Convert.ToString((dataTable.Rows.Count + groupedBydep.Count() + 6)) + ")";
                                            ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                                            ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).Style.Alignment.Indent = 1;
                                            ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).Style.Font.Bold = true;
                                            ws.Range(Convert.ToString(header[j]) + (dataTable.Rows.Count + groupedBydep.Count() + 7)).Style.NumberFormat.Format = "#,##0.00";
                                        }
                                    }
                                }
                                break;

                            default:
                                ws.Cell(dataCell).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                                ws.Cell(dataCell).Style.Alignment.Indent = 1;
                                ws.Cell(dataCell).SetValue(a.Replace("*", ""));
                                break;
                        }
                        ws.Cell(dataCell).WorksheetColumn().AdjustToContents();

                        // StartCharData++;
                        lastChar = Convert.ToString(header[j]);

                    }
                    //StartCharData = 'A';
                    startIndexData++;
                }

            }
            int totalRows = dataTable.Rows.Count + groupedBydep.Count() + 7;
            range = "A6:" + lastChar + totalRows;
            ws.Range(range).Style.Font.FontSize = 10;
            ws.Range(range).Style.Font.FontName = "Cambria";

            ws.Range(range).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.RightBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.TopBorder = XLBorderStyleValues.Thin;
            ws.Range(range).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                return File(memoryStream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    Utility.GetSession("CompanyName") + "_" + sheetName + ".xlsx");
            }
        }
    }
}