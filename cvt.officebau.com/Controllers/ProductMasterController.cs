﻿/*Code Review Done Neeed Proper Commands for all the methods and removed some unwanted local variables need Regions*/

using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class ProductMasterController : Controller
    {
        #region Product Master

        public ActionResult ProductMaster(string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }
            Utility.SetSession("SubMenuRetention", "PRODUCTMASTER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Product Master");

            ProductMasterBo productMasterBo = new ProductMasterBo();
            if (Utility.GetSession("MenuID") == "INVPRODU")
            {
                Utility.SetSession("MenuRetention", "INVENTORY");
                productMasterBo.MenuCode = "INVPRODU";
            }
            else
            {
                Utility.SetSession("MenuRetention", "MASTER");
                productMasterBo.MenuCode = "PRODU";
            }

            _productMasterRepository = new ProductMasterRepository();
            CommonRepository commonRepository = new CommonRepository();

            ViewData["productList"] = _productMasterRepository.SearchProduct(productMasterBo);
            productMasterBo.TransactionTypeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "ProductType", "Type");
            productMasterBo.TypeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "ProductServiceType", "Type");
            //productMasterBO.TransactionTypeList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "ProductType");
            return View(productMasterBo);
        }

        #endregion Product Master

        #region Search Product Master

        public ActionResult SearchProductMaster(ProductMasterBo productMasterBo)
        {
            return PartialView(_productMasterRepository.SearchProduct(productMasterBo));
        }

        #endregion Search Product Master

        #region Auto Complete

        public ActionResult AutoCompleteHsnCode(string hsnCode)
        {
            return Json(_productMasterRepository.AutoCompleteHsnCode(hsnCode), JsonRequestBehavior.AllowGet);
        }

        #endregion Auto Complete

        #region Get HSN Code

        public ActionResult GetHsnCode(int hsnId)
        {
            return Json(_productMasterRepository.GetGstHsnCode(hsnId), JsonRequestBehavior.AllowGet);
        }

        #endregion Get HSN Code

        #region Variables

        private ProductMasterRepository _productMasterRepository = new ProductMasterRepository();

        #endregion Variables

        #region Create Product Master

        public ActionResult CreateProductMaster(int productId, string menuCode)
        {
            ProductMasterBo productMasterBo = _productMasterRepository.GetProduct(productId);
            productMasterBo.MenuCode = menuCode;
            CommonRepository commonRepository = new CommonRepository();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            productMasterBo.TransactionTypeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "ProductType", "Type");
            productMasterBo.TypeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "ProductServiceType", "Type");
            productMasterBo.LedgerList = masterDataRepository.SearchMasterDataDropDown("Ledger");
            productMasterBo.UomList = masterDataRepository.SearchMasterDataDropDown("UOM", string.Empty);
            return PartialView(productMasterBo);
        }

        [HttpPost]
        public ActionResult CreateProductMaster(ProductMasterBo productMasterBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (productMasterBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), productMasterBo.MenuCode, productMasterBo.Id == 0 ? "Save" : "Edit");
            }

            if (productMasterBo != null && productMasterBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), productMasterBo.MenuCode, "Delete");
            }

            return Json(!hasAccess ? Constants.UnAuthorizedAccess : _productMasterRepository.ManageProduct(productMasterBo));
        }

        #endregion Create Product Master

        #region GST Configuration

        public ActionResult CreateGstConfiguration()
        {
            Utility.SetSession("MenuRetention", "MASTERDATA");
            Utility.SetSession("SubMenuRetention", "GSTCONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Master Data");

            ProductMasterBo productMasterBo = new ProductMasterBo();

            ViewData["GstConfigurationDetails"] = _productMasterRepository.GetGstHsnList(productMasterBo);
            return PartialView(productMasterBo);
        }

        [HttpPost]
        public ActionResult CreateGstConfiguration(ProductMasterBo productMasterBo)
        {
            return Json(_productMasterRepository.ManageGstConfiguration(productMasterBo));
        }

        public ActionResult GstConfiguration()
        {
            ProductMasterBo productMasterBo = new ProductMasterBo();
            return PartialView(_productMasterRepository.GetGstHsnList(productMasterBo));
        }

        #endregion GST Configuration
    }
}