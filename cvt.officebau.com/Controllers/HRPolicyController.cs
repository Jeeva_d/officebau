﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class HRPolicyController : Controller
    {
        #region Claims Eligibility

        public ActionResult ClaimsEligibilityGrid()
        {
            ClaimCategoryRepository claimCategoryRepository = new ClaimCategoryRepository();

            int employeeId = Utility.UserId();
            return PartialView(claimCategoryRepository.SearchClaimsEligibilityList(employeeId));
        }

        #endregion Claims Eligibility

        #region HR Policy

        public ActionResult HrPolicy(string menuCode)
        {
            Utility.SetSession("MenuRetention", "EMPLOYEEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "HRPOLICY");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("HR Policy");

            RbsRepository rBsRepository = new RbsRepository();
            return View(rBsRepository.SearchMenuForConfiguration());
        }

        public ActionResult HrPolicyDocument()
        {
            string url = WebConfigurationManager.AppSettings["URL"];
            HolidayBo holidayBo = new HolidayBo
            {
                DocumentPath = url + "\\Content\\Documents\\HR_Manual_4.0_Final_1.pdf"
            };
            return PartialView(holidayBo);
        }

        #endregion HR Policy

        #region variable

        private readonly EmployeeMasterRepository _employeeMasterRepository = new EmployeeMasterRepository();

        #endregion variable

        #region Skill Update

        public ActionResult SkillUpdate()
        {
            Utility.SetSession("MenuRetention", "EMPLOYEEMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "SKILLUPDATE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Skill Update");

            SkillDetailsBo skillDetailsBo = new SkillDetailsBo
            {
                EmployeeId = Utility.UserId(),
                MenuCode = MenuCode.MySkill,
                Operation = Operations.PageLoad
            };
            List<SkillDetailsBo> skillDetailsBoList = _employeeMasterRepository.GetEmployeeSkillsList(skillDetailsBo);
            if (skillDetailsBoList != null)
            {
                skillDetailsBo = skillDetailsBoList.FirstOrDefault(s => s.IsLocked);
            }
            return View(skillDetailsBo);
        }

        #endregion Skill Update
    }
}