﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    public class InternalMeetingController : Controller
    {
        InternalMeetingRepository _internalMeetingRepository = new InternalMeetingRepository();
        readonly InternalMeetingBO _InternalMeetingBO = new InternalMeetingBO();

        #region InternalMeeting
        // GET: InternalMeeting
        public ActionResult Index()
        {
            MasterDataRepository MasterDataRepository = new MasterDataRepository();
            _InternalMeetingBO.EmployeeList = _internalMeetingRepository.GetEmployeeList();
            _InternalMeetingBO.MeetingTypeList = _internalMeetingRepository.GetMeetingTypeList();
            return View(_InternalMeetingBO);
        }

        public ActionResult SearchInternalMeeting(string title, int? organizedBy, int? meetingType)
        {
            return PartialView(_internalMeetingRepository.SearchInternalMeeting(title, organizedBy, meetingType));
        }

        public ActionResult CreateNewInternalMeeting(int id)
        {
            InternalMeetingBO _InternalMeetingBO = new InternalMeetingBO();

            if (id != 0)
            {
                _InternalMeetingBO = _internalMeetingRepository.GetInternalMeeting(id);

                if (!_InternalMeetingBO.Attendees.IsNullOrEmpty())
                    _InternalMeetingBO.AttendeesArray = _InternalMeetingBO.Attendees.Split(',');
            }

            MasterDataRepository MasterDataRepository = new MasterDataRepository();

            _InternalMeetingBO.EmployeeList = _internalMeetingRepository.GetEmployeeList();
            _InternalMeetingBO.MeetingTypeList = _internalMeetingRepository.GetMeetingTypeList();

            return PartialView(_InternalMeetingBO);
        }

        [HttpPost]
        public ActionResult CreateNewInternalMeeting(InternalMeetingBO InternalMeetingBO)
        {
            if (InternalMeetingBO.AttendeesArray != null)
                InternalMeetingBO.Attendees = string.Join(",", InternalMeetingBO.AttendeesArray);
            return Json(_internalMeetingRepository.ManageInternalMeeting(InternalMeetingBO));
        }

        #endregion
    }
}