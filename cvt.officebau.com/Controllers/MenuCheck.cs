﻿using cvt.officebau.com.Services;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace cvt.officebau.com.Controllers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class MenuCheck : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string menuCode = HttpContext.Current.Request.QueryString["Menucode"];
            if (!string.IsNullOrEmpty(menuCode))
            {
                RbsRepository rep = new RbsRepository();
                string menu = rep.SearchrMenu(menuCode);
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"Controller", menu.Split('/')[0]},
                        {"Action", menu.Split('/')[1]}
                    });
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"Controller", "UserLogin"},
                        {"Action", "UserLogin"}
                    });
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
