﻿using FSM.OfficeBAU.com.Content.Utility;
using FSM.OfficeBAU.com.Models;
using FSM.OfficeBAU.com.Models.BO;
using System.Web.Mvc;

namespace FSM.OfficeBAU.com.Controllers
{
    [SessionExpire]
    public class ReportsController : Controller
    {
        #region Variables

        private readonly ReportRepository reportRepository = new ReportRepository();
        private readonly CommonRepository commonRepository = new CommonRepository();

        #endregion

        #region Expense Reports

        public ActionResult ExpenseReports(ReportsBo reportsBo)
        {
            Utility.SetSession("MenuRetention", "REPORTS");
            Utility.SetSession("SubMenuRetention", "EXPENSEREPORT");
            Utility.TrackFormNavigation("Account - Reports");

            ViewData["ExpenseReportList"] = reportRepository.SearchExpenseReport(reportsBo);
            return View(reportsBo);
        }

        public ActionResult SearchExpenseReports(ReportsBo reportsBo)
        {
            return PartialView(reportRepository.SearchExpenseReport(reportsBo));
        }

        #endregion Expense Reports

        #region Income Reports

        public ActionResult IncomeReports(ReportsBo reportsBO)
        {
            Utility.SetSession("MenuRetention", "REPORTS");
            Utility.SetSession("SubMenuRetention", "INCOMEREPORT");

            ViewData["IncomeReportList"] = reportRepository.SearchIncomeReport(reportsBO);
            return View(reportsBO);
        }

        public ActionResult SearchIncomeReports(ReportsBo reportsBO)
        {
            return PartialView(reportRepository.SearchIncomeReport(reportsBO));
        }

        #endregion Income Reports

        #region AutoComplete 

        public ActionResult AutoCompleteVendor(string vendorName)
        {
            return Json(commonRepository.SearchAutoCompleteParameter("Vendor", "Name", vendorName), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AutoCompleteCustomer(string customerName)
        {
            return Json(commonRepository.SearchAutoCompleteParameter("Customer", "Name", customerName), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
