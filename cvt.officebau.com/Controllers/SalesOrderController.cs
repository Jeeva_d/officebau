﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mime;
using System.Text;
using System.Web.Mvc;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class SalesOrderController : Controller
    {
        // GET: SalesOrder
        public ActionResult Index(string menuCode, int? id, string screenType)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "SALES");
            Utility.SetSession("SubMenuRetention", "SALESORDER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Income");
            SearchParamsBo searchParamsBo = new SearchParamsBo
            {
                Id = id ?? 0,
                ScreenType = screenType
            };
            return View(searchParamsBo);
        }

        public ActionResult SearchSalesOrder(SearchParamsBo searchParamsBOs)
        {
            return PartialView(_salesOrderRepository.SearchSalesOrder(searchParamsBOs));
        }

        #region Variables

        private readonly SalesOrderRepository _salesOrderRepository = new SalesOrderRepository();

        #endregion Variables

        #region SalesOrder

        public ActionResult SalesOrder(int id, string localScreenType, string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "SALES");
            Utility.SetSession("SubMenuRetention", "SALESORDER");

            ItemDetailsBO itemDetailsBo = new ItemDetailsBO();

            IncomeBo incomeBo = _salesOrderRepository.GetSalesOrder(id);
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            if (id == 0)
            {
                incomeBo.ItemDetail = new List<ItemDetailsBO> { itemDetailsBo };

                itemDetailsBo.ProductList = masterDataRepository.GetPreRequestProduct("Sales");
            }

            if (localScreenType != null)
            {
                incomeBo.ScreenType = localScreenType;
            }

            incomeBo.MenuCode = menuCode;
            incomeBo.Emailhtml = PopulateMailBody(incomeBo);
            return PartialView(incomeBo);
        }

        [HttpPost]
        public ActionResult SalesOrder(IncomeBo incomeBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (incomeBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), incomeBo.MenuCode, incomeBo.Id == 0 ? "Save" : "Edit");
            }

            if (incomeBo != null && incomeBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), incomeBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            if (incomeBo.DiscountPercentage != 0)
            {
                incomeBo.DiscountPercentage = incomeBo.DiscountPercentage / 100;
            }

            incomeBo.ScreenType = "SALESORDER";
            return Json(_salesOrderRepository.ManageSalesOrder(incomeBo), JsonRequestBehavior.AllowGet);
        }

        public string PopulateMailBody(IncomeBo income)
        {
            string body;
            using (StreamReader reader = new StreamReader(HttpContext.Server.MapPath("~/Content/Mail-Template/SalesOrderEmail.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{InvNo}", income.InvoiceNo);
            body = body.Replace("{invDate}", Convert.ToDateTime(income.InvoiceDate).ToString("dd-MMMM-yyyy"));
            body = body.Replace("{Amount}", Utility.PrecisionForDecimal(income.TotalAmount, 2));
            body = body.Replace("{Companyemail}", income.CompanyEmaild);
            body = body.Replace("{Description}", income.Description);
            body = body.Replace("{companyName}", income.CompanyName);

            return body;
        }

        public ActionResult SendInvoiceEmail(int id, string toid, string ccid, string bccid, string subject)
        {
            IncomeBo inv = _salesOrderRepository.GetSalesOrder(id);
            UtilityRepository email = new UtilityRepository();
            string msg =BOConstants.Email_Not_Configured;
            if (!string.IsNullOrWhiteSpace(inv.ToEmail))
            {
                email.SendEmail(toid, ccid,
                    PopulateMailBody(inv), subject, GetInvoicePdfByteArray(inv),
                    bccid);
                msg =BOConstants.Email_Sent;
            }

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public string GetInvoicePdfByteArray(IncomeBo inv)
        {
            string uploadFolder = ConfigurationManager.AppSettings["UploadPath"];

            string html = RenderRazorViewToString("SalesOrderPrint", inv);
            string pdf_page_size = "A4";
            PdfPageSize pageSize = (PdfPageSize)Enum.Parse(typeof(PdfPageSize),
                pdf_page_size, true);

            string pdf_orientation = "Portrait";
            PdfPageOrientation pdfOrientation =
                (PdfPageOrientation)Enum.Parse(typeof(PdfPageOrientation),
                    pdf_orientation, true);

            int webPageWidth = 1024;

            int webPageHeight = 0;

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // set converter options
            converter.Options.PdfPageSize = pageSize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;

            // create a new pdf document converting an url
            PdfDocument doc = converter.ConvertHtmlString(html, "");

            // save pdf document
            doc.Save(uploadFolder + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("SALESORDER") + inv.CompanyName + "_" + inv.InvoiceNo + ".pdf");
            // close pdf document
            doc.Close();

            return uploadFolder + Utility.GetSession("CompanyName") + "\\" + Utility.UploadUrl("SALESORDER") + inv.CompanyName + "_" + inv.InvoiceNo + ".pdf";
        }

        public ActionResult SalesOrderdownload(int inid)
        {
            IncomeBo inv;
            inv = _salesOrderRepository.GetSalesOrder(inid);

            int afterdecimal = Convert.ToInt32(inv.TotalAmount.ToString("0.00").Split('.')[1]);
            inv.Totalinwords = NumberToText((int)inv.TotalAmount, true, false) + " Rupees And " + NumberToText(afterdecimal, true, false) + " Paisa Only.";
            return File(GetInvoicePdfByteArray(inv), MediaTypeNames.Application.Pdf, inv.CompanyName + "_" + inv.InvoiceNo + ".pdf");
        }

        public static string NumberToText(int number, bool useAnd, bool useArab)
        {
            if (number == 0)
            {
                return "Zero";
            }

            string and = useAnd ? "and " : ""; // deals with using 'and' separator

            if (number == -2147483648)
            {
                return "Minus Two Hundred " + and + "Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred " + and + "Forty Eight";
            }

            int[] num = new int[4];
            int first = 0;
            int u, h, t;
            StringBuilder sb = new StringBuilder();

            if (number < 0)
            {
                sb.Append("Minus ");
                number = -number;
            }

            string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
            string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };
            num[0] = number % 1000; // units
            num[1] = number / 1000;
            num[2] = number / 100000;
            num[1] = num[1] - 100 * num[2]; // thousands
            num[3] = number / 10000000; // crores
            num[2] = num[2] - 100 * num[3]; // lakhs
            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }

            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0)
                {
                    continue;
                }

                u = num[i] % 10; // ones
                t = num[i] / 10;
                h = num[i] / 100; // hundreds
                t = t - 10 * h; // tens

                if (h > 0)
                {
                    sb.Append(words0[h] + "Hundred ");
                }

                if (u > 0 || t > 0)
                {
                    if (h > 0 || i < first)
                    {
                        sb.Append(and);
                    }

                    if (t == 0)
                    {
                        sb.Append(words0[u]);
                    }
                    else if (t == 1)
                    {
                        sb.Append(words1[u]);
                    }
                    else
                    {
                        sb.Append(words2[t - 2] + words0[u]);
                    }
                }

                if (i != 0)
                {
                    sb.Append(words3[i - 1]);
                }
            }

            string temp = sb.ToString().TrimEnd();

            if (useArab && Math.Abs(number) >= 1000000000)
            {
                int index = temp.IndexOf("Hundred Crore", StringComparison.Ordinal);
                if (index > -1)
                {
                    return temp.Substring(0, index) + "Arab" + temp.Substring(index + 13);
                }

                index = temp.IndexOf("Hundred", StringComparison.Ordinal);
                return temp.Substring(0, index) + "Arab" + temp.Substring(index + 7);
            }

            return temp;
        }

        public ActionResult SalesOrderPrint(int inid)
        {
            return View(_salesOrderRepository.GetSalesOrder(inid));
        }

        #endregion SalesOrder
    }
}