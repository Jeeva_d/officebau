﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class DashBoardController : Controller
    {
        #region DashBoard

        public ActionResult DashBoard()
        {
            Utility.SetSession("MenuRetention", "DASHBOARDANDREPORTS");
            Utility.SetSession("SubMenuRetention", "ACCOUNTS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Accounts Dashboard");

            return View();
        }

        #endregion DashBoard

        #region DashBoard TileDetails

        public ActionResult DashBoardTileDetails()
        {
            return Json(dashBoardRepository.GetAccountsDashBoardTileDetails(), JsonRequestBehavior.AllowGet);
        }

        #endregion DashBoard TileDetails

        #region DashBoard Payable Details

        public ActionResult PayableDetails()
        {
            return Json(dashBoardRepository.GetDashBoardPayable(), JsonRequestBehavior.AllowGet);
        }

        #endregion DashBoard Payable Details

        #region DashBoard Receivables Details

        public ActionResult ReceivablesDetails()
        {
            return Json(dashBoardRepository.GetDashBoardReceivables(), JsonRequestBehavior.AllowGet);
        }

        #endregion DashBoard Receivables Details

        #region DashBoard PandL Details

        public ActionResult ProfitAndLossDetails()
        {
            return Json(dashBoardRepository.GetDashBoardPandL(), JsonRequestBehavior.AllowGet);
        }

        #endregion DashBoard PandL Details

        #region DashBoard Agging Expense

        public ActionResult AgingExpense()
        {
            return Json(dashBoardRepository.GetDashBoardAggingExpense(), JsonRequestBehavior.AllowGet);
        }

        #endregion DashBoard Agging Expense

        #region DashBoard Recent History

        public ActionResult RecentHistory()
        {
            return PartialView(dashBoardRepository.GetDashBoardRecentModified());
        }

        #endregion DashBoard Recent History

        #region

        public ActionResult GetAccDetails(SearchParamsBo searchParamsBo)
        {
            return PartialView(dashBoardRepository.GetAccDetails(searchParamsBo));
        }

        #endregion

        #region Variables

        private readonly DashBoardRepository dashBoardRepository = new DashBoardRepository();
        private readonly CommonRepository _commonRepository = new CommonRepository();
        private string _formName = string.Empty;

        #endregion

        #region DashBoard BankDetails

        public ActionResult BankDetails()
        {
            return PartialView(dashBoardRepository.GetDashboardBankDetails());
        }

        public ActionResult SystemBankDetails()
        {
            return PartialView(dashBoardRepository.GetDashboardSystemBankDetails());
        }

        #endregion

        #region BalanceSheet

        public ActionResult SearchBalanceSheet(string menuCode)
        {
            _formName = "BALANCESHEET";
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "DASHBOARDANDREPORTS");
            Utility.SetSession("SubMenuRetention", "BALANCESHEET");
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));
            searchParamsBo.StartMonth = Convert.ToInt32(Session["StartMonth"]);
            return View(searchParamsBo);
        }

        public ActionResult BalanceSheet(SearchParamsBo searchParamsBo)
        {
            _formName = "BALANCESHEET";

            System.Collections.Generic.List<BankBo> list = dashBoardRepository.GetBalanceSheet("BalanceSheet", searchParamsBo).OrderBy(a => a.Description).ToList();

            System.Collections.Generic.List<BankBo> listpandl = dashBoardRepository.GetBalanceSheet("PAndL", searchParamsBo);
            decimal expense = listpandl.Where(a => a.UserMessage.ToUpper() == "EXPENSE").Sum(a => a.OpeningBalance);
            decimal income = listpandl.Where(a => a.UserMessage.ToUpper() == "INCOME").Sum(a => a.OpeningBalance);

            BankBo bankBo = new BankBo();

            if (income > expense)
            {
                bankBo.UserMessage = "Liabilities";
                bankBo.Description = "Profit & Loss";
                bankBo.AccountNo = "Profit";
                bankBo.OpeningBalance = income - expense;
                list.Add(bankBo);
            }
            else
            {
                bankBo.UserMessage = "Assets";
                bankBo.AccountNo = "Loss";
                bankBo.Description = "Profit & Loss";
                bankBo.OpeningBalance = expense - income;
                list.Add(bankBo);
            }

            return View(list);
        }

        #endregion

        #region PAndL

        public ActionResult SearchPAndL(string menuCode)
        {
            _formName = "PANDL";
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "DASHBOARDANDREPORTS");
            Utility.SetSession("SubMenuRetention", "PROFITANDLOSS");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            SearchParamsBo searchParamsBo = new SearchParamsBo();

            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();
            searchParamsBo.IsCostCenter = applicationConfigurationRepository.SearchCostCenter();

            searchParamsBo.CostCenterList = masterDataRepository.SearchMasterDataDropDown("CostCenter");
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return View(searchParamsBo);
        }

        public ActionResult PAndL(SearchParamsBo searchParamsBo)
        {
            _formName = "PANDL";
            System.Collections.Generic.List<BankBo> list = dashBoardRepository.GetBalanceSheet("PAndL", searchParamsBo);
            decimal expense = list.Where(a => a.UserMessage.ToUpper() == "EXPENSE").Sum(a => a.OpeningBalance);
            decimal income = list.Where(a => a.UserMessage.ToUpper() == "INCOME").Sum(a => a.OpeningBalance);

            BankBo bankBo = new BankBo();

            if (income > expense)
            {
                bankBo.UserMessage = "EXPENSE";
                bankBo.AccountNo = "Profit";
                bankBo.Description = "Profit C/F";
                bankBo.OpeningBalance = income - expense;
                list.Add(bankBo);
            }
            else
            {
                bankBo.UserMessage = "INCOME";
                bankBo.AccountNo = "Loss";
                bankBo.Description = "Loss C/F";
                bankBo.OpeningBalance = expense - income;
                list.Add(bankBo);
            }

            return View(list);
        }

        #endregion

        #region TrailBalance

        public ActionResult SearchTrailBalance(string menuCode)
        {
            _formName = "TRAILBALANCE";
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "DASHBOARDANDREPORTS");
            Utility.SetSession("SubMenuRetention", "TRAILBALANCE");
            SearchParamsBo searchParamsBo = new SearchParamsBo();
            _commonRepository.GetSearchDataHistory(searchParamsBo, "pageload", _formName, Convert.ToInt32(Session["StartMonth"]));

            return View(searchParamsBo);
        }

        public ActionResult TrailBalance(SearchParamsBo searchParamsBo)
        {
            _formName = "TRAILBALANCE";
            System.Collections.Generic.List<BankBo> listBalance = dashBoardRepository.GetBalanceSheet("BalanceSheet", searchParamsBo);
            System.Collections.Generic.List<BankBo> listPAndL = dashBoardRepository.GetBalanceSheet("PAndL", searchParamsBo);
            listBalance.AddRange(listPAndL);
            return View(listBalance);
        }

        public ActionResult PayableDrillDown(string tileType)
        {
            return PartialView(dashBoardRepository.SearchPayablesTileDetails(tileType));
        }

        public ActionResult ReceivablesDrillDown(string tileType)
        {
            return PartialView(dashBoardRepository.SearchReceivablesTileDetails(tileType));
        }

        #endregion
    }
}