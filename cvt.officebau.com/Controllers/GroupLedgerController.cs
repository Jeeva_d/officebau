﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class GroupLedgerController : Controller
    {
        #region GroupLedger

        public ActionResult GroupLedger(string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }
            Utility.SetSession("MenuRetention", "MASTER");
            Utility.SetSession("SubMenuRetention", "GROUP");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Group Ledger");

            GroupLedgerBo groupLedgerBo = new GroupLedgerBo();
            _groupLedgerRepository = new GroupLedgerRepository();

            return View(_groupLedgerRepository.SearchGroupLedger(groupLedgerBo));
        }

        #endregion GroupLedger

        #region Variables

        private GroupLedgerRepository _groupLedgerRepository = new GroupLedgerRepository();

        #endregion Variables

        #region Create GroupLedger

        public ActionResult CreateGroupLedger(int groupLedgerId, string menuCode)
        {
            GroupLedgerBo groupLedgerBo = _groupLedgerRepository.GetGroupLedger(groupLedgerId);
            groupLedgerBo.MenuCode = menuCode;
            CommonRepository commonRepository = new CommonRepository();
            groupLedgerBo.ReportTypeIdList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "GroupType", "Type");
            return PartialView(groupLedgerBo);
        }

        [HttpPost]
        public ActionResult CreateGroupLedger(GroupLedgerBo groupLedgerBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (groupLedgerBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), groupLedgerBo.MenuCode, groupLedgerBo.Id == 0 ? "Save" : "Edit");
            }

            if (groupLedgerBo != null && groupLedgerBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), groupLedgerBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            string result = _groupLedgerRepository.ManageGroupLedger(groupLedgerBo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion Create GroupLedger
    }
}