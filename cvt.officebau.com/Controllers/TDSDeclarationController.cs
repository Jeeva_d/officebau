﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class TDSDeclarationController : Controller
    {
        #region Variable

        private readonly TdsDeclarationRepository _repository = new TdsDeclarationRepository();

        #endregion Variable

        #region TDS Configuration

        #region Get

        public ActionResult TdsConfiguration()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("TDS Declaration Configuration");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            TdsDeclarationBo tdsDeclarationBo = new TdsDeclarationBo
            {
                BusinessUnitList = masterDataRepository.GetRegionDropDownDependOnEmployee(Utility.UserId())
            };
            return PartialView(tdsDeclarationBo);
        }

        public ActionResult GetTdsConfiguration(int regionId)
        {
            TdsDeclarationBo tdsDeclarationBo = _repository.GetTdsConfiguration(regionId);
            tdsDeclarationBo.BusinessUnitId = regionId;
            return PartialView(tdsDeclarationBo);
        }

        #endregion Get

        #region Create

        [HttpPost]
        public JsonResult TdsConfiguration(TdsDeclarationBo tdsDeclarationBo)
        {
            List<TdsDeclarationBo> list = new List<TdsDeclarationBo>();
            if (tdsDeclarationBo != null)
            {
                TdsDeclarationBo tdsConfigurationBo = new TdsDeclarationBo();
                list.Add(tdsConfigurationBo);
                list[0].Value = tdsDeclarationBo.MonthOpenDate.ToString();
                list[0].Code = "TDSMOD";

                tdsConfigurationBo = new TdsDeclarationBo();
                list.Add(tdsConfigurationBo);
                list[1].Value = tdsDeclarationBo.MonthCloseDate.ToString();
                list[1].Code = "TDSMCD";

                tdsConfigurationBo = new TdsDeclarationBo();
                list.Add(tdsConfigurationBo);
                list[2].Value = string.Format("{0:dd-MMM-yyyy}", tdsDeclarationBo.ProofOpenDate);
                list[2].Code = "TDSPOD";

                tdsConfigurationBo = new TdsDeclarationBo();
                list.Add(tdsConfigurationBo);
                list[3].Value = string.Format("{0:dd-MMM-yyyy}", tdsDeclarationBo.ProofCloseDate);
                list[3].Code = "TDSPCD";
            }

            if (tdsDeclarationBo != null)
                return Json(_repository.ManageTdsConfiguration(list, tdsDeclarationBo.BusinessUnitId));
            else
                return Json(string.Empty);
        }

        #endregion Create

        #endregion TDS Configuration

        #region Employee TDS Declaration

        #region Main View

        public ActionResult TdsDeclaration(string menuCode)
        {
            Utility.SetSession("MenuRetention", "TDS");
            Utility.SetSession("SubMenuRetention", "ITDECLARATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("IT DECLARATION");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            TdsDeclarationBo tdsDeclarationBo = new TdsDeclarationBo
            {
                FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty)
            };
            UtilityRepository utilityRepository = new UtilityRepository();
            tdsDeclarationBo.FinancialYearList = utilityRepository.GetFinancialYearText();
            return View(tdsDeclarationBo);
        }

        #endregion Main View

        #region Create

        [HttpPost]
        public ActionResult CreateTdsDeclaration(List<TdsDeclarationBo> tdsDeclarationBoList)
        {
            RbsRepository rbsRepository = new RbsRepository();
            TdsDeclarationBo tdsDeclarationBo = new TdsDeclarationBo
            {
                MenuCode = tdsDeclarationBoList[0].ScreenType == 1 ? MenuCode.ITDeclaration_HR : MenuCode.ITDeclaration
            };

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), tdsDeclarationBo.MenuCode, tdsDeclarationBoList[0].Id == 0 ? "Save" : "Edit");
            return !hasAccess ? Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet) :
                Json(_repository.ManageTdsDeclaration(tdsDeclarationBoList));
        }

        [HttpPost]
        public ActionResult CreateTdsHouseTax(TdsDeclarationBo tdsDeclarationBo)
        {
            return Json(_repository.ManageTdsHouseTax(tdsDeclarationBo));
        }

        #endregion Create

        #region List

        public ActionResult EmployeeTdsDeclarationList(int? financialYearId, int employeeId)
        {
            TdsDeclarationBo tdsDeclarationBo = new TdsDeclarationBo();

            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            else
            {
                tdsDeclarationBo.EmployeeId = employeeId;
            }

            return PartialView(_repository.SearchTdsDeclaration(employeeId, false, financialYearId, "housing"));
        }

        public ActionResult EmployeeTdsDeclarationSubList(int? financialYearId)
        {
            return PartialView(_repository.SearchTdsDeclaration(Utility.UserId(), false, financialYearId, "IT"));
        }

        public ActionResult EmployeeTdsDeclaration()
        {
            Utility.SetSession("MenuRetention", "TDS");
            Utility.SetSession("SubMenuRetention", "ITDECLARATION-HR");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("IT DECLARATION-HR");
            UtilityRepository utilityRepository = new UtilityRepository();

            TdsDeclarationBo tdsDeclarationBo = new TdsDeclarationBo
            {
                FinancialYearList = utilityRepository.GetFinancialYearText()
            };

            return View(tdsDeclarationBo);
        }

        public ActionResult AutoCompleteEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployee(searchName), JsonRequestBehavior.AllowGet);
        }

        #endregion List

        #endregion Employee TDS Declaration

        #region TDS Clearance

        #region Main View

        public ActionResult TdsClearance(string menuCode)
        {
            Utility.SetSession("MenuRetention", "TDS");
            Utility.SetSession("SubMenuRetention", "ITCLEARANCE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("IT Clearance");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            UtilityRepository utilityRepository = new UtilityRepository();

            TdsDeclarationBo tdsDeclarationBo = new TdsDeclarationBo
            {
                EmployeeList = masterDataRepository.SearchMasterDataDropDown("EmployeeMaster", string.Empty),
                FinancialYearList = utilityRepository.GetFinancialYearText(),
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return View(tdsDeclarationBo);
        }

        #endregion Main View

        #region Process

        [HttpPost]
        public ActionResult ProcessTdsDeclaration(List<TdsDeclarationBo> tdsDeclarationBoList)
        {
            return Json(_repository.ProcessTdsDeclaration(tdsDeclarationBoList));
        }

        [HttpPost]
        public ActionResult ProcessTdsHouseTax(TdsDeclarationBo tdsDeclarationBo)
        {
            tdsDeclarationBo.IsApprover = true;
            return Json(_repository.ManageTdsHouseTax(tdsDeclarationBo));
        }

        #endregion Process

        #region List

        public ActionResult EmployeeList(TdsDeclarationBo tdsDeclarationBo)
        {
            return PartialView(_repository.SearchEmployee(tdsDeclarationBo));
        }

        public ActionResult TdsDeclarationList(int employeeId, int? startYear)
        {
            return PartialView(_repository.SearchTdsDeclaration(employeeId, true, startYear));
        }

        #endregion List

        #endregion TDS Clearance
    }
}