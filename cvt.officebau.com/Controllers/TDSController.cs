﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.Mvc;
using cvt.officebau.com.MessageConstants;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class TDSController : Controller
    {
        #region Varaibles

        private readonly TdsRepository _repository = new TdsRepository();

        #endregion Varaibles

        #region Employee List

        public ActionResult EmployeeTdsList(int monthId, int yearId, string businessUnitIDs)
        {
            return PartialView(_repository.SearchEmployeeTds(monthId, yearId, businessUnitIDs, string.Empty));
        }

        #endregion Employee List

        #region Upload

        public JsonResult Upload(HttpPostedFileBase file)
        {
            List<string> list = new List<string>();
            if (file != null)
            {
                list = UploadUtility.UploadFile(file, Utility.UploadUrl("FORM16"));
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion Upload

        #region TDS

        public ActionResult Tds()
        {
            Tdsbo tdsBo = new Tdsbo();

            List<Tdsbo> yearList = _repository.SearchFinancialYear(tdsBo);
            ViewData["yearList"] = yearList;
            return View(tdsBo);
        }

        public ActionResult FinancialMonth(int yearId)
        {
            List<Tdsbo> monthList = _repository.SearchCurrentFinancialMonth(yearId);
            return PartialView(monthList);
        }

        public ActionResult RunEmployeeTds()
        {
            Utility.SetSession("MenuRetention", "TDS");
            Utility.SetSession("SubMenuRetention", "PROCESSTDS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("PROCESS TDS");

            Tdsbo tdsDetailsBo = new Tdsbo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            tdsDetailsBo.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            UtilityRepository utilityRepository = new UtilityRepository();
            tdsDetailsBo.YearList = utilityRepository.GetFinancialYearText();
            tdsDetailsBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            tdsDetailsBo.MonthList.RemoveAt(0);
            tdsDetailsBo.YearList.RemoveAt(0);
            tdsDetailsBo.BusinessUnitList.RemoveAt(0);
            return View(tdsDetailsBo);
        }

        #endregion TDS

        #region My TDS

        public ActionResult TDS_Employee()
        {
            TdsHrabo tdsHrabo = new TdsHrabo();
            Utility.SetSession("MenuRetention", "TDS");
            Utility.SetSession("SubMenuRetention", "MYTDS");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("My TDS");
            UtilityRepository utilityRepository = new UtilityRepository();

            tdsHrabo.FinancialYearList = utilityRepository.GetFinancialYearText();
            return View(tdsHrabo);
        }

        public ActionResult SearchEmployeeTds(int? financialYearId)
        {
            TdsHrabo tdsHrabo = new TdsHrabo();
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("My TDS");

            tdsHrabo.EmployeeId = Utility.UserId();
            tdsHrabo.FinancialYearId = Convert.ToInt32(financialYearId);
            return PartialView(_repository.SearchemployeeTdsList(tdsHrabo, string.Empty));
        }

        #region EmployeeList

        public ActionResult EmployeeList(TdsHrabo tdsHrabo)
        {
            return PartialView(_repository.SearchEmployeeTds(tdsHrabo));
        }

        #endregion EmployeeList

        public ActionResult TdsCalculationResult(int monthId, int yearId, int employeeId, string payrollProcessed)
        {
            TdsCalculator tds = new TdsCalculator();
            return PartialView(tds.Calculatedetails(employeeId, yearId, monthId, Utility.DomainId(), payrollProcessed));
        }

        #endregion My TDS

        #region TDS Other Component

        public ActionResult SearchTdsOtherComponent()
        {
            TdsComponentBo tdsComponentBo = new TdsComponentBo();
            Utility.SetSession("MenuRetention", "TDS");
            Utility.SetSession("SubMenuRetention", "OTHERTDSCOMPONENT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Other TDS Component");

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            tdsComponentBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            tdsComponentBo.BusinessUnitList.RemoveAt(0);
            tdsComponentBo.BusinessUnitList.Insert(0, new SelectListItem { Text = "All", Value = "" });
            tdsComponentBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            UtilityRepository utilityRepository = new UtilityRepository();
            tdsComponentBo.FinancialYearList = utilityRepository.GetFinancialYearText();
            return View(tdsComponentBo);
        }

        public ActionResult TdsOtherComponentList(TdsComponentBo tdsComponentBo)
        {
            TdsRepository tdsRepository = new TdsRepository();

            return PartialView(tdsRepository.SearchTdsOtherComponent(tdsComponentBo));
        }

        public ActionResult TdsComponentExcelUpload()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            TdsComponentBo tdsComponentBo = new TdsComponentBo { BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown() };

            tdsComponentBo.BusinessUnitList.RemoveAt(0);
            tdsComponentBo.BusinessUnitList.Insert(0, new SelectListItem { Text = "All", Value = "" });
            UtilityRepository utilityRepository = new UtilityRepository();
            tdsComponentBo.FinancialYearList = utilityRepository.GetFinancialYearText();
            return PartialView(tdsComponentBo);
        }

        [HttpPost]
        public ActionResult ExcelUploadTdsComponent(HttpPostedFileBase file, int yearId, string yearName)
        {
            DataSet ds = new DataSet();
            string message = string.Empty, fileLocation, fileName;

            // ReSharper disable once PossibleNullReferenceException
            if (Request.Files != null && Request.Files["file"].ContentLength > 0)
            {
                string fileExtension = Path.GetExtension(Request.Files["file"].FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    fileLocation = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\LOP";
                    fileName = Server.MapPath("~/Content/Payroll/") + Session["CompanyName"] + "\\LOP\\" + file.FileName;

                    if (!Directory.Exists(fileLocation))
                    {
                        Directory.CreateDirectory(fileLocation);
                    }

                    if (System.IO.File.Exists(fileName))
                    {
                        System.IO.File.Delete(fileName);
                    }

                    file.SaveAs(fileLocation + "\\" + file.FileName);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        file.InputStream.CopyTo(ms);
                        ms.GetBuffer();
                    }

                    Request.Files["file"].SaveAs(fileName);

                    string excelConnectionString = string.Empty;
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileName + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    else if (fileExtension == ".xlsx")
                    {
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }

                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    string[] excelSheets = new string[dt.Rows.Count];
                    int t = 0;
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }

                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }

                    excelConnection.Close();
                }

                string conn = ConfigurationManager.ConnectionStrings["Conn"].ConnectionString;

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Columns[0].ToString() == "EmpCode" && ds.Tables[0].Rows[i][2].ToString().ToUpper().Trim() == yearName.ToUpper().Trim())
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[i][2].ToString()) != 0)
                        {
                            decimal bonus = ds.Tables[0].Rows[i][3].ToString() == "" ? 0 : Convert.ToDecimal(ds.Tables[0].Rows[i][3].ToString());
                            decimal pli = ds.Tables[0].Rows[i][4].ToString() == "" ? 0 : Convert.ToDecimal(ds.Tables[0].Rows[i][4].ToString());
                            decimal otherIncome = ds.Tables[0].Rows[i][5].ToString() == "" ? 0 : Convert.ToDecimal(ds.Tables[0].Rows[i][5].ToString());

                            if (bonus != 0 || pli != 0 || otherIncome != 0)
                            {
                                using (SqlConnection connection = new SqlConnection(conn))
                                {
                                    using (SqlCommand commd = new SqlCommand(Constants.UPLOAD_TDSCOMPONENT, connection))
                                    {
                                        commd.CommandType = CommandType.StoredProcedure;
                                        commd.Parameters.AddWithValue(DBParam.Input.EmployeeCode, ds.Tables[0].Rows[i][0].ToString());
                                        commd.Parameters.AddWithValue(DBParam.Input.EmployeeName, ds.Tables[0].Rows[i][1].ToString().Replace(" ", ""));
                                        commd.Parameters.AddWithValue(DBParam.Input.YearID, Convert.ToInt32(ds.Tables[0].Rows[i][2]));
                                        commd.Parameters.AddWithValue(DBParam.Input.Bonus, bonus);
                                        commd.Parameters.AddWithValue(DBParam.Input.PLI, pli);
                                        commd.Parameters.AddWithValue(DBParam.Input.OtherIncome, otherIncome);
                                        commd.Parameters.AddWithValue(DBParam.Input.DomainID, Utility.DomainId());
                                        commd.Parameters.AddWithValue(DBParam.Input.UserID, Utility.UserId());
                                        connection.Open();
                                        using (SqlDataReader reader = commd.ExecuteReader())
                                        {
                                            while (reader.Read())
                                            {
                                                message = reader["Output"].ToString();
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                message = BOConstants.Uploaded;
                            }
                        }
                    }
                    else
                    {
                        message = BOConstants.Excel_InValid;
                    }
                }
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTdsComponentPopup(int id)
        {
            TdsRepository tdsRepository = new TdsRepository();
            return PartialView(tdsRepository.GetTdsComponent(id));
        }

        [HttpPost]
        public ActionResult DeleteTdsComponent(int id)
        {
            TdsRepository tdsRepository = new TdsRepository();

            return Json(tdsRepository.DeleteTdsComponent(id), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTdsComponent(TdsComponentBo tdsComponentBo)
        {
            TdsRepository tdsRepository = new TdsRepository();

            return Json(tdsRepository.UpdateTdsComponent(tdsComponentBo), JsonRequestBehavior.AllowGet);
        }

        #endregion TDS Other Component
    }
}