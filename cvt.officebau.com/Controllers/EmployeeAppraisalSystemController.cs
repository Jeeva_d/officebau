﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeeAppraisalSystemController : Controller
    {
        #region Employee Appraisal

        public ActionResult SearchEmployeeAppraisal()
        {
            Utility.SetSession("MenuRetention", "APPRAISALSYSTEM");
            Utility.SetSession("SubMenuRetention", "SELFEVALUATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Self Evaluation");

            return View();
        }

        #endregion Employee Appraisal

        #region variable

        private readonly EmployeeAppraisalSystemRepository _employeeAppraisalSystemRepository = new EmployeeAppraisalSystemRepository();
        private AppraisalConfigBo _appraisalConfigBo = new AppraisalConfigBo();
        private readonly MasterDataRepository _masterDataRepository = new MasterDataRepository();

        private string _message = string.Empty;
        private bool _hasAccess;

       // public int QuestionId { get; private set; } // Anbu

        #endregion variable

        #region Appraisal System Configuration

        public ActionResult SearchAppraisalSystemConfiguration(AppraisalConfigBo appraisalConfigBo)
        {
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Appraisal System Configuration");

            appraisalConfigBo.BusinessUnitList = _masterDataRepository.GetBusinessUnitDropDown();
            return PartialView(appraisalConfigBo);
        }

        public ActionResult AppraisalSystemConfigurationList(AppraisalConfigBo appraisalConfigBo)
        {
            appraisalConfigBo.DomainId = Utility.DomainId();
            appraisalConfigBo.UserId = Utility.UserId(); // Anbu
            return PartialView(_employeeAppraisalSystemRepository.GetAppraisalSystemConfigurationList(appraisalConfigBo));
        }

        public ActionResult CreateAppraisalSystemConfiguration(int id)
        {
            if (id != 0)
            {
                _appraisalConfigBo = _employeeAppraisalSystemRepository.GetAppraisalSystemConfiguration(id);
            }
            _appraisalConfigBo.BusinessUnitList = _masterDataRepository.GetBusinessUnitDropDown();

            return PartialView(_appraisalConfigBo);
        }

        [HttpPost]
        public ActionResult CreateAppraisalSystemConfiguration(AppraisalConfigBo appraisalConfigBo)
        {
            appraisalConfigBo.ModifiedBy = Utility.UserId();
            appraisalConfigBo.CreatedOn = Utility.GetCurrentDate();
            appraisalConfigBo.ModifiedOn = Utility.GetCurrentDate();
            appraisalConfigBo.DomainId = Utility.DomainId();
            appraisalConfigBo.MenuCode = MenuCode.AppraisalConfiguration;

            RbsRepository rbsRepository = new RbsRepository();
            string message = string.Empty;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), appraisalConfigBo.MenuCode, appraisalConfigBo.Id == 0 ? "Save" : "Edit");

            if (appraisalConfigBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), appraisalConfigBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (appraisalConfigBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    message = _employeeAppraisalSystemRepository.CreateAppraisalSystemConfiguration(appraisalConfigBo);
                    break;

                case Operations.Update:
                    message = _employeeAppraisalSystemRepository.UpdateAppraisalSystemConfiguration(appraisalConfigBo);
                    break;

                case Operations.Delete:
                    appraisalConfigBo.IsDeleted = true;
                    message = _employeeAppraisalSystemRepository.DeleteAppraisalSystemConfiguration(appraisalConfigBo);
                    break;
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Appraisal System Configuration

        #region Questionnaire

        public ActionResult SearchQuestionnaire()
        {
            Utility.SetSession("MenuRetention", "APPRAISALSYSTEM");
            Utility.SetSession("SubMenuRetention", "QUESTIONNAIRE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Questionnaire");

            return View();
        }

        public ActionResult AddNewQuestionnaire(int id)
        {
            QuestionnaireBo questionnaireBo = new QuestionnaireBo();
            if (id != 0)
            {
                questionnaireBo = _employeeAppraisalSystemRepository.GetQuestions(id);
            }

            return PartialView(questionnaireBo);
        }

        public ActionResult QuestionList(QuestionnaireBo questionnaireBo)
        {
            return PartialView(_employeeAppraisalSystemRepository.GetQuestionList(questionnaireBo));
        }

        [HttpPost]
        public ActionResult ManageQuestions(QuestionnaireBo questionnaireBo)
        {
            questionnaireBo.ModifiedBy = Utility.UserId();
            questionnaireBo.DomainId = Utility.DomainId();
            questionnaireBo.MenuCode = MenuCode.EmployeeMaster;
            RbsRepository rbsRepository = new RbsRepository();

            _hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), questionnaireBo.MenuCode, questionnaireBo.Id == 0 ? "Save" : "Edit");

            if (!_hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            switch (questionnaireBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    questionnaireBo.CreatedBy = Utility.UserId();
                    _message = _employeeAppraisalSystemRepository.CreateNewQuestions(questionnaireBo);
                    break;

                case Operations.Update:
                    _message = _employeeAppraisalSystemRepository.UpdateQuestion(questionnaireBo);
                    break;

                case Operations.Delete:
                    _message = _employeeAppraisalSystemRepository.DeleteQuestion(questionnaireBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        #endregion Questionnaire
    }
}