﻿/*Code Review Done Neeed Proper Commands for all the methods and removed some unwanted local variables*/

using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class ExpenseController : Controller
    {
        #region Autocompleted

        public ActionResult AutoCompleteVendor(string vendorName)
        {
            CommonRepository commonRepository = new CommonRepository();
            return Json(commonRepository.SearchAutoCompleteParameter("Vendor", "Name", vendorName), JsonRequestBehavior.AllowGet);
        }

        #endregion Autocompleted

        #region Duplicate Name Check

        public ActionResult DuplicateCheck(string name, string table)
        {
            ExpenseBo expenseBo = new ExpenseBo();
            if (name.Trim() == string.Empty)
            {
                expenseBo.UserMessage = "1";
            }
            else
            {
                expenseBo = _expenseRepository.DuplicateCheck(name, table);
            }

            return Json(expenseBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion Duplicate Name Check

        #region Get Available CashBalance

        public ActionResult GetAvailableCashBalance()
        {
            ExpenseBo expenseBo = _expenseRepository.GetAvailableCashBalance();
            return Json(expenseBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion Get Available CashBalance

        public ActionResult PurchaseOrderDetails(int vendorId, string screenName)
        {
            return PartialView(_expenseRepository.SearchVendorPoDetails(vendorId, screenName));
        }

        #region Variables

        private readonly ExpenseRepository _expenseRepository = new ExpenseRepository();

        #endregion Variables

        #region Transaction

        public ActionResult SearchTransaction(string menuCode, int? id, int? vendorId, string screenType)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "PURCHASE");
            Utility.SetSession("SubMenuRetention", "EXPENSE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Expense");

            SearchParamsBo searchParamsBo = new SearchParamsBo();
            ApplicationConfigurationRepository applicationConfigurationRepository = new ApplicationConfigurationRepository();

            searchParamsBo.IsCostCenter = applicationConfigurationRepository.SearchCostCenter();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            searchParamsBo.CostCenterList = masterDataRepository.SearchMasterDataDropDown("CostCenter");
            searchParamsBo.Id = id ?? 0;
            searchParamsBo.VenderId = vendorId ?? 0;
            searchParamsBo.ScreenType = screenType;

            //commonRepository.GetSearchDataHistory(searchParamsBO, "pageload", FormName, Convert.ToInt32(Session["StartMonth"]));
            return View(searchParamsBo);
        }

        public ActionResult Transaction(SearchParamsBo searchParamsBo)
        {
            //if (searchParamsBO.Event == "search")
            //    commonRepository.GetSearchDataHistory(searchParamsBO, "search", FormName, Convert.ToInt32(Session["StartMonth"]));

            if (searchParamsBo.Event == "report")
            {
                return PartialView(_expenseRepository.SearchExpenseDuplicatedRecords(searchParamsBo));
            }

            return PartialView(_expenseRepository.SearchExpenseTransactionDetails(searchParamsBo));
        }

        #endregion Transaction

        #region Bill

        public ActionResult Bill(int id, string localScreenType, string menuCode, string poiDs, string vendorName, int? vId, int? costCenterId)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }
            ExpenseItemsBO expenseItemBo = new ExpenseItemsBO();
            ExpenseDetailsBO expenseDetailsBo = new ExpenseDetailsBO();
            ExpenseBo expenseBo = _expenseRepository.GetExpenseTransactionDetails(id);
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            if (id == 0)
            {
                expenseBo.ExpenseDetail = new List<ExpenseDetailsBO>();
                expenseBo.ExpenseItems = new List<ExpenseItemsBO>();
                expenseBo.ExpenseDetail.Add(expenseDetailsBo);
                expenseBo.ExpenseItems.Add(expenseItemBo);
                expenseDetailsBo.LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger");
                expenseItemBo.ProductList = masterDataRepository.GetPreRequestProduct("Purchase");
            }

            PurchaseOrderRepository purchaseOrderRepository = new PurchaseOrderRepository();
            if (!string.IsNullOrWhiteSpace(poiDs))
            {
                expenseBo.VendorId = Convert.ToInt32(vId);
                expenseBo.VendorName = vendorName;
                expenseBo.CostCenter = new Entity { Id = costCenterId ?? 0 };

                expenseBo.ExpenseDetail.AddRange(purchaseOrderRepository.SearchPoBillDetailList(poiDs));
                expenseBo.ExpenseDetail = expenseBo.ExpenseDetail.FindAll(a => a.Qty != 0);
            }

            expenseDetailsBo.LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger");
            expenseBo.CostCenterList = masterDataRepository.SearchMasterDataDropDown("CostCenter");
            expenseItemBo.ProductList = masterDataRepository.GetPreRequestProduct("Purchase");
            ApplicationConfigurationRepository applicationConfigrepository = new ApplicationConfigurationRepository();
            expenseBo.IsCostCenter = applicationConfigrepository.SearchCostCenter();
            expenseBo.LocalScreenType = localScreenType;
            expenseBo.MenuCode = menuCode;
            return PartialView(expenseBo);
        }

        [HttpPost]
        public ActionResult Bill(ExpenseBo expenseBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (expenseBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), expenseBo.MenuCode, expenseBo.Id == 0 ? "Save" : "Edit");
            }

            if (expenseBo != null && expenseBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), expenseBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            expenseBo.ScreenType = "Bill";
            string message = _expenseRepository.ManageBill(expenseBo);
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        #endregion Bill

        #region Expense

        public ActionResult Expenses(int id, string localScreenType, string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "VOUCHER");
            Utility.SetSession("SubMenuRetention", "EXPENSE");

            ExpenseItemsBO expenseItemsBo = new ExpenseItemsBO();
            ExpenseDetailsBO expenseDetailsBo = new ExpenseDetailsBO();
            ExpenseBo expenseBo = _expenseRepository.GetExpenseTransactionDetails(id);

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            CommonRepository commonRepository = new CommonRepository();
            ApplicationConfigurationRepository applicationConfigrepository = new ApplicationConfigurationRepository();
            expenseBo.IsCostCenter = applicationConfigrepository.SearchCostCenter();
            expenseBo.PaymentModeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");
            expenseBo.CostCenterList = masterDataRepository.SearchMasterDataDropDown("CostCenter");

            if (id == 0)
            {
                expenseBo.ExpenseDetail = new List<ExpenseDetailsBO>();
                expenseBo.ExpenseItems = new List<ExpenseItemsBO>();
                expenseBo.ExpenseDetail.Add(expenseDetailsBo);
                expenseBo.ExpenseItems.Add(expenseItemsBo);
                expenseDetailsBo.LedgerList = masterDataRepository.GetPreRequestProductLedger("Ledger");
                expenseItemsBo.ProductList = masterDataRepository.GetPreRequestProduct("Purchase");
            }

            expenseBo.MenuCode = menuCode;
            expenseBo.LocalScreenType = localScreenType;
            return PartialView(expenseBo);
        }

        [HttpPost]
        public ActionResult Expenses(ExpenseBo expenseBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (expenseBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), expenseBo.MenuCode, expenseBo.Id == 0 ? "Save" : "Edit");
            }

            if (expenseBo != null && expenseBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), expenseBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            expenseBo.ScreenType = "Expense";
            return Json(_expenseRepository.ManageExpense(expenseBo), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBankNameBasedonPaymentMode(string paymentMode)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            List<SelectListItem> bankList = masterDataRepository.GetBankNameBasedonPaymentMode(paymentMode);
            bankList.RemoveAt(0);
            return Json(bankList, JsonRequestBehavior.AllowGet);
        }

        #endregion Expense

        #region BillPay

        public ActionResult BillPay(int? id, string menuCode, int vendorId, string vendorName, string localScreenType, string methodName)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "PURCHASE");
            Utility.SetSession("SubMenuRetention", "EXPENSE");

            ExpenseBo expenseBo = new ExpenseBo();
            BillPayBo billPayBo = new BillPayBo();

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            CommonRepository commonRepository = new CommonRepository();

            if (vendorId == 0)
            {
                expenseBo.BillPay = new List<BillPayBo> { billPayBo };
            }
            if (methodName == "get")
            {
                expenseBo = _expenseRepository.GetBillPayDetails(vendorId);
            }
            else
            {
                expenseBo = _expenseRepository.GetVendorDetails(vendorId);
                expenseBo.BillPay = _expenseRepository.SearchBillDetailsList(id, vendorId, vendorName);
            }

            expenseBo.BankList = masterDataRepository.SearchMasterDataDropDown("Bank");
            expenseBo.PaymentModeList = commonRepository.SearchDependantDropDown(0, "CodeMaster", "PaymentType", "Type");

            expenseBo.LocalScreenType = localScreenType;
            expenseBo.MenuCode = menuCode;
            return PartialView(expenseBo);
        }

        [HttpPost]
        public ActionResult BillPay(ExpenseBo expenseBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (expenseBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), expenseBo.Id == 0 ? "Save" : "Edit");
            }

            if (expenseBo != null && expenseBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), Utility.GetSession("MenuID"), "Delete");
            }

            return !hasAccess ? Json(Constants.UnAuthorizedAccess) : Json(_expenseRepository.ManageBillPay(expenseBo), JsonRequestBehavior.AllowGet);
        }

        #endregion BillPay

        #region AddVendor

        public ActionResult Vendor()
        {
            VendorMasterBo vendorMasterBo = new VendorMasterBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            vendorMasterBo.CurrencyList = masterDataRepository.SearchMasterDataDropDown("Currency");
            return PartialView(vendorMasterBo);
        }

        [HttpPost]
        public ActionResult Vendor(VendorMasterBo vendorMasterBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = false;
            if (vendorMasterBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), vendorMasterBo.MenuCode, vendorMasterBo.Id == 0 ? "Save" : "Edit");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            VendorMasterRepository vendorMasterRepository = new VendorMasterRepository();
            string userMessage = vendorMasterRepository.ManageVendor(vendorMasterBo);
            return Json(userMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion AddVendor

        #region AddLedger

        public ActionResult Ledger()
        {
            LedgerBo ledgerBo = new LedgerBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            ledgerBo.GroupIdList = masterDataRepository.SearchMasterDataDropDown("GroupLedger");
            return PartialView(ledgerBo);
        }

        [HttpPost]
        public ActionResult Ledger(LedgerBo ledgerBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            LedgerRepository ledgerRepository = new LedgerRepository();
            bool hasAccess = false;
            if (ledgerBo != null)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), ledgerBo.MenuCode, ledgerBo.Id == 0 ? "Save" : "Edit");
            }

            return !hasAccess ? Json(Constants.UnAuthorizedAccess) : Json(ledgerRepository.ManageLedger(ledgerBo), JsonRequestBehavior.AllowGet);
        }

        #endregion AddLedger

        #region Upload Download

        public JsonResult Upload(HttpPostedFileBase file)
        {
            List<string> list = new List<string>();
            if (file != null)
            {
                list = UploadUtility.UploadFile(file, Utility.UploadUrl("EXPENSE"));
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteFile(string deleteFile)
        {
            //string result = string.Empty;
            //result = UploadUtility.DeleteUploadFile(deleteFile, Utility.DomainID(), Utility.UserID());

            return Json(Notification.Deleted, JsonRequestBehavior.AllowGet);
        }

        #endregion Upload Download

        #region HoldingDetails

        public ActionResult ExpenseHoldingDetails(int id)
        {
            return PartialView(_expenseRepository.SearchExpenseHoldingList(id));
        }

        [HttpPost]
        public ActionResult ExpenseHoldingDetails(IList<ExpenseHoldingBO> expenseHoldingBOs)
        {
            return Json(_expenseRepository.ManageExpenseHoldings(expenseHoldingBOs));
        }

        #endregion HoldingDetails
    }
}