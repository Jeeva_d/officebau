﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class TDSUpdateController : Controller
    {
        #region Variable

        private readonly TdsUpdateRepository _tdsUpdateRepository = new TdsUpdateRepository();

        #endregion Variable

        public ActionResult TdsUpdateDetails(string menuCode)
        {
            Utility.SetSession("MenuRetention", "PAYROLL");
            Utility.SetSession("SubMenuRetention", "TDSUPDATE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("TDS Update");

            TDSUpdateBo tdsUpdateBo = new TDSUpdateBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            tdsUpdateBo.MonthList = masterDataRepository.SearchMasterDataDropDown("Month", string.Empty);
            tdsUpdateBo.YearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            //tdsUpdateBo.YearList = Utility.GetFinancialYearText();
            tdsUpdateBo.BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown();
            tdsUpdateBo.MonthList.RemoveAt(0);
            tdsUpdateBo.YearList.RemoveAt(0);
            tdsUpdateBo.BusinessUnitList.RemoveAt(0);
            return View(tdsUpdateBo);
        }

        public ActionResult SearchCopyTdsMonthYear(int businessUnitId)
        {
            return Json(_tdsUpdateRepository.GetLastMonthYear(businessUnitId), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CopyTds(TDSUpdateBo tdsUpdateBo)
        {
            return Json(_tdsUpdateRepository.CopyTds(tdsUpdateBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchTdsDetails(TDSUpdateBo tdsUpdateBo)
        {
            return PartialView(_tdsUpdateRepository.SearchTdsDetails(tdsUpdateBo));
        }

        [HttpPost]
        public ActionResult ManageTdsDetails(List<TDSUpdateBo> tdsUpdateBoList)
        {
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.TDSUpdate, "Edit");
            return Json(!hasAccess ? Constants.UnAuthorizedAccess : _tdsUpdateRepository.ManageTdsDetails(tdsUpdateBoList), JsonRequestBehavior.AllowGet);
        }
    }
}