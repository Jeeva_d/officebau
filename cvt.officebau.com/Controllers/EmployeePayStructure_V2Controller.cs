﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class EmployeePayStructure_V2Controller : Controller
    {
        #region variables

        private readonly EmployeePayStructureRepositoryV2 _employeePayStructureRepositoryV2 = new EmployeePayStructureRepositoryV2();

        #endregion variables

        public ActionResult EmployeePay_Structure(string menuCode)
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Employee PayStructure");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeePayStructureBo_V2 employeePayStructureBo = new EmployeePayStructureBo_V2
            {
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            employeePayStructureBo.BusinessUnitList.RemoveAt(0);
            return PartialView(employeePayStructureBo);
        }

        public ActionResult EmployeePay_StructureDetail(EmployeePayStructureBo_V2 employeePayStructureBo)
        {
            List<EmployeePayStructureBo_V2> employeePayStructureBoList = _employeePayStructureRepositoryV2.SearchEmployee(employeePayStructureBo);
            return PartialView(employeePayStructureBoList);
        }

        public ActionResult CreateEmployeePayStructure(int id)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeePayStructureBo_V2 employeePayStructureBo = new EmployeePayStructureBo_V2();

            if (id != 0)
            {
                employeePayStructureBo = _employeePayStructureRepositoryV2.GetEmployeePayStructureDetails(id);
            }
            employeePayStructureBo.CompanyPayStructureList = masterDataRepository.SearchMasterDataDropDown("Pay_CompanyPayStructure", "");

            return PartialView(employeePayStructureBo);
        }

        public ActionResult AutoCompleteEmployee(string employeeName)
        {
            return Json(_employeePayStructureRepositoryV2.SearchAutoCompleteEmployee(employeeName), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmployeePayStructure(int payStructureId, decimal gross)
        {
            return PartialView(_employeePayStructureRepositoryV2.GetEmployeePayStructure(payStructureId, gross, new List<EmployeePayrollBo_v2>()));
        }

        public ActionResult CotractEmployeePayStructure(int employeeid, DateTime effFrom, string employeeName, int id = 0)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            EmployeePayStructureBo_V2 employeePayStructureBo = new EmployeePayStructureBo_V2
            {
                EmployeeId = employeeid,
                EffectiveFrom = effFrom,
                EmployeeName = employeeName
            };
            if (id != 0)
            {
                employeePayStructureBo = _employeePayStructureRepositoryV2.GetEmployeePayStructureDetails(id);
            }
            employeePayStructureBo.CompanyPayStructureList = masterDataRepository.SearchMasterDataDropDown("Pay_CompanyPayStructure", "");

            return PartialView(employeePayStructureBo);
        }


        [HttpPost]
        public ActionResult CreateEmployeePayStructure(EmployeePayStructureBo_V2 employeePayStructureBo)
        {
            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.EmployeePay, employeePayStructureBo.Id == 0 ? "Save" : "Edit");
            if (employeePayStructureBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.EmployeePay, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_employeePayStructureRepositoryV2.ManageEmployeePayStructure(employeePayStructureBo));
        }

        public ActionResult GetEmployeePayStructureOnEdit(EmployeePayStructureBo_V2 employeePayStructureBo)
        {
            List<EmployeePayrollBo_v2> list = new List<EmployeePayrollBo_v2>();
            foreach (CompanyPayStructureBo_V2 n in employeePayStructureBo.CompanyPayStructureBo)
            {
                list.Add(new EmployeePayrollBo_v2()
                {
                    ComponentId = n.ComponentId,
                    EmployeeId = 0,
                    Id = n.Id,
                    Amount = n.Amount
                });
            }
            return Json(_employeePayStructureRepositoryV2.GetEmployeePayStructure(employeePayStructureBo.CompanyPayStructureId, Convert.ToDecimal(employeePayStructureBo.Gross), list), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PayStubConfiguration()
        {
            List<EmployeePayStructureBo_V2> employeePayStructureBoV2List = _employeePayStructureRepositoryV2.GetPayStubComponentsList();
            return PartialView(employeePayStructureBoV2List);
        }

        [HttpPost]
        public ActionResult SubmitPayStubConfiguration(List<EmployeePayStructureBo_V2> employeePayStructureBoList)
        {
            RbsRepository rbsRepository = new RbsRepository();
            EmployeePayStructureBo_V2 employeePayStructureBo = new EmployeePayStructureBo_V2();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.CLAMSAPPROVALCONFIG, employeePayStructureBo.Id == 0 ? "Save" : "Edit");
            if (employeePayStructureBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), MenuCode.CLAMSAPPROVALCONFIG, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            employeePayStructureBo = _employeePayStructureRepositoryV2.ManagePayStubConfiguration(employeePayStructureBoList);
            return Json(employeePayStructureBo.UserMessage, JsonRequestBehavior.AllowGet);
        }
    }
}