﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class TravelController : Controller
    {
        #region Upload

        public JsonResult Upload(HttpPostedFileBase file)
        {
            List<string> list = new List<string>();
            if (file != null)
            {
                list = UploadUtility.UploadFile(file, Utility.UploadUrl("TRAVEL"));
            }

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion Upload

        #region variable

        private readonly MasterDataRepository _masterDataRepository = new MasterDataRepository();
        private readonly TravelRepository _travelRepository = new TravelRepository();

        private string _message = string.Empty;
        private bool _hasAccess;

        #endregion variable

        #region Request

        public ActionResult SearchTravelRequest()
        {
            Utility.SetSession("MenuRetention", "TRAVELANDCLAIMS");
            Utility.SetSession("SubMenuRetention", "TRAVELREQUEST");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Travel Request");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            TravelBO travelBo = new TravelBO
            {
                StatusList = _masterDataRepository.SearchMasterDataDropDown("Status", "Travel"),
                TravelTypeList = _masterDataRepository.SearchMasterDataDropDown("CodeMaster", "TravelType"),
                StatusId = _masterDataRepository.GetStatusId("Travel", "Pending"),
                EmployeeList = masterDataRepository.GetEmployeeDropDown("EmployeeMaster", Utility.UserId(), true)
            };
            if (Utility.TmpUserId() > 0)
            {
                travelBo.EmployeeId = Utility.TmpUserId();
                travelBo.RequestBy = "Other";
            }
            else
            {
                Utility.SetSession("TmpUserID", null);
                travelBo.EmployeeId = Utility.UserId();
                travelBo.RequestBy = "Self";
            }
            if (masterDataRepository.HasOtherUserAccess(Utility.UserId(), "Travel Request"))
            {
                travelBo.HasHRApplicationRole = true;
            }
            return View(travelBo);
        }

        public ActionResult TravelRequestList(TravelBO travelBo)
        {
            Utility.SetSession("TmpUserID", travelBo.EmployeeId);
            if (travelBo.EmployeeId == 0)
            {
                travelBo.EmployeeId = Utility.UserId();
            }
            return PartialView(_travelRepository.SearchTravelRequest(travelBo));
        }

        public ActionResult CreateTravelRequest(int id, int employeeId)
        {
            TravelBO travelBo = new TravelBO();
            if (employeeId == 0)
            {
                employeeId = Utility.UserId();
            }
            if (id != 0)
            {
                travelBo = _travelRepository.GetTravelRequest(id);
                travelBo.ApproverStatusId = _masterDataRepository.GetStatusId("Travel", "Pending");
            }
            else
            {
                travelBo.Approver = _masterDataRepository.GetApprover(employeeId, "Name");
                travelBo.ApproverId = Convert.ToInt32(_masterDataRepository.GetApprover(employeeId, "ID"));
                travelBo.SubordinatesList = new List<Subordinates> { new Subordinates { Name = string.Empty } };
            }
            travelBo.EmployeeId = employeeId;
            if (Utility.TmpUserId() > 0)
            {
                travelBo.EmployeeName = _masterDataRepository.GetEmployeeDetails(employeeId).FullName;
            }
            travelBo.StatusList = _masterDataRepository.SearchMasterDataDropDown("Status", "Travel");
            travelBo.TravelTypeList = _masterDataRepository.SearchMasterDataDropDown("CodeMaster", "TravelType");
            travelBo.CurrencyList = _masterDataRepository.SearchMasterDataDropDown("Currency");

            return PartialView(travelBo);
        }

        [HttpPost]
        public ActionResult CreateTravelRequest(TravelBO travelBo)
        {
            travelBo.MenuCode = MenuCode.TRAVELREQ;
            travelBo.EmployeeId = travelBo.EmployeeId;
            travelBo.CreatedBy = Utility.UserId();
            travelBo.ModifiedBy = Utility.UserId();
            travelBo.DomainId = Utility.DomainId();
            if (travelBo.StatusId == 0)
            {
                travelBo.StatusId = _masterDataRepository.GetStatusId("Travel", "Pending");
            }

            RbsRepository rbsRepository = new RbsRepository();
            _hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), travelBo.MenuCode, travelBo.Id == 0 ? "Save" : "Edit");

            if (travelBo.IsDeleted)
            {
                _hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), travelBo.MenuCode, "Delete");
            }

            if (!_hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            switch (travelBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    _message = _travelRepository.CreateTravelRequest(travelBo);
                    //if (Message.Contains("Successfully"))
                    //{
                    //    int leaveID = Convert.ToInt32(message.Split(',')[1]);
                    //    leaveManagementBO = leaveManagementRepository.GetEmailContent(leaveID);
                    //    email.SendEmail(leaveManagementBO.ApproverEmail, leaveManagementBO.EmployeeEmail,
                    //        leaveManagementRepository.PopulateLeaveMailBody(leaveManagementBO, "Request", string.Empty),
                    //        "Leave Request : " + leaveManagementBO.EmployeeCode + " ( " + leaveManagementBO.EmployeeName + " )");
                    //    Message = message.Split(',')[0];
                    //}
                    break;

                case Operations.Update:
                    _message = _travelRepository.UpdateTravelRequest(travelBo);
                    break;

                case Operations.Delete:
                    travelBo.StatusId = _masterDataRepository.GetStatusId("Travel", "Pending");
                    _message = _travelRepository.DeleteTravelRequest(travelBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubordinatesList(int travelRequestId)
        {
            return PartialView(_travelRepository.GetSubordinatesList(travelRequestId, Utility.DomainId()));
        }

        public ActionResult ValidateTravelRequestNo(int id, string travelReqNo)
        {
            return Json(_travelRepository.ValidateTravelRequestNo(id, travelReqNo), JsonRequestBehavior.AllowGet);
        }

        #endregion Request

        #region Approval

        public ActionResult SearchTravelRequestApproval()
        {
            Utility.SetSession("MenuRetention", "TRAVELANDCLAIMS");
            Utility.SetSession("SubMenuRetention", "TRAVELAPPROVAL");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Travel Approval");
            TravelBO travelBo = new TravelBO
            {
                EmployeeId = Utility.UserId(),
                StatusList = _masterDataRepository.SearchMasterDataDropDown("Status", "Travel"),
                TravelTypeList = _masterDataRepository.SearchMasterDataDropDown("CodeMaster", "TravelType"),
                StatusId = _masterDataRepository.GetStatusId("Travel", "Pending")
            };
            return View(travelBo);
        }

        public ActionResult TravelRequestApprovalList(TravelBO travelBo)
        {
            travelBo.EmployeeId = Utility.UserId();
            return PartialView(_travelRepository.SearchTravelRequestApproval(travelBo));
        }

        public ActionResult ApproveTravelRequest(int id)
        {
            TravelBO travelBo = new TravelBO();

            if (id != 0)
            {
                int approverStatusId = _masterDataRepository.GetStatusId("Travel", "Pending");
                travelBo = _travelRepository.GetTravelRequestApproval(id);
                if (travelBo.StatusId == approverStatusId)
                {
                    travelBo.StatusId = 0;
                    travelBo.ApproverStatusId = 0;
                }
            }
            else
            {
                travelBo.Approver = _masterDataRepository.GetApprover(Utility.UserId(), "Name");
                travelBo.ApproverId = Convert.ToInt32(_masterDataRepository.GetApprover(Utility.UserId(), "ID"));
            }

            travelBo.StatusList = _masterDataRepository.SearchMasterDataDropDown("Status", "Travel");
            travelBo.TravelTypeList = _masterDataRepository.SearchMasterDataDropDown("CodeMaster", "TravelType");
            travelBo.CurrencyList = _masterDataRepository.SearchMasterDataDropDown("Currency");

            return PartialView(travelBo);
        }

        [HttpPost]
        public ActionResult ApproveTravelRequest(TravelBO travelBo)
        {
            travelBo.MenuCode = MenuCode.TRAVELAPP;
            travelBo.EmployeeId = Utility.UserId();
            travelBo.CreatedBy = Utility.UserId();
            travelBo.ModifiedBy = Utility.UserId();
            travelBo.DomainId = Utility.DomainId();

            RbsRepository rbsRepository = new RbsRepository();
            _hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), travelBo.MenuCode, travelBo.Id == 0 ? "Save" : "Edit");

            if (!_hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            switch (travelBo.Operation.ToUpper())
            {
                case Operations.Approve:
                    travelBo.StatusId = _masterDataRepository.GetStatusId("Travel", "Approved");
                    _message = _travelRepository.ApproveTravelRequest(travelBo);
                    //if (Message.Contains("Successfully"))
                    //{
                    //    int leaveID = Convert.ToInt32(message.Split(',')[1]);
                    //    leaveManagementBO = leaveManagementRepository.GetEmailContent(leaveID);
                    //    email.SendEmail(leaveManagementBO.ApproverEmail, leaveManagementBO.EmployeeEmail,
                    //        leaveManagementRepository.PopulateLeaveMailBody(leaveManagementBO, "Request", string.Empty),
                    //        "Leave Request : " + leaveManagementBO.EmployeeCode + " ( " + leaveManagementBO.EmployeeName + " )");
                    //    Message = message.Split(',')[0];
                    //}
                    break;

                case Operations.Reject:
                    travelBo.StatusId = _masterDataRepository.GetStatusId("Travel", "Rejected");
                    _message = _travelRepository.RejectTravelRequest(travelBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        #endregion Approval
    }
}