﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class TDS_HRAController : Controller
    {
        #region TDS_HRADeclaration
        UtilityRepository utilityRepository = new UtilityRepository();

        public ActionResult TDS_HRADeclaration()
        {
            Utility.SetSession("MenuRetention", "TDS");
            Utility.SetSession("SubMenuRetention", "HRADECLARATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("HRA DECLARATION");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            TdsHrabo tdsDeclaration = new TdsHrabo
            {
                FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty)
            };
            tdsDeclaration.FinancialYearList = utilityRepository.GetFinancialYearText();
            return View(tdsDeclaration);
        }

        #endregion TDS_HRADeclaration

        #region SaveTDS_HRA

        [HttpPost]
        public ActionResult SaveTDS_HRA(IList<TdsHrabo> tdsHraboList)
        {
            string result = string.Empty;
            RbsRepository rbsRepository = new RbsRepository();
            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), tdsHraboList[0].MenuCode, tdsHraboList[0].Id == 0 ? "Save" : "Edit");
            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            foreach (TdsHrabo list in tdsHraboList)
            {
                result = _tdsHraRepository.ManageHraDeclare(list);
            }

            return Json(result);
        }

        #endregion SaveTDS_HRA

        #region TDS_HRAClarance

        public ActionResult TDS_HRAClarance()
        {
            Utility.SetSession("MenuRetention", "TDS");
            Utility.SetSession("SubMenuRetention", "HRACLEARANCE");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("HRA Clearance");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            TdsHrabo tdsDeclaration = new TdsHrabo
            {
                FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty)
            };
            tdsDeclaration.FinancialYearList = utilityRepository.GetFinancialYearText();
            return View(tdsDeclaration);
        }

        #endregion TDS_HRAClarance

        #region EmployeeList

        public ActionResult EmployeeList(TdsHrabo tdsHrabo)
        {
            return PartialView(_tdsHraRepository.SearchEmployee(tdsHrabo));
        }

        #endregion EmployeeList

        #region HRAClearanceList

        public ActionResult HraClearanceList(int? financialYearId, int? employeeId)
        {
            TdsHrabo tdsDeclaration = new TdsHrabo
            {
                EmployeeId = Convert.ToInt32(employeeId),
                FinancialYearId = Convert.ToInt32(financialYearId)
            };
            return PartialView(_tdsHraRepository.SearchTdsClearance(tdsDeclaration));
        }

        #endregion HRAClearanceList

        #region HRADeclarationList

        public ActionResult HraDeclarationList(int? financialYearId, int employeeId)
        {
            TdsHrabo tdsDeclaration = new TdsHrabo
            {
                EmployeeId = employeeId == 0 ? Utility.UserId() : employeeId,
                FinancialYearId = financialYearId
            };

            return PartialView(_tdsHraRepository.Search_TDSHRADeclaration(tdsDeclaration));
        }

        #endregion HRADeclarationList

        #region Search HRA Declaration

        public ActionResult SearchHraDeclaration()
        {
            Utility.SetSession("MenuRetention", "TDS");
            Utility.SetSession("SubMenuRetention", "HRADECLARATION-HR");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("HRA DECLARATION - HR");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            TdsHrabo tdsDeclaration = new TdsHrabo
            {
                FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty)
            };
            tdsDeclaration.FinancialYearList = utilityRepository.GetFinancialYearText();
            return View(tdsDeclaration);
        }

        #endregion Search HRA Declaration

        #region Variables

        private readonly TdsHraRepository _tdsHraRepository = new TdsHraRepository();

        #endregion Variables

        #region TDS IT Declaration Report

        public ActionResult TDS_SearchITDeclarationRPT()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            TdsHrabo tdsDeclaration = new TdsHrabo
            {
                FinancialYearList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty)
            };
            tdsDeclaration.FinancialYearList = utilityRepository.GetFinancialYearText();
            return PartialView(tdsDeclaration);
        }

        public ActionResult TDS_ITDeclarationRPTList(int financialyearId, string formType)
        {
            DataTable dataTable = _tdsHraRepository.TDSITDeclarationRPTList(financialyearId, formType);
            return PartialView(dataTable);
        }

        #endregion TDS IT Declaration Report

        #region TSD HRA Clearance Report

        public ActionResult TDS_SearchHRAClearanceRPT()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            TdsHrabo tdsDeclaration = new TdsHrabo
            {
                FinancialYearList = masterDataRepository.SearchMasterDataDropDown("Financialyear", string.Empty)
            };
            tdsDeclaration.FinancialYearList = utilityRepository.GetFinancialYearText();
            return PartialView(tdsDeclaration);
        }

        public ActionResult TDS_HRAClearanceRPTList(int financialyearId, string status)
        {
            DataTable dataTable = _tdsHraRepository.TDSHRAClearanceRPTList(financialyearId, status);
            return PartialView(dataTable);
        }

        #endregion TDS HRA Clearance Report
    }
}