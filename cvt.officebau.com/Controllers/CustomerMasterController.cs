﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class CustomerMasterController : Controller
    {
        #region Get Country List

        public JsonResult GetCountryList()
        {
            return Json(_customerMasterService.GetCountryList(), JsonRequestBehavior.AllowGet);
        }

        #endregion Get Country List

        #region Get State List

        public JsonResult GetStateList(int? countryId)
        {
            return Json(_customerMasterService.GetStateList(countryId), JsonRequestBehavior.AllowGet);
        }

        #endregion Get State List

        #region Get City List

        public JsonResult GetCityList(int? stateId)
        {
            return Json(_customerMasterService.GetCityList(stateId), JsonRequestBehavior.AllowGet);
        }

        #endregion Get City List

        #region Variable Declaration

        private CustomerBo _customerBo = new CustomerBo();
        private readonly CustomerMasterRepository _customerMasterService = new CustomerMasterRepository();
        private string _message = string.Empty;

        #endregion Variable Declaration

        #region Customer Master

        public ActionResult SearchCustomerMaster()
        {
            Utility.SetSession("MenuRetention", "SALES");
            Utility.SetSession("SubMenuRetention", "CUSTOMERMASTER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Customer Master");

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _customerBo.StatusList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Status");
            _customerBo.SalesPersonList = masterDataRepository.GetBusinessUnitBasedSalesExecutives(0, "SalesPerson"); //masterDataRepository.SearchEmpDropdownDesignation();
            _customerBo.UserId = Utility.UserId();
            return View(_customerBo);
        }

        public ActionResult CustomerMaster(CustomerBo customerBo)
        {
            customerBo.Operation = Operations.Search;
            customerBo.EmployeeId = Utility.UserId();
            return PartialView(_customerMasterService.GetCustomerList(customerBo));
        }

        #endregion Customer Master

        #region Create Customer Master

        public ActionResult CreateCustomerMaster(int id)
        {
            _customerBo.MenuCode = MenuCode.CustomerMaster;
            if (id != 0)
            {
                _customerBo = _customerMasterService.GetCustomer(id);
            }

            MasterDataRepository masterDataRepository = new MasterDataRepository();

            _customerBo.StatusList = masterDataRepository.SearchMasterDataDropDown("CodeMaster", "Status");
            _customerBo.CountryList = masterDataRepository.SearchMasterDataDropDown("Country", string.Empty);
            _customerBo.SalesPersonList = masterDataRepository.GetBusinessUnitBasedSalesExecutives(id, "SalesPerson");
            return PartialView(_customerBo);
        }

        [HttpPost]
        public ActionResult CreateCustomerMaster(CustomerBo customerBo)
        {
            customerBo.MenuCode = MenuCode.CustomerMaster;
            customerBo.ModifiedBy = Utility.UserId();
            customerBo.CreatedOn = Utility.GetCurrentDate();
            customerBo.ModifiedOn = Utility.GetCurrentDate();
            customerBo.DomainId = Utility.DomainId();
            customerBo.UserId = Utility.UserId();
            RbsRepository rbsRepository = new RbsRepository();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), customerBo.MenuCode, customerBo.Id == 0 ? "Save" : "Edit");

            if (customerBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), customerBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrWhiteSpace(customerBo.OrderName))
            {
                customerBo.OrderName = customerBo.EnquiryName;
            }

            switch (customerBo.Operation.ToUpper())
            {
                case Operations.Insert:
                    customerBo.CreatedBy = Utility.UserId();
                    customerBo.IsDeleted = false;

                    if (customerBo.StatusId == 0)
                    {
                        customerBo.StatusId = masterDataRepository.GetCodeMasterDataId("Status", "Active");
                    }

                    _message = _customerMasterService.CreateCustomerMaster(customerBo);
                    break;

                case Operations.Update:
                    customerBo.IsDeleted = false;
                    _message = _customerMasterService.UpdateCustomerMaster(customerBo);
                    break;

                case Operations.Delete:
                    customerBo.IsDeleted = true;
                    _message = _customerMasterService.DeleteCustomerMaster(customerBo);
                    break;
            }

            return Json(_message, JsonRequestBehavior.AllowGet);
        }

        #endregion Create Customer Master
    }

    [SessionExpire]
    public class CustomerController : Controller
    {
        #region Customer Master

        public ActionResult CustomerMaster(string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "MASTER");
            Utility.SetSession("SubMenuRetention", "CUSTOMERMASTER");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Account - Customer Master");

            return View(_customerRepository.SearchCustomer(_customerDetailsBo));
        }

        #endregion Customer Master

        #region Auto Complete

        public ActionResult AutoCompleteCity(string name)
        {
            CommonRepository commonRepository = new CommonRepository();
            return Json(commonRepository.SearchAutoComplete("City", "Name", name), JsonRequestBehavior.AllowGet);
        }

        #endregion Auto Complete

        #region Variables

        private readonly CustomerRepository _customerRepository = new CustomerRepository();
        private readonly CustomerDetailsBo _customerDetailsBo = new CustomerDetailsBo();       

        #endregion Variables

        #region Create Customer

        public ActionResult CreateCustomer(int id, string menuCode)
        {
            CustomerDetailsBo customerDetailsBo = _customerRepository.GetCustomer(id);          
            customerDetailsBo.MenuCode = menuCode;
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            customerDetailsBo.CurrencyList = masterDataRepository.SearchMasterDataDropDown("Currency");
            customerDetailsBo.BankList = masterDataRepository.GetBankNameBasedonPaymentMode("Cheque");
            return PartialView(customerDetailsBo);
        }

        [HttpPost]
        public ActionResult CreateCustomer(CustomerDetailsBo customerDetailsBo)
        {
            if (!string.IsNullOrWhiteSpace(customerDetailsBo.PanNo))
            {
                customerDetailsBo.PanNo = customerDetailsBo.PanNo.ToUpper();
            }

            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), customerDetailsBo.MenuCode, customerDetailsBo.Id == 0 ? "Save" : "Edit");

            if (customerDetailsBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), customerDetailsBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess);
            }

            string userMessage = _customerRepository.ManageCustomer(customerDetailsBo);
            return Json(userMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion Create Customer
    }
}