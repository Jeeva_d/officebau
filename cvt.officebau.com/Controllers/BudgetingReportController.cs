﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    public class BudgetingReportController : Controller
    {
        private readonly BudgetingRepository _budgetingRepository = new BudgetingRepository();

        [SessionExpire]
        public ActionResult Index(int? id, int? fyid, string menuCode)
        {
            if (!string.IsNullOrWhiteSpace(menuCode))
            {
                Utility.SetSession("MenuID", menuCode);
            }

            Utility.SetSession("MenuRetention", "MASTER");
            Utility.SetSession("SubMenuRetention", "BUDGETING");

            BudgetingBo budgetingBo = new BudgetingBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            if (id != null)
            {
                budgetingBo = _budgetingRepository.SearchBudgetingList(Convert.ToInt32(fyid)).FirstOrDefault(i => i.Id == id);
            }

            if (budgetingBo != null)
            {
                budgetingBo.FinancialYearId = Convert.ToInt32(fyid);
                budgetingBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");
                budgetingBo.FinancialYearList.RemoveAll(i => i.Text == "-- Select --");
                budgetingBo.BudgetNameList = masterDataRepository.SearchMasterDataDropDown("Ledger");
                budgetingBo.BudgetNameList = budgetingBo.BudgetNameList.OrderBy(a => a.Text).ToList();
            }
            return View(budgetingBo);
        }

        [HttpPost]
        public ActionResult Index(BudgetingBo budgetingBo)
        {
            return Json(_budgetingRepository.ManageBudgeting(budgetingBo));
        }

        public ActionResult GetBudgetingResult(int fyid)
        {
            return PartialView(_budgetingRepository.SearchBudgetingList(fyid));
        }

        public ActionResult DeleteBudget(int id)
        {
            BudgetingBo budgetingBo = new BudgetingBo
            {
                Id = id,
                IsDeleted = true
            };
            return Json(_budgetingRepository.ManageBudgeting(budgetingBo), JsonRequestBehavior.AllowGet);
        }

        public ActionResult BudgetingReport(int? fyid)
        {
            BudgetingBo budgetingBo = new BudgetingBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            budgetingBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear");
            budgetingBo.FinancialYearList.RemoveAll(i => i.Text == "-- Select --");
            return View(budgetingBo);
        }

        public ActionResult GetBudgetingReport(int fyid)
        {
            return PartialView(_budgetingRepository.SearchBudgetingReport(fyid));
        }
    }
}