﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class ClaimCategoryController : Controller
    {
        #region Variables

        private MasterDataBo _masterDataBo = new MasterDataBo();
        private readonly ClaimCategoryRepository _claimCategoryRepository = new ClaimCategoryRepository();

        #endregion Variables

        #region Claim Category

        public ActionResult ClaimCategory()
        {
            Utility.SetSession("MenuRetention", "MASTERDATA");
            Utility.SetSession("SubMenuRetention", "CLAIMCATEGORY");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Master Data");

            _masterDataBo.DomainId = Utility.DomainId();

            return PartialView(_claimCategoryRepository.GetClaimsCatergoryList(_masterDataBo));
        }

        public ActionResult CreateClaimCategory(int id)
        {
            if (id != 0)
            {
                _masterDataBo = _claimCategoryRepository.GetClaimsCategory(id);
            }

            return PartialView(_masterDataBo);
        }

        [HttpPost]
        public ActionResult CreateClaimCategory(MasterDataBo masterDataBo)
        {
            masterDataBo.ModifiedBy = Utility.UserId();
            masterDataBo.CreatedOn = Utility.GetCurrentDate();
            masterDataBo.ModifiedOn = Utility.GetCurrentDate();
            masterDataBo.DomainId = Utility.DomainId();
            masterDataBo.MenuCode = MenuCode.MasterData;

            RbsRepository rbsRepository = new RbsRepository();

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), masterDataBo.MenuCode, masterDataBo.Id == 0 ? "Save" : "Edit");

            if (masterDataBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), masterDataBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_claimCategoryRepository.ManageClaimCategory(masterDataBo), JsonRequestBehavior.AllowGet);
        }

        #endregion Claim Category
    }
}