﻿using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class ApplicationConfigurationController : Controller
    {
        #region Variables

        private readonly ApplicationConfigurationRepository _applicationConfigurationRepository = new ApplicationConfigurationRepository();
        private ApplicationConfigurationBo _applicationConfigurationBo;

        #endregion Variables

        #region Application Configuration

        public ActionResult ApplicationConfiguration()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "APPLICATIONCONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Application Configuration");

            MasterDataRepository masterDataRepository = new MasterDataRepository();
            _applicationConfigurationBo = new ApplicationConfigurationBo
            {
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };

            return View(_applicationConfigurationBo);
        }

        public ActionResult GetApplicationConfiguration(ApplicationConfigurationBo applicationConfigurationBo)
        {
            return PartialView(_applicationConfigurationRepository.GetApplicationConfiguration(applicationConfigurationBo));
        }

        [HttpPost]
        public ActionResult ManageApplicationConfiguration(List<ApplicationConfigurationBo> applicationConfigurationBoList)
        {
            RbsRepository rbsRepository = new RbsRepository();

            _applicationConfigurationBo = new ApplicationConfigurationBo
            {
                MenuCode = MenuCode.APPCONFIG
            };

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), _applicationConfigurationBo.MenuCode, applicationConfigurationBoList[0].Id == 0 ? "Save" : "Edit");

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_applicationConfigurationRepository.ManageApplicationConfiguration(applicationConfigurationBoList), JsonRequestBehavior.AllowGet);
        }

        #endregion Application Configuration

        #region Payroll Component Configuration

        public ActionResult PayrollComponentConfiguration()
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            ApplicationConfigurationBo applicationConfigurationBo = new ApplicationConfigurationBo
            {
                BusinessUnitList = masterDataRepository.GetBusinessUnitDropDown()
            };
            return View(applicationConfigurationBo);
        }

        public ActionResult GetPayrollComponentConfiguration(ApplicationConfigurationBo applicationConfigurationBo)
        {
            return PartialView(_applicationConfigurationRepository.GetPayrollComponentConfiguration(applicationConfigurationBo));
        }

        public ActionResult ManagePayrollComponentConfiguration(List<ApplicationConfigurationBo> applicationConfigurationBoList)
        {
            return Json(_applicationConfigurationRepository.ManagePayrollComponentConfiguration(applicationConfigurationBoList), JsonRequestBehavior.AllowGet);
        }

        #endregion Payroll Component Configuration

        #region Reports

        public ActionResult ApplicationConfigurationReport()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "REPORT");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("Business Access Mapping");

            return View();
        }

        #region Business Access Mapping Report

        public ActionResult SearchBusinessAccessReport()
        {
            return PartialView(new ApplicationConfigurationReportBo());
        }

        #endregion Business Access Mapping Report

        public ActionResult GetBusinessAccessReport(int employeeId)
        {
            return PartialView(_applicationConfigurationRepository.GetBusinessAccessReport(employeeId));
        }

        #region Employee Autocomplete based on BU

        public ActionResult AutoCompleteEmployee(string searchName)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            return Json(masterDataRepository.SearchAutoCompleteEmployee(searchName), JsonRequestBehavior.AllowGet);
        }

        #endregion Employee Autocomplete based on BU

        #endregion Reports

        #region Screen for Others User Access

        public ActionResult OtherUserAcess()
        {
            return PartialView(_applicationConfigurationRepository.GetOtherUserAcessList());
        }
        public ActionResult GetOtherUserAcess(int id, string desid, string name)
        {
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            MasterDataBo masterDataBo = new MasterDataBo
            {
                DependantDropDownList = masterDataRepository.SearchMasterDataDropDown("Designation", string.Empty),
                Id = id,
                Code = name,
                Parameters = desid
            };
            masterDataBo.DependantDropDownList.RemoveAt(0);
            return PartialView(masterDataBo);
        }
        public ActionResult SaveUserAccess(int id, string deslist)
        {
            return Json(_applicationConfigurationRepository.SaveOtherUserAccess(id, deslist), JsonRequestBehavior.AllowGet);
        }

        #endregion Screen for Others User Access

        #region Category Mapping 

        public ActionResult DocumentCategoryMapping()
        {
            Utility.SetSession("MenuRetention", "DOCUMENTMANAGEMENT");
            Utility.SetSession("SubMenuRetention", "DOCUMENTCATEGORYMAPPING");
            return PartialView(_applicationConfigurationRepository.GetCategoryMappingList());
        }

        /// <summary>
        /// The method is used to get the Catorgory Mappging details of an employee.
        /// </summary>
        /// <param name="employeeId"> Employee ID </param>
        /// <returns> List of Category Mapping </returns>
        public ActionResult GetDocumentCategoryMapping(int employeeId)
        {
            var masterDataRepository = new MasterDataRepository();
            var documentCategory = new DocumentManagerBO();
            var documentManagerBO = _applicationConfigurationRepository.GetDocumentCategoryMapping(employeeId);

            if (documentManagerBO == null)
            {
                documentCategory.DependantDropDownList = masterDataRepository.SearchMasterDataDropDown("DocumentCategory", string.Empty);
                documentCategory.DependantDropDownList.RemoveAt(0);
            }
            else
            {
                documentCategory = documentManagerBO;
                documentCategory.DependantDropDownList = masterDataRepository.SearchMasterDataDropDown("DocumentCategory", string.Empty);

                foreach (var a in documentCategory.DependantDropDownList)
                {
                    a.Selected = documentManagerBO.CategoryId.Contains(a.Value);
                }
                documentCategory.DependantDropDownList.RemoveAt(0);
            }
            return PartialView(documentCategory);
        }

        /// <summary>
        /// The method saves the Category for an Employee.
        /// </summary>
        /// <param name="id"> </param>
        /// <param name="EmployeeId"></param>
        /// <param name="CategoryIds"></param>
        /// <returns></returns>
        public ActionResult SaveDocumentCategory(int id, int employeeId, string categoryIds)
        {
            var documentManagerBO = new DocumentManagerBO
            {
                Id = id,
                EmployeeId = employeeId,
                CategoryId = categoryIds
            };
            return Json(_applicationConfigurationRepository.SaveDocumentCategoryMapping(documentManagerBO), JsonRequestBehavior.AllowGet);
        }

        #endregion Category Mapping
    }
}