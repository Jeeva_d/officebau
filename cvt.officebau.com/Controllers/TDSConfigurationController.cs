﻿using System.Collections.Generic;
using System.Web.Mvc;
using cvt.officebau.com.Utilities;
using cvt.officebau.com.Services;
using cvt.officebau.com.ViewModels;

namespace cvt.officebau.com.Controllers
{
    [SessionExpire]
    public class TDSConfigurationController : Controller
    {
        #region Variables

        private readonly TdsConfigurationRepository _tdsConfigurationRepository = new TdsConfigurationRepository();

        #endregion Variables

        #region TDS Configuration

        public ActionResult TdsConfiguration()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "TDSCONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("TDS Configuration");

            RbsRepository rBsRepository = new RbsRepository();
            return View(rBsRepository.SearchMenuForConfiguration());
        }

        public ActionResult SearchTdsConfiguration()
        {
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("TDS Component Configuration");

            TdsConfigurationBo tdsConfigurationBo = new TdsConfigurationBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            tdsConfigurationBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            UtilityRepository utilityRepository = new UtilityRepository();
            tdsConfigurationBo.FinancialYearList = utilityRepository.GetFinancialYearText();
            tdsConfigurationBo.RegionList = masterDataRepository.GetRegionDropDownDependOnEmployee(Utility.UserId());
            return PartialView(tdsConfigurationBo);
        }

        public ActionResult TdsConfigurationList(int fyid, int regionId)
        {
            TdsConfigurationBo tdsConfigurationBo = new TdsConfigurationBo
            {
                FinancialYearId = fyid,
                RegionId = regionId
            };

            return PartialView(_tdsConfigurationRepository.SearchTdsLimitConfiguration(tdsConfigurationBo));
        }

        public ActionResult ManageTdsConfiguration(int id)
        {
            TdsConfigurationBo tdsConfigurationBo = _tdsConfigurationRepository.GetTdsLimitConfiguration(id);
            MasterDataRepository masterDataRepository = new MasterDataRepository();
            tdsConfigurationBo.FinancialYearList = masterDataRepository.SearchMasterDataDropDown("FinancialYear", string.Empty);
            UtilityRepository utilityRepository = new UtilityRepository();
            tdsConfigurationBo.FinancialYearList = utilityRepository.GetFinancialYearText();
            tdsConfigurationBo.SectionList = masterDataRepository.SearchMasterDataDropDown("TDSSection", string.Empty);
            tdsConfigurationBo.RegionList = masterDataRepository.GetRegionDropDownDependOnEmployee(Utility.UserId());
            return PartialView(tdsConfigurationBo);
        }

        [HttpPost]
        public ActionResult ManageTdsConfiguration(TdsConfigurationBo tdsConfigurationBo)
        {
            RbsRepository rbsRepository = new RbsRepository();
            tdsConfigurationBo.MenuCode = MenuCode.TDSLimitConfiguration;

            bool hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), tdsConfigurationBo.MenuCode, tdsConfigurationBo.Id == 0 ? "Save" : "Edit");
            if (tdsConfigurationBo.IsDeleted)
            {
                hasAccess = rbsRepository.IsAuthorized(Utility.UserId(), tdsConfigurationBo.MenuCode, "Delete");
            }

            if (!hasAccess)
            {
                return Json(Constants.UnAuthorizedAccess, JsonRequestBehavior.AllowGet);
            }

            return Json(_tdsConfigurationRepository.ManageTdsLimitConfiguration(tdsConfigurationBo), JsonRequestBehavior.AllowGet);
        }

        #endregion TDS Configuration

        #region TDS Configuration Payroll Components

        public ActionResult TdsConfigurationPayrollComponents()
        {
            Utility.SetSession("MenuRetention", "APPLICATIONCONFIGURATION");
            Utility.SetSession("SubMenuRetention", "CONFIGURATION");
             UtilityRepository track= new UtilityRepository(); track.TrackFormNavigation("TDS Configuration");

            TdsConfigurationBo tdsConfigurationBo = new TdsConfigurationBo();
            MasterDataRepository masterDataRepository = new MasterDataRepository();

            tdsConfigurationBo = _tdsConfigurationRepository.GetTdsConfiguration();
            tdsConfigurationBo.PayrollComponentList = masterDataRepository.SearchMasterDataDropDown("Pay_PayrollCompontents", string.Empty);
            return PartialView(tdsConfigurationBo);
        }

        [HttpPost]
        public JsonResult SubmitTdsConfiguration(TdsConfigurationBo tdsConfigurationBo)
        {
            List<TdsConfigurationBo> list = new List<TdsConfigurationBo>();
            if (tdsConfigurationBo != null)
            {
                TdsConfigurationBo tdsConfigurationListBo = new TdsConfigurationBo();
                list.Add(tdsConfigurationListBo);
                list[0].Values = "CTC";
                list[0].PayrollComponentID = tdsConfigurationBo.CTCComponentID;

                tdsConfigurationListBo = new TdsConfigurationBo();
                list.Add(tdsConfigurationListBo);
                list[1].Values = "BASIC";
                list[1].PayrollComponentID = tdsConfigurationBo.BasicComponentID;

                //tdsConfigurationListBo = new TdsConfigurationBo();
                //list.Add(tdsConfigurationListBo);
                //list[2].Values = "MEDICAL";
                //list[2].PayrollComponentID = tdsConfigurationBo.MedicalComponentID;

                //tdsConfigurationListBo = new TdsConfigurationBo();
                //list.Add(tdsConfigurationListBo);
                //list[3].Values = "CONVEYANCE";
                //list[3].PayrollComponentID = tdsConfigurationBo.ConveyanceComponentID;

                tdsConfigurationListBo = new TdsConfigurationBo();
                list.Add(tdsConfigurationListBo);
                list[2].Values = "HRA";
                list[2].PayrollComponentID = tdsConfigurationBo.HRAComponentID;
            }

            if (tdsConfigurationBo != null)
            {
                tdsConfigurationBo = _tdsConfigurationRepository.ManageTdsConfiguration(list);
            }
            return Json(tdsConfigurationBo.UserMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion TDS Configuration
    }
}