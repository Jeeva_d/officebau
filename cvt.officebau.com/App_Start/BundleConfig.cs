﻿
using System.Web.Optimization;

namespace cvt.officebau.com
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/plugins/jQuery/jquery-2.2.3.min.js", "~/plugins/jQueryUI/jquery-ui.min.js", "~/plugins/datepicker/bootstrap-datepicker.js",
                "~/plugins/slimScroll/jquery.slimscroll.min.js", "~/Content/dist/js/app.js", "~/Scripts/bootstrap-notify.js"
              , "~/Content/js/bootstrap.min.js", "~/plugins/datatables/jquery.dataTables.min.js", "~/plugins/datatables/dataTables.bootstrap.min.js"
              , "~/Scripts/PageResize.js", "~/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js", "~/Content/Utility/Common.js"
              , "~/Content/Landing-Page/js/wow.min.js", "~/Content/Landing-Page/js/particles.js", "~/Content/Landing-Page/js/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/plugins/datatables/dataTables.bootstrap.css",
                "~/Content/css/bootstrap.min.css", "~/Content/dist/css/AdminLTE.css", "~/Content/dist/css/skins/_all-skins.min.css", "~/Content/dist/css/materialdesign.css", "~/Content/dist/css/waveeffect.css"
              , "~/plugins/datepicker/datepicker3.css", "~/Content/themes/base/jquery-ui.css", "~/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css"
              , "~/Content/Utility/Common.css"));
            BundleTable.EnableOptimizations = false;
        }
    }
}
