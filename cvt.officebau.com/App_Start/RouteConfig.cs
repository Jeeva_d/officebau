﻿using System.Web.Mvc;
using System.Web.Routing;

namespace cvt.officebau.com
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new {controller = "UserLogin", action = "UserLogin", id = UrlParameter.Optional}
            );
        }
    }
}
