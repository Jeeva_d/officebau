﻿var private_const_browser_version_checkBrowser = new private_browser_version_checkBrowser();
function private_browser_version_checkBrowser() {
    debugger;
    this.usrAgent = navigator.userAgent;
    this.ie11 = (this.usrAgent.indexOf("Trident/7.0") > -1) ? 1 : 0;
    this.ie10 = (this.usrAgent.indexOf("Trident/6.0") > -1) ? 1 : 0;
    this.ie9 = (this.usrAgent.indexOf("Trident/5.0") > -1) ? 1 : 0;
    this.ie8 = (this.usrAgent.indexOf("Trident/4.0") > -1) ? 1 : 0;
    this.ie7 = !this.ie11 && !this.ie10 && !this.ie9 && !this.ie8 && (this.usrAgent.indexOf("MSIE 7") > -1) ? 1 : 0;
    this.is7OrNewer = (this.ie7 || this.ie8 || this.ie9 || this.ie10 || this.ie11) ? 1 : 0;
    this.is8OrNewer = (this.ie8 || this.ie9 || this.ie10 || this.ie11) ? 1 : 0;
    this.is9OrNewer = (this.ie9 || this.ie10 || this.ie11) ? 1 : 0;
    this.is8OrOlder = (this.ie7 || this.ie8) ? 1 : 0;
    this.edge = (this.usrAgent.indexOf("Edge/") > -1) ? 1 : 0;
    this.firefox = (this.usrAgent.indexOf("Firefox/") > -1) ? 1 : 0;
    this.chrome = (this.usrAgent.indexOf("Chrome") > -1) ? 1 : 0;
    this.sf4 = !this.chrome && (this.usrAgent.indexOf("Safari") > -1 && this.usrAgent.indexOf("Version/4") > -1) ? 1 : 0;
    this.sf5 = !this.chrome && (this.usrAgent.indexOf("Safari") > -1 && this.usrAgent.indexOf("Version/5") > -1) ? 1 : 0;
    this.sf6 = !this.chrome && (this.usrAgent.indexOf("Safari") > -1 && this.usrAgent.indexOf("Version/6") > -1) ? 1 : 0;
    this.sf7 = !this.chrome && (this.usrAgent.indexOf("Safari") > -1 && this.usrAgent.indexOf("Version/7") > -1) ? 1 : 0;
    this.safari = !this.chrome && (this.usrAgent.indexOf("Safari") > -1) ? 1 : 0;
    this.sf5OrOlder = (this.sf4 || this.sf5) ? 1 : 0;
    this.qboMac1 = this.usrAgent.indexOf("AppleWebKit") && this.usrAgent.indexOf("IntuitEmbedded") && this.usrAgent.indexOf("QB-Cloud/1.0") ? 1 : 0;
    this.webkit = (this.chrome || this.safari || this.qboMac1) ? 1 : 0;
    this.macOS = ((navigator.platform != null) && (navigator.platform.indexOf("Mac") > -1)) ? 1 : 0;
    this.winOS = ((navigator.platform != null) && (navigator.platform.indexOf("Win") > -1)) ? 1 : 0;
    this.ipad = ((navigator.platform != null) && (navigator.platform.indexOf("iPad") > -1)) ? 1 : 0;
    this.ipod = ((navigator.platform != null) && (navigator.platform.indexOf("iPod") > -1)) ? 1 : 0;
    this.iphone = ((navigator.platform != null) && (navigator.platform.indexOf("iPhone") > -1)) ? 1 : 0;
    this.andriod = ((navigator.platform != null) && (navigator.platform.indexOf("Andriod") > -1)) ? 1 : 0;
    this.blackberry = ((navigator.platform != null) && (navigator.platform.indexOf("Blackberry") > -1)) ? 1 : 0;
    this.mobile = (this.ipad || this.iphone || this.ipod || this.andriod || this.blackberry) ? 1 : 0;
    return this;
}

function cu_browser_version_isIE_10(a) {
    return (private_const_browser_version_checkBrowser.ie10 || private_const_browser_version_checkBrowser.ie11)
}

function cu_browser_version_isIE_9_orNewer(a) {
    return (private_const_browser_version_checkBrowser.is9OrNewer)
}

function cu_browser_version_isIE_7_orNewer(a) {
    return (private_const_browser_version_checkBrowser.is7OrNewer)
}

function cu_browser_version_isIE_7(a) {
    return (private_const_browser_version_checkBrowser.ie7)
}

function cu_browser_version_isIE_8(a) {
    return (private_const_browser_version_checkBrowser.ie8)
}

function cu_browser_version_is_valid_IE_browser(a) {
    return (private_const_browser_version_checkBrowser.is7OrNewer)
}

function cu_browser_version_is_valid_browser(a) {
    return (private_const_browser_version_checkBrowser.is7OrNewer || private_const_browser_version_checkBrowser.firefox || private_const_browser_version_checkBrowser.webkit)
}

function cu_browser_version_is_valid_non_IE_browser() {
    return (private_const_browser_version_checkBrowser.firefox || private_const_browser_version_checkBrowser.webkit)
}

function cu_browser_version_is_valid_FF_browser(a) {
    return private_const_browser_version_checkBrowser.firefox
}

function cu_browser_version_is_valid_FF_Mac(a) {
    return cu_browser_version_is_valid_FF_browser(null) && cu_browser_version_OS_is_MacOS(null)
}

function cu_browser_version_is_valid_SF_browser(a) {
    return (private_const_browser_version_checkBrowser.safari)
}

function cu_browser_version_is_valid_SF_5_orOlder(a) {
    return (private_const_browser_version_checkBrowser.sf5OrOlder)
}

function cu_browser_version_is_valid_Chrome_browser(a) {
    return private_const_browser_version_checkBrowser.chrome
}

function cu_browser_version_is_valid_webkit_browser(a) {
    return private_const_browser_version_checkBrowser.webkit
}

function cu_browser_version_OS_is_MacOS(a) {
    return private_const_browser_version_checkBrowser.macOS
}

function cu_browser_version_OS_is_Win(a) {
    return private_const_browser_version_checkBrowser.winOS
}

function cu_browser_version_get_IE_type(a) {
    var b = navigator.userAgent;
    if (b.indexOf("Windows NT 6") >= 0) {
        return "IE7VISTA"
    } else {
        if (private_const_browser_version_checkBrowser.ie8) {
            return "IE8"
        } else {
            if (private_const_browser_version_checkBrowser.ie7) {
                return "IE7"
            } else {
                return "UNKNOWN"
            }
        }
    }
}

function cu_browser_version_IE_TYPE_IS(a, d) {
    var b = cu_browser_version_get_IE_type(a);
    var e = d.replace(/^\s*/, "").replace(/\s$/, "").split(/\s*,\s*/);
    if (e != null && e.length > 0) {
        for (var c = 0; c < e.length; c++) {
            if (b == e[c]) {
                return true
            }
        }
    }
    return false
}

function cu_browser_version_isAOL(a) {
    return (navigator.userAgent.indexOf("AOL") >= 0)
}

function cu_browser_version_is_valid_mobile_browser(a) {
    return private_const_browser_version_checkBrowser.mobile
}
var cu_browser_version_js_loaded = true;









