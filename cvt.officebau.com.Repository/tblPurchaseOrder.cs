//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cvt.officebau.com.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPurchaseOrder
    {
        public tblPurchaseOrder()
        {
            this.tblPODetails = new HashSet<tblPODetail>();
        }
    
        public int ID { get; set; }
        public System.DateTime Date { get; set; }
        public int VendorID { get; set; }
        public string Remarks { get; set; }
        public string PONo { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int DomainID { get; set; }
        public System.Guid HistoryID { get; set; }
        public string Reference { get; set; }
        public Nullable<int> CostCenterID { get; set; }
    
        public virtual ICollection<tblPODetail> tblPODetails { get; set; }
    }
}
