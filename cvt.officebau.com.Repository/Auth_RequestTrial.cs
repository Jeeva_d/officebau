//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cvt.officebau.com.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Auth_RequestTrial
    {
        public int ID { get; set; }
        public string CompanyName { get; set; }
        public string EmailID { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> ActivatedOn { get; set; }
        public Nullable<bool> IsEmailSend { get; set; }
        public string AppPassword { get; set; }
        public Nullable<bool> IsReject { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.Guid> HistoryID { get; set; }
        public Nullable<int> IndustryID { get; set; }
        public Nullable<int> TeamSize { get; set; }
    }
}
