//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cvt.officebau.com.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class TDSHouseTax
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public string Component { get; set; }
        public string Type { get; set; }
        public Nullable<decimal> Declaration { get; set; }
        public Nullable<decimal> Submitted { get; set; }
        public string Remarks { get; set; }
        public Nullable<decimal> Cleared { get; set; }
        public Nullable<decimal> Rejected { get; set; }
        public string ApproverRemarks { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public bool IsDeleted { get; set; }
        public int DomainID { get; set; }
        public System.Guid HistoryID { get; set; }
        public Nullable<int> FinancialYearID { get; set; }
    }
}
