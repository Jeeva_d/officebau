//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cvt.officebau.com.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblBR
    {
        public int ID { get; set; }
        public Nullable<int> BankID { get; set; }
        public Nullable<int> SourceID { get; set; }
        public Nullable<System.DateTime> SourceDate { get; set; }
        public Nullable<int> SourceType { get; set; }
        public Nullable<bool> IsReconsiled { get; set; }
        public string BRSDescription { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.DateTime> ReconsiledDate { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int DomainID { get; set; }
        public System.Guid HistoryID { get; set; }
        public Nullable<decimal> AvailableBalance { get; set; }
    }
}
