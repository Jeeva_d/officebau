//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cvt.officebau.com.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_LopDetails
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int MonthID { get; set; }
        public int Year { get; set; }
        public decimal LopDays { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public int DomainId { get; set; }
        public bool IsManual { get; set; }
    }
}
