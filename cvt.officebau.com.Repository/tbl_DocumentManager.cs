//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cvt.officebau.com.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_DocumentManager
    {
        public int Id { get; set; }
        public int DocumentCategoryId { get; set; }
        public Nullable<System.DateTime> ExpiryDate { get; set; }
        public bool Active { get; set; }
        public string Description { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int DomainId { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid HistoryId { get; set; }
        public string DocumentName { get; set; }
    
        public virtual tbl_DocumentCategory tbl_DocumentCategory { get; set; }
    }
}
