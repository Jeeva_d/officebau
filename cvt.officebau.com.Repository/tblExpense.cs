//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cvt.officebau.com.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblExpense
    {
        public tblExpense()
        {
            this.tblExpenseDetails = new HashSet<tblExpenseDetail>();
            this.tblExpensePaymentMappings = new HashSet<tblExpensePaymentMapping>();
        }
    
        public int ID { get; set; }
        public System.DateTime Date { get; set; }
        public int VendorID { get; set; }
        public string Remarks { get; set; }
        public System.DateTime DueDate { get; set; }
        public string BillNo { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public string Type { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int DomainID { get; set; }
        public System.Guid HistoryID { get; set; }
        public string AutoGeneratedNo { get; set; }
        public Nullable<int> CostCenterID { get; set; }
        public bool IsApprovalRequired { get; set; }
        public bool IsApproved { get; set; }
        public string Tag { get; set; }
    
        public virtual ICollection<tblExpenseDetail> tblExpenseDetails { get; set; }
        public virtual ICollection<tblExpensePaymentMapping> tblExpensePaymentMappings { get; set; }
    }
}
