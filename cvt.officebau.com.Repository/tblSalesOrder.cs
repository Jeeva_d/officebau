//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cvt.officebau.com.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSalesOrder
    {
        public tblSalesOrder()
        {
            this.tblSOItems = new HashSet<tblSOItem>();
        }
    
        public int ID { get; set; }
        public System.DateTime Date { get; set; }
        public int CustomerID { get; set; }
        public string Description { get; set; }
        public string SONo { get; set; }
        public Nullable<int> StatusID { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int DomainID { get; set; }
        public System.Guid HistoryID { get; set; }
        public Nullable<decimal> CurrencyRate { get; set; }
        public string BillAddress { get; set; }
        public string AutoGeneratedNo { get; set; }
    
        public virtual ICollection<tblSOItem> tblSOItems { get; set; }
    }
}
