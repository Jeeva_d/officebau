//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace cvt.officebau.com.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Pay_PayrollCompontents
    {
        public tbl_Pay_PayrollCompontents()
        {
            this.tbl_Pay_EmployeePayrollDetails = new HashSet<tbl_Pay_EmployeePayrollDetails>();
            this.tbl_Pay_EmployeePayStructureDetails = new HashSet<tbl_Pay_EmployeePayStructureDetails>();
            this.tbl_Pay_PayrollCompanyPayStructure = new HashSet<tbl_Pay_PayrollCompanyPayStructure>();
        }
    
        public int ID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Nullable<int> Ordinal { get; set; }
        public bool IsDeleted { get; set; }
        public int DomainID { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public System.Guid HistoryID { get; set; }
    
        public virtual ICollection<tbl_Pay_EmployeePayrollDetails> tbl_Pay_EmployeePayrollDetails { get; set; }
        public virtual ICollection<tbl_Pay_EmployeePayStructureDetails> tbl_Pay_EmployeePayStructureDetails { get; set; }
        public virtual ICollection<tbl_Pay_PayrollCompanyPayStructure> tbl_Pay_PayrollCompanyPayStructure { get; set; }
    }
}
